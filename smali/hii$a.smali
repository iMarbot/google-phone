.class public final enum Lhii$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhii;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lhii$a;

.field public static final enum b:Lhii$a;

.field public static final enum c:Lhii$a;

.field public static final enum d:Lhii$a;

.field public static final enum e:Lhii$a;

.field public static final f:Lhby;

.field private static synthetic h:[Lhii$a;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lhii$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lhii$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhii$a;->a:Lhii$a;

    .line 14
    new-instance v0, Lhii$a;

    const-string v1, "BUG_FOOD"

    invoke-direct {v0, v1, v3, v3}, Lhii$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhii$a;->b:Lhii$a;

    .line 15
    new-instance v0, Lhii$a;

    const-string v1, "FISH_FOOD"

    invoke-direct {v0, v1, v4, v4}, Lhii$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhii$a;->c:Lhii$a;

    .line 16
    new-instance v0, Lhii$a;

    const-string v1, "DOG_FOOD"

    invoke-direct {v0, v1, v5, v5}, Lhii$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhii$a;->d:Lhii$a;

    .line 17
    new-instance v0, Lhii$a;

    const-string v1, "RELEASE"

    invoke-direct {v0, v1, v6, v6}, Lhii$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhii$a;->e:Lhii$a;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lhii$a;

    sget-object v1, Lhii$a;->a:Lhii$a;

    aput-object v1, v0, v2

    sget-object v1, Lhii$a;->b:Lhii$a;

    aput-object v1, v0, v3

    sget-object v1, Lhii$a;->c:Lhii$a;

    aput-object v1, v0, v4

    sget-object v1, Lhii$a;->d:Lhii$a;

    aput-object v1, v0, v5

    sget-object v1, Lhii$a;->e:Lhii$a;

    aput-object v1, v0, v6

    sput-object v0, Lhii$a;->h:[Lhii$a;

    .line 19
    new-instance v0, Lhij;

    invoke-direct {v0}, Lhij;-><init>()V

    sput-object v0, Lhii$a;->f:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lhii$a;->g:I

    .line 12
    return-void
.end method

.method public static a(I)Lhii$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 9
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhii$a;->a:Lhii$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhii$a;->b:Lhii$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhii$a;->c:Lhii$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lhii$a;->d:Lhii$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lhii$a;->e:Lhii$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static values()[Lhii$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhii$a;->h:[Lhii$a;

    invoke-virtual {v0}, [Lhii$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhii$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhii$a;->g:I

    return v0
.end method
