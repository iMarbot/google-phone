.class public final Ldmx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjx;


# static fields
.field private static j:Lhjr;


# instance fields
.field public final a:Lbjy;

.field public final b:Lbew;

.field public c:J

.field public d:Z

.field public e:[B

.field public f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

.field public g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

.field public h:Lde/dreamchip/dreamstream/DreamVideo;

.field public i:I

.field private k:Z

.field private l:Landroid/hardware/camera2/CameraManager;

.field private m:Landroid/view/SurfaceView;

.field private n:Lhqm;

.field private o:Lhqd;

.field private p:Landroid/hardware/Camera;

.field private q:Landroid/view/SurfaceHolder$Callback;

.field private r:Lhqd$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 297
    new-instance v0, Lhjr;

    const/4 v1, 0x4

    const/16 v2, 0x4e20

    const/4 v3, 0x1

    const/16 v4, 0x1f4

    const/4 v5, 0x3

    const/16 v6, 0x3a98

    invoke-direct/range {v0 .. v6}, Lhjr;-><init>(IIIIII)V

    sput-object v0, Ldmx;->j:Lhjr;

    return-void
.end method

.method constructor <init>(Lbew;ZZLbjy;Lde/dreamchip/dreamstream/DreamVideo;Landroid/hardware/camera2/CameraManager;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldmx;->c:J

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Ldmx;->i:I

    .line 4
    new-instance v0, Ldmz;

    invoke-direct {v0, p0}, Ldmz;-><init>(Ldmx;)V

    iput-object v0, p0, Ldmx;->q:Landroid/view/SurfaceHolder$Callback;

    .line 5
    new-instance v0, Lhqd$a;

    invoke-direct {v0, p0}, Lhqd$a;-><init>(Ldmx;)V

    iput-object v0, p0, Ldmx;->r:Lhqd$a;

    .line 6
    invoke-static {}, Lbdf;->b()V

    .line 7
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbew;

    iput-object v0, p0, Ldmx;->b:Lbew;

    .line 8
    iput-boolean p2, p0, Ldmx;->k:Z

    .line 9
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjy;

    iput-object v0, p0, Ldmx;->a:Lbjy;

    .line 10
    iput-boolean p3, p0, Ldmx;->d:Z

    .line 11
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lde/dreamchip/dreamstream/DreamVideo;

    iput-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 12
    invoke-static {p6}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Ldmx;->l:Landroid/hardware/camera2/CameraManager;

    .line 13
    return-void
.end method

.method private final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    .line 207
    packed-switch p1, :pswitch_data_0

    .line 216
    const-string v0, "Codec selected which is not supported by DCT lib"

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 208
    :pswitch_0
    const-string v0, "setEncoderParams"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/16 v2, 0x9

    const/4 v3, 0x1

    .line 209
    invoke-virtual {v1, v2, v5, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(IIZF)I

    move-result v1

    .line 210
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 215
    :goto_0
    return-void

    .line 212
    :pswitch_1
    const-string v0, "setEncoderParams"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v2, 0x5

    const/4 v3, 0x0

    .line 213
    invoke-virtual {v1, v2, v5, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(IIZF)I

    move-result v1

    .line 214
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/rcs/client/session/rtp/RtpMedia;[B)V
    .locals 7

    .prologue
    .line 288
    new-instance v0, Lges;

    const/4 v1, 0x1

    sget-object v2, Lget;->a:Lget;

    invoke-direct {v0, v1, v2, p1}, Lges;-><init>(ILget;[B)V

    .line 290
    const-string v1, "crypto"

    iget v2, v0, Lges;->a:I

    iget-object v3, v0, Lges;->b:Lget;

    .line 291
    iget-object v3, v3, Lget;->b:Ljava/lang/String;

    .line 292
    iget-object v4, v0, Lges;->d:Ljava/lang/String;

    iget-object v0, v0, Lges;->c:[B

    const/4 v5, 0x3

    .line 293
    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294
    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/session/Media;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->makeSecure()V

    .line 296
    return-void
.end method

.method private static a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 277
    new-instance v0, Lbjv;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Error while doing: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbjv;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 279
    const-string v0, "VP8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    .line 281
    :cond_0
    const-string v0, "H264"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    const/4 v0, 0x0

    goto :goto_0

    .line 283
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private final b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 217
    packed-switch p1, :pswitch_data_0

    .line 227
    const-string v0, "Codec selected which is not supported by DCT lib"

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 218
    :pswitch_0
    const-string v0, "setQualityMapping"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    sget-object v2, Lhjq;->b:[I

    .line 219
    invoke-static {}, Lhjq;->a()[F

    move-result-object v3

    const/4 v4, 0x0

    .line 220
    invoke-virtual {v1, v5, v2, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(I[I[F[I)I

    move-result v1

    .line 221
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 226
    :goto_0
    return-void

    .line 223
    :pswitch_1
    const-string v0, "setQualityMapping"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    sget-object v2, Lhjq;->c:[I

    sget-object v3, Lhjq;->a:[F

    sget-object v4, Lhjq;->d:[I

    .line 224
    invoke-virtual {v1, v5, v2, v3, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(I[I[F[I)I

    move-result v1

    .line 225
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static i()[B
    .locals 2

    .prologue
    .line 284
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 285
    const/16 v1, 0x1e

    new-array v1, v1, [B

    .line 286
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 287
    return-object v1
.end method

.method private final j()V
    .locals 6

    .prologue
    .line 228
    iget-object v0, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getExtensions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgex;

    .line 230
    iget-object v1, v0, Lgex;->a:Ljava/lang/String;

    .line 232
    const-string v3, "urn:3gpp:video-orientation"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 233
    const/4 v1, 0x1

    .line 238
    :goto_1
    if-gez v1, :cond_2

    .line 239
    const-string v1, "VideoShareSessionImpl.setDreamVideoExtensions"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "skipping unexpected extension "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 234
    :cond_0
    const-string v3, "urn:jibe:video-quality-level"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 235
    const/4 v1, 0x2

    goto :goto_1

    .line 236
    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    .line 241
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "extension supported by peer: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    const-string v3, "setExtensionId"

    iget-object v4, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 244
    iget v0, v0, Lgex;->b:I

    .line 245
    invoke-virtual {v4, v1, v0}, Lde/dreamchip/dreamstream/DreamVideo;->a(II)I

    move-result v0

    .line 246
    invoke-static {v3, v0}, Ldmx;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 248
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Ldmx;->c:J

    return-wide v0
.end method

.method public final a(Landroid/view/SurfaceView;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x2

    const/4 v8, 0x1

    .line 24
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    iget-object v0, p0, Ldmx;->m:Landroid/view/SurfaceView;

    if-ne p1, v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Ldmx;->m:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Ldmx;->m:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v2, p0, Ldmx;->q:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 29
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 30
    invoke-virtual {v0, v4, v1, v8}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 32
    :cond_1
    invoke-virtual {p1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    .line 33
    iget-object v0, p0, Ldmx;->q:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 34
    iget-boolean v0, p0, Ldmx;->k:Z

    if-eqz v0, :cond_6

    .line 35
    iget-object v0, p0, Ldmx;->o:Lhqd;

    if-eqz v0, :cond_4

    .line 36
    iget-object v0, p0, Ldmx;->o:Lhqd;

    .line 37
    iget v0, v0, Lhqd;->e:I

    .line 39
    iget-object v3, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v3}, Lhqd;->b()V

    .line 40
    iget-object v3, p0, Ldmx;->o:Lhqd;

    .line 41
    iput-object v4, v3, Lhqd;->f:Lhqd$a;

    .line 46
    :goto_1
    const-string v3, "VideoShareSessionImpl.setSurfaceView"

    const-string v4, "creating CameraMediaSource: %s, %s"

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 47
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    aput-object p1, v5, v8

    .line 48
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    new-instance v3, Lhqd;

    invoke-direct {v3, v0, p1}, Lhqd;-><init>(ILandroid/view/SurfaceView;)V

    iput-object v3, p0, Ldmx;->o:Lhqd;

    .line 50
    iget-object v0, p0, Ldmx;->o:Lhqd;

    iget-object v3, p0, Ldmx;->r:Lhqd$a;

    .line 51
    iput-object v3, v0, Lhqd;->f:Lhqd$a;

    .line 52
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->a()V

    .line 61
    :cond_2
    :goto_2
    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v0, v2, v1, v8}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 63
    :cond_3
    iput-object p1, p0, Ldmx;->m:Landroid/view/SurfaceView;

    goto :goto_0

    .line 43
    :cond_4
    invoke-static {}, Lhqd;->f()I

    move-result v0

    if-le v0, v8, :cond_5

    move v0, v1

    .line 44
    goto :goto_1

    .line 45
    :cond_5
    invoke-static {}, Lhqd;->h()I

    move-result v0

    goto :goto_1

    .line 54
    :cond_6
    new-instance v0, Lhqm;

    invoke-direct {v0, p1}, Lhqm;-><init>(Landroid/view/SurfaceView;)V

    iput-object v0, p0, Ldmx;->n:Lhqm;

    .line 55
    iget-object v0, p0, Ldmx;->n:Lhqm;

    .line 56
    iget-boolean v0, v0, Lhqm;->a:Z

    .line 57
    if-nez v0, :cond_2

    .line 58
    iget-object v0, p0, Ldmx;->n:Lhqm;

    .line 59
    iget-boolean v3, v0, Lhqm;->a:Z

    if-nez v3, :cond_2

    .line 60
    iput-boolean v8, v0, Lhqm;->a:Z

    goto :goto_2
.end method

.method public final a(Lhqd;Landroid/hardware/Camera;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 160
    iget-object v1, p0, Ldmx;->p:Landroid/hardware/Camera;

    if-ne p2, v1, :cond_0

    .line 179
    :goto_0
    return-void

    .line 162
    :cond_0
    iput-object p2, p0, Ldmx;->p:Landroid/hardware/Camera;

    .line 163
    if-nez p2, :cond_1

    .line 164
    :try_start_0
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/hardware/Camera;IIII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    const-string v1, "VideoShareSessionImpl.updateCamera"

    const-string v2, "error while setting camera"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 170
    :cond_1
    iget v1, p1, Lhqd;->e:I

    .line 171
    if-ne v1, v7, :cond_2

    move v3, v5

    .line 172
    :goto_1
    invoke-virtual {p1}, Lhqd;->e()I

    move-result v2

    .line 173
    sget-object v1, Lde/dreamchip/dreamstream/DreamVideo;->a:[I

    aget v4, v1, v5

    .line 174
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 175
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v5

    .line 176
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v0

    .line 177
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v7

    .line 178
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/hardware/Camera;IIII)I

    goto :goto_0

    :cond_2
    move v3, v0

    .line 171
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 65
    const-string v0, "VideoShareSessionImpl.setCamera"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->b()V

    .line 67
    iget-object v0, p0, Ldmx;->o:Lhqd;

    const/4 v1, 0x0

    .line 68
    iput-object v1, v0, Lhqd;->f:Lhqd$a;

    .line 69
    if-nez p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 71
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldmx;->l:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0, p1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    .line 72
    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    .line 73
    const/4 v0, 0x2

    .line 79
    :goto_1
    new-instance v1, Lhqd;

    iget-object v2, p0, Ldmx;->m:Landroid/view/SurfaceView;

    invoke-direct {v1, v0, v2}, Lhqd;-><init>(ILandroid/view/SurfaceView;)V

    iput-object v1, p0, Ldmx;->o:Lhqd;

    .line 80
    iget-object v0, p0, Ldmx;->o:Lhqd;

    iget-object v1, p0, Ldmx;->r:Lhqd$a;

    .line 81
    iput-object v1, v0, Lhqd;->f:Lhqd$a;

    .line 82
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->a()V

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "VideoShareSessionImpl.setCamera"

    const-string v2, "error using CameraManager"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(I[Lcom/google/android/rcs/client/session/Media;)Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 84
    iput p1, p0, Ldmx;->i:I

    .line 85
    if-ne p1, v9, :cond_5

    .line 86
    invoke-virtual {p0}, Ldmx;->f()V

    .line 87
    iget-object v1, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    if-eqz v1, :cond_1

    .line 89
    :try_start_0
    iget-object v0, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getLocalInterface()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getLocalPort()I

    move-result v1

    .line 91
    const-string v2, "VideoShareSessionImpl.startReceiving"

    const-string v3, "from: %s:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v2, p0, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getSelectedFormat()Ljava/lang/String;

    move-result-object v2

    .line 93
    iget-object v3, p0, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v3}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getPayload()I

    move-result v3

    .line 94
    invoke-static {v2}, Ldmx;->b(Ljava/lang/String;)I

    move-result v2

    .line 95
    invoke-direct {p0, v2}, Ldmx;->a(I)V

    .line 96
    invoke-direct {p0, v2}, Ldmx;->b(I)V

    .line 97
    const-string v4, "setVideoFormat"

    iget-object v5, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v5, v2}, Lde/dreamchip/dreamstream/DreamVideo;->g(I)I

    move-result v2

    invoke-static {v4, v2}, Ldmx;->a(Ljava/lang/String;I)V

    .line 98
    const-string v2, "setRtpPayloadType"

    iget-object v4, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v4, v3}, Lde/dreamchip/dreamstream/DreamVideo;->e(I)I

    move-result v3

    invoke-static {v2, v3}, Ldmx;->a(Ljava/lang/String;I)V

    .line 99
    const-string v2, "setRemotePort"

    iget-object v3, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v3, v1}, Lde/dreamchip/dreamstream/DreamVideo;->a(I)I

    move-result v3

    invoke-static {v2, v3}, Ldmx;->a(Ljava/lang/String;I)V

    .line 100
    const-string v2, "setRemoteRTCPPort"

    iget-object v3, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lde/dreamchip/dreamstream/DreamVideo;->c(I)I

    move-result v1

    invoke-static {v2, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 101
    const-string v1, "setRemoteHost"

    iget-object v2, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v2, v0}, Lde/dreamchip/dreamstream/DreamVideo;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Ldmx;->a(Ljava/lang/String;I)V

    .line 102
    invoke-direct {p0}, Ldmx;->j()V

    .line 103
    iget-object v0, p0, Ldmx;->e:[B

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "setEncryption"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v2, 0x1

    iget-object v3, p0, Ldmx;->e:[B

    iget-object v4, p0, Ldmx;->e:[B

    const/4 v5, 0x0

    .line 105
    invoke-virtual {v1, v2, v3, v4, v5}, Lde/dreamchip/dreamstream/DreamVideo;->a(I[B[BZ)I

    move-result v1

    .line 106
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 107
    :cond_0
    const-string v0, "prepare"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1}, Lde/dreamchip/dreamstream/DreamVideo;->e()I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 108
    const-string v0, "start"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lde/dreamchip/dreamstream/DreamVideo;->a(ZZ)I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Lbjv; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v0, v6

    .line 159
    :goto_1
    return v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "VideoShareSessionImpl.startReceiving"

    const-string v2, "error preparing to receive video"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    iget-object v1, p0, Ldmx;->a:Lbjy;

    invoke-virtual {v1, p0, v0}, Lbjy;->a(Lbjx;Ljava/lang/Exception;)V

    goto :goto_0

    .line 114
    :cond_1
    new-instance v1, Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    aget-object v2, p2, v0

    invoke-direct {v1, v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lcom/google/android/rcs/client/session/Media;)V

    iput-object v1, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 116
    :try_start_1
    iget-object v1, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getLocalInterface()Ljava/lang/String;

    move-result-object v1

    .line 117
    iget-object v2, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getLocalPort()I

    move-result v2

    .line 118
    const-string v3, "VideoShareSessionImpl.startSharing"

    const-string v4, "to %s:%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v5, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    iget-object v3, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v3}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getSelectedFormat()Ljava/lang/String;

    move-result-object v3

    .line 120
    iget-object v4, p0, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v4}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getPayload()I

    move-result v4

    .line 121
    invoke-static {v3}, Ldmx;->b(Ljava/lang/String;)I

    move-result v3

    .line 122
    invoke-direct {p0, v3}, Ldmx;->a(I)V

    .line 123
    invoke-direct {p0, v3}, Ldmx;->b(I)V

    .line 124
    const-string v5, "setVideoFormat"

    iget-object v7, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v7, v3}, Lde/dreamchip/dreamstream/DreamVideo;->g(I)I

    move-result v3

    invoke-static {v5, v3}, Ldmx;->a(Ljava/lang/String;I)V

    .line 125
    const-string v3, "setRtpPayloadType"

    iget-object v5, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v5, v4}, Lde/dreamchip/dreamstream/DreamVideo;->e(I)I

    move-result v4

    invoke-static {v3, v4}, Ldmx;->a(Ljava/lang/String;I)V

    .line 126
    const-string v3, "setRemotePort"

    iget-object v4, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v4, v2}, Lde/dreamchip/dreamstream/DreamVideo;->a(I)I

    move-result v4

    invoke-static {v3, v4}, Ldmx;->a(Ljava/lang/String;I)V

    .line 127
    const-string v3, "setRemoteRTCPPort"

    iget-object v4, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v4, v2}, Lde/dreamchip/dreamstream/DreamVideo;->c(I)I

    move-result v2

    invoke-static {v3, v2}, Ldmx;->a(Ljava/lang/String;I)V

    .line 128
    const-string v2, "setRemoteHost"

    iget-object v3, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v3, v1}, Lde/dreamchip/dreamstream/DreamVideo;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v2, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 129
    invoke-direct {p0}, Ldmx;->j()V

    .line 130
    iget-object v1, p0, Ldmx;->e:[B

    if-eqz v1, :cond_2

    .line 131
    const-string v1, "setEncryption"

    iget-object v2, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v3, 0x1

    iget-object v4, p0, Ldmx;->e:[B

    iget-object v5, p0, Ldmx;->e:[B

    const/4 v7, 0x0

    .line 132
    invoke-virtual {v2, v3, v4, v5, v7}, Lde/dreamchip/dreamstream/DreamVideo;->a(I[B[BZ)I

    move-result v2

    .line 133
    invoke-static {v1, v2}, Ldmx;->a(Ljava/lang/String;I)V

    .line 134
    :cond_2
    iget-object v1, p0, Ldmx;->o:Lhqd;

    if-eqz v1, :cond_3

    .line 135
    iget-object v1, p0, Ldmx;->o:Lhqd;

    .line 136
    iget v1, v1, Lhqd;->e:I

    .line 137
    if-ne v1, v9, :cond_4

    move v3, v0

    .line 138
    :goto_2
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->e()I

    move-result v2

    .line 139
    sget-object v0, Lde/dreamchip/dreamstream/DreamVideo;->a:[I

    const/4 v1, 0x0

    aget v4, v0, v1

    .line 140
    const-string v7, "setCamera"

    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    iget-object v1, p0, Ldmx;->o:Lhqd;

    .line 142
    iget-object v1, v1, Lhqd;->b:Landroid/hardware/Camera;

    .line 143
    const/4 v5, 0x0

    .line 144
    invoke-virtual/range {v0 .. v5}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/hardware/Camera;IIII)I

    move-result v0

    .line 145
    invoke-static {v7, v0}, Ldmx;->a(Ljava/lang/String;I)V

    .line 146
    :cond_3
    const-string v0, "prepare"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1}, Lde/dreamchip/dreamstream/DreamVideo;->e()I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 147
    const-string v0, "start"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lde/dreamchip/dreamstream/DreamVideo;->a(ZZ)I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V
    :try_end_1
    .catch Lbjv; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move v0, v6

    .line 152
    goto/16 :goto_1

    :cond_4
    move v3, v6

    .line 137
    goto :goto_2

    .line 149
    :catch_1
    move-exception v0

    .line 150
    const-string v1, "VideoShareSessionImpl.startSharing"

    const-string v2, "error starting to share video"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 151
    iget-object v1, p0, Ldmx;->a:Lbjy;

    invoke-virtual {v1, p0, v0}, Lbjy;->a(Lbjx;Ljava/lang/Exception;)V

    goto :goto_3

    .line 153
    :cond_5
    if-eq p1, v6, :cond_6

    const/4 v1, 0x3

    if-ne p1, v1, :cond_7

    .line 154
    :cond_6
    :try_start_2
    invoke-virtual {p0}, Ldmx;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 155
    iget-object v1, p0, Ldmx;->a:Lbjy;

    invoke-virtual {v1, p0}, Lbjy;->a(Lbjx;)V

    goto/16 :goto_1

    .line 157
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldmx;->a:Lbjy;

    invoke-virtual {v1, p0}, Lbjy;->a(Lbjx;)V

    throw v0

    :cond_7
    move v0, v6

    .line 159
    goto/16 :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Ldmx;->i:I

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Ldmx;->o:Lhqd;

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->c()V

    .line 18
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Ldmx;->o:Lhqd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldmx;->o:Lhqd;

    .line 20
    iget v0, v0, Lhqd;->d:I

    .line 21
    sget v1, Lmg$c;->aY:I

    if-ne v0, v1, :cond_0

    .line 22
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->d()V

    .line 23
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 180
    const-string v0, "VideoShareSessionImpl.dispose"

    const-string v1, "session id: %d"

    new-array v2, v8, [Ljava/lang/Object;

    iget-wide v4, p0, Ldmx;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    iget-object v0, p0, Ldmx;->o:Lhqd;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Ldmx;->o:Lhqd;

    .line 183
    iput-object v6, v0, Lhqd;->f:Lhqd$a;

    .line 184
    iget-object v0, p0, Ldmx;->o:Lhqd;

    invoke-virtual {v0}, Lhqd;->b()V

    .line 185
    :cond_0
    iput-object v6, p0, Ldmx;->o:Lhqd;

    .line 186
    iget-object v0, p0, Ldmx;->n:Lhqm;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Ldmx;->n:Lhqm;

    .line 188
    iput-object v6, v0, Lhqm;->b:Lhqn;

    .line 189
    iget-object v0, p0, Ldmx;->n:Lhqm;

    .line 190
    iput-boolean v7, v0, Lhqm;->a:Z

    .line 191
    :cond_1
    iput-object v6, p0, Ldmx;->n:Lhqm;

    .line 192
    iget-object v0, p0, Ldmx;->m:Landroid/view/SurfaceView;

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Ldmx;->m:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Ldmx;->q:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 194
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 195
    const/4 v1, 0x2

    invoke-virtual {v0, v6, v1, v8}, Lde/dreamchip/dreamstream/DreamVideo;->a(Landroid/view/SurfaceHolder;II)I

    .line 197
    :cond_2
    iput-object v6, p0, Ldmx;->m:Landroid/view/SurfaceView;

    .line 198
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    if-eqz v0, :cond_3

    .line 199
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v0}, Lde/dreamchip/dreamstream/DreamVideo;->f()V

    .line 200
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v0}, Lde/dreamchip/dreamstream/DreamVideo;->g()V

    .line 201
    iput-object v6, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 202
    :cond_3
    return-void
.end method

.method final f()V
    .locals 6

    .prologue
    .line 203
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ldmy;

    invoke-direct {v1, p0}, Ldmy;-><init>(Ldmx;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    .line 204
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 205
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 206
    return-void
.end method

.method public final g()V
    .locals 7

    .prologue
    const/16 v6, 0x96

    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 249
    const-string v0, "setPerformanceTuningParameters"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 250
    invoke-virtual {v1, v4, v2, v2, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(ZZIZ)I

    move-result v1

    .line 251
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 252
    const-string v0, "setRtpPayloadSize"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/16 v2, 0x3b6

    .line 253
    invoke-virtual {v1, v2}, Lde/dreamchip/dreamstream/DreamVideo;->f(I)I

    move-result v1

    .line 254
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 255
    const-string v0, "setJitterParameters"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/16 v2, 0x3e8

    const/16 v3, 0x1388

    .line 256
    invoke-virtual {v1, v6, v2, v3}, Lde/dreamchip/dreamstream/DreamVideo;->a(III)I

    move-result v1

    .line 257
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 258
    const-string v0, "setQualityMonitorParameters"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    sget-object v2, Ldmx;->j:Lhjr;

    .line 259
    invoke-virtual {v1, v2}, Lde/dreamchip/dreamstream/DreamVideo;->a(Lhjr;)I

    move-result v1

    .line 260
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 261
    const-string v0, "setSendBufferSize"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/16 v2, 0x4000

    .line 262
    invoke-virtual {v1, v2}, Lde/dreamchip/dreamstream/DreamVideo;->h(I)I

    move-result v1

    .line 263
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 264
    const-string v0, "setRenderParameters"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    const/16 v2, 0x32

    const/16 v3, 0xfa

    .line 265
    invoke-virtual {v1, v4, v2, v6, v3}, Lde/dreamchip/dreamstream/DreamVideo;->a(ZIII)I

    move-result v1

    .line 266
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 267
    const-string v0, "setLocalPort"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1, v5}, Lde/dreamchip/dreamstream/DreamVideo;->b(I)I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 268
    const-string v0, "setLocalRTCPPort"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1, v5}, Lde/dreamchip/dreamstream/DreamVideo;->d(I)I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 269
    const-string v0, "setProcessOwnSSRC"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    .line 270
    invoke-virtual {v1, v4}, Lde/dreamchip/dreamstream/DreamVideo;->a(Z)I

    move-result v1

    .line 271
    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 272
    return-void
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 273
    const-string v0, "bindLocalPorts"

    iget-object v1, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1}, Lde/dreamchip/dreamstream/DreamVideo;->d()I

    move-result v1

    invoke-static {v0, v1}, Ldmx;->a(Ljava/lang/String;I)V

    .line 274
    iget-object v0, p0, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v0}, Lde/dreamchip/dreamstream/DreamVideo;->a()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 275
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "VideoShareSession %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Ldmx;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
