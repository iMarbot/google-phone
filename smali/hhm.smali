.class public final Lhhm;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhhm;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Lhhj;

.field private e:Lhhl;

.field private f:Lhhn;

.field private g:Lhhk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v0, p0, Lhhm;->b:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lhhm;->c:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lhhm;->d:Lhhj;

    .line 12
    iput-object v0, p0, Lhhm;->e:Lhhl;

    .line 13
    iput-object v0, p0, Lhhm;->f:Lhhn;

    .line 14
    iput-object v0, p0, Lhhm;->g:Lhhk;

    .line 15
    iput-object v0, p0, Lhhm;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lhhm;->cachedSize:I

    .line 17
    return-void
.end method

.method private a(Lhfp;)Lhhm;
    .locals 6

    .prologue
    .line 52
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 53
    sparse-switch v0, :sswitch_data_0

    .line 55
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    :sswitch_0
    return-object p0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 59
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 61
    packed-switch v2, :pswitch_data_0

    .line 63
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x2f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum SegmentTypeEnum"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 68
    invoke-virtual {p0, p1, v0}, Lhhm;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 64
    :pswitch_0
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lhhm;->b:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 70
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhm;->c:Ljava/lang/String;

    goto :goto_0

    .line 72
    :sswitch_3
    iget-object v0, p0, Lhhm;->d:Lhhj;

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lhhj;

    invoke-direct {v0}, Lhhj;-><init>()V

    iput-object v0, p0, Lhhm;->d:Lhhj;

    .line 74
    :cond_1
    iget-object v0, p0, Lhhm;->d:Lhhj;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 76
    :sswitch_4
    iget-object v0, p0, Lhhm;->e:Lhhl;

    if-nez v0, :cond_2

    .line 77
    new-instance v0, Lhhl;

    invoke-direct {v0}, Lhhl;-><init>()V

    iput-object v0, p0, Lhhm;->e:Lhhl;

    .line 78
    :cond_2
    iget-object v0, p0, Lhhm;->e:Lhhl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 80
    :sswitch_5
    iget-object v0, p0, Lhhm;->f:Lhhn;

    if-nez v0, :cond_3

    .line 81
    new-instance v0, Lhhn;

    invoke-direct {v0}, Lhhn;-><init>()V

    iput-object v0, p0, Lhhm;->f:Lhhn;

    .line 82
    :cond_3
    iget-object v0, p0, Lhhm;->f:Lhhn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 84
    :sswitch_6
    iget-object v0, p0, Lhhm;->g:Lhhk;

    if-nez v0, :cond_4

    .line 85
    new-instance v0, Lhhk;

    invoke-direct {v0}, Lhhk;-><init>()V

    iput-object v0, p0, Lhhm;->g:Lhhk;

    .line 86
    :cond_4
    iget-object v0, p0, Lhhm;->g:Lhhk;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 53
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    .line 61
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a()[Lhhm;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhhm;->a:[Lhhm;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhhm;->a:[Lhhm;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhhm;

    sput-object v0, Lhhm;->a:[Lhhm;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhhm;->a:[Lhhm;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 32
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 33
    iget-object v1, p0, Lhhm;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x1

    iget-object v2, p0, Lhhm;->b:Ljava/lang/Integer;

    .line 35
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_0
    iget-object v1, p0, Lhhm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 37
    const/4 v1, 0x2

    iget-object v2, p0, Lhhm;->c:Ljava/lang/String;

    .line 38
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_1
    iget-object v1, p0, Lhhm;->d:Lhhj;

    if-eqz v1, :cond_2

    .line 40
    const/4 v1, 0x3

    iget-object v2, p0, Lhhm;->d:Lhhj;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_2
    iget-object v1, p0, Lhhm;->e:Lhhl;

    if-eqz v1, :cond_3

    .line 43
    const/4 v1, 0x4

    iget-object v2, p0, Lhhm;->e:Lhhl;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_3
    iget-object v1, p0, Lhhm;->f:Lhhn;

    if-eqz v1, :cond_4

    .line 46
    const/4 v1, 0x5

    iget-object v2, p0, Lhhm;->f:Lhhn;

    .line 47
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_4
    iget-object v1, p0, Lhhm;->g:Lhhk;

    if-eqz v1, :cond_5

    .line 49
    const/4 v1, 0x6

    iget-object v2, p0, Lhhm;->g:Lhhk;

    .line 50
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lhhm;->a(Lhfp;)Lhhm;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lhhm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iget-object v1, p0, Lhhm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 20
    :cond_0
    iget-object v0, p0, Lhhm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x2

    iget-object v1, p0, Lhhm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 22
    :cond_1
    iget-object v0, p0, Lhhm;->d:Lhhj;

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x3

    iget-object v1, p0, Lhhm;->d:Lhhj;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_2
    iget-object v0, p0, Lhhm;->e:Lhhl;

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x4

    iget-object v1, p0, Lhhm;->e:Lhhl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_3
    iget-object v0, p0, Lhhm;->f:Lhhn;

    if-eqz v0, :cond_4

    .line 27
    const/4 v0, 0x5

    iget-object v1, p0, Lhhm;->f:Lhhn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_4
    iget-object v0, p0, Lhhm;->g:Lhhk;

    if-eqz v0, :cond_5

    .line 29
    const/4 v0, 0x6

    iget-object v1, p0, Lhhm;->g:Lhhk;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 31
    return-void
.end method
