.class final Lcav;
.super Lcan;
.source "PG"


# instance fields
.field private b:J

.field private c:J

.field private d:J

.field private e:Z

.field private f:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcan;-><init>()V

    return-void
.end method

.method private final a(ZJ)V
    .locals 4

    .prologue
    .line 17
    iget-wide v0, p0, Lcav;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 18
    iget-boolean v0, p0, Lcav;->e:Z

    if-eqz v0, :cond_0

    .line 19
    iget-wide v0, p0, Lcav;->d:J

    iget-wide v2, p0, Lcav;->c:J

    sub-long v2, p2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcav;->d:J

    .line 20
    :cond_0
    if-eqz p1, :cond_1

    .line 21
    iput-wide p2, p0, Lcav;->c:J

    .line 22
    :cond_1
    iput-boolean p1, p0, Lcav;->e:Z

    .line 23
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 3

    .prologue
    .line 24
    iget v1, p0, Lcav;->f:F

    .line 25
    const/4 v0, 0x0

    .line 26
    const v2, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 27
    const/high16 v0, 0x40000000    # 2.0f

    .line 29
    :cond_0
    return v0
.end method

.method public final a(Landroid/hardware/SensorEvent;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 3
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-wide v2, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-direct {p0, v0, v2, v3}, Lcav;->a(ZJ)V

    .line 4
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcav;->b:J

    .line 8
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcav;->c:J

    .line 9
    iput-wide v4, p0, Lcav;->d:J

    .line 10
    :cond_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 11
    :cond_1
    iget-boolean v0, p0, Lcav;->e:Z

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcav;->a(ZJ)V

    .line 12
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcav;->b:J

    sub-long/2addr v0, v2

    .line 13
    cmp-long v2, v0, v4

    if-nez v2, :cond_4

    .line 14
    iget-boolean v0, p0, Lcav;->e:Z

    if-eqz v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lcav;->f:F

    .line 16
    :cond_2
    :goto_1
    return-void

    .line 14
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 15
    :cond_4
    iget-wide v2, p0, Lcav;->d:J

    long-to-float v2, v2

    long-to-float v0, v0

    div-float v0, v2, v0

    iput v0, p0, Lcav;->f:F

    goto :goto_1
.end method
