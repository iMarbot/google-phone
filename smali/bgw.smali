.class final Lbgw;
.super Lbhd;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:Ljava/io/ByteArrayOutputStream;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/io/ByteArrayOutputStream;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbhd;-><init>()V

    .line 2
    iput-object p1, p0, Lbgw;->a:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lbgw;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lbgw;->c:Ljava/lang/String;

    .line 5
    iput-boolean p4, p0, Lbgw;->d:Z

    .line 6
    iput p5, p0, Lbgw;->e:I

    .line 7
    iput-object p6, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    .line 8
    iput-object p7, p0, Lbgw;->g:Ljava/util/List;

    .line 9
    iput-object p8, p0, Lbgw;->h:Ljava/util/List;

    .line 10
    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lbgw;->a:Ljava/lang/String;

    return-object v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lbgw;->b:Ljava/lang/String;

    return-object v0
.end method

.method final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lbgw;->c:Ljava/lang/String;

    return-object v0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lbgw;->d:Z

    return v0
.end method

.method final e()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lbgw;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 20
    if-ne p1, p0, :cond_1

    .line 33
    :cond_0
    :goto_0
    return v0

    .line 22
    :cond_1
    instance-of v2, p1, Lbhd;

    if-eqz v2, :cond_5

    .line 23
    check-cast p1, Lbhd;

    .line 24
    iget-object v2, p0, Lbgw;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lbhd;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbgw;->b:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lbhd;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbgw;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 26
    invoke-virtual {p1}, Lbhd;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-boolean v2, p0, Lbgw;->d:Z

    .line 27
    invoke-virtual {p1}, Lbhd;->d()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lbgw;->e:I

    .line 28
    invoke-virtual {p1}, Lbhd;->e()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    if-nez v2, :cond_4

    .line 29
    invoke-virtual {p1}, Lbhd;->f()Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Lbgw;->g:Ljava/util/List;

    .line 30
    invoke-virtual {p1}, Lbhd;->g()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbgw;->h:Ljava/util/List;

    .line 31
    invoke-virtual {p1}, Lbhd;->h()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 32
    goto :goto_0

    .line 26
    :cond_3
    iget-object v2, p0, Lbgw;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lbhd;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 29
    :cond_4
    iget-object v2, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1}, Lbhd;->f()Ljava/io/ByteArrayOutputStream;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_5
    move v0, v1

    .line 33
    goto :goto_0
.end method

.method final f()Ljava/io/ByteArrayOutputStream;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    return-object v0
.end method

.method final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lbgw;->g:Ljava/util/List;

    return-object v0
.end method

.method final h()Ljava/util/List;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lbgw;->h:Ljava/util/List;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const v3, 0xf4243

    .line 34
    iget-object v0, p0, Lbgw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    xor-int/2addr v0, v3

    .line 35
    mul-int/2addr v0, v3

    .line 36
    iget-object v2, p0, Lbgw;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    .line 37
    mul-int v2, v0, v3

    .line 38
    iget-object v0, p0, Lbgw;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    .line 39
    mul-int v2, v0, v3

    .line 40
    iget-boolean v0, p0, Lbgw;->d:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    xor-int/2addr v0, v2

    .line 41
    mul-int/2addr v0, v3

    .line 42
    iget v2, p0, Lbgw;->e:I

    xor-int/2addr v0, v2

    .line 43
    mul-int/2addr v0, v3

    .line 44
    iget-object v2, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    if-nez v2, :cond_2

    :goto_2
    xor-int/2addr v0, v1

    .line 45
    mul-int/2addr v0, v3

    .line 46
    iget-object v1, p0, Lbgw;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 47
    mul-int/2addr v0, v3

    .line 48
    iget-object v1, p0, Lbgw;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 49
    return v0

    .line 38
    :cond_0
    iget-object v0, p0, Lbgw;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 40
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 44
    :cond_2
    iget-object v1, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 19
    iget-object v0, p0, Lbgw;->a:Ljava/lang/String;

    iget-object v1, p0, Lbgw;->b:Ljava/lang/String;

    iget-object v2, p0, Lbgw;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lbgw;->d:Z

    iget v4, p0, Lbgw;->e:I

    iget-object v5, p0, Lbgw;->f:Ljava/io/ByteArrayOutputStream;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lbgw;->g:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lbgw;->h:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x75

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Contact{accountType="

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ", accountName="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isStarred="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pinned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", photoStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phoneNumbers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", emails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
