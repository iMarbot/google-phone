.class public Lcpv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:S

.field private b:Landroid/content/Context;

.field private c:Landroid/telecom/PhoneAccountHandle;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcpv;->b:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcpv;->c:Landroid/telecom/PhoneAccountHandle;

    .line 4
    iput-short p3, p0, Lcpv;->a:S

    .line 5
    iput-object p4, p0, Lcpv;->d:Ljava/lang/String;

    .line 6
    return-void
.end method

.method protected static a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15
    return-void
.end method


# virtual methods
.method public a(Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 7
    return-void
.end method

.method protected final a(Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 10
    const-string v0, "Sending sms \'%s\' to %s:%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcpv;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-short v3, p0, Lcpv;->a:S

    .line 11
    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lcpv;->b:Landroid/content/Context;

    iget-object v1, p0, Lcpv;->c:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lcpv;->d:Ljava/lang/String;

    iget-short v3, p0, Lcpv;->a:S

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lbvs;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)Ljava/lang/String;

    .line 13
    return-void
.end method

.method public b(Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 8
    return-void
.end method

.method public c(Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method
