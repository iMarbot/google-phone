.class public final enum Lbtr;
.super Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;IB)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 2
    const v0, 0x7f1100bf

    .line 4
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 5
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 7
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->h:Landroid/widget/TextView;

    .line 8
    const v1, 0x7f1100c0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 10
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    .line 11
    const v1, 0x7f1100bc

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 13
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 14
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    return-void
.end method

.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33
    if-nez p2, :cond_0

    .line 34
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 35
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    .line 55
    :goto_0
    return-void

    .line 39
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 46
    const-string v0, "VmChangePinActivity"

    const/16 v2, 0x26

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected ChangePinResult "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 51
    :goto_1
    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 53
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    .line 54
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 40
    :pswitch_0
    const v0, 0x7f11033b

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 41
    :pswitch_1
    const v0, 0x7f11033a

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 42
    :pswitch_2
    const v0, 0x7f11033c

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 43
    :pswitch_3
    const v0, 0x7f110337

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 44
    :pswitch_4
    const v0, 0x7f110338

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 45
    :pswitch_5
    const v0, 0x7f110339

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 16
    .line 18
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 21
    :goto_0
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 22
    return-void

    .line 19
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 23
    .line 25
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 27
    iput-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    .line 31
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method
