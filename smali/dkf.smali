.class public final Ldkf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjf;
.implements Lcdb;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

.field public final c:Ljava/util/Map;

.field public final d:Ljava/util/List;

.field private e:Landroid/content/Context;

.field private f:Ljava/util/concurrent/ExecutorService;

.field private g:Ldml;

.field private h:Ldly;

.field private i:Ldmo;

.field private j:Ljava/util/List;

.field private k:Ljava/util/Set;

.field private l:Ljava/util/Set;

.field private m:Ljava/util/Set;

.field private n:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ldml;Ldly;Lcct;Ldmo;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldkf;->j:Ljava/util/List;

    .line 3
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ldkf;->c:Ljava/util/Map;

    .line 4
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Ldkf;->k:Ljava/util/Set;

    .line 5
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Ldkf;->l:Ljava/util/Set;

    .line 6
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Ldkf;->m:Ljava/util/Set;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldkf;->d:Ljava/util/List;

    .line 8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    .line 9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported SDK: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0

    .line 10
    :cond_0
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldkf;->e:Landroid/content/Context;

    .line 11
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    .line 12
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldml;

    iput-object v0, p0, Ldkf;->g:Ldml;

    .line 13
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldly;

    iput-object v0, p0, Ldkf;->h:Ldly;

    .line 14
    invoke-static {p6}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmo;

    iput-object v0, p0, Ldkf;->i:Ldmo;

    .line 16
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enriched_call_history_buffer_time_millis"

    const-wide/16 v2, 0x7530

    .line 17
    invoke-interface {v0, v1, v2, v3}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Ldkf;->n:J

    .line 18
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcct;

    invoke-virtual {v0, p0}, Lcct;->a(Lcdb;)V

    .line 19
    return-void
.end method

.method private final a(Ljava/lang/String;Ljava/util/List;)Lbjn;
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 371
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const-string v0, ""

    invoke-static {v0, v2, v3, v2, v3}, Lbjn;->a(Ljava/lang/String;JJ)Lbjn;

    move-result-object v0

    .line 384
    :goto_0
    return-object v0

    .line 373
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 374
    iget-wide v2, v0, Lbao$a;->e:J

    .line 376
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 379
    iget-wide v4, v0, Lbao$a;->e:J

    .line 380
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 382
    iget-wide v6, v0, Lbao$a;->f:J

    .line 383
    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    add-long/2addr v0, v4

    iget-wide v4, p0, Ldkf;->n:J

    add-long/2addr v0, v4

    .line 384
    invoke-static {p1, v2, v3, v0, v1}, Lbjn;->a(Ljava/lang/String;JJ)Lbjn;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Ljava/util/function/Predicate;)Ldll;
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Ldkf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/stream/Stream;->findFirst()Ljava/util/Optional;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldll;

    return-object v0
.end method

.method private final a(Ljava/util/List;Ljava/util/List;)Ljava/util/Map;
    .locals 16

    .prologue
    .line 298
    new-instance v10, Landroid/util/ArrayMap;

    invoke-direct {v10}, Landroid/util/ArrayMap;-><init>()V

    .line 299
    const/4 v3, 0x0

    .line 300
    const/4 v2, 0x0

    move v5, v2

    move v4, v3

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_6

    .line 301
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbao$a;

    .line 302
    add-int/lit8 v3, v5, 0x1

    .line 303
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 304
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbao$a;

    .line 307
    :goto_1
    iget-wide v12, v2, Lbao$a;->e:J

    .line 310
    if-nez v3, :cond_1

    .line 312
    iget-wide v6, v2, Lbao$a;->e:J

    .line 313
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 315
    iget-wide v8, v2, Lbao$a;->f:J

    .line 316
    invoke-virtual {v3, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-wide v8, v0, Ldkf;->n:J

    add-long/2addr v6, v8

    move-wide v8, v6

    .line 322
    :goto_2
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    move v6, v4

    .line 323
    :goto_3
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v6, v3, :cond_5

    .line 324
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldrq;

    .line 325
    iget-wide v14, v3, Ldrq;->e:J

    cmp-long v4, v12, v14

    if-gtz v4, :cond_5

    iget-wide v14, v3, Ldrq;->e:J

    cmp-long v4, v14, v8

    if-gtz v4, :cond_5

    .line 327
    sget-object v4, Lbjo;->g:Lbjo;

    invoke-virtual {v4}, Lbjo;->createBuilder()Lhbr$a;

    move-result-object v4

    check-cast v4, Lhbr$a;

    .line 329
    iget v7, v3, Ldrq;->a:I

    .line 330
    invoke-static {v7}, Ldrq;->a(I)Z

    move-result v14

    invoke-static {v14}, Lbdf;->a(Z)V

    .line 331
    packed-switch v7, :pswitch_data_0

    .line 336
    const-string v2, ""

    invoke-static {v2}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v2

    throw v2

    .line 305
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 319
    :cond_1
    iget-wide v6, v3, Lbao$a;->e:J

    .line 320
    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    move-wide v8, v6

    goto :goto_2

    .line 332
    :pswitch_0
    sget-object v7, Lbjo$a;->a:Lbjo$a;

    .line 337
    :goto_4
    invoke-virtual {v4, v7}, Lhbr$a;->a(Lbjo$a;)Lhbr$a;

    .line 338
    iget-object v7, v3, Ldrq;->b:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 339
    iget-object v7, v3, Ldrq;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lhbr$a;->n(Ljava/lang/String;)Lhbr$a;

    .line 340
    :cond_2
    iget-object v7, v3, Ldrq;->c:Landroid/net/Uri;

    if-eqz v7, :cond_3

    .line 341
    iget-object v7, v3, Ldrq;->c:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lhbr$a;->o(Ljava/lang/String;)Lhbr$a;

    .line 342
    :cond_3
    iget-object v7, v3, Ldrq;->d:Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 343
    iget-object v7, v3, Ldrq;->d:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lhbr$a;->p(Ljava/lang/String;)Lhbr$a;

    .line 344
    :cond_4
    iget-wide v14, v3, Ldrq;->e:J

    invoke-virtual {v4, v14, v15}, Lhbr$a;->i(J)Lhbr$a;

    .line 345
    invoke-virtual {v4}, Lhbr$a;->build()Lhbr;

    move-result-object v3

    check-cast v3, Lbjo;

    .line 346
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 333
    :pswitch_1
    sget-object v7, Lbjo$a;->b:Lbjo$a;

    goto :goto_4

    .line 334
    :pswitch_2
    sget-object v7, Lbjo$a;->c:Lbjo$a;

    goto :goto_4

    .line 335
    :pswitch_3
    sget-object v7, Lbjo$a;->d:Lbjo$a;

    goto :goto_4

    .line 348
    :cond_5
    invoke-interface {v10, v2, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v6

    goto/16 :goto_0

    .line 350
    :cond_6
    return-object v10

    .line 331
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private final a(Ljava/lang/String;Ldmj;J)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 587
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    const-string v0, "EnrichedCallManagerImpl.insertHistoryEntry"

    const-string v1, "writing session to history. number: %s, type: %d, data: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 589
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 591
    iget v3, p2, Ldmj;->e:I

    .line 592
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x2

    .line 594
    iget-object v4, p2, Ldmj;->d:Lbln;

    .line 595
    aput-object v4, v2, v3

    .line 596
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v6

    new-instance v0, Ldlm;

    iget-object v1, p0, Ldkf;->h:Ldly;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    .line 599
    invoke-direct/range {v0 .. v5}, Ldlm;-><init>(Ldly;Ljava/lang/String;Ldmj;J)V

    .line 600
    invoke-virtual {v6, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkv;

    invoke-direct {v1, p0, p1}, Ldkv;-><init>(Ldkf;Ljava/lang/String;)V

    .line 601
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    sget-object v1, Ldkw;->a:Lbea;

    .line 602
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 603
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 604
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 606
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 607
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 608
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "enriched_call_made"

    .line 609
    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 610
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 611
    return-void
.end method

.method static a(Ldmj;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 461
    sget-object v2, Ldlc;->a:Ljava/util/function/Predicate;

    .line 462
    invoke-interface {v2, p0}, Ljava/util/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v0

    .line 464
    :cond_1
    invoke-static {p0}, Ldkf;->b(Ldmj;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 465
    goto :goto_0

    .line 466
    :cond_2
    invoke-virtual {p0}, Ldmj;->c()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_0

    .line 469
    iget v0, p0, Ldmj;->e:I

    .line 470
    if-eqz v0, :cond_3

    move v0, v1

    .line 471
    goto :goto_0

    .line 472
    :cond_3
    invoke-virtual {p0}, Ldmj;->f()Z

    move-result v0

    goto :goto_0
.end method

.method private final b(Ljava/util/function/Predicate;)Ldmj;
    .locals 2

    .prologue
    .line 626
    invoke-static {}, Lbdf;->b()V

    .line 627
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/stream/Stream;->findFirst()Ljava/util/Optional;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmj;

    return-object v0
.end method

.method private static b(Ldmj;)Z
    .locals 2

    .prologue
    .line 460
    invoke-virtual {p0}, Ldmj;->c()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(J)Ljava/util/function/Predicate;
    .locals 2

    .prologue
    .line 628
    new-instance v0, Ldla;

    invoke-direct {v0, p0, p1}, Ldla;-><init>(J)V

    return-object v0
.end method

.method static f(Ljava/lang/String;)Ljava/util/function/Predicate;
    .locals 1

    .prologue
    .line 630
    new-instance v0, Ldle;

    invoke-direct {v0, p0}, Ldle;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private final g(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 455
    const-string v0, "EnrichedCallManagerImpl.clearCapabilities"

    const-string v1, "session state failed, clearing capabilities for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 456
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 457
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 458
    iget-object v0, p0, Ldkf;->j:Ljava/util/List;

    invoke-static {p1}, Ldkf;->h(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v1

    invoke-direct {p0, v1}, Ldkf;->a(Ljava/util/function/Predicate;)Ldll;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 459
    return-void
.end method

.method private static h(Ljava/lang/String;)Ljava/util/function/Predicate;
    .locals 1

    .prologue
    .line 613
    new-instance v0, Ldkz;

    invoke-direct {v0, p0}, Ldkz;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private final i(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 614
    .line 615
    invoke-static {p1}, Ldkf;->h(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->a(Ljava/util/function/Predicate;)Ldll;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_0

    .line 617
    iget-object v1, v0, Ldll;->b:Ljava/lang/String;

    .line 618
    if-nez v1, :cond_1

    .line 619
    :cond_0
    const-string v0, "EnrichedCallManagerImpl.getE164FormattedNumber"

    const-string v1, "No e164 number for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 620
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 621
    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 625
    :goto_0
    return-object p1

    .line 624
    :cond_1
    iget-object p1, v0, Ldll;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method private static j(Ljava/lang/String;)Ljava/util/function/Predicate;
    .locals 1

    .prologue
    .line 629
    new-instance v0, Ldlb;

    invoke-direct {v0, p0}, Ldlb;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private final j()V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->o:Ldnt$a;

    .line 452
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    .line 453
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 454
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lbjh;)Lbjl;
    .locals 2

    .prologue
    .line 241
    invoke-static {}, Lbdf;->b()V

    .line 242
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    if-nez p3, :cond_0

    .line 245
    sget-object p3, Ldlj;->a:Lbjh;

    .line 247
    :cond_0
    invoke-static {p1}, Ldkf;->j(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 248
    new-instance v1, Ldlk;

    invoke-direct {v1, p3}, Ldlk;-><init>(Lbjh;)V

    .line 249
    invoke-interface {v0, v1}, Ljava/util/function/Predicate;->and(Ljava/util/function/Predicate;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_1

    .line 258
    :goto_0
    return-object v0

    .line 252
    :cond_1
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    sget-object v1, Ldki;->a:Ljava/util/function/Predicate;

    invoke-interface {v0, v1}, Ljava/util/List;->removeIf(Ljava/util/function/Predicate;)Z

    .line 254
    sget-object v0, Ldlc;->a:Ljava/util/function/Predicate;

    .line 255
    invoke-static {p2}, Ldkf;->f(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/function/Predicate;->and(Ljava/util/function/Predicate;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 256
    new-instance v1, Ldkj;

    invoke-direct {v1, p3}, Ldkj;-><init>(Lbjh;)V

    .line 257
    invoke-interface {v0, v1}, Ljava/util/function/Predicate;->and(Ljava/util/function/Predicate;)Ljava/util/function/Predicate;

    move-result-object v0

    .line 258
    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lbdf;->b()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Ldkf;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 80
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 186
    invoke-static {}, Lbdf;->b()V

    .line 187
    const-string v0, "EnrichedCallManagerImpl.endCallComposerSession"

    const-string v3, "sessionId: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    const-wide/16 v4, -0x1

    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v3

    .line 191
    if-eqz v3, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "No existing session for id: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v0, v4, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldkd;

    iget-object v2, p0, Ldkf;->g:Ldml;

    .line 195
    iget-wide v4, v3, Ldmj;->a:J

    .line 196
    invoke-direct {v1, v2, v4, v5}, Ldkd;-><init>(Ldml;J)V

    .line 197
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldlf;

    invoke-direct {v1, p0, v3}, Ldlf;-><init>(Ldkf;Ldmj;)V

    .line 198
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldlg;

    invoke-direct {v1, p0, v3}, Ldlg;-><init>(Ldkf;Ldmj;)V

    .line 199
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 200
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 201
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 191
    goto :goto_1
.end method

.method public final a(JLbln;)V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 169
    invoke-static {}, Lbdf;->b()V

    .line 170
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const-string v0, "EnrichedCallManagerImpl.sendCallComposerData"

    const-string v3, "sessionId: %s, multimediaData: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 172
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    .line 173
    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v3

    .line 175
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "No existing session for: %s"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0, v4, v5}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-virtual {v3}, Ldmj;->c()I

    move-result v0

    if-ne v0, v7, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lbdf;->b(Z)V

    .line 177
    invoke-virtual {v3, p3, v1}, Ldmj;->a(Lbln;I)V

    .line 178
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldmh;

    iget-object v2, p0, Ldkf;->g:Ldml;

    invoke-direct {v1, v2, p1, p2, p3}, Ldmh;-><init>(Ldml;JLbln;)V

    .line 180
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldks;

    invoke-direct {v1, p0, v3}, Ldks;-><init>(Ldkf;Ldmj;)V

    .line 181
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldld;

    invoke-direct {v1, p0, v3}, Ldld;-><init>(Ldkf;Ldmj;)V

    .line 182
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 183
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 184
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 185
    return-void

    :cond_1
    move v0, v2

    .line 175
    goto :goto_0
.end method

.method public final a(JLjava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 390
    invoke-static {}, Lbdf;->b()V

    .line 391
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    invoke-static {p4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->isSessionState(I)Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 393
    const-string v0, "EnrichedCallManagerImpl.onSessionStatusUpdate"

    const-string v3, "sessionId: %d, number: %s, sessionState: %s"

    new-array v4, v6, [Ljava/lang/Object;

    .line 394
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    .line 395
    invoke-static {p3}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 396
    invoke-static {p4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->sessionStateToString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    .line 397
    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 398
    iget-object v0, p0, Ldkf;->i:Ldmo;

    .line 399
    iget-object v3, v0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v3, p1, p2, p4}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->onSessionStatusUpdate(JI)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 400
    iget-object v3, v0, Ldmo;->b:Ljava/util/Set;

    new-instance v4, Ldms;

    invoke-direct {v4, v0}, Ldms;-><init>(Ldmo;)V

    invoke-interface {v3, v4}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    :cond_0
    :goto_0
    move v0, v1

    .line 408
    :goto_1
    if-eqz v0, :cond_4

    .line 450
    :goto_2
    return-void

    .line 402
    :cond_1
    iget-object v3, v0, Ldmo;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v2

    .line 403
    goto :goto_1

    .line 404
    :cond_2
    if-eq p4, v1, :cond_3

    if-ne p4, v6, :cond_0

    .line 405
    :cond_3
    iget-object v3, v0, Ldmo;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    iget-object v3, v0, Ldmo;->b:Ljava/util/Set;

    new-instance v4, Ldmt;

    invoke-direct {v4, v0}, Ldmt;-><init>(Ldmo;)V

    invoke-interface {v3, v4}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    goto :goto_0

    .line 410
    :cond_4
    if-ne p4, v1, :cond_5

    .line 411
    invoke-direct {p0}, Ldkf;->j()V

    .line 412
    const-string v0, "EnrichedCallManagerImpl.onSessionStatusUpdate"

    const-string v3, "session failed to start, refreshing capabilities"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    invoke-virtual {p0, p3}, Ldkf;->a(Ljava/lang/String;)V

    .line 414
    :cond_5
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v0

    .line 415
    if-nez v0, :cond_6

    .line 416
    new-instance v0, Ldmj;

    invoke-direct {v0, p1, p2, p3, p4}, Ldmj;-><init>(JLjava/lang/String;I)V

    .line 417
    iget-object v3, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 418
    :cond_6
    invoke-virtual {v0, p4}, Ldmj;->a(I)V

    .line 419
    invoke-virtual {p0}, Ldkf;->g()V

    .line 423
    iget v3, v0, Ldmj;->e:I

    .line 424
    if-eq v3, v7, :cond_7

    .line 426
    iget v3, v0, Ldmj;->e:I

    .line 427
    if-ne v3, v6, :cond_9

    :cond_7
    move v3, v1

    .line 428
    :goto_3
    if-eqz v3, :cond_c

    .line 430
    invoke-static {v0}, Ldkf;->b(Ldmj;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v0}, Ldmj;->c()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_a

    .line 431
    :cond_8
    :goto_4
    if-eqz v1, :cond_b

    .line 432
    const-string v1, "EnrichedCallManagerImpl.cleanupSession"

    const-string v3, "deleting closed or failed post call session"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 433
    iget-object v1, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    move v3, v2

    .line 427
    goto :goto_3

    :cond_a
    move v1, v2

    .line 430
    goto :goto_4

    .line 435
    :cond_b
    const-string v0, "EnrichedCallManagerImpl.cleanupSession"

    const-string v1, "retaining non-failed post call session"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 437
    :cond_c
    invoke-static {v0}, Ldkf;->a(Ldmj;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 438
    const-string v3, "EnrichedCallManagerImpl.cleanupSession"

    const-string v4, "retaining session %d"

    new-array v1, v1, [Ljava/lang/Object;

    .line 440
    iget-wide v6, v0, Ldmj;->a:J

    .line 441
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v1, v2

    .line 442
    invoke-static {v3, v4, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 444
    :cond_d
    const-string v3, "EnrichedCallManagerImpl.cleanupSession"

    const-string v4, "removing session %d"

    new-array v1, v1, [Ljava/lang/Object;

    .line 446
    iget-wide v6, v0, Ldmj;->a:J

    .line 447
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    .line 448
    invoke-static {v3, v4, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    iget-object v1, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method public final a(Landroid/content/BroadcastReceiver$PendingResult;JLbln;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 518
    invoke-static {}, Lbdf;->b()V

    .line 519
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    const-string v2, "EnrichedCallManagerImpl.onIncomingPostCallData"

    const-string v3, "sessionId: %d, multimediaData: %s"

    new-array v4, v6, [Ljava/lang/Object;

    .line 522
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p4, v4, v0

    .line 523
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 524
    invoke-static {p2, p3}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v2

    invoke-direct {p0, v2}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v2

    .line 525
    if-eqz v2, :cond_0

    :goto_0
    const-string v3, "No session for incoming post call data"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lbdf;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 526
    invoke-virtual {v2, p4, v6}, Ldmj;->a(Lbln;I)V

    .line 528
    iget-object v0, v2, Ldmj;->c:Ljava/lang/String;

    .line 529
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v0, v2, v4, v5}, Ldkf;->a(Ljava/lang/String;Ldmj;J)V

    .line 530
    invoke-virtual {p0}, Ldkf;->g()V

    .line 531
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 532
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldmb;

    iget-object v3, p0, Ldkf;->e:Landroid/content/Context;

    invoke-direct {v1, v3}, Ldmb;-><init>(Landroid/content/Context;)V

    .line 534
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkr;

    invoke-direct {v1, p1}, Ldkr;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 535
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkt;

    invoke-direct {v1, p1}, Ldkt;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 536
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 537
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    .line 539
    iget-object v2, v2, Ldmj;->c:Ljava/lang/String;

    .line 540
    invoke-virtual {p4}, Lbln;->a()Ljava/lang/String;

    move-result-object v3

    .line 541
    new-instance v4, Ldkb;

    invoke-direct {v4, v2, v3}, Ldkb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    invoke-interface {v0, v1, v4}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 543
    return-void

    :cond_0
    move v0, v1

    .line 525
    goto :goto_0
.end method

.method public final a(Lbjg;)V
    .locals 4

    .prologue
    .line 20
    const-string v0, "EnrichedCallManagerImpl.registerCapabilitiesListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    invoke-static {}, Lbdf;->b()V

    .line 22
    iget-object v1, p0, Ldkf;->k:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjg;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public final a(Lbji;)V
    .locals 4

    .prologue
    .line 264
    invoke-static {}, Lbdf;->b()V

    .line 265
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    const-string v0, "EnrichedCallManagerImpl.registerHistoricalDataChangedListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    iget-object v0, p0, Ldkf;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 268
    return-void
.end method

.method public final a(Lbjj;)V
    .locals 4

    .prologue
    .line 236
    invoke-static {}, Lbdf;->b()V

    .line 237
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const-string v0, "EnrichedCallManagerImpl.registerStateChangedListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-object v0, p0, Ldkf;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 240
    return-void
.end method

.method public final a(Lbjw;)V
    .locals 3

    .prologue
    .line 631
    iget-object v1, p0, Ldkf;->i:Ldmo;

    .line 632
    const-string v0, "VideoShareManager.registerListener"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 633
    invoke-static {}, Lbdf;->b()V

    .line 634
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    invoke-virtual {v1}, Ldmo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    iget-object v2, v1, Ldmo;->b:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjw;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 637
    iget-object v0, v1, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v0}, Lgng;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, v1, Ldmo;->e:Z

    if-nez v0, :cond_0

    .line 638
    iget-object v0, v1, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v0}, Lgng;->connect()Z

    move-result v0

    iput-boolean v0, v1, Ldmo;->e:Z

    .line 639
    :cond_0
    return-void
.end method

.method public final a(Lcct;)V
    .locals 0

    .prologue
    .line 652
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 25
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 26
    invoke-static {}, Lbdf;->b()V

    .line 27
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-static {p1}, Ldkf;->h(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->a(Ljava/util/function/Predicate;)Ldll;

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Ldkf;->j:Ljava/util/List;

    new-instance v1, Ldll;

    invoke-direct {v1, p1}, Ldll;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v7

    new-instance v0, Ldme;

    iget-object v1, p0, Ldkf;->e:Landroid/content/Context;

    .line 34
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    iget-object v2, p0, Ldkf;->g:Ldml;

    iget-object v3, p0, Ldkf;->a:Ljava/lang/Integer;

    iget-object v4, p0, Ldkf;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    iget-object v5, p0, Ldkf;->e:Landroid/content/Context;

    .line 35
    invoke-static {v5}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Ldme;-><init>(Lbew;Ldml;Ljava/lang/Integer;Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v7, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkg;

    invoke-direct {v1, p0}, Ldkg;-><init>(Ldkf;)V

    .line 37
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkh;

    invoke-direct {v1, p0}, Ldkh;-><init>(Ldkf;)V

    .line 38
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 40
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public final a(Ljava/lang/String;Lbao;)V
    .locals 4

    .prologue
    .line 351
    invoke-static {}, Lbdf;->b()V

    .line 352
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    .line 356
    iget-object v1, p2, Lbao;->a:Lhce;

    .line 357
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 358
    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v1, Ldko;->a:Ljava/util/Comparator;

    .line 359
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->sorted(Ljava/util/Comparator;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 360
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 361
    invoke-direct {p0, p1, v0}, Ldkf;->a(Ljava/lang/String;Ljava/util/List;)Lbjn;

    move-result-object v0

    .line 362
    iget-object v1, p0, Ldkf;->c:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    iget-object v1, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    .line 364
    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    new-instance v2, Ldmc;

    iget-object v3, p0, Ldkf;->h:Ldly;

    invoke-direct {v2, v3, v0}, Ldmc;-><init>(Ldly;Lbjn;)V

    .line 365
    invoke-virtual {v1, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Ldkp;

    invoke-direct {v2, p0, v0}, Ldkp;-><init>(Ldkf;Lbjn;)V

    .line 366
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldkq;

    invoke-direct {v1, p0}, Ldkq;-><init>(Ldkf;)V

    .line 367
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 368
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 369
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 370
    return-void
.end method

.method public final a(Ljava/lang/String;Lbjb;)V
    .locals 2

    .prologue
    .line 220
    invoke-static {}, Lbdf;->b()V

    .line 221
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    invoke-static {p1}, Ldkf;->h(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->a(Ljava/util/function/Predicate;)Ldll;

    move-result-object v0

    .line 225
    if-nez v0, :cond_0

    .line 226
    new-instance v0, Ldll;

    invoke-direct {v0, p1}, Ldll;-><init>(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Ldkf;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_0
    iput-object p2, v0, Ldll;->c:Lbjb;

    .line 232
    iput-object p1, v0, Ldll;->b:Ljava/lang/String;

    .line 234
    invoke-virtual {p0}, Ldkf;->f()V

    .line 235
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 203
    invoke-static {}, Lbdf;->b()V

    .line 204
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    invoke-direct {p0, p1}, Ldkf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    const-string v3, "EnrichedCallManagerImpl.sendPostCallNote"

    const-string v4, "number: %s, message: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 208
    invoke-static {v2}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 209
    invoke-static {p2}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    .line 210
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x3c

    if-gt v3, v4, :cond_0

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 212
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Ldmi;

    iget-object v3, p0, Ldkf;->g:Ldml;

    invoke-direct {v1, v3, v2, p2}, Ldmi;-><init>(Ldml;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldlh;

    invoke-direct {v1, p0, p1, p2}, Ldlh;-><init>(Ldkf;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Ldli;

    invoke-direct {v1, p0, p1, p2}, Ldli;-><init>(Ldkf;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 217
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iget-object v1, p0, Ldkf;->f:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    .line 218
    invoke-interface {v0, v1, v2}, Lbdy;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V

    .line 219
    return-void

    :cond_0
    move v0, v1

    .line 211
    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 544
    iget-object v2, p0, Ldkf;->i:Ldmo;

    .line 545
    invoke-static {}, Lbdf;->b()V

    .line 546
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    const-string v3, "VideoShareManager.onIncomingVideoShareInvite"

    const-string v4, "sessionId: %d, number: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 548
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v0

    .line 549
    invoke-static {p3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 550
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 551
    invoke-virtual {v2}, Ldmo;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 552
    const-string v1, "VideoShareManager.onIncomingVideoShareInvite"

    const-string v2, "video share disabled, rejecting invite"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 557
    :goto_0
    return v0

    .line 554
    :cond_0
    iget-object v0, v2, Ldmo;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    iget-object v0, v2, Ldmo;->b:Ljava/util/Set;

    new-instance v3, Ldmp;

    invoke-direct {v3, v2}, Ldmp;-><init>(Ldmo;)V

    invoke-interface {v0, v3}, Ljava/util/Set;->forEach(Ljava/util/function/Consumer;)V

    move v0, v1

    .line 557
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lbjb;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 42
    invoke-static {}, Lbdf;->b()V

    .line 43
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-static {p1}, Ldkf;->h(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->a(Ljava/util/function/Predicate;)Ldll;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    new-array v0, v5, [Ljava/lang/Object;

    .line 48
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 49
    const/4 v0, 0x0

    .line 77
    :goto_0
    return-object v0

    .line 51
    :cond_0
    iget-object v3, v0, Ldll;->c:Lbjb;

    .line 53
    invoke-virtual {v3}, Lbjb;->b()Z

    move-result v0

    .line 54
    invoke-virtual {v3}, Lbjb;->c()Z

    move-result v2

    .line 56
    iget-object v4, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v4}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v4

    const-string v6, "enable_rcs_post_call"

    invoke-interface {v4, v6, v5}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    .line 59
    :goto_1
    if-eqz v4, :cond_1

    move v0, v1

    .line 62
    :cond_1
    iget-object v4, p0, Ldkf;->i:Ldmo;

    invoke-virtual {v4}, Ldmo;->a()Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v5

    .line 63
    :goto_2
    if-eqz v4, :cond_2

    move v2, v1

    .line 65
    :cond_2
    invoke-virtual {v3}, Lbjb;->b()Z

    move-result v4

    if-ne v0, v4, :cond_3

    .line 66
    invoke-virtual {v3}, Lbjb;->c()Z

    move-result v4

    if-eq v2, v4, :cond_7

    .line 67
    :cond_3
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 68
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v1

    .line 69
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    .line 70
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    .line 72
    invoke-virtual {v3}, Lbjb;->e()Lbjc;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v0}, Lbjc;->b(Z)Lbjc;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v2}, Lbjc;->c(Z)Lbjc;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lbjc;->a()Lbjb;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_4
    iget-object v4, p0, Ldkf;->e:Landroid/content/Context;

    const-string v6, "android.permission.SEND_SMS"

    invoke-static {v4, v6}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_5

    move v4, v5

    goto :goto_1

    :cond_5
    move v4, v1

    goto :goto_1

    :cond_6
    move v4, v1

    .line 62
    goto :goto_2

    :cond_7
    move-object v0, v3

    .line 77
    goto :goto_0
.end method

.method public final b(J)Lbjl;
    .locals 1

    .prologue
    .line 259
    invoke-static {}, Lbdf;->b()V

    .line 260
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v1, Ldkk;->a:Ljava/util/function/Function;

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v0

    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lbao;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 279
    invoke-static {}, Lbdf;->b()V

    .line 280
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    iget-object v0, p2, Lbao;->a:Lhce;

    .line 284
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 297
    :goto_0
    return-object v0

    .line 286
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 288
    iget-object v1, p2, Lbao;->a:Lhce;

    .line 289
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 290
    invoke-virtual {v0}, Ljava/util/ArrayList;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    sget-object v1, Ldkn;->a:Ljava/util/Comparator;

    .line 291
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->sorted(Ljava/util/Comparator;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 292
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 293
    iget-object v1, p0, Ldkf;->c:Ljava/util/Map;

    .line 294
    invoke-direct {p0, p1, v0}, Ldkf;->a(Ljava/lang/String;Ljava/util/List;)Lbjn;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 295
    if-nez v1, :cond_1

    .line 296
    const/4 v0, 0x0

    goto :goto_0

    .line 297
    :cond_1
    invoke-direct {p0, v0, v1}, Ldkf;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(JLbln;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 508
    invoke-static {}, Lbdf;->b()V

    .line 509
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    const-string v2, "EnrichedCallManagerImpl.onIncomingCallComposerData"

    const-string v3, "sessionId: %d, multimediaData: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 511
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p3, v4, v0

    .line 512
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 513
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v2

    invoke-direct {p0, v2}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v2

    .line 514
    if-eqz v2, :cond_0

    :goto_0
    const-string v3, "No session for incoming call composer data"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lbdf;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 515
    invoke-interface {v2, p3, v1}, Lbjl;->a(Lbln;I)V

    .line 516
    invoke-virtual {p0}, Ldkf;->g()V

    .line 517
    return-void

    :cond_0
    move v0, v1

    .line 514
    goto :goto_0
.end method

.method public final b(JLjava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 473
    invoke-static {}, Lbdf;->b()V

    .line 474
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    invoke-static {p4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->isMessageState(I)Z

    move-result v2

    invoke-static {v2}, Lbdf;->a(Z)V

    .line 476
    const-string v2, "EnrichedCallManagerImpl.onMessageUpdate"

    const-string v3, "sessionId: %d, messageId: %s, messageState: %s"

    new-array v4, v7, [Ljava/lang/Object;

    .line 477
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    aput-object p3, v4, v0

    const/4 v5, 0x2

    .line 478
    invoke-static {p4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->messageStateToString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 479
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 480
    invoke-static {p1, p2}, Ldkf;->f(J)Ljava/util/function/Predicate;

    move-result-object v2

    invoke-direct {p0, v2}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v2

    .line 481
    if-eqz v2, :cond_0

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 482
    invoke-virtual {v2, p3, p4}, Ldmj;->a(Ljava/lang/String;I)V

    .line 484
    iget v0, v2, Ldmj;->e:I

    .line 485
    if-ne v0, v7, :cond_2

    .line 487
    const-string v0, "EnrichedCallManagerImpl.handleOutgoingPostCallMessageUpdate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 489
    invoke-virtual {v2}, Ldmj;->c()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_1

    .line 490
    const-string v0, "EnrichedCallManagerImpl.handleOutgoingPostCallMessageUpdate"

    const-string v3, "failed to send Rcs message, falling back to Sms"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    iget-object v0, v2, Ldmj;->c:Ljava/lang/String;

    .line 494
    iget-object v1, v2, Ldmj;->d:Lbln;

    .line 495
    invoke-virtual {v1}, Lbln;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ldkf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 481
    goto :goto_0

    .line 498
    :cond_1
    iget-object v0, v2, Ldmj;->c:Ljava/lang/String;

    .line 499
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v0, v2, v4, v5}, Ldkf;->a(Ljava/lang/String;Ldmj;J)V

    goto :goto_1

    .line 501
    :cond_2
    invoke-virtual {p0}, Ldkf;->g()V

    goto :goto_1
.end method

.method public final b(Lbjg;)V
    .locals 4

    .prologue
    .line 89
    const-string v0, "EnrichedCallManagerImpl.unregisterCapabilitiesListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    invoke-static {}, Lbdf;->b()V

    .line 91
    iget-object v0, p0, Ldkf;->k:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public final b(Lbji;)V
    .locals 4

    .prologue
    .line 269
    invoke-static {}, Lbdf;->b()V

    .line 270
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    const-string v0, "EnrichedCallManagerImpl.unregisterHistoricalDataChangedListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    iget-object v0, p0, Ldkf;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 273
    return-void
.end method

.method public final b(Lbjj;)V
    .locals 4

    .prologue
    .line 385
    invoke-static {}, Lbdf;->b()V

    .line 386
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    const-string v0, "EnrichedCallManagerImpl.unregisterStateChangedListener"

    const-string v1, "listener: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 388
    iget-object v0, p0, Ldkf;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 389
    return-void
.end method

.method public final b(Lbjw;)V
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Ldkf;->i:Ldmo;

    .line 641
    const-string v1, "VideoShareManager.unregisterListener"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 642
    invoke-static {}, Lbdf;->b()V

    .line 643
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    iget-object v0, v0, Ldmo;->b:Ljava/util/Set;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 645
    return-void
.end method

.method public final b(Lcdc;)V
    .locals 0

    .prologue
    .line 653
    return-void
.end method

.method final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    const-string v1, "android.permission.SEND_SMS"

    invoke-static {v0, v1}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 504
    const-string v0, "EnrichedCallManagerImpl.handleOutgoingPostCallMessageUpdate"

    const-string v1, "no SEND_SMS permission"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 507
    :goto_0
    return-void

    .line 506
    :cond_0
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)J
    .locals 8

    .prologue
    const/4 v5, 0x1

    const-wide/16 v0, -0x1

    const/4 v7, 0x0

    .line 93
    invoke-static {}, Lbdf;->b()V

    .line 94
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    invoke-direct {p0, p1}, Ldkf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 96
    const-string v2, "EnrichedCallManagerImpl.startCallComposerSession"

    const-string v4, "number: %s"

    new-array v5, v5, [Ljava/lang/Object;

    .line 97
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 98
    invoke-static {v2, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget-object v2, p0, Ldkf;->g:Ldml;

    .line 100
    iget-object v2, v2, Ldml;->a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    .line 101
    invoke-virtual {v2}, Lgng;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 102
    const-string v2, "EnrichedCallManagerImpl.startCallComposerSession"

    const-string v4, "service not connected"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    invoke-direct {p0, v3}, Ldkf;->g(Ljava/lang/String;)V

    .line 126
    :goto_0
    return-wide v0

    .line 105
    :cond_0
    :try_start_0
    iget-object v2, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v2}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v2

    sget-object v4, Ldnt$a;->n:Ldnt$a;

    .line 106
    invoke-virtual {v4}, Ldnt$a;->getNumber()I

    move-result v4

    .line 107
    invoke-interface {v2, v4}, Lbku;->a(I)V

    .line 108
    iget-object v2, p0, Ldkf;->g:Ldml;

    .line 110
    iget-object v2, v2, Ldml;->a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    .line 111
    invoke-virtual {v2, v3}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;->startCallComposerSession(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    move-result-object v2

    .line 112
    const-string v4, "EnrichedCallManagerImpl.startCallComposerSession"

    const-string v5, "startCallComposerSession result: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    invoke-virtual {v2}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->getCode()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 114
    const-string v4, "EnrichedCallManagerImpl.startCallComposerSession"

    const-string v5, "rcs not initialized"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    invoke-virtual {p0}, Ldkf;->i()V

    .line 116
    invoke-virtual {p0}, Ldkf;->f()V

    .line 117
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->succeeded()Z

    move-result v4

    if-nez v4, :cond_2

    .line 118
    invoke-direct {p0}, Ldkf;->j()V

    .line 119
    invoke-direct {p0, v3}, Ldkf;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Lgek; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v2

    .line 123
    const-string v4, "EnrichedCallManagerImpl.startCallComposerSession"

    const-string v5, "exception when starting session"

    invoke-static {v4, v5, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    invoke-direct {p0}, Ldkf;->j()V

    .line 125
    invoke-direct {p0, v3}, Ldkf;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->getSessionId()J
    :try_end_1
    .catch Lgek; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()Lbjh;
    .locals 1

    .prologue
    .line 262
    sget-object v0, Ldkl;->a:Lbjh;

    return-object v0
.end method

.method public final c(Lcdc;)V
    .locals 0

    .prologue
    .line 654
    return-void
.end method

.method public final c(J)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldkf;->i:Ldmo;

    invoke-virtual {v0, p1, p2}, Ldmo;->a(J)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Ldkf;->i:Ldmo;

    invoke-direct {p0, p1}, Ldkf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmo;->c(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()Lbjh;
    .locals 1

    .prologue
    .line 263
    sget-object v0, Ldkm;->a:Lbjh;

    return-object v0
.end method

.method public final d(J)Lbjx;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ldkf;->i:Ldmo;

    .line 161
    invoke-static {}, Lbdf;->b()V

    .line 162
    iget-object v0, v0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->getSession(J)Lbjx;

    move-result-object v0

    .line 163
    return-object v0
.end method

.method public final d(Lcdc;)V
    .locals 0

    .prologue
    .line 655
    return-void
.end method

.method public final e(Ljava/lang/String;)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 129
    iget-object v2, p0, Ldkf;->i:Ldmo;

    invoke-direct {p0, p1}, Ldkf;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-static {}, Lbdf;->b()V

    .line 131
    invoke-static {v3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-virtual {v2}, Ldmo;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 141
    :goto_0
    return-wide v0

    .line 134
    :cond_0
    iget-object v2, v2, Ldmo;->c:Ljava/util/Map;

    .line 135
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 136
    invoke-interface {v2}, Ljava/util/Set;->stream()Ljava/util/stream/Stream;

    move-result-object v2

    new-instance v4, Ldmq;

    invoke-direct {v4, v3}, Ldmq;-><init>(Ljava/lang/String;)V

    .line 137
    invoke-interface {v2, v4}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v2

    sget-object v3, Ldmr;->a:Ljava/util/function/Function;

    .line 138
    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v2

    .line 139
    invoke-interface {v2}, Ljava/util/stream/Stream;->findFirst()Ljava/util/Optional;

    move-result-object v2

    .line 140
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final e(J)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 142
    iget-object v0, p0, Ldkf;->i:Ldmo;

    .line 143
    invoke-static {}, Lbdf;->b()V

    .line 144
    const-string v1, "VideoShareManager.endVideoShareSession"

    const-string v2, "sessionId: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    :try_start_0
    iget-object v1, v0, Ldmo;->d:Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->succeeded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 147
    const-string v2, "VideoShareManager.endVideoShareSession"

    const-string v3, "ending session failed: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 148
    invoke-virtual {v1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 149
    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget-object v0, v0, Ldmo;->c:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lgek; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const-string v1, "VideoShareManager.endVideoShareSession"

    const-string v2, "ending session failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final e(Lcdc;)V
    .locals 0

    .prologue
    .line 649
    return-void
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 155
    invoke-static {}, Lbdf;->b()V

    .line 156
    iget-object v0, p0, Ldkf;->e:Landroid/content/Context;

    invoke-static {v0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enriched_call_made"

    const/4 v2, 0x0

    .line 158
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 159
    return v0
.end method

.method final f()V
    .locals 6

    .prologue
    .line 81
    invoke-static {}, Lbdf;->b()V

    .line 82
    iget-object v0, p0, Ldkf;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjg;

    .line 83
    if-nez v0, :cond_0

    .line 84
    const-string v0, "EnrichedCallManagerImpl.notifyCapabilitiesListeners"

    const-string v2, "found null listener. capabilitiesListeners: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Ldkf;->k:Ljava/util/Set;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 86
    :cond_0
    invoke-interface {v0}, Lbjg;->f()V

    goto :goto_0

    .line 88
    :cond_1
    return-void
.end method

.method public final f(Lcdc;)V
    .locals 0

    .prologue
    .line 650
    return-void
.end method

.method final g()V
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lbdf;->b()V

    .line 165
    iget-object v0, p0, Ldkf;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjj;

    .line 166
    invoke-interface {v0}, Lbjj;->m_()V

    goto :goto_0

    .line 168
    :cond_0
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 0

    .prologue
    .line 651
    return-void
.end method

.method final h()V
    .locals 2

    .prologue
    .line 274
    invoke-static {}, Lbdf;->b()V

    .line 275
    iget-object v0, p0, Ldkf;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbji;

    .line 276
    invoke-interface {v0}, Lbji;->g()V

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method public final h(Lcdc;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 558
    const-string v0, "EnrichedCallManagerImpl.onDisconnect"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 559
    if-nez p1, :cond_0

    .line 586
    :goto_0
    return-void

    .line 562
    :cond_0
    iget-object v0, p1, Lcdc;->b:Ljava/lang/String;

    .line 563
    invoke-static {v0}, Ldkf;->j(Ljava/lang/String;)Ljava/util/function/Predicate;

    move-result-object v0

    invoke-direct {p0, v0}, Ldkf;->b(Ljava/util/function/Predicate;)Ldmj;

    move-result-object v0

    .line 564
    if-nez v0, :cond_1

    .line 565
    const-string v0, "EnrichedCallManagerImpl.onDisconnect"

    const-string v1, "disconnected call with no enriched call session"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 568
    :cond_1
    iget v1, v0, Ldmj;->e:I

    .line 569
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 570
    const-string v1, "EnrichedCallManagerImpl.onDisconnect"

    const-string v2, "ending outgoing call composer session"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 572
    iget-wide v2, v0, Ldmj;->a:J

    .line 573
    invoke-virtual {p0, v2, v3}, Ldkf;->a(J)V

    .line 574
    :cond_2
    const-string v1, "EnrichedCallManagerImpl.onDisconnect"

    const-string v2, "removing old multimediaData"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    iget-object v1, p0, Ldkf;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 576
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 578
    iget-object v1, p1, Lcdc;->d:Lcgu;

    .line 580
    iget-wide v4, v1, Lcgu;->e:J

    .line 581
    add-long/2addr v2, v4

    .line 583
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v1

    .line 584
    invoke-direct {p0, v1, v0, v2, v3}, Ldkf;->a(Ljava/lang/String;Ldmj;J)V

    .line 585
    iget-object v0, p0, Ldkf;->d:Ljava/util/List;

    new-instance v1, Ldku;

    invoke-direct {v1, p0, p1}, Ldku;-><init>(Ldkf;Lcdc;)V

    invoke-interface {v0, v1}, Ljava/util/List;->removeIf(Ljava/util/function/Predicate;)Z

    goto :goto_0
.end method

.method final i()V
    .locals 1

    .prologue
    .line 646
    invoke-virtual {p0}, Ldkf;->a()V

    .line 647
    iget-object v0, p0, Ldkf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 648
    return-void
.end method
