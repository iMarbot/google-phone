.class public final Lazk;
.super Landroid/view/SurfaceView;
.source "PG"

# interfaces
.implements Lazf;


# instance fields
.field public final a:Laze;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Laze;

    invoke-direct {v0, p0}, Laze;-><init>(Lazf;)V

    iput-object v0, p0, Lazk;->a:Laze;

    .line 3
    invoke-virtual {p0}, Lazk;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lazl;

    invoke-direct {v1, p0}, Lazl;-><init>(Lazk;)V

    .line 4
    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 5
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 26
    return-object p0
.end method

.method public final a(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lazk;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 29
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lazk;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lazk;->a:Laze;

    invoke-virtual {v0}, Laze;->a()V

    .line 7
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 15
    iget-object v0, p0, Lazk;->a:Laze;

    .line 16
    invoke-virtual {v0}, Laze;->b()V

    .line 17
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 12
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    invoke-virtual {v0}, Layq;->c()V

    .line 13
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lazk;->a:Laze;

    invoke-virtual {v0, p1}, Laze;->a(I)I

    move-result v0

    .line 23
    iget-object v1, p0, Lazk;->a:Laze;

    invoke-virtual {v1, v0, p2}, Laze;->a(II)I

    move-result v1

    .line 24
    invoke-super {p0, v0, v1}, Landroid/view/SurfaceView;->onMeasure(II)V

    .line 25
    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 19
    iget-object v0, p0, Lazk;->a:Laze;

    .line 20
    invoke-virtual {v0}, Laze;->b()V

    .line 21
    return-void
.end method

.method protected final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 8
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 9
    iget-object v0, p0, Lazk;->a:Laze;

    invoke-virtual {v0, p2}, Laze;->b(I)V

    .line 10
    return-void
.end method
