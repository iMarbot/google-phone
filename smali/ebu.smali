.class public final Lebu;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lesq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static b:Letf;

.field private static c:Ledb;

.field private static d:[Lezg;

.field private static e:[Ljava/lang/String;

.field private static f:[[B


# instance fields
.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:I

.field private o:Lebz;

.field private p:Lekm;

.field private q:Leby;

.field private r:Lebw;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Letf;

    invoke-direct {v0, v4}, Letf;-><init>(B)V

    sput-object v0, Lebu;->b:Letf;

    new-instance v0, Lecj;

    invoke-direct {v0}, Lecj;-><init>()V

    sput-object v0, Lebu;->c:Ledb;

    new-instance v0, Lesq;

    const-string v1, "ClearcutLogger.API"

    sget-object v2, Lebu;->c:Ledb;

    sget-object v3, Lebu;->b:Letf;

    invoke-direct {v0, v1, v2, v3}, Lesq;-><init>(Ljava/lang/String;Ledb;Letf;)V

    sput-object v0, Lebu;->a:Lesq;

    new-array v0, v4, [Lezg;

    sput-object v0, Lebu;->d:[Lezg;

    new-array v0, v4, [Ljava/lang/String;

    sput-object v0, Lebu;->e:[Ljava/lang/String;

    new-array v0, v4, [[B

    sput-object v0, Lebu;->f:[[B

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLebz;Lekm;Leby;Lebw;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lebu;->j:I

    iput v1, p0, Lebu;->n:I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebu;->g:Ljava/lang/String;

    invoke-static {p1}, Lebu;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lebu;->h:I

    iput v2, p0, Lebu;->j:I

    iput-object p3, p0, Lebu;->i:Ljava/lang/String;

    iput-object p4, p0, Lebu;->k:Ljava/lang/String;

    iput-object p5, p0, Lebu;->l:Ljava/lang/String;

    iput-boolean v1, p0, Lebu;->m:Z

    iput-object p7, p0, Lebu;->o:Lebz;

    iput-object p8, p0, Lebu;->p:Lekm;

    new-instance v0, Leby;

    invoke-direct {v0}, Leby;-><init>()V

    iput-object v0, p0, Lebu;->q:Leby;

    iput v1, p0, Lebu;->n:I

    iput-object p10, p0, Lebu;->r:Lebw;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 4
    const/4 v2, -0x1

    const/4 v6, 0x0

    invoke-static {p1}, Lecc;->a(Landroid/content/Context;)Lebz;

    move-result-object v7

    .line 5
    sget-object v8, Lekn;->a:Lekn;

    .line 6
    new-instance v10, Lech;

    invoke-direct {v10, p1}, Lech;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v9, v5

    invoke-direct/range {v0 .. v10}, Lebu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLebz;Lekm;Leby;Lebw;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1
    const/4 v2, -0x1

    const/4 v6, 0x0

    invoke-static {p1}, Lecc;->a(Landroid/content/Context;)Lebz;

    move-result-object v7

    .line 2
    sget-object v8, Lekn;->a:Lekn;

    .line 3
    new-instance v10, Lech;

    invoke-direct {v10, p1}, Lech;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v9, v5

    invoke-direct/range {v0 .. v10}, Lebu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLebz;Lekm;Leby;Lebw;)V

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "ClearcutLogger"

    const-string v2, "This can\'t happen."

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lebu;)I
    .locals 1

    iget v0, p0, Lebu;->j:I

    return v0
.end method

.method static synthetic a(Ljava/util/ArrayList;)[I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 13
    .line 14
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    .line 14
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [I

    check-cast p0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    move v3, v0

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v0, v3, 0x1

    aput v5, v1, v3

    move v3, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 15
    goto :goto_0
.end method

.method static synthetic b(Lebu;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebu;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lebu;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebu;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lebu;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebu;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lebu;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic f(Lebu;)Lekm;
    .locals 1

    iget-object v0, p0, Lebu;->p:Lekm;

    return-object v0
.end method

.method static synthetic g(Lebu;)Leby;
    .locals 1

    iget-object v0, p0, Lebu;->q:Leby;

    return-object v0
.end method

.method public static synthetic h(Lebu;)Z
    .locals 1

    iget-boolean v0, p0, Lebu;->m:Z

    return v0
.end method

.method static synthetic i(Lebu;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lebu;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lebu;)I
    .locals 1

    iget v0, p0, Lebu;->h:I

    return v0
.end method

.method static synthetic k(Lebu;)Lebw;
    .locals 1

    iget-object v0, p0, Lebu;->r:Lebw;

    return-object v0
.end method

.method static synthetic l(Lebu;)Lebz;
    .locals 1

    iget-object v0, p0, Lebu;->o:Lebz;

    return-object v0
.end method


# virtual methods
.method public final a(Lebx;)Lebv;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lebv;

    .line 11
    invoke-direct {v0, p0, p1}, Lebv;-><init>(Lebu;Lebx;)V

    .line 12
    return-object v0
.end method

.method public final a([B)Lebv;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lebv;

    .line 8
    invoke-direct {v0, p0, p1}, Lebv;-><init>(Lebu;[B)V

    .line 9
    return-object v0
.end method
