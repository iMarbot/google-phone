.class public final Lhxf;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private a:Ljava/io/PushbackInputStream;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lhxf;-><init>(Ljava/io/InputStream;I)V

    .line 2
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 3
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Lhxf;->b:I

    .line 6
    iput v2, p0, Lhxf;->c:I

    .line 7
    new-instance v0, Ljava/io/PushbackInputStream;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    .line 8
    iput v2, p0, Lhxf;->c:I

    .line 9
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v0}, Ljava/io/PushbackInputStream;->close()V

    .line 11
    return-void
.end method

.method public final read()I
    .locals 5

    .prologue
    const/16 v1, 0xd

    const/4 v2, -0x1

    const/16 v4, 0xa

    .line 12
    iget-object v0, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    .line 13
    if-ne v0, v2, :cond_0

    move v0, v2

    .line 25
    :goto_0
    return v0

    .line 15
    :cond_0
    iget v3, p0, Lhxf;->c:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    if-ne v0, v1, :cond_3

    .line 16
    iget-object v1, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v1}, Ljava/io/PushbackInputStream;->read()I

    move-result v1

    .line 17
    if-eq v1, v2, :cond_1

    .line 18
    iget-object v2, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v2, v1}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 19
    :cond_1
    if-eq v1, v4, :cond_2

    .line 20
    iget-object v1, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v1, v4}, Ljava/io/PushbackInputStream;->unread(I)V

    .line 24
    :cond_2
    :goto_1
    iput v0, p0, Lhxf;->b:I

    goto :goto_0

    .line 21
    :cond_3
    iget v2, p0, Lhxf;->c:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    if-ne v0, v4, :cond_2

    iget v2, p0, Lhxf;->b:I

    if-eq v2, v1, :cond_2

    .line 23
    iget-object v0, p0, Lhxf;->a:Ljava/io/PushbackInputStream;

    invoke-virtual {v0, v4}, Ljava/io/PushbackInputStream;->unread(I)V

    move v0, v1

    goto :goto_1
.end method
