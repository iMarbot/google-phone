.class public Laeh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 5

    .prologue
    .line 20
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    iget-object v2, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 22
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    .line 23
    iget-object v3, v0, Laak;->a:Laam;

    invoke-virtual {v3, v1}, Laam;->a(Landroid/view/View;)I

    move-result v3

    .line 24
    if-ltz v3, :cond_1

    .line 25
    iget-object v4, v0, Laak;->b:Laal;

    invoke-virtual {v4, v3}, Laal;->d(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    invoke-virtual {v0, v1}, Laak;->b(Landroid/view/View;)Z

    .line 27
    :cond_0
    iget-object v0, v0, Laak;->a:Laam;

    invoke-virtual {v0, v3}, Laam;->a(I)V

    .line 28
    :cond_1
    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/view/View;)V

    .line 29
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$k;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 2
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    .line 3
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 4
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 5
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$d;->a(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 7
    :cond_0
    return-void
.end method

.method public b(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    .line 9
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 10
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v1, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$d;->b(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 12
    :cond_0
    return-void
.end method

.method public c(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 14
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v0, :cond_1

    .line 15
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$d;->a(Landroid/support/v7/widget/RecyclerView$r;Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 19
    :cond_0
    :goto_0
    return-void

    .line 17
    :cond_1
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$d;->c(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Laeh;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->l()V

    goto :goto_0
.end method
