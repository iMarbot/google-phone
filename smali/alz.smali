.class final Lalz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Lalw;


# direct methods
.method constructor <init>(Lalw;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lalz;->a:Lalw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2
    iget-object v0, p0, Lalz;->a:Lalw;

    .line 3
    iput-boolean v3, v0, Lalw;->b:Z

    .line 5
    iget-object v0, p0, Lalz;->a:Lalw;

    .line 6
    iget-object v0, v0, Lalw;->a:Ljava/util/List;

    .line 7
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 8
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 9
    const-string v2, "extra_selected_account_handle"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 10
    const-string v0, "extra_set_default"

    iget-object v2, p0, Lalz;->a:Lalw;

    .line 11
    iget-boolean v2, v2, Lalw;->c:Z

    .line 12
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 13
    const-string v0, "extra_call_id"

    iget-object v2, p0, Lalz;->a:Lalw;

    .line 14
    invoke-virtual {v2}, Lalw;->a()Ljava/lang/String;

    move-result-object v2

    .line 15
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lalz;->a:Lalw;

    .line 17
    iget-object v0, v0, Lalw;->d:Lamd;

    .line 18
    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lalz;->a:Lalw;

    .line 20
    iget-object v0, v0, Lalw;->d:Lamd;

    .line 21
    invoke-virtual {v0, v3, v1}, Lamd;->onReceiveResult(ILandroid/os/Bundle;)V

    .line 22
    :cond_0
    return-void
.end method
