.class public final Lbsa;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbry;


# instance fields
.field public final b:Ljava/util/ArrayList;

.field public c:Ljava/lang/String;

.field public d:Z

.field private e:Lbry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lbrx;

    invoke-direct {v0}, Lbrx;-><init>()V

    sput-object v0, Lbsa;->a:Lbry;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lbry;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbsa;->b:Ljava/util/ArrayList;

    .line 3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbsa;->d:Z

    .line 4
    iput-object p1, p0, Lbsa;->c:Ljava/lang/String;

    .line 5
    iput-object p2, p0, Lbsa;->e:Lbry;

    .line 6
    return-void
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;I)Lbrz;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 41
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    :cond_0
    iget-boolean v0, p0, Lbsa;->d:Z

    if-eqz v0, :cond_1

    new-instance v0, Lbrz;

    invoke-direct {v0, p3, p3}, Lbrz;-><init>(II)V

    .line 57
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v3

    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    move v2, p3

    move v0, p3

    .line 45
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge p3, v4, :cond_6

    .line 46
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_6

    .line 47
    invoke-virtual {p1, p3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 48
    iget-object v5, p0, Lbsa;->e:Lbry;

    invoke-interface {v5, v4}, Lbry;->b(C)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 49
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v4, v5, :cond_3

    move-object v0, v3

    .line 50
    goto :goto_0

    .line 51
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 55
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 56
    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    .line 52
    :cond_5
    if-nez v1, :cond_4

    .line 53
    if-eqz v2, :cond_4

    .line 54
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 57
    :cond_6
    new-instance v1, Lbrz;

    add-int/lit8 v2, v2, 0x0

    invoke-direct {v1, v2, v0}, Lbrz;-><init>(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ILbry;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 10
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 11
    invoke-interface {p2, v1}, Lbry;->b(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 13
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lbry;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lbsa;->a(Ljava/lang/String;ILbry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;I)V
    .locals 2

    .prologue
    .line 15
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 16
    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lbrz;)V
    .locals 3

    .prologue
    .line 19
    iget v0, p1, Lbrz;->a:I

    :goto_0
    iget v1, p1, Lbrz;->b:I

    if-ge v0, v1, :cond_0

    .line 20
    add-int/lit8 v1, v0, 0x1

    const-string v2, "1"

    invoke-virtual {p0, v0, v1, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Z
    .locals 12

    .prologue
    .line 58
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v7, v0}, Lbsa;->a(Ljava/lang/StringBuilder;I)V

    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    .line 62
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    .line 63
    if-ge v8, v9, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 122
    :goto_0
    return v0

    .line 65
    :cond_0
    if-nez v9, :cond_1

    .line 66
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v4, 0x0

    .line 68
    const/4 v3, 0x0

    .line 69
    const/4 v1, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v5, v1

    move v6, v3

    move v1, v4

    move v4, v0

    .line 72
    :goto_1
    if-ge v1, v8, :cond_b

    if-ge v6, v9, :cond_b

    .line 73
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 74
    iget-object v3, p0, Lbsa;->e:Lbry;

    invoke-interface {v3, v0}, Lbry;->d(C)C

    move-result v0

    .line 75
    iget-object v3, p0, Lbsa;->e:Lbry;

    invoke-interface {v3, v0}, Lbry;->c(C)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 76
    iget-object v3, p0, Lbsa;->e:Lbry;

    invoke-interface {v3, v0}, Lbry;->a(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 77
    iget-object v3, p0, Lbsa;->e:Lbry;

    invoke-interface {v3, v0}, Lbry;->f(C)C

    move-result v0

    .line 78
    :cond_2
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v0, v3, :cond_5

    .line 79
    if-eqz v6, :cond_3

    iget-object v0, p0, Lbsa;->e:Lbry;

    iget-object v3, p0, Lbsa;->e:Lbry;

    add-int/lit8 v4, v1, -0x1

    .line 80
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-interface {v3, v4}, Lbry;->d(C)C

    move-result v3

    .line 81
    invoke-interface {v0, v3}, Lbry;->c(C)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 82
    :cond_3
    :goto_2
    if-ge v1, v8, :cond_4

    iget-object v0, p0, Lbsa;->e:Lbry;

    iget-object v3, p0, Lbsa;->e:Lbry;

    .line 83
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-interface {v3, v4}, Lbry;->d(C)C

    move-result v3

    .line 84
    invoke-interface {v0, v3}, Lbry;->c(C)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 86
    :cond_4
    add-int/lit8 v0, v1, 0x1

    .line 87
    :goto_3
    const/4 v3, 0x0

    .line 88
    const/4 v1, 0x0

    move v4, v1

    move v5, v0

    move v6, v3

    move v1, v0

    .line 89
    goto :goto_1

    .line 90
    :cond_5
    add-int/lit8 v0, v9, -0x1

    if-ne v6, v0, :cond_7

    .line 91
    new-instance v0, Lbrz;

    add-int v1, v9, v5

    add-int/2addr v1, v4

    invoke-direct {v0, v5, v1}, Lbrz;-><init>(II)V

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    check-cast p3, Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v2, :cond_6

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    check-cast v0, Lbrz;

    .line 93
    invoke-static {v7, v0}, Lbsa;->a(Ljava/lang/StringBuilder;Lbrz;)V

    goto :goto_4

    .line 96
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 97
    :cond_7
    if-gtz v6, :cond_e

    move v3, v1

    .line 98
    :goto_5
    if-ge v3, v8, :cond_8

    .line 99
    iget-object v0, p0, Lbsa;->e:Lbry;

    iget-object v10, p0, Lbsa;->e:Lbry;

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-interface {v10, v11}, Lbry;->d(C)C

    move-result v10

    invoke-interface {v0, v10}, Lbry;->c(C)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 100
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 101
    :cond_8
    add-int/lit8 v0, v8, -0x1

    if-ge v3, v0, :cond_e

    .line 102
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 104
    add-int/lit8 v11, v6, 0x1

    invoke-virtual {p2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11, v0}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 105
    add-int/lit8 v2, v3, 0x1

    invoke-static {v0, v2}, Lbrz;->a(Ljava/util/ArrayList;I)V

    .line 106
    const/4 v2, 0x0

    new-instance v3, Lbrz;

    add-int/lit8 v10, v1, 0x1

    invoke-direct {v3, v1, v10}, Lbrz;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 108
    :goto_6
    add-int/lit8 v2, v1, 0x1

    .line 109
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v2

    move-object v2, v0

    goto/16 :goto_1

    .line 110
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 111
    if-nez v6, :cond_a

    move v5, v1

    .line 112
    goto/16 :goto_1

    .line 113
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    .line 114
    goto/16 :goto_1

    .line 115
    :cond_b
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 116
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 117
    check-cast p3, Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v2, :cond_c

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    check-cast v0, Lbrz;

    .line 118
    invoke-static {v7, v0}, Lbsa;->a(Ljava/lang/StringBuilder;Lbrz;)V

    goto :goto_7

    .line 121
    :cond_c
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 122
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_e
    move-object v0, v2

    goto :goto_6

    :cond_f
    move v0, v1

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lbrz;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    iget-boolean v0, p0, Lbsa;->d:Z

    if-eqz v0, :cond_1

    new-instance v0, Lbrz;

    invoke-direct {v0, v2, v2}, Lbrz;-><init>(II)V

    .line 40
    :cond_0
    :goto_0
    return-object v0

    .line 24
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 25
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v1, v0}, Lbsa;->a(Ljava/lang/StringBuilder;I)V

    .line 28
    invoke-direct {p0, p1, p2, v2}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;I)Lbrz;

    move-result-object v0

    .line 29
    if-nez v0, :cond_4

    .line 30
    invoke-static {p1}, Lbsb;->c(Ljava/lang/String;)Lbsc;

    move-result-object v2

    .line 31
    if-eqz v2, :cond_0

    .line 33
    iget v3, v2, Lbsc;->a:I

    if-eqz v3, :cond_3

    .line 34
    iget v0, v2, Lbsc;->a:I

    invoke-direct {p0, p1, p2, v0}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;I)Lbrz;

    move-result-object v0

    .line 35
    :cond_3
    if-nez v0, :cond_4

    iget v3, v2, Lbsc;->b:I

    if-eqz v3, :cond_4

    .line 36
    iget v0, v2, Lbsc;->b:I

    invoke-direct {p0, p1, p2, v0}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;I)Lbrz;

    move-result-object v0

    .line 37
    :cond_4
    if-eqz v0, :cond_0

    .line 38
    invoke-static {v1, v0}, Lbsa;->a(Ljava/lang/StringBuilder;Lbrz;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lbsa;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    iget-object v0, p0, Lbsa;->c:Ljava/lang/String;

    iget-object v1, p0, Lbsa;->b:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0, v1}, Lbsa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method
