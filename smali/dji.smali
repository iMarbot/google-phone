.class public final Ldji;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# instance fields
.field private a:Ldjg;

.field private b:Lhqc;

.field private c:Lhqc;

.field private d:Lhqc;


# direct methods
.method public constructor <init>(Ldjg;Lhqc;Lhqc;Lhqc;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldji;->a:Ldjg;

    .line 3
    iput-object p2, p0, Ldji;->b:Lhqc;

    .line 4
    iput-object p3, p0, Ldji;->c:Lhqc;

    .line 5
    iput-object p4, p0, Ldji;->d:Lhqc;

    .line 6
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 7
    .line 8
    iget-object v0, p0, Ldji;->b:Lhqc;

    .line 9
    invoke-interface {v0}, Lhqc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Ldji;->c:Lhqc;

    invoke-interface {v1}, Lhqc;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbiy;

    iget-object v2, p0, Ldji;->d:Lhqc;

    invoke-interface {v2}, Lhqc;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldjo;

    .line 10
    const-string v3, "android.permission.READ_PHONE_STATE"

    invoke-static {v0, v3}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 11
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "missing read phone state permission, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    :goto_0
    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    .line 46
    invoke-static {v1, v0}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbis;

    .line 47
    return-object v0

    .line 13
    :cond_0
    invoke-static {v0}, Ldjo;->c(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 14
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "Duo not installed, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 16
    :cond_1
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v4, "force_enable_duo_video_calls"

    invoke-interface {v3, v4, v5}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 17
    const-string v0, "DuoModule.provideDuo"

    const-string v1, "force enabled by flag, returning impl"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    .line 18
    goto :goto_0

    .line 19
    :cond_2
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-ge v3, v4, :cond_3

    .line 20
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "pre-N sdk version, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_3
    const-class v3, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/TelecomManager;

    .line 23
    const-string v4, "tel"

    .line 24
    invoke-virtual {v3, v4}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 25
    if-nez v4, :cond_4

    .line 26
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "no default phone account, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_4
    invoke-virtual {v3, v4}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 29
    if-eqz v3, :cond_5

    .line 30
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 31
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "android.telecom.extra.SUPPORTS_VIDEO_CALLING_FALLBACK"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 32
    :cond_5
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "disabled in phone account, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_6
    const-class v3, Landroid/telephony/CarrierConfigManager;

    .line 35
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/CarrierConfigManager;

    .line 36
    invoke-virtual {v3}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v3

    const-string v4, "allow_video_calling_fallback_bool"

    .line 37
    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 38
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "disabled in carrier config, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 40
    :cond_7
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v3, "enable_duo_video_calls"

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 41
    const-string v0, "DuoModule.provideDuo"

    const-string v1, "enabled, returning impl"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v2

    .line 42
    goto/16 :goto_0

    .line 43
    :cond_8
    const-string v0, "DuoModule.provideDuo"

    const-string v2, "disabled via flag, returning stub"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
