.class public final Lfsp;
.super Lfrz;
.source "PG"


# instance fields
.field public final videoSource:Lfsm;


# direct methods
.method public constructor <init>(Lfsm;)V
    .locals 1

    .prologue
    .line 1
    if-nez p1, :cond_0

    const-string v0, "NoSource"

    :goto_0
    invoke-direct {p0, v0}, Lfrz;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p1, p0, Lfsp;->videoSource:Lfsm;

    .line 3
    return-void

    .line 1
    :cond_0
    invoke-virtual {p1}, Lfsm;->getDebugName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final drawFrame()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4
    iget-object v1, p0, Lfsp;->videoSource:Lfsm;

    invoke-virtual {v1}, Lfsm;->getOutputFormat()Lfwe;

    move-result-object v1

    .line 5
    iget-object v2, p0, Lfsp;->videoSource:Lfsm;

    invoke-virtual {v2}, Lfsm;->getTextureName()I

    move-result v2

    if-eqz v2, :cond_0

    .line 7
    iget v2, v1, Lfwe;->a:I

    .line 8
    if-eqz v2, :cond_0

    .line 10
    iget v2, v1, Lfwe;->b:I

    .line 11
    if-nez v2, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    iget-object v2, p0, Lfsp;->videoSource:Lfsm;

    invoke-virtual {v2}, Lfsm;->getTransformationMatrix()[F

    move-result-object v2

    invoke-virtual {p0, v2}, Lfsp;->setTransformationMatrix([F)V

    .line 14
    iget-object v2, p0, Lfsp;->videoSource:Lfsm;

    .line 15
    invoke-virtual {v2}, Lfsm;->getTextureName()I

    move-result v2

    .line 17
    iget v3, v1, Lfwe;->a:I

    .line 20
    iget v4, v1, Lfwe;->b:I

    .line 21
    iget-object v5, p0, Lfsp;->videoSource:Lfsm;

    .line 22
    invoke-virtual {v5}, Lfsm;->isExternalTexture()Z

    move-result v5

    .line 23
    invoke-virtual {p0, v2, v3, v4, v5}, Lfsp;->setInputInfo(IIIZ)V

    .line 25
    iget-object v2, v1, Lfwe;->f:Landroid/graphics/RectF;

    .line 26
    invoke-virtual {p0, v2}, Lfsp;->setInputCropping(Landroid/graphics/RectF;)V

    .line 28
    iget-object v2, v1, Lfwe;->e:Landroid/graphics/RectF;

    .line 29
    invoke-virtual {p0, v2}, Lfsp;->setRegionOfInterest(Landroid/graphics/RectF;)V

    .line 30
    iget-object v2, p0, Lfsp;->videoSource:Lfsm;

    invoke-virtual {v2}, Lfsm;->getOutputWidth()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 32
    iget v2, v1, Lfwe;->a:I

    .line 34
    iget v1, v1, Lfwe;->b:I

    .line 35
    invoke-virtual {p0, v2, v1, v0}, Lfsp;->setOutputInfo(IIZ)V

    .line 41
    :goto_1
    invoke-super {p0}, Lfrz;->drawFrame()Z

    move-result v0

    goto :goto_0

    .line 36
    :cond_2
    iget-object v0, p0, Lfsp;->videoSource:Lfsm;

    .line 37
    invoke-virtual {v0}, Lfsm;->getOutputWidth()I

    move-result v0

    iget-object v1, p0, Lfsp;->videoSource:Lfsm;

    .line 38
    invoke-virtual {v1}, Lfsm;->getOutputHeight()I

    move-result v1

    iget-object v2, p0, Lfsp;->videoSource:Lfsm;

    .line 39
    invoke-virtual {v2}, Lfsm;->shouldClipToSurface()Z

    move-result v2

    .line 40
    invoke-virtual {p0, v0, v1, v2}, Lfsp;->setOutputInfo(IIZ)V

    goto :goto_1
.end method
