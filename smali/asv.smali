.class public final Lasv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laua;
.implements Lbgh;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/Map;

.field public final c:Landroid/database/ContentObserver;

.field public d:Z

.field public e:Z

.field private f:Lbgf;

.field private g:Latn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lanz;Laow;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lasv;->b:Ljava/util/Map;

    .line 3
    new-instance v0, Lasw;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lasw;-><init>(Lasv;Landroid/os/Handler;)V

    iput-object v0, p0, Lasv;->c:Landroid/database/ContentObserver;

    .line 4
    iput-object p1, p0, Lasv;->a:Landroid/content/Context;

    .line 5
    new-instance v0, Latn;

    new-instance v1, Latx;

    invoke-direct {v1}, Latx;-><init>()V

    invoke-direct {v0, p1, p2, p3, v1}, Latn;-><init>(Landroid/content/Context;Lane;Lane;Latx;)V

    iput-object v0, p0, Lasv;->g:Latn;

    .line 6
    new-instance v0, Lbgf;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p1, v1, p0}, Lbgf;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;)V

    iput-object v0, p0, Lasv;->f:Lbgf;

    .line 7
    invoke-virtual {p0}, Lasv;->b()V

    .line 8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lasv;->b()V

    .line 43
    return-void
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lasv;->d:Z

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasv;->e:Z

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lasv;->f:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    goto :goto_0
.end method

.method public final b(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11
    new-instance v2, Laty;

    iget-object v0, p0, Lasv;->a:Landroid/content/Context;

    invoke-direct {v2, v0, p1}, Laty;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 12
    invoke-virtual {v2}, Laty;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    invoke-static {}, Lbdf;->b()V

    .line 16
    iget-object v0, p0, Lasv;->a:Landroid/content/Context;

    invoke-static {v0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    invoke-interface {v0}, Lcln;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 17
    const-string v0, "VoicemailErrorManager.addServiceStateListener"

    const-string v2, "VVM module not enabled"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, v2, Laty;->a:Ljava/lang/String;

    iget-object v3, p0, Lasv;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 20
    const-string v0, "VoicemailErrorManager.addServiceStateListener"

    const-string v2, "non-dialer source"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_2
    iget-object v0, p0, Lasv;->a:Landroid/content/Context;

    const-class v3, Landroid/telephony/TelephonyManager;

    .line 23
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 24
    invoke-virtual {v2}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 25
    if-nez v0, :cond_3

    .line 26
    const-string v0, "VoicemailErrorManager.addServiceStateListener"

    const-string v2, "invalid PhoneAccountHandle"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_3
    invoke-virtual {v2}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 29
    iget-object v3, p0, Lasv;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 30
    const-string v3, "VoicemailErrorManager.addServiceStateListener"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "adding listener for "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    new-instance v3, Lasx;

    .line 32
    invoke-direct {v3, p0}, Lasx;-><init>(Lasv;)V

    .line 34
    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 35
    iget-object v0, p0, Lasv;->b:Ljava/util/Map;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 37
    :cond_4
    iget-object v0, p0, Lasv;->g:Latn;

    invoke-virtual {v0, v1, p0}, Latn;->a(Ljava/util/List;Laua;)V

    .line 38
    return-void
.end method

.method public final c(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public final d(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 40
    return-void
.end method
