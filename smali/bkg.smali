.class public final Lbkg;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field public a:F

.field public b:F

.field public c:Z

.field public d:I

.field public e:Ljava/lang/Character;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Rect;

.field private h:[C

.field private i:Landroid/content/res/TypedArray;

.field private j:I

.field private k:I

.field private l:I

.field private m:F

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:I

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    .line 3
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lbkg;->g:Landroid/graphics/Rect;

    .line 4
    new-array v0, v2, [C

    iput-object v0, p0, Lbkg;->h:[C

    .line 5
    iput v2, p0, Lbkg;->s:I

    .line 6
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbkg;->a:F

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lbkg;->b:F

    .line 8
    iput-boolean v3, p0, Lbkg;->c:Z

    .line 9
    iput-object v1, p0, Lbkg;->e:Ljava/lang/Character;

    .line 10
    const v0, 0x7f0a000b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lbkg;->i:Landroid/content/res/TypedArray;

    .line 11
    const v0, 0x7f0c00db

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lbkg;->j:I

    .line 12
    const v0, 0x7f0c0096

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lbkg;->k:I

    .line 13
    const v0, 0x7f0c0097

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lbkg;->l:I

    .line 14
    const v0, 0x7f0d0178

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lbkg;->m:F

    .line 15
    const v0, 0x7f020121

    .line 16
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbkg;->n:Landroid/graphics/drawable/Drawable;

    .line 17
    const v0, 0x7f02012b

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbkg;->o:Landroid/graphics/drawable/Drawable;

    .line 18
    const v0, 0x7f02017d

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbkg;->p:Landroid/graphics/drawable/Drawable;

    .line 19
    const v0, 0x7f02016d

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbkg;->q:Landroid/graphics/drawable/Drawable;

    .line 20
    const v0, 0x7f020147

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbkg;->r:Landroid/graphics/drawable/Drawable;

    .line 21
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    const-string v1, "sans-serif-medium"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 22
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 23
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 25
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 26
    iget v0, p0, Lbkg;->k:I

    iput v0, p0, Lbkg;->d:I

    .line 27
    return-void
.end method

.method public static a(ZZZIZ)I
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 116
    if-eqz p0, :cond_1

    .line 117
    const/4 v0, 0x3

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    if-eqz p1, :cond_2

    .line 119
    const/4 v0, 0x5

    goto :goto_0

    .line 120
    :cond_2
    if-nez p2, :cond_0

    .line 122
    if-ne p3, v0, :cond_3

    .line 123
    const/4 v0, 0x4

    goto :goto_0

    .line 124
    :cond_3
    if-eqz p4, :cond_4

    .line 125
    const/4 v0, 0x6

    goto :goto_0

    .line 126
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;)Lbkg;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 88
    const/16 v2, 0x41

    if-gt v2, v0, :cond_0

    const/16 v2, 0x5a

    if-le v0, v2, :cond_1

    :cond_0
    const/16 v2, 0x61

    if-gt v2, v0, :cond_2

    const/16 v2, 0x7a

    if-gt v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 89
    :goto_0
    if-eqz v0, :cond_3

    .line 90
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    iput-object v0, p0, Lbkg;->e:Ljava/lang/Character;

    .line 93
    :goto_1
    iget v0, p0, Lbkg;->s:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 94
    iget v0, p0, Lbkg;->j:I

    .line 100
    :goto_2
    iput v0, p0, Lbkg;->d:I

    .line 101
    return-object p0

    :cond_2
    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lbkg;->e:Ljava/lang/Character;

    goto :goto_1

    .line 95
    :cond_4
    iget v0, p0, Lbkg;->s:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    iget v0, p0, Lbkg;->s:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    .line 96
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 97
    :cond_5
    iget v0, p0, Lbkg;->k:I

    goto :goto_2

    .line 98
    :cond_6
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lbkg;->i:Landroid/content/res/TypedArray;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    rem-int/2addr v0, v1

    .line 99
    iget-object v1, p0, Lbkg;->i:Landroid/content/res/TypedArray;

    iget v2, p0, Lbkg;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 102
    if-ne p3, v1, :cond_2

    move v0, v1

    .line 103
    :goto_0
    iput-boolean v0, p0, Lbkg;->c:Z

    .line 104
    if-ne p4, v1, :cond_3

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    if-eqz p1, :cond_3

    iget-object v0, p0, Lbkg;->t:Ljava/lang/String;

    .line 105
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    :cond_1
    :goto_1
    return-object p0

    .line 102
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_3
    iput-object p1, p0, Lbkg;->t:Ljava/lang/String;

    .line 109
    iput p4, p0, Lbkg;->s:I

    .line 110
    if-eq p4, v1, :cond_4

    .line 111
    invoke-direct {p0, v2, v2}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;)Lbkg;

    goto :goto_1

    .line 112
    :cond_4
    if-eqz p2, :cond_5

    .line 113
    invoke-direct {p0, p1, p2}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;)Lbkg;

    goto :goto_1

    .line 114
    :cond_5
    invoke-direct {p0, p1, p1}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;)Lbkg;

    goto :goto_1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/16 v0, 0x8a

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v9, 0x3f333333    # 0.7f

    .line 28
    invoke-virtual {p0}, Lbkg;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 29
    invoke-virtual {p0}, Lbkg;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v1, p0, Lbkg;->f:Landroid/graphics/Paint;

    iget v4, p0, Lbkg;->d:I

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    invoke-virtual {p0}, Lbkg;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    .line 34
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 35
    iget-boolean v4, p0, Lbkg;->c:Z

    if-eqz v4, :cond_2

    .line 36
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    int-to-float v6, v6

    div-int/lit8 v7, v1, 0x2

    int-to-float v7, v7

    iget-object v8, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 38
    :goto_1
    iget-object v4, p0, Lbkg;->e:Ljava/lang/Character;

    if-eqz v4, :cond_3

    .line 39
    iget-object v4, p0, Lbkg;->h:[C

    iget-object v6, p0, Lbkg;->e:Ljava/lang/Character;

    invoke-virtual {v6}, Ljava/lang/Character;->charValue()C

    move-result v6

    aput-char v6, v4, v2

    .line 40
    iget-object v4, p0, Lbkg;->f:Landroid/graphics/Paint;

    iget v6, p0, Lbkg;->a:F

    iget v7, p0, Lbkg;->m:F

    mul-float/2addr v6, v7

    int-to-float v1, v1

    mul-float/2addr v1, v6

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 41
    iget-object v1, p0, Lbkg;->f:Landroid/graphics/Paint;

    iget-object v4, p0, Lbkg;->h:[C

    iget-object v6, p0, Lbkg;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v2, v3, v6}, Landroid/graphics/Paint;->getTextBounds([CIILandroid/graphics/Rect;)V

    .line 42
    iget-object v1, p0, Lbkg;->f:Landroid/graphics/Paint;

    const-string v4, "sans-serif"

    invoke-static {v4, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 43
    iget-object v1, p0, Lbkg;->f:Landroid/graphics/Paint;

    iget v4, p0, Lbkg;->l:I

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    iget-object v1, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 45
    iget-object v1, p0, Lbkg;->h:[C

    .line 46
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v4, v0

    .line 47
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v0, v0

    iget v6, p0, Lbkg;->b:F

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v0, v5

    iget-object v5, p0, Lbkg;->g:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    sub-float v5, v0, v5

    iget-object v6, p0, Lbkg;->f:Landroid/graphics/Paint;

    move-object v0, p1

    .line 48
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 37
    :cond_2
    iget-object v4, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 49
    :cond_3
    iget v1, p0, Lbkg;->s:I

    .line 50
    packed-switch v1, :pswitch_data_0

    .line 59
    :pswitch_0
    iget-object v1, p0, Lbkg;->n:Landroid/graphics/drawable/Drawable;

    .line 61
    :goto_2
    if-nez v1, :cond_4

    .line 62
    iget v0, p0, Lbkg;->s:I

    const/16 v1, 0x34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unable to find drawable for contact type "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 51
    :pswitch_1
    iput v9, p0, Lbkg;->a:F

    .line 52
    iget-object v1, p0, Lbkg;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 53
    :pswitch_2
    iput v9, p0, Lbkg;->a:F

    .line 54
    iget-object v1, p0, Lbkg;->p:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 55
    :pswitch_3
    iput v9, p0, Lbkg;->a:F

    .line 56
    iget-object v1, p0, Lbkg;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 57
    :pswitch_4
    iput v9, p0, Lbkg;->a:F

    .line 58
    iget-object v1, p0, Lbkg;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 63
    :cond_4
    iget v2, p0, Lbkg;->a:F

    iget v3, p0, Lbkg;->b:F

    .line 64
    invoke-virtual {p0}, Lbkg;->copyBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 65
    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    float-to-int v2, v2

    .line 67
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    sub-int/2addr v5, v2

    .line 68
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    sub-int/2addr v6, v2

    int-to-float v6, v6

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v3

    add-float/2addr v6, v7

    float-to-int v6, v6

    .line 69
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    add-int/2addr v7, v2

    .line 70
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    add-int/2addr v2, v8

    int-to-float v2, v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v3, v8

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 71
    invoke-virtual {v4, v5, v6, v7, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 73
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 74
    iget-object v2, p0, Lbkg;->q:Landroid/graphics/drawable/Drawable;

    if-ne v1, v2, :cond_5

    const/16 v0, 0xff

    :cond_5
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 75
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, -0x1

    return v0
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lbkg;->c:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lbkg;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setOval(Landroid/graphics/Rect;)V

    .line 85
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setAlpha(F)V

    .line 86
    return-void

    .line 84
    :cond_0
    invoke-virtual {p0}, Lbkg;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Outline;->setRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 78
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbkg;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 80
    return-void
.end method
