.class public abstract Lajm;
.super Lajc;
.source "PG"


# static fields
.field public static final i:Laji;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384
    new-instance v0, Lajn;

    invoke-direct {v0}, Lajn;-><init>()V

    sput-object v0, Lajm;->i:Laji;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lajc;-><init>()V

    .line 2
    iput-object v0, p0, Lajm;->a:Ljava/lang/String;

    .line 3
    iput-object v0, p0, Lajm;->b:Ljava/lang/String;

    .line 4
    return-void
.end method

.method static a(Landroid/util/AttributeSet;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1, p2}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected static a(I)Lajg;
    .locals 2

    .prologue
    .line 5
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    return-object v0
.end method

.method protected static a(IZ)Lajg;
    .locals 2

    .prologue
    .line 9
    new-instance v0, Lajh;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/ContactsContract$CommonDataKinds$Event;->getTypeResource(Ljava/lang/Integer;)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajh;-><init>(II)V

    .line 10
    iput-boolean p1, v0, Lajh;->e:Z

    .line 12
    return-object v0
.end method

.method static a(Landroid/util/AttributeSet;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/util/AttributeSet;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1, p2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected static b(I)Lajg;
    .locals 2

    .prologue
    .line 6
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    return-object v0
.end method

.method protected static c(I)Lajg;
    .locals 2

    .prologue
    .line 7
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->getTypeLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    return-object v0
.end method

.method protected static d(I)Lajg;
    .locals 2

    .prologue
    .line 8
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$Im;->getProtocolLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    return-object v0
.end method

.method protected static e(I)Lajg;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lajg;

    invoke-static {p0}, Landroid/provider/ContactsContract$CommonDataKinds$Relation;->getTypeLabelResource(I)I

    move-result v1

    invoke-direct {v0, p0, v1}, Lajg;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lakt;
    .locals 9

    .prologue
    const v8, 0x7f1101fe

    const v7, 0x7f1101fd

    const v3, 0x7f1101fc

    const/16 v6, 0x2061

    const/4 v5, 0x1

    .line 53
    new-instance v0, Lakt;

    const-string v1, "#displayName"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2, v5}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 54
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 55
    new-instance v1, Lakj;

    invoke-direct {v1, v3}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 56
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 57
    iput v5, v0, Lakt;->j:I

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 59
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110182

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 61
    iput-boolean v5, v2, Lajf;->b:Z

    .line 63
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 66
    if-nez v1, :cond_0

    .line 67
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 69
    iput-boolean v5, v2, Lajf;->c:Z

    .line 71
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    invoke-direct {v2, v3, v7, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 74
    iput-boolean v5, v2, Lajf;->c:Z

    .line 76
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 79
    iput-boolean v5, v2, Lajf;->c:Z

    .line 81
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    invoke-direct {v2, v3, v8, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 84
    iput-boolean v5, v2, Lajf;->c:Z

    .line 86
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 89
    iput-boolean v5, v2, Lajf;->c:Z

    .line 91
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :goto_0
    return-object v0

    .line 92
    :cond_0
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 94
    iput-boolean v5, v2, Lajf;->c:Z

    .line 96
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    invoke-direct {v2, v3, v8, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 99
    iput-boolean v5, v2, Lajf;->c:Z

    .line 101
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 104
    iput-boolean v5, v2, Lajf;->c:Z

    .line 106
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    invoke-direct {v2, v3, v7, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 109
    iput-boolean v5, v2, Lajf;->c:Z

    .line 111
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 114
    iput-boolean v5, v2, Lajf;->c:Z

    .line 116
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    .line 362
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 363
    :cond_0
    :goto_0
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    .line 364
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    if-le v2, v1, :cond_5

    .line 365
    :cond_1
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    .line 366
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    add-int/lit8 v0, v1, 0x1

    if-ne v2, v0, :cond_0

    .line 367
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 368
    const-string v2, "DataKind"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    sget-object v0, Lajx;->a:Lajx;

    .line 370
    const-string v2, "kind"

    .line 372
    const/4 v3, 0x0

    invoke-interface {p3, v3, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 374
    iget-object v0, v0, Lajx;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajw;

    .line 375
    if-eqz v0, :cond_2

    .line 376
    invoke-virtual {v0, p1, p2, p3}, Lajw;->a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;

    move-result-object v0

    .line 378
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakt;

    .line 379
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    goto :goto_1

    .line 377
    :cond_2
    new-instance v0, Laje;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Undefined data kind \'"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laje;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_3
    const-string v2, "BaseAccountType.parseEditSchema"

    const-string v3, "Skipping unknown tag "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 383
    :cond_5
    return-void
.end method

.method protected b(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const v5, 0x7f11021a

    const/4 v3, 0x1

    .line 145
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/nickname"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v5, v2, v3}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 146
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 147
    iput v3, v0, Lakt;->j:I

    .line 148
    new-instance v1, Lakj;

    invoke-direct {v1, v5}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 149
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 150
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 151
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 153
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x2061

    invoke-direct {v2, v3, v5, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    return-object v0
.end method

.method protected c(Landroid/content/Context;)Lakt;
    .locals 8

    .prologue
    const v7, 0x7f11026a

    const/16 v6, 0xa

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 155
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/phone_v2"

    invoke-direct {v0, v1, v7, v6, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 156
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 157
    const v1, 0x7f020152

    iput v1, v0, Lakt;->c:I

    .line 158
    const v1, 0x7f1102b8

    iput v1, v0, Lakt;->d:I

    .line 159
    new-instance v1, Lakd;

    invoke-direct {v1}, Lakd;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 160
    new-instance v1, Lakc;

    invoke-direct {v1}, Lakc;-><init>()V

    iput-object v1, v0, Lakt;->g:Laji;

    .line 161
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 162
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 164
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v4}, Lajm;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v5}, Lajm;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 168
    iput-boolean v4, v2, Lajg;->b:Z

    .line 170
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 172
    iput-boolean v4, v2, Lajg;->b:Z

    .line 174
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 176
    iput-boolean v4, v2, Lajg;->b:Z

    .line 178
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x7

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 181
    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 182
    iput-boolean v4, v2, Lajg;->b:Z

    .line 184
    const-string v3, "data3"

    .line 185
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 187
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x8

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 189
    iput-boolean v4, v2, Lajg;->b:Z

    .line 191
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x9

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 193
    iput-boolean v4, v2, Lajg;->b:Z

    .line 195
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v6}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 197
    iput-boolean v4, v2, Lajg;->b:Z

    .line 199
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xb

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 201
    iput-boolean v4, v2, Lajg;->b:Z

    .line 203
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xc

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 205
    iput-boolean v4, v2, Lajg;->b:Z

    .line 207
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xd

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 209
    iput-boolean v4, v2, Lajg;->b:Z

    .line 211
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xe

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 213
    iput-boolean v4, v2, Lajg;->b:Z

    .line 215
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0xf

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 217
    iput-boolean v4, v2, Lajg;->b:Z

    .line 219
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x10

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 221
    iput-boolean v4, v2, Lajg;->b:Z

    .line 223
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x11

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 225
    iput-boolean v4, v2, Lajg;->b:Z

    .line 227
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x12

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 229
    iput-boolean v4, v2, Lajg;->b:Z

    .line 231
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x13

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 233
    iput-boolean v4, v2, Lajg;->b:Z

    .line 235
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/16 v2, 0x14

    invoke-static {v2}, Lajm;->a(I)Lajg;

    move-result-object v2

    .line 237
    iput-boolean v4, v2, Lajg;->b:Z

    .line 239
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 241
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    invoke-direct {v2, v3, v7, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    return-object v0
.end method

.method protected d(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const v5, 0x7f110165

    const/4 v3, 0x1

    .line 243
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/email_v2"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v5, v2, v3}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 244
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 245
    new-instance v1, Lajp;

    invoke-direct {v1}, Lajp;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 246
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 247
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 249
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v3}, Lajm;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lajm;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Lajm;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lajm;->b(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 254
    invoke-static {v2}, Lajm;->b(I)Lajg;

    move-result-object v2

    .line 255
    iput-boolean v3, v2, Lajg;->b:Z

    .line 257
    const-string v3, "data3"

    .line 258
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 260
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 262
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x21

    invoke-direct {v2, v3, v5, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    return-object v0
.end method

.method protected e(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 264
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/postal-address_v2"

    const v2, 0x7f110279

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 265
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 266
    new-instance v1, Lakg;

    invoke-direct {v1}, Lakg;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 267
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 268
    const-string v1, "data2"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 269
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 270
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v4}, Lajm;->c(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lajm;->c(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Lajm;->c(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    .line 274
    invoke-static {v2}, Lajm;->c(I)Lajg;

    move-result-object v2

    .line 276
    iput-boolean v4, v2, Lajg;->b:Z

    .line 278
    const-string v3, "data3"

    .line 280
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 282
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 284
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f11027a

    const v5, 0x22071

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    const/16 v1, 0xa

    iput v1, v0, Lakt;->p:I

    .line 286
    return-object v0
.end method

.method protected f(Landroid/content/Context;)Lakt;
    .locals 7

    .prologue
    const v6, 0x7f110192

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 287
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/im"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v6, v2, v4}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 288
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 289
    new-instance v1, Laju;

    invoke-direct {v1}, Laju;-><init>()V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 290
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 291
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 292
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 293
    const-string v1, "data5"

    iput-object v1, v0, Lakt;->i:Ljava/lang/String;

    .line 294
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->k:Ljava/util/List;

    .line 295
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v4}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    invoke-static {v5}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x4

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x5

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x6

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, 0x7

    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    iget-object v1, v0, Lakt;->k:Ljava/util/List;

    const/4 v2, -0x1

    .line 304
    invoke-static {v2}, Lajm;->d(I)Lajg;

    move-result-object v2

    .line 305
    iput-boolean v4, v2, Lajg;->b:Z

    .line 307
    const-string v3, "data6"

    .line 308
    iput-object v3, v2, Lajg;->d:Ljava/lang/String;

    .line 310
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 312
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x21

    invoke-direct {v2, v3, v6, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    return-object v0
.end method

.method protected g(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const v4, 0x7f110252

    const/16 v5, 0x2001

    const/4 v3, 0x1

    .line 314
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/organization"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v4, v2, v3}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 315
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 316
    new-instance v1, Lakj;

    invoke-direct {v1, v4}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 317
    sget-object v1, Lajm;->i:Laji;

    iput-object v1, v0, Lakt;->h:Laji;

    .line 318
    iput v3, v0, Lakt;->j:I

    .line 319
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 320
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110188

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110189

    invoke-direct {v2, v3, v4, v5}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x0

    return v0
.end method

.method protected h()Lakt;
    .locals 8

    .prologue
    const v3, 0x7f1101fc

    const/16 v7, 0xc1

    const/16 v6, 0x2061

    const/4 v5, 0x1

    .line 17
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/name"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2, v5}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 18
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 19
    new-instance v1, Lakj;

    invoke-direct {v1, v3}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 20
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 21
    iput v5, v0, Lakt;->j:I

    .line 22
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 23
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x7f110182

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data4"

    const v4, 0x7f110204

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 26
    iput-boolean v5, v2, Lajf;->c:Z

    .line 28
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data3"

    const v4, 0x7f1101fd

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 31
    iput-boolean v5, v2, Lajf;->c:Z

    .line 33
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data5"

    const v4, 0x7f1101ff

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 36
    iput-boolean v5, v2, Lajf;->c:Z

    .line 38
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data2"

    const v4, 0x7f1101fe

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 41
    iput-boolean v5, v2, Lajf;->c:Z

    .line 43
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data6"

    const v4, 0x7f110205

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 46
    iput-boolean v5, v2, Lajf;->c:Z

    .line 48
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    invoke-direct {v2, v3, v4, v7}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f110203

    invoke-direct {v2, v3, v4, v7}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f110202

    invoke-direct {v2, v3, v4, v7}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    return-object v0
.end method

.method protected h(Landroid/content/Context;)Lakt;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 323
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/photo"

    invoke-direct {v0, v1, v4, v4, v2}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 324
    iput v2, v0, Lakt;->j:I

    .line 325
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 326
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data15"

    invoke-direct {v2, v3, v4, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    return-object v0
.end method

.method protected i()Lakt;
    .locals 7

    .prologue
    const v4, 0x7f110200

    const/16 v6, 0xc1

    const/4 v5, 0x1

    .line 118
    new-instance v0, Lakt;

    const-string v1, "#phoneticName"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2, v5}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 119
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 120
    new-instance v1, Lakj;

    const v2, 0x7f1101fc

    invoke-direct {v1, v2}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 121
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 122
    iput v5, v0, Lakt;->j:I

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 124
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "#phoneticName"

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 126
    iput-boolean v5, v2, Lajf;->b:Z

    .line 128
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data9"

    const v4, 0x7f110201

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 131
    iput-boolean v5, v2, Lajf;->c:Z

    .line 133
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data8"

    const v4, 0x7f110203

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 136
    iput-boolean v5, v2, Lajf;->c:Z

    .line 138
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data7"

    const v4, 0x7f110202

    invoke-direct {v2, v3, v4, v6}, Lajf;-><init>(Ljava/lang/String;II)V

    .line 141
    iput-boolean v5, v2, Lajf;->c:Z

    .line 143
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    return-object v0
.end method

.method protected i(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const v5, 0x7f1101ce

    .line 328
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/note"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v5, v2, v3}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 329
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 330
    iput v3, v0, Lakt;->j:I

    .line 331
    new-instance v1, Lakj;

    invoke-direct {v1, v5}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 332
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 334
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const v4, 0x24001

    invoke-direct {v2, v3, v5, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    const/16 v1, 0x64

    iput v1, v0, Lakt;->p:I

    .line 336
    return-object v0
.end method

.method protected final j()Lakt;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const v5, 0x7f1101cf

    .line 346
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/sip_address"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v5, v2, v6}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 347
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 348
    new-instance v1, Lakj;

    invoke-direct {v1, v5}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 349
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 351
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x21

    invoke-direct {v2, v3, v5, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    iput v6, v0, Lakt;->j:I

    .line 353
    return-object v0
.end method

.method protected j(Landroid/content/Context;)Lakt;
    .locals 6

    .prologue
    const v5, 0x7f1103c3

    .line 337
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/website"

    const/16 v2, 0xa0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 338
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 339
    new-instance v1, Lakj;

    invoke-direct {v1, v5}, Lakj;-><init>(I)V

    iput-object v1, v0, Lakt;->f:Laji;

    .line 340
    new-instance v1, Lakj;

    const-string v2, "data1"

    invoke-direct {v1, v2}, Lakj;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lakt;->h:Laji;

    .line 341
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 342
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 344
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x11

    invoke-direct {v2, v3, v5, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    return-object v0
.end method

.method protected final k()Lakt;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 354
    new-instance v0, Lakt;

    const-string v1, "vnd.android.cursor.item/group_membership"

    const v2, 0x7f11018e

    const/16 v3, 0x96

    invoke-direct {v0, v1, v2, v3, v5}, Lakt;-><init>(Ljava/lang/String;IIZ)V

    .line 355
    invoke-virtual {p0, v0}, Lajm;->a(Lakt;)Lakt;

    move-result-object v0

    .line 356
    iput v5, v0, Lakt;->j:I

    .line 357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lakt;->l:Ljava/util/List;

    .line 358
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    invoke-direct {v2, v3, v4, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    const/16 v1, 0xa

    iput v1, v0, Lakt;->p:I

    .line 360
    return-object v0
.end method
