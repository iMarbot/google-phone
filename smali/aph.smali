.class abstract Laph;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"


# instance fields
.field private c:Landroid/database/ContentObserver;

.field private d:Landroid/database/DataSetObserver;

.field private e:Landroid/database/Cursor;

.field private f:Landroid/util/SparseIntArray;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    new-instance v0, Lapi;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lapi;-><init>(Laph;Landroid/os/Handler;)V

    iput-object v0, p0, Laph;->c:Landroid/database/ContentObserver;

    .line 3
    new-instance v0, Lapj;

    invoke-direct {v0, p0}, Lapj;-><init>(Laph;)V

    iput-object v0, p0, Laph;->d:Landroid/database/DataSetObserver;

    .line 4
    invoke-direct {p0}, Laph;->b()V

    .line 5
    return-void
.end method

.method private final b()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput v0, p0, Laph;->g:I

    .line 38
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    .line 39
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Laph;->g:I

    return v0
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 23
    if-ltz v0, :cond_0

    iget-object v1, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 24
    :cond_0
    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 26
    :goto_0
    return-void

    .line 25
    :cond_1
    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->append(II)V

    goto :goto_0
.end method

.method protected abstract a(Landroid/database/Cursor;)V
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    if-ne p1, v0, :cond_1

    .line 21
    :cond_0
    :goto_0
    return-void

    .line 8
    :cond_1
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 9
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    iget-object v1, p0, Laph;->c:Landroid/database/ContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 10
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    iget-object v1, p0, Laph;->d:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 11
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 12
    :cond_2
    invoke-direct {p0}, Laph;->b()V

    .line 13
    iput-object p1, p0, Laph;->e:Landroid/database/Cursor;

    .line 14
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Laph;->a(Landroid/database/Cursor;)V

    .line 16
    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    iput v0, p0, Laph;->g:I

    .line 17
    iget-object v0, p0, Laph;->c:Landroid/database/ContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 18
    iget-object v0, p0, Laph;->d:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 20
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    goto :goto_0
.end method

.method protected abstract d()V
.end method

.method public f(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    iget-object v1, p0, Laph;->e:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return-object v0

    .line 33
    :cond_1
    iget-object v1, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v1

    .line 34
    iget-object v2, p0, Laph;->e:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    iget-object v0, p0, Laph;->e:Landroid/database/Cursor;

    goto :goto_0
.end method

.method public g(I)I
    .locals 1

    .prologue
    .line 28
    if-ltz p1, :cond_0

    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Laph;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v0

    goto :goto_0
.end method
