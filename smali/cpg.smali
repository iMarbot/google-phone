.class public final Lcpg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcpe;


# instance fields
.field public a:Lcmc;

.field private b:I

.field private c:I

.field private d:Lcom/android/voicemail/impl/scheduling/BaseTask;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcpg;->b:I

    .line 3
    const/16 v0, 0x1388

    iput v0, p0, Lcpg;->c:I

    .line 4
    return-void
.end method

.method private final d()Z
    .locals 2

    .prologue
    .line 5
    iget v0, p0, Lcpg;->e:I

    iget v1, p0, Lcpg;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 22
    iget-boolean v0, p0, Lcpg;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcpg;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 23
    :cond_0
    iget-boolean v0, p0, Lcpg;->f:Z

    if-nez v0, :cond_1

    .line 24
    const-string v0, "RetryPolicy"

    iget-object v1, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " completed successfully"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    :cond_1
    invoke-direct {p0}, Lcpg;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 26
    const-string v0, "RetryPolicy"

    iget-object v1, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Retry limit for "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reached"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    :cond_2
    const-string v0, "RetryPolicy"

    iget-object v1, p0, Lcpg;->a:Lcmc;

    invoke-virtual {v1}, Lcnw;->d()Landroid/content/ContentValues;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "committing deferred status: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcpg;->a:Lcmc;

    invoke-virtual {v0}, Lcmc;->b()V

    .line 36
    :goto_0
    return-void

    .line 30
    :cond_3
    const-string v0, "RetryPolicy"

    iget-object v1, p0, Lcpg;->a:Lcmc;

    invoke-virtual {v1}, Lcnw;->d()Landroid/content/ContentValues;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "discarding deferred status: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-virtual {v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a()Landroid/content/Intent;

    move-result-object v0

    .line 32
    const-string v1, "extra_retry_count"

    iget v2, p0, Lcpg;->e:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 33
    iget-object v1, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 34
    iget-object v1, v1, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 35
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 6
    iput-object p1, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 7
    const-string v0, "extra_retry_count"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcpg;->e:I

    .line 8
    iget v0, p0, Lcpg;->e:I

    if-lez v0, :cond_0

    .line 9
    const-string v0, "RetryPolicy"

    iget v1, p0, Lcpg;->e:I

    iget-object v2, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcpg;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x38

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "retry #"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " queued, executing in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcpg;->d:Lcom/android/voicemail/impl/scheduling/BaseTask;

    invoke-static {}, Lcom/android/voicemail/impl/scheduling/BaseTask;->d()J

    move-result-wide v2

    iget v1, p0, Lcpg;->c:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(J)V

    .line 12
    :cond_0
    iget-object v0, p1, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 14
    if-nez v0, :cond_1

    .line 15
    const-string v1, "RetryPolicy"

    .line 16
    iget-object v2, p1, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 17
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "null phone account for phoneAccountHandle "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    :cond_1
    iget-object v1, p1, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 20
    invoke-static {v1, v0}, Lcov;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcmc;

    move-result-object v0

    iput-object v0, p0, Lcpg;->a:Lcmc;

    .line 21
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpg;->f:Z

    .line 38
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
