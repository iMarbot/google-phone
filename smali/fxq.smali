.class final Lfxq;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfxb;
.implements Lgaj;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfxq$a;,
        Lfxq$b;
    }
.end annotation


# static fields
.field private static volatile h:Lfxq;


# instance fields
.field public final d:Lfxq$a;

.field public final e:Ljava/util/Map;

.field public final f:Z

.field public final g:Z

.field private i:Lfxe;

.field private j:I


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;ZZI)V
    .locals 6

    .prologue
    .line 17
    sget v4, Lmg$c;->D:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;II)V

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfxq;->e:Ljava/util/Map;

    .line 19
    invoke-static {p2}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    iput-object v0, p0, Lfxq;->i:Lfxe;

    .line 20
    iput-boolean p4, p0, Lfxq;->f:Z

    .line 21
    iput-boolean p5, p0, Lfxq;->g:Z

    .line 22
    invoke-static {p2}, Lgcl;->a(Landroid/app/Application;)I

    move-result v0

    iput v0, p0, Lfxq;->j:I

    .line 23
    new-instance v0, Lfxq$a;

    new-instance v1, Lfxq$b;

    invoke-direct {v1, p0}, Lfxq$b;-><init>(Lfxq;)V

    invoke-direct {v0, v1, p5}, Lfxq$a;-><init>(Lfxq$b;Z)V

    iput-object v0, p0, Lfxq;->d:Lfxq$a;

    .line 24
    iget-object v0, p0, Lfxq;->i:Lfxe;

    iget-object v1, p0, Lfxq;->d:Lfxq$a;

    invoke-virtual {v0, v1}, Lfxe;->a(Lfwt;)V

    .line 25
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Lfzz;)Lfxq;
    .locals 8

    .prologue
    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lhcw;->b(Z)V

    .line 2
    sget-object v0, Lfxq;->h:Lfxq;

    if-nez v0, :cond_1

    .line 3
    const-class v7, Lfxq;

    monitor-enter v7

    .line 4
    :try_start_0
    sget-object v0, Lfxq;->h:Lfxq;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Lfxq;

    .line 7
    iget-boolean v4, p3, Lfzz;->e:Z

    .line 10
    iget-boolean v5, p3, Lfzz;->c:Z

    .line 13
    iget v6, p3, Lfzz;->f:I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 14
    invoke-direct/range {v0 .. v6}, Lfxq;-><init>(Lgdc;Landroid/app/Application;Lgax;ZZI)V

    sput-object v0, Lfxq;->h:Lfxq;

    .line 15
    :cond_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    :cond_1
    sget-object v0, Lfxq;->h:Lfxq;

    return-object v0

    .line 1
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 15
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(I)V
    .locals 4

    .prologue
    .line 31
    iget-object v1, p0, Lfxq;->e:Ljava/util/Map;

    monitor-enter v1

    .line 32
    :try_start_0
    iget-object v0, p0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxt;

    .line 33
    iget v3, p0, Lfxq;->j:I

    invoke-interface {v0, p1, v3}, Lfxt;->a(II)V

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 28
    iget-object v1, p0, Lfxq;->e:Ljava/util/Map;

    monitor-enter v1

    .line 29
    :try_start_0
    iget-object v0, p0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 30
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lfxq;->i:Lfxe;

    iget-object v1, p0, Lfxq;->d:Lfxq$a;

    invoke-virtual {v0, v1}, Lfxe;->b(Lfwt;)V

    .line 37
    iget-object v1, p0, Lfxq;->d:Lfxq$a;

    .line 38
    monitor-enter v1

    .line 39
    :try_start_0
    invoke-virtual {v1}, Lfxq$a;->b()V

    .line 40
    iget-object v0, v1, Lfxq$a;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, v1, Lfxq$a;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, v1, Lfxq$a;->c:Landroid/os/HandlerThread;

    .line 43
    const/4 v0, 0x0

    iput-object v0, v1, Lfxq$a;->d:Landroid/os/Handler;

    .line 44
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    iget-object v1, p0, Lfxq;->e:Ljava/util/Map;

    monitor-enter v1

    .line 46
    :try_start_1
    iget-object v0, p0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 47
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 44
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 47
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method
