.class public abstract Laya;
.super Lip;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laya$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    return-void
.end method


# virtual methods
.method public final T()Laya$a;
    .locals 1

    .prologue
    .line 7
    const-class v0, Laya$a;

    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laya$a;

    return-object v0
.end method

.method public abstract U()Z
.end method

.method public abstract V()V
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 3
    const-class v0, Laya$a;

    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4
    const-string v0, "CallComposerFragment.onAttach"

    const-string v1, "Container activity must implement CallComposerListener."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    invoke-static {}, Lbdf;->a()V

    .line 6
    :cond_0
    return-void
.end method
