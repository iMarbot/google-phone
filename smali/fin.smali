.class public final Lfin;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfin$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lfiz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    sget-object v0, Lfim;->a:Ljava/util/concurrent/Executor;

    .line 4
    iput-object v0, p0, Lfin;->a:Ljava/util/concurrent/Executor;

    .line 5
    new-instance v1, Lfit;

    .line 6
    invoke-direct {v1}, Lfit;-><init>()V

    .line 7
    new-instance v0, Lfin$a;

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lfin$a;-><init>(Landroid/content/Context;)V

    .line 9
    invoke-static {v0}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfin$a;

    iput-object v0, v1, Lfit;->a:Lfin$a;

    .line 10
    iget-object v0, v1, Lfit;->a:Lfin$a;

    if-nez v0, :cond_0

    .line 11
    new-instance v0, Ljava/lang/IllegalStateException;

    const-class v1, Lfin$a;

    .line 12
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :cond_0
    iget-object v0, v1, Lfit;->b:Lfjd;

    if-nez v0, :cond_1

    .line 14
    new-instance v0, Lfjd;

    invoke-direct {v0}, Lfjd;-><init>()V

    iput-object v0, v1, Lfit;->b:Lfjd;

    .line 15
    :cond_1
    new-instance v0, Lfis;

    .line 16
    invoke-direct {v0, v1}, Lfis;-><init>(Lfit;)V

    .line 18
    invoke-interface {v0}, Lfip;->a()Lfiz;

    move-result-object v0

    iput-object v0, p0, Lfin;->b:Lfiz;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 20
    :try_start_0
    iget-object v0, p0, Lfin;->b:Lfiz;

    invoke-interface {v0, p1}, Lfiz;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    return-void

    .line 22
    :catch_0
    move-exception v0

    .line 23
    const-string v1, "AuthUtil.invalidateAuthTokenSynchronous, failed to invalidate access token"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
