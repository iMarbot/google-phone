.class public final Lglt;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lgmb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lglt;->a:Ljava/lang/Integer;

    .line 4
    iput-object v0, p0, Lglt;->c:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lglt;->d:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lglt;->e:Ljava/lang/Long;

    .line 7
    iput-object v0, p0, Lglt;->f:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lglt;->g:Ljava/lang/Integer;

    .line 9
    iput-object v0, p0, Lglt;->h:Ljava/lang/Long;

    .line 10
    iput-object v0, p0, Lglt;->b:Ljava/lang/Long;

    .line 11
    iput-object v0, p0, Lglt;->i:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lglt;->j:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lglt;->k:Lgmb;

    .line 14
    iput-object v0, p0, Lglt;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lglt;->cachedSize:I

    .line 16
    return-void
.end method

.method private a(Lhfp;)Lglt;
    .locals 3

    .prologue
    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 79
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    :sswitch_0
    return-object p0

    .line 81
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 83
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 84
    invoke-static {v2}, Lhcw;->d(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lglt;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 88
    invoke-virtual {p0, p1, v0}, Lglt;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 90
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglt;->c:Ljava/lang/String;

    goto :goto_0

    .line 92
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglt;->d:Ljava/lang/String;

    goto :goto_0

    .line 95
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 96
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lglt;->e:Ljava/lang/Long;

    goto :goto_0

    .line 99
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 100
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lglt;->h:Ljava/lang/Long;

    goto :goto_0

    .line 103
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 104
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lglt;->b:Ljava/lang/Long;

    goto :goto_0

    .line 106
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglt;->i:Ljava/lang/String;

    goto :goto_0

    .line 108
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglt;->j:Ljava/lang/String;

    goto :goto_0

    .line 110
    :sswitch_9
    iget-object v0, p0, Lglt;->k:Lgmb;

    if-nez v0, :cond_1

    .line 111
    new-instance v0, Lgmb;

    invoke-direct {v0}, Lgmb;-><init>()V

    iput-object v0, p0, Lglt;->k:Lgmb;

    .line 112
    :cond_1
    iget-object v0, p0, Lglt;->k:Lgmb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 114
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglt;->f:Ljava/lang/String;

    goto :goto_0

    .line 117
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 118
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lglt;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 41
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 42
    iget-object v1, p0, Lglt;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 43
    const/4 v1, 0x1

    iget-object v2, p0, Lglt;->a:Ljava/lang/Integer;

    .line 44
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_0
    iget-object v1, p0, Lglt;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 46
    const/4 v1, 0x2

    iget-object v2, p0, Lglt;->c:Ljava/lang/String;

    .line 47
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lglt;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 49
    const/4 v1, 0x3

    iget-object v2, p0, Lglt;->d:Ljava/lang/String;

    .line 50
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_2
    iget-object v1, p0, Lglt;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 52
    const/4 v1, 0x4

    iget-object v2, p0, Lglt;->e:Ljava/lang/Long;

    .line 53
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_3
    iget-object v1, p0, Lglt;->h:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 55
    const/4 v1, 0x5

    iget-object v2, p0, Lglt;->h:Ljava/lang/Long;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_4
    iget-object v1, p0, Lglt;->b:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 58
    const/4 v1, 0x6

    iget-object v2, p0, Lglt;->b:Ljava/lang/Long;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_5
    iget-object v1, p0, Lglt;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 61
    const/4 v1, 0x7

    iget-object v2, p0, Lglt;->i:Ljava/lang/String;

    .line 62
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_6
    iget-object v1, p0, Lglt;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 64
    const/16 v1, 0x8

    iget-object v2, p0, Lglt;->j:Ljava/lang/String;

    .line 65
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_7
    iget-object v1, p0, Lglt;->k:Lgmb;

    if-eqz v1, :cond_8

    .line 67
    const/16 v1, 0x9

    iget-object v2, p0, Lglt;->k:Lgmb;

    .line 68
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_8
    iget-object v1, p0, Lglt;->f:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 70
    const/16 v1, 0xa

    iget-object v2, p0, Lglt;->f:Ljava/lang/String;

    .line 71
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_9
    iget-object v1, p0, Lglt;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 73
    const/16 v1, 0xb

    iget-object v2, p0, Lglt;->g:Ljava/lang/Integer;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_a
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lglt;->a(Lhfp;)Lglt;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 17
    iget-object v0, p0, Lglt;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lglt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_0
    iget-object v0, p0, Lglt;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lglt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_1
    iget-object v0, p0, Lglt;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lglt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_2
    iget-object v0, p0, Lglt;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 24
    const/4 v0, 0x4

    iget-object v1, p0, Lglt;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 25
    :cond_3
    iget-object v0, p0, Lglt;->h:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 26
    const/4 v0, 0x5

    iget-object v1, p0, Lglt;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 27
    :cond_4
    iget-object v0, p0, Lglt;->b:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 28
    const/4 v0, 0x6

    iget-object v1, p0, Lglt;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 29
    :cond_5
    iget-object v0, p0, Lglt;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 30
    const/4 v0, 0x7

    iget-object v1, p0, Lglt;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_6
    iget-object v0, p0, Lglt;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 32
    const/16 v0, 0x8

    iget-object v1, p0, Lglt;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 33
    :cond_7
    iget-object v0, p0, Lglt;->k:Lgmb;

    if-eqz v0, :cond_8

    .line 34
    const/16 v0, 0x9

    iget-object v1, p0, Lglt;->k:Lgmb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_8
    iget-object v0, p0, Lglt;->f:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 36
    const/16 v0, 0xa

    iget-object v1, p0, Lglt;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 37
    :cond_9
    iget-object v0, p0, Lglt;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 38
    const/16 v0, 0xb

    iget-object v1, p0, Lglt;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 39
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 40
    return-void
.end method
