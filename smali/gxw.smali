.class public Lgxw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Externalizable;


# instance fields
.field private a:Lgxg;

.field private b:Lgxx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lgxw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v0

    iput-object v0, p0, Lgxw;->a:Lgxg;

    .line 3
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 13
    iget-object v0, p0, Lgxw;->b:Lgxx;

    .line 14
    iget v0, v0, Lgxx;->a:I

    .line 16
    if-nez v0, :cond_0

    move-object v0, v3

    .line 48
    :goto_0
    return-object v0

    .line 19
    :cond_0
    add-int/lit8 v1, v0, -0x1

    .line 20
    iget-object v0, p0, Lgxw;->b:Lgxx;

    .line 21
    iget-object v0, v0, Lgxx;->b:Ljava/util/TreeSet;

    move v2, v1

    move-object v1, v0

    .line 23
    :goto_1
    invoke-interface {v1}, Ljava/util/SortedSet;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 24
    invoke-interface {v1}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 25
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 26
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v6, v7, :cond_1

    .line 27
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p1

    :cond_1
    move v5, v2

    move v6, v4

    move v2, v4

    .line 30
    :goto_2
    if-gt v6, v5, :cond_3

    .line 31
    add-int v2, v6, v5

    ushr-int/lit8 v2, v2, 0x1

    .line 32
    iget-object v7, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v7, v2}, Lgxx;->a(I)I

    move-result v7

    .line 33
    int-to-long v8, v7

    cmp-long v8, v8, p1

    if-eqz v8, :cond_3

    .line 34
    int-to-long v8, v7

    cmp-long v7, v8, p1

    if-lez v7, :cond_2

    .line 35
    add-int/lit8 v2, v2, -0x1

    move v5, v2

    .line 36
    goto :goto_2

    .line 37
    :cond_2
    add-int/lit8 v6, v2, 0x1

    .line 38
    goto :goto_2

    .line 41
    :cond_3
    if-gez v2, :cond_4

    move-object v0, v3

    .line 42
    goto :goto_0

    .line 43
    :cond_4
    iget-object v5, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v5, v2}, Lgxx;->a(I)I

    move-result v5

    .line 44
    int-to-long v6, v5

    cmp-long v5, p1, v6

    if-nez v5, :cond_5

    .line 45
    iget-object v0, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v0, v2}, Lgxx;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_5
    invoke-interface {v1, v0}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    move-object v1, v0

    .line 47
    goto :goto_1

    :cond_6
    move-object v0, v3

    .line 48
    goto :goto_0
.end method


# virtual methods
.method public final a(Lgxl;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    iget v1, p1, Lgxl;->b:I

    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 53
    invoke-direct {p0, v0, v1}, Lgxw;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    .prologue
    .line 4
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    new-instance v0, Lgxu;

    invoke-direct {v0}, Lgxu;-><init>()V

    iput-object v0, p0, Lgxw;->b:Lgxx;

    .line 8
    :goto_0
    iget-object v0, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v0, p1}, Lgxx;->a(Ljava/io/ObjectInput;)V

    .line 9
    return-void

    .line 7
    :cond_0
    new-instance v0, Lgxt;

    invoke-direct {v0}, Lgxt;-><init>()V

    iput-object v0, p0, Lgxw;->b:Lgxx;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v0}, Lgxx;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lgxw;->b:Lgxx;

    instance-of v0, v0, Lgxu;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 11
    iget-object v0, p0, Lgxw;->b:Lgxx;

    invoke-virtual {v0, p1}, Lgxx;->a(Ljava/io/ObjectOutput;)V

    .line 12
    return-void
.end method
