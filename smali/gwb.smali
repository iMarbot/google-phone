.class public final Lgwb;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:J

.field public e:[Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lgwb;->a:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lgwb;->g:Ljava/lang/String;

    .line 5
    iput-wide v2, p0, Lgwb;->b:J

    .line 6
    iput-wide v2, p0, Lgwb;->c:J

    .line 7
    iput-wide v2, p0, Lgwb;->d:J

    .line 8
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgwb;->e:[Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lgwb;->f:Ljava/lang/String;

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lgwb;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lgwb;->cachedSize:I

    .line 12
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 34
    iget-object v2, p0, Lgwb;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgwb;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    const/4 v2, 0x1

    iget-object v3, p0, Lgwb;->a:Ljava/lang/String;

    .line 36
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 37
    :cond_0
    iget-object v2, p0, Lgwb;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgwb;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 38
    const/4 v2, 0x2

    iget-object v3, p0, Lgwb;->g:Ljava/lang/String;

    .line 39
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 40
    :cond_1
    iget-wide v2, p0, Lgwb;->b:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 41
    const/4 v2, 0x3

    iget-wide v4, p0, Lgwb;->b:J

    .line 42
    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 43
    :cond_2
    iget-wide v2, p0, Lgwb;->c:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 44
    const/4 v2, 0x4

    iget-wide v4, p0, Lgwb;->c:J

    .line 45
    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 46
    :cond_3
    iget-wide v2, p0, Lgwb;->d:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_4

    .line 47
    const/4 v2, 0x5

    iget-wide v4, p0, Lgwb;->d:J

    .line 48
    invoke-static {v2, v4, v5}, Lhfq;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 49
    :cond_4
    iget-object v2, p0, Lgwb;->e:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgwb;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 52
    :goto_0
    iget-object v4, p0, Lgwb;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 53
    iget-object v4, p0, Lgwb;->e:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 54
    if-eqz v4, :cond_5

    .line 55
    add-int/lit8 v3, v3, 0x1

    .line 57
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 58
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    :cond_6
    add-int/2addr v0, v2

    .line 60
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 61
    :cond_7
    iget-object v1, p0, Lgwb;->f:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lgwb;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 62
    const/4 v1, 0x7

    iget-object v2, p0, Lgwb;->f:Ljava/lang/String;

    .line 63
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 65
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 69
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :sswitch_0
    return-object p0

    .line 71
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgwb;->a:Ljava/lang/String;

    goto :goto_0

    .line 73
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgwb;->g:Ljava/lang/String;

    goto :goto_0

    .line 76
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 77
    iput-wide v2, p0, Lgwb;->b:J

    goto :goto_0

    .line 80
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 81
    iput-wide v2, p0, Lgwb;->c:J

    goto :goto_0

    .line 84
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 85
    iput-wide v2, p0, Lgwb;->d:J

    goto :goto_0

    .line 87
    :sswitch_6
    const/16 v0, 0x32

    .line 88
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 89
    iget-object v0, p0, Lgwb;->e:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 90
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 91
    if-eqz v0, :cond_1

    .line 92
    iget-object v3, p0, Lgwb;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 94
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 95
    invoke-virtual {p1}, Lhfp;->a()I

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 89
    :cond_2
    iget-object v0, p0, Lgwb;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 97
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 98
    iput-object v2, p0, Lgwb;->e:[Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgwb;->f:Ljava/lang/String;

    goto :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 13
    iget-object v0, p0, Lgwb;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwb;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v1, p0, Lgwb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lgwb;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwb;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lgwb;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 17
    :cond_1
    iget-wide v0, p0, Lgwb;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-wide v2, p0, Lgwb;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 19
    :cond_2
    iget-wide v0, p0, Lgwb;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 20
    const/4 v0, 0x4

    iget-wide v2, p0, Lgwb;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 21
    :cond_3
    iget-wide v0, p0, Lgwb;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 22
    const/4 v0, 0x5

    iget-wide v2, p0, Lgwb;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 23
    :cond_4
    iget-object v0, p0, Lgwb;->e:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgwb;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 24
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgwb;->e:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 25
    iget-object v1, p0, Lgwb;->e:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 26
    if-eqz v1, :cond_5

    .line 27
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_6
    iget-object v0, p0, Lgwb;->f:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgwb;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 30
    const/4 v0, 0x7

    iget-object v1, p0, Lgwb;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 32
    return-void
.end method
