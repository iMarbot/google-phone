.class public final Lgqk;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqk;


# instance fields
.field public createActivity:Ljava/lang/Boolean;

.field public deliveryConfirmation:Lgqo;

.field public hangoutId:Ljava/lang/String;

.field public iNTERNALPhoneNumber:Ljava/lang/String;

.field public invitationId:Ljava/lang/Long;

.field public invitedEntity:[Lgqp;

.field public invitee:Lglc;

.field public notificationStatus:Ljava/lang/Integer;

.field public notificationType:Ljava/lang/Integer;

.field public userProvidedMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lhft;-><init>()V

    .line 22
    invoke-virtual {p0}, Lgqk;->clear()Lgqk;

    .line 23
    return-void
.end method

.method public static checkNotificationStatusOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum NotificationStatus"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkNotificationStatusOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgqk;->checkNotificationStatusOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static checkNotificationTypeOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum NotificationType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkNotificationTypeOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgqk;->checkNotificationTypeOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgqk;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lgqk;->_emptyArray:[Lgqk;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lgqk;->_emptyArray:[Lgqk;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lgqk;

    sput-object v0, Lgqk;->_emptyArray:[Lgqk;

    .line 19
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lgqk;->_emptyArray:[Lgqk;

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqk;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lgqk;

    invoke-direct {v0}, Lgqk;-><init>()V

    invoke-virtual {v0, p0}, Lgqk;->mergeFrom(Lhfp;)Lgqk;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqk;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lgqk;

    invoke-direct {v0}, Lgqk;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqk;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqk;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    iput-object v1, p0, Lgqk;->invitationId:Ljava/lang/Long;

    .line 25
    iput-object v1, p0, Lgqk;->hangoutId:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lgqk;->invitee:Lglc;

    .line 27
    invoke-static {}, Lgqp;->emptyArray()[Lgqp;

    move-result-object v0

    iput-object v0, p0, Lgqk;->invitedEntity:[Lgqp;

    .line 28
    iput-object v1, p0, Lgqk;->notificationType:Ljava/lang/Integer;

    .line 29
    iput-object v1, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    .line 30
    iput-object v1, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;

    .line 31
    iput-object v1, p0, Lgqk;->deliveryConfirmation:Lgqo;

    .line 32
    iput-object v1, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lgqk;->unknownFieldData:Lhfv;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lgqk;->cachedSize:I

    .line 36
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 63
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 64
    iget-object v1, p0, Lgqk;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65
    const/4 v1, 0x1

    iget-object v2, p0, Lgqk;->hangoutId:Ljava/lang/String;

    .line 66
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_0
    iget-object v1, p0, Lgqk;->invitee:Lglc;

    if-eqz v1, :cond_1

    .line 68
    const/4 v1, 0x2

    iget-object v2, p0, Lgqk;->invitee:Lglc;

    .line 69
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_1
    iget-object v1, p0, Lgqk;->notificationType:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x3

    iget-object v2, p0, Lgqk;->notificationType:Ljava/lang/Integer;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_2
    iget-object v1, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 74
    const/4 v1, 0x4

    iget-object v2, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    .line 75
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 76
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 77
    add-int/2addr v0, v1

    .line 78
    :cond_3
    iget-object v1, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 79
    const/4 v1, 0x6

    iget-object v2, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;

    .line 80
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_4
    iget-object v1, p0, Lgqk;->deliveryConfirmation:Lgqo;

    if-eqz v1, :cond_5

    .line 82
    const/4 v1, 0x7

    iget-object v2, p0, Lgqk;->deliveryConfirmation:Lgqo;

    .line 83
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_5
    iget-object v1, p0, Lgqk;->invitationId:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 85
    const/16 v1, 0x8

    iget-object v2, p0, Lgqk;->invitationId:Ljava/lang/Long;

    .line 86
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_6
    iget-object v1, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 88
    const/16 v1, 0x9

    iget-object v2, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    .line 89
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_7
    iget-object v1, p0, Lgqk;->invitedEntity:[Lgqp;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lgqk;->invitedEntity:[Lgqp;

    array-length v1, v1

    if-lez v1, :cond_a

    .line 91
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgqk;->invitedEntity:[Lgqp;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 92
    iget-object v2, p0, Lgqk;->invitedEntity:[Lgqp;

    aget-object v2, v2, v0

    .line 93
    if-eqz v2, :cond_8

    .line 94
    const/16 v3, 0xb

    .line 95
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 96
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move v0, v1

    .line 97
    :cond_a
    iget-object v1, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 98
    const/16 v1, 0x63

    iget-object v2, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_b
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 101
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 102
    sparse-switch v0, :sswitch_data_0

    .line 104
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    :sswitch_0
    return-object p0

    .line 106
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqk;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 108
    :sswitch_2
    iget-object v0, p0, Lgqk;->invitee:Lglc;

    if-nez v0, :cond_1

    .line 109
    new-instance v0, Lglc;

    invoke-direct {v0}, Lglc;-><init>()V

    iput-object v0, p0, Lgqk;->invitee:Lglc;

    .line 110
    :cond_1
    iget-object v0, p0, Lgqk;->invitee:Lglc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 112
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 114
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 115
    invoke-static {v3}, Lgqk;->checkNotificationTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgqk;->notificationType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 118
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 119
    invoke-virtual {p0, p1, v0}, Lgqk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 121
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    goto :goto_0

    .line 123
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 125
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 126
    invoke-static {v3}, Lgqk;->checkNotificationStatusOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 129
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 130
    invoke-virtual {p0, p1, v0}, Lgqk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 132
    :sswitch_6
    iget-object v0, p0, Lgqk;->deliveryConfirmation:Lgqo;

    if-nez v0, :cond_2

    .line 133
    new-instance v0, Lgqo;

    invoke-direct {v0}, Lgqo;-><init>()V

    iput-object v0, p0, Lgqk;->deliveryConfirmation:Lgqo;

    .line 134
    :cond_2
    iget-object v0, p0, Lgqk;->deliveryConfirmation:Lgqo;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 137
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v2

    .line 138
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqk;->invitationId:Ljava/lang/Long;

    goto/16 :goto_0

    .line 140
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 142
    :sswitch_9
    const/16 v0, 0x5a

    .line 143
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 144
    iget-object v0, p0, Lgqk;->invitedEntity:[Lgqp;

    if-nez v0, :cond_4

    move v0, v1

    .line 145
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgqp;

    .line 146
    if-eqz v0, :cond_3

    .line 147
    iget-object v3, p0, Lgqk;->invitedEntity:[Lgqp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 149
    new-instance v3, Lgqp;

    invoke-direct {v3}, Lgqp;-><init>()V

    aput-object v3, v2, v0

    .line 150
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 151
    invoke-virtual {p1}, Lhfp;->a()I

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 144
    :cond_4
    iget-object v0, p0, Lgqk;->invitedEntity:[Lgqp;

    array-length v0, v0

    goto :goto_1

    .line 153
    :cond_5
    new-instance v3, Lgqp;

    invoke-direct {v3}, Lgqp;-><init>()V

    aput-object v3, v2, v0

    .line 154
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 155
    iput-object v2, p0, Lgqk;->invitedEntity:[Lgqp;

    goto/16 :goto_0

    .line 157
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x5a -> :sswitch_9
        0x31a -> :sswitch_a
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lgqk;->mergeFrom(Lhfp;)Lgqk;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lgqk;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lgqk;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lgqk;->invitee:Lglc;

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x2

    iget-object v1, p0, Lgqk;->invitee:Lglc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_1
    iget-object v0, p0, Lgqk;->notificationType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 42
    const/4 v0, 0x3

    iget-object v1, p0, Lgqk;->notificationType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 43
    :cond_2
    iget-object v0, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 44
    const/4 v0, 0x4

    iget-object v1, p0, Lgqk;->createActivity:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 45
    :cond_3
    iget-object v0, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x6

    iget-object v1, p0, Lgqk;->notificationStatus:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 47
    :cond_4
    iget-object v0, p0, Lgqk;->deliveryConfirmation:Lgqo;

    if-eqz v0, :cond_5

    .line 48
    const/4 v0, 0x7

    iget-object v1, p0, Lgqk;->deliveryConfirmation:Lgqo;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 49
    :cond_5
    iget-object v0, p0, Lgqk;->invitationId:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 50
    const/16 v0, 0x8

    iget-object v1, p0, Lgqk;->invitationId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 51
    :cond_6
    iget-object v0, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 52
    const/16 v0, 0x9

    iget-object v1, p0, Lgqk;->userProvidedMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 53
    :cond_7
    iget-object v0, p0, Lgqk;->invitedEntity:[Lgqp;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgqk;->invitedEntity:[Lgqp;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 54
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgqk;->invitedEntity:[Lgqp;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 55
    iget-object v1, p0, Lgqk;->invitedEntity:[Lgqp;

    aget-object v1, v1, v0

    .line 56
    if-eqz v1, :cond_8

    .line 57
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 58
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_9
    iget-object v0, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 60
    const/16 v0, 0x63

    iget-object v1, p0, Lgqk;->iNTERNALPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 61
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 62
    return-void
.end method
