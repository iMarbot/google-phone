.class public Lfws;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Application;Lfzn;Lgax;Lgax;Lgan;)Lfwr;
    .locals 8

    .prologue
    .line 1
    invoke-static {}, Lfza;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2
    new-instance v1, Lfza;

    .line 3
    invoke-static {p5}, Lfmk;->a(Lgan;)Lgax;

    move-result-object v0

    invoke-direct {v1, p1, v0}, Lfza;-><init>(Landroid/app/Application;Lgax;)V

    .line 5
    iget-object v7, p5, Lgan;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 8
    if-nez v7, :cond_2

    .line 11
    iget v0, p5, Lgan;->b:I

    .line 12
    invoke-static {v0}, Lfmk;->d(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    move-object v6, v0

    .line 14
    :goto_0
    :try_start_0
    new-instance v5, Lfwu;

    iget-object v0, v1, Lfza;->a:Landroid/app/Application;

    .line 15
    invoke-static {v0}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    invoke-direct {v5, v0}, Lfwu;-><init>(Lfxe;)V

    .line 16
    new-instance v0, Lfzb;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfzb;-><init>(Lfza;Lfzn;Lgax;Lgax;Lfwu;)V

    .line 17
    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    :goto_1
    if-eq v6, v7, :cond_0

    .line 23
    invoke-interface {v6}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 24
    :cond_0
    new-instance v0, Lfwr;

    invoke-direct {v0, v1}, Lfwr;-><init>(Lfyz;)V

    .line 25
    :goto_2
    return-object v0

    .line 19
    :catch_0
    move-exception v0

    .line 20
    const-string v2, "Primes"

    const-string v3, "Primes failed to initialized"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v0, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 21
    invoke-virtual {v1}, Lfza;->d()V

    goto :goto_1

    .line 25
    :cond_1
    new-instance v0, Lfwr;

    new-instance v1, Lfyu;

    invoke-direct {v1}, Lfyu;-><init>()V

    invoke-direct {v0, v1}, Lfwr;-><init>(Lfyz;)V

    goto :goto_2

    :cond_2
    move-object v6, v7

    goto :goto_0
.end method
