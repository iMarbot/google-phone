.class public final Laty;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:Z

.field private k:Landroid/net/Uri;

.field private l:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, ""

    invoke-static {p2, v1, v0}, Laty;->a(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laty;->a:Ljava/lang/String;

    .line 3
    invoke-static {p2, v2}, Laty;->a(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Laty;->k:Landroid/net/Uri;

    .line 4
    const/4 v0, 0x2

    invoke-static {p2, v0}, Laty;->a(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Laty;->l:Landroid/net/Uri;

    .line 5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-lt v0, v3, :cond_1

    .line 6
    const/16 v0, 0xa

    const-string v3, "vvm_type_omtp"

    .line 7
    invoke-static {p2, v0, v3}, Laty;->a(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laty;->b:Ljava/lang/String;

    .line 8
    const/16 v0, 0x8

    const-string v3, ""

    .line 9
    invoke-static {p2, v0, v3}, Laty;->a(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laty;->c:Ljava/lang/String;

    .line 10
    const/16 v0, 0x9

    const-string v3, ""

    invoke-static {p2, v0, v3}, Laty;->a(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laty;->d:Ljava/lang/String;

    .line 14
    :goto_0
    const/4 v0, 0x3

    .line 15
    invoke-static {p2, v0, v2}, Laty;->a(Landroid/database/Cursor;II)I

    move-result v0

    iput v0, p0, Laty;->e:I

    .line 16
    const/4 v0, 0x4

    .line 17
    invoke-static {p2, v0, v2}, Laty;->a(Landroid/database/Cursor;II)I

    move-result v0

    iput v0, p0, Laty;->f:I

    .line 18
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Laty;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20
    invoke-virtual {p0}, Laty;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 21
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 22
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 23
    invoke-virtual {v0, v3}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 24
    if-nez v0, :cond_2

    .line 25
    const-string v0, "VoicemailStatus.constructor"

    const-string v3, "invalid PhoneAccountHandle"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move v0, v2

    .line 31
    :goto_1
    iput v0, p0, Laty;->g:I

    .line 35
    :goto_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    iput-boolean v2, p0, Laty;->j:Z

    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_5

    .line 37
    const/4 v0, 0x6

    .line 38
    invoke-static {p2, v0, v5}, Laty;->a(Landroid/database/Cursor;II)I

    move-result v0

    iput v0, p0, Laty;->h:I

    .line 39
    const/4 v0, 0x7

    .line 40
    invoke-static {p2, v0, v5}, Laty;->a(Landroid/database/Cursor;II)I

    move-result v0

    iput v0, p0, Laty;->i:I

    .line 43
    :goto_4
    return-void

    .line 11
    :cond_1
    const-string v0, "vvm_type_omtp"

    iput-object v0, p0, Laty;->b:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Laty;->c:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Laty;->d:Ljava/lang/String;

    goto :goto_0

    .line 27
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    .line 28
    if-nez v0, :cond_0

    move v0, v1

    .line 29
    goto :goto_1

    .line 32
    :cond_3
    const/4 v0, 0x5

    .line 33
    invoke-static {p2, v0, v2}, Laty;->a(Landroid/database/Cursor;II)I

    move-result v0

    iput v0, p0, Laty;->g:I

    goto :goto_2

    :cond_4
    move v2, v1

    .line 35
    goto :goto_3

    .line 41
    :cond_5
    iput v5, p0, Laty;->h:I

    .line 42
    iput v5, p0, Laty;->i:I

    goto :goto_4
.end method

.method private static a(Landroid/database/Cursor;II)I
    .locals 1

    .prologue
    .line 51
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return p2

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p2

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 48
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-object p2

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Laty;->e:I

    sparse-switch v0, :sswitch_data_0

    .line 46
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 44
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b()Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Laty;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Laty;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    iget-object v1, p0, Laty;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_0

    .line 62
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Laty;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 47
    iget-object v0, p0, Laty;->a:Ljava/lang/String;

    iget-object v1, p0, Laty;->b:Ljava/lang/String;

    iget-object v2, p0, Laty;->k:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Laty;->l:Landroid/net/Uri;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Laty;->e:I

    iget v5, p0, Laty;->f:I

    iget v6, p0, Laty;->g:I

    iget v7, p0, Laty;->h:I

    iget v8, p0, Laty;->i:I

    iget-boolean v9, p0, Laty;->j:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit16 v10, v10, 0xff

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "VoicemailStatus[sourcePackage: "

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ", type:"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", settingsUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", voicemailAccessUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", configurationState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataChannelState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notificationChannelState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", quotaOccupied: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", quotaTotal: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isAirplaneMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
