.class public abstract Leib;
.super Lejb;

# interfaces
.implements Ledd;
.implements Leid;


# instance fields
.field private a:Ljava/util/Set;

.field public final b:Lejm;

.field private h:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;ILejm;Ledl;Ledm;)V
    .locals 9

    .prologue
    .line 1
    invoke-static {p1}, Leie;->a(Landroid/content/Context;)Leie;

    move-result-object v3

    .line 2
    sget-object v4, Lecn;->a:Lecn;

    .line 3
    invoke-static {p5}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ledl;

    invoke-static {p6}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ledm;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Leib;-><init>(Landroid/content/Context;Landroid/os/Looper;Leie;Lecn;ILejm;Ledl;Ledm;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Leie;Lecn;ILejm;Ledl;Ledm;)V
    .locals 10

    .prologue
    .line 7
    if-nez p7, :cond_1

    const/4 v7, 0x0

    :goto_0
    if-nez p8, :cond_2

    const/4 v8, 0x0

    .line 8
    :goto_1
    move-object/from16 v0, p6

    iget-object v9, v0, Lejm;->f:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    .line 9
    invoke-direct/range {v1 .. v9}, Lejb;-><init>(Landroid/content/Context;Landroid/os/Looper;Leie;Lecp;ILejd;Leje;Ljava/lang/String;)V

    move-object/from16 v0, p6

    iput-object v0, p0, Leib;->b:Lejm;

    .line 10
    move-object/from16 v0, p6

    iget-object v1, v0, Lejm;->a:Landroid/accounts/Account;

    .line 11
    iput-object v1, p0, Leib;->h:Landroid/accounts/Account;

    .line 12
    move-object/from16 v0, p6

    iget-object v2, v0, Lejm;->c:Ljava/util/Set;

    .line 15
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/Scope;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Expanding scopes is not permitted, use implied scopes instead"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7
    :cond_1
    new-instance v7, Lejd;

    move-object/from16 v0, p7

    invoke-direct {v7, v0}, Lejd;-><init>(Ledl;)V

    goto :goto_0

    :cond_2
    new-instance v8, Leje;

    move-object/from16 v0, p8

    invoke-direct {v8, v0}, Leje;-><init>(Ledm;)V

    goto :goto_1

    .line 15
    :cond_3
    iput-object v2, p0, Leib;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lejm;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 4
    invoke-static {p1}, Leie;->a(Landroid/content/Context;)Leie;

    move-result-object v3

    .line 5
    sget-object v4, Lecn;->a:Lecn;

    .line 6
    const/16 v5, 0x19

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Leib;-><init>(Landroid/content/Context;Landroid/os/Looper;Leie;Lecn;ILejm;Ledl;Ledm;)V

    return-void
.end method


# virtual methods
.method protected final A_()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Leib;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final j()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Leib;->h:Landroid/accounts/Account;

    return-object v0
.end method

.method public final l()[Lekw;
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lekw;

    return-object v0
.end method
