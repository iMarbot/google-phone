.class final Lfdi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfee;
.implements Lfeh;
.implements Lffu;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lfdd;

.field private c:Lfft;

.field private d:Lfeg;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfdd;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfdi;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfdi;->b:Lfdd;

    .line 4
    new-instance v0, Lfft;

    invoke-direct {v0, p1}, Lfft;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfdi;->c:Lfft;

    .line 5
    iget-object v0, p0, Lfdi;->c:Lfft;

    invoke-virtual {v0, p0}, Lfft;->a(Lffu;)V

    .line 6
    new-instance v0, Lfeg;

    invoke-direct {v0, p1}, Lfeg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfdi;->d:Lfeg;

    .line 7
    iget-object v0, p0, Lfdi;->d:Lfeg;

    invoke-virtual {v0, p0}, Lfeg;->a(Lfeh;)V

    .line 8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 11
    invoke-virtual {p0}, Lfdi;->c()V

    .line 12
    return-void
.end method

.method public final a(Lffy;)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0}, Lfdi;->c()V

    .line 10
    return-void
.end method

.method public final a(Lgiq;)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 13
    const-string v0, "DomesticLteEmergencySwitchPolicy.performManualHandoff, handoff not allowed for emergency call over LTE."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 16
    .line 17
    iget-object v0, p0, Lfdi;->a:Landroid/content/Context;

    invoke-static {v0}, Lfft;->a(Landroid/content/Context;)Lffy;

    move-result-object v0

    iget-boolean v0, v0, Lffy;->a:Z

    if-eqz v0, :cond_1

    .line 18
    iget-object v0, p0, Lfdi;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 20
    const v0, 0x7f0200eb

    .line 24
    :goto_0
    new-instance v3, Landroid/telecom/StatusHints;

    iget-object v4, p0, Lfdi;->a:Landroid/content/Context;

    .line 25
    invoke-static {v4, v0}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    .line 26
    iget-object v4, p0, Lfdi;->b:Lfdd;

    invoke-virtual {v4}, Lfdd;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/telecom/StatusHints;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 28
    if-nez v0, :cond_2

    const-string v0, "0"

    :goto_1
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x43

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "DomesticLteEmergencySwitchPolicy.updateStatusHints, label: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", icon: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    .line 29
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lfdi;->b:Lfdd;

    invoke-virtual {v0, v3}, Lfdd;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 31
    :cond_0
    return-void

    .line 21
    :cond_1
    const v0, 0x7f0200ea

    .line 22
    iget-object v2, p0, Lfdi;->a:Landroid/content/Context;

    const v3, 0x7f1101e4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lfdi;->a:Landroid/content/Context;

    .line 23
    invoke-static {v5}, Lfmd;->G(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 28
    :cond_2
    iget-object v4, p0, Lfdi;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lfdi;->c:Lfft;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lfdi;->c:Lfft;

    invoke-virtual {v0}, Lfft;->a()V

    .line 35
    iput-object v1, p0, Lfdi;->c:Lfft;

    .line 36
    :cond_0
    iget-object v0, p0, Lfdi;->d:Lfeg;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lfdi;->d:Lfeg;

    invoke-virtual {v0}, Lfeg;->a()V

    .line 38
    iput-object v1, p0, Lfdi;->d:Lfeg;

    .line 39
    :cond_1
    return-void
.end method
