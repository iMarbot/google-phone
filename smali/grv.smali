.class public final Lgrv;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Lgsw;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgrv;->a:Ljava/lang/String;

    .line 4
    iput-object v0, p0, Lgrv;->b:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lgrv;->c:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lgrv;->d:Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lgrv;->e:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lgrv;->h:Lgsw;

    .line 9
    iput-object v0, p0, Lgrv;->i:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lgrv;->f:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgrv;->j:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgrv;->k:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lgrv;->g:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lgrv;->l:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lgrv;->m:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lgrv;->n:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lgrv;->o:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lgrv;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgrv;->cachedSize:I

    .line 20
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 54
    iget-object v1, p0, Lgrv;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    const/4 v1, 0x1

    iget-object v2, p0, Lgrv;->a:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_0
    iget-object v1, p0, Lgrv;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 58
    const/4 v1, 0x2

    iget-object v2, p0, Lgrv;->b:Ljava/lang/String;

    .line 59
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_1
    iget-object v1, p0, Lgrv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 61
    const/4 v1, 0x3

    iget-object v2, p0, Lgrv;->c:Ljava/lang/String;

    .line 62
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_2
    iget-object v1, p0, Lgrv;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Lgrv;->d:Ljava/lang/String;

    .line 65
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Lgrv;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lgrv;->e:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_4
    iget-object v1, p0, Lgrv;->h:Lgsw;

    if-eqz v1, :cond_5

    .line 70
    const/4 v1, 0x6

    iget-object v2, p0, Lgrv;->h:Lgsw;

    .line 71
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_5
    iget-object v1, p0, Lgrv;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 73
    const/4 v1, 0x7

    iget-object v2, p0, Lgrv;->i:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_6
    iget-object v1, p0, Lgrv;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 76
    const/16 v1, 0x8

    iget-object v2, p0, Lgrv;->f:Ljava/lang/String;

    .line 77
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_7
    iget-object v1, p0, Lgrv;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 79
    const/16 v1, 0x9

    iget-object v2, p0, Lgrv;->j:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_8
    iget-object v1, p0, Lgrv;->g:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 82
    const/16 v1, 0xa

    iget-object v2, p0, Lgrv;->g:Ljava/lang/String;

    .line 83
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_9
    iget-object v1, p0, Lgrv;->l:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 85
    const/16 v1, 0xb

    iget-object v2, p0, Lgrv;->l:Ljava/lang/String;

    .line 86
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_a
    iget-object v1, p0, Lgrv;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 88
    const/16 v1, 0xc

    iget-object v2, p0, Lgrv;->k:Ljava/lang/String;

    .line 89
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_b
    iget-object v1, p0, Lgrv;->m:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 91
    const/16 v1, 0xd

    iget-object v2, p0, Lgrv;->m:Ljava/lang/String;

    .line 92
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_c
    iget-object v1, p0, Lgrv;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 94
    const/16 v1, 0xe

    iget-object v2, p0, Lgrv;->n:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_d
    iget-object v1, p0, Lgrv;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 97
    const/16 v1, 0xf

    iget-object v2, p0, Lgrv;->o:Ljava/lang/String;

    .line 98
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 100
    .line 101
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 102
    sparse-switch v0, :sswitch_data_0

    .line 104
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    :sswitch_0
    return-object p0

    .line 106
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->a:Ljava/lang/String;

    goto :goto_0

    .line 108
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->b:Ljava/lang/String;

    goto :goto_0

    .line 110
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->c:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->d:Ljava/lang/String;

    goto :goto_0

    .line 114
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->e:Ljava/lang/String;

    goto :goto_0

    .line 116
    :sswitch_6
    iget-object v0, p0, Lgrv;->h:Lgsw;

    if-nez v0, :cond_1

    .line 117
    new-instance v0, Lgsw;

    invoke-direct {v0}, Lgsw;-><init>()V

    iput-object v0, p0, Lgrv;->h:Lgsw;

    .line 118
    :cond_1
    iget-object v0, p0, Lgrv;->h:Lgsw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 120
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->i:Ljava/lang/String;

    goto :goto_0

    .line 122
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->f:Ljava/lang/String;

    goto :goto_0

    .line 124
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->j:Ljava/lang/String;

    goto :goto_0

    .line 126
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->g:Ljava/lang/String;

    goto :goto_0

    .line 128
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->l:Ljava/lang/String;

    goto :goto_0

    .line 130
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->k:Ljava/lang/String;

    goto :goto_0

    .line 132
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->m:Ljava/lang/String;

    goto :goto_0

    .line 134
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->n:Ljava/lang/String;

    goto :goto_0

    .line 136
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrv;->o:Ljava/lang/String;

    goto :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lgrv;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgrv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_0
    iget-object v0, p0, Lgrv;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Lgrv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_1
    iget-object v0, p0, Lgrv;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 26
    const/4 v0, 0x3

    iget-object v1, p0, Lgrv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_2
    iget-object v0, p0, Lgrv;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 28
    const/4 v0, 0x4

    iget-object v1, p0, Lgrv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 29
    :cond_3
    iget-object v0, p0, Lgrv;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 30
    const/4 v0, 0x5

    iget-object v1, p0, Lgrv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_4
    iget-object v0, p0, Lgrv;->h:Lgsw;

    if-eqz v0, :cond_5

    .line 32
    const/4 v0, 0x6

    iget-object v1, p0, Lgrv;->h:Lgsw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_5
    iget-object v0, p0, Lgrv;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgrv;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 35
    :cond_6
    iget-object v0, p0, Lgrv;->f:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 36
    const/16 v0, 0x8

    iget-object v1, p0, Lgrv;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 37
    :cond_7
    iget-object v0, p0, Lgrv;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 38
    const/16 v0, 0x9

    iget-object v1, p0, Lgrv;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 39
    :cond_8
    iget-object v0, p0, Lgrv;->g:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 40
    const/16 v0, 0xa

    iget-object v1, p0, Lgrv;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 41
    :cond_9
    iget-object v0, p0, Lgrv;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 42
    const/16 v0, 0xb

    iget-object v1, p0, Lgrv;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 43
    :cond_a
    iget-object v0, p0, Lgrv;->k:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 44
    const/16 v0, 0xc

    iget-object v1, p0, Lgrv;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 45
    :cond_b
    iget-object v0, p0, Lgrv;->m:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 46
    const/16 v0, 0xd

    iget-object v1, p0, Lgrv;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 47
    :cond_c
    iget-object v0, p0, Lgrv;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 48
    const/16 v0, 0xe

    iget-object v1, p0, Lgrv;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 49
    :cond_d
    iget-object v0, p0, Lgrv;->o:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 50
    const/16 v0, 0xf

    iget-object v1, p0, Lgrv;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 51
    :cond_e
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 52
    return-void
.end method
