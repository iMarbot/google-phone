.class public Lasf;
.super Larh;
.source "PG"


# instance fields
.field public A:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1}, Larh;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lasf;->b(IZ)Z

    .line 3
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lasf;->b(IZ)Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10
    .line 11
    invoke-virtual {p0}, Lasf;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 13
    iget-object v3, p0, Lahd;->l:Ljava/lang/String;

    .line 15
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    move v2, v1

    .line 16
    :goto_0
    if-ge v2, v4, :cond_2

    .line 17
    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_1

    move v2, v0

    .line 21
    :goto_1
    if-eqz v2, :cond_3

    .line 22
    :goto_2
    invoke-static {p1}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lasf;->A:Z

    .line 23
    invoke-virtual {p0, v0}, Lasf;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {p0}, Lasf;->notifyDataSetChanged()V

    .line 25
    :cond_0
    invoke-super {p0, p1}, Larh;->a(Ljava/lang/String;)V

    .line 26
    return-void

    .line 19
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 20
    goto :goto_1

    :cond_3
    move v0, v1

    .line 21
    goto :goto_2
.end method

.method protected b(Z)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lasf;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {p0, v1, v0}, Lasf;->b(IZ)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 28
    const/4 v3, 0x3

    invoke-virtual {p0, v3, p1}, Lasf;->b(IZ)Z

    move-result v3

    or-int/2addr v0, v3

    .line 29
    const/4 v3, 0x4

    if-eqz p1, :cond_2

    .line 31
    iget-object v4, p0, Lafx;->a:Landroid/content/Context;

    .line 32
    invoke-static {v4}, Lbib;->ah(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 33
    :goto_1
    invoke-virtual {p0, v3, v2}, Lasf;->b(IZ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 34
    return v0

    :cond_1
    move v0, v1

    .line 27
    goto :goto_0

    :cond_2
    move v2, v1

    .line 32
    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    iget-boolean v0, p0, Lasf;->A:Z

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lahd;->l:Ljava/lang/String;

    .line 9
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Larh;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
