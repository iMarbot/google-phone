.class public final Lces;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/view/View;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/ViewAnimator;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/Chronometer;

.field private o:Landroid/widget/Space;

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:Lcgq;

.field private x:Lcgp;

.field private y:Lbkg;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/ImageView;IZ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lces;->s:Z

    .line 3
    invoke-static {}, Lcgq;->a()Lcgq;

    move-result-object v0

    iput-object v0, p0, Lces;->w:Lcgq;

    .line 5
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(ILjava/lang/String;)Lcgp;

    move-result-object v0

    .line 6
    iput-object v0, p0, Lces;->x:Lcgp;

    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lces;->b:Landroid/content/Context;

    .line 8
    iget-object v0, p0, Lces;->b:Landroid/content/Context;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iput-object p2, p0, Lces;->f:Landroid/widget/ImageView;

    .line 10
    iput p3, p0, Lces;->p:I

    .line 11
    iput-boolean p4, p0, Lces;->r:Z

    .line 12
    const v0, 0x7f0e0017

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lces;->c:Landroid/widget/ImageView;

    .line 13
    const v0, 0x7f0e001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lces;->d:Landroid/widget/TextView;

    .line 14
    const v0, 0x7f0e0018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lces;->e:Landroid/widget/TextView;

    .line 15
    const v0, 0x7f0e0021

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lces;->g:Landroid/widget/ImageView;

    .line 16
    const v0, 0x7f0e001b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    .line 17
    const v0, 0x7f0e001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lces;->i:Landroid/widget/ImageView;

    .line 18
    const v0, 0x7f0e01ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lces;->j:Landroid/widget/TextView;

    .line 19
    const v0, 0x7f0e001d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lces;->k:Landroid/widget/ImageView;

    .line 20
    const v0, 0x7f0e01ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewAnimator;

    iput-object v0, p0, Lces;->l:Landroid/widget/ViewAnimator;

    .line 21
    const v0, 0x7f0e0015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lces;->m:Landroid/widget/TextView;

    .line 22
    const v0, 0x7f0e0016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Chronometer;

    iput-object v0, p0, Lces;->n:Landroid/widget/Chronometer;

    .line 23
    const v0, 0x7f0e0020

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Space;

    iput-object v0, p0, Lces;->o:Landroid/widget/Space;

    .line 24
    iget-object v0, p0, Lces;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lces;->a:Landroid/view/View;

    .line 25
    new-instance v0, Lbkg;

    iget-object v1, p0, Lces;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lces;->y:Lbkg;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lces;->t:Z

    .line 27
    const v0, 0x7f0e0019

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lces;->u:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f0e001c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lces;->v:Landroid/view/View;

    .line 29
    return-void
.end method

.method private static a(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 86
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 87
    invoke-virtual {p1, p0}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 88
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 89
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    return-void
.end method

.method private final b()Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 91
    iget-object v2, p0, Lces;->f:Landroid/widget/ImageView;

    if-nez v2, :cond_0

    .line 101
    :goto_0
    return v0

    .line 93
    :cond_0
    iget-boolean v2, p0, Lces;->s:Z

    if-nez v2, :cond_1

    .line 94
    iget-object v1, p0, Lces;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 96
    :cond_1
    iget-object v2, p0, Lces;->w:Lcgq;

    iget-object v2, v2, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lces;->w:Lcgq;

    iget v2, v2, Lcgq;->g:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v1

    .line 97
    :goto_1
    if-nez v2, :cond_3

    iget-boolean v2, p0, Lces;->r:Z

    if-nez v2, :cond_3

    .line 98
    iget-object v1, p0, Lces;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 96
    goto :goto_1

    .line 100
    :cond_3
    iget-object v2, p0, Lces;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    move v0, v1

    .line 101
    goto :goto_0
.end method

.method private final c()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-object v0, v0, Lcgq;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lces;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    :goto_0
    iget-object v0, p0, Lces;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 114
    iget-boolean v0, p0, Lces;->q:Z

    if-eqz v0, :cond_3

    .line 115
    iget-object v0, p0, Lces;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    :cond_0
    :goto_1
    return-void

    .line 104
    :cond_1
    iget-object v3, p0, Lces;->e:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-boolean v0, v0, Lcgq;->c:Z

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-object v0, v0, Lcgq;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 108
    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-boolean v0, v0, Lcgq;->c:Z

    if-eqz v0, :cond_6

    .line 111
    const/4 v0, 0x3

    .line 112
    :goto_3
    iget-object v3, p0, Lces;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-object v0, v0, Lcgq;->b:Ljava/lang/String;

    goto :goto_2

    .line 116
    :cond_3
    iget v0, p0, Lces;->p:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lces;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-object v0, v0, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lces;->w:Lcgq;

    iget v0, v0, Lcgq;->g:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v1, v2

    .line 118
    :cond_4
    if-eqz v1, :cond_5

    .line 119
    iget-object v0, p0, Lces;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lces;->b:Landroid/content/Context;

    iget-object v2, p0, Lces;->w:Lcgq;

    iget-object v2, v2, Lcgq;->f:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lces;->p:I

    iget v4, p0, Lces;->p:I

    .line 120
    invoke-static {v1, v2, v3, v4}, Lbib;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 121
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 122
    :cond_5
    iget-object v0, p0, Lces;->y:Lbkg;

    iget-object v1, p0, Lces;->w:Lcgq;

    iget-object v1, v1, Lcgq;->b:Ljava/lang/String;

    iget-object v3, p0, Lces;->w:Lcgq;

    iget-object v3, v3, Lcgq;->l:Ljava/lang/String;

    iget-object v4, p0, Lces;->x:Lcgp;

    iget-boolean v4, v4, Lcgp;->q:Z

    iget-object v5, p0, Lces;->w:Lcgq;

    iget-boolean v5, v5, Lcgq;->h:Z

    iget-object v6, p0, Lces;->x:Lcgp;

    iget-boolean v6, v6, Lcgp;->s:Z

    iget-object v7, p0, Lces;->w:Lcgq;

    iget v7, v7, Lcgq;->o:I

    iget-object v8, p0, Lces;->x:Lcgp;

    iget-boolean v8, v8, Lcgp;->k:Z

    .line 123
    invoke-static {v4, v5, v6, v7, v8}, Lbkg;->a(ZZZIZ)I

    move-result v4

    .line 124
    invoke-virtual {v0, v1, v3, v2, v4}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    .line 125
    iget-object v0, p0, Lces;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 126
    iget-object v0, p0, Lces;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lces;->y:Lbkg;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_3
.end method

.method private final d()V
    .locals 9

    .prologue
    const v6, 0x7f02005a

    const/4 v5, 0x2

    const/16 v3, 0x8

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lces;->b:Landroid/content/Context;

    iget-object v1, p0, Lces;->x:Lcgp;

    iget-object v4, p0, Lces;->w:Lcgq;

    invoke-static {v0, v1, v4}, Lbvs;->a(Landroid/content/Context;Lcgp;Lcgq;)Lcer;

    move-result-object v4

    .line 129
    iget-object v0, p0, Lces;->m:Landroid/widget/TextView;

    iget-object v1, v4, Lcer;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lces;->m:Landroid/widget/TextView;

    iget-boolean v1, v4, Lcer;->g:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 131
    iget-object v1, p0, Lces;->g:Landroid/widget/ImageView;

    iget-boolean v0, v4, Lcer;->c:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 133
    iget-boolean v0, v4, Lcer;->d:Z

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 135
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 137
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 138
    instance-of v0, v1, Landroid/graphics/drawable/Animatable;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    check-cast v1, Landroid/graphics/drawable/Animatable;

    invoke-interface {v1}, Landroid/graphics/drawable/Animatable;->start()V

    .line 148
    :cond_0
    :goto_1
    iget-object v1, p0, Lces;->k:Landroid/widget/ImageView;

    iget-boolean v0, v4, Lcer;->g:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    iget-boolean v0, v4, Lcer;->f:Z

    if-eqz v0, :cond_9

    .line 150
    iget-object v0, p0, Lces;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lces;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-boolean v0, v4, Lcer;->b:Z

    if-eqz v0, :cond_8

    .line 153
    iget-object v0, p0, Lces;->l:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v2}, Landroid/widget/ViewAnimator;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lces;->a:Landroid/view/View;

    .line 155
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->j(Landroid/view/View;)I

    move-result v0

    .line 156
    if-nez v0, :cond_7

    .line 157
    iget-object v0, p0, Lces;->j:Landroid/widget/TextView;

    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v3, v4, Lcer;->a:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const-string v3, " \u2022 "

    aput-object v3, v1, v8

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :goto_3
    iget-boolean v0, v4, Lcer;->b:Z

    if-eqz v0, :cond_a

    .line 164
    iget-object v0, p0, Lces;->l:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v8}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 165
    iget-object v0, p0, Lces;->n:Landroid/widget/Chronometer;

    iget-object v1, p0, Lces;->x:Lcgp;

    iget-wide v4, v1, Lcgp;->p:J

    .line 166
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 167
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 168
    invoke-virtual {v0, v4, v5}, Landroid/widget/Chronometer;->setBase(J)V

    .line 169
    iget-boolean v0, p0, Lces;->t:Z

    if-nez v0, :cond_1

    .line 170
    const-string v0, "ContactGridManager.updateBottomRow"

    const-string v1, "starting timer with base: %d"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lces;->n:Landroid/widget/Chronometer;

    .line 171
    invoke-virtual {v4}, Landroid/widget/Chronometer;->getBase()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    .line 172
    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Lces;->n:Landroid/widget/Chronometer;

    invoke-virtual {v0}, Landroid/widget/Chronometer;->start()V

    .line 174
    iput-boolean v8, p0, Lces;->t:Z

    .line 178
    :cond_1
    :goto_4
    return-void

    :cond_2
    move v0, v3

    .line 131
    goto/16 :goto_0

    .line 140
    :cond_3
    iget-boolean v0, v4, Lcer;->e:Z

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 142
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setActivated(Z)V

    goto/16 :goto_1

    .line 144
    :cond_4
    iget-boolean v0, v4, Lcer;->e:Z

    if-eqz v0, :cond_5

    .line 145
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setActivated(Z)V

    goto/16 :goto_1

    .line 146
    :cond_5
    iget-boolean v0, v4, Lcer;->d:Z

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lces;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_6
    move v0, v3

    .line 148
    goto/16 :goto_2

    .line 158
    :cond_7
    iget-object v0, p0, Lces;->j:Landroid/widget/TextView;

    new-array v1, v5, [Ljava/lang/CharSequence;

    const-string v3, " \u2022 "

    aput-object v3, v1, v2

    iget-object v3, v4, Lcer;->a:Ljava/lang/CharSequence;

    aput-object v3, v1, v8

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 159
    :cond_8
    iget-object v0, p0, Lces;->l:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v3}, Landroid/widget/ViewAnimator;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lces;->j:Landroid/widget/TextView;

    iget-object v1, v4, Lcer;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 161
    :cond_9
    iget-object v0, p0, Lces;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lces;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 175
    :cond_a
    iget-object v0, p0, Lces;->l:Landroid/widget/ViewAnimator;

    invoke-virtual {v0, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 176
    iget-object v0, p0, Lces;->n:Landroid/widget/Chronometer;

    invoke-virtual {v0}, Landroid/widget/Chronometer;->stop()V

    .line 177
    iput-boolean v2, p0, Lces;->t:Z

    goto :goto_4
.end method

.method private final e()V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v5, 0x0

    .line 179
    iget-object v0, p0, Lces;->u:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-boolean v0, p0, Lces;->z:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lces;->x:Lcgp;

    iget-object v0, v0, Lcgp;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    :cond_2
    iget-object v0, p0, Lces;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lces;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 185
    :cond_3
    iget-object v0, p0, Lces;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lces;->b:Landroid/content/Context;

    const v2, 0x7f1100ed

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lces;->x:Lcgp;

    iget-object v4, v4, Lcgp;->i:Ljava/lang/String;

    aput-object v4, v3, v5

    .line 186
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lces;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lces;->w:Lcgq;

    iget-boolean v0, v0, Lcgq;->k:Z

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lces;->v:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lces;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lces;->d:Landroid/widget/TextView;

    invoke-static {p1, v0}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    .line 70
    iget-object v0, p0, Lces;->e:Landroid/widget/TextView;

    invoke-static {p1, v0}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lces;->b:Landroid/content/Context;

    iget-object v1, p0, Lces;->x:Lcgp;

    iget-object v2, p0, Lces;->w:Lcgq;

    invoke-static {v0, v1, v2}, Lbvs;->a(Landroid/content/Context;Lcgp;Lcgq;)Lcer;

    move-result-object v0

    .line 72
    iget-boolean v0, v0, Lcer;->h:Z

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lces;->m:Landroid/widget/TextView;

    invoke-static {p1, v0}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;Landroid/view/View;)V

    .line 74
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/ImageView;IZ)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lces;->f:Landroid/widget/ImageView;

    .line 76
    iput p2, p0, Lces;->p:I

    .line 77
    iput-boolean p3, p0, Lces;->r:Z

    .line 78
    invoke-direct {p0}, Lces;->c()V

    .line 79
    return-void
.end method

.method public final a(Lcgp;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 47
    iput-object p1, p0, Lces;->x:Lcgp;

    .line 48
    invoke-direct {p0}, Lces;->c()V

    .line 49
    invoke-direct {p0}, Lces;->d()V

    .line 51
    iget-object v0, p0, Lces;->b:Landroid/content/Context;

    iget-object v1, p0, Lces;->x:Lcgp;

    iget-object v2, p0, Lces;->w:Lcgq;

    invoke-static {v0, v1, v2}, Lbvw;->a(Landroid/content/Context;Lcgp;Lcgq;)Lcet;

    move-result-object v0

    .line 52
    iget-object v1, v0, Lcet;->a:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    iget-object v1, p0, Lces;->d:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    iget-object v1, p0, Lces;->d:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :goto_0
    iget-object v1, v0, Lcet;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 59
    iget-object v0, p0, Lces;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    :cond_0
    iget-object v0, p0, Lces;->o:Landroid/widget/Space;

    invoke-virtual {v0, v4}, Landroid/widget/Space;->setVisibility(I)V

    .line 67
    :goto_1
    invoke-direct {p0}, Lces;->e()V

    .line 68
    return-void

    .line 55
    :cond_1
    iget-object v1, p0, Lces;->d:Landroid/widget/TextView;

    iget-object v2, v0, Lcet;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p0, Lces;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 57
    iget-object v1, p0, Lces;->d:Landroid/widget/TextView;

    iget-boolean v2, v0, Lcet;->c:Z

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_0

    .line 61
    :cond_2
    iget-object v1, p0, Lces;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 62
    iget-object v1, p0, Lces;->c:Landroid/widget/ImageView;

    iget-object v0, v0, Lcet;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    iget-object v0, p0, Lces;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lces;->d:Landroid/widget/TextView;

    .line 64
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lces;->o:Landroid/widget/Space;

    invoke-virtual {v0, v3}, Landroid/widget/Space;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Lcgq;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lces;->w:Lcgq;

    .line 43
    invoke-direct {p0}, Lces;->c()V

    .line 44
    invoke-direct {p0}, Lces;->d()V

    .line 45
    invoke-direct {p0}, Lces;->e()V

    .line 46
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    iget-boolean v0, p0, Lces;->q:Z

    if-eq v1, v0, :cond_0

    .line 33
    iput-boolean v1, p0, Lces;->q:Z

    .line 34
    invoke-direct {p0}, Lces;->c()V

    .line 35
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 36
    iget-boolean v0, p0, Lces;->s:Z

    if-ne v0, p1, :cond_0

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    iput-boolean p1, p0, Lces;->s:Z

    .line 39
    iget-object v1, p0, Lces;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 40
    invoke-direct {p0}, Lces;->b()Z

    goto :goto_0

    .line 39
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lces;->z:Z

    if-ne v0, p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iput-boolean p1, p0, Lces;->z:Z

    .line 83
    invoke-direct {p0}, Lces;->e()V

    goto :goto_0
.end method
