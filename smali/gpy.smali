.class public final Lgpy;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpy;


# instance fields
.field public deleted:[Lgny;

.field public endCause:Ljava/lang/Integer;

.field public modified:[Lgnm;

.field public removalReason:Ljava/lang/Integer;

.field public resyncCollection:Lgny;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpy;->clear()Lgpy;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpy;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpy;->_emptyArray:[Lgpy;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpy;->_emptyArray:[Lgpy;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpy;

    sput-object v0, Lgpy;->_emptyArray:[Lgpy;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpy;->_emptyArray:[Lgpy;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpy;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lgpy;

    invoke-direct {v0}, Lgpy;-><init>()V

    invoke-virtual {v0, p0}, Lgpy;->mergeFrom(Lhfp;)Lgpy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpy;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lgpy;

    invoke-direct {v0}, Lgpy;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpy;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpy;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgpy;->syncMetadata:Lgoa;

    .line 11
    invoke-static {}, Lgnm;->emptyArray()[Lgnm;

    move-result-object v0

    iput-object v0, p0, Lgpy;->modified:[Lgnm;

    .line 12
    invoke-static {}, Lgny;->emptyArray()[Lgny;

    move-result-object v0

    iput-object v0, p0, Lgpy;->deleted:[Lgny;

    .line 13
    iput-object v1, p0, Lgpy;->endCause:Ljava/lang/Integer;

    .line 14
    iput-object v1, p0, Lgpy;->resyncCollection:Lgny;

    .line 15
    iput-object v1, p0, Lgpy;->removalReason:Ljava/lang/Integer;

    .line 16
    iput-object v1, p0, Lgpy;->unknownFieldData:Lhfv;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lgpy;->cachedSize:I

    .line 18
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 42
    iget-object v2, p0, Lgpy;->syncMetadata:Lgoa;

    if-eqz v2, :cond_0

    .line 43
    const/4 v2, 0x1

    iget-object v3, p0, Lgpy;->syncMetadata:Lgoa;

    .line 44
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 45
    :cond_0
    iget-object v2, p0, Lgpy;->modified:[Lgnm;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgpy;->modified:[Lgnm;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 46
    :goto_0
    iget-object v3, p0, Lgpy;->modified:[Lgnm;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 47
    iget-object v3, p0, Lgpy;->modified:[Lgnm;

    aget-object v3, v3, v0

    .line 48
    if-eqz v3, :cond_1

    .line 49
    const/4 v4, 0x2

    .line 50
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 51
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 52
    :cond_3
    iget-object v2, p0, Lgpy;->deleted:[Lgny;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgpy;->deleted:[Lgny;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 53
    :goto_1
    iget-object v2, p0, Lgpy;->deleted:[Lgny;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 54
    iget-object v2, p0, Lgpy;->deleted:[Lgny;

    aget-object v2, v2, v1

    .line 55
    if-eqz v2, :cond_4

    .line 56
    const/4 v3, 0x3

    .line 57
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 58
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 59
    :cond_5
    iget-object v1, p0, Lgpy;->endCause:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 60
    const/4 v1, 0x4

    iget-object v2, p0, Lgpy;->endCause:Ljava/lang/Integer;

    .line 61
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_6
    iget-object v1, p0, Lgpy;->resyncCollection:Lgny;

    if-eqz v1, :cond_7

    .line 63
    const/4 v1, 0x5

    iget-object v2, p0, Lgpy;->resyncCollection:Lgny;

    .line 64
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_7
    iget-object v1, p0, Lgpy;->removalReason:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 66
    const/4 v1, 0x6

    iget-object v2, p0, Lgpy;->removalReason:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_8
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 70
    sparse-switch v0, :sswitch_data_0

    .line 72
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    iget-object v0, p0, Lgpy;->syncMetadata:Lgoa;

    if-nez v0, :cond_1

    .line 75
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgpy;->syncMetadata:Lgoa;

    .line 76
    :cond_1
    iget-object v0, p0, Lgpy;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 78
    :sswitch_2
    const/16 v0, 0x12

    .line 79
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 80
    iget-object v0, p0, Lgpy;->modified:[Lgnm;

    if-nez v0, :cond_3

    move v0, v1

    .line 81
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnm;

    .line 82
    if-eqz v0, :cond_2

    .line 83
    iget-object v3, p0, Lgpy;->modified:[Lgnm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 84
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 85
    new-instance v3, Lgnm;

    invoke-direct {v3}, Lgnm;-><init>()V

    aput-object v3, v2, v0

    .line 86
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 87
    invoke-virtual {p1}, Lhfp;->a()I

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 80
    :cond_3
    iget-object v0, p0, Lgpy;->modified:[Lgnm;

    array-length v0, v0

    goto :goto_1

    .line 89
    :cond_4
    new-instance v3, Lgnm;

    invoke-direct {v3}, Lgnm;-><init>()V

    aput-object v3, v2, v0

    .line 90
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 91
    iput-object v2, p0, Lgpy;->modified:[Lgnm;

    goto :goto_0

    .line 93
    :sswitch_3
    const/16 v0, 0x1a

    .line 94
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 95
    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    if-nez v0, :cond_6

    move v0, v1

    .line 96
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgny;

    .line 97
    if-eqz v0, :cond_5

    .line 98
    iget-object v3, p0, Lgpy;->deleted:[Lgny;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    .line 100
    new-instance v3, Lgny;

    invoke-direct {v3}, Lgny;-><init>()V

    aput-object v3, v2, v0

    .line 101
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 102
    invoke-virtual {p1}, Lhfp;->a()I

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 95
    :cond_6
    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    array-length v0, v0

    goto :goto_3

    .line 104
    :cond_7
    new-instance v3, Lgny;

    invoke-direct {v3}, Lgny;-><init>()V

    aput-object v3, v2, v0

    .line 105
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 106
    iput-object v2, p0, Lgpy;->deleted:[Lgny;

    goto/16 :goto_0

    .line 108
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 110
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 111
    invoke-static {v3}, Lgne;->checkEndCauseOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgpy;->endCause:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 114
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 115
    invoke-virtual {p0, p1, v0}, Lgpy;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 117
    :sswitch_5
    iget-object v0, p0, Lgpy;->resyncCollection:Lgny;

    if-nez v0, :cond_8

    .line 118
    new-instance v0, Lgny;

    invoke-direct {v0}, Lgny;-><init>()V

    iput-object v0, p0, Lgpy;->resyncCollection:Lgny;

    .line 119
    :cond_8
    iget-object v0, p0, Lgpy;->resyncCollection:Lgny;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 121
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 123
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 124
    invoke-static {v3}, Lgnf;->checkRemovalReasonOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgpy;->removalReason:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 127
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 128
    invoke-virtual {p0, p1, v0}, Lgpy;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 70
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lgpy;->mergeFrom(Lhfp;)Lgpy;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19
    iget-object v0, p0, Lgpy;->syncMetadata:Lgoa;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v2, p0, Lgpy;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_0
    iget-object v0, p0, Lgpy;->modified:[Lgnm;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpy;->modified:[Lgnm;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 22
    :goto_0
    iget-object v2, p0, Lgpy;->modified:[Lgnm;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 23
    iget-object v2, p0, Lgpy;->modified:[Lgnm;

    aget-object v2, v2, v0

    .line 24
    if-eqz v2, :cond_1

    .line 25
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_2
    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 28
    :goto_1
    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 29
    iget-object v0, p0, Lgpy;->deleted:[Lgny;

    aget-object v0, v0, v1

    .line 30
    if-eqz v0, :cond_3

    .line 31
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 33
    :cond_4
    iget-object v0, p0, Lgpy;->endCause:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 34
    const/4 v0, 0x4

    iget-object v1, p0, Lgpy;->endCause:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 35
    :cond_5
    iget-object v0, p0, Lgpy;->resyncCollection:Lgny;

    if-eqz v0, :cond_6

    .line 36
    const/4 v0, 0x5

    iget-object v1, p0, Lgpy;->resyncCollection:Lgny;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_6
    iget-object v0, p0, Lgpy;->removalReason:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 38
    const/4 v0, 0x6

    iget-object v1, p0, Lgpy;->removalReason:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 39
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 40
    return-void
.end method
