.class public final enum Lblf$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lblf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lblf$a;

.field public static final enum b:Lblf$a;

.field public static final enum c:Lblf$a;

.field public static final enum d:Lblf$a;

.field public static final e:Lhby;

.field private static synthetic g:[Lblf$a;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lblf$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, Lblf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblf$a;->a:Lblf$a;

    .line 13
    new-instance v0, Lblf$a;

    const-string v1, "IMS_VIDEO_TECH"

    invoke-direct {v0, v1, v3, v3}, Lblf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblf$a;->b:Lblf$a;

    .line 14
    new-instance v0, Lblf$a;

    const-string v1, "LIGHTBRINGER_VIDEO_TECH"

    invoke-direct {v0, v1, v4, v4}, Lblf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblf$a;->c:Lblf$a;

    .line 15
    new-instance v0, Lblf$a;

    const-string v1, "RCS_VIDEO_SHARE"

    invoke-direct {v0, v1, v5, v5}, Lblf$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lblf$a;->d:Lblf$a;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lblf$a;

    sget-object v1, Lblf$a;->a:Lblf$a;

    aput-object v1, v0, v2

    sget-object v1, Lblf$a;->b:Lblf$a;

    aput-object v1, v0, v3

    sget-object v1, Lblf$a;->c:Lblf$a;

    aput-object v1, v0, v4

    sget-object v1, Lblf$a;->d:Lblf$a;

    aput-object v1, v0, v5

    sput-object v0, Lblf$a;->g:[Lblf$a;

    .line 17
    new-instance v0, Lblg;

    invoke-direct {v0}, Lblg;-><init>()V

    sput-object v0, Lblf$a;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput p3, p0, Lblf$a;->f:I

    .line 11
    return-void
.end method

.method public static a(I)Lblf$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 8
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lblf$a;->a:Lblf$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lblf$a;->b:Lblf$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lblf$a;->c:Lblf$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lblf$a;->d:Lblf$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static values()[Lblf$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lblf$a;->g:[Lblf$a;

    invoke-virtual {v0}, [Lblf$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lblf$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lblf$a;->f:I

    return v0
.end method
