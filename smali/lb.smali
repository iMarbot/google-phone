.class public Llb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/app/Notification$Builder;

.field public final b:Lle;

.field public c:Landroid/widget/RemoteViews;

.field public d:Landroid/widget/RemoteViews;

.field public final e:Ljava/util/List;

.field public final f:Landroid/os/Bundle;

.field public g:I

.field public h:Landroid/widget/RemoteViews;


# direct methods
.method constructor <init>(Lle;)V
    .locals 10

    .prologue
    const/16 v9, 0x15

    const/16 v8, 0x14

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llb;->e:Ljava/util/List;

    .line 4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Llb;->f:Landroid/os/Bundle;

    .line 5
    iput-object p1, p0, Llb;->b:Lle;

    .line 6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 7
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p1, Lle;->a:Landroid/content/Context;

    iget-object v4, p1, Lle;->o:Ljava/lang/String;

    invoke-direct {v0, v1, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    .line 9
    :goto_0
    iget-object v1, p1, Lle;->r:Landroid/app/Notification;

    .line 10
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-wide v4, v1, Landroid/app/Notification;->when:J

    invoke-virtual {v0, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v4, v1, Landroid/app/Notification;->icon:I

    iget v5, v1, Landroid/app/Notification;->iconLevel:I

    .line 11
    invoke-virtual {v0, v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(II)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 12
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 13
    invoke-virtual {v0, v4, v7}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    iget v5, v1, Landroid/app/Notification;->audioStreamType:I

    .line 14
    invoke-virtual {v0, v4, v5}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, v1, Landroid/app/Notification;->vibrate:[J

    .line 15
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setVibrate([J)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v4, v1, Landroid/app/Notification;->ledARGB:I

    iget v5, v1, Landroid/app/Notification;->ledOnMS:I

    iget v6, v1, Landroid/app/Notification;->ledOffMS:I

    .line 16
    invoke-virtual {v0, v4, v5, v6}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v4

    iget v0, v1, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    move v0, v2

    .line 17
    :goto_1
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    iget v0, v1, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    move v0, v2

    .line 18
    :goto_2
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    iget v0, v1, Landroid/app/Notification;->flags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    move v0, v2

    .line 19
    :goto_3
    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v4, v1, Landroid/app/Notification;->defaults:I

    .line 20
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, p1, Lle;->c:Ljava/lang/CharSequence;

    .line 21
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, p1, Lle;->d:Ljava/lang/CharSequence;

    .line 22
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, p1, Lle;->e:Landroid/app/PendingIntent;

    .line 24
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v4, v1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 25
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    iget v0, v1, Landroid/app/Notification;->flags:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    move v0, v2

    .line 26
    :goto_4
    invoke-virtual {v4, v7, v0}, Landroid/app/Notification$Builder;->setFullScreenIntent(Landroid/app/PendingIntent;Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lle;->f:Landroid/graphics/Bitmap;

    .line 27
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {v0, v3, v3, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 30
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_8

    .line 31
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setUsesChronometer(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v1, p1, Lle;->g:I

    .line 33
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 34
    iget-object v0, p1, Lle;->b:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_5
    if-ge v4, v5, :cond_5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v4, v4, 0x1

    check-cast v1, Llc;

    .line 35
    invoke-virtual {p0, v1}, Llb;->a(Llc;)V

    goto :goto_5

    .line 8
    :cond_0
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p1, Lle;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    goto/16 :goto_0

    :cond_1
    move v0, v3

    .line 16
    goto/16 :goto_1

    :cond_2
    move v0, v3

    .line 17
    goto/16 :goto_2

    :cond_3
    move v0, v3

    .line 18
    goto :goto_3

    :cond_4
    move v0, v3

    .line 25
    goto :goto_4

    .line 37
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v8, :cond_7

    .line 38
    iget-boolean v0, p1, Lle;->l:Z

    if-eqz v0, :cond_6

    .line 39
    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    const-string v1, "android.support.localOnly"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    :cond_6
    iget-object v0, p1, Lle;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 41
    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    const-string v1, "android.support.groupKey"

    iget-object v4, p1, Lle;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-boolean v0, p1, Lle;->k:Z

    if-eqz v0, :cond_b

    .line 43
    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    const-string v1, "android.support.isGroupSummary"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 45
    :cond_7
    :goto_6
    iput-object v7, p0, Llb;->c:Landroid/widget/RemoteViews;

    .line 46
    iput-object v7, p0, Llb;->d:Landroid/widget/RemoteViews;

    .line 47
    :cond_8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_9

    .line 48
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-boolean v1, p1, Lle;->h:Z

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v9, :cond_9

    .line 50
    iget-object v0, p1, Lle;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lle;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 51
    iget-object v1, p0, Llb;->f:Landroid/os/Bundle;

    const-string v2, "android.people"

    iget-object v0, p1, Lle;->s:Ljava/util/ArrayList;

    iget-object v4, p1, Lle;->s:Ljava/util/ArrayList;

    .line 52
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 53
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 54
    :cond_9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_a

    .line 55
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-boolean v1, p1, Lle;->l:Z

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLocalOnly(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-object v1, p1, Lle;->j:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget-boolean v1, p1, Lle;->k:Z

    .line 57
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroupSummary(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setSortKey(Ljava/lang/String;)Landroid/app/Notification$Builder;

    .line 59
    iget v0, p1, Lle;->q:I

    iput v0, p0, Llb;->g:I

    .line 60
    :cond_a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_d

    .line 61
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v1, p1, Lle;->m:I

    .line 62
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    .line 65
    iget-object v0, p1, Lle;->s:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    :goto_7
    if-ge v2, v4, :cond_c

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Ljava/lang/String;

    .line 66
    iget-object v5, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v5, v1}, Landroid/app/Notification$Builder;->addPerson(Ljava/lang/String;)Landroid/app/Notification$Builder;

    goto :goto_7

    .line 44
    :cond_b
    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    const-string v1, "android.support.useSideChannel"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_6

    .line 68
    :cond_c
    iput-object v7, p0, Llb;->h:Landroid/widget/RemoteViews;

    .line 69
    :cond_d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_e

    .line 70
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setRemoteInputHistory([Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 72
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_f

    .line 73
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setBadgeIconType(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setShortcutId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 75
    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setTimeoutAfter(J)Landroid/app/Notification$Builder;

    move-result-object v0

    iget v1, p1, Lle;->q:I

    .line 76
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setGroupAlertBehavior(I)Landroid/app/Notification$Builder;

    .line 77
    :cond_f
    return-void
.end method

.method static a(Landroid/app/Notification;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 162
    iput-object v0, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 163
    iput-object v0, p0, Landroid/app/Notification;->vibrate:[J

    .line 164
    iget v0, p0, Landroid/app/Notification;->defaults:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Landroid/app/Notification;->defaults:I

    .line 165
    iget v0, p0, Landroid/app/Notification;->defaults:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Landroid/app/Notification;->defaults:I

    .line 166
    return-void
.end method


# virtual methods
.method public a()Landroid/app/Notification$Builder;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    return-object v0
.end method

.method a(Llc;)V
    .locals 2

    .prologue
    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 86
    new-instance v0, Landroid/app/Notification$Action$Builder;

    .line 87
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 88
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 89
    iget-object v0, p0, Llb;->e:Ljava/util/List;

    iget-object v1, p0, Llb;->a:Landroid/app/Notification$Builder;

    .line 90
    invoke-static {v1, p1}, Llg;->a(Landroid/app/Notification$Builder;Llc;)Landroid/os/Bundle;

    move-result-object v1

    .line 91
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_1
    return-void
.end method

.method public b()Landroid/app/Notification;
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Llb;->b:Lle;

    iget-object v0, v0, Lle;->i:Llf;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0, p0}, Llf;->a(Llb;)V

    .line 81
    :cond_0
    invoke-virtual {p0}, Llb;->c()Landroid/app/Notification;

    move-result-object v1

    .line 82
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    if-eqz v0, :cond_1

    .line 83
    invoke-static {v1}, Lbw;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    .line 84
    :cond_1
    return-object v1
.end method

.method protected c()Landroid/app/Notification;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    .line 94
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 95
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_3

    .line 96
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 97
    iget v1, p0, Llb;->g:I

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_2

    iget v1, p0, Llb;->g:I

    if-ne v1, v3, :cond_2

    .line 99
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    .line 100
    :cond_2
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-nez v1, :cond_0

    iget v1, p0, Llb;->g:I

    if-ne v1, v2, :cond_0

    .line 101
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    goto :goto_0

    .line 103
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_8

    .line 104
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-object v1, p0, Llb;->f:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    .line 105
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 106
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_4

    .line 107
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 108
    :cond_4
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_5

    .line 109
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 110
    :cond_5
    iget-object v1, p0, Llb;->h:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_6

    .line 111
    iget-object v1, p0, Llb;->h:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->headsUpContentView:Landroid/widget/RemoteViews;

    .line 112
    :cond_6
    iget v1, p0, Llb;->g:I

    if-eqz v1, :cond_0

    .line 113
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_7

    iget v1, p0, Llb;->g:I

    if-ne v1, v3, :cond_7

    .line 114
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    .line 115
    :cond_7
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-nez v1, :cond_0

    iget v1, p0, Llb;->g:I

    if-ne v1, v2, :cond_0

    .line 116
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    goto/16 :goto_0

    .line 118
    :cond_8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_c

    .line 119
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-object v1, p0, Llb;->f:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    .line 120
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 121
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_9

    .line 122
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 123
    :cond_9
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_a

    .line 124
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 125
    :cond_a
    iget v1, p0, Llb;->g:I

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_b

    iget v1, p0, Llb;->g:I

    if-ne v1, v3, :cond_b

    .line 127
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    .line 128
    :cond_b
    invoke-virtual {v0}, Landroid/app/Notification;->getGroup()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/app/Notification;->flags:I

    and-int/lit16 v1, v1, 0x200

    if-nez v1, :cond_0

    iget v1, p0, Llb;->g:I

    if-ne v1, v2, :cond_0

    .line 129
    invoke-static {v0}, Llb;->a(Landroid/app/Notification;)V

    goto/16 :goto_0

    .line 131
    :cond_c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_f

    .line 132
    iget-object v0, p0, Llb;->e:Ljava/util/List;

    .line 133
    invoke-static {v0}, Llg;->a(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_d

    .line 135
    iget-object v1, p0, Llb;->f:Landroid/os/Bundle;

    const-string v2, "android.support.actionExtras"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 136
    :cond_d
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    iget-object v1, p0, Llb;->f:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    .line 137
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 138
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_e

    .line 139
    iget-object v1, p0, Llb;->c:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 140
    :cond_e
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Llb;->d:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    goto/16 :goto_0

    .line 143
    :cond_f
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_15

    .line 144
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 145
    invoke-static {v1}, Lbw;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v2

    .line 146
    new-instance v3, Landroid/os/Bundle;

    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    invoke-direct {v3, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Llb;->f:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 149
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_1

    .line 151
    :cond_11
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Llb;->e:Ljava/util/List;

    .line 153
    invoke-static {v0}, Llg;->a(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_12

    .line 155
    invoke-static {v1}, Lbw;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.support.actionExtras"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 156
    :cond_12
    iget-object v0, p0, Llb;->c:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_13

    .line 157
    iget-object v0, p0, Llb;->c:Landroid/widget/RemoteViews;

    iput-object v0, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 158
    :cond_13
    iget-object v0, p0, Llb;->d:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_14

    .line 159
    iget-object v0, p0, Llb;->d:Landroid/widget/RemoteViews;

    iput-object v0, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    :cond_14
    move-object v0, v1

    .line 160
    goto/16 :goto_0

    .line 161
    :cond_15
    iget-object v0, p0, Llb;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_0
.end method
