.class public final enum Lhiw;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static enum A:Lhiw;

.field private static enum B:Lhiw;

.field private static enum C:Lhiw;

.field private static enum D:Lhiw;

.field private static enum E:Lhiw;

.field private static enum F:Lhiw;

.field private static enum G:Lhiw;

.field private static enum H:Lhiw;

.field private static enum I:Lhiw;

.field private static enum J:Lhiw;

.field private static enum K:Lhiw;

.field private static enum L:Lhiw;

.field private static enum M:Lhiw;

.field private static enum N:Lhiw;

.field private static enum O:Lhiw;

.field private static enum P:Lhiw;

.field private static enum Q:Lhiw;

.field private static enum R:Lhiw;

.field private static enum S:Lhiw;

.field private static enum T:Lhiw;

.field private static enum U:Lhiw;

.field private static enum V:Lhiw;

.field private static enum W:Lhiw;

.field private static enum X:Lhiw;

.field private static enum Y:Lhiw;

.field private static enum Z:Lhiw;

.field public static final enum a:Lhiw;

.field private static enum aA:Lhiw;

.field private static enum aB:Lhiw;

.field private static enum aC:Lhiw;

.field private static enum aD:Lhiw;

.field private static enum aE:Lhiw;

.field private static enum aF:Lhiw;

.field private static enum aG:Lhiw;

.field private static enum aH:Lhiw;

.field private static enum aI:Lhiw;

.field private static enum aJ:Lhiw;

.field private static enum aK:Lhiw;

.field private static enum aL:Lhiw;

.field private static enum aM:Lhiw;

.field private static enum aN:Lhiw;

.field private static enum aO:Lhiw;

.field private static enum aP:Lhiw;

.field private static enum aQ:Lhiw;

.field private static enum aR:Lhiw;

.field private static enum aS:Lhiw;

.field private static synthetic aT:[Lhiw;

.field private static enum aa:Lhiw;

.field private static enum ab:Lhiw;

.field private static enum ac:Lhiw;

.field private static enum ad:Lhiw;

.field private static enum ae:Lhiw;

.field private static enum af:Lhiw;

.field private static enum ag:Lhiw;

.field private static enum ah:Lhiw;

.field private static enum ai:Lhiw;

.field private static enum aj:Lhiw;

.field private static enum ak:Lhiw;

.field private static enum al:Lhiw;

.field private static enum am:Lhiw;

.field private static enum an:Lhiw;

.field private static enum ao:Lhiw;

.field private static enum ap:Lhiw;

.field private static enum aq:Lhiw;

.field private static enum ar:Lhiw;

.field private static enum as:Lhiw;

.field private static enum at:Lhiw;

.field private static enum au:Lhiw;

.field private static enum av:Lhiw;

.field private static enum aw:Lhiw;

.field private static enum ax:Lhiw;

.field private static enum ay:Lhiw;

.field private static enum az:Lhiw;

.field public static final enum b:Lhiw;

.field public static final enum c:Lhiw;

.field public static final enum d:Lhiw;

.field public static final enum e:Lhiw;

.field public static final enum f:Lhiw;

.field public static final enum g:Lhiw;

.field public static final enum h:Lhiw;

.field public static final enum i:Lhiw;

.field public static final enum j:Lhiw;

.field public static final enum k:Lhiw;

.field public static final enum l:Lhiw;

.field public static final enum m:Lhiw;

.field public static final enum n:Lhiw;

.field public static final enum o:Lhiw;

.field public static final enum p:Lhiw;

.field public static final enum q:Lhiw;

.field public static final enum r:Lhiw;

.field private static enum t:Lhiw;

.field private static enum u:Lhiw;

.field private static enum v:Lhiw;

.field private static enum w:Lhiw;

.field private static enum x:Lhiw;

.field private static enum y:Lhiw;

.field private static enum z:Lhiw;


# instance fields
.field public final s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_NULL_MD5"

    const-string v2, "SSL_RSA_WITH_NULL_MD5"

    invoke-direct {v0, v1, v4, v2}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->t:Lhiw;

    .line 10
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_NULL_SHA"

    const-string v2, "SSL_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v5, v2}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->u:Lhiw;

    .line 11
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_EXPORT_WITH_RC4_40_MD5"

    const-string v2, "SSL_RSA_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v6, v2}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->v:Lhiw;

    .line 12
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_RC4_128_MD5"

    const-string v2, "SSL_RSA_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v7, v2}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->w:Lhiw;

    .line 13
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_RC4_128_SHA"

    const-string v2, "SSL_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v8, v2}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->x:Lhiw;

    .line 14
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/4 v2, 0x5

    const-string v3, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->y:Lhiw;

    .line 15
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_DES_CBC_SHA"

    const/4 v2, 0x6

    const-string v3, "SSL_RSA_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->z:Lhiw;

    .line 16
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_3DES_EDE_CBC_SHA"

    const/4 v2, 0x7

    const-string v3, "SSL_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->a:Lhiw;

    .line 17
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0x8

    const-string v3, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->A:Lhiw;

    .line 18
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_DES_CBC_SHA"

    const/16 v2, 0x9

    const-string v3, "SSL_DHE_DSS_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->B:Lhiw;

    .line 19
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0xa

    const-string v3, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->C:Lhiw;

    .line 20
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0xb

    const-string v3, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->D:Lhiw;

    .line 21
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_DES_CBC_SHA"

    const/16 v2, 0xc

    const-string v3, "SSL_DHE_RSA_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->E:Lhiw;

    .line 22
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0xd

    const-string v3, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->F:Lhiw;

    .line 23
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_EXPORT_WITH_RC4_40_MD5"

    const/16 v2, 0xe

    const-string v3, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->G:Lhiw;

    .line 24
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_RC4_128_MD5"

    const/16 v2, 0xf

    const-string v3, "SSL_DH_anon_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->H:Lhiw;

    .line 25
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    const/16 v2, 0x10

    const-string v3, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->I:Lhiw;

    .line 26
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_DES_CBC_SHA"

    const/16 v2, 0x11

    const-string v3, "SSL_DH_anon_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->J:Lhiw;

    .line 27
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x12

    const-string v3, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->K:Lhiw;

    .line 28
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_DES_CBC_SHA"

    const/16 v2, 0x13

    const-string v3, "TLS_KRB5_WITH_DES_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->L:Lhiw;

    .line 29
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x14

    const-string v3, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->M:Lhiw;

    .line 30
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_RC4_128_SHA"

    const/16 v2, 0x15

    const-string v3, "TLS_KRB5_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->N:Lhiw;

    .line 31
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_DES_CBC_MD5"

    const/16 v2, 0x16

    const-string v3, "TLS_KRB5_WITH_DES_CBC_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->O:Lhiw;

    .line 32
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    const/16 v2, 0x17

    const-string v3, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->P:Lhiw;

    .line 33
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_WITH_RC4_128_MD5"

    const/16 v2, 0x18

    const-string v3, "TLS_KRB5_WITH_RC4_128_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->Q:Lhiw;

    .line 34
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    const/16 v2, 0x19

    const-string v3, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->R:Lhiw;

    .line 35
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    const/16 v2, 0x1a

    const-string v3, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->S:Lhiw;

    .line 36
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    const/16 v2, 0x1b

    const-string v3, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->T:Lhiw;

    .line 37
    new-instance v0, Lhiw;

    const-string v1, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    const/16 v2, 0x1c

    const-string v3, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->U:Lhiw;

    .line 38
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1d

    const-string v3, "TLS_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->b:Lhiw;

    .line 39
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1e

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->V:Lhiw;

    .line 40
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x1f

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->c:Lhiw;

    .line 41
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x20

    const-string v3, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->W:Lhiw;

    .line 42
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x21

    const-string v3, "TLS_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->d:Lhiw;

    .line 43
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x22

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->X:Lhiw;

    .line 44
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x23

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->e:Lhiw;

    .line 45
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x24

    const-string v3, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->Y:Lhiw;

    .line 46
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_NULL_SHA256"

    const/16 v2, 0x25

    const-string v3, "TLS_RSA_WITH_NULL_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->Z:Lhiw;

    .line 47
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x26

    const-string v3, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aa:Lhiw;

    .line 48
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x27

    const-string v3, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ab:Lhiw;

    .line 49
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x28

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ac:Lhiw;

    .line 50
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x29

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ad:Lhiw;

    .line 51
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2a

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ae:Lhiw;

    .line 52
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2b

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->af:Lhiw;

    .line 53
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x2c

    const-string v3, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ag:Lhiw;

    .line 54
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    const/16 v2, 0x2d

    const-string v3, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ah:Lhiw;

    .line 55
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x2e

    const-string v3, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->f:Lhiw;

    .line 56
    new-instance v0, Lhiw;

    const-string v1, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x2f

    const-string v3, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ai:Lhiw;

    .line 57
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x30

    const-string v3, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->g:Lhiw;

    .line 58
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x31

    const-string v3, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->h:Lhiw;

    .line 59
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x32

    const-string v3, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->i:Lhiw;

    .line 60
    new-instance v0, Lhiw;

    const-string v1, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x33

    const-string v3, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->j:Lhiw;

    .line 61
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x34

    const-string v3, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aj:Lhiw;

    .line 62
    new-instance v0, Lhiw;

    const-string v1, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x35

    const-string v3, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ak:Lhiw;

    .line 63
    new-instance v0, Lhiw;

    const-string v1, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    const/16 v2, 0x36

    const-string v3, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->al:Lhiw;

    .line 64
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    const/16 v2, 0x37

    const-string v3, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->am:Lhiw;

    .line 65
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    const/16 v2, 0x38

    const-string v3, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->an:Lhiw;

    .line 66
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x39

    const-string v3, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ao:Lhiw;

    .line 67
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x3a

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ap:Lhiw;

    .line 68
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x3b

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aq:Lhiw;

    .line 69
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    const/16 v2, 0x3c

    const-string v3, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ar:Lhiw;

    .line 70
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    const/16 v2, 0x3d

    const-string v3, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->as:Lhiw;

    .line 71
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x3e

    const-string v3, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->at:Lhiw;

    .line 72
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x3f

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->k:Lhiw;

    .line 73
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x40

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->l:Lhiw;

    .line 74
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_NULL_SHA"

    const/16 v2, 0x41

    const-string v3, "TLS_ECDH_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->au:Lhiw;

    .line 75
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    const/16 v2, 0x42

    const-string v3, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->av:Lhiw;

    .line 76
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x43

    const-string v3, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aw:Lhiw;

    .line 77
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x44

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ax:Lhiw;

    .line 78
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x45

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->ay:Lhiw;

    .line 79
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    const/16 v2, 0x46

    const-string v3, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->az:Lhiw;

    .line 80
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    const/16 v2, 0x47

    const-string v3, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aA:Lhiw;

    .line 81
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x48

    const-string v3, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aB:Lhiw;

    .line 82
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x49

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->m:Lhiw;

    .line 83
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x4a

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->n:Lhiw;

    .line 84
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_anon_WITH_NULL_SHA"

    const/16 v2, 0x4b

    const-string v3, "TLS_ECDH_anon_WITH_NULL_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aC:Lhiw;

    .line 85
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    const/16 v2, 0x4c

    const-string v3, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aD:Lhiw;

    .line 86
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    const/16 v2, 0x4d

    const-string v3, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aE:Lhiw;

    .line 87
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    const/16 v2, 0x4e

    const-string v3, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aF:Lhiw;

    .line 88
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    const/16 v2, 0x4f

    const-string v3, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aG:Lhiw;

    .line 89
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x50

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aH:Lhiw;

    .line 90
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x51

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aI:Lhiw;

    .line 91
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x52

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aJ:Lhiw;

    .line 92
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x53

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aK:Lhiw;

    .line 93
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x54

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aL:Lhiw;

    .line 94
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x55

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aM:Lhiw;

    .line 95
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    const/16 v2, 0x56

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aN:Lhiw;

    .line 96
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    const/16 v2, 0x57

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aO:Lhiw;

    .line 97
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x58

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->o:Lhiw;

    .line 98
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x59

    const-string v3, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->p:Lhiw;

    .line 99
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5a

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aP:Lhiw;

    .line 100
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5b

    const-string v3, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aQ:Lhiw;

    .line 101
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5c

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->q:Lhiw;

    .line 102
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5d

    const-string v3, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->r:Lhiw;

    .line 103
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    const/16 v2, 0x5e

    const-string v3, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aR:Lhiw;

    .line 104
    new-instance v0, Lhiw;

    const-string v1, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    const/16 v2, 0x5f

    const-string v3, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    invoke-direct {v0, v1, v2, v3}, Lhiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhiw;->aS:Lhiw;

    .line 105
    const/16 v0, 0x60

    new-array v0, v0, [Lhiw;

    sget-object v1, Lhiw;->t:Lhiw;

    aput-object v1, v0, v4

    sget-object v1, Lhiw;->u:Lhiw;

    aput-object v1, v0, v5

    sget-object v1, Lhiw;->v:Lhiw;

    aput-object v1, v0, v6

    sget-object v1, Lhiw;->w:Lhiw;

    aput-object v1, v0, v7

    sget-object v1, Lhiw;->x:Lhiw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lhiw;->y:Lhiw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhiw;->z:Lhiw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhiw;->a:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhiw;->A:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhiw;->B:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhiw;->C:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhiw;->D:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhiw;->E:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhiw;->F:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhiw;->G:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhiw;->H:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhiw;->I:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lhiw;->J:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lhiw;->K:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lhiw;->L:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lhiw;->M:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lhiw;->N:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lhiw;->O:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lhiw;->P:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lhiw;->Q:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lhiw;->R:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lhiw;->S:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lhiw;->T:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lhiw;->U:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lhiw;->b:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lhiw;->V:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lhiw;->c:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lhiw;->W:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lhiw;->d:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lhiw;->X:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lhiw;->e:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lhiw;->Y:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lhiw;->Z:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lhiw;->aa:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lhiw;->ab:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lhiw;->ac:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lhiw;->ad:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lhiw;->ae:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lhiw;->af:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lhiw;->ag:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lhiw;->ah:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lhiw;->f:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lhiw;->ai:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lhiw;->g:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lhiw;->h:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lhiw;->i:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lhiw;->j:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lhiw;->aj:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lhiw;->ak:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lhiw;->al:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lhiw;->am:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lhiw;->an:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lhiw;->ao:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lhiw;->ap:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lhiw;->aq:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lhiw;->ar:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lhiw;->as:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lhiw;->at:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lhiw;->k:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lhiw;->l:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lhiw;->au:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lhiw;->av:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lhiw;->aw:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lhiw;->ax:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lhiw;->ay:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lhiw;->az:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lhiw;->aA:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lhiw;->aB:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lhiw;->m:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lhiw;->n:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lhiw;->aC:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lhiw;->aD:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lhiw;->aE:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lhiw;->aF:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lhiw;->aG:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lhiw;->aH:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lhiw;->aI:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lhiw;->aJ:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lhiw;->aK:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lhiw;->aL:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lhiw;->aM:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lhiw;->aN:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lhiw;->aO:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lhiw;->o:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lhiw;->p:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lhiw;->aP:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lhiw;->aQ:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lhiw;->q:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lhiw;->r:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lhiw;->aR:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lhiw;->aS:Lhiw;

    aput-object v2, v0, v1

    sput-object v0, Lhiw;->aT:[Lhiw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4
    iput-object p3, p0, Lhiw;->s:Ljava/lang/String;

    .line 5
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhiw;
    .locals 2

    .prologue
    .line 6
    const-string v0, "SSL_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TLS_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    .line 7
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhiw;->b(Ljava/lang/String;)Lhiw;

    move-result-object v0

    .line 8
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lhiw;->b(Ljava/lang/String;)Lhiw;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Lhiw;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lhiw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhiw;

    return-object v0
.end method

.method public static values()[Lhiw;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhiw;->aT:[Lhiw;

    invoke-virtual {v0}, [Lhiw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhiw;

    return-object v0
.end method
