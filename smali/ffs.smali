.class public Lffs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfee;
.implements Lffu;


# instance fields
.field public final a:Lfdd;

.field public final b:Landroid/content/Context;

.field public final c:Lfdr;

.field public d:I

.field public e:Z

.field public f:Lfft;

.field public g:Lffp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfdd;Lfdr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput v2, p0, Lffs;->d:I

    .line 19
    iput-object p1, p0, Lffs;->b:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lffs;->a:Lfdd;

    .line 21
    iput-object p3, p0, Lffs;->c:Lfdr;

    .line 22
    new-instance v0, Lfft;

    invoke-direct {v0, p1}, Lfft;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lffs;->f:Lfft;

    .line 23
    iget-object v0, p0, Lffs;->f:Lfft;

    invoke-virtual {v0, p0}, Lfft;->a(Lffu;)V

    .line 24
    new-instance v0, Lffp;

    invoke-direct {v0, p1}, Lffp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lffs;->g:Lffp;

    .line 25
    iget-object v0, p0, Lffs;->g:Lffp;

    .line 26
    const-string v1, "UserActivityMonitor.register"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iput-object p0, v0, Lffp;->d:Lffs;

    .line 28
    iget-object v1, v0, Lffp;->b:Lfjz;

    invoke-interface {v1}, Lfjz;->a()V

    .line 29
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 30
    const-string v2, "com.google.android.libraries.dialer.voip.call.util.user_activity_action"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 31
    iget-object v2, v0, Lffp;->a:Landroid/content/Context;

    iget-object v0, v0, Lffp;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 32
    return-void
.end method

.method public static a(Lgiq;Lfdd;)I
    .locals 3

    .prologue
    const/4 v0, 0x5

    .line 33
    iget-object v1, p0, Lgiq;->b:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    const/16 v0, 0xb38

    invoke-virtual {p1, v0}, Lfdd;->a(I)V

    .line 35
    const/4 v0, 0x6

    .line 45
    :goto_0
    return v0

    .line 36
    :cond_0
    iget-object v1, p0, Lgiq;->b:Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37
    const/16 v1, 0xb36

    invoke-virtual {p1, v1}, Lfdd;->a(I)V

    goto :goto_0

    .line 39
    :cond_1
    iget-object v1, p0, Lgiq;->b:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 40
    const/16 v0, 0xb3a

    invoke-virtual {p1, v0}, Lfdd;->a(I)V

    .line 41
    const/4 v0, 0x7

    goto :goto_0

    .line 42
    :cond_2
    iget-object v1, p0, Lgiq;->b:Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43
    const/16 v0, 0xb34

    invoke-virtual {p1, v0}, Lfdd;->a(I)V

    .line 44
    const/16 v0, 0x9

    goto :goto_0

    .line 45
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x4b

    const/16 v6, 0xa

    .line 1
    const-string v1, "DomesticSwitchPolicy.onActivityTypeChanged, activityType: "

    .line 2
    invoke-static {p1}, Lffp;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 3
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v0, p0, Lffs;->b:Landroid/content/Context;

    .line 5
    const-string v0, "biking_handoff_confidence_percentage_threshold"

    invoke-static {v0, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v0

    .line 7
    iget-object v2, p0, Lffs;->b:Landroid/content/Context;

    .line 8
    const-string v2, "driving_handoff_confidence_percentage_threshold"

    invoke-static {v2, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 10
    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    int-to-long v4, p2

    cmp-long v0, v4, v0

    if-ltz v0, :cond_2

    .line 11
    iget-object v0, p0, Lffs;->a:Lfdd;

    const/16 v1, 0xb9e

    invoke-virtual {v0, v1}, Lfdd;->a(I)V

    .line 12
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    invoke-virtual {v0, v1, v2, v6}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    .line 16
    :cond_0
    :goto_1
    return-void

    .line 2
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 13
    :cond_2
    if-nez p1, :cond_0

    int-to-long v0, p2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 14
    iget-object v0, p0, Lffs;->a:Lfdd;

    const/16 v1, 0xba0

    invoke-virtual {v0, v1}, Lfdd;->a(I)V

    .line 15
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    invoke-virtual {v0, v1, v2, v6}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    goto :goto_1
.end method

.method public a(Lffy;)V
    .locals 5

    .prologue
    const/16 v4, 0xb55

    .line 46
    iget-object v0, p0, Lffs;->b:Landroid/content/Context;

    const-string v1, "phone"

    .line 47
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 48
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    .line 49
    iget-boolean v1, p1, Lffy;->a:Z

    if-nez v1, :cond_1

    .line 50
    iget-object v0, p0, Lffs;->a:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->a(I)V

    .line 51
    const/4 v0, 0x3

    iput v0, p0, Lffs;->d:I

    .line 52
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    iget v3, p0, Lffs;->d:I

    invoke-virtual {v0, v1, v2, v3}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    .line 55
    iget-object v2, v2, Lfdd;->j:Lffb;

    .line 56
    iget-object v3, p0, Lffs;->b:Landroid/content/Context;

    .line 57
    invoke-static {v3}, Lfho;->e(Landroid/content/Context;)I

    move-result v3

    .line 58
    invoke-static {v1, v2, p1, v0, v3}, Lfmd;->a(Landroid/content/Context;Lffb;Lffy;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p0, Lffs;->a:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->a(I)V

    .line 60
    const/4 v0, 0x1

    iput v0, p0, Lffs;->d:I

    .line 61
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    iget v3, p0, Lffs;->d:I

    invoke-virtual {v0, v1, v2, v3}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    goto :goto_0
.end method

.method public a(Lgiq;)V
    .locals 4

    .prologue
    .line 66
    if-eqz p1, :cond_0

    iget-object v0, p1, Lgiq;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lgiq;->a:Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lffs;->a:Lfdd;

    invoke-static {p1, v0}, Lffs;->a(Lgiq;Lfdd;)I

    move-result v0

    iput v0, p0, Lffs;->d:I

    .line 69
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    iget v3, p0, Lffs;->d:I

    invoke-virtual {v0, v1, v2, v3}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    .line 70
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lffs;->e:Z

    .line 99
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 63
    const/4 v0, 0x2

    iput v0, p0, Lffs;->d:I

    .line 64
    iget-object v0, p0, Lffs;->c:Lfdr;

    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    iget-object v2, p0, Lffs;->a:Lfdd;

    iget v3, p0, Lffs;->d:I

    invoke-virtual {v0, v1, v2, v3}, Lfdr;->a(Landroid/content/Context;Lfdd;I)V

    .line 65
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 71
    .line 73
    iget-object v0, p0, Lffs;->a:Lfdd;

    .line 74
    iget-object v0, v0, Lfdd;->g:Lfdk;

    .line 75
    if-eqz v0, :cond_5

    iget-object v0, p0, Lffs;->a:Lfdd;

    .line 76
    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    .line 77
    iget-object v0, p0, Lffs;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->G(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 79
    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    const v4, 0x7f11028d

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 80
    const v0, 0x7f0200df

    .line 81
    :goto_0
    if-nez v1, :cond_1

    .line 82
    iget-object v1, p0, Lffs;->b:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 83
    if-nez v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 84
    iget-boolean v0, p0, Lffs;->e:Z

    if-eqz v0, :cond_3

    .line 85
    const v0, 0x7f0200ce

    .line 87
    :cond_0
    :goto_1
    iget-object v4, p0, Lffs;->a:Lfdd;

    .line 88
    iget-object v4, v4, Lfdd;->a:Lfef;

    .line 89
    iput-boolean v6, v4, Lfef;->b:Z

    .line 90
    :cond_1
    new-instance v4, Landroid/telecom/StatusHints;

    iget-object v5, p0, Lffs;->b:Landroid/content/Context;

    .line 91
    invoke-static {v5, v0}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v5

    invoke-direct {v4, v1, v5, v3}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    .line 92
    iget-object v3, p0, Lffs;->a:Lfdd;

    invoke-virtual {v3}, Lfdd;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/telecom/StatusHints;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 94
    if-nez v0, :cond_4

    const-string v0, "0"

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x37

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v3, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "DomesticSwitchPolicy.updateStatusHints, label: "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", icon: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    .line 95
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lffs;->a:Lfdd;

    invoke-virtual {v0, v4}, Lfdd;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 97
    :cond_2
    return-void

    .line 86
    :cond_3
    const v0, 0x7f0200eb

    goto :goto_1

    .line 94
    :cond_4
    iget-object v3, p0, Lffs;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v0, v2

    move-object v1, v3

    goto/16 :goto_0
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    iget-object v0, p0, Lffs;->f:Lfft;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lffs;->f:Lfft;

    invoke-virtual {v0}, Lfft;->a()V

    .line 102
    iput-object v4, p0, Lffs;->f:Lfft;

    .line 103
    :cond_0
    iget-object v0, p0, Lffs;->g:Lffp;

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lffs;->g:Lffp;

    .line 105
    iput-object v4, v0, Lffp;->d:Lffs;

    .line 106
    iget-object v1, v0, Lffp;->b:Lfjz;

    invoke-interface {v1}, Lfjz;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, v0, Lffp;->c:Lfmu;

    iget-object v2, v0, Lffp;->b:Lfjz;

    invoke-virtual {v0}, Lffp;->a()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lfmu;->a(Lfjz;Landroid/app/PendingIntent;)Lfkc;

    .line 108
    :cond_1
    iget-object v1, v0, Lffp;->b:Lfjz;

    invoke-interface {v1}, Lfjz;->b()V

    .line 109
    iget-object v1, v0, Lffp;->a:Landroid/content/Context;

    iget-object v0, v0, Lffp;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 110
    iput-object v4, p0, Lffs;->g:Lffp;

    .line 111
    :cond_2
    return-void
.end method
