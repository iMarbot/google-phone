.class public final Lgmk;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmk;


# instance fields
.field public broadcastId:Ljava/lang/String;

.field public hangoutId:Ljava/lang/String;

.field public liveStreamDetails:Lgmn;

.field public liveStreamStatus:Lgmo;

.field public ownerProfileId:Ljava/lang/String;

.field public producerParticipantId:Ljava/lang/String;

.field public projectId:Ljava/lang/String;

.field public recordingDetails:Lgmp;

.field public recordingStatus:Lgmq;

.field public requestedReliability:Ljava/lang/Integer;

.field public resolution:Ljava/lang/Integer;

.field public uiDescription:Lgmt;

.field public useCase:Ljava/lang/Integer;

.field public visibleParticipantId:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lhft;-><init>()V

    .line 29
    invoke-virtual {p0}, Lgmk;->clear()Lgmk;

    .line 30
    return-void
.end method

.method public static checkFullScreenOrThrow(I)I
    .locals 3

    .prologue
    .line 15
    packed-switch p0, :pswitch_data_0

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum FullScreen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :pswitch_0
    return p0

    .line 15
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkFullScreenOrThrow([I)[I
    .locals 3

    .prologue
    .line 18
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 19
    invoke-static {v2}, Lgmk;->checkFullScreenOrThrow(I)I

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_0
    return-object p0
.end method

.method public static checkReliabilityOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Reliability"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkReliabilityOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgmk;->checkReliabilityOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static checkResolutionOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Resolution"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkResolutionOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgmk;->checkResolutionOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgmk;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lgmk;->_emptyArray:[Lgmk;

    if-nez v0, :cond_1

    .line 23
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lgmk;->_emptyArray:[Lgmk;

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [Lgmk;

    sput-object v0, Lgmk;->_emptyArray:[Lgmk;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lgmk;->_emptyArray:[Lgmk;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmk;
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lgmk;

    invoke-direct {v0}, Lgmk;-><init>()V

    invoke-virtual {v0, p0}, Lgmk;->mergeFrom(Lhfp;)Lgmk;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmk;
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lgmk;

    invoke-direct {v0}, Lgmk;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmk;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmk;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iput-object v1, p0, Lgmk;->hangoutId:Ljava/lang/String;

    .line 32
    iput-object v1, p0, Lgmk;->broadcastId:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lgmk;->projectId:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lgmk;->resolution:Ljava/lang/Integer;

    .line 35
    iput-object v1, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    .line 36
    iput-object v1, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lgmk;->uiDescription:Lgmt;

    .line 38
    iput-object v1, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;

    .line 39
    iput-object v1, p0, Lgmk;->recordingDetails:Lgmp;

    .line 40
    iput-object v1, p0, Lgmk;->liveStreamDetails:Lgmn;

    .line 41
    iput-object v1, p0, Lgmk;->liveStreamStatus:Lgmo;

    .line 42
    iput-object v1, p0, Lgmk;->recordingStatus:Lgmq;

    .line 43
    iput-object v1, p0, Lgmk;->useCase:Ljava/lang/Integer;

    .line 44
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lgmk;->unknownFieldData:Lhfv;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lgmk;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 83
    iget-object v2, p0, Lgmk;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 84
    const/4 v2, 0x1

    iget-object v3, p0, Lgmk;->hangoutId:Ljava/lang/String;

    .line 85
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_0
    iget-object v2, p0, Lgmk;->broadcastId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 87
    const/4 v2, 0x2

    iget-object v3, p0, Lgmk;->broadcastId:Ljava/lang/String;

    .line 88
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 89
    :cond_1
    iget-object v2, p0, Lgmk;->projectId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 90
    const/4 v2, 0x3

    iget-object v3, p0, Lgmk;->projectId:Ljava/lang/String;

    .line 91
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    :cond_2
    iget-object v2, p0, Lgmk;->resolution:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 93
    const/4 v2, 0x4

    iget-object v3, p0, Lgmk;->resolution:Ljava/lang/Integer;

    .line 94
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 95
    :cond_3
    iget-object v2, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 96
    const/4 v2, 0x5

    iget-object v3, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    .line 97
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 98
    :cond_4
    iget-object v2, p0, Lgmk;->uiDescription:Lgmt;

    if-eqz v2, :cond_5

    .line 99
    const/4 v2, 0x6

    iget-object v3, p0, Lgmk;->uiDescription:Lgmt;

    .line 100
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_5
    iget-object v2, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    .line 102
    const/4 v2, 0x7

    iget-object v3, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;

    .line 103
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_6
    iget-object v2, p0, Lgmk;->recordingDetails:Lgmp;

    if-eqz v2, :cond_7

    .line 105
    const/16 v2, 0x8

    iget-object v3, p0, Lgmk;->recordingDetails:Lgmp;

    .line 106
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 107
    :cond_7
    iget-object v2, p0, Lgmk;->liveStreamDetails:Lgmn;

    if-eqz v2, :cond_8

    .line 108
    const/16 v2, 0x9

    iget-object v3, p0, Lgmk;->liveStreamDetails:Lgmn;

    .line 109
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 110
    :cond_8
    iget-object v2, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 111
    const/16 v2, 0xa

    iget-object v3, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    .line 112
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 113
    :cond_9
    iget-object v2, p0, Lgmk;->liveStreamStatus:Lgmo;

    if-eqz v2, :cond_a

    .line 114
    const/16 v2, 0xb

    iget-object v3, p0, Lgmk;->liveStreamStatus:Lgmo;

    .line 115
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 116
    :cond_a
    iget-object v2, p0, Lgmk;->recordingStatus:Lgmq;

    if-eqz v2, :cond_b

    .line 117
    const/16 v2, 0xc

    iget-object v3, p0, Lgmk;->recordingStatus:Lgmq;

    .line 118
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 119
    :cond_b
    iget-object v2, p0, Lgmk;->useCase:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    .line 120
    const/16 v2, 0xd

    iget-object v3, p0, Lgmk;->useCase:Ljava/lang/Integer;

    .line 121
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 122
    :cond_c
    iget-object v2, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_f

    move v2, v1

    move v3, v1

    .line 125
    :goto_0
    iget-object v4, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_e

    .line 126
    iget-object v4, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 127
    if-eqz v4, :cond_d

    .line 128
    add-int/lit8 v3, v3, 0x1

    .line 130
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 131
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    :cond_e
    add-int/2addr v0, v2

    .line 133
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 134
    :cond_f
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 135
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 136
    sparse-switch v0, :sswitch_data_0

    .line 138
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    :sswitch_0
    return-object p0

    .line 140
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmk;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 142
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmk;->broadcastId:Ljava/lang/String;

    goto :goto_0

    .line 144
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmk;->projectId:Ljava/lang/String;

    goto :goto_0

    .line 146
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 148
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 149
    invoke-static {v3}, Lgmk;->checkResolutionOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgmk;->resolution:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 153
    invoke-virtual {p0, p1, v0}, Lgmk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 155
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_6
    iget-object v0, p0, Lgmk;->uiDescription:Lgmt;

    if-nez v0, :cond_1

    .line 158
    new-instance v0, Lgmt;

    invoke-direct {v0}, Lgmt;-><init>()V

    iput-object v0, p0, Lgmk;->uiDescription:Lgmt;

    .line 159
    :cond_1
    iget-object v0, p0, Lgmk;->uiDescription:Lgmt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 161
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 163
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 164
    invoke-static {v3}, Lgmk;->checkReliabilityOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 167
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 168
    invoke-virtual {p0, p1, v0}, Lgmk;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 170
    :sswitch_8
    iget-object v0, p0, Lgmk;->recordingDetails:Lgmp;

    if-nez v0, :cond_2

    .line 171
    new-instance v0, Lgmp;

    invoke-direct {v0}, Lgmp;-><init>()V

    iput-object v0, p0, Lgmk;->recordingDetails:Lgmp;

    .line 172
    :cond_2
    iget-object v0, p0, Lgmk;->recordingDetails:Lgmp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 174
    :sswitch_9
    iget-object v0, p0, Lgmk;->liveStreamDetails:Lgmn;

    if-nez v0, :cond_3

    .line 175
    new-instance v0, Lgmn;

    invoke-direct {v0}, Lgmn;-><init>()V

    iput-object v0, p0, Lgmk;->liveStreamDetails:Lgmn;

    .line 176
    :cond_3
    iget-object v0, p0, Lgmk;->liveStreamDetails:Lgmn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 178
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    goto/16 :goto_0

    .line 180
    :sswitch_b
    iget-object v0, p0, Lgmk;->liveStreamStatus:Lgmo;

    if-nez v0, :cond_4

    .line 181
    new-instance v0, Lgmo;

    invoke-direct {v0}, Lgmo;-><init>()V

    iput-object v0, p0, Lgmk;->liveStreamStatus:Lgmo;

    .line 182
    :cond_4
    iget-object v0, p0, Lgmk;->liveStreamStatus:Lgmo;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 184
    :sswitch_c
    iget-object v0, p0, Lgmk;->recordingStatus:Lgmq;

    if-nez v0, :cond_5

    .line 185
    new-instance v0, Lgmq;

    invoke-direct {v0}, Lgmq;-><init>()V

    iput-object v0, p0, Lgmk;->recordingStatus:Lgmq;

    .line 186
    :cond_5
    iget-object v0, p0, Lgmk;->recordingStatus:Lgmq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 188
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 190
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 191
    invoke-static {v3}, Lgmx;->checkBroadcastUseCaseOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgmk;->useCase:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 194
    :catch_2
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 195
    invoke-virtual {p0, p1, v0}, Lgmk;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 197
    :sswitch_e
    const/16 v0, 0x72

    .line 198
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 199
    iget-object v0, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    .line 200
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 201
    if-eqz v0, :cond_6

    .line 202
    iget-object v3, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    :cond_6
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    .line 204
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 205
    invoke-virtual {p1}, Lhfp;->a()I

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 199
    :cond_7
    iget-object v0, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 207
    :cond_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 208
    iput-object v2, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 136
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lgmk;->mergeFrom(Lhfp;)Lgmk;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lgmk;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x1

    iget-object v1, p0, Lgmk;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lgmk;->broadcastId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x2

    iget-object v1, p0, Lgmk;->broadcastId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 52
    :cond_1
    iget-object v0, p0, Lgmk;->projectId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 53
    const/4 v0, 0x3

    iget-object v1, p0, Lgmk;->projectId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 54
    :cond_2
    iget-object v0, p0, Lgmk;->resolution:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 55
    const/4 v0, 0x4

    iget-object v1, p0, Lgmk;->resolution:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 56
    :cond_3
    iget-object v0, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 57
    const/4 v0, 0x5

    iget-object v1, p0, Lgmk;->producerParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 58
    :cond_4
    iget-object v0, p0, Lgmk;->uiDescription:Lgmt;

    if-eqz v0, :cond_5

    .line 59
    const/4 v0, 0x6

    iget-object v1, p0, Lgmk;->uiDescription:Lgmt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 60
    :cond_5
    iget-object v0, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x7

    iget-object v1, p0, Lgmk;->requestedReliability:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 62
    :cond_6
    iget-object v0, p0, Lgmk;->recordingDetails:Lgmp;

    if-eqz v0, :cond_7

    .line 63
    const/16 v0, 0x8

    iget-object v1, p0, Lgmk;->recordingDetails:Lgmp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 64
    :cond_7
    iget-object v0, p0, Lgmk;->liveStreamDetails:Lgmn;

    if-eqz v0, :cond_8

    .line 65
    const/16 v0, 0x9

    iget-object v1, p0, Lgmk;->liveStreamDetails:Lgmn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 66
    :cond_8
    iget-object v0, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 67
    const/16 v0, 0xa

    iget-object v1, p0, Lgmk;->ownerProfileId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 68
    :cond_9
    iget-object v0, p0, Lgmk;->liveStreamStatus:Lgmo;

    if-eqz v0, :cond_a

    .line 69
    const/16 v0, 0xb

    iget-object v1, p0, Lgmk;->liveStreamStatus:Lgmo;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 70
    :cond_a
    iget-object v0, p0, Lgmk;->recordingStatus:Lgmq;

    if-eqz v0, :cond_b

    .line 71
    const/16 v0, 0xc

    iget-object v1, p0, Lgmk;->recordingStatus:Lgmq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 72
    :cond_b
    iget-object v0, p0, Lgmk;->useCase:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 73
    const/16 v0, 0xd

    iget-object v1, p0, Lgmk;->useCase:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 74
    :cond_c
    iget-object v0, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 75
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_e

    .line 76
    iget-object v1, p0, Lgmk;->visibleParticipantId:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 77
    if-eqz v1, :cond_d

    .line 78
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 79
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_e
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 81
    return-void
.end method
