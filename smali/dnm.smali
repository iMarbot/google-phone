.class public Ldnm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjl;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcjk;

.field public final c:Lbjw;

.field public final d:I

.field public final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcjk;Lbjw;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ldnq;

    invoke-direct {v0, p0}, Ldnq;-><init>(Ldnm;)V

    iput-object v0, p0, Ldnm;->e:Ljava/lang/Runnable;

    .line 5
    const-string v0, "RcsVideoSharePresenter.constructor"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 6
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldnm;->a:Landroid/content/Context;

    .line 7
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjk;

    iput-object v0, p0, Ldnm;->b:Lcjk;

    .line 8
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjw;

    iput-object v0, p0, Ldnm;->c:Lbjw;

    .line 10
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Ldnm;->d:I

    .line 11
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    iget-object v0, p0, Ldnm;->c:Lbjw;

    invoke-virtual {v0, p0}, Lbjw;->a(Ldnm;)V

    .line 14
    invoke-virtual {p0}, Ldnm;->l()V

    .line 15
    iget-object v0, p0, Ldnm;->b:Lcjk;

    invoke-interface {v0, v1, v1}, Lcjk;->a(ZZ)V

    .line 16
    invoke-virtual {p0}, Ldnm;->g()V

    .line 17
    return-void
.end method

.method public a(Landroid/content/Context;Lcjk;)V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public a(Landroid/view/SurfaceView;Landroid/view/SurfaceView;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldnm;->c:Lbjw;

    invoke-virtual {v0, p1, p2}, Lbjw;->a(Landroid/view/SurfaceView;Landroid/view/SurfaceView;)V

    .line 42
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Ldnm;->c:Lbjw;

    invoke-virtual {v0, p0}, Lbjw;->b(Ldnm;)V

    .line 19
    invoke-virtual {p0}, Ldnm;->i()V

    .line 20
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public c()Lcjr;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Lcjr;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "RcsVideoSharePresenter.onCameraPermissionGranted"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Ldnm;->a:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->m(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0}, Ldnm;->l()V

    .line 31
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 23
    invoke-virtual {p0}, Ldnm;->i()V

    .line 24
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Ldnm;->e:Ljava/lang/Runnable;

    iget v2, p0, Ldnm;->d:I

    int-to-long v2, v2

    .line 25
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 26
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 1
    invoke-virtual {p0}, Ldnm;->l()V

    .line 2
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 21
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Ldnm;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 22
    return-void
.end method

.method public j()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 32
    iget-object v2, p0, Ldnm;->a:Landroid/content/Context;

    invoke-static {v2}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "camera_permission_dialog_allowed"

    invoke-interface {v2, v3, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 33
    const-string v1, "RcsVideoSharePresenter.shouldShowCameraPermissionToast"

    const-string v2, "disabled by config"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 35
    :cond_1
    iget-object v2, p0, Ldnm;->a:Landroid/content/Context;

    invoke-static {v2}, Lbvs;->e(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldnm;->a:Landroid/content/Context;

    .line 36
    invoke-static {v2}, Lbsw;->k(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public k()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method l()V
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Ldnm;->b:Lcjk;

    iget-object v1, p0, Ldnm;->c:Lbjw;

    invoke-virtual {v1}, Lbjw;->j()Z

    move-result v1

    iget-object v2, p0, Ldnm;->c:Lbjw;

    invoke-virtual {v2}, Lbjw;->p()Z

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcjk;->a(ZZZ)V

    .line 45
    return-void
.end method

.method m()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Ldnm;->b:Lcjk;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcjk;->a(ZZ)V

    .line 47
    return-void
.end method

.method synthetic n()V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Ldnm;->m()V

    return-void
.end method
