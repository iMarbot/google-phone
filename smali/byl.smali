.class public final Lbyl;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source "PG"

# interfaces
.implements Lcjk;


# instance fields
.field public a:Landroid/hardware/camera2/CameraDevice;

.field public b:Landroid/hardware/camera2/CaptureRequest$Builder;

.field private c:Ljava/lang/String;

.field private d:Lip;

.field private e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

.field private f:Landroid/content/Context;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lip;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbyl;->c:Ljava/lang/String;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    iput-object v0, p0, Lbyl;->d:Lip;

    .line 4
    invoke-virtual {p2}, Lip;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbyl;->f:Landroid/content/Context;

    .line 5
    const v0, 0x7f0e01d0

    .line 6
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    .line 7
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    iput-object v0, p0, Lbyl;->e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    .line 8
    iget-object v0, p0, Lbyl;->e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    invoke-virtual {v0, v1}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->setVisibility(I)V

    .line 9
    const v0, 0x7f0e01d1

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    const/high16 v0, -0x1000000

    invoke-virtual {p3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 11
    return-void
.end method

.method private final a(Landroid/hardware/camera2/CameraManager;)Landroid/hardware/camera2/params/StreamConfigurationMap;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 52
    :try_start_0
    invoke-virtual {p1}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 57
    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 58
    :try_start_1
    invoke-virtual {p1, v6}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 63
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v7, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 64
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_STREAM_CONFIGURATION_MAP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 65
    invoke-virtual {v7, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/params/StreamConfigurationMap;

    .line 66
    if-eqz v0, :cond_0

    .line 67
    iput-object v6, p0, Lbyl;->g:Ljava/lang/String;

    .line 71
    :goto_1
    return-object v0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    const-string v2, "SelfManagedAnswerVideoCallScreen.getFrontFacingCameraSizes"

    const-string v3, "failed to get camera ids"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 56
    goto :goto_1

    .line 60
    :catch_1
    move-exception v0

    .line 61
    const-string v6, "SelfManagedAnswerVideoCallScreen.getFrontFacingCameraSizes"

    const-string v7, "failed to get camera characteristics"

    invoke-static {v6, v7, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 70
    :cond_1
    const-string v0, "SelfManagedAnswerVideoCallScreen.getFrontFacingCameraSizes"

    const-string v2, "No valid configurations."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 71
    goto :goto_1
.end method

.method private final h()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lbyl;->a:Landroid/hardware/camera2/CameraDevice;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lbyl;->a:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lbyl;->a:Landroid/hardware/camera2/CameraDevice;

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const/4 v2, 0x1

    const v13, 0x3fe38e39

    const v12, 0x3dcccccd    # 0.1f

    const/4 v3, 0x0

    .line 12
    .line 13
    iget-object v0, p0, Lbyl;->f:Landroid/content/Context;

    const-class v1, Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    .line 14
    invoke-direct {p0, v0}, Lbyl;->a(Landroid/hardware/camera2/CameraManager;)Landroid/hardware/camera2/params/StreamConfigurationMap;

    move-result-object v1

    .line 15
    if-eqz v1, :cond_5

    .line 16
    const-class v4, Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v4}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(Ljava/lang/Class;)[Landroid/util/Size;

    move-result-object v10

    .line 17
    aget-object v6, v10, v3

    .line 19
    invoke-virtual {v6}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v6}, Landroid/util/Size;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v1, v4

    .line 20
    array-length v11, v10

    move v9, v3

    :goto_0
    if-ge v9, v11, :cond_4

    aget-object v7, v10, v9

    .line 21
    invoke-virtual {v7}, Landroid/util/Size;->getWidth()I

    move-result v1

    const/16 v5, 0x780

    if-ge v1, v5, :cond_6

    .line 22
    invoke-virtual {v7}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v7}, Landroid/util/Size;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v1, v5

    .line 23
    sub-float v1, v5, v13

    .line 24
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v12

    if-gez v1, :cond_2

    move v1, v2

    .line 25
    :goto_1
    sub-float v8, v4, v13

    .line 26
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v12

    if-gez v8, :cond_3

    move v8, v2

    .line 27
    :goto_2
    if-eqz v1, :cond_0

    if-eqz v8, :cond_1

    .line 28
    :cond_0
    invoke-virtual {v7}, Landroid/util/Size;->getWidth()I

    move-result v1

    invoke-virtual {v6}, Landroid/util/Size;->getWidth()I

    move-result v8

    if-le v1, v8, :cond_6

    :cond_1
    move v1, v5

    move-object v4, v7

    .line 31
    :goto_3
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move-object v6, v4

    move v4, v1

    goto :goto_0

    :cond_2
    move v1, v3

    .line 24
    goto :goto_1

    :cond_3
    move v8, v3

    .line 26
    goto :goto_2

    .line 34
    :cond_4
    const-string v1, "SelfManagedAnswerVideoCallScreen.openCamera"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Optimal size: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    invoke-virtual {v6}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v6}, Landroid/util/Size;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 36
    iget-object v2, p0, Lbyl;->e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    invoke-virtual {v2, v1}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->a(F)V

    .line 37
    iget-object v1, p0, Lbyl;->e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    invoke-virtual {v1}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/util/Size;->getWidth()I

    move-result v2

    invoke-virtual {v6}, Landroid/util/Size;->getHeight()I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 38
    :try_start_0
    iget-object v1, p0, Lbyl;->g:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_5
    :goto_4
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    const-string v1, "SelfManagedAnswerVideoCallScreen.openCamera"

    const-string v2, "failed to open camera"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_6
    move v1, v4

    move-object v4, v6

    goto :goto_3
.end method

.method public final a(ZZ)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final a(ZZZ)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public final f()Lip;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbyl;->d:Lip;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbyl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lbyl;->h()V

    .line 85
    return-void
.end method

.method public final onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lbyl;->h()V

    .line 87
    return-void
.end method

.method public final onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    const-string v0, "SelfManagedAnswerVideoCallScreen.opOpened"

    const-string v1, "camera opened."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    iput-object p1, p0, Lbyl;->a:Landroid/hardware/camera2/CameraDevice;

    .line 74
    iget-object v0, p0, Lbyl;->e:Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;

    invoke-virtual {v0}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 75
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v1

    iput-object v1, p0, Lbyl;->b:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 76
    iget-object v1, p0, Lbyl;->b:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 77
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/Surface;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lbym;

    .line 78
    invoke-direct {v1, p0}, Lbym;-><init>(Lbyl;)V

    .line 79
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "SelfManagedAnswerVideoCallScreen.createCameraPreview"

    const-string v2, "failed to create preview"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final p_()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lbyl;->h()V

    .line 44
    return-void
.end method

.method public final q_()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method
