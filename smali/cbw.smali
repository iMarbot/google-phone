.class public final Lcbw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcby;
.implements Lcdi;


# instance fields
.field private a:Lcdc;

.field private b:Lcbx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcdc;Lcca;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcbw;->a:Lcdc;

    .line 3
    const-string v0, "AnswerProximitySensor.constructor"

    const-string v1, "acquiring lock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "answer_pseudo_proximity_wake_lock_enabled"

    const/4 v2, 0x1

    .line 5
    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    new-instance v0, Lcbz;

    invoke-direct {v0, p1, p3}, Lcbz;-><init>(Landroid/content/Context;Lcca;)V

    iput-object v0, p0, Lcbw;->b:Lcbx;

    .line 8
    :goto_0
    iget-object v0, p0, Lcbw;->b:Lcbx;

    invoke-interface {v0, p0}, Lcbx;->a(Lcby;)V

    .line 9
    iget-object v0, p0, Lcbw;->b:Lcbx;

    invoke-interface {v0}, Lcbx;->a()V

    .line 10
    invoke-virtual {p2, p0}, Lcdc;->a(Lcdi;)V

    .line 11
    return-void

    .line 7
    :cond_0
    new-instance v0, Lccc;

    invoke-direct {v0, p1}, Lccc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcbw;->b:Lcbx;

    goto :goto_0
.end method

.method private final l()V
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lcbw;->a:Lcdc;

    invoke-virtual {v0, p0}, Lcdc;->b(Lcdi;)V

    .line 14
    iget-object v0, p0, Lcbw;->b:Lcbx;

    invoke-interface {v0}, Lcbx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15
    const-string v0, "AnswerProximitySensor.releaseProximityWakeLock"

    const-string v1, "releasing lock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Lcbw;->b:Lcbx;

    invoke-interface {v0}, Lcbx;->b()V

    .line 17
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 18
    const-string v0, "AnswerProximitySensor.onDialerCallDisconnect"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-direct {p0}, Lcbw;->l()V

    .line 20
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcbw;->a:Lcdc;

    invoke-virtual {v0}, Lcdc;->j()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 22
    const-string v0, "AnswerProximitySensor.onDialerCallUpdate"

    const-string v1, "no longer incoming, cleaning up"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    invoke-direct {p0}, Lcbw;->l()V

    .line 24
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 28
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcbw;->l()V

    .line 34
    return-void
.end method
