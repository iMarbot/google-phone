.class public Lcov;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;

.field public c:Landroid/telecom/PhoneAccountHandle;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Landroid/net/Uri;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcov;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcln;
    .locals 4

    .prologue
    .line 11
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    const-string v0, "VoicemailModule.provideVoicemailClient"

    const-string v1, "SDK below O"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcrh;

    invoke-direct {v0}, Lcrh;-><init>()V

    .line 20
    :goto_0
    return-object v0

    .line 14
    :cond_0
    invoke-static {p0}, Lclr;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15
    const-string v0, "VoicemailModule.provideVoicemailClient"

    .line 16
    invoke-static {p0}, Lclr;->b(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "missing permissions "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcrh;

    invoke-direct {v0}, Lcrh;-><init>()V

    goto :goto_0

    .line 19
    :cond_1
    const-string v0, "VoicemailModule.provideVoicemailClient"

    const-string v1, "providing VoicemailClientImpl"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcmb;

    invoke-direct {v0}, Lcmb;-><init>()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;
    .locals 2

    .prologue
    .line 1
    new-instance v0, Lcnw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcnw;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;B)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2
    invoke-static {p0, p1}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    .line 3
    invoke-virtual {v0, v1}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    .line 4
    invoke-virtual {v0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 5
    invoke-virtual {v0, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lcnw;->a()Z

    .line 7
    return-void
.end method

.method public static c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcmc;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lcmc;

    .line 9
    invoke-direct {v0, p0, p1}, Lcmc;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 10
    return-object v0
.end method


# virtual methods
.method public a()Lclz;
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v10, 0x0

    .line 42
    iget-object v0, p0, Lcov;->d:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->d:Ljava/lang/Long;

    .line 43
    iget-object v0, p0, Lcov;->a:Ljava/lang/Long;

    if-nez v0, :cond_1

    move-wide v0, v2

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->a:Ljava/lang/Long;

    .line 44
    iget-object v0, p0, Lcov;->e:Ljava/lang/Long;

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->e:Ljava/lang/Long;

    .line 45
    iget-object v0, p0, Lcov;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    move v0, v10

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcov;->i:Ljava/lang/Boolean;

    .line 46
    new-instance v0, Lclz;

    iget-object v1, p0, Lcov;->a:Ljava/lang/Long;

    iget-object v2, p0, Lcov;->b:Ljava/lang/String;

    iget-object v3, p0, Lcov;->c:Landroid/telecom/PhoneAccountHandle;

    iget-object v4, p0, Lcov;->d:Ljava/lang/Long;

    iget-object v5, p0, Lcov;->e:Ljava/lang/Long;

    iget-object v6, p0, Lcov;->f:Ljava/lang/String;

    iget-object v7, p0, Lcov;->g:Ljava/lang/String;

    iget-object v8, p0, Lcov;->h:Landroid/net/Uri;

    iget-object v9, p0, Lcov;->i:Ljava/lang/Boolean;

    .line 47
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-object v11, p0, Lcov;->j:Ljava/lang/String;

    .line 48
    invoke-direct/range {v0 .. v11}, Lclz;-><init>(Ljava/lang/Long;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 49
    return-object v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcov;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcov;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1

    .line 44
    :cond_2
    iget-object v0, p0, Lcov;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_2

    .line 45
    :cond_3
    iget-object v0, p0, Lcov;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_3
.end method

.method public a(J)Lcov;
    .locals 1

    .prologue
    .line 24
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->a:Ljava/lang/Long;

    .line 25
    return-object p0
.end method

.method public a(Landroid/net/Uri;)Lcov;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcov;->h:Landroid/net/Uri;

    .line 37
    return-object p0
.end method

.method public a(Landroid/telecom/PhoneAccountHandle;)Lcov;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcov;->c:Landroid/telecom/PhoneAccountHandle;

    .line 27
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcov;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcov;->b:Ljava/lang/String;

    .line 23
    return-object p0
.end method

.method public a(Z)Lcov;
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcov;->i:Ljava/lang/Boolean;

    .line 39
    return-object p0
.end method

.method public b(J)Lcov;
    .locals 1

    .prologue
    .line 28
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->d:Ljava/lang/Long;

    .line 29
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcov;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcov;->f:Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public c(J)Lcov;
    .locals 1

    .prologue
    .line 30
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcov;->e:Ljava/lang/Long;

    .line 31
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcov;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcov;->g:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcov;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcov;->j:Ljava/lang/String;

    .line 41
    return-object p0
.end method
