.class public final Lfcq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfey;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/Intent;

.field public c:Lfex;

.field public d:Lfez;

.field public e:Lfga;

.field private f:Lfcs;

.field private g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lfcs;

    invoke-direct {v0}, Lfcs;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lfcq;-><init>(Landroid/content/Context;Landroid/content/Intent;Lfcs;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lfcs;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lfcq;->a:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lfcq;->b:Landroid/content/Intent;

    .line 6
    iput-object p3, p0, Lfcq;->f:Lfcs;

    .line 7
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lfcs;->a(I)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    iput-object v0, p0, Lfcq;->g:Ljava/util/concurrent/CountDownLatch;

    .line 8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 9
    const-string v0, "BlockingIncomingHangoutsCallController.getNetworkStatus"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v0, p0, Lfcq;->c:Lfex;

    if-nez v0, :cond_0

    .line 11
    iget-object v0, p0, Lfcq;->f:Lfcs;

    iget-object v1, p0, Lfcq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lfcs;->a(Landroid/content/Context;Lfey;)Lfex;

    move-result-object v0

    iput-object v0, p0, Lfcq;->c:Lfex;

    .line 12
    :cond_0
    new-instance v0, Lfcr;

    invoke-direct {v0, p0}, Lfcr;-><init>(Lfcq;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 13
    :try_start_0
    iget-object v0, p0, Lfcq;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    :goto_0
    return-void

    .line 15
    :catch_0
    move-exception v0

    .line 16
    const-string v1, "BlockingIncomingHangoutsCallController.getNetworkStatus, interrupted"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public final a(Lfez;)V
    .locals 2

    .prologue
    .line 19
    const-string v0, "BlockingIncomingHangoutsCallController.onNetworkSelectionStateFetched"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    iput-object p1, p0, Lfcq;->d:Lfez;

    .line 21
    iget-object v0, p0, Lfcq;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 22
    return-void
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    const-string v0, "BlockingIncomingHangoutsCallController.awaitPstnCall"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    iget-object v3, p0, Lfcq;->a:Landroid/content/Context;

    .line 25
    const-string v0, "phone"

    .line 26
    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 27
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    .line 28
    invoke-static {v3}, Lfmd;->q(Landroid/content/Context;)J

    move-result-wide v4

    .line 31
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 32
    const-string v0, "BlockingIncomingHangoutsCallController.awaitPstnCall, fallback disabled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    move v0, v2

    .line 39
    :goto_2
    return v0

    .line 29
    :cond_0
    invoke-static {v3}, Lfmd;->r(Landroid/content/Context;)J

    move-result-wide v4

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lfcq;->f:Lfcs;

    invoke-virtual {v0, v1}, Lfcs;->a(I)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    .line 34
    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 35
    goto :goto_2

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v1, "BlockingIncomingHangoutsCallController.awaitPstnCall, interrupted"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 40
    const-string v0, "BlockingIncomingHangoutsCallController.addNewIncomingCall"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lfcq;->a:Landroid/content/Context;

    const-string v1, "telecom"

    .line 42
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 43
    const-string v1, "tel"

    .line 44
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 45
    iget-object v2, p0, Lfcq;->e:Lfga;

    invoke-static {v2}, Lfmd;->a(Lfga;)Landroid/os/Bundle;

    move-result-object v2

    .line 46
    if-eqz v1, :cond_0

    .line 47
    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v1

    .line 50
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit16 v3, v3, 0x83

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "BlockingIncomingHangoutsCallController.addNewIncomingCall, adding call with SIM account failed, trying non-SIM account, exception: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :cond_0
    iget-object v1, p0, Lfcq;->a:Landroid/content/Context;

    .line 52
    invoke-static {v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0
.end method
