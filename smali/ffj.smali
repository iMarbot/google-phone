.class public Lffj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Landroid/telecom/ConnectionRequest;

.field public final synthetic b:Lffd;

.field public final synthetic c:Landroid/telecom/PhoneAccountHandle;

.field public final synthetic d:Lfdd;

.field public final synthetic e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Lffd;Landroid/telecom/PhoneAccountHandle;Lfdd;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lffj;->e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iput-object p2, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    iput-object p3, p0, Lffj;->b:Lffd;

    iput-object p4, p0, Lffj;->c:Landroid/telecom/PhoneAccountHandle;

    iput-object p5, p0, Lffj;->d:Lfdd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 13
    const-string v0, "DialerConnectionService.makeProxyNumberConnection.onFailure"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lffj;->e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lffj;->b:Lffd;

    iget-object v2, p0, Lffj;->d:Lfdd;

    .line 16
    iget-object v2, v2, Lfdd;->j:Lffb;

    .line 17
    invoke-static {v0, v1, v2}, Lfmd;->a(Landroid/content/Context;Lffd;Lffb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18
    const-string v0, "DialerConnectionService.makeProxyNumberConnection.onFailure, anonymizing"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lffj;->b:Lffd;

    .line 20
    invoke-virtual {v0}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lffj;->d:Lfdd;

    .line 22
    iget-object v1, v1, Lfdd;->j:Lffb;

    .line 24
    iget-object v1, v1, Lffb;->b:Ljava/lang/String;

    .line 25
    invoke-static {v0, v1}, Lffe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26
    const-string v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 27
    new-instance v2, Landroid/telecom/ConnectionRequest;

    iget-object v3, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    .line 28
    invoke-virtual {v3}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    iget-object v4, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    invoke-virtual {v4}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, Landroid/telecom/ConnectionRequest;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 29
    const-string v1, "DialerConnectionService.makeProxyNumberConnection.onFailure, anonymizing, "

    .line 30
    invoke-static {v0}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v1, v5, [Ljava/lang/Object;

    .line 31
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    iget-object v0, p0, Lffj;->e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lffj;->c:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p0, Lffj;->d:Lfdd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V

    .line 35
    :goto_1
    return-void

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p0, Lffj;->e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lffj;->c:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    iget-object v3, p0, Lffj;->d:Lfdd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1
    const-string v1, "DialerConnectionService.makeProxyNumberConnection, got proxy number: "

    .line 2
    invoke-static {p1}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 3
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    iget-object v0, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    invoke-virtual {v0}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 5
    :goto_1
    const-string v1, "unproxied_phone_number_key"

    iget-object v2, p0, Lffj;->b:Lffd;

    .line 6
    invoke-virtual {v2}, Lffd;->c()Ljava/lang/String;

    move-result-object v2

    .line 7
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    const-string v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 9
    new-instance v2, Landroid/telecom/ConnectionRequest;

    iget-object v3, p0, Lffj;->a:Landroid/telecom/ConnectionRequest;

    .line 10
    invoke-virtual {v3}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Landroid/telecom/ConnectionRequest;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 11
    iget-object v0, p0, Lffj;->e:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    iget-object v1, p0, Lffj;->c:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p0, Lffj;->d:Lfdd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V

    .line 12
    return-void

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 4
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_1
.end method
