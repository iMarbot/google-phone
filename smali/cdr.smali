.class public final Lcdr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Lcdr;


# instance fields
.field public a:Landroid/telecom/InCallService;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/telecom/Call;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcct;->a:Lcct;

    .line 8
    invoke-virtual {v0, p0}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0

    .line 10
    :cond_0
    iget-object v0, v0, Lcdc;->c:Landroid/telecom/Call;

    goto :goto_0
.end method

.method public static a()Lcdr;
    .locals 1

    .prologue
    .line 2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 4
    :cond_0
    sget-object v0, Lcdr;->b:Lcdr;

    if-nez v0, :cond_1

    .line 5
    new-instance v0, Lcdr;

    invoke-direct {v0}, Lcdr;-><init>()V

    sput-object v0, Lcdr;->b:Lcdr;

    .line 6
    :cond_1
    sget-object v0, Lcdr;->b:Lcdr;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 16
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallService;->setAudioRoute(I)V

    .line 19
    :goto_0
    return-void

    .line 18
    :cond_0
    const-string v0, "TelecomAdapter.setAudioRoute"

    const-string v1, "mInCallService is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 39
    invoke-static {p1}, Lcdr;->a(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->postDialContinue(Z)V

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v1, "TelecomAdapter.postDialContinue"

    const-string v2, "call not in call list "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    if-eqz v0, :cond_0

    .line 13
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallService;->setMuted(Z)V

    .line 15
    :goto_0
    return-void

    .line 14
    :cond_0
    const-string v0, "TelecomAdapter.mute"

    const-string v1, "mInCallService is null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcdr;->a:Landroid/telecom/InCallService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService;->stopForeground(Z)V

    .line 47
    :goto_0
    return-void

    .line 46
    :cond_0
    const-string v0, "TelecomAdapter.stopForegroundNotification"

    const-string v1, "no inCallService available for stopping foreground notification"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 20
    invoke-static {p1}, Lcdr;->a(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    invoke-virtual {v1}, Landroid/telecom/Call;->getConferenceableCalls()Ljava/util/List;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 24
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    invoke-virtual {v1, v0}, Landroid/telecom/Call;->conference(Landroid/telecom/Call;)V

    .line 25
    sput v4, Lcdc;->a:I

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    invoke-virtual {v1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {v1}, Landroid/telecom/Call;->mergeConference()V

    .line 29
    sput v4, Lcdc;->a:I

    goto :goto_0

    .line 31
    :cond_2
    const-string v1, "TelecomAdapter.merge"

    const-string v2, "call not in call list "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 33
    invoke-static {p1}, Lcdr;->a(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    invoke-virtual {v0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    invoke-virtual {v0}, Landroid/telecom/Call;->swapConference()V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const-string v1, "TelecomAdapter.swap"

    const-string v2, "call not in call list "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
