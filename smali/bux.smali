.class final Lbux;
.super Landroid/content/AsyncQueryHandler;
.source "PG"


# instance fields
.field public a:Landroid/content/Context;

.field private b:Landroid/net/Uri;

.field private c:Lbut;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 2
    iput-object p1, p0, Lbux;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lbux;->b:Landroid/net/Uri;

    .line 4
    return-void
.end method


# virtual methods
.method final a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 19
    const/16 v0, 0x30

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "##### updateData() #####  for token: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    check-cast p2, Lbuz;

    .line 21
    if-nez p2, :cond_1

    .line 22
    const-string v0, "Cookie is null, ignoring onQueryComplete() request."

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbux;->c:Lbut;

    if-nez v0, :cond_6

    .line 27
    iget-object v0, p0, Lbux;->a:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbux;->b:Landroid/net/Uri;

    if-nez v0, :cond_4

    .line 28
    :cond_2
    new-instance v0, Lbvd;

    const-string v1, "Bad context or query uri, or CallerInfoAsyncQuery already released."

    invoke-direct {v0, v1}, Lbvd;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 53
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 29
    :cond_4
    :try_start_2
    iget v0, p2, Lbuz;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 30
    new-instance v0, Lbut;

    invoke-direct {v0}, Lbut;-><init>()V

    iget-object v1, p0, Lbux;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lbut;->a(Landroid/content/Context;)Lbut;

    move-result-object v0

    iput-object v0, p0, Lbux;->c:Lbut;

    .line 47
    :cond_5
    :goto_1
    const/16 v0, 0x35

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "constructing CallerInfo object for token: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p2, Lbuz;->a:Lbvc;

    if-eqz v0, :cond_6

    .line 49
    iget-object v0, p2, Lbuz;->a:Lbvc;

    iget-object v1, p2, Lbuz;->b:Ljava/lang/Object;

    iget-object v2, p0, Lbux;->c:Lbut;

    invoke-interface {v0, p1, v1, v2}, Lbvc;->b(ILjava/lang/Object;Lbut;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 50
    :cond_6
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 31
    :cond_7
    :try_start_3
    iget v0, p2, Lbuz;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    .line 32
    new-instance v0, Lbut;

    invoke-direct {v0}, Lbut;-><init>()V

    iget-object v1, p0, Lbux;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lbut;->b(Landroid/content/Context;)Lbut;

    move-result-object v0

    iput-object v0, p0, Lbux;->c:Lbut;

    goto :goto_1

    .line 33
    :cond_8
    iget-object v0, p0, Lbux;->a:Landroid/content/Context;

    iget-object v1, p0, Lbux;->b:Landroid/net/Uri;

    invoke-static {v0, v1, p3}, Lbut;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;)Lbut;

    move-result-object v0

    iput-object v0, p0, Lbux;->c:Lbut;

    .line 34
    iget-object v0, p0, Lbux;->c:Lbut;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "==> Got mCallerInfo: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lbux;->a:Landroid/content/Context;

    iget-object v1, p2, Lbuz;->d:Ljava/lang/String;

    iget-object v2, p0, Lbux;->c:Lbut;

    .line 36
    invoke-static {v0, v1, v2}, Lbut;->a(Landroid/content/Context;Ljava/lang/String;Lbut;)Lbut;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lbux;->c:Lbut;

    if-eq v0, v1, :cond_9

    .line 38
    iput-object v0, p0, Lbux;->c:Lbut;

    .line 39
    iget-object v0, p0, Lbux;->c:Lbut;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "#####async contact look up with numeric username"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    :cond_9
    iget-object v0, p0, Lbux;->c:Lbut;

    iget-object v1, p2, Lbuz;->e:Ljava/lang/String;

    iput-object v1, v0, Lbut;->w:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lbux;->c:Lbut;

    iget-object v0, v0, Lbut;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 42
    iget-object v1, p0, Lbux;->c:Lbut;

    iget-object v2, p0, Lbux;->a:Landroid/content/Context;

    iget-object v0, p2, Lbuz;->d:Ljava/lang/String;

    .line 43
    iget-object v3, v1, Lbut;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 44
    :goto_2
    iget-object v3, v1, Lbut;->w:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lbmw;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbut;->f:Ljava/lang/String;

    .line 45
    :cond_a
    iget-object v0, p2, Lbuz;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 46
    iget-object v0, p0, Lbux;->c:Lbut;

    iget-object v1, p2, Lbuz;->d:Ljava/lang/String;

    iput-object v1, v0, Lbut;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 43
    :cond_b
    iget-object v0, v1, Lbut;->c:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method protected final createHandler(Landroid/os/Looper;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lbuy;

    invoke-direct {v0, p0, p1}, Lbuy;-><init>(Lbux;Landroid/os/Looper;)V

    return-object v0
.end method

.method protected final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8
    const/16 v0, 0x45

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "##### onQueryComplete() #####   query complete for token: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    check-cast p2, Lbuz;

    .line 10
    iget-object v0, p2, Lbuz;->a:Lbvc;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p2, Lbuz;->a:Lbvc;

    .line 12
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbux;->c:Lbut;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2b

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "notifying listener: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for token: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 13
    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p2, Lbuz;->a:Lbvc;

    iget-object v1, p2, Lbuz;->b:Ljava/lang/Object;

    iget-object v2, p0, Lbux;->c:Lbut;

    invoke-interface {v0, p1, v1, v2}, Lbvc;->a(ILjava/lang/Object;Lbut;)V

    .line 15
    :cond_0
    iput-object v4, p0, Lbux;->a:Landroid/content/Context;

    .line 16
    iput-object v4, p0, Lbux;->b:Landroid/net/Uri;

    .line 17
    iput-object v4, p0, Lbux;->c:Lbut;

    .line 18
    return-void
.end method

.method public final startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 5
    invoke-super/range {p0 .. p7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    return-void
.end method
