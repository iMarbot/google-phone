.class public final enum Lbld$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbld$a;

.field public static final enum b:Lbld$a;

.field public static final enum c:Lbld$a;

.field public static final enum d:Lbld$a;

.field public static final enum e:Lbld$a;

.field public static final enum f:Lbld$a;

.field public static final enum g:Lbld$a;

.field public static final enum h:Lbld$a;

.field public static final enum i:Lbld$a;

.field public static final enum j:Lbld$a;

.field public static final enum k:Lbld$a;

.field public static final enum l:Lbld$a;

.field public static final enum m:Lbld$a;

.field public static final enum n:Lbld$a;

.field public static final enum o:Lbld$a;

.field public static final enum p:Lbld$a;

.field public static final enum q:Lbld$a;

.field public static final enum r:Lbld$a;

.field public static final enum s:Lbld$a;

.field public static final enum t:Lbld$a;

.field public static final u:Lhby;

.field private static enum v:Lbld$a;

.field private static synthetic x:[Lbld$a;


# instance fields
.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 29
    new-instance v0, Lbld$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->a:Lbld$a;

    .line 30
    new-instance v0, Lbld$a;

    const-string v1, "CHANGE_TAB_TO_FAVORITE"

    invoke-direct {v0, v1, v5, v5}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->b:Lbld$a;

    .line 31
    new-instance v0, Lbld$a;

    const-string v1, "CHANGE_TAB_TO_CALL_LOG"

    invoke-direct {v0, v1, v6, v6}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->c:Lbld$a;

    .line 32
    new-instance v0, Lbld$a;

    const-string v1, "CHANGE_TAB_TO_CONTACTS"

    invoke-direct {v0, v1, v7, v7}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->d:Lbld$a;

    .line 33
    new-instance v0, Lbld$a;

    const-string v1, "CHANGE_TAB_TO_VOICEMAIL"

    invoke-direct {v0, v1, v8, v8}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->e:Lbld$a;

    .line 34
    new-instance v0, Lbld$a;

    const-string v1, "PRESS_ANDROID_BACK_BUTTON"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->f:Lbld$a;

    .line 35
    new-instance v0, Lbld$a;

    const-string v1, "TEXT_CHANGE_WITH_INPUT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->g:Lbld$a;

    .line 36
    new-instance v0, Lbld$a;

    const-string v1, "SCROLL"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->h:Lbld$a;

    .line 37
    new-instance v0, Lbld$a;

    const-string v1, "CLICK_CALL_LOG_ITEM"

    const/16 v2, 0x8

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->i:Lbld$a;

    .line 38
    new-instance v0, Lbld$a;

    const-string v1, "OPEN_CALL_DETAIL"

    const/16 v2, 0x9

    const/16 v3, 0x65

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->j:Lbld$a;

    .line 39
    new-instance v0, Lbld$a;

    const-string v1, "CLOSE_CALL_DETAIL_WITH_CANCEL_BUTTON"

    const/16 v2, 0xa

    const/16 v3, 0x66

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->k:Lbld$a;

    .line 40
    new-instance v0, Lbld$a;

    const-string v1, "COPY_NUMBER_IN_CALL_DETAIL"

    const/16 v2, 0xb

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->l:Lbld$a;

    .line 41
    new-instance v0, Lbld$a;

    const-string v1, "EDIT_NUMBER_BEFORE_CALL_IN_CALL_DETAIL"

    const/16 v2, 0xc

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->m:Lbld$a;

    .line 42
    new-instance v0, Lbld$a;

    const-string v1, "OPEN_DIALPAD"

    const/16 v2, 0xd

    const/16 v3, 0xc8

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->n:Lbld$a;

    .line 43
    new-instance v0, Lbld$a;

    const-string v1, "CLOSE_DIALPAD"

    const/16 v2, 0xe

    const/16 v3, 0xc9

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->o:Lbld$a;

    .line 44
    new-instance v0, Lbld$a;

    const-string v1, "PRESS_CALL_BUTTON_WITHOUT_CALLING"

    const/16 v2, 0xf

    const/16 v3, 0xca

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->p:Lbld$a;

    .line 45
    new-instance v0, Lbld$a;

    const-string v1, "OPEN_SEARCH"

    const/16 v2, 0x10

    const/16 v3, 0x12c

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->q:Lbld$a;

    .line 46
    new-instance v0, Lbld$a;

    const-string v1, "HIDE_KEYBOARD_IN_SEARCH"

    const/16 v2, 0x11

    const/16 v3, 0x12d

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->r:Lbld$a;

    .line 47
    new-instance v0, Lbld$a;

    const-string v1, "CLOSE_SEARCH_WITH_HIDE_BUTTON"

    const/16 v2, 0x12

    const/16 v3, 0x12e

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->v:Lbld$a;

    .line 48
    new-instance v0, Lbld$a;

    const-string v1, "OPEN_CALL_HISTORY"

    const/16 v2, 0x13

    const/16 v3, 0x190

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->s:Lbld$a;

    .line 49
    new-instance v0, Lbld$a;

    const-string v1, "CLOSE_CALL_HISTORY_WITH_CANCEL_BUTTON"

    const/16 v2, 0x14

    const/16 v3, 0x191

    invoke-direct {v0, v1, v2, v3}, Lbld$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbld$a;->t:Lbld$a;

    .line 50
    const/16 v0, 0x15

    new-array v0, v0, [Lbld$a;

    sget-object v1, Lbld$a;->a:Lbld$a;

    aput-object v1, v0, v4

    sget-object v1, Lbld$a;->b:Lbld$a;

    aput-object v1, v0, v5

    sget-object v1, Lbld$a;->c:Lbld$a;

    aput-object v1, v0, v6

    sget-object v1, Lbld$a;->d:Lbld$a;

    aput-object v1, v0, v7

    sget-object v1, Lbld$a;->e:Lbld$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbld$a;->f:Lbld$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbld$a;->g:Lbld$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbld$a;->h:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbld$a;->i:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbld$a;->j:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbld$a;->k:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbld$a;->l:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbld$a;->m:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbld$a;->n:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbld$a;->o:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbld$a;->p:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbld$a;->q:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbld$a;->r:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lbld$a;->v:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lbld$a;->s:Lbld$a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lbld$a;->t:Lbld$a;

    aput-object v2, v0, v1

    sput-object v0, Lbld$a;->x:[Lbld$a;

    .line 51
    new-instance v0, Lble;

    invoke-direct {v0}, Lble;-><init>()V

    sput-object v0, Lbld$a;->u:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lbld$a;->w:I

    .line 28
    return-void
.end method

.method public static a(I)Lbld$a;
    .locals 1

    .prologue
    .line 3
    sparse-switch p0, :sswitch_data_0

    .line 25
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :sswitch_0
    sget-object v0, Lbld$a;->a:Lbld$a;

    goto :goto_0

    .line 5
    :sswitch_1
    sget-object v0, Lbld$a;->b:Lbld$a;

    goto :goto_0

    .line 6
    :sswitch_2
    sget-object v0, Lbld$a;->c:Lbld$a;

    goto :goto_0

    .line 7
    :sswitch_3
    sget-object v0, Lbld$a;->d:Lbld$a;

    goto :goto_0

    .line 8
    :sswitch_4
    sget-object v0, Lbld$a;->e:Lbld$a;

    goto :goto_0

    .line 9
    :sswitch_5
    sget-object v0, Lbld$a;->f:Lbld$a;

    goto :goto_0

    .line 10
    :sswitch_6
    sget-object v0, Lbld$a;->g:Lbld$a;

    goto :goto_0

    .line 11
    :sswitch_7
    sget-object v0, Lbld$a;->h:Lbld$a;

    goto :goto_0

    .line 12
    :sswitch_8
    sget-object v0, Lbld$a;->i:Lbld$a;

    goto :goto_0

    .line 13
    :sswitch_9
    sget-object v0, Lbld$a;->j:Lbld$a;

    goto :goto_0

    .line 14
    :sswitch_a
    sget-object v0, Lbld$a;->k:Lbld$a;

    goto :goto_0

    .line 15
    :sswitch_b
    sget-object v0, Lbld$a;->l:Lbld$a;

    goto :goto_0

    .line 16
    :sswitch_c
    sget-object v0, Lbld$a;->m:Lbld$a;

    goto :goto_0

    .line 17
    :sswitch_d
    sget-object v0, Lbld$a;->n:Lbld$a;

    goto :goto_0

    .line 18
    :sswitch_e
    sget-object v0, Lbld$a;->o:Lbld$a;

    goto :goto_0

    .line 19
    :sswitch_f
    sget-object v0, Lbld$a;->p:Lbld$a;

    goto :goto_0

    .line 20
    :sswitch_10
    sget-object v0, Lbld$a;->q:Lbld$a;

    goto :goto_0

    .line 21
    :sswitch_11
    sget-object v0, Lbld$a;->r:Lbld$a;

    goto :goto_0

    .line 22
    :sswitch_12
    sget-object v0, Lbld$a;->v:Lbld$a;

    goto :goto_0

    .line 23
    :sswitch_13
    sget-object v0, Lbld$a;->s:Lbld$a;

    goto :goto_0

    .line 24
    :sswitch_14
    sget-object v0, Lbld$a;->t:Lbld$a;

    goto :goto_0

    .line 3
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x64 -> :sswitch_8
        0x65 -> :sswitch_9
        0x66 -> :sswitch_a
        0x67 -> :sswitch_b
        0x68 -> :sswitch_c
        0xc8 -> :sswitch_d
        0xc9 -> :sswitch_e
        0xca -> :sswitch_f
        0x12c -> :sswitch_10
        0x12d -> :sswitch_11
        0x12e -> :sswitch_12
        0x190 -> :sswitch_13
        0x191 -> :sswitch_14
    .end sparse-switch
.end method

.method public static values()[Lbld$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbld$a;->x:[Lbld$a;

    invoke-virtual {v0}, [Lbld$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbld$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbld$a;->w:I

    return v0
.end method
