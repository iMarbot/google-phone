.class public final enum Lctx;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lctx;

.field public static final enum b:Lctx;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:Lctx;

.field public static final d:Lctx;

.field private static synthetic e:[Lctx;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lctx;

    const-string v1, "PREFER_ARGB_8888"

    invoke-direct {v0, v1, v2}, Lctx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctx;->a:Lctx;

    .line 4
    new-instance v0, Lctx;

    const-string v1, "PREFER_ARGB_8888_DISALLOW_HARDWARE"

    invoke-direct {v0, v1, v3}, Lctx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctx;->b:Lctx;

    .line 5
    new-instance v0, Lctx;

    const-string v1, "PREFER_RGB_565"

    invoke-direct {v0, v1, v4}, Lctx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lctx;->c:Lctx;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lctx;

    sget-object v1, Lctx;->a:Lctx;

    aput-object v1, v0, v2

    sget-object v1, Lctx;->b:Lctx;

    aput-object v1, v0, v3

    sget-object v1, Lctx;->c:Lctx;

    aput-object v1, v0, v4

    sput-object v0, Lctx;->e:[Lctx;

    .line 7
    sget-object v0, Lctx;->b:Lctx;

    sput-object v0, Lctx;->d:Lctx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lctx;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lctx;->e:[Lctx;

    invoke-virtual {v0}, [Lctx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lctx;

    return-object v0
.end method
