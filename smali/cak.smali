.class final Lcak;
.super Lcbf;
.source "PG"


# direct methods
.method public constructor <init>(Lcae;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcbf;-><init>()V

    .line 2
    iput-object p1, p0, Lcak;->a:Lcae;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Lcbe;)F
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    iget v2, p1, Lcbe;->d:F

    .line 6
    cmpl-float v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    .line 13
    :goto_0
    float-to-double v4, v2

    const-wide v6, 0x3feb333333333333L    # 0.85

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    move v0, v1

    .line 15
    :cond_0
    float-to-double v4, v2

    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 16
    add-float/2addr v0, v1

    .line 17
    :cond_1
    float-to-double v4, v2

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 18
    add-float/2addr v0, v1

    .line 19
    :cond_2
    float-to-double v4, v2

    const-wide v6, 0x3fe199999999999aL    # 0.55

    cmpg-double v3, v4, v6

    if-gez v3, :cond_3

    .line 20
    add-float/2addr v0, v1

    .line 21
    :cond_3
    float-to-double v4, v2

    const-wide v6, 0x3fdccccccccccccdL    # 0.45

    cmpg-double v3, v4, v6

    if-gez v3, :cond_4

    .line 22
    add-float/2addr v0, v1

    .line 23
    :cond_4
    float-to-double v2, v2

    const-wide v4, 0x3fd6666666666666L    # 0.35

    cmpg-double v2, v2, v4

    if-gez v2, :cond_5

    .line 24
    add-float/2addr v0, v1

    .line 26
    :cond_5
    return v0

    .line 8
    :cond_6
    invoke-virtual {p1}, Lcbe;->a()F

    move-result v2

    .line 9
    iget v3, p1, Lcbe;->d:F

    .line 10
    div-float/2addr v2, v3

    goto :goto_0
.end method
