.class public final Lfdw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfhf;


# instance fields
.field public a:Lfds;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Z

.field private e:Lffd;

.field private f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(ILfds;Lffd;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfdx;

    invoke-direct {v0, p0}, Lfdx;-><init>(Lfdw;)V

    iput-object v0, p0, Lfdw;->f:Ljava/lang/Runnable;

    .line 3
    iput p1, p0, Lfdw;->c:I

    .line 4
    iput-object p2, p0, Lfdw;->a:Lfds;

    .line 5
    iput-object p3, p0, Lfdw;->e:Lffd;

    .line 6
    iput-object p4, p0, Lfdw;->b:Landroid/content/Context;

    .line 7
    return-void
.end method

.method private final d()V
    .locals 4

    .prologue
    .line 197
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 198
    const-string v0, "setActive"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 199
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setActive()V

    .line 201
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    .line 202
    iget-object v0, v0, Lfdd;->d:Lffd;

    .line 205
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 208
    const-string v2, "maybeFixAddress, stripping post dial chars: "

    invoke-static {v1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 209
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    .line 210
    invoke-static {v1}, Lffe;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    .line 211
    invoke-virtual {v0, v1, v2}, Lfdd;->setAddress(Landroid/net/Uri;I)V

    .line 212
    :cond_0
    return-void

    .line 208
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private final e()Lfdd;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 227
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 228
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfhj;)V
    .locals 7

    .prologue
    .line 8
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onCallJoin, joinInfo: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v6

    .line 10
    if-eqz v6, :cond_0

    .line 12
    iget-object v0, v6, Lfdd;->a:Lfef;

    .line 13
    iget-object v1, p1, Lfhj;->a:Ljava/lang/String;

    iput-object v1, v0, Lfef;->f:Ljava/lang/String;

    .line 15
    iget-object v0, v6, Lfdd;->a:Lfef;

    .line 16
    iget-object v1, p1, Lfhj;->b:Ljava/lang/String;

    iput-object v1, v0, Lfef;->e:Ljava/lang/String;

    .line 17
    iget-object v0, p0, Lfdw;->b:Landroid/content/Context;

    .line 19
    iget-object v1, v6, Lfdd;->f:Ljava/lang/String;

    .line 20
    iget-object v2, p1, Lfhj;->a:Ljava/lang/String;

    iget-object v3, p1, Lfhj;->b:Ljava/lang/String;

    iget-object v4, p0, Lfdw;->b:Landroid/content/Context;

    .line 21
    invoke-static {v4}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lfht;

    invoke-direct {v5, p0, v6}, Lfht;-><init>(Lfdw;Lfdd;)V

    .line 22
    invoke-static/range {v0 .. v5}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfht;)V

    .line 23
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfds;

    invoke-virtual {v0}, Lfds;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfdw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const-string v0, "No one in hangout, will leave on timeout."

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfdw;->f:Ljava/lang/Runnable;

    iget-object v2, p0, Lfdw;->b:Landroid/content/Context;

    .line 26
    invoke-static {v2}, Lfmd;->w(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 27
    :cond_0
    return-void
.end method

.method public final a(Lfhk;)V
    .locals 3

    .prologue
    .line 109
    iget-boolean v0, p1, Lfhk;->a:Z

    const/16 v1, 0x22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onParticipantAdded, isLocal: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lfhk;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->e:Lffd;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfdw;->d:Z

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lfdw;->c()V

    .line 112
    :cond_0
    return-void
.end method

.method public final a(Lfvx;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 42
    .line 43
    iget v2, p1, Lfvx;->a:I

    .line 45
    const/16 v3, 0x27

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onCallEnd, serviceEndCause: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lfdw;->a(Ljava/lang/String;)V

    .line 48
    iget-object v3, p0, Lfdw;->b:Landroid/content/Context;

    .line 49
    const-string v3, "is_fallback_to_cellular_allowed"

    invoke-static {v3, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v3

    .line 50
    if-nez v3, :cond_1

    .line 51
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, fallback to cellular not allowed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 76
    :goto_0
    if-eqz v2, :cond_9

    .line 77
    const-string v2, "HangoutsCallbacks.attemptFallbackToCellular"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v2

    .line 80
    iget-object v3, v2, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 82
    iget-object v4, p0, Lfdw;->b:Landroid/content/Context;

    .line 83
    invoke-static {v4}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 85
    iget-object v5, v2, Lfdd;->c:Landroid/telecom/ConnectionRequest;

    .line 86
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->createRemoteOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/RemoteConnection;

    move-result-object v3

    .line 87
    if-nez v3, :cond_8

    .line 88
    const-string v2, "HangoutsCallbacks.attemptFallbackToCellular, unable to create remote connection"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 98
    :goto_1
    if-eqz v2, :cond_9

    .line 99
    const-string v1, "onCallEnd, falling back to cellular"

    invoke-virtual {p0, v1}, Lfdw;->a(Ljava/lang/String;)V

    .line 105
    :goto_2
    iget-object v1, p0, Lfdw;->a:Lfds;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lfdw;->a:Lfds;

    invoke-virtual {v1, v0}, Lfds;->c(Z)V

    .line 107
    :cond_0
    invoke-static {}, Lhcw;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfdw;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 108
    return-void

    .line 53
    :cond_1
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v3

    if-nez v3, :cond_2

    .line 54
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, connection is null"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 55
    goto :goto_0

    .line 56
    :cond_2
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v3

    invoke-virtual {v3}, Lfdd;->getState()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    .line 58
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v2

    invoke-virtual {v2}, Lfdd;->getState()I

    move-result v2

    const/16 v3, 0x41

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "HangoutsCallbacks.shouldFallbackToCellular, state is: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    .line 59
    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 60
    goto :goto_0

    .line 61
    :cond_3
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v3

    .line 62
    iget-object v3, v3, Lfdd;->a:Lfef;

    .line 63
    iget-object v3, v3, Lfef;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 64
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, already has a hangout room"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 65
    goto/16 :goto_0

    .line 66
    :cond_4
    const/16 v3, 0x2afc

    if-ne v2, v3, :cond_5

    .line 67
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, local user ended"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 68
    goto/16 :goto_0

    .line 69
    :cond_5
    iget-object v2, p0, Lfdw;->b:Landroid/content/Context;

    .line 70
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_6

    move v2, v1

    .line 71
    :goto_3
    if-eqz v2, :cond_7

    .line 72
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, in airplane mode"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v0

    .line 73
    goto/16 :goto_0

    :cond_6
    move v2, v0

    .line 70
    goto :goto_3

    .line 74
    :cond_7
    const-string v2, "HangoutsCallbacks.shouldFallbackToCellular, returning true"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v1

    .line 75
    goto/16 :goto_0

    .line 90
    :cond_8
    new-instance v4, Lfct;

    iget-object v5, p0, Lfdw;->b:Landroid/content/Context;

    new-instance v6, Lfdr;

    invoke-direct {v6}, Lfdr;-><init>()V

    .line 92
    iget-object v7, v2, Lfdd;->d:Lffd;

    .line 93
    invoke-virtual {v7}, Lffd;->d()Z

    move-result v7

    invoke-direct {v4, v5, v6, v3, v7}, Lfct;-><init>(Landroid/content/Context;Lfdr;Landroid/telecom/RemoteConnection;Z)V

    .line 94
    invoke-virtual {v2, v4}, Lfdd;->a(Lfdb;)V

    .line 96
    const-string v3, "stopConnectingSound"

    invoke-virtual {v2, v3}, Lfdd;->a(Ljava/lang/String;)V

    move v2, v1

    .line 97
    goto/16 :goto_1

    .line 101
    :cond_9
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 102
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    iget-object v2, p0, Lfdw;->b:Landroid/content/Context;

    .line 103
    invoke-static {v2, p1}, Lfmd;->a(Landroid/content/Context;Lfvx;)Landroid/telecom/DisconnectCause;

    move-result-object v2

    .line 104
    invoke-virtual {v0, v2}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method public final a(Lgiq;)V
    .locals 1

    .prologue
    .line 185
    const-string v0, "onQualityNotification"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 187
    iget-object v0, v0, Lfds;->c:Lfee;

    .line 188
    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 190
    iget-object v0, v0, Lfds;->c:Lfee;

    .line 191
    invoke-interface {v0, p1}, Lfee;->a(Lgiq;)V

    .line 192
    :cond_0
    return-void
.end method

.method public final a(Lgpn;)V
    .locals 8

    .prologue
    .line 165
    const-string v0, "onLogDataPrepared"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 166
    new-instance v4, Lgot;

    invoke-direct {v4}, Lgot;-><init>()V

    .line 167
    iput-object p1, v4, Lgot;->logData:Lgpn;

    .line 168
    iget-object v0, v4, Lgot;->logData:Lgpn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 169
    iget-object v0, p0, Lfdw;->b:Landroid/content/Context;

    iget-object v1, p0, Lfdw;->b:Landroid/content/Context;

    .line 170
    invoke-static {v1}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfdw;->b:Landroid/content/Context;

    .line 171
    invoke-static {v2}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 172
    invoke-static {v0, v1, v2, v3}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v0

    iput-object v0, v4, Lgot;->requestHeader:Lgls;

    .line 173
    iget-object v0, p1, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p1, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v0, v0, Lgit;->a:Ljava/lang/String;

    iput-object v0, v4, Lgot;->sessionId:Ljava/lang/String;

    .line 175
    :cond_0
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 176
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 177
    invoke-interface {v0}, Lfhg;->c()Lfhi;

    move-result-object v1

    .line 178
    iget-object v7, p0, Lfdw;->b:Landroid/content/Context;

    const-string v0, "media_session_log_request_key"

    .line 179
    if-eqz v1, :cond_1

    iget-object v1, v1, Lfhi;->a:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lfdw;->b:Landroid/content/Context;

    .line 180
    invoke-static {v2}, Lfmd;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_sessions/log"

    const/4 v5, 0x1

    iget-object v6, p0, Lfdw;->b:Landroid/content/Context;

    .line 181
    invoke-static {v6}, Lfmd;->l(Landroid/content/Context;)I

    move-result v6

    .line 182
    invoke-static/range {v0 .. v6}, Lfic;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 183
    invoke-static {v7, v0}, Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;->a(Landroid/content/Context;Landroid/os/PersistableBundle;)V

    .line 184
    return-void

    .line 179
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 238
    invoke-virtual {p0}, Lfdw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    return-void
.end method

.method final a()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 29
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 30
    if-eqz v0, :cond_2

    .line 33
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 34
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 35
    invoke-interface {v0}, Lfhg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v4, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhk;

    .line 36
    iget-boolean v0, v0, Lfhk;->a:Z

    if-eqz v0, :cond_0

    move v4, v3

    .line 37
    goto :goto_0

    :cond_0
    move v1, v3

    .line 39
    goto :goto_0

    .line 40
    :cond_1
    if-eqz v4, :cond_2

    if-nez v1, :cond_2

    move v2, v3

    .line 41
    :cond_2
    return v2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 193
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 194
    const-string v0, "onFirstAudioPacket, setActive"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 195
    invoke-direct {p0}, Lfdw;->d()V

    .line 196
    :cond_0
    return-void
.end method

.method public final b(Lfhk;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 113
    .line 115
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 116
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 120
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 121
    invoke-interface {v0}, Lfhg;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhk;

    .line 123
    iget-boolean v4, v0, Lfhk;->a:Z

    if-nez v4, :cond_3

    iget-wide v4, v0, Lfhk;->b:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    .line 124
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 125
    goto :goto_0

    :cond_0
    move v1, v2

    .line 128
    :cond_1
    const-string v0, "onParticipantRemoved, isLocal: %b, connectedRemoteEndpointCount: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-boolean v4, p1, Lfhk;->a:Z

    .line 129
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    .line 130
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 133
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 134
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 135
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 136
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 137
    const/16 v1, 0x3d

    const/16 v2, 0xe5

    .line 138
    invoke-interface {v0, v1, v2}, Lfhg;->a(II)V

    .line 139
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected final c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213
    const-string v3, "sendInvite, sending invite to: "

    iget-object v0, p0, Lfdw;->e:Lffd;

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 214
    iget-boolean v0, p0, Lfdw;->d:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 215
    iput-boolean v1, p0, Lfdw;->d:Z

    .line 216
    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 217
    iget-object v0, v0, Lfds;->e:Lfhg;

    .line 219
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v1

    .line 220
    iget-object v1, v1, Lfdd;->f:Ljava/lang/String;

    .line 221
    iget-object v3, p0, Lfdw;->e:Lffd;

    .line 222
    invoke-virtual {v3}, Lffd;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfdw;->e:Lffd;

    .line 223
    invoke-virtual {v4}, Lffd;->b()Z

    move-result v4

    .line 224
    invoke-interface {v0, v1, v3, v4, v2}, Lfhg;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 225
    return-void

    .line 213
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 214
    goto :goto_1
.end method

.method public final c(Lfhk;)V
    .locals 5

    .prologue
    const/16 v4, 0x15

    const/4 v3, 0x3

    const/4 v1, 0x1

    .line 140
    const-string v0, "onParticipantChanged"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 141
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 142
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    .line 143
    iget-object v0, v0, Lfdd;->d:Lffd;

    .line 144
    if-eqz v0, :cond_2

    .line 145
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    .line 146
    iget-object v0, v0, Lfdd;->d:Lffd;

    .line 147
    invoke-virtual {v0}, Lffd;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 148
    iget v0, p1, Lfhk;->c:I

    .line 149
    const/16 v2, 0x14

    if-eq v0, v2, :cond_0

    const/16 v2, 0x16

    if-eq v0, v2, :cond_0

    if-ne v0, v4, :cond_3

    :cond_0
    move v0, v1

    .line 150
    :goto_0
    if-eqz v0, :cond_4

    .line 151
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 152
    const-string v0, "onParticipantChanged, setDialing"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 153
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->setDialing()V

    .line 154
    :cond_1
    iget v0, p1, Lfhk;->c:I

    .line 155
    if-ne v0, v4, :cond_2

    .line 156
    const-string v0, "onParticipantChanged, requesting local ringing"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 157
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfdd;->setRingbackRequested(Z)V

    .line 164
    :cond_2
    :goto_1
    return-void

    .line 149
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :cond_4
    iget v0, p1, Lfhk;->c:I

    .line 159
    if-ne v0, v1, :cond_2

    .line 161
    invoke-direct {p0}, Lfdw;->e()Lfdd;

    move-result-object v0

    invoke-virtual {v0}, Lfdd;->getState()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 162
    const-string v0, "onParticipantChanged, setActive"

    invoke-virtual {p0, v0}, Lfdw;->a(Ljava/lang/String;)V

    .line 163
    invoke-direct {p0}, Lfdw;->d()V

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lfdw;->a:Lfds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfds;

    .line 230
    iget-object v0, v0, Lfds;->d:Lfdd;

    .line 231
    if-nez v0, :cond_1

    .line 232
    :cond_0
    iget v0, p0, Lfdw;->c:I

    const/16 v1, 0x21

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HangoutsCallbacks<"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    :goto_0
    return-object v0

    .line 233
    :cond_1
    iget v0, p0, Lfdw;->c:I

    iget-object v1, p0, Lfdw;->a:Lfds;

    .line 235
    iget-object v1, v1, Lfds;->d:Lfdd;

    .line 236
    invoke-virtual {v1}, Lfdd;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HangoutsCallbacks<"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
