.class public final Lhvl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Date;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 7

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 10
    :goto_0
    iput v0, p0, Lhvl;->b:I

    .line 11
    iget v1, p0, Lhvl;->b:I

    .line 12
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v2, "GMT+0"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 13
    add-int/lit8 v2, p2, -0x1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 14
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 15
    const/high16 v1, -0x80000000

    if-eq p7, v1, :cond_0

    .line 16
    div-int/lit8 v1, p7, 0x64

    mul-int/lit8 v1, v1, 0x3c

    rem-int/lit8 v2, p7, 0x64

    add-int/2addr v1, v2

    .line 17
    const/16 v2, 0xc

    mul-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 18
    :cond_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 19
    iput-object v0, p0, Lhvl;->a:Ljava/util/Date;

    .line 20
    iput p2, p0, Lhvl;->c:I

    .line 21
    iput p3, p0, Lhvl;->d:I

    .line 22
    iput p4, p0, Lhvl;->e:I

    .line 23
    iput p5, p0, Lhvl;->f:I

    .line 24
    iput p6, p0, Lhvl;->g:I

    .line 25
    iput p7, p0, Lhvl;->h:I

    .line 26
    return-void

    .line 5
    :pswitch_0
    if-ltz v0, :cond_1

    const/16 v1, 0x32

    if-ge v0, v1, :cond_1

    .line 6
    add-int/lit16 v0, v0, 0x7d0

    goto :goto_0

    .line 7
    :cond_1
    add-int/lit16 v0, v0, 0x76c

    goto :goto_0

    .line 8
    :pswitch_1
    add-int/lit16 v0, v0, 0x76c

    goto :goto_0

    .line 4
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51
    if-ne p0, p1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 54
    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 56
    goto :goto_0

    .line 57
    :cond_3
    check-cast p1, Lhvl;

    .line 58
    iget-object v2, p0, Lhvl;->a:Ljava/util/Date;

    if-nez v2, :cond_4

    .line 59
    iget-object v2, p1, Lhvl;->a:Ljava/util/Date;

    if-eqz v2, :cond_5

    move v0, v1

    .line 60
    goto :goto_0

    .line 61
    :cond_4
    iget-object v2, p0, Lhvl;->a:Ljava/util/Date;

    iget-object v3, p1, Lhvl;->a:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 62
    goto :goto_0

    .line 63
    :cond_5
    iget v2, p0, Lhvl;->d:I

    iget v3, p1, Lhvl;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 64
    goto :goto_0

    .line 65
    :cond_6
    iget v2, p0, Lhvl;->e:I

    iget v3, p1, Lhvl;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 66
    goto :goto_0

    .line 67
    :cond_7
    iget v2, p0, Lhvl;->f:I

    iget v3, p1, Lhvl;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 68
    goto :goto_0

    .line 69
    :cond_8
    iget v2, p0, Lhvl;->c:I

    iget v3, p1, Lhvl;->c:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 70
    goto :goto_0

    .line 71
    :cond_9
    iget v2, p0, Lhvl;->g:I

    iget v3, p1, Lhvl;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 72
    goto :goto_0

    .line 73
    :cond_a
    iget v2, p0, Lhvl;->h:I

    iget v3, p1, Lhvl;->h:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 74
    goto :goto_0

    .line 75
    :cond_b
    iget v2, p0, Lhvl;->b:I

    iget v3, p1, Lhvl;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 76
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lhvl;->a:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 43
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->d:I

    add-int/2addr v0, v1

    .line 44
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->e:I

    add-int/2addr v0, v1

    .line 45
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->f:I

    add-int/2addr v0, v1

    .line 46
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->c:I

    add-int/2addr v0, v1

    .line 47
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->g:I

    add-int/2addr v0, v1

    .line 48
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->h:I

    add-int/2addr v0, v1

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhvl;->b:I

    add-int/2addr v0, v1

    .line 50
    return v0

    .line 42
    :cond_0
    iget-object v0, p0, Lhvl;->a:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    iget v1, p0, Lhvl;->b:I

    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    iget v1, p0, Lhvl;->c:I

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 32
    iget v1, p0, Lhvl;->d:I

    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 34
    iget v1, p0, Lhvl;->e:I

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 36
    iget v1, p0, Lhvl;->f:I

    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 38
    iget v1, p0, Lhvl;->g:I

    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 40
    iget v1, p0, Lhvl;->h:I

    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
