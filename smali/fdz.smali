.class public final Lfdz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfee;
.implements Lfeh;
.implements Lffu;


# instance fields
.field public a:Lfdd;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/Runnable;

.field private d:Lfft;

.field private e:Lfeg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfdd;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfdz;->b:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfdz;->a:Lfdd;

    .line 4
    iget-object v0, p0, Lfdz;->d:Lfft;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Lfft;

    invoke-direct {v0, p1}, Lfft;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfdz;->d:Lfft;

    .line 6
    iget-object v0, p0, Lfdz;->d:Lfft;

    invoke-virtual {v0, p0}, Lfft;->a(Lffu;)V

    .line 7
    :cond_0
    iget-object v0, p0, Lfdz;->e:Lfeg;

    if-nez v0, :cond_1

    .line 8
    new-instance v0, Lfeg;

    invoke-direct {v0, p1}, Lfeg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfdz;->e:Lfeg;

    .line 9
    iget-object v0, p0, Lfdz;->e:Lfeg;

    invoke-virtual {v0, p0}, Lfeg;->a(Lfeh;)V

    .line 10
    :cond_1
    return-void
.end method

.method private final a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 83
    if-nez p1, :cond_1

    .line 84
    iget-object v0, p0, Lfdz;->a:Lfdd;

    .line 85
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 86
    const/4 v1, 0x1

    iput-boolean v1, v0, Lfef;->b:Z

    .line 87
    iget-object v0, p0, Lfdz;->a:Lfdd;

    .line 88
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 89
    iget v0, v0, Lfef;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lfdz;->a:Lfdd;

    const/16 v1, 0xb57

    invoke-virtual {v0, v1}, Lfdd;->a(I)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lfdz;->a:Lfdd;

    .line 92
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 93
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lfef;->c:I

    .line 94
    iget-object v0, p0, Lfdz;->a:Lfdd;

    .line 95
    iget-object v0, v0, Lfdd;->a:Lfef;

    .line 96
    iget-boolean v0, v0, Lfef;->b:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lfdz;->a:Lfdd;

    const/16 v1, 0xb56

    invoke-virtual {v0, v1}, Lfdd;->a(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 11
    .line 12
    const-string v0, "HutchSwitchPolicy.startDisconnectTimerIfNeeded"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    invoke-virtual {p0}, Lfdz;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfdz;->c:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 14
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->t(Landroid/content/Context;)J

    move-result-wide v0

    .line 15
    new-instance v2, Lfea;

    invoke-direct {v2, p0}, Lfea;-><init>(Lfdz;)V

    iput-object v2, p0, Lfdz;->c:Ljava/lang/Runnable;

    .line 16
    iget-object v2, p0, Lfdz;->c:Ljava/lang/Runnable;

    invoke-static {v2, v0, v1}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 17
    :cond_0
    invoke-virtual {p0}, Lfdz;->c()V

    .line 18
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lfdz;->f()V

    .line 112
    return-void
.end method

.method public final a(Lffy;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lfdz;->f()V

    .line 110
    return-void
.end method

.method public final a(Lgiq;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 61
    iget-object v0, p0, Lfdz;->a:Lfdd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HutchSwitchPolicy.updateStatusHints, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 64
    const v2, 0x7f0200ea

    .line 65
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    const-string v3, "phone"

    .line 66
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 67
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v3}, Lfdz;->a(Ljava/lang/Integer;)V

    .line 69
    invoke-static {v0}, Lfmd;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    const v0, 0x7f1102fa

    .line 72
    :goto_0
    iget-object v3, p0, Lfdz;->b:Landroid/content/Context;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v5}, Lfmd;->G(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    .line 78
    :goto_1
    new-instance v2, Landroid/telecom/StatusHints;

    iget-object v3, p0, Lfdz;->b:Landroid/content/Context;

    .line 79
    invoke-static {v3, v0}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v0

    invoke-direct {v2, v1, v0, v6}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lfdz;->a:Lfdd;

    invoke-virtual {v0}, Lfdd;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/telecom/StatusHints;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lfdz;->a:Lfdd;

    invoke-virtual {v0, v2}, Lfdd;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 82
    :cond_0
    return-void

    .line 71
    :cond_1
    const v0, 0x7f1101e4

    goto :goto_0

    .line 74
    :cond_2
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 76
    const v0, 0x7f0200eb

    .line 77
    :goto_2
    invoke-direct {p0, v6}, Lfdz;->a(Ljava/lang/Integer;)V

    move-object v1, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lfdz;->d:Lfft;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lfdz;->d:Lfft;

    invoke-virtual {v0}, Lfft;->a()V

    .line 101
    iput-object v1, p0, Lfdz;->d:Lfft;

    .line 102
    :cond_0
    iget-object v0, p0, Lfdz;->e:Lfeg;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lfdz;->e:Lfeg;

    invoke-virtual {v0}, Lfeg;->a()V

    .line 104
    iput-object v1, p0, Lfdz;->e:Lfeg;

    .line 105
    :cond_1
    iget-object v0, p0, Lfdz;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 106
    iget-object v0, p0, Lfdz;->c:Ljava/lang/Runnable;

    invoke-static {v0}, Lhcw;->b(Ljava/lang/Runnable;)V

    .line 107
    iput-object v1, p0, Lfdz;->c:Ljava/lang/Runnable;

    .line 108
    :cond_2
    return-void
.end method

.method final e()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 19
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->h(Landroid/content/Context;)Z

    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    const-string v0, "HutchSwitchPolicy.isConnectedToInternetViaNetworkUsableForCalling not connected"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :cond_0
    :goto_0
    return v2

    .line 23
    :cond_1
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    const-string v0, "HutchSwitchPolicy.isConnectedToInternetViaNetworkUsableForCalling connected over wifi"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v1

    .line 25
    goto :goto_0

    .line 26
    :cond_2
    iget-object v0, p0, Lfdz;->b:Landroid/content/Context;

    const-string v3, "phone"

    .line 27
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 28
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v3

    .line 29
    iget-object v4, p0, Lfdz;->b:Landroid/content/Context;

    iget-object v0, p0, Lfdz;->a:Lfdd;

    .line 31
    iget-object v0, v0, Lfdd;->j:Lffb;

    .line 33
    iget-object v0, v0, Lffb;->a:Ljava/lang/String;

    .line 34
    if-eqz v0, :cond_9

    .line 35
    const-string v5, "310260"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 36
    const/4 v0, 0x2

    .line 46
    :goto_1
    iget-object v5, p0, Lfdz;->a:Lfdd;

    .line 48
    iget-object v5, v5, Lfdd;->d:Lffd;

    .line 49
    invoke-virtual {v5}, Lffd;->d()Z

    move-result v5

    .line 51
    const/16 v6, 0xd

    if-ne v3, v6, :cond_3

    .line 52
    invoke-static {v4, v0, v5}, Lfmd;->a(Landroid/content/Context;IZ)Z

    move-result v6

    if-nez v6, :cond_4

    .line 53
    :cond_3
    invoke-static {v3}, Lfmd;->d(I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 54
    invoke-static {v4, v0, v5}, Lfmd;->b(Landroid/content/Context;IZ)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_4
    move v0, v1

    .line 55
    :goto_2
    if-eqz v0, :cond_0

    .line 56
    const/16 v0, 0x79

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "HutchSwitchPolicy.isConnectedToInternetViaNetworkUsableForCalling connected over cellular data, networkType = "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v1

    .line 57
    goto :goto_0

    .line 37
    :cond_5
    const-string v5, "310120"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    move v0, v1

    .line 38
    goto :goto_1

    .line 39
    :cond_6
    const-string v5, "311580"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 40
    const/4 v0, 0x3

    goto :goto_1

    .line 41
    :cond_7
    const-string v5, "23420"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 42
    const/4 v0, 0x4

    goto :goto_1

    .line 43
    :cond_8
    const-string v5, "45403"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 44
    const/4 v0, 0x5

    goto :goto_1

    :cond_9
    move v0, v2

    .line 45
    goto :goto_1

    :cond_a
    move v0, v2

    .line 54
    goto :goto_2
.end method
