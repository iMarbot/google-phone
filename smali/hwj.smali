.class public final Lhwj;
.super Lhvo;
.source "PG"

# interfaces
.implements Lhvm;


# static fields
.field public static final a:Lhvk;


# instance fields
.field private b:Z

.field private c:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lhwk;

    invoke-direct {v0}, Lhwk;-><init>()V

    sput-object v0, Lhwj;->a:Lhvk;

    return-void
.end method

.method constructor <init>(Lhxx;Lhvg;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lhvo;-><init>(Lhxx;Lhvg;)V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwj;->b:Z

    .line 3
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Date;
    .locals 10

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 4
    iget-boolean v3, p0, Lhwj;->b:Z

    if-nez v3, :cond_0

    .line 6
    invoke-virtual {p0}, Lhwj;->c()Ljava/lang/String;

    move-result-object v3

    .line 7
    :try_start_0
    new-instance v9, Lhwv;

    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v9, v4}, Lhwv;-><init>(Ljava/io/Reader;)V

    .line 9
    iget v3, v9, Lhwv;->b:I

    if-ne v3, v0, :cond_1

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 33
    iget-object v3, v9, Lhwv;->d:[I

    const/4 v4, 0x1

    iget v5, v9, Lhwv;->c:I

    aput v5, v3, v4

    .line 36
    :goto_1
    const/16 v3, 0x2e

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    move-result-object v3

    .line 37
    invoke-static {v3}, Lhwv;->a(Lhxc;)I

    move-result v4

    .line 40
    iget v3, v9, Lhwv;->b:I

    if-ne v3, v0, :cond_3

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v3

    :goto_2
    packed-switch v3, :pswitch_data_1

    .line 65
    iget-object v0, v9, Lhwv;->d:[I

    const/4 v1, 0x3

    iget v2, v9, Lhwv;->c:I

    aput v2, v0, v1

    .line 66
    const/4 v0, -0x1

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 67
    new-instance v0, Lhxa;

    invoke-direct {v0}, Lhxa;-><init>()V

    throw v0
    :try_end_0
    .catch Lhxa; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lhxd; {:try_start_0 .. :try_end_0} :catch_1

    .line 171
    :catch_0
    move-exception v0

    .line 174
    :goto_3
    iput-boolean v8, p0, Lhwj;->b:Z

    .line 175
    :cond_0
    iget-object v0, p0, Lhwj;->c:Ljava/util/Date;

    return-object v0

    .line 9
    :cond_1
    :try_start_1
    iget v3, v9, Lhwv;->b:I

    goto :goto_0

    .line 11
    :pswitch_0
    iget v3, v9, Lhwv;->b:I

    if-ne v3, v0, :cond_2

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v3

    :goto_4
    packed-switch v3, :pswitch_data_2

    .line 26
    iget-object v0, v9, Lhwv;->d:[I

    const/4 v1, 0x2

    iget v2, v9, Lhwv;->c:I

    aput v2, v0, v1

    .line 27
    const/4 v0, -0x1

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 28
    new-instance v0, Lhxa;

    invoke-direct {v0}, Lhxa;-><init>()V

    throw v0
    :try_end_1
    .catch Lhxa; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lhxd; {:try_start_1 .. :try_end_1} :catch_1

    .line 172
    :catch_1
    move-exception v0

    .line 173
    new-instance v1, Lhxa;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lhxa;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 11
    :cond_2
    :try_start_2
    iget v3, v9, Lhwv;->b:I

    goto :goto_4

    .line 12
    :pswitch_1
    const/4 v3, 0x4

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    .line 29
    :goto_5
    iget-object v3, v9, Lhwv;->a:Lhxc;

    iget-object v3, v3, Lhxc;->d:Ljava/lang/String;

    .line 31
    const/4 v3, 0x3

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_1

    .line 14
    :pswitch_2
    const/4 v3, 0x5

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 16
    :pswitch_3
    const/4 v3, 0x6

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 18
    :pswitch_4
    const/4 v3, 0x7

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 20
    :pswitch_5
    const/16 v3, 0x8

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 22
    :pswitch_6
    const/16 v3, 0x9

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 24
    :pswitch_7
    const/16 v3, 0xa

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_5

    .line 40
    :cond_3
    iget v3, v9, Lhwv;->b:I

    goto :goto_2

    .line 41
    :pswitch_8
    const/16 v2, 0xb

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    move v2, v8

    .line 70
    :goto_6
    const/16 v3, 0x2e

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    move-result-object v3

    .line 71
    iget-object v3, v3, Lhxc;->d:Ljava/lang/String;

    .line 73
    new-instance v5, Lhww;

    invoke-direct {v5, v3, v2, v4}, Lhww;-><init>(Ljava/lang/String;II)V

    .line 78
    const/16 v2, 0x2e

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    move-result-object v2

    .line 79
    invoke-static {v2}, Lhwv;->a(Lhxc;)I

    move-result v4

    .line 81
    const/16 v2, 0x17

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 83
    const/16 v2, 0x2e

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    move-result-object v2

    .line 84
    invoke-static {v2}, Lhwv;->a(Lhxc;)I

    move-result v6

    .line 86
    iget v2, v9, Lhwv;->b:I

    if-ne v2, v0, :cond_4

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v2

    :goto_7
    packed-switch v2, :pswitch_data_3

    .line 93
    iget-object v2, v9, Lhwv;->d:[I

    const/4 v3, 0x4

    iget v7, v9, Lhwv;->c:I

    aput v7, v2, v3

    move v3, v1

    .line 95
    :goto_8
    iget v2, v9, Lhwv;->b:I

    if-ne v2, v0, :cond_5

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v2

    :goto_9
    packed-switch v2, :pswitch_data_4

    .line 141
    iget-object v0, v9, Lhwv;->d:[I

    const/4 v1, 0x5

    iget v2, v9, Lhwv;->c:I

    aput v2, v0, v1

    .line 142
    const/4 v0, -0x1

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 143
    new-instance v0, Lhxa;

    invoke-direct {v0}, Lhxa;-><init>()V

    throw v0

    .line 43
    :pswitch_9
    const/16 v2, 0xc

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 44
    const/4 v2, 0x2

    goto :goto_6

    .line 45
    :pswitch_a
    const/16 v3, 0xd

    invoke-virtual {v9, v3}, Lhwv;->a(I)Lhxc;

    goto :goto_6

    .line 47
    :pswitch_b
    const/16 v2, 0xe

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 48
    const/4 v2, 0x4

    goto :goto_6

    .line 49
    :pswitch_c
    const/16 v2, 0xf

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 50
    const/4 v2, 0x5

    goto :goto_6

    .line 51
    :pswitch_d
    const/16 v2, 0x10

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 52
    const/4 v2, 0x6

    goto :goto_6

    .line 53
    :pswitch_e
    const/16 v2, 0x11

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 54
    const/4 v2, 0x7

    goto :goto_6

    .line 55
    :pswitch_f
    const/16 v2, 0x12

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 56
    const/16 v2, 0x8

    goto/16 :goto_6

    .line 57
    :pswitch_10
    const/16 v2, 0x13

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 58
    const/16 v2, 0x9

    goto/16 :goto_6

    .line 59
    :pswitch_11
    const/16 v2, 0x14

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 60
    const/16 v2, 0xa

    goto/16 :goto_6

    .line 61
    :pswitch_12
    const/16 v2, 0x15

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 62
    const/16 v2, 0xb

    goto/16 :goto_6

    .line 63
    :pswitch_13
    const/16 v2, 0x16

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 64
    const/16 v2, 0xc

    goto/16 :goto_6

    .line 86
    :cond_4
    iget v2, v9, Lhwv;->b:I

    goto :goto_7

    .line 87
    :pswitch_14
    const/16 v2, 0x17

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    .line 89
    const/16 v2, 0x2e

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    move-result-object v2

    .line 90
    invoke-static {v2}, Lhwv;->a(Lhxc;)I

    move-result v2

    move v3, v2

    .line 92
    goto/16 :goto_8

    .line 95
    :cond_5
    iget v2, v9, Lhwv;->b:I

    goto/16 :goto_9

    .line 96
    :pswitch_15
    const/16 v1, 0x18

    invoke-virtual {v9, v1}, Lhwv;->a(I)Lhxc;

    move-result-object v1

    .line 97
    const/16 v2, 0x2e

    invoke-virtual {v9, v2}, Lhwv;->a(I)Lhxc;

    move-result-object v2

    .line 98
    invoke-static {v2}, Lhwv;->a(Lhxc;)I

    move-result v2

    iget-object v1, v1, Lhxc;->d:Ljava/lang/String;

    const-string v7, "-"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :goto_a
    mul-int/2addr v0, v2

    .line 146
    :goto_b
    new-instance v7, Lhwx;

    invoke-direct {v7, v4, v6, v3, v0}, Lhwx;-><init>(IIII)V

    .line 148
    new-instance v0, Lhvl;

    .line 149
    iget-object v1, v5, Lhww;->a:Ljava/lang/String;

    .line 151
    iget v2, v5, Lhww;->b:I

    .line 153
    iget v3, v5, Lhww;->c:I

    .line 155
    iget v4, v7, Lhwx;->a:I

    .line 157
    iget v5, v7, Lhwx;->b:I

    .line 159
    iget v6, v7, Lhwx;->c:I

    .line 161
    iget v7, v7, Lhwx;->d:I

    .line 162
    invoke-direct/range {v0 .. v7}, Lhvl;-><init>(Ljava/lang/String;IIIIII)V

    .line 164
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Lhwv;->a(I)Lhxc;

    .line 167
    iget-object v0, v0, Lhvl;->a:Ljava/util/Date;

    .line 168
    iput-object v0, p0, Lhwj;->c:Ljava/util/Date;

    goto/16 :goto_3

    :cond_6
    move v0, v8

    .line 98
    goto :goto_a

    .line 101
    :pswitch_16
    iget v2, v9, Lhwv;->b:I

    if-ne v2, v0, :cond_7

    invoke-virtual {v9}, Lhwv;->a()I

    move-result v0

    :goto_c
    packed-switch v0, :pswitch_data_5

    .line 135
    iget-object v0, v9, Lhwv;->d:[I

    const/4 v1, 0x6

    iget v2, v9, Lhwv;->c:I

    aput v2, v0, v1

    .line 136
    const/4 v0, -0x1

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 137
    new-instance v0, Lhxa;

    invoke-direct {v0}, Lhxa;-><init>()V

    throw v0

    .line 101
    :cond_7
    iget v0, v9, Lhwv;->b:I

    goto :goto_c

    .line 102
    :pswitch_17
    const/16 v0, 0x19

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    move v0, v1

    .line 138
    :goto_d
    mul-int/lit8 v0, v0, 0x64

    .line 140
    goto :goto_b

    .line 105
    :pswitch_18
    const/16 v0, 0x1a

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    move v0, v1

    .line 107
    goto :goto_d

    .line 108
    :pswitch_19
    const/16 v0, 0x1b

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 109
    const/4 v0, -0x5

    .line 110
    goto :goto_d

    .line 111
    :pswitch_1a
    const/16 v0, 0x1c

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 112
    const/4 v0, -0x4

    .line 113
    goto :goto_d

    .line 114
    :pswitch_1b
    const/16 v0, 0x1d

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 115
    const/4 v0, -0x6

    .line 116
    goto :goto_d

    .line 117
    :pswitch_1c
    const/16 v0, 0x1e

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 118
    const/4 v0, -0x5

    .line 119
    goto :goto_d

    .line 120
    :pswitch_1d
    const/16 v0, 0x1f

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 121
    const/4 v0, -0x7

    .line 122
    goto :goto_d

    .line 123
    :pswitch_1e
    const/16 v0, 0x20

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 124
    const/4 v0, -0x6

    .line 125
    goto :goto_d

    .line 126
    :pswitch_1f
    const/16 v0, 0x21

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 127
    const/4 v0, -0x8

    .line 128
    goto :goto_d

    .line 129
    :pswitch_20
    const/16 v0, 0x22

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    .line 130
    const/4 v0, -0x7

    .line 131
    goto :goto_d

    .line 132
    :pswitch_21
    const/16 v0, 0x23

    invoke-virtual {v9, v0}, Lhwv;->a(I)Lhxc;

    move-result-object v0

    .line 133
    iget-object v0, v0, Lhxc;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C
    :try_end_2
    .catch Lhxa; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lhxd; {:try_start_2 .. :try_end_2} :catch_1

    move v0, v1

    .line 134
    goto :goto_d

    .line 9
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 40
    :pswitch_data_1
    .packed-switch 0xb
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    .line 11
    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 86
    :pswitch_data_3
    .packed-switch 0x17
        :pswitch_14
    .end packed-switch

    .line 95
    :pswitch_data_4
    .packed-switch 0x18
        :pswitch_15
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
    .end packed-switch

    .line 101
    :pswitch_data_5
    .packed-switch 0x19
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method
