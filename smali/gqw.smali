.class public final Lgqw;
.super Lhft;
.source "PG"


# instance fields
.field private a:Lgqz;

.field private b:Lgqy;

.field private c:Lgrb;

.field private d:Ljava/lang/Long;

.field private e:Lgra;

.field private f:Lgqx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgqw;->a:Lgqz;

    .line 4
    iput-object v0, p0, Lgqw;->b:Lgqy;

    .line 5
    iput-object v0, p0, Lgqw;->c:Lgrb;

    .line 6
    iput-object v0, p0, Lgqw;->d:Ljava/lang/Long;

    .line 7
    iput-object v0, p0, Lgqw;->e:Lgra;

    .line 8
    iput-object v0, p0, Lgqw;->f:Lgqx;

    .line 9
    iput-object v0, p0, Lgqw;->unknownFieldData:Lhfv;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lgqw;->cachedSize:I

    .line 11
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lgqw;->a:Lgqz;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lgqw;->a:Lgqz;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lgqw;->b:Lgqy;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lgqw;->b:Lgqy;

    .line 32
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lgqw;->c:Lgrb;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lgqw;->c:Lgrb;

    .line 35
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lgqw;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lgqw;->d:Ljava/lang/Long;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    iget-object v1, p0, Lgqw;->e:Lgra;

    if-eqz v1, :cond_4

    .line 40
    const/4 v1, 0x5

    iget-object v2, p0, Lgqw;->e:Lgra;

    .line 41
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_4
    iget-object v1, p0, Lgqw;->f:Lgqx;

    if-eqz v1, :cond_5

    .line 43
    const/4 v1, 0x6

    iget-object v2, p0, Lgqw;->f:Lgqx;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 2

    .prologue
    .line 46
    .line 47
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :sswitch_0
    return-object p0

    .line 52
    :sswitch_1
    iget-object v0, p0, Lgqw;->a:Lgqz;

    if-nez v0, :cond_1

    .line 53
    new-instance v0, Lgqz;

    invoke-direct {v0}, Lgqz;-><init>()V

    iput-object v0, p0, Lgqw;->a:Lgqz;

    .line 54
    :cond_1
    iget-object v0, p0, Lgqw;->a:Lgqz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 56
    :sswitch_2
    iget-object v0, p0, Lgqw;->b:Lgqy;

    if-nez v0, :cond_2

    .line 57
    new-instance v0, Lgqy;

    invoke-direct {v0}, Lgqy;-><init>()V

    iput-object v0, p0, Lgqw;->b:Lgqy;

    .line 58
    :cond_2
    iget-object v0, p0, Lgqw;->b:Lgqy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 60
    :sswitch_3
    iget-object v0, p0, Lgqw;->c:Lgrb;

    if-nez v0, :cond_3

    .line 61
    new-instance v0, Lgrb;

    invoke-direct {v0}, Lgrb;-><init>()V

    iput-object v0, p0, Lgqw;->c:Lgrb;

    .line 62
    :cond_3
    iget-object v0, p0, Lgqw;->c:Lgrb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 65
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 66
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqw;->d:Ljava/lang/Long;

    goto :goto_0

    .line 68
    :sswitch_5
    iget-object v0, p0, Lgqw;->e:Lgra;

    if-nez v0, :cond_4

    .line 69
    new-instance v0, Lgra;

    invoke-direct {v0}, Lgra;-><init>()V

    iput-object v0, p0, Lgqw;->e:Lgra;

    .line 70
    :cond_4
    iget-object v0, p0, Lgqw;->e:Lgra;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 72
    :sswitch_6
    iget-object v0, p0, Lgqw;->f:Lgqx;

    if-nez v0, :cond_5

    .line 73
    new-instance v0, Lgqx;

    invoke-direct {v0}, Lgqx;-><init>()V

    iput-object v0, p0, Lgqw;->f:Lgqx;

    .line 74
    :cond_5
    iget-object v0, p0, Lgqw;->f:Lgqx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 12
    iget-object v0, p0, Lgqw;->a:Lgqz;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v1, p0, Lgqw;->a:Lgqz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lgqw;->b:Lgqy;

    if-eqz v0, :cond_1

    .line 15
    const/4 v0, 0x2

    iget-object v1, p0, Lgqw;->b:Lgqy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_1
    iget-object v0, p0, Lgqw;->c:Lgrb;

    if-eqz v0, :cond_2

    .line 17
    const/4 v0, 0x3

    iget-object v1, p0, Lgqw;->c:Lgrb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_2
    iget-object v0, p0, Lgqw;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 19
    const/4 v0, 0x4

    iget-object v1, p0, Lgqw;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 20
    :cond_3
    iget-object v0, p0, Lgqw;->e:Lgra;

    if-eqz v0, :cond_4

    .line 21
    const/4 v0, 0x5

    iget-object v1, p0, Lgqw;->e:Lgra;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_4
    iget-object v0, p0, Lgqw;->f:Lgqx;

    if-eqz v0, :cond_5

    .line 23
    const/4 v0, 0x6

    iget-object v1, p0, Lgqw;->f:Lgqx;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
