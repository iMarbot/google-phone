.class public Lfjw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lecu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {p1}, Lecu;->a(Landroid/content/Context;)Lecu;

    move-result-object v0

    iput-object v0, p0, Lfjw;->a:Lecu;

    .line 13
    return-void
.end method


# virtual methods
.method public a(Landroid/content/pm/PackageManager;I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1
    iget-object v2, p0, Lfjw;->a:Lecu;

    .line 4
    iget-object v1, v2, Lecu;->a:Landroid/content/Context;

    .line 5
    sget-object v3, Leqk;->a:Leqk;

    invoke-virtual {v3, v1}, Leqk;->a(Landroid/content/Context;)Leqj;

    move-result-object v1

    .line 7
    iget-object v1, v1, Leqj;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3

    .line 8
    if-eqz v3, :cond_0

    array-length v1, v3

    if-nez v1, :cond_1

    .line 9
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Uid is not Google Signed"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_1
    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    invoke-virtual {v2, v5}, Lecu;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10
    :cond_3
    return-void
.end method
