.class public final Lgrx;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lgry;

.field public c:Lgrm;

.field public d:Lgse;

.field private e:Ljava/lang/String;

.field private f:Lgre;

.field private g:Lgrn;

.field private h:Lgsb;

.field private i:Lgro;

.field private j:Lgrr;

.field private k:Lgsc;

.field private l:Lgrs;

.field private m:Lgrp;

.field private n:Lgsd;

.field private o:Lgrl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgrx;->a:Ljava/lang/Integer;

    .line 4
    iput-object v0, p0, Lgrx;->e:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lgrx;->b:Lgry;

    .line 6
    iput-object v0, p0, Lgrx;->f:Lgre;

    .line 7
    iput-object v0, p0, Lgrx;->g:Lgrn;

    .line 8
    iput-object v0, p0, Lgrx;->c:Lgrm;

    .line 9
    iput-object v0, p0, Lgrx;->h:Lgsb;

    .line 10
    iput-object v0, p0, Lgrx;->i:Lgro;

    .line 11
    iput-object v0, p0, Lgrx;->d:Lgse;

    .line 12
    iput-object v0, p0, Lgrx;->j:Lgrr;

    .line 13
    iput-object v0, p0, Lgrx;->k:Lgsc;

    .line 14
    iput-object v0, p0, Lgrx;->l:Lgrs;

    .line 15
    iput-object v0, p0, Lgrx;->m:Lgrp;

    .line 16
    iput-object v0, p0, Lgrx;->n:Lgsd;

    .line 17
    iput-object v0, p0, Lgrx;->o:Lgrl;

    .line 18
    iput-object v0, p0, Lgrx;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgrx;->cachedSize:I

    .line 20
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 54
    iget-object v1, p0, Lgrx;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 55
    const/4 v1, 0x1

    iget-object v2, p0, Lgrx;->a:Ljava/lang/Integer;

    .line 56
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_0
    iget-object v1, p0, Lgrx;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 58
    const/4 v1, 0x2

    iget-object v2, p0, Lgrx;->e:Ljava/lang/String;

    .line 59
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_1
    iget-object v1, p0, Lgrx;->b:Lgry;

    if-eqz v1, :cond_2

    .line 61
    const/4 v1, 0x3

    iget-object v2, p0, Lgrx;->b:Lgry;

    .line 62
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_2
    iget-object v1, p0, Lgrx;->f:Lgre;

    if-eqz v1, :cond_3

    .line 64
    const/4 v1, 0x4

    iget-object v2, p0, Lgrx;->f:Lgre;

    .line 65
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Lgrx;->g:Lgrn;

    if-eqz v1, :cond_4

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lgrx;->g:Lgrn;

    .line 68
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_4
    iget-object v1, p0, Lgrx;->c:Lgrm;

    if-eqz v1, :cond_5

    .line 70
    const/4 v1, 0x6

    iget-object v2, p0, Lgrx;->c:Lgrm;

    .line 71
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_5
    iget-object v1, p0, Lgrx;->h:Lgsb;

    if-eqz v1, :cond_6

    .line 73
    const/4 v1, 0x7

    iget-object v2, p0, Lgrx;->h:Lgsb;

    .line 74
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_6
    iget-object v1, p0, Lgrx;->i:Lgro;

    if-eqz v1, :cond_7

    .line 76
    const/16 v1, 0x8

    iget-object v2, p0, Lgrx;->i:Lgro;

    .line 77
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_7
    iget-object v1, p0, Lgrx;->d:Lgse;

    if-eqz v1, :cond_8

    .line 79
    const/16 v1, 0x9

    iget-object v2, p0, Lgrx;->d:Lgse;

    .line 80
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_8
    iget-object v1, p0, Lgrx;->j:Lgrr;

    if-eqz v1, :cond_9

    .line 82
    const/16 v1, 0xa

    iget-object v2, p0, Lgrx;->j:Lgrr;

    .line 83
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_9
    iget-object v1, p0, Lgrx;->k:Lgsc;

    if-eqz v1, :cond_a

    .line 85
    const/16 v1, 0xb

    iget-object v2, p0, Lgrx;->k:Lgsc;

    .line 86
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_a
    iget-object v1, p0, Lgrx;->l:Lgrs;

    if-eqz v1, :cond_b

    .line 88
    const/16 v1, 0xc

    iget-object v2, p0, Lgrx;->l:Lgrs;

    .line 89
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_b
    iget-object v1, p0, Lgrx;->m:Lgrp;

    if-eqz v1, :cond_c

    .line 91
    const/16 v1, 0xd

    iget-object v2, p0, Lgrx;->m:Lgrp;

    .line 92
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_c
    iget-object v1, p0, Lgrx;->n:Lgsd;

    if-eqz v1, :cond_d

    .line 94
    const/16 v1, 0xe

    iget-object v2, p0, Lgrx;->n:Lgsd;

    .line 95
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_d
    iget-object v1, p0, Lgrx;->o:Lgrl;

    if-eqz v1, :cond_e

    .line 97
    const/16 v1, 0xf

    iget-object v2, p0, Lgrx;->o:Lgrl;

    .line 98
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 100
    .line 101
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 102
    sparse-switch v0, :sswitch_data_0

    .line 104
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    :sswitch_0
    return-object p0

    .line 107
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 108
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgrx;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 110
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgrx;->e:Ljava/lang/String;

    goto :goto_0

    .line 112
    :sswitch_3
    iget-object v0, p0, Lgrx;->b:Lgry;

    if-nez v0, :cond_1

    .line 113
    new-instance v0, Lgry;

    invoke-direct {v0}, Lgry;-><init>()V

    iput-object v0, p0, Lgrx;->b:Lgry;

    .line 114
    :cond_1
    iget-object v0, p0, Lgrx;->b:Lgry;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 116
    :sswitch_4
    iget-object v0, p0, Lgrx;->f:Lgre;

    if-nez v0, :cond_2

    .line 117
    new-instance v0, Lgre;

    invoke-direct {v0}, Lgre;-><init>()V

    iput-object v0, p0, Lgrx;->f:Lgre;

    .line 118
    :cond_2
    iget-object v0, p0, Lgrx;->f:Lgre;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 120
    :sswitch_5
    iget-object v0, p0, Lgrx;->g:Lgrn;

    if-nez v0, :cond_3

    .line 121
    new-instance v0, Lgrn;

    invoke-direct {v0}, Lgrn;-><init>()V

    iput-object v0, p0, Lgrx;->g:Lgrn;

    .line 122
    :cond_3
    iget-object v0, p0, Lgrx;->g:Lgrn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 124
    :sswitch_6
    iget-object v0, p0, Lgrx;->c:Lgrm;

    if-nez v0, :cond_4

    .line 125
    new-instance v0, Lgrm;

    invoke-direct {v0}, Lgrm;-><init>()V

    iput-object v0, p0, Lgrx;->c:Lgrm;

    .line 126
    :cond_4
    iget-object v0, p0, Lgrx;->c:Lgrm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 128
    :sswitch_7
    iget-object v0, p0, Lgrx;->h:Lgsb;

    if-nez v0, :cond_5

    .line 129
    new-instance v0, Lgsb;

    invoke-direct {v0}, Lgsb;-><init>()V

    iput-object v0, p0, Lgrx;->h:Lgsb;

    .line 130
    :cond_5
    iget-object v0, p0, Lgrx;->h:Lgsb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 132
    :sswitch_8
    iget-object v0, p0, Lgrx;->i:Lgro;

    if-nez v0, :cond_6

    .line 133
    new-instance v0, Lgro;

    invoke-direct {v0}, Lgro;-><init>()V

    iput-object v0, p0, Lgrx;->i:Lgro;

    .line 134
    :cond_6
    iget-object v0, p0, Lgrx;->i:Lgro;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 136
    :sswitch_9
    iget-object v0, p0, Lgrx;->d:Lgse;

    if-nez v0, :cond_7

    .line 137
    new-instance v0, Lgse;

    invoke-direct {v0}, Lgse;-><init>()V

    iput-object v0, p0, Lgrx;->d:Lgse;

    .line 138
    :cond_7
    iget-object v0, p0, Lgrx;->d:Lgse;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 140
    :sswitch_a
    iget-object v0, p0, Lgrx;->j:Lgrr;

    if-nez v0, :cond_8

    .line 141
    new-instance v0, Lgrr;

    invoke-direct {v0}, Lgrr;-><init>()V

    iput-object v0, p0, Lgrx;->j:Lgrr;

    .line 142
    :cond_8
    iget-object v0, p0, Lgrx;->j:Lgrr;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 144
    :sswitch_b
    iget-object v0, p0, Lgrx;->k:Lgsc;

    if-nez v0, :cond_9

    .line 145
    new-instance v0, Lgsc;

    invoke-direct {v0}, Lgsc;-><init>()V

    iput-object v0, p0, Lgrx;->k:Lgsc;

    .line 146
    :cond_9
    iget-object v0, p0, Lgrx;->k:Lgsc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 148
    :sswitch_c
    iget-object v0, p0, Lgrx;->l:Lgrs;

    if-nez v0, :cond_a

    .line 149
    new-instance v0, Lgrs;

    invoke-direct {v0}, Lgrs;-><init>()V

    iput-object v0, p0, Lgrx;->l:Lgrs;

    .line 150
    :cond_a
    iget-object v0, p0, Lgrx;->l:Lgrs;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 152
    :sswitch_d
    iget-object v0, p0, Lgrx;->m:Lgrp;

    if-nez v0, :cond_b

    .line 153
    new-instance v0, Lgrp;

    invoke-direct {v0}, Lgrp;-><init>()V

    iput-object v0, p0, Lgrx;->m:Lgrp;

    .line 154
    :cond_b
    iget-object v0, p0, Lgrx;->m:Lgrp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 156
    :sswitch_e
    iget-object v0, p0, Lgrx;->n:Lgsd;

    if-nez v0, :cond_c

    .line 157
    new-instance v0, Lgsd;

    invoke-direct {v0}, Lgsd;-><init>()V

    iput-object v0, p0, Lgrx;->n:Lgsd;

    .line 158
    :cond_c
    iget-object v0, p0, Lgrx;->n:Lgsd;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 160
    :sswitch_f
    iget-object v0, p0, Lgrx;->o:Lgrl;

    if-nez v0, :cond_d

    .line 161
    new-instance v0, Lgrl;

    invoke-direct {v0}, Lgrl;-><init>()V

    iput-object v0, p0, Lgrx;->o:Lgrl;

    .line 162
    :cond_d
    iget-object v0, p0, Lgrx;->o:Lgrl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lgrx;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgrx;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_0
    iget-object v0, p0, Lgrx;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Lgrx;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_1
    iget-object v0, p0, Lgrx;->b:Lgry;

    if-eqz v0, :cond_2

    .line 26
    const/4 v0, 0x3

    iget-object v1, p0, Lgrx;->b:Lgry;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 27
    :cond_2
    iget-object v0, p0, Lgrx;->f:Lgre;

    if-eqz v0, :cond_3

    .line 28
    const/4 v0, 0x4

    iget-object v1, p0, Lgrx;->f:Lgre;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_3
    iget-object v0, p0, Lgrx;->g:Lgrn;

    if-eqz v0, :cond_4

    .line 30
    const/4 v0, 0x5

    iget-object v1, p0, Lgrx;->g:Lgrn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 31
    :cond_4
    iget-object v0, p0, Lgrx;->c:Lgrm;

    if-eqz v0, :cond_5

    .line 32
    const/4 v0, 0x6

    iget-object v1, p0, Lgrx;->c:Lgrm;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_5
    iget-object v0, p0, Lgrx;->h:Lgsb;

    if-eqz v0, :cond_6

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgrx;->h:Lgsb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_6
    iget-object v0, p0, Lgrx;->i:Lgro;

    if-eqz v0, :cond_7

    .line 36
    const/16 v0, 0x8

    iget-object v1, p0, Lgrx;->i:Lgro;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_7
    iget-object v0, p0, Lgrx;->d:Lgse;

    if-eqz v0, :cond_8

    .line 38
    const/16 v0, 0x9

    iget-object v1, p0, Lgrx;->d:Lgse;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 39
    :cond_8
    iget-object v0, p0, Lgrx;->j:Lgrr;

    if-eqz v0, :cond_9

    .line 40
    const/16 v0, 0xa

    iget-object v1, p0, Lgrx;->j:Lgrr;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_9
    iget-object v0, p0, Lgrx;->k:Lgsc;

    if-eqz v0, :cond_a

    .line 42
    const/16 v0, 0xb

    iget-object v1, p0, Lgrx;->k:Lgsc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 43
    :cond_a
    iget-object v0, p0, Lgrx;->l:Lgrs;

    if-eqz v0, :cond_b

    .line 44
    const/16 v0, 0xc

    iget-object v1, p0, Lgrx;->l:Lgrs;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 45
    :cond_b
    iget-object v0, p0, Lgrx;->m:Lgrp;

    if-eqz v0, :cond_c

    .line 46
    const/16 v0, 0xd

    iget-object v1, p0, Lgrx;->m:Lgrp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 47
    :cond_c
    iget-object v0, p0, Lgrx;->n:Lgsd;

    if-eqz v0, :cond_d

    .line 48
    const/16 v0, 0xe

    iget-object v1, p0, Lgrx;->n:Lgsd;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 49
    :cond_d
    iget-object v0, p0, Lgrx;->o:Lgrl;

    if-eqz v0, :cond_e

    .line 50
    const/16 v0, 0xf

    iget-object v1, p0, Lgrx;->o:Lgrl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_e
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 52
    return-void
.end method
