.class public final Lbkd$c;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbkd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/widget/ListAdapter;

.field private c:Ljava/util/List;

.field private d:Lbbj;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 21
    invoke-virtual {p0}, Lbkd$c;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 22
    if-nez v2, :cond_0

    .line 46
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 24
    check-cast v0, Landroid/app/AlertDialog;

    .line 25
    iget-object v1, p0, Lbkd$c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p2, :cond_4

    if-ltz p2, :cond_4

    .line 26
    iget-object v1, p0, Lbkd$c;->c:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbkd$d;

    .line 27
    const v3, 0x7f0e0260

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 28
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29
    iget-object v0, p0, Lbkd$c;->d:Lbbj;

    .line 30
    iget v0, v0, Lbbj;->b:I

    invoke-static {v0}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v0

    .line 31
    if-nez v0, :cond_1

    sget-object v0, Lbbf$a;->a:Lbbf$a;

    .line 32
    :cond_1
    sget-object v3, Lbbf$a;->d:Lbbf$a;

    if-ne v0, v3, :cond_2

    .line 33
    invoke-virtual {p0}, Lbkd$c;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbks$a;->k:Lbks$a;

    .line 34
    invoke-interface {v0, v3}, Lbku;->a(Lbks$a;)V

    .line 35
    :cond_2
    iget-wide v4, v1, Lbkd$d;->a:J

    .line 37
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/android/dialer/interactions/ContactUpdateService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    const-string v3, "phone_number_data_id"

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 41
    invoke-virtual {v2, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 42
    :cond_3
    iget-object v0, v1, Lbkd$d;->b:Ljava/lang/String;

    iget v1, p0, Lbkd$c;->a:I

    iget-boolean v3, p0, Lbkd$c;->e:Z

    iget-object v4, p0, Lbkd$c;->d:Lbbj;

    .line 43
    invoke-static {v2, v0, v1, v3, v4}, Lbkd;->a(Landroid/content/Context;Ljava/lang/String;IZLbbj;)V

    goto :goto_0

    .line 45
    :cond_4
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 3
    invoke-virtual {p0}, Lbkd$c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 4
    instance-of v1, v0, Lbkd$a;

    invoke-static {v1}, Lbdf;->b(Z)V

    .line 5
    invoke-virtual {p0}, Lbkd$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "phoneList"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lbkd$c;->c:Ljava/util/List;

    .line 6
    invoke-virtual {p0}, Lbkd$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "interactionType"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lbkd$c;->a:I

    .line 7
    invoke-virtual {p0}, Lbkd$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "is_video_call"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lbkd$c;->e:Z

    .line 8
    invoke-virtual {p0}, Lbkd$c;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lapw;->a(Landroid/os/Bundle;)Lbbj;

    move-result-object v1

    iput-object v1, p0, Lbkd$c;->d:Lbbj;

    .line 9
    new-instance v1, Lbkd$e;

    iget-object v2, p0, Lbkd$c;->c:Ljava/util/List;

    iget v3, p0, Lbkd$c;->a:I

    invoke-direct {v1, v0, v2, v3}, Lbkd$e;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    iput-object v1, p0, Lbkd$c;->b:Landroid/widget/ListAdapter;

    .line 10
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 11
    const v2, 0x7f0400b5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 12
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lbkd$c;->b:Landroid/widget/ListAdapter;

    .line 13
    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 14
    iget v0, p0, Lbkd$c;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 15
    const v0, 0x7f1102be

    .line 17
    :goto_0
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 18
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 20
    return-object v0

    .line 16
    :cond_0
    const v0, 0x7f110076

    goto :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 48
    invoke-virtual {p0}, Lbkd$c;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    check-cast v0, Lbkd$a;

    invoke-interface {v0}, Lbkd$a;->A()V

    .line 51
    :cond_0
    return-void
.end method
