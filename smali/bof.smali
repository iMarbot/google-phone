.class final Lbof;
.super Landroid/database/MergeCursor;
.source "PG"

# interfaces
.implements Lboc;


# direct methods
.method private constructor <init>([Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 7
    return-void
.end method

.method static a(Landroid/content/Context;Landroid/database/Cursor;)Lbof;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lbof;

    new-array v1, v3, [Landroid/database/Cursor;

    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v3, Lbnz;->a:[Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lbof;-><init>([Landroid/database/Cursor;)V

    .line 5
    :goto_0
    return-object v0

    .line 3
    :cond_0
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v0, Lbof;->a:[Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 4
    new-array v0, v3, [Ljava/lang/String;

    const v2, 0x7f110039

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 5
    new-instance v0, Lbof;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    aput-object v1, v2, v4

    aput-object p1, v2, v3

    invoke-direct {v0, v2}, Lbof;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lbof;->isFirst()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 10
    const-wide/16 v0, 0x0

    return-wide v0
.end method
