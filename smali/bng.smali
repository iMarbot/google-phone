.class public final Lbng;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbnb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final a(Lbne;)V
    .locals 2

    .prologue
    .line 49
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {p1}, Lbne;->a()Lbbh;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbng;->b(Landroid/content/Context;Lbbh;)V

    .line 50
    return-void
.end method

.method public final a(Landroid/content/Context;Lbbh;)Z
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/content/Context;Lbbh;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 3
    .line 4
    iget-boolean v0, p2, Lbbh;->e:Z

    .line 5
    if-nez v0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 7
    :cond_1
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 8
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 9
    invoke-static {v0, p1}, Lapw;->a(Landroid/telephony/TelephonyManager;Landroid/content/Context;)Lava;

    move-result-object v1

    .line 10
    invoke-interface {v1}, Lava;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    iget-object v0, p2, Lbbh;->f:Landroid/os/Bundle;

    .line 14
    const-string v2, "android.telecom.extra.ALLOW_ASSISTED_DIAL"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 16
    iget-object v0, p2, Lbbh;->a:Landroid/net/Uri;

    .line 17
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "tel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19
    iget-object v0, p2, Lbbh;->a:Landroid/net/Uri;

    .line 20
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 23
    :goto_1
    invoke-interface {v1, v0}, Lava;->a(Ljava/lang/String;)Ljava/util/Optional;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Ljava/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    .line 26
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 27
    const-string v3, "TRANSFORMATION_INFO_ORIGINAL_NUMBER"

    invoke-virtual {v0}, Lavk;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v3, "TRANSFORMATION_INFO_TRANSFORMED_NUMBER"

    invoke-virtual {v0}, Lavk;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v3, "TRANSFORMATION_INFO_USER_HOME_COUNTRY_CODE"

    invoke-virtual {v0}, Lavk;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v3, "TRANSFORMATION_INFO_USER_ROAMING_COUNTRY_CODE"

    invoke-virtual {v0}, Lavk;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v3, "TRANSFORMED_NUMBER_COUNTRY_CALLING_CODE"

    .line 32
    invoke-virtual {v0}, Lavk;->e()I

    move-result v0

    .line 33
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    iget-object v0, p2, Lbbh;->f:Landroid/os/Bundle;

    .line 38
    const-string v3, "android.telecom.extra.IS_ASSISTED_DIALED"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 41
    iget-object v0, p2, Lbbh;->f:Landroid/os/Bundle;

    .line 42
    const-string v3, "android.telecom.extra.ASSISTED_DIALING_EXTRAS"

    .line 43
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {v1}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    invoke-virtual {v0}, Lavk;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbib;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p2, Lbbh;->a:Landroid/net/Uri;

    goto/16 :goto_0

    .line 21
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method
