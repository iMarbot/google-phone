.class final Ldun;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ldum;


# direct methods
.method constructor <init>(Ldum;)V
    .locals 0

    iput-object p1, p0, Ldun;->a:Ldum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Ldun;->a:Ldum;

    iget-object v0, v0, Ldum;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldun;->a:Ldum;

    iget-object v0, v0, Ldum;->d:Ldul;

    .line 2
    iget-object v0, v0, Ldul;->b:Landroid/content/Context;

    .line 3
    check-cast v0, Lduo;

    iget-object v1, p0, Ldun;->a:Ldum;

    iget-object v1, v1, Ldum;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lduo;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldun;->a:Ldum;

    iget-object v0, v0, Ldum;->b:Ldue;

    const-string v1, "Local AnalyticsService processed last dispatch request"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    .line 5
    :cond_0
    :goto_0
    return-void

    .line 3
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Ldun;->a:Ldum;

    iget-object v0, v0, Ldum;->b:Ldue;

    const-string v1, "AnalyticsJobService processed last dispatch request"

    invoke-virtual {v0, v1}, Lduy;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldun;->a:Ldum;

    iget-object v0, v0, Ldum;->d:Ldul;

    .line 4
    iget-object v0, v0, Ldul;->b:Landroid/content/Context;

    .line 5
    check-cast v0, Lduo;

    iget-object v1, p0, Ldun;->a:Ldum;

    iget-object v1, v1, Ldum;->c:Landroid/app/job/JobParameters;

    invoke-interface {v0, v1}, Lduo;->a(Landroid/app/job/JobParameters;)V

    goto :goto_0
.end method
