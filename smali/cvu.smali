.class public final enum Lcvu;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lcvu;

.field public static final enum b:Lcvu;

.field public static final enum c:Lcvu;

.field public static final enum d:Lcvu;

.field public static final enum e:Lcvu;

.field public static final enum f:Lcvu;

.field private static synthetic g:[Lcvu;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3
    new-instance v0, Lcvu;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v3}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->a:Lcvu;

    .line 4
    new-instance v0, Lcvu;

    const-string v1, "RESOURCE_CACHE"

    invoke-direct {v0, v1, v4}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->b:Lcvu;

    .line 5
    new-instance v0, Lcvu;

    const-string v1, "DATA_CACHE"

    invoke-direct {v0, v1, v5}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->c:Lcvu;

    .line 6
    new-instance v0, Lcvu;

    const-string v1, "SOURCE"

    invoke-direct {v0, v1, v6}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->d:Lcvu;

    .line 7
    new-instance v0, Lcvu;

    const-string v1, "ENCODE"

    invoke-direct {v0, v1, v7}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->e:Lcvu;

    .line 8
    new-instance v0, Lcvu;

    const-string v1, "FINISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcvu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcvu;->f:Lcvu;

    .line 9
    const/4 v0, 0x6

    new-array v0, v0, [Lcvu;

    sget-object v1, Lcvu;->a:Lcvu;

    aput-object v1, v0, v3

    sget-object v1, Lcvu;->b:Lcvu;

    aput-object v1, v0, v4

    sget-object v1, Lcvu;->c:Lcvu;

    aput-object v1, v0, v5

    sget-object v1, Lcvu;->d:Lcvu;

    aput-object v1, v0, v6

    sget-object v1, Lcvu;->e:Lcvu;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcvu;->f:Lcvu;

    aput-object v2, v0, v1

    sput-object v0, Lcvu;->g:[Lcvu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lcvu;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcvu;->g:[Lcvu;

    invoke-virtual {v0}, [Lcvu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcvu;

    return-object v0
.end method
