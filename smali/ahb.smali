.class public Lahb;
.super Landroid/widget/ListView;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lahb;->a:I

    .line 3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Lahb;->a:I

    .line 6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lahb;->a:I

    .line 9
    return-void
.end method


# virtual methods
.method protected layoutChildren()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 10
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 11
    iget v0, p0, Lahb;->a:I

    if-ne v0, v1, :cond_1

    .line 22
    :cond_0
    :goto_0
    return-void

    .line 13
    :cond_1
    iget v0, p0, Lahb;->a:I

    .line 14
    iput v1, p0, Lahb;->a:I

    .line 15
    invoke-virtual {p0}, Lahb;->getFirstVisiblePosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 16
    invoke-virtual {p0}, Lahb;->getLastVisiblePosition()I

    move-result v2

    .line 17
    if-lt v0, v1, :cond_2

    if-le v0, v2, :cond_0

    .line 19
    :cond_2
    invoke-virtual {p0}, Lahb;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 20
    invoke-virtual {p0, v0, v1}, Lahb;->setSelectionFromTop(II)V

    .line 21
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 23
    invoke-super/range {p0 .. p5}, Landroid/widget/ListView;->onLayout(ZIIII)V

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-ne v0, v1, :cond_1

    .line 25
    :cond_0
    invoke-virtual {p0}, Lahb;->layoutChildren()V

    .line 26
    :cond_1
    return-void
.end method
