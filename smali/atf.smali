.class public Latf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Latf$a;,
        Latf$b;,
        Latf$c;,
        Latf$d;,
        Latf$e;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static f:Latf;

.field public static g:Ljava/util/concurrent/ScheduledExecutorService;


# instance fields
.field public final h:Ljava/util/concurrent/atomic/AtomicInteger;

.field public i:Landroid/content/Context;

.field public j:J

.field public k:Landroid/net/Uri;

.field public l:Landroid/media/MediaPlayer;

.field public m:Lbdi;

.field public n:Landroid/app/Activity;

.field public o:Latf$d;

.field public p:I

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Latf$a;

.field public v:Lasu;

.field public w:Latf$c;

.field public x:Lbdy;

.field private y:Landroid/os/PowerManager$WakeLock;

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 224
    const-class v0, Latf;

    .line 225
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".VOICEMAIL_URI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latf;->a:Ljava/lang/String;

    .line 226
    const-class v0, Latf;

    .line 227
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".IS_PREPARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latf;->b:Ljava/lang/String;

    .line 228
    const-class v0, Latf;

    .line 229
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".IS_PLAYING_STATE_KEY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latf;->c:Ljava/lang/String;

    .line 230
    const-class v0, Latf;

    .line 231
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".CLIP_POSITION_KEY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latf;->d:Ljava/lang/String;

    .line 232
    const-class v0, Latf;

    .line 233
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".IS_SPEAKER_PHONE_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latf;->e:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/16 v2, 0x20

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 4
    invoke-static {}, Lbdj;->a()Lbdi;

    move-result-object v1

    iput-object v1, p0, Latf;->m:Lbdi;

    .line 5
    new-instance v1, Lasu;

    invoke-direct {v1, v0, p0}, Lasu;-><init>(Landroid/content/Context;Latf;)V

    iput-object v1, p0, Latf;->v:Lasu;

    .line 6
    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 7
    invoke-virtual {v0, v2}, Landroid/os/PowerManager;->isWakeLockLevelSupported(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8
    const-string v1, "VoicemailPlaybackPresenter"

    .line 9
    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    .line 10
    :cond_0
    return-void
.end method

.method static a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 218
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    .line 219
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 220
    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 221
    invoke-static {}, Lbmk;->a()[Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 222
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 209
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    :goto_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yy_hhmmaa"

    .line 211
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 212
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p3, p4}, Ljava/util/Date;-><init>(J)V

    .line 214
    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    return-object v0

    :cond_0
    move-object p1, p0

    .line 209
    goto :goto_0

    .line 215
    :cond_1
    const-string v3, "."

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Latf$b;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Latf;->m:Lbdi;

    new-instance v1, Latj;

    invoke-direct {v1, p0, p1}, Latj;-><init>(Latf;Latf$b;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 48
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    const-string v0, "VoicemailPlaybackPresenter.handlerError"

    const-string v1, "could not play voicemail"

    invoke-static {v0, v1, p1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    iget-boolean v0, p0, Latf;->r:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    .line 115
    iput-boolean v2, p0, Latf;->r:Z

    .line 116
    :cond_0
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->c()V

    .line 118
    :cond_1
    iput v2, p0, Latf;->p:I

    .line 119
    iput-boolean v2, p0, Latf;->q:Z

    .line 120
    invoke-virtual {p0, v2}, Latf;->d(Z)V

    .line 121
    return-void
.end method

.method static a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 217
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 223
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "number"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "date"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "mime_type"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "transcription"

    aput-object v1, v2, v0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized c()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 2

    .prologue
    .line 11
    const-class v1, Latf;

    monitor-enter v1

    :try_start_0
    sget-object v0, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_0

    .line 12
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 13
    :cond_0
    sget-object v0, Latf;->g:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 11
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final e(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 171
    iget-object v1, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 179
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v1, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    const-string v1, "VoicemailPlaybackPresenter.disableProximitySensor"

    const-string v2, "releasing proximity wake lock"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 176
    :cond_1
    iget-object v1, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v0}, Landroid/os/PowerManager$WakeLock;->release(I)V

    goto :goto_0

    .line 178
    :cond_2
    const-string v1, "VoicemailPlaybackPresenter.disableProximitySensor"

    const-string v2, "proximity wake lock already released"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    .line 77
    :cond_2
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->h()V

    .line 78
    iput-boolean v1, p0, Latf;->r:Z

    .line 79
    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    if-eqz v0, :cond_3

    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    invoke-static {v0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot play voicemail when call is in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Latf;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 82
    :cond_3
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    .line 83
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 84
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 85
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 86
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 87
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    iget-object v1, p0, Latf;->i:Landroid/content/Context;

    iget-object v2, p0, Latf;->k:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 88
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 89
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-direct {p0, v0}, Latf;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Latf$d;JLandroid/net/Uri;ZLandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 14
    iput-wide p2, p0, Latf;->j:J

    .line 15
    iput-object p1, p0, Latf;->o:Latf$d;

    .line 16
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0, p0, p4}, Latf$d;->a(Latf;Landroid/net/Uri;)V

    .line 17
    iget-object v0, p0, Latf;->o:Latf$d;

    iget-boolean v1, p0, Latf;->s:Z

    invoke-interface {v0, v1}, Latf$d;->a(Z)V

    .line 18
    iput-object p6, p0, Latf;->z:Landroid/view/View;

    .line 19
    invoke-virtual {p0, v2}, Latf;->d(Z)V

    .line 20
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Latf;->r:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Latf;->k:Landroid/net/Uri;

    invoke-virtual {p4, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Latf;->p:I

    .line 22
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {p0, v0}, Latf;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Latf;->d(Z)V

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    iget-object v0, p0, Latf;->k:Landroid/net/Uri;

    invoke-virtual {p4, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 25
    iput-object p4, p0, Latf;->k:Landroid/net/Uri;

    .line 26
    iput v2, p0, Latf;->p:I

    .line 27
    :cond_2
    new-instance v0, Lath;

    invoke-direct {v0, p0, p5}, Lath;-><init>(Latf;Z)V

    invoke-direct {p0, v0}, Latf;->a(Latf$b;)V

    .line 28
    if-eqz p5, :cond_0

    .line 29
    iput-boolean p5, p0, Latf;->q:Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    .line 32
    invoke-virtual {p0, v2}, Latf;->b(Z)V

    .line 33
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    .line 36
    :cond_0
    invoke-direct {p0, v2}, Latf;->e(Z)V

    .line 37
    iput-boolean v2, p0, Latf;->r:Z

    .line 38
    iput-boolean v2, p0, Latf;->q:Z

    .line 39
    if-eqz p1, :cond_1

    .line 40
    iput v2, p0, Latf;->p:I

    .line 41
    :cond_1
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_2

    .line 42
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->b()V

    .line 43
    if-eqz p1, :cond_3

    .line 44
    iget-object v0, p0, Latf;->o:Latf$d;

    iget-object v1, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-interface {v0, v2, v1}, Latf$d;->a(II)V

    .line 46
    :cond_2
    :goto_0
    return-void

    .line 45
    :cond_3
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->g()I

    move-result v0

    iput v0, p0, Latf;->p:I

    goto :goto_0
.end method

.method protected final a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Latf;->i:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Latf;->k:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    new-instance v1, Latf$a;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iget-object v3, p0, Latf;->k:Landroid/net/Uri;

    invoke-direct {v1, p0, v2, v3}, Latf$a;-><init>(Latf;Landroid/os/Handler;Landroid/net/Uri;)V

    .line 66
    iget-object v2, p0, Latf;->u:Latf$a;

    if-eqz v2, :cond_2

    .line 67
    iget-object v2, p0, Latf;->u:Latf$a;

    invoke-virtual {v2}, Latf$a;->a()V

    .line 68
    :cond_2
    iget-object v2, p0, Latf;->o:Latf$d;

    invoke-interface {v2}, Latf$d;->e()V

    .line 69
    iput-object v1, p0, Latf;->u:Latf$a;

    .line 70
    iget-object v1, p0, Latf;->m:Lbdi;

    new-instance v2, Latk;

    invoke-direct {v2, p0}, Latk;-><init>(Latf;)V

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final a(Landroid/net/Uri;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 49
    if-eqz p1, :cond_0

    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v7

    .line 51
    :cond_1
    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 52
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_4

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    const-string v0, "duration"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 55
    iget-object v2, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    if-lez v0, :cond_2

    mul-int/lit16 v0, v0, 0x3e8

    :goto_1
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 56
    const-string v0, "has_content"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v6

    .line 57
    :goto_2
    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    move v7, v0

    .line 58
    goto :goto_0

    :cond_2
    move v0, v7

    .line 55
    goto :goto_1

    :cond_3
    move v0, v7

    .line 56
    goto :goto_2

    .line 59
    :cond_4
    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 129
    iget-object v0, p0, Latf;->o:Latf$d;

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-boolean v0, p0, Latf;->r:Z

    if-nez v0, :cond_1

    .line 132
    new-instance v0, Lati;

    invoke-direct {v0, p0}, Lati;-><init>(Latf;)V

    invoke-direct {p0, v0}, Latf;->a(Latf$b;)V

    goto :goto_0

    .line 134
    :cond_1
    iput-boolean v4, p0, Latf;->q:Z

    .line 135
    iget-object v0, p0, Latf;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 136
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    iget v0, p0, Latf;->p:I

    iget-object v1, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Latf;->p:I

    .line 138
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    iget v1, p0, Latf;->p:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 139
    :try_start_0
    iget-object v0, p0, Latf;->v:Lasu;

    .line 140
    iget-object v1, v0, Lasu;->a:Landroid/media/AudioManager;

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 141
    invoke-virtual {v1, v0, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 142
    if-eq v1, v4, :cond_3

    .line 143
    new-instance v0, Ljava/util/concurrent/RejectedExecutionException;

    const-string v1, "Could not capture audio focus."

    invoke-direct {v0, v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    invoke-direct {p0, v0}, Latf;->a(Ljava/lang/Exception;)V

    .line 151
    :cond_2
    :goto_1
    new-array v0, v4, [Ljava/lang/Object;

    iget v1, p0, Latf;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 152
    iget-object v0, p0, Latf;->o:Latf$d;

    iget-object v1, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {}, Latf;->c()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Latf$d;->a(ILjava/util/concurrent/ScheduledExecutorService;)V

    goto :goto_0

    .line 144
    :cond_3
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v0, v1}, Lasu;->b(Z)V

    .line 145
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 146
    iget-boolean v0, p0, Latf;->s:Z

    invoke-virtual {p0, v0}, Latf;->c(Z)V

    .line 147
    iget-object v0, p0, Latf;->v:Lasu;

    iget-boolean v1, p0, Latf;->s:Z

    invoke-virtual {v0, v1}, Lasu;->a(Z)V
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 154
    iget-boolean v0, p0, Latf;->r:Z

    if-nez v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 156
    :cond_0
    iput-boolean v1, p0, Latf;->q:Z

    .line 157
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 159
    :cond_1
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput v0, p0, Latf;->p:I

    .line 160
    new-array v0, v3, [Ljava/lang/Object;

    iget v2, p0, Latf;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 161
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->b()V

    .line 163
    :cond_2
    if-nez p1, :cond_3

    .line 164
    iget-object v0, p0, Latf;->v:Lasu;

    .line 165
    invoke-virtual {v0, v1}, Lasu;->b(Z)V

    .line 166
    iget-object v1, v0, Lasu;->a:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 167
    :cond_3
    iget-object v0, p0, Latf;->n:Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 168
    iget-object v0, p0, Latf;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 169
    :cond_4
    invoke-direct {p0, v3}, Latf;->e(Z)V

    goto :goto_0

    .line 159
    :cond_5
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 180
    iget-object v0, p0, Latf;->o:Latf$d;

    if-nez v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0, p1}, Latf$d;->a(Z)V

    .line 183
    iput-boolean p1, p0, Latf;->s:Z

    .line 184
    iget-boolean v0, p0, Latf;->q:Z

    if-eqz v0, :cond_0

    .line 185
    if-nez p1, :cond_2

    iget-object v0, p0, Latf;->v:Lasu;

    .line 186
    iget-object v0, v0, Lasu;->b:Latm;

    .line 187
    iget-boolean v0, v0, Latm;->c:Z

    .line 188
    if-eqz v0, :cond_3

    .line 189
    :cond_2
    invoke-direct {p0, v2}, Latf;->e(Z)V

    goto :goto_0

    .line 191
    :cond_3
    iget-object v0, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Latf;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Latf;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    .line 192
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_4

    .line 195
    const-string v0, "VoicemailPlaybackPresenter.enableProximitySensor"

    const-string v1, "acquiring proximity wake lock"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-object v0, p0, Latf;->y:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0

    .line 197
    :cond_4
    const-string v0, "VoicemailPlaybackPresenter.enableProximitySensor"

    const-string v1, "proximity wake lock already acquired"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final d(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 199
    iget-object v1, p0, Latf;->i:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v1, p0, Latf;->i:Landroid/content/Context;

    .line 202
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    const-string v2, "share_voicemail_allowed"

    invoke-interface {v1, v2, v3}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 203
    if-eqz v1, :cond_0

    iget-object v1, p0, Latf;->z:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 204
    if-eqz p1, :cond_2

    .line 205
    iget-object v1, p0, Latf;->i:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->am:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 206
    :cond_2
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    .line 207
    iget-object v1, p0, Latf;->z:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    .line 123
    invoke-virtual {p0, v2}, Latf;->b(Z)V

    .line 124
    iput v2, p0, Latf;->p:I

    .line 125
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 127
    iget-object v0, p0, Latf;->o:Latf$d;

    iget-object v1, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-interface {v0, v2, v1}, Latf$d;->a(II)V

    .line 128
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    .prologue
    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "MediaPlayer error listener invoked: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Latf;->a(Ljava/lang/Exception;)V

    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Latf;->o:Latf$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latf;->i:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Latf;->r:Z

    .line 97
    iget-object v0, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v1, p0, Latf;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 98
    iget v0, p0, Latf;->p:I

    const/16 v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "mPosition="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    iget-object v0, p0, Latf;->o:Latf$d;

    iget v1, p0, Latf;->p:I

    iget-object v2, p0, Latf;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-interface {v0, v1, v2}, Latf$d;->a(II)V

    .line 100
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->i()V

    .line 101
    iget-object v0, p0, Latf;->o:Latf$d;

    invoke-interface {v0}, Latf$d;->d()V

    .line 102
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 103
    iget-object v0, p0, Latf;->l:Landroid/media/MediaPlayer;

    iget v1, p0, Latf;->p:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 104
    :cond_2
    iget-boolean v0, p0, Latf;->q:Z

    if-eqz v0, :cond_3

    .line 105
    invoke-virtual {p0}, Latf;->b()V

    goto :goto_0

    .line 107
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Latf;->b(Z)V

    goto :goto_0
.end method
