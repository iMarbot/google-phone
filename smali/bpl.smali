.class final Lbpl;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation


# static fields
.field private static a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lbpo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_last_updated_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lbpl;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbpl;->b:Landroid/content/Context;

    .line 3
    new-instance v0, Lbpo;

    new-instance v1, Lbpk;

    invoke-direct {v1, p1}, Lbpk;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1}, Lbpo;-><init>(Landroid/content/Context;Lbpk;)V

    iput-object v0, p0, Lbpl;->c:Lbpo;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    const-string v0, "PinnedShortcuts.refresh"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lbpl;->b:Landroid/content/Context;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 8
    const-string v0, "PinnedShortcuts.refresh"

    const-string v1, "no contact permissions"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    new-instance v7, Lbpm;

    .line 11
    invoke-direct {v7}, Lbpm;-><init>()V

    .line 13
    iget-object v0, p0, Lbpl;->b:Landroid/content/Context;

    const-class v1, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    .line 14
    invoke-virtual {v0}, Landroid/content/pm/ShortcutManager;->getPinnedShortcuts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/content/pm/ShortcutInfo;

    .line 15
    invoke-virtual {v6}, Landroid/content/pm/ShortcutInfo;->isDeclaredInManifest()Z

    move-result v0

    if-nez v0, :cond_2

    .line 16
    invoke-virtual {v6}, Landroid/content/pm/ShortcutInfo;->isDynamic()Z

    move-result v0

    if-nez v0, :cond_2

    .line 18
    invoke-virtual {v6}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v9

    .line 20
    invoke-static {v6}, Lbpg;->a(Landroid/content/pm/ShortcutInfo;)Landroid/net/Uri;

    move-result-object v1

    .line 21
    iget-object v0, p0, Lbpl;->b:Landroid/content/Context;

    .line 22
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lbpl;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 24
    if-eqz v2, :cond_3

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 25
    :cond_3
    const-string v0, "PinnedShortcuts.refresh"

    const-string v1, "contact disabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iget-object v0, v7, Lbpm;->a:Ljava/util/List;

    invoke-virtual {v6}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 27
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 28
    :cond_4
    :try_start_1
    invoke-static {}, Lbpg;->f()Lbph;

    move-result-object v0

    const-string v1, "_id"

    .line 29
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lbph;->a(J)Lbph;

    move-result-object v0

    .line 30
    invoke-virtual {v0, v9}, Lbph;->a(Ljava/lang/String;)Lbph;

    move-result-object v0

    const-string v1, "display_name"

    .line 31
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lbph;->b(Ljava/lang/String;)Lbph;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbph;->a()Lbpg;

    move-result-object v0

    .line 34
    invoke-virtual {v0, v6}, Lbpg;->b(Landroid/content/pm/ShortcutInfo;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 35
    const-string v1, "PinnedShortcuts.refresh"

    const-string v4, "contact updated"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v1, v7, Lbpm;->b:Ljava/util/Map;

    invoke-virtual {v6}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 37
    :cond_5
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 38
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 39
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_6

    if-eqz v3, :cond_7

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_6
    :goto_3
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v3, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 42
    :cond_8
    iget-object v0, p0, Lbpl;->b:Landroid/content/Context;

    const-class v1, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ShortcutManager;

    .line 43
    iget-object v1, p0, Lbpl;->b:Landroid/content/Context;

    .line 44
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 45
    iget-object v2, v7, Lbpm;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 46
    iget-object v2, v7, Lbpm;->a:Ljava/util/List;

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/ShortcutManager;->disableShortcuts(Ljava/util/List;Ljava/lang/CharSequence;)V

    .line 47
    :cond_9
    iget-object v1, v7, Lbpm;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iget-object v1, p0, Lbpl;->c:Lbpo;

    iget-object v2, v7, Lbpm;->b:Ljava/util/Map;

    .line 49
    invoke-virtual {v1, v2}, Lbpo;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Landroid/content/pm/ShortcutManager;->updateShortcuts(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const-string v0, "PinnedShortcuts.applyDelta"

    const-string v1, "shortcutManager rate limited."

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 39
    :catchall_1
    move-exception v0

    goto :goto_2
.end method
