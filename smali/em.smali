.class public final Lem;
.super Let;
.source "PG"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable$Callback;

.field private c:Leo;

.field private d:Landroid/content/Context;

.field private e:Landroid/animation/ArgbEvaluator;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, v0, v0}, Lem;-><init>(Landroid/content/Context;Leo;Landroid/content/res/Resources;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0, p1, v0, v0}, Lem;-><init>(Landroid/content/Context;Leo;Landroid/content/res/Resources;)V

    .line 4
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Leo;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5
    invoke-direct {p0}, Let;-><init>()V

    .line 6
    iput-object v2, p0, Lem;->e:Landroid/animation/ArgbEvaluator;

    .line 7
    new-instance v0, Len;

    invoke-direct {v0, p0}, Len;-><init>(Lem;)V

    iput-object v0, p0, Lem;->a:Landroid/graphics/drawable/Drawable$Callback;

    .line 8
    iput-object p1, p0, Lem;->d:Landroid/content/Context;

    .line 9
    new-instance v0, Leo;

    iget-object v1, p0, Lem;->a:Landroid/graphics/drawable/Drawable$Callback;

    invoke-direct {v0, v2, v1, v2}, Leo;-><init>(Leo;Landroid/graphics/drawable/Drawable$Callback;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lem;->c:Leo;

    .line 10
    return-void
.end method

.method private final a(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 152
    instance-of v0, p1, Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 153
    check-cast v0, Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v2

    .line 154
    if-eqz v2, :cond_0

    .line 155
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 156
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-direct {p0, v0}, Lem;->a(Landroid/animation/Animator;)V

    .line 157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 158
    :cond_0
    instance-of v0, p1, Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_3

    .line 159
    check-cast p1, Landroid/animation/ObjectAnimator;

    .line 160
    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    .line 161
    const-string v1, "fillColor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "strokeColor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 162
    :cond_1
    iget-object v0, p0, Lem;->e:Landroid/animation/ArgbEvaluator;

    if-nez v0, :cond_2

    .line 163
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lem;->e:Landroid/animation/ArgbEvaluator;

    .line 164
    :cond_2
    iget-object v0, p0, Lem;->e:Landroid/animation/ArgbEvaluator;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 165
    :cond_3
    return-void
.end method


# virtual methods
.method public final applyTheme(Landroid/content/res/Resources$Theme;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V

    .line 148
    :cond_0
    return-void
.end method

.method public final canApplyTheme()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lbw;->d(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 151
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic clearColorFilter()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Let;->clearColorFilter()V

    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->draw(Landroid/graphics/Canvas;)V

    .line 24
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lem;->invalidateSelf()V

    goto :goto_0
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lbw;->c(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    .line 40
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->getAlpha()I

    move-result v0

    goto :goto_0
.end method

.method public final getChangingConfigurations()I
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    .line 19
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Let;->getChangingConfigurations()I

    move-result v0

    iget-object v1, p0, Lem;->c:Leo;

    iget v1, v1, Leo;->a:I

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    .prologue
    .line 195
    invoke-super {p0}, Let;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 15
    new-instance v0, Lep;

    iget-object v1, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-direct {v0, v1}, Lep;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V

    .line 16
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, Let;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 78
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic getMinimumHeight()I
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Let;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getMinimumWidth()I
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Let;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    .line 75
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->getOpacity()I

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 186
    invoke-super {p0, p1}, Let;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getState()[I
    .locals 1

    .prologue
    .line 185
    invoke-super {p0}, Let;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Let;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public final inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lem;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    .line 144
    return-void
.end method

.method public final inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 90
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2, p3, p4}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    .line 142
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 94
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 95
    :goto_1
    if-eq v0, v7, :cond_a

    .line 96
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    if-ge v2, v1, :cond_1

    const/4 v2, 0x3

    if-eq v0, v2, :cond_a

    .line 97
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 98
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 99
    const-string v2, "animated-vector"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 100
    sget-object v0, Lel;->e:[I

    .line 101
    invoke-static {p1, p4, p3, v0}, Lbw;->a(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 103
    if-eqz v2, :cond_3

    .line 104
    invoke-static {p1, v2, p4}, Leu;->a(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Leu;

    move-result-object v2

    .line 106
    iput-boolean v6, v2, Leu;->c:Z

    .line 107
    iget-object v3, p0, Lem;->a:Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v2, v3}, Leu;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 108
    iget-object v3, p0, Lem;->c:Leo;

    iget-object v3, v3, Leo;->b:Leu;

    if-eqz v3, :cond_2

    .line 109
    iget-object v3, p0, Lem;->c:Leo;

    iget-object v3, v3, Leo;->b:Leu;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Leu;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 110
    :cond_2
    iget-object v3, p0, Lem;->c:Leo;

    iput-object v2, v3, Leo;->b:Leu;

    .line 111
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 140
    :cond_4
    :goto_2
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_1

    .line 112
    :cond_5
    const-string v2, "target"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 113
    sget-object v0, Lel;->f:[I

    .line 114
    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-virtual {v0, v7, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 117
    if-eqz v3, :cond_8

    .line 118
    iget-object v4, p0, Lem;->d:Landroid/content/Context;

    if-eqz v4, :cond_9

    .line 119
    iget-object v4, p0, Lem;->d:Landroid/content/Context;

    invoke-static {v4, v3}, Lbw;->b(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v3

    .line 121
    iget-object v4, p0, Lem;->c:Leo;

    iget-object v4, v4, Leo;->b:Leu;

    invoke-virtual {v4, v2}, Leu;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 122
    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 123
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_6

    .line 124
    invoke-direct {p0, v3}, Lem;->a(Landroid/animation/Animator;)V

    .line 125
    :cond_6
    iget-object v4, p0, Lem;->c:Leo;

    .line 126
    iget-object v4, v4, Leo;->d:Ljava/util/ArrayList;

    .line 127
    if-nez v4, :cond_7

    .line 128
    iget-object v4, p0, Lem;->c:Leo;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 129
    iput-object v5, v4, Leo;->d:Ljava/util/ArrayList;

    .line 131
    iget-object v4, p0, Lem;->c:Leo;

    new-instance v5, Lpd;

    invoke-direct {v5}, Lpd;-><init>()V

    iput-object v5, v4, Leo;->e:Lpd;

    .line 132
    :cond_7
    iget-object v4, p0, Lem;->c:Leo;

    .line 133
    iget-object v4, v4, Leo;->d:Ljava/util/ArrayList;

    .line 134
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v4, p0, Lem;->c:Leo;

    iget-object v4, v4, Leo;->e:Lpd;

    invoke-virtual {v4, v3, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_8
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_2

    .line 137
    :cond_9
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 138
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Context can\'t be null when inflating animators"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_a
    iget-object v0, p0, Lem;->c:Leo;

    invoke-virtual {v0}, Leo;->a()V

    goto/16 :goto_0
.end method

.method public final isAutoMirrored()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lbw;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 84
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->isAutoMirrored()Z

    move-result v0

    goto :goto_0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->isRunning()Z

    move-result v0

    .line 168
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    goto :goto_0
.end method

.method public final isStateful()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0}, Leu;->isStateful()Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic jumpToCurrentState()V
    .locals 0

    .prologue
    .line 191
    invoke-super {p0}, Let;->jumpToCurrentState()V

    return-void
.end method

.method public final mutate()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 13
    :cond_0
    return-object p0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 31
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    .line 37
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setLevel(I)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onStateChange([I)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    .line 34
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setState([I)Z

    move-result v0

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 45
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setAlpha(I)V

    goto :goto_0
.end method

.method public final setAutoMirrored(Z)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setAutoMirrored(Z)V

    goto :goto_0
.end method

.method public final bridge synthetic setChangingConfigurations(I)V
    .locals 0

    .prologue
    .line 183
    invoke-super {p0, p1}, Let;->setChangingConfigurations(I)V

    return-void
.end method

.method public final bridge synthetic setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0, p1, p2}, Let;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final bridge synthetic setFilterBitmap(Z)V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0, p1}, Let;->setFilterBitmap(Z)V

    return-void
.end method

.method public final bridge synthetic setHotspot(FF)V
    .locals 0

    .prologue
    .line 194
    invoke-super {p0, p1, p2}, Let;->setHotspot(FF)V

    return-void
.end method

.method public final bridge synthetic setHotspotBounds(IIII)V
    .locals 0

    .prologue
    .line 193
    invoke-super {p0, p1, p2, p3, p4}, Let;->setHotspotBounds(IIII)V

    return-void
.end method

.method public final bridge synthetic setState([I)Z
    .locals 1

    .prologue
    .line 182
    invoke-super {p0, p1}, Let;->setState([I)Z

    move-result v0

    return v0
.end method

.method public final setTint(I)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setTint(I)V

    goto :goto_0
.end method

.method public final setTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public final setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1}, Leu;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method

.method public final setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 68
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->b:Leu;

    invoke-virtual {v0, p1, p2}, Leu;->setVisible(ZZ)Z

    .line 69
    invoke-super {p0, p1, p2}, Let;->setVisible(ZZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 175
    invoke-virtual {p0}, Lem;->invalidateSelf()V

    goto :goto_0
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lem;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->stop()V

    .line 181
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lem;->c:Leo;

    iget-object v0, v0, Leo;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto :goto_0
.end method
