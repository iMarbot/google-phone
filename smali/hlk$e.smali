.class public final Lhlk$e;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhlk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field private static a:Ljava/lang/ref/ReferenceQueue;

.field private static b:Ljava/util/Collection;

.field private static volatile c:Lhll;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    sput-object v0, Lhlk$e;->a:Ljava/lang/ref/ReferenceQueue;

    .line 30
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lhlk$e;->b:Ljava/util/Collection;

    return-void
.end method

.method private static declared-synchronized a()V
    .locals 3

    .prologue
    .line 11
    const-class v1, Lhlk$e;

    monitor-enter v1

    const/4 v0, 0x0

    .line 12
    :goto_0
    :try_start_0
    sget-object v2, Lhlk$e;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v2}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 13
    const/4 v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    if-eqz v0, :cond_2

    .line 15
    sget-object v0, Lhlk$e;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 16
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 18
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 11
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 19
    :cond_2
    monitor-exit v1

    return-void
.end method

.method static a(Lhlk;)V
    .locals 4

    .prologue
    .line 20
    sget-object v0, Lhlk$e;->c:Lhll;

    if-nez v0, :cond_1

    .line 21
    const-class v1, Lhlk;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Lhlk$e;->c:Lhll;

    if-nez v0, :cond_0

    .line 23
    sget-object v0, Lhlk$e;->b:Ljava/util/Collection;

    new-instance v2, Ljava/lang/ref/WeakReference;

    sget-object v3, Lhlk$e;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 24
    invoke-static {}, Lhlk$e;->a()V

    .line 25
    monitor-exit v1

    .line 28
    :goto_0
    return-void

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    invoke-virtual {v0, p0}, Lhll;->a(Lhlk;)V

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized a(Lhll;)V
    .locals 4

    .prologue
    .line 1
    const-class v2, Lhlk$e;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lhlk$e;->c:Lhll;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "callback already present"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 2
    const-string v0, "registerCallback"

    invoke-static {p0, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhll;

    sput-object v0, Lhlk$e;->c:Lhll;

    .line 3
    sget-object v0, Lhlk$e;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 4
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhlk;

    .line 5
    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 7
    invoke-virtual {p0, v1}, Lhll;->a(Lhlk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 9
    :cond_2
    :try_start_1
    invoke-static {}, Lhlk$e;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 10
    monitor-exit v2

    return-void
.end method
