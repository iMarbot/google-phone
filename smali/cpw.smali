.class public final Lcpw;
.super Lcpv;
.source "PG"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcpv;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;SLjava/lang/String;)V

    .line 2
    iput-object p5, p0, Lcpw;->b:Ljava/lang/String;

    .line 3
    iput-object p6, p0, Lcpw;->c:Ljava/lang/String;

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Lcpw;->d:Ljava/lang/String;

    .line 5
    return-void
.end method

.method private final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 25
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    const-string v0, "pv"

    iget-object v1, p0, Lcpw;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcpw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    const-string v0, "ct"

    iget-object v1, p0, Lcpw;->b:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcpw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method private final b(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 30
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    const-string v0, "pt"

    iget-short v1, p0, Lcpw;->a:S

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcpw;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method private final c(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 33
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    iget-object v0, p0, Lcpw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activate"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, v0}, Lcpw;->a(Ljava/lang/StringBuilder;)V

    .line 8
    iget-object v1, p0, Lcpw;->c:Ljava/lang/String;

    const-string v2, "12"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcpw;->c:Ljava/lang/String;

    const-string v2, "13"

    .line 9
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 10
    :cond_0
    invoke-direct {p0, v0}, Lcpw;->b(Ljava/lang/StringBuilder;)V

    .line 11
    invoke-direct {p0, v0}, Lcpw;->c(Ljava/lang/StringBuilder;)V

    .line 12
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcpw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 13
    return-void
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Deactivate"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, v0}, Lcpw;->a(Ljava/lang/StringBuilder;)V

    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcpw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 17
    return-void
.end method

.method public final c(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Status"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 19
    iget-object v1, p0, Lcpw;->c:Ljava/lang/String;

    const-string v2, "13"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20
    invoke-direct {p0, v0}, Lcpw;->a(Ljava/lang/StringBuilder;)V

    .line 21
    invoke-direct {p0, v0}, Lcpw;->b(Ljava/lang/StringBuilder;)V

    .line 22
    invoke-direct {p0, v0}, Lcpw;->c(Ljava/lang/StringBuilder;)V

    .line 23
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcpw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 24
    return-void
.end method
