.class public final Lafm;
.super Landroid/transition/Transition;
.source "PG"


# static fields
.field private static a:Landroid/util/Property;

.field private static b:Landroid/util/Property;


# instance fields
.field private c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lafn;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "topLeft"

    invoke-direct {v0, v1, v2}, Lafn;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lafm;->a:Landroid/util/Property;

    .line 51
    new-instance v0, Lafo;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "bottomRight"

    invoke-direct {v0, v1, v2}, Lafo;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lafm;->b:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 2
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lafm;->c:[I

    return-void
.end method

.method private final a(Landroid/transition/TransitionValues;)V
    .locals 9

    .prologue
    .line 7
    iget-object v3, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 8
    invoke-virtual {v3}, Landroid/view/View;->isLaidOut()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    :cond_0
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bubble:changeScreenBounds:width"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 10
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "bubble:changeScreenBounds:height"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 11
    iget-object v4, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "bubble:changeScreenBounds:bounds"

    new-instance v6, Landroid/graphics/Rect;

    .line 12
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v7

    .line 13
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v8

    .line 14
    if-nez v0, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v0

    move v2, v0

    .line 15
    :goto_0
    if-nez v1, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_1
    invoke-direct {v6, v7, v8, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 16
    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v1, p0, Lafm;->c:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 18
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bubble:changeScreenBounds:screenX"

    iget-object v2, p0, Lafm;->c:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iget-object v0, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bubble:changeScreenBounds:screenY"

    iget-object v2, p0, Lafm;->c:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    :cond_1
    return-void

    .line 14
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_0

    .line 15
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0, p1}, Lafm;->a(Landroid/transition/TransitionValues;)V

    .line 6
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1}, Lafm;->a(Landroid/transition/TransitionValues;)V

    .line 4
    return-void
.end method

.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 12

    .prologue
    .line 21
    iget-object v0, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "bubble:changeScreenBounds:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 22
    iget-object v1, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "bubble:changeScreenBounds:bounds"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 23
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    .line 25
    :cond_1
    iget-object v2, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "bubble:changeScreenBounds:screenX"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 26
    iget-object v2, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v4, "bubble:changeScreenBounds:screenY"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 27
    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "bubble:changeScreenBounds:screenX"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 28
    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v6, "bubble:changeScreenBounds:screenY"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 29
    sub-int/2addr v3, v5

    sub-int v2, v4, v2

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 30
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 31
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 32
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 33
    iget v5, v1, Landroid/graphics/Rect;->top:I

    .line 34
    iget v6, v0, Landroid/graphics/Rect;->right:I

    .line 35
    iget v7, v1, Landroid/graphics/Rect;->right:I

    .line 36
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 37
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 38
    new-instance v8, Lafp;

    iget-object v9, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-direct {v8, v9}, Lafp;-><init>(Landroid/view/View;)V

    .line 39
    new-instance v9, Landroid/graphics/PointF;

    int-to-float v10, v2

    int-to-float v11, v4

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v9}, Lafp;->a(Landroid/graphics/PointF;)V

    .line 40
    new-instance v9, Landroid/graphics/PointF;

    int-to-float v10, v6

    int-to-float v11, v0

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v9}, Lafp;->b(Landroid/graphics/PointF;)V

    .line 41
    invoke-virtual {p0}, Lafm;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v9

    int-to-float v2, v2

    int-to-float v4, v4

    int-to-float v3, v3

    int-to-float v5, v5

    invoke-virtual {v9, v2, v4, v3, v5}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v2

    .line 42
    sget-object v3, Lafm;->a:Landroid/util/Property;

    const/4 v4, 0x0

    .line 43
    invoke-static {v8, v3, v4, v2}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 44
    invoke-virtual {p0}, Lafm;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v3

    int-to-float v4, v6

    int-to-float v0, v0

    int-to-float v5, v7

    int-to-float v1, v1

    invoke-virtual {v3, v4, v0, v5, v1}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v0

    .line 45
    sget-object v1, Lafm;->b:Landroid/util/Property;

    const/4 v3, 0x0

    .line 46
    invoke-static {v8, v1, v3, v0}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Landroid/util/Property;Landroid/animation/TypeConverter;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 47
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 48
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v1, v3, v2

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto/16 :goto_0
.end method
