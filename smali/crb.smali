.class final Lcrb;
.super Lhkt;
.source "PG"


# instance fields
.field private synthetic b:Lcra;


# direct methods
.method constructor <init>(Lcra;Lhjx;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcrb;->b:Lcra;

    invoke-direct {p0, p2}, Lhkt;-><init>(Lhjx;)V

    return-void
.end method


# virtual methods
.method public final a(Lhjy;Lhlh;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 3
    iget-object v0, v0, Lcra;->a:Ljava/lang/String;

    .line 4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    const-string v1, "TranscriptionClientFactory.interceptCall"

    const-string v2, "attaching package name: "

    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 7
    iget-object v0, v0, Lcra;->a:Ljava/lang/String;

    .line 8
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    .line 9
    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    sget-object v0, Lcra;->f:Lhlh$e;

    .line 12
    iget-object v1, p0, Lcrb;->b:Lcra;

    .line 13
    iget-object v1, v1, Lcra;->a:Ljava/lang/String;

    .line 14
    invoke-virtual {p2, v0, v1}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 16
    iget-object v0, v0, Lcra;->b:Ljava/lang/String;

    .line 17
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 18
    const-string v0, "TranscriptionClientFactory.interceptCall"

    const-string v1, "attaching android cert"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    sget-object v0, Lcra;->g:Lhlh$e;

    .line 21
    iget-object v1, p0, Lcrb;->b:Lcra;

    .line 22
    iget-object v1, v1, Lcra;->b:Ljava/lang/String;

    .line 23
    invoke-virtual {p2, v0, v1}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 24
    :cond_1
    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 25
    iget-object v0, v0, Lcra;->c:Ljava/lang/String;

    .line 26
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 27
    const-string v0, "TranscriptionClientFactory.interceptCall"

    const-string v1, "attaching API Key"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    sget-object v0, Lcra;->e:Lhlh$e;

    .line 30
    iget-object v1, p0, Lcrb;->b:Lcra;

    .line 31
    iget-object v1, v1, Lcra;->c:Ljava/lang/String;

    .line 32
    invoke-virtual {p2, v0, v1}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 33
    :cond_2
    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 34
    iget-object v0, v0, Lcra;->d:Ljava/lang/String;

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 36
    const-string v0, "TranscriptionClientFactory.interceptCall"

    const-string v1, "attaching auth token"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    sget-object v1, Lcra;->h:Lhlh$e;

    .line 39
    const-string v2, "Bearer "

    iget-object v0, p0, Lcrb;->b:Lcra;

    .line 40
    iget-object v0, v0, Lcra;->d:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, v1, v0}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 42
    :cond_3
    invoke-super {p0, p1, p2}, Lhkt;->a(Lhjy;Lhlh;)V

    .line 43
    return-void

    .line 8
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
