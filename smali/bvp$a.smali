.class final Lbvp$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lbvp$c;)Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 23
    :goto_0
    return-object v7

    .line 4
    :cond_0
    new-instance v0, Lbml;

    invoke-direct {v0}, Lbml;-><init>()V

    .line 5
    iget-object v1, p0, Lbvp$c;->d:Lbmi;

    invoke-interface {v1, v0}, Lbmi;->a(Lbml;)Lbmj;

    move-result-object v1

    .line 6
    sget-object v2, Lbko$a;->f:Lbko$a;

    const-string v3, "CNAP"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, Lbmj;->a(Lbko$a;Ljava/lang/String;J)V

    .line 7
    iget-object v2, p0, Lbvp$c;->b:Ljava/lang/String;

    iput-object v2, v0, Lbml;->d:Ljava/lang/String;

    .line 8
    iget-object v2, p0, Lbvp$c;->a:Ljava/lang/String;

    iput-object v2, v0, Lbml;->h:Ljava/lang/String;

    .line 9
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "vnd.android.cursor.item/phone_v2"

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "data1"

    iget-object v6, v0, Lbml;->h:Ljava/lang/String;

    .line 10
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v4

    .line 11
    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    .line 12
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "display_name"

    iget-object v0, v0, Lbml;->d:Ljava/lang/String;

    .line 13
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v3, "display_name_source"

    const/16 v4, 0x28

    .line 14
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v3, "vnd.android.cursor.item/contact"

    .line 15
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 17
    invoke-interface {v1, v0}, Lbmj;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    :goto_1
    iget-object v0, p0, Lbvp$c;->d:Lbmi;

    iget-object v2, p0, Lbvp$c;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lbmi;->a(Landroid/content/Context;Lbmj;)V

    goto :goto_0

    .line 20
    :catch_0
    move-exception v0

    sget-object v0, Lbvp;->a:Ljava/lang/String;

    .line 21
    const-string v2, "Creation of lookup key failed when caching CNAP information"

    invoke-static {v0, v2}, Lbvs;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lbvp$c;

    invoke-static {p1}, Lbvp$a;->a(Lbvp$c;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
