.class public final Lhgu;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lhgu;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:[Lhgx;

.field private g:Lhhb;

.field private h:Lhgv;

.field private i:Ljava/lang/String;

.field private j:Lhgw;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lhgu;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lhgu;->c:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lhgu;->d:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lhgu;->e:I

    .line 13
    invoke-static {}, Lhgx;->a()[Lhgx;

    move-result-object v0

    iput-object v0, p0, Lhgu;->f:[Lhgx;

    .line 14
    iput-object v1, p0, Lhgu;->g:Lhhb;

    .line 15
    iput-object v1, p0, Lhgu;->h:Lhgv;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lhgu;->i:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lhgu;->j:Lhgw;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lhgu;->k:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lhgu;->unknownFieldData:Lhfv;

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lhgu;->cachedSize:I

    .line 21
    return-void
.end method

.method public static a()[Lhgu;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhgu;->a:[Lhgu;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhgu;->a:[Lhgu;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhgu;

    sput-object v0, Lhgu;->a:[Lhgu;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhgu;->a:[Lhgu;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 48
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 49
    iget-object v1, p0, Lhgu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhgu;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    const/4 v1, 0x1

    iget-object v2, p0, Lhgu;->b:Ljava/lang/String;

    .line 51
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_0
    iget-object v1, p0, Lhgu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhgu;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 53
    const/4 v1, 0x2

    iget-object v2, p0, Lhgu;->c:Ljava/lang/String;

    .line 54
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lhgu;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhgu;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 56
    const/4 v1, 0x3

    iget-object v2, p0, Lhgu;->d:Ljava/lang/String;

    .line 57
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget v1, p0, Lhgu;->e:I

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x4

    iget v2, p0, Lhgu;->e:I

    .line 60
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_3
    iget-object v1, p0, Lhgu;->f:[Lhgx;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lhgu;->f:[Lhgx;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 62
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lhgu;->f:[Lhgx;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 63
    iget-object v2, p0, Lhgu;->f:[Lhgx;

    aget-object v2, v2, v0

    .line 64
    if-eqz v2, :cond_4

    .line 65
    const/4 v3, 0x5

    .line 66
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 67
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 68
    :cond_6
    iget-object v1, p0, Lhgu;->h:Lhgv;

    if-eqz v1, :cond_7

    .line 69
    const/4 v1, 0x6

    iget-object v2, p0, Lhgu;->h:Lhgv;

    .line 70
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_7
    iget-object v1, p0, Lhgu;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhgu;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 72
    const/4 v1, 0x7

    iget-object v2, p0, Lhgu;->i:Ljava/lang/String;

    .line 73
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_8
    iget-object v1, p0, Lhgu;->g:Lhhb;

    if-eqz v1, :cond_9

    .line 75
    const/16 v1, 0x8

    iget-object v2, p0, Lhgu;->g:Lhhb;

    .line 76
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_9
    iget-object v1, p0, Lhgu;->j:Lhgw;

    if-eqz v1, :cond_a

    .line 78
    const/16 v1, 0x9

    iget-object v2, p0, Lhgu;->j:Lhgw;

    .line 79
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_a
    iget-object v1, p0, Lhgu;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lhgu;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 81
    const/16 v1, 0xa

    iget-object v2, p0, Lhgu;->k:Ljava/lang/String;

    .line 82
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_b
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 86
    sparse-switch v0, :sswitch_data_0

    .line 88
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 90
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgu;->b:Ljava/lang/String;

    goto :goto_0

    .line 92
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgu;->c:Ljava/lang/String;

    goto :goto_0

    .line 94
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgu;->d:Ljava/lang/String;

    goto :goto_0

    .line 97
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 98
    iput v0, p0, Lhgu;->e:I

    goto :goto_0

    .line 100
    :sswitch_5
    const/16 v0, 0x2a

    .line 101
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 102
    iget-object v0, p0, Lhgu;->f:[Lhgx;

    if-nez v0, :cond_2

    move v0, v1

    .line 103
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgx;

    .line 104
    if-eqz v0, :cond_1

    .line 105
    iget-object v3, p0, Lhgu;->f:[Lhgx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 107
    new-instance v3, Lhgx;

    invoke-direct {v3}, Lhgx;-><init>()V

    aput-object v3, v2, v0

    .line 108
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 109
    invoke-virtual {p1}, Lhfp;->a()I

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 102
    :cond_2
    iget-object v0, p0, Lhgu;->f:[Lhgx;

    array-length v0, v0

    goto :goto_1

    .line 111
    :cond_3
    new-instance v3, Lhgx;

    invoke-direct {v3}, Lhgx;-><init>()V

    aput-object v3, v2, v0

    .line 112
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 113
    iput-object v2, p0, Lhgu;->f:[Lhgx;

    goto :goto_0

    .line 115
    :sswitch_6
    iget-object v0, p0, Lhgu;->h:Lhgv;

    if-nez v0, :cond_4

    .line 116
    new-instance v0, Lhgv;

    invoke-direct {v0}, Lhgv;-><init>()V

    iput-object v0, p0, Lhgu;->h:Lhgv;

    .line 117
    :cond_4
    iget-object v0, p0, Lhgu;->h:Lhgv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 119
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgu;->i:Ljava/lang/String;

    goto :goto_0

    .line 121
    :sswitch_8
    iget-object v0, p0, Lhgu;->g:Lhhb;

    if-nez v0, :cond_5

    .line 122
    new-instance v0, Lhhb;

    invoke-direct {v0}, Lhhb;-><init>()V

    iput-object v0, p0, Lhgu;->g:Lhhb;

    .line 123
    :cond_5
    iget-object v0, p0, Lhgu;->g:Lhhb;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 125
    :sswitch_9
    iget-object v0, p0, Lhgu;->j:Lhgw;

    if-nez v0, :cond_6

    .line 126
    new-instance v0, Lhgw;

    invoke-direct {v0}, Lhgw;-><init>()V

    iput-object v0, p0, Lhgu;->j:Lhgw;

    .line 127
    :cond_6
    iget-object v0, p0, Lhgu;->j:Lhgw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 129
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgu;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lhgu;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgu;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iget-object v1, p0, Lhgu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 24
    :cond_0
    iget-object v0, p0, Lhgu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgu;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25
    const/4 v0, 0x2

    iget-object v1, p0, Lhgu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 26
    :cond_1
    iget-object v0, p0, Lhgu;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhgu;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 27
    const/4 v0, 0x3

    iget-object v1, p0, Lhgu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_2
    iget v0, p0, Lhgu;->e:I

    if-eqz v0, :cond_3

    .line 29
    const/4 v0, 0x4

    iget v1, p0, Lhgu;->e:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 30
    :cond_3
    iget-object v0, p0, Lhgu;->f:[Lhgx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhgu;->f:[Lhgx;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 31
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhgu;->f:[Lhgx;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 32
    iget-object v1, p0, Lhgu;->f:[Lhgx;

    aget-object v1, v1, v0

    .line 33
    if-eqz v1, :cond_4

    .line 34
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 35
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_5
    iget-object v0, p0, Lhgu;->h:Lhgv;

    if-eqz v0, :cond_6

    .line 37
    const/4 v0, 0x6

    iget-object v1, p0, Lhgu;->h:Lhgv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 38
    :cond_6
    iget-object v0, p0, Lhgu;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhgu;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 39
    const/4 v0, 0x7

    iget-object v1, p0, Lhgu;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 40
    :cond_7
    iget-object v0, p0, Lhgu;->g:Lhhb;

    if-eqz v0, :cond_8

    .line 41
    const/16 v0, 0x8

    iget-object v1, p0, Lhgu;->g:Lhhb;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_8
    iget-object v0, p0, Lhgu;->j:Lhgw;

    if-eqz v0, :cond_9

    .line 43
    const/16 v0, 0x9

    iget-object v1, p0, Lhgu;->j:Lhgw;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_9
    iget-object v0, p0, Lhgu;->k:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhgu;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 45
    const/16 v0, 0xa

    iget-object v1, p0, Lhgu;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 46
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 47
    return-void
.end method
