.class public abstract Lgzw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdd;


# static fields
.field public static usingExperimentalRuntime:Z


# instance fields
.field public memoizedHashCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-boolean v0, Lgzw;->usingExperimentalRuntime:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lgzw;->memoizedHashCode:I

    return-void
.end method

.method protected static addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 74
    check-cast p1, Ljava/util/List;

    invoke-static {p0, p1}, Lgzx;->addAll(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 75
    return-void
.end method

.method public static addAll(Ljava/lang/Iterable;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 76
    invoke-static {p0, p1}, Lgzx;->addAll(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 77
    return-void
.end method

.method protected static checkByteStringIsUtf8(Lhah;)V
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lhah;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Byte string is not UTF-8."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    return-void
.end method

.method private getClassInternal()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method private getSerializingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3e

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Serializing "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " threw an IOException (should never happen)."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static useExperimentalRuntime()V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x1

    sput-boolean v0, Lgzw;->usingExperimentalRuntime:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final getSerializedSizeInternal()I
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lhdp;->a:Lhdp;

    .line 42
    invoke-virtual {v0, p0}, Lhdp;->b(Ljava/lang/Object;)Lhdz;

    move-result-object v0

    invoke-interface {v0, p0}, Lhdz;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final isInitializedInternal()Z
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lhdp;->a:Lhdp;

    .line 40
    invoke-virtual {v0, p0}, Lhdp;->b(Ljava/lang/Object;)Lhdz;

    move-result-object v0

    invoke-interface {v0, p0}, Lhdz;->d(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected makeImmutableInternal()V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lhdp;->a:Lhdp;

    .line 66
    invoke-direct {p0}, Lgzw;->getClassInternal()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    invoke-interface {v0, p0}, Lhdz;->c(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public mergeFromInternal(Lhaq;Lhbg;)V
    .locals 2

    .prologue
    .line 52
    :try_start_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 54
    invoke-direct {p0}, Lgzw;->getClassInternal()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v1

    .line 56
    iget-object v0, p1, Lhaq;->d:Lhav;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p1, Lhaq;->d:Lhav;

    .line 59
    :goto_0
    invoke-interface {v1, p0, v0, p2}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 60
    return-void

    .line 58
    :cond_0
    new-instance v0, Lhav;

    invoke-direct {v0, p1}, Lhav;-><init>(Lhaq;)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    throw v0

    .line 63
    :catch_1
    move-exception v0

    .line 64
    new-instance v1, Lhcf;

    invoke-direct {v1, v0}, Lhcf;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public mutableCopy()Lhdi;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mutableCopy() is not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method newUninitializedMessageException()Lheo;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lheo;

    invoke-direct {v0}, Lheo;-><init>()V

    return-object v0
.end method

.method public toByteArray()[B
    .locals 3

    .prologue
    .line 13
    :try_start_0
    invoke-virtual {p0}, Lgzw;->getSerializedSize()I

    move-result v0

    new-array v0, v0, [B

    .line 14
    invoke-static {v0}, Lhaw;->a([B)Lhaw;

    move-result-object v1

    .line 15
    invoke-virtual {p0, v1}, Lgzw;->writeTo(Lhaw;)V

    .line 16
    invoke-virtual {v1}, Lhaw;->j()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "byte array"

    invoke-direct {p0, v2}, Lgzw;->getSerializingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toByteString()Lhah;
    .locals 3

    .prologue
    .line 5
    .line 6
    :try_start_0
    invoke-virtual {p0}, Lgzw;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Lhah;->b(I)Lham;

    move-result-object v0

    .line 8
    iget-object v1, v0, Lham;->a:Lhaw;

    .line 9
    invoke-virtual {p0, v1}, Lgzw;->writeTo(Lhaw;)V

    .line 10
    invoke-virtual {v0}, Lham;->a()Lhah;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "ByteString"

    invoke-direct {p0, v2}, Lgzw;->getSerializingExceptionMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeDelimitedTo(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lgzw;->getSerializedSize()I

    move-result v0

    .line 29
    invoke-static {v0}, Lhaw;->l(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 30
    invoke-static {v1}, Lhaw;->a(I)I

    move-result v1

    .line 32
    invoke-static {p1, v1}, Lhaw;->a(Ljava/io/OutputStream;I)Lhaw;

    move-result-object v1

    .line 34
    invoke-virtual {v1, v0}, Lhaw;->c(I)V

    .line 35
    invoke-virtual {p0, v1}, Lgzw;->writeTo(Lhaw;)V

    .line 36
    invoke-virtual {v1}, Lhaw;->h()V

    .line 37
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 20
    .line 21
    invoke-virtual {p0}, Lgzw;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Lhaw;->a(I)I

    move-result v0

    .line 23
    invoke-static {p1, v0}, Lhaw;->a(Ljava/io/OutputStream;I)Lhaw;

    move-result-object v0

    .line 24
    invoke-virtual {p0, v0}, Lgzw;->writeTo(Lhaw;)V

    .line 25
    invoke-virtual {v0}, Lhaw;->h()V

    .line 26
    return-void
.end method

.method public final writeToInternal(Lhaw;)V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lhdp;->a:Lhdp;

    .line 45
    invoke-direct {p0}, Lgzw;->getClassInternal()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v1

    .line 47
    iget-object v0, p1, Lhaw;->b:Lhax;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p1, Lhaw;->b:Lhax;

    .line 50
    :goto_0
    invoke-interface {v1, p0, v0}, Lhdz;->a(Ljava/lang/Object;Lhfn;)V

    .line 51
    return-void

    .line 49
    :cond_0
    new-instance v0, Lhax;

    invoke-direct {v0, p1}, Lhax;-><init>(Lhaw;)V

    goto :goto_0
.end method
