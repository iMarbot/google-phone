.class public final Lbpp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbpp;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 4
    check-cast p1, Ljava/util/List;

    .line 5
    const-string v1, "ShortcutRefresher.Task.doInBackground"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 6
    new-instance v2, Lbpi;

    iget-object v1, p0, Lbpp;->a:Landroid/content/Context;

    new-instance v3, Lbpk;

    iget-object v4, p0, Lbpp;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lbpk;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v1, v3}, Lbpi;-><init>(Landroid/content/Context;Lbpk;)V

    .line 7
    invoke-static {}, Lbdf;->c()V

    .line 8
    const-string v1, "DynamicShortcuts.refresh"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 9
    iget-object v1, v2, Lbpi;->a:Landroid/content/Context;

    invoke-static {v1}, Lbpi;->a(Landroid/content/Context;)Landroid/content/pm/ShortcutManager;

    move-result-object v3

    .line 10
    iget-object v1, v2, Lbpi;->a:Landroid/content/Context;

    const-string v4, "android.permission.READ_CONTACTS"

    invoke-static {v1, v4}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    const-string v1, "DynamicShortcuts.refresh"

    const-string v2, "no contact permissions"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    invoke-virtual {v3}, Landroid/content/pm/ShortcutManager;->removeAllDynamicShortcuts()V

    .line 46
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 47
    return-object v0

    .line 14
    :cond_1
    const/4 v1, 0x3

    .line 15
    invoke-virtual {v3}, Landroid/content/pm/ShortcutManager;->getMaxShortcutCountPerActivity()I

    move-result v4

    .line 16
    invoke-virtual {v3}, Landroid/content/pm/ShortcutManager;->getManifestShortcuts()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v4, v5

    .line 17
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 18
    new-instance v5, Landroid/util/ArrayMap;

    invoke-direct {v5, v4}, Landroid/util/ArrayMap;-><init>(I)V

    .line 20
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    .line 21
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v7

    if-ge v7, v4, :cond_2

    .line 22
    invoke-static {}, Lbpg;->f()Lbph;

    move-result-object v7

    iget-wide v8, v0, Lahc;->j:J

    .line 23
    invoke-virtual {v7, v8, v9}, Lbph;->a(J)Lbph;

    move-result-object v7

    iget-object v8, v0, Lahc;->i:Ljava/lang/String;

    .line 24
    invoke-virtual {v7, v8}, Lbph;->a(Ljava/lang/String;)Lbph;

    move-result-object v7

    .line 25
    invoke-virtual {v0}, Lahc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lbph;->b(Ljava/lang/String;)Lbph;

    move-result-object v7

    add-int/lit8 v0, v1, 0x1

    .line 26
    invoke-virtual {v7, v1}, Lbph;->a(I)Lbph;

    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lbph;->a()Lbpg;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lbpg;->b()Ljava/lang/String;

    move-result-object v7

    .line 30
    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 31
    goto :goto_1

    .line 32
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v3}, Landroid/content/pm/ShortcutManager;->getDynamicShortcuts()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 33
    invoke-virtual {v2, v0, v5}, Lbpi;->a(Ljava/util/List;Ljava/util/Map;)Lbpj;

    move-result-object v0

    .line 35
    iget-object v1, v2, Lbpi;->a:Landroid/content/Context;

    invoke-static {v1}, Lbpi;->a(Landroid/content/Context;)Landroid/content/pm/ShortcutManager;

    move-result-object v1

    .line 36
    iget-object v3, v0, Lbpj;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 37
    iget-object v3, v0, Lbpj;->b:Ljava/util/List;

    invoke-virtual {v1, v3}, Landroid/content/pm/ShortcutManager;->removeDynamicShortcuts(Ljava/util/List;)V

    .line 38
    :cond_3
    iget-object v3, v0, Lbpj;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 39
    iget-object v3, v2, Lbpi;->b:Lbpo;

    iget-object v4, v0, Lbpj;->a:Ljava/util/Map;

    .line 40
    invoke-virtual {v3, v4}, Lbpo;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object v3

    .line 41
    invoke-virtual {v1, v3}, Landroid/content/pm/ShortcutManager;->updateShortcuts(Ljava/util/List;)Z

    .line 42
    :cond_4
    iget-object v3, v0, Lbpj;->c:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 43
    iget-object v2, v2, Lbpi;->b:Lbpo;

    iget-object v0, v0, Lbpj;->c:Ljava/util/Map;

    .line 44
    invoke-virtual {v2, v0}, Lbpo;->a(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 45
    invoke-virtual {v1, v0}, Landroid/content/pm/ShortcutManager;->addDynamicShortcuts(Ljava/util/List;)Z

    goto/16 :goto_0
.end method
