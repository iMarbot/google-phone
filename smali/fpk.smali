.class final Lfpk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public glSurface:Lfut;

.field public lastBufferHeight:I

.field public lastBufferWidth:I

.field public final synthetic this$0:Lfpc;

.field public final videoSource:Lfsm;

.field public final videoSourceRenderer:Lfsp;


# direct methods
.method constructor <init>(Lfpc;Lfsm;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lfpk;->this$0:Lfpc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lfpk;->glSurface:Lfut;

    .line 3
    iput-object p2, p0, Lfpk;->videoSource:Lfsm;

    .line 4
    new-instance v0, Lfsp;

    invoke-direct {v0, p2}, Lfsp;-><init>(Lfsm;)V

    iput-object v0, p0, Lfpk;->videoSourceRenderer:Lfsp;

    .line 5
    return-void
.end method

.method private final initializeGlSurface()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 52
    iget-object v1, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v1}, Lfsm;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    .line 53
    if-nez v1, :cond_0

    .line 54
    const-string v1, "Null SurfaceTexture passed for renderer"

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    .line 64
    :goto_0
    return v0

    .line 56
    :cond_0
    iget-object v2, p0, Lfpk;->this$0:Lfpc;

    invoke-static {v2}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v2

    invoke-interface {v2, v1}, Lfus;->createSurface(Ljava/lang/Object;)Lfut;

    move-result-object v2

    .line 57
    if-nez v2, :cond_1

    .line 58
    const-string v1, "Invalid SurfaceTexture passed for renderer"

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-interface {v2}, Lfut;->release()V

    .line 61
    iget v0, p0, Lfpk;->lastBufferWidth:I

    iget v2, p0, Lfpk;->lastBufferHeight:I

    invoke-virtual {v1, v0, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 62
    iget-object v0, p0, Lfpk;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$000(Lfpc;)Lfus;

    move-result-object v0

    invoke-interface {v0, v1}, Lfus;->createSurface(Ljava/lang/Object;)Lfut;

    move-result-object v0

    iput-object v0, p0, Lfpk;->glSurface:Lfut;

    .line 63
    const-string v0, "OutputRenderer.initializeGlSurface"

    invoke-static {v0}, Lfmk;->c(Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final releaseGlSurface()V
    .locals 4

    .prologue
    .line 9
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    if-eqz v0, :cond_0

    .line 10
    const-string v0, "Destroying surface for %s."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfpk;->videoSource:Lfsm;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lfpk;->this$0:Lfpc;

    invoke-static {v0}, Lfpc;->access$400(Lfpc;)Lfph;

    move-result-object v0

    invoke-virtual {v0}, Lfph;->makeCurrent()Z

    .line 12
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    invoke-interface {v0}, Lfut;->release()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lfpk;->glSurface:Lfut;

    .line 14
    :cond_0
    return-void
.end method

.method private final render()I
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v0}, Lfsm;->getOutputFormat()Lfwe;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v1}, Lfsm;->getOutputWidth()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 23
    iget v1, v0, Lfwe;->c:I

    .line 24
    iget v2, p0, Lfpk;->lastBufferWidth:I

    if-ne v1, v2, :cond_0

    .line 26
    iget v1, v0, Lfwe;->d:I

    .line 27
    iget v2, p0, Lfpk;->lastBufferHeight:I

    if-eq v1, v2, :cond_1

    .line 29
    :cond_0
    iget v1, v0, Lfwe;->c:I

    .line 30
    iput v1, p0, Lfpk;->lastBufferWidth:I

    .line 32
    iget v0, v0, Lfwe;->d:I

    .line 33
    iput v0, p0, Lfpk;->lastBufferHeight:I

    .line 34
    invoke-direct {p0}, Lfpk;->releaseGlSurface()V

    .line 40
    :cond_1
    :goto_0
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    if-nez v0, :cond_5

    .line 41
    invoke-direct {p0}, Lfpk;->initializeGlSurface()I

    move-result v0

    .line 42
    if-eqz v0, :cond_5

    .line 51
    :cond_2
    :goto_1
    return v0

    .line 35
    :cond_3
    iget-object v0, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v0}, Lfsm;->getOutputWidth()I

    move-result v0

    iget v1, p0, Lfpk;->lastBufferWidth:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lfpk;->videoSource:Lfsm;

    .line 36
    invoke-virtual {v0}, Lfsm;->getOutputHeight()I

    move-result v0

    iget v1, p0, Lfpk;->lastBufferHeight:I

    if-eq v0, v1, :cond_1

    .line 37
    :cond_4
    iget-object v0, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v0}, Lfsm;->getOutputWidth()I

    move-result v0

    iput v0, p0, Lfpk;->lastBufferWidth:I

    .line 38
    iget-object v0, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v0}, Lfsm;->getOutputHeight()I

    move-result v0

    iput v0, p0, Lfpk;->lastBufferHeight:I

    .line 39
    invoke-direct {p0}, Lfpk;->releaseGlSurface()V

    goto :goto_0

    .line 44
    :cond_5
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    if-nez v0, :cond_6

    .line 45
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempted to render a released OutputRenderer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_6
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    invoke-interface {v0}, Lfut;->makeCurrent()I

    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 49
    iget-object v1, p0, Lfpk;->videoSourceRenderer:Lfsp;

    invoke-virtual {v1}, Lfsp;->drawFrame()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 50
    iget-object v0, p0, Lfpk;->glSurface:Lfut;

    invoke-interface {v0}, Lfut;->swapBuffers()I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method final release()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lfpk;->videoSourceRenderer:Lfsp;

    invoke-virtual {v0}, Lfsp;->release()V

    .line 7
    invoke-direct {p0}, Lfpk;->releaseGlSurface()V

    .line 8
    return-void
.end method

.method final renderFrame()V
    .locals 4

    .prologue
    .line 15
    invoke-direct {p0}, Lfpk;->render()I

    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    const-string v1, "Failed to render; EGL error=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v0, p0, Lfpk;->this$0:Lfpc;

    iget-object v1, p0, Lfpk;->videoSource:Lfsm;

    invoke-virtual {v0, v1}, Lfpc;->removeVideoSource(Lfsm;)V

    .line 19
    :cond_0
    return-void
.end method
