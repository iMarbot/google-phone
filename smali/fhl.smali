.class public final Lfhl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfhg;


# instance fields
.field public final a:Lfvj;

.field private b:Landroid/content/Context;

.field private c:Lfhm;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfhm;

    .line 3
    invoke-direct {v0, p0}, Lfhm;-><init>(Lfhl;)V

    .line 4
    iput-object v0, p0, Lfhl;->c:Lfhm;

    .line 5
    new-instance v0, Lfiw;

    invoke-direct {v0}, Lfiw;-><init>()V

    invoke-static {}, Lfiw;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfhl;->d:Ljava/lang/String;

    .line 6
    const-string v0, "CallContextImpl constructor"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iput-object p1, p0, Lfhl;->b:Landroid/content/Context;

    .line 8
    new-instance v0, Lfvj;

    invoke-direct {v0, p1}, Lfvj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfhl;->a:Lfvj;

    .line 9
    return-void
.end method

.method static a(Lfvz;)Lfhk;
    .locals 4

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    if-eqz p0, :cond_0

    .line 242
    new-instance v1, Lfhk;

    invoke-direct {v1}, Lfhk;-><init>()V

    .line 244
    iget-boolean v0, p0, Lfvz;->d:Z

    .line 245
    iput-boolean v0, v1, Lfhk;->a:Z

    .line 247
    iget-wide v2, p0, Lfvz;->i:J

    .line 248
    iput-wide v2, v1, Lfhk;->b:J

    .line 251
    iget-object v0, p0, Lfvz;->n:Lgnm;

    .line 252
    if-nez v0, :cond_1

    .line 253
    const/4 v0, 0x0

    .line 256
    :goto_0
    iput v0, v1, Lfhk;->c:I

    move-object v0, v1

    .line 257
    :cond_0
    return-object v0

    .line 255
    :cond_1
    iget-object v0, p0, Lfvz;->n:Lgnm;

    .line 256
    iget-object v0, v0, Lgnm;->participantState:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private final a(III)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 108
    const-string v0, "CallContextImpl.leaveHangout, serviceEndCause=%d, protoEndCause=%d, callStartupEventCode=%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 109
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 110
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 113
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 114
    if-eqz v0, :cond_1

    .line 115
    const/16 v0, 0x2b0c

    if-ne p1, v0, :cond_0

    .line 117
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 118
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 119
    invoke-interface {v0, p2, p3}, Lfvr;->leaveWithAppError(II)V

    .line 128
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 122
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 123
    invoke-interface {v0}, Lfvr;->leave()V

    goto :goto_0

    .line 124
    :cond_1
    const-string v0, "CallContextImpl.leaveHangout, failed. No hangouts call."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    const-string v0, "CallContextImpl.leaveHangout, invoking callbackRelay to end the call."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    new-instance v0, Lfvx;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p3, v1}, Lfvx;-><init>(IIILjava/lang/String;)V

    .line 127
    iget-object v1, p0, Lfhl;->c:Lfhm;

    invoke-virtual {v1, v0}, Lfhm;->onCallEnd(Lfvx;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 104
    const/16 v0, 0x2afc

    const/4 v1, 0x0

    const/16 v2, 0xdb

    invoke-direct {p0, v0, v1, v2}, Lfhl;->a(III)V

    .line 105
    return-void
.end method

.method public final a(CI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 191
    const-string v1, "CallContextImpl.sendDtmf, char: "

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 194
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 195
    const-class v1, Lfwi;

    invoke-interface {v0, v1}, Lfvr;->as(Ljava/lang/Class;)Lfvr;

    move-result-object v0

    check-cast v0, Lfwi;

    .line 196
    invoke-interface {v0}, Lfwi;->getParticipants()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 197
    invoke-interface {v0}, Lfwi;->getParticipants()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 198
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfvz;

    .line 200
    iget-boolean v3, v1, Lfvz;->l:Z

    .line 201
    if-eqz v3, :cond_0

    .line 202
    const/16 v3, 0x15e

    .line 203
    iget-object v1, v1, Lfvz;->a:Ljava/lang/String;

    .line 204
    invoke-interface {v0, p1, v3, v1}, Lfwi;->sendDtmf(CILjava/lang/String;)V

    goto :goto_1

    .line 191
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_2
    const-string v0, "CallContextImpl.sendDtmf, no participants."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    :cond_3
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0x2b0c

    invoke-direct {p0, v0, p1, p2}, Lfhl;->a(III)V

    .line 107
    return-void
.end method

.method public final a(Ljava/lang/String;Lfhh;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 20
    const-string v1, "CallContextImpl.createHangout, account: "

    invoke-static {p1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    iget-object v0, p0, Lfhl;->a:Lfvj;

    new-instance v1, Lfvm;

    invoke-direct {v1, p2}, Lfvm;-><init>(Lfhh;)V

    .line 22
    iget-object v2, v0, Lfvj;->b:Lfmz;

    invoke-virtual {v2, p1}, Lfmz;->a(Ljava/lang/String;)Lfmy;

    move-result-object v2

    .line 23
    new-instance v3, Lfvn;

    invoke-direct {v3, v0, v2, v1}, Lfvn;-><init>(Lfvj;Lfmy;Lfvm;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v0, v1}, Lfvn;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 24
    return-void

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 25
    .line 26
    invoke-static {p1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x46

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallContextImpl.joinHangout, account: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hangoutId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isIncoming: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    .line 27
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    new-instance v1, Lfvs;

    invoke-direct {v1}, Lfvs;-><init>()V

    iget-object v0, p0, Lfhl;->d:Ljava/lang/String;

    .line 32
    iput-object v0, v1, Lfvs;->a:Ljava/lang/String;

    .line 36
    iput-object p3, v1, Lfvs;->b:Ljava/lang/String;

    .line 38
    if-eqz p4, :cond_0

    .line 39
    const/16 v0, 0x56

    .line 42
    :goto_0
    iput v0, v1, Lfvs;->c:I

    .line 46
    iput-object v6, v1, Lfvs;->e:Ljava/lang/String;

    .line 48
    invoke-static {}, Lfmd;->h()Lhgi;

    move-result-object v0

    .line 49
    iput-object v0, v1, Lfvs;->f:Lhgi;

    .line 53
    iput-object p2, v1, Lfvs;->g:Ljava/lang/String;

    .line 57
    iput-object p1, v1, Lfvs;->h:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lfhl;->b:Landroid/content/Context;

    .line 60
    invoke-static {v0}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 61
    iput-object v0, v1, Lfvs;->i:Ljava/lang/String;

    .line 65
    iput-boolean v4, v1, Lfvs;->k:Z

    .line 69
    iput-object v6, v1, Lfvs;->l:[B

    .line 71
    new-instance v0, Lgir;

    invoke-direct {v0}, Lgir;-><init>()V

    .line 72
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lgir;->d:Ljava/lang/Boolean;

    .line 75
    iput-object v0, v1, Lfvs;->m:Lgir;

    .line 78
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 79
    iget-object v2, v0, Lfvj;->d:Lfnp;

    .line 80
    const-string v3, "Expected null"

    invoke-static {v3, v2}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    new-instance v2, Lfnp;

    iget-object v3, v0, Lfvj;->a:Landroid/content/Context;

    iget-object v4, v0, Lfvj;->c:Lfvv;

    invoke-direct {v2, v3, v4, v1}, Lfnp;-><init>(Landroid/content/Context;Lfvv;Lfvs;)V

    iput-object v2, v0, Lfvj;->d:Lfnp;

    .line 82
    iget-object v2, v0, Lfvj;->d:Lfnp;

    new-instance v3, Lfvl;

    invoke-direct {v3, v0}, Lfvl;-><init>(Lfvj;)V

    invoke-virtual {v2, v3}, Lfnp;->addCallbacks(Lfvt;)V

    .line 83
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 85
    iget-object v2, p0, Lfhl;->c:Lfhm;

    invoke-interface {v0, v2}, Lfvr;->addCallbacks(Lfvt;)V

    .line 86
    invoke-interface {v0, v1}, Lfvr;->join(Lfvs;)V

    .line 87
    new-instance v1, Lfvp;

    invoke-direct {v1}, Lfvp;-><init>()V

    .line 88
    invoke-virtual {v1, v5}, Lfvp;->a(Z)V

    .line 89
    invoke-interface {v0, v1}, Lfvr;->setAudioCapturer(Lfvp;)V

    .line 90
    new-instance v1, Lfvq;

    invoke-direct {v1}, Lfvq;-><init>()V

    .line 91
    invoke-virtual {v1, v5}, Lfvq;->a(Z)V

    .line 92
    invoke-interface {v0, v1}, Lfvr;->setAudioController(Lfvq;)V

    .line 93
    return-void

    .line 40
    :cond_0
    const/16 v0, 0x55

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 94
    .line 95
    invoke-static {p2}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x4d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallContextImpl.invitePstnParticipant, phone number: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callerIdBlocked: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    .line 96
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lfhl;->b:Landroid/content/Context;

    .line 99
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 100
    iget-object v1, v1, Lfvj;->d:Lfnp;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    .line 102
    invoke-static/range {v0 .. v5}, Lfmd;->a(Landroid/content/Context;Lfvr;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 103
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 209
    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallContextImpl.muteMic, doMute: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 212
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 213
    if-eqz v1, :cond_1

    .line 214
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 215
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 216
    invoke-interface {v1}, Lfvr;->getAudioCapturer()Lfvp;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 218
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 219
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 220
    invoke-interface {v1}, Lfvr;->getAudioCapturer()Lfvp;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lfvp;->a(Z)V

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_1
    const-string v1, "CallContextImpl.muteMic, failed. No hangouts call."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lfhf;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    const-string v0, "CallContextImpl.addCallbacks"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lfhl;->c:Lfhm;

    .line 12
    const-string v1, "CallContextImpl.CallbackRelay.addCallbacks"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, v0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 14
    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    const-string v0, "CallContextImpl.release"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lfhl;->c:Lfhm;

    .line 133
    const-string v1, "CallContextImpl.CallbackRelay.cancel"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    iget-object v1, v0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    .line 135
    invoke-virtual {v0}, Lfhm;->a()V

    .line 137
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 138
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 139
    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 142
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 143
    iget-object v1, p0, Lfhl;->c:Lfhm;

    invoke-interface {v0, v1}, Lfvr;->removeCallbacks(Lfvt;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lfhl;->a:Lfvj;

    invoke-virtual {v0}, Lfvj;->a()V

    .line 145
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 223
    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallContextImpl.muteSpeaker, doMute: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 225
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 226
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 227
    if-eqz v1, :cond_1

    .line 228
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 229
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 230
    invoke-interface {v1}, Lfvr;->getAudioController()Lfvq;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 232
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 233
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 234
    invoke-interface {v1}, Lfvr;->getAudioController()Lfvq;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lfvq;->a(Z)V

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_1
    const-string v1, "CallContextImpl.muteSpeaker, failed. No hangouts call."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lfhf;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    const-string v0, "CallContextImpl.removeCallbacks"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Lfhl;->c:Lfhm;

    .line 17
    const-string v1, "CallContextImpl.CallbackRelay.removeCallbacks"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v0, v0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 19
    return v0
.end method

.method public final c()Lfhi;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 146
    .line 147
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 148
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 149
    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lfhl;->a:Lfvj;

    .line 152
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 153
    invoke-interface {v1}, Lfvr;->getCallStateInfo()Lfvu;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_0

    .line 156
    iget-object v2, v1, Lfvu;->b:Lfvs;

    .line 157
    if-eqz v2, :cond_0

    .line 158
    new-instance v0, Lfhi;

    invoke-direct {v0}, Lfhi;-><init>()V

    .line 160
    iget-object v2, v1, Lfvu;->b:Lfvs;

    .line 162
    iget-object v2, v2, Lfvs;->h:Ljava/lang/String;

    .line 163
    iput-object v2, v0, Lfhi;->a:Ljava/lang/String;

    .line 165
    iget-object v2, v1, Lfvu;->d:Ljava/lang/String;

    .line 166
    iput-object v2, v0, Lfhi;->b:Ljava/lang/String;

    .line 168
    iget-object v2, v1, Lfvu;->e:Ljava/lang/String;

    .line 169
    iput-object v2, v0, Lfhi;->c:Ljava/lang/String;

    .line 171
    iget v1, v1, Lfvu;->f:I

    .line 172
    iput v1, v0, Lfhi;->d:I

    .line 176
    :cond_0
    :goto_0
    return-object v0

    .line 175
    :cond_1
    const-string v1, "CallContextImpl.getCallInfo, no current call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d()Ljava/util/Map;
    .locals 5

    .prologue
    .line 177
    new-instance v2, Lggw;

    invoke-direct {v2}, Lggw;-><init>()V

    .line 179
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 180
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 181
    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 184
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 185
    invoke-interface {v0}, Lfvr;->getParticipants()Ljava/util/Map;

    move-result-object v3

    .line 186
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 187
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfvz;

    invoke-static {v1}, Lfhl;->a(Lfvz;)Lfhk;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 189
    :cond_0
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x3f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallContextImpl.getParticipants, participant count: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    return-object v2
.end method

.method final e()Lfvr;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lfhl;->a:Lfvj;

    .line 238
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 239
    return-object v0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 129
    invoke-virtual {p0}, Lfhl;->b()V

    .line 130
    return-void
.end method
