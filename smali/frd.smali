.class public final Lfrd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final EPSILON:D = 0.001

.field public static final SYNC_STATE_NONE:I = 0x0

.field public static final SYNC_STATE_STARTED:I = 0x1

.field public static final SYNC_STATE_SYNCED:I = 0x2


# instance fields
.field public final call:Lfvr;

.field public final collectionListener:Lfrg;

.field public isScreencast:Z

.field public syncState:I

.field public waitingForJoin:Z


# direct methods
.method public constructor <init>(Lfvr;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean v0, p0, Lfrd;->isScreencast:Z

    .line 3
    iput v0, p0, Lfrd;->syncState:I

    .line 4
    new-instance v0, Lfrg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfrg;-><init>(Lfrd;Lfre;)V

    iput-object v0, p0, Lfrd;->collectionListener:Lfrg;

    .line 5
    iput-object p1, p0, Lfrd;->call:Lfvr;

    .line 6
    return-void
.end method

.method static synthetic access$102(Lfrd;Z)Z
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lfrd;->waitingForJoin:Z

    return p1
.end method

.method static synthetic access$200(Lfrd;)Lfvr;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lfrd;->call:Lfvr;

    return-object v0
.end method

.method static synthetic access$302(Lfrd;I)I
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lfrd;->syncState:I

    return p1
.end method

.method private final floatsEqual(FF)Z
    .locals 4

    .prologue
    .line 15
    sub-float v0, p1, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final maybeSendUpdate()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 16
    iget-boolean v0, p0, Lfrd;->waitingForJoin:Z

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 18
    :cond_1
    iget-object v0, p0, Lfrd;->call:Lfvr;

    invoke-interface {v0}, Lfvr;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 19
    iput-boolean v3, p0, Lfrd;->waitingForJoin:Z

    .line 20
    iget-object v0, p0, Lfrd;->call:Lfvr;

    new-instance v1, Lfre;

    invoke-direct {v1, p0}, Lfre;-><init>(Lfrd;)V

    invoke-interface {v0, v1}, Lfvr;->addCallbacks(Lfvt;)V

    goto :goto_0

    .line 22
    :cond_2
    iget-object v0, p0, Lfrd;->call:Lfvr;

    invoke-interface {v0}, Lfvr;->getCollections()Lfnm;

    move-result-object v0

    .line 23
    const-class v1, Lfnh;

    .line 24
    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnh;

    .line 25
    iget v1, p0, Lfrd;->syncState:I

    packed-switch v1, :pswitch_data_0

    .line 30
    iget-object v1, p0, Lfrd;->call:Lfvr;

    .line 31
    invoke-interface {v1}, Lfvr;->getCollections()Lfnm;

    move-result-object v1

    const-class v4, Lfnf;

    .line 32
    invoke-virtual {v1, v4}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v1

    check-cast v1, Lfnf;

    .line 33
    invoke-interface {v1}, Lfnf;->getLocalParticipant()Lgnm;

    move-result-object v5

    .line 34
    const/4 v4, 0x0

    .line 35
    invoke-interface {v0}, Lfnh;->getResources()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgou;

    .line 36
    iget-object v7, v1, Lgou;->sourceId:Ljava/lang/String;

    const-string v8, "2"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, v1, Lgou;->participantId:Ljava/lang/String;

    iget-object v8, v5, Lgnm;->participantId:Ljava/lang/String;

    .line 37
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v4, v1

    .line 41
    :cond_4
    if-nez v4, :cond_5

    .line 42
    const-string v0, "Media source collection is missing a local video source. Skipping source updates."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 26
    :pswitch_1
    iput v3, p0, Lfrd;->syncState:I

    .line 27
    iget-object v1, p0, Lfrd;->collectionListener:Lfrg;

    invoke-interface {v0, v1}, Lfnh;->addListener(Lfnl;)V

    goto :goto_0

    .line 45
    :cond_5
    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    if-nez v1, :cond_6

    .line 46
    iget-boolean v1, p0, Lfrd;->isScreencast:Z

    .line 60
    :goto_1
    if-eqz v1, :cond_0

    .line 62
    new-instance v1, Lgpa;

    invoke-direct {v1}, Lgpa;-><init>()V

    .line 63
    :try_start_0
    invoke-static {v4}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v4

    invoke-static {v4}, Lgou;->parseFrom([B)Lgou;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 68
    new-instance v5, Lgoy;

    invoke-direct {v5}, Lgoy;-><init>()V

    iput-object v5, v4, Lgou;->videoDetails:Lgoy;

    .line 69
    iget-boolean v5, p0, Lfrd;->isScreencast:Z

    if-eqz v5, :cond_a

    .line 70
    new-instance v5, Lgoz;

    invoke-direct {v5}, Lgoz;-><init>()V

    .line 71
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lgoz;->y:Ljava/lang/Float;

    iput-object v6, v5, Lgoz;->x:Ljava/lang/Float;

    .line 72
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    iput-object v6, v5, Lgoz;->height:Ljava/lang/Float;

    iput-object v6, v5, Lgoz;->width:Ljava/lang/Float;

    .line 73
    iget-object v6, v4, Lgou;->videoDetails:Lgoy;

    new-array v7, v3, [Lgoz;

    aput-object v5, v7, v2

    iput-object v7, v6, Lgoy;->interestRegion:[Lgoz;

    .line 74
    iget-object v5, v4, Lgou;->videoDetails:Lgoy;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lgoy;->captureType:Ljava/lang/Integer;

    .line 77
    :goto_2
    new-array v3, v3, [Lgou;

    aput-object v4, v3, v2

    iput-object v3, v1, Lgpa;->resource:[Lgou;

    .line 78
    new-instance v2, Lfrf;

    invoke-direct {v2, p0}, Lfrf;-><init>(Lfrd;)V

    invoke-interface {v0, v1, v2}, Lfnh;->add(Lhfz;Lfnn;)V

    goto/16 :goto_0

    .line 47
    :cond_6
    iget-boolean v1, p0, Lfrd;->isScreencast:Z

    if-eqz v1, :cond_8

    .line 48
    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->captureType:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->captureType:Ljava/lang/Integer;

    .line 49
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v11, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    if-eqz v1, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    array-length v1, v1

    if-ne v1, v3, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->x:Ljava/lang/Float;

    .line 50
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1, v9}, Lfrd;->floatsEqual(FF)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->y:Ljava/lang/Float;

    .line 51
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1, v9}, Lfrd;->floatsEqual(FF)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->width:Ljava/lang/Float;

    .line 52
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1, v10}, Lfrd;->floatsEqual(FF)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->height:Ljava/lang/Float;

    .line 53
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1, v10}, Lfrd;->floatsEqual(FF)Z

    move-result v1

    if-nez v1, :cond_b

    :cond_7
    move v1, v3

    .line 54
    goto/16 :goto_1

    .line 55
    :cond_8
    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->captureType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_9

    .line 56
    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    if-eqz v1, :cond_b

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    array-length v1, v1

    if-ne v1, v3, :cond_b

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->width:Ljava/lang/Float;

    .line 57
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v9

    if-gtz v1, :cond_9

    iget-object v1, v4, Lgou;->videoDetails:Lgoy;

    iget-object v1, v1, Lgoy;->interestRegion:[Lgoz;

    aget-object v1, v1, v2

    iget-object v1, v1, Lgoz;->height:Ljava/lang/Float;

    .line 58
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_b

    :cond_9
    move v1, v3

    .line 59
    goto/16 :goto_1

    .line 65
    :catch_0
    move-exception v0

    .line 66
    invoke-virtual {v0}, Lhfy;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 76
    :cond_a
    iget-object v5, v4, Lgou;->videoDetails:Lgoy;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lgoy;->captureType:Ljava/lang/Integer;

    goto/16 :goto_2

    :cond_b
    move v1, v2

    goto/16 :goto_1

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final release()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lfrd;->call:Lfvr;

    .line 12
    invoke-interface {v0}, Lfvr;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnh;

    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnh;

    .line 13
    iget-object v1, p0, Lfrd;->collectionListener:Lfrg;

    invoke-interface {v0, v1}, Lfnh;->removeListener(Lfnl;)V

    .line 14
    return-void
.end method

.method public final setIsScreencast(Z)V
    .locals 2

    .prologue
    .line 7
    iput-boolean p1, p0, Lfrd;->isScreencast:Z

    .line 8
    const/16 v0, 0x24

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Set media source screencast to "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 9
    invoke-virtual {p0}, Lfrd;->maybeSendUpdate()V

    .line 10
    return-void
.end method
