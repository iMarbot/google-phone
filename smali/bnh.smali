.class public final Lbnh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbnb;


# instance fields
.field public a:Z

.field private b:Lalw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/content/ContentResolver;Ljava/lang/String;)Lgtm;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 89
    invoke-static {}, Lbdf;->c()V

    .line 90
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 91
    sget-object v0, Lgsz;->a:Lgsz;

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 94
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "data_id"

    aput-object v0, v2, v4

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 95
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 97
    if-nez v2, :cond_2

    .line 98
    :try_start_0
    sget-object v0, Lgsz;->a:Lgsz;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 100
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 102
    :cond_2
    :try_start_1
    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    .line 103
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 104
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 114
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 115
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_3

    if-eqz v3, :cond_6

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_3
    throw v0

    .line 105
    :cond_4
    :try_start_4
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v6, :cond_5

    .line 106
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 107
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 109
    :cond_5
    :try_start_5
    const-string v0, "CallingAccountSelector.getDataId"

    const-string v1, "lookup result not unique, ignoring"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    sget-object v0, Lgsz;->a:Lgsz;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 112
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 115
    :catch_1
    move-exception v1

    invoke-static {v3, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Lgtm;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 116
    invoke-static {}, Lbdf;->c()V

    .line 117
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbnr;->a:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "preferred_phone_account_component_name"

    aput-object v3, v2, v6

    const-string v3, "preferred_phone_account_id"

    aput-object v3, v2, v4

    const-string v3, "data_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    .line 120
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 122
    if-nez v2, :cond_1

    .line 123
    :try_start_0
    sget-object v0, Lgsz;->a:Lgsz;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 125
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 128
    sget-object v0, Lgsz;->a:Lgsz;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 130
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 132
    :cond_2
    const/4 v0, 0x0

    .line 133
    :try_start_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lgtm;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 135
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 138
    :catchall_0
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v5, :cond_4

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v5, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbnh;->a:Z

    .line 86
    iget-object v0, p0, Lbnh;->b:Lalw;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lbnh;->b:Lalw;

    invoke-virtual {v0}, Lalw;->dismiss()V

    .line 88
    :cond_0
    return-void
.end method

.method public final a(Lbne;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 17
    invoke-interface {p1}, Lbne;->a()Lbbh;

    move-result-object v2

    .line 18
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lbnh;->a(Landroid/content/Context;Lbbh;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v0, v2, Lbbh;->a:Landroid/net/Uri;

    .line 22
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 41
    const-string v3, "CallingAccountSelector.run"

    const-string v4, "unable to process scheme "

    .line 43
    iget-object v0, v2, Lbbh;->a:Landroid/net/Uri;

    .line 44
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    .line 45
    invoke-static {v3, v0, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :sswitch_0
    const-string v5, "voicemail"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v5, "tel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    .line 23
    :pswitch_0
    invoke-interface {p1}, Lbne;->d()Lbnf;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lbnh;->a(Lbne;Lbnf;Ljava/lang/String;Ljava/lang/String;Lbnx;)V

    .line 24
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dU:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 27
    :pswitch_1
    invoke-static {}, Lbdf;->b()V

    .line 28
    invoke-interface {p1}, Lbne;->a()Lbbh;

    move-result-object v5

    .line 29
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v4

    .line 31
    iget-object v0, v5, Lbbh;->a:Landroid/net/Uri;

    .line 32
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v6

    .line 33
    invoke-interface {p1}, Lbne;->d()Lbnf;

    move-result-object v3

    .line 34
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lbnj;

    invoke-direct {v1, v6}, Lbnj;-><init>(Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v7

    new-instance v0, Lbni;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lbni;-><init>(Lbnh;Lbne;Lbnf;Landroid/app/Activity;Lbbh;Ljava/lang/String;)V

    .line 37
    invoke-interface {v7, v0}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 38
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 39
    invoke-interface {v0, v4}, Lbdy;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 44
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 22
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3cb20217 -> :sswitch_0
        0x1c01b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Lbne;Lbnf;Ljava/lang/String;Ljava/lang/String;Lbnx;)V
    .locals 13

    .prologue
    .line 47
    invoke-static {}, Lbdf;->b()V

    .line 48
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->dT:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 49
    if-eqz p3, :cond_0

    .line 50
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->dV:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 51
    :cond_0
    if-eqz p5, :cond_1

    .line 52
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->dW:Lbkq$a;

    .line 53
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 54
    move-object/from16 v0, p5

    iget-object v1, v0, Lbnx;->b:Lbnw;

    invoke-virtual {v1}, Lbnw;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 61
    :cond_1
    :goto_0
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Landroid/telecom/TelecomManager;

    .line 62
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/TelecomManager;

    .line 63
    invoke-virtual {v1}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v10

    .line 64
    const v11, 0x7f110280

    if-eqz p3, :cond_3

    const/4 v1, 0x1

    move v9, v1

    :goto_1
    const v12, 0x7f110283

    new-instance v1, Lbnl;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lbnl;-><init>(Lbnh;Lbne;Lbnf;Ljava/lang/String;Ljava/lang/String;Lbnx;)V

    const/4 v7, 0x0

    .line 65
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v3

    .line 66
    if-nez p5, :cond_4

    .line 67
    const/4 v8, 0x0

    :cond_2
    move v2, v11

    move v3, v9

    move v4, v12

    move-object v5, v10

    move-object v6, v1

    .line 80
    invoke-static/range {v2 .. v8}, Lalw;->a(IZILjava/util/List;Lamd;Ljava/lang/String;Ljava/util/List;)Lalw;

    move-result-object v1

    iput-object v1, p0, Lbnh;->b:Lalw;

    .line 81
    iget-object v1, p0, Lbnh;->b:Lalw;

    .line 82
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "CallingAccountSelector"

    .line 83
    invoke-virtual {v1, v2, v3}, Lalw;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 84
    return-void

    .line 55
    :pswitch_0
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->dX:Lbkq$a;

    .line 56
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 58
    :pswitch_1
    invoke-interface {p1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->dY:Lbkq$a;

    .line 59
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 64
    :cond_3
    const/4 v1, 0x0

    move v9, v1

    goto :goto_1

    .line 68
    :cond_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 69
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccountHandle;

    .line 70
    move-object/from16 v0, p5

    iget-object v5, v0, Lbnx;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v2, v5}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 71
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 73
    :cond_5
    move-object/from16 v0, p5

    iget-object v2, v0, Lbnx;->b:Lbnw;

    invoke-virtual {v2}, Lbnw;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 78
    move-object/from16 v0, p5

    iget-object v1, v0, Lbnx;->b:Lbnw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected reason "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v1

    throw v1

    .line 74
    :pswitch_2
    const v2, 0x7f110282

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 76
    :pswitch_3
    const v2, 0x7f110281

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 73
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lbbh;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v3, "precall_calling_account_selector_enabled"

    .line 3
    invoke-interface {v0, v3, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 15
    :goto_0
    return v0

    .line 6
    :cond_0
    iget-object v0, p2, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 7
    if-eqz v0, :cond_1

    move v0, v1

    .line 8
    goto :goto_0

    .line 9
    :cond_1
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 10
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v2, :cond_2

    move v0, v1

    .line 12
    goto :goto_0

    .line 13
    :cond_2
    invoke-static {p1}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 14
    goto :goto_0

    :cond_3
    move v0, v2

    .line 15
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lbbh;)V
    .locals 0

    .prologue
    .line 16
    return-void
.end method
