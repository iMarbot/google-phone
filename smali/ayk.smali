.class public final Layk;
.super Landroid/widget/CursorAdapter;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;

.field public b:Layl;

.field private c:Landroid/view/View$OnClickListener;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Layk;->a:Ljava/util/List;

    .line 3
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p0, Layk;->c:Landroid/view/View$OnClickListener;

    .line 4
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Layk;->d:Landroid/content/Context;

    .line 5
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 19
    check-cast p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;

    .line 21
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    invoke-virtual {v0, p3}, Layl;->a(Landroid/database/Cursor;)V

    .line 22
    invoke-virtual {p1, v7}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a(Z)V

    .line 24
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 25
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 26
    iget-object v1, v1, Layl;->b:Ljava/lang/String;

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 28
    :cond_0
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 29
    iget-object v0, v0, Layl;->b:Ljava/lang/String;

    .line 30
    iput-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->c:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcsw;->c(Landroid/content/Context;)Lcte;

    move-result-object v0

    iget-object v1, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 32
    invoke-virtual {v1}, Layl;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcte;->a(Ljava/lang/Object;)Lctb;

    move-result-object v0

    sget-object v1, Ldct;->c:Ldct;

    .line 34
    new-instance v2, Ldgn;

    invoke-direct {v2}, Ldgn;-><init>()V

    invoke-virtual {v2, v1}, Ldgn;->a(Ldct;)Ldgn;

    move-result-object v1

    .line 35
    invoke-virtual {v1, v5}, Ldgn;->b(Z)Ldgn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lctb;->a(Ldgn;)Lctb;

    move-result-object v0

    .line 36
    invoke-static {}, Lddz;->b()Lddz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lctb;->a(Lcth;)Lctb;

    move-result-object v0

    iget-object v1, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    .line 37
    invoke-virtual {v0, v1}, Lctb;->a(Landroid/widget/ImageView;)Ldha;

    .line 38
    :cond_1
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 39
    iget-wide v0, v0, Layl;->d:J

    .line 41
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 42
    iget-object v2, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    .line 43
    invoke-virtual {p1}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110183

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 44
    invoke-virtual {v6, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v7

    .line 45
    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 51
    :goto_0
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 52
    iget-object v1, p0, Layk;->b:Layl;

    invoke-virtual {v0, v1}, Layl;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->setSelected(Z)V

    .line 53
    return-void

    .line 47
    :cond_2
    iget-object v0, p1, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    .line 48
    invoke-virtual {p1}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110184

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 6
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 7
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Layk;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    add-int/lit8 v0, p1, -0x1

    const/16 v1, 0x2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "couldn\'t move cursor to position "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    .line 9
    :cond_0
    if-nez p2, :cond_1

    .line 10
    iget-object v0, p0, Layk;->d:Landroid/content/Context;

    invoke-virtual {p0}, Layk;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Layk;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 12
    :goto_0
    iget-object v0, p0, Layk;->d:Landroid/content/Context;

    invoke-virtual {p0}, Layk;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 13
    if-nez p1, :cond_2

    move-object v0, v1

    .line 14
    check-cast v0, Lcom/android/dialer/callcomposer/GalleryGridItemView;

    .line 15
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a(Z)V

    .line 18
    :goto_1
    return-object v1

    :cond_1
    move-object v1, p2

    .line 11
    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p0, v1, v0, v2}, Layk;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_1
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040077

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/callcomposer/GalleryGridItemView;

    .line 56
    iget-object v1, p0, Layk;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v1, p0, Layk;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-object v0
.end method
