.class public final Lfbo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[B

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Lfbq;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lfbp;

    invoke-direct {v0}, Lfbp;-><init>()V

    sput-object v0, Lfbo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object v1, p0, Lfbo;->a:Ljava/lang/String;

    .line 3
    iput-object v1, p0, Lfbo;->b:Ljava/lang/String;

    .line 4
    iput-object v1, p0, Lfbo;->c:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lfbo;->d:Ljava/lang/String;

    .line 6
    iput-object v1, p0, Lfbo;->e:Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lfbo;->f:[B

    .line 8
    iput-object v1, p0, Lfbo;->g:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lfbo;->h:Ljava/lang/String;

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lfbo;->i:I

    .line 11
    iput-object v1, p0, Lfbo;->j:Lfbq;

    .line 12
    iput-object v1, p0, Lfbo;->k:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lfbo;->l:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lfbo;->m:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lfbo;->n:Ljava/lang/String;

    .line 16
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lfbo;->a:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lfbo;->b:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lfbo;->c:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lfbo;->d:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lfbo;->e:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lfbo;->f:[B

    .line 45
    iput-object v1, p0, Lfbo;->g:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lfbo;->h:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lfbo;->i:I

    .line 48
    iput-object v1, p0, Lfbo;->j:Lfbq;

    .line 49
    iput-object v1, p0, Lfbo;->k:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lfbo;->l:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lfbo;->m:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lfbo;->n:Ljava/lang/String;

    .line 53
    invoke-virtual {p0, p1}, Lfbo;->a(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->a:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->b:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->c:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->d:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->e:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 61
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 62
    iput-object v2, p0, Lfbo;->f:[B

    .line 65
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->g:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->h:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lfbo;->i:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 69
    if-nez v0, :cond_1

    .line 70
    iput-object v2, p0, Lfbo;->j:Lfbq;

    .line 72
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->k:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->l:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->m:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->n:Ljava/lang/String;

    .line 76
    return-void

    .line 63
    :cond_0
    new-array v0, v0, [B

    iput-object v0, p0, Lfbo;->f:[B

    .line 64
    iget-object v0, p0, Lfbo;->f:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    goto :goto_0

    .line 71
    :cond_1
    invoke-static {v0}, Lfbq;->a(Ljava/lang/String;)Lfbq;

    move-result-object v0

    iput-object v0, p0, Lfbo;->j:Lfbq;

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lfbo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lfbo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lfbo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lfbo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lfbo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lfbo;->f:[B

    if-nez v0, :cond_0

    .line 24
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 27
    :goto_0
    iget-object v0, p0, Lfbo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lfbo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget v0, p0, Lfbo;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 30
    iget-object v0, p0, Lfbo;->j:Lfbq;

    if-nez v0, :cond_1

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 33
    :goto_1
    iget-object v0, p0, Lfbo;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lfbo;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lfbo;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lfbo;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lfbo;->f:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 26
    iget-object v0, p0, Lfbo;->f:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lfbo;->j:Lfbq;

    invoke-virtual {v0}, Lfbq;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1
.end method
