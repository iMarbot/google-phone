.class final Latl;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic a:Latf$a;


# direct methods
.method constructor <init>(Latf$a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Latl;->a:Latf$a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 11
    .line 12
    iget-object v0, p0, Latl;->a:Latf$a;

    iget-object v0, v0, Latf$a;->c:Latf;

    iget-object v1, p0, Latl;->a:Latf$a;

    .line 13
    iget-object v1, v1, Latf$a;->a:Landroid/net/Uri;

    .line 15
    invoke-virtual {v0, v1}, Latf;->a(Landroid/net/Uri;)Z

    move-result v0

    .line 16
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 17
    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2
    check-cast p1, Ljava/lang/Boolean;

    .line 3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Latl;->a:Latf$a;

    iget-object v0, v0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latl;->a:Latf$a;

    .line 4
    iget-object v0, v0, Latf$a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 5
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Latl;->a:Latf$a;

    iget-object v0, v0, Latf$a;->c:Latf;

    iget-object v0, v0, Latf;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Latl;->a:Latf$a;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 7
    iget-object v0, p0, Latl;->a:Latf$a;

    iget-object v0, v0, Latf$a;->c:Latf;

    .line 8
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Latf;->d(Z)V

    .line 9
    iget-object v0, p0, Latl;->a:Latf$a;

    iget-object v0, v0, Latf$a;->c:Latf;

    invoke-virtual {v0}, Latf;->a()V

    .line 10
    :cond_0
    return-void
.end method
