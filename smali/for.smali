.class final Lfor;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final STATIC_LOCK_TIMEOUT_MILLIS:J

.field public static allSpecificationsSet:Z

.field public static incomingPrimaryVideo:Lpd;

.field public static incomingSecondaryVideo:Lpd;

.field public static final lock:Ljava/lang/Object;

.field public static maxOutgoingVideo:Lfwp;

.field public static outgoingVideo:Lpd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfor;->STATIC_LOCK_TIMEOUT_MILLIS:J

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lfor;->lock:Ljava/lang/Object;

    .line 37
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    sput-object v0, Lfor;->incomingPrimaryVideo:Lpd;

    .line 38
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    sput-object v0, Lfor;->incomingSecondaryVideo:Lpd;

    .line 39
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    sput-object v0, Lfor;->outgoingVideo:Lpd;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIncomingPrimaryVideoSpec(I)Lfwp;
    .locals 2

    .prologue
    .line 2
    invoke-static {}, Lfor;->waitForVideoSpecs()V

    .line 3
    sget-object v0, Lfor;->incomingPrimaryVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwp;

    return-object v0
.end method

.method public static getIncomingSecondaryVideoSpec(I)Lfwp;
    .locals 2

    .prologue
    .line 4
    invoke-static {}, Lfor;->waitForVideoSpecs()V

    .line 5
    sget-object v0, Lfor;->incomingSecondaryVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwp;

    return-object v0
.end method

.method public static getMaxOutgoingVideoSpec()Lfwp;
    .locals 1

    .prologue
    .line 6
    invoke-static {}, Lfor;->waitForVideoSpecs()V

    .line 7
    sget-object v0, Lfor;->maxOutgoingVideo:Lfwp;

    return-object v0
.end method

.method public static getOutgoingVideoSpec(I)Lfwp;
    .locals 2

    .prologue
    .line 8
    invoke-static {}, Lfor;->waitForVideoSpecs()V

    .line 9
    sget-object v0, Lfor;->outgoingVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwp;

    return-object v0
.end method

.method public static setComplete(Z)V
    .locals 2

    .prologue
    .line 21
    sget-object v1, Lfor;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    const/4 v0, 0x1

    :try_start_0
    sput-boolean v0, Lfor;->allSpecificationsSet:Z

    .line 23
    sget-object v0, Lfor;->lock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 24
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setIncomingPrimary(ILfwp;)V
    .locals 3

    .prologue
    .line 10
    sget-object v1, Lfor;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 11
    :try_start_0
    sget-object v0, Lfor;->incomingPrimaryVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setIncomingSecondary(ILfwp;)V
    .locals 3

    .prologue
    .line 13
    sget-object v1, Lfor;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 14
    :try_start_0
    sget-object v0, Lfor;->incomingSecondaryVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setOutgoing(ILfwp;)V
    .locals 3

    .prologue
    .line 16
    sget-object v1, Lfor;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lfor;->outgoingVideo:Lpd;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lfor;->maxOutgoingVideo:Lfwp;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lfwp;->a()I

    move-result v0

    sget-object v2, Lfor;->maxOutgoingVideo:Lfwp;

    invoke-virtual {v2}, Lfwp;->a()I

    move-result v2

    if-le v0, v2, :cond_1

    .line 19
    :cond_0
    sput-object p1, Lfor;->maxOutgoingVideo:Lfwp;

    .line 20
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static waitForVideoSpecs()V
    .locals 8

    .prologue
    .line 25
    sget-object v1, Lfor;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lfor;->STATIC_LOCK_TIMEOUT_MILLIS:J

    add-long/2addr v2, v4

    .line 27
    :cond_0
    sget-boolean v0, Lfor;->allSpecificationsSet:Z

    if-nez v0, :cond_1

    .line 28
    sget-object v0, Lfor;->lock:Ljava/lang/Object;

    sget-wide v4, Lfor;->STATIC_LOCK_TIMEOUT_MILLIS:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-ltz v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/RuntimeException;

    sget-wide v2, Lfor;->STATIC_LOCK_TIMEOUT_MILLIS:J

    const/16 v4, 0x44

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Video specs wait timed out (at least "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms elapsed)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    :catch_0
    move-exception v0

    .line 33
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method
