.class public final Lhlb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhlb;


# instance fields
.field public final b:Lhld;

.field public final c:Lhke;

.field public final d:Lhlw;

.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    new-instance v0, Lhlb;

    sget-object v1, Lhlw;->b:Lhlw;

    const/4 v2, 0x0

    invoke-direct {v0, v3, v3, v1, v2}, Lhlb;-><init>(Lhld;Lhke;Lhlw;Z)V

    sput-object v0, Lhlb;->a:Lhlb;

    return-void
.end method

.method private constructor <init>(Lhld;Lhke;Lhlw;Z)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhlb;->b:Lhld;

    .line 3
    iput-object p2, p0, Lhlb;->c:Lhke;

    .line 4
    const-string v0, "status"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    iput-object v0, p0, Lhlb;->d:Lhlw;

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhlb;->e:Z

    .line 6
    return-void
.end method

.method public static a(Lhld;)Lhlb;
    .locals 5

    .prologue
    .line 7
    .line 8
    new-instance v1, Lhlb;

    const-string v0, "subchannel"

    .line 9
    invoke-static {p0, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhld;

    const/4 v2, 0x0

    sget-object v3, Lhlw;->b:Lhlw;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lhlb;-><init>(Lhld;Lhke;Lhlw;Z)V

    .line 10
    return-object v1
.end method

.method public static a(Lhlw;)Lhlb;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 11
    invoke-virtual {p0}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "error status shouldn\'t be OK"

    invoke-static {v0, v2}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 12
    new-instance v0, Lhlb;

    invoke-direct {v0, v3, v3, p0, v1}, Lhlb;-><init>(Lhld;Lhke;Lhlw;Z)V

    return-object v0

    :cond_0
    move v0, v1

    .line 11
    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 29
    instance-of v1, p1, Lhlb;

    if-nez v1, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    check-cast p1, Lhlb;

    .line 32
    iget-object v1, p0, Lhlb;->b:Lhld;

    iget-object v2, p1, Lhlb;->b:Lhld;

    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhlb;->d:Lhlw;

    iget-object v2, p1, Lhlb;->d:Lhlw;

    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhlb;->c:Lhke;

    iget-object v2, p1, Lhlb;->c:Lhke;

    .line 33
    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lhlb;->e:Z

    iget-boolean v2, p1, Lhlb;->e:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lhlb;->b:Lhld;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lhlb;->d:Lhlw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lhlb;->c:Lhke;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lhlb;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 27
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 28
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 13
    invoke-static {p0}, Lhcw;->e(Ljava/lang/Object;)Lgtj;

    move-result-object v0

    const-string v1, "subchannel"

    iget-object v2, p0, Lhlb;->b:Lhld;

    .line 15
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Ljava/lang/Object;)Lgtj;

    move-result-object v0

    .line 16
    const-string v1, "streamTracerFactory"

    iget-object v2, p0, Lhlb;->c:Lhke;

    .line 18
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Ljava/lang/Object;)Lgtj;

    move-result-object v0

    .line 19
    const-string v1, "status"

    iget-object v2, p0, Lhlb;->d:Lhlw;

    .line 21
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Ljava/lang/Object;)Lgtj;

    move-result-object v0

    .line 22
    const-string v1, "drop"

    iget-boolean v2, p0, Lhlb;->e:Z

    .line 23
    invoke-virtual {v0, v1, v2}, Lgtj;->a(Ljava/lang/String;Z)Lgtj;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lgtj;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25
    return-object v0
.end method
