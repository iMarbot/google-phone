.class public final Ldgp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgh;
.implements Ldgo;
.implements Ldgz;
.implements Ldie;


# static fields
.field public static final a:Lps;


# instance fields
.field private A:I

.field public b:Ldgm;

.field public c:Ldgi;

.field public d:Landroid/content/Context;

.field public e:Lcsy;

.field public f:Ljava/lang/Object;

.field public g:Ljava/lang/Class;

.field public h:Ldgn;

.field public i:I

.field public j:I

.field public k:Lcsz;

.field public l:Ldha;

.field public m:Ldgm;

.field public n:Lcwd;

.field public o:Ldhk;

.field public p:I

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ldig;

.field private t:Lcwz;

.field private u:Lcwi;

.field private v:J

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 399
    const/16 v0, 0x96

    new-instance v1, Ldgq;

    invoke-direct {v1}, Ldgq;-><init>()V

    invoke-static {v0, v1}, Ldhy;->a(ILdic;)Lps;

    move-result-object v0

    sput-object v0, Ldgp;->a:Lps;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgp;->r:Ljava/lang/String;

    .line 4
    new-instance v0, Ldih;

    invoke-direct {v0}, Ldih;-><init>()V

    .line 5
    iput-object v0, p0, Ldgp;->s:Ldig;

    .line 6
    return-void
.end method

.method private static a(IF)I
    .locals 1

    .prologue
    .line 296
    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_0

    :goto_0
    return p0

    :cond_0
    int-to-float v0, p0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p0

    goto :goto_0
.end method

.method private final a(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 133
    iget-object v0, v0, Ldgn;->t:Landroid/content/res/Resources$Theme;

    .line 134
    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 136
    iget-object v0, v0, Ldgn;->t:Landroid/content/res/Resources$Theme;

    .line 138
    :goto_0
    iget-object v1, p0, Ldgp;->e:Lcsy;

    invoke-static {v1, p1, v0}, Lddx;->a(Landroid/content/Context;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    .line 137
    :cond_0
    iget-object v0, p0, Ldgp;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lcwt;I)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 346
    iget-object v1, p0, Ldgp;->s:Ldig;

    invoke-virtual {v1}, Ldig;->a()V

    .line 347
    iget-object v1, p0, Ldgp;->e:Lcsy;

    .line 348
    iget v1, v1, Lcsy;->g:I

    .line 350
    if-gt v1, p2, :cond_0

    .line 351
    const-string v2, "Glide"

    iget-object v3, p0, Ldgp;->f:Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Ldgp;->z:I

    iget v5, p0, Ldgp;->A:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x34

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Load failed for "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " with size ["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 352
    const/4 v2, 0x4

    if-gt v1, v2, :cond_0

    .line 353
    const-string v1, "Glide"

    invoke-virtual {p1, v1}, Lcwt;->a(Ljava/lang/String;)V

    .line 354
    :cond_0
    iput-object v0, p0, Ldgp;->u:Lcwi;

    .line 355
    sget v1, Lmg$c;->n:I

    iput v1, p0, Ldgp;->p:I

    .line 356
    const/4 v1, 0x1

    iput-boolean v1, p0, Ldgp;->q:Z

    .line 357
    :try_start_0
    iget-object v1, p0, Ldgp;->m:Ldgm;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldgp;->m:Ldgm;

    .line 358
    invoke-direct {p0}, Ldgp;->m()Z

    invoke-interface {v1, p1}, Ldgm;->a(Lcwt;)Z

    :cond_1
    iget-object v1, p0, Ldgp;->b:Ldgm;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldgp;->b:Ldgm;

    .line 359
    invoke-direct {p0}, Ldgp;->m()Z

    invoke-interface {v1, p1}, Ldgm;->a(Lcwt;)Z

    .line 361
    :cond_2
    invoke-direct {p0}, Ldgp;->l()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 363
    iget-object v1, p0, Ldgp;->f:Ljava/lang/Object;

    if-nez v1, :cond_3

    .line 364
    invoke-direct {p0}, Ldgp;->k()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 365
    :cond_3
    if-nez v0, :cond_5

    .line 367
    iget-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_4

    .line 368
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 369
    iget-object v0, v0, Ldgn;->d:Landroid/graphics/drawable/Drawable;

    .line 370
    iput-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    .line 371
    iget-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_4

    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 372
    iget v0, v0, Ldgn;->e:I

    .line 373
    if-lez v0, :cond_4

    .line 374
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 375
    iget v0, v0, Ldgn;->e:I

    .line 376
    invoke-direct {p0, v0}, Ldgp;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    .line 377
    :cond_4
    iget-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    .line 379
    :cond_5
    if-nez v0, :cond_6

    .line 380
    invoke-direct {p0}, Ldgp;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 381
    :cond_6
    iget-object v1, p0, Ldgp;->l:Ldha;

    invoke-interface {v1, v0}, Ldha;->c(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :cond_7
    iput-boolean v8, p0, Ldgp;->q:Z

    .line 386
    iget-object v0, p0, Ldgp;->c:Ldgi;

    if-eqz v0, :cond_8

    .line 387
    iget-object v0, p0, Ldgp;->c:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->d(Ldgh;)V

    .line 388
    :cond_8
    return-void

    .line 384
    :catchall_0
    move-exception v0

    iput-boolean v8, p0, Ldgp;->q:Z

    throw v0
.end method

.method private final a(Lcwz;)V
    .locals 2

    .prologue
    .line 100
    .line 101
    invoke-static {}, Ldhw;->a()V

    .line 102
    instance-of v0, p1, Lcwr;

    if-eqz v0, :cond_0

    .line 103
    check-cast p1, Lcwr;

    invoke-virtual {p1}, Lcwr;->f()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Ldgp;->t:Lcwz;

    .line 106
    return-void

    .line 104
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot release anything but an EngineResource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Ldgp;->r:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " this: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    return-void
.end method

.method private final i()V
    .locals 2

    .prologue
    .line 54
    iget-boolean v0, p0, Ldgp;->q:Z

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You can\'t start or clear loads in RequestListener or Target callbacks. If you\'re trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    return-void
.end method

.method private final j()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 112
    iget-object v0, v0, Ldgn;->f:Landroid/graphics/drawable/Drawable;

    .line 113
    iput-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    .line 114
    iget-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 115
    iget v0, v0, Ldgn;->g:I

    .line 116
    if-lez v0, :cond_0

    .line 117
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 118
    iget v0, v0, Ldgn;->g:I

    .line 119
    invoke-direct {p0, v0}, Ldgp;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    .line 120
    :cond_0
    iget-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private final k()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 123
    iget-object v0, v0, Ldgn;->n:Landroid/graphics/drawable/Drawable;

    .line 124
    iput-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    .line 125
    iget-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 126
    iget v0, v0, Ldgn;->o:I

    .line 127
    if-lez v0, :cond_0

    .line 128
    iget-object v0, p0, Ldgp;->h:Ldgn;

    .line 129
    iget v0, v0, Ldgn;->o:I

    .line 130
    invoke-direct {p0, v0}, Ldgp;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    .line 131
    :cond_0
    iget-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private final l()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Ldgp;->c:Ldgi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgp;->c:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->b(Ldgh;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final m()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Ldgp;->c:Ldgi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgp;->c:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0}, Ldgp;->i()V

    .line 30
    iget-object v0, p0, Ldgp;->s:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 31
    invoke-static {}, Ldhs;->a()J

    move-result-wide v0

    iput-wide v0, p0, Ldgp;->v:J

    .line 32
    iget-object v0, p0, Ldgp;->f:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 33
    iget v0, p0, Ldgp;->i:I

    iget v1, p0, Ldgp;->j:I

    invoke-static {v0, v1}, Ldhw;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget v0, p0, Ldgp;->i:I

    iput v0, p0, Ldgp;->z:I

    .line 35
    iget v0, p0, Ldgp;->j:I

    iput v0, p0, Ldgp;->A:I

    .line 36
    :cond_0
    invoke-direct {p0}, Ldgp;->k()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x5

    .line 37
    :goto_0
    new-instance v1, Lcwt;

    const-string v2, "Received null model"

    invoke-direct {v1, v2}, Lcwt;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Ldgp;->a(Lcwt;I)V

    .line 53
    :cond_1
    :goto_1
    return-void

    .line 36
    :cond_2
    const/4 v0, 0x3

    goto :goto_0

    .line 39
    :cond_3
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->k:I

    if-ne v0, v1, :cond_4

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot restart a running request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_4
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->m:I

    if-ne v0, v1, :cond_5

    .line 42
    iget-object v0, p0, Ldgp;->t:Lcwz;

    sget-object v1, Lctw;->e:Lctw;

    invoke-virtual {p0, v0, v1}, Ldgp;->a(Lcwz;Lctw;)V

    goto :goto_1

    .line 44
    :cond_5
    sget v0, Lmg$c;->l:I

    iput v0, p0, Ldgp;->p:I

    .line 45
    iget v0, p0, Ldgp;->i:I

    iget v1, p0, Ldgp;->j:I

    invoke-static {v0, v1}, Ldhw;->a(II)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 46
    iget v0, p0, Ldgp;->i:I

    iget v1, p0, Ldgp;->j:I

    invoke-virtual {p0, v0, v1}, Ldgp;->a(II)V

    .line 48
    :goto_2
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->k:I

    if-eq v0, v1, :cond_6

    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->l:I

    if-ne v0, v1, :cond_7

    .line 49
    :cond_6
    invoke-direct {p0}, Ldgp;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 50
    iget-object v0, p0, Ldgp;->l:Ldha;

    invoke-direct {p0}, Ldgp;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Ldha;->b(Landroid/graphics/drawable/Drawable;)V

    .line 51
    :cond_7
    const-string v0, "Request"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-wide v0, p0, Ldgp;->v:J

    invoke-static {v0, v1}, Ldhs;->a(J)D

    move-result-wide v0

    const/16 v2, 0x2f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "finished run method in "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldgp;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 47
    :cond_8
    iget-object v0, p0, Ldgp;->l:Ldha;

    invoke-interface {v0, p0}, Ldha;->a(Ldgz;)V

    goto :goto_2
.end method

.method public final a(II)V
    .locals 27

    .prologue
    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->s:Ldig;

    invoke-virtual {v2}, Ldig;->a()V

    .line 140
    const-string v2, "Request"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    move-object/from16 v0, p0

    iget-wide v2, v0, Ldgp;->v:J

    invoke-static {v2, v3}, Ldhs;->a(J)D

    move-result-wide v2

    const/16 v4, 0x2b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Got onSizeReady in "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldgp;->a(Ljava/lang/String;)V

    .line 142
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Ldgp;->p:I

    sget v3, Lmg$c;->l:I

    if-eq v2, v3, :cond_2

    .line 295
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    sget v2, Lmg$c;->k:I

    move-object/from16 v0, p0

    iput v2, v0, Ldgp;->p:I

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 146
    iget v2, v2, Ldgn;->a:F

    .line 148
    move/from16 v0, p1

    invoke-static {v0, v2}, Ldgp;->a(IF)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Ldgp;->z:I

    .line 149
    move/from16 v0, p2

    invoke-static {v0, v2}, Ldgp;->a(IF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Ldgp;->A:I

    .line 150
    const-string v2, "Request"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 151
    move-object/from16 v0, p0

    iget-wide v2, v0, Ldgp;->v:J

    invoke-static {v2, v3}, Ldhs;->a(J)D

    move-result-wide v2

    const/16 v4, 0x3b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "finished setup for calling load in "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldgp;->a(Ljava/lang/String;)V

    .line 152
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Ldgp;->n:Lcwd;

    move-object/from16 v0, p0

    iget-object v14, v0, Ldgp;->e:Lcsy;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldgp;->f:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 154
    iget-object v4, v2, Ldgn;->k:Lcud;

    .line 155
    move-object/from16 v0, p0

    iget v5, v0, Ldgp;->z:I

    move-object/from16 v0, p0

    iget v6, v0, Ldgp;->A:I

    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 157
    iget-object v8, v2, Ldgn;->r:Ljava/lang/Class;

    .line 158
    move-object/from16 v0, p0

    iget-object v9, v0, Ldgp;->g:Ljava/lang/Class;

    move-object/from16 v0, p0

    iget-object v15, v0, Ldgp;->k:Lcsz;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 160
    iget-object v0, v2, Ldgn;->b:Lcvx;

    move-object/from16 v16, v0

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 163
    iget-object v7, v2, Ldgn;->q:Ljava/util/Map;

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 166
    iget-boolean v0, v2, Ldgn;->l:Z

    move/from16 v17, v0

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 169
    iget-boolean v0, v2, Ldgn;->w:Z

    move/from16 v18, v0

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 172
    iget-object v10, v2, Ldgn;->p:Lcuh;

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 175
    iget-boolean v0, v2, Ldgn;->h:Z

    move/from16 v19, v0

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 178
    iget-boolean v0, v2, Ldgn;->u:Z

    move/from16 v20, v0

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 181
    iget-boolean v0, v2, Ldgn;->x:Z

    move/from16 v21, v0

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Ldgp;->h:Ldgn;

    .line 184
    iget-boolean v0, v2, Ldgn;->v:Z

    move/from16 v22, v0

    .line 187
    invoke-static {}, Ldhw;->a()V

    .line 188
    invoke-static {}, Ldhs;->a()J

    move-result-wide v24

    .line 190
    new-instance v2, Lcwp;

    invoke-direct/range {v2 .. v10}, Lcwp;-><init>(Ljava/lang/Object;Lcud;IILjava/util/Map;Ljava/lang/Class;Ljava/lang/Class;Lcuh;)V

    .line 193
    if-nez v19, :cond_6

    .line 194
    const/4 v11, 0x0

    .line 209
    :cond_4
    :goto_1
    if-eqz v11, :cond_9

    .line 210
    sget-object v3, Lctw;->e:Lctw;

    move-object/from16 v0, p0

    invoke-interface {v0, v11, v3}, Ldgo;->a(Lcwz;Lctw;)V

    .line 211
    const-string v3, "Engine"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 212
    const-string v3, "Loaded resource from cache"

    move-wide/from16 v0, v24

    invoke-static {v3, v0, v1, v2}, Lcwd;->a(Ljava/lang/String;JLcud;)V

    .line 213
    :cond_5
    const/4 v2, 0x0

    .line 292
    :goto_2
    move-object/from16 v0, p0

    iput-object v2, v0, Ldgp;->u:Lcwi;

    .line 293
    const-string v2, "Request"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 294
    move-object/from16 v0, p0

    iget-wide v2, v0, Ldgp;->v:J

    invoke-static {v2, v3}, Ldhs;->a(J)D

    move-result-wide v2

    const/16 v4, 0x30

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "finished onSizeReady in "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldgp;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 196
    :cond_6
    iget-object v11, v13, Lcwd;->b:Lcym;

    invoke-interface {v11, v2}, Lcym;->a(Lcud;)Lcwz;

    move-result-object v11

    .line 197
    if-nez v11, :cond_7

    .line 198
    const/4 v11, 0x0

    .line 204
    :goto_3
    if-eqz v11, :cond_4

    .line 205
    invoke-virtual {v11}, Lcwr;->e()V

    .line 206
    iget-object v12, v13, Lcwd;->d:Ljava/util/Map;

    new-instance v23, Lcwk;

    invoke-virtual {v13}, Lcwd;->a()Ljava/lang/ref/ReferenceQueue;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v2, v11, v1}, Lcwk;-><init>(Lcud;Lcwr;Ljava/lang/ref/ReferenceQueue;)V

    move-object/from16 v0, v23

    invoke-interface {v12, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 199
    :cond_7
    instance-of v12, v11, Lcwr;

    if-eqz v12, :cond_8

    .line 200
    check-cast v11, Lcwr;

    goto :goto_3

    .line 201
    :cond_8
    new-instance v12, Lcwr;

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-direct {v12, v11, v0}, Lcwr;-><init>(Lcwz;Z)V

    move-object v11, v12

    goto :goto_3

    .line 215
    :cond_9
    if-nez v19, :cond_b

    .line 216
    const/4 v11, 0x0

    .line 226
    :goto_4
    if-eqz v11, :cond_d

    .line 227
    sget-object v3, Lctw;->e:Lctw;

    move-object/from16 v0, p0

    invoke-interface {v0, v11, v3}, Ldgo;->a(Lcwz;Lctw;)V

    .line 228
    const-string v3, "Engine"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 229
    const-string v3, "Loaded resource from active resources"

    move-wide/from16 v0, v24

    invoke-static {v3, v0, v1, v2}, Lcwd;->a(Ljava/lang/String;JLcud;)V

    .line 230
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 217
    :cond_b
    const/4 v12, 0x0

    .line 218
    iget-object v11, v13, Lcwd;->d:Ljava/util/Map;

    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/ref/WeakReference;

    .line 219
    if-eqz v11, :cond_14

    .line 220
    invoke-virtual {v11}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcwr;

    .line 221
    if-eqz v11, :cond_c

    .line 222
    invoke-virtual {v11}, Lcwr;->e()V

    goto :goto_4

    .line 223
    :cond_c
    iget-object v12, v13, Lcwd;->d:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 231
    :cond_d
    iget-object v11, v13, Lcwd;->a:Ljava/util/Map;

    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcwl;

    .line 232
    if-eqz v11, :cond_f

    .line 233
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcwl;->a(Ldgo;)V

    .line 234
    const-string v3, "Engine"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 235
    const-string v3, "Added to existing load"

    move-wide/from16 v0, v24

    invoke-static {v3, v0, v1, v2}, Lcwd;->a(Ljava/lang/String;JLcud;)V

    .line 236
    :cond_e
    new-instance v2, Lcwi;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v11}, Lcwi;-><init>(Ldgo;Lcwl;)V

    goto/16 :goto_2

    .line 237
    :cond_f
    iget-object v11, v13, Lcwd;->c:Lcwg;

    .line 238
    iget-object v11, v11, Lcwg;->f:Lps;

    invoke-interface {v11}, Lps;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcwl;

    .line 240
    iput-object v2, v11, Lcwl;->e:Lcud;

    .line 241
    move/from16 v0, v19

    iput-boolean v0, v11, Lcwl;->f:Z

    .line 242
    move/from16 v0, v20

    iput-boolean v0, v11, Lcwl;->g:Z

    .line 243
    move/from16 v0, v21

    iput-boolean v0, v11, Lcwl;->h:Z

    .line 246
    iget-object v0, v13, Lcwd;->e:Lcwe;

    move-object/from16 v19, v0

    .line 247
    move-object/from16 v0, v19

    iget-object v12, v0, Lcwe;->b:Lps;

    invoke-interface {v12}, Lps;->a()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcvo;

    .line 248
    move-object/from16 v0, v19

    iget v0, v0, Lcwe;->c:I

    move/from16 v20, v0

    add-int/lit8 v21, v20, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v19

    iput v0, v1, Lcwe;->c:I

    .line 249
    iget-object v0, v12, Lcvo;->a:Lcvn;

    move-object/from16 v19, v0

    iget-object v0, v12, Lcvo;->b:Lcvr;

    move-object/from16 v21, v0

    .line 250
    move-object/from16 v0, v19

    iput-object v14, v0, Lcvn;->c:Lcsy;

    .line 251
    move-object/from16 v0, v19

    iput-object v3, v0, Lcvn;->d:Ljava/lang/Object;

    .line 252
    move-object/from16 v0, v19

    iput-object v4, v0, Lcvn;->n:Lcud;

    .line 253
    move-object/from16 v0, v19

    iput v5, v0, Lcvn;->e:I

    .line 254
    move-object/from16 v0, v19

    iput v6, v0, Lcvn;->f:I

    .line 255
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    iput-object v0, v1, Lcvn;->p:Lcvx;

    .line 256
    move-object/from16 v0, v19

    iput-object v8, v0, Lcvn;->g:Ljava/lang/Class;

    .line 257
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    iput-object v0, v1, Lcvn;->h:Lcvr;

    .line 258
    move-object/from16 v0, v19

    iput-object v9, v0, Lcvn;->k:Ljava/lang/Class;

    .line 259
    move-object/from16 v0, v19

    iput-object v15, v0, Lcvn;->o:Lcsz;

    .line 260
    move-object/from16 v0, v19

    iput-object v10, v0, Lcvn;->i:Lcuh;

    .line 261
    move-object/from16 v0, v19

    iput-object v7, v0, Lcvn;->j:Ljava/util/Map;

    .line 262
    move/from16 v0, v17

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcvn;->q:Z

    .line 263
    move/from16 v0, v18

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcvn;->r:Z

    .line 264
    iput-object v14, v12, Lcvo;->e:Lcsy;

    .line 265
    iput-object v4, v12, Lcvo;->f:Lcud;

    .line 266
    iput-object v15, v12, Lcvo;->g:Lcsz;

    .line 267
    iput-object v2, v12, Lcvo;->h:Lcwp;

    .line 268
    iput v5, v12, Lcvo;->i:I

    .line 269
    iput v6, v12, Lcvo;->j:I

    .line 270
    move-object/from16 v0, v16

    iput-object v0, v12, Lcvo;->k:Lcvx;

    .line 271
    move/from16 v0, v22

    iput-boolean v0, v12, Lcvo;->p:Z

    .line 272
    iput-object v10, v12, Lcvo;->l:Lcuh;

    .line 273
    iput-object v11, v12, Lcvo;->m:Lcvp;

    .line 274
    move/from16 v0, v20

    iput v0, v12, Lcvo;->n:I

    .line 275
    sget-object v3, Lcvt;->a:Lcvt;

    iput-object v3, v12, Lcvo;->o:Lcvt;

    .line 278
    iget-object v3, v13, Lcwd;->a:Ljava/util/Map;

    invoke-interface {v3, v2, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcwl;->a(Ldgo;)V

    .line 281
    iput-object v12, v11, Lcwl;->o:Lcvo;

    .line 283
    sget-object v3, Lcvu;->a:Lcvu;

    invoke-virtual {v12, v3}, Lcvo;->a(Lcvu;)Lcvu;

    move-result-object v3

    .line 284
    sget-object v4, Lcvu;->b:Lcvu;

    if-eq v3, v4, :cond_10

    sget-object v4, Lcvu;->c:Lcvu;

    if-ne v3, v4, :cond_12

    :cond_10
    const/4 v3, 0x1

    .line 285
    :goto_5
    if-eqz v3, :cond_13

    .line 286
    iget-object v3, v11, Lcwl;->d:Lcyu;

    .line 288
    :goto_6
    invoke-virtual {v3, v12}, Lcyu;->execute(Ljava/lang/Runnable;)V

    .line 289
    const-string v3, "Engine"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 290
    const-string v3, "Started new load"

    move-wide/from16 v0, v24

    invoke-static {v3, v0, v1, v2}, Lcwd;->a(Ljava/lang/String;JLcud;)V

    .line 291
    :cond_11
    new-instance v2, Lcwi;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v11}, Lcwi;-><init>(Ldgo;Lcwl;)V

    goto/16 :goto_2

    .line 284
    :cond_12
    const/4 v3, 0x0

    goto :goto_5

    .line 287
    :cond_13
    invoke-virtual {v11}, Lcwl;->a()Lcyu;

    move-result-object v3

    goto :goto_6

    :cond_14
    move-object v11, v12

    goto/16 :goto_4
.end method

.method public final a(Lcwt;)V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Ldgp;->a(Lcwt;I)V

    .line 345
    return-void
.end method

.method public final a(Lcwz;Lctw;)V
    .locals 13

    .prologue
    const/4 v8, 0x5

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 299
    iget-object v0, p0, Ldgp;->s:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Ldgp;->u:Lcwi;

    .line 301
    if-nez p1, :cond_1

    .line 302
    new-instance v0, Lcwt;

    iget-object v1, p0, Ldgp;->g:Ljava/lang/Class;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x52

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Expected to receive a Resource<R> with an object of "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " inside, but instead got null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcwt;-><init>(Ljava/lang/String;)V

    .line 304
    invoke-direct {p0, v0, v8}, Ldgp;->a(Lcwt;I)V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    invoke-interface {p1}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v1

    .line 307
    if-eqz v1, :cond_2

    iget-object v0, p0, Ldgp;->g:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 308
    :cond_2
    invoke-direct {p0, p1}, Ldgp;->a(Lcwz;)V

    .line 309
    new-instance v2, Lcwt;

    iget-object v0, p0, Ldgp;->g:Ljava/lang/Class;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 310
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 311
    if-eqz v1, :cond_4

    const-string v0, ""

    :goto_2
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x47

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v1, v7

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v1, v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v1, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v1, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Expected to receive an object of "

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but instead got "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "} inside Resource{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "}."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcwt;-><init>(Ljava/lang/String;)V

    .line 313
    invoke-direct {p0, v2, v8}, Ldgp;->a(Lcwt;I)V

    goto/16 :goto_0

    .line 310
    :cond_3
    const-string v0, ""

    goto/16 :goto_1

    .line 311
    :cond_4
    const-string v0, " To indicate failure return a null Resource object, rather than a Resource object containing null data."

    goto :goto_2

    .line 316
    :cond_5
    iget-object v0, p0, Ldgp;->c:Ldgi;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ldgp;->c:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->a(Ldgh;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    .line 317
    :goto_3
    if-nez v0, :cond_8

    .line 318
    invoke-direct {p0, p1}, Ldgp;->a(Lcwz;)V

    .line 319
    sget v0, Lmg$c;->m:I

    iput v0, p0, Ldgp;->p:I

    goto/16 :goto_0

    :cond_7
    move v0, v6

    .line 316
    goto :goto_3

    .line 322
    :cond_8
    invoke-direct {p0}, Ldgp;->m()Z

    move-result v5

    .line 323
    sget v0, Lmg$c;->m:I

    iput v0, p0, Ldgp;->p:I

    .line 324
    iput-object p1, p0, Ldgp;->t:Lcwz;

    .line 325
    iget-object v0, p0, Ldgp;->e:Lcsy;

    .line 326
    iget v0, v0, Lcsy;->g:I

    .line 327
    const/4 v3, 0x3

    if-gt v0, v3, :cond_9

    .line 328
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldgp;->f:Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget v7, p0, Ldgp;->z:I

    iget v8, p0, Ldgp;->A:I

    iget-wide v10, p0, Ldgp;->v:J

    .line 329
    invoke-static {v10, v11}, Ldhs;->a(J)D

    move-result-wide v10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x5f

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v9, v12

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v9, v12

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Finished loading "

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " from "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " with size ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] in "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ms"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    :cond_9
    iput-boolean v2, p0, Ldgp;->q:Z

    .line 331
    :try_start_0
    iget-object v0, p0, Ldgp;->m:Ldgm;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ldgp;->m:Ldgm;

    iget-object v2, p0, Ldgp;->f:Ljava/lang/Object;

    iget-object v3, p0, Ldgp;->l:Ldha;

    move-object v4, p2

    .line 332
    invoke-interface/range {v0 .. v5}, Ldgm;->a(Ljava/lang/Object;Ljava/lang/Object;Ldha;Lctw;Z)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_a
    iget-object v0, p0, Ldgp;->b:Ldgm;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ldgp;->b:Ldgm;

    iget-object v2, p0, Ldgp;->f:Ljava/lang/Object;

    iget-object v3, p0, Ldgp;->l:Ldha;

    move-object v4, p2

    .line 333
    invoke-interface/range {v0 .. v5}, Ldgm;->a(Ljava/lang/Object;Ljava/lang/Object;Ldha;Lctw;Z)Z

    move-result v0

    if-nez v0, :cond_c

    .line 334
    :cond_b
    iget-object v0, p0, Ldgp;->o:Ldhk;

    .line 335
    invoke-interface {v0, p2}, Ldhk;->a(Lctw;)Ldhi;

    move-result-object v0

    .line 336
    iget-object v2, p0, Ldgp;->l:Ldha;

    invoke-interface {v2, v1, v0}, Ldha;->a(Ljava/lang/Object;Ldhi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    :cond_c
    iput-boolean v6, p0, Ldgp;->q:Z

    .line 341
    iget-object v0, p0, Ldgp;->c:Ldgi;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Ldgp;->c:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->c(Ldgh;)V

    goto/16 :goto_0

    .line 339
    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Ldgp;->q:Z

    throw v0
.end method

.method public final a(Ldgh;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 389
    instance-of v1, p1, Ldgp;

    if-eqz v1, :cond_0

    .line 390
    check-cast p1, Ldgp;

    .line 391
    iget v1, p0, Ldgp;->i:I

    iget v2, p1, Ldgp;->i:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Ldgp;->j:I

    iget v2, p1, Ldgp;->j:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgp;->f:Ljava/lang/Object;

    iget-object v2, p1, Ldgp;->f:Ljava/lang/Object;

    .line 392
    invoke-static {v1, v2}, Ldhw;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgp;->g:Ljava/lang/Class;

    iget-object v2, p1, Ldgp;->g:Ljava/lang/Class;

    .line 393
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgp;->h:Ldgn;

    iget-object v2, p1, Ldgp;->h:Ldgn;

    .line 394
    invoke-virtual {v1, v2}, Ldgn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldgp;->k:Lcsz;

    iget-object v2, p1, Ldgp;->k:Lcsz;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Ldgp;->m:Ldgm;

    if-eqz v1, :cond_1

    iget-object v1, p1, Ldgp;->m:Ldgm;

    if-eqz v1, :cond_0

    :goto_0
    const/4 v0, 0x1

    .line 396
    :cond_0
    return v0

    .line 394
    :cond_1
    iget-object v1, p1, Ldgp;->m:Ldgm;

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Ldgp;->d()V

    .line 98
    sget v0, Lmg$c;->q:I

    iput v0, p0, Ldgp;->p:I

    .line 99
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    invoke-static {}, Ldhw;->a()V

    .line 58
    invoke-direct {p0}, Ldgp;->i()V

    .line 59
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->p:I

    if-ne v0, v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-direct {p0}, Ldgp;->i()V

    .line 63
    iget-object v0, p0, Ldgp;->s:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 64
    iget-object v0, p0, Ldgp;->l:Ldha;

    invoke-interface {v0, p0}, Ldha;->b(Ldgz;)V

    .line 65
    sget v0, Lmg$c;->o:I

    iput v0, p0, Ldgp;->p:I

    .line 66
    iget-object v0, p0, Ldgp;->u:Lcwi;

    if-eqz v0, :cond_4

    .line 67
    iget-object v0, p0, Ldgp;->u:Lcwi;

    .line 68
    iget-object v1, v0, Lcwi;->a:Lcwl;

    iget-object v0, v0, Lcwi;->b:Ldgo;

    .line 69
    invoke-static {}, Ldhw;->a()V

    .line 70
    iget-object v2, v1, Lcwl;->b:Ldig;

    invoke-virtual {v2}, Ldig;->a()V

    .line 71
    iget-boolean v2, v1, Lcwl;->k:Z

    if-nez v2, :cond_1

    iget-boolean v2, v1, Lcwl;->l:Z

    if-eqz v2, :cond_7

    .line 73
    :cond_1
    iget-object v2, v1, Lcwl;->m:Ljava/util/List;

    if-nez v2, :cond_2

    .line 74
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v1, Lcwl;->m:Ljava/util/List;

    .line 75
    :cond_2
    iget-object v2, v1, Lcwl;->m:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    iget-object v1, v1, Lcwl;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_3
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Ldgp;->u:Lcwi;

    .line 91
    :cond_4
    iget-object v0, p0, Ldgp;->t:Lcwz;

    if-eqz v0, :cond_5

    .line 92
    iget-object v0, p0, Ldgp;->t:Lcwz;

    invoke-direct {p0, v0}, Ldgp;->a(Lcwz;)V

    .line 93
    :cond_5
    invoke-direct {p0}, Ldgp;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 94
    iget-object v0, p0, Ldgp;->l:Ldha;

    invoke-direct {p0}, Ldgp;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Ldha;->a(Landroid/graphics/drawable/Drawable;)V

    .line 95
    :cond_6
    sget v0, Lmg$c;->p:I

    iput v0, p0, Ldgp;->p:I

    goto :goto_0

    .line 78
    :cond_7
    iget-object v2, v1, Lcwl;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, v1, Lcwl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81
    iget-boolean v0, v1, Lcwl;->l:Z

    if-nez v0, :cond_3

    iget-boolean v0, v1, Lcwl;->k:Z

    if-nez v0, :cond_3

    iget-boolean v0, v1, Lcwl;->p:Z

    if-nez v0, :cond_3

    .line 83
    iput-boolean v3, v1, Lcwl;->p:Z

    .line 84
    iget-object v0, v1, Lcwl;->o:Lcvo;

    .line 85
    iput-boolean v3, v0, Lcvo;->s:Z

    .line 86
    iget-object v0, v0, Lcvo;->r:Lcvl;

    .line 87
    if-eqz v0, :cond_8

    .line 88
    invoke-interface {v0}, Lcvl;->b()V

    .line 89
    :cond_8
    iget-object v0, v1, Lcwl;->c:Lcwo;

    iget-object v2, v1, Lcwl;->e:Lcud;

    invoke-interface {v0, v1, v2}, Lcwo;->a(Lcwl;Lcud;)V

    goto :goto_1
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->k:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->l:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->m:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->o:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldgp;->p:I

    sget v1, Lmg$c;->p:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ldgp;->i()V

    .line 9
    iput-object v0, p0, Ldgp;->d:Landroid/content/Context;

    .line 10
    iput-object v0, p0, Ldgp;->e:Lcsy;

    .line 11
    iput-object v0, p0, Ldgp;->f:Ljava/lang/Object;

    .line 12
    iput-object v0, p0, Ldgp;->g:Ljava/lang/Class;

    .line 13
    iput-object v0, p0, Ldgp;->h:Ldgn;

    .line 14
    iput v1, p0, Ldgp;->i:I

    .line 15
    iput v1, p0, Ldgp;->j:I

    .line 16
    iput-object v0, p0, Ldgp;->l:Ldha;

    .line 17
    iput-object v0, p0, Ldgp;->m:Ldgm;

    .line 18
    iput-object v0, p0, Ldgp;->b:Ldgm;

    .line 19
    iput-object v0, p0, Ldgp;->c:Ldgi;

    .line 20
    iput-object v0, p0, Ldgp;->o:Ldhk;

    .line 21
    iput-object v0, p0, Ldgp;->u:Lcwi;

    .line 22
    iput-object v0, p0, Ldgp;->w:Landroid/graphics/drawable/Drawable;

    .line 23
    iput-object v0, p0, Ldgp;->x:Landroid/graphics/drawable/Drawable;

    .line 24
    iput-object v0, p0, Ldgp;->y:Landroid/graphics/drawable/Drawable;

    .line 25
    iput v1, p0, Ldgp;->z:I

    .line 26
    iput v1, p0, Ldgp;->A:I

    .line 27
    sget-object v0, Ldgp;->a:Lps;

    invoke-interface {v0, p0}, Lps;->a(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public final u_()Ldig;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Ldgp;->s:Ldig;

    return-object v0
.end method
