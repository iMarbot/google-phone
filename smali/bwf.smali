.class public final Lbwf;
.super Landroid/view/OrientationEventListener;
.source "PG"


# static fields
.field public static a:I


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput v0, Lbwf;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwf;->b:Z

    .line 3
    return-void
.end method

.method private static a(III)Z
    .locals 1

    .prologue
    .line 4
    if-lt p0, p1, :cond_0

    if-ge p0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(III)Z
    .locals 2

    .prologue
    .line 5
    add-int/lit8 v0, p1, -0xa

    add-int/lit8 v1, p1, 0xa

    invoke-static {p0, v0, v1}, Lbwf;->a(III)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lbwf;->b:Z

    if-eqz v0, :cond_1

    .line 33
    const-string v0, "enable: Orientation listener is already enabled. Ignoring..."

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbwf;->b:Z

    .line 37
    if-eqz p1, :cond_0

    .line 38
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    sget v1, Lbwf;->a:I

    invoke-virtual {v0, v1}, Lbwg;->a(I)V

    goto :goto_0
.end method

.method public final disable()V
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lbwf;->b:Z

    if-nez v0, :cond_0

    .line 43
    const-string v0, "enable: Orientation listener is already disabled. Ignoring..."

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbwf;->b:Z

    .line 46
    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    goto :goto_0
.end method

.method public final enable()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbwf;->a(Z)V

    .line 41
    return-void
.end method

.method public final onOrientationChanged(I)V
    .locals 7

    .prologue
    const/16 v0, 0xb4

    const/16 v1, 0x5a

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/16 v6, 0xa

    .line 6
    if-ne p1, v2, :cond_1

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    const/16 v4, 0x15e

    const/16 v5, 0x168

    invoke-static {p1, v4, v5}, Lbwf;->a(III)Z

    move-result v4

    .line 11
    if-nez v4, :cond_2

    .line 13
    invoke-static {p1, v3, v6}, Lbwf;->a(III)Z

    move-result v4

    .line 14
    if-eqz v4, :cond_4

    :cond_2
    move v0, v3

    .line 24
    :cond_3
    :goto_1
    if-eq v0, v2, :cond_0

    sget v1, Lbwf;->a:I

    if-eq v1, v0, :cond_0

    .line 25
    const-string v1, "InCallOrientationEventListener.onOrientationChanged"

    const-string v2, "orientation: %d -> %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    sget v5, Lbwf;->a:I

    .line 26
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    .line 28
    invoke-static {v1, v2, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    sput v0, Lbwf;->a:I

    .line 30
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    sget v1, Lbwf;->a:I

    invoke-virtual {v0, v1}, Lbwg;->a(I)V

    goto :goto_0

    .line 16
    :cond_4
    invoke-static {p1, v1, v6}, Lbwf;->b(III)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 17
    const/16 v0, 0x10e

    goto :goto_1

    .line 18
    :cond_5
    invoke-static {p1, v0, v6}, Lbwf;->b(III)Z

    move-result v4

    if-nez v4, :cond_3

    .line 20
    const/16 v0, 0x10e

    invoke-static {p1, v0, v6}, Lbwf;->b(III)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 21
    goto :goto_1

    :cond_6
    move v0, v2

    .line 22
    goto :goto_1
.end method
