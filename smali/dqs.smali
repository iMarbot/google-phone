.class public final Ldqs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldqs;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method final a(Lgvg;)Lgvg;
    .locals 6

    .prologue
    .line 4
    iget-object v0, p0, Ldqs;->a:Landroid/content/Context;

    iget-object v1, p1, Lgvg;->d:Ljava/lang/String;

    .line 5
    new-instance v2, Landroid/util/ArrayMap;

    invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V

    .line 6
    const-string v3, "phone_number"

    invoke-virtual {v2, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    const-string v1, "current_global_blacklist_version "

    .line 8
    invoke-static {v0}, Ldqt;->b(Landroid/content/Context;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 9
    invoke-virtual {v2, v1, v3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    const-string v1, "dialer_spam_report"

    invoke-static {v0, v1, v2}, Letf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 11
    iput-object v0, p1, Lgvg;->b:Ljava/lang/String;

    .line 12
    const-string v0, "dialer"

    iput-object v0, p1, Lgvg;->a:Ljava/lang/String;

    .line 13
    iget-object v0, p0, Ldqs;->a:Landroid/content/Context;

    .line 14
    invoke-static {v0}, Ldqt;->b(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lgvg;->i:Ljava/lang/String;

    .line 15
    new-instance v0, Lhga;

    invoke-direct {v0}, Lhga;-><init>()V

    .line 16
    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    iput-wide v2, v0, Lhga;->a:J

    .line 18
    iput-object v0, p1, Lgvg;->e:Lhga;

    .line 19
    iget-object v0, p0, Ldqs;->a:Landroid/content/Context;

    const-string v1, "phone"

    .line 20
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 21
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lgvg;->g:Ljava/lang/String;

    .line 22
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lgvg;->h:Ljava/lang/String;

    .line 23
    return-object p1
.end method

.method final b(Lgvg;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 24
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 25
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method must not be called from the UI thread!"

    .line 26
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    new-instance v0, Ledk;

    iget-object v1, p0, Ldqs;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ledk;-><init>(Landroid/content/Context;)V

    sget-object v1, Lebu;->a:Lesq;

    .line 28
    invoke-virtual {v0, v1}, Ledk;->a(Lesq;)Ledk;

    move-result-object v0

    invoke-virtual {v0}, Ledk;->b()Ledj;

    move-result-object v0

    .line 29
    const-wide/16 v2, 0x3e8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ledj;->a(JLjava/util/concurrent/TimeUnit;)Lecl;

    .line 30
    invoke-virtual {v0}, Ledj;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    new-instance v0, Lgvh;

    invoke-direct {v0}, Lgvh;-><init>()V

    .line 32
    iput-object p1, v0, Lgvh;->a:Lgvg;

    .line 33
    new-instance v1, Lebu;

    iget-object v2, p0, Ldqs;->a:Landroid/content/Context;

    const-string v3, "SCOOBY_SPAM_REPORT_LOG"

    invoke-direct {v1, v2, v3, v5, v5}, Lebu;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-static {v0}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lebu;->a([B)Lebv;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lebv;->a()Ledn;

    .line 37
    const-string v0, "SpamClearcutLoggerHelper.logEvent"

    const-string v1, "Spam Report Logged"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    :goto_0
    return-void

    .line 39
    :cond_1
    const-string v0, "SpamClearcutLoggerHelper.logEvent"

    const-string v1, "Not connected to Google API client, can\'t send spam clearcut log."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
