.class public Lccq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lccr;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final synthetic c:Lbvp;


# direct methods
.method public constructor <init>(Lbvp;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lccq;->c:Lbvp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p2, p0, Lccq;->a:Ljava/lang/String;

    .line 12
    iput p3, p0, Lccq;->b:I

    .line 13
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1
    iget-object v0, p0, Lccq;->c:Lbvp;

    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    iget v2, p0, Lccq;->b:I

    .line 2
    invoke-virtual {v0, v1, v2}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 3
    if-nez v0, :cond_0

    .line 9
    :goto_0
    return-void

    .line 5
    :cond_0
    new-instance v0, Lbvp$b;

    iget v1, p0, Lccq;->b:I

    iget-object v2, p0, Lccq;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lbvp$b;-><init>(ILjava/lang/String;)V

    .line 6
    iget-object v1, p0, Lccq;->c:Lbvp;

    .line 7
    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1, v0}, Lbvp;->b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    .line 8
    iget-object v1, p0, Lccq;->c:Lbvp;

    invoke-virtual {v1, v0}, Lbvp;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lccs;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 14
    iget-object v0, p0, Lccq;->c:Lbvp;

    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    iget v2, p0, Lccq;->b:I

    .line 15
    invoke-virtual {v0, v1, v2}, Lbvp;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 16
    if-nez v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 18
    :cond_1
    if-nez p1, :cond_2

    .line 19
    iget-object v0, p0, Lccq;->c:Lbvp;

    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    .line 20
    invoke-virtual {v0, v1}, Lbvp;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 22
    :cond_2
    new-instance v2, Lbvp$d;

    invoke-direct {v2}, Lbvp$d;-><init>()V

    .line 23
    invoke-interface {p1}, Lccs;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbvp$d;->a:Ljava/lang/String;

    .line 24
    invoke-interface {p1}, Lccs;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbvp$d;->c:Ljava/lang/String;

    .line 25
    invoke-interface {p1}, Lccs;->g()Lbkm$a;

    move-result-object v0

    iput-object v0, v2, Lbvp$d;->m:Lbkm$a;

    .line 26
    invoke-interface {p1}, Lccs;->f()Z

    move-result v0

    iput-boolean v0, v2, Lbvp$d;->s:Z

    .line 27
    invoke-interface {p1}, Lccs;->c()I

    move-result v1

    .line 28
    invoke-interface {p1}, Lccs;->d()Ljava/lang/String;

    move-result-object v0

    .line 29
    if-nez v1, :cond_5

    move-object v1, v2

    .line 34
    :goto_1
    iput-object v0, v1, Lbvp$d;->e:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lccq;->c:Lbvp;

    .line 36
    iget-object v0, v0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 37
    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp$d;

    .line 38
    if-eqz v0, :cond_3

    .line 39
    iget-object v1, v0, Lbvp$d;->d:Ljava/lang/String;

    iput-object v1, v2, Lbvp$d;->d:Ljava/lang/String;

    .line 40
    iget-boolean v1, v0, Lbvp$d;->r:Z

    iput-boolean v1, v2, Lbvp$d;->r:Z

    .line 41
    iget-object v1, v0, Lbvp$d;->o:Landroid/net/Uri;

    iput-object v1, v2, Lbvp$d;->o:Landroid/net/Uri;

    .line 42
    iget-object v0, v0, Lbvp$d;->q:Ljava/lang/String;

    iput-object v0, v2, Lbvp$d;->q:Ljava/lang/String;

    .line 43
    :cond_3
    invoke-interface {p1}, Lccs;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-interface {p1}, Lccs;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44
    iput v3, v2, Lbvp$d;->g:I

    .line 45
    :cond_4
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "put entry into map: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    iget-object v0, p0, Lccq;->c:Lbvp;

    .line 47
    iget-object v0, v0, Lbvp;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 48
    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lccq;->c:Lbvp;

    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1, v2}, Lbvp;->a(Ljava/lang/String;Lbvp$d;)V

    .line 51
    invoke-interface {p1}, Lccs;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v3

    :goto_2
    iput-boolean v0, v2, Lbvp$d;->i:Z

    .line 52
    iget-boolean v0, v2, Lbvp$d;->i:Z

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lccq;->c:Lbvp;

    iget-object v1, p0, Lccq;->a:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1}, Lbvp;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 31
    :cond_5
    iget-object v4, p0, Lccq;->c:Lbvp;

    .line 32
    iget-object v4, v4, Lbvp;->b:Landroid/content/Context;

    .line 33
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v1, v0}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 34
    if-nez v0, :cond_6

    const/4 v0, 0x0

    move-object v1, v2

    goto :goto_1

    :cond_6
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_1

    .line 51
    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method
