.class final Lfvk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Lfvj;


# direct methods
.method constructor <init>(Lfvj;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfvk;->a:Lfvj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 2
    const-string v0, "onServiceConnected"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 3
    if-nez p2, :cond_1

    .line 4
    const-string v0, "Failed to bind to CallService."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 22
    :cond_0
    :goto_0
    return-void

    .line 6
    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    if-nez v0, :cond_2

    .line 7
    const-string v0, "CallService does not appear to be running in the current process. This is most likely because the application crashed and restarted the service in another process. This instance will be unusable."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 9
    :cond_2
    iget-object v0, p0, Lfvk;->a:Lfvj;

    check-cast p2, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    .line 10
    iput-object p2, v0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    .line 12
    iget-object v0, p0, Lfvk;->a:Lfvj;

    .line 13
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 14
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvk;->a:Lfvj;

    .line 15
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 16
    invoke-virtual {v0}, Lfnp;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lfvk;->a:Lfvj;

    .line 18
    iget-object v0, v0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    .line 19
    iget-object v1, p0, Lfvk;->a:Lfvj;

    .line 20
    iget-object v1, v1, Lfvj;->d:Lfnp;

    .line 21
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->bindToCall(Lfnp;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 23
    const-string v0, "onServiceDisconnected"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lfvk;->a:Lfvj;

    const/4 v1, 0x0

    .line 25
    iput-object v1, v0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    .line 27
    return-void
.end method
