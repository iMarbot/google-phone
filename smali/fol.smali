.class final Lfol;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public bandwidthEstimationStats:Lfom;

.field public connectionInfoStats:Lfom;

.field public videoReceiverStats:Ljava/util/Map;

.field public videoSenderStats:Lfom;

.field public voiceReceiverStats:Ljava/util/Map;

.field public voiceSenderStats:Lfom;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object v1, p0, Lfol;->voiceSenderStats:Lfom;

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    .line 4
    iput-object v1, p0, Lfol;->videoSenderStats:Lfom;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfol;->videoReceiverStats:Ljava/util/Map;

    .line 6
    iput-object v1, p0, Lfol;->bandwidthEstimationStats:Lfom;

    .line 7
    iput-object v1, p0, Lfol;->connectionInfoStats:Lfom;

    .line 8
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    iput-object v1, p0, Lfol;->voiceSenderStats:Lfom;

    .line 10
    iget-object v0, p0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 11
    iput-object v1, p0, Lfol;->videoSenderStats:Lfom;

    .line 12
    iget-object v0, p0, Lfol;->videoReceiverStats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 13
    iput-object v1, p0, Lfol;->bandwidthEstimationStats:Lfom;

    .line 14
    iput-object v1, p0, Lfol;->connectionInfoStats:Lfom;

    .line 15
    return-void
.end method

.method public final getStatsUpdates()Ljava/util/List;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    .line 21
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iget-object v2, p0, Lfol;->videoReceiverStats:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 22
    iget-object v1, p0, Lfol;->voiceSenderStats:Lfom;

    if-eqz v1, :cond_0

    .line 23
    iget-object v1, p0, Lfol;->voiceSenderStats:Lfom;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    :cond_0
    iget-object v1, p0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 25
    iget-object v1, p0, Lfol;->videoSenderStats:Lfom;

    if-eqz v1, :cond_1

    .line 26
    iget-object v1, p0, Lfol;->videoSenderStats:Lfom;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    :cond_1
    iget-object v1, p0, Lfol;->videoReceiverStats:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 28
    iget-object v1, p0, Lfol;->bandwidthEstimationStats:Lfom;

    if-eqz v1, :cond_2

    .line 29
    iget-object v1, p0, Lfol;->bandwidthEstimationStats:Lfom;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_2
    iget-object v1, p0, Lfol;->connectionInfoStats:Lfom;

    if-eqz v1, :cond_3

    .line 31
    iget-object v1, p0, Lfol;->connectionInfoStats:Lfom;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_3
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 33
    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfol;->voiceSenderStats:Lfom;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    .line 17
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfol;->videoSenderStats:Lfom;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfol;->videoReceiverStats:Ljava/util/Map;

    .line 18
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfol;->bandwidthEstimationStats:Lfom;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfol;->connectionInfoStats:Lfom;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 19
    :goto_0
    return v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_0
.end method
