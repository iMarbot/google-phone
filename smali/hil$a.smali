.class public final enum Lhil$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lhil$a;

.field public static final enum b:Lhil$a;

.field public static final enum c:Lhil$a;

.field public static final enum d:Lhil$a;

.field public static final enum e:Lhil$a;

.field public static final f:Lhby;

.field private static synthetic h:[Lhil$a;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lhil$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, Lhil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhil$a;->a:Lhil$a;

    .line 14
    new-instance v0, Lhil$a;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v3, v3}, Lhil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhil$a;->b:Lhil$a;

    .line 15
    new-instance v0, Lhil$a;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v4, v4}, Lhil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhil$a;->c:Lhil$a;

    .line 16
    new-instance v0, Lhil$a;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v5, v5}, Lhil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhil$a;->d:Lhil$a;

    .line 17
    new-instance v0, Lhil$a;

    const-string v1, "IMPORTANT"

    invoke-direct {v0, v1, v6, v6}, Lhil$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhil$a;->e:Lhil$a;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lhil$a;

    sget-object v1, Lhil$a;->a:Lhil$a;

    aput-object v1, v0, v2

    sget-object v1, Lhil$a;->b:Lhil$a;

    aput-object v1, v0, v3

    sget-object v1, Lhil$a;->c:Lhil$a;

    aput-object v1, v0, v4

    sget-object v1, Lhil$a;->d:Lhil$a;

    aput-object v1, v0, v5

    sget-object v1, Lhil$a;->e:Lhil$a;

    aput-object v1, v0, v6

    sput-object v0, Lhil$a;->h:[Lhil$a;

    .line 19
    new-instance v0, Lhin;

    invoke-direct {v0}, Lhin;-><init>()V

    sput-object v0, Lhil$a;->f:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lhil$a;->g:I

    .line 12
    return-void
.end method

.method public static a(I)Lhil$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 9
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhil$a;->a:Lhil$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhil$a;->b:Lhil$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhil$a;->c:Lhil$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lhil$a;->d:Lhil$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lhil$a;->e:Lhil$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static values()[Lhil$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhil$a;->h:[Lhil$a;

    invoke-virtual {v0}, [Lhil$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhil$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhil$a;->g:I

    return v0
.end method
