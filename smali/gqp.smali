.class public final Lgqp;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqp;


# instance fields
.field public avatarUrl:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public invitee:Lgqq;

.field public notificationsSent:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgqp;->clear()Lgqp;

    .line 16
    return-void
.end method

.method public static checkNotificationSentOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x30

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum NotificationSent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkNotificationSentOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgqp;->checkNotificationSentOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgqp;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgqp;->_emptyArray:[Lgqp;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgqp;->_emptyArray:[Lgqp;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgqp;

    sput-object v0, Lgqp;->_emptyArray:[Lgqp;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgqp;->_emptyArray:[Lgqp;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqp;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lgqp;

    invoke-direct {v0}, Lgqp;-><init>()V

    invoke-virtual {v0, p0}, Lgqp;->mergeFrom(Lhfp;)Lgqp;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqp;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lgqp;

    invoke-direct {v0}, Lgqp;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqp;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqp;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lgqp;->invitee:Lgqq;

    .line 18
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgqp;->notificationsSent:[I

    .line 19
    iput-object v1, p0, Lgqp;->displayName:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lgqp;->unknownFieldData:Lhfv;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lgqp;->cachedSize:I

    .line 23
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 37
    iget-object v2, p0, Lgqp;->invitee:Lgqq;

    if-eqz v2, :cond_0

    .line 38
    const/4 v2, 0x1

    iget-object v3, p0, Lgqp;->invitee:Lgqq;

    .line 39
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 40
    :cond_0
    iget-object v2, p0, Lgqp;->notificationsSent:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgqp;->notificationsSent:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 42
    :goto_0
    iget-object v3, p0, Lgqp;->notificationsSent:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 43
    iget-object v3, p0, Lgqp;->notificationsSent:[I

    aget v3, v3, v1

    .line 45
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_1
    add-int/2addr v0, v2

    .line 48
    iget-object v1, p0, Lgqp;->notificationsSent:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 49
    :cond_2
    iget-object v1, p0, Lgqp;->displayName:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 50
    const/4 v1, 0x3

    iget-object v2, p0, Lgqp;->displayName:Ljava/lang/String;

    .line 51
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_3
    iget-object v1, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 53
    const/4 v1, 0x4

    iget-object v2, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    .line 54
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_4
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqp;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v1, 0x0

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v3

    .line 57
    sparse-switch v3, :sswitch_data_0

    .line 59
    invoke-super {p0, p1, v3}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    :sswitch_0
    return-object p0

    .line 61
    :sswitch_1
    iget-object v0, p0, Lgqp;->invitee:Lgqq;

    if-nez v0, :cond_1

    .line 62
    new-instance v0, Lgqq;

    invoke-direct {v0}, Lgqq;-><init>()V

    iput-object v0, p0, Lgqp;->invitee:Lgqq;

    .line 63
    :cond_1
    iget-object v0, p0, Lgqp;->invitee:Lgqq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 66
    :sswitch_2
    invoke-static {p1, v8}, Lhgc;->a(Lhfp;I)I

    move-result v4

    .line 67
    new-array v5, v4, [I

    move v2, v1

    move v0, v1

    .line 69
    :goto_1
    if-ge v2, v4, :cond_3

    .line 70
    if-eqz v2, :cond_2

    .line 71
    invoke-virtual {p1}, Lhfp;->a()I

    .line 72
    :cond_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v6

    .line 74
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v7

    .line 75
    invoke-static {v7}, Lgqp;->checkNotificationSentOrThrow(I)I

    move-result v7

    aput v7, v5, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    add-int/lit8 v0, v0, 0x1

    .line 81
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 79
    :catch_0
    move-exception v7

    invoke-virtual {p1, v6}, Lhfp;->e(I)V

    .line 80
    invoke-virtual {p0, p1, v3}, Lgqp;->storeUnknownField(Lhfp;I)Z

    goto :goto_2

    .line 82
    :cond_3
    if-eqz v0, :cond_0

    .line 83
    iget-object v2, p0, Lgqp;->notificationsSent:[I

    if-nez v2, :cond_4

    move v2, v1

    .line 84
    :goto_3
    if-nez v2, :cond_5

    array-length v3, v5

    if-ne v0, v3, :cond_5

    .line 85
    iput-object v5, p0, Lgqp;->notificationsSent:[I

    goto :goto_0

    .line 83
    :cond_4
    iget-object v2, p0, Lgqp;->notificationsSent:[I

    array-length v2, v2

    goto :goto_3

    .line 86
    :cond_5
    add-int v3, v2, v0

    new-array v3, v3, [I

    .line 87
    if-eqz v2, :cond_6

    .line 88
    iget-object v4, p0, Lgqp;->notificationsSent:[I

    invoke-static {v4, v1, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    :cond_6
    invoke-static {v5, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    iput-object v3, p0, Lgqp;->notificationsSent:[I

    goto :goto_0

    .line 92
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 93
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 95
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 96
    :goto_4
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_7

    .line 98
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 99
    invoke-static {v4}, Lgqp;->checkNotificationSentOrThrow(I)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 104
    :cond_7
    if-eqz v0, :cond_b

    .line 105
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 106
    iget-object v2, p0, Lgqp;->notificationsSent:[I

    if-nez v2, :cond_9

    move v2, v1

    .line 107
    :goto_5
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 108
    if-eqz v2, :cond_8

    .line 109
    iget-object v4, p0, Lgqp;->notificationsSent:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 110
    :cond_8
    :goto_6
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_a

    .line 111
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v4

    .line 113
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v5

    .line 114
    invoke-static {v5}, Lgqp;->checkNotificationSentOrThrow(I)I

    move-result v5

    aput v5, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 115
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 106
    :cond_9
    iget-object v2, p0, Lgqp;->notificationsSent:[I

    array-length v2, v2

    goto :goto_5

    .line 118
    :catch_1
    move-exception v5

    invoke-virtual {p1, v4}, Lhfp;->e(I)V

    .line 119
    invoke-virtual {p0, p1, v8}, Lgqp;->storeUnknownField(Lhfp;I)Z

    goto :goto_6

    .line 121
    :cond_a
    iput-object v0, p0, Lgqp;->notificationsSent:[I

    .line 122
    :cond_b
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 124
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqp;->displayName:Ljava/lang/String;

    goto/16 :goto_0

    .line 126
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 103
    :catch_2
    move-exception v4

    goto :goto_4

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lgqp;->mergeFrom(Lhfp;)Lgqp;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lgqp;->invitee:Lgqq;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Lgqp;->invitee:Lgqq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_0
    iget-object v0, p0, Lgqp;->notificationsSent:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgqp;->notificationsSent:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 27
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgqp;->notificationsSent:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 28
    const/4 v1, 0x2

    iget-object v2, p0, Lgqp;->notificationsSent:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_1
    iget-object v0, p0, Lgqp;->displayName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 31
    const/4 v0, 0x3

    iget-object v1, p0, Lgqp;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_2
    iget-object v0, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 33
    const/4 v0, 0x4

    iget-object v1, p0, Lgqp;->avatarUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 35
    return-void
.end method
