.class public final Lfzl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lgdc;

.field public final b:Lgab;

.field public final c:Lgap;

.field public final d:Lfzp;

.field public final e:Lgad;

.field public final f:Lgae;

.field public final g:Lfzz;

.field public final h:Lgaq;

.field public final i:Lfzk;

.field public final j:Lgac;

.field public final k:Lfzu;

.field public final l:Lfzo;


# direct methods
.method public constructor <init>(Lgdc;Lgab;Lgap;Lfzp;Lgad;Lgae;Lfzz;Lgaq;Lfzk;Lgac;Lfzu;Lfzo;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    if-eqz p1, :cond_3

    :goto_0
    iput-object p1, p0, Lfzl;->a:Lgdc;

    .line 4
    if-eqz p2, :cond_0

    .line 5
    iget v0, p2, Lgab;->c:I

    .line 6
    if-gtz v0, :cond_4

    .line 7
    :cond_0
    sget-object v0, Lgab;->a:Lgab;

    iput-object v0, p0, Lfzl;->b:Lgab;

    .line 9
    :goto_1
    if-eqz p3, :cond_1

    .line 10
    iget v0, p3, Lgap;->c:I

    .line 11
    if-gtz v0, :cond_5

    .line 12
    :cond_1
    sget-object v0, Lgap;->a:Lgap;

    iput-object v0, p0, Lfzl;->c:Lgap;

    .line 14
    :goto_2
    if-nez p4, :cond_6

    .line 15
    sget-object v0, Lfzp;->a:Lfzp;

    iput-object v0, p0, Lfzl;->d:Lfzp;

    .line 17
    :goto_3
    if-nez p5, :cond_7

    .line 18
    sget-object v0, Lgad;->a:Lgad;

    iput-object v0, p0, Lfzl;->e:Lgad;

    .line 20
    :goto_4
    if-nez p6, :cond_8

    .line 21
    sget-object v0, Lgae;->a:Lgae;

    iput-object v0, p0, Lfzl;->f:Lgae;

    .line 23
    :goto_5
    if-nez p7, :cond_9

    .line 24
    sget-object v0, Lfzz;->a:Lfzz;

    iput-object v0, p0, Lfzl;->g:Lfzz;

    .line 26
    :goto_6
    if-nez p8, :cond_a

    .line 27
    sget-object v0, Lgaq;->a:Lgaq;

    iput-object v0, p0, Lfzl;->h:Lgaq;

    .line 29
    :goto_7
    if-nez p9, :cond_b

    .line 30
    sget-object v0, Lfzk;->a:Lfzk;

    iput-object v0, p0, Lfzl;->i:Lfzk;

    .line 32
    :goto_8
    if-nez p10, :cond_c

    .line 33
    sget-object v0, Lgac;->a:Lgac;

    iput-object v0, p0, Lfzl;->j:Lgac;

    .line 35
    :goto_9
    if-nez p11, :cond_d

    .line 36
    sget-object v0, Lfzu;->a:Lfzu;

    iput-object v0, p0, Lfzl;->k:Lfzu;

    .line 38
    :goto_a
    if-eqz p12, :cond_2

    .line 40
    iget v0, p12, Lfzo;->d:I

    .line 41
    if-lez v0, :cond_2

    .line 43
    iget v0, p12, Lfzo;->c:I

    .line 44
    if-lez v0, :cond_2

    .line 46
    iget v0, p12, Lfzo;->e:I

    .line 47
    const/16 v1, 0x64

    if-ge v0, v1, :cond_e

    .line 48
    :cond_2
    sget-object v0, Lfzo;->a:Lfzo;

    iput-object v0, p0, Lfzl;->l:Lfzo;

    .line 50
    :goto_b
    return-void

    .line 3
    :cond_3
    sget-object p1, Lgdc;->a:Lgdc;

    goto :goto_0

    .line 8
    :cond_4
    iput-object p2, p0, Lfzl;->b:Lgab;

    goto :goto_1

    .line 13
    :cond_5
    iput-object p3, p0, Lfzl;->c:Lgap;

    goto :goto_2

    .line 16
    :cond_6
    iput-object p4, p0, Lfzl;->d:Lfzp;

    goto :goto_3

    .line 19
    :cond_7
    iput-object p5, p0, Lfzl;->e:Lgad;

    goto :goto_4

    .line 22
    :cond_8
    iput-object p6, p0, Lfzl;->f:Lgae;

    goto :goto_5

    .line 25
    :cond_9
    iput-object p7, p0, Lfzl;->g:Lfzz;

    goto :goto_6

    .line 28
    :cond_a
    iput-object p8, p0, Lfzl;->h:Lgaq;

    goto :goto_7

    .line 31
    :cond_b
    iput-object p9, p0, Lfzl;->i:Lfzk;

    goto :goto_8

    .line 34
    :cond_c
    iput-object p10, p0, Lfzl;->j:Lgac;

    goto :goto_9

    .line 37
    :cond_d
    iput-object p11, p0, Lfzl;->k:Lfzu;

    goto :goto_a

    .line 49
    :cond_e
    iput-object p12, p0, Lfzl;->l:Lfzo;

    goto :goto_b
.end method
