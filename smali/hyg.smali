.class public final Lhyg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/util/BitSet;

.field public static final b:Lhyg;

.field private static c:Ljava/util/BitSet;

.field private static d:Ljava/util/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    new-array v0, v3, [I

    const/16 v1, 0x3a

    aput v1, v0, v2

    invoke-static {v0}, Lhyg;->a([I)Ljava/util/BitSet;

    move-result-object v0

    sput-object v0, Lhyg;->c:Ljava/util/BitSet;

    .line 184
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lhyg;->a([I)Ljava/util/BitSet;

    move-result-object v0

    sput-object v0, Lhyg;->d:Ljava/util/BitSet;

    .line 185
    new-array v0, v3, [I

    const/16 v1, 0x3b

    aput v1, v0, v2

    invoke-static {v0}, Lhyg;->a([I)Ljava/util/BitSet;

    move-result-object v0

    sput-object v0, Lhyg;->a:Ljava/util/BitSet;

    .line 186
    new-instance v0, Lhyg;

    invoke-direct {v0}, Lhyg;-><init>()V

    sput-object v0, Lhyg;->b:Lhyg;

    return-void

    .line 184
    :array_0
    .array-data 4
        0x3d
        0x3b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static varargs a([I)Ljava/util/BitSet;
    .locals 3

    .prologue
    .line 2
    new-instance v1, Ljava/util/BitSet;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/BitSet;-><init>(I)V

    .line 3
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 4
    aget v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6
    :cond_0
    return-object v1
.end method

.method private static a(Lhyi;Lhyd;Ljava/lang/StringBuilder;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x5c

    const/16 v7, 0x22

    .line 149
    invoke-virtual {p1}, Lhyd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget v0, p1, Lhyd;->b:I

    .line 155
    iget v3, p1, Lhyd;->b:I

    .line 158
    iget v4, p1, Lhyd;->a:I

    .line 160
    invoke-interface {p0, v0}, Lhyi;->b(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    .line 161
    if-ne v2, v7, :cond_0

    .line 163
    add-int/lit8 v2, v0, 0x1

    .line 164
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v1

    .line 166
    :goto_1
    if-ge v3, v4, :cond_7

    .line 167
    invoke-interface {p0, v3}, Lhyi;->b(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-char v5, v5

    .line 168
    if-eqz v0, :cond_4

    .line 169
    if-eq v5, v7, :cond_2

    if-eq v5, v8, :cond_2

    .line 170
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 180
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    :cond_4
    if-ne v5, v7, :cond_5

    .line 174
    add-int/lit8 v0, v2, 0x1

    .line 181
    :goto_3
    invoke-virtual {p1, v0}, Lhyd;->a(I)V

    goto :goto_0

    .line 176
    :cond_5
    if-ne v5, v8, :cond_6

    .line 177
    const/4 v0, 0x1

    goto :goto_2

    .line 178
    :cond_6
    const/16 v6, 0xd

    if-eq v5, v6, :cond_3

    const/16 v6, 0xa

    if-eq v5, v6, :cond_3

    .line 179
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method private static a(Lhyi;Lhyd;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    .line 132
    .line 133
    iget v1, p1, Lhyd;->b:I

    .line 136
    iget v0, p1, Lhyd;->b:I

    .line 139
    iget v2, p1, Lhyd;->a:I

    .line 141
    :goto_0
    if-ge v0, v2, :cond_1

    .line 142
    invoke-interface {p0, v0}, Lhyi;->b(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-char v3, v3

    .line 143
    if-eqz p2, :cond_0

    invoke-virtual {p2, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-static {v3}, Lhyj;->a(C)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v4, 0x28

    if-eq v3, v4, :cond_1

    .line 144
    add-int/lit8 v1, v1, 0x1

    .line 145
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    invoke-virtual {p1, v1}, Lhyd;->a(I)V

    .line 148
    return-void
.end method

.method private b(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v1, 0x0

    .line 59
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 61
    :goto_0
    invoke-virtual {p2}, Lhyd;->a()Z

    move-result v3

    if-nez v3, :cond_6

    .line 63
    iget v3, p2, Lhyd;->b:I

    .line 64
    invoke-interface {p1, v3}, Lhyi;->b(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-char v3, v3

    .line 65
    if-eqz p3, :cond_0

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_6

    .line 66
    :cond_0
    invoke-static {v3}, Lhyj;->a(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 67
    invoke-static {p1, p2}, Lhyg;->b(Lhyi;Lhyd;)V

    .line 68
    const/4 v0, 0x1

    goto :goto_0

    .line 69
    :cond_1
    const/16 v4, 0x28

    if-ne v3, v4, :cond_2

    .line 70
    invoke-static {p1, p2}, Lhyg;->c(Lhyi;Lhyd;)V

    goto :goto_0

    .line 71
    :cond_2
    const/16 v4, 0x22

    if-ne v3, v4, :cond_4

    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_3

    if-eqz v0, :cond_3

    .line 73
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    :cond_3
    invoke-static {p1, p2, v2}, Lhyg;->a(Lhyi;Lhyd;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 75
    goto :goto_0

    .line 76
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_5

    if-eqz v0, :cond_5

    .line 77
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    :cond_5
    invoke-static {p1, p2, p3, v2}, Lhyg;->a(Lhyi;Lhyd;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 80
    goto :goto_0

    .line 81
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lhyi;Lhyd;)V
    .locals 4

    .prologue
    .line 82
    .line 83
    iget v1, p1, Lhyd;->b:I

    .line 86
    iget v0, p1, Lhyd;->b:I

    .line 89
    iget v2, p1, Lhyd;->a:I

    .line 91
    :goto_0
    if-ge v0, v2, :cond_0

    .line 92
    invoke-interface {p0, v0}, Lhyi;->b(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-char v3, v3

    .line 93
    invoke-static {v3}, Lhyj;->a(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    add-int/lit8 v1, v1, 0x1

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    invoke-virtual {p1, v1}, Lhyd;->a(I)V

    .line 97
    return-void
.end method

.method private static c(Lhyi;Lhyd;)V
    .locals 10

    .prologue
    const/16 v9, 0x28

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    invoke-virtual {p1}, Lhyd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget v0, p1, Lhyd;->b:I

    .line 104
    iget v4, p1, Lhyd;->b:I

    .line 107
    iget v6, p1, Lhyd;->a:I

    .line 109
    invoke-interface {p0, v0}, Lhyi;->b(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-char v1, v1

    .line 110
    if-ne v1, v9, :cond_0

    .line 112
    add-int/lit8 v1, v0, 0x1

    .line 113
    add-int/lit8 v0, v4, 0x1

    move v5, v0

    move v4, v3

    move v0, v1

    move v1, v2

    .line 116
    :goto_1
    if-ge v5, v6, :cond_3

    .line 117
    invoke-interface {p0, v5}, Lhyi;->b(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    int-to-char v7, v7

    .line 118
    if-eqz v1, :cond_4

    move v1, v2

    .line 126
    :cond_2
    :goto_2
    if-gtz v4, :cond_7

    .line 127
    add-int/lit8 v0, v0, 0x1

    .line 130
    :cond_3
    invoke-virtual {p1, v0}, Lhyd;->a(I)V

    goto :goto_0

    .line 120
    :cond_4
    const/16 v8, 0x5c

    if-ne v7, v8, :cond_5

    move v1, v3

    .line 121
    goto :goto_2

    .line 122
    :cond_5
    if-ne v7, v9, :cond_6

    .line 123
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 124
    :cond_6
    const/16 v8, 0x29

    if-ne v7, v8, :cond_2

    .line 125
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 129
    :cond_7
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lhyi;)Lhyf;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 7
    if-nez p1, :cond_0

    .line 15
    :goto_0
    return-object v0

    .line 9
    :cond_0
    new-instance v2, Lhyd;

    const/4 v1, 0x0

    invoke-interface {p1}, Lhyi;->a()I

    move-result v3

    invoke-direct {v2, v1, v3}, Lhyd;-><init>(II)V

    .line 10
    sget-object v1, Lhyg;->c:Ljava/util/BitSet;

    invoke-virtual {p0, p1, v2, v1}, Lhyg;->a(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v3

    .line 11
    invoke-virtual {v2}, Lhyd;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 12
    new-instance v0, Lhvc;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid MIME field: no name/value separator found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lhvc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :cond_1
    new-instance v1, Lhyf;

    .line 14
    iget v2, v2, Lhyd;->b:I

    .line 15
    invoke-direct {v1, p1, v2, v3, v0}, Lhyf;-><init>(Lhyi;ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 43
    :goto_0
    invoke-virtual {p2}, Lhyd;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 45
    iget v3, p2, Lhyd;->b:I

    .line 46
    invoke-interface {p1, v3}, Lhyi;->b(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-char v3, v3

    .line 47
    if-eqz p3, :cond_0

    invoke-virtual {p3, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 48
    :cond_0
    invoke-static {v3}, Lhyj;->a(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    invoke-static {p1, p2}, Lhyg;->b(Lhyi;Lhyd;)V

    .line 50
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :cond_1
    const/16 v4, 0x28

    if-ne v3, v4, :cond_2

    .line 52
    invoke-static {p1, p2}, Lhyg;->c(Lhyi;Lhyd;)V

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_3

    if-eqz v0, :cond_3

    .line 54
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    :cond_3
    invoke-static {p1, p2, p3, v2}, Lhyg;->a(Lhyi;Lhyd;Ljava/util/BitSet;Ljava/lang/StringBuilder;)V

    move v0, v1

    .line 57
    goto :goto_0

    .line 58
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lhyi;Lhyd;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 16
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    invoke-static {p1, p2}, Lhyg;->b(Lhyi;Lhyd;)V

    .line 18
    :goto_0
    invoke-virtual {p2}, Lhyd;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 20
    sget-object v0, Lhyg;->d:Ljava/util/BitSet;

    invoke-virtual {p0, p1, p2, v0}, Lhyg;->a(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v2

    .line 21
    invoke-virtual {p2}, Lhyd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    new-instance v0, Lhyc;

    invoke-direct {v0, v2, v4}, Lhyc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 24
    :cond_0
    iget v0, p2, Lhyd;->b:I

    .line 25
    invoke-interface {p1, v0}, Lhyi;->b(I)B

    move-result v0

    .line 27
    iget v3, p2, Lhyd;->b:I

    .line 28
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2, v3}, Lhyd;->a(I)V

    .line 29
    const/16 v3, 0x3b

    if-ne v0, v3, :cond_1

    .line 30
    new-instance v0, Lhyc;

    invoke-direct {v0, v2, v4}, Lhyc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 31
    :cond_1
    sget-object v0, Lhyg;->a:Ljava/util/BitSet;

    invoke-direct {p0, p1, p2, v0}, Lhyg;->b(Lhyi;Lhyd;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v3

    .line 32
    invoke-virtual {p2}, Lhyd;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 34
    iget v0, p2, Lhyd;->b:I

    .line 35
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Lhyd;->a(I)V

    .line 36
    :cond_2
    new-instance v0, Lhyc;

    invoke-direct {v0, v2, v3}, Lhyc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 40
    :cond_3
    return-object v1
.end method
