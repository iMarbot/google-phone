.class Lhaw$a;
.super Lhaw;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhaw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation


# instance fields
.field public final c:[B

.field public final d:I

.field public e:I

.field public f:I

.field public final g:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lhaw;-><init>()V

    .line 3
    if-gez p1, :cond_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5
    :cond_0
    const/16 v0, 0x14

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lhaw$a;->c:[B

    .line 6
    iget-object v0, p0, Lhaw$a;->c:[B

    array-length v0, v0

    iput v0, p0, Lhaw$a;->d:I

    .line 7
    return-void
.end method

.method constructor <init>(Ljava/io/OutputStream;I)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0, p2}, Lhaw$a;-><init>(I)V

    .line 69
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "out"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iput-object p1, p0, Lhaw$a;->g:Ljava/io/OutputStream;

    .line 72
    return-void
.end method


# virtual methods
.method public a(B)V
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lhaw$a;->e:I

    iget v1, p0, Lhaw$a;->d:I

    if-ne v0, v1, :cond_0

    .line 133
    invoke-virtual {p0}, Lhaw$a;->k()V

    .line 134
    :cond_0
    invoke-virtual {p0, p1}, Lhaw$a;->b(B)V

    .line 135
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 73
    .line 74
    shl-int/lit8 v0, p1, 0x3

    or-int/2addr v0, p2

    .line 75
    invoke-virtual {p0, v0}, Lhaw$a;->c(I)V

    .line 76
    return-void
.end method

.method public a(IJ)V
    .locals 2

    .prologue
    .line 92
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 94
    invoke-virtual {p0, p2, p3}, Lhaw$a;->g(J)V

    .line 95
    return-void
.end method

.method public a(ILhah;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lhaw$a;->a(II)V

    .line 108
    invoke-virtual {p0, p2}, Lhaw$a;->a(Lhah;)V

    .line 109
    return-void
.end method

.method public a(ILhdd;)V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lhaw$a;->a(II)V

    .line 117
    invoke-virtual {p0, p2}, Lhaw$a;->a(Lhdd;)V

    .line 118
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lhaw$a;->a(II)V

    .line 105
    invoke-virtual {p0, p2}, Lhaw$a;->a(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 100
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lhaw$a;->o(I)V

    .line 101
    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 102
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lhaw$a;->b(B)V

    .line 103
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 146
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 147
    invoke-virtual {p0, p1, p2}, Lhaw$a;->g(J)V

    .line 148
    return-void
.end method

.method public a(Lhah;)V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p1}, Lhah;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw$a;->c(I)V

    .line 111
    invoke-virtual {p1, p0}, Lhah;->a(Lhag;)V

    .line 112
    return-void
.end method

.method public a(Lhdd;)V
    .locals 1

    .prologue
    .line 129
    invoke-interface {p1}, Lhdd;->getSerializedSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lhaw$a;->c(I)V

    .line 130
    invoke-interface {p1, p0}, Lhdd;->writeTo(Lhaw;)V

    .line 131
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 152
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    .line 153
    invoke-static {v0}, Lhaw$a;->h(I)I

    move-result v1

    .line 154
    add-int v2, v1, v0

    iget v3, p0, Lhaw$a;->d:I

    if-le v2, v3, :cond_0

    .line 155
    new-array v1, v0, [B

    .line 156
    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0}, Lhex;->a(Ljava/lang/CharSequence;[BII)I

    move-result v0

    .line 157
    invoke-virtual {p0, v0}, Lhaw$a;->c(I)V

    .line 158
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lhaw$a;->a([BII)V

    .line 185
    :goto_0
    return-void

    .line 160
    :cond_0
    add-int/2addr v0, v1

    iget v2, p0, Lhaw$a;->d:I

    iget v3, p0, Lhaw$a;->e:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_1

    .line 161
    invoke-virtual {p0}, Lhaw$a;->k()V

    .line 162
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v0}, Lhaw$a;->h(I)I

    move-result v0

    .line 163
    iget v2, p0, Lhaw$a;->e:I
    :try_end_0
    .catch Lhfa; {:try_start_0 .. :try_end_0} :catch_1

    .line 164
    if-ne v0, v1, :cond_2

    .line 165
    add-int v1, v2, v0

    :try_start_1
    iput v1, p0, Lhaw$a;->e:I

    .line 166
    iget-object v1, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    iget v4, p0, Lhaw$a;->d:I

    iget v5, p0, Lhaw$a;->e:I

    sub-int/2addr v4, v5

    invoke-static {p1, v1, v3, v4}, Lhex;->a(Ljava/lang/CharSequence;[BII)I

    move-result v1

    .line 167
    iput v2, p0, Lhaw$a;->e:I

    .line 168
    sub-int v3, v1, v2

    sub-int v0, v3, v0

    .line 169
    invoke-virtual {p0, v0}, Lhaw$a;->m(I)V

    .line 170
    iput v1, p0, Lhaw$a;->e:I

    .line 175
    :goto_1
    iget v1, p0, Lhaw$a;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lhaw$a;->f:I
    :try_end_1
    .catch Lhfa; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    :try_start_2
    iget v1, p0, Lhaw$a;->f:I

    iget v3, p0, Lhaw$a;->e:I

    sub-int/2addr v3, v2

    sub-int/2addr v1, v3

    iput v1, p0, Lhaw$a;->f:I

    .line 179
    iput v2, p0, Lhaw$a;->e:I

    .line 180
    throw v0
    :try_end_2
    .catch Lhfa; {:try_start_2 .. :try_end_2} :catch_1

    .line 183
    :catch_1
    move-exception v0

    .line 184
    invoke-virtual {p0, p1, v0}, Lhaw$a;->a(Ljava/lang/String;Lhfa;)V

    goto :goto_0

    .line 172
    :cond_2
    :try_start_3
    invoke-static {p1}, Lhex;->a(Ljava/lang/CharSequence;)I

    move-result v0

    .line 173
    invoke-virtual {p0, v0}, Lhaw$a;->m(I)V

    .line 174
    iget-object v1, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    invoke-static {p1, v1, v3, v0}, Lhex;->a(Ljava/lang/CharSequence;[BII)I

    move-result v1

    iput v1, p0, Lhaw$a;->e:I
    :try_end_3
    .catch Lhfa; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 181
    :catch_2
    move-exception v0

    .line 182
    :try_start_4
    new-instance v1, Lhaw$c;

    invoke-direct {v1, v0}, Lhaw$c;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Lhfa; {:try_start_4 .. :try_end_4} :catch_1
.end method

.method public a([BII)V
    .locals 0

    .prologue
    .line 206
    invoke-virtual {p0, p1, p2, p3}, Lhaw$a;->d([BII)V

    .line 207
    return-void
.end method

.method final b(B)V
    .locals 3

    .prologue
    .line 9
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    aput-byte p1, v0, v1

    .line 10
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhaw$a;->f:I

    .line 11
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 136
    if-ltz p1, :cond_0

    .line 137
    invoke-virtual {p0, p1}, Lhaw$a;->c(I)V

    .line 139
    :goto_0
    return-void

    .line 138
    :cond_0
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lhaw$a;->a(J)V

    goto :goto_0
.end method

.method public b(II)V
    .locals 2

    .prologue
    .line 77
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 80
    if-ltz p2, :cond_0

    .line 81
    invoke-virtual {p0, p2}, Lhaw$a;->m(I)V

    .line 83
    :goto_0
    return-void

    .line 82
    :cond_0
    int-to-long v0, p2

    invoke-virtual {p0, v0, v1}, Lhaw$a;->g(J)V

    goto :goto_0
.end method

.method public b(ILhah;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 124
    invoke-virtual {p0, v1, v2}, Lhaw$a;->a(II)V

    .line 125
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lhaw$a;->c(II)V

    .line 126
    invoke-virtual {p0, v2, p2}, Lhaw$a;->a(ILhah;)V

    .line 127
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Lhaw$a;->a(II)V

    .line 128
    return-void
.end method

.method public b(ILhdd;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 119
    invoke-virtual {p0, v1, v2}, Lhaw$a;->a(II)V

    .line 120
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lhaw$a;->c(II)V

    .line 121
    invoke-virtual {p0, v2, p2}, Lhaw$a;->a(ILhdd;)V

    .line 122
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Lhaw$a;->a(II)V

    .line 123
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 140
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 141
    invoke-virtual {p0, p1}, Lhaw$a;->m(I)V

    .line 142
    return-void
.end method

.method public c(II)V
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 86
    invoke-virtual {p0, p2}, Lhaw$a;->m(I)V

    .line 87
    return-void
.end method

.method public c(IJ)V
    .locals 2

    .prologue
    .line 96
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 97
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 98
    invoke-virtual {p0, p2, p3}, Lhaw$a;->h(J)V

    .line 99
    return-void
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 149
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 150
    invoke-virtual {p0, p1, p2}, Lhaw$a;->h(J)V

    .line 151
    return-void
.end method

.method public c([BII)V
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0, p3}, Lhaw$a;->c(I)V

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p3}, Lhaw$a;->d([BII)V

    .line 115
    return-void
.end method

.method public d([BII)V
    .locals 3

    .prologue
    .line 189
    iget v0, p0, Lhaw$a;->d:I

    iget v1, p0, Lhaw$a;->e:I

    sub-int/2addr v0, v1

    if-lt v0, p3, :cond_0

    .line 190
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 191
    iget v0, p0, Lhaw$a;->e:I

    add-int/2addr v0, p3

    iput v0, p0, Lhaw$a;->e:I

    .line 204
    :goto_0
    iget v0, p0, Lhaw$a;->f:I

    add-int/2addr v0, p3

    iput v0, p0, Lhaw$a;->f:I

    .line 205
    return-void

    .line 193
    :cond_0
    iget v0, p0, Lhaw$a;->d:I

    iget v1, p0, Lhaw$a;->e:I

    sub-int/2addr v0, v1

    .line 194
    iget-object v1, p0, Lhaw$a;->c:[B

    iget v2, p0, Lhaw$a;->e:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    add-int v1, p2, v0

    .line 196
    sub-int/2addr p3, v0

    .line 197
    iget v2, p0, Lhaw$a;->d:I

    iput v2, p0, Lhaw$a;->e:I

    .line 198
    iget v2, p0, Lhaw$a;->f:I

    add-int/2addr v0, v2

    iput v0, p0, Lhaw$a;->f:I

    .line 199
    invoke-virtual {p0}, Lhaw$a;->k()V

    .line 200
    iget v0, p0, Lhaw$a;->d:I

    if-gt p3, v0, :cond_1

    .line 201
    iget-object v0, p0, Lhaw$a;->c:[B

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 202
    iput p3, p0, Lhaw$a;->e:I

    goto :goto_0

    .line 203
    :cond_1
    iget-object v0, p0, Lhaw$a;->g:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, v1, p3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 144
    invoke-virtual {p0, p1}, Lhaw$a;->n(I)V

    .line 145
    return-void
.end method

.method public e(II)V
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lhaw$a;->o(I)V

    .line 89
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lhaw$a;->l(II)V

    .line 90
    invoke-virtual {p0, p2}, Lhaw$a;->n(I)V

    .line 91
    return-void
.end method

.method final g(J)V
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v8, -0x80

    const/4 v6, 0x7

    .line 34
    sget-boolean v0, Lhaw;->a:Z

    .line 35
    if-eqz v0, :cond_2

    .line 36
    iget v0, p0, Lhaw$a;->e:I

    int-to-long v0, v0

    .line 37
    :goto_0
    and-long v2, p1, v8

    cmp-long v2, v2, v10

    if-nez v2, :cond_0

    .line 38
    iget-object v2, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhaw$a;->e:I

    int-to-long v4, v3

    long-to-int v3, p1

    int-to-byte v3, v3

    invoke-static {v2, v4, v5, v3}, Lhev;->a([BJB)V

    .line 42
    iget v2, p0, Lhaw$a;->e:I

    int-to-long v2, v2

    sub-long v0, v2, v0

    long-to-int v0, v0

    .line 43
    iget v1, p0, Lhaw$a;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lhaw$a;->f:I

    .line 48
    :goto_1
    return-void

    .line 40
    :cond_0
    iget-object v2, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhaw$a;->e:I

    int-to-long v4, v3

    long-to-int v3, p1

    and-int/lit8 v3, v3, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v2, v4, v5, v3}, Lhev;->a([BJB)V

    .line 41
    ushr-long/2addr p1, v6

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    long-to-int v2, p1

    and-int/lit8 v2, v2, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 50
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhaw$a;->f:I

    .line 51
    ushr-long/2addr p1, v6

    .line 45
    :cond_2
    and-long v0, p1, v8

    cmp-long v0, v0, v10

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    long-to-int v2, p1

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 47
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhaw$a;->f:I

    goto :goto_1
.end method

.method public h()V
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lhaw$a;->e:I

    if-lez v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lhaw$a;->k()V

    .line 188
    :cond_0
    return-void
.end method

.method final h(J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0xff

    .line 58
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    and-long v2, p1, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 59
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x8

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 60
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x10

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 61
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x18

    shr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 62
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x20

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 63
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x28

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 64
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x30

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 65
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    const/16 v2, 0x38

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 66
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhaw$a;->f:I

    .line 67
    return-void
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array or ByteBuffer."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 211
    iget-object v0, p0, Lhaw$a;->g:Ljava/io/OutputStream;

    iget-object v1, p0, Lhaw$a;->c:[B

    iget v2, p0, Lhaw$a;->e:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 212
    iput v3, p0, Lhaw$a;->e:I

    .line 213
    return-void
.end method

.method final l(II)V
    .locals 1

    .prologue
    .line 12
    .line 13
    shl-int/lit8 v0, p1, 0x3

    or-int/2addr v0, p2

    .line 14
    invoke-virtual {p0, v0}, Lhaw$a;->m(I)V

    .line 15
    return-void
.end method

.method final m(I)V
    .locals 6

    .prologue
    .line 16
    sget-boolean v0, Lhaw;->a:Z

    .line 17
    if-eqz v0, :cond_2

    .line 18
    iget v0, p0, Lhaw$a;->e:I

    int-to-long v0, v0

    .line 19
    :goto_0
    and-int/lit8 v2, p1, -0x80

    if-nez v2, :cond_0

    .line 20
    iget-object v2, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhaw$a;->e:I

    int-to-long v4, v3

    int-to-byte v3, p1

    invoke-static {v2, v4, v5, v3}, Lhev;->a([BJB)V

    .line 24
    iget v2, p0, Lhaw$a;->e:I

    int-to-long v2, v2

    sub-long v0, v2, v0

    long-to-int v0, v0

    .line 25
    iget v1, p0, Lhaw$a;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lhaw$a;->f:I

    .line 30
    :goto_1
    return-void

    .line 22
    :cond_0
    iget-object v2, p0, Lhaw$a;->c:[B

    iget v3, p0, Lhaw$a;->e:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhaw$a;->e:I

    int-to-long v4, v3

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v2, v4, v5, v3}, Lhev;->a([BJB)V

    .line 23
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    and-int/lit8 v2, p1, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 32
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhaw$a;->f:I

    .line 33
    ushr-int/lit8 p1, p1, 0x7

    .line 27
    :cond_2
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_1

    .line 28
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 29
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhaw$a;->f:I

    goto :goto_1
.end method

.method final n(I)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 53
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    shr-int/lit8 v2, p1, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 54
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    shr-int/lit8 v2, p1, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 55
    iget-object v0, p0, Lhaw$a;->c:[B

    iget v1, p0, Lhaw$a;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhaw$a;->e:I

    ushr-int/lit8 v2, p1, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 56
    iget v0, p0, Lhaw$a;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lhaw$a;->f:I

    .line 57
    return-void
.end method

.method o(I)V
    .locals 2

    .prologue
    .line 208
    iget v0, p0, Lhaw$a;->d:I

    iget v1, p0, Lhaw$a;->e:I

    sub-int/2addr v0, v1

    if-ge v0, p1, :cond_0

    .line 209
    invoke-virtual {p0}, Lhaw$a;->k()V

    .line 210
    :cond_0
    return-void
.end method
