.class public final Lcpj;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# static fields
.field public static b:Lcpj;


# instance fields
.field public final a:Lcpq;

.field public final c:Lcpo;

.field public final d:Landroid/content/Context;

.field public final e:Lcpr;

.field public f:Z

.field public g:Z

.field public h:Lcpm;

.field public final i:Ljava/lang/Runnable;

.field private j:Lcpp;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcpp;

    invoke-direct {v0}, Lcpp;-><init>()V

    iput-object v0, p0, Lcpj;->j:Lcpp;

    .line 3
    new-instance v0, Lcpr;

    invoke-direct {v0}, Lcpr;-><init>()V

    iput-object v0, p0, Lcpj;->e:Lcpr;

    .line 4
    iput-boolean v1, p0, Lcpj;->f:Z

    .line 5
    iput-boolean v1, p0, Lcpj;->g:Z

    .line 6
    new-instance v0, Lcpk;

    invoke-direct {v0, p0}, Lcpk;-><init>(Lcpj;)V

    iput-object v0, p0, Lcpj;->i:Ljava/lang/Runnable;

    .line 7
    iput-boolean v1, p0, Lcpj;->k:Z

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcpj;->d:Landroid/content/Context;

    .line 9
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VvmTaskExecutor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 10
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 11
    new-instance v1, Lcpq;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcpq;-><init>(Lcpj;Landroid/os/Looper;)V

    iput-object v1, p0, Lcpj;->a:Lcpq;

    .line 12
    new-instance v0, Lcpo;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcpo;-><init>(Lcpj;Landroid/os/Looper;)V

    iput-object v0, p0, Lcpj;->c:Lcpo;

    .line 13
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 14
    invoke-static {}, Lbvs;->f()V

    .line 15
    iget-boolean v0, p0, Lcpj;->f:Z

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 18
    :cond_1
    invoke-static {}, Lbvs;->f()V

    .line 20
    invoke-static {}, Lbvs;->f()V

    .line 21
    iget-object v0, p0, Lcpj;->e:Lcpr;

    .line 22
    invoke-virtual {v0}, Lcpr;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    const-string v0, "VvmTaskExecutor"

    const-string v1, "no more tasks, stopping service if no task are added in 5000 millis"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcpj;->c:Lcpo;

    iget-object v1, p0, Lcpj;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcpo;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 28
    :cond_2
    invoke-static {}, Lbvs;->f()V

    .line 29
    iget-object v0, p0, Lcpj;->e:Lcpr;

    .line 32
    iget-object v0, v0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcph;

    .line 33
    invoke-interface {v0}, Lcph;->g()J

    move-result-wide v4

    .line 34
    const-wide/16 v6, 0x64

    cmp-long v6, v4, v6

    if-gez v6, :cond_3

    .line 35
    new-instance v1, Lcps;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcps;-><init>(Lcph;Ljava/lang/Long;)V

    move-object v0, v1

    .line 41
    :goto_2
    iget-object v1, v0, Lcps;->a:Lcph;

    if-eqz v1, :cond_6

    .line 42
    iget-object v1, v0, Lcps;->a:Lcph;

    invoke-interface {v1}, Lcph;->h()V

    .line 43
    iget-object v1, p0, Lcpj;->a:Lcpq;

    invoke-virtual {v1}, Lcpq;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 44
    iget-object v0, v0, Lcps;->a:Lcph;

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpj;->f:Z

    .line 47
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 36
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_8

    .line 37
    :cond_4
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 38
    goto :goto_1

    .line 39
    :cond_5
    new-instance v0, Lcps;

    invoke-direct {v0, v2, v1}, Lcps;-><init>(Lcph;Ljava/lang/Long;)V

    goto :goto_2

    .line 49
    :cond_6
    const-string v1, "VvmTaskExecutor"

    iget-object v2, v0, Lcps;->b:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "minimal wait time:"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v1, v0, Lcps;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 51
    iget-object v0, v0, Lcps;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 52
    const-string v2, "VvmTaskExecutor"

    const/16 v3, 0x25

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "sleep for "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " millis"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-wide/16 v2, 0x2710

    cmp-long v2, v0, v2

    if-gez v2, :cond_7

    .line 54
    iget-object v2, p0, Lcpj;->c:Lcpo;

    new-instance v3, Lcpl;

    invoke-direct {v3, p0}, Lcpl;-><init>(Lcpj;)V

    invoke-virtual {v2, v3, v0, v1}, Lcpo;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 56
    :cond_7
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcpj;->a(JZ)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_3
.end method

.method public final a(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 63
    invoke-static {}, Lbvs;->f()V

    .line 66
    iget-boolean v0, p0, Lcpj;->g:Z

    .line 67
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbvs;->c(Z)V

    .line 68
    invoke-static {}, Lbvs;->f()V

    .line 69
    const-string v0, "VvmTaskExecutor"

    const-string v2, "finishing Job"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcpj;->h:Lcpm;

    invoke-interface {v0}, Lcpm;->a()V

    .line 71
    iput-boolean v1, p0, Lcpj;->g:Z

    .line 72
    iget-object v0, p0, Lcpj;->c:Lcpo;

    iget-object v1, p0, Lcpj;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcpo;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 73
    iget-object v0, p0, Lcpj;->c:Lcpo;

    new-instance v1, Lcpn;

    invoke-direct {v1, p0, p1, p2, p3}, Lcpn;-><init>(Lcpj;JZ)V

    invoke-virtual {v0, v1}, Lcpo;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcpm;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 58
    const-string v0, "VvmTaskExecutor"

    const-string v1, "onStartJob"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iput-object p1, p0, Lcpj;->h:Lcpm;

    .line 60
    iget-object v0, p0, Lcpj;->e:Lcpr;

    iget-object v1, p0, Lcpj;->d:Landroid/content/Context;

    invoke-virtual {v0, v1, p2}, Lcpr;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 61
    invoke-virtual {p0}, Lcpj;->a()V

    .line 62
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcpj;->h:Lcpm;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
