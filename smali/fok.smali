.class final Lfok;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final TIME_NOT_SET:J = -0x1L


# instance fields
.field public elapsedRealTimeAtMediaInitiateInMillis:J

.field public elapsedRealTimeAtMediaSetupInMillis:J

.field public final elapsedRealTimeAtStartInMillis:J

.field public ended:Z

.field public final latestStatsUpdate:Lfol;

.field public lengthSeconds:J

.field public protoEndCause:I

.field public final startTime:Ljava/lang/String;

.field public final startTimeMillis:J

.field public final statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

.field public timeAtLatestUpdate:J


# direct methods
.method constructor <init>(I)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x6

    iput v0, p0, Lfok;->protoEndCause:I

    .line 3
    new-instance v0, Lfsa;

    invoke-direct {v0, p1}, Lfsa;-><init>(I)V

    iput-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    .line 4
    new-instance v0, Lfol;

    invoke-direct {v0}, Lfol;-><init>()V

    iput-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    .line 5
    iput-wide v2, p0, Lfok;->lengthSeconds:J

    .line 6
    iput-wide v2, p0, Lfok;->elapsedRealTimeAtMediaSetupInMillis:J

    .line 7
    iput-wide v2, p0, Lfok;->elapsedRealTimeAtMediaInitiateInMillis:J

    .line 8
    iput-wide v2, p0, Lfok;->timeAtLatestUpdate:J

    .line 9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lfok;->startTimeMillis:J

    .line 10
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE MMM d HH:mm:ss yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 11
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 12
    iget-wide v2, p0, Lfok;->startTimeMillis:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfok;->startTime:Ljava/lang/String;

    .line 13
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfok;->elapsedRealTimeAtStartInMillis:J

    .line 14
    return-void
.end method

.method static synthetic access$100(Lfok;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lfok;->setElapsedRealTimeAtMediaSetup()V

    return-void
.end method

.method static synthetic access$1300(Lfok;)Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lfok;->ended:Z

    return v0
.end method

.method static synthetic access$1500$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SP4ADIN6SR9DTN4ORR78HGN8O9R9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7D666RRD5TJMURR7DHIIUOJLF9T2US3IDTQ6UBREC5N6UBQ3C5M6OSRKC5Q76923C5M6OK35E9J4ORR78LN78SJP7CKLC___0(Lfok;Lfsa;Lgit;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lfok;->outputToCallPerfLogEntry$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TKOORFDKNMERRFCTM6ABR2ELT7KBRGE9NN8RPFDPGMSRPF8DGMOR3JEHGN8SP48DGMOR2GCLP6CJ3FCT2MST3IF4TIILG_0(Lfsa;Lgit;)V

    return-void
.end method

.method static synthetic access$1700$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TKMST35E9N62R1F8DGMOR2JEHGN8QBJEHKM6SP4ADIN6SR9DTN4ORR78HGN8O9R55666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TG____0(Lfok;)Lfsa;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    return-object v0
.end method

.method static synthetic access$1800(Lfok;)Lfol;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    return-object v0
.end method

.method static synthetic access$200(Lfok;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lfok;->setElapsedRealTimeAtMediaInitiate()V

    return-void
.end method

.method static synthetic access$300(Lfok;Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lfok;->log(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    return-void
.end method

.method static synthetic access$400(Lfok;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Lfok;->logLatestStats()V

    return-void
.end method

.method static synthetic access$500(Lfok;Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lfok;->addStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    return-void
.end method

.method static synthetic access$600(Lfok;I)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lfok;->end(I)V

    return-void
.end method

.method private final addStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 9

    .prologue
    .line 64
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 65
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtStartInMillis:J

    sub-long v0, v2, v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 66
    iget-wide v0, p0, Lfok;->timeAtLatestUpdate:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 67
    iget-wide v0, p0, Lfok;->timeAtLatestUpdate:J

    sub-long v6, v2, v0

    .line 69
    :goto_0
    new-instance v1, Lfom;

    move-object v8, p1

    invoke-direct/range {v1 .. v8}, Lfom;-><init>(JJJLcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 70
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v1, v0, Lfol;->voiceSenderStats:Lfom;

    .line 85
    :goto_1
    return-void

    .line 68
    :cond_0
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtStartInMillis:J

    sub-long v6, v2, v0

    goto :goto_0

    .line 72
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;

    if-eqz v0, :cond_2

    .line 73
    check-cast p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;

    .line 74
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v0, v0, Lfol;->voiceReceiverStats:Ljava/util/Map;

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;->ssrc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 75
    :cond_2
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    if-eqz v0, :cond_3

    .line 76
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v1, v0, Lfol;->videoSenderStats:Lfom;

    goto :goto_1

    .line 77
    :cond_3
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    if-eqz v0, :cond_4

    .line 78
    check-cast p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    .line 79
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v0, v0, Lfol;->videoReceiverStats:Ljava/util/Map;

    iget v2, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->ssrc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 80
    :cond_4
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    if-eqz v0, :cond_5

    .line 81
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v1, v0, Lfol;->bandwidthEstimationStats:Lfom;

    goto :goto_1

    .line 82
    :cond_5
    instance-of v0, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;

    if-eqz v0, :cond_6

    .line 83
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v1, v0, Lfol;->connectionInfoStats:Lfom;

    goto :goto_1

    .line 84
    :cond_6
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Received unrecognized stats update, "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private final end(I)V
    .locals 4

    .prologue
    .line 19
    iget-boolean v0, p0, Lfok;->ended:Z

    .line 20
    const-string v1, "Expected condition to be false"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Z)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfok;->ended:Z

    .line 22
    iput p1, p0, Lfok;->protoEndCause:I

    .line 23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lfok;->elapsedRealTimeAtStartInMillis:J

    sub-long/2addr v0, v2

    .line 24
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lfok;->lengthSeconds:J

    .line 25
    return-void
.end method

.method private final log(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 30
    .line 31
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->voiceSenderStats:Lfom;

    .line 33
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v0, v2, Lfol;->voiceSenderStats:Lfom;

    .line 52
    :goto_0
    if-eqz v1, :cond_6

    .line 53
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0, v1}, Lfsa;->a(Ljava/lang/Object;)V

    .line 55
    :goto_1
    return-void

    .line 34
    :cond_0
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;

    .line 36
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->voiceReceiverStats:Ljava/util/Map;

    iget v2, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;->ssrc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfom;

    .line 37
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v2, v2, Lfol;->voiceReceiverStats:Ljava/util/Map;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 38
    :cond_1
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;

    if-eqz v1, :cond_2

    .line 39
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->videoSenderStats:Lfom;

    .line 40
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v0, v2, Lfol;->videoSenderStats:Lfom;

    goto :goto_0

    .line 41
    :cond_2
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    if-eqz v1, :cond_3

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;

    .line 43
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->videoReceiverStats:Ljava/util/Map;

    iget v2, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->ssrc:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfom;

    .line 44
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v2, v2, Lfol;->videoReceiverStats:Ljava/util/Map;

    iget v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;->ssrc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 45
    :cond_3
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;

    if-eqz v1, :cond_4

    .line 46
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->bandwidthEstimationStats:Lfom;

    .line 47
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v0, v2, Lfol;->bandwidthEstimationStats:Lfom;

    goto :goto_0

    .line 48
    :cond_4
    instance-of v1, p1, Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;

    if-eqz v1, :cond_5

    .line 49
    iget-object v1, p0, Lfok;->latestStatsUpdate:Lfol;

    iget-object v1, v1, Lfol;->connectionInfoStats:Lfom;

    .line 50
    iget-object v2, p0, Lfok;->latestStatsUpdate:Lfol;

    iput-object v0, v2, Lfol;->connectionInfoStats:Lfom;

    goto :goto_0

    .line 51
    :cond_5
    const-string v1, "Received unrecognized stats log, %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    goto/16 :goto_0

    .line 54
    :cond_6
    const-string v0, "Received stats object, %s that wasn\'t already in latestStatsUpdate"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method private final logLatestStats()V
    .locals 3

    .prologue
    .line 56
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfok;->timeAtLatestUpdate:J

    .line 57
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    invoke-virtual {v0}, Lfol;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    invoke-virtual {v0}, Lfol;->getStatsUpdates()Ljava/util/List;

    move-result-object v0

    .line 59
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    .line 60
    iget-object v2, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v2, v0}, Lfsa;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    invoke-virtual {v0}, Lfol;->clear()V

    .line 63
    :cond_1
    return-void
.end method

.method private final mergeAndSortDataPoints$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TIIMQCCDNMQBR7DTNMER355TH7AUJQ5TO74RRKDSNMSOBEDSNK6OBCDHPN8OBKECI46OBCDH86ASJ69HNMEHBEEHP7I924C5Q62K3FD5N78EO_0(Lfsa;)[Lgiv;
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Lfok;->logLatestStats()V

    .line 104
    invoke-direct {p0}, Lfok;->sortStatsUpdates()V

    .line 105
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    .line 106
    invoke-static {p1, v0}, Lfok;->mergeSortedStatsLists$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TKOORFDKNMERRFCTM6ABR1DPI74RR9CGNMOQB2E9GN4QB5ECNMGOBECTNNAT3J5TQN8QBC5T36IU35CH1M2S31CDKN8UA3D5P66TBCC5P42SJIC5SJMAACD9GNCO9FELQ6IR1F9HKN6T1R0(Lfsa;Lfsa;)Ljava/util/List;

    move-result-object v0

    .line 107
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 108
    check-cast v0, Ljava/util/List;

    .line 109
    invoke-static {v0}, Lfog;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lgiv;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgiv;

    return-object v0
.end method

.method private static mergeSortedStatsLists$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TKOORFDKNMERRFCTM6ABR1DPI74RR9CGNMOQB2E9GN4QB5ECNMGOBECTNNAT3J5TQN8QBC5T36IU35CH1M2S31CDKN8UA3D5P66TBCC5P42SJIC5SJMAACD9GNCO9FELQ6IR1F9HKN6T1R0(Lfsa;Lfsa;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 86
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 87
    if-nez p1, :cond_1

    move v1, v0

    .line 88
    :goto_0
    invoke-virtual {p0}, Lfsa;->b()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 89
    invoke-virtual {p0, v1}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v4

    .line 102
    :goto_1
    return-object v0

    :cond_1
    move v1, v0

    move v3, v0

    .line 94
    :goto_2
    invoke-virtual {p0}, Lfsa;->b()I

    move-result v0

    if-ge v3, v0, :cond_3

    invoke-virtual {p1}, Lfsa;->b()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 95
    invoke-virtual {p0, v3}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    iget-wide v6, v0, Lfom;->secondsSinceCallStart:J

    invoke-virtual {p1, v1}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    iget-wide v8, v0, Lfom;->secondsSinceCallStart:J

    cmp-long v0, v6, v8

    if-gez v0, :cond_2

    .line 96
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {p0, v3}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v2

    goto :goto_2

    .line 97
    :cond_2
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_2

    .line 98
    :cond_3
    :goto_3
    invoke-virtual {p0}, Lfsa;->b()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 99
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {p0, v3}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v2

    goto :goto_3

    .line 100
    :cond_4
    :goto_4
    invoke-virtual {p1}, Lfsa;->b()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 101
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto :goto_4

    :cond_5
    move-object v0, v4

    .line 102
    goto :goto_1
.end method

.method private final outputToCallPerfLogEntry$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TKOORFDKNMERRFCTM6ABR2ELT7KBRGE9NN8RPFDPGMSRPF8DGMOR3JEHGN8SP48DGMOR2GCLP6CJ3FCT2MST3IF4TIILG_0(Lfsa;Lgit;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 121
    iget-object v0, p0, Lfok;->startTime:Ljava/lang/String;

    iput-object v0, p2, Lgit;->b:Ljava/lang/String;

    .line 122
    iget-wide v0, p0, Lfok;->startTimeMillis:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, Lgit;->c:Ljava/lang/Long;

    .line 123
    iget-boolean v0, p0, Lfok;->ended:Z

    if-eqz v0, :cond_2

    .line 124
    iget v0, p0, Lfok;->protoEndCause:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lgit;->k:Ljava/lang/Integer;

    .line 125
    iget-wide v0, p0, Lfok;->lengthSeconds:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 126
    :goto_0
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 127
    iget-wide v0, p0, Lfok;->lengthSeconds:J

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lgit;->d:Ljava/lang/Integer;

    .line 129
    :goto_1
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtMediaInitiateInMillis:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 130
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtMediaInitiateInMillis:J

    iget-wide v2, p0, Lfok;->elapsedRealTimeAtStartInMillis:J

    sub-long/2addr v0, v2

    .line 131
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, Lgit;->f:Ljava/lang/Long;

    .line 132
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtMediaSetupInMillis:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 133
    iget-wide v0, p0, Lfok;->elapsedRealTimeAtMediaSetupInMillis:J

    iget-wide v2, p0, Lfok;->elapsedRealTimeAtMediaInitiateInMillis:J

    sub-long/2addr v0, v2

    .line 134
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, Lgit;->g:Ljava/lang/Long;

    .line 135
    :cond_0
    invoke-direct {p0, p1}, Lfok;->mergeAndSortDataPoints$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFELQ6IR1F8PKNGPB48DGN0OB3D5Q7IGR9E9HNAR31E90N4SJ1F4TIIMQCCDNMQBR7DTNMER355TH7AUJQ5TO74RRKDSNMSOBEDSNK6OBCDHPN8OBKECI46OBCDH86ASJ69HNMEHBEEHP7I924C5Q62K3FD5N78EO_0(Lfsa;)[Lgiv;

    move-result-object v0

    iput-object v0, p2, Lgit;->i:[Lgiv;

    .line 136
    return-void

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 128
    :cond_2
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, Lgit;->d:Ljava/lang/Integer;

    goto :goto_1
.end method

.method private final setElapsedRealTimeAtMediaInitiate()V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfok;->elapsedRealTimeAtMediaInitiateInMillis:J

    .line 29
    return-void
.end method

.method private final setElapsedRealTimeAtMediaSetup()V
    .locals 2

    .prologue
    .line 26
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfok;->elapsedRealTimeAtMediaSetupInMillis:J

    .line 27
    return-void
.end method

.method private final sortStatsUpdates()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->b()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 112
    :goto_0
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->b()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 113
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0, v2}, Lfsa;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfom;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 115
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 116
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->a()V

    move-object v0, v1

    .line 117
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v3, v2, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lfom;

    .line 118
    iget-object v4, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v4, v1}, Lfsa;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 120
    :cond_1
    return-void
.end method


# virtual methods
.method public final clearBuffer()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lfok;->latestStatsUpdate:Lfol;

    invoke-virtual {v0}, Lfol;->clear()V

    .line 17
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->a()V

    .line 18
    return-void
.end method

.method public final hasFilledBuffer()Z
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lfok;->statsUpdates$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRLEHKMOBQ6D5S6AP23C5O62OR9EHSK6QBICDQMOOBI85P74OBP7C______0:Lfsa;

    invoke-virtual {v0}, Lfsa;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
