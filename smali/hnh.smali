.class final Lhnh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field public final synthetic b:Lhng$a;

.field private c:Lhuh;

.field private d:Z


# direct methods
.method constructor <init>(Lhng$a;Lhuh;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lhnh;->b:Lhng$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lhnh;->c:Lhuh;

    .line 3
    iput-boolean p3, p0, Lhnh;->d:Z

    .line 4
    return-void
.end method


# virtual methods
.method final a()I
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lhnh;->c:Lhuh;

    .line 6
    iget-wide v0, v0, Lhuh;->c:J

    .line 7
    long-to-int v0, v0

    return v0
.end method

.method final a(I)Lhnh;
    .locals 6

    .prologue
    .line 43
    iget-object v0, p0, Lhnh;->c:Lhuh;

    .line 44
    iget-wide v0, v0, Lhuh;->c:J

    .line 45
    long-to-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 46
    new-instance v1, Lhuh;

    invoke-direct {v1}, Lhuh;-><init>()V

    .line 47
    iget-object v2, p0, Lhnh;->c:Lhuh;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Lhuh;->a_(Lhuh;J)V

    .line 48
    new-instance v2, Lhnh;

    iget-object v3, p0, Lhnh;->b:Lhng$a;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Lhnh;-><init>(Lhng$a;Lhuh;Z)V

    .line 49
    iget-boolean v1, p0, Lhnh;->a:Z

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lhnh;->b:Lhng$a;

    iget v3, v1, Lhng$a;->c:I

    sub-int v0, v3, v0

    iput v0, v1, Lhng$a;->c:I

    .line 51
    :cond_0
    return-object v2
.end method

.method final b()V
    .locals 8

    .prologue
    const v7, 0x8000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8
    :cond_0
    invoke-virtual {p0}, Lhnh;->a()I

    move-result v4

    .line 9
    iget-object v2, p0, Lhnh;->b:Lhng$a;

    iget-object v2, v2, Lhng$a;->g:Lhng;

    .line 10
    iget-object v2, v2, Lhng;->b:Lhnv;

    .line 11
    invoke-interface {v2}, Lhnv;->c()I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 12
    if-ne v2, v4, :cond_6

    .line 13
    iget-object v2, p0, Lhnh;->b:Lhng$a;

    iget-object v2, v2, Lhng$a;->g:Lhng;

    .line 14
    iget-object v2, v2, Lhng;->d:Lhng$a;

    .line 15
    neg-int v3, v4

    invoke-virtual {v2, v3}, Lhng$a;->a(I)I

    .line 16
    iget-object v2, p0, Lhnh;->b:Lhng$a;

    neg-int v3, v4

    invoke-virtual {v2, v3}, Lhng$a;->a(I)I

    .line 17
    :try_start_0
    iget-object v2, p0, Lhnh;->b:Lhng$a;

    iget-object v2, v2, Lhng$a;->g:Lhng;

    .line 18
    iget-object v2, v2, Lhng;->b:Lhnv;

    .line 19
    iget-boolean v3, p0, Lhnh;->d:Z

    iget-object v5, p0, Lhnh;->b:Lhng$a;

    iget v5, v5, Lhng$a;->b:I

    iget-object v6, p0, Lhnh;->c:Lhuh;

    invoke-interface {v2, v3, v5, v6, v4}, Lhnv;->a(ZILhuh;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    iget-object v2, p0, Lhnh;->b:Lhng$a;

    iget-object v2, v2, Lhng$a;->f:Lhmx;

    .line 24
    iget-object v5, v2, Lhmx;->j:Lhmz;

    .line 26
    iget-object v6, v5, Lio/grpc/internal/f;->f:Ljava/lang/Object;

    monitor-enter v6

    .line 27
    :try_start_1
    iget-boolean v2, v5, Lio/grpc/internal/f;->h:Z

    const-string v3, "onStreamAllocated was not called, but it seems the stream is active"

    invoke-static {v2, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 28
    iget v2, v5, Lio/grpc/internal/f;->g:I

    if-ge v2, v7, :cond_3

    move v3, v0

    .line 29
    :goto_0
    iget v2, v5, Lio/grpc/internal/f;->g:I

    sub-int/2addr v2, v4

    iput v2, v5, Lio/grpc/internal/f;->g:I

    .line 30
    iget v2, v5, Lio/grpc/internal/f;->g:I

    if-ge v2, v7, :cond_4

    move v2, v0

    .line 31
    :goto_1
    if-nez v3, :cond_5

    if-eqz v2, :cond_5

    .line 32
    :goto_2
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    invoke-virtual {v5}, Lio/grpc/internal/f;->c()V

    .line 35
    :cond_1
    iget-boolean v0, p0, Lhnh;->a:Z

    if-eqz v0, :cond_2

    .line 36
    iget-object v0, p0, Lhnh;->b:Lhng$a;

    iget v1, v0, Lhng$a;->c:I

    sub-int/2addr v1, v4

    iput v1, v0, Lhng$a;->c:I

    .line 37
    iget-object v0, p0, Lhnh;->b:Lhng$a;

    iget-object v0, v0, Lhng$a;->a:Ljava/util/Queue;

    invoke-interface {v0, p0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 42
    :cond_2
    :goto_3
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    move v3, v1

    .line 28
    goto :goto_0

    :cond_4
    move v2, v1

    .line 30
    goto :goto_1

    :cond_5
    move v0, v1

    .line 31
    goto :goto_2

    .line 32
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 39
    :cond_6
    invoke-virtual {p0, v2}, Lhnh;->a(I)Lhnh;

    move-result-object v2

    .line 40
    invoke-virtual {v2}, Lhnh;->b()V

    .line 41
    invoke-virtual {p0}, Lhnh;->a()I

    move-result v2

    if-gtz v2, :cond_0

    goto :goto_3
.end method
