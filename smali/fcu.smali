.class public final Lfcu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdc;


# instance fields
.field public final a:Lgpn;

.field private b:Z

.field private c:J

.field private d:J

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfcu;->c:J

    .line 3
    iput-boolean p1, p0, Lfcu;->b:Z

    .line 4
    new-instance v0, Lgpn;

    invoke-direct {v0}, Lgpn;-><init>()V

    iput-object v0, p0, Lfcu;->a:Lgpn;

    .line 5
    iget-object v0, p0, Lfcu;->a:Lgpn;

    new-instance v1, Lgit;

    invoke-direct {v1}, Lgit;-><init>()V

    iput-object v1, v0, Lgpn;->callPerfLogEntry:Lgit;

    .line 6
    iget-object v0, p0, Lfcu;->a:Lgpn;

    new-instance v1, Lgje;

    invoke-direct {v1}, Lgje;-><init>()V

    iput-object v1, v0, Lgpn;->callStartupEntry:Lgje;

    .line 7
    iget-object v0, p0, Lfcu;->a:Lgpn;

    new-instance v1, Lfiw;

    invoke-direct {v1}, Lfiw;-><init>()V

    invoke-static {}, Lfiw;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgpn;->localSessionId:Ljava/lang/String;

    .line 8
    iget-object v0, p0, Lfcu;->a:Lgpn;

    const/16 v1, 0x3b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgpn;->logSource:Ljava/lang/Integer;

    .line 9
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v1, p0, Lfcu;->a:Lgpn;

    iget-object v1, v1, Lgpn;->localSessionId:Ljava/lang/String;

    iput-object v1, v0, Lgit;->a:Ljava/lang/String;

    .line 10
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callStartupEntry:Lgje;

    invoke-static {}, Lfmd;->d()Lgjt;

    move-result-object v1

    iput-object v1, v0, Lgje;->b:Lgjt;

    .line 11
    return-void
.end method

.method private final a(Lfct;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12
    .line 13
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lfcu;->e:J

    .line 14
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lfcu;->c:J

    .line 15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 16
    iget-object v0, p0, Lfcu;->a:Lgpn;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lgpn;->clientReportedTimestampMsec:Ljava/lang/Long;

    .line 17
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callStartupEntry:Lgje;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lgje;->f:Ljava/lang/Long;

    .line 18
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v3, v0, Lgpn;->callStartupEntry:Lgje;

    .line 19
    iget-boolean v0, p0, Lfcu;->b:Z

    if-eqz v0, :cond_4

    .line 20
    const/16 v0, 0x68

    .line 22
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lgje;->e:Ljava/lang/Integer;

    .line 23
    const-string v3, ""

    .line 24
    const/4 v0, 0x0

    .line 26
    iget-object v6, p1, Lfct;->e:Lfdd;

    .line 28
    if-eqz v6, :cond_0

    .line 29
    iget-object v7, v6, Lfdd;->d:Lffd;

    .line 30
    if-eqz v7, :cond_0

    .line 33
    iget-object v0, v6, Lfdd;->a:Lfef;

    .line 34
    iget-object v0, v0, Lfef;->e:Ljava/lang/String;

    .line 35
    iget-object v3, v6, Lfdd;->d:Lffd;

    .line 36
    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Lffd;)Ljava/lang/String;

    move-result-object v0

    .line 38
    iget-object v3, v6, Lfdd;->d:Lffd;

    .line 39
    invoke-virtual {v3}, Lffd;->a()Ljava/lang/String;

    move-result-object v3

    .line 40
    iget-object v7, p0, Lfcu;->a:Lgpn;

    iget-object v7, v7, Lgpn;->callPerfLogEntry:Lgit;

    iput-object v3, v7, Lgit;->h:Ljava/lang/String;

    .line 41
    :cond_0
    if-eqz v0, :cond_1

    .line 42
    iget-object v7, p0, Lfcu;->a:Lgpn;

    invoke-static {v0}, Lfmd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lgpn;->participantId:Ljava/lang/String;

    .line 43
    iget-object v7, p0, Lfcu;->a:Lgpn;

    iget-object v7, v7, Lgpn;->callStartupEntry:Lgje;

    invoke-static {v0}, Lfmd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lgje;->d:Ljava/lang/String;

    .line 44
    :cond_1
    if-eqz v6, :cond_3

    .line 46
    iget-object v7, v6, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 49
    iget-object v0, v6, Lfdd;->a:Lfef;

    .line 50
    iget-object v0, v0, Lfef;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 51
    iget-object v0, p0, Lfcu;->a:Lgpn;

    .line 52
    iget-object v8, v6, Lfdd;->a:Lfef;

    .line 53
    iget-object v8, v8, Lfef;->d:Ljava/lang/String;

    iput-object v8, v0, Lgpn;->participantLogId:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lfcu;->a:Lgpn;

    invoke-static {v7}, Lfmd;->d(Landroid/content/Context;)Lgju;

    move-result-object v8

    iput-object v8, v0, Lgpn;->systemInfoLogEntry:Lgju;

    .line 55
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callStartupEntry:Lgje;

    .line 57
    iget-object v6, v6, Lfdd;->j:Lffb;

    .line 59
    iget-object v6, v6, Lffb;->a:Ljava/lang/String;

    .line 60
    if-eqz v6, :cond_9

    .line 61
    const-string v8, "310260"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 62
    const/4 v1, 0x2

    .line 73
    :cond_2
    :goto_2
    invoke-static {v7}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-static {v1, v2, v3}, Lfmd;->a(ILjava/lang/String;Ljava/lang/String;)Lgjr;

    move-result-object v1

    iput-object v1, v0, Lgje;->c:Lgjr;

    .line 75
    :cond_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE MMM d HH:mm:ss yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 76
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 77
    iget-object v1, p0, Lfcu;->a:Lgpn;

    iget-object v1, v1, Lgpn;->callPerfLogEntry:Lgit;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lgit;->b:Ljava/lang/String;

    .line 78
    return-void

    .line 21
    :cond_4
    const/16 v0, 0x67

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 50
    goto :goto_1

    .line 63
    :cond_6
    const-string v8, "310120"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 65
    const-string v1, "311580"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 66
    const/4 v1, 0x3

    goto :goto_2

    .line 67
    :cond_7
    const-string v1, "23420"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 68
    const/4 v1, 0x4

    goto :goto_2

    .line 69
    :cond_8
    const-string v1, "45403"

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 70
    const/4 v1, 0x5

    goto :goto_2

    :cond_9
    move v1, v2

    .line 71
    goto :goto_2
.end method

.method private final b(Lfct;)V
    .locals 8

    .prologue
    .line 79
    const-string v0, "CircuitSwitchedCallStatistics.reportCallStats"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v0, p1, Lfct;->e:Lfdd;

    .line 83
    iget-object v7, v0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 85
    invoke-static {v7}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    iget-object v1, p0, Lfcu;->a:Lgpn;

    iput-object v0, v1, Lgpn;->clientId:Ljava/lang/String;

    .line 88
    :cond_0
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v0, v0, Lgit;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 89
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    const-string v1, "never"

    iput-object v1, v0, Lgit;->b:Ljava/lang/String;

    .line 91
    :cond_1
    iget-object v0, p1, Lfct;->e:Lfdd;

    .line 93
    iget-object v1, v0, Lfdd;->f:Ljava/lang/String;

    .line 95
    new-instance v4, Lgot;

    invoke-direct {v4}, Lgot;-><init>()V

    .line 96
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iput-object v0, v4, Lgot;->logData:Lgpn;

    .line 97
    iget-object v0, v4, Lgot;->logData:Lgpn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 99
    invoke-static {v7}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {v7}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 101
    invoke-static {v7, v0, v2, v3}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v0

    iput-object v0, v4, Lgot;->requestHeader:Lgls;

    .line 102
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v0, v0, Lgit;->a:Ljava/lang/String;

    iput-object v0, v4, Lgot;->sessionId:Ljava/lang/String;

    .line 104
    :cond_2
    const-string v0, "media_session_log_request_key"

    .line 105
    invoke-static {v7}, Lfmd;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_sessions/log"

    const/4 v5, 0x1

    .line 106
    invoke-static {v7}, Lfmd;->l(Landroid/content/Context;)I

    move-result v6

    .line 107
    invoke-static/range {v0 .. v6}, Lfic;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 108
    invoke-static {v7, v0}, Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;->a(Landroid/content/Context;Landroid/os/PersistableBundle;)V

    .line 109
    return-void
.end method


# virtual methods
.method public final a(Lfdb;I)V
    .locals 2

    .prologue
    .line 110
    packed-switch p2, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 111
    :pswitch_0
    check-cast p1, Lfct;

    invoke-direct {p0, p1}, Lfcu;->a(Lfct;)V

    goto :goto_0

    .line 113
    :pswitch_1
    iget-boolean v0, p0, Lfcu;->b:Z

    if-eqz v0, :cond_0

    .line 114
    check-cast p1, Lfct;

    invoke-direct {p0, p1}, Lfcu;->a(Lfct;)V

    goto :goto_0

    .line 115
    :pswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfcu;->f:Z

    .line 117
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lfcu;->d:J

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lfdb;Landroid/telecom/DisconnectCause;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 119
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x44

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedCallStatistics.onCallDisconnected, disconnect cause: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    check-cast p1, Lfct;

    .line 121
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callStartupEntry:Lgje;

    iget-boolean v1, p0, Lfcu;->f:Z

    .line 122
    invoke-static {p2, v1}, Lfmd;->a(Landroid/telecom/DisconnectCause;Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgje;->a:Ljava/lang/Integer;

    .line 123
    const-wide/16 v0, -0x3e8

    .line 124
    iget-wide v4, p0, Lfcu;->c:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 125
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lfcu;->c:J

    sub-long/2addr v0, v4

    .line 126
    iget-wide v4, p0, Lfcu;->e:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_0

    .line 127
    iget-object v3, p0, Lfcu;->a:Lgpn;

    iget-object v3, v3, Lgpn;->callPerfLogEntry:Lgit;

    iget-wide v4, p0, Lfcu;->e:J

    iget-wide v6, p0, Lfcu;->c:J

    sub-long/2addr v4, v6

    .line 128
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lgit;->f:Ljava/lang/Long;

    .line 129
    iget-wide v4, p0, Lfcu;->d:J

    cmp-long v3, v4, v8

    if-lez v3, :cond_0

    .line 130
    iget-object v3, p0, Lfcu;->a:Lgpn;

    iget-object v3, v3, Lgpn;->callPerfLogEntry:Lgit;

    iget-wide v4, p0, Lfcu;->d:J

    iget-wide v6, p0, Lfcu;->e:J

    sub-long/2addr v4, v6

    .line 131
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lgit;->g:Ljava/lang/Long;

    .line 132
    :cond_0
    iget-object v3, p0, Lfcu;->a:Lgpn;

    iget-object v3, v3, Lgpn;->callPerfLogEntry:Lgit;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 133
    invoke-virtual {v4, v0, v1, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lgit;->d:Ljava/lang/Integer;

    .line 134
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    invoke-virtual {p2}, Landroid/telecom/DisconnectCause;->getReason()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgit;->l:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lfcu;->a:Lgpn;

    iget-object v1, v0, Lgpn;->callPerfLogEntry:Lgit;

    .line 136
    iget-boolean v0, p0, Lfcu;->f:Z

    if-eqz v0, :cond_2

    .line 137
    invoke-static {p2}, Lfmd;->a(Landroid/telecom/DisconnectCause;)I

    move-result v0

    .line 139
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lgit;->k:Ljava/lang/Integer;

    .line 142
    iget-object v0, p1, Lfct;->e:Lfdd;

    .line 144
    if-nez v0, :cond_3

    .line 145
    const-string v0, "CircuitSwitchedCallStatistics.reportCallStats, no connection"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 160
    :goto_1
    if-eqz v0, :cond_1

    .line 161
    invoke-direct {p0, p1}, Lfcu;->b(Lfct;)V

    .line 162
    :cond_1
    return-void

    .line 138
    :cond_2
    const/16 v0, 0x48

    goto :goto_0

    .line 148
    :cond_3
    iget-object v1, v0, Lfdd;->f:Ljava/lang/String;

    .line 150
    if-nez v1, :cond_4

    .line 151
    const-string v0, "CircuitSwitchedCallStatistics.reportCallStats, no account name"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 152
    goto :goto_1

    .line 154
    :cond_4
    iget-object v0, v0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 155
    invoke-static {v0}, Lfho;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    const/4 v0, 0x1

    goto :goto_1

    .line 158
    :cond_5
    const-string v0, "CircuitSwitchedCallStatistics.reportCallStats, no previous remote session id"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 159
    goto :goto_1
.end method
