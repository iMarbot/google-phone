.class public Lbna;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;Lbbh;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 14
    invoke-static {p0}, Lbnc;->a(Landroid/content/Context;)Lbnc;

    move-result-object v0

    invoke-virtual {v0}, Lbnc;->a()Lbna;

    move-result-object v0

    invoke-virtual {v0}, Lbna;->a()Lgue;

    move-result-object v0

    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v4

    move v3, v2

    :cond_0
    if-ge v3, v4, :cond_1

    invoke-virtual {v0, v3}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lbnb;

    .line 15
    invoke-interface {v1, p0, p1}, Lbnb;->a(Landroid/content/Context;Lbbh;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 16
    const-string v0, "PreCallImpl.requiresUi"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " requested UI"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    const/4 v0, 0x1

    .line 19
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->dP:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 3
    invoke-static {p1, p2}, Lbna;->b(Landroid/content/Context;Lbbh;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4
    const-string v0, "PreCallImpl.buildIntent"

    const-string v2, "No UI requested, running pre-call directly"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    invoke-static {p1}, Lbnc;->a(Landroid/content/Context;)Lbnc;

    move-result-object v0

    invoke-virtual {v0}, Lbnc;->a()Lbna;

    move-result-object v0

    invoke-virtual {v0}, Lbna;->a()Lgue;

    move-result-object v0

    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lbnb;

    .line 6
    invoke-interface {v1, p1, p2}, Lbnb;->b(Landroid/content/Context;Lbbh;)V

    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p2}, Lbbh;->a()Landroid/content/Intent;

    move-result-object v0

    .line 12
    :goto_1
    return-object v0

    .line 9
    :cond_1
    const-string v0, "PreCallImpl.buildIntent"

    const-string v2, "building intent to start activity"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/precall/impl/PreCallActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11
    const-string v1, "extra_call_intent_builder"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public a()Lgue;
    .locals 2

    .prologue
    .line 1
    new-instance v0, Lbnh;

    invoke-direct {v0}, Lbnh;-><init>()V

    new-instance v1, Lbng;

    invoke-direct {v1}, Lbng;-><init>()V

    invoke-static {v0, v1}, Lgue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lgue;

    move-result-object v0

    return-object v0
.end method
