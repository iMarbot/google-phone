.class public final Latn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lane;

.field private c:Latx;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Lane;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lane;Lane;Latx;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Latn;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Latn;->b:Lane;

    .line 4
    iput-object p3, p0, Latn;->k:Lane;

    .line 5
    iput-object p4, p0, Latn;->c:Latx;

    .line 6
    const v0, 0x7f0400c8

    invoke-interface {p2, v0}, Lane;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Latn;->d:Landroid/view/View;

    .line 7
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e0286

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->e:Landroid/widget/TextView;

    .line 8
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e0287

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->f:Landroid/widget/TextView;

    .line 9
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e023f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->g:Landroid/widget/TextView;

    .line 10
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e023e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->h:Landroid/widget/TextView;

    .line 11
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e0289

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->i:Landroid/widget/TextView;

    .line 12
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    const v1, 0x7f0e0288

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latn;->j:Landroid/widget/TextView;

    .line 13
    return-void
.end method

.method private final a(Lato;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v3, 0x0

    .line 98
    new-array v4, v8, [Landroid/widget/TextView;

    iget-object v0, p0, Latn;->g:Landroid/widget/TextView;

    aput-object v0, v4, v3

    iget-object v0, p0, Latn;->h:Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 99
    new-array v5, v8, [Landroid/widget/TextView;

    iget-object v0, p0, Latn;->i:Landroid/widget/TextView;

    aput-object v0, v5, v3

    iget-object v0, p0, Latn;->j:Landroid/widget/TextView;

    aput-object v0, v5, v1

    move v2, v3

    .line 100
    :goto_0
    if-ge v2, v8, :cond_2

    .line 102
    iget-object v0, p1, Lato;->c:Ljava/util/List;

    .line 103
    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p1, Lato;->c:Ljava/util/List;

    .line 105
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 107
    iget-object v0, p1, Lato;->c:Ljava/util/List;

    .line 108
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latw;

    .line 110
    iget-boolean v1, v0, Latw;->c:Z

    .line 111
    if-eqz v1, :cond_0

    .line 112
    aget-object v1, v5, v2

    .line 113
    aget-object v6, v4, v2

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    :goto_1
    iget-object v6, v0, Latw;->a:Ljava/lang/CharSequence;

    .line 118
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, v0, Latw;->b:Landroid/view/View$OnClickListener;

    .line 121
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 114
    :cond_0
    aget-object v1, v4, v2

    .line 115
    aget-object v6, v5, v2

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 124
    :cond_1
    aget-object v0, v4, v2

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    aget-object v0, v5, v2

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 127
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Laua;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 14
    const-string v0, "VoicemailErrorAlert.updateStatus"

    const-string v2, "%d status"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v0, v2, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Latn;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 17
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v0, v1

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laty;

    .line 18
    iget-object v6, p0, Latn;->a:Landroid/content/Context;

    .line 19
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x19

    if-ge v2, v7, :cond_4

    move-object v0, v1

    .line 25
    :goto_0
    if-eqz v0, :cond_0

    .line 27
    :cond_1
    iget-object v1, p0, Latn;->b:Lane;

    invoke-interface {v1}, Lane;->a()V

    .line 28
    iget-object v1, p0, Latn;->k:Lane;

    invoke-interface {v1}, Lane;->a()V

    .line 29
    if-eqz v0, :cond_3

    .line 30
    const-string v1, "VoicemailErrorAlert.updateStatus"

    const-string v2, "isModal: %b, %s"

    new-array v5, v9, [Ljava/lang/Object;

    .line 32
    iget-boolean v6, v0, Lato;->d:Z

    .line 33
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v4

    .line 35
    iget-object v6, v0, Lato;->a:Ljava/lang/CharSequence;

    .line 36
    aput-object v6, v5, v3

    .line 37
    invoke-static {v1, v2, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-boolean v1, v0, Lato;->d:Z

    .line 40
    if-eqz v1, :cond_8

    .line 41
    instance-of v1, v0, Lauc;

    if-eqz v1, :cond_7

    .line 42
    iget-object v1, p0, Latn;->k:Lane;

    check-cast v0, Lauc;

    .line 43
    const v2, 0x7f0400ca

    invoke-interface {v1, v2}, Lane;->a(I)Landroid/view/View;

    move-result-object v5

    .line 44
    const v1, 0x7f0e0293

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 46
    iget-object v2, v0, Lato;->a:Ljava/lang/CharSequence;

    .line 47
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const v1, 0x7f0e0294

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 50
    iget-object v2, v0, Lato;->b:Ljava/lang/CharSequence;

    .line 51
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 54
    iget-object v1, v0, Lato;->c:Ljava/util/List;

    .line 55
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v9, :cond_6

    move v1, v3

    :goto_1
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 57
    iget-object v1, v0, Lato;->c:Ljava/util/List;

    .line 58
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Latw;

    .line 59
    const v2, 0x7f0e0296

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 61
    iget-object v6, v1, Latw;->a:Ljava/lang/CharSequence;

    .line 62
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v1, v1, Latw;->b:Landroid/view/View$OnClickListener;

    .line 65
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v1, v0, Lato;->c:Ljava/util/List;

    .line 68
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Latw;

    .line 69
    const v2, 0x7f0e0297

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 71
    iget-object v3, v1, Latw;->a:Ljava/lang/CharSequence;

    .line 72
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v1, v1, Latw;->b:Landroid/view/View$OnClickListener;

    .line 75
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v1, v0, Lato;->e:Ljava/lang/Integer;

    .line 78
    if-eqz v1, :cond_2

    .line 79
    const v1, 0x7f0e0292

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 81
    iget-object v0, v0, Lato;->e:Ljava/lang/Integer;

    .line 82
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 83
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    :cond_2
    iput-object v5, p0, Latn;->l:Landroid/view/View;

    .line 87
    iget-object v0, p0, Latn;->k:Lane;

    iget-object v1, p0, Latn;->l:Landroid/view/View;

    invoke-interface {v0, v1}, Lane;->a(Landroid/view/View;)V

    .line 97
    :cond_3
    :goto_2
    return-void

    .line 21
    :cond_4
    iget-object v7, v0, Laty;->b:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_5
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 23
    invoke-static {v6, v0, p2}, Lapw;->a(Landroid/content/Context;Laty;Laua;)Lato;

    move-result-object v0

    goto/16 :goto_0

    .line 21
    :pswitch_0
    const-string v8, "vvm_type_vvm3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v2, v4

    goto :goto_3

    .line 22
    :pswitch_1
    invoke-static {v6, v0, p2}, Laqx;->a(Landroid/content/Context;Laty;Laua;)Lato;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v1, v4

    .line 55
    goto :goto_1

    .line 86
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Modal message type is undefined!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_8
    iget-object v1, p0, Latn;->e:Landroid/widget/TextView;

    .line 90
    iget-object v2, v0, Lato;->a:Ljava/lang/CharSequence;

    .line 91
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v1, p0, Latn;->f:Landroid/widget/TextView;

    .line 93
    iget-object v2, v0, Lato;->b:Ljava/lang/CharSequence;

    .line 94
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    invoke-direct {p0, v0}, Latn;->a(Lato;)V

    .line 96
    iget-object v0, p0, Latn;->b:Lane;

    iget-object v1, p0, Latn;->d:Landroid/view/View;

    invoke-interface {v0, v1}, Lane;->a(Landroid/view/View;)V

    goto :goto_2

    .line 21
    :pswitch_data_0
    .packed-switch -0x5821a607
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
