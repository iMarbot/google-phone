.class public final Lbaa;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static i:Ljava/util/HashSet;


# instance fields
.field public j:Lazz;

.field private k:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 140
    const/16 v0, 0x112

    invoke-static {v1, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->a:I

    .line 141
    const/16 v0, -0x7897

    invoke-static {v1, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->b:I

    .line 142
    const/16 v0, -0x77db

    invoke-static {v1, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->c:I

    .line 143
    const/16 v0, 0x111

    invoke-static {v1, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->d:I

    .line 144
    const/16 v0, 0x117

    invoke-static {v1, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->e:I

    .line 145
    const/16 v0, 0x201

    invoke-static {v2, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->f:I

    .line 146
    const/16 v0, 0x202

    invoke-static {v2, v0}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->g:I

    .line 147
    const/4 v0, 0x2

    const/16 v1, -0x5ffb

    invoke-static {v0, v1}, Lbaa;->a(IS)I

    move-result v0

    sput v0, Lbaa;->h:I

    .line 148
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 149
    sput-object v0, Lbaa;->i:Ljava/util/HashSet;

    sget v1, Lbaa;->c:I

    .line 150
    int-to-short v1, v1

    .line 151
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v0, Lbaa;->i:Ljava/util/HashSet;

    sget v1, Lbaa;->b:I

    .line 153
    int-to-short v1, v1

    .line 154
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v0, Lbaa;->i:Ljava/util/HashSet;

    sget v1, Lbaa;->f:I

    .line 156
    int-to-short v1, v1

    .line 157
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v0, Lbaa;->i:Ljava/util/HashSet;

    sget v1, Lbaa;->h:I

    .line 159
    int-to-short v1, v1

    .line 160
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v0, Lbaa;->i:Ljava/util/HashSet;

    sget v1, Lbaa;->d:I

    .line 162
    int-to-short v1, v1

    .line 163
    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 164
    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lazz;

    invoke-direct {v0}, Lazz;-><init>()V

    iput-object v0, p0, Lbaa;->j:Lazz;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    .line 4
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy:MM:dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 5
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 6
    return-void
.end method

.method private static a(IS)I
    .locals 2

    .prologue
    .line 74
    const v0, 0xffff

    and-int/2addr v0, p1

    shl-int/lit8 v1, p0, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method private static a([I)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 103
    array-length v0, p0

    if-nez v0, :cond_1

    move v0, v1

    .line 115
    :cond_0
    return v0

    .line 106
    :cond_1
    sget-object v4, Lbaj;->c:[I

    move v3, v1

    move v0, v1

    .line 108
    :goto_0
    const/4 v2, 0x5

    if-ge v3, v2, :cond_0

    .line 109
    array-length v5, p0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget v6, p0, v2

    .line 110
    aget v7, v4, v3

    if-ne v7, v6, :cond_3

    .line 111
    const/4 v2, 0x1

    shl-int/2addr v2, v3

    or-int/2addr v0, v2

    .line 114
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 113
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static a(I)S
    .locals 1

    .prologue
    .line 73
    int-to-short v0, p0

    return v0
.end method

.method public static a(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 135
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x5a

    invoke-virtual {p0, v0, v1, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 138
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 139
    return-void
.end method

.method static a(II)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 75
    sget-object v3, Lbaj;->c:[I

    .line 78
    ushr-int/lit8 v4, p0, 0x18

    move v0, v1

    .line 80
    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_0

    .line 81
    aget v5, v3, v0

    if-ne p1, v5, :cond_1

    shr-int v5, v4, v0

    and-int/lit8 v5, v5, 0x1

    if-ne v5, v2, :cond_1

    move v1, v2

    .line 84
    :cond_0
    return v1

    .line 83
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 116
    ushr-int/lit8 v0, p0, 0x10

    return v0
.end method

.method public static c(I)Lbab;
    .locals 4

    .prologue
    const/16 v3, 0x10e

    const/16 v2, 0x5a

    const/4 v1, 0x1

    .line 117
    new-instance v0, Lbab;

    invoke-direct {v0}, Lbab;-><init>()V

    .line 118
    packed-switch p0, :pswitch_data_0

    .line 134
    :goto_0
    :pswitch_0
    return-object v0

    .line 121
    :pswitch_1
    const/16 v1, 0xb4

    iput v1, v0, Lbab;->a:I

    goto :goto_0

    .line 123
    :pswitch_2
    iput v3, v0, Lbab;->a:I

    .line 124
    iput-boolean v1, v0, Lbab;->b:Z

    goto :goto_0

    .line 126
    :pswitch_3
    iput v2, v0, Lbab;->a:I

    .line 127
    iput-boolean v1, v0, Lbab;->b:Z

    goto :goto_0

    .line 129
    :pswitch_4
    iput v2, v0, Lbab;->a:I

    .line 130
    iput-boolean v1, v0, Lbab;->b:Z

    goto :goto_0

    .line 132
    :pswitch_5
    iput v3, v0, Lbab;->a:I

    .line 133
    iput-boolean v1, v0, Lbab;->b:Z

    goto :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Landroid/util/SparseIntArray;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/high16 v4, 0x40000

    .line 85
    iget-object v0, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    .line 88
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    .line 89
    invoke-static {v0}, Lbaa;->a([I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    .line 90
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->d:I

    or-int v3, v0, v4

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 91
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->b:I

    or-int v3, v0, v4

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 92
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->c:I

    or-int v3, v0, v4

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 93
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->a:I

    const/high16 v3, 0x30000

    or-int/2addr v3, v0

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 94
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->e:I

    or-int/2addr v0, v4

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 95
    new-array v0, v5, [I

    aput v5, v0, v6

    .line 96
    invoke-static {v0}, Lbaa;->a([I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    .line 97
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->f:I

    or-int v3, v0, v4

    or-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 98
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->g:I

    or-int/2addr v0, v4

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 99
    new-array v0, v5, [I

    aput v7, v0, v6

    .line 100
    invoke-static {v0}, Lbaa;->a([I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    .line 101
    iget-object v1, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    sget v2, Lbaa;->h:I

    or-int/2addr v0, v4

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 102
    :cond_0
    iget-object v0, p0, Lbaa;->k:Landroid/util/SparseIntArray;

    return-object v0

    .line 88
    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final a(Ljava/io/InputStream;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 7
    :try_start_0
    new-instance v0, Lbah;

    invoke-direct {v0, p0}, Lbah;-><init>(Lbaa;)V

    .line 8
    iget-object v0, v0, Lbah;->a:Lbaa;

    .line 9
    new-instance v2, Lbad;

    const/16 v3, 0x3f

    invoke-direct {v2, p1, v3, v0}, Lbad;-><init>(Ljava/io/InputStream;ILbaa;)V

    .line 11
    new-instance v3, Lazz;

    invoke-direct {v3}, Lazz;-><init>()V

    .line 12
    invoke-virtual {v2}, Lbad;->a()I

    move-result v0

    .line 13
    :goto_0
    const/4 v4, 0x5

    if-eq v0, v4, :cond_5

    .line 14
    packed-switch v0, :pswitch_data_0

    .line 65
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lbad;->a()I

    move-result v0

    goto :goto_0

    .line 15
    :pswitch_0
    new-instance v0, Lbaj;

    .line 16
    iget v4, v2, Lbad;->b:I

    .line 17
    invoke-direct {v0, v4}, Lbaj;-><init>(I)V

    .line 18
    iget-object v4, v3, Lazz;->a:[Lbaj;

    .line 19
    iget v5, v0, Lbaj;->a:I

    .line 20
    aput-object v0, v4, v5
    :try_end_0
    .catch Lbac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 69
    :catch_0
    move-exception v0

    .line 70
    new-instance v1, Ljava/io/IOException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid exif format : "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 23
    :pswitch_1
    :try_start_1
    iget-object v0, v2, Lbad;->c:Lbai;

    .line 25
    invoke-virtual {v0}, Lbai;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 28
    iget v4, v0, Lbai;->i:I

    .line 29
    iget-object v5, v2, Lbad;->a:Lazy;

    .line 30
    iget v5, v5, Lazy;->a:I

    .line 31
    if-lt v4, v5, :cond_0

    .line 32
    iget-object v4, v2, Lbad;->f:Ljava/util/TreeMap;

    .line 33
    iget v5, v0, Lbai;->i:I

    .line 34
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lbae;

    const/4 v7, 0x1

    invoke-direct {v6, v0, v7}, Lbae;-><init>(Lbai;Z)V

    invoke-virtual {v4, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 37
    :cond_1
    iget v4, v0, Lbai;->g:I

    .line 38
    invoke-virtual {v3, v4}, Lazz;->a(I)Lbaj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lbaj;->a(Lbai;)Lbai;

    goto :goto_1

    .line 41
    :pswitch_2
    iget-object v0, v2, Lbad;->c:Lbai;

    .line 44
    iget-short v4, v0, Lbai;->d:S

    .line 45
    const/4 v5, 0x7

    if-ne v4, v5, :cond_2

    .line 46
    invoke-virtual {v2, v0}, Lbad;->a(Lbai;)V

    .line 48
    :cond_2
    iget v4, v0, Lbai;->g:I

    .line 49
    invoke-virtual {v3, v4}, Lazz;->a(I)Lbaj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lbaj;->a(Lbai;)Lbai;

    goto :goto_1

    .line 52
    :pswitch_3
    iget-object v0, v2, Lbad;->e:Lbai;

    if-nez v0, :cond_3

    move v0, v1

    .line 55
    :goto_2
    new-array v0, v0, [B

    .line 56
    array-length v4, v0

    invoke-virtual {v2, v0}, Lbad;->a([B)I

    move-result v0

    if-eq v4, v0, :cond_0

    .line 57
    const-string v0, "ExifReader.read"

    const-string v4, "Failed to read the compressed thumbnail"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 54
    :cond_3
    iget-object v0, v2, Lbad;->e:Lbai;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lbai;->b(I)J

    move-result-wide v4

    long-to-int v0, v4

    goto :goto_2

    .line 59
    :pswitch_4
    iget-object v0, v2, Lbad;->d:Lbai;

    if-nez v0, :cond_4

    move v0, v1

    .line 62
    :goto_3
    new-array v0, v0, [B

    .line 63
    array-length v4, v0

    invoke-virtual {v2, v0}, Lbad;->a([B)I

    move-result v0

    if-eq v4, v0, :cond_0

    .line 64
    const-string v0, "ExifReader.read"

    const-string v4, "Failed to read the strip bytes"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 61
    :cond_4
    iget-object v0, v2, Lbad;->d:Lbai;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lbai;->b(I)J
    :try_end_1
    .catch Lbac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    long-to-int v0, v4

    goto :goto_3

    .line 71
    :cond_5
    iput-object v3, p0, Lbaa;->j:Lazz;

    .line 72
    return-void

    .line 14
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
