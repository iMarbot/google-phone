.class final Lhme;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhnv;


# static fields
.field public static final a:Ljava/util/logging/Logger;


# instance fields
.field public b:Lhnv;

.field public c:Ljava/net/Socket;

.field public final d:Lhna;

.field private e:Lio/grpc/internal/es;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lhna;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lhme;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lhna;Lio/grpc/internal/es;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhme;->d:Lhna;

    .line 3
    iput-object p2, p0, Lhme;->e:Lio/grpc/internal/es;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmf;

    invoke-direct {v1, p0}, Lhmf;-><init>(Lhme;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 10
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmj;

    invoke-direct {v1, p0, p1, p2, p3}, Lhmj;-><init>(Lhme;IJ)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 28
    return-void
.end method

.method public final a(ILhns;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmo;

    invoke-direct {v1, p0, p1, p2}, Lhmo;-><init>(Lhme;ILhns;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 18
    return-void
.end method

.method public final a(ILhns;[B)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0, p1, p2, p3}, Lhmi;-><init>(Lhme;ILhns;[B)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 26
    return-void
.end method

.method final a(Lhnv;Ljava/net/Socket;)V
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lhme;->b:Lhnv;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "AsyncFrameWriter\'s setFrameWriter() should only be called once."

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 6
    const-string v0, "frameWriter"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnv;

    iput-object v0, p0, Lhme;->b:Lhnv;

    .line 7
    const-string v0, "socket"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Socket;

    iput-object v0, p0, Lhme;->c:Ljava/net/Socket;

    .line 8
    return-void

    .line 5
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhof;)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhml;

    invoke-direct {v1, p0, p1}, Lhml;-><init>(Lhme;Lhof;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 12
    return-void
.end method

.method public final a(ZII)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmh;

    invoke-direct {v1, p0, p1, p2, p3}, Lhmh;-><init>(Lhme;ZII)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 24
    return-void
.end method

.method public final a(ZILhuh;I)V
    .locals 7

    .prologue
    .line 19
    iget-object v6, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v0, Lhmp;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lhmp;-><init>(Lhme;ZILhuh;I)V

    invoke-virtual {v6, v0}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 20
    return-void
.end method

.method public final a(ZZIILjava/util/List;)V
    .locals 8

    .prologue
    .line 15
    iget-object v7, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v0, Lhmn;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lhmn;-><init>(Lhme;ZZIILjava/util/List;)V

    invoke-virtual {v7, v0}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 16
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmm;

    invoke-direct {v1, p0}, Lhmm;-><init>(Lhme;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 14
    return-void
.end method

.method public final b(Lhof;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmg;

    invoke-direct {v1, p0, p1}, Lhmg;-><init>(Lhme;Lhof;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 22
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lhme;->b:Lhnv;

    if-nez v0, :cond_0

    const/16 v0, 0x4000

    .line 33
    :goto_0
    return v0

    .line 32
    :cond_0
    iget-object v0, p0, Lhme;->b:Lhnv;

    invoke-interface {v0}, Lhnv;->c()I

    move-result v0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lhme;->e:Lio/grpc/internal/es;

    new-instance v1, Lhmk;

    invoke-direct {v1, p0}, Lhmk;-><init>(Lhme;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/es;->execute(Ljava/lang/Runnable;)V

    .line 30
    return-void
.end method
