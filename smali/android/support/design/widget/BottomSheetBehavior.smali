.class public Landroid/support/design/widget/BottomSheetBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$a;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/widget/BottomSheetBehavior$b;,
        Landroid/support/design/widget/BottomSheetBehavior$c;,
        Landroid/support/design/widget/BottomSheetBehavior$a;
    }
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:Ltr;

.field public h:I

.field public i:Ljava/lang/ref/WeakReference;

.field public j:Ljava/lang/ref/WeakReference;

.field public k:Landroid/support/design/widget/BottomSheetBehavior$a;

.field public l:I

.field public m:Z

.field private n:F

.field private o:I

.field private p:Z

.field private q:I

.field private r:I

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Z

.field private w:Landroid/view/VelocityTracker;

.field private x:I

.field private y:Ltu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$a;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    .line 3
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 4
    new-instance v0, Lcd;

    invoke-direct {v0, p0}, Lcd;-><init>(Landroid/support/design/widget/BottomSheetBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->y:Ltu;

    .line 5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 6
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 7
    iput-boolean v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    .line 8
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 9
    new-instance v0, Lcd;

    invoke-direct {v0, p0}, Lcd;-><init>(Landroid/support/design/widget/BottomSheetBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->y:Ltu;

    .line 10
    sget-object v0, Lao;->h:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 11
    sget v0, Lao;->k:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    .line 12
    if-eqz v0, :cond_2

    iget v2, v0, Landroid/util/TypedValue;->data:I

    if-ne v2, v3, :cond_2

    .line 13
    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-direct {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->d(I)V

    .line 17
    :goto_0
    sget v0, Lao;->j:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 18
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    .line 19
    sget v0, Lao;->i:I

    .line 20
    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 22
    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    if-eq v2, v0, :cond_1

    .line 23
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    .line 24
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 25
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetBehavior;->b()V

    .line 26
    :cond_0
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    if-eqz v0, :cond_3

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    const/4 v0, 0x3

    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    .line 27
    :cond_1
    sget v0, Lao;->l:I

    .line 28
    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 30
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->s:Z

    .line 31
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 32
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->n:F

    .line 34
    return-void

    .line 14
    :cond_2
    sget v0, Lao;->k:I

    .line 15
    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 16
    invoke-direct {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->d(I)V

    goto :goto_0

    .line 26
    :cond_3
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    goto :goto_1
.end method

.method public static a(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 288
    instance-of v1, v0, Landroid/support/design/widget/CoordinatorLayout$d;

    if-nez v1, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The view is not a child of CoordinatorLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$d;

    .line 291
    iget-object v0, v0, Landroid/support/design/widget/CoordinatorLayout$d;->a:Landroid/support/design/widget/CoordinatorLayout$a;

    .line 293
    instance-of v1, v0, Landroid/support/design/widget/BottomSheetBehavior;

    if-nez v1, :cond_1

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The view is not associated with BottomSheetBehavior"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_1
    check-cast v0, Landroid/support/design/widget/BottomSheetBehavior;

    return-object v0
.end method

.method private b(Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    .line 258
    .line 259
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p1}, Lri;->v(Landroid/view/View;)Z

    move-result v0

    .line 260
    if-eqz v0, :cond_0

    .line 269
    :goto_0
    return-object p1

    .line 262
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 263
    check-cast p1, Landroid/view/ViewGroup;

    .line 264
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 265
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_1

    move-object p1, v0

    .line 267
    goto :goto_0

    .line 268
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 269
    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private final b()V
    .locals 2

    .prologue
    .line 243
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    if-eqz v0, :cond_0

    .line 244
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->r:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->r:I

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    goto :goto_0
.end method

.method private final c()V
    .locals 1

    .prologue
    .line 247
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->l:I

    .line 248
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    .line 251
    :cond_0
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 203
    .line 204
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    .line 205
    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->p:Z

    if-nez v2, :cond_3

    .line 206
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->p:Z

    .line 213
    :goto_0
    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 215
    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 217
    :cond_0
    return-void

    .line 208
    :cond_1
    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->p:Z

    if-nez v2, :cond_2

    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->o:I

    if-eq v2, p1, :cond_3

    .line 209
    :cond_2
    iput-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->p:Z

    .line 210
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->o:I

    .line 211
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    sub-int/2addr v1, p1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 218
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    if-ne p1, v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_3

    .line 221
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 222
    :cond_2
    iput p1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    goto :goto_0

    .line 224
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 225
    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 228
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 229
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->r(Landroid/view/View;)Z

    move-result v1

    .line 230
    if-eqz v1, :cond_4

    .line 232
    new-instance v1, Lcc;

    invoke-direct {v1, p0, v0, p1}, Lcc;-><init>(Landroid/support/design/widget/BottomSheetBehavior;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 234
    :cond_4
    invoke-virtual {p0, v0, p1}, Landroid/support/design/widget/BottomSheetBehavior;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 271
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 272
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    .line 280
    :goto_0
    iget-object v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1, p1, v2, v0}, Ltr;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 281
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    .line 282
    new-instance v0, Landroid/support/design/widget/BottomSheetBehavior$c;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/design/widget/BottomSheetBehavior$c;-><init>(Landroid/support/design/widget/BottomSheetBehavior;Landroid/view/View;I)V

    invoke-static {p1, v0}, Lqy;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 284
    :goto_1
    return-void

    .line 273
    :cond_0
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 274
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    goto :goto_0

    .line 275
    :cond_1
    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    .line 276
    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v0

    goto :goto_0

    .line 277
    :cond_2
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    if-ne p2, v0, :cond_3

    .line 278
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    goto :goto_0

    .line 279
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Illegal state argument: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_4
    invoke-virtual {p0, p2}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;F)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 252
    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->s:Z

    if-eqz v2, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v0

    .line 254
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    iget v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    if-ge v2, v3, :cond_2

    move v0, v1

    .line 255
    goto :goto_0

    .line 256
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    .line 257
    iget v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->o:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    if-ne v0, p1, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iput p1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 239
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 240
    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->k:Landroid/support/design/widget/BottomSheetBehavior$a;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->k:Landroid/support/design/widget/BottomSheetBehavior$a;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/BottomSheetBehavior$a;->a(I)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    .line 286
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    invoke-virtual {p2}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    iput-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    .line 109
    :cond_0
    :goto_0
    return v2

    .line 81
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 82
    if-nez v3, :cond_2

    .line 83
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetBehavior;->c()V

    .line 84
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    if-nez v0, :cond_3

    .line 85
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    .line 86
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 87
    packed-switch v3, :pswitch_data_0

    .line 101
    :cond_4
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    invoke-virtual {v0, p3}, Ltr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v2, v1

    .line 102
    goto :goto_0

    .line 88
    :pswitch_1
    iput-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->m:Z

    .line 89
    iput v6, p0, Landroid/support/design/widget/BottomSheetBehavior;->l:I

    .line 90
    iget-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    if-eqz v0, :cond_4

    .line 91
    iput-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    goto :goto_0

    .line 93
    :pswitch_2
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 94
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->x:I

    .line 95
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 96
    :goto_2
    if-eqz v0, :cond_5

    iget v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->x:I

    invoke-virtual {p1, v0, v4, v5}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 97
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->l:I

    .line 98
    iput-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->m:Z

    .line 99
    :cond_5
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->l:I

    if-ne v0, v6, :cond_7

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->x:I

    .line 100
    invoke-virtual {p1, p2, v4, v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    goto :goto_1

    .line 95
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    move v0, v2

    .line 100
    goto :goto_3

    .line 103
    :cond_8
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 104
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    if-nez v3, :cond_0

    iget v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    if-eq v3, v1, :cond_0

    .line 105
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1, v0, v3, v4}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->x:I

    int-to-float v0, v0

    .line 106
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    .line 107
    iget v3, v3, Ltr;->a:I

    .line 108
    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto/16 :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 44
    .line 45
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p1}, Lri;->h(Landroid/view/View;)Z

    move-result v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p2}, Lri;->h(Landroid/view/View;)Z

    move-result v0

    .line 48
    if-nez v0, :cond_0

    .line 49
    invoke-static {p2, v4}, Lqy;->b(Landroid/view/View;Z)V

    .line 50
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 51
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 52
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    .line 53
    iget-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->p:Z

    if-eqz v1, :cond_4

    .line 54
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->q:I

    if-nez v1, :cond_1

    .line 56
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00eb

    .line 57
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->q:I

    .line 58
    :cond_1
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->q:I

    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v3

    mul-int/lit8 v3, v3, 0x9

    div-int/lit8 v3, v3, 0x10

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->r:I

    .line 60
    :goto_0
    const/4 v1, 0x0

    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    .line 61
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    .line 62
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetBehavior;->b()V

    .line 63
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 64
    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v0

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 73
    :cond_2
    :goto_1
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    if-nez v0, :cond_3

    .line 74
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->y:Ltu;

    invoke-static {p1, v0}, Ltr;->a(Landroid/view/ViewGroup;Ltu;)Ltr;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    .line 75
    :cond_3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->i:Ljava/lang/ref/WeakReference;

    .line 76
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, p2}, Landroid/support/design/widget/BottomSheetBehavior;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    .line 77
    return v4

    .line 59
    :cond_4
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->o:I

    iput v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->r:I

    goto :goto_0

    .line 65
    :cond_5
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_6

    .line 66
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    goto :goto_1

    .line 67
    :cond_6
    iget-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    if-eqz v1, :cond_7

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_7

    .line 68
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    goto :goto_1

    .line 69
    :cond_7
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_8

    .line 70
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    goto :goto_1

    .line 71
    :cond_8
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    if-eq v1, v4, :cond_9

    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 72
    :cond_9
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    goto :goto_1
.end method

.method public onNestedPreFling(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;FF)Z
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p3, v0, :cond_1

    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 201
    invoke-super/range {p0 .. p5}, Landroid/support/design/widget/CoordinatorLayout$a;->onNestedPreFling(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    .line 201
    :cond_1
    const/4 v0, 0x0

    .line 202
    goto :goto_0
.end method

.method public onNestedPreScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 131
    iget-object v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 132
    if-eq p3, v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 135
    sub-int v1, v0, p5

    .line 136
    if-lez p5, :cond_3

    .line 137
    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 138
    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v1

    sub-int/2addr v0, v1

    aput v0, p6, v3

    .line 139
    aget v0, p6, v3

    neg-int v0, v0

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 140
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    .line 153
    :cond_1
    :goto_1
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->c(I)V

    .line 154
    iput p5, p0, Landroid/support/design/widget/BottomSheetBehavior;->u:I

    .line 155
    iput-boolean v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->v:Z

    goto :goto_0

    .line 141
    :cond_2
    aput p5, p6, v3

    .line 142
    neg-int v0, p5

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 143
    invoke-virtual {p0, v3}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    goto :goto_1

    .line 144
    :cond_3
    if-gez p5, :cond_1

    .line 145
    const/4 v2, -0x1

    invoke-static {p3, v2}, Lqy;->a(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 146
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    if-le v1, v2, :cond_4

    iget-boolean v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    if-eqz v1, :cond_5

    .line 147
    :cond_4
    aput p5, p6, v3

    .line 148
    neg-int v0, p5

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 149
    invoke-virtual {p0, v3}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    goto :goto_1

    .line 150
    :cond_5
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    sub-int/2addr v0, v1

    aput v0, p6, v3

    .line 151
    aget v0, p6, v3

    neg-int v0, v0

    invoke-static {p2, v0}, Lqy;->d(Landroid/view/View;I)V

    .line 152
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 36
    check-cast p3, Landroid/support/design/widget/BottomSheetBehavior$b;

    .line 38
    iget-object v0, p3, Lpy;->e:Landroid/os/Parcelable;

    .line 39
    invoke-super {p0, p1, p2, v0}, Landroid/support/design/widget/CoordinatorLayout$a;->onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 40
    iget v0, p3, Landroid/support/design/widget/BottomSheetBehavior$b;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p3, Landroid/support/design/widget/BottomSheetBehavior$b;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 41
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_1
    iget v0, p3, Landroid/support/design/widget/BottomSheetBehavior$b;->a:I

    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Landroid/support/design/widget/BottomSheetBehavior$b;

    invoke-super {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$a;->onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v1

    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    invoke-direct {v0, v1, v2}, Landroid/support/design/widget/BottomSheetBehavior$b;-><init>(Landroid/os/Parcelable;I)V

    return-object v0
.end method

.method public onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 128
    iput v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->u:I

    .line 129
    iput-boolean v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->v:Z

    .line 130
    and-int/lit8 v1, p5, 0x2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onStopNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v1, 0x4

    const/4 v0, 0x3

    .line 157
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 158
    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne p3, v4, :cond_0

    iget-boolean v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->v:Z

    if-eqz v4, :cond_0

    .line 162
    iget v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->u:I

    if-lez v4, :cond_2

    .line 163
    invoke-virtual {p0}, Landroid/support/design/widget/BottomSheetBehavior;->a()I

    move-result v1

    .line 194
    :goto_1
    iget-object v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v2, p2, v4, v1}, Ltr;->a(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 195
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    .line 196
    new-instance v1, Landroid/support/design/widget/BottomSheetBehavior$c;

    invoke-direct {v1, p0, p2, v0}, Landroid/support/design/widget/BottomSheetBehavior$c;-><init>(Landroid/support/design/widget/BottomSheetBehavior;Landroid/view/View;I)V

    invoke-static {p2, v1}, Lqy;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 198
    :goto_2
    iput-boolean v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->v:Z

    goto :goto_0

    .line 165
    :cond_2
    iget-boolean v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    if-eqz v4, :cond_3

    .line 166
    iget-object v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    const/16 v5, 0x3e8

    iget v6, p0, Landroid/support/design/widget/BottomSheetBehavior;->n:F

    invoke-virtual {v4, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 167
    iget-object v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    iget v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->l:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    .line 168
    invoke-virtual {p0, p2, v4}, Landroid/support/design/widget/BottomSheetBehavior;->a(Landroid/view/View;F)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 169
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->h:I

    .line 170
    const/4 v0, 0x5

    goto :goto_1

    .line 171
    :cond_3
    iget v4, p0, Landroid/support/design/widget/BottomSheetBehavior;->u:I

    if-nez v4, :cond_9

    .line 172
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 173
    iget-boolean v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->a:Z

    if-eqz v5, :cond_5

    .line 174
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    sub-int v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 175
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->b:I

    goto :goto_1

    .line 177
    :cond_4
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    move v7, v1

    move v1, v0

    move v0, v7

    .line 178
    goto :goto_1

    .line 179
    :cond_5
    iget v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    if-ge v4, v5, :cond_7

    .line 180
    iget v1, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    sub-int v1, v4, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v4, v1, :cond_6

    move v1, v3

    .line 182
    goto :goto_1

    .line 183
    :cond_6
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    move v1, v0

    move v0, v2

    .line 184
    goto :goto_1

    .line 185
    :cond_7
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    sub-int v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v5, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    sub-int/2addr v4, v5

    .line 186
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v0, v4, :cond_8

    .line 187
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->c:I

    move v1, v0

    move v0, v2

    .line 188
    goto/16 :goto_1

    .line 189
    :cond_8
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    move v7, v1

    move v1, v0

    move v0, v7

    .line 191
    goto/16 :goto_1

    .line 192
    :cond_9
    iget v0, p0, Landroid/support/design/widget/BottomSheetBehavior;->d:I

    move v7, v1

    move v1, v0

    move v0, v7

    .line 193
    goto/16 :goto_1

    .line 197
    :cond_a
    invoke-virtual {p0, v0}, Landroid/support/design/widget/BottomSheetBehavior;->b(I)V

    goto/16 :goto_2
.end method

.method public onTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 110
    invoke-virtual {p2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 113
    iget v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    if-ne v3, v1, :cond_2

    if-nez v2, :cond_2

    move v0, v1

    .line 114
    goto :goto_0

    .line 115
    :cond_2
    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    if-eqz v3, :cond_3

    .line 116
    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    invoke-virtual {v3, p3}, Ltr;->b(Landroid/view/MotionEvent;)V

    .line 117
    :cond_3
    if-nez v2, :cond_4

    .line 118
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetBehavior;->c()V

    .line 119
    :cond_4
    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    if-nez v3, :cond_5

    .line 120
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    .line 121
    :cond_5
    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->w:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p3}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 122
    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    if-nez v2, :cond_6

    .line 123
    iget v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->x:I

    int-to-float v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    .line 124
    iget v3, v3, Ltr;->a:I

    .line 125
    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 126
    iget-object v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->g:Ltr;

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    invoke-virtual {p3, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    invoke-virtual {v2, p2, v3}, Ltr;->a(Landroid/view/View;I)V

    .line 127
    :cond_6
    iget-boolean v2, p0, Landroid/support/design/widget/BottomSheetBehavior;->t:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
