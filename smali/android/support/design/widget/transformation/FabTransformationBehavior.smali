.class public abstract Landroid/support/design/widget/transformation/FabTransformationBehavior;
.super Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/widget/transformation/FabTransformationBehavior$a;
    }
.end annotation


# instance fields
.field private c:Landroid/graphics/Rect;

.field private d:Landroid/graphics/RectF;

.field private e:Landroid/graphics/RectF;

.field private f:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->c:Landroid/graphics/Rect;

    .line 3
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 4
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 5
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->f:[I

    .line 6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 8
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->c:Landroid/graphics/Rect;

    .line 9
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 10
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->f:[I

    .line 12
    return-void
.end method

.method private static a(Landroid/support/design/widget/transformation/FabTransformationBehavior$a;Law;FF)F
    .locals 8

    .prologue
    .line 265
    .line 266
    iget-wide v0, p1, Law;->a:J

    .line 269
    iget-wide v2, p1, Law;->b:J

    .line 271
    iget-object v4, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v5, "expansion"

    invoke-virtual {v4, v5}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v4

    .line 273
    iget-wide v6, v4, Law;->a:J

    .line 275
    iget-wide v4, v4, Law;->b:J

    .line 276
    add-long/2addr v4, v6

    .line 277
    const-wide/16 v6, 0x11

    add-long/2addr v4, v6

    .line 278
    sub-long v0, v4, v0

    long-to-float v0, v0

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 279
    invoke-virtual {p1}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    .line 280
    invoke-static {p2, p3, v0}, Lap;->a(FFF)F

    move-result v0

    return v0
.end method

.method private final a(Landroid/view/View;Landroid/view/View;Lgh;)F
    .locals 4

    .prologue
    .line 232
    iget-object v1, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 233
    iget-object v2, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 234
    invoke-direct {p0, p1, v1}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 235
    invoke-direct {p0, p2, v2}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 236
    const/4 v0, 0x0

    .line 237
    iget v3, p3, Lgh;->a:I

    and-int/lit8 v3, v3, 0x7

    packed-switch v3, :pswitch_data_0

    .line 243
    :goto_0
    :pswitch_0
    iget v1, p3, Lgh;->b:F

    add-float/2addr v0, v1

    .line 244
    return v0

    .line 238
    :pswitch_1
    iget v0, v2, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    .line 239
    goto :goto_0

    .line 240
    :pswitch_2
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 241
    goto :goto_0

    .line 242
    :pswitch_3
    iget v0, v2, Landroid/graphics/RectF;->right:F

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    goto :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 281
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 282
    check-cast p0, Landroid/view/ViewGroup;

    .line 283
    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;JIIFLjava/util/List;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 284
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 285
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 287
    invoke-static {p0, p3, p4, p5, p5}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 288
    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 289
    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 290
    invoke-interface {p6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    :cond_0
    return-void
.end method

.method private final a(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 258
    .line 259
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 260
    iget-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->f:[I

    .line 261
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 262
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p2, v1, v0}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 263
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    neg-float v1, v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 264
    return-void
.end method

.method private final b(Landroid/view/View;Landroid/view/View;Lgh;)F
    .locals 4

    .prologue
    .line 245
    iget-object v1, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 246
    iget-object v2, p0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 247
    invoke-direct {p0, p1, v1}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 248
    invoke-direct {p0, p2, v2}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 249
    const/4 v0, 0x0

    .line 250
    iget v3, p3, Lgh;->a:I

    and-int/lit8 v3, v3, 0x70

    sparse-switch v3, :sswitch_data_0

    .line 256
    :goto_0
    iget v1, p3, Lgh;->c:F

    add-float/2addr v0, v1

    .line 257
    return v0

    .line 251
    :sswitch_0
    iget v0, v2, Landroid/graphics/RectF;->top:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    .line 252
    goto :goto_0

    .line 253
    :sswitch_1
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    sub-float/2addr v0, v1

    .line 254
    goto :goto_0

    .line 255
    :sswitch_2
    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    goto :goto_0

    .line 250
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;Z)Landroid/support/design/widget/transformation/FabTransformationBehavior$a;
.end method

.method protected final b(Landroid/view/View;Landroid/view/View;ZZ)Landroid/animation/AnimatorSet;
    .locals 22

    .prologue
    .line 26
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/content/Context;Z)Landroid/support/design/widget/transformation/FabTransformationBehavior$a;

    move-result-object v14

    .line 27
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 28
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 29
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_1

    .line 32
    sget-object v4, Lqy;->a:Lri;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lri;->t(Landroid/view/View;)F

    move-result v4

    .line 34
    sget-object v5, Lqy;->a:Lri;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lri;->t(Landroid/view/View;)F

    move-result v5

    .line 35
    sub-float/2addr v4, v5

    .line 36
    if-eqz p3, :cond_c

    .line 37
    if-nez p4, :cond_0

    .line 38
    neg-float v4, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationZ(F)V

    .line 39
    :cond_0
    sget-object v4, Landroid/view/View;->TRANSLATION_Z:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 41
    :goto_0
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "elevation"

    invoke-virtual {v5, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 42
    invoke-virtual {v5, v4}, Law;->a(Landroid/animation/Animator;)V

    .line 43
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 46
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->b:Lgh;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/view/View;Lgh;)F

    move-result v9

    .line 47
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->b:Lgh;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->b(Landroid/view/View;Landroid/view/View;Lgh;)F

    move-result v10

    .line 48
    const/4 v4, 0x0

    cmpl-float v4, v9, v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    cmpl-float v4, v10, v4

    if-nez v4, :cond_d

    .line 49
    :cond_2
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v5, "translationXLinear"

    invoke-virtual {v4, v5}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 50
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "translationYLinear"

    invoke-virtual {v4, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v4

    move-object v6, v4

    move-object v7, v5

    .line 56
    :goto_1
    if-eqz p3, :cond_11

    .line 57
    if-nez p4, :cond_3

    .line 58
    neg-float v4, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationX(F)V

    .line 59
    neg-float v4, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 60
    :cond_3
    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v5, v12

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 61
    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v12, 0x1

    new-array v12, v12, [F

    const/4 v13, 0x0

    const/16 v16, 0x0

    aput v16, v12, v13

    move-object/from16 v0, p2

    invoke-static {v0, v4, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 62
    neg-float v9, v9

    neg-float v10, v10

    .line 63
    const/4 v12, 0x0

    .line 64
    invoke-static {v14, v7, v9, v12}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/support/design/widget/transformation/FabTransformationBehavior$a;Law;FF)F

    move-result v9

    .line 65
    const/4 v12, 0x0

    .line 66
    invoke-static {v14, v6, v10, v12}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/support/design/widget/transformation/FabTransformationBehavior$a;Law;FF)F

    move-result v10

    .line 67
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->c:Landroid/graphics/Rect;

    .line 68
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 69
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 70
    invoke-virtual {v13, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 72
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v12}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 73
    invoke-virtual {v12, v9, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 74
    invoke-virtual {v12, v13}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    .line 75
    invoke-virtual {v8, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 79
    :goto_2
    invoke-virtual {v7, v5}, Law;->a(Landroid/animation/Animator;)V

    .line 80
    invoke-virtual {v6, v4}, Law;->a(Landroid/animation/Animator;)V

    .line 81
    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 84
    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 86
    move-object/from16 v0, p2

    instance-of v4, v0, Lba;

    if-eqz v4, :cond_4

    move-object/from16 v0, p1

    instance-of v4, v0, Landroid/widget/ImageView;

    if-nez v4, :cond_12

    .line 104
    :cond_4
    :goto_3
    move-object/from16 v0, p2

    instance-of v4, v0, Lba;

    if-eqz v4, :cond_6

    move-object/from16 v4, p2

    .line 105
    check-cast v4, Lba;

    .line 106
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->b:Lgh;

    .line 107
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 108
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 109
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 110
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v9}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/view/View;Lgh;)F

    move-result v5

    .line 112
    neg-float v5, v5

    const/4 v10, 0x0

    invoke-virtual {v9, v5, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 113
    invoke-virtual {v6}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    iget v6, v9, Landroid/graphics/RectF;->left:F

    sub-float v16, v5, v6

    .line 115
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->b:Lgh;

    .line 116
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->d:Landroid/graphics/RectF;

    .line 117
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->e:Landroid/graphics/RectF;

    .line 118
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 119
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v9}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 120
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->b(Landroid/view/View;Landroid/view/View;Lgh;)F

    move-result v5

    .line 121
    const/4 v10, 0x0

    neg-float v5, v5

    invoke-virtual {v9, v10, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 122
    invoke-virtual {v6}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    iget v6, v9, Landroid/graphics/RectF;->top:F

    sub-float v17, v5, v6

    move-object/from16 v5, p1

    .line 124
    check-cast v5, Landroid/support/design/widget/FloatingActionButton;

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->c:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/graphics/Rect;)Z

    .line 125
    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/design/widget/transformation/FabTransformationBehavior;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v12, v5, v6

    .line 126
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "expansion"

    invoke-virtual {v5, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v18

    .line 127
    if-eqz p3, :cond_19

    .line 128
    if-nez p4, :cond_5

    .line 129
    new-instance v5, Lba$a;

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v5, v0, v1, v12}, Lba$a;-><init>(FFF)V

    invoke-interface {v4}, Lba;->d()V

    .line 130
    :cond_5
    if-eqz p4, :cond_15

    invoke-interface {v4}, Lba;->c()Lba$a;

    move-result-object v5

    iget v10, v5, Lba$a;->c:F

    .line 133
    :goto_4
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 134
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v1, v5, v6}, Lbh;->a(FFFF)F

    move-result v5

    const/4 v6, 0x0

    .line 135
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v1, v7, v6}, Lbh;->a(FFFF)F

    move-result v6

    .line 136
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v1, v7, v8}, Lbh;->a(FFFF)F

    move-result v7

    const/4 v9, 0x0

    .line 137
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v1, v9, v8}, Lbh;->a(FFFF)F

    move-result v8

    .line 139
    cmpl-float v9, v5, v6

    if-lez v9, :cond_16

    cmpl-float v9, v5, v7

    if-lez v9, :cond_16

    cmpl-float v9, v5, v8

    if-lez v9, :cond_16

    .line 142
    :goto_5
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v4, v0, v1, v5}, Ljb;->a(Lba;FFF)Landroid/animation/Animator;

    move-result-object v12

    .line 143
    new-instance v5, Leh;

    invoke-direct {v5, v4}, Leh;-><init>(Lba;)V

    invoke-virtual {v12, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 146
    move-object/from16 v0, v18

    iget-wide v6, v0, Law;->a:J

    .line 147
    move/from16 v0, v16

    float-to-int v8, v0

    move/from16 v0, v17

    float-to-int v9, v0

    move-object/from16 v5, p2

    .line 148
    invoke-static/range {v5 .. v11}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;JIIFLjava/util/List;)V

    move-object v5, v12

    .line 174
    :goto_6
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Law;->a(Landroid/animation/Animator;)V

    .line 175
    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    invoke-static {v4}, Ljb;->a(Lba;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v4

    invoke-interface {v15, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_6
    move-object/from16 v0, p2

    instance-of v4, v0, Lba;

    if-eqz v4, :cond_8

    move-object/from16 v4, p2

    .line 179
    check-cast v4, Lba;

    .line 182
    sget-object v5, Lqy;->a:Lri;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lri;->x(Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 184
    if-eqz v5, :cond_1b

    .line 185
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getDrawableState()[I

    move-result-object v6

    invoke-virtual {v5}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v5

    .line 188
    :goto_7
    const v6, 0xffffff

    and-int/2addr v6, v5

    .line 189
    if-eqz p3, :cond_1c

    .line 190
    if-nez p4, :cond_7

    .line 191
    invoke-interface {v4}, Lba;->f()V

    .line 192
    :cond_7
    sget-object v5, Lbd;->a:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v6, v7, v8

    .line 193
    invoke-static {v4, v5, v7}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 197
    :goto_8
    sget-object v5, Laq;->a:Laq;

    .line 198
    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 199
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "color"

    invoke-virtual {v5, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 200
    invoke-virtual {v5, v4}, Law;->a(Landroid/animation/Animator;)V

    .line 201
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_8
    move-object/from16 v0, p2

    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_b

    .line 204
    move-object/from16 v0, p2

    instance-of v4, v0, Lba;

    if-eqz v4, :cond_9

    sget v4, Laz;->a:I

    if-eqz v4, :cond_b

    .line 206
    :cond_9
    const v4, 0x7f0e0032

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 207
    if-eqz v4, :cond_1d

    .line 208
    invoke-static {v4}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v4

    .line 214
    :goto_9
    if-eqz v4, :cond_b

    .line 215
    if-eqz p3, :cond_20

    .line 216
    if-nez p4, :cond_a

    .line 217
    sget-object v5, Lar;->a:Landroid/util/Property;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 218
    :cond_a
    sget-object v5, Lar;->a:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    .line 219
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 222
    :goto_a
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "contentFade"

    invoke-virtual {v5, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 223
    invoke-virtual {v5, v4}, Law;->a(Landroid/animation/Animator;)V

    .line 224
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    :cond_b
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 226
    invoke-static {v6, v11}, Lbi;->a(Landroid/animation/AnimatorSet;Ljava/util/List;)V

    .line 227
    new-instance v4, Lee;

    move/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v4, v0, v1, v2}, Lee;-><init>(ZLandroid/view/View;Landroid/view/View;)V

    invoke-virtual {v6, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 228
    const/4 v4, 0x0

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v7

    move v5, v4

    :goto_b
    if-ge v5, v7, :cond_21

    .line 229
    invoke-interface {v15, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v6, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 230
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_b

    .line 40
    :cond_c
    sget-object v5, Landroid/view/View;->TRANSLATION_Z:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    neg-float v4, v4

    aput v4, v6, v7

    move-object/from16 v0, p2

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    goto/16 :goto_0

    .line 51
    :cond_d
    if-eqz p3, :cond_e

    const/4 v4, 0x0

    cmpg-float v4, v10, v4

    if-ltz v4, :cond_f

    :cond_e
    if-nez p3, :cond_10

    const/4 v4, 0x0

    cmpl-float v4, v10, v4

    if-lez v4, :cond_10

    .line 52
    :cond_f
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v5, "translationXCurveUpwards"

    invoke-virtual {v4, v5}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 53
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "translationYCurveUpwards"

    invoke-virtual {v4, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v4

    move-object v6, v4

    move-object v7, v5

    goto/16 :goto_1

    .line 54
    :cond_10
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v5, "translationXCurveDownwards"

    invoke-virtual {v4, v5}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v5

    .line 55
    iget-object v4, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v6, "translationYCurveDownwards"

    invoke-virtual {v4, v6}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v4

    move-object v6, v4

    move-object v7, v5

    goto/16 :goto_1

    .line 77
    :cond_11
    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v12, 0x0

    neg-float v9, v9

    aput v9, v5, v12

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 78
    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v9, 0x1

    new-array v9, v9, [F

    const/4 v12, 0x0

    neg-float v10, v10

    aput v10, v9, v12

    move-object/from16 v0, p2

    invoke-static {v0, v4, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    goto/16 :goto_2

    :cond_12
    move-object/from16 v4, p2

    .line 88
    check-cast v4, Lba;

    move-object/from16 v5, p1

    .line 89
    check-cast v5, Landroid/widget/ImageView;

    .line 90
    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 91
    if-eqz v6, :cond_4

    .line 92
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 93
    if-eqz p3, :cond_14

    .line 94
    if-nez p4, :cond_13

    .line 95
    const/16 v5, 0xff

    invoke-virtual {v6, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 96
    :cond_13
    sget-object v5, Las;->a:Landroid/util/Property;

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v12, 0x0

    aput v12, v9, v10

    invoke-static {v6, v5, v9}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 98
    :goto_c
    new-instance v9, Lef;

    move-object/from16 v0, p2

    invoke-direct {v9, v0}, Lef;-><init>(Landroid/view/View;)V

    invoke-virtual {v5, v9}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 99
    iget-object v9, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    const-string v10, "iconFade"

    invoke-virtual {v9, v10}, Lav;->a(Ljava/lang/String;)Law;

    move-result-object v9

    .line 100
    invoke-virtual {v9, v5}, Law;->a(Landroid/animation/Animator;)V

    .line 101
    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    new-instance v5, Leg;

    invoke-direct {v5, v4, v6}, Leg;-><init>(Lba;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v15, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 97
    :cond_14
    sget-object v5, Las;->a:Landroid/util/Property;

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/16 v12, 0xff

    aput v12, v9, v10

    invoke-static {v6, v5, v9}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v5

    goto :goto_c

    :cond_15
    move v10, v12

    .line 130
    goto/16 :goto_4

    .line 139
    :cond_16
    cmpl-float v5, v6, v7

    if-lez v5, :cond_17

    cmpl-float v5, v6, v8

    if-lez v5, :cond_17

    move v5, v6

    goto/16 :goto_5

    :cond_17
    cmpl-float v5, v7, v8

    if-lez v5, :cond_18

    move v5, v7

    goto/16 :goto_5

    :cond_18
    move v5, v8

    goto/16 :goto_5

    .line 150
    :cond_19
    invoke-interface {v4}, Lba;->c()Lba$a;

    move-result-object v5

    iget v10, v5, Lba$a;->c:F

    .line 152
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v4, v0, v1, v12}, Ljb;->a(Lba;FFF)Landroid/animation/Animator;

    move-result-object v13

    .line 155
    move-object/from16 v0, v18

    iget-wide v6, v0, Law;->a:J

    .line 156
    move/from16 v0, v16

    float-to-int v8, v0

    move/from16 v0, v17

    float-to-int v9, v0

    move-object/from16 v5, p2

    .line 157
    invoke-static/range {v5 .. v11}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;JIIFLjava/util/List;)V

    .line 160
    move-object/from16 v0, v18

    iget-wide v6, v0, Law;->a:J

    .line 163
    move-object/from16 v0, v18

    iget-wide v8, v0, Law;->b:J

    .line 164
    iget-object v5, v14, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    .line 165
    invoke-virtual {v5}, Lav;->a()J

    move-result-wide v20

    move/from16 v0, v16

    float-to-int v5, v0

    move/from16 v0, v17

    float-to-int v10, v0

    .line 167
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v17, 0x15

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_1a

    .line 168
    add-long v16, v6, v8

    cmp-long v16, v16, v20

    if-gez v16, :cond_1a

    .line 170
    move-object/from16 v0, p2

    invoke-static {v0, v5, v10, v12, v12}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v5

    .line 171
    add-long v16, v6, v8

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 172
    add-long/2addr v6, v8

    sub-long v6, v20, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 173
    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1a
    move-object v5, v13

    goto/16 :goto_6

    .line 186
    :cond_1b
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 194
    :cond_1c
    sget-object v6, Lbd;->a:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v5, v7, v8

    .line 195
    invoke-static {v4, v6, v7}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v4

    goto/16 :goto_8

    .line 209
    :cond_1d
    move-object/from16 v0, p2

    instance-of v4, v0, Lek;

    if-nez v4, :cond_1e

    move-object/from16 v0, p2

    instance-of v4, v0, Lej;

    if-eqz v4, :cond_1f

    :cond_1e
    move-object/from16 v4, p2

    .line 210
    check-cast v4, Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 211
    invoke-static {v4}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v4

    goto/16 :goto_9

    .line 212
    :cond_1f
    invoke-static/range {p2 .. p2}, Landroid/support/design/widget/transformation/FabTransformationBehavior;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v4

    goto/16 :goto_9

    .line 220
    :cond_20
    sget-object v5, Lar;->a:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v6, v7

    .line 221
    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    goto/16 :goto_a

    .line 231
    :cond_21
    return-object v6
.end method

.method public layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 14
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This behavior cannot be attached to a GONE view. Set the view to INVISIBLE instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_0
    instance-of v1, p3, Landroid/support/design/widget/FloatingActionButton;

    if-eqz v1, :cond_2

    .line 16
    check-cast p3, Landroid/support/design/widget/FloatingActionButton;

    .line 18
    iget-object v1, p3, Landroid/support/design/widget/FloatingActionButton;->d:Leb;

    .line 19
    iget v1, v1, Leb;->c:I

    .line 21
    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 22
    :cond_2
    return v0
.end method

.method public onAttachedToLayoutParams(Landroid/support/design/widget/CoordinatorLayout$d;)V
    .locals 1

    .prologue
    .line 23
    iget v0, p1, Landroid/support/design/widget/CoordinatorLayout$d;->h:I

    if-nez v0, :cond_0

    .line 24
    const/16 v0, 0x50

    iput v0, p1, Landroid/support/design/widget/CoordinatorLayout$d;->h:I

    .line 25
    :cond_0
    return-void
.end method
