.class public Landroid/support/design/widget/transformation/FabTransformationSheetBehavior;
.super Landroid/support/design/widget/transformation/FabTransformationBehavior;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/design/widget/transformation/FabTransformationBehavior;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/transformation/FabTransformationBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Z)Landroid/support/design/widget/transformation/FabTransformationBehavior$a;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4
    if-eqz p2, :cond_0

    .line 5
    const v0, 0x7f06000b

    .line 7
    :goto_0
    new-instance v1, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;

    invoke-direct {v1}, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;-><init>()V

    .line 8
    invoke-static {p1, v0}, Lav;->a(Landroid/content/Context;I)Lav;

    move-result-object v0

    iput-object v0, v1, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->a:Lav;

    .line 9
    new-instance v0, Lgh;

    const/16 v2, 0x11

    invoke-direct {v0, v2, v3, v3}, Lgh;-><init>(IFF)V

    iput-object v0, v1, Landroid/support/design/widget/transformation/FabTransformationBehavior$a;->b:Lgh;

    .line 10
    return-object v1

    .line 6
    :cond_0
    const v0, 0x7f06000a

    goto :goto_0
.end method
