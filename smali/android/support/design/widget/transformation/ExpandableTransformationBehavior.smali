.class public abstract Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;
.super Landroid/support/design/widget/transformation/ExpandableBehavior;
.source "PG"


# instance fields
.field public b:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/design/widget/transformation/ExpandableBehavior;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/transformation/ExpandableBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/View;ZZ)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 4
    iget-object v0, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    move v0, v1

    .line 5
    :goto_0
    if-eqz v0, :cond_0

    .line 6
    iget-object v2, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    .line 8
    :cond_0
    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b(Landroid/view/View;Landroid/view/View;ZZ)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    .line 9
    iget-object v0, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    new-instance v2, Led;

    invoke-direct {v2, p0}, Led;-><init>(Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;)V

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 10
    iget-object v0, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 11
    if-nez p4, :cond_1

    .line 12
    iget-object v0, p0, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 13
    :cond_1
    return v1

    .line 4
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Landroid/view/View;Landroid/view/View;ZZ)Landroid/animation/AnimatorSet;
.end method
