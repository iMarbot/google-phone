.class public Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;
.super Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;
.source "PG"


# instance fields
.field private c:Law;

.field private d:Law;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x96

    .line 1
    invoke-direct {p0}, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;-><init>()V

    .line 2
    new-instance v0, Law;

    const-wide/16 v2, 0x4b

    invoke-direct {v0, v2, v3, v4, v5}, Law;-><init>(JJ)V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->c:Law;

    .line 3
    new-instance v0, Law;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Law;-><init>(JJ)V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->d:Law;

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x96

    .line 5
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/transformation/ExpandableTransformationBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 6
    new-instance v0, Law;

    const-wide/16 v2, 0x4b

    invoke-direct {v0, v2, v3, v4, v5}, Law;-><init>(JJ)V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->c:Law;

    .line 7
    new-instance v0, Law;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Law;-><init>(JJ)V

    iput-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->d:Law;

    .line 8
    return-void
.end method


# virtual methods
.method protected final b(Landroid/view/View;Landroid/view/View;ZZ)Landroid/animation/AnimatorSet;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 10
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    if-eqz p3, :cond_1

    iget-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->c:Law;

    move-object v1, v0

    .line 14
    :goto_0
    if-eqz p3, :cond_2

    .line 15
    if-nez p4, :cond_0

    .line 16
    invoke-virtual {p2, v4}, Landroid/view/View;->setAlpha(F)V

    .line 17
    :cond_0
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v3, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v5

    invoke-static {p2, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 19
    :goto_1
    invoke-virtual {v1, v0}, Law;->a(Landroid/animation/Animator;)V

    .line 20
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 22
    invoke-static {v0, v2}, Lbi;->a(Landroid/animation/AnimatorSet;Ljava/util/List;)V

    .line 23
    new-instance v1, Lei;

    invoke-direct {v1, p3, p2}, Lei;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 24
    return-object v0

    .line 13
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/transformation/FabTransformationScrimBehavior;->d:Law;

    move-object v1, v0

    goto :goto_0

    .line 18
    :cond_2
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v3, [F

    aput v4, v3, v5

    invoke-static {p2, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_1
.end method

.method public layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 9
    instance-of v0, p3, Landroid/support/design/widget/FloatingActionButton;

    return v0
.end method
