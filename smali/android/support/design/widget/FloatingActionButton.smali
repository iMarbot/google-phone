.class public Landroid/support/design/widget/FloatingActionButton;
.super Ldz;
.source "PG"

# interfaces
.implements Lea;
.implements Lqx;
.implements Ltq;


# annotations
.annotation runtime Landroid/support/design/widget/CoordinatorLayout$b;
    a = Landroid/support/design/widget/FloatingActionButton$Behavior;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/widget/FloatingActionButton$ShadowDelegateImpl;,
        Landroid/support/design/widget/FloatingActionButton$Behavior;,
        Landroid/support/design/widget/FloatingActionButton$Size;,
        Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public final c:Landroid/graphics/Rect;

.field public final d:Leb;

.field private f:Landroid/content/res/ColorStateList;

.field private g:Landroid/graphics/PorterDuff$Mode;

.field private h:Landroid/content/res/ColorStateList;

.field private i:Landroid/graphics/PorterDuff$Mode;

.field private j:I

.field private k:I

.field private l:Landroid/content/res/ColorStateList;

.field private m:I

.field private n:I

.field private o:Landroid/graphics/Rect;

.field private p:Lzk;

.field private q:Lcq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const v0, 0x7f01016a

    invoke-direct {p0, p1, p2, v0}, Landroid/support/design/widget/FloatingActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Ldz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    .line 7
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->o:Landroid/graphics/Rect;

    .line 8
    invoke-static {p1}, Ldu;->a(Landroid/content/Context;)V

    .line 9
    sget-object v0, Lao;->x:[I

    const v1, 0x7f1201f2

    .line 10
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 11
    sget v1, Lao;->A:I

    .line 12
    invoke-static {p1, v0, v1}, Lbi;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->f:Landroid/content/res/ColorStateList;

    .line 13
    sget v1, Lao;->B:I

    .line 14
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Lbw;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->g:Landroid/graphics/PorterDuff$Mode;

    .line 16
    sget v1, Lao;->K:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/FloatingActionButton;->k:I

    .line 17
    sget v1, Lao;->J:I

    .line 18
    invoke-static {p1, v0, v1}, Lbi;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->l:Landroid/content/res/ColorStateList;

    .line 19
    sget v1, Lao;->E:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/FloatingActionButton;->m:I

    .line 20
    sget v1, Lao;->C:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/FloatingActionButton;->j:I

    .line 21
    sget v1, Lao;->D:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    .line 22
    sget v1, Lao;->G:I

    .line 23
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v7

    .line 24
    sget v1, Lao;->I:I

    .line 25
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v8

    .line 26
    sget v1, Lao;->M:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/design/widget/FloatingActionButton;->b:Z

    .line 27
    sget v1, Lao;->H:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/FloatingActionButton;->n:I

    .line 28
    sget v1, Lao;->L:I

    .line 29
    invoke-static {p1, v0, v1}, Lav;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Lav;

    move-result-object v9

    .line 30
    sget v1, Lao;->F:I

    .line 31
    invoke-static {p1, v0, v1}, Lav;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Lav;

    move-result-object v10

    .line 32
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 33
    new-instance v0, Lzk;

    invoke-direct {v0, p0}, Lzk;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->p:Lzk;

    .line 34
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->p:Lzk;

    invoke-virtual {v0, p2, p3}, Lzk;->a(Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Leb;

    invoke-direct {v0, p0}, Leb;-><init>(Lea;)V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->d:Leb;

    .line 36
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->f:Landroid/content/res/ColorStateList;

    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton;->g:Landroid/graphics/PorterDuff$Mode;

    iget v3, p0, Landroid/support/design/widget/FloatingActionButton;->k:I

    iget-object v4, p0, Landroid/support/design/widget/FloatingActionButton;->l:Landroid/content/res/ColorStateList;

    iget v5, p0, Landroid/support/design/widget/FloatingActionButton;->j:I

    .line 37
    invoke-virtual/range {v0 .. v5}, Lcq;->a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;ILandroid/content/res/ColorStateList;I)V

    .line 38
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 39
    iget v1, v0, Lcq;->n:F

    cmpl-float v1, v1, v6

    if-eqz v1, :cond_0

    .line 40
    iput v6, v0, Lcq;->n:F

    .line 41
    iget v1, v0, Lcq;->n:F

    iget v2, v0, Lcq;->o:F

    iget v3, v0, Lcq;->p:F

    invoke-virtual {v0, v1, v2, v3}, Lcq;->a(FFF)V

    .line 42
    :cond_0
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 43
    iget v1, v0, Lcq;->o:F

    cmpl-float v1, v1, v7

    if-eqz v1, :cond_1

    .line 44
    iput v7, v0, Lcq;->o:F

    .line 45
    iget v1, v0, Lcq;->n:F

    iget v2, v0, Lcq;->o:F

    iget v3, v0, Lcq;->p:F

    invoke-virtual {v0, v1, v2, v3}, Lcq;->a(FFF)V

    .line 46
    :cond_1
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 47
    iget v1, v0, Lcq;->p:F

    cmpl-float v1, v1, v8

    if-eqz v1, :cond_2

    .line 48
    iput v8, v0, Lcq;->p:F

    .line 49
    iget v1, v0, Lcq;->n:F

    iget v2, v0, Lcq;->o:F

    iget v3, v0, Lcq;->p:F

    invoke-virtual {v0, v1, v2, v3}, Lcq;->a(FFF)V

    .line 50
    :cond_2
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    iget v1, p0, Landroid/support/design/widget/FloatingActionButton;->n:I

    .line 51
    iget v2, v0, Lcq;->q:I

    if-eq v2, v1, :cond_3

    .line 52
    iput v1, v0, Lcq;->q:I

    .line 53
    invoke-virtual {v0}, Lcq;->a()V

    .line 54
    :cond_3
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 55
    iput-object v9, v0, Lcq;->d:Lav;

    .line 56
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 57
    iput-object v10, v0, Lcq;->e:Lav;

    .line 58
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/FloatingActionButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 59
    return-void
.end method

.method private final a(I)I
    .locals 2

    .prologue
    .line 186
    :goto_0
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 187
    packed-switch p1, :pswitch_data_0

    .line 194
    :pswitch_0
    const v1, 0x7f0d00f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_1
    return v0

    .line 188
    :pswitch_1
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenWidthDp:I

    .line 189
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 190
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v1, 0x1d6

    if-ge v0, v1, :cond_0

    .line 191
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/design/widget/FloatingActionButton;->a(I)I

    move-result v0

    goto :goto_1

    .line 192
    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    .line 193
    :pswitch_2
    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1

    .line 187
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 253
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 254
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 255
    sparse-switch v1, :sswitch_data_0

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 258
    :sswitch_0
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    .line 263
    :goto_0
    :sswitch_1
    return p0

    :sswitch_2
    move p0, v0

    .line 261
    goto :goto_0

    .line 255
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private final a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;)Lcx;
    .locals 1

    .prologue
    .line 182
    if-nez p1, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/design/widget/FloatingActionButton$1;

    invoke-direct {v0, p0, p1}, Landroid/support/design/widget/FloatingActionButton$1;-><init>(Landroid/support/design/widget/FloatingActionButton;Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;)V

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/design/widget/FloatingActionButton;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 276
    invoke-super {p0, p1}, Ldz;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private final g()V
    .locals 4

    .prologue
    .line 100
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 101
    if-nez v1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->h:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_1

    .line 104
    invoke-static {v1}, Lbw;->f(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getDrawableState()[I

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    .line 107
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->i:Landroid/graphics/PorterDuff$Mode;

    .line 108
    if-nez v0, :cond_2

    .line 109
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 111
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 112
    invoke-static {v2, v0}, Lzd;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method private final h()Lcq;
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->q:Lcq;

    if-nez v0, :cond_0

    .line 270
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 271
    new-instance v0, Lda;

    new-instance v1, Landroid/support/design/widget/FloatingActionButton$ShadowDelegateImpl;

    invoke-direct {v1, p0}, Landroid/support/design/widget/FloatingActionButton$ShadowDelegateImpl;-><init>(Landroid/support/design/widget/FloatingActionButton;)V

    invoke-direct {v0, p0, v1}, Lda;-><init>(Ldz;Ldg;)V

    .line 273
    :goto_0
    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->q:Lcq;

    .line 274
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->q:Lcq;

    return-object v0

    .line 272
    :cond_1
    new-instance v0, Lcq;

    new-instance v1, Landroid/support/design/widget/FloatingActionButton$ShadowDelegateImpl;

    invoke-direct {v1, p0}, Landroid/support/design/widget/FloatingActionButton$ShadowDelegateImpl;-><init>(Landroid/support/design/widget/FloatingActionButton;)V

    invoke-direct {v0, p0, v1}, Lcq;-><init>(Ldz;Ldg;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 85
    return-void
.end method

.method public final a(Landroid/graphics/PorterDuff$Mode;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 88
    return-void
.end method

.method public final a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 122
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v2

    invoke-direct {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;)Lcx;

    move-result-object v3

    .line 124
    iget-object v4, v2, Lcq;->x:Ldz;

    invoke-virtual {v4}, Ldz;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_5

    .line 125
    iget v4, v2, Lcq;->b:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 127
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 128
    iget-object v0, v2, Lcq;->c:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, v2, Lcq;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 130
    :cond_1
    invoke-virtual {v2}, Lcq;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 131
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0}, Ldz;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v7}, Ldz;->setAlpha(F)V

    .line 133
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v7}, Ldz;->setScaleY(F)V

    .line 134
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v7}, Ldz;->setScaleX(F)V

    .line 135
    invoke-virtual {v2, v7}, Lcq;->a(F)V

    .line 137
    :cond_2
    iget-object v0, v2, Lcq;->d:Lav;

    if-eqz v0, :cond_6

    iget-object v0, v2, Lcq;->d:Lav;

    .line 143
    :goto_1
    invoke-virtual {v2, v0, v6, v6, v6}, Lcq;->a(Lav;FFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 144
    new-instance v1, Lcs;

    invoke-direct {v1, v2, p2, v3}, Lcs;-><init>(Lcq;ZLcx;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 145
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 154
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 125
    goto :goto_0

    .line 126
    :cond_5
    iget v4, v2, Lcq;->b:I

    if-ne v4, v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 138
    :cond_6
    iget-object v0, v2, Lcq;->f:Lav;

    if-nez v0, :cond_7

    .line 139
    iget-object v0, v2, Lcq;->x:Ldz;

    .line 140
    invoke-virtual {v0}, Ldz;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060004

    invoke-static {v0, v1}, Lav;->a(Landroid/content/Context;I)Lav;

    move-result-object v0

    iput-object v0, v2, Lcq;->f:Lav;

    .line 141
    :cond_7
    iget-object v0, v2, Lcq;->f:Lav;

    goto :goto_1

    .line 147
    :cond_8
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v1, p2}, Ldz;->a(IZ)V

    .line 148
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v6}, Ldz;->setAlpha(F)V

    .line 149
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v6}, Ldz;->setScaleY(F)V

    .line 150
    iget-object v0, v2, Lcq;->x:Ldz;

    invoke-virtual {v0, v6}, Ldz;->setScaleX(F)V

    .line 151
    invoke-virtual {v2, v6}, Lcq;->a(F)V

    .line 152
    if-eqz v3, :cond_3

    .line 153
    invoke-interface {v3}, Lcx;->onShown()V

    goto :goto_2
.end method

.method public final a(Landroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 243
    .line 244
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, p0}, Lri;->q(Landroid/view/View;)Z

    move-result v1

    .line 245
    if-eqz v1, :cond_0

    .line 246
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getHeight()I

    move-result v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 247
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 248
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 249
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 250
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 251
    const/4 v0, 0x1

    .line 252
    :cond_0
    return v0
.end method

.method public final b()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->h:Landroid/content/res/ColorStateList;

    if-eq v0, p1, :cond_0

    .line 91
    iput-object p1, p0, Landroid/support/design/widget/FloatingActionButton;->h:Landroid/content/res/ColorStateList;

    .line 92
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->g()V

    .line 93
    :cond_0
    return-void
.end method

.method public final b(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->i:Landroid/graphics/PorterDuff$Mode;

    if-eq v0, p1, :cond_0

    .line 96
    iput-object p1, p0, Landroid/support/design/widget/FloatingActionButton;->i:Landroid/graphics/PorterDuff$Mode;

    .line 97
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->g()V

    .line 98
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 155
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v2

    invoke-direct {p0, p1}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;)Lcx;

    move-result-object v3

    .line 157
    iget-object v4, v2, Lcq;->x:Ldz;

    invoke-virtual {v4}, Ldz;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    .line 158
    iget v4, v2, Lcq;->b:I

    if-ne v4, v0, :cond_3

    .line 160
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 161
    iget-object v0, v2, Lcq;->c:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, v2, Lcq;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 163
    :cond_1
    invoke-virtual {v2}, Lcq;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 165
    iget-object v0, v2, Lcq;->e:Lav;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcq;->e:Lav;

    .line 171
    :goto_1
    invoke-virtual {v2, v0, v6, v6, v6}, Lcq;->a(Lav;FFF)Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 172
    new-instance v1, Lcr;

    invoke-direct {v1, v2, p2, v3}, Lcr;-><init>(Lcq;ZLcx;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 173
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 178
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 158
    goto :goto_0

    .line 159
    :cond_4
    iget v4, v2, Lcq;->b:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    move v0, v1

    goto :goto_0

    .line 166
    :cond_5
    iget-object v0, v2, Lcq;->g:Lav;

    if-nez v0, :cond_6

    .line 167
    iget-object v0, v2, Lcq;->x:Ldz;

    .line 168
    invoke-virtual {v0}, Ldz;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060003

    invoke-static {v0, v1}, Lav;->a(Landroid/content/Context;I)Lav;

    move-result-object v0

    iput-object v0, v2, Lcq;->g:Lav;

    .line 169
    :cond_6
    iget-object v0, v2, Lcq;->g:Lav;

    goto :goto_1

    .line 175
    :cond_7
    iget-object v1, v2, Lcq;->x:Ldz;

    if-eqz p2, :cond_8

    const/16 v0, 0x8

    :goto_3
    invoke-virtual {v1, v0, p2}, Ldz;->a(IZ)V

    .line 176
    if-eqz v3, :cond_2

    .line 177
    invoke-interface {v3}, Lcx;->onHidden()V

    goto :goto_2

    .line 175
    :cond_8
    const/4 v0, 0x4

    goto :goto_3
.end method

.method public final c()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->h:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final c_()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getBackgroundTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->i:Landroid/graphics/PorterDuff$Mode;

    return-object v0
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0}, Ldz;->drawableStateChanged()V

    .line 210
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcq;->a([I)V

    .line 211
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->d:Leb;

    .line 180
    iget-boolean v0, v0, Leb;->b:Z

    .line 181
    return v0
.end method

.method final f()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Landroid/support/design/widget/FloatingActionButton;->m:I

    invoke-direct {p0, v0}, Landroid/support/design/widget/FloatingActionButton;->a(I)I

    move-result v0

    return v0
.end method

.method public getBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->f:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->g:Landroid/graphics/PorterDuff$Mode;

    return-object v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0}, Ldz;->jumpDrawablesToCurrentState()V

    .line 213
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    invoke-virtual {v0}, Lcq;->b()V

    .line 214
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 195
    invoke-super {p0}, Ldz;->onAttachedToWindow()V

    .line 196
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Lcq;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-nez v1, :cond_0

    .line 200
    new-instance v1, Lct;

    invoke-direct {v1, v0}, Lct;-><init>(Lcq;)V

    iput-object v1, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 201
    :cond_0
    iget-object v1, v0, Lcq;->x:Ldz;

    invoke-virtual {v1}, Ldz;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v0, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 202
    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 203
    invoke-super {p0}, Ldz;->onDetachedFromWindow()V

    .line 204
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 205
    iget-object v1, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, v0, Lcq;->x:Ldz;

    invoke-virtual {v1}, Ldz;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 207
    const/4 v1, 0x0

    iput-object v1, v0, Lcq;->z:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 208
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 60
    invoke-virtual {p0}, Landroid/support/design/widget/FloatingActionButton;->f()I

    move-result v0

    .line 61
    iget v1, p0, Landroid/support/design/widget/FloatingActionButton;->n:I

    sub-int v1, v0, v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Landroid/support/design/widget/FloatingActionButton;->a:I

    .line 62
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v1

    invoke-virtual {v1}, Lcq;->c()V

    .line 63
    invoke-static {v0, p1}, Landroid/support/design/widget/FloatingActionButton;->a(II)I

    move-result v1

    .line 64
    invoke-static {v0, p2}, Landroid/support/design/widget/FloatingActionButton;->a(II)I

    move-result v0

    .line 65
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 66
    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/design/widget/FloatingActionButton;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Landroid/support/design/widget/FloatingActionButton;->setMeasuredDimension(II)V

    .line 67
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 225
    instance-of v0, p1, Lbk;

    if-nez v0, :cond_1

    .line 226
    invoke-super {p0, p1}, Ldz;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    check-cast p1, Lbk;

    .line 230
    iget-object v0, p1, Lpy;->e:Landroid/os/Parcelable;

    .line 231
    invoke-super {p0, v0}, Ldz;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 232
    iget-object v1, p0, Landroid/support/design/widget/FloatingActionButton;->d:Leb;

    iget-object v0, p1, Lbk;->a:Lpv;

    const-string v2, "expandableWidgetHelper"

    .line 233
    invoke-virtual {v0, v2}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 235
    const-string v2, "expanded"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Leb;->b:Z

    .line 236
    const-string v2, "expandedComponentIdHint"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Leb;->c:I

    .line 237
    iget-boolean v0, v1, Leb;->b:Z

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, v1, Leb;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 240
    instance-of v2, v0, Landroid/support/design/widget/CoordinatorLayout;

    if-eqz v2, :cond_0

    .line 241
    check-cast v0, Landroid/support/design/widget/CoordinatorLayout;

    iget-object v1, v1, Leb;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 7

    .prologue
    .line 215
    invoke-super {p0}, Ldz;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 216
    new-instance v1, Lbk;

    invoke-direct {v1, v0}, Lbk;-><init>(Landroid/os/Parcelable;)V

    .line 217
    iget-object v0, v1, Lbk;->a:Lpv;

    const-string v2, "expandableWidgetHelper"

    iget-object v3, p0, Landroid/support/design/widget/FloatingActionButton;->d:Leb;

    .line 219
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 220
    const-string v5, "expanded"

    iget-boolean v6, v3, Leb;->b:Z

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    const-string v5, "expandedComponentIdHint"

    iget v3, v3, Leb;->c:I

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    invoke-virtual {v0, v2, v4}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 265
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->o:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->o:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    const/4 v0, 0x0

    .line 267
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ldz;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public setBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->f:Landroid/content/res/ColorStateList;

    if-eq v0, p1, :cond_1

    .line 70
    iput-object p1, p0, Landroid/support/design/widget/FloatingActionButton;->f:Landroid/content/res/ColorStateList;

    .line 71
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 72
    iget-object v1, v0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, v0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 74
    :cond_0
    iget-object v1, v0, Lcq;->l:Lcl;

    if-eqz v1, :cond_1

    .line 75
    iget-object v0, v0, Lcq;->l:Lcl;

    invoke-virtual {v0, p1}, Lcl;->a(Landroid/content/res/ColorStateList;)V

    .line 76
    :cond_1
    return-void
.end method

.method public setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->g:Landroid/graphics/PorterDuff$Mode;

    if-eq v0, p1, :cond_0

    .line 79
    iput-object p1, p0, Landroid/support/design/widget/FloatingActionButton;->g:Landroid/graphics/PorterDuff$Mode;

    .line 80
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    .line 81
    iget-object v1, v0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 82
    iget-object v0, v0, Lcq;->j:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 83
    :cond_0
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Ldz;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    invoke-direct {p0}, Landroid/support/design/widget/FloatingActionButton;->h()Lcq;

    move-result-object v0

    invoke-virtual {v0}, Lcq;->a()V

    .line 121
    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton;->p:Lzk;

    invoke-virtual {v0, p1}, Lzk;->a(I)V

    .line 118
    return-void
.end method

.method public bridge synthetic setVisibility(I)V
    .locals 0

    .prologue
    .line 275
    invoke-super {p0, p1}, Ldz;->setVisibility(I)V

    return-void
.end method
