.class public Landroid/support/design/widget/AppBarLayout$Behavior;
.super Ldc;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/design/widget/AppBarLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Behavior"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/widget/AppBarLayout$Behavior$b;,
        Landroid/support/design/widget/AppBarLayout$Behavior$a;
    }
.end annotation


# instance fields
.field public a:I

.field private c:Landroid/animation/ValueAnimator;

.field private d:I

.field private e:Z

.field private f:F

.field private g:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ldc;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    .line 3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ldc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    .line 6
    return-void
.end method

.method private final a(ILandroid/support/design/widget/AppBarLayout;Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7
    if-ne p4, v2, :cond_2

    .line 8
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v0

    .line 9
    if-gez p1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    if-lez p1, :cond_2

    .line 10
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->d()I

    move-result v1

    neg-int v1, v1

    if-ne v0, v1, :cond_2

    .line 11
    :cond_1
    invoke-static {p3, v2}, Lqy;->c(Landroid/view/View;I)V

    .line 12
    :cond_2
    return-void
.end method

.method private final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v4

    .line 36
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v1

    move v0, v3

    :goto_0
    if-ge v0, v1, :cond_4

    .line 37
    invoke-virtual {p2, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 38
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    neg-int v6, v4

    if-gt v5, v6, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    neg-int v5, v4

    if-lt v2, v5, :cond_3

    move v1, v0

    .line 43
    :goto_1
    if-ltz v1, :cond_2

    .line 44
    invoke-virtual {p2, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 45
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout$a;

    .line 47
    iget v6, v0, Landroid/support/design/widget/AppBarLayout$a;->a:I

    .line 49
    and-int/lit8 v0, v6, 0x11

    const/16 v2, 0x11

    if-ne v0, v2, :cond_2

    .line 50
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v2, v0

    .line 51
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    neg-int v0, v0

    .line 52
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne v1, v7, :cond_0

    .line 53
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_0
    const/4 v1, 0x2

    invoke-static {v6, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 56
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v5}, Lri;->f(Landroid/view/View;)I

    move-result v1

    .line 57
    add-int/2addr v0, v1

    move v1, v2

    .line 65
    :cond_1
    :goto_2
    add-int v2, v0, v1

    div-int/lit8 v2, v2, 0x2

    if-ge v4, v2, :cond_6

    .line 67
    :goto_3
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v1

    neg-int v1, v1

    invoke-static {v0, v1, v3}, Lbw;->a(III)I

    move-result v0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)V

    .line 69
    :cond_2
    return-void

    .line 40
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_4
    const/4 v0, -0x1

    move v1, v0

    goto :goto_1

    .line 58
    :cond_5
    const/4 v1, 0x5

    invoke-static {v6, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(II)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 60
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v5}, Lri;->f(Landroid/view/View;)I

    move-result v1

    .line 61
    add-int/2addr v1, v0

    .line 62
    if-lt v4, v1, :cond_1

    move v0, v1

    move v1, v2

    .line 64
    goto :goto_2

    :cond_6
    move v0, v1

    .line 65
    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_2
.end method

.method private final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 13
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v0

    sub-int/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 14
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 15
    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    .line 16
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v0, v0

    div-float/2addr v0, v1

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    .line 20
    :goto_0
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v1

    .line 21
    if-ne v1, p3, :cond_2

    .line 22
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 33
    :cond_0
    :goto_1
    return-void

    .line 17
    :cond_1
    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 18
    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x43160000    # 150.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 25
    :cond_2
    iget-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    if-nez v2, :cond_3

    .line 26
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    .line 27
    iget-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    sget-object v3, Lap;->d:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 28
    iget-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    new-instance v3, Lbm;

    invoke-direct {v3, p0, p1, p2}, Lbm;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 30
    :goto_2
    iget-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    const/16 v3, 0x258

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 31
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v1, 0x1

    aput p3, v2, v1

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 32
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    .line 29
    :cond_3
    iget-object v2, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_2
.end method

.method private final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IIZ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    .line 72
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 73
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_5

    .line 74
    invoke-virtual {p2, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    if-lt v4, v6, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    if-gt v4, v6, :cond_4

    move-object v3, v0

    .line 80
    :goto_1
    if-eqz v3, :cond_3

    .line 81
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout$a;

    .line 83
    iget v0, v0, Landroid/support/design/widget/AppBarLayout$a;->a:I

    .line 86
    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_b

    .line 88
    sget-object v4, Lqy;->a:Lri;

    invoke-virtual {v4, v3}, Lri;->f(Landroid/view/View;)I

    move-result v4

    .line 90
    if-lez p4, :cond_7

    and-int/lit8 v5, v0, 0xc

    if-eqz v5, :cond_7

    .line 91
    neg-int v0, p3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    sub-int/2addr v3, v4

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v4

    sub-int/2addr v3, v4

    if-lt v0, v3, :cond_6

    move v0, v1

    .line 95
    :goto_2
    iget-boolean v3, p2, Landroid/support/design/widget/AppBarLayout;->d:Z

    if-eq v3, v0, :cond_9

    .line 96
    iput-boolean v0, p2, Landroid/support/design/widget/AppBarLayout;->d:Z

    .line 97
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->refreshDrawableState()V

    move v0, v1

    .line 101
    :goto_3
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_3

    if-nez p5, :cond_2

    if-eqz v0, :cond_3

    .line 104
    iget-object v0, p1, Landroid/support/design/widget/CoordinatorLayout;->a:Lcp;

    invoke-virtual {v0, p2}, Lcp;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 105
    iget-object v3, p1, Landroid/support/design/widget/CoordinatorLayout;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 106
    if-eqz v0, :cond_0

    .line 107
    iget-object v3, p1, Landroid/support/design/widget/CoordinatorLayout;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 108
    :cond_0
    iget-object v4, p1, Landroid/support/design/widget/CoordinatorLayout;->b:Ljava/util/List;

    .line 110
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_4
    if-ge v3, v5, :cond_1

    .line 111
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 113
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$d;

    .line 115
    iget-object v0, v0, Landroid/support/design/widget/CoordinatorLayout$d;->a:Landroid/support/design/widget/CoordinatorLayout$a;

    .line 117
    instance-of v6, v0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    if-eqz v6, :cond_a

    .line 118
    check-cast v0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;

    .line 119
    iget v0, v0, Lde;->c:I

    .line 120
    if-eqz v0, :cond_1

    move v2, v1

    .line 123
    :cond_1
    if-eqz v2, :cond_3

    .line 124
    :cond_2
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->jumpDrawablesToCurrentState()V

    .line 125
    :cond_3
    return-void

    .line 77
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 78
    :cond_5
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_1

    :cond_6
    move v0, v2

    .line 91
    goto :goto_2

    .line 92
    :cond_7
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b

    .line 93
    neg-int v0, p3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    sub-int/2addr v3, v4

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v4

    sub-int/2addr v3, v4

    if-lt v0, v3, :cond_8

    move v0, v1

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    move v0, v2

    .line 99
    goto :goto_3

    .line 121
    :cond_a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method private static a(II)Z
    .locals 1

    .prologue
    .line 70
    and-int v0, p0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()I
    .locals 2

    .prologue
    .line 126
    .line 127
    invoke-super {p0}, Ldc;->b()I

    move-result v0

    .line 128
    iget v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->a:I

    add-int/2addr v0, v1

    return v0
.end method

.method final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I
    .locals 10

    .prologue
    .line 147
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 148
    invoke-virtual {p0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v4

    .line 149
    const/4 v0, 0x0

    .line 150
    if-eqz p4, :cond_8

    if-lt v4, p4, :cond_8

    if-gt v4, p5, :cond_8

    .line 151
    invoke-static {p3, p4, p5}, Lbw;->a(III)I

    move-result v3

    .line 152
    if-eq v4, v3, :cond_3

    .line 154
    iget-boolean v0, v2, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 155
    if-eqz v0, :cond_6

    .line 157
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 158
    const/4 v0, 0x0

    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v6

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_5

    .line 159
    invoke-virtual {v2, v1}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 160
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout$a;

    .line 162
    iget-object v8, v0, Landroid/support/design/widget/AppBarLayout$a;->b:Landroid/view/animation/Interpolator;

    .line 164
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v9

    if-lt v5, v9, :cond_4

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v9

    if-gt v5, v9, :cond_4

    .line 165
    if-eqz v8, :cond_5

    .line 166
    const/4 v1, 0x0

    .line 168
    iget v6, v0, Landroid/support/design/widget/AppBarLayout$a;->a:I

    .line 170
    and-int/lit8 v9, v6, 0x1

    if-eqz v9, :cond_9

    .line 171
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v9, v0, Landroid/support/design/widget/AppBarLayout$a;->topMargin:I

    add-int/2addr v1, v9

    iget v0, v0, Landroid/support/design/widget/AppBarLayout$a;->bottomMargin:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 172
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_0

    .line 174
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v7}, Lri;->f(Landroid/view/View;)I

    move-result v1

    .line 175
    sub-int/2addr v0, v1

    .line 177
    :cond_0
    :goto_1
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v7}, Lri;->h(Landroid/view/View;)Z

    move-result v1

    .line 178
    if-eqz v1, :cond_1

    .line 179
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v1

    sub-int/2addr v0, v1

    .line 180
    :cond_1
    if-lez v0, :cond_5

    .line 181
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, v5, v1

    .line 182
    int-to-float v5, v0

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 183
    invoke-interface {v8, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v5

    .line 184
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v0, v5

    mul-int/2addr v0, v1

    .line 192
    :goto_2
    invoke-super {p0, v0}, Ldc;->a(I)Z

    move-result v1

    .line 194
    sub-int v6, v4, v3

    .line 195
    sub-int v0, v3, v0

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->a:I

    .line 196
    if-nez v1, :cond_2

    .line 197
    iget-boolean v0, v2, Landroid/support/design/widget/AppBarLayout;->a:Z

    .line 198
    if-eqz v0, :cond_2

    .line 199
    invoke-virtual {p1, v2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)V

    .line 201
    :cond_2
    invoke-super {p0}, Ldc;->b()I

    .line 204
    if-ge v3, v4, :cond_7

    const/4 v4, -0x1

    :goto_3
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    .line 205
    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IIZ)V

    move v0, v6

    .line 209
    :cond_3
    :goto_4
    return v0

    .line 187
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 189
    goto :goto_2

    :cond_6
    move v0, v3

    .line 190
    goto :goto_2

    .line 204
    :cond_7
    const/4 v4, 0x1

    goto :goto_3

    .line 207
    :cond_8
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->a:I

    goto :goto_4

    :cond_9
    move v0, v1

    goto :goto_1
.end method

.method final synthetic a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 129
    check-cast p1, Landroid/support/design/widget/AppBarLayout;

    .line 130
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v0

    .line 131
    return v0
.end method

.method final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 144
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 145
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    .line 146
    return-void
.end method

.method public final bridge synthetic a(I)Z
    .locals 1

    .prologue
    .line 211
    invoke-super {p0, p1}, Ldc;->a(I)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b()I
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Ldc;->b()I

    move-result v0

    return v0
.end method

.method final synthetic b(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 132
    check-cast p1, Landroid/support/design/widget/AppBarLayout;

    .line 133
    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->d()I

    move-result v0

    neg-int v0, v0

    .line 134
    return v0
.end method

.method final synthetic c(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 135
    check-cast p1, Landroid/support/design/widget/AppBarLayout;

    .line 136
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 138
    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, -0x1

    .line 140
    invoke-static {v0, v2}, Lqy;->a(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 143
    :goto_0
    return v0

    .line 140
    :cond_1
    const/4 v0, 0x0

    .line 141
    goto :goto_0
.end method

.method public synthetic onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 212
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 213
    invoke-super {p0, p1, v2, p3}, Ldc;->onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v6

    .line 215
    iget v1, v2, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 217
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    if-ltz v0, :cond_2

    and-int/lit8 v0, v1, 0x8

    if-nez v0, :cond_2

    .line 218
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    invoke-virtual {v2, v0}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    .line 220
    iget-boolean v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Z

    if-eqz v3, :cond_1

    .line 222
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v0}, Lri;->f(Landroid/view/View;)I

    move-result v0

    .line 223
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 225
    :goto_0
    invoke-virtual {p0, p1, v2, v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a_(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)I

    .line 240
    :cond_0
    :goto_1
    iput v4, v2, Landroid/support/design/widget/AppBarLayout;->b:I

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    .line 244
    invoke-super {p0}, Ldc;->b()I

    move-result v0

    .line 245
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v1

    neg-int v1, v1

    invoke-static {v0, v1, v4}, Lbw;->a(III)I

    move-result v0

    .line 247
    invoke-super {p0, v0}, Ldc;->a(I)Z

    .line 251
    invoke-super {p0}, Ldc;->b()I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    .line 253
    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IIZ)V

    .line 255
    invoke-super {p0}, Ldc;->b()I

    .line 258
    return v6

    .line 224
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0

    .line 226
    :cond_2
    if-eqz v1, :cond_0

    .line 227
    and-int/lit8 v0, v1, 0x4

    if-eqz v0, :cond_3

    move v0, v5

    .line 228
    :goto_2
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_5

    .line 230
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v1

    .line 231
    neg-int v1, v1

    .line 232
    if-eqz v0, :cond_4

    .line 233
    invoke-direct {p0, p1, v2, v1, v7}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)V

    goto :goto_1

    :cond_3
    move v0, v4

    .line 227
    goto :goto_2

    .line 234
    :cond_4
    invoke-virtual {p0, p1, v2, v1}, Landroid/support/design/widget/AppBarLayout$Behavior;->a_(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)I

    goto :goto_1

    .line 235
    :cond_5
    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 236
    if-eqz v0, :cond_6

    .line 237
    invoke-direct {p0, p1, v2, v4, v7}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)V

    goto :goto_1

    .line 238
    :cond_6
    invoke-virtual {p0, p1, v2, v4}, Landroid/support/design/widget/AppBarLayout$Behavior;->a_(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)I

    goto :goto_1
.end method

.method public synthetic onMeasureChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;IIII)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 326
    move-object v1, p2

    check-cast v1, Landroid/support/design/widget/AppBarLayout;

    .line 328
    invoke-virtual {v1}, Landroid/support/design/widget/AppBarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$d;

    .line 329
    iget v0, v0, Landroid/support/design/widget/CoordinatorLayout$d;->height:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_0

    .line 331
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move-object v0, p1

    move v2, p3

    move v3, p4

    move v5, p6

    .line 332
    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;IIII)V

    .line 333
    const/4 v0, 0x1

    .line 335
    :goto_0
    return v0

    :cond_0
    move-object v2, p0

    move-object v3, p1

    move-object v4, v1

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    .line 334
    invoke-super/range {v2 .. v8}, Ldc;->onMeasureChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;IIII)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic onNestedPreScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[II)V
    .locals 7

    .prologue
    .line 292
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 293
    if-eqz p5, :cond_0

    .line 294
    if-gez p5, :cond_1

    .line 295
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v0

    neg-int v4, v0

    .line 296
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->c()I

    move-result v0

    add-int v5, v4, v0

    .line 301
    :goto_0
    if-eq v4, v5, :cond_0

    .line 302
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I

    move-result v0

    aput v0, p6, v6

    .line 303
    invoke-direct {p0, p5, v2, p3, p7}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(ILandroid/support/design/widget/AppBarLayout;Landroid/view/View;I)V

    .line 304
    :cond_0
    return-void

    .line 298
    :cond_1
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v0

    .line 299
    neg-int v4, v0

    .line 300
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public synthetic onNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;IIIII)V
    .locals 6

    .prologue
    .line 305
    move-object v2, p2

    check-cast v2, Landroid/support/design/widget/AppBarLayout;

    .line 306
    if-gez p7, :cond_0

    .line 307
    invoke-virtual {v2}, Landroid/support/design/widget/AppBarLayout;->d()I

    move-result v0

    neg-int v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p7

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/AppBarLayout$Behavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I

    .line 308
    invoke-direct {p0, p7, v2, p3, p8}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(ILandroid/support/design/widget/AppBarLayout;Landroid/view/View;I)V

    .line 309
    :cond_0
    return-void
.end method

.method public synthetic onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 279
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 280
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$b;

    if-eqz v0, :cond_0

    .line 281
    check-cast p3, Landroid/support/design/widget/AppBarLayout$Behavior$b;

    .line 283
    iget-object v0, p3, Lpy;->e:Landroid/os/Parcelable;

    .line 284
    invoke-super {p0, p1, p2, v0}, Ldc;->onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 285
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$b;->a:I

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    .line 286
    iget v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$b;->b:F

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->f:F

    .line 287
    iget-boolean v0, p3, Landroid/support/design/widget/AppBarLayout$Behavior$b;->c:Z

    iput-boolean v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->e:Z

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    invoke-super {p0, p1, p2, p3}, Ldc;->onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->d:I

    goto :goto_0
.end method

.method public synthetic onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 259
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 260
    invoke-super {p0, p1, p2}, Ldc;->onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;

    move-result-object v2

    .line 262
    invoke-super {p0}, Ldc;->b()I

    move-result v4

    .line 264
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getChildCount()I

    move-result v5

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_2

    .line 265
    invoke-virtual {p2, v3}, Landroid/support/design/widget/AppBarLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 266
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int v7, v1, v4

    .line 267
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v4

    if-gtz v1, :cond_1

    if-ltz v7, :cond_1

    .line 268
    new-instance v1, Landroid/support/design/widget/AppBarLayout$Behavior$b;

    invoke-direct {v1, v2}, Landroid/support/design/widget/AppBarLayout$Behavior$b;-><init>(Landroid/os/Parcelable;)V

    .line 269
    iput v3, v1, Landroid/support/design/widget/AppBarLayout$Behavior$b;->a:I

    .line 272
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v6}, Lri;->f(Landroid/view/View;)I

    move-result v2

    .line 273
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->e()I

    move-result v3

    add-int/2addr v2, v3

    if-ne v7, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$b;->c:Z

    .line 274
    int-to-float v0, v7

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, v1, Landroid/support/design/widget/AppBarLayout$Behavior$b;->b:F

    move-object v0, v1

    .line 278
    :goto_1
    return-object v0

    .line 276
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 278
    goto :goto_1
.end method

.method public synthetic onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 315
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 316
    and-int/lit8 v2, p5, 0x2

    if-eqz v2, :cond_2

    .line 318
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->b()I

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 319
    :goto_0
    if-eqz v2, :cond_2

    .line 320
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v2

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result v3

    if-gt v2, v3, :cond_2

    .line 321
    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 322
    iget-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 323
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Ljava/lang/ref/WeakReference;

    .line 325
    return v0

    :cond_1
    move v2, v1

    .line 318
    goto :goto_0

    :cond_2
    move v0, v1

    .line 320
    goto :goto_1
.end method

.method public synthetic onStopNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 310
    check-cast p2, Landroid/support/design/widget/AppBarLayout;

    .line 311
    if-nez p4, :cond_0

    .line 312
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/AppBarLayout$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V

    .line 313
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/design/widget/AppBarLayout$Behavior;->g:Ljava/lang/ref/WeakReference;

    .line 314
    return-void
.end method
