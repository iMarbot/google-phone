.class final Landroid/support/design/widget/TabLayout$f;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/design/widget/TabLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "f"
.end annotation


# instance fields
.field private a:Landroid/support/design/widget/TabLayout$d;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private g:I

.field private synthetic h:Landroid/support/design/widget/TabLayout;


# direct methods
.method public constructor <init>(Landroid/support/design/widget/TabLayout;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1
    iput-object p1, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    .line 2
    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/design/widget/TabLayout$f;->g:I

    .line 4
    iget v0, p1, Landroid/support/design/widget/TabLayout;->l:I

    if-eqz v0, :cond_0

    .line 5
    iget v0, p1, Landroid/support/design/widget/TabLayout;->l:I

    .line 6
    invoke-static {p2, v0}, Lvy;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 7
    invoke-static {p0, v0}, Lqy;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 8
    :cond_0
    iget v0, p1, Landroid/support/design/widget/TabLayout;->b:I

    iget v2, p1, Landroid/support/design/widget/TabLayout;->c:I

    iget v3, p1, Landroid/support/design/widget/TabLayout;->d:I

    iget v4, p1, Landroid/support/design/widget/TabLayout;->e:I

    invoke-static {p0, v0, v2, v3, v4}, Lqy;->a(Landroid/view/View;IIII)V

    .line 9
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->setGravity(I)V

    .line 10
    iget-boolean v0, p1, Landroid/support/design/widget/TabLayout;->p:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->setOrientation(I)V

    .line 11
    invoke-virtual {p0, v1}, Landroid/support/design/widget/TabLayout$f;->setClickable(Z)V

    .line 13
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 14
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v0, v2, :cond_2

    .line 15
    new-instance v0, Lqw;

    const/16 v2, 0x3ea

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->getSystemIcon(Landroid/content/Context;I)Landroid/view/PointerIcon;

    move-result-object v1

    invoke-direct {v0, v1}, Lqw;-><init>(Ljava/lang/Object;)V

    .line 18
    :goto_1
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, p0, v0}, Lri;->a(Landroid/view/View;Lqw;)V

    .line 19
    return-void

    :cond_1
    move v0, v1

    .line 10
    goto :goto_0

    .line 16
    :cond_2
    new-instance v0, Lqw;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lqw;-><init>(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private final a(Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 141
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 142
    iget-object v0, v0, Landroid/support/design/widget/TabLayout$d;->a:Landroid/graphics/drawable/Drawable;

    .line 143
    if-eqz v0, :cond_4

    .line 144
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 145
    iget-object v0, v0, Landroid/support/design/widget/TabLayout$d;->a:Landroid/graphics/drawable/Drawable;

    .line 146
    invoke-static {v0}, Lbw;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 148
    :goto_0
    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 149
    iget-object v2, v2, Landroid/support/design/widget/TabLayout$d;->b:Ljava/lang/CharSequence;

    .line 151
    :goto_1
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    if-eqz v4, :cond_6

    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 152
    iget-object v4, v4, Landroid/support/design/widget/TabLayout$d;->c:Ljava/lang/CharSequence;

    move-object v5, v4

    .line 154
    :goto_2
    if-eqz p2, :cond_1

    .line 155
    if-eqz v0, :cond_7

    .line 156
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-object v4, v4, Landroid/support/design/widget/TabLayout;->h:Landroid/content/res/ColorStateList;

    invoke-static {v0, v4}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 157
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-object v4, v4, Landroid/support/design/widget/TabLayout;->i:Landroid/graphics/PorterDuff$Mode;

    if-eqz v4, :cond_0

    .line 158
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-object v4, v4, Landroid/support/design/widget/TabLayout;->i:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v4}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 159
    :cond_0
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    invoke-virtual {p0, v3}, Landroid/support/design/widget/TabLayout$f;->setVisibility(I)V

    .line 164
    :goto_3
    invoke-virtual {p2, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    move v4, v0

    .line 166
    :goto_4
    if-eqz p1, :cond_2

    .line 167
    if-eqz v4, :cond_9

    .line 168
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    invoke-virtual {p0, v3}, Landroid/support/design/widget/TabLayout$f;->setVisibility(I)V

    .line 173
    :goto_5
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 174
    :cond_2
    if-eqz p2, :cond_3

    .line 175
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 177
    if-eqz v4, :cond_c

    invoke-virtual {p2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_c

    .line 178
    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2, v6}, Landroid/support/design/widget/TabLayout;->b(I)I

    move-result v2

    .line 179
    :goto_6
    iget-object v6, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-boolean v6, v6, Landroid/support/design/widget/TabLayout;->p:Z

    if-eqz v6, :cond_a

    .line 180
    invoke-static {v0}, Lbw;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v6

    if-eq v2, v6, :cond_3

    .line 181
    invoke-static {v0, v2}, Lbw;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 182
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 183
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    invoke-virtual {p2}, Landroid/widget/ImageView;->requestLayout()V

    .line 190
    :cond_3
    :goto_7
    if-nez v4, :cond_b

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 191
    invoke-virtual {p0, p0}, Landroid/support/design/widget/TabLayout$f;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 194
    :goto_8
    return-void

    :cond_4
    move-object v0, v1

    .line 147
    goto/16 :goto_0

    :cond_5
    move-object v2, v1

    .line 150
    goto/16 :goto_1

    :cond_6
    move-object v5, v1

    .line 153
    goto :goto_2

    .line 162
    :cond_7
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_8
    move v4, v3

    .line 165
    goto :goto_4

    .line 171
    :cond_9
    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 185
    :cond_a
    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-eq v2, v6, :cond_3

    .line 186
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 187
    invoke-static {v0, v3}, Lbw;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 188
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    invoke-virtual {p2}, Landroid/widget/ImageView;->requestLayout()V

    goto :goto_7

    .line 192
    :cond_b
    invoke-virtual {p0, v1}, Landroid/support/design/widget/TabLayout$f;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 193
    invoke-virtual {p0, v3}, Landroid/support/design/widget/TabLayout$f;->setLongClickable(Z)V

    goto :goto_8

    :cond_c
    move v2, v3

    goto :goto_6
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 84
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 85
    if-eqz v4, :cond_9

    .line 86
    iget-object v0, v4, Landroid/support/design/widget/TabLayout$d;->e:Landroid/view/View;

    move-object v2, v0

    .line 88
    :goto_0
    if-eqz v2, :cond_a

    .line 89
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 90
    if-eq v0, p0, :cond_1

    .line 91
    if-eqz v0, :cond_0

    .line 92
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 93
    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/design/widget/TabLayout$f;->addView(Landroid/view/View;)V

    .line 94
    :cond_1
    iput-object v2, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    .line 95
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    :cond_3
    const v0, 0x1020014

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    .line 103
    sget-object v3, Ltj;->a:Lto;

    invoke-virtual {v3, v0}, Lto;->a(Landroid/widget/TextView;)I

    move-result v0

    .line 104
    iput v0, p0, Landroid/support/design/widget/TabLayout$f;->g:I

    .line 105
    :cond_4
    const v0, 0x1020006

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/design/widget/TabLayout$f;->f:Landroid/widget/ImageView;

    .line 112
    :goto_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    if-nez v0, :cond_c

    .line 113
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    .line 115
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040041

    .line 116
    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 117
    invoke-virtual {p0, v0, v1}, Landroid/support/design/widget/TabLayout$f;->addView(Landroid/view/View;I)V

    .line 118
    iput-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    .line 119
    :cond_5
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    if-nez v0, :cond_6

    .line 121
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040042

    .line 122
    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 123
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->addView(Landroid/view/View;)V

    .line 124
    iput-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    .line 125
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    .line 126
    sget-object v2, Ltj;->a:Lto;

    invoke-virtual {v2, v0}, Lto;->a(Landroid/widget/TextView;)I

    move-result v0

    .line 127
    iput v0, p0, Landroid/support/design/widget/TabLayout$f;->g:I

    .line 128
    :cond_6
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget v2, v2, Landroid/support/design/widget/TabLayout;->f:I

    .line 129
    sget-object v3, Ltj;->a:Lto;

    invoke-virtual {v3, v0, v2}, Lto;->a(Landroid/widget/TextView;I)V

    .line 130
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-object v0, v0, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_7

    .line 131
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget-object v2, v2, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 132
    :cond_7
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v2}, Landroid/support/design/widget/TabLayout$f;->a(Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 135
    :cond_8
    :goto_2
    if-eqz v4, :cond_f

    .line 136
    iget-object v0, v4, Landroid/support/design/widget/TabLayout$d;->f:Landroid/support/design/widget/TabLayout;

    if-nez v0, :cond_e

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab not attached to a TabLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move-object v2, v3

    .line 87
    goto/16 :goto_0

    .line 107
    :cond_a
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 108
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->removeView(Landroid/view/View;)V

    .line 109
    iput-object v3, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    .line 110
    :cond_b
    iput-object v3, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    .line 111
    iput-object v3, p0, Landroid/support/design/widget/TabLayout$f;->f:Landroid/widget/ImageView;

    goto/16 :goto_1

    .line 133
    :cond_c
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    if-nez v0, :cond_d

    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 134
    :cond_d
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->e:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->f:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v2}, Landroid/support/design/widget/TabLayout$f;->a(Landroid/widget/TextView;Landroid/widget/ImageView;)V

    goto :goto_2

    .line 138
    :cond_e
    iget-object v0, v4, Landroid/support/design/widget/TabLayout$d;->f:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout;->a()I

    move-result v0

    iget v2, v4, Landroid/support/design/widget/TabLayout$d;->d:I

    if-ne v0, v2, :cond_f

    .line 139
    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->setSelected(Z)V

    .line 140
    return-void

    :cond_f
    move v0, v1

    .line 139
    goto :goto_3
.end method

.method final a(Landroid/support/design/widget/TabLayout$d;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    if-eq p1, v0, :cond_0

    .line 81
    iput-object p1, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 82
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->a()V

    .line 83
    :cond_0
    return-void
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 39
    const-class v0, Ltv$a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 42
    const-class v0, Ltv$a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 43
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 195
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 196
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 197
    invoke-virtual {p0, v1}, Landroid/support/design/widget/TabLayout$f;->getLocationOnScreen([I)V

    .line 198
    invoke-virtual {p0, v2}, Landroid/support/design/widget/TabLayout$f;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 199
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 200
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getWidth()I

    move-result v0

    .line 201
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getHeight()I

    move-result v4

    .line 202
    aget v5, v1, v8

    div-int/lit8 v6, v4, 0x2

    add-int/2addr v5, v6

    .line 203
    aget v6, v1, v7

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v6

    .line 205
    sget-object v6, Lqy;->a:Lri;

    invoke-virtual {v6, p1}, Lri;->j(Landroid/view/View;)I

    move-result v6

    .line 206
    if-nez v6, :cond_0

    .line 207
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 208
    sub-int v0, v6, v0

    .line 209
    :cond_0
    iget-object v6, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    .line 210
    iget-object v6, v6, Landroid/support/design/widget/TabLayout$d;->c:Ljava/lang/CharSequence;

    .line 211
    invoke-static {v3, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 212
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 213
    const v5, 0x800035

    aget v1, v1, v8

    add-int/2addr v1, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    invoke-virtual {v3, v5, v0, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 215
    :goto_0
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 216
    return v8

    .line 214
    :cond_1
    const/16 v0, 0x51

    invoke-virtual {v3, v0, v7, v4}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 44
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 45
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 46
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    .line 47
    iget v4, v4, Landroid/support/design/widget/TabLayout;->m:I

    .line 49
    if-lez v4, :cond_1

    if-eqz v2, :cond_0

    if-le v0, v4, :cond_1

    .line 50
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget v0, v0, Landroid/support/design/widget/TabLayout;->m:I

    const/high16 v2, -0x80000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 52
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 53
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 54
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget v2, v0, Landroid/support/design/widget/TabLayout;->j:F

    .line 55
    iget v0, p0, Landroid/support/design/widget/TabLayout$f;->g:I

    .line 56
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    if-eqz v4, :cond_7

    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_7

    move v0, v1

    .line 60
    :cond_2
    :goto_0
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTextSize()F

    move-result v4

    .line 61
    iget-object v5, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLineCount()I

    move-result v5

    .line 62
    iget-object v6, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    .line 63
    sget-object v7, Ltj;->a:Lto;

    invoke-virtual {v7, v6}, Lto;->a(Landroid/widget/TextView;)I

    move-result v6

    .line 65
    cmpl-float v7, v2, v4

    if-nez v7, :cond_3

    if-ltz v6, :cond_6

    if-eq v0, v6, :cond_6

    .line 67
    :cond_3
    iget-object v6, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget v6, v6, Landroid/support/design/widget/TabLayout;->o:I

    if-ne v6, v1, :cond_5

    cmpl-float v4, v2, v4

    if-lez v4, :cond_5

    if-ne v5, v1, :cond_5

    .line 68
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    .line 69
    if-eqz v4, :cond_4

    .line 71
    invoke-virtual {v4, v3}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    invoke-virtual {v4}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    div-float v4, v2, v4

    mul-float/2addr v4, v5

    .line 73
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    :cond_4
    move v1, v3

    .line 75
    :cond_5
    if-eqz v1, :cond_6

    .line 76
    iget-object v1, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 77
    iget-object v1, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 78
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 79
    :cond_6
    return-void

    .line 58
    :cond_7
    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLineCount()I

    move-result v4

    if-le v4, v1, :cond_2

    .line 59
    iget-object v2, p0, Landroid/support/design/widget/TabLayout$f;->h:Landroid/support/design/widget/TabLayout;

    iget v2, v2, Landroid/support/design/widget/TabLayout;->k:F

    goto :goto_0
.end method

.method public final performClick()Z
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Landroid/widget/LinearLayout;->performClick()Z

    move-result v0

    .line 21
    iget-object v1, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    if-eqz v1, :cond_1

    .line 22
    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->playSoundEffect(I)V

    .line 24
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->a:Landroid/support/design/widget/TabLayout$d;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$d;->a()V

    .line 25
    const/4 v0, 0x1

    .line 26
    :cond_1
    return v0
.end method

.method public final setSelected(Z)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout$f;->isSelected()Z

    move-result v0

    if-eq v0, p1, :cond_4

    const/4 v0, 0x1

    .line 28
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 29
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 30
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout$f;->sendAccessibilityEvent(I)V

    .line 31
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 33
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 34
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 35
    :cond_2
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 36
    iget-object v0, p0, Landroid/support/design/widget/TabLayout$f;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 37
    :cond_3
    return-void

    .line 27
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
