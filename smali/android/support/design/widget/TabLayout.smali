.class public Landroid/support/design/widget/TabLayout;
.super Landroid/widget/HorizontalScrollView;
.source "PG"


# annotations
.annotation runtime Landroid/support/v4/view/ViewPager$a;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/design/widget/TabLayout$b;,
        Landroid/support/design/widget/TabLayout$e;,
        Landroid/support/design/widget/TabLayout$c;,
        Landroid/support/design/widget/TabLayout$f;,
        Landroid/support/design/widget/TabLayout$d;,
        Landroid/support/design/widget/TabLayout$a;
    }
.end annotation


# static fields
.field private static r:Lps;


# instance fields
.field private A:Landroid/animation/ValueAnimator;

.field private B:Lqv;

.field private C:Landroid/database/DataSetObserver;

.field private D:Landroid/support/design/widget/TabLayout$e;

.field private E:Landroid/support/v4/view/ViewPager$e;

.field private F:Z

.field private G:Lps;

.field public final a:Ljava/util/ArrayList;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Landroid/content/res/ColorStateList;

.field public h:Landroid/content/res/ColorStateList;

.field public i:Landroid/graphics/PorterDuff$Mode;

.field public j:F

.field public k:F

.field public final l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:Landroid/support/v4/view/ViewPager;

.field private s:Landroid/support/design/widget/TabLayout$d;

.field private t:Landroid/support/design/widget/TabLayout$c;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/util/ArrayList;

.field private z:Landroid/support/design/widget/TabLayout$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 431
    new-instance v0, Lpu;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lpu;-><init>(I)V

    sput-object v0, Landroid/support/design/widget/TabLayout;->r:Lps;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    .line 7
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/design/widget/TabLayout;->m:I

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    .line 9
    new-instance v0, Lpt;

    const/16 v2, 0xc

    invoke-direct {v0, v2}, Lpt;-><init>(I)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->G:Lps;

    .line 10
    invoke-static {p1}, Ldu;->a(Landroid/content/Context;)V

    .line 11
    invoke-virtual {p0, v1}, Landroid/support/design/widget/TabLayout;->setHorizontalScrollBarEnabled(Z)V

    .line 12
    new-instance v0, Landroid/support/design/widget/TabLayout$c;

    invoke-direct {v0, p0, p1}, Landroid/support/design/widget/TabLayout$c;-><init>(Landroid/support/design/widget/TabLayout;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    .line 13
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-super {p0, v0, v1, v2}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 14
    sget-object v0, Lao;->X:[I

    const v2, 0x7f1201f6

    .line 15
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 16
    iget-object v2, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    sget v3, Lao;->ae:I

    .line 17
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 19
    iget v4, v2, Landroid/support/design/widget/TabLayout$c;->a:I

    if-eq v4, v3, :cond_0

    .line 20
    iput v3, v2, Landroid/support/design/widget/TabLayout$c;->a:I

    .line 22
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v2}, Lri;->c(Landroid/view/View;)V

    .line 23
    :cond_0
    iget-object v2, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    sget v3, Lao;->ad:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 24
    iget-object v4, v2, Landroid/support/design/widget/TabLayout$c;->b:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    if-eq v4, v3, :cond_1

    .line 25
    iget-object v4, v2, Landroid/support/design/widget/TabLayout$c;->b:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 27
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v2}, Lri;->c(Landroid/view/View;)V

    .line 28
    :cond_1
    sget v2, Lao;->aj:I

    .line 29
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->e:I

    iput v2, p0, Landroid/support/design/widget/TabLayout;->d:I

    iput v2, p0, Landroid/support/design/widget/TabLayout;->c:I

    iput v2, p0, Landroid/support/design/widget/TabLayout;->b:I

    .line 30
    sget v2, Lao;->am:I

    iget v3, p0, Landroid/support/design/widget/TabLayout;->b:I

    .line 31
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->b:I

    .line 32
    sget v2, Lao;->an:I

    iget v3, p0, Landroid/support/design/widget/TabLayout;->c:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->c:I

    .line 33
    sget v2, Lao;->al:I

    iget v3, p0, Landroid/support/design/widget/TabLayout;->d:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->d:I

    .line 34
    sget v2, Lao;->ak:I

    iget v3, p0, Landroid/support/design/widget/TabLayout;->e:I

    .line 35
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->e:I

    .line 36
    sget v2, Lao;->ap:I

    const v3, 0x7f120154

    .line 37
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->f:I

    .line 38
    iget v2, p0, Landroid/support/design/widget/TabLayout;->f:I

    sget-object v3, Lvu;->bS:[I

    .line 39
    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 40
    :try_start_0
    sget v3, Lvu;->bX:I

    const/4 v4, 0x0

    .line 41
    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Landroid/support/design/widget/TabLayout;->j:F

    .line 42
    sget v3, Lvu;->bU:I

    .line 43
    invoke-static {p1, v2, v3}, Lbi;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/content/res/ColorStateList;

    move-result-object v3

    iput-object v3, p0, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    sget v2, Lao;->aq:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 48
    sget v2, Lao;->aq:I

    .line 49
    invoke-static {p1, v0, v2}, Lbi;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;

    .line 50
    :cond_2
    sget v2, Lao;->ao:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 51
    sget v2, Lao;->ao:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 52
    iget-object v3, p0, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v3

    .line 53
    new-array v4, v5, [[I

    .line 54
    new-array v5, v5, [I

    .line 55
    sget-object v6, Landroid/support/design/widget/TabLayout;->SELECTED_STATE_SET:[I

    aput-object v6, v4, v1

    .line 56
    aput v2, v5, v1

    .line 57
    sget-object v2, Landroid/support/design/widget/TabLayout;->EMPTY_STATE_SET:[I

    aput-object v2, v4, v7

    .line 58
    aput v3, v5, v7

    .line 59
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 60
    iput-object v2, p0, Landroid/support/design/widget/TabLayout;->g:Landroid/content/res/ColorStateList;

    .line 61
    :cond_3
    sget v2, Lao;->ab:I

    .line 62
    invoke-static {p1, v0, v2}, Lbi;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Landroid/support/design/widget/TabLayout;->h:Landroid/content/res/ColorStateList;

    .line 63
    sget v2, Lao;->ac:I

    .line 64
    invoke-virtual {v0, v2, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lbw;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    iput-object v2, p0, Landroid/support/design/widget/TabLayout;->i:Landroid/graphics/PorterDuff$Mode;

    .line 65
    sget v2, Lao;->ah:I

    .line 66
    invoke-virtual {v0, v2, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->u:I

    .line 67
    sget v2, Lao;->ag:I

    .line 68
    invoke-virtual {v0, v2, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->v:I

    .line 69
    sget v2, Lao;->Y:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->l:I

    .line 70
    sget v2, Lao;->Z:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->x:I

    .line 71
    sget v2, Lao;->ai:I

    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->o:I

    .line 72
    sget v2, Lao;->aa:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->n:I

    .line 73
    sget v2, Lao;->af:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Landroid/support/design/widget/TabLayout;->p:Z

    .line 74
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 75
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 76
    const v2, 0x7f0d0106

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Landroid/support/design/widget/TabLayout;->k:F

    .line 77
    const v2, 0x7f0d0104

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/TabLayout;->w:I

    .line 80
    iget v0, p0, Landroid/support/design/widget/TabLayout;->o:I

    if-nez v0, :cond_4

    .line 81
    iget v0, p0, Landroid/support/design/widget/TabLayout;->x:I

    iget v2, p0, Landroid/support/design/widget/TabLayout;->b:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 82
    :goto_0
    iget-object v2, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-static {v2, v0, v1, v1, v1}, Lqy;->a(Landroid/view/View;IIII)V

    .line 83
    iget v0, p0, Landroid/support/design/widget/TabLayout;->o:I

    packed-switch v0, :pswitch_data_0

    .line 87
    :goto_1
    invoke-virtual {p0, v7}, Landroid/support/design/widget/TabLayout;->a(Z)V

    .line 88
    return-void

    .line 46
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 84
    :pswitch_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0, v7}, Landroid/support/design/widget/TabLayout$c;->setGravity(I)V

    goto :goto_1

    .line 86
    :pswitch_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$c;->setGravity(I)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final a(IF)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 404
    iget v1, p0, Landroid/support/design/widget/TabLayout;->o:I

    if-nez v1, :cond_1

    .line 405
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v1, p1}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 406
    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 407
    :goto_0
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 408
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 409
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    div-int/lit8 v3, v1, 0x2

    add-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 410
    add-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    .line 412
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, p0}, Lri;->j(Landroid/view/View;)I

    move-result v1

    .line 413
    if-nez v1, :cond_4

    .line 414
    add-int/2addr v0, v2

    .line 417
    :cond_1
    :goto_2
    return v0

    .line 406
    :cond_2
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_0

    :cond_3
    move v1, v0

    .line 407
    goto :goto_1

    .line 415
    :cond_4
    sub-int v0, v2, v0

    .line 416
    goto :goto_2
.end method

.method private a(IFZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Landroid/support/design/widget/TabLayout;->a(IFZZ)V

    .line 90
    return-void
.end method

.method private final a(Landroid/support/design/widget/TabLayout$d;I)V
    .locals 3

    .prologue
    .line 258
    .line 259
    iput p2, p1, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 260
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 261
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 262
    add-int/lit8 v0, p2, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 263
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$d;

    .line 264
    iput v1, v0, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 265
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 266
    :cond_0
    return-void
.end method

.method private final a(Landroid/support/v4/view/ViewPager;ZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 146
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->b(Landroid/support/v4/view/ViewPager$f;)V

    .line 149
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    .line 151
    iget-object v2, v0, Landroid/support/v4/view/ViewPager;->e:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 152
    iget-object v0, v0, Landroid/support/v4/view/ViewPager;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 153
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->z:Landroid/support/design/widget/TabLayout$a;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->z:Landroid/support/design/widget/TabLayout$a;

    .line 155
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 156
    iput-object v3, p0, Landroid/support/design/widget/TabLayout;->z:Landroid/support/design/widget/TabLayout$a;

    .line 157
    :cond_2
    if-eqz p1, :cond_8

    .line 158
    iput-object p1, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    .line 159
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    if-nez v0, :cond_3

    .line 160
    new-instance v0, Landroid/support/design/widget/TabLayout$e;

    invoke-direct {v0, p0}, Landroid/support/design/widget/TabLayout$e;-><init>(Landroid/support/design/widget/TabLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    .line 161
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    .line 162
    iput v4, v0, Landroid/support/design/widget/TabLayout$e;->b:I

    iput v4, v0, Landroid/support/design/widget/TabLayout$e;->a:I

    .line 163
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->D:Landroid/support/design/widget/TabLayout$e;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 164
    new-instance v0, Landroid/support/design/widget/TabLayout$a;

    invoke-direct {v0, p1}, Landroid/support/design/widget/TabLayout$a;-><init>(Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->z:Landroid/support/design/widget/TabLayout$a;

    .line 165
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->z:Landroid/support/design/widget/TabLayout$a;

    .line 166
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 167
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_4
    iget-object v0, p1, Landroid/support/v4/view/ViewPager;->b:Lqv;

    .line 171
    if-eqz v0, :cond_5

    .line 172
    invoke-virtual {p0, v0, p2}, Landroid/support/design/widget/TabLayout;->a(Lqv;Z)V

    .line 173
    :cond_5
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    if-nez v0, :cond_6

    .line 174
    new-instance v0, Landroid/support/v4/view/ViewPager$e;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager$e;-><init>(Landroid/support/design/widget/TabLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    .line 175
    :cond_6
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    invoke-virtual {v0, p2}, Landroid/support/v4/view/ViewPager$e;->a(Z)V

    .line 176
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->E:Landroid/support/v4/view/ViewPager$e;

    .line 177
    iget-object v1, p1, Landroid/support/v4/view/ViewPager;->e:Ljava/util/List;

    if-nez v1, :cond_7

    .line 178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Landroid/support/v4/view/ViewPager;->e:Ljava/util/List;

    .line 179
    :cond_7
    iget-object v1, p1, Landroid/support/v4/view/ViewPager;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    iget v0, p1, Landroid/support/v4/view/ViewPager;->c:I

    .line 182
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Landroid/support/design/widget/TabLayout;->a(IFZ)V

    .line 186
    :goto_0
    iput-boolean p3, p0, Landroid/support/design/widget/TabLayout;->F:Z

    .line 187
    return-void

    .line 184
    :cond_8
    iput-object v3, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    .line 185
    invoke-virtual {p0, v3, v4}, Landroid/support/design/widget/TabLayout;->a(Lqv;Z)V

    goto :goto_0
.end method

.method private final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 275
    instance-of v0, p1, Ldq;

    if-eqz v0, :cond_1

    .line 276
    check-cast p1, Ldq;

    .line 277
    invoke-direct {p0}, Landroid/support/design/widget/TabLayout;->c()Landroid/support/design/widget/TabLayout$d;

    move-result-object v0

    .line 278
    invoke-virtual {p1}, Ldq;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    invoke-virtual {p1}, Ldq;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    .line 280
    iput-object v1, v0, Landroid/support/design/widget/TabLayout$d;->c:Ljava/lang/CharSequence;

    .line 281
    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$d;->b()V

    .line 283
    :cond_0
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Landroid/support/design/widget/TabLayout;->b(Landroid/support/design/widget/TabLayout$d;Z)V

    .line 284
    return-void

    .line 285
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only TabItem instances can be added to TabLayout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final a(Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 2

    .prologue
    .line 286
    iget v0, p0, Landroid/support/design/widget/TabLayout;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/design/widget/TabLayout;->n:I

    if-nez v0, :cond_0

    .line 287
    const/4 v0, 0x0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 288
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    const/4 v0, -0x2

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 290
    const/4 v0, 0x0

    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0
.end method

.method private b(Landroid/support/design/widget/TabLayout$d;Z)V
    .locals 6

    .prologue
    .line 107
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 108
    iget-object v1, p1, Landroid/support/design/widget/TabLayout$d;->f:Landroid/support/design/widget/TabLayout;

    if-eq v1, p0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tab belongs to a different TabLayout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/design/widget/TabLayout$d;I)V

    .line 112
    iget-object v0, p1, Landroid/support/design/widget/TabLayout$d;->g:Landroid/support/design/widget/TabLayout$f;

    .line 113
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    .line 114
    iget v2, p1, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 116
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 117
    invoke-direct {p0, v3}, Landroid/support/design/widget/TabLayout;->a(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 119
    invoke-virtual {v1, v0, v2, v3}, Landroid/support/design/widget/TabLayout$c;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 120
    if-eqz p2, :cond_1

    .line 121
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$d;->a()V

    .line 122
    :cond_1
    return-void
.end method

.method private c()Landroid/support/design/widget/TabLayout$d;
    .locals 3

    .prologue
    .line 123
    sget-object v0, Landroid/support/design/widget/TabLayout;->r:Lps;

    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$d;

    .line 124
    if-nez v0, :cond_2

    .line 125
    new-instance v0, Landroid/support/design/widget/TabLayout$d;

    invoke-direct {v0}, Landroid/support/design/widget/TabLayout$d;-><init>()V

    move-object v1, v0

    .line 126
    :goto_0
    iput-object p0, v1, Landroid/support/design/widget/TabLayout$d;->f:Landroid/support/design/widget/TabLayout;

    .line 128
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->G:Lps;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->G:Lps;

    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$f;

    .line 129
    :goto_1
    if-nez v0, :cond_0

    .line 130
    new-instance v0, Landroid/support/design/widget/TabLayout$f;

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/support/design/widget/TabLayout$f;-><init>(Landroid/support/design/widget/TabLayout;Landroid/content/Context;)V

    .line 131
    :cond_0
    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$f;->a(Landroid/support/design/widget/TabLayout$d;)V

    .line 132
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TabLayout$f;->setFocusable(Z)V

    .line 133
    invoke-direct {p0}, Landroid/support/design/widget/TabLayout;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TabLayout$f;->setMinimumWidth(I)V

    .line 135
    iput-object v0, v1, Landroid/support/design/widget/TabLayout$d;->g:Landroid/support/design/widget/TabLayout$f;

    .line 136
    return-object v1

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private final c(I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 338
    :cond_0
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 339
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->q(Landroid/view/View;)Z

    move-result v0

    .line 340
    if-eqz v0, :cond_1

    iget-object v3, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    .line 341
    invoke-virtual {v3}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v4

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_3

    .line 342
    invoke-virtual {v3, v0}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 343
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    if-gtz v5, :cond_2

    move v0, v1

    .line 347
    :goto_2
    if-eqz v0, :cond_4

    .line 348
    :cond_1
    invoke-direct {p0, p1, v6, v1}, Landroid/support/design/widget/TabLayout;->a(IFZ)V

    goto :goto_0

    .line 345
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 346
    goto :goto_2

    .line 350
    :cond_4
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getScrollX()I

    move-result v0

    .line 351
    invoke-direct {p0, p1, v6}, Landroid/support/design/widget/TabLayout;->a(IF)I

    move-result v3

    .line 352
    if-eq v0, v3, :cond_6

    .line 354
    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    if-nez v4, :cond_5

    .line 355
    new-instance v4, Landroid/animation/ValueAnimator;

    invoke-direct {v4}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    .line 356
    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    sget-object v5, Lap;->a:Landroid/animation/TimeInterpolator;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 357
    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 358
    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    new-instance v5, Ldr;

    invoke-direct {v5, p0}, Ldr;-><init>(Landroid/support/design/widget/TabLayout;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 359
    :cond_5
    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    const/4 v5, 0x2

    new-array v5, v5, [I

    aput v0, v5, v2

    aput v3, v5, v1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 360
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 361
    :cond_6
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    const/16 v1, 0x12c

    invoke-virtual {v0, p1, v1}, Landroid/support/design/widget/TabLayout$c;->b(II)V

    goto :goto_0
.end method

.method private final d()I
    .locals 2

    .prologue
    .line 426
    iget v0, p0, Landroid/support/design/widget/TabLayout;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 427
    iget v0, p0, Landroid/support/design/widget/TabLayout;->u:I

    .line 428
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/design/widget/TabLayout;->o:I

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/design/widget/TabLayout;->w:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 363
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v4

    .line 364
    if-ge p1, v4, :cond_2

    move v3, v2

    .line 365
    :goto_0
    if-ge v3, v4, :cond_2

    .line 366
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0, v3}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 367
    if-ne v3, p1, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setSelected(Z)V

    .line 368
    if-ne v3, p1, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setActivated(Z)V

    .line 369
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 367
    goto :goto_1

    :cond_1
    move v0, v2

    .line 368
    goto :goto_2

    .line 370
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->s:Landroid/support/design/widget/TabLayout$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->s:Landroid/support/design/widget/TabLayout$d;

    .line 141
    iget v0, v0, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(I)Landroid/support/design/widget/TabLayout$d;
    .locals 1

    .prologue
    .line 137
    if-ltz p1, :cond_0

    .line 138
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 139
    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$d;

    goto :goto_0
.end method

.method final a(IFZZ)V
    .locals 3

    .prologue
    .line 91
    int-to-float v0, p1

    add-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 92
    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v1}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    if-eqz p4, :cond_3

    .line 95
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    .line 96
    iget-object v2, v1, Landroid/support/design/widget/TabLayout$c;->e:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/design/widget/TabLayout$c;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    iget-object v2, v1, Landroid/support/design/widget/TabLayout$c;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 98
    :cond_2
    iput p1, v1, Landroid/support/design/widget/TabLayout$c;->c:I

    .line 99
    iput p2, v1, Landroid/support/design/widget/TabLayout$c;->d:F

    .line 100
    invoke-virtual {v1}, Landroid/support/design/widget/TabLayout$c;->a()V

    .line 101
    :cond_3
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 102
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->A:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 103
    :cond_4
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/TabLayout;->a(IF)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/support/design/widget/TabLayout;->scrollTo(II)V

    .line 104
    if-eqz p3, :cond_0

    .line 105
    invoke-direct {p0, v0}, Landroid/support/design/widget/TabLayout;->d(I)V

    goto :goto_0
.end method

.method final a(Landroid/support/design/widget/TabLayout$d;Z)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 371
    iget-object v2, p0, Landroid/support/design/widget/TabLayout;->s:Landroid/support/design/widget/TabLayout$d;

    .line 372
    if-ne v2, p1, :cond_2

    .line 373
    if-eqz v2, :cond_1

    .line 375
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 376
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 377
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 379
    :cond_0
    iget v0, p1, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 380
    invoke-direct {p0, v0}, Landroid/support/design/widget/TabLayout;->c(I)V

    .line 403
    :cond_1
    return-void

    .line 381
    :cond_2
    if-eqz p1, :cond_5

    .line 382
    iget v0, p1, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 384
    :goto_1
    if-eqz p2, :cond_4

    .line 385
    if-eqz v2, :cond_3

    .line 386
    iget v3, v2, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 387
    if-ne v3, v1, :cond_6

    :cond_3
    if-eq v0, v1, :cond_6

    .line 388
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, v0, v3, v4}, Landroid/support/design/widget/TabLayout;->a(IFZ)V

    .line 390
    :goto_2
    if-eq v0, v1, :cond_4

    .line 391
    invoke-direct {p0, v0}, Landroid/support/design/widget/TabLayout;->d(I)V

    .line 392
    :cond_4
    if-eqz v2, :cond_7

    .line 394
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-ltz v0, :cond_7

    .line 395
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 396
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_5
    move v0, v1

    .line 383
    goto :goto_1

    .line 389
    :cond_6
    invoke-direct {p0, v0}, Landroid/support/design/widget/TabLayout;->c(I)V

    goto :goto_2

    .line 397
    :cond_7
    iput-object p1, p0, Landroid/support/design/widget/TabLayout;->s:Landroid/support/design/widget/TabLayout$d;

    .line 398
    if-eqz p1, :cond_1

    .line 400
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_1

    .line 401
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$a;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout$a;->a(Landroid/support/design/widget/TabLayout$d;)V

    .line 402
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 143
    .line 144
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/v4/view/ViewPager;ZZ)V

    .line 145
    return-void
.end method

.method public final a(Lqv;Z)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->C:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->C:Landroid/database/DataSetObserver;

    .line 204
    iget-object v0, v0, Lqv;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, v1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 205
    :cond_0
    iput-object p1, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    .line 206
    if-eqz p2, :cond_2

    if-eqz p1, :cond_2

    .line 207
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->C:Landroid/database/DataSetObserver;

    if-nez v0, :cond_1

    .line 208
    new-instance v0, Landroid/support/design/widget/TabLayout$b;

    invoke-direct {v0, p0}, Landroid/support/design/widget/TabLayout$b;-><init>(Landroid/support/design/widget/TabLayout;)V

    iput-object v0, p0, Landroid/support/design/widget/TabLayout;->C:Landroid/database/DataSetObserver;

    .line 209
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->C:Landroid/database/DataSetObserver;

    .line 210
    iget-object v1, p1, Lqv;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v1, v0}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 211
    :cond_2
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->b()V

    .line 212
    return-void
.end method

.method final a(Z)V
    .locals 3

    .prologue
    .line 418
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 419
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 420
    invoke-direct {p0}, Landroid/support/design/widget/TabLayout;->d()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setMinimumWidth(I)V

    .line 421
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v0}, Landroid/support/design/widget/TabLayout;->a(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 422
    if-eqz p1, :cond_0

    .line 423
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 424
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 425
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1}, Landroid/support/design/widget/TabLayout;->a(Landroid/view/View;)V

    .line 268
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0, p1}, Landroid/support/design/widget/TabLayout;->a(Landroid/view/View;)V

    .line 270
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0, p1}, Landroid/support/design/widget/TabLayout;->a(Landroid/view/View;)V

    .line 274
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0, p1}, Landroid/support/design/widget/TabLayout;->a(Landroid/view/View;)V

    .line 272
    return-void
.end method

.method final b(I)I
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 213
    .line 214
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$c;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 216
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/TabLayout$c;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$f;

    .line 217
    iget-object v3, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v3, v2}, Landroid/support/design/widget/TabLayout$c;->removeViewAt(I)V

    .line 218
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v0, v4}, Landroid/support/design/widget/TabLayout$f;->a(Landroid/support/design/widget/TabLayout$d;)V

    .line 221
    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout$f;->setSelected(Z)V

    .line 222
    iget-object v3, p0, Landroid/support/design/widget/TabLayout;->G:Lps;

    invoke-interface {v3, v0}, Lps;->a(Ljava/lang/Object;)Z

    .line 223
    :cond_0
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->requestLayout()V

    .line 224
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 225
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$d;

    .line 227
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 229
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->f:Landroid/support/design/widget/TabLayout;

    .line 230
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->g:Landroid/support/design/widget/TabLayout$f;

    .line 231
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->a:Landroid/graphics/drawable/Drawable;

    .line 232
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->b:Ljava/lang/CharSequence;

    .line 233
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->c:Ljava/lang/CharSequence;

    .line 234
    const/4 v3, -0x1

    iput v3, v0, Landroid/support/design/widget/TabLayout$d;->d:I

    .line 235
    iput-object v4, v0, Landroid/support/design/widget/TabLayout$d;->e:Landroid/view/View;

    .line 236
    sget-object v3, Landroid/support/design/widget/TabLayout;->r:Lps;

    invoke-interface {v3, v0}, Lps;->a(Ljava/lang/Object;)Z

    goto :goto_1

    .line 238
    :cond_2
    iput-object v4, p0, Landroid/support/design/widget/TabLayout;->s:Landroid/support/design/widget/TabLayout$d;

    .line 239
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    if-eqz v0, :cond_4

    .line 240
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    invoke-virtual {v0}, Lqv;->b()I

    move-result v2

    move v0, v1

    .line 241
    :goto_2
    if-ge v0, v2, :cond_3

    .line 242
    invoke-direct {p0}, Landroid/support/design/widget/TabLayout;->c()Landroid/support/design/widget/TabLayout$d;

    move-result-object v3

    iget-object v4, p0, Landroid/support/design/widget/TabLayout;->B:Lqv;

    invoke-virtual {v4, v0}, Lqv;->c(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 243
    iput-object v4, v3, Landroid/support/design/widget/TabLayout$d;->b:Ljava/lang/CharSequence;

    .line 244
    invoke-virtual {v3}, Landroid/support/design/widget/TabLayout$d;->b()V

    .line 246
    invoke-direct {p0, v3, v1}, Landroid/support/design/widget/TabLayout;->b(Landroid/support/design/widget/TabLayout$d;Z)V

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 248
    :cond_3
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_4

    if-lez v2, :cond_4

    .line 249
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    .line 250
    iget v0, v0, Landroid/support/v4/view/ViewPager;->c:I

    .line 252
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->a()I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 253
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 254
    if-ge v0, v1, :cond_4

    .line 255
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout;->a(I)Landroid/support/design/widget/TabLayout$d;

    move-result-object v0

    .line 256
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/design/widget/TabLayout$d;Z)V

    .line 257
    :cond_4
    return-void
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p0, p1}, Landroid/support/design/widget/TabLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 429
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 191
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 192
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->q:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 193
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 194
    instance-of v1, v0, Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    .line 195
    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {p0, v0, v2, v2}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/v4/view/ViewPager;ZZ)V

    .line 196
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    .line 198
    iget-boolean v0, p0, Landroid/support/design/widget/TabLayout;->F:Z

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/v4/view/ViewPager;)V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/TabLayout;->F:Z

    .line 201
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 293
    .line 295
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_7

    .line 296
    iget-object v0, p0, Landroid/support/design/widget/TabLayout;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout$d;

    .line 297
    if-eqz v0, :cond_2

    .line 298
    iget-object v5, v0, Landroid/support/design/widget/TabLayout$d;->a:Landroid/graphics/drawable/Drawable;

    .line 299
    if-eqz v5, :cond_2

    .line 300
    iget-object v0, v0, Landroid/support/design/widget/TabLayout$d;->b:Ljava/lang/CharSequence;

    .line 301
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 305
    :goto_1
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/design/widget/TabLayout;->p:Z

    if-nez v0, :cond_3

    const/16 v0, 0x48

    .line 306
    :goto_2
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TabLayout;->b(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    .line 307
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 313
    :goto_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 314
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-eqz v3, :cond_0

    .line 316
    iget v3, p0, Landroid/support/design/widget/TabLayout;->v:I

    if-lez v3, :cond_4

    .line 317
    iget v0, p0, Landroid/support/design/widget/TabLayout;->v:I

    .line 318
    :goto_4
    iput v0, p0, Landroid/support/design/widget/TabLayout;->m:I

    .line 319
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 320
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 321
    invoke-virtual {p0, v2}, Landroid/support/design/widget/TabLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 323
    iget v0, p0, Landroid/support/design/widget/TabLayout;->o:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    .line 327
    :goto_5
    if-eqz v0, :cond_1

    .line 329
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 331
    invoke-static {p2, v0, v1}, Landroid/support/design/widget/TabLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 333
    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 334
    invoke-virtual {v3, v1, v0}, Landroid/view/View;->measure(II)V

    .line 335
    :cond_1
    return-void

    .line 304
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 305
    :cond_3
    const/16 v0, 0x30

    goto :goto_2

    .line 309
    :sswitch_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 310
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_3

    .line 312
    :sswitch_1
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_3

    .line 318
    :cond_4
    const/16 v3, 0x38

    invoke-virtual {p0, v3}, Landroid/support/design/widget/TabLayout;->b(I)I

    move-result v3

    sub-int/2addr v0, v3

    goto :goto_4

    .line 324
    :pswitch_0
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getMeasuredWidth()I

    move-result v4

    if-ge v0, v4, :cond_5

    move v0, v1

    goto :goto_5

    :cond_5
    move v0, v2

    goto :goto_5

    .line 326
    :pswitch_1
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getMeasuredWidth()I

    move-result v4

    if-eq v0, v4, :cond_6

    :goto_6
    move v0, v1

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_1

    .line 307
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
    .end sparse-switch

    .line 323
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public shouldDelayChildPressedState()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 188
    .line 189
    iget-object v1, p0, Landroid/support/design/widget/TabLayout;->t:Landroid/support/design/widget/TabLayout$c;

    invoke-virtual {v1}, Landroid/support/design/widget/TabLayout$c;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/design/widget/TabLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 190
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
