.class public abstract Landroid/support/v7/widget/RecyclerView$o;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "o"
.end annotation


# instance fields
.field public a:I

.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Landroid/support/v7/widget/RecyclerView$f;

.field public d:Z

.field public e:Z

.field public f:Landroid/view/View;

.field public final g:Lacx;


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView$o;->e:Z

    if-nez v0, :cond_0

    .line 17
    :goto_0
    return-void

    .line 3
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$o;->b()V

    .line 4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$o;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 5
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->a:I

    .line 7
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView$o;->f:Landroid/view/View;

    .line 8
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$o;->a:I

    .line 9
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView$o;->d:Z

    .line 10
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView$o;->e:Z

    .line 11
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$o;->c:Landroid/support/v7/widget/RecyclerView$f;

    .line 13
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    if-ne v1, p0, :cond_1

    .line 14
    iput-object v2, v0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    .line 15
    :cond_1
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView$o;->c:Landroid/support/v7/widget/RecyclerView$f;

    .line 16
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView$o;->b:Landroid/support/v7/widget/RecyclerView;

    goto :goto_0
.end method

.method protected abstract a(IILacx;)V
.end method

.method protected abstract a(Landroid/view/View;Lacx;)V
.end method

.method protected abstract b()V
.end method
