.class public Landroid/support/v7/widget/ActionMenuView$b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lxv;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/ActionMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# static fields
.field public static a:Lagx;

.field public static b:Laqv;

.field public static c:Lbdi;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    return-void
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;Z)I
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p4}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    .line 30
    :cond_1
    if-nez p5, :cond_2

    .line 31
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v0

    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p1, p3}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    .line 33
    invoke-virtual {p1, p2}, Lacl;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 34
    invoke-virtual {p1}, Lacl;->e()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;ZZ)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-virtual {p4}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v1

    .line 8
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v2

    .line 9
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 10
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v2

    .line 11
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v3

    .line 12
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 13
    if-eqz p6, :cond_2

    .line 14
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v1

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 16
    :goto_1
    if-eqz p5, :cond_0

    .line 18
    invoke-virtual {p1, p3}, Lacl;->b(Landroid/view/View;)I

    move-result v1

    .line 19
    invoke-virtual {p1, p2}, Lacl;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 20
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 21
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v2

    .line 22
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    .line 23
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 24
    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 25
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Lacl;->b()I

    move-result v1

    .line 26
    invoke-virtual {p1, p2}, Lacl;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 27
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 15
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method public static a(Landroid/text/format/Time;JJ)I
    .locals 5

    .prologue
    .line 273
    invoke-virtual {p0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 274
    iget-wide v0, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p1, p2, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    .line 275
    invoke-virtual {p0, p3, p4}, Landroid/text/format/Time;->set(J)V

    .line 276
    iget-wide v2, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p3, p4, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 277
    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 314
    .line 315
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt p1, v0, :cond_3

    .line 316
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 329
    :cond_0
    :goto_1
    return p1

    .line 318
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 319
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 320
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr p1, v0

    .line 321
    goto :goto_0

    .line 325
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    .line 326
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 327
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr p1, v0

    .line 322
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 323
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p1, v0, :cond_2

    goto :goto_1
.end method

.method public static a(Ljava/lang/Long;Ljava/lang/Long;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x1

    const-wide/16 v2, 0x0

    .line 80
    if-eqz p0, :cond_2

    .line 81
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/support/v7/widget/ActionMenuView$b;->c(J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    move-wide v0, v2

    .line 84
    goto :goto_0

    .line 85
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/provider/ContactsContract$Contacts;->isEnterpriseContactId(J)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_3
    move-wide v0, v2

    .line 87
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lagx;
    .locals 2

    .prologue
    .line 118
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    .line 126
    :goto_0
    return-object v0

    .line 121
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 122
    instance-of v1, v0, Lagy;

    if-eqz v1, :cond_1

    .line 123
    check-cast v0, Lagy;

    invoke-interface {v0}, Lagy;->a()Lagx;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    .line 124
    :cond_1
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    if-nez v0, :cond_2

    .line 125
    new-instance v0, Lagz;

    invoke-direct {v0}, Lagz;-><init>()V

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    .line 126
    :cond_2
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->a:Lagx;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 71
    packed-switch p1, :pswitch_data_0

    .line 73
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 72
    :pswitch_0
    invoke-static {p1}, Landroid/provider/ContactsContract$StatusUpdates;->getPresenceIconResourceId(I)I

    move-result v0

    invoke-static {p0, v0}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/telecom/PhoneAccount;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 101
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 102
    :cond_0
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, p1}, Landroid/support/v7/widget/ActionMenuView$b;->b(Landroid/telecom/PhoneAccount;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 92
    sget-object v0, Landroid/provider/ContactsContract$Directory;->ENTERPRISE_CONTENT_URI:Landroid/net/Uri;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$Directory;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 254
    if-nez p1, :cond_0

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "uri must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 257
    const-string v1, "com.android.contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 258
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 259
    const-string v1, "vnd.android.cursor.item/contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 271
    :goto_0
    return-object p1

    .line 261
    :cond_1
    const-string v1, "vnd.android.cursor.item/raw_contact"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 262
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 263
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 264
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 265
    invoke-static {p0, v0}, Landroid/provider/ContactsContract$RawContacts;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    .line 266
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "uri format is unknown"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_3
    const-string v1, "contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 268
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 269
    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 270
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 271
    invoke-static {p0, v0}, Landroid/provider/ContactsContract$RawContacts;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    .line 272
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "uri authority is unknown"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/telecom/TelecomManager;)Landroid/telecom/PhoneAccountHandle;
    .locals 1

    .prologue
    .line 111
    if-eqz p0, :cond_0

    .line 112
    invoke-virtual {p0}, Landroid/telecom/TelecomManager;->getSimCallManager()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 205
    if-nez p0, :cond_1

    .line 206
    const/4 v0, 0x0

    .line 215
    :cond_0
    return-object v0

    .line 207
    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 208
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, -0x1

    .line 209
    :goto_0
    if-ltz v1, :cond_0

    .line 210
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    .line 211
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;)Landroid/text/style/TtsSpan;

    move-result-object v3

    .line 212
    const/16 v4, 0x21

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 213
    invoke-virtual {p0, p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Landroid/text/style/TtsSpan;
    .locals 1

    .prologue
    .line 110
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->createTtsSpan(Ljava/lang/String;)Landroid/text/style/TtsSpan;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 216
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p0, p1, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {v0, p2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 109
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->createTtsSpannable(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;Ljava/lang/CharSequence;ILandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 147
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 148
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    if-nez p1, :cond_0

    const-string p1, ""

    .line 155
    :cond_0
    :goto_0
    return-object p1

    .line 150
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    .line 151
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->c(Ljava/lang/Integer;)I

    move-result v0

    .line 155
    :cond_2
    :goto_1
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 152
    :cond_3
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/Integer;)I

    move-result v0

    .line 153
    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    .line 154
    const-string v1, "ContactDisplayUtils.getLabelForCallOrSms"

    const/16 v2, 0x60

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "un-recognized interaction type: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Defaulting to ContactDisplayUtils.INTERACTION_CALL."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lami;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 368
    .line 369
    iget-wide v0, p1, Lami;->b:J

    .line 372
    iget v2, p1, Lami;->c:I

    .line 374
    const-string v3, "third_party_licenses"

    invoke-static {p0, v3, v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JI)Ljava/lang/String;
    .locals 4

    .prologue
    .line 375
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 376
    const v1, 0x7f0e01f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    .line 377
    const-string v2, "raw"

    .line 378
    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 379
    invoke-static {v0, p2, p3, p4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/io/InputStream;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;JI)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v0, 0x400

    .line 380
    new-array v0, v0, [B

    .line 381
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 382
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    .line 383
    if-lez p3, :cond_0

    .line 384
    :goto_0
    if-lez p3, :cond_1

    const/4 v2, 0x0

    const/16 v3, 0x400

    .line 385
    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {p0, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 386
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 387
    sub-int/2addr p3, v2

    goto :goto_0

    .line 383
    :cond_0
    const p3, 0x7fffffff

    goto :goto_0

    .line 388
    :cond_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :try_start_1
    const-string v0, "UTF-8"

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to read license or metadata text."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 393
    :catch_1
    move-exception v0

    .line 394
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unsupported encoding UTF8. This should always be supported."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    if-nez p2, :cond_2

    .line 219
    if-eqz p0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object p0, p1

    .line 219
    goto :goto_0

    .line 220
    :cond_2
    invoke-virtual {p2}, Lalj;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 222
    invoke-virtual {p2}, Lalj;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 223
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object p0, p1

    .line 224
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-static {p0, p1, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 406
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CallLogAsyncTaskUtil.markVoicemailAsRead, voicemailUri: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 407
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    if-nez v0, :cond_0

    .line 408
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView$b;->c()V

    .line 409
    :cond_0
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    new-instance v1, Laof;

    invoke-direct {v1, p0, p1}, Laof;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 410
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Laoi;)V
    .locals 3

    .prologue
    .line 411
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    if-nez v0, :cond_0

    .line 412
    invoke-static {}, Lbdj;->b()Lbdi;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    .line 413
    :cond_0
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    new-instance v1, Laog;

    invoke-direct {v1, p0, p1, p2}, Laog;-><init>(Landroid/content/Context;Landroid/net/Uri;Laoi;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 414
    return-void
.end method

.method public static a(Landroid/content/Context;[J)V
    .locals 3

    .prologue
    .line 420
    invoke-static {p0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    invoke-static {p0}, Lbsw;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    if-nez v0, :cond_2

    .line 424
    invoke-static {}, Lbdj;->b()Lbdi;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    .line 425
    :cond_2
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    new-instance v1, Laoh;

    invoke-direct {v1, p1, p0}, Laoh;-><init>([JLandroid/content/Context;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/widget/ListView;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 243
    const v0, 0x7f0f000a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 244
    const v1, 0x7f0f0009

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 245
    if-lez v0, :cond_1

    if-lez v1, :cond_1

    .line 246
    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 247
    const v2, 0x7f0e015c

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 248
    if-nez v2, :cond_0

    .line 249
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a list card view who can be turned visible whenever it is necessary."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 251
    new-instance v2, Lalq;

    invoke-direct {v2, p1, p2, v0, v1}, Lalq;-><init>(Landroid/widget/ListView;Landroid/view/View;II)V

    invoke-static {p1, v3, v2}, Lbib;->a(Landroid/view/View;ZLjava/lang/Runnable;)V

    .line 252
    :cond_1
    return-void
.end method

.method public static a(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 45
    if-eqz p0, :cond_0

    .line 46
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 47
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/ListView;III)V
    .locals 8

    .prologue
    const-wide v6, 0x3ff199999999999aL    # 1.1

    .line 234
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    .line 235
    int-to-double v0, p2

    shl-int/lit8 v2, p2, 0x1

    add-int/2addr v2, p3

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 236
    int-to-double v2, p1

    mul-double/2addr v2, v0

    mul-double/2addr v2, v6

    double-to-int v2, v2

    .line 237
    invoke-virtual {p0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v3

    int-to-double v4, p1

    mul-double/2addr v0, v4

    mul-double/2addr v0, v6

    double-to-int v0, v0

    .line 238
    invoke-virtual {p0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v1

    .line 239
    invoke-virtual {p0, v2, v3, v0, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 240
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 241
    const/high16 v0, 0x2000000

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 242
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 48
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 49
    const/16 v0, 0x14

    if-le v4, v0, :cond_1

    .line 70
    :cond_0
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_5

    .line 52
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafz;

    .line 53
    if-eqz v0, :cond_4

    .line 54
    add-int/lit8 v1, v3, 0x1

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    .line 55
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lafz;

    .line 56
    if-eqz v1, :cond_2

    .line 57
    invoke-interface {v0, v1, p1}, Lafz;->a(Ljava/lang/Object;Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 58
    invoke-interface {v0, v1}, Lafz;->a(Ljava/lang/Object;)V

    .line 59
    invoke-interface {p0, v2, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 60
    :cond_3
    invoke-interface {v1, v0, p1}, Lafz;->a(Ljava/lang/Object;Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 61
    invoke-interface {v1, v0}, Lafz;->a(Ljava/lang/Object;)V

    .line 62
    invoke-interface {p0, v3, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 65
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 66
    :cond_5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 67
    :cond_6
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_6

    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_2
.end method

.method public static a(J)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 94
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_2

    .line 95
    cmp-long v2, p0, v4

    if-eqz v2, :cond_0

    const-wide/32 v2, 0x3b9aca01

    cmp-long v2, p0, v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 96
    :cond_1
    :goto_0
    return v0

    :cond_2
    cmp-long v2, p0, v4

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/telecom/Call;)Z
    .locals 3

    .prologue
    const/high16 v2, 0x800000

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    .line 89
    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getCallCapabilities()I

    move-result v0

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 90
    goto :goto_0
.end method

.method public static a(Landroid/view/View;Lahk;Z)Z
    .locals 2

    .prologue
    .line 127
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/view/View;Lahk;ZZ)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;Lahk;ZZ)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 128
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 129
    const v0, 0x7f0e00d1

    .line 130
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 132
    if-eqz p1, :cond_2

    .line 133
    iget v4, p1, Lahk;->a:I

    const/4 v5, -0x2

    if-ne v4, v5, :cond_0

    .line 134
    if-eqz p2, :cond_2

    .line 135
    const v2, 0x7f1101d8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    .line 145
    :goto_0
    return v0

    .line 137
    :cond_0
    iget v4, p1, Lahk;->a:I

    if-nez v4, :cond_1

    .line 138
    const v4, 0x7f1101d4

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p1, Lahk;->c:Ljava/lang/String;

    aput-object v6, v5, v2

    .line 139
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 140
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 141
    goto :goto_0

    .line 142
    :cond_1
    iget v3, p1, Lahk;->a:I

    const/4 v4, -0x3

    if-ne v3, v4, :cond_2

    .line 143
    const v2, 0x7f1101d5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    .line 144
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/Integer;)Z
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/support/v7/widget/RecyclerView$p;Lacl;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$f;Z)I
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p4}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 36
    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_0
    return v0

    .line 37
    :cond_1
    if-nez p5, :cond_2

    .line 38
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v0

    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {p1, p3}, Lacl;->b(Landroid/view/View;)I

    move-result v0

    .line 40
    invoke-virtual {p1, p2}, Lacl;->a(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 41
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v1

    .line 42
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    .line 43
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 44
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Integer;)I
    .locals 2

    .prologue
    const v0, 0x7f1100a8

    .line 156
    if-nez p0, :cond_0

    .line 179
    :goto_0
    :pswitch_0
    return v0

    .line 158
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 179
    const v0, 0x7f11006e

    goto :goto_0

    .line 159
    :pswitch_1
    const v0, 0x7f11007b

    goto :goto_0

    .line 160
    :pswitch_2
    const v0, 0x7f1100a7

    goto :goto_0

    .line 161
    :pswitch_3
    const v0, 0x7f1100b3

    goto :goto_0

    .line 162
    :pswitch_4
    const v0, 0x7f11007a

    goto :goto_0

    .line 163
    :pswitch_5
    const v0, 0x7f110079

    goto :goto_0

    .line 164
    :pswitch_6
    const v0, 0x7f1100aa

    goto :goto_0

    .line 166
    :pswitch_7
    const v0, 0x7f110069

    goto :goto_0

    .line 167
    :pswitch_8
    const v0, 0x7f11006a

    goto :goto_0

    .line 168
    :pswitch_9
    const v0, 0x7f11006b

    goto :goto_0

    .line 169
    :pswitch_a
    const v0, 0x7f11008c

    goto :goto_0

    .line 170
    :pswitch_b
    const v0, 0x7f1100a5

    goto :goto_0

    .line 171
    :pswitch_c
    const v0, 0x7f1100a9

    goto :goto_0

    .line 172
    :pswitch_d
    const v0, 0x7f1100ab

    goto :goto_0

    .line 173
    :pswitch_e
    const v0, 0x7f1100b0

    goto :goto_0

    .line 174
    :pswitch_f
    const v0, 0x7f1100b1

    goto :goto_0

    .line 175
    :pswitch_10
    const v0, 0x7f1100b4

    goto :goto_0

    .line 176
    :pswitch_11
    const v0, 0x7f1100b5

    goto :goto_0

    .line 177
    :pswitch_12
    const v0, 0x7f110066

    goto :goto_0

    .line 178
    :pswitch_13
    const v0, 0x7f1100a6

    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lalu;
    .locals 6

    .prologue
    const/16 v5, 0xa

    .line 278
    new-instance v2, Lalu;

    invoke-direct {v2}, Lalu;-><init>()V

    .line 279
    invoke-static {p0, p1}, Landroid/support/v7/widget/ActionMenuView$b;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 280
    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    .line 281
    add-int/lit8 v0, v3, -0x1

    move v1, v0

    .line 282
    :goto_0
    if-ltz v1, :cond_0

    .line 283
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_0

    .line 284
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 285
    :cond_0
    add-int/lit8 v0, v3, 0x1

    .line 286
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 287
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eq v4, v5, :cond_1

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 289
    :cond_1
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lalu;->b:Ljava/lang/String;

    .line 290
    add-int/lit8 v0, v1, 0x1

    sub-int v0, v3, v0

    iput v0, v2, Lalu;->a:I

    .line 291
    :cond_2
    return-object v2
.end method

.method public static b(Landroid/telecom/PhoneAccount;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Landroid/telecom/PhoneAccount;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v0

    .line 105
    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 347
    const-string v0, "phone"

    .line 348
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 349
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getVoiceMailAlphaTag()Ljava/lang/String;

    move-result-object v0

    .line 350
    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    packed-switch p1, :pswitch_data_0

    .line 79
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    const v1, 0x7f1102e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_1
    const v1, 0x7f1102e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_2
    const v1, 0x7f1102eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 330
    const/4 v0, 0x0

    .line 331
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 332
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    .line 333
    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 334
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    goto :goto_0

    .line 336
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 337
    const-string v0, ""

    .line 346
    :goto_1
    return-object v0

    .line 338
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 339
    :goto_2
    if-ltz v1, :cond_3

    .line 340
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 341
    add-int/lit8 v1, v1, -0x1

    .line 342
    :cond_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 343
    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 344
    add-int/lit8 v1, v1, -0x1

    .line 345
    goto :goto_2

    .line 346
    :cond_3
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    if-nez p2, :cond_2

    .line 227
    if-eqz p0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move-object p0, p1

    .line 227
    goto :goto_0

    .line 228
    :cond_2
    invoke-virtual {p2}, Lalj;->a()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 230
    invoke-virtual {p2}, Lalj;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 231
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object p0, p1

    .line 232
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 415
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 416
    const-string v1, "deleted"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 418
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->e(Landroid/content/Context;)V

    .line 419
    return-void
.end method

.method public static synthetic b(Landroid/widget/ListView;III)V
    .locals 0

    .prologue
    .line 253
    invoke-static {p0, p1, p2, p3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/widget/ListView;III)V

    return-void
.end method

.method public static b()Z
    .locals 3

    .prologue
    .line 114
    :try_start_0
    const-class v0, Landroid/telecom/TelecomManager;

    const-string v1, "EXTRA_IS_HANDOVER"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 115
    const-string v1, "android.telecom.extra.IS_HANDOVER"

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 117
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(J)Z
    .locals 2

    .prologue
    .line 97
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 98
    invoke-static {p0, p1}, Landroid/provider/ContactsContract$Directory;->isRemoteDirectoryId(J)Z

    move-result v0

    .line 99
    :goto_0
    return v0

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 204
    if-eqz p0, :cond_0

    sget-object v0, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/Integer;)I
    .locals 2

    .prologue
    const v0, 0x7f1102c6

    .line 180
    if-nez p0, :cond_0

    .line 203
    :goto_0
    :pswitch_0
    return v0

    .line 182
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 203
    const v0, 0x7f1102bd

    goto :goto_0

    .line 183
    :pswitch_1
    const v0, 0x7f1102c1

    goto :goto_0

    .line 184
    :pswitch_2
    const v0, 0x7f1102c5

    goto :goto_0

    .line 185
    :pswitch_3
    const v0, 0x7f1102cc

    goto :goto_0

    .line 186
    :pswitch_4
    const v0, 0x7f1102c0

    goto :goto_0

    .line 187
    :pswitch_5
    const v0, 0x7f1102bf

    goto :goto_0

    .line 188
    :pswitch_6
    const v0, 0x7f1102c8

    goto :goto_0

    .line 190
    :pswitch_7
    const v0, 0x7f1102ba

    goto :goto_0

    .line 191
    :pswitch_8
    const v0, 0x7f1102bb

    goto :goto_0

    .line 192
    :pswitch_9
    const v0, 0x7f1102bc

    goto :goto_0

    .line 193
    :pswitch_a
    const v0, 0x7f1102c2

    goto :goto_0

    .line 194
    :pswitch_b
    const v0, 0x7f1102c3

    goto :goto_0

    .line 195
    :pswitch_c
    const v0, 0x7f1102c7

    goto :goto_0

    .line 196
    :pswitch_d
    const v0, 0x7f1102c9

    goto :goto_0

    .line 197
    :pswitch_e
    const v0, 0x7f1102ca

    goto :goto_0

    .line 198
    :pswitch_f
    const v0, 0x7f1102cb

    goto :goto_0

    .line 199
    :pswitch_10
    const v0, 0x7f1102cd

    goto :goto_0

    .line 200
    :pswitch_11
    const v0, 0x7f1102ce

    goto :goto_0

    .line 201
    :pswitch_12
    const v0, 0x7f1102b9

    goto :goto_0

    .line 202
    :pswitch_13
    const v0, 0x7f1102c4

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 292
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    move v0, v5

    .line 313
    :cond_0
    :goto_0
    return v0

    .line 294
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    new-array v6, v0, [I

    move v0, v1

    move v2, v1

    .line 296
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 297
    invoke-static {p1, v0}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v3

    .line 298
    aput v3, v6, v2

    .line 299
    add-int/lit8 v2, v2, 0x1

    .line 300
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 301
    goto :goto_1

    :cond_2
    move v0, v1

    .line 302
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_4

    move v3, v0

    move v4, v1

    .line 304
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v3, v7, :cond_3

    if-ge v4, v2, :cond_3

    .line 305
    invoke-virtual {p0, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v7

    .line 306
    aget v8, v6, v4

    .line 307
    if-ne v7, v8, :cond_3

    .line 308
    invoke-static {v7}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    add-int/2addr v3, v7

    .line 309
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 310
    :cond_3
    if-eq v4, v2, :cond_0

    .line 312
    invoke-static {p0, v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v5

    .line 313
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 351
    .line 352
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "third_party_license_metadata"

    const-wide/16 v2, 0x0

    const/4 v4, -0x1

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 354
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 355
    new-instance v6, Ljava/util/ArrayList;

    array-length v0, v5

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 356
    array-length v7, v5

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_2

    aget-object v8, v5, v4

    .line 357
    const/16 v0, 0x20

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 358
    invoke-virtual {v8, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 359
    if-lez v9, :cond_0

    array-length v0, v10

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_1
    const-string v11, "Invalid license meta-data line:\n"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v11, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    new-array v11, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v11}, Lbdf;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 360
    aget-object v0, v10, v2

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 361
    aget-object v0, v10, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 362
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v8, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 363
    new-instance v8, Lami;

    invoke-direct {v8, v3, v12, v13, v0}, Lami;-><init>(Ljava/lang/String;JI)V

    .line 364
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 359
    goto :goto_1

    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 366
    :cond_2
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 367
    return-object v6
.end method

.method public static c()V
    .locals 1

    .prologue
    .line 404
    invoke-static {}, Lbdj;->b()Lbdi;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->c:Lbdi;

    .line 405
    return-void
.end method

.method public static c(J)Z
    .locals 2

    .prologue
    .line 100
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    invoke-static {p0, p1}, Landroid/provider/ContactsContract$Directory;->isEnterpriseDirectoryId(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Laqv;
    .locals 2

    .prologue
    .line 395
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 396
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    if-eqz v0, :cond_0

    .line 397
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    .line 403
    :goto_0
    return-object v0

    .line 398
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 399
    instance-of v1, v0, Laqw;

    if-eqz v1, :cond_1

    .line 400
    check-cast v0, Laqw;

    invoke-interface {v0}, Laqw;->b()Laqv;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    .line 401
    :cond_1
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    if-nez v0, :cond_2

    .line 402
    new-instance v0, Laqx;

    invoke-direct {v0}, Laqx;-><init>()V

    sput-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    .line 403
    :cond_2
    sget-object v0, Landroid/support/v7/widget/ActionMenuView$b;->b:Laqv;

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 427
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.voicemail.VoicemailClient.ACTION_UPLOAD"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 428
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 430
    return-void
.end method

.method public static synthetic f(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 431
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->e(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Lxf;Z)V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public final a(Lxf;)Z
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    return v0
.end method
