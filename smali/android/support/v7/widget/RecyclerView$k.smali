.class public final Landroid/support/v7/widget/RecyclerView$k;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "k"
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public b:Ljava/util/ArrayList;

.field public final c:Ljava/util/ArrayList;

.field public final d:Ljava/util/List;

.field public final synthetic e:Landroid/support/v7/widget/RecyclerView;

.field private f:I

.field private g:I

.field private h:Landroid/support/v7/widget/RecyclerView$j;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    .line 5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    .line 6
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->d:Ljava/util/List;

    .line 7
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$k;->f:I

    .line 8
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$k;->g:I

    return-void
.end method

.method private a(IZ)Landroid/support/v7/widget/RecyclerView$r;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 369
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 370
    :goto_0
    if-ge v3, v4, :cond_3

    .line 371
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 372
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 373
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v5, v5, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    if-nez v5, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v5

    if-nez v5, :cond_2

    .line 374
    :cond_0
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 416
    :cond_1
    :goto_1
    return-object v0

    .line 376
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 377
    :cond_3
    if-nez p2, :cond_9

    .line 378
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 379
    iget-object v0, v4, Laak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    .line 380
    :goto_2
    if-ge v3, v5, :cond_5

    .line 381
    iget-object v0, v4, Laak;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 382
    iget-object v6, v4, Laak;->a:Laam;

    invoke-virtual {v6, v0}, Laam;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v6

    .line 383
    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v7

    if-ne v7, p1, :cond_4

    .line 384
    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v7

    if-nez v7, :cond_4

    .line 385
    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v6

    if-nez v6, :cond_4

    move-object v3, v0

    .line 390
    :goto_3
    if-eqz v3, :cond_9

    .line 391
    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 392
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 393
    iget-object v2, v1, Laak;->a:Laam;

    invoke-virtual {v2, v3}, Laam;->a(Landroid/view/View;)I

    move-result v2

    .line 394
    if-gez v2, :cond_6

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "view is not a child, cannot hide "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move-object v3, v1

    .line 388
    goto :goto_3

    .line 396
    :cond_6
    iget-object v4, v1, Laak;->b:Laal;

    invoke-virtual {v4, v2}, Laal;->c(I)Z

    move-result v4

    if-nez v4, :cond_7

    .line 397
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "trying to unhide a view that was not hidden"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 398
    :cond_7
    iget-object v4, v1, Laak;->b:Laal;

    invoke-virtual {v4, v2}, Laal;->b(I)V

    .line 399
    invoke-virtual {v1, v3}, Laak;->b(Landroid/view/View;)Z

    .line 400
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1, v3}, Laak;->c(Landroid/view/View;)I

    move-result v1

    .line 401
    const/4 v2, -0x1

    if-ne v1, v2, :cond_8

    .line 402
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "layout index should not be -1 after unhiding a view:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 403
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 404
    :cond_8
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v2, v1}, Laak;->d(I)V

    .line 405
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView$k;->c(Landroid/view/View;)V

    .line 406
    const/16 v1, 0x2020

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    goto/16 :goto_1

    .line 408
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 409
    :goto_4
    if-ge v2, v3, :cond_b

    .line 410
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 411
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v4

    if-ne v4, p1, :cond_a

    .line 412
    if-nez p2, :cond_1

    .line 413
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_1

    .line 415
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_b
    move-object v0, v1

    .line 416
    goto/16 :goto_1
.end method

.method private a(JIZ)Landroid/support/v7/widget/RecyclerView$r;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 417
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 418
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 419
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 421
    iget-wide v4, v0, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 422
    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v3

    if-nez v3, :cond_2

    .line 424
    iget v3, v0, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 425
    if-ne p3, v3, :cond_1

    .line 426
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 427
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 429
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 430
    if-nez v1, :cond_0

    .line 431
    const/4 v1, 0x2

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView$r;->a(II)V

    .line 454
    :cond_0
    :goto_1
    return-object v0

    .line 433
    :cond_1
    if-nez p4, :cond_2

    .line 434
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 435
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 436
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$k;->b(Landroid/view/View;)V

    .line 437
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 438
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 439
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_6

    .line 440
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 442
    iget-wide v4, v0, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 443
    cmp-long v3, v4, p1

    if-nez v3, :cond_5

    .line 445
    iget v3, v0, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 446
    if-ne p3, v3, :cond_4

    .line 447
    if-nez p4, :cond_0

    .line 448
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 450
    :cond_4
    if-nez p4, :cond_5

    .line 451
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$k;->a(I)V

    move-object v0, v1

    .line 452
    goto :goto_1

    .line 453
    :cond_5
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_6
    move-object v0, v1

    .line 454
    goto :goto_1
.end method

.method private final a(Landroid/view/ViewGroup;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 200
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 201
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 202
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 203
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/view/ViewGroup;Z)V

    .line 204
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 205
    :cond_1
    if-nez p2, :cond_2

    .line 213
    :goto_1
    return-void

    .line 207
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 208
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 209
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 210
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    .line 211
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 212
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private b(I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x20

    const/4 v2, 0x0

    .line 346
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v0, v1

    .line 368
    :goto_0
    return-object v0

    :cond_1
    move v3, v2

    .line 348
    :goto_1
    if-ge v3, v4, :cond_3

    .line 349
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 350
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 351
    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    goto :goto_0

    .line 353
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 354
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 355
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 356
    if-eqz v0, :cond_5

    .line 357
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0, p1}, Lack;->c(I)I

    move-result v0

    .line 358
    if-lez v0, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 359
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView$a;->b(I)J

    move-result-wide v6

    .line 360
    :goto_2
    if-ge v2, v4, :cond_5

    .line 361
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 362
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v3

    if-nez v3, :cond_4

    .line 363
    iget-wide v8, v0, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 364
    cmp-long v3, v8, v6

    if-nez v3, :cond_4

    .line 365
    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    goto :goto_0

    .line 367
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 368
    goto :goto_0
.end method


# virtual methods
.method public final a(IZJ)Landroid/support/v7/widget/RecyclerView$r;
    .locals 11

    .prologue
    .line 19
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Item count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 21
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 22
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    const/4 v2, 0x0

    .line 25
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 26
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 27
    if-eqz v1, :cond_2

    .line 28
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$k;->b(I)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 29
    if-eqz v2, :cond_7

    const/4 v0, 0x1

    .line 30
    :cond_2
    :goto_0
    if-nez v2, :cond_5

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$k;->a(IZ)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 32
    if-eqz v2, :cond_5

    .line 34
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 35
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 36
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 56
    :goto_1
    if-nez v1, :cond_f

    .line 57
    if-nez p2, :cond_4

    .line 58
    const/4 v1, 0x4

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 59
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 60
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 61
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->f()V

    .line 64
    :cond_3
    :goto_2
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 65
    :cond_4
    const/4 v2, 0x0

    .line 67
    :cond_5
    :goto_3
    if-nez v2, :cond_28

    .line 68
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v1, p1}, Lack;->c(I)I

    move-result v1

    .line 69
    if-ltz v1, :cond_6

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v3

    if-lt v1, v3, :cond_10

    .line 70
    :cond_6
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inconsistency detected. Invalid item position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(offset:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 71
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 38
    :cond_8
    iget v1, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    if-ltz v1, :cond_9

    iget v1, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v3

    if-lt v1, v3, :cond_a

    .line 39
    :cond_9
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Inconsistency detected. Invalid view holder adapter position"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 40
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_a
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 42
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 43
    if-nez v1, :cond_b

    .line 44
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    iget v3, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView$a;->a(I)I

    move-result v1

    .line 46
    iget v3, v2, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 47
    if-eq v1, v3, :cond_b

    .line 48
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 49
    :cond_b
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 50
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 51
    if-eqz v1, :cond_d

    .line 53
    iget-wide v4, v2, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 54
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    iget v3, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView$a;->b(I)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_c

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 55
    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 62
    :cond_e
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 63
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->h()V

    goto/16 :goto_2

    .line 66
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 72
    :cond_10
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/RecyclerView$a;->a(I)I

    move-result v3

    .line 73
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 74
    iget-boolean v4, v4, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 75
    if-eqz v4, :cond_27

    .line 76
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView$a;->b(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v3, p2}, Landroid/support/v7/widget/RecyclerView$k;->a(JIZ)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_27

    .line 78
    iput v1, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    .line 79
    const/4 v0, 0x1

    move v1, v0

    .line 80
    :goto_4
    if-nez v2, :cond_11

    .line 81
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$k;->d()Landroid/support/v7/widget/RecyclerView$j;

    move-result-object v0

    .line 82
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$j;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    .line 83
    if-eqz v0, :cond_13

    iget-object v2, v0, Lacv;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_13

    .line 84
    iget-object v0, v0, Lacv;->a:Ljava/util/ArrayList;

    .line 85
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    move-object v2, v0

    .line 88
    :goto_5
    if-eqz v2, :cond_11

    .line 89
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->q()V

    .line 90
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->a:Z

    if-eqz v0, :cond_11

    .line 92
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_11

    .line 93
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/view/ViewGroup;Z)V

    .line 94
    :cond_11
    if-nez v2, :cond_17

    .line 95
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->q()J

    move-result-wide v4

    .line 96
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v0, p3, v6

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    .line 98
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView$j;->a(I)Lacv;

    move-result-object v0

    iget-wide v6, v0, Lacv;->c:J

    .line 99
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_12

    add-long/2addr v6, v4

    cmp-long v0, v6, p3

    if-gez v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    .line 100
    :goto_6
    if-nez v0, :cond_15

    .line 101
    const/4 v0, 0x0

    .line 199
    :goto_7
    return-object v0

    .line 86
    :cond_13
    const/4 v2, 0x0

    goto :goto_5

    .line 99
    :cond_14
    const/4 v0, 0x0

    goto :goto_6

    .line 102
    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 103
    const-string v6, "RV CreateView"

    invoke-static {v6}, Lbw;->e(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/RecyclerView$a;->a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 105
    iput v3, v2, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 106
    invoke-static {}, Lbw;->e()V

    .line 109
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    .line 110
    if-eqz v0, :cond_16

    .line 111
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_16

    .line 113
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v2, Landroid/support/v7/widget/RecyclerView$r;->b:Ljava/lang/ref/WeakReference;

    .line 114
    :cond_16
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->q()J

    move-result-wide v6

    .line 115
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    sub-long v4, v6, v4

    .line 116
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView$j;->a(I)Lacv;

    move-result-object v0

    .line 117
    iget-wide v6, v0, Lacv;->c:J

    invoke-static {v6, v7, v4, v5}, Landroid/support/v7/widget/RecyclerView$j;->a(JJ)J

    move-result-wide v4

    iput-wide v4, v0, Lacv;->c:J

    :cond_17
    move v3, v1

    .line 118
    :goto_8
    if-eqz v3, :cond_18

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 119
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 120
    if-nez v0, :cond_18

    const/16 v0, 0x2000

    .line 121
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 122
    const/4 v0, 0x0

    const/16 v1, 0x2000

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(II)V

    .line 123
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v0, :cond_18

    .line 125
    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView$d;->e(Landroid/support/v7/widget/RecyclerView$r;)I

    .line 126
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 127
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->p()Ljava/util/List;

    .line 128
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView$d;->d(Landroid/support/v7/widget/RecyclerView$r;)Lacs;

    move-result-object v0

    .line 129
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 130
    :cond_18
    const/4 v0, 0x0

    .line 131
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 132
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 133
    if-eqz v1, :cond_19

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->l()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 134
    iput p1, v2, Landroid/support/v7/widget/RecyclerView$r;->g:I

    move v1, v0

    .line 189
    :goto_9
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 190
    if-nez v0, :cond_23

    .line 191
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 192
    iget-object v4, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    :goto_a
    iput-object v2, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    .line 198
    if-eqz v3, :cond_25

    if-eqz v1, :cond_25

    const/4 v1, 0x1

    :goto_b
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$g;->d:Z

    move-object v0, v2

    .line 199
    goto/16 :goto_7

    .line 135
    :cond_19
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->l()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->k()Z

    move-result v1

    if-nez v1, :cond_1a

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 136
    :cond_1a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0, p1}, Lack;->c(I)I

    move-result v1

    .line 138
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iput-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->o:Landroid/support/v7/widget/RecyclerView;

    .line 140
    iget v0, v2, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 142
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->q()J

    move-result-wide v4

    .line 143
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v6, p3, v6

    if-eqz v6, :cond_1d

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    .line 145
    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView$j;->a(I)Lacv;

    move-result-object v0

    iget-wide v6, v0, Lacv;->d:J

    .line 146
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1b

    add-long/2addr v6, v4

    cmp-long v0, v6, p3

    if-gez v0, :cond_1c

    :cond_1b
    const/4 v0, 0x1

    .line 147
    :goto_c
    if-nez v0, :cond_1d

    .line 148
    const/4 v0, 0x0

    :goto_d
    move v1, v0

    .line 188
    goto :goto_9

    .line 146
    :cond_1c
    const/4 v0, 0x0

    goto :goto_c

    .line 149
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 150
    iput v1, v2, Landroid/support/v7/widget/RecyclerView$r;->c:I

    .line 152
    iget-boolean v6, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 153
    if-eqz v6, :cond_1e

    .line 154
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$a;->b(I)J

    move-result-wide v6

    iput-wide v6, v2, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 155
    :cond_1e
    const/4 v6, 0x1

    const/16 v7, 0x207

    invoke-virtual {v2, v6, v7}, Landroid/support/v7/widget/RecyclerView$r;->a(II)V

    .line 156
    const-string v6, "RV OnBindView"

    invoke-static {v6}, Lbw;->e(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->p()Ljava/util/List;

    .line 158
    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/RecyclerView$a;->a(Landroid/support/v7/widget/RecyclerView$r;I)V

    .line 159
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->o()V

    .line 160
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 161
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView$g;

    if-eqz v1, :cond_1f

    .line 162
    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    .line 163
    :cond_1f
    invoke-static {}, Lbw;->e()V

    .line 164
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->q()J

    move-result-wide v0

    .line 165
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    .line 166
    iget v7, v2, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 167
    sub-long/2addr v0, v4

    .line 168
    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView$j;->a(I)Lacv;

    move-result-object v4

    .line 169
    iget-wide v6, v4, Lacv;->d:J

    invoke-static {v6, v7, v0, v1}, Landroid/support/v7/widget/RecyclerView$j;->a(JJ)J

    move-result-wide v0

    iput-wide v0, v4, Lacv;->d:J

    .line 171
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->j()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 172
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 174
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->d(Landroid/view/View;)I

    move-result v1

    .line 175
    if-nez v1, :cond_20

    .line 176
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lqy;->b(Landroid/view/View;I)V

    .line 177
    :cond_20
    invoke-static {v0}, Lqy;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 178
    const/16 v1, 0x4000

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 179
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->L:Lacy;

    .line 181
    iget-object v1, v1, Lacy;->e:Lqa;

    .line 182
    invoke-static {v0, v1}, Lqy;->a(Landroid/view/View;Lqa;)V

    .line 183
    :cond_21
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 184
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 185
    if-eqz v0, :cond_22

    .line 186
    iput p1, v2, Landroid/support/v7/widget/RecyclerView$r;->g:I

    .line 187
    :cond_22
    const/4 v0, 0x1

    goto/16 :goto_d

    .line 193
    :cond_23
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v4

    if-nez v4, :cond_24

    .line 194
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 195
    iget-object v4, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_a

    .line 196
    :cond_24
    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    goto/16 :goto_a

    .line 198
    :cond_25
    const/4 v1, 0x0

    goto/16 :goto_b

    :cond_26
    move v1, v0

    goto/16 :goto_9

    :cond_27
    move v1, v0

    goto/16 :goto_4

    :cond_28
    move v3, v0

    goto/16 :goto_8
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 10
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$k;->c()V

    .line 11
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 233
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;Z)V

    .line 234
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 235
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 236
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 237
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Scrapped or attached views may not be recycled. isScrap:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " isAttached:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 239
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    move v0, v2

    goto :goto_0

    .line 240
    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 241
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tmp detached view should be removed from RecyclerView before it can be recycled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 242
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 244
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 245
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_4
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$r;->j:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_7

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 250
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v0}, Lri;->b(Landroid/view/View;)Z

    move-result v0

    .line 251
    if-eqz v0, :cond_7

    move v3, v1

    .line 253
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_5

    if-eqz v3, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 256
    :cond_5
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->r()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 257
    iget v0, p0, Landroid/support/v7/widget/RecyclerView$k;->g:I

    if-lez v0, :cond_c

    const/16 v0, 0x20e

    .line 258
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 259
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 260
    iget v4, p0, Landroid/support/v7/widget/RecyclerView$k;->g:I

    if-lt v0, v4, :cond_6

    if-lez v0, :cond_6

    .line 261
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView$k;->a(I)V

    .line 262
    add-int/lit8 v0, v0, -0x1

    .line 264
    :cond_6
    sget-boolean v4, Landroid/support/v7/widget/RecyclerView;->d:Z

    .line 265
    if-eqz v4, :cond_9

    if-lez v0, :cond_9

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v7/widget/RecyclerView$f$a;

    iget v5, p1, Landroid/support/v7/widget/RecyclerView$r;->c:I

    .line 266
    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView$f$a;->a(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 267
    add-int/lit8 v0, v0, -0x1

    move v4, v0

    .line 268
    :goto_2
    if-ltz v4, :cond_8

    .line 269
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$r;->c:I

    .line 270
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v7/widget/RecyclerView$f$a;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/RecyclerView$f$a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 271
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    .line 272
    goto :goto_2

    :cond_7
    move v3, v2

    .line 251
    goto :goto_1

    .line 273
    :cond_8
    add-int/lit8 v0, v4, 0x1

    .line 274
    :cond_9
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    .line 276
    :goto_3
    if-nez v0, :cond_a

    .line 277
    invoke-virtual {p0, p1, v1}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;Z)V

    move v2, v1

    .line 279
    :cond_a
    :goto_4
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v1, p1}, Laef;->d(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 280
    if-nez v0, :cond_b

    if-nez v2, :cond_b

    if-eqz v3, :cond_b

    .line 281
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->o:Landroid/support/v7/widget/RecyclerView;

    .line 282
    :cond_b
    return-void

    :cond_c
    move v0, v2

    goto :goto_3

    :cond_d
    move v0, v2

    goto :goto_4
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x4000

    .line 283
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 284
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(II)V

    .line 286
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-static {v0, v2}, Lqy;->a(Landroid/view/View;Lqa;)V

    .line 287
    :cond_0
    if-eqz p2, :cond_2

    .line 289
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/support/v7/widget/RecyclerView$l;

    .line 290
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$a;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 292
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, p1}, Laef;->d(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 294
    :cond_2
    iput-object v2, p1, Landroid/support/v7/widget/RecyclerView$r;->o:Landroid/support/v7/widget/RecyclerView;

    .line 295
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$k;->d()Landroid/support/v7/widget/RecyclerView$j;

    move-result-object v0

    .line 297
    iget v1, p1, Landroid/support/v7/widget/RecyclerView$r;->f:I

    .line 299
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$j;->a(I)Lacv;

    move-result-object v2

    iget-object v2, v2, Lacv;->a:Ljava/util/ArrayList;

    .line 300
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$j;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    iget v0, v0, Lacv;->b:I

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 301
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->q()V

    .line 302
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    :cond_3
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 214
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 217
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 218
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->f()V

    .line 221
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 222
    return-void

    .line 219
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->h()V

    goto :goto_0
.end method

.method final b()V
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$f;->n:I

    .line 13
    :goto_0
    iget v1, p0, Landroid/support/v7/widget/RecyclerView$k;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$k;->g:I

    .line 14
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 15
    :goto_1
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView$k;->g:I

    if-le v1, v2, :cond_1

    .line 16
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$k;->a(I)V

    .line 17
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 12
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 18
    :cond_1
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 1

    .prologue
    .line 333
    .line 334
    iget-boolean v0, p1, Landroid/support/v7/widget/RecyclerView$r;->l:Z

    .line 335
    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 339
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->k:Landroid/support/v7/widget/RecyclerView$k;

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v7/widget/RecyclerView$r;->l:Z

    .line 344
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->h()V

    .line 345
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 304
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 306
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$r;->k:Landroid/support/v7/widget/RecyclerView$k;

    .line 309
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$r;->l:Z

    .line 311
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->h()V

    .line 312
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 313
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 224
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 225
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$k;->a(I)V

    .line 226
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 227
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 228
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    .line 229
    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v7/widget/RecyclerView$f$a;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f$a;->a()V

    .line 231
    :cond_1
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 314
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 315
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 317
    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 318
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->p()Ljava/util/List;

    move-result-object v4

    .line 319
    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/RecyclerView$d;->a(Landroid/support/v7/widget/RecyclerView$r;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 320
    :goto_0
    if-eqz v0, :cond_4

    .line 321
    :cond_1
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 322
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 323
    if-nez v0, :cond_3

    .line 324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    .line 325
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    .line 319
    goto :goto_0

    .line 326
    :cond_3
    invoke-virtual {v3, p0, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(Landroid/support/v7/widget/RecyclerView$k;Z)V

    .line 327
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    :goto_1
    return-void

    .line 328
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    .line 330
    :cond_5
    invoke-virtual {v3, p0, v2}, Landroid/support/v7/widget/RecyclerView$r;->a(Landroid/support/v7/widget/RecyclerView$k;Z)V

    .line 331
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method final d()Landroid/support/v7/widget/RecyclerView$j;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    if-nez v0, :cond_0

    .line 456
    new-instance v0, Landroid/support/v7/widget/RecyclerView$j;

    invoke-direct {v0}, Landroid/support/v7/widget/RecyclerView$j;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    .line 457
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$k;->h:Landroid/support/v7/widget/RecyclerView$j;

    return-object v0
.end method
