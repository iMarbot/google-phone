.class public Landroid/support/v7/widget/RecyclerView;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lqp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/RecyclerView$d;,
        Landroid/support/v7/widget/RecyclerView$p;,
        Landroid/support/v7/widget/RecyclerView$n;,
        Landroid/support/v7/widget/RecyclerView$b;,
        Landroid/support/v7/widget/RecyclerView$o;,
        Landroid/support/v7/widget/RecyclerView$c;,
        Landroid/support/v7/widget/RecyclerView$g;,
        Landroid/support/v7/widget/RecyclerView$r;,
        Landroid/support/v7/widget/RecyclerView$l;,
        Landroid/support/v7/widget/RecyclerView$i;,
        Landroid/support/v7/widget/RecyclerView$h;,
        Landroid/support/v7/widget/RecyclerView$e;,
        Landroid/support/v7/widget/RecyclerView$f;,
        Landroid/support/v7/widget/RecyclerView$a;,
        Landroid/support/v7/widget/RecyclerView$k;,
        Landroid/support/v7/widget/RecyclerView$j;,
        Landroid/support/v7/widget/RecyclerView$m;,
        Landroid/support/v7/widget/RecyclerView$q;
    }
.end annotation


# static fields
.field public static final N:Landroid/view/animation/Interpolator;

.field private static O:[I

.field private static P:[I

.field private static Q:Z

.field private static R:Z

.field private static S:[Ljava/lang/Class;

.field public static final a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z


# instance fields
.field public A:Landroid/widget/EdgeEffect;

.field public B:Landroid/widget/EdgeEffect;

.field public C:Landroid/support/v7/widget/RecyclerView$d;

.field public D:I

.field public final E:Landroid/support/v7/widget/RecyclerView$q;

.field public F:Labm;

.field public G:Landroid/support/v7/widget/RecyclerView$f$a;

.field public final H:Landroid/support/v7/widget/RecyclerView$p;

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Lacy;

.field public final M:[I

.field private T:Landroid/support/v7/widget/RecyclerView$m;

.field private U:Landroid/support/v7/widget/RecyclerView$n;

.field private V:Z

.field private W:Landroid/graphics/Rect;

.field private aA:Ljava/lang/Runnable;

.field private aB:Laeh;

.field private aa:Z

.field private ab:I

.field private ac:Z

.field private ad:I

.field private ae:Landroid/view/accessibility/AccessibilityManager;

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:Landroid/view/VelocityTracker;

.field private aj:I

.field private ak:I

.field private al:I

.field private am:I

.field private an:I

.field private ao:I

.field private ap:I

.field private aq:F

.field private ar:F

.field private as:Z

.field private at:Ljava/util/List;

.field private au:Lacr;

.field private av:[I

.field private aw:Lqq;

.field private ax:[I

.field private ay:[I

.field private az:Ljava/util/List;

.field public final e:Landroid/support/v7/widget/RecyclerView$k;

.field public f:Lack;

.field public g:Laak;

.field public final h:Laef;

.field public final i:Ljava/lang/Runnable;

.field public final j:Landroid/graphics/Rect;

.field public final k:Landroid/graphics/RectF;

.field public l:Landroid/support/v7/widget/RecyclerView$a;

.field public m:Landroid/support/v7/widget/RecyclerView$f;

.field public n:Landroid/support/v7/widget/RecyclerView$l;

.field public final o:Ljava/util/ArrayList;

.field public final p:Ljava/util/ArrayList;

.field public q:Landroid/support/v7/widget/RecyclerView$h;

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Landroid/widget/EdgeEffect;

.field public z:Landroid/widget/EdgeEffect;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1730
    new-array v0, v2, [I

    const v3, 0x1010436

    aput v3, v0, v1

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->O:[I

    .line 1731
    new-array v0, v2, [I

    const v3, 0x10100eb

    aput v3, v0, v1

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->P:[I

    .line 1732
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-eq v0, v3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-eq v0, v3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->a:Z

    .line 1733
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_2

    move v0, v2

    :goto_1
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->b:Z

    .line 1734
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_3

    move v0, v2

    :goto_2
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->c:Z

    .line 1735
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_4

    move v0, v2

    :goto_3
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    .line 1736
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v0, v4, :cond_5

    move v0, v2

    :goto_4
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->Q:Z

    .line 1737
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v0, v4, :cond_6

    move v0, v2

    :goto_5
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->R:Z

    .line 1738
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v0, v1

    const-class v1, Landroid/util/AttributeSet;

    aput-object v1, v0, v2

    const/4 v1, 0x2

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->S:[Ljava/lang/Class;

    .line 1739
    new-instance v0, Lacq;

    invoke-direct {v0}, Lacq;-><init>()V

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->N:Landroid/view/animation/Interpolator;

    return-void

    :cond_1
    move v0, v1

    .line 1732
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1733
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1734
    goto :goto_2

    :cond_4
    move v0, v1

    .line 1735
    goto :goto_3

    :cond_5
    move v0, v1

    .line 1736
    goto :goto_4

    :cond_6
    move v0, v1

    .line 1737
    goto :goto_5
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 16

    .prologue
    .line 5
    invoke-direct/range {p0 .. p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    new-instance v3, Landroid/support/v7/widget/RecyclerView$m;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/support/v7/widget/RecyclerView$m;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/RecyclerView$m;

    .line 7
    new-instance v3, Landroid/support/v7/widget/RecyclerView$k;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/support/v7/widget/RecyclerView$k;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 8
    new-instance v3, Laef;

    invoke-direct {v3}, Laef;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 9
    new-instance v3, Laco;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laco;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->i:Ljava/lang/Runnable;

    .line 10
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    .line 11
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    .line 12
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/graphics/RectF;

    .line 13
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    .line 14
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    .line 15
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->ab:I

    .line 16
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 17
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->af:I

    .line 18
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->ag:I

    .line 19
    new-instance v3, Laap;

    invoke-direct {v3}, Laap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 20
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->D:I

    .line 21
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 22
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->aq:F

    .line 23
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->ar:F

    .line 24
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->as:Z

    .line 25
    new-instance v3, Landroid/support/v7/widget/RecyclerView$q;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/support/v7/widget/RecyclerView$q;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v7/widget/RecyclerView$q;

    .line 26
    sget-boolean v3, Landroid/support/v7/widget/RecyclerView;->d:Z

    if-eqz v3, :cond_3

    new-instance v3, Landroid/support/v7/widget/RecyclerView$f$a;

    invoke-direct {v3}, Landroid/support/v7/widget/RecyclerView$f$a;-><init>()V

    :goto_0
    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v7/widget/RecyclerView$f$a;

    .line 27
    new-instance v3, Landroid/support/v7/widget/RecyclerView$p;

    invoke-direct {v3}, Landroid/support/v7/widget/RecyclerView$p;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 28
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->I:Z

    .line 29
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->J:Z

    .line 30
    new-instance v3, Lacr;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lacr;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->au:Lacr;

    .line 31
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->K:Z

    .line 32
    const/4 v3, 0x2

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->av:[I

    .line 33
    const/4 v3, 0x2

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    .line 34
    const/4 v3, 0x2

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->M:[I

    .line 35
    const/4 v3, 0x2

    new-array v3, v3, [I

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    .line 37
    new-instance v3, Lacp;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lacp;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->aA:Ljava/lang/Runnable;

    .line 38
    new-instance v3, Laeh;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laeh;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->aB:Laeh;

    .line 39
    if-eqz p2, :cond_4

    .line 40
    sget-object v3, Landroid/support/v7/widget/RecyclerView;->P:[I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 41
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView;->V:Z

    .line 42
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    :goto_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setScrollContainer(Z)V

    .line 46
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setFocusableInTouchMode(Z)V

    .line 47
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    .line 48
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/RecyclerView;->an:I

    .line 50
    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lrj;->a(Landroid/view/ViewConfiguration;Landroid/content/Context;)F

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/RecyclerView;->aq:F

    .line 52
    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lrj;->b(Landroid/view/ViewConfiguration;Landroid/content/Context;)F

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/RecyclerView;->ar:F

    .line 53
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Landroid/support/v7/widget/RecyclerView;->ao:I

    .line 54
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v7/widget/RecyclerView;->ap:I

    .line 55
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 56
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->au:Lacr;

    .line 57
    iput-object v4, v3, Landroid/support/v7/widget/RecyclerView$d;->h:Lacr;

    .line 59
    new-instance v3, Lack;

    new-instance v4, Lyv;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lyv;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v3, v4}, Lack;-><init>(Lyv;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    .line 61
    new-instance v3, Laak;

    new-instance v4, Laam;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Laam;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v3, v4}, Laak;-><init>(Laam;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 63
    sget-object v3, Lqy;->a:Lri;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lri;->d(Landroid/view/View;)I

    move-result v3

    .line 64
    if-nez v3, :cond_0

    .line 65
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lqy;->b(Landroid/view/View;I)V

    .line 66
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "accessibility"

    .line 67
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->ae:Landroid/view/accessibility/AccessibilityManager;

    .line 68
    new-instance v3, Lacy;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lacy;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    .line 69
    move-object/from16 v0, p0

    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView;->L:Lacy;

    .line 70
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView;->L:Lacy;

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lqy;->a(Landroid/view/View;Lqa;)V

    .line 71
    const/4 v12, 0x1

    .line 72
    if-eqz p2, :cond_c

    .line 73
    sget-object v3, Lwd;->a:[I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v13

    .line 74
    sget v3, Lwd;->h:I

    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 75
    sget v3, Lwd;->b:I

    const/4 v4, -0x1

    invoke-virtual {v13, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 76
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 77
    const/high16 v3, 0x40000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setDescendantFocusability(I)V

    .line 78
    :cond_1
    sget v3, Lwd;->c:I

    const/4 v4, 0x0

    invoke-virtual {v13, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->aa:Z

    .line 79
    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->aa:Z

    if-eqz v3, :cond_7

    .line 80
    sget v3, Lwd;->f:I

    .line 81
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/StateListDrawable;

    .line 82
    sget v3, Lwd;->g:I

    .line 83
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 84
    sget v3, Lwd;->d:I

    .line 85
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    check-cast v7, Landroid/graphics/drawable/StateListDrawable;

    .line 86
    sget v3, Lwd;->e:I

    .line 87
    invoke-virtual {v13, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 89
    if-eqz v5, :cond_2

    if-eqz v6, :cond_2

    if-eqz v7, :cond_2

    if-nez v8, :cond_6

    .line 90
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying to set fast scroller without both required drawables."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 26
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 44
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView;->V:Z

    goto/16 :goto_1

    .line 55
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 92
    :cond_6
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 93
    new-instance v3, Labc;

    const v9, 0x7f0d013b

    .line 94
    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const v10, 0x7f0d013d

    .line 95
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const v11, 0x7f0d013c

    .line 96
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v11

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v11}, Labc;-><init>(Landroid/support/v7/widget/RecyclerView;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;III)V

    .line 97
    :cond_7
    invoke-virtual {v13}, Landroid/content/res/TypedArray;->recycle()V

    .line 99
    if-eqz v14, :cond_8

    .line 100
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 101
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 103
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-ne v4, v5, :cond_9

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 109
    :goto_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/RecyclerView;->isInEditMode()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 110
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 113
    :goto_4
    invoke-virtual {v3, v4}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-class v5, Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v3, v5}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v7

    .line 114
    const/4 v5, 0x0

    .line 115
    :try_start_1
    sget-object v3, Landroid/support/v7/widget/RecyclerView;->S:[Ljava/lang/Class;

    .line 116
    invoke-virtual {v7, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v6

    .line 117
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v3, v8

    const/4 v8, 0x1

    aput-object p2, v3, v8

    const/4 v8, 0x2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v3, v8

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v3, v8
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_6

    move-object v5, v6

    .line 125
    :goto_5
    const/4 v6, 0x1

    :try_start_2
    invoke-virtual {v5, v6}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 126
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView$f;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_6

    .line 138
    :cond_8
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_d

    .line 139
    sget-object v3, Landroid/support/v7/widget/RecyclerView;->O:[I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 140
    const/4 v3, 0x0

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 141
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 144
    :goto_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 145
    return-void

    .line 105
    :cond_9
    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object v4, v3

    .line 106
    goto :goto_3

    .line 107
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-class v5, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_3

    .line 111
    :cond_b
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_6

    move-result-object v3

    goto/16 :goto_4

    .line 119
    :catch_0
    move-exception v3

    .line 120
    const/4 v6, 0x0

    :try_start_4
    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v7, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_6

    move-result-object v3

    move-object v15, v5

    move-object v5, v3

    move-object v3, v15

    .line 121
    goto :goto_5

    .line 122
    :catch_1
    move-exception v5

    .line 123
    :try_start_5
    invoke-virtual {v5, v3}, Ljava/lang/NoSuchMethodException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 124
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Error creating LayoutManager "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_5} :catch_6

    .line 128
    :catch_2
    move-exception v3

    .line 129
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Unable to find LayoutManager "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 130
    :catch_3
    move-exception v3

    .line 131
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 132
    :catch_4
    move-exception v3

    .line 133
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 134
    :catch_5
    move-exception v3

    .line 135
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Cannot access non-public constructor "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 136
    :catch_6
    move-exception v3

    .line 137
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": Class is not a LayoutManager "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 143
    :cond_c
    const/high16 v3, 0x40000

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setDescendantFocusability(I)V

    :cond_d
    move v3, v12

    goto/16 :goto_6
.end method

.method private final A()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1205
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Landroid/support/v7/widget/RecyclerView$p;->l:J

    .line 1206
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->k:I

    .line 1207
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->m:I

    .line 1208
    return-void
.end method

.method private final B()Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1209
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->k:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->k:I

    .line 1210
    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$p;->a()I

    move-result v3

    move v2, v0

    .line 1211
    :goto_1
    if-ge v2, v3, :cond_2

    .line 1212
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->d(I)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v4

    .line 1213
    if-eqz v4, :cond_2

    .line 1214
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1215
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1225
    :goto_2
    return-object v0

    .line 1209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1216
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1217
    :cond_2
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1218
    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-ltz v0, :cond_5

    .line 1219
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->d(I)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 1220
    if-nez v2, :cond_3

    move-object v0, v1

    .line 1221
    goto :goto_2

    .line 1222
    :cond_3
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->hasFocusable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1223
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    goto :goto_2

    .line 1224
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 1225
    goto :goto_2
.end method

.method private final C()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1235
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView$p;->a(I)V

    .line 1236
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$p;)V

    .line 1237
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1238
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v1}, Laef;->a()V

    .line 1239
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 1240
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->y()V

    .line 1243
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->as:Z

    if-eqz v1, :cond_14

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_14

    .line 1244
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 1245
    :goto_0
    if-nez v1, :cond_2

    move-object v3, v0

    .line 1249
    :goto_1
    if-nez v3, :cond_4

    .line 1250
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->A()V

    .line 1269
    :goto_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_9

    move v0, v4

    :goto_3
    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView$p;->h:Z

    .line 1270
    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->I:Z

    .line 1271
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 1272
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->e:I

    .line 1273
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a([I)V

    .line 1274
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v0, :cond_a

    .line 1275
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v1

    move v0, v5

    .line 1276
    :goto_4
    if-ge v0, v1, :cond_a

    .line 1277
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v3, v0}, Laak;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1278
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1279
    iget-boolean v6, v6, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1280
    if-eqz v6, :cond_1

    .line 1281
    :cond_0
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 1282
    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView$d;->e(Landroid/support/v7/widget/RecyclerView$r;)I

    .line 1283
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->p()Ljava/util/List;

    .line 1285
    new-instance v6, Lacs;

    invoke-direct {v6}, Lacs;-><init>()V

    .line 1288
    iget-object v7, v3, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1289
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    iput v8, v6, Lacs;->a:I

    .line 1290
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    iput v8, v6, Lacs;->b:I

    .line 1291
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    .line 1292
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    .line 1295
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v7, v3, v6}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 1296
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v6, v6, Landroid/support/v7/widget/RecyclerView$p;->h:Z

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1297
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1298
    invoke-direct {p0, v3}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/support/v7/widget/RecyclerView$r;)J

    move-result-wide v6

    .line 1299
    iget-object v8, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v8, v6, v7, v3}, Laef;->a(JLandroid/support/v7/widget/RecyclerView$r;)V

    .line 1300
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1246
    :cond_2
    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1247
    if-nez v1, :cond_3

    move-object v3, v0

    goto/16 :goto_1

    :cond_3
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_1

    .line 1251
    :cond_4
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1252
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1253
    if-eqz v0, :cond_5

    .line 1254
    iget-wide v0, v3, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 1255
    :goto_5
    iput-wide v0, v6, Landroid/support/v7/widget/RecyclerView$p;->l:J

    .line 1256
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v0, :cond_6

    move v0, v2

    .line 1258
    :goto_6
    iput v0, v1, Landroid/support/v7/widget/RecyclerView$p;->k:I

    .line 1259
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v1, v3, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1260
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    move v9, v0

    move-object v0, v1

    move v1, v9

    .line 1261
    :goto_7
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v3

    if-nez v3, :cond_8

    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1262
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    .line 1263
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    .line 1264
    if-eq v0, v2, :cond_13

    .line 1265
    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    :goto_8
    move v1, v0

    move-object v0, v3

    .line 1266
    goto :goto_7

    .line 1255
    :cond_5
    const-wide/16 v0, -0x1

    goto :goto_5

    .line 1257
    :cond_6
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, v3, Landroid/support/v7/widget/RecyclerView$r;->d:I

    goto :goto_6

    .line 1258
    :cond_7
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->d()I

    move-result v0

    goto :goto_6

    .line 1268
    :cond_8
    iput v1, v6, Landroid/support/v7/widget/RecyclerView$p;->m:I

    goto/16 :goto_2

    :cond_9
    move v0, v5

    .line 1269
    goto/16 :goto_3

    .line 1301
    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    if-eqz v0, :cond_12

    .line 1303
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->b()I

    move-result v1

    move v0, v5

    .line 1304
    :goto_9
    if-ge v0, v1, :cond_c

    .line 1305
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v3, v0}, Laak;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1306
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v6

    if-nez v6, :cond_b

    .line 1308
    iget v6, v3, Landroid/support/v7/widget/RecyclerView$r;->d:I

    if-ne v6, v2, :cond_b

    .line 1309
    iget v6, v3, Landroid/support/v7/widget/RecyclerView$r;->c:I

    iput v6, v3, Landroid/support/v7/widget/RecyclerView$r;->d:I

    .line 1310
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1311
    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    .line 1312
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v5, v1, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    .line 1313
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)V

    .line 1314
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    move v1, v5

    .line 1315
    :goto_a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 1316
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v1}, Laak;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1317
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 1318
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1319
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 1320
    iget-object v0, v0, Laef;->a:Lpd;

    invoke-virtual {v0, v2}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeg;

    .line 1321
    if-eqz v0, :cond_e

    iget v0, v0, Laeg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_e

    move v0, v4

    .line 1322
    :goto_b
    if-nez v0, :cond_d

    .line 1323
    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView$d;->e(Landroid/support/v7/widget/RecyclerView$r;)I

    .line 1324
    const/16 v0, 0x2000

    .line 1325
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    .line 1326
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 1327
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->p()Ljava/util/List;

    .line 1329
    new-instance v3, Lacs;

    invoke-direct {v3}, Lacs;-><init>()V

    .line 1332
    iget-object v6, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1333
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v7

    iput v7, v3, Lacs;->a:I

    .line 1334
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v7

    iput v7, v3, Lacs;->b:I

    .line 1335
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    .line 1336
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    .line 1339
    if-eqz v0, :cond_f

    .line 1340
    invoke-virtual {p0, v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 1348
    :cond_d
    :goto_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    :cond_e
    move v0, v5

    .line 1321
    goto :goto_b

    .line 1341
    :cond_f
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 1342
    iget-object v0, v6, Laef;->a:Lpd;

    invoke-virtual {v0, v2}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeg;

    .line 1343
    if-nez v0, :cond_10

    .line 1344
    invoke-static {}, Laeg;->a()Laeg;

    move-result-object v0

    .line 1345
    iget-object v6, v6, Laef;->a:Lpd;

    invoke-virtual {v6, v2, v0}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1346
    :cond_10
    iget v2, v0, Laeg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Laeg;->a:I

    .line 1347
    iput-object v3, v0, Laeg;->b:Lacs;

    goto :goto_c

    .line 1349
    :cond_11
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    .line 1353
    :goto_d
    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 1354
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1355
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x2

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->d:I

    .line 1356
    return-void

    .line 1351
    :cond_12
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->E()V

    goto :goto_d

    :cond_13
    move v0, v1

    goto/16 :goto_8

    :cond_14
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private final D()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1357
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1358
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 1359
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView$p;->a(I)V

    .line 1360
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->e()V

    .line 1361
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v3

    iput v3, v0, Landroid/support/v7/widget/RecyclerView$p;->e:I

    .line 1362
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput v2, v0, Landroid/support/v7/widget/RecyclerView$p;->c:I

    .line 1363
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 1364
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)V

    .line 1365
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    .line 1366
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    .line 1367
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    .line 1368
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v3, 0x4

    iput v3, v0, Landroid/support/v7/widget/RecyclerView$p;->d:I

    .line 1370
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 1371
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1372
    return-void

    :cond_0
    move v0, v2

    .line 1367
    goto :goto_0
.end method

.method private E()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1506
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->b()I

    move-result v2

    move v0, v1

    .line 1507
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1508
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v3, v0}, Laak;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1509
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1510
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->a()V

    .line 1511
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1512
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 1513
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 1514
    :goto_1
    if-ge v2, v4, :cond_2

    .line 1515
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1516
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->a()V

    .line 1517
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1518
    :cond_2
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 1519
    :goto_2
    if-ge v2, v4, :cond_3

    .line 1520
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->a()V

    .line 1521
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1522
    :cond_3
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1523
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1524
    :goto_3
    if-ge v1, v2, :cond_4

    .line 1525
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->a()V

    .line 1526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1527
    :cond_4
    return-void
.end method

.method private a(J)Landroid/support/v7/widget/RecyclerView$r;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1607
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1608
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1609
    if-nez v1, :cond_1

    .line 1622
    :cond_0
    :goto_0
    return-object v0

    .line 1611
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1}, Laak;->b()I

    move-result v3

    .line 1613
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 1614
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v2}, Laak;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1615
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1616
    iget-wide v4, v0, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 1617
    cmp-long v4, v4, p1

    if-nez v4, :cond_3

    .line 1618
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Laak;->d(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1621
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1622
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public static synthetic a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 1727
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->detachViewFromParent(I)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 1729
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void
.end method

.method public static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 1726
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private final a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 897
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 898
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    if-ne v1, v2, :cond_0

    .line 899
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 900
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 901
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    .line 902
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    .line 903
    :cond_0
    return-void

    .line 899
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 1623
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 1624
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 1625
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/support/v7/widget/RecyclerView$g;->leftMargin:I

    sub-int/2addr v2, v3

    .line 1626
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$g;->topMargin:I

    sub-int/2addr v3, v4

    .line 1627
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$g;->rightMargin:I

    add-int/2addr v4, v5

    .line 1628
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$g;->bottomMargin:I

    add-int/2addr v0, v1

    .line 1629
    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1630
    return-void
.end method

.method private final a(Landroid/view/View;Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 618
    if-eqz p2, :cond_2

    move-object v0, p2

    .line 619
    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 620
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 621
    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView$g;

    if-eqz v2, :cond_0

    .line 622
    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 623
    iget-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    if-nez v2, :cond_0

    .line 624
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 625
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 626
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 627
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 628
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 629
    :cond_0
    if-eqz p2, :cond_1

    .line 630
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 631
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 632
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-nez v2, :cond_3

    move v4, v5

    :goto_1
    if-nez p2, :cond_4

    :goto_2
    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z

    .line 633
    return-void

    :cond_2
    move-object v0, p1

    .line 618
    goto :goto_0

    :cond_3
    move v4, v1

    .line 632
    goto :goto_1

    :cond_4
    move v5, v1

    goto :goto_2
.end method

.method private final a([I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 1380
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v5

    .line 1381
    if-nez v5, :cond_0

    .line 1382
    aput v1, p1, v4

    .line 1383
    aput v1, p1, v7

    .line 1398
    :goto_0
    return-void

    .line 1385
    :cond_0
    const v2, 0x7fffffff

    .line 1386
    const/high16 v1, -0x80000000

    move v3, v4

    .line 1387
    :goto_1
    if-ge v3, v5, :cond_2

    .line 1388
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v3}, Laak;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1389
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1390
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v0

    .line 1391
    if-ge v0, v2, :cond_1

    move v2, v0

    .line 1393
    :cond_1
    if-le v0, v1, :cond_3

    move v1, v2

    .line 1395
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1396
    :cond_2
    aput v2, p1, v4

    .line 1397
    aput v1, p1, v7

    goto :goto_0

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private a(IILandroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    .line 343
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 344
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 345
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 346
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v4, :cond_2

    .line 347
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 348
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 349
    const-string v4, "RV Scroll"

    invoke-static {v4}, Lbw;->e(Ljava/lang/String;)V

    .line 350
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$p;)V

    .line 351
    if-eqz p1, :cond_0

    .line 352
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v2, p1, v3, v4}, Landroid/support/v7/widget/RecyclerView$f;->a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v2

    .line 353
    sub-int v3, p1, v2

    .line 354
    :cond_0
    if-eqz p2, :cond_1

    .line 355
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, p2, v1, v4}, Landroid/support/v7/widget/RecyclerView$f;->b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    .line 356
    sub-int v1, p2, v0

    .line 357
    :cond_1
    invoke-static {}, Lbw;->e()V

    .line 358
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->p()V

    .line 360
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 361
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    :cond_2
    move v4, v1

    move v1, v2

    move v2, v0

    .line 362
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 363
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 364
    :cond_3
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/support/v7/widget/RecyclerView;->a(IIII[II)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 365
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    .line 366
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    .line 367
    if-eqz p3, :cond_4

    .line 368
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v3, 0x0

    aget v0, v0, v3

    int-to-float v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-virtual {p3, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 369
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    const/4 v3, 0x0

    aget v4, v0, v3

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    add-int/2addr v4, v5

    aput v4, v0, v3

    .line 370
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    const/4 v3, 0x1

    aget v4, v0, v3

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    aput v4, v0, v3

    .line 395
    :cond_5
    :goto_0
    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    .line 396
    :cond_6
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->c(II)V

    .line 397
    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_8

    .line 398
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 399
    :cond_8
    if-nez v1, :cond_9

    if-eqz v2, :cond_11

    :cond_9
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 371
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v0

    const/4 v5, 0x2

    if-eq v0, v5, :cond_5

    .line 372
    if-eqz p3, :cond_e

    const/16 v0, 0x2002

    invoke-static {p3, v0}, Lbw;->a(Landroid/view/MotionEvent;I)Z

    move-result v0

    if-nez v0, :cond_e

    .line 373
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    int-to-float v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    int-to-float v4, v4

    .line 374
    const/4 v0, 0x0

    .line 375
    const/4 v7, 0x0

    cmpg-float v7, v3, v7

    if-gez v7, :cond_f

    .line 376
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->e()V

    .line 377
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    neg-float v7, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v6, v9

    sub-float v6, v8, v6

    invoke-static {v0, v7, v6}, Lsw;->a(Landroid/widget/EdgeEffect;FF)V

    .line 378
    const/4 v0, 0x1

    .line 383
    :cond_b
    :goto_2
    const/4 v6, 0x0

    cmpg-float v6, v4, v6

    if-gez v6, :cond_10

    .line 384
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->g()V

    .line 385
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    neg-float v6, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v5, v7

    invoke-static {v0, v6, v5}, Lsw;->a(Landroid/widget/EdgeEffect;FF)V

    .line 386
    const/4 v0, 0x1

    .line 391
    :cond_c
    :goto_3
    if-nez v0, :cond_d

    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-nez v0, :cond_d

    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-eqz v0, :cond_e

    .line 393
    :cond_d
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->c(Landroid/view/View;)V

    .line 394
    :cond_e
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    goto :goto_0

    .line 379
    :cond_f
    const/4 v7, 0x0

    cmpl-float v7, v3, v7

    if-lez v7, :cond_b

    .line 380
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    .line 381
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v7, v3, v7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v6, v8

    invoke-static {v0, v7, v6}, Lsw;->a(Landroid/widget/EdgeEffect;FF)V

    .line 382
    const/4 v0, 0x1

    goto :goto_2

    .line 387
    :cond_10
    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_c

    .line 388
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->h()V

    .line 389
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float v6, v4, v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v5, v8

    sub-float v5, v7, v5

    invoke-static {v0, v6, v5}, Lsw;->a(Landroid/widget/EdgeEffect;FF)V

    .line 390
    const/4 v0, 0x1

    goto :goto_3

    .line 399
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 1728
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;
    .locals 1

    .prologue
    .line 1589
    if-nez p0, :cond_0

    .line 1590
    const/4 v0, 0x0

    .line 1591
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    goto :goto_0
.end method

.method static b(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1671
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$r;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1672
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$r;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1673
    :goto_0
    if-eqz v0, :cond_3

    .line 1674
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    if-ne v0, v2, :cond_1

    .line 1682
    :cond_0
    :goto_1
    return-void

    .line 1676
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1677
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1678
    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1680
    goto :goto_0

    .line 1681
    :cond_3
    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView$r;->b:Ljava/lang/ref/WeakReference;

    goto :goto_1
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1592
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1593
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private d(Landroid/support/v7/widget/RecyclerView$r;)J
    .locals 2

    .prologue
    .line 1410
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1411
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1412
    if-eqz v0, :cond_0

    .line 1413
    iget-wide v0, p1, Landroid/support/v7/widget/RecyclerView$r;->e:J

    .line 1414
    :goto_0
    return-wide v0

    :cond_0
    iget v0, p1, Landroid/support/v7/widget/RecyclerView$r;->c:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method static d(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1658
    instance-of v1, p0, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    move-object p0, v0

    .line 1670
    :goto_0
    return-object p0

    .line 1660
    :cond_0
    instance-of v1, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1

    .line 1661
    check-cast p0, Landroid/support/v7/widget/RecyclerView;

    goto :goto_0

    .line 1662
    :cond_1
    check-cast p0, Landroid/view/ViewGroup;

    .line 1663
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 1664
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    .line 1665
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1666
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    .line 1667
    if-eqz v1, :cond_2

    move-object p0, v1

    .line 1668
    goto :goto_0

    .line 1669
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move-object p0, v0

    .line 1670
    goto :goto_0
.end method

.method private d(II)Z
    .locals 11

    .prologue
    const v6, 0x7fffffff

    const/high16 v5, -0x80000000

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 433
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_1

    .line 434
    const-string v0, "RecyclerView"

    const-string v2, "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_0
    :goto_0
    return v1

    .line 436
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v2

    .line 439
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v3

    .line 440
    if-eqz v2, :cond_2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->ao:I

    if-ge v0, v4, :cond_3

    :cond_2
    move p1, v1

    .line 442
    :cond_3
    if-eqz v3, :cond_4

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->ao:I

    if-ge v0, v4, :cond_5

    :cond_4
    move p2, v1

    .line 444
    :cond_5
    if-nez p1, :cond_6

    if-eqz p2, :cond_0

    .line 446
    :cond_6
    int-to-float v0, p1

    int-to-float v4, p2

    invoke-virtual {p0, v0, v4}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedPreFling(FF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 447
    if-nez v2, :cond_7

    if-eqz v3, :cond_9

    :cond_7
    move v0, v9

    .line 448
    :goto_1
    int-to-float v4, p1

    int-to-float v7, p2

    invoke-virtual {p0, v4, v7, v0}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedFling(FFZ)Z

    .line 449
    if-eqz v0, :cond_0

    .line 451
    if-eqz v2, :cond_a

    move v0, v9

    .line 453
    :goto_2
    if-eqz v3, :cond_8

    .line 454
    or-int/lit8 v0, v0, 0x2

    .line 455
    :cond_8
    invoke-direct {p0, v0, v9}, Landroid/support/v7/widget/RecyclerView;->e(II)Z

    .line 456
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ap:I

    neg-int v0, v0

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->ap:I

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 457
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ap:I

    neg-int v0, v0

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->ap:I

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 458
    iget-object v10, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v7/widget/RecyclerView$q;

    .line 459
    iget-object v0, v10, Landroid/support/v7/widget/RecyclerView$q;->d:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 460
    iput v1, v10, Landroid/support/v7/widget/RecyclerView$q;->b:I

    iput v1, v10, Landroid/support/v7/widget/RecyclerView$q;->a:I

    .line 461
    iget-object v0, v10, Landroid/support/v7/widget/RecyclerView$q;->c:Landroid/widget/OverScroller;

    move v2, v1

    move v7, v5

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 462
    invoke-virtual {v10}, Landroid/support/v7/widget/RecyclerView$q;->a()V

    move v1, v9

    .line 463
    goto :goto_0

    :cond_9
    move v0, v1

    .line 447
    goto :goto_1

    :cond_a
    move v0, v1

    goto :goto_2
.end method

.method private e(II)Z
    .locals 1

    .prologue
    .line 1708
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lqq;->a(II)Z

    move-result v0

    return v0
.end method

.method private f(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1584
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, p1

    .line 1585
    :goto_0
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1586
    check-cast v0, Landroid/view/View;

    .line 1587
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_0

    .line 1588
    :cond_0
    if-ne v0, p0, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static q()J
    .locals 2

    .prologue
    .line 1683
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    if-eqz v0, :cond_0

    .line 1684
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 1685
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 466
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()V

    .line 467
    return-void
.end method

.method private final t()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v7/widget/RecyclerView$q;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$q;->b()V

    .line 469
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 471
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    if-eqz v1, :cond_0

    .line 472
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$o;->a()V

    .line 473
    :cond_0
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    .line 528
    return-void
.end method

.method private final v()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 873
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_0

    .line 874
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 875
    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    .line 878
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_1

    .line 879
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 880
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    .line 881
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_2

    .line 882
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 883
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 884
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_3

    .line 885
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 886
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 887
    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_4

    .line 888
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 889
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 890
    :cond_4
    if-eqz v0, :cond_5

    .line 892
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->c(Landroid/view/View;)V

    .line 893
    :cond_5
    return-void
.end method

.method private final w()V
    .locals 1

    .prologue
    .line 894
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    .line 895
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 896
    return-void
.end method

.method private final x()Z
    .locals 1

    .prologue
    .line 1042
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final y()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1043
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->a()V

    .line 1045
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->x()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1046
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->b()V

    .line 1048
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->I:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->J:Z

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 1049
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_6

    :cond_2
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1051
    iget-boolean v3, v3, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1052
    if-eqz v3, :cond_6

    :cond_3
    move v3, v2

    :goto_2
    iput-boolean v3, v4, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    .line 1053
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v4, v4, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v4, :cond_7

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_7

    .line 1054
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->x()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_3
    iput-boolean v2, v3, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    .line 1055
    return-void

    .line 1047
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->e()V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1048
    goto :goto_1

    :cond_6
    move v3, v1

    .line 1052
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1054
    goto :goto_3
.end method

.method private z()V
    .locals 10

    .prologue
    .line 1056
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-nez v0, :cond_0

    .line 1057
    const-string v0, "RecyclerView"

    const-string v1, "No adapter attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    :goto_0
    return-void

    .line 1059
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_1

    .line 1060
    const-string v0, "RecyclerView"

    const-string v1, "No layout manager attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1062
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1063
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->C()V

    .line 1064
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 1065
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    .line 1076
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$p;->a(I)V

    .line 1077
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1078
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 1079
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->d:I

    .line 1080
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    if-eqz v0, :cond_10

    .line 1081
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_f

    .line 1082
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v2}, Laak;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1083
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1084
    invoke-direct {p0, v3}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/support/v7/widget/RecyclerView$r;)J

    move-result-wide v4

    .line 1085
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 1087
    new-instance v1, Lacs;

    invoke-direct {v1}, Lacs;-><init>()V

    .line 1090
    iget-object v0, v3, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1091
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    iput v6, v1, Lacs;->a:I

    .line 1092
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    iput v6, v1, Lacs;->b:I

    .line 1093
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    .line 1094
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    .line 1097
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 1098
    iget-object v0, v0, Laef;->b:Lpj;

    invoke-virtual {v0, v4, v5}, Lpj;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1100
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v6

    if-nez v6, :cond_e

    .line 1101
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v6, v0}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;)Z

    move-result v6

    .line 1102
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v7, v3}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;)Z

    move-result v7

    .line 1103
    if-eqz v6, :cond_6

    if-ne v0, v3, :cond_6

    .line 1104
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, v3, v1}, Laef;->b(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 1149
    :cond_2
    :goto_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    .line 1066
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->f()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 1067
    iget v0, v0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 1068
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 1070
    iget v0, v0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 1071
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 1072
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 1073
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    goto/16 :goto_1

    .line 1074
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView;)V

    goto/16 :goto_1

    .line 1105
    :cond_6
    iget-object v8, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 1106
    const/4 v9, 0x4

    invoke-virtual {v8, v0, v9}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;I)Lacs;

    move-result-object v8

    .line 1108
    iget-object v9, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v9, v3, v1}, Laef;->b(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 1109
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    .line 1110
    const/16 v9, 0x8

    invoke-virtual {v1, v3, v9}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;I)Lacs;

    move-result-object v1

    .line 1112
    if-nez v8, :cond_a

    .line 1114
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1}, Laak;->a()I

    move-result v6

    .line 1115
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v6, :cond_9

    .line 1116
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v7, v1}, Laak;->b(I)Landroid/view/View;

    move-result-object v7

    .line 1117
    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v7

    .line 1118
    if-eq v7, v3, :cond_8

    .line 1119
    invoke-direct {p0, v7}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/support/v7/widget/RecyclerView$r;)J

    move-result-wide v8

    .line 1120
    cmp-long v8, v8, v4

    if-nez v8, :cond_8

    .line 1121
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1122
    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1123
    if-eqz v0, :cond_7

    .line 1124
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \n View Holder 2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1125
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1126
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \n View Holder 2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1127
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1128
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1129
    :cond_9
    const-string v1, "RecyclerView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Problem while matching changed view holders with the newones. The pre-layout information for the change holder "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " cannot be found but it is necessary for "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1130
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1131
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1134
    :cond_a
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 1135
    if-eqz v6, :cond_b

    .line 1136
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 1137
    :cond_b
    if-eq v0, v3, :cond_d

    .line 1138
    if-eqz v7, :cond_c

    .line 1139
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 1140
    :cond_c
    iput-object v3, v0, Landroid/support/v7/widget/RecyclerView$r;->h:Landroid/support/v7/widget/RecyclerView$r;

    .line 1141
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 1142
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView$k;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 1143
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 1144
    iput-object v0, v3, Landroid/support/v7/widget/RecyclerView$r;->i:Landroid/support/v7/widget/RecyclerView$r;

    .line 1145
    :cond_d
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v4, v0, v3, v8, v1}, Landroid/support/v7/widget/RecyclerView$d;->a(Landroid/support/v7/widget/RecyclerView$r;Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    goto/16 :goto_3

    .line 1148
    :cond_e
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, v3, v1}, Laef;->b(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    goto/16 :goto_3

    .line 1150
    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->aB:Laeh;

    invoke-virtual {v0, v1}, Laef;->a(Laeh;)V

    .line 1151
    :cond_10
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 1152
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v1, v1, Landroid/support/v7/widget/RecyclerView$p;->e:I

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$p;->b:I

    .line 1153
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 1154
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$p;->i:Z

    .line 1155
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    .line 1156
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$f;->j:Z

    .line 1157
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    .line 1158
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1159
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$f;->o:Z

    if-eqz v0, :cond_12

    .line 1160
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$f;->n:I

    .line 1161
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$f;->o:Z

    .line 1162
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$k;->b()V

    .line 1163
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$p;)V

    .line 1165
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 1166
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1167
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0}, Laef;->a()V

    .line 1168
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 1169
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a([I)V

    .line 1170
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-ne v2, v0, :cond_13

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->av:[I

    const/4 v2, 0x1

    aget v0, v0, v2

    if-eq v0, v1, :cond_16

    :cond_13
    const/4 v0, 0x1

    .line 1171
    :goto_5
    if-eqz v0, :cond_14

    .line 1172
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->c(II)V

    .line 1174
    :cond_14
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->as:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1175
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getDescendantFocusability()I

    move-result v0

    const/high16 v1, 0x60000

    if-eq v0, v1, :cond_15

    .line 1176
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getDescendantFocusability()I

    move-result v0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_17

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1203
    :cond_15
    :goto_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->A()V

    goto/16 :goto_0

    .line 1170
    :cond_16
    const/4 v0, 0x0

    goto :goto_5

    .line 1178
    :cond_17
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1179
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 1180
    sget-boolean v1, Landroid/support/v7/widget/RecyclerView;->R:Z

    if-eqz v1, :cond_19

    .line 1181
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_19

    .line 1182
    :cond_18
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v0

    if-nez v0, :cond_1a

    .line 1183
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    goto :goto_6

    .line 1185
    :cond_19
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1, v0}, Laak;->d(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1186
    :cond_1a
    const/4 v0, 0x0

    .line 1187
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-wide v2, v1, Landroid/support/v7/widget/RecyclerView$p;->l:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1b

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1188
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1189
    if-eqz v1, :cond_1b

    .line 1190
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-wide v0, v0, Landroid/support/v7/widget/RecyclerView$p;->l:J

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(J)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1191
    :cond_1b
    const/4 v1, 0x0

    .line 1192
    if-eqz v0, :cond_1c

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v2, v3}, Laak;->d(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1c

    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1193
    invoke-virtual {v2}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-nez v2, :cond_1e

    .line 1194
    :cond_1c
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v0

    if-lez v0, :cond_1d

    .line 1195
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->B()Landroid/view/View;

    move-result-object v1

    .line 1197
    :cond_1d
    :goto_7
    if-eqz v1, :cond_15

    .line 1198
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->m:I

    int-to-long v2, v0

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1f

    .line 1199
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->m:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1200
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1202
    :goto_8
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_6

    .line 1196
    :cond_1e
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    goto :goto_7

    :cond_1f
    move-object v0, v1

    goto :goto_8
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;
    .locals 3

    .prologue
    .line 1580
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1581
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    .line 1582
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a direct child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1583
    :cond_0
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/view/ViewGroup;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", adapter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", layout:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", context:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 147
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1712
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lqq;->b(I)V

    .line 1713
    return-void
.end method

.method final a(II)V
    .locals 2

    .prologue
    .line 474
    const/4 v0, 0x0

    .line 475
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    if-lez p1, :cond_0

    .line 476
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 477
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    .line 478
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    if-gez p1, :cond_1

    .line 479
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 480
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 481
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    if-lez p2, :cond_2

    .line 482
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 483
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 484
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    if-gez p2, :cond_3

    .line 485
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 486
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    .line 487
    :cond_3
    if-eqz v0, :cond_4

    .line 489
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->c(Landroid/view/View;)V

    .line 490
    :cond_4
    return-void
.end method

.method public final a(IIZ)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    .line 1528
    add-int v1, p1, p2

    .line 1529
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->b()I

    move-result v2

    .line 1530
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    .line 1531
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v3, v0}, Laak;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1532
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1533
    iget v4, v3, Landroid/support/v7/widget/RecyclerView$r;->c:I

    if-lt v4, v1, :cond_1

    .line 1534
    neg-int v4, p2

    invoke-virtual {v3, v4, p3}, Landroid/support/v7/widget/RecyclerView$r;->a(IZ)V

    .line 1535
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v6, v3, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    .line 1542
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1536
    :cond_1
    iget v4, v3, Landroid/support/v7/widget/RecyclerView$r;->c:I

    if-lt v4, p1, :cond_0

    .line 1537
    add-int/lit8 v4, p1, -0x1

    neg-int v5, p2

    .line 1538
    invoke-virtual {v3, v7}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 1539
    invoke-virtual {v3, v5, p3}, Landroid/support/v7/widget/RecyclerView$r;->a(IZ)V

    .line 1540
    iput v4, v3, Landroid/support/v7/widget/RecyclerView$r;->c:I

    .line 1541
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v6, v3, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    goto :goto_1

    .line 1543
    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 1544
    add-int v3, p1, p2

    .line 1545
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1546
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_5

    .line 1547
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1548
    if-eqz v0, :cond_3

    .line 1549
    iget v4, v0, Landroid/support/v7/widget/RecyclerView$r;->c:I

    if-lt v4, v3, :cond_4

    .line 1550
    neg-int v4, p2

    invoke-virtual {v0, v4, p3}, Landroid/support/v7/widget/RecyclerView$r;->a(IZ)V

    .line 1554
    :cond_3
    :goto_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1551
    :cond_4
    iget v4, v0, Landroid/support/v7/widget/RecyclerView$r;->c:I

    if-lt v4, p1, :cond_3

    .line 1552
    invoke-virtual {v0, v7}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 1553
    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView$k;->a(I)V

    goto :goto_3

    .line 1555
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 1556
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$a;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 156
    .line 157
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-eqz v1, :cond_1

    .line 158
    const-string v1, "Do not setLayoutFrozen in layout or scroll"

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 159
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 160
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 162
    :cond_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 164
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_2

    .line 165
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/RecyclerView$m;

    .line 166
    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView$b;->unregisterObserver(Ljava/lang/Object;)V

    .line 167
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->b()V

    .line 168
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v1}, Lack;->a()V

    .line 169
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 170
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 171
    if-eqz p1, :cond_3

    .line 172
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/RecyclerView$m;

    .line 173
    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView$b;->registerObserver(Ljava/lang/Object;)V

    .line 174
    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 175
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$k;->a()V

    .line 176
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$k;->d()Landroid/support/v7/widget/RecyclerView$j;

    move-result-object v2

    .line 177
    if-eqz v1, :cond_4

    .line 179
    iget v1, v2, Landroid/support/v7/widget/RecyclerView$j;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v2, Landroid/support/v7/widget/RecyclerView$j;->b:I

    .line 180
    :cond_4
    iget v1, v2, Landroid/support/v7/widget/RecyclerView$j;->b:I

    if-nez v1, :cond_5

    move v1, v0

    .line 182
    :goto_0
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$j;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 183
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$j;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    .line 184
    iget-object v0, v0, Lacv;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 186
    :cond_5
    if-eqz v3, :cond_6

    .line 188
    iget v0, v2, Landroid/support/v7/widget/RecyclerView$j;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Landroid/support/v7/widget/RecyclerView$j;->b:I

    .line 189
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$p;->f:Z

    .line 190
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->n()V

    .line 191
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 192
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$f;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-ne p1, v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 206
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$d;->d()V

    .line 209
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 210
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 211
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$k;->a()V

    .line 212
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 214
    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$k;)V

    .line 215
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 216
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 218
    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 219
    iget-object v0, v2, Laak;->b:Laal;

    invoke-virtual {v0}, Laal;->a()V

    .line 220
    iget-object v0, v2, Laak;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 221
    iget-object v3, v2, Laak;->a:Laam;

    iget-object v0, v2, Laak;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v3, v0}, Laam;->d(Landroid/view/View;)V

    .line 222
    iget-object v0, v2, Laak;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 223
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 217
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$k;->a()V

    goto :goto_1

    .line 224
    :cond_4
    iget-object v0, v2, Laak;->a:Laam;

    invoke-virtual {v0}, Laam;->b()V

    .line 225
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 226
    if-eqz p1, :cond_6

    .line 227
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_5

    .line 228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LayoutManager "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already attached to a RecyclerView:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 229
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 231
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$k;->b()V

    .line 232
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto/16 :goto_0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$i;)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    if-nez v0, :cond_0

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    .line 286
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    return-void
.end method

.method final a(Landroid/support/v7/widget/RecyclerView$p;)V
    .locals 2

    .prologue
    .line 1226
    .line 1227
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    .line 1228
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1229
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v7/widget/RecyclerView$q;

    .line 1230
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$q;->c:Landroid/widget/OverScroller;

    .line 1232
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalX()I

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    .line 1233
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalY()I

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    .line 1234
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    .line 257
    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 258
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    move v0, v1

    .line 259
    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView$k;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 260
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 261
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3, v1}, Laak;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 272
    :goto_1
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 262
    :cond_1
    if-nez v0, :cond_2

    .line 263
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 264
    invoke-virtual {v0, v2, v5, v1}, Laak;->a(Landroid/view/View;IZ)V

    goto :goto_1

    .line 266
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    .line 267
    iget-object v1, v0, Laak;->a:Laam;

    invoke-virtual {v1, v2}, Laam;->a(Landroid/view/View;)I

    move-result v1

    .line 268
    if-gez v1, :cond_3

    .line 269
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "view is not a child, cannot hide "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_3
    iget-object v3, v0, Laak;->b:Laal;

    invoke-virtual {v3, v1}, Laal;->a(I)V

    .line 271
    invoke-virtual {v0, v2}, Laak;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method final a(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V
    .locals 3

    .prologue
    .line 1373
    const/4 v0, 0x0

    const/16 v1, 0x2000

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/RecyclerView$r;->a(II)V

    .line 1374
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView$p;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1375
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1376
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/support/v7/widget/RecyclerView$r;)J

    move-result-wide v0

    .line 1377
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v2, v0, v1, p1}, Laef;->a(JLandroid/support/v7/widget/RecyclerView$r;)V

    .line 1378
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, p1, p2}, Laef;->a(Landroid/support/v7/widget/RecyclerView$r;Lacs;)V

    .line 1379
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 679
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 680
    if-nez p1, :cond_0

    .line 681
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 682
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 683
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 684
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:I

    if-lez v0, :cond_2

    .line 685
    const-string v0, "RecyclerView"

    const-string v1, "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame."

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 686
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 687
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 688
    :cond_2
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 422
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    if-gtz v0, :cond_0

    .line 423
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    .line 424
    :cond_0
    if-nez p1, :cond_1

    .line 425
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 426
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    if-ne v0, v2, :cond_3

    .line 427
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_2

    .line 428
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 429
    :cond_2
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_3

    .line 430
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 431
    :cond_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    .line 432
    return-void
.end method

.method public final a(IIII[II)Z
    .locals 7

    .prologue
    .line 1718
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lqq;->a(IIII[II)Z

    move-result v0

    return v0
.end method

.method public final a(II[I[II)Z
    .locals 6

    .prologue
    .line 1720
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lqq;->a(II[I[II)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)Z
    .locals 1

    .prologue
    .line 1690
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1691
    iput p2, p1, Landroid/support/v7/widget/RecyclerView$r;->n:I

    .line 1692
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1693
    const/4 v0, 0x0

    .line 1695
    :goto_0
    return v0

    .line 1694
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-static {v0, p2}, Lqy;->b(Landroid/view/View;I)V

    .line 1695
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$d;->d()V

    .line 195
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 197
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView$k;)V

    .line 198
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$k;->a()V

    .line 199
    return-void
.end method

.method final b(I)V
    .locals 2

    .prologue
    .line 273
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    if-ne p1, v0, :cond_1

    .line 283
    :cond_0
    return-void

    .line 275
    :cond_1
    iput p1, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    .line 276
    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    .line 277
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()V

    .line 279
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 281
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/RecyclerView$i;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 282
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method final b(II)V
    .locals 3

    .prologue
    .line 981
    .line 982
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 984
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, p0}, Lri;->e(Landroid/view/View;)I

    move-result v1

    .line 985
    invoke-static {p1, v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(III)I

    move-result v0

    .line 987
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 989
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, p0}, Lri;->f(Landroid/view/View;)I

    move-result v2

    .line 990
    invoke-static {p2, v1, v2}, Landroid/support/v7/widget/RecyclerView$f;->a(III)I

    move-result v1

    .line 991
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    .line 992
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView$i;)V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 290
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 999
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    .line 1000
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    if-gtz v0, :cond_3

    .line 1001
    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    .line 1002
    if-eqz p1, :cond_3

    .line 1004
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ad:I

    .line 1005
    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->ad:I

    .line 1006
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1007
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 1008
    const/16 v2, 0x800

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    .line 1009
    invoke-static {v1, v0}, Lbw;->a(Landroid/view/accessibility/AccessibilityEvent;I)V

    .line 1010
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1012
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1013
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1014
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1015
    iget v2, v0, Landroid/support/v7/widget/RecyclerView$r;->n:I

    .line 1016
    if-eq v2, v4, :cond_1

    .line 1017
    iget-object v3, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-static {v3, v2}, Lqy;->b(Landroid/view/View;I)V

    .line 1018
    iput v4, v0, Landroid/support/v7/widget/RecyclerView$r;->n:I

    .line 1019
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1020
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1021
    :cond_3
    return-void
.end method

.method final c(Landroid/support/v7/widget/RecyclerView$r;)I
    .locals 2

    .prologue
    .line 1696
    const/16 v0, 0x20c

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$r;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1697
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$r;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1698
    :cond_0
    const/4 v0, -0x1

    .line 1699
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    iget v1, p1, Landroid/support/v7/widget/RecyclerView$r;->c:I

    invoke-virtual {v0, v1}, Lack;->d(I)I

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 308
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v2, :cond_2

    .line 309
    :cond_0
    const-string v0, "RV FullInvalidate"

    invoke-static {v0}, Lbw;->e(Ljava/lang/String;)V

    .line 310
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 311
    invoke-static {}, Lbw;->e()V

    .line 342
    :cond_1
    :goto_0
    return-void

    .line 313
    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v2}, Lack;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lack;->b(I)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    const/16 v3, 0xb

    .line 316
    invoke-virtual {v2, v3}, Lack;->b(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 317
    const-string v2, "RV PartialInvalidate"

    invoke-static {v2}, Lbw;->e(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 319
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 320
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v2}, Lack;->b()V

    .line 321
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-nez v2, :cond_4

    .line 323
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v2}, Laak;->a()I

    move-result v3

    move v2, v0

    .line 324
    :goto_1
    if-ge v2, v3, :cond_3

    .line 325
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v4, v2}, Laak;->b(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v4

    .line 326
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v5

    if-nez v5, :cond_5

    .line 327
    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$r;->s()Z

    move-result v4

    if-eqz v4, :cond_5

    move v0, v1

    .line 331
    :cond_3
    if-eqz v0, :cond_6

    .line 332
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 334
    :cond_4
    :goto_2
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 336
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 337
    invoke-static {}, Lbw;->e()V

    goto :goto_0

    .line 329
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 333
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->c()V

    goto :goto_2

    .line 338
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v0}, Lack;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    const-string v0, "RV FullInvalidate"

    invoke-static {v0}, Lbw;->e(Ljava/lang/String;)V

    .line 340
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 341
    invoke-static {}, Lbw;->e()V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$f;->b(I)V

    .line 294
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    goto :goto_0
.end method

.method final c(II)V
    .locals 2

    .prologue
    .line 1631
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:I

    .line 1632
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollX()I

    move-result v0

    .line 1633
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollY()I

    move-result v1

    .line 1634
    invoke-virtual {p0, v0, v1, v0, v1}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    .line 1635
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1636
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1637
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->at:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$i;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView$i;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 1638
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1639
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ag:I

    .line 1640
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1486
    instance-of v2, p1, Landroid/support/v7/widget/RecyclerView$g;

    if-eqz v2, :cond_1

    check-cast p1, Landroid/support/v7/widget/RecyclerView$g;

    .line 1487
    if-eqz p1, :cond_0

    move v2, v0

    .line 1488
    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 1487
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1488
    goto :goto_1
.end method

.method public computeHorizontalScrollExtent()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 403
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 405
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->d(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 400
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->b(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public computeHorizontalScrollRange()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 406
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 408
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->f(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollExtent()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 412
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 414
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->e(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollOffset()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 409
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 411
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->c(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollRange()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 415
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->g(Landroid/support/v7/widget/RecyclerView$p;)I

    move-result v0

    goto :goto_0
.end method

.method public final d(I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1594
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v1, :cond_1

    .line 1606
    :cond_0
    :goto_0
    return-object v0

    .line 1596
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1}, Laak;->b()I

    move-result v3

    .line 1598
    const/4 v1, 0x0

    move v2, v1

    move-object v1, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 1599
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v2}, Laak;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1600
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1601
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView$r;)I

    move-result v4

    if-ne v4, p1, :cond_3

    .line 1602
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v1, v4}, Laak;->d(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1605
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1606
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 418
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    .line 419
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_0

    .line 420
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 421
    :cond_0
    return-void
.end method

.method public dispatchNestedFling(FFZ)Z
    .locals 1

    .prologue
    .line 1721
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lqq;->a(FFZ)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreFling(FF)Z
    .locals 1

    .prologue
    .line 1722
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lqq;->a(FF)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    .prologue
    .line 1719
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lqq;->a(II[I[I)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedScroll(IIII[I)Z
    .locals 6

    .prologue
    .line 1717
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lqq;->a(IIII[I)Z

    move-result v0

    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 256
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 254
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1438
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 1439
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 1440
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1441
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$e;

    invoke-virtual {v0, p1, p0}, Landroid/support/v7/widget/RecyclerView$e;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V

    .line 1442
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1444
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1445
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 1446
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    .line 1447
    :goto_1
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1448
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    add-int/2addr v0, v4

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1449
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 1450
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1451
    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1452
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 1453
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v3, :cond_1

    .line 1454
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1455
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_9

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    or-int/2addr v0, v3

    .line 1456
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1457
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1458
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 1459
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v5

    .line 1460
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    .line 1461
    :goto_5
    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1462
    neg-int v3, v3

    int-to-float v3, v3

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1463
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_b

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {v3, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v2

    :goto_6
    or-int/2addr v0, v3

    .line 1464
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1465
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1466
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 1467
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1468
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v4, :cond_c

    .line 1469
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1471
    :goto_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    :cond_4
    or-int/2addr v0, v1

    .line 1472
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1473
    :cond_5
    if-nez v0, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    .line 1474
    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$d;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1476
    :goto_8
    if-eqz v2, :cond_6

    .line 1478
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->c(Landroid/view/View;)V

    .line 1479
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 1446
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 1449
    goto/16 :goto_2

    :cond_9
    move v3, v1

    .line 1455
    goto/16 :goto_4

    :cond_a
    move v3, v1

    .line 1460
    goto/16 :goto_5

    :cond_b
    move v3, v1

    .line 1463
    goto :goto_6

    .line 1470
    :cond_c
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_7

    :cond_d
    move v2, v0

    goto :goto_8

    :cond_e
    move v0, v1

    goto/16 :goto_3
.end method

.method final e()V
    .locals 4

    .prologue
    .line 491
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 499
    :goto_0
    return-void

    .line 493
    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    .line 494
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 496
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 497
    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    .line 498
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public final e(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1686
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1687
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1688
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView$a;->c(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 1689
    :cond_0
    return-void
.end method

.method final f()V
    .locals 4

    .prologue
    .line 500
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 508
    :goto_0
    return-void

    .line 502
    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    .line 503
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 505
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 506
    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    .line 507
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 529
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_3

    .line 530
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 531
    :goto_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v6

    .line 532
    if-eqz v0, :cond_d

    if-eq p2, v8, :cond_0

    if-ne p2, v1, :cond_d

    .line 534
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 535
    if-ne p2, v8, :cond_4

    const/16 v0, 0x82

    .line 536
    :goto_1
    invoke-virtual {v6, p0, p1, v0}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 537
    if-nez v3, :cond_5

    move v3, v1

    .line 538
    :goto_2
    sget-boolean v7, Landroid/support/v7/widget/RecyclerView;->Q:Z

    if-eqz v7, :cond_1

    move p2, v0

    .line 540
    :cond_1
    :goto_3
    if-nez v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 541
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 542
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 543
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v0}, Lri;->j(Landroid/view/View;)I

    move-result v0

    .line 544
    if-ne v0, v1, :cond_6

    move v3, v1

    .line 545
    :goto_4
    if-ne p2, v8, :cond_7

    move v0, v1

    :goto_5
    xor-int/2addr v0, v3

    if-eqz v0, :cond_8

    const/16 v0, 0x42

    .line 546
    :goto_6
    invoke-virtual {v6, p0, p1, v0}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 547
    if-nez v3, :cond_9

    move v3, v1

    .line 548
    :goto_7
    sget-boolean v7, Landroid/support/v7/widget/RecyclerView;->Q:Z

    if-eqz v7, :cond_2

    move p2, v0

    .line 550
    :cond_2
    if-eqz v3, :cond_b

    .line 551
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 552
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 553
    if-nez v0, :cond_a

    move-object p1, v5

    .line 606
    :goto_8
    return-object p1

    :cond_3
    move v0, v2

    .line 530
    goto :goto_0

    .line 535
    :cond_4
    const/16 v0, 0x21

    goto :goto_1

    :cond_5
    move v3, v2

    .line 537
    goto :goto_2

    :cond_6
    move v3, v2

    .line 544
    goto :goto_4

    :cond_7
    move v0, v2

    .line 545
    goto :goto_5

    :cond_8
    const/16 v0, 0x11

    goto :goto_6

    :cond_9
    move v3, v2

    .line 547
    goto :goto_7

    .line 555
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 556
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, p2, v3, v7}, Landroid/support/v7/widget/RecyclerView$f;->c(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    .line 557
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 558
    :cond_b
    invoke-virtual {v6, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    .line 569
    :cond_c
    :goto_9
    if-eqz v6, :cond_10

    invoke-virtual {v6}, Landroid/view/View;->hasFocusable()Z

    move-result v0

    if-nez v0, :cond_10

    .line 570
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_f

    .line 571
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    goto :goto_8

    .line 560
    :cond_d
    invoke-virtual {v6, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    .line 561
    if-nez v6, :cond_c

    if-eqz v0, :cond_c

    .line 562
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 563
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 564
    if-nez v0, :cond_e

    move-object p1, v5

    .line 565
    goto :goto_8

    .line 566
    :cond_e
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 567
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    invoke-virtual {v0, p2, v3, v6}, Landroid/support/v7/widget/RecyclerView$f;->c(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;

    move-result-object v6

    .line 568
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    goto :goto_9

    .line 572
    :cond_f
    invoke-direct {p0, v6, v5}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_8

    .line 575
    :cond_10
    if-eqz v6, :cond_11

    if-ne v6, p0, :cond_13

    :cond_11
    move v1, v2

    .line 605
    :cond_12
    :goto_a
    if-eqz v1, :cond_1f

    move-object p1, v6

    goto :goto_8

    .line 577
    :cond_13
    if-eqz p1, :cond_12

    .line 579
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v0, v2, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 580
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v0, v2, v2, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 581
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 582
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    invoke-virtual {p0, v6, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 583
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 584
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 585
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v0}, Lri;->j(Landroid/view/View;)I

    move-result v0

    .line 586
    if-ne v0, v1, :cond_17

    move v0, v4

    .line 588
    :goto_b
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-lt v3, v5, :cond_14

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-gt v3, v5, :cond_18

    :cond_14
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-ge v3, v5, :cond_18

    move v3, v1

    .line 593
    :goto_c
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-lt v5, v7, :cond_15

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-gt v5, v7, :cond_1a

    :cond_15
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v7, :cond_1a

    move v4, v1

    .line 597
    :cond_16
    :goto_d
    sparse-switch p2, :sswitch_data_0

    .line 604
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid direction: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    move v0, v1

    .line 586
    goto :goto_b

    .line 590
    :cond_18
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-gt v3, v5, :cond_19

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-lt v3, v5, :cond_20

    :cond_19
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-le v3, v5, :cond_20

    move v3, v4

    .line 591
    goto :goto_c

    .line 595
    :cond_1a
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    if-gt v5, v7, :cond_1b

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    if-lt v5, v7, :cond_1c

    :cond_1b
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-gt v5, v7, :cond_16

    :cond_1c
    move v4, v2

    goto :goto_d

    .line 598
    :sswitch_0
    if-ltz v3, :cond_12

    move v1, v2

    goto/16 :goto_a

    .line 599
    :sswitch_1
    if-gtz v3, :cond_12

    move v1, v2

    goto/16 :goto_a

    .line 600
    :sswitch_2
    if-ltz v4, :cond_12

    move v1, v2

    goto/16 :goto_a

    .line 601
    :sswitch_3
    if-gtz v4, :cond_12

    move v1, v2

    goto/16 :goto_a

    .line 602
    :sswitch_4
    if-gtz v4, :cond_12

    if-nez v4, :cond_1d

    mul-int/2addr v0, v3

    if-gez v0, :cond_12

    :cond_1d
    move v1, v2

    goto/16 :goto_a

    .line 603
    :sswitch_5
    if-ltz v4, :cond_12

    if-nez v4, :cond_1e

    mul-int/2addr v0, v3

    if-lez v0, :cond_12

    :cond_1e
    move v1, v2

    goto/16 :goto_a

    .line 606
    :cond_1f
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    goto/16 :goto_8

    :cond_20
    move v3, v2

    goto/16 :goto_c

    :cond_21
    move v3, v2

    goto/16 :goto_3

    .line 597
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method final g()V
    .locals 4

    .prologue
    .line 509
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 517
    :goto_0
    return-void

    .line 511
    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    .line 512
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v0, :cond_1

    .line 513
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 514
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 515
    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    .line 516
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 1489
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_0

    .line 1490
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecyclerView has no LayoutManager"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1491
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->a()Landroid/support/v7/widget/RecyclerView$g;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 1492
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_0

    .line 1493
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecyclerView has no LayoutManager"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1494
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1495
    new-instance v1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-direct {v1, v0, p1}, Landroid/support/v7/widget/RecyclerView$g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1496
    return-object v1
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    .line 1497
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_0

    .line 1498
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecyclerView has no LayoutManager"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1500
    :cond_0
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$g;

    if-eqz v0, :cond_1

    .line 1501
    new-instance v0, Landroid/support/v7/widget/RecyclerView$g;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$g;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$g;-><init>(Landroid/support/v7/widget/RecyclerView$g;)V

    .line 1505
    :goto_0
    return-object v0

    .line 1502
    :cond_1
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_2

    .line 1503
    new-instance v0, Landroid/support/v7/widget/RecyclerView$g;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$g;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 1504
    :cond_2
    new-instance v0, Landroid/support/v7/widget/RecyclerView$g;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/RecyclerView$g;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getBaseline()I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_0

    .line 201
    const/4 v0, -0x1

    .line 202
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result v0

    goto :goto_0
.end method

.method public getClipToPadding()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    return v0
.end method

.method final h()V
    .locals 4

    .prologue
    .line 518
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 526
    :goto_0
    return-void

    .line 520
    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    .line 521
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eqz v0, :cond_1

    .line 522
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 523
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 524
    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    .line 525
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public hasNestedScrollingParent()Z
    .locals 2

    .prologue
    .line 1714
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    .line 1715
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lqq;->a(I)Z

    move-result v0

    .line 1716
    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 997
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    .line 998
    return-void
.end method

.method public isAttachedToWindow()Z
    .locals 1

    .prologue
    .line 678
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    return v0
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    .prologue
    .line 1702
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    .line 1703
    iget-boolean v0, v0, Lqq;->a:Z

    .line 1704
    return v0
.end method

.method final j()Z
    .locals 1

    .prologue
    .line 1022
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ae:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ae:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1023
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1038
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    if-eqz v0, :cond_0

    .line 1039
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aA:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lqy;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1040
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Z

    .line 1041
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1424
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->b()I

    move-result v3

    move v2, v1

    .line 1425
    :goto_0
    if-ge v2, v3, :cond_0

    .line 1426
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0, v2}, Laak;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1427
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    .line 1428
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1429
    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 1430
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1431
    :goto_1
    if-ge v1, v3, :cond_2

    .line 1432
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1433
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 1434
    if-eqz v0, :cond_1

    .line 1435
    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    .line 1436
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1437
    :cond_2
    return-void
.end method

.method final n()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v0, 0x0

    .line 1557
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 1559
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v1}, Laak;->b()I

    move-result v2

    move v1, v0

    .line 1560
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1561
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v3, v1}, Laak;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1562
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1563
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 1564
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1565
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    .line 1566
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 1567
    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_3

    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$k;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    .line 1568
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$a;->b:Z

    .line 1569
    if-eqz v1, :cond_3

    .line 1570
    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 1571
    :goto_1
    if-ge v1, v3, :cond_4

    .line 1572
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    .line 1573
    if-eqz v0, :cond_2

    .line 1574
    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView$r;->b(I)V

    .line 1575
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView$r;->a(Ljava/lang/Object;)V

    .line 1576
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1578
    :cond_3
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$k;->c()V

    .line 1579
    :cond_4
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 1641
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    .line 1642
    invoke-virtual {v0}, Lack;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 640
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 641
    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->af:I

    .line 642
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    .line 643
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isLayoutRequested()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    .line 644
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->K:Z

    .line 645
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    if-eqz v0, :cond_1

    .line 646
    sget-object v0, Labm;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labm;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    .line 647
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    if-nez v0, :cond_0

    .line 648
    new-instance v0, Labm;

    invoke-direct {v0}, Labm;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    .line 650
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->o(Landroid/view/View;)Landroid/view/Display;

    move-result-object v0

    .line 652
    const/high16 v1, 0x42700000    # 60.0f

    .line 653
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v0, :cond_3

    .line 654
    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    .line 655
    const/high16 v2, 0x41f00000    # 30.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_3

    .line 657
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float v0, v2, v0

    float-to-long v2, v0

    iput-wide v2, v1, Labm;->c:J

    .line 658
    sget-object v0, Labm;->a:Ljava/lang/ThreadLocal;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 659
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    .line 660
    iget-object v0, v0, Labm;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 661
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 643
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 662
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 663
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$d;->d()V

    .line 665
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 666
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Z

    .line 667
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_1

    .line 668
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    .line 669
    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$k;)V

    .line 670
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->az:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 671
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aA:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 672
    :cond_2
    sget-object v0, Laeg;->d:Lps;

    invoke-interface {v0}, Lps;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 673
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->d:Z

    if-eqz v0, :cond_3

    .line 674
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    .line 675
    iget-object v0, v0, Labm;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 676
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    .line 677
    :cond_3
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1480
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1481
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1482
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1483
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 1484
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1485
    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 904
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_1

    .line 931
    :cond_0
    :goto_0
    return v4

    .line 906
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_0

    .line 908
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    .line 909
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 910
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 911
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    neg-float v0, v0

    .line 913
    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 914
    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    move v5, v2

    move v2, v0

    move v0, v5

    .line 929
    :goto_2
    cmpl-float v3, v2, v1

    if-nez v3, :cond_2

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 930
    :cond_2
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->aq:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->ar:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-direct {p0, v0, v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 912
    goto :goto_1

    :cond_4
    move v2, v0

    move v0, v1

    .line 915
    goto :goto_2

    .line 916
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    const/high16 v2, 0x400000

    and-int/2addr v0, v2

    if-eqz v0, :cond_8

    .line 917
    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 918
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 919
    neg-float v0, v0

    move v2, v0

    move v0, v1

    .line 920
    goto :goto_2

    .line 921
    :cond_6
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v1

    .line 923
    goto :goto_2

    :cond_7
    move v0, v1

    move v2, v1

    .line 926
    goto :goto_2

    :cond_8
    move v0, v1

    move v2, v1

    .line 928
    goto :goto_2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 689
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 761
    :goto_0
    return v0

    .line 692
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 693
    if-eq v4, v8, :cond_1

    if-nez v4, :cond_2

    .line 694
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    .line 695
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    .line 696
    :goto_1
    if-ge v3, v5, :cond_4

    .line 697
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$h;

    .line 698
    invoke-interface {v0, p1}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/view/MotionEvent;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eq v4, v8, :cond_3

    .line 699
    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    move v0, v2

    .line 703
    :goto_2
    if-eqz v0, :cond_5

    .line 704
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    move v0, v2

    .line 705
    goto :goto_0

    .line 701
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 702
    goto :goto_2

    .line 706
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v0, :cond_6

    move v0, v1

    .line 707
    goto :goto_0

    .line 708
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v0

    .line 709
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v3

    .line 710
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    if-nez v4, :cond_7

    .line 711
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    .line 712
    :cond_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 713
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    .line 714
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v5

    .line 715
    packed-switch v4, :pswitch_data_0

    .line 761
    :cond_8
    :goto_3
    :pswitch_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    if-ne v0, v2, :cond_e

    move v0, v2

    goto :goto_0

    .line 716
    :pswitch_1
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->ac:Z

    if-eqz v4, :cond_9

    .line 717
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->ac:Z

    .line 718
    :cond_9
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 719
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    add-float/2addr v4, v7

    float-to-int v4, v4

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    .line 720
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    add-float/2addr v4, v7

    float-to-int v4, v4

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    .line 721
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a

    .line 722
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 723
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 724
    :cond_a
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aput v1, v5, v2

    aput v1, v4, v1

    .line 726
    if-eqz v0, :cond_10

    move v0, v2

    .line 728
    :goto_4
    if-eqz v3, :cond_b

    .line 729
    or-int/lit8 v0, v0, 0x2

    .line 730
    :cond_b
    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->e(II)Z

    goto :goto_3

    .line 732
    :pswitch_2
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 733
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    .line 734
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    goto :goto_3

    .line 736
    :pswitch_3
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 737
    if-gez v4, :cond_c

    .line 738
    const-string v0, "RecyclerView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error processing scroll; pointer index for id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 739
    goto/16 :goto_0

    .line 740
    :cond_c
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    add-float/2addr v5, v7

    float-to-int v5, v5

    .line 741
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    add-float/2addr v4, v7

    float-to-int v4, v4

    .line 742
    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    if-eq v6, v2, :cond_8

    .line 743
    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    sub-int v6, v5, v6

    .line 744
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    sub-int v7, v4, v7

    .line 746
    if-eqz v0, :cond_f

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    if-le v0, v6, :cond_f

    .line 747
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    move v0, v2

    .line 749
    :goto_5
    if-eqz v3, :cond_d

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    if-le v3, v5, :cond_d

    .line 750
    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    move v0, v2

    .line 752
    :cond_d
    if-eqz v0, :cond_8

    .line 753
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    goto/16 :goto_3

    .line 755
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_3

    .line 757
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 758
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto/16 :goto_3

    .line 760
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    goto/16 :goto_3

    :cond_e
    move v0, v1

    .line 761
    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto :goto_5

    :cond_10
    move v0, v1

    goto/16 :goto_4

    .line 715
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1415
    const-string v0, "RV OnLayout"

    invoke-static {v0}, Lbw;->e(Ljava/lang/String;)V

    .line 1416
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->z()V

    .line 1417
    invoke-static {}, Lbw;->e()V

    .line 1418
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    .line 1419
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 932
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v2, :cond_1

    .line 933
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->b(II)V

    .line 980
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-boolean v2, v2, Landroid/support/v7/widget/RecyclerView$f;->k:Z

    if-eqz v2, :cond_4

    .line 936
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 937
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 938
    if-ne v2, v4, :cond_2

    if-ne v3, v4, :cond_2

    move v0, v1

    .line 939
    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v2, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->c(II)V

    .line 940
    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$p;->d:I

    if-ne v0, v1, :cond_3

    .line 943
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->C()V

    .line 944
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->a(II)V

    .line 945
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    .line 946
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->b(II)V

    .line 947
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 949
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 950
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 951
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView$f;->a(II)V

    .line 952
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->D()V

    .line 953
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->b(II)V

    goto :goto_0

    .line 955
    :cond_4
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->s:Z

    if-eqz v2, :cond_5

    .line 956
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->c(II)V

    goto :goto_0

    .line 958
    :cond_5
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v2, :cond_8

    .line 959
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 960
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->i()V

    .line 961
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->y()V

    .line 963
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->b(Z)V

    .line 964
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v2, v2, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    if-eqz v2, :cond_7

    .line 965
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v1, v2, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 968
    :goto_1
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 969
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 973
    :cond_6
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v1, :cond_9

    .line 974
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v2

    iput v2, v1, Landroid/support/v7/widget/RecyclerView$p;->e:I

    .line 976
    :goto_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 977
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1, p1, p2}, Landroid/support/v7/widget/RecyclerView$f;->c(II)V

    .line 978
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 979
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    goto/16 :goto_0

    .line 966
    :cond_7
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Lack;

    invoke-virtual {v1}, Lack;->e()V

    .line 967
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput-boolean v0, v1, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    goto :goto_1

    .line 970
    :cond_8
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$p;->j:Z

    if-eqz v1, :cond_6

    .line 971
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 975
    :cond_9
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    iput v0, v1, Landroid/support/v7/widget/RecyclerView$p;->e:I

    goto :goto_2
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 637
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    const/4 v0, 0x0

    .line 639
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 243
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$n;

    if-nez v0, :cond_1

    .line 244
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    check-cast p1, Landroid/support/v7/widget/RecyclerView$n;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    .line 247
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    .line 248
    iget-object v0, v0, Lpy;->e:Landroid/os/Parcelable;

    .line 249
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 250
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Landroid/support/v7/widget/RecyclerView$n;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView$n;-><init>(Landroid/os/Parcelable;)V

    .line 235
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/RecyclerView$n;

    .line 237
    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    .line 242
    :goto_0
    return-object v0

    .line 239
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v1, :cond_1

    .line 240
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->b()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    goto :goto_0

    .line 241
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$n;->a:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 993
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 994
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 995
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 996
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 768
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->ac:Z

    if-eqz v0, :cond_1

    .line 872
    :cond_0
    :goto_0
    return v5

    .line 771
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 772
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    if-eqz v2, :cond_2

    .line 773
    if-nez v0, :cond_3

    .line 774
    iput-object v3, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    .line 779
    :cond_2
    if-eqz v0, :cond_7

    .line 780
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v5

    .line 781
    :goto_1
    if-ge v2, v3, :cond_7

    .line 782
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$h;

    .line 783
    invoke-interface {v0, p1}, Landroid/support/v7/widget/RecyclerView$h;->a(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 784
    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    move v0, v6

    .line 788
    :goto_2
    if-eqz v0, :cond_8

    .line 789
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    move v5, v6

    .line 790
    goto :goto_0

    .line 775
    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    invoke-interface {v2, p1}, Landroid/support/v7/widget/RecyclerView$h;->b(Landroid/view/MotionEvent;)V

    .line 776
    const/4 v2, 0x3

    if-eq v0, v2, :cond_4

    if-ne v0, v6, :cond_5

    .line 777
    :cond_4
    iput-object v3, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/RecyclerView$h;

    :cond_5
    move v0, v6

    .line 778
    goto :goto_2

    .line 786
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_7
    move v0, v5

    .line 787
    goto :goto_2

    .line 791
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-eqz v0, :cond_0

    .line 793
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v7

    .line 794
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v8

    .line 795
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    if-nez v0, :cond_9

    .line 796
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    .line 798
    :cond_9
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v9

    .line 799
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 800
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    .line 801
    if-nez v0, :cond_a

    .line 802
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aput v5, v4, v6

    aput v5, v3, v5

    .line 803
    :cond_a
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aget v3, v3, v5

    int-to-float v3, v3

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aget v4, v4, v6

    int-to-float v4, v4

    invoke-virtual {v9, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 804
    packed-switch v0, :pswitch_data_0

    .line 869
    :cond_b
    :goto_3
    :pswitch_0
    if-nez v5, :cond_c

    .line 870
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v9}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 871
    :cond_c
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    move v5, v6

    .line 872
    goto/16 :goto_0

    .line 805
    :pswitch_1
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 806
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    .line 807
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    .line 809
    if-eqz v7, :cond_1e

    move v0, v6

    .line 811
    :goto_4
    if-eqz v8, :cond_d

    .line 812
    or-int/lit8 v0, v0, 0x2

    .line 813
    :cond_d
    invoke-direct {p0, v0, v5}, Landroid/support/v7/widget/RecyclerView;->e(II)Z

    goto :goto_3

    .line 815
    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 816
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->aj:I

    .line 817
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ak:I

    goto :goto_3

    .line 819
    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 820
    if-gez v0, :cond_e

    .line 821
    const-string v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error processing scroll; pointer index for id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 823
    :cond_e
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v1, v11

    float-to-int v10, v1

    .line 824
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v11, v0

    .line 825
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    sub-int v1, v0, v10

    .line 826
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    sub-int v2, v0, v11

    .line 827
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->M:[I

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView;->a(II[I[II)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 828
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:[I

    aget v0, v0, v5

    sub-int/2addr v1, v0

    .line 829
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->M:[I

    aget v0, v0, v6

    sub-int/2addr v2, v0

    .line 830
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v0, v0, v5

    int-to-float v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v3, v3, v6

    int-to-float v3, v3

    invoke-virtual {v9, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 831
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aget v3, v0, v5

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v4, v4, v5

    add-int/2addr v3, v4

    aput v3, v0, v5

    .line 832
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ay:[I

    aget v3, v0, v6

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v4, v4, v6

    add-int/2addr v3, v4

    aput v3, v0, v6

    .line 833
    :cond_f
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    if-eq v0, v6, :cond_11

    .line 835
    if-eqz v7, :cond_1d

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    if-le v0, v3, :cond_1d

    .line 836
    if-lez v1, :cond_14

    .line 837
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    sub-int v0, v1, v0

    :goto_5
    move v1, v0

    move v0, v6

    .line 840
    :goto_6
    if-eqz v8, :cond_10

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    if-le v3, v4, :cond_10

    .line 841
    if-lez v2, :cond_15

    .line 842
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    sub-int v0, v2, v0

    :goto_7
    move v2, v0

    move v0, v6

    .line 845
    :cond_10
    if-eqz v0, :cond_11

    .line 846
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 847
    :cond_11
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->D:I

    if-ne v0, v6, :cond_b

    .line 848
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v0, v0, v5

    sub-int v0, v10, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->al:I

    .line 849
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ax:[I

    aget v0, v0, v6

    sub-int v0, v11, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->am:I

    .line 850
    if-eqz v7, :cond_16

    move v3, v1

    :goto_8
    if-eqz v8, :cond_17

    move v0, v2

    :goto_9
    invoke-direct {p0, v3, v0, v9}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 851
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 852
    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    if-eqz v0, :cond_b

    if-nez v1, :cond_13

    if-eqz v2, :cond_b

    .line 853
    :cond_13
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Labm;

    invoke-virtual {v0, p0, v1, v2}, Labm;->a(Landroid/support/v7/widget/RecyclerView;II)V

    goto/16 :goto_3

    .line 838
    :cond_14
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    add-int/2addr v0, v1

    goto :goto_5

    .line 843
    :cond_15
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->an:I

    add-int/2addr v0, v2

    goto :goto_7

    :cond_16
    move v3, v5

    .line 850
    goto :goto_8

    :cond_17
    move v0, v5

    goto :goto_9

    .line 855
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_3

    .line 857
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v9}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 859
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->ap:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 860
    if-eqz v7, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 861
    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    neg-float v0, v0

    move v2, v0

    .line 862
    :goto_a
    if-eqz v8, :cond_1c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->ai:Landroid/view/VelocityTracker;

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->ah:I

    .line 863
    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    neg-float v0, v0

    .line 864
    :goto_b
    cmpl-float v3, v2, v1

    if-nez v3, :cond_18

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_19

    :cond_18
    float-to-int v1, v2

    float-to-int v0, v0

    invoke-direct {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->d(II)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 865
    :cond_19
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 866
    :cond_1a
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    move v5, v6

    .line 867
    goto/16 :goto_3

    :cond_1b
    move v2, v1

    .line 861
    goto :goto_a

    :cond_1c
    move v0, v1

    .line 863
    goto :goto_b

    .line 868
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    goto/16 :goto_3

    :cond_1d
    move v0, v5

    goto/16 :goto_6

    :cond_1e
    move v0, v5

    goto/16 :goto_4

    .line 804
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method final p()V
    .locals 7

    .prologue
    .line 1643
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v1

    .line 1644
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 1645
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    invoke-virtual {v2, v0}, Laak;->b(I)Landroid/view/View;

    move-result-object v2

    .line 1646
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 1647
    if-eqz v3, :cond_1

    iget-object v4, v3, Landroid/support/v7/widget/RecyclerView$r;->i:Landroid/support/v7/widget/RecyclerView$r;

    if-eqz v4, :cond_1

    .line 1648
    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView$r;->i:Landroid/support/v7/widget/RecyclerView$r;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 1649
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 1650
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1651
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v5

    if-eq v2, v5, :cond_1

    .line 1653
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    .line 1654
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v2

    .line 1655
    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 1656
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1657
    :cond_2
    return-void
.end method

.method final r()Lqq;
    .locals 1

    .prologue
    .line 1723
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aw:Lqq;

    if-nez v0, :cond_0

    .line 1724
    new-instance v0, Lqq;

    invoke-direct {v0, p0}, Lqq;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aw:Lqq;

    .line 1725
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aw:Lqq;

    return-object v0
.end method

.method public removeDetachedView(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 1399
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 1400
    if-eqz v0, :cond_0

    .line 1401
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1402
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->i()V

    .line 1406
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1407
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)V

    .line 1408
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeDetachedView(Landroid/view/View;Z)V

    .line 1409
    return-void

    .line 1403
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1404
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called removeDetachedView with a view which is not flagged as tmp detached."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1405
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 607
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 610
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    if-eqz v3, :cond_3

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    .line 611
    iget-boolean v2, v2, Landroid/support/v7/widget/RecyclerView$o;->e:Z

    .line 612
    if-eqz v2, :cond_3

    move v2, v1

    .line 613
    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 614
    :cond_1
    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 615
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 616
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 617
    return-void

    :cond_3
    move v2, v0

    .line 612
    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 6

    .prologue
    .line 634
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 635
    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z

    move-result v0

    .line 636
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 3

    .prologue
    .line 762
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 763
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 764
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 765
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 766
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 767
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1420
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->ab:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v0, :cond_0

    .line 1421
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1423
    :goto_0
    return-void

    .line 1422
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    goto :goto_0
.end method

.method public scrollBy(II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 298
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v1, :cond_1

    .line 299
    const-string v0, "RecyclerView"

    const-string v1, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v1, :cond_0

    .line 303
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v1

    .line 304
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v2

    .line 305
    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    .line 306
    :cond_2
    if-eqz v1, :cond_3

    :goto_1
    if-eqz v2, :cond_4

    :goto_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;->a(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_1

    :cond_4
    move p2, v0

    goto :goto_2
.end method

.method public scrollTo(II)V
    .locals 2

    .prologue
    .line 296
    const-string v0, "RecyclerView"

    const-string v1, "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1024
    .line 1025
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    if-eqz p1, :cond_3

    .line 1028
    invoke-static {p1}, Lbw;->a(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    .line 1029
    :goto_0
    if-nez v1, :cond_2

    .line 1031
    :goto_1
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->ad:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->ad:I

    .line 1032
    const/4 v0, 0x1

    .line 1034
    :cond_0
    if-eqz v0, :cond_1

    .line 1037
    :goto_2
    return-void

    .line 1036
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-eq p1, v0, :cond_0

    .line 149
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 150
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    .line 151
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 152
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Z

    if-eqz v0, :cond_1

    .line 153
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 154
    :cond_1
    return-void
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    .prologue
    .line 1700
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lqq;->a(Z)V

    .line 1701
    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 2

    .prologue
    .line 1705
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    .line 1706
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lqq;->a(II)Z

    move-result v0

    .line 1707
    return v0
.end method

.method public stopNestedScroll()V
    .locals 2

    .prologue
    .line 1709
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->r()Lqq;

    move-result-object v0

    .line 1710
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lqq;->b(I)V

    .line 1711
    return-void
.end method
