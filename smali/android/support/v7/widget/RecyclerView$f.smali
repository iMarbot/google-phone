.class public abstract Landroid/support/v7/widget/RecyclerView$f;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/RecyclerView$f$a;
    }
.end annotation


# instance fields
.field private a:Laee;

.field private b:Laee;

.field public e:Laak;

.field public f:Landroid/support/v7/widget/RecyclerView;

.field public g:Laec;

.field public h:Laec;

.field public i:Landroid/support/v7/widget/RecyclerView$o;

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:I

.field public o:Z

.field public p:I

.field public q:I

.field public r:I

.field public s:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lact;

    invoke-direct {v0, p0}, Lact;-><init>(Landroid/support/v7/widget/RecyclerView$f;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->a:Laee;

    .line 3
    new-instance v0, Lacu;

    invoke-direct {v0, p0}, Lacu;-><init>(Landroid/support/v7/widget/RecyclerView$f;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->b:Laee;

    .line 4
    new-instance v0, Laec;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->a:Laee;

    invoke-direct {v0, v1}, Laec;-><init>(Laee;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->g:Laec;

    .line 5
    new-instance v0, Laec;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->b:Laee;

    invoke-direct {v0, v1}, Laec;-><init>(Laee;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->h:Laec;

    .line 6
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView$f;->j:Z

    .line 7
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView$f;->k:Z

    .line 8
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView$f;->l:Z

    .line 9
    iput-boolean v3, p0, Landroid/support/v7/widget/RecyclerView$f;->m:Z

    .line 10
    return-void
.end method

.method public static a(III)I
    .locals 2

    .prologue
    .line 72
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 73
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 74
    sparse-switch v1, :sswitch_data_0

    .line 77
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    .line 76
    :sswitch_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(IIIIZ)I
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/high16 v3, -0x80000000

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 221
    sub-int v0, p0, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 224
    if-eqz p4, :cond_3

    .line 225
    if-ltz p3, :cond_1

    move v1, v2

    move v0, p3

    .line 250
    :cond_0
    :goto_0
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    .line 228
    :cond_1
    if-ne p3, v5, :cond_2

    .line 229
    sparse-switch p1, :sswitch_data_0

    :sswitch_0
    move v0, v1

    .line 235
    goto :goto_0

    :sswitch_1
    move v1, p1

    .line 232
    goto :goto_0

    .line 236
    :cond_2
    if-ne p3, v4, :cond_7

    move v0, v1

    .line 238
    goto :goto_0

    .line 239
    :cond_3
    if-ltz p3, :cond_4

    move v1, v2

    move v0, p3

    .line 241
    goto :goto_0

    .line 242
    :cond_4
    if-ne p3, v5, :cond_5

    move v1, p1

    .line 244
    goto :goto_0

    .line 245
    :cond_5
    if-ne p3, v4, :cond_7

    .line 247
    if-eq p1, v3, :cond_6

    if-ne p1, v2, :cond_0

    :cond_6
    move v1, v3

    .line 248
    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0

    .line 229
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 155
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->a:Landroid/support/v7/widget/RecyclerView$r;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v0

    .line 156
    return v0
.end method

.method public static b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 251
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 252
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(III)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 212
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 213
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 214
    if-lez p2, :cond_1

    if-eq p0, p2, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 216
    :cond_1
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 218
    :sswitch_0
    if-lt v3, p0, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    move v0, v1

    .line 217
    goto :goto_0

    .line 219
    :sswitch_2
    if-ne v3, p0, :cond_0

    move v0, v1

    goto :goto_0

    .line 216
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 253
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 254
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a()Landroid/support/v7/widget/RecyclerView$g;
.end method

.method public a(I)Landroid/view/View;
    .locals 5

    .prologue
    .line 157
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v2

    .line 158
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 159
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v0

    .line 160
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 161
    if-eqz v3, :cond_1

    .line 162
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->c()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 164
    iget-boolean v4, v4, Landroid/support/v7/widget/RecyclerView$p;->g:Z

    .line 165
    if-nez v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v3

    if-nez v3, :cond_1

    .line 168
    :cond_0
    :goto_1
    return-object v0

    .line 167
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 24
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->p:I

    .line 25
    iget v0, p0, Landroid/support/v7/widget/RecyclerView$f;->p:I

    if-nez v0, :cond_0

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->b:Z

    if-nez v0, :cond_0

    .line 26
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 27
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 28
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->q:I

    .line 29
    iget v0, p0, Landroid/support/v7/widget/RecyclerView$f;->q:I

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->b:Z

    if-nez v0, :cond_1

    .line 30
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 31
    :cond_1
    return-void
.end method

.method public a(IILandroid/support/v7/widget/RecyclerView$p;Landroid/support/v7/widget/RecyclerView$f$a;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public a(ILandroid/support/v7/widget/RecyclerView$f$a;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public final a(ILandroid/support/v7/widget/RecyclerView$k;)V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v0

    .line 173
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->c(I)V

    .line 174
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView$k;->a(Landroid/view/View;)V

    .line 175
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 363
    return-void
.end method

.method final a(Landroid/support/v7/widget/RecyclerView$k;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 188
    .line 189
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 191
    add-int/lit8 v0, v2, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 193
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$r;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 195
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v3

    .line 196
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v4

    if-nez v4, :cond_2

    .line 197
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 198
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$r;->n()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 199
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0, v5}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 200
    :cond_0
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    if-eqz v4, :cond_1

    .line 201
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->C:Landroid/support/v7/widget/RecyclerView$d;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView$d;->c(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 202
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView$r;->a(Z)V

    .line 203
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$k;->b(Landroid/view/View;)V

    .line 204
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 206
    :cond_3
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 207
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 208
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 209
    :cond_4
    if-lez v2, :cond_5

    .line 210
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 211
    :cond_5
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)V
    .locals 2

    .prologue
    .line 85
    const-string v0, "RecyclerView"

    const-string v1, "You must override onLayoutChildren(Recycler recycler, State state) "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$p;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 11
    if-nez p1, :cond_0

    .line 12
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 13
    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    .line 14
    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 15
    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 20
    :goto_0
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$f;->p:I

    .line 21
    iput v1, p0, Landroid/support/v7/widget/RecyclerView$f;->q:I

    .line 22
    return-void

    .line 16
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 17
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->g:Laak;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    .line 18
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 19
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$k;)V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public final a(Landroid/view/View;IZ)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x0

    .line 96
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v2

    .line 97
    if-nez p3, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 98
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, v2}, Laef;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 100
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    .line 101
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->g()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 102
    :cond_1
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 103
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->f()V

    .line 105
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v1, p1, p2, v3, v7}, Laak;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 140
    :cond_2
    :goto_2
    iget-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$g;->d:Z

    if-eqz v1, :cond_3

    .line 141
    iget-object v1, v2, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 142
    iput-boolean v7, v0, Landroid/support/v7/widget/RecyclerView$g;->d:Z

    .line 143
    :cond_3
    return-void

    .line 99
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v0, v2}, Laef;->c(Landroid/support/v7/widget/RecyclerView$r;)V

    goto :goto_0

    .line 104
    :cond_5
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$r;->h()V

    goto :goto_1

    .line 106
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v3, :cond_b

    .line 107
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v1, p1}, Laak;->c(Landroid/view/View;)I

    move-result v1

    .line 108
    if-ne p2, v4, :cond_7

    .line 109
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v3}, Laak;->a()I

    move-result p2

    .line 110
    :cond_7
    if-ne v1, v4, :cond_8

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 112
    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_8
    if-eq v1, p2, :cond_2

    .line 114
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 115
    invoke-virtual {v3, v1}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v4

    .line 116
    if-nez v4, :cond_9

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Cannot move a child from non-existing index:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v3, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 118
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_9
    invoke-virtual {v3, v1}, Landroid/support/v7/widget/RecyclerView$f;->d(I)V

    .line 121
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$g;

    .line 122
    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v5

    .line 123
    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 124
    iget-object v6, v3, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v6, v5}, Laef;->b(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 126
    :goto_3
    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v5

    invoke-virtual {v3, v4, p2, v1, v5}, Laak;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    goto/16 :goto_2

    .line 125
    :cond_a
    iget-object v6, v3, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->h:Laef;

    invoke-virtual {v6, v5}, Laef;->c(Landroid/support/v7/widget/RecyclerView$r;)V

    goto :goto_3

    .line 128
    :cond_b
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v1, p1, p2, v7}, Laak;->a(Landroid/view/View;IZ)V

    .line 129
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$g;->c:Z

    .line 130
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    .line 131
    iget-boolean v1, v1, Landroid/support/v7/widget/RecyclerView$o;->e:Z

    .line 132
    if-eqz v1, :cond_2

    .line 133
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->i:Landroid/support/v7/widget/RecyclerView$o;

    .line 135
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/view/View;)I

    move-result v3

    .line 137
    iget v4, v1, Landroid/support/v7/widget/RecyclerView$o;->a:I

    .line 138
    if-ne v3, v4, :cond_2

    .line 139
    iput-object p1, v1, Landroid/support/v7/widget/RecyclerView$o;->f:Landroid/view/View;

    goto/16 :goto_2
.end method

.method public final a(Landroid/view/View;ZLandroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    .line 256
    iget v1, v0, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    .line 257
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v4

    .line 258
    invoke-virtual {p3, v1, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 259
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    if-nez v1, :cond_0

    .line 262
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->k:Landroid/graphics/RectF;

    .line 263
    invoke-virtual {v1, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 264
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 265
    iget v0, v1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v0

    .line 266
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    iget v2, v1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    .line 267
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-double v4, v3

    .line 268
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v1

    .line 269
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 270
    invoke-virtual {p3, v0, v2, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 271
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 272
    return-void
.end method

.method public a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x1

    .line 370
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 371
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 374
    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 375
    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 376
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->canScrollHorizontally(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 377
    :cond_2
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 378
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->l:Landroid/support/v7/widget/RecyclerView$a;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$a;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    goto :goto_0

    .line 376
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z
    .locals 14

    .prologue
    .line 286
    .line 287
    const/4 v1, 0x2

    new-array v4, v1, [I

    .line 288
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v5

    .line 289
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v6

    .line 291
    iget v1, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 292
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v2

    sub-int v7, v1, v2

    .line 294
    iget v1, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 295
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v2

    sub-int v8, v1, v2

    .line 296
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v1

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getScrollX()I

    move-result v2

    sub-int v9, v1, v2

    .line 297
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTop()I

    move-result v1

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int v10, v1, v2

    .line 298
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int v11, v9, v1

    .line 299
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int v12, v10, v1

    .line 300
    const/4 v1, 0x0

    sub-int v2, v9, v5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 301
    const/4 v1, 0x0

    sub-int v3, v10, v6

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 302
    const/4 v1, 0x0

    sub-int v13, v11, v7

    invoke-static {v1, v13}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 303
    const/4 v13, 0x0

    sub-int v8, v12, v8

    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 304
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->j()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_4

    .line 305
    if-eqz v1, :cond_3

    :goto_0
    move v2, v1

    .line 309
    :goto_1
    if-eqz v3, :cond_6

    move v1, v3

    .line 311
    :goto_2
    const/4 v3, 0x0

    aput v2, v4, v3

    .line 312
    const/4 v2, 0x1

    aput v1, v4, v2

    .line 315
    const/4 v1, 0x0

    aget v1, v4, v1

    .line 316
    const/4 v2, 0x1

    aget v2, v4, v2

    .line 317
    if-eqz p5, :cond_0

    .line 318
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    .line 319
    if-nez v3, :cond_7

    .line 320
    const/4 v3, 0x0

    .line 335
    :goto_3
    if-eqz v3, :cond_f

    .line 336
    :cond_0
    if-nez v1, :cond_1

    if-eqz v2, :cond_f

    .line 337
    :cond_1
    if-eqz p4, :cond_a

    .line 338
    invoke-virtual {p1, v1, v2}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    .line 352
    :cond_2
    :goto_4
    const/4 v1, 0x1

    .line 353
    :goto_5
    return v1

    .line 305
    :cond_3
    sub-int v1, v11, v7

    .line 306
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    .line 307
    :cond_4
    if-eqz v2, :cond_5

    move v1, v2

    :goto_6
    move v2, v1

    .line 308
    goto :goto_1

    .line 307
    :cond_5
    sub-int v2, v9, v5

    .line 308
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_6

    .line 309
    :cond_6
    sub-int v1, v10, v6

    .line 310
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    .line 321
    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v4

    .line 322
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v5

    .line 324
    iget v6, p0, Landroid/support/v7/widget/RecyclerView$f;->r:I

    .line 325
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v7

    sub-int/2addr v6, v7

    .line 327
    iget v7, p0, Landroid/support/v7/widget/RecyclerView$f;->s:I

    .line 328
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v8

    sub-int/2addr v7, v8

    .line 329
    iget-object v8, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v8, v8, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    .line 331
    invoke-static {v3, v8}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 332
    iget v3, v8, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v1

    if-ge v3, v6, :cond_8

    iget v3, v8, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v1

    if-le v3, v4, :cond_8

    iget v3, v8, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v2

    if-ge v3, v7, :cond_8

    iget v3, v8, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v2

    if-gt v3, v5, :cond_9

    .line 333
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 334
    :cond_9
    const/4 v3, 0x1

    goto :goto_3

    .line 341
    :cond_a
    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    if-nez v3, :cond_b

    .line 342
    const-string v1, "RecyclerView"

    const-string v2, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 344
    :cond_b
    iget-boolean v3, p1, Landroid/support/v7/widget/RecyclerView;->v:Z

    if-nez v3, :cond_2

    .line 345
    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v3

    if-nez v3, :cond_c

    .line 346
    const/4 v1, 0x0

    .line 347
    :cond_c
    iget-object v3, p1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v3

    if-nez v3, :cond_10

    .line 348
    const/4 v2, 0x0

    move v3, v2

    .line 349
    :goto_7
    if-nez v1, :cond_d

    if-eqz v3, :cond_2

    .line 350
    :cond_d
    iget-object v4, p1, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v7/widget/RecyclerView$q;

    const/4 v2, 0x0

    .line 351
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v3, v5, v6}, Landroid/support/v7/widget/RecyclerView$q;->a(IIII)I

    move-result v5

    if-nez v2, :cond_e

    sget-object v2, Landroid/support/v7/widget/RecyclerView;->N:Landroid/view/animation/Interpolator;

    :cond_e
    invoke-virtual {v4, v1, v3, v5, v2}, Landroid/support/v7/widget/RecyclerView$q;->a(IIILandroid/view/animation/Interpolator;)V

    goto/16 :goto_4

    .line 353
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_10
    move v3, v2

    goto :goto_7
.end method

.method public b(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 355
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method final b(II)V
    .locals 8

    .prologue
    const v2, 0x7fffffff

    const/high16 v3, -0x80000000

    .line 32
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v5

    .line 33
    if-nez v5, :cond_0

    .line 34
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->b(II)V

    .line 68
    :goto_0
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    move v4, v0

    move v1, v3

    move v0, v2

    :goto_1
    if-ge v4, v5, :cond_5

    .line 41
    invoke-virtual {p0, v4}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v6

    .line 42
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, v7, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    .line 44
    invoke-static {v6, v7}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 45
    iget v6, v7, Landroid/graphics/Rect;->left:I

    if-ge v6, v0, :cond_1

    .line 46
    iget v0, v7, Landroid/graphics/Rect;->left:I

    .line 47
    :cond_1
    iget v6, v7, Landroid/graphics/Rect;->right:I

    if-le v6, v1, :cond_2

    .line 48
    iget v1, v7, Landroid/graphics/Rect;->right:I

    .line 49
    :cond_2
    iget v6, v7, Landroid/graphics/Rect;->top:I

    if-ge v6, v2, :cond_3

    .line 50
    iget v2, v7, Landroid/graphics/Rect;->top:I

    .line 51
    :cond_3
    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    if-le v6, v3, :cond_4

    .line 52
    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    .line 53
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 54
    :cond_5
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 55
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/graphics/Rect;

    .line 56
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->l()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->n()I

    move-result v2

    add-int/2addr v1, v2

    .line 57
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->m()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->o()I

    move-result v2

    add-int/2addr v0, v2

    .line 59
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 60
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v2}, Lri;->e(Landroid/view/View;)I

    move-result v2

    .line 61
    invoke-static {p1, v1, v2}, Landroid/support/v7/widget/RecyclerView$f;->a(III)I

    move-result v1

    .line 63
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 64
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v2}, Lri;->f(Landroid/view/View;)I

    move-result v2

    .line 65
    invoke-static {p2, v0, v2}, Landroid/support/v7/widget/RecyclerView$f;->a(III)I

    move-result v0

    .line 67
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView$k;)V
    .locals 2

    .prologue
    .line 364
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$f;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 365
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v1

    .line 366
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$r;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/RecyclerView$f;->a(ILandroid/support/v7/widget/RecyclerView$k;)V

    .line 368
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 369
    :cond_1
    return-void
.end method

.method final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 381
    .line 382
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 383
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 384
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView$f;->a(II)V

    .line 385
    return-void
.end method

.method public c(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method public c(ILandroid/support/v7/widget/RecyclerView$k;Landroid/support/v7/widget/RecyclerView$p;)Landroid/view/View;
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    .line 147
    invoke-virtual {v0, p1}, Laak;->a(I)I

    move-result v1

    .line 148
    iget-object v2, v0, Laak;->a:Laam;

    invoke-virtual {v2, v1}, Laam;->b(I)Landroid/view/View;

    move-result-object v2

    .line 149
    if-eqz v2, :cond_1

    .line 150
    iget-object v3, v0, Laak;->b:Laal;

    invoke-virtual {v3, v1}, Laal;->d(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 151
    invoke-virtual {v0, v2}, Laak;->b(Landroid/view/View;)Z

    .line 152
    :cond_0
    iget-object v0, v0, Laak;->a:Laam;

    invoke-virtual {v0, v1}, Laam;->a(I)V

    .line 153
    :cond_1
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->b(II)V

    .line 361
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 275
    sub-int v0, v1, v0

    return v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView$f;->e(I)Landroid/view/View;

    .line 170
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v0, p1}, Laak;->d(I)V

    .line 171
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 276
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 277
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 278
    sub-int v0, v1, v0

    return v0
.end method

.method public final e(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v0, p1}, Laak;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return v0
.end method

.method public f(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    .line 280
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    .line 281
    add-int/2addr v0, v1

    return v0
.end method

.method public g(Landroid/support/v7/widget/RecyclerView$p;)I
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method

.method public final g(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 282
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 283
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$g;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$g;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 284
    add-int/2addr v0, v1

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 71
    :cond_0
    return-void
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    .line 94
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->j(Landroid/view/View;)I

    move-result v0

    .line 95
    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v0}, Laak;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 182
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-object v0

    .line 184
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 185
    if-eqz v1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    invoke-virtual {v2, v1}, Laak;->d(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 187
    goto :goto_0
.end method
