.class public abstract Landroid/support/v7/widget/RecyclerView$d;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "d"
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;

.field public h:Lacr;

.field public i:J

.field public j:J

.field public k:J

.field public l:J


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    const-wide/16 v2, 0x78

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->h:Lacr;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->a:Ljava/util/ArrayList;

    .line 4
    iput-wide v2, p0, Landroid/support/v7/widget/RecyclerView$d;->i:J

    .line 5
    iput-wide v2, p0, Landroid/support/v7/widget/RecyclerView$d;->j:J

    .line 6
    iput-wide v4, p0, Landroid/support/v7/widget/RecyclerView$d;->k:J

    .line 7
    iput-wide v4, p0, Landroid/support/v7/widget/RecyclerView$d;->l:J

    .line 8
    return-void
.end method

.method static e(Landroid/support/v7/widget/RecyclerView$r;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 19
    .line 20
    iget v0, p0, Landroid/support/v7/widget/RecyclerView$r;->j:I

    .line 21
    and-int/lit8 v0, v0, 0xe

    .line 22
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$r;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23
    const/4 v0, 0x4

    .line 31
    :cond_0
    :goto_0
    return v0

    .line 24
    :cond_1
    and-int/lit8 v1, v0, 0x4

    if-nez v1, :cond_0

    .line 26
    iget v1, p0, Landroid/support/v7/widget/RecyclerView$r;->d:I

    .line 28
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$r;->d()I

    move-result v2

    .line 29
    if-eq v1, v3, :cond_0

    if-eq v2, v3, :cond_0

    if-eq v1, v2, :cond_0

    .line 30
    or-int/lit16 v0, v0, 0x800

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z
.end method

.method public abstract a(Landroid/support/v7/widget/RecyclerView$r;Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z
.end method

.method public a(Landroid/support/v7/widget/RecyclerView$r;Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView$d;->g(Landroid/support/v7/widget/RecyclerView$r;)Z

    move-result v0

    return v0
.end method

.method public abstract b()Z
.end method

.method public abstract b(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z
.end method

.method public abstract c(Landroid/support/v7/widget/RecyclerView$r;)V
.end method

.method public abstract c(Landroid/support/v7/widget/RecyclerView$r;Lacs;Lacs;)Z
.end method

.method public final d(Landroid/support/v7/widget/RecyclerView$r;)Lacs;
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lacs;

    invoke-direct {v0}, Lacs;-><init>()V

    .line 12
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    .line 13
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, v0, Lacs;->a:I

    .line 14
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, v0, Lacs;->b:I

    .line 15
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    .line 16
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    .line 18
    return-object v0
.end method

.method public abstract d()V
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 38
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 39
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 42
    return-void
.end method

.method public final f(Landroid/support/v7/widget/RecyclerView$r;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->h:Lacr;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$d;->h:Lacr;

    invoke-virtual {v0, p1}, Lacr;->a(Landroid/support/v7/widget/RecyclerView$r;)V

    .line 34
    :cond_0
    return-void
.end method

.method public g(Landroid/support/v7/widget/RecyclerView$r;)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method
