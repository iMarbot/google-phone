.class public Landroid/support/v7/widget/ContentFrameLayout$a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/ContentFrameLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Luw;


# direct methods
.method public constructor <init>(Luw;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Landroid/support/v7/widget/ContentFrameLayout$a;->a:Luw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1
    iget-object v0, p0, Landroid/support/v7/widget/ContentFrameLayout$a;->a:Luw;

    .line 2
    iget-object v1, v0, Luw;->q:Laan;

    if-eqz v1, :cond_0

    .line 3
    iget-object v1, v0, Luw;->q:Laan;

    invoke-interface {v1}, Laan;->i()V

    .line 4
    :cond_0
    iget-object v1, v0, Luw;->t:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_2

    .line 5
    iget-object v1, v0, Luw;->c:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Luw;->u:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 6
    iget-object v1, v0, Luw;->t:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7
    :try_start_0
    iget-object v1, v0, Luw;->t:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    :cond_1
    :goto_0
    const/4 v1, 0x0

    iput-object v1, v0, Luw;->t:Landroid/widget/PopupWindow;

    .line 11
    :cond_2
    invoke-virtual {v0}, Luw;->n()V

    .line 12
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Luw;->g(I)Lvv;

    move-result-object v0

    .line 13
    if-eqz v0, :cond_3

    iget-object v1, v0, Lvv;->h:Lxf;

    if-eqz v1, :cond_3

    .line 14
    iget-object v0, v0, Lvv;->h:Lxf;

    invoke-virtual {v0}, Lxf;->close()V

    .line 15
    :cond_3
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
