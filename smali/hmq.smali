.class abstract Lhmq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhme;


# direct methods
.method private constructor <init>(Lhme;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lhmq;->a:Lhme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhme;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lhmq;-><init>(Lhme;)V

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 2
    :try_start_0
    iget-object v0, p0, Lhmq;->a:Lhme;

    .line 3
    iget-object v0, v0, Lhme;->b:Lhnv;

    .line 4
    if-nez v0, :cond_0

    .line 5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to perform write due to unavailable frameWriter."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 8
    :catch_0
    move-exception v0

    .line 9
    iget-object v1, p0, Lhmq;->a:Lhme;

    .line 10
    iget-object v1, v1, Lhme;->d:Lhna;

    .line 11
    invoke-virtual {v1, v0}, Lhna;->a(Ljava/lang/Throwable;)V

    .line 17
    :goto_0
    return-void

    .line 6
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lhmq;->a()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 13
    :catch_1
    move-exception v0

    .line 14
    iget-object v1, p0, Lhmq;->a:Lhme;

    .line 15
    iget-object v1, v1, Lhme;->d:Lhna;

    .line 16
    invoke-virtual {v1, v0}, Lhna;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
