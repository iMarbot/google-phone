.class public final Ldsg;
.super Ldvp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldsg$b;,
        Ldsg$a;
    }
.end annotation


# static fields
.field private static j:Ljava/util/List;


# instance fields
.field public a:Z

.field public b:Ljava/util/Set;

.field public c:Z

.field public d:Z

.field public volatile e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ldsg;->j:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ldvb;)V
    .locals 1

    invoke-direct {p0, p1}, Ldvp;-><init>(Ldvb;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldsg;->b:Ljava/util/Set;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ldsg;
    .locals 1

    invoke-static {p0}, Ldvb;->a(Landroid/content/Context;)Ldvb;

    move-result-object v0

    invoke-virtual {v0}, Ldvb;->d()Ldsg;

    move-result-object v0

    return-object v0
.end method

.method public static a()V
    .locals 3

    const-class v1, Ldsg;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldsg;->j:Ljava/util/List;

    if-eqz v0, :cond_1

    sget-object v0, Ldsg;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    sput-object v0, Ldsg;->j:Ljava/util/List;

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(I)Ldsk;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1
    monitor-enter p0

    :try_start_0
    new-instance v3, Ldsk;

    .line 2
    iget-object v0, p0, Ldvp;->f:Ldvb;

    .line 3
    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Ldsk;-><init>(Ldvb;Ljava/lang/String;)V

    if-lez p1, :cond_6

    new-instance v0, Ldtq;

    .line 4
    iget-object v4, p0, Ldvp;->f:Ldvb;

    .line 5
    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Ldtq;-><init>(Ldvb;B)V

    invoke-virtual {v0, p1}, Ldtq;->a(I)Ldtp;

    move-result-object v0

    check-cast v0, Ldur;

    if-eqz v0, :cond_6

    .line 6
    const-string v4, "Loading Tracker config values"

    invoke-virtual {v3, v4}, Lduy;->a(Ljava/lang/String;)V

    iput-object v0, v3, Ldsk;->e:Ldur;

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget-object v0, v0, Ldur;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget-object v0, v0, Ldur;->a:Ljava/lang/String;

    const-string v4, "&tid"

    invoke-virtual {v3, v4, v0}, Ldsk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "trackingId loaded"

    invoke-virtual {v3, v4, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, v3, Ldsk;->e:Ldur;

    iget-wide v4, v0, Ldur;->b:D

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget-wide v4, v0, Ldur;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    const-string v4, "&sf"

    invoke-virtual {v3, v4, v0}, Ldsk;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Sample frequency loaded"

    invoke-virtual {v3, v4, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->c:I

    if-ltz v0, :cond_9

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->c:I

    int-to-long v4, v0

    .line 7
    iget-object v6, v3, Ldsk;->d:Ldsl;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    .line 8
    iput-wide v4, v6, Ldsl;->b:J

    invoke-virtual {v6}, Ldsl;->c()V

    .line 9
    const-string v4, "Session timeout loaded"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->d:I

    if-eq v0, v10, :cond_3

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->d:I

    if-ne v0, v1, :cond_a

    move v0, v1

    .line 10
    :goto_3
    iget-object v4, v3, Ldsk;->d:Ldsl;

    .line 11
    iput-boolean v0, v4, Ldsl;->a:Z

    invoke-virtual {v4}, Ldsl;->c()V

    .line 12
    const-string v4, "Auto activity tracking loaded"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->e:I

    if-eq v0, v10, :cond_5

    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->e:I

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    const-string v4, "&aip"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Ldsk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v4, "Anonymize ip loaded"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_5
    iget-object v0, v3, Ldsk;->e:Ldur;

    iget v0, v0, Ldur;->f:I

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Ldsk;->a(Z)V

    .line 13
    :cond_6
    invoke-virtual {v3}, Lduz;->n()V

    monitor-exit p0

    return-object v3

    :cond_7
    move v0, v2

    .line 6
    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    move v0, v2

    goto :goto_2

    :cond_a
    move v0, v2

    .line 9
    goto :goto_3

    :cond_b
    move v0, v2

    .line 12
    goto :goto_4

    :cond_c
    move v0, v2

    goto :goto_5

    .line 13
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
