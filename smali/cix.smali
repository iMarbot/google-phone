.class public final Lcix;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcgn;

.field private b:Lcjl;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:Z

.field private g:Lcgr;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Lcgn;Lcjl;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcix;->c:Landroid/view/View;

    .line 3
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcix;->d:Landroid/view/View;

    .line 5
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgn;

    iput-object v0, p0, Lcix;->a:Lcgn;

    .line 6
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjl;

    iput-object v0, p0, Lcix;->b:Lcjl;

    .line 7
    return-void
.end method

.method private final c()Z
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcix;->g:Lcgr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcix;->g:Lcgr;

    iget-boolean v0, v0, Lcgr;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcix;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcix;->e:Z

    .line 15
    invoke-virtual {p0}, Lcix;->b()V

    .line 16
    return-void
.end method

.method public final a(Lcgr;)V
    .locals 1

    .prologue
    .line 17
    iput-object p1, p0, Lcix;->g:Lcgr;

    .line 18
    invoke-direct {p0}, Lcix;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcix;->e:Z

    .line 19
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 8
    iput-boolean p1, p0, Lcix;->f:Z

    .line 9
    invoke-virtual {p0}, Lcix;->b()V

    .line 10
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 21
    iget-object v0, p0, Lcix;->c:Landroid/view/View;

    iget-boolean v3, p0, Lcix;->f:Z

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 22
    iget-object v3, p0, Lcix;->c:Landroid/view/View;

    iget-boolean v0, p0, Lcix;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 23
    iget-object v0, p0, Lcix;->d:Landroid/view/View;

    iget-boolean v3, p0, Lcix;->e:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    return-void

    :cond_0
    move v0, v2

    .line 22
    goto :goto_0

    :cond_1
    move v1, v2

    .line 23
    goto :goto_1
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 11
    iput-boolean p1, p0, Lcix;->e:Z

    .line 12
    invoke-virtual {p0}, Lcix;->b()V

    .line 13
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcix;->a:Lcgn;

    invoke-interface {v0}, Lcgn;->n()V

    .line 26
    iget-object v0, p0, Lcix;->b:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    .line 27
    return-void
.end method
