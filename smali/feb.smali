.class public final Lfeb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfey;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lfex;

.field private c:Lfiv;

.field private d:Lfga;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfeb;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 47
    const-string v0, "IncomingHangoutsCallController.addNewIncomingCall"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lfeb;->a:Landroid/content/Context;

    const-string v1, "telecom"

    .line 49
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 50
    const-string v1, "tel"

    .line 51
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lfeb;->d:Lfga;

    invoke-static {v2}, Lfmd;->a(Lfga;)Landroid/os/Bundle;

    move-result-object v2

    .line 53
    if-eqz v1, :cond_0

    .line 54
    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v1

    .line 57
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x7b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "IncomingHangoutsCallController.addNewIncomingCall, adding call with SIM account failed, trying non-SIM account, exception: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :cond_0
    iget-object v1, p0, Lfeb;->a:Landroid/content/Context;

    .line 59
    invoke-static {v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1, v2}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfez;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    const-string v0, "IncomingHangoutsCallController.onNetworkSelectionStateFetched"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    iget-object v0, p0, Lfeb;->d:Lfga;

    iget-object v1, p1, Lfez;->c:Lffb;

    invoke-virtual {v1}, Lffb;->e()Lfgb;

    move-result-object v1

    iput-object v1, v0, Lfga;->f:Lfgb;

    .line 35
    iget-object v0, p0, Lfeb;->a:Landroid/content/Context;

    .line 36
    sget-object v1, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 37
    invoke-static {v1}, Lfds;->a(Landroid/telecom/ConnectionService;)Z

    move-result v1

    .line 38
    invoke-static {v0, p1, v3, v1}, Lfmd;->a(Landroid/content/Context;Lfez;ZZ)Lfew;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lfeb;->d:Lfga;

    iget-boolean v2, v0, Lfew;->b:Z

    iput-boolean v2, v1, Lfga;->g:Z

    .line 40
    iget-object v1, p0, Lfeb;->a:Landroid/content/Context;

    iget-object v2, p0, Lfeb;->d:Lfga;

    invoke-static {v1, v2}, Lfmd;->b(Landroid/content/Context;Lfga;)V

    .line 41
    iget-boolean v0, v0, Lfew;->a:Z

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lfeb;->a()V

    .line 45
    :goto_0
    invoke-virtual {p0}, Lfeb;->b()V

    .line 46
    return-void

    .line 43
    :cond_0
    const-string v0, "IncomingHangoutsCallController.onNetworkSelectionStateFetched, waiting for PSTN"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lfeb;->a:Landroid/content/Context;

    iget-object v1, p0, Lfeb;->d:Lfga;

    invoke-static {v0, v1}, Lfmd;->a(Landroid/content/Context;Lfga;)Z

    goto :goto_0
.end method

.method public final a(Lfiv;Lfga;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "IncomingHangoutsCallController.onFallbackToWifi, invite: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lfeb;->a:Landroid/content/Context;

    iget-object v1, p0, Lfeb;->a:Landroid/content/Context;

    .line 7
    const/4 v2, -0x1

    .line 8
    invoke-static {v1, v3, v2}, Lfem;->a(Landroid/content/Context;II)Lfeo;

    move-result-object v2

    .line 9
    new-instance v3, Lffa;

    invoke-direct {v3}, Lffa;-><init>()V

    .line 12
    iput-object v2, v3, Lffa;->a:Lfeo;

    .line 15
    invoke-static {v1}, Lfft;->a(Landroid/content/Context;)Lffy;

    move-result-object v1

    .line 16
    iput-object v1, v3, Lffa;->b:Lffy;

    .line 18
    iget-object v1, p2, Lfga;->f:Lfgb;

    .line 19
    invoke-static {v1}, Lffb;->a(Lfgb;)Lffb;

    move-result-object v1

    .line 20
    iput-object v1, v3, Lffa;->c:Lffb;

    .line 22
    invoke-virtual {v3}, Lffa;->a()Lfez;

    move-result-object v1

    .line 23
    const/4 v2, 0x1

    .line 24
    sget-object v3, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 25
    invoke-static {v3}, Lfds;->a(Landroid/telecom/ConnectionService;)Z

    move-result v3

    .line 26
    invoke-static {v0, v1, v2, v3}, Lfmd;->a(Landroid/content/Context;Lfez;ZZ)Lfew;

    move-result-object v0

    .line 27
    iget-boolean v1, v0, Lfew;->b:Z

    iput-boolean v1, p2, Lfga;->g:Z

    .line 28
    invoke-virtual {p0, p1, p2}, Lfeb;->b(Lfiv;Lfga;)V

    .line 29
    iget-boolean v0, v0, Lfew;->a:Z

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0}, Lfeb;->a()V

    .line 31
    :cond_0
    invoke-virtual {p0}, Lfeb;->b()V

    .line 32
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    const-string v0, "IncomingHangoutsCallController.cleanup"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lfeb;->b:Lfex;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lfeb;->b:Lfex;

    invoke-virtual {v0}, Lfex;->a()V

    .line 69
    iput-object v2, p0, Lfeb;->b:Lfex;

    .line 70
    :cond_0
    iget-object v0, p0, Lfeb;->c:Lfiv;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lfeb;->c:Lfiv;

    invoke-virtual {v0}, Lfiv;->b()V

    .line 72
    iput-object v2, p0, Lfeb;->c:Lfiv;

    .line 73
    :cond_1
    return-void
.end method

.method public final b(Lfiv;Lfga;)V
    .locals 1

    .prologue
    .line 62
    iput-object p1, p0, Lfeb;->c:Lfiv;

    .line 63
    iput-object p2, p0, Lfeb;->d:Lfga;

    .line 64
    iget-object v0, p0, Lfeb;->c:Lfiv;

    invoke-virtual {v0}, Lfiv;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 65
    return-void
.end method
