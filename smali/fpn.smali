.class final Lfpn;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field public batteryLevel:I

.field public isOnBattery:Z


# direct methods
.method public constructor <init>(Lfpm;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpn;->isOnBattery:Z

    .line 3
    const/4 v0, -0x1

    iput v0, p0, Lfpn;->batteryLevel:I

    .line 4
    return-void
.end method


# virtual methods
.method public final getBatteryLevel()I
    .locals 1

    .prologue
    .line 6
    iget v0, p0, Lfpn;->batteryLevel:I

    return v0
.end method

.method public final getIsOnBattery()Z
    .locals 1

    .prologue
    .line 5
    iget-boolean v0, p0, Lfpn;->isOnBattery:Z

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7
    const-string v0, "plugged"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 8
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfpn;->isOnBattery:Z

    .line 9
    const-string v0, "scale"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 10
    const-string v2, "level"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 11
    if-eqz v0, :cond_1

    .line 12
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    iput v0, p0, Lfpn;->batteryLevel:I

    .line 14
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 8
    goto :goto_0

    .line 13
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lfpn;->batteryLevel:I

    goto :goto_1
.end method
