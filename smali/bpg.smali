.class abstract Lbpg;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/content/pm/ShortcutInfo;)Landroid/net/Uri;
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/content/pm/ShortcutInfo;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "contactId"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 4
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 5
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "No contact ID found for shortcut: "

    invoke-virtual {p0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 7
    :cond_1
    invoke-virtual {p0}, Landroid/content/pm/ShortcutInfo;->getId()Ljava/lang/String;

    move-result-object v2

    .line 9
    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static f()Lbph;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lbph;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lbph;-><init>(B)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lbph;->a(I)Lbph;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a()J
.end method

.method abstract b()Ljava/lang/String;
.end method

.method final b(Landroid/content/pm/ShortcutInfo;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p0}, Lbpg;->d()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/content/pm/ShortcutInfo;->getRank()I

    move-result v1

    invoke-virtual {p0}, Lbpg;->d()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 21
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    invoke-virtual {p1}, Landroid/content/pm/ShortcutInfo;->getShortLabel()Ljava/lang/CharSequence;

    move-result-object v1

    .line 14
    invoke-virtual {p0}, Lbpg;->c()Ljava/lang/String;

    move-result-object v2

    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17
    invoke-virtual {p1}, Landroid/content/pm/ShortcutInfo;->getLongLabel()Ljava/lang/CharSequence;

    move-result-object v1

    .line 18
    invoke-virtual {p0}, Lbpg;->c()Ljava/lang/String;

    move-result-object v2

    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract c()Ljava/lang/String;
.end method

.method abstract d()I
.end method

.method final e()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 10
    invoke-virtual {p0}, Lbpg;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lbpg;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
