.class public final enum Lgyq;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lgyq;

.field public static final enum b:Lgyq;

.field public static final enum c:Lgyq;

.field private static enum d:Lgyq;

.field private static enum e:Lgyq;

.field private static synthetic f:[Lgyq;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lgyq;

    const-string v1, "SCALAR"

    invoke-direct {v0, v1, v2}, Lgyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyq;->a:Lgyq;

    .line 4
    new-instance v0, Lgyq;

    const-string v1, "BITS"

    invoke-direct {v0, v1, v3}, Lgyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyq;->d:Lgyq;

    .line 5
    new-instance v0, Lgyq;

    const-string v1, "BYTES"

    invoke-direct {v0, v1, v4}, Lgyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyq;->b:Lgyq;

    .line 6
    new-instance v0, Lgyq;

    const-string v1, "SECONDS"

    invoke-direct {v0, v1, v5}, Lgyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyq;->c:Lgyq;

    .line 7
    new-instance v0, Lgyq;

    const-string v1, "CORES"

    invoke-direct {v0, v1, v6}, Lgyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyq;->e:Lgyq;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lgyq;

    sget-object v1, Lgyq;->a:Lgyq;

    aput-object v1, v0, v2

    sget-object v1, Lgyq;->d:Lgyq;

    aput-object v1, v0, v3

    sget-object v1, Lgyq;->b:Lgyq;

    aput-object v1, v0, v4

    sget-object v1, Lgyq;->c:Lgyq;

    aput-object v1, v0, v5

    sget-object v1, Lgyq;->e:Lgyq;

    aput-object v1, v0, v6

    sput-object v0, Lgyq;->f:[Lgyq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgyq;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgyq;->f:[Lgyq;

    invoke-virtual {v0}, [Lgyq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgyq;

    return-object v0
.end method
