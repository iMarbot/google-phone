.class public final Lfi;
.super Lgi;
.source "PG"


# static fields
.field private static i:[Ljava/lang/String;

.field private static j:Landroid/util/Property;

.field private static k:Landroid/util/Property;

.field private static l:Landroid/util/Property;

.field private static m:Landroid/util/Property;

.field private static n:Landroid/util/Property;

.field private static o:Landroid/util/Property;

.field private static s:Lgg;


# instance fields
.field private p:[I

.field private q:Z

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android:changeBounds:bounds"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android:changeBounds:clip"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android:changeBounds:parent"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android:changeBounds:windowX"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android:changeBounds:windowY"

    aput-object v2, v0, v1

    sput-object v0, Lfi;->i:[Ljava/lang/String;

    .line 96
    new-instance v0, Lfj;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "boundsOrigin"

    invoke-direct {v0, v1, v2}, Lfj;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->j:Landroid/util/Property;

    .line 97
    new-instance v0, Lfk;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "topLeft"

    invoke-direct {v0, v1, v2}, Lfk;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->k:Landroid/util/Property;

    .line 98
    new-instance v0, Lfl;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "bottomRight"

    invoke-direct {v0, v1, v2}, Lfl;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->l:Landroid/util/Property;

    .line 99
    new-instance v0, Lfm;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "bottomRight"

    invoke-direct {v0, v1, v2}, Lfm;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->m:Landroid/util/Property;

    .line 100
    new-instance v0, Lfn;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "topLeft"

    invoke-direct {v0, v1, v2}, Lfn;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->n:Landroid/util/Property;

    .line 101
    new-instance v0, Lfo;

    const-class v1, Landroid/graphics/PointF;

    const-string v2, "position"

    invoke-direct {v0, v1, v2}, Lfo;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lfi;->o:Landroid/util/Property;

    .line 102
    new-instance v0, Lgg;

    invoke-direct {v0}, Lgg;-><init>()V

    sput-object v0, Lfi;->s:Lgg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lgi;-><init>()V

    .line 2
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lfi;->p:[I

    .line 3
    iput-boolean v1, p0, Lfi;->q:Z

    .line 4
    iput-boolean v1, p0, Lfi;->r:Z

    .line 5
    return-void
.end method

.method private final d(Lgv;)V
    .locals 7

    .prologue
    .line 7
    iget-object v0, p1, Lgv;->b:Landroid/view/View;

    .line 9
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->q(Landroid/view/View;)Z

    move-result v1

    .line 10
    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    :cond_0
    iget-object v1, p1, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:changeBounds:bounds"

    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    .line 12
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-direct {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 13
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    iget-object v0, p1, Lgv;->a:Ljava/util/Map;

    const-string v1, "android:changeBounds:parent"

    iget-object v2, p1, Lgv;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lgv;Lgv;)Landroid/animation/Animator;
    .locals 18

    .prologue
    .line 20
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 94
    :cond_1
    :goto_0
    return-object v2

    .line 22
    :cond_2
    move-object/from16 v0, p2

    iget-object v1, v0, Lgv;->a:Ljava/util/Map;

    .line 23
    move-object/from16 v0, p3

    iget-object v2, v0, Lgv;->a:Ljava/util/Map;

    .line 24
    const-string v3, "android:changeBounds:parent"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 25
    const-string v3, "android:changeBounds:parent"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 26
    if-eqz v1, :cond_3

    if-nez v2, :cond_4

    .line 27
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 28
    :cond_4
    move-object/from16 v0, p3

    iget-object v4, v0, Lgv;->b:Landroid/view/View;

    .line 30
    move-object/from16 v0, p2

    iget-object v1, v0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:changeBounds:bounds"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 31
    move-object/from16 v0, p3

    iget-object v2, v0, Lgv;->a:Ljava/util/Map;

    const-string v3, "android:changeBounds:bounds"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 32
    iget v5, v1, Landroid/graphics/Rect;->left:I

    .line 33
    iget v6, v2, Landroid/graphics/Rect;->left:I

    .line 34
    iget v7, v1, Landroid/graphics/Rect;->top:I

    .line 35
    iget v8, v2, Landroid/graphics/Rect;->top:I

    .line 36
    iget v9, v1, Landroid/graphics/Rect;->right:I

    .line 37
    iget v10, v2, Landroid/graphics/Rect;->right:I

    .line 38
    iget v11, v1, Landroid/graphics/Rect;->bottom:I

    .line 39
    iget v12, v2, Landroid/graphics/Rect;->bottom:I

    .line 40
    sub-int v13, v9, v5

    .line 41
    sub-int v14, v11, v7

    .line 42
    sub-int v15, v10, v6

    .line 43
    sub-int v16, v12, v8

    .line 44
    move-object/from16 v0, p2

    iget-object v1, v0, Lgv;->a:Ljava/util/Map;

    const-string v2, "android:changeBounds:clip"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 45
    move-object/from16 v0, p3

    iget-object v2, v0, Lgv;->a:Ljava/util/Map;

    const-string v3, "android:changeBounds:clip"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 46
    const/4 v3, 0x0

    .line 47
    if-eqz v13, :cond_5

    if-nez v14, :cond_6

    :cond_5
    if-eqz v15, :cond_a

    if-eqz v16, :cond_a

    .line 48
    :cond_6
    if-ne v5, v6, :cond_7

    if-eq v7, v8, :cond_8

    :cond_7
    const/4 v3, 0x1

    .line 49
    :cond_8
    if-ne v9, v10, :cond_9

    if-eq v11, v12, :cond_a

    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 50
    :cond_a
    if-eqz v1, :cond_b

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    :cond_b
    if-nez v1, :cond_12

    if-eqz v2, :cond_12

    .line 51
    :cond_c
    add-int/lit8 v1, v3, 0x1

    .line 52
    :goto_1
    if-lez v1, :cond_11

    .line 53
    invoke-static {v4, v5, v7, v9, v11}, Lhg;->a(Landroid/view/View;IIII)V

    .line 54
    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    .line 55
    if-ne v13, v15, :cond_d

    move/from16 v0, v16

    if-ne v14, v0, :cond_d

    .line 57
    move-object/from16 v0, p0

    iget-object v1, v0, Lgi;->h:Lge;

    .line 58
    int-to-float v2, v5

    int-to-float v3, v7

    int-to-float v5, v6

    int-to-float v6, v8

    invoke-virtual {v1, v2, v3, v5, v6}, Lge;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 59
    sget-object v2, Lfi;->o:Landroid/util/Property;

    invoke-static {v4, v2, v1}, Lga;->a(Ljava/lang/Object;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    move-object v2, v1

    .line 87
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 88
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 89
    const/4 v3, 0x1

    invoke-static {v1, v3}, Lha;->a(Landroid/view/ViewGroup;Z)V

    .line 90
    new-instance v3, Lfq;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v1}, Lfq;-><init>(Lfi;Landroid/view/ViewGroup;)V

    .line 91
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lfi;->a(Lgn;)Lgi;

    goto/16 :goto_0

    .line 61
    :cond_d
    new-instance v2, Lfr;

    invoke-direct {v2, v4}, Lfr;-><init>(Landroid/view/View;)V

    .line 63
    move-object/from16 v0, p0

    iget-object v1, v0, Lgi;->h:Lge;

    .line 64
    int-to-float v3, v5

    int-to-float v5, v7

    int-to-float v6, v6

    int-to-float v7, v8

    invoke-virtual {v1, v3, v5, v6, v7}, Lge;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 65
    sget-object v3, Lfi;->k:Landroid/util/Property;

    .line 66
    invoke-static {v2, v3, v1}, Lga;->a(Ljava/lang/Object;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 68
    move-object/from16 v0, p0

    iget-object v1, v0, Lgi;->h:Lge;

    .line 69
    int-to-float v5, v9

    int-to-float v6, v11

    int-to-float v7, v10

    int-to-float v8, v12

    invoke-virtual {v1, v5, v6, v7, v8}, Lge;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 70
    sget-object v5, Lfi;->l:Landroid/util/Property;

    invoke-static {v2, v5, v1}, Lga;->a(Ljava/lang/Object;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 71
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 72
    const/4 v6, 0x2

    new-array v6, v6, [Landroid/animation/Animator;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v5, v6, v3

    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 74
    new-instance v3, Lfp;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lfp;-><init>(Lfi;Lfr;)V

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v2, v1

    .line 75
    goto :goto_2

    .line 76
    :cond_e
    if-ne v5, v6, :cond_f

    if-eq v7, v8, :cond_10

    .line 78
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lgi;->h:Lge;

    .line 79
    int-to-float v2, v5

    int-to-float v3, v7

    int-to-float v5, v6

    int-to-float v6, v8

    invoke-virtual {v1, v2, v3, v5, v6}, Lge;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 80
    sget-object v2, Lfi;->n:Landroid/util/Property;

    invoke-static {v4, v2, v1}, Lga;->a(Ljava/lang/Object;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    move-object v2, v1

    .line 81
    goto :goto_2

    .line 83
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lgi;->h:Lge;

    .line 84
    int-to-float v2, v9

    int-to-float v3, v11

    int-to-float v5, v10

    int-to-float v6, v12

    invoke-virtual {v1, v2, v3, v5, v6}, Lge;->a(FFFF)Landroid/graphics/Path;

    move-result-object v1

    .line 85
    sget-object v2, Lfi;->m:Landroid/util/Property;

    invoke-static {v4, v2, v1}, Lga;->a(Ljava/lang/Object;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    move-object v2, v1

    .line 86
    goto/16 :goto_2

    .line 94
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_12
    move v1, v3

    goto/16 :goto_1
.end method

.method public final a(Lgv;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lfi;->d(Lgv;)V

    .line 17
    return-void
.end method

.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lfi;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lgv;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lfi;->d(Lgv;)V

    .line 19
    return-void
.end method
