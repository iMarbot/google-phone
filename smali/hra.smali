.class public final Lhra;
.super Lhft;
.source "PG"


# static fields
.field private static volatile d:[Lhra;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    iput-object v0, p0, Lhra;->a:Ljava/lang/Integer;

    .line 9
    iput-object v0, p0, Lhra;->b:Ljava/lang/String;

    .line 10
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lhra;->c:[I

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lhra;->cachedSize:I

    .line 12
    return-void
.end method

.method public static a()[Lhra;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lhra;->d:[Lhra;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lhra;->d:[Lhra;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lhra;

    sput-object v0, Lhra;->d:[Lhra;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lhra;->d:[Lhra;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 24
    iget-object v2, p0, Lhra;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 25
    const/4 v2, 0x1

    iget-object v3, p0, Lhra;->a:Ljava/lang/Integer;

    .line 26
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 27
    :cond_0
    iget-object v2, p0, Lhra;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 28
    const/4 v2, 0x2

    iget-object v3, p0, Lhra;->b:Ljava/lang/String;

    .line 29
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 30
    :cond_1
    iget-object v2, p0, Lhra;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhra;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 32
    :goto_0
    iget-object v3, p0, Lhra;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 33
    iget-object v3, p0, Lhra;->c:[I

    aget v3, v3, v1

    .line 35
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_2
    add-int/2addr v0, v2

    .line 38
    iget-object v1, p0, Lhra;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 40
    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :sswitch_0
    return-object p0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 48
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhra;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 50
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhra;->b:Ljava/lang/String;

    goto :goto_0

    .line 52
    :sswitch_3
    const/16 v0, 0x28

    .line 53
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 54
    iget-object v0, p0, Lhra;->c:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 55
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 56
    if-eqz v0, :cond_1

    .line 57
    iget-object v3, p0, Lhra;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 60
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 61
    aput v3, v2, v0

    .line 62
    invoke-virtual {p1}, Lhfp;->a()I

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 54
    :cond_2
    iget-object v0, p0, Lhra;->c:[I

    array-length v0, v0

    goto :goto_1

    .line 65
    :cond_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 66
    aput v3, v2, v0

    .line 67
    iput-object v2, p0, Lhra;->c:[I

    goto :goto_0

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 70
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 72
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 73
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 75
    invoke-virtual {p1}, Lhfp;->g()I

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 78
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 79
    iget-object v2, p0, Lhra;->c:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 80
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 81
    if-eqz v2, :cond_5

    .line 82
    iget-object v4, p0, Lhra;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 85
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 86
    aput v4, v0, v2

    .line 87
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 79
    :cond_6
    iget-object v2, p0, Lhra;->c:[I

    array-length v2, v2

    goto :goto_4

    .line 88
    :cond_7
    iput-object v0, p0, Lhra;->c:[I

    .line 89
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x28 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 13
    iget-object v0, p0, Lhra;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v1, p0, Lhra;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 15
    :cond_0
    iget-object v0, p0, Lhra;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lhra;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 17
    :cond_1
    iget-object v0, p0, Lhra;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhra;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 18
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhra;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 19
    const/4 v1, 0x5

    iget-object v2, p0, Lhra;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 22
    return-void
.end method
