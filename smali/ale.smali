.class public final Lale;
.super Laks;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Laks;-><init>(Landroid/content/ContentValues;)V

    .line 2
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3
    .line 4
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    .line 5
    const-string v1, "data1"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6
    .line 7
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    .line 8
    const-string v1, "data3"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Laks;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 9
    instance-of v2, p1, Lale;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lale;->b:Lakt;

    if-eqz v2, :cond_0

    .line 10
    iget-object v2, p1, Laks;->b:Lakt;

    .line 11
    if-nez v2, :cond_1

    .line 29
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    check-cast p1, Lale;

    .line 14
    invoke-direct {p0}, Lale;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1}, Lale;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16
    iget-object v2, p0, Lale;->b:Lakt;

    invoke-virtual {p0, v2}, Lale;->a(Lakt;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 17
    iget-object v2, p1, Laks;->b:Lakt;

    .line 18
    invoke-virtual {p1, v2}, Lale;->a(Lakt;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 19
    :cond_2
    iget-object v2, p0, Lale;->b:Lakt;

    invoke-virtual {p0, v2}, Lale;->a(Lakt;)Z

    move-result v2

    .line 20
    iget-object v3, p1, Laks;->b:Lakt;

    .line 21
    invoke-virtual {p1, v3}, Lale;->a(Lakt;)Z

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 22
    :cond_3
    iget-object v2, p0, Lale;->b:Lakt;

    invoke-virtual {p0, v2}, Lale;->b(Lakt;)I

    move-result v2

    .line 23
    iget-object v3, p1, Laks;->b:Lakt;

    .line 24
    invoke-virtual {p1, v3}, Lale;->b(Lakt;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 26
    iget-object v2, p0, Lale;->b:Lakt;

    invoke-virtual {p0, v2}, Lale;->b(Lakt;)I

    move-result v2

    if-nez v2, :cond_4

    .line 27
    invoke-direct {p0}, Lale;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1}, Lale;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 29
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 30
    check-cast p1, Laks;

    invoke-virtual {p0, p1, p2}, Lale;->a(Laks;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
