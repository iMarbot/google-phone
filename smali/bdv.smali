.class final Lbdv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbdy;


# instance fields
.field private a:Lben;


# direct methods
.method constructor <init>(Lben;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbdv;->a:Lben;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lbdv;->a:Lben;

    .line 5
    iget-object v1, v0, Lben;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lbeo;

    invoke-direct {v2, v0, p1}, Lbeo;-><init>(Lben;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 6
    return-void
.end method

.method public final a(Ljava/lang/Object;J)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 7
    iget-object v0, p0, Lbdv;->a:Lben;

    const-wide/16 v2, 0x64

    .line 8
    iget-object v1, v0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    .line 9
    const-string v1, "DialerUiTaskFragment.executeSerialWithWait"

    const-string v4, "cancelling waiting task"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    iget-object v1, v0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 11
    :cond_0
    iget-object v1, v0, Lben;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lbep;

    invoke-direct {v4, v0, p1}, Lbep;-><init>(Lben;Ljava/lang/Object;)V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 12
    invoke-interface {v1, v4, v2, v3, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    iput-object v1, v0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 13
    return-void
.end method

.method public final a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 17
    iget-object v1, p0, Lbdv;->a:Lben;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    .line 18
    new-instance v2, Lber;

    invoke-direct {v2, v1, p2}, Lber;-><init>(Lben;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 19
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lbdv;->a:Lben;

    .line 15
    iget-object v1, v0, Lben;->e:Ljava/util/concurrent/Executor;

    new-instance v2, Lbeq;

    invoke-direct {v2, v0, p1}, Lbeq;-><init>(Lben;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 16
    return-void
.end method
