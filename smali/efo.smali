.class final Lefo;
.super Ljava/lang/Object;

# interfaces
.implements Legl;
.implements Lejh;


# instance fields
.field public final a:Ledd;

.field public final b:Legz;

.field public c:Z

.field public final synthetic d:Lefj;

.field private e:Lcom/google/android/gms/common/internal/zzam;

.field private f:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lefj;Ledd;Legz;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lefo;->d:Lefj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lefo;->e:Lcom/google/android/gms/common/internal/zzam;

    iput-object v0, p0, Lefo;->f:Ljava/util/Set;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefo;->c:Z

    iput-object p2, p0, Lefo;->a:Ledd;

    iput-object p3, p0, Lefo;->b:Legz;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    iget-boolean v0, p0, Lefo;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefo;->e:Lcom/google/android/gms/common/internal/zzam;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefo;->a:Ledd;

    iget-object v1, p0, Lefo;->e:Lcom/google/android/gms/common/internal/zzam;

    iget-object v2, p0, Lefo;->f:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Ledd;->a(Lcom/google/android/gms/common/internal/zzam;Ljava/util/Set;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/internal/zzam;Ljava/util/Set;)V
    .locals 3

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const-string v0, "GoogleApiManager"

    const-string v1, "Received null response from onSignInSuccess"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lecl;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lecl;-><init>(I)V

    invoke-virtual {p0, v0}, Lefo;->b(Lecl;)V

    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lefo;->e:Lcom/google/android/gms/common/internal/zzam;

    iput-object p2, p0, Lefo;->f:Ljava/util/Set;

    invoke-virtual {p0}, Lefo;->a()V

    goto :goto_0
.end method

.method public final a(Lecl;)V
    .locals 2

    iget-object v0, p0, Lefo;->d:Lefj;

    invoke-static {v0}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lefp;

    invoke-direct {v1, p0, p1}, Lefp;-><init>(Lefo;Lecl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Lecl;)V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lefo;->d:Lefj;

    invoke-static {v0}, Lefj;->j(Lefj;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lefo;->b:Legz;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    .line 2
    iget-object v1, v0, Lefk;->h:Lefj;

    invoke-static {v1}, Lefj;->a(Lefj;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1}, Letf;->a(Landroid/os/Handler;)V

    iget-object v1, v0, Lefk;->a:Ledd;

    invoke-interface {v1}, Ledd;->e()V

    invoke-virtual {v0, p1}, Lefk;->onConnectionFailed(Lecl;)V

    .line 3
    return-void
.end method
