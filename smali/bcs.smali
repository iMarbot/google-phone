.class public Lbcs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field public c:Lamg;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/Integer;

.field public r:Ljava/lang/Boolean;

.field public s:Ljava/lang/Boolean;

.field public t:Ljava/lang/Integer;

.field public u:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lbcs;-><init>()V

    .line 88
    return-void
.end method


# virtual methods
.method public a()Lbcr;
    .locals 26

    .prologue
    .line 46
    const-string v2, ""

    .line 47
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->a:Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 48
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->b:Ljava/lang/Long;

    if-nez v3, :cond_1

    .line 50
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " timestamp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->c:Lamg;

    if-nez v3, :cond_2

    .line 52
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " number"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->g:Ljava/lang/Long;

    if-nez v3, :cond_3

    .line 54
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " photoId"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->j:Ljava/lang/Boolean;

    if-nez v3, :cond_4

    .line 56
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " isRead"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 57
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->k:Ljava/lang/Boolean;

    if-nez v3, :cond_5

    .line 58
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " isNew"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->p:Ljava/lang/Integer;

    if-nez v3, :cond_6

    .line 60
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " phoneAccountColor"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->q:Ljava/lang/Integer;

    if-nez v3, :cond_7

    .line 62
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " features"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->r:Ljava/lang/Boolean;

    if-nez v3, :cond_8

    .line 64
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " isBusiness"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 65
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->s:Ljava/lang/Boolean;

    if-nez v3, :cond_9

    .line 66
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " isVoicemail"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->t:Ljava/lang/Integer;

    if-nez v3, :cond_a

    .line 68
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " callType"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 69
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->u:Ljava/lang/Integer;

    if-nez v3, :cond_b

    .line 70
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " numberCalls"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 71
    :cond_b
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_d

    .line 72
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Missing required properties:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_c
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_d
    new-instance v2, Lbcq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbcs;->a:Ljava/lang/Integer;

    .line 74
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbcs;->b:Ljava/lang/Long;

    .line 75
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lbcs;->c:Lamg;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbcs;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbcs;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbcs;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbcs;->g:Ljava/lang/Long;

    .line 76
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lbcs;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbcs;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbcs;->j:Ljava/lang/Boolean;

    .line 77
    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lbcs;->k:Ljava/lang/Boolean;

    .line 78
    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->l:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->m:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->n:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->o:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->p:Ljava/lang/Integer;

    move-object/from16 v20, v0

    .line 79
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->q:Ljava/lang/Integer;

    move-object/from16 v21, v0

    .line 80
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->r:Ljava/lang/Boolean;

    move-object/from16 v22, v0

    .line 81
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->s:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    .line 82
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->t:Ljava/lang/Integer;

    move-object/from16 v24, v0

    .line 83
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcs;->u:Ljava/lang/Integer;

    move-object/from16 v25, v0

    .line 84
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .line 85
    invoke-direct/range {v2 .. v25}, Lbcq;-><init>(IJLamg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZII)V

    .line 86
    return-object v2
.end method

.method public a(I)Lbcs;
    .locals 1

    .prologue
    .line 2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcs;->a:Ljava/lang/Integer;

    .line 3
    return-object p0
.end method

.method public a(J)Lbcs;
    .locals 1

    .prologue
    .line 4
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbcs;->b:Ljava/lang/Long;

    .line 5
    return-object p0
.end method

.method public a(Lamg;)Lbcs;
    .locals 2

    .prologue
    .line 6
    if-nez p1, :cond_0

    .line 7
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null number"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    iput-object p1, p0, Lbcs;->c:Lamg;

    .line 9
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lbcs;->d:Ljava/lang/String;

    .line 11
    return-object p0
.end method

.method public a(Z)Lbcs;
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbcs;->j:Ljava/lang/Boolean;

    .line 23
    return-object p0
.end method

.method public b(I)Lbcs;
    .locals 1

    .prologue
    .line 34
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcs;->p:Ljava/lang/Integer;

    .line 35
    return-object p0
.end method

.method public b(J)Lbcs;
    .locals 1

    .prologue
    .line 16
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbcs;->g:Ljava/lang/Long;

    .line 17
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lbcs;->e:Ljava/lang/String;

    .line 13
    return-object p0
.end method

.method public b(Z)Lbcs;
    .locals 1

    .prologue
    .line 24
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbcs;->k:Ljava/lang/Boolean;

    .line 25
    return-object p0
.end method

.method public c(I)Lbcs;
    .locals 1

    .prologue
    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcs;->q:Ljava/lang/Integer;

    .line 37
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lbcs;->f:Ljava/lang/String;

    .line 15
    return-object p0
.end method

.method public c(Z)Lbcs;
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbcs;->r:Ljava/lang/Boolean;

    .line 39
    return-object p0
.end method

.method public d(I)Lbcs;
    .locals 1

    .prologue
    .line 42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcs;->t:Ljava/lang/Integer;

    .line 43
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lbcs;->h:Ljava/lang/String;

    .line 19
    return-object p0
.end method

.method public d(Z)Lbcs;
    .locals 1

    .prologue
    .line 40
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbcs;->s:Ljava/lang/Boolean;

    .line 41
    return-object p0
.end method

.method public e(I)Lbcs;
    .locals 1

    .prologue
    .line 44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcs;->u:Ljava/lang/Integer;

    .line 45
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lbcs;->i:Ljava/lang/String;

    .line 21
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lbcs;->l:Ljava/lang/String;

    .line 27
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lbcs;->m:Ljava/lang/String;

    .line 29
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lbcs;->n:Ljava/lang/String;

    .line 31
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lbcs;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lbcs;->o:Ljava/lang/String;

    .line 33
    return-object p0
.end method
