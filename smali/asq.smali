.class public Lasq;
.super Landroid/preference/PreferenceFragment;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lasq$a;
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/os/Bundle;

.field public c:Ljava/lang/String;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-virtual {p0}, Lasq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "target_fragment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lasq;->a:Ljava/lang/String;

    .line 4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lasq;->b:Landroid/os/Bundle;

    .line 5
    iget-object v0, p0, Lasq;->b:Landroid/os/Bundle;

    invoke-virtual {p0}, Lasq;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arguments"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 6
    invoke-virtual {p0}, Lasq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "phone_account_handle_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lasq;->c:Ljava/lang/String;

    .line 7
    invoke-virtual {p0}, Lasq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "target_title_res"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lasq;->d:I

    .line 8
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    .line 9
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 10
    invoke-virtual {p0}, Lasq;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lasq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasq;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 11
    invoke-virtual {p0}, Lasq;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 12
    invoke-virtual {p0}, Lasq;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 13
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v1

    .line 14
    invoke-virtual {p0}, Lasq;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 15
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 16
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v6

    .line 17
    if-eqz v6, :cond_0

    .line 19
    invoke-virtual {v6}, Landroid/telecom/PhoneAccount;->getCapabilities()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    .line 20
    :goto_1
    if-eqz v2, :cond_0

    .line 21
    new-instance v2, Lasq$a;

    invoke-direct {v2, p0, v4, v1, v6}, Lasq$a;-><init>(Lasq;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccount;)V

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 19
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 23
    :cond_2
    return-void
.end method
