.class public final Lhik;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final h:Lhik;

.field private static volatile i:Lhdm;


# instance fields
.field public a:I

.field public b:Lhid;

.field public c:Lhis;

.field public d:Lhip;

.field public e:Lhio;

.field public f:Lhit;

.field public g:Lhiq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 214
    new-instance v0, Lhik;

    invoke-direct {v0}, Lhik;-><init>()V

    .line 215
    sput-object v0, Lhik;->h:Lhik;

    invoke-virtual {v0}, Lhik;->makeImmutable()V

    .line 216
    const-class v0, Lhik;

    sget-object v1, Lhik;->h:Lhik;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 217
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method

.method public static synthetic a(Lhik;Lhbr$a;)V
    .locals 1

    .prologue
    .line 206
    .line 207
    invoke-virtual {p1}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhip;

    iput-object v0, p0, Lhik;->d:Lhip;

    .line 208
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lhik;->a:I

    .line 209
    return-void
.end method

.method public static synthetic b(Lhik;Lhbr$a;)V
    .locals 1

    .prologue
    .line 210
    .line 211
    invoke-virtual {p1}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lhio;

    iput-object v0, p0, Lhik;->e:Lhio;

    .line 212
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhik;->a:I

    .line 213
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 203
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    .line 204
    const-string v1, "\u0001\u0006\u0000\u0001\u0001\u0006\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\t\u0002\u0004\t\u0003\u0005\t\u0004\u0006\t\u0005"

    .line 205
    sget-object v2, Lhik;->h:Lhik;

    invoke-static {v2, v1, v0}, Lhik;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 95
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 202
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 96
    :pswitch_0
    new-instance v0, Lhik;

    invoke-direct {v0}, Lhik;-><init>()V

    .line 201
    :goto_0
    return-object v0

    .line 97
    :pswitch_1
    sget-object v0, Lhik;->h:Lhik;

    goto :goto_0

    :pswitch_2
    move-object v0, v1

    .line 98
    goto :goto_0

    .line 99
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v2, v1}, Lhbr$a;-><init>(B[[[[[[C)V

    goto :goto_0

    .line 100
    :pswitch_4
    check-cast p2, Lhaq;

    .line 101
    check-cast p3, Lhbg;

    .line 102
    if-nez p3, :cond_0

    .line 103
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 104
    :cond_0
    :try_start_0
    sget-boolean v0, Lhik;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p0, p2, p3}, Lhik;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 106
    sget-object v0, Lhik;->h:Lhik;

    goto :goto_0

    :cond_1
    move v3, v2

    .line 108
    :cond_2
    :goto_1
    if-nez v3, :cond_9

    .line 109
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 110
    sparse-switch v0, :sswitch_data_0

    .line 113
    invoke-virtual {p0, v0, p2}, Lhik;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 114
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 112
    goto :goto_1

    .line 116
    :sswitch_1
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_11

    .line 117
    iget-object v0, p0, Lhik;->b:Lhid;

    invoke-virtual {v0}, Lhid;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 119
    :goto_2
    sget-object v0, Lhid;->H:Lhid;

    .line 121
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhid;

    iput-object v0, p0, Lhik;->b:Lhid;

    .line 122
    if-eqz v2, :cond_3

    .line 123
    iget-object v0, p0, Lhik;->b:Lhid;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 124
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhid;

    iput-object v0, p0, Lhik;->b:Lhid;

    .line 125
    :cond_3
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhik;->a:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 188
    :catch_0
    move-exception v0

    .line 189
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    :catchall_0
    move-exception v0

    throw v0

    .line 128
    :sswitch_2
    :try_start_2
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_10

    .line 129
    iget-object v0, p0, Lhik;->c:Lhis;

    invoke-virtual {v0}, Lhis;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 131
    :goto_3
    sget-object v0, Lhis;->d:Lhis;

    .line 133
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhis;

    iput-object v0, p0, Lhik;->c:Lhis;

    .line 134
    if-eqz v2, :cond_4

    .line 135
    iget-object v0, p0, Lhik;->c:Lhis;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 136
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhis;

    iput-object v0, p0, Lhik;->c:Lhis;

    .line 137
    :cond_4
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhik;->a:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 190
    :catch_1
    move-exception v0

    .line 191
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 192
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 140
    :sswitch_3
    :try_start_4
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_f

    .line 141
    iget-object v0, p0, Lhik;->d:Lhip;

    invoke-virtual {v0}, Lhip;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 143
    :goto_4
    sget-object v0, Lhip;->d:Lhip;

    .line 145
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhip;

    iput-object v0, p0, Lhik;->d:Lhip;

    .line 146
    if-eqz v2, :cond_5

    .line 147
    iget-object v0, p0, Lhik;->d:Lhip;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 148
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhip;

    iput-object v0, p0, Lhik;->d:Lhip;

    .line 149
    :cond_5
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lhik;->a:I

    goto/16 :goto_1

    .line 152
    :sswitch_4
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_e

    .line 153
    iget-object v0, p0, Lhik;->e:Lhio;

    invoke-virtual {v0}, Lhio;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 155
    :goto_5
    sget-object v0, Lhio;->h:Lhio;

    .line 157
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhio;

    iput-object v0, p0, Lhik;->e:Lhio;

    .line 158
    if-eqz v2, :cond_6

    .line 159
    iget-object v0, p0, Lhik;->e:Lhio;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 160
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhio;

    iput-object v0, p0, Lhik;->e:Lhio;

    .line 161
    :cond_6
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhik;->a:I

    goto/16 :goto_1

    .line 164
    :sswitch_5
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_d

    .line 165
    iget-object v0, p0, Lhik;->f:Lhit;

    invoke-virtual {v0}, Lhit;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 167
    :goto_6
    sget-object v0, Lhit;->i:Lhit;

    .line 169
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhit;

    iput-object v0, p0, Lhik;->f:Lhit;

    .line 170
    if-eqz v2, :cond_7

    .line 171
    iget-object v0, p0, Lhik;->f:Lhit;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 172
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhit;

    iput-object v0, p0, Lhik;->f:Lhit;

    .line 173
    :cond_7
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lhik;->a:I

    goto/16 :goto_1

    .line 176
    :sswitch_6
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_c

    .line 177
    iget-object v0, p0, Lhik;->g:Lhiq;

    invoke-virtual {v0}, Lhiq;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 179
    :goto_7
    sget-object v0, Lhiq;->f:Lhiq;

    .line 181
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhiq;

    iput-object v0, p0, Lhik;->g:Lhiq;

    .line 182
    if-eqz v2, :cond_8

    .line 183
    iget-object v0, p0, Lhik;->g:Lhiq;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 184
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhiq;

    iput-object v0, p0, Lhik;->g:Lhiq;

    .line 185
    :cond_8
    iget v0, p0, Lhik;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lhik;->a:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 194
    :cond_9
    :pswitch_5
    sget-object v0, Lhik;->h:Lhik;

    goto/16 :goto_0

    .line 195
    :pswitch_6
    sget-object v0, Lhik;->i:Lhdm;

    if-nez v0, :cond_b

    const-class v1, Lhik;

    monitor-enter v1

    .line 196
    :try_start_5
    sget-object v0, Lhik;->i:Lhdm;

    if-nez v0, :cond_a

    .line 197
    new-instance v0, Lhaa;

    sget-object v2, Lhik;->h:Lhik;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhik;->i:Lhdm;

    .line 198
    :cond_a
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 199
    :cond_b
    sget-object v0, Lhik;->i:Lhdm;

    goto/16 :goto_0

    .line 198
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 200
    :pswitch_7
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    move-object v0, v1

    .line 201
    goto/16 :goto_0

    :cond_c
    move-object v2, v1

    goto :goto_7

    :cond_d
    move-object v2, v1

    goto :goto_6

    :cond_e
    move-object v2, v1

    goto/16 :goto_5

    :cond_f
    move-object v2, v1

    goto/16 :goto_4

    :cond_10
    move-object v2, v1

    goto/16 :goto_3

    :cond_11
    move-object v2, v1

    goto/16 :goto_2

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 44
    iget v0, p0, Lhik;->memoizedSerializedSize:I

    .line 45
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 94
    :goto_0
    return v0

    .line 46
    :cond_0
    sget-boolean v0, Lhik;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p0}, Lhik;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhik;->memoizedSerializedSize:I

    .line 48
    iget v0, p0, Lhik;->memoizedSerializedSize:I

    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x0

    .line 50
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 53
    iget-object v0, p0, Lhik;->b:Lhid;

    if-nez v0, :cond_8

    .line 54
    sget-object v0, Lhid;->H:Lhid;

    .line 56
    :goto_1
    invoke-static {v2, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :cond_2
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 60
    iget-object v1, p0, Lhik;->c:Lhis;

    if-nez v1, :cond_9

    .line 61
    sget-object v1, Lhis;->d:Lhis;

    .line 63
    :goto_2
    invoke-static {v3, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_3
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 65
    const/4 v2, 0x3

    .line 67
    iget-object v1, p0, Lhik;->d:Lhip;

    if-nez v1, :cond_a

    .line 68
    sget-object v1, Lhip;->d:Lhip;

    .line 70
    :goto_3
    invoke-static {v2, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_4
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 74
    iget-object v1, p0, Lhik;->e:Lhio;

    if-nez v1, :cond_b

    .line 75
    sget-object v1, Lhio;->h:Lhio;

    .line 77
    :goto_4
    invoke-static {v4, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_5
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 79
    const/4 v2, 0x5

    .line 81
    iget-object v1, p0, Lhik;->f:Lhit;

    if-nez v1, :cond_c

    .line 82
    sget-object v1, Lhit;->i:Lhit;

    .line 84
    :goto_5
    invoke-static {v2, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_6
    iget v1, p0, Lhik;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 86
    const/4 v2, 0x6

    .line 88
    iget-object v1, p0, Lhik;->g:Lhiq;

    if-nez v1, :cond_d

    .line 89
    sget-object v1, Lhiq;->f:Lhiq;

    .line 91
    :goto_6
    invoke-static {v2, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_7
    iget-object v1, p0, Lhik;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lhik;->memoizedSerializedSize:I

    goto/16 :goto_0

    .line 55
    :cond_8
    iget-object v0, p0, Lhik;->b:Lhid;

    goto :goto_1

    .line 62
    :cond_9
    iget-object v1, p0, Lhik;->c:Lhis;

    goto :goto_2

    .line 69
    :cond_a
    iget-object v1, p0, Lhik;->d:Lhip;

    goto :goto_3

    .line 76
    :cond_b
    iget-object v1, p0, Lhik;->e:Lhio;

    goto :goto_4

    .line 83
    :cond_c
    iget-object v1, p0, Lhik;->f:Lhit;

    goto :goto_5

    .line 90
    :cond_d
    iget-object v1, p0, Lhik;->g:Lhiq;

    goto :goto_6
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lhik;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhik;->writeToInternal(Lhaw;)V

    .line 43
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 8
    iget-object v0, p0, Lhik;->b:Lhid;

    if-nez v0, :cond_7

    .line 9
    sget-object v0, Lhid;->H:Lhid;

    .line 11
    :goto_1
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 12
    :cond_1
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 14
    iget-object v0, p0, Lhik;->c:Lhis;

    if-nez v0, :cond_8

    .line 15
    sget-object v0, Lhis;->d:Lhis;

    .line 17
    :goto_2
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 18
    :cond_2
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 19
    const/4 v1, 0x3

    .line 20
    iget-object v0, p0, Lhik;->d:Lhip;

    if-nez v0, :cond_9

    .line 21
    sget-object v0, Lhip;->d:Lhip;

    .line 23
    :goto_3
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 24
    :cond_3
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 26
    iget-object v0, p0, Lhik;->e:Lhio;

    if-nez v0, :cond_a

    .line 27
    sget-object v0, Lhio;->h:Lhio;

    .line 29
    :goto_4
    invoke-virtual {p1, v3, v0}, Lhaw;->a(ILhdd;)V

    .line 30
    :cond_4
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 31
    const/4 v1, 0x5

    .line 32
    iget-object v0, p0, Lhik;->f:Lhit;

    if-nez v0, :cond_b

    .line 33
    sget-object v0, Lhit;->i:Lhit;

    .line 35
    :goto_5
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 36
    :cond_5
    iget v0, p0, Lhik;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 37
    const/4 v1, 0x6

    .line 38
    iget-object v0, p0, Lhik;->g:Lhiq;

    if-nez v0, :cond_c

    .line 39
    sget-object v0, Lhiq;->f:Lhiq;

    .line 41
    :goto_6
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 42
    :cond_6
    iget-object v0, p0, Lhik;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0

    .line 10
    :cond_7
    iget-object v0, p0, Lhik;->b:Lhid;

    goto :goto_1

    .line 16
    :cond_8
    iget-object v0, p0, Lhik;->c:Lhis;

    goto :goto_2

    .line 22
    :cond_9
    iget-object v0, p0, Lhik;->d:Lhip;

    goto :goto_3

    .line 28
    :cond_a
    iget-object v0, p0, Lhik;->e:Lhio;

    goto :goto_4

    .line 34
    :cond_b
    iget-object v0, p0, Lhik;->f:Lhit;

    goto :goto_5

    .line 40
    :cond_c
    iget-object v0, p0, Lhik;->g:Lhiq;

    goto :goto_6
.end method
