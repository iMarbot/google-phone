.class final Laxe;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic a:Landroid/content/Context;

.field private synthetic b:Laxh;


# direct methods
.method constructor <init>(Landroid/content/Context;Laxh;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Laxe;->a:Landroid/content/Context;

    iput-object p2, p0, Laxe;->b:Laxh;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 2
    iget-object v0, p0, Laxe;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laxe;->a:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    :cond_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 13
    :goto_0
    return-object v0

    .line 4
    :cond_1
    iget-object v0, p0, Laxe;->a:Landroid/content/Context;

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Laxi;->a:[Ljava/lang/String;

    const-string v3, "send_to_voicemail=1"

    move-object v5, v4

    .line 6
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 8
    if-eqz v1, :cond_3

    .line 9
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 10
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 13
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v6

    .line 9
    goto :goto_1

    .line 12
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Laxe;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 14
    check-cast p1, Ljava/lang/Boolean;

    .line 15
    iget-object v0, p0, Laxe;->b:Laxh;

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Laxe;->b:Laxh;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Laxh;->a(Z)V

    .line 17
    :cond_0
    return-void
.end method
