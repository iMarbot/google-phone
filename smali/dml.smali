.class public final Ldml;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

.field private b:Ldmm;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;Ldmm;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldml;->c:Z

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    iput-object v0, p0, Ldml;->a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmm;

    iput-object v0, p0, Ldml;->b:Ldmm;

    .line 5
    return-void
.end method

.method private static declared-synchronized a(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;Ldmm;)Ldmk;
    .locals 6

    .prologue
    .line 21
    const-class v1, Ldml;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lbdf;->c()V

    .line 22
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-virtual {p0}, Lgng;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    sget-object v0, Ldmk;->a:Ldmk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :goto_0
    monitor-exit v1

    return-object v0

    .line 26
    :cond_0
    :try_start_1
    const-string v0, "ServiceTracker.attemptToConnectService"

    const-string v2, "calling EnrichedCallService#connect"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    invoke-virtual {p0}, Lgng;->connect()Z

    move-result v0

    if-nez v0, :cond_1

    .line 28
    const-string v0, "ServiceTracker.attemptToConnectService"

    const-string v2, "unable to bind to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 30
    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    sget-object v0, Ldmk;->b:Ldmk;

    goto :goto_0

    .line 32
    :cond_1
    const-string v0, "ServiceTracker.attemptToConnectService"

    const-string v2, "waiting to connect"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-virtual {p1}, Ldmm;->a()Ldmk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6
    invoke-static {}, Lbdf;->c()V

    .line 7
    iget-boolean v1, p0, Ldml;->c:Z

    if-eqz v1, :cond_1

    .line 20
    :cond_0
    :goto_0
    return-object v0

    .line 9
    :cond_1
    iget-object v1, p0, Ldml;->a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    iget-object v2, p0, Ldml;->b:Ldmm;

    .line 10
    invoke-static {v1, v2}, Ldml;->a(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;Ldmm;)Ldmk;

    move-result-object v1

    .line 12
    iget-boolean v2, v1, Ldmk;->e:Z

    .line 13
    if-eqz v2, :cond_2

    .line 14
    const-string v2, "ServiceTracker.getConnectedEnrichedCallService"

    const-string v3, "received permanent connection failure"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    const/4 v2, 0x1

    iput-boolean v2, p0, Ldml;->c:Z

    .line 17
    :cond_2
    iget-boolean v1, v1, Ldmk;->d:Z

    .line 18
    if-nez v1, :cond_0

    .line 20
    iget-object v0, p0, Ldml;->a:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    goto :goto_0
.end method
