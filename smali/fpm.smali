.class public final Lfpm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final batteryStateReceiver:Lfpn;

.field public final context:Landroid/content/Context;

.field public receiverRegistered:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfpm;->context:Landroid/content/Context;

    .line 3
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 4
    new-instance v1, Lfpn;

    invoke-direct {v1, p0}, Lfpn;-><init>(Lfpm;)V

    iput-object v1, p0, Lfpm;->batteryStateReceiver:Lfpn;

    .line 5
    iget-object v1, p0, Lfpm;->batteryStateReceiver:Lfpn;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lfpm;->receiverRegistered:Landroid/content/Intent;

    .line 6
    return-void
.end method


# virtual methods
.method final release()V
    .locals 2

    .prologue
    .line 7
    iget-object v0, p0, Lfpm;->receiverRegistered:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lfpm;->context:Landroid/content/Context;

    iget-object v1, p0, Lfpm;->batteryStateReceiver:Lfpn;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lfpm;->receiverRegistered:Landroid/content/Intent;

    .line 10
    :cond_0
    return-void
.end method

.method public final updateStats(Lcom/google/android/libraries/hangouts/video/internal/Stats$a;)V
    .locals 2

    .prologue
    .line 11
    invoke-static {}, Lfoo;->getInstance()Lfoo;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lfoo;->getOnlineCpuCount()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->setOnlineCpuCores(I)V

    .line 13
    invoke-virtual {v0}, Lfoo;->getCurrentCpuFrequencyKHz()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->setCurrentCpuSpeedMHz(I)V

    .line 14
    invoke-virtual {v0}, Lfoo;->getCurrentCpuUtilization()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->setUtilizationPerCpu(I)V

    .line 15
    iget-object v0, p0, Lfpm;->batteryStateReceiver:Lfpn;

    invoke-virtual {v0}, Lfpn;->getIsOnBattery()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->setIsOnBattery(Z)V

    .line 16
    iget-object v0, p0, Lfpm;->batteryStateReceiver:Lfpn;

    invoke-virtual {v0}, Lfpn;->getBatteryLevel()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->setBatteryLevel(I)V

    .line 17
    return-void
.end method
