.class public final Lfqj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2
    iget-object v0, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->access$100(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->access$000(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3
    iget-object v0, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->access$100(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-static {v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->access$000(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4
    :try_start_0
    iget-object v0, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->processInput()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    :goto_0
    return-void

    .line 6
    :catch_0
    move-exception v0

    .line 7
    iget-object v1, p0, Lfqj;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;)V

    goto :goto_0
.end method
