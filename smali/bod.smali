.class public final Lbod;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/widget/QuickContactBadge;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/ImageView;

.field public final t:Landroid/content/Context;

.field public u:I

.field public v:Ljava/lang/String;

.field public w:Lbhj;

.field public x:I

.field private y:Lbob;


# direct methods
.method public constructor <init>(Landroid/view/View;Lbob;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    iput-object p2, p0, Lbod;->y:Lbob;

    .line 3
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4
    const v0, 0x7f0e00d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbod;->p:Landroid/widget/QuickContactBadge;

    .line 5
    const v0, 0x7f0e0255

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbod;->q:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e0256

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbod;->r:Landroid/widget/TextView;

    .line 7
    const v0, 0x7f0e000d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbod;->s:Landroid/widget/ImageView;

    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbod;->t:Landroid/content/Context;

    .line 9
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 10
    iget-object v0, p0, Lbod;->s:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 11
    iget v0, p0, Lbod;->x:I

    packed-switch v0, :pswitch_data_0

    .line 18
    iget v0, p0, Lbod;->x:I

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid Call to action type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 12
    :pswitch_0
    iget-object v0, p0, Lbod;->y:Lbob;

    iget-object v1, p0, Lbod;->w:Lbhj;

    invoke-interface {v0, v1}, Lbob;->a(Lbhj;)V

    .line 20
    :goto_0
    return-void

    .line 14
    :pswitch_1
    iget-object v0, p0, Lbod;->y:Lbob;

    iget-object v1, p0, Lbod;->v:Ljava/lang/String;

    iget v2, p0, Lbod;->u:I

    invoke-interface {v0, v1, v2}, Lbob;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 16
    :pswitch_2
    iget-object v0, p0, Lbod;->y:Lbob;

    iget-object v1, p0, Lbod;->v:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbob;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lbod;->y:Lbob;

    iget-object v1, p0, Lbod;->v:Ljava/lang/String;

    iget v2, p0, Lbod;->u:I

    invoke-interface {v0, v1, v2}, Lbob;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
