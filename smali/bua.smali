.class public final Lbua;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbgh;


# instance fields
.field public a:Z

.field private b:Landroid/content/SharedPreferences;

.field private c:Lbgf;

.field private d:Landroid/content/Context;

.field private e:Lbub;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbub;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbua;->d:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lbua;->e:Lbub;

    .line 4
    iget-object v0, p0, Lbua;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbua;->b:Landroid/content/SharedPreferences;

    .line 5
    iget-object v0, p0, Lbua;->b:Landroid/content/SharedPreferences;

    const-string v1, "has_active_voicemail_provider"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbua;->a:Z

    .line 6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 7
    new-instance v0, Lbgf;

    iget-object v1, p0, Lbua;->d:Landroid/content/Context;

    iget-object v2, p0, Lbua;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lbgf;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;)V

    iput-object v0, p0, Lbua;->c:Lbgf;

    .line 8
    iget-object v0, p0, Lbua;->c:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 9
    return-void
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 10
    .line 11
    invoke-static {p1}, Lbib;->a(Landroid/database/Cursor;)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 12
    :goto_0
    iget-boolean v1, p0, Lbua;->a:Z

    if-eq v0, v1, :cond_0

    .line 13
    iput-boolean v0, p0, Lbua;->a:Z

    .line 14
    iget-object v0, p0, Lbua;->b:Landroid/content/SharedPreferences;

    .line 15
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "has_active_voicemail_provider"

    iget-boolean v2, p0, Lbua;->a:Z

    .line 16
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 17
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 18
    iget-object v0, p0, Lbua;->e:Lbub;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lbua;->e:Lbub;

    invoke-interface {v0}, Lbub;->a()V

    .line 20
    :cond_0
    return-void

    .line 11
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public final d(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
