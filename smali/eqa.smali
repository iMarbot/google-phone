.class public final Leqa;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Leqc;

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leqb;

    invoke-direct {v0}, Leqb;-><init>()V

    sput-object v0, Leqa;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILeqc;)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Leqa;->b:I

    iput-object p2, p0, Leqa;->a:Leqc;

    return-void
.end method

.method private constructor <init>(Leqc;)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Leqa;->b:I

    iput-object p1, p0, Leqa;->a:Leqc;

    return-void
.end method

.method public static a(Lejx;)Leqa;
    .locals 2

    instance-of v0, p0, Leqc;

    if-eqz v0, :cond_0

    new-instance v0, Leqa;

    check-cast p0, Leqc;

    invoke-direct {v0, p0}, Leqa;-><init>(Leqc;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported safe parcelable field converter class."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Leqa;->b:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Leqa;->a:Leqc;

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
