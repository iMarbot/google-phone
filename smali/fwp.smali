.class public final Lfwp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lfwo;

.field public final b:I


# direct methods
.method public constructor <init>(Lfwo;I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfwp;->a:Lfwo;

    .line 3
    iput p2, p0, Lfwp;->b:I

    .line 4
    return-void
.end method

.method public static a(Ljava/lang/String;)Lfwp;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5
    if-nez p0, :cond_0

    move-object v0, v1

    .line 18
    :goto_0
    return-object v0

    .line 7
    :cond_0
    const-string v0, "x"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 8
    array-length v2, v0

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 9
    const-string v2, "VideoSpecification can\'t parse "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    move-object v0, v1

    .line 10
    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 11
    :cond_2
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 12
    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 13
    const/4 v4, 0x2

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 18
    new-instance v0, Lfwp;

    new-instance v4, Lfwo;

    invoke-direct {v4, v2, v3}, Lfwo;-><init>(II)V

    invoke-direct {v0, v4, v1}, Lfwp;-><init>(Lfwo;I)V

    goto :goto_0

    .line 16
    :catch_0
    move-exception v0

    const-string v2, "VideoSpecification can\'t parse "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    move-object v0, v1

    .line 17
    goto :goto_0

    .line 16
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lfwp;->a:Lfwo;

    iget v0, v0, Lfwo;->a:I

    iget-object v1, p0, Lfwp;->a:Lfwo;

    iget v1, v1, Lfwo;->b:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 20
    instance-of v1, p1, Lfwp;

    if-nez v1, :cond_1

    .line 23
    :cond_0
    :goto_0
    return v0

    .line 22
    :cond_1
    check-cast p1, Lfwp;

    .line 23
    iget-object v1, p0, Lfwp;->a:Lfwo;

    iget-object v2, p1, Lfwp;->a:Lfwo;

    invoke-virtual {v1, v2}, Lfwo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lfwp;->b:I

    iget v2, p1, Lfwp;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lfwp;->a:Lfwo;

    invoke-virtual {v0}, Lfwo;->hashCode()I

    move-result v0

    mul-int/lit16 v0, v0, 0x115

    iget v1, p0, Lfwp;->b:I

    add-int/2addr v0, v1

    return v0
.end method
