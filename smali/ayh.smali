.class final synthetic Layh;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Layg;


# direct methods
.method constructor <init>(Layg;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Layh;->a:Layg;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1
    iget-object v1, p0, Layh;->a:Layg;

    check-cast p1, Lpr;

    .line 2
    iget-object v2, v1, Layg;->a:Layk;

    iget-object v0, p1, Lpr;->a:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    .line 3
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p1, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 4
    const-string v4, "GalleryGridAdapter.insertRow"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    new-instance v4, Landroid/database/MatrixCursor;

    sget-object v5, Layl;->a:[Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 6
    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object v3, v5, v9

    aput-object v0, v5, v10

    const/4 v0, 0x3

    const-string v3, ""

    aput-object v3, v5, v0

    invoke-virtual {v4, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 7
    invoke-virtual {v4}, Landroid/database/MatrixCursor;->moveToFirst()Z

    .line 8
    new-instance v0, Landroid/database/MergeCursor;

    new-array v3, v10, [Landroid/database/Cursor;

    aput-object v4, v3, v8

    invoke-virtual {v2}, Layk;->getCursor()Landroid/database/Cursor;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 9
    invoke-virtual {v2, v0}, Layk;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 10
    new-instance v0, Layl;

    invoke-direct {v0, v4}, Layl;-><init>(Landroid/database/Cursor;)V

    .line 12
    iget-object v2, v1, Layg;->Y:Ljava/util/List;

    invoke-interface {v2, v8, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 13
    invoke-virtual {v1, v0, v9}, Layg;->a(Layl;Z)V

    .line 14
    return-void
.end method
