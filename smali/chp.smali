.class public final Lchp;
.super Lip;
.source "PG"

# interfaces
.implements Lcho;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lchp$a;
    }
.end annotation


# instance fields
.field private W:Z

.field private X:Z

.field private a:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    return-void
.end method

.method public static a(Lbln;ZZZ)Lchp;
    .locals 5

    .prologue
    .line 2
    .line 3
    invoke-virtual {p0}, Lbln;->a()Ljava/lang/String;

    move-result-object v0

    .line 4
    invoke-virtual {p0}, Lbln;->c()Landroid/net/Uri;

    move-result-object v1

    .line 5
    invoke-virtual {p0}, Lbln;->b()Landroid/location/Location;

    move-result-object v2

    .line 7
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 8
    const-string v4, "subject"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    const-string v0, "image"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 10
    const-string v0, "location"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 11
    const-string v0, "interactive"

    invoke-virtual {v3, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 12
    const-string v0, "show_avatar"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 13
    const-string v0, "is_spam"

    invoke-virtual {v3, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 14
    new-instance v0, Lchp;

    invoke-direct {v0}, Lchp;-><init>()V

    .line 15
    invoke-virtual {v0, v3}, Lchp;->f(Landroid/os/Bundle;)V

    .line 17
    return-object v0
.end method


# virtual methods
.method public final T()I
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lchp;->i()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lchp;->W:Z

    return v0
.end method

.method public final V()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    .line 104
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 105
    const-string v1, "subject"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final W()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 106
    .line 107
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 108
    const-string v1, "image"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public final X()Landroid/location/Location;
    .locals 2

    .prologue
    .line 109
    .line 110
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 111
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    iget-boolean v0, p0, Lchp;->X:Z

    if-eqz v0, :cond_0

    .line 27
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show spam layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    const v0, 0x7f040075

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 29
    :cond_0
    invoke-virtual {p0}, Lchp;->W()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 30
    :goto_1
    invoke-virtual {p0}, Lchp;->V()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    .line 31
    :goto_2
    invoke-virtual {p0}, Lchp;->X()Landroid/location/Location;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 32
    :goto_3
    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lchp;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lchb;->a(Landroid/content/Context;)Lchb;

    move-result-object v1

    invoke-virtual {v1}, Lchb;->a()Lcha;

    .line 33
    if-eqz v0, :cond_5

    .line 34
    if-eqz v3, :cond_4

    .line 35
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show text, image, location layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    const v0, 0x7f04006d

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 29
    goto :goto_1

    :cond_2
    move v3, v2

    .line 30
    goto :goto_2

    :cond_3
    move v1, v2

    .line 31
    goto :goto_3

    .line 37
    :cond_4
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show image, location layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    const v0, 0x7f040069

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 39
    :cond_5
    if-eqz v3, :cond_6

    .line 40
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show text, location layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    const v0, 0x7f04006b

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 42
    :cond_6
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show location layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    const v0, 0x7f040067

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_7
    if-eqz v0, :cond_9

    .line 45
    if-eqz v3, :cond_8

    .line 46
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show text, image layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    const v0, 0x7f04006c

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 48
    :cond_8
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show image layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    const v0, 0x7f040068

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 50
    :cond_9
    const-string v0, "MultimediaFragment.onCreateView"

    const-string v1, "show text layout"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const v0, 0x7f04006a

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final a()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lchp;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x7f0e0008

    const/4 v4, 0x1

    .line 52
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f0e0007

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0, v4}, Landroid/view/View;->setClipToOutline(Z)V

    .line 56
    :cond_0
    iget-boolean v0, p0, Lchp;->X:Z

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p0}, Lchp;->X()Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_1

    .line 58
    invoke-virtual {p0}, Lchp;->W()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lchp;->V()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    const v0, 0x7f0e01e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020153

    .line 61
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    const v0, 0x7f0e01e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1102d4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 63
    :cond_1
    const v0, 0x7f0e000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    if-eqz v0, :cond_2

    .line 65
    invoke-virtual {p0}, Lchp;->V()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    :cond_2
    const v0, 0x7f0e0009

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 67
    if-eqz v0, :cond_3

    .line 69
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v1

    invoke-static {v1}, Lcsw;->b(Landroid/content/Context;)Ldfm;

    move-result-object v1

    .line 70
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v2

    const-string v3, "You cannot start a load on a fragment before it is attached or after it is destroyed"

    invoke-static {v2, v3}, Ldhh;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 71
    invoke-static {}, Ldhw;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 72
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v2

    invoke-virtual {v2}, Lit;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldfm;->a(Landroid/content/Context;)Lcte;

    move-result-object v1

    .line 76
    :goto_0
    invoke-virtual {p0}, Lchp;->W()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcte;->a(Ljava/lang/Object;)Lctb;

    move-result-object v1

    .line 77
    invoke-static {}, Lddz;->b()Lddz;

    move-result-object v2

    invoke-virtual {v1, v2}, Lctb;->a(Lcth;)Lctb;

    move-result-object v1

    new-instance v2, Lchq;

    invoke-direct {v2, p1}, Lchq;-><init>(Landroid/view/View;)V

    .line 79
    iput-object v2, v1, Lctb;->c:Ldgm;

    .line 82
    invoke-virtual {v1, v0}, Lctb;->a(Landroid/widget/ImageView;)Ldha;

    .line 83
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 84
    :cond_3
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 85
    if-eqz v0, :cond_4

    .line 86
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClipToOutline(Z)V

    .line 88
    invoke-virtual {p0}, Lchp;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lchb;->a(Landroid/content/Context;)Lchb;

    move-result-object v0

    invoke-virtual {v0}, Lchb;->a()Lcha;

    move-result-object v0

    invoke-virtual {p0}, Lchp;->X()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcha;->a(Landroid/location/Location;)Lip;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lchp;->j()Lja;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    .line 91
    invoke-virtual {v1, v5, v0}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljy;->c()V

    .line 93
    :cond_4
    const v0, 0x7f0e0006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lchp;->a:Landroid/widget/ImageView;

    .line 94
    iget-object v0, p0, Lchp;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 95
    iget-object v1, p0, Lchp;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lchp;->W:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 96
    :cond_5
    const-class v0, Lchp$a;

    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchp$a;

    .line 97
    if-eqz v0, :cond_6

    .line 98
    invoke-interface {v0, p0}, Lchp$a;->a(Lcho;)V

    .line 99
    :cond_6
    return-void

    .line 73
    :cond_7
    invoke-virtual {p0}, Lip;->j()Lja;

    move-result-object v2

    .line 74
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v3

    invoke-virtual {v1, v3, v2, p0}, Ldfm;->a(Landroid/content/Context;Lja;Lip;)Lcte;

    move-result-object v1

    goto :goto_0

    .line 95
    :cond_8
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 20
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 21
    const-string v1, "show_avatar"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lchp;->W:Z

    .line 23
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 24
    const-string v1, "is_spam"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lchp;->X:Z

    .line 25
    return-void
.end method
