.class final Lfza;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfyz;


# instance fields
.field public final a:Landroid/app/Application;

.field public final b:Lgax;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/List;

.field public volatile e:Z

.field public f:Lgdc;

.field public g:Lgab;

.field public h:Lgap;

.field public i:Lfzp;

.field public j:Lgae;

.field public k:Lfzz;

.field public l:Lgaq;

.field public m:Lfzk;

.field public n:Lgac;

.field public o:Lfzu;

.field public p:Lfzo;

.field public q:Landroid/content/SharedPreferences;

.field public r:Lfzx;


# direct methods
.method constructor <init>(Landroid/app/Application;Lgax;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfza;->c:Ljava/lang/Object;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfza;->d:Ljava/util/List;

    .line 4
    invoke-static {}, Lfza;->f()Z

    move-result v0

    invoke-static {v0}, Lhcw;->b(Z)V

    .line 5
    iput-object p2, p0, Lfza;->b:Lgax;

    .line 6
    iput-object p1, p0, Lfza;->a:Landroid/app/Application;

    .line 7
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 134
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v1, p0, Lfza;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_3

    .line 140
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 143
    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 142
    :cond_3
    :try_start_1
    iget-object v0, p0, Lfza;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method static f()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 208
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 209
    const-string v1, "Primes"

    const-string v2, "Primes calls will be ignored. API\'s < 16 are not supported."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lfza;->e()V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    new-instance v0, Lfze;

    invoke-direct {v0, p0}, Lfze;-><init>(Lfza;)V

    invoke-direct {p0, v0}, Lfza;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final a(Lfwu;)V
    .locals 10

    .prologue
    const/16 v9, 0x18

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 36
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 39
    iget-object v0, p0, Lfza;->i:Lfzp;

    .line 40
    iget-boolean v0, v0, Lfzp;->b:Z

    .line 41
    if-eqz v0, :cond_4

    .line 42
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->i:Lfzp;

    iget-object v4, p0, Lfza;->r:Lfzx;

    .line 44
    iget-boolean v4, v4, Lfzx;->f:Z

    .line 45
    invoke-static {v0, v1, v2, v3, v4}, Lfxo;->a(Lgdc;Landroid/app/Application;Lgax;Lfzp;Z)Lfxo;

    move-result-object v0

    .line 46
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-gt v0, v1, :cond_6

    iget-object v0, p0, Lfza;->j:Lgae;

    .line 49
    iget-boolean v0, v0, Lgae;->b:Z

    .line 50
    if-eqz v0, :cond_6

    .line 51
    iget-object v0, p0, Lfza;->q:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lfyw;->a(Landroid/content/SharedPreferences;)Z

    move-result v0

    .line 52
    iget-object v1, p0, Lfza;->j:Lgae;

    .line 53
    iget-boolean v1, v1, Lgae;->c:Z

    .line 55
    iget-object v2, p0, Lfza;->j:Lgae;

    .line 56
    iget-boolean v5, v2, Lgae;->d:Z

    .line 58
    if-nez v1, :cond_5

    if-nez v0, :cond_5

    .line 59
    new-instance v0, Lfyw;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->f:Lgdc;

    iget-object v3, p0, Lfza;->b:Lgax;

    iget-object v4, p0, Lfza;->q:Landroid/content/SharedPreferences;

    invoke-direct/range {v0 .. v5}, Lfyw;-><init>(Landroid/app/Application;Lgdc;Lgax;Landroid/content/SharedPreferences;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    const-string v0, "Package metric: registered for startup notifications"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_7

    .line 68
    sget-object v0, Lgat;->a:Lgat;

    .line 70
    iget-boolean v0, v0, Lgat;->c:Z

    .line 71
    if-nez v0, :cond_7

    iget-object v0, p0, Lfza;->r:Lfzx;

    .line 73
    iget-boolean v0, v0, Lfzx;->d:Z

    .line 74
    if-nez v0, :cond_2

    iget-object v0, p0, Lfza;->m:Lfzk;

    .line 75
    iget-boolean v0, v0, Lfzk;->b:Z

    .line 76
    if-eqz v0, :cond_7

    :cond_2
    move v0, v8

    .line 77
    :goto_3
    if-eqz v0, :cond_8

    .line 78
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->q:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lfza;->m:Lfzk;

    .line 79
    invoke-static {v0, v1, v2, v3, v4}, Lfxi;->a(Lgdc;Landroid/app/Application;Lgax;Landroid/content/SharedPreferences;Lfzk;)Lfxi;

    move-result-object v0

    .line 80
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v0, "Battery metrics enabled"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    :goto_4
    iget-object v0, p0, Lfza;->r:Lfzx;

    .line 84
    iget-boolean v0, v0, Lfzx;->e:Z

    .line 85
    if-eqz v0, :cond_9

    .line 86
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    .line 87
    invoke-static {v0, v1, v2}, Lfxw;->a(Lgdc;Landroid/app/Application;Lgax;)Lfxw;

    move-result-object v0

    .line 88
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :goto_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_3

    iget-object v0, p0, Lfza;->k:Lfzz;

    .line 91
    iget-boolean v0, v0, Lfzz;->b:Z

    .line 92
    if-eqz v0, :cond_3

    iget-object v0, p0, Lfza;->k:Lfzz;

    .line 93
    iget-boolean v0, v0, Lfzz;->d:Z

    .line 94
    if-nez v0, :cond_3

    .line 95
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->k:Lfzz;

    .line 96
    invoke-static {v0, v1, v2, v3}, Lfxq;->a(Lgdc;Landroid/app/Application;Lgax;Lfzz;)Lfxq;

    move-result-object v0

    .line 97
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_3
    iget-object v0, p0, Lfza;->p:Lfzo;

    .line 99
    iget-boolean v0, v0, Lfzo;->b:Z

    .line 100
    if-eqz v0, :cond_a

    .line 101
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->p:Lfzo;

    .line 102
    invoke-static {v0, v1, v2, v3}, Lfxm;->a(Lgdc;Landroid/app/Application;Lgax;Lfzo;)Lfxm;

    move-result-object v0

    .line 103
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_6
    move-object v0, v6

    .line 105
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v7

    :goto_7
    if-ge v2, v3, :cond_b

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Lgaj;

    .line 106
    invoke-interface {v1}, Lgaj;->d()V

    .line 107
    invoke-virtual {p1, v1}, Lfwu;->a(Lgaj;)V

    goto :goto_7

    .line 47
    :cond_4
    const-string v0, "Crash metric disabled - not registering for startup notifications."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 61
    :cond_5
    const-string v2, "Package metric: not registering on startup - manual: %b / recently: %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 62
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v8

    .line 63
    invoke-virtual {p0, v2, v3}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 65
    :cond_6
    const-string v0, "Package metric disabled."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_7
    move v0, v7

    .line 76
    goto/16 :goto_3

    .line 82
    :cond_8
    const-string v0, "Battery metric disabled"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 89
    :cond_9
    const-string v0, "MagicEye logging metric disabled"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 104
    :cond_a
    const-string v0, "Cpu metric disabled - not registering for startup notifications."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 109
    :cond_b
    iget-object v0, p0, Lfza;->h:Lgap;

    .line 110
    iget-boolean v0, v0, Lgap;->b:Z

    .line 111
    if-eqz v0, :cond_c

    .line 114
    iget-object v0, p0, Lfza;->f:Lgdc;

    .line 115
    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->h:Lgap;

    .line 116
    invoke-static {v0, v1, v2, v3}, Lgba;->a(Lgdc;Landroid/app/Application;Lgax;Lgap;)Lgba;

    .line 117
    sget-object v0, Lgak;->a:Lgak;

    .line 119
    iget-wide v0, v0, Lgak;->c:J

    .line 120
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_c

    .line 121
    new-instance v0, Lgal;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    .line 122
    invoke-static {v1}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v1

    new-instance v2, Lfzc;

    invoke-direct {v2, p0}, Lfzc;-><init>(Lfza;)V

    new-instance v3, Lfzd;

    invoke-direct {v3, p0}, Lfzd;-><init>(Lfza;)V

    invoke-direct {v0, v1, v2, v3}, Lgal;-><init>(Lfxe;Lgax;Lgax;)V

    .line 124
    :cond_c
    iget-object v0, p0, Lfza;->k:Lfzz;

    .line 125
    iget-boolean v0, v0, Lfzz;->e:Z

    .line 126
    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lfza;->f:Lgdc;

    .line 129
    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    invoke-static {v0, v1, v2}, Lgay;->a(Lgdc;Landroid/app/Application;Lgax;)Lgay;

    goto/16 :goto_0
.end method

.method public final a(Lgaz;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 174
    const/4 v4, 0x1

    .line 175
    if-eqz p1, :cond_0

    sget-object v0, Lgaz;->c:Lgaz;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfza;->h:Lgap;

    .line 178
    iget-boolean v0, v0, Lgap;->b:Z

    .line 179
    if-eqz v0, :cond_0

    .line 183
    :cond_2
    invoke-static {}, Lfmk;->i()J

    move-result-wide v0

    iput-wide v0, p1, Lgaz;->b:J

    .line 184
    new-instance v0, Lfzg;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lfzg;-><init>(Lfza;Lgaz;Ljava/lang/String;ZLhrz;)V

    invoke-direct {p0, v0}, Lfza;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 158
    const/4 v0, 0x1

    .line 159
    iget-boolean v1, p0, Lfza;->e:Z

    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {p0, p1, v0, v2}, Lfza;->a(Ljava/lang/String;ZLhrz;)V

    .line 162
    :goto_0
    return-void

    .line 161
    :cond_0
    new-instance v1, Lfzf;

    invoke-direct {v1, p0, p1, v0, v2}, Lfzf;-><init>(Lfza;Ljava/lang/String;ZLhrz;)V

    invoke-direct {p0, v1}, Lfza;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;ZLhrz;)V
    .locals 6

    .prologue
    .line 163
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfza;->g:Lgab;

    .line 164
    iget-boolean v0, v0, Lgab;->b:Z

    .line 165
    if-nez v0, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->g:Lgab;

    iget-object v4, p0, Lfza;->r:Lfzx;

    .line 169
    iget-boolean v4, v4, Lfzx;->c:Z

    .line 170
    invoke-static {v0, v1, v2, v3, v4}, Lfyj;->a(Lgdc;Landroid/app/Application;Lgax;Lgab;Z)Lfyj;

    move-result-object v0

    .line 172
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lfyj;->a(Ljava/lang/String;ZILjava/lang/String;Lhrz;)V

    goto :goto_0
.end method

.method final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 131
    const-string v0, "Primes"

    invoke-static {v0}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const-string v0, "Primes"

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 186
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_3

    .line 187
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfza;->i:Lfzp;

    .line 188
    iget-boolean v0, v0, Lfzp;->b:Z

    .line 189
    if-nez v0, :cond_2

    .line 190
    :cond_0
    const-string v0, "Primes crash monitoring is not enabled, yet crash monitoring was requested."

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lfza;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->i:Lfzp;

    iget-object v4, p0, Lfza;->r:Lfzx;

    .line 194
    iget-boolean v4, v4, Lfzx;->f:Z

    .line 195
    invoke-static {v0, v1, v2, v3, v4}, Lfxo;->a(Lgdc;Landroid/app/Application;Lgax;Lfzp;Z)Lfxo;

    move-result-object v0

    .line 197
    iget-object v1, v0, Lfxo;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v5, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    .line 200
    invoke-virtual {v0, v1}, Lfxo;->a(Ljava/lang/Thread$UncaughtExceptionHandler;)Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 201
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto :goto_0

    .line 203
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 204
    new-instance v1, Lfzh;

    .line 205
    invoke-direct {v1, p0, v0}, Lfzh;-><init>(Lfza;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 206
    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto :goto_0
.end method

.method public final c()Lgax;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lfza;->b:Lgax;

    return-object v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 9
    sget-object v0, Lgat;->a:Lgat;

    .line 10
    invoke-virtual {v0}, Lgat;->a()V

    .line 11
    iget-object v0, p0, Lfza;->b:Lgax;

    invoke-interface {v0}, Lgax;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 12
    sget-object v0, Lgdc;->a:Lgdc;

    iput-object v0, p0, Lfza;->f:Lgdc;

    .line 13
    sget-object v0, Lgab;->a:Lgab;

    iput-object v0, p0, Lfza;->g:Lgab;

    .line 14
    sget-object v0, Lgap;->a:Lgap;

    iput-object v0, p0, Lfza;->h:Lgap;

    .line 15
    sget-object v0, Lfzp;->a:Lfzp;

    iput-object v0, p0, Lfza;->i:Lfzp;

    .line 16
    sget-object v0, Lgae;->a:Lgae;

    iput-object v0, p0, Lfza;->j:Lgae;

    .line 17
    sget-object v0, Lfzz;->a:Lfzz;

    iput-object v0, p0, Lfza;->k:Lfzz;

    .line 18
    sget-object v0, Lgaq;->a:Lgaq;

    iput-object v0, p0, Lfza;->l:Lgaq;

    .line 19
    sget-object v0, Lfzo;->a:Lfzo;

    iput-object v0, p0, Lfza;->p:Lfzo;

    .line 20
    sget-object v0, Lgac;->a:Lgac;

    iput-object v0, p0, Lfza;->n:Lgac;

    .line 21
    :try_start_0
    iget-object v0, p0, Lfza;->a:Landroid/app/Application;

    .line 22
    const-class v1, Lfxe;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :try_start_1
    sget-object v2, Lfxe;->a:Lfxe;

    if-eqz v2, :cond_0

    .line 24
    sget-object v2, Lfxe;->a:Lfxe;

    .line 25
    iget-object v2, v2, Lfxe;->b:Lfxf;

    .line 26
    iget-object v3, v2, Lfxf;->a:Lfxg;

    invoke-virtual {v0, v3}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 27
    iget-object v2, v2, Lfxf;->a:Lfxg;

    invoke-virtual {v0, v2}, Landroid/app/Application;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lfxe;->a:Lfxe;

    .line 29
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 32
    :goto_0
    iget-object v1, p0, Lfza;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 33
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lfza;->e:Z

    .line 34
    iget-object v0, p0, Lfza;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 35
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 29
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    .line 31
    :catch_0
    move-exception v0

    const-string v0, "Primes"

    const-string v1, "Failed to shutdown app lifecycle monitor"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 35
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0
.end method

.method final e()V
    .locals 5

    .prologue
    .line 149
    invoke-virtual {p0}, Lfza;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfza;->g:Lgab;

    .line 150
    iget-boolean v0, v0, Lgab;->b:Z

    .line 151
    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lfza;->f:Lgdc;

    iget-object v1, p0, Lfza;->a:Landroid/app/Application;

    iget-object v2, p0, Lfza;->b:Lgax;

    iget-object v3, p0, Lfza;->g:Lgab;

    iget-object v4, p0, Lfza;->r:Lfzx;

    .line 154
    iget-boolean v4, v4, Lfzx;->c:Z

    .line 155
    invoke-static {v0, v1, v2, v3, v4}, Lfyj;->a(Lgdc;Landroid/app/Application;Lgax;Lgab;Z)Lfyj;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lfyj;->d()V

    .line 157
    :cond_0
    return-void
.end method

.method final g()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lfza;->e:Z

    if-eqz v0, :cond_0

    .line 213
    sget-object v0, Lgat;->a:Lgat;

    .line 215
    iget-boolean v0, v0, Lgat;->c:Z

    .line 216
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
