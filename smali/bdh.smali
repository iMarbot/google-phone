.class public final Lbdh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/SharedPreferences$Editor;

.field private synthetic b:Lbdg;


# direct methods
.method constructor <init>(Lbdg;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lbdh;->b:Lbdg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iget-object v0, p1, Lbdg;->a:Landroid/content/SharedPreferences;

    .line 4
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lbdh;->a:Landroid/content/SharedPreferences$Editor;

    .line 5
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lbdh;
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lbdh;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lbdh;->b:Lbdg;

    .line 13
    invoke-virtual {v1, p1}, Lbdg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 14
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 15
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Lbdh;
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lbdh;->a:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lbdh;->b:Lbdg;

    .line 9
    invoke-virtual {v1, p1}, Lbdg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 10
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 11
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lbdh;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 7
    return-void
.end method
