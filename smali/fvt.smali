.class public Lfvt;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthError()V
    .locals 0

    .prologue
    .line 23
    return-void
.end method

.method public onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public onCallEnd(I)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public onCallEnd(Lfvx;)V
    .locals 1

    .prologue
    .line 5
    .line 6
    iget v0, p1, Lfvx;->a:I

    .line 7
    invoke-virtual {p0, v0}, Lfvt;->onCallEnd(I)V

    .line 8
    return-void
.end method

.method public onCallJoin(Lfvy;)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method

.method public onClientDataMessageReceived(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public onCloudMediaSessionIdAvailable(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method public onFirstAudioPacket()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onFocusedParticipantChanged(Lfvz;)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public onHangoutLogRequestPrepared(Lgsi;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public onInitialCallStateSynchronized(Z)V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public onLogDataPrepared(Lgpn;)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public onMediaStats(Lgiv;)V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public onMeetingsPush(Lgqi;)V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public onParticipantAdded(Lfvz;)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method

.method public onParticipantChanged(Lfvz;)V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method public onParticipantRemoved(Lfvz;)V
    .locals 0

    .prologue
    .line 10
    return-void
.end method

.method public onPendingParticipantAdded(Lfvz;)V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public onPendingParticipantChanged(Lfvz;)V
    .locals 0

    .prologue
    .line 14
    return-void
.end method

.method public onPendingParticipantRemoved(Lfvz;)V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method public onQualityNotification(Lfwb;)V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public onVolumeLevelUpdate(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    return-void
.end method
