.class final Lfxi;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfxb;
.implements Lfxc;
.implements Lgaj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfxi$a;
    }
.end annotation


# static fields
.field private static volatile d:Lfxi;


# instance fields
.field private e:Lfxh;

.field private f:Z

.field private g:Ljava/util/concurrent/locks/ReentrantLock;

.field private h:Lgbl;

.field private i:Lgbn;

.field private j:Lgax;

.field private k:Lfxi$a;

.field private l:Lfxi$a;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;Lgbn;Lfxi$a;Lfxi$a;Landroid/content/SharedPreferences;Lfxh;)V
    .locals 2

    .prologue
    .line 11
    sget v0, Lmg$c;->C:I

    invoke-direct {p0, p1, p2, p3, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfxi;->f:Z

    .line 13
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    .line 14
    iput-object p4, p0, Lfxi;->i:Lgbn;

    .line 15
    new-instance v0, Lgbl;

    invoke-direct {v0, p7}, Lgbl;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lfxi;->h:Lgbl;

    .line 16
    invoke-static {p2}, Lfyn;->b(Landroid/content/Context;)Lgax;

    move-result-object v0

    iput-object v0, p0, Lfxi;->j:Lgax;

    .line 17
    iput-object p5, p0, Lfxi;->k:Lfxi$a;

    .line 18
    iput-object p6, p0, Lfxi;->l:Lfxi$a;

    .line 19
    iput-object p8, p0, Lfxi;->e:Lfxh;

    .line 20
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Landroid/content/SharedPreferences;Lfzk;)Lfxi;
    .locals 10

    .prologue
    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lhcw;->b(Z)V

    .line 2
    sget-object v0, Lfxi;->d:Lfxi;

    if-nez v0, :cond_1

    .line 3
    const-class v9, Lfxi;

    monitor-enter v9

    .line 4
    :try_start_0
    sget-object v0, Lfxi;->d:Lfxi;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Lfxi;

    new-instance v4, Lgbn;

    invoke-direct {v4}, Lgbn;-><init>()V

    new-instance v5, Lfxj;

    invoke-direct {v5}, Lfxj;-><init>()V

    new-instance v6, Lfxk;

    invoke-direct {v6}, Lfxk;-><init>()V

    .line 7
    iget-object v8, p4, Lfzk;->c:Lfxh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v7, p3

    .line 8
    invoke-direct/range {v0 .. v8}, Lfxi;-><init>(Lgdc;Landroid/app/Application;Lgax;Lgbn;Lfxi$a;Lfxi$a;Landroid/content/SharedPreferences;Lfxh;)V

    sput-object v0, Lfxi;->d:Lfxi;

    .line 9
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    :cond_1
    sget-object v0, Lfxi;->d:Lfxi;

    return-object v0

    .line 1
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 9
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    packed-switch p0, :pswitch_data_0

    .line 242
    const-string v0, "UNEXPECTED"

    :goto_0
    return-object v0

    .line 237
    :pswitch_0
    const-string v0, "UNKNOWN/MANUAL"

    goto :goto_0

    .line 238
    :pswitch_1
    const-string v0, "FG_BG"

    goto :goto_0

    .line 239
    :pswitch_2
    const-string v0, "BG_FG"

    goto :goto_0

    .line 240
    :pswitch_3
    const-string v0, "FG_SRV_START"

    goto :goto_0

    .line 241
    :pswitch_4
    const-string v0, "FG_SRV_STOP"

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private final b(ILjava/lang/String;Z)Ljava/util/concurrent/Future;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lfxi;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lfxl;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, p0, p1, v2, v3}, Lfxl;-><init>(Lfxi;ILjava/lang/String;Z)V

    .line 41
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 42
    return-object v0
.end method


# virtual methods
.method final a(ILjava/lang/String;Z)V
    .locals 21

    .prologue
    .line 43
    invoke-static {}, Lhcw;->c()V

    .line 44
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 46
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lfwq;->c:Z

    .line 47
    if-eqz v4, :cond_0

    .line 48
    const-string v4, "BatteryMetricService"

    const-string v5, "shutdown - skipping capture"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 234
    :goto_0
    return-void

    .line 51
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->k:Lfxi$a;

    .line 52
    invoke-interface {v4}, Lfxi$a;->a()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->l:Lfxi$a;

    .line 53
    invoke-interface {v4}, Lfxi$a;->a()J

    move-result-wide v18

    .line 55
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->j:Lgax;

    invoke-interface {v4}, Lgax;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lfyn;

    .line 56
    iget-object v15, v4, Lfyn;->b:Ljava/lang/Long;

    .line 58
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->j:Lgax;

    invoke-interface {v4}, Lgax;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lfyn;

    .line 59
    iget-object v4, v4, Lfyn;->a:Ljava/lang/String;

    .line 61
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v14, v4

    .line 62
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lfxi;->i:Lgbn;

    .line 63
    move-object/from16 v0, p0

    iget-object v4, v0, Lfwq;->a:Landroid/app/Application;

    .line 66
    invoke-static {}, Lfmk;->i()J

    move-result-wide v6

    .line 67
    const-string v8, "systemhealth"

    .line 68
    invoke-virtual {v4, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/health/SystemHealthManager;

    .line 69
    invoke-virtual {v4}, Landroid/os/health/SystemHealthManager;->takeMyUidSnapshot()Landroid/os/health/HealthStats;

    move-result-object v4

    .line 70
    invoke-static {}, Lfmk;->i()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 71
    const-string v8, "SystemHealthCapture"

    const-string v9, "HealthStats capture took %d ms"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v11

    invoke-static {v8, v9, v10}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    invoke-static {}, Lfmk;->i()J

    move-result-wide v6

    .line 75
    invoke-static {v4}, Lfmk;->a(Landroid/os/health/HealthStats;)Lhti;

    move-result-object v20

    .line 76
    iget-object v4, v5, Lgbn;->a:Lgbd;

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lfmk;->a(Lhti;Lgbd;)V

    .line 77
    invoke-static {}, Lfmk;->i()J

    move-result-wide v4

    sub-long/2addr v4, v6

    .line 78
    const-string v6, "SystemHealthCapture"

    const-string v7, "Convert and hash battery capture took %d ms"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v6, v7, v8}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->h:Lgbl;

    .line 82
    new-instance v13, Lgdm;

    invoke-direct {v13}, Lgdm;-><init>()V

    .line 83
    iget-object v4, v4, Lgbl;->a:Lgcs;

    const-string v5, "primes.battery.snapshot"

    invoke-virtual {v4, v5, v13}, Lgcs;->a(Ljava/lang/String;Lhfz;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 84
    new-instance v4, Lgbm;

    iget-object v5, v13, Lgdm;->a:Lhti;

    iget-object v6, v13, Lgdm;->b:Ljava/lang/Long;

    iget-object v7, v13, Lgdm;->c:Ljava/lang/Long;

    iget-object v8, v13, Lgdm;->d:Ljava/lang/Long;

    iget-object v9, v13, Lgdm;->e:Ljava/lang/Long;

    iget-object v10, v13, Lgdm;->f:Ljava/lang/Integer;

    iget-object v11, v13, Lgdm;->g:Ljava/lang/String;

    iget-object v12, v13, Lgdm;->h:Ljava/lang/Boolean;

    iget-object v13, v13, Lgdm;->i:Lhrz;

    invoke-direct/range {v4 .. v13}, Lgbm;-><init>(Lhti;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Lhrz;)V

    move-object v6, v4

    .line 87
    :goto_2
    const-string v4, "BatteryMetricService"

    invoke-static {v4}, Lfmk;->e(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 88
    const-string v4, "BatteryMetricService"

    const-string v5, "\n\n\nCurrent Stats:\n%s\n metric_extension:\n%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v20, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput-object v9, v7, v8

    invoke-static {v4, v5, v7}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    const-string v5, "BatteryMetricService"

    const-string v7, "\nPrevious Stats:\n%s"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    if-nez v6, :cond_5

    const-string v4, "<null>"

    .line 91
    :goto_3
    aput-object v4, v8, v9

    invoke-static {v5, v7, v8}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->h:Lgbl;

    .line 93
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 94
    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    .line 96
    new-instance v9, Lgdm;

    invoke-direct {v9}, Lgdm;-><init>()V

    .line 97
    move-object/from16 v0, v20

    iput-object v0, v9, Lgdm;->a:Lhti;

    .line 98
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    iput-object v10, v9, Lgdm;->b:Ljava/lang/Long;

    .line 99
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    iput-object v10, v9, Lgdm;->c:Ljava/lang/Long;

    .line 100
    iput-object v15, v9, Lgdm;->d:Ljava/lang/Long;

    .line 101
    iput-object v14, v9, Lgdm;->e:Ljava/lang/Long;

    .line 102
    iput-object v5, v9, Lgdm;->f:Ljava/lang/Integer;

    .line 103
    move-object/from16 v0, p2

    iput-object v0, v9, Lgdm;->g:Ljava/lang/String;

    .line 104
    iput-object v7, v9, Lgdm;->h:Ljava/lang/Boolean;

    .line 105
    iput-object v8, v9, Lgdm;->i:Lhrz;

    .line 106
    iget-object v4, v4, Lgbl;->a:Lgcs;

    const-string v5, "primes.battery.snapshot"

    invoke-virtual {v4, v5, v9}, Lgcs;->b(Ljava/lang/String;Lhfz;)Z

    move-result v4

    .line 108
    if-nez v4, :cond_6

    .line 109
    invoke-virtual/range {p0 .. p0}, Lfxi;->c()V

    .line 110
    const-string v4, "BatteryMetricService"

    const-string v5, "Failure storing persistent snapshot and helper data"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_0

    .line 61
    :cond_3
    const/4 v4, 0x0

    move-object v14, v4

    goto/16 :goto_1

    .line 85
    :cond_4
    const/4 v4, 0x0

    move-object v6, v4

    goto :goto_2

    .line 90
    :cond_5
    :try_start_2
    iget-object v4, v6, Lgbm;->a:Lhti;

    goto :goto_3

    .line 112
    :cond_6
    const-string v4, "BatteryMetricService"

    invoke-static {v4}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 113
    const-string v5, "BatteryMetricService"

    const-string v7, "MEASUREMENT: %s <=> %s"

    const/4 v4, 0x2

    new-array v8, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 114
    if-eqz v6, :cond_a

    .line 115
    iget-object v4, v6, Lgbm;->f:Ljava/lang/Integer;

    .line 116
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lfxi;->a(I)Ljava/lang/String;

    move-result-object v4

    :goto_5
    aput-object v4, v8, v9

    const/4 v4, 0x1

    .line 117
    invoke-static/range {p1 .. p1}, Lfxi;->a(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    .line 118
    invoke-static {v5, v7, v8}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    :cond_7
    if-eqz v6, :cond_19

    .line 121
    if-nez v15, :cond_c

    .line 123
    iget-object v4, v6, Lgbm;->d:Ljava/lang/Long;

    .line 124
    if-nez v4, :cond_b

    const/4 v4, 0x1

    move v5, v4

    .line 128
    :goto_6
    if-nez v14, :cond_f

    .line 130
    iget-object v4, v6, Lgbm;->e:Ljava/lang/Long;

    .line 131
    if-nez v4, :cond_e

    const/4 v4, 0x1

    .line 135
    :goto_7
    if-eqz v5, :cond_11

    if-eqz v4, :cond_11

    const/4 v4, 0x1

    .line 136
    :goto_8
    if-eqz v4, :cond_19

    .line 138
    if-eqz v6, :cond_8

    .line 139
    iget-object v4, v6, Lgbm;->b:Ljava/lang/Long;

    .line 140
    if-eqz v4, :cond_8

    .line 141
    iget-object v4, v6, Lgbm;->c:Ljava/lang/Long;

    .line 142
    if-nez v4, :cond_12

    .line 143
    :cond_8
    const/4 v4, 0x0

    .line 179
    :cond_9
    :goto_9
    if-eqz v4, :cond_19

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxi;->i:Lgbn;

    .line 181
    iget-object v5, v6, Lgbm;->a:Lhti;

    .line 183
    move-object/from16 v0, v20

    invoke-static {v0, v5}, Lfmk;->a(Lhti;Lhti;)Lhti;

    move-result-object v5

    .line 184
    iget-object v4, v4, Lgbn;->a:Lgbd;

    invoke-static {v5, v4}, Lfmk;->b(Lhti;Lgbd;)V

    .line 187
    iget-object v4, v5, Lhti;->a:Ljava/lang/Long;

    if-eqz v4, :cond_18

    iget-object v4, v5, Lhti;->a:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-lez v4, :cond_18

    .line 190
    iget-object v4, v6, Lgbm;->f:Ljava/lang/Integer;

    .line 191
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 193
    iget-object v7, v6, Lgbm;->g:Ljava/lang/String;

    .line 196
    iget-object v8, v6, Lgbm;->h:Ljava/lang/Boolean;

    .line 197
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 199
    iget-object v9, v6, Lgbm;->i:Lhrz;

    .line 202
    iget-object v10, v6, Lgbm;->b:Ljava/lang/Long;

    .line 203
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 205
    new-instance v12, Lhqw;

    invoke-direct {v12}, Lhqw;-><init>()V

    .line 206
    sub-long v10, v16, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    iput-object v10, v12, Lhqw;->g:Ljava/lang/Long;

    .line 207
    iput v4, v12, Lhqw;->a:I

    .line 208
    if-eqz v8, :cond_17

    .line 209
    iput-object v7, v12, Lhqw;->d:Ljava/lang/String;

    .line 211
    :goto_a
    iput-object v9, v12, Lhqw;->e:Lhrz;

    .line 212
    move/from16 v0, p1

    iput v0, v12, Lhqw;->f:I

    .line 213
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v12, Lhqw;->i:Ljava/lang/Long;

    .line 214
    iput-object v5, v12, Lhqw;->h:Lhti;

    .line 215
    new-instance v4, Lhqx;

    invoke-direct {v4}, Lhqx;-><init>()V

    .line 216
    iput-object v12, v4, Lhqx;->a:Lhqw;

    .line 217
    new-instance v7, Lhtd;

    invoke-direct {v7}, Lhtd;-><init>()V

    .line 218
    iput-object v4, v7, Lhtd;->j:Lhqx;

    .line 221
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v7, v4}, Lfxi;->a(Ljava/lang/String;ZLhtd;Lhrz;)V

    .line 222
    const-string v4, "BatteryMetricService"

    invoke-static {v4}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 223
    const-string v4, "BatteryMetricService"

    const-string v7, "\n\n\nStats diff [%d ms in %s]\n%s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 225
    iget-object v6, v6, Lgbm;->b:Ljava/lang/Long;

    .line 226
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long v10, v16, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v9

    const/4 v6, 0x1

    .line 227
    invoke-static/range {p1 .. p1}, Lfxi;->a(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    const/4 v6, 0x2

    aput-object v5, v8, v6

    .line 228
    invoke-static {v4, v7, v8}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4

    .line 235
    :catchall_0
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4

    .line 116
    :cond_a
    :try_start_3
    const-string v4, "null"

    goto/16 :goto_5

    .line 124
    :cond_b
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_6

    .line 125
    :cond_c
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 126
    iget-object v7, v6, Lgbm;->d:Ljava/lang/Long;

    .line 127
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-nez v4, :cond_d

    const/4 v4, 0x1

    move v5, v4

    goto/16 :goto_6

    :cond_d
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_6

    .line 131
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 132
    :cond_f
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 133
    iget-object v4, v6, Lgbm;->e:Ljava/lang/Long;

    .line 134
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_10

    const/4 v4, 0x1

    goto/16 :goto_7

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 135
    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 145
    :cond_12
    iget-object v4, v6, Lgbm;->b:Ljava/lang/Long;

    .line 146
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v16, v4

    .line 148
    iget-object v7, v6, Lgbm;->c:Ljava/lang/Long;

    .line 149
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v18, v8

    .line 150
    const-string v7, "BatteryMetricService"

    invoke-static {v7}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 151
    const-string v7, "BatteryMetricService"

    const-string v10, "         elapsed/current: %d / %d \nstats elapsed/current: %d / %d \nduration elapsed/current: %d / %d"

    const/4 v11, 0x6

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 152
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    .line 153
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    .line 155
    iget-object v13, v6, Lgbm;->b:Ljava/lang/Long;

    .line 156
    aput-object v13, v11, v12

    const/4 v12, 0x3

    .line 158
    iget-object v13, v6, Lgbm;->c:Ljava/lang/Long;

    .line 159
    aput-object v13, v11, v12

    const/4 v12, 0x4

    .line 160
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x5

    .line 161
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    .line 162
    invoke-static {v7, v10, v11}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    :cond_13
    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gtz v7, :cond_14

    .line 164
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 165
    :cond_14
    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 166
    const-wide/16 v10, 0x19

    cmp-long v7, v4, v10

    if-ltz v7, :cond_15

    long-to-double v4, v4

    long-to-double v8, v8

    div-double/2addr v4, v8

    const-wide v8, 0x3f023456789abcdfL    # 3.472222222222222E-5

    cmpg-double v4, v4, v8

    if-gtz v4, :cond_16

    :cond_15
    const/4 v4, 0x1

    .line 167
    :goto_b
    if-nez v4, :cond_9

    .line 168
    const-string v5, "BatteryMetricService"

    const-string v7, "drift: elapsed / current: %d / %d - stats elapsed / current: %d / %d"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 169
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 170
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    .line 172
    iget-object v10, v6, Lgbm;->b:Ljava/lang/Long;

    .line 173
    aput-object v10, v8, v9

    const/4 v9, 0x3

    .line 175
    iget-object v10, v6, Lgbm;->c:Ljava/lang/Long;

    .line 176
    aput-object v10, v8, v9

    .line 177
    invoke-static {v5, v7, v8}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_9

    .line 166
    :cond_16
    const/4 v4, 0x0

    goto :goto_b

    .line 210
    :cond_17
    iput-object v7, v12, Lhqw;->c:Ljava/lang/String;

    goto/16 :goto_a

    .line 230
    :cond_18
    const-string v4, "BatteryMetricService"

    const-string v6, "Invalid battery duration: \'%d\', skipping measurement"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v5, v5, Lhti;->a:Ljava/lang/Long;

    aput-object v5, v7, v8

    invoke-static {v4, v6, v7}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 232
    :cond_19
    const-string v4, "BatteryMetricService"

    const-string v5, "Missing or inconsistent previous stats, skipping measurement"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 36
    .line 37
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lfxi;->b(ILjava/lang/String;Z)Ljava/util/concurrent/Future;

    .line 39
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lfxi;->b(ILjava/lang/String;Z)Ljava/util/concurrent/Future;

    .line 35
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    .line 243
    .line 244
    iget-object v0, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 245
    :try_start_0
    iget-boolean v0, p0, Lfxi;->f:Z

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 248
    invoke-static {v0}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfxi;->f:Z

    .line 250
    iget-object v0, p0, Lfxi;->h:Lgbl;

    .line 251
    iget-object v0, v0, Lgbl;->a:Lgcs;

    const-string v1, "primes.battery.snapshot"

    .line 252
    iget-object v0, v0, Lgcs;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    :cond_0
    iget-object v0, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 255
    return-void

    .line 256
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 21
    .line 22
    iget-object v0, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 23
    :try_start_0
    iget-boolean v0, p0, Lfxi;->f:Z

    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 26
    invoke-static {v0}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfxe;->a(Lfwt;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfxi;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_0
    iget-object v0, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 29
    return-void

    .line 30
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfxi;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method
