.class public final Lbly;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/util/List;

.field public static final b:Ljava/util/List;

.field public static c:Z

.field public static d:J

.field public static e:J

.field public static f:I

.field private static g:J

.field private static h:Landroid/support/v7/widget/RecyclerView$i;

.field private static i:J

.field private static j:Lbld$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lbly;->g:J

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lbly;->a:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lbly;->b:Ljava/util/List;

    .line 46
    new-instance v0, Lblz;

    invoke-direct {v0}, Lblz;-><init>()V

    sput-object v0, Lbly;->h:Landroid/support/v7/widget/RecyclerView$i;

    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lbly;->c:Z

    .line 48
    sput-wide v4, Lbly;->d:J

    .line 49
    sput-wide v4, Lbly;->e:J

    .line 50
    sput-wide v4, Lbly;->i:J

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lbly;->j:Lbld$a;

    .line 52
    const/4 v0, -0x1

    sput v0, Lbly;->f:I

    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 1
    const-string v0, "PerformanceReport.startRecording"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 3
    sput-wide v0, Lbly;->d:J

    sput-wide v0, Lbly;->i:J

    .line 4
    sget-object v0, Lbly;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    sget-object v0, Lbly;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 6
    sget-object v0, Lbly;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 7
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lbly;->c:Z

    .line 8
    return-void
.end method

.method public static a(I)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 31
    sget-object v0, Lbld$a;->h:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 32
    :cond_0
    return-void
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbly;->h:Landroid/support/v7/widget/RecyclerView$i;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/support/v7/widget/RecyclerView$i;)V

    .line 34
    sget-object v0, Lbly;->h:Landroid/support/v7/widget/RecyclerView$i;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$i;)V

    .line 35
    return-void
.end method

.method public static a(Lbld$a;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 12
    :goto_0
    sget-boolean v0, Lbly;->c:Z

    if-nez v0, :cond_0

    .line 29
    :goto_1
    return-void

    .line 14
    :cond_0
    sget-object v0, Lbly;->j:Lbld$a;

    if-ne p0, v0, :cond_1

    .line 15
    const-string v0, "PerformanceReport.recordClick"

    const-string v1, "%s is ignored"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbld$a;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    sput-object v6, Lbly;->j:Lbld$a;

    goto :goto_1

    .line 18
    :cond_1
    sput-object v6, Lbly;->j:Lbld$a;

    .line 19
    invoke-virtual {p0}, Lbld$a;->toString()Ljava/lang/String;

    .line 20
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 21
    sget-wide v2, Lbly;->i:J

    sub-long v2, v0, v2

    sget-wide v4, Lbly;->g:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 22
    invoke-static {}, Lbly;->a()V

    goto :goto_0

    .line 24
    :cond_2
    sput-wide v0, Lbly;->i:J

    .line 25
    sget-object v2, Lbly;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 26
    sput-wide v0, Lbly;->e:J

    .line 27
    :cond_3
    sget-object v2, Lbly;->a:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    sget-object v2, Lbly;->b:Ljava/util/List;

    sget-wide v4, Lbly;->d:J

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "PerformanceReport.stopRecording"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lbly;->c:Z

    .line 11
    return-void
.end method

.method public static b(I)V
    .locals 0

    .prologue
    .line 36
    sput p0, Lbly;->f:I

    .line 37
    return-void
.end method

.method public static b(Lbld$a;)V
    .locals 5

    .prologue
    .line 38
    sput-object p0, Lbly;->j:Lbld$a;

    .line 39
    const-string v0, "PerformanceReport.setIgnoreActionOnce"

    const-string v1, "next action will be ignored once if it is %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 40
    invoke-virtual {p0}, Lbld$a;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 41
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    return-void
.end method
