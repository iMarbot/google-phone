.class final Lasq$a;
.super Landroid/preference/Preference;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lasq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/telecom/PhoneAccountHandle;

.field private synthetic b:Lasq;


# direct methods
.method public constructor <init>(Lasq;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/PhoneAccount;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lasq$a;->b:Lasq;

    .line 2
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 3
    iput-object p3, p0, Lasq$a;->a:Landroid/telecom/PhoneAccountHandle;

    .line 4
    invoke-virtual {p4}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasq$a;->setTitle(Ljava/lang/CharSequence;)V

    .line 5
    invoke-virtual {p4}, Landroid/telecom/PhoneAccount;->getShortDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasq$a;->setSummary(Ljava/lang/CharSequence;)V

    .line 6
    invoke-virtual {p4}, Landroid/telecom/PhoneAccount;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lasq$a;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 9
    :cond_0
    return-void
.end method


# virtual methods
.method protected final onClick()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 10
    invoke-super {p0}, Landroid/preference/Preference;->onClick()V

    .line 11
    iget-object v0, p0, Lasq$a;->b:Lasq;

    invoke-virtual {v0}, Lasq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    .line 12
    iget-object v1, p0, Lasq$a;->b:Lasq;

    .line 13
    iget-object v1, v1, Lasq;->b:Landroid/os/Bundle;

    .line 14
    iget-object v2, p0, Lasq$a;->b:Lasq;

    .line 15
    iget-object v2, v2, Lasq;->c:Ljava/lang/String;

    .line 16
    iget-object v3, p0, Lasq$a;->a:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 17
    iget-object v1, p0, Lasq$a;->b:Lasq;

    .line 18
    iget-object v1, v1, Lasq;->a:Ljava/lang/String;

    .line 19
    iget-object v2, p0, Lasq$a;->b:Lasq;

    .line 20
    iget-object v2, v2, Lasq;->b:Landroid/os/Bundle;

    .line 21
    const/4 v3, 0x0

    iget-object v5, p0, Lasq$a;->b:Lasq;

    .line 22
    iget v5, v5, Lasq;->d:I

    move v6, v4

    .line 23
    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startWithFragment(Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Fragment;III)V

    .line 24
    return-void
.end method
