.class final Lfyw;
.super Lfwq;
.source "PG"

# interfaces
.implements Lfxb;
.implements Lgaj;


# instance fields
.field public final d:Landroid/content/SharedPreferences;

.field public final e:Z

.field private f:Lfxe;


# direct methods
.method constructor <init>(Landroid/app/Application;Lgdc;Lgax;Landroid/content/SharedPreferences;Z)V
    .locals 1

    .prologue
    .line 17
    sget v0, Lmg$c;->C:I

    invoke-direct {p0, p2, p1, p3, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 18
    iput-object p4, p0, Lfyw;->d:Landroid/content/SharedPreferences;

    .line 19
    iput-boolean p5, p0, Lfyw;->e:Z

    .line 20
    invoke-static {p1}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    iput-object v0, p0, Lfyw;->f:Lfxe;

    .line 21
    return-void
.end method

.method static a(Landroid/content/SharedPreferences;)Z
    .locals 12

    .prologue
    const-wide/32 v10, 0x2932e00

    const/4 v1, 0x1

    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    .line 1
    invoke-static {}, Lhcw;->c()V

    .line 3
    const-string v0, "primes.packageMetric.lastSendTime"

    invoke-interface {p0, v0, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 5
    invoke-static {}, Lfmk;->i()J

    move-result-wide v8

    .line 6
    cmp-long v0, v8, v4

    if-gez v0, :cond_1

    .line 7
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "primes.packageMetric.lastSendTime"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 8
    :goto_0
    if-eqz v0, :cond_0

    .line 9
    const-string v0, "PackageMetricService"

    const-string v3, "Failure storing timestamp persistently"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-wide v4, v6

    .line 11
    :cond_1
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    add-long v6, v4, v10

    cmp-long v0, v8, v6

    if-lez v0, :cond_4

    :cond_2
    move v0, v2

    .line 16
    :goto_1
    return v0

    :cond_3
    move v0, v2

    .line 7
    goto :goto_0

    .line 13
    :cond_4
    const-string v0, "PackageMetricService"

    invoke-static {v0}, Lfmk;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 14
    add-long/2addr v4, v10

    sub-long/2addr v4, v8

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 15
    const-string v3, "PackageMetricService"

    const-string v6, "SentRecently countdown: "

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    move v0, v1

    .line 16
    goto :goto_1

    .line 15
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lfyw;->f:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 27
    invoke-virtual {p0}, Lfyw;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lfyx;

    invoke-direct {v1, p0}, Lfyx;-><init>(Lfyw;)V

    .line 28
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 30
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lfyw;->f:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->b(Lfwt;)V

    .line 32
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfyw;->f:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->a(Lfwt;)V

    .line 23
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method
