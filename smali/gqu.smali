.class public final Lgqu;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Lgrc;

.field private d:Lgqv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgqu;->a:Ljava/lang/Integer;

    .line 4
    iput-object v0, p0, Lgqu;->b:Ljava/lang/Integer;

    .line 5
    iput-object v0, p0, Lgqu;->c:Lgrc;

    .line 6
    iput-object v0, p0, Lgqu;->d:Lgqv;

    .line 7
    iput-object v0, p0, Lgqu;->unknownFieldData:Lhfv;

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lgqu;->cachedSize:I

    .line 9
    return-void
.end method

.method private a(Lhfp;)Lgqu;
    .locals 6

    .prologue
    .line 32
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 33
    sparse-switch v0, :sswitch_data_0

    .line 35
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    :sswitch_0
    return-object p0

    .line 37
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 40
    invoke-static {v2}, Lgsm;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqu;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 44
    invoke-virtual {p0, p1, v0}, Lgqu;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 46
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 48
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 50
    packed-switch v2, :pswitch_data_0

    .line 52
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const/16 v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is not a valid enum EventType"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 56
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 57
    invoke-virtual {p0, p1, v0}, Lgqu;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 53
    :pswitch_0
    :try_start_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqu;->b:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 59
    :sswitch_3
    iget-object v0, p0, Lgqu;->c:Lgrc;

    if-nez v0, :cond_1

    .line 60
    new-instance v0, Lgrc;

    invoke-direct {v0}, Lgrc;-><init>()V

    iput-object v0, p0, Lgqu;->c:Lgrc;

    .line 61
    :cond_1
    iget-object v0, p0, Lgqu;->c:Lgrc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 63
    :sswitch_4
    iget-object v0, p0, Lgqu;->d:Lgqv;

    if-nez v0, :cond_2

    .line 64
    new-instance v0, Lgqv;

    invoke-direct {v0}, Lgqv;-><init>()V

    iput-object v0, p0, Lgqu;->d:Lgqv;

    .line 65
    :cond_2
    iget-object v0, p0, Lgqu;->d:Lgqv;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 19
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 20
    const/4 v1, 0x1

    iget-object v2, p0, Lgqu;->a:Ljava/lang/Integer;

    .line 21
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 22
    iget-object v1, p0, Lgqu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 23
    const/4 v1, 0x2

    iget-object v2, p0, Lgqu;->b:Ljava/lang/Integer;

    .line 24
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25
    :cond_0
    iget-object v1, p0, Lgqu;->c:Lgrc;

    if-eqz v1, :cond_1

    .line 26
    const/4 v1, 0x3

    iget-object v2, p0, Lgqu;->c:Lgrc;

    .line 27
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_1
    iget-object v1, p0, Lgqu;->d:Lgqv;

    if-eqz v1, :cond_2

    .line 29
    const/4 v1, 0x4

    iget-object v2, p0, Lgqu;->d:Lgqv;

    .line 30
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lgqu;->a(Lhfp;)Lgqu;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 10
    const/4 v0, 0x1

    iget-object v1, p0, Lgqu;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 11
    iget-object v0, p0, Lgqu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 12
    const/4 v0, 0x2

    iget-object v1, p0, Lgqu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 13
    :cond_0
    iget-object v0, p0, Lgqu;->c:Lgrc;

    if-eqz v0, :cond_1

    .line 14
    const/4 v0, 0x3

    iget-object v1, p0, Lgqu;->c:Lgrc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 15
    :cond_1
    iget-object v0, p0, Lgqu;->d:Lgqv;

    if-eqz v0, :cond_2

    .line 16
    const/4 v0, 0x4

    iget-object v1, p0, Lgqu;->d:Lgqv;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 17
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 18
    return-void
.end method
