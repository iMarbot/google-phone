.class final Lhdr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdb;


# instance fields
.field public final a:Lhdd;

.field public final b:Lhds;


# direct methods
.method constructor <init>(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhdr;->a:Lhdd;

    .line 3
    new-instance v0, Lhds;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1, p2, p3}, Lhds;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v0, p0, Lhdr;->b:Lhds;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()Lhdo;
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lhdr;->b:Lhds;

    .line 6
    iget v0, v0, Lhds;->c:I

    .line 7
    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lhdo;->a:Lhdo;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhdo;->b:Lhdo;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lhdr;->b:Lhds;

    .line 9
    iget v0, v0, Lhds;->c:I

    .line 10
    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lhdd;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lhdr;->a:Lhdd;

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lhdr;->b:Lhds;

    .line 14
    iget v0, v0, Lhds;->f:I

    .line 15
    iget-object v1, p0, Lhdr;->b:Lhds;

    .line 16
    iget v1, v1, Lhds;->g:I

    .line 17
    iget-object v2, p0, Lhdr;->b:Lhds;

    .line 18
    iget v2, v2, Lhds;->d:I

    .line 19
    invoke-static {v0, v1, v2}, Lheb;->a(III)Z

    move-result v0

    return v0
.end method
