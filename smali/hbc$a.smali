.class public final Lhbc$a;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final d:Lhbc$a;

.field private static volatile f:Lhdm;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Z

.field private e:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lhbc$a;

    invoke-direct {v0}, Lhbc$a;-><init>()V

    .line 93
    sput-object v0, Lhbc$a;->d:Lhbc$a;

    invoke-virtual {v0}, Lhbc$a;->makeImmutable()V

    .line 94
    const-class v0, Lhbc$a;

    sget-object v1, Lhbc$a;->d:Lhbc$a;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 95
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lhbc$a;->e:B

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lhbc$a;->b:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 90
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0002\u0001\u0508\u0000\u0002\u0507\u0001"

    .line 91
    sget-object v2, Lhbc$a;->d:Lhbc$a;

    invoke-static {v2, v1, v0}, Lhbc$a;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 30
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 31
    :pswitch_0
    new-instance v0, Lhbc$a;

    invoke-direct {v0}, Lhbc$a;-><init>()V

    .line 87
    :goto_0
    return-object v0

    .line 32
    :pswitch_1
    iget-byte v3, p0, Lhbc$a;->e:B

    .line 33
    if-ne v3, v1, :cond_0

    sget-object v0, Lhbc$a;->d:Lhbc$a;

    goto :goto_0

    .line 34
    :cond_0
    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 35
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 36
    sget-boolean v4, Lhbc$a;->usingExperimentalRuntime:Z

    if-eqz v4, :cond_5

    .line 37
    invoke-virtual {p0}, Lhbc$a;->isInitializedInternal()Z

    move-result v4

    if-nez v4, :cond_3

    .line 38
    if-eqz v3, :cond_2

    iput-byte v0, p0, Lhbc$a;->e:B

    :cond_2
    move-object v0, v2

    .line 39
    goto :goto_0

    .line 40
    :cond_3
    if-eqz v3, :cond_4

    iput-byte v1, p0, Lhbc$a;->e:B

    .line 41
    :cond_4
    sget-object v0, Lhbc$a;->d:Lhbc$a;

    goto :goto_0

    .line 43
    :cond_5
    iget v3, p0, Lhbc$a;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_6

    move v3, v1

    .line 44
    :goto_1
    if-nez v3, :cond_7

    move-object v0, v2

    .line 45
    goto :goto_0

    :cond_6
    move v3, v0

    .line 43
    goto :goto_1

    .line 47
    :cond_7
    iget v3, p0, Lhbc$a;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    .line 48
    :goto_2
    if-nez v1, :cond_9

    move-object v0, v2

    .line 49
    goto :goto_0

    :cond_8
    move v1, v0

    .line 47
    goto :goto_2

    .line 50
    :cond_9
    sget-object v0, Lhbc$a;->d:Lhbc$a;

    goto :goto_0

    :pswitch_2
    move-object v0, v2

    .line 51
    goto :goto_0

    .line 52
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v0, v2}, Lhbr$a;-><init>(B[[[[[S)V

    move-object v0, v1

    goto :goto_0

    .line 53
    :pswitch_4
    check-cast p2, Lhaq;

    .line 54
    check-cast p3, Lhbg;

    .line 55
    if-nez p3, :cond_a

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :sswitch_0
    move v0, v1

    .line 58
    :cond_a
    :goto_3
    if-nez v0, :cond_b

    .line 59
    :try_start_0
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 60
    sparse-switch v2, :sswitch_data_0

    .line 63
    invoke-virtual {p0, v2, p2}, Lhbc$a;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 64
    goto :goto_3

    .line 65
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 66
    iget v3, p0, Lhbc$a;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhbc$a;->a:I

    .line 67
    iput-object v2, p0, Lhbc$a;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 73
    :catch_0
    move-exception v0

    .line 74
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    throw v0

    .line 69
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhbc$a;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhbc$a;->a:I

    .line 70
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lhbc$a;->c:Z
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 75
    :catch_1
    move-exception v0

    .line 76
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 77
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 79
    :cond_b
    :pswitch_5
    sget-object v0, Lhbc$a;->d:Lhbc$a;

    goto/16 :goto_0

    .line 80
    :pswitch_6
    sget-object v0, Lhbc$a;->f:Lhdm;

    if-nez v0, :cond_d

    const-class v1, Lhbc$a;

    monitor-enter v1

    .line 81
    :try_start_4
    sget-object v0, Lhbc$a;->f:Lhdm;

    if-nez v0, :cond_c

    .line 82
    new-instance v0, Lhaa;

    sget-object v2, Lhbc$a;->d:Lhbc$a;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhbc$a;->f:Lhdm;

    .line 83
    :cond_c
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 84
    :cond_d
    sget-object v0, Lhbc$a;->f:Lhdm;

    goto/16 :goto_0

    .line 83
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 85
    :pswitch_7
    iget-byte v0, p0, Lhbc$a;->e:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 86
    :pswitch_8
    if-nez p2, :cond_e

    :goto_4
    int-to-byte v0, v0

    iput-byte v0, p0, Lhbc$a;->e:B

    move-object v0, v2

    .line 87
    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 86
    goto :goto_4

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 13
    iget v0, p0, Lhbc$a;->memoizedSerializedSize:I

    .line 14
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29
    :goto_0
    return v0

    .line 15
    :cond_0
    sget-boolean v0, Lhbc$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 16
    invoke-virtual {p0}, Lhbc$a;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhbc$a;->memoizedSerializedSize:I

    .line 17
    iget v0, p0, Lhbc$a;->memoizedSerializedSize:I

    goto :goto_0

    .line 18
    :cond_1
    const/4 v0, 0x0

    .line 19
    iget v1, p0, Lhbc$a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 22
    iget-object v0, p0, Lhbc$a;->b:Ljava/lang/String;

    .line 23
    invoke-static {v2, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24
    :cond_2
    iget v1, p0, Lhbc$a;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 25
    iget-boolean v1, p0, Lhbc$a;->c:Z

    .line 26
    invoke-static {v3, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_3
    iget-object v1, p0, Lhbc$a;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    iput v0, p0, Lhbc$a;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5
    iget v0, p0, Lhbc$a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7
    iget-object v0, p0, Lhbc$a;->b:Ljava/lang/String;

    .line 8
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 9
    :cond_0
    iget v0, p0, Lhbc$a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 10
    iget-boolean v0, p0, Lhbc$a;->c:Z

    invoke-virtual {p1, v2, v0}, Lhaw;->a(IZ)V

    .line 11
    :cond_1
    iget-object v0, p0, Lhbc$a;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    .line 12
    return-void
.end method
