.class public final Lbut;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static A:[Ljava/lang/String;

.field private static z:[Ljava/lang/String;


# instance fields
.field private B:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:I

.field public j:I

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:I

.field public n:I

.field public o:J

.field public p:Ljava/lang/String;

.field public q:J

.field public r:Landroid/net/Uri;

.field public s:Landroid/net/Uri;

.field public t:Landroid/graphics/drawable/Drawable;

.field public u:Z

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Z

.field public y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 137
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "lookup"

    aput-object v1, v0, v5

    const-string v1, "number"

    aput-object v1, v0, v6

    const-string v1, "normalized_number"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "label"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "photo_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "custom_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "send_to_voicemail"

    aput-object v2, v0, v1

    sput-object v0, Lbut;->z:[Ljava/lang/String;

    .line 138
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "lookup"

    aput-object v1, v0, v5

    const-string v1, "number"

    aput-object v1, v0, v6

    const-string v1, "normalized_number"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "label"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "photo_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "custom_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "send_to_voicemail"

    aput-object v2, v0, v1

    sput-object v0, Lbut;->A:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean v0, p0, Lbut;->x:Z

    .line 3
    iput-boolean v0, p0, Lbut;->y:Z

    .line 4
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbut;->q:J

    .line 5
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;)Lbut;
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 14
    new-instance v3, Lbut;

    invoke-direct {v3}, Lbut;-><init>()V

    .line 15
    iput-object v1, v3, Lbut;->t:Landroid/graphics/drawable/Drawable;

    .line 16
    iput-boolean v0, v3, Lbut;->k:Z

    .line 17
    iput-boolean v0, v3, Lbut;->u:Z

    .line 18
    iput-object v1, v3, Lbut;->a:Ljava/lang/String;

    .line 19
    iput-object v1, v3, Lbut;->B:Ljava/lang/String;

    .line 20
    iput v0, v3, Lbut;->m:I

    .line 21
    iput-object v1, v3, Lbut;->l:Ljava/lang/String;

    .line 22
    iput v0, v3, Lbut;->n:I

    .line 23
    iput-wide v6, v3, Lbut;->q:J

    .line 24
    const-string v0, "CallerInfo"

    const-string v4, "getCallerInfo() based on cursor..."

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 106
    :goto_0
    return-object v0

    .line 28
    :cond_1
    const-string v0, "number"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 29
    if-eq v0, v2, :cond_2

    if-eqz p1, :cond_2

    .line 30
    invoke-static {p2, v0, p1}, Lbmw;->a(Landroid/database/Cursor;ILandroid/net/Uri;)Landroid/database/Cursor;

    move-result-object p2

    .line 31
    if-eqz p2, :cond_c

    .line 32
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->c:Ljava/lang/String;

    .line 34
    :cond_2
    const-string v0, "display_name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 35
    if-eq v0, v2, :cond_4

    .line 36
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_d

    .line 40
    :cond_3
    :goto_1
    iput-object v0, v3, Lbut;->a:Ljava/lang/String;

    .line 41
    :cond_4
    const-string v0, "normalized_number"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 42
    if-eq v0, v2, :cond_5

    .line 43
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->d:Ljava/lang/String;

    .line 44
    :cond_5
    const-string v0, "label"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 45
    if-eq v0, v2, :cond_6

    .line 46
    const-string v4, "type"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 47
    if-eq v4, v2, :cond_6

    .line 48
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lbut;->m:I

    .line 49
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->B:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v4, v3, Lbut;->m:I

    iget-object v5, v3, Lbut;->B:Ljava/lang/String;

    invoke-static {v0, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->l:Ljava/lang/String;

    .line 53
    :cond_6
    const-string v0, "lookup"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 54
    if-eq v0, v2, :cond_7

    .line 55
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->p:Ljava/lang/String;

    .line 57
    :cond_7
    const-string v0, "CallerInfo"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x33

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "- getColumnIndexForPersonId: contactRef URI = \'"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v4, "content://com.android.contacts/data/phones"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 61
    const-string v0, "CallerInfo"

    const-string v4, "\'data/phones\' URI; using RawContacts.CONTACT_ID"

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    const-string v0, "contact_id"

    move-object v4, v0

    .line 70
    :goto_2
    if-eqz v4, :cond_11

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 71
    :goto_3
    const-string v5, "CallerInfo"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x47

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "==> Using column \'"

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "\' (columnIndex = "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ") for person_id lookup..."

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    if-eq v0, v2, :cond_12

    .line 75
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 76
    cmp-long v0, v4, v6

    if-eqz v0, :cond_9

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x18

    if-ge v0, v6, :cond_8

    .line 77
    invoke-static {v4, v5}, Landroid/provider/ContactsContract$Contacts;->isEnterpriseContactId(J)Z

    move-result v0

    if-nez v0, :cond_9

    .line 78
    :cond_8
    iput-wide v4, v3, Lbut;->o:J

    .line 79
    const-string v0, "CallerInfo"

    iget-wide v6, v3, Lbut;->o:J

    const/16 v8, 0x32

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "==> got info.contactIdOrZero: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    :cond_9
    :goto_4
    const-string v0, "photo_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 82
    if-eq v0, v2, :cond_13

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_13

    .line 83
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v3, Lbut;->r:Landroid/net/Uri;

    .line 85
    :goto_5
    const-string v0, "custom_ringtone"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 86
    if-eq v0, v2, :cond_15

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_15

    .line 87
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 88
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, v3, Lbut;->s:Landroid/net/Uri;

    .line 91
    :goto_6
    const-string v0, "send_to_voicemail"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 92
    if-eq v0, v2, :cond_a

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 93
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, v3, Lbut;->k:Z

    .line 94
    if-nez p1, :cond_16

    move-object v0, v1

    .line 98
    :goto_7
    if-eqz v0, :cond_b

    .line 99
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 102
    :cond_b
    :goto_8
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/Long;Ljava/lang/Long;)J

    move-result-wide v4

    iput-wide v4, v3, Lbut;->q:J

    .line 103
    iget-object v0, v3, Lbut;->p:Ljava/lang/String;

    iget-wide v4, v3, Lbut;->q:J

    .line 104
    invoke-static {p0, v0, v4, v5, v1}, Lbmm;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lbut;->b:Ljava/lang/String;

    .line 105
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    move-object v0, v3

    .line 106
    goto/16 :goto_0

    :cond_c
    move-object v0, v3

    .line 33
    goto/16 :goto_0

    :cond_d
    move-object v0, v1

    .line 39
    goto/16 :goto_1

    .line 63
    :cond_e
    const-string v4, "content://com.android.contacts/data"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 64
    const-string v0, "CallerInfo"

    const-string v4, "\'data\' URI; using Data.CONTACT_ID"

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    const-string v0, "contact_id"

    move-object v4, v0

    goto/16 :goto_2

    .line 66
    :cond_f
    const-string v4, "content://com.android.contacts/phone_lookup"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 67
    const-string v0, "CallerInfo"

    const-string v4, "\'phone_lookup\' URI; using PhoneLookup._ID"

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {p1}, Lbib;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto/16 :goto_2

    .line 69
    :cond_10
    const-string v4, "CallerInfo"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unexpected prefix for contactRef \'"

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\'"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v1

    goto/16 :goto_2

    :cond_11
    move v0, v2

    .line 70
    goto/16 :goto_3

    .line 80
    :cond_12
    const-string v0, "CallerInfo"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Couldn\'t find contactId column for "

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-wide v4, v6

    goto/16 :goto_4

    .line 84
    :cond_13
    iput-object v1, v3, Lbut;->r:Landroid/net/Uri;

    goto/16 :goto_5

    .line 89
    :cond_14
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v3, Lbut;->s:Landroid/net/Uri;

    goto/16 :goto_6

    .line 90
    :cond_15
    iput-object v1, v3, Lbut;->s:Landroid/net/Uri;

    goto/16 :goto_6

    .line 96
    :cond_16
    const-string v0, "directory"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :catch_0
    move-exception v0

    goto/16 :goto_8
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Lbut;)Lbut;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-boolean v0, p2, Lbut;->k:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-static {p1}, Lbmw;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    sget-object v1, Landroid/provider/ContactsContract$PhoneLookup;->ENTERPRISE_CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 111
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 115
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 116
    invoke-static {p0, v1, v0}, Lbut;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/database/Cursor;)Lbut;

    move-result-object p2

    .line 118
    :cond_0
    return-object p2
.end method

.method public static a(Landroid/net/Uri;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 7
    sget-object v0, Lbut;->z:[Ljava/lang/String;

    .line 13
    :goto_0
    return-object v0

    .line 8
    :cond_0
    const-string v0, "sip"

    const/4 v1, 0x0

    .line 9
    invoke-virtual {p0, v0, v1}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    sget-object v0, Lbut;->z:[Ljava/lang/String;

    goto :goto_0

    .line 12
    :cond_1
    sget-object v0, Lbut;->A:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/content/Context;)Lbut;
    .locals 1

    .prologue
    .line 119
    const v0, 0x7f11016b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbut;->a:Ljava/lang/String;

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lbut;->c:Ljava/lang/String;

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbut;->x:Z

    .line 122
    return-object p0
.end method

.method final b(Landroid/content/Context;)Lbut;
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbut;->y:Z

    .line 124
    :try_start_0
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbut;->a:Ljava/lang/String;

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lbut;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    return-object p0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    const-string v1, "CallerInfo"

    const-string v2, "Cannot access VoiceMail."

    invoke-static {v1, v2, v0}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 131
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " { "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "name "

    .line 132
    iget-object v0, p0, Lbut;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", phoneNumber "

    .line 133
    iget-object v0, p0, Lbut;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    return-object v0

    .line 132
    :cond_0
    const-string v0, "non-null"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 133
    :cond_2
    const-string v0, "non-null"

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method
