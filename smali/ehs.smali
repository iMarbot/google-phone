.class public final Lehs;
.super Ljava/lang/Object;


# instance fields
.field public a:Landroid/os/Looper;

.field private b:Legm;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ledi;
    .locals 3

    .prologue
    .line 1
    iget-object v0, p0, Lehs;->b:Legm;

    if-nez v0, :cond_0

    new-instance v0, Legm;

    invoke-direct {v0}, Legm;-><init>()V

    iput-object v0, p0, Lehs;->b:Legm;

    :cond_0
    iget-object v0, p0, Lehs;->a:Landroid/os/Looper;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lehs;->a:Landroid/os/Looper;

    :cond_1
    :goto_0
    new-instance v0, Ledi;

    iget-object v1, p0, Lehs;->b:Legm;

    iget-object v2, p0, Lehs;->a:Landroid/os/Looper;

    .line 2
    invoke-direct {v0, v1, v2}, Ledi;-><init>(Legm;Landroid/os/Looper;)V

    .line 3
    return-object v0

    .line 1
    :cond_2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lehs;->a:Landroid/os/Looper;

    goto :goto_0
.end method

.method public final a(Legm;)Lehs;
    .locals 1

    const-string v0, "StatusExceptionMapper must not be null."

    invoke-static {p1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lehs;->b:Legm;

    return-object p0
.end method
