.class final Lfxr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfxt;


# instance fields
.field private a:Lgcw;

.field private b:Lgcv;

.field private c:Lgcv;

.field private d:Lgcv;

.field private e:Lgcv;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lgay;->d()Lgcw;

    move-result-object v0

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcw;

    iput-object v0, p0, Lfxr;->a:Lgcw;

    .line 3
    new-instance v0, Lgcv;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Lgcv;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lfxr;->b:Lgcv;

    .line 4
    new-instance v0, Lgcv;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1}, Lgcv;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lfxr;->c:Lgcv;

    .line 5
    new-instance v0, Lgcv;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Lgcv;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lfxr;->d:Lgcv;

    .line 6
    new-instance v0, Lgcv;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1}, Lgcv;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lfxr;->e:Lgcv;

    .line 7
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x0

    .line 8
    iget-object v0, p0, Lfxr;->a:Lgcw;

    iget-object v1, p0, Lfxr;->b:Lgcv;

    int-to-long v6, p1

    invoke-virtual {v0, v1, v6, v7}, Lgcw;->a(Lgcv;J)V

    .line 9
    iget-object v6, p0, Lfxr;->a:Lgcw;

    iget-object v7, p0, Lfxr;->c:Lgcv;

    if-le p1, p2, :cond_0

    move-wide v0, v2

    :goto_0
    invoke-virtual {v6, v7, v0, v1}, Lgcw;->a(Lgcv;J)V

    .line 10
    iget-object v6, p0, Lfxr;->a:Lgcw;

    iget-object v7, p0, Lfxr;->d:Lgcv;

    .line 11
    const/16 v0, 0x96

    if-le p1, v0, :cond_1

    move-wide v0, v2

    .line 12
    :goto_1
    invoke-virtual {v6, v7, v0, v1}, Lgcw;->a(Lgcv;J)V

    .line 13
    iget-object v0, p0, Lfxr;->a:Lgcw;

    iget-object v1, p0, Lfxr;->e:Lgcv;

    const/16 v6, 0x2bc

    if-le p1, v6, :cond_2

    :goto_2
    invoke-virtual {v0, v1, v2, v3}, Lgcw;->a(Lgcv;J)V

    .line 14
    return-void

    :cond_0
    move-wide v0, v4

    .line 9
    goto :goto_0

    :cond_1
    move-wide v0, v4

    .line 11
    goto :goto_1

    :cond_2
    move-wide v2, v4

    .line 13
    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lhru;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return-object v0
.end method
