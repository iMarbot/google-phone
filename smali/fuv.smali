.class public final Lfuv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public clearcutWrapper:Lfuu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getClearcutWrapper()Lfuu;
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lfuv;->clearcutWrapper:Lfuu;

    .line 7
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 8
    check-cast v0, Lfuu;

    return-object v0
.end method

.method private final reportInternal(ILjava/lang/String;Lgry;)V
    .locals 4

    .prologue
    .line 21
    invoke-static {}, Lfmw;->a()V

    .line 22
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v0

    invoke-virtual {v0}, Lfuu;->createDefaultLogRequest()Lgsi;

    move-result-object v0

    .line 23
    if-eqz p2, :cond_0

    .line 24
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->a:Lgrv;

    iput-object p2, v1, Lgrv;->b:Ljava/lang/String;

    .line 25
    :cond_0
    iget-object v1, v0, Lgsi;->a:Lgrw;

    new-instance v2, Lgrx;

    invoke-direct {v2}, Lgrx;-><init>()V

    iput-object v2, v1, Lgrw;->d:Lgrx;

    .line 26
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgrx;->a:Ljava/lang/Integer;

    .line 27
    if-eqz p3, :cond_1

    .line 28
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    iput-object p3, v1, Lgrx;->b:Lgry;

    .line 29
    :cond_1
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfuu;->logHangoutLogRequest(Lgsi;)V

    .line 30
    const-string v0, "Reporting impression %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method private final reportTimingLogEntryInternal(Lgsj;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lfmw;->a()V

    .line 67
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v0

    invoke-virtual {v0}, Lfuu;->createDefaultLogRequest()Lgsi;

    move-result-object v0

    .line 68
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->a:Lgrv;

    iput-object p2, v1, Lgrv;->b:Ljava/lang/String;

    .line 69
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iput-object p1, v1, Lgrw;->e:Lgsj;

    .line 70
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfuu;->logHangoutLogRequest(Lgsi;)V

    .line 71
    const-string v0, "Reporting timingLogEntry to clearcut. sessionId: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    return-void
.end method

.method private final reportTransportEventInternal(Lgst;J)V
    .locals 4

    .prologue
    .line 55
    invoke-static {}, Lfmw;->a()V

    .line 56
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v0

    invoke-virtual {v0}, Lfuu;->createDefaultLogRequest()Lgsi;

    move-result-object v0

    .line 57
    new-instance v1, Lgsh;

    invoke-direct {v1}, Lgsh;-><init>()V

    iput-object v1, v0, Lgsi;->c:Lgsh;

    .line 58
    iget-object v1, v0, Lgsi;->c:Lgsh;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lgsh;->a:Ljava/lang/Long;

    .line 59
    iput-object p1, v0, Lgsi;->b:Lgst;

    .line 60
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfuu;->logHangoutLogRequest(Lgsi;)V

    .line 61
    return-void
.end method

.method private final reportUmaEventInternal(JII)V
    .locals 5

    .prologue
    .line 36
    invoke-static {}, Lfmw;->a()V

    .line 37
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v0

    invoke-virtual {v0}, Lfuu;->createDefaultLogRequest()Lgsi;

    move-result-object v0

    .line 38
    iget-object v1, v0, Lgsi;->a:Lgrw;

    new-instance v2, Lgrx;

    invoke-direct {v2}, Lgrx;-><init>()V

    iput-object v2, v1, Lgrw;->d:Lgrx;

    .line 39
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    const/16 v2, 0xbc7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgrx;->a:Ljava/lang/Integer;

    .line 40
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    new-instance v2, Lgse;

    invoke-direct {v2}, Lgse;-><init>()V

    iput-object v2, v1, Lgrx;->d:Lgse;

    .line 41
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    iget-object v1, v1, Lgrx;->d:Lgse;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lgse;->a:Ljava/lang/Long;

    .line 42
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    iget-object v1, v1, Lgrx;->d:Lgse;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgse;->b:Ljava/lang/Integer;

    .line 43
    iget-object v1, v0, Lgsi;->a:Lgrw;

    iget-object v1, v1, Lgrw;->d:Lgrx;

    iget-object v1, v1, Lgrx;->d:Lgse;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lgse;->c:Ljava/lang/Integer;

    .line 44
    invoke-direct {p0}, Lfuv;->getClearcutWrapper()Lfuu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfuu;->logHangoutLogRequest(Lgsi;)V

    .line 45
    const-string v0, "Reporting UMA event. id: %d, type: %d, value: %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 46
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 47
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 48
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 49
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    return-void
.end method


# virtual methods
.method final synthetic lambda$report$0$ImpressionReporter(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1, v0, v0}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    return-void
.end method

.method final synthetic lambda$report$1$ImpressionReporter(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    return-void
.end method

.method final synthetic lambda$report$2$ImpressionReporter(ILgry;)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    return-void
.end method

.method final synthetic lambda$reportTimingLogEntry$5$ImpressionReporter(Lgsj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lfuv;->reportTimingLogEntryInternal(Lgsj;Ljava/lang/String;)V

    return-void
.end method

.method final synthetic lambda$reportTransportEvent$4$ImpressionReporter(Lgst;J)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lfuv;->reportTransportEventInternal(Lgst;J)V

    return-void
.end method

.method final synthetic lambda$reportUmaEvent$3$ImpressionReporter(JII)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3, p4}, Lfuv;->reportUmaEventInternal(JII)V

    return-void
.end method

.method public final report(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    invoke-direct {p0, p1, v1, v1}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    .line 12
    :goto_0
    return-void

    .line 11
    :cond_0
    new-instance v0, Lfuw;

    invoke-direct {v0, p0, p1}, Lfuw;-><init>(Lfuv;I)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final report(ILgry;)V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    .line 20
    :goto_0
    return-void

    .line 19
    :cond_0
    new-instance v0, Lfuy;

    invoke-direct {v0, p0, p1, p2}, Lfuy;-><init>(Lfuv;ILgry;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final report(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfuv;->reportInternal(ILjava/lang/String;Lgry;)V

    .line 16
    :goto_0
    return-void

    .line 15
    :cond_0
    new-instance v0, Lfux;

    invoke-direct {v0, p0, p1, p2}, Lfux;-><init>(Lfuv;ILjava/lang/String;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final reportTimingLogEntry(Lgsj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0, p1, p2}, Lfuv;->reportTimingLogEntryInternal(Lgsj;Ljava/lang/String;)V

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v0, Lfvb;

    invoke-direct {v0, p0, p1, p2}, Lfvb;-><init>(Lfuv;Lgsj;Ljava/lang/String;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final reportTransportEvent(Lgst;J)V
    .locals 2

    .prologue
    .line 51
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lfuv;->reportTransportEventInternal(Lgst;J)V

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Lfva;

    invoke-direct {v0, p0, p1, p2, p3}, Lfva;-><init>(Lfuv;Lgst;J)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final reportUmaEvent(JII)V
    .locals 7

    .prologue
    .line 32
    invoke-static {}, Lhcw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Lfuv;->reportUmaEventInternal(JII)V

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_0
    new-instance v0, Lfuz;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lfuz;-><init>(Lfuv;JII)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final setClearcutWrapper(Lfuu;)V
    .locals 0

    .prologue
    .line 2
    if-nez p1, :cond_0

    .line 5
    :goto_0
    return-void

    .line 4
    :cond_0
    iput-object p1, p0, Lfuv;->clearcutWrapper:Lfuu;

    goto :goto_0
.end method
