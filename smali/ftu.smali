.class final Lftu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfth;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfmt;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lftu;-><init>()V

    return-void
.end method

.method private final createModifiedPushNotification(Lgoa;[Lgou;)Lgqi;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lgqf;

    invoke-direct {v0}, Lgqf;-><init>()V

    .line 20
    iput-object p1, v0, Lgqf;->syncMetadata:Lgoa;

    .line 21
    iput-object p2, v0, Lgqf;->modified:[Lgou;

    .line 22
    invoke-direct {p0, v0}, Lftu;->wrap(Lgqf;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method private final wrap(Lgqf;)Lgqi;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    .line 24
    iput-object p1, v0, Lgqi;->mediaSourcesUpdate:Lgqf;

    .line 25
    return-object v0
.end method


# virtual methods
.method public final createAddPushNotification(Lgoi$b;)Lgqi;
    .locals 4

    .prologue
    .line 2
    iget-object v1, p1, Lgoi$b;->syncMetadata:Lgoa;

    .line 3
    iget-object v0, p1, Lgoi$b;->source:Lgou;

    if-nez v0, :cond_0

    iget-object v0, p1, Lgoi$b;->resource:[Lgou;

    .line 4
    :goto_0
    invoke-direct {p0, v1, v0}, Lftu;->createModifiedPushNotification(Lgoa;[Lgou;)Lgqi;

    move-result-object v0

    return-object v0

    .line 3
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgou;

    const/4 v2, 0x0

    iget-object v3, p1, Lgoi$b;->source:Lgou;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createAddPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lgoi$b;

    invoke-virtual {p0, p1}, Lftu;->createAddPushNotification(Lgoi$b;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createModifyPushNotification(Lgoi$c;)Lgqi;
    .locals 4

    .prologue
    .line 5
    iget-object v1, p1, Lgoi$c;->syncMetadata:Lgoa;

    .line 6
    iget-object v0, p1, Lgoi$c;->source:Lgou;

    if-nez v0, :cond_0

    iget-object v0, p1, Lgoi$c;->resource:[Lgou;

    .line 7
    :goto_0
    invoke-direct {p0, v1, v0}, Lftu;->createModifiedPushNotification(Lgoa;[Lgou;)Lgqi;

    move-result-object v0

    return-object v0

    .line 6
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lgou;

    const/4 v2, 0x0

    iget-object v3, p1, Lgoi$c;->source:Lgou;

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public final bridge synthetic createModifyPushNotification(Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lgoi$c;

    invoke-virtual {p0, p1}, Lftu;->createModifyPushNotification(Lgoi$c;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final createRemovePushNotification(Lgpc;Lgoi$d;)Lgqi;
    .locals 5

    .prologue
    .line 8
    new-instance v1, Lgqf;

    invoke-direct {v1}, Lgqf;-><init>()V

    .line 9
    iget-object v0, p2, Lgoi$d;->syncMetadata:Lgoa;

    iput-object v0, v1, Lgqf;->syncMetadata:Lgoa;

    .line 10
    iget-object v0, p1, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Lgnz;

    .line 11
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p1, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 12
    new-instance v3, Lgnz;

    invoke-direct {v3}, Lgnz;-><init>()V

    aput-object v3, v2, v0

    .line 13
    aget-object v3, v2, v0

    iget-object v4, p1, Lgpc;->hangoutId:Ljava/lang/String;

    iput-object v4, v3, Lgnz;->hangoutId:Ljava/lang/String;

    .line 14
    aget-object v3, v2, v0

    iget-object v4, p1, Lgpc;->participantId:Ljava/lang/String;

    iput-object v4, v3, Lgnz;->participantId:Ljava/lang/String;

    .line 15
    aget-object v3, v2, v0

    iget-object v4, p1, Lgpc;->resourceId:[Ljava/lang/String;

    aget-object v4, v4, v0

    iput-object v4, v3, Lgnz;->sourceId:Ljava/lang/String;

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17
    :cond_0
    iput-object v2, v1, Lgqf;->deleted:[Lgnz;

    .line 18
    invoke-direct {p0, v1}, Lftu;->wrap(Lgqf;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic createRemovePushNotification(Lhfz;Lhfz;)Lgqi;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lgpc;

    check-cast p2, Lgoi$d;

    invoke-virtual {p0, p1, p2}, Lftu;->createRemovePushNotification(Lgpc;Lgoi$d;)Lgqi;

    move-result-object v0

    return-object v0
.end method
