.class final Lfru;
.super Lfof;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfrn;


# direct methods
.method private constructor <init>(Lfrn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfru;->this$0:Lfrn;

    invoke-direct {p0}, Lfof;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfrn;Lfmt;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lfru;-><init>(Lfrn;)V

    return-void
.end method


# virtual methods
.method public final onEndpointEvent(Lfue;Lfuf;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2
    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfru;->this$0:Lfrn;

    iget-object v1, v1, Lfrn;->participant:Lfrh;

    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    instance-of v0, p2, Lfuj;

    if-eqz v0, :cond_2

    .line 4
    check-cast p2, Lfuj;

    .line 5
    iget v0, p2, Lfuj;->type:I

    if-eq v0, v4, :cond_0

    iget v0, p2, Lfuj;->type:I

    if-nez v0, :cond_1

    .line 6
    :cond_0
    iget-object v0, p0, Lfru;->this$0:Lfrn;

    iget-object v1, p2, Lfuj;->streamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lfrn;->access$302(Lfrn;Ljava/lang/String;)Ljava/lang/String;

    .line 7
    const-string v0, "%s: Updating stream ID to %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfru;->this$0:Lfrn;

    invoke-virtual {v3}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lfru;->this$0:Lfrn;

    invoke-static {v2}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v0, p0, Lfru;->this$0:Lfrn;

    iget-object v1, p0, Lfru;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lfue;->getVideoSsrc(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lfrn;->setSsrc(I)V

    .line 13
    :cond_1
    :goto_0
    return-void

    .line 9
    :cond_2
    instance-of v0, p2, Lful;

    if-eqz v0, :cond_3

    .line 10
    iget-object v0, p0, Lfru;->this$0:Lfrn;

    iget-object v1, p0, Lfru;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lfrn;->setVideoMute(Z)V

    goto :goto_0

    .line 11
    :cond_3
    instance-of v0, p2, Lfud;

    if-eqz v0, :cond_1

    .line 12
    iget-object v0, p0, Lfru;->this$0:Lfrn;

    iget-object v1, p0, Lfru;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lfue;->isVideoCroppable(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lfrn;->setIsVideoCroppable(Z)V

    goto :goto_0
.end method
