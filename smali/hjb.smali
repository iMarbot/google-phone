.class public final Lhjb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lhjc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v0, p1, Lhjc;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lhjb;->a(Ljava/lang/String;Z)Ljava/lang/String;

    .line 3
    iget-object v0, p1, Lhjc;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lhjb;->a(Ljava/lang/String;Z)Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lhjc;->d:Ljava/lang/String;

    iput-object v0, p0, Lhjb;->a:Ljava/lang/String;

    .line 5
    invoke-virtual {p1}, Lhjc;->a()I

    move-result v0

    iput v0, p0, Lhjb;->b:I

    .line 6
    iget-object v0, p1, Lhjc;->f:Ljava/util/List;

    invoke-static {v0, v1}, Lhjb;->a(Ljava/util/List;Z)Ljava/util/List;

    .line 7
    invoke-virtual {p1}, Lhjc;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhjb;->c:Ljava/lang/String;

    .line 8
    return-void
.end method

.method static a(C)I
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    add-int/lit8 v0, p0, -0x30

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 71
    :cond_1
    const/16 v0, 0x41

    if-lt p0, v0, :cond_2

    const/16 v0, 0x46

    if-gt p0, v0, :cond_2

    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 72
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 9
    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    const/16 v0, 0x50

    .line 13
    :goto_0
    return v0

    .line 11
    :cond_0
    const-string v0, "https"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12
    const/16 v0, 0x1bb

    goto :goto_0

    .line 13
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IIZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x2b

    const/16 v6, 0x25

    const/4 v5, -0x1

    .line 28
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_a

    .line 29
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 30
    if-eq v1, v6, :cond_0

    if-ne v1, v7, :cond_9

    if-eqz p3, :cond_9

    .line 31
    :cond_0
    new-instance v1, Lhuh;

    invoke-direct {v1}, Lhuh;-><init>()V

    .line 32
    invoke-virtual {v1, p0, p1, v0}, Lhuh;->a(Ljava/lang/String;II)Lhuh;

    .line 34
    :goto_1
    if-ge v0, p2, :cond_8

    .line 35
    invoke-virtual {p0, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 36
    if-ne v2, v6, :cond_1

    add-int/lit8 v3, v0, 0x2

    if-ge v3, p2, :cond_1

    .line 37
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lhjb;->a(C)I

    move-result v3

    .line 38
    add-int/lit8 v4, v0, 0x2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lhjb;->a(C)I

    move-result v4

    .line 39
    if-eq v3, v5, :cond_2

    if-eq v4, v5, :cond_2

    .line 40
    shl-int/lit8 v3, v3, 0x4

    add-int/2addr v3, v4

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 41
    add-int/lit8 v0, v0, 0x2

    .line 65
    :goto_2
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    .line 43
    :cond_1
    if-ne v2, v7, :cond_2

    if-eqz p3, :cond_2

    .line 44
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 47
    :cond_2
    const/16 v3, 0x80

    if-ge v2, v3, :cond_3

    .line 48
    invoke-virtual {v1, v2}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 49
    :cond_3
    const/16 v3, 0x800

    if-ge v2, v3, :cond_4

    .line 50
    shr-int/lit8 v3, v2, 0x6

    or-int/lit16 v3, v3, 0xc0

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 51
    and-int/lit8 v3, v2, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 52
    :cond_4
    const/high16 v3, 0x10000

    if-ge v2, v3, :cond_6

    .line 53
    const v3, 0xd800

    if-lt v2, v3, :cond_5

    const v3, 0xdfff

    if-gt v2, v3, :cond_5

    .line 54
    const/16 v3, 0x3f

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 55
    :cond_5
    shr-int/lit8 v3, v2, 0xc

    or-int/lit16 v3, v3, 0xe0

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 56
    shr-int/lit8 v3, v2, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 57
    and-int/lit8 v3, v2, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 58
    :cond_6
    const v3, 0x10ffff

    if-gt v2, v3, :cond_7

    .line 59
    shr-int/lit8 v3, v2, 0x12

    or-int/lit16 v3, v3, 0xf0

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 60
    shr-int/lit8 v3, v2, 0xc

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 61
    shr-int/lit8 v3, v2, 0x6

    and-int/lit8 v3, v3, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    .line 62
    and-int/lit8 v3, v2, 0x3f

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {v1, v3}, Lhuh;->a(I)Lhuh;

    goto :goto_2

    .line 63
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected code point: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 64
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_8
    invoke-virtual {v1}, Lhuh;->g()Ljava/lang/String;

    move-result-object v0

    .line 68
    :goto_3
    return-object v0

    .line 67
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 68
    :cond_a
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p0, v0, v1, p1}, Lhjb;->a(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Z)Ljava/util/List;
    .locals 3

    .prologue
    .line 23
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 24
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 25
    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lhjb;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 27
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 15
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 16
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 18
    :cond_0
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 19
    instance-of v0, p1, Lhjb;

    if-eqz v0, :cond_0

    check-cast p1, Lhjb;

    iget-object v0, p1, Lhjb;->c:Ljava/lang/String;

    iget-object v1, p0, Lhjb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lhjb;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lhjb;->c:Ljava/lang/String;

    return-object v0
.end method
