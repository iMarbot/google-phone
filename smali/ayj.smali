.class public final Layj;
.super Llx;
.source "PG"


# static fields
.field public static final m:[Ljava/lang/String;

.field private static n:Landroid/net/Uri;

.field private static o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "image/jpeg"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "image/jpg"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image/png"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "image/webp"

    aput-object v2, v0, v1

    sput-object v0, Layj;->m:[Ljava/lang/String;

    .line 4
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Layj;->n:Landroid/net/Uri;

    .line 5
    const-string v0, "mime_type IN (\'image/jpeg\', \'image/jpg\', \'image/png\', \'image/webp\') AND media_type in (1)"

    .line 6
    sput-object v0, Layj;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1
    sget-object v2, Layj;->n:Landroid/net/Uri;

    sget-object v3, Layl;->a:[Ljava/lang/String;

    sget-object v4, Layj;->o:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "date_modified DESC"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Llx;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    return-void
.end method
