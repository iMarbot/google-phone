.class public final Lacz;
.super Lqa;
.source "PG"


# instance fields
.field private d:Lacy;


# direct methods
.method public constructor <init>(Lacy;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lqa;-><init>()V

    .line 2
    iput-object p1, p0, Lacz;->d:Lacy;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lsd;)V
    .locals 8

    .prologue
    const/16 v7, 0x13

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 4
    invoke-super {p0, p1, p2}, Lqa;->a(Landroid/view/View;Lsd;)V

    .line 5
    iget-object v0, p0, Lacz;->d:Lacy;

    .line 6
    iget-object v0, v0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->o()Z

    move-result v0

    .line 7
    if-nez v0, :cond_0

    iget-object v0, p0, Lacz;->d:Lacy;

    iget-object v0, v0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 9
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 10
    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lacz;->d:Lacy;

    iget-object v0, v0, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 12
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 15
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$r;

    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$r;->m()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView$f;->e:Laak;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$r;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Laak;->d(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v0, v2, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    .line 18
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v0

    .line 19
    :goto_0
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$f;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView$f;->a(Landroid/view/View;)I

    move-result v2

    .line 22
    :goto_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v3, v5, :cond_3

    .line 23
    new-instance v6, Lsf;

    move v3, v1

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->obtain(IIIIZZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v0

    invoke-direct {v6, v0}, Lsf;-><init>(Ljava/lang/Object;)V

    move-object v0, v6

    .line 29
    :goto_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v7, :cond_0

    .line 30
    iget-object v1, p2, Lsd;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    check-cast v0, Lsf;

    iget-object v0, v0, Lsf;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionItemInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;)V

    .line 31
    :cond_0
    return-void

    :cond_1
    move v0, v4

    .line 18
    goto :goto_0

    :cond_2
    move v2, v4

    .line 19
    goto :goto_1

    .line 24
    :cond_3
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v7, :cond_4

    .line 25
    new-instance v3, Lsf;

    invoke-static {v0, v1, v2, v1, v4}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->obtain(IIIIZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v0

    invoke-direct {v3, v0}, Lsf;-><init>(Ljava/lang/Object;)V

    move-object v0, v3

    goto :goto_2

    .line 26
    :cond_4
    new-instance v0, Lsf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lsf;-><init>(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-super {p0, p1, p2, p3}, Lqa;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    const/4 v0, 0x1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v1, p0, Lacz;->d:Lacy;

    .line 35
    iget-object v1, v1, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->o()Z

    move-result v1

    .line 36
    if-nez v1, :cond_0

    iget-object v1, p0, Lacz;->d:Lacy;

    iget-object v1, v1, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 38
    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 39
    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lacz;->d:Lacy;

    iget-object v1, v1, Lacy;->d:Landroid/support/v7/widget/RecyclerView;

    .line 41
    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->m:Landroid/support/v7/widget/RecyclerView$f;

    .line 44
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/RecyclerView$k;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$f;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v7/widget/RecyclerView$p;

    goto :goto_0
.end method
