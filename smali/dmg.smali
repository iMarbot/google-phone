.class Ldmg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

.field public c:Ljava/lang/Integer;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ldmg;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method a()Ldmf;
    .locals 4

    .prologue
    .line 8
    const-string v0, ""

    .line 9
    iget-object v1, p0, Ldmg;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 10
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " jibeServiceResult"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 11
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 12
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 13
    :cond_2
    new-instance v0, Ldkc;

    iget-object v1, p0, Ldmg;->a:Ljava/lang/Integer;

    iget-object v2, p0, Ldmg;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    iget-object v3, p0, Ldmg;->c:Ljava/lang/Integer;

    .line 14
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 15
    invoke-direct {v0, v1, v2, v3}, Ldkc;-><init>(Ljava/lang/Integer;Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;I)V

    .line 16
    return-object v0
.end method

.method a(I)Ldmg;
    .locals 1

    .prologue
    .line 6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldmg;->c:Ljava/lang/Integer;

    .line 7
    return-object p0
.end method

.method a(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;)Ldmg;
    .locals 0

    .prologue
    .line 4
    iput-object p1, p0, Ldmg;->b:Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 5
    return-object p0
.end method

.method a(Ljava/lang/Integer;)Ldmg;
    .locals 0

    .prologue
    .line 2
    iput-object p1, p0, Ldmg;->a:Ljava/lang/Integer;

    .line 3
    return-object p0
.end method
