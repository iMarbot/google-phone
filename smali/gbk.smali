.class public final Lgbk;
.super Lgbi;
.source "PG"


# static fields
.field public static final a:Lgbk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lgbk;

    invoke-direct {v0}, Lgbk;-><init>()V

    sput-object v0, Lgbk;->a:Lgbk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const-class v0, Lhtg;

    .line 2
    invoke-direct {p0, v0}, Lgbi;-><init>(Ljava/lang/Class;)V

    .line 3
    return-void
.end method


# virtual methods
.method final synthetic a(Lhfz;Lhfz;)Lhfz;
    .locals 1

    .prologue
    .line 8
    check-cast p1, Lhtg;

    check-cast p2, Lhtg;

    .line 9
    invoke-static {p1, p2}, Lfmk;->a(Lhtg;Lhtg;)Lhtg;

    move-result-object v0

    .line 10
    return-object v0
.end method

.method final synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lhfz;
    .locals 1

    .prologue
    .line 11
    check-cast p2, Landroid/os/health/TimerStat;

    .line 12
    invoke-static {p1, p2}, Lfmk;->b(Ljava/lang/String;Landroid/os/health/TimerStat;)Lhtg;

    move-result-object v0

    .line 13
    return-object v0
.end method

.method final synthetic a(Lhfz;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4
    check-cast p1, Lhtg;

    .line 5
    iget-object v0, p1, Lhtg;->c:Lhro;

    iget-object v0, v0, Lhro;->b:Ljava/lang/String;

    .line 6
    if-eqz v0, :cond_0

    .line 7
    :goto_0
    return-object v0

    .line 6
    :cond_0
    iget-object v0, p1, Lhtg;->c:Lhro;

    iget-object v0, v0, Lhro;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
