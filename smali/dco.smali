.class public final Ldco;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcub;


# static fields
.field private static a:[B

.field private static b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "Exif\u0000\u0000"

    const-string v1, "UTF-8"

    .line 132
    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Ldco;->a:[B

    .line 133
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldco;->b:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x4
        0x8
        0x1
        0x1
        0x2
        0x4
        0x8
        0x4
        0x8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ldcq;)I
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 85
    const/4 v1, 0x6

    .line 86
    invoke-virtual {p0, v1}, Ldcq;->b(I)S

    move-result v0

    .line 87
    const/16 v2, 0x4d4d

    if-eq v0, v2, :cond_3

    .line 88
    const/16 v2, 0x4949

    if-ne v0, v2, :cond_2

    .line 89
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 94
    :goto_0
    iget-object v2, p0, Ldcq;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 95
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ldcq;->a(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 96
    invoke-virtual {p0, v1}, Ldcq;->b(I)S

    move-result v2

    .line 97
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_c

    .line 99
    add-int/lit8 v3, v1, 0x2

    mul-int/lit8 v4, v0, 0xc

    add-int/2addr v3, v4

    .line 101
    invoke-virtual {p0, v3}, Ldcq;->b(I)S

    move-result v4

    .line 102
    const/16 v5, 0x112

    if-ne v4, v5, :cond_1

    .line 103
    add-int/lit8 v5, v3, 0x2

    invoke-virtual {p0, v5}, Ldcq;->b(I)S

    move-result v5

    .line 104
    if-lez v5, :cond_0

    const/16 v6, 0xc

    if-le v5, v6, :cond_4

    .line 105
    :cond_0
    const-string v3, "DfltImageHeaderParser"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 106
    const/16 v3, 0x25

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Got invalid format code = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :cond_2
    const-string v2, "DfltImageHeaderParser"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 91
    const/16 v2, 0x1b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown endianness = "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    :cond_3
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    goto :goto_0

    .line 107
    :cond_4
    add-int/lit8 v6, v3, 0x4

    invoke-virtual {p0, v6}, Ldcq;->a(I)I

    move-result v6

    .line 108
    if-gez v6, :cond_5

    .line 109
    const-string v3, "DfltImageHeaderParser"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    .line 111
    :cond_5
    const-string v7, "DfltImageHeaderParser"

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 112
    const/16 v7, 0x5e

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Got tagIndex="

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " tagType="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " formatCode="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " componentCount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    :cond_6
    sget-object v7, Ldco;->b:[I

    aget v7, v7, v5

    add-int/2addr v6, v7

    .line 114
    const/4 v7, 0x4

    if-le v6, v7, :cond_7

    .line 115
    const-string v3, "DfltImageHeaderParser"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    const/16 v3, 0x47

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Got byte count > 4, not orientation, continuing, formatCode="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 117
    :cond_7
    add-int/lit8 v3, v3, 0x8

    .line 118
    if-ltz v3, :cond_8

    .line 119
    iget-object v5, p0, Ldcq;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    .line 120
    if-le v3, v5, :cond_9

    .line 121
    :cond_8
    const-string v5, "DfltImageHeaderParser"

    invoke-static {v5, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 122
    const/16 v5, 0x36

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Illegal tagValueOffset="

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " tagType="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 123
    :cond_9
    if-ltz v6, :cond_a

    add-int v5, v3, v6

    .line 124
    iget-object v6, p0, Ldcq;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    .line 125
    if-le v5, v6, :cond_b

    .line 126
    :cond_a
    const-string v3, "DfltImageHeaderParser"

    invoke-static {v3, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 127
    const/16 v3, 0x3b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Illegal number of bytes for TI tag data tagType="

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 128
    :cond_b
    invoke-virtual {p0, v3}, Ldcq;->b(I)S

    move-result v0

    .line 130
    :goto_3
    return v0

    :cond_c
    const/4 v0, -0x1

    goto :goto_3
.end method

.method private final a(Ldcr;Lcxg;)I
    .locals 6

    .prologue
    const v4, 0xffd8

    const/4 v3, 0x3

    const/4 v1, -0x1

    .line 33
    invoke-interface {p1}, Ldcr;->a()I

    move-result v2

    .line 35
    and-int v0, v2, v4

    if-eq v0, v4, :cond_0

    const/16 v0, 0x4d4d

    if-eq v2, v0, :cond_0

    const/16 v0, 0x4949

    if-ne v2, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 36
    :goto_0
    if-nez v0, :cond_3

    .line 37
    const-string v0, "DfltImageHeaderParser"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    const/16 v0, 0x2f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Parser doesn\'t handle magic number: "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    :cond_1
    :goto_1
    return v1

    .line 35
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_3
    invoke-static {p1}, Ldco;->b(Ldcr;)I

    move-result v2

    .line 41
    if-eq v2, v1, :cond_1

    .line 43
    const-class v0, [B

    invoke-virtual {p2, v2, v0}, Lcxg;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 45
    :try_start_0
    invoke-interface {p1, v0, v2}, Ldcr;->a([BI)I

    move-result v3

    .line 46
    if-eq v3, v2, :cond_5

    .line 47
    const-string v4, "DfltImageHeaderParser"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 48
    const/16 v4, 0x51

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to read exif segment data, length: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", actually read: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :cond_4
    :goto_2
    const-class v2, [B

    invoke-virtual {p2, v0, v2}, Lcxg;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    goto :goto_1

    .line 50
    :cond_5
    :try_start_1
    invoke-static {v0, v2}, Ldco;->a([BI)Z

    move-result v3

    .line 51
    if-eqz v3, :cond_4

    .line 52
    new-instance v1, Ldcq;

    invoke-direct {v1, v0, v2}, Ldcq;-><init>([BI)V

    invoke-static {v1}, Ldco;->a(Ldcq;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_2

    .line 57
    :catchall_0
    move-exception v1

    const-class v2, [B

    invoke-virtual {p2, v0, v2}, Lcxg;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    throw v1
.end method

.method private static a(Ldcr;)Lcuc;
    .locals 8

    .prologue
    const-wide/16 v6, 0x4

    const v4, 0xffff

    const/high16 v3, -0x10000

    .line 7
    invoke-interface {p0}, Ldcr;->a()I

    move-result v0

    .line 8
    const v1, 0xffd8

    if-ne v0, v1, :cond_0

    .line 9
    sget-object v0, Lcuc;->b:Lcuc;

    .line 32
    :goto_0
    return-object v0

    .line 10
    :cond_0
    shl-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v3

    invoke-interface {p0}, Ldcr;->a()I

    move-result v1

    and-int/2addr v1, v4

    or-int/2addr v0, v1

    .line 11
    const v1, -0x76afb1b9

    if-ne v0, v1, :cond_2

    .line 12
    const-wide/16 v0, 0x15

    invoke-interface {p0, v0, v1}, Ldcr;->a(J)J

    .line 13
    invoke-interface {p0}, Ldcr;->c()I

    move-result v0

    .line 14
    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    sget-object v0, Lcuc;->c:Lcuc;

    goto :goto_0

    :cond_1
    sget-object v0, Lcuc;->d:Lcuc;

    goto :goto_0

    .line 15
    :cond_2
    shr-int/lit8 v1, v0, 0x8

    const v2, 0x474946

    if-ne v1, v2, :cond_3

    .line 16
    sget-object v0, Lcuc;->a:Lcuc;

    goto :goto_0

    .line 17
    :cond_3
    const v1, 0x52494646

    if-eq v0, v1, :cond_4

    .line 18
    sget-object v0, Lcuc;->g:Lcuc;

    goto :goto_0

    .line 19
    :cond_4
    invoke-interface {p0, v6, v7}, Ldcr;->a(J)J

    .line 20
    invoke-interface {p0}, Ldcr;->a()I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v3

    invoke-interface {p0}, Ldcr;->a()I

    move-result v1

    and-int/2addr v1, v4

    or-int/2addr v0, v1

    .line 21
    const v1, 0x57454250

    if-eq v0, v1, :cond_5

    .line 22
    sget-object v0, Lcuc;->g:Lcuc;

    goto :goto_0

    .line 23
    :cond_5
    invoke-interface {p0}, Ldcr;->a()I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    and-int/2addr v0, v3

    invoke-interface {p0}, Ldcr;->a()I

    move-result v1

    and-int/2addr v1, v4

    or-int/2addr v0, v1

    .line 24
    and-int/lit16 v1, v0, -0x100

    const v2, 0x56503800

    if-eq v1, v2, :cond_6

    .line 25
    sget-object v0, Lcuc;->g:Lcuc;

    goto :goto_0

    .line 26
    :cond_6
    and-int/lit16 v1, v0, 0xff

    const/16 v2, 0x58

    if-ne v1, v2, :cond_8

    .line 27
    invoke-interface {p0, v6, v7}, Ldcr;->a(J)J

    .line 28
    invoke-interface {p0}, Ldcr;->c()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    sget-object v0, Lcuc;->e:Lcuc;

    goto :goto_0

    :cond_7
    sget-object v0, Lcuc;->f:Lcuc;

    goto :goto_0

    .line 29
    :cond_8
    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0x4c

    if-ne v0, v1, :cond_a

    .line 30
    invoke-interface {p0, v6, v7}, Ldcr;->a(J)J

    .line 31
    invoke-interface {p0}, Ldcr;->c()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_9

    sget-object v0, Lcuc;->e:Lcuc;

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcuc;->f:Lcuc;

    goto/16 :goto_0

    .line 32
    :cond_a
    sget-object v0, Lcuc;->f:Lcuc;

    goto/16 :goto_0
.end method

.method private static a([BI)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 58
    if-eqz p0, :cond_0

    sget-object v0, Ldco;->a:[B

    array-length v0, v0

    if-le p1, v0, :cond_0

    const/4 v2, 0x1

    .line 59
    :goto_0
    if-eqz v2, :cond_2

    move v0, v1

    .line 60
    :goto_1
    sget-object v3, Ldco;->a:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 61
    aget-byte v3, p0, v0

    sget-object v4, Ldco;->a:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_1

    .line 65
    :goto_2
    return v1

    :cond_0
    move v2, v1

    .line 58
    goto :goto_0

    .line 64
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private static b(Ldcr;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v0, -0x1

    .line 66
    :cond_0
    invoke-interface {p0}, Ldcr;->b()S

    move-result v1

    .line 67
    const/16 v2, 0xff

    if-eq v1, v2, :cond_2

    .line 68
    const-string v2, "DfltImageHeaderParser"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    const/16 v2, 0x18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown segmentId="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    :cond_1
    :goto_0
    return v0

    .line 71
    :cond_2
    invoke-interface {p0}, Ldcr;->b()S

    move-result v2

    .line 72
    const/16 v1, 0xda

    if-eq v2, v1, :cond_1

    .line 74
    const/16 v1, 0xd9

    if-eq v2, v1, :cond_1

    .line 76
    invoke-interface {p0}, Ldcr;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    .line 77
    const/16 v3, 0xe1

    if-eq v2, v3, :cond_3

    .line 78
    int-to-long v4, v1

    invoke-interface {p0, v4, v5}, Ldcr;->a(J)J

    move-result-wide v4

    .line 79
    int-to-long v6, v1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 80
    const-string v3, "DfltImageHeaderParser"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    const/16 v3, 0x71

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to skip enough data, type: "

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", wanted to skip: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", but actually skipped: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 84
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;Lcxg;)I
    .locals 2

    .prologue
    .line 4
    new-instance v1, Ldcs;

    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {v1, v0}, Ldcs;-><init>(Ljava/io/InputStream;)V

    .line 5
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxg;

    .line 6
    invoke-direct {p0, v1, v0}, Ldco;->a(Ldcr;Lcxg;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/io/InputStream;)Lcuc;
    .locals 2

    .prologue
    .line 2
    new-instance v1, Ldcs;

    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {v1, v0}, Ldcs;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Ldco;->a(Ldcr;)Lcuc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/nio/ByteBuffer;)Lcuc;
    .locals 2

    .prologue
    .line 3
    new-instance v1, Ldcp;

    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-direct {v1, v0}, Ldcp;-><init>(Ljava/nio/ByteBuffer;)V

    invoke-static {v1}, Ldco;->a(Ldcr;)Lcuc;

    move-result-object v0

    return-object v0
.end method
