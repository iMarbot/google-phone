.class public final Lhxj;
.super Lhxi;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field private c:Lhxi;

.field private d:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1}, Lhxi;-><init>(Ljava/io/InputStream;)V

    .line 2
    iput-boolean v0, p0, Lhxj;->a:Z

    .line 3
    iput-boolean v0, p0, Lhxj;->b:Z

    .line 4
    instance-of v0, p1, Lhxi;

    if-eqz v0, :cond_0

    .line 5
    check-cast p1, Lhxi;

    iput-object p1, p0, Lhxj;->c:Lhxi;

    .line 7
    :goto_0
    iput p2, p0, Lhxj;->d:I

    .line 8
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhxj;->c:Lhxi;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhyh;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 17
    iget-object v0, p0, Lhxj;->c:Lhxi;

    if-eqz v0, :cond_2

    .line 18
    iget-object v0, p0, Lhxj;->c:Lhxi;

    invoke-virtual {v0, p1}, Lhxi;->a(Lhyh;)I

    move-result v0

    .line 33
    :cond_0
    :goto_0
    if-ne v0, v3, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, p0, Lhxj;->b:Z

    .line 34
    iput-boolean v2, p0, Lhxj;->a:Z

    .line 35
    return v0

    :cond_2
    move v0, v1

    .line 21
    :cond_3
    iget-object v4, p0, Lhxj;->in:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v4

    if-eq v4, v3, :cond_5

    .line 22
    invoke-virtual {p1, v4}, Lhyh;->a(I)V

    .line 23
    add-int/lit8 v0, v0, 0x1

    .line 24
    iget v5, p0, Lhxj;->d:I

    if-lez v5, :cond_4

    .line 25
    iget v5, p1, Lhyh;->b:I

    .line 26
    iget v6, p0, Lhxj;->d:I

    if-lt v5, v6, :cond_4

    .line 27
    new-instance v0, Lhxm;

    const-string v1, "Maximum line length limit exceeded"

    invoke-direct {v0, v1}, Lhxm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_4
    const/16 v5, 0xa

    if-ne v4, v5, :cond_3

    .line 29
    :cond_5
    if-nez v0, :cond_0

    if-ne v4, v3, :cond_0

    move v0, v3

    .line 30
    goto :goto_0
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 9
    iget-object v0, p0, Lhxj;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 10
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lhxj;->b:Z

    .line 11
    iput-boolean v1, p0, Lhxj;->a:Z

    .line 12
    return v2

    .line 10
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 13
    iget-object v0, p0, Lhxj;->in:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 14
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lhxj;->b:Z

    .line 15
    iput-boolean v1, p0, Lhxj;->a:Z

    .line 16
    return v2

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 37
    cmp-long v0, p1, v2

    if-gtz v0, :cond_1

    move-wide v0, v2

    .line 48
    :cond_0
    return-wide v0

    .line 39
    :cond_1
    const-wide/16 v0, 0x2000

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    const/16 v0, 0x2000

    .line 40
    :goto_0
    new-array v4, v0, [B

    move-wide v0, v2

    .line 42
    :goto_1
    cmp-long v5, p1, v2

    if-lez v5, :cond_0

    .line 43
    invoke-virtual {p0, v4}, Lhxj;->read([B)I

    move-result v5

    .line 44
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 45
    int-to-long v6, v5

    add-long/2addr v0, v6

    .line 46
    int-to-long v6, v5

    sub-long/2addr p1, v6

    .line 47
    goto :goto_1

    .line 39
    :cond_2
    long-to-int v0, p1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[LineReaderInputStreamAdaptor: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhxj;->c:Lhxi;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
