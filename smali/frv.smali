.class final Lfrv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public lastVideoViewRequest:Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

.field public final synthetic this$0:Lfrn;


# direct methods
.method private constructor <init>(Lfrn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrv;->this$0:Lfrn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfrn;Lfmt;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lfrv;-><init>(Lfrn;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v0}, Lfrn;->access$400(Lfrn;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    iget-object v0, v0, Lfrn;->participant:Lfrh;

    invoke-virtual {v0}, Lfrh;->getEndpoint()Lfue;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$500(Lfrn;)I

    move-result v1

    if-nez v1, :cond_0

    .line 5
    const-string v0, "%s: No ssrc for renderer; not sending ViewRequest"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lfrv;->this$0:Lfrn;

    invoke-virtual {v2}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    :goto_0
    return-void

    .line 7
    :cond_0
    iget-object v1, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lfrv;->this$0:Lfrn;

    iget-object v1, v1, Lfrn;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    if-nez v1, :cond_2

    .line 8
    :cond_1
    const-string v1, "%s: No view request: muted=%b, surface=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lfrv;->this$0:Lfrn;

    .line 9
    invoke-virtual {v3}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lfrv;->this$0:Lfrn;

    .line 10
    invoke-static {v3}, Lfrn;->access$300(Lfrn;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v2, v7

    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    iget-object v0, v0, Lfrn;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    aput-object v0, v2, v8

    .line 11
    invoke-static {v1, v2}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v6

    move v4, v6

    move v3, v6

    .line 31
    :goto_1
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    iget-object v1, p0, Lfrv;->this$0:Lfrn;

    .line 32
    invoke-static {v1}, Lfrn;->access$700(Lfrn;)Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;

    move-result-object v1

    iget-object v2, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v2}, Lfrn;->access$500(Lfrn;)I

    move-result v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;-><init>(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;IIII)V

    .line 33
    iget-object v1, p0, Lfrv;->lastVideoViewRequest:Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 34
    const-string v1, "%s: Not sending duplicate request %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lfrv;->this$0:Lfrn;

    invoke-virtual {v3}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 15
    :cond_2
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v0}, Lfrn;->access$600(Lfrn;)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v7

    .line 18
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 23
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    invoke-virtual {v0}, Lfrn;->getCurrentCodec()I

    move-result v0

    invoke-static {v0}, Lfor;->getIncomingSecondaryVideoSpec(I)Lfwp;

    move-result-object v0

    .line 24
    :goto_3
    if-nez v0, :cond_4

    move v4, v6

    .line 28
    :goto_4
    if-nez v0, :cond_5

    move v5, v6

    move v3, v4

    goto :goto_1

    .line 17
    :cond_3
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v0}, Lfrn;->access$600(Lfrn;)I

    move-result v0

    goto :goto_2

    .line 19
    :pswitch_0
    iget-object v0, p0, Lfrv;->this$0:Lfrn;

    invoke-virtual {v0}, Lfrn;->getCurrentCodec()I

    move-result v0

    invoke-static {v0}, Lfor;->getIncomingPrimaryVideoSpec(I)Lfwp;

    move-result-object v0

    goto :goto_3

    .line 21
    :pswitch_1
    const/4 v0, 0x0

    .line 22
    goto :goto_3

    .line 25
    :cond_4
    iget-object v1, v0, Lfwp;->a:Lfwo;

    .line 26
    iget v4, v1, Lfwo;->a:I

    goto :goto_4

    .line 29
    :cond_5
    iget v5, v0, Lfwp;->b:I

    move v3, v4

    goto :goto_1

    .line 36
    :cond_6
    iput-object v0, p0, Lfrv;->lastVideoViewRequest:Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    .line 37
    const-string v1, "%s: Sending view request %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lfrv;->this$0:Lfrn;

    invoke-virtual {v3}, Lfrn;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iget-object v1, p0, Lfrv;->this$0:Lfrn;

    invoke-static {v1}, Lfrn;->access$800(Lfrn;)Lfnv;

    move-result-object v1

    new-array v2, v7, [Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lfnv;->requestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V

    goto/16 :goto_0

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
