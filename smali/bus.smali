.class public final Lbus;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvp$e;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private b:Z


# direct methods
.method public constructor <init>(Lbuq;Z)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbus;->a:Ljava/lang/ref/WeakReference;

    .line 3
    iput-boolean p2, p0, Lbus;->b:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lbvp$d;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 5
    iget-object v0, p0, Lbus;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuq;

    .line 6
    if-eqz v0, :cond_3

    .line 7
    iget-boolean v3, p0, Lbus;->b:Z

    .line 9
    if-eqz v3, :cond_0

    iget-object v1, v0, Lbuq;->c:Lcdc;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbuq;->c:Lcdc;

    .line 11
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 12
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-nez v3, :cond_4

    iget-object v1, v0, Lbuq;->d:Lcdc;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lbuq;->d:Lcdc;

    .line 14
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 15
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    .line 16
    :goto_0
    if-eqz v1, :cond_5

    .line 17
    invoke-virtual {v0, p2, v3}, Lbuq;->a(Lbvp$d;Z)V

    .line 19
    :goto_1
    sget-object v1, Lcct;->a:Lcct;

    .line 20
    invoke-virtual {v1, p1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v1

    .line 21
    if-eqz v1, :cond_2

    .line 23
    iget-object v1, v1, Lcdc;->g:Lcdf;

    .line 24
    iget-object v2, p2, Lbvp$d;->m:Lbkm$a;

    iput-object v2, v1, Lcdf;->c:Lbkm$a;

    .line 25
    :cond_2
    iget-object v1, p2, Lbvp$d;->k:Landroid/net/Uri;

    if-eqz v1, :cond_3

    .line 26
    iget-object v0, v0, Lbuq;->a:Landroid/content/Context;

    iget-object v1, p2, Lbvp$d;->k:Landroid/net/Uri;

    invoke-static {v0, v1}, Lbve;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 27
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 15
    goto :goto_0

    .line 18
    :cond_5
    const-string v3, "CallCardPresenter.onContactInfoComplete"

    const-string v4, "dropping stale contact lookup info for "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;Lbvp$d;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lbus;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuq;

    .line 29
    if-eqz v0, :cond_0

    .line 33
    iget-object v1, v0, Lbuq;->e:Lcgm;

    .line 34
    if-eqz v1, :cond_0

    .line 35
    iget-object v1, p2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, v0, Lbuq;->c:Lcdc;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lbuq;->c:Lcdc;

    .line 37
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 38
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lbuq;->a(Lbvp$d;Z)V

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    iget-object v1, v0, Lbuq;->d:Lcdc;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lbuq;->d:Lcdc;

    .line 41
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 42
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lbuq;->a(Lbvp$d;Z)V

    goto :goto_0
.end method
