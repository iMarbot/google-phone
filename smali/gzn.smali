.class public final Lgzn;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final d:Lgzn;

.field private static volatile e:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lgzn;

    invoke-direct {v0}, Lgzn;-><init>()V

    .line 86
    sput-object v0, Lgzn;->d:Lgzn;

    invoke-virtual {v0}, Lgzn;->makeImmutable()V

    .line 87
    const-class v0, Lgzn;

    sget-object v1, Lgzn;->d:Lgzn;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 88
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lgzn;->c:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 81
    sget-object v2, Lgzt;->i:Lhby;

    .line 82
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 83
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0000\u0001\u000c\u0000\u0002\u0008\u0001"

    .line 84
    sget-object v2, Lgzn;->d:Lgzn;

    invoke-static {v2, v1, v0}, Lgzn;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 33
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 34
    :pswitch_0
    new-instance v0, Lgzn;

    invoke-direct {v0}, Lgzn;-><init>()V

    .line 78
    :goto_0
    :pswitch_1
    return-object v0

    .line 35
    :pswitch_2
    sget-object v0, Lgzn;->d:Lgzn;

    goto :goto_0

    .line 37
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[B)V

    move-object v0, v1

    goto :goto_0

    .line 38
    :pswitch_4
    check-cast p2, Lhaq;

    .line 39
    check-cast p3, Lhbg;

    .line 40
    if-nez p3, :cond_0

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42
    :cond_0
    :try_start_0
    sget-boolean v0, Lgzn;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p0, p2, p3}, Lgzn;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 44
    sget-object v0, Lgzn;->d:Lgzn;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 46
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 47
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 48
    sparse-switch v2, :sswitch_data_0

    .line 51
    invoke-virtual {p0, v2, p2}, Lgzn;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 52
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 50
    goto :goto_1

    .line 53
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 54
    invoke-static {v2}, Lgzt;->a(I)Lgzt;

    move-result-object v3

    .line 55
    if-nez v3, :cond_3

    .line 56
    const/4 v3, 0x1

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 65
    :catch_0
    move-exception v0

    .line 66
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    throw v0

    .line 57
    :cond_3
    :try_start_2
    iget v3, p0, Lgzn;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lgzn;->a:I

    .line 58
    iput v2, p0, Lgzn;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 67
    :catch_1
    move-exception v0

    .line 68
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 69
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_2
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 61
    iget v3, p0, Lgzn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lgzn;->a:I

    .line 62
    iput-object v2, p0, Lgzn;->c:Ljava/lang/String;
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 71
    :cond_4
    :pswitch_5
    sget-object v0, Lgzn;->d:Lgzn;

    goto :goto_0

    .line 72
    :pswitch_6
    sget-object v0, Lgzn;->e:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lgzn;

    monitor-enter v1

    .line 73
    :try_start_5
    sget-object v0, Lgzn;->e:Lhdm;

    if-nez v0, :cond_5

    .line 74
    new-instance v0, Lhaa;

    sget-object v2, Lgzn;->d:Lgzn;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgzn;->e:Lhdm;

    .line 75
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 76
    :cond_6
    sget-object v0, Lgzn;->e:Lhdm;

    goto/16 :goto_0

    .line 75
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 77
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 16
    iget v0, p0, Lgzn;->memoizedSerializedSize:I

    .line 17
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32
    :goto_0
    return v0

    .line 18
    :cond_0
    sget-boolean v0, Lgzn;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 19
    invoke-virtual {p0}, Lgzn;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgzn;->memoizedSerializedSize:I

    .line 20
    iget v0, p0, Lgzn;->memoizedSerializedSize:I

    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    iget v1, p0, Lgzn;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 23
    iget v0, p0, Lgzn;->b:I

    .line 24
    invoke-static {v2, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25
    :cond_2
    iget v1, p0, Lgzn;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 28
    iget-object v1, p0, Lgzn;->c:Ljava/lang/String;

    .line 29
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_3
    iget-object v1, p0, Lgzn;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    iput v0, p0, Lgzn;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4
    sget-boolean v0, Lgzn;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lgzn;->writeToInternal(Lhaw;)V

    .line 15
    :goto_0
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lgzn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 8
    iget v0, p0, Lgzn;->b:I

    .line 9
    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 10
    :cond_1
    iget v0, p0, Lgzn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 12
    iget-object v0, p0, Lgzn;->c:Ljava/lang/String;

    .line 13
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 14
    :cond_2
    iget-object v0, p0, Lgzn;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
