.class public final Ldoq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Z

.field private static b:Z

.field private static c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    sput-boolean v0, Ldoq;->b:Z

    .line 20
    sput-boolean v0, Ldoq;->a:Z

    .line 21
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Ldoq;->c:Ljava/util/Map;

    return-void
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x1

    sput-boolean v0, Ldoq;->a:Z

    .line 2
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 9
    :cond_0
    :goto_0
    return-void

    .line 5
    :cond_1
    invoke-static {}, Ldoq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    sget-object v0, Ldoq;->c:Ljava/util/Map;

    .line 7
    new-instance v1, Lgaz;

    invoke-direct {v1}, Lgaz;-><init>()V

    .line 8
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 10
    invoke-static {}, Ldoq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldoq;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    sget-object v1, Lfyy;->a:Lfyy;

    .line 12
    sget-object v0, Ldoq;->c:Ljava/util/Map;

    .line 13
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgaz;

    invoke-static {p0}, Lfyt;->a(Ljava/lang/String;)Lfyt;

    move-result-object v2

    .line 14
    iget-object v1, v1, Lfyy;->b:Lfyz;

    invoke-static {v2}, Lfyy;->a(Lfyt;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v0, v2, v3}, Lfyz;->a(Lgaz;Ljava/lang/String;Z)V

    .line 15
    sget-object v0, Ldoq;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 18
    sget-object v0, Ldny;->l:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldny;->m:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static c()Z
    .locals 1

    .prologue
    .line 17
    sget-object v0, Ldny;->l:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldny;->n:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
