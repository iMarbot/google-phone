.class final Lfoa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;
.implements Ljava/lang/Runnable;


# static fields
.field public static final LEAVE_DEADLINE_MINUTES:I = 0x1


# instance fields
.field public final hangoutId:Ljava/lang/String;

.field public final mesiClient:Lfnj;

.field public final participantCollection:Lfnf;

.field public requestsPending:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lfnf;Ljava/lang/String;Lfnj;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfoa;->participantCollection:Lfnf;

    .line 3
    iput-object p2, p0, Lfoa;->hangoutId:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lfoa;->mesiClient:Lfnj;

    .line 5
    return-void
.end method


# virtual methods
.method public final onError(Lgng$e;)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "Leave RPC failed!"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lfoa;->requestsPending:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 27
    return-void
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lgng$e;

    invoke-virtual {p0, p1}, Lfoa;->onError(Lgng$e;)V

    return-void
.end method

.method public final onSuccess(Lgng$e;)V
    .locals 1

    .prologue
    .line 28
    const-string v0, "Leave RPC succeeded!"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lfoa;->requestsPending:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 30
    return-void
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lgng$e;

    invoke-virtual {p0, p1}, Lfoa;->onSuccess(Lgng$e;)V

    return-void
.end method

.method public final run()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 6
    const-string v0, "LeaveHandler starting"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 7
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfoa;->requestsPending:Ljava/util/concurrent/CountDownLatch;

    .line 8
    new-instance v0, Lgnr;

    invoke-direct {v0}, Lgnr;-><init>()V

    .line 9
    new-array v1, v3, [Lgny;

    new-instance v2, Lgny;

    invoke-direct {v2}, Lgny;-><init>()V

    aput-object v2, v1, v4

    iput-object v1, v0, Lgnr;->resource:[Lgny;

    .line 10
    iget-object v1, v0, Lgnr;->resource:[Lgny;

    aget-object v1, v1, v4

    iget-object v2, p0, Lfoa;->hangoutId:Ljava/lang/String;

    iput-object v2, v1, Lgny;->hangoutId:Ljava/lang/String;

    .line 11
    const-string v1, "Sending leave RPC: %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lfoa;->hangoutId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v1, p0, Lfoa;->participantCollection:Lfnf;

    invoke-interface {v1, v0, p0}, Lfnf;->remove(Lhfz;Lfnn;)V

    .line 13
    :try_start_0
    iget-object v0, p0, Lfoa;->requestsPending:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    const-string v0, "LeaveRPC not complete yet! Not waiting any further"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    :cond_0
    iget-object v0, p0, Lfoa;->mesiClient:Lfnj;

    invoke-interface {v0}, Lfnj;->release()V

    .line 23
    :goto_0
    const-string v0, "LeaveHandler terminating"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 24
    return-void

    .line 19
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "LeaveHandler was interrupted!"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    iget-object v0, p0, Lfoa;->mesiClient:Lfnj;

    invoke-interface {v0}, Lfnj;->release()V

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfoa;->mesiClient:Lfnj;

    invoke-interface {v1}, Lfnj;->release()V

    throw v0
.end method
