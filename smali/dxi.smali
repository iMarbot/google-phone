.class public final Ldxi;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/app/PendingIntent;

.field private d:I

.field private e:Landroid/os/Bundle;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ldxk;

    invoke-direct {v0}, Ldxk;-><init>()V

    sput-object v0, Ldxi;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILandroid/app/PendingIntent;ILandroid/os/Bundle;[B)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Ldxi;->a:I

    iput p2, p0, Ldxi;->b:I

    iput p4, p0, Ldxi;->d:I

    iput-object p5, p0, Ldxi;->e:Landroid/os/Bundle;

    iput-object p6, p0, Ldxi;->f:[B

    iput-object p3, p0, Ldxi;->c:Landroid/app/PendingIntent;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldxi;->b:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Ldxi;->c:Landroid/app/PendingIntent;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    iget v2, p0, Ldxi;->d:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x4

    iget-object v2, p0, Ldxi;->e:Landroid/os/Bundle;

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Ldxi;->f:[B

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;I[BZ)V

    const/16 v1, 0x3e8

    iget v2, p0, Ldxi;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
