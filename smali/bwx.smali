.class final Lbwx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvp$e;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lbww;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lbwx;->a:Ljava/lang/ref/WeakReference;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lbvp$d;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 4
    iget-object v0, p0, Lbwx;->a:Ljava/lang/ref/WeakReference;

    .line 5
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbww;

    .line 6
    if-nez v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 8
    :cond_0
    iget-object v1, p2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 9
    iget-object v1, p2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 11
    iget-object v0, v0, Lbww;->b:Lckb;

    .line 12
    iget-object v2, v0, Lckb;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 13
    iget-object v3, v0, Lckb;->a:Landroid/content/Context;

    .line 14
    invoke-static {v3, v1, v2, v2}, Lbib;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lckb;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 17
    :cond_1
    sget-object v1, Lcct;->a:Lcct;

    .line 18
    invoke-virtual {v1, p1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v2

    .line 22
    iget-object v1, p2, Lbvp$d;->a:Ljava/lang/String;

    iget-object v3, p2, Lbvp$d;->b:Ljava/lang/String;

    iget-object v4, v0, Lbww;->a:Landroid/content/Context;

    .line 23
    invoke-static {v4}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v4

    .line 24
    invoke-static {v1, v3, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 26
    iget-object v1, p2, Lbvp$d;->c:Ljava/lang/String;

    .line 27
    :cond_2
    new-instance v3, Lbkg;

    iget-object v4, v0, Lbww;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 29
    invoke-virtual {v2, v1}, Lcdc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p2, Lbvp$d;->l:Ljava/lang/String;

    .line 31
    iget-boolean v5, v2, Lcdc;->G:Z

    .line 34
    iget-boolean v6, v2, Lcdc;->s:Z

    .line 35
    iget-boolean v7, p2, Lbvp$d;->s:Z

    .line 36
    invoke-virtual {v2}, Lcdc;->k()I

    move-result v8

    .line 38
    invoke-virtual {v2, v9}, Lcdc;->d(I)Z

    move-result v2

    .line 39
    invoke-static {v5, v6, v7, v8, v2}, Lbkg;->a(ZZZIZ)I

    move-result v2

    .line 40
    invoke-virtual {v3, v1, v4, v9, v2}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    .line 44
    iget-object v0, v0, Lbww;->b:Lckb;

    invoke-virtual {v0, v3}, Lckb;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lbvp$d;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 46
    iget-object v0, p0, Lbwx;->a:Ljava/lang/ref/WeakReference;

    .line 47
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbww;

    .line 48
    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v1, p2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 51
    iget-object v1, p2, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    .line 53
    iget-object v0, v0, Lbww;->b:Lckb;

    .line 54
    iget-object v2, v0, Lckb;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 55
    iget-object v3, v0, Lckb;->a:Landroid/content/Context;

    .line 56
    invoke-static {v3, v1, v2, v2}, Lbib;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lckb;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 59
    :cond_1
    sget-object v1, Lcct;->a:Lcct;

    .line 60
    invoke-virtual {v1, p1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v2

    .line 64
    iget-object v1, p2, Lbvp$d;->a:Ljava/lang/String;

    iget-object v3, p2, Lbvp$d;->b:Ljava/lang/String;

    iget-object v4, v0, Lbww;->a:Landroid/content/Context;

    .line 65
    invoke-static {v4}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v4

    .line 66
    invoke-static {v1, v3, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 68
    iget-object v1, p2, Lbvp$d;->c:Ljava/lang/String;

    .line 69
    :cond_2
    new-instance v3, Lbkg;

    iget-object v4, v0, Lbww;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 71
    invoke-virtual {v2, v1}, Lcdc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p2, Lbvp$d;->l:Ljava/lang/String;

    .line 73
    iget-boolean v5, v2, Lcdc;->G:Z

    .line 76
    iget-boolean v6, v2, Lcdc;->s:Z

    .line 77
    iget-boolean v7, p2, Lbvp$d;->s:Z

    .line 78
    invoke-virtual {v2}, Lcdc;->k()I

    move-result v8

    .line 80
    invoke-virtual {v2, v9}, Lcdc;->d(I)Z

    move-result v2

    .line 81
    invoke-static {v5, v6, v7, v8, v2}, Lbkg;->a(ZZZIZ)I

    move-result v2

    .line 82
    invoke-virtual {v3, v1, v4, v9, v2}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    .line 86
    iget-object v0, v0, Lbww;->b:Lckb;

    invoke-virtual {v0, v3}, Lckb;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
