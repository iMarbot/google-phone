.class public final Lgok;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgok;


# instance fields
.field public bitRate:Ljava/lang/Integer;

.field public channelCount:Ljava/lang/Integer;

.field public mediaType:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public param:[Lgol;

.field public payloadId:Ljava/lang/Integer;

.field public preference:Ljava/lang/Integer;

.field public sampleRate:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgok;->clear()Lgok;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgok;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgok;->_emptyArray:[Lgok;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgok;->_emptyArray:[Lgok;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgok;

    sput-object v0, Lgok;->_emptyArray:[Lgok;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgok;->_emptyArray:[Lgok;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgok;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lgok;

    invoke-direct {v0}, Lgok;-><init>()V

    invoke-virtual {v0, p0}, Lgok;->mergeFrom(Lhfp;)Lgok;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgok;
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lgok;

    invoke-direct {v0}, Lgok;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgok;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgok;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgok;->payloadId:Ljava/lang/Integer;

    .line 11
    iput-object v1, p0, Lgok;->name:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgok;->mediaType:Ljava/lang/Integer;

    .line 13
    iput-object v1, p0, Lgok;->preference:Ljava/lang/Integer;

    .line 14
    iput-object v1, p0, Lgok;->bitRate:Ljava/lang/Integer;

    .line 15
    iput-object v1, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    .line 16
    iput-object v1, p0, Lgok;->channelCount:Ljava/lang/Integer;

    .line 17
    invoke-static {}, Lgol;->emptyArray()[Lgol;

    move-result-object v0

    iput-object v0, p0, Lgok;->param:[Lgol;

    .line 18
    iput-object v1, p0, Lgok;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgok;->cachedSize:I

    .line 20
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 43
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 44
    iget-object v1, p0, Lgok;->payloadId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x1

    iget-object v2, p0, Lgok;->payloadId:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_0
    iget-object v1, p0, Lgok;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lgok;->name:Ljava/lang/String;

    .line 49
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_1
    iget-object v1, p0, Lgok;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 51
    const/4 v1, 0x3

    iget-object v2, p0, Lgok;->mediaType:Ljava/lang/Integer;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_2
    iget-object v1, p0, Lgok;->preference:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 54
    const/4 v1, 0x4

    iget-object v2, p0, Lgok;->preference:Ljava/lang/Integer;

    .line 55
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_3
    iget-object v1, p0, Lgok;->bitRate:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 57
    const/4 v1, 0x5

    iget-object v2, p0, Lgok;->bitRate:Ljava/lang/Integer;

    .line 58
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_4
    iget-object v1, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 60
    const/4 v1, 0x6

    iget-object v2, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    .line 61
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_5
    iget-object v1, p0, Lgok;->channelCount:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 63
    const/4 v1, 0x7

    iget-object v2, p0, Lgok;->channelCount:Ljava/lang/Integer;

    .line 64
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_6
    iget-object v1, p0, Lgok;->param:[Lgol;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lgok;->param:[Lgol;

    array-length v1, v1

    if-lez v1, :cond_9

    .line 66
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgok;->param:[Lgol;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 67
    iget-object v2, p0, Lgok;->param:[Lgol;

    aget-object v2, v2, v0

    .line 68
    if-eqz v2, :cond_7

    .line 69
    const/16 v3, 0x8

    .line 70
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 71
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    move v0, v1

    .line 72
    :cond_9
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgok;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 74
    sparse-switch v0, :sswitch_data_0

    .line 76
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :sswitch_0
    return-object p0

    .line 79
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 80
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgok;->payloadId:Ljava/lang/Integer;

    goto :goto_0

    .line 82
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgok;->name:Ljava/lang/String;

    goto :goto_0

    .line 84
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 86
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 87
    invoke-static {v3}, Lgoi;->checkMediaTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgok;->mediaType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 91
    invoke-virtual {p0, p1, v0}, Lgok;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 94
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 95
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgok;->preference:Ljava/lang/Integer;

    goto :goto_0

    .line 98
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 99
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgok;->bitRate:Ljava/lang/Integer;

    goto :goto_0

    .line 102
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 103
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    goto :goto_0

    .line 106
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 107
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgok;->channelCount:Ljava/lang/Integer;

    goto :goto_0

    .line 109
    :sswitch_8
    const/16 v0, 0x42

    .line 110
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 111
    iget-object v0, p0, Lgok;->param:[Lgol;

    if-nez v0, :cond_2

    move v0, v1

    .line 112
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgol;

    .line 113
    if-eqz v0, :cond_1

    .line 114
    iget-object v3, p0, Lgok;->param:[Lgol;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 116
    new-instance v3, Lgol;

    invoke-direct {v3}, Lgol;-><init>()V

    aput-object v3, v2, v0

    .line 117
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 118
    invoke-virtual {p1}, Lhfp;->a()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 111
    :cond_2
    iget-object v0, p0, Lgok;->param:[Lgol;

    array-length v0, v0

    goto :goto_1

    .line 120
    :cond_3
    new-instance v3, Lgol;

    invoke-direct {v3}, Lgol;-><init>()V

    aput-object v3, v2, v0

    .line 121
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 122
    iput-object v2, p0, Lgok;->param:[Lgol;

    goto/16 :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lgok;->mergeFrom(Lhfp;)Lgok;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lgok;->payloadId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgok;->payloadId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_0
    iget-object v0, p0, Lgok;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Lgok;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_1
    iget-object v0, p0, Lgok;->mediaType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 26
    const/4 v0, 0x3

    iget-object v1, p0, Lgok;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 27
    :cond_2
    iget-object v0, p0, Lgok;->preference:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 28
    const/4 v0, 0x4

    iget-object v1, p0, Lgok;->preference:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 29
    :cond_3
    iget-object v0, p0, Lgok;->bitRate:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 30
    const/4 v0, 0x5

    iget-object v1, p0, Lgok;->bitRate:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 31
    :cond_4
    iget-object v0, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 32
    const/4 v0, 0x6

    iget-object v1, p0, Lgok;->sampleRate:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 33
    :cond_5
    iget-object v0, p0, Lgok;->channelCount:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 34
    const/4 v0, 0x7

    iget-object v1, p0, Lgok;->channelCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 35
    :cond_6
    iget-object v0, p0, Lgok;->param:[Lgol;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgok;->param:[Lgol;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 36
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgok;->param:[Lgol;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 37
    iget-object v1, p0, Lgok;->param:[Lgol;

    aget-object v1, v1, v0

    .line 38
    if-eqz v1, :cond_7

    .line 39
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 42
    return-void
.end method
