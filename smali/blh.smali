.class public Lblh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/dialer/main/impl/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    invoke-virtual {v0, v1, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 11
    return-void
.end method

.method public static c(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .prologue
    .line 12
    new-instance v0, Lml;

    const-string v1, "nui_launcher_shortcut"

    invoke-direct {v0, p0, v1}, Lml;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const v1, 0x7f020119

    .line 14
    if-nez p0, :cond_0

    .line 15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    new-instance v2, Lnn;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lnn;-><init>(I)V

    .line 17
    iput v1, v2, Lnn;->c:I

    .line 18
    iput-object p0, v2, Lnn;->b:Ljava/lang/Object;

    .line 21
    iget-object v1, v0, Lml;->a:Lmk;

    .line 22
    iput-object v2, v1, Lmk;->e:Lnn;

    .line 26
    invoke-static {p0}, Lcom/android/dialer/main/impl/MainActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 27
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    .line 28
    iget-object v1, v0, Lml;->a:Lmk;

    .line 29
    iput-object v2, v1, Lmk;->c:[Landroid/content/Intent;

    .line 32
    const v1, 0x7f11024f

    .line 33
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 34
    iget-object v2, v0, Lml;->a:Lmk;

    .line 35
    iput-object v1, v2, Lmk;->d:Ljava/lang/CharSequence;

    .line 37
    iget-object v1, v0, Lml;->a:Lmk;

    .line 38
    iget-object v1, v1, Lmk;->d:Ljava/lang/CharSequence;

    .line 39
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Shortcut much have a non-empty label"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    iget-object v1, v0, Lml;->a:Lmk;

    .line 42
    iget-object v1, v1, Lmk;->c:[Landroid/content/Intent;

    .line 43
    if-eqz v1, :cond_2

    iget-object v1, v0, Lml;->a:Lmk;

    .line 44
    iget-object v1, v1, Lmk;->c:[Landroid/content/Intent;

    .line 45
    array-length v1, v1

    if-nez v1, :cond_3

    .line 46
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Shortcut much have an intent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_3
    iget-object v0, v0, Lml;->a:Lmk;

    .line 49
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbw;->a(Landroid/content/Context;Lmk;Landroid/content/IntentSender;)Z

    .line 50
    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    const-string v1, "android.intent.extra.shortcut.ICON"

    const v2, 0x7f020119

    .line 53
    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 55
    const-string v1, "android.intent.extra.shortcut.NAME"

    const v2, 0x7f11024f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-static {p0}, Lcom/android/dialer/main/impl/MainActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 58
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2
    invoke-static {p1}, Lblh;->b(Landroid/content/Context;)V

    .line 3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 4
    invoke-static {p1}, Lblh;->c(Landroid/content/Context;)V

    .line 6
    :goto_0
    return-void

    .line 5
    :cond_0
    invoke-static {p1}, Lblh;->d(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1
    invoke-static {}, Lapw;->b()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-static {}, Lapw;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
