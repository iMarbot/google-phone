.class final Lbog;
.super Landroid/database/MergeCursor;
.source "PG"

# interfaces
.implements Lboc;


# direct methods
.method private constructor <init>([Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 9
    return-void
.end method

.method private static a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 5

    .prologue
    .line 13
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v0, Lbnz;->a:[Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 14
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 32
    :goto_0
    return-object v0

    .line 16
    :cond_0
    sget-object v0, Lbnz;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/Object;

    .line 17
    const/4 v0, 0x0

    :goto_1
    sget-object v3, Lbnz;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 18
    sget-object v3, Lbnz;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    .line 19
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 20
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 21
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getType(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 29
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 22
    :pswitch_0
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_2

    .line 24
    :pswitch_1
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_2

    .line 26
    :pswitch_2
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_2

    .line 28
    :pswitch_3
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_2

    .line 30
    :cond_2
    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 31
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 32
    goto :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static a(Landroid/content/Context;Landroid/database/Cursor;)Lbog;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Lbog;

    new-array v1, v3, [Landroid/database/Cursor;

    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v3, Lbnz;->a:[Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lbog;-><init>([Landroid/database/Cursor;)V

    .line 7
    :goto_0
    return-object v0

    .line 3
    :cond_0
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v0, Lbog;->a:[Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 4
    new-array v0, v3, [Ljava/lang/String;

    const v2, 0x7f110039

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 5
    new-instance v0, Lbog;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    aput-object v1, v2, v4

    .line 6
    invoke-static {p1}, Lbog;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Lbog;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lbog;->isFirst()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    return v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 12
    const-wide/16 v0, 0x0

    return-wide v0
.end method
