.class final Lhau;
.super Lhaq;
.source "PG"


# instance fields
.field private e:Ljava/nio/ByteBuffer;

.field private f:Z

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;Z)V
    .locals 4

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lhaq;-><init>()V

    .line 3
    const v0, 0x7fffffff

    iput v0, p0, Lhau;->m:I

    .line 4
    iput-object p1, p0, Lhau;->e:Ljava/nio/ByteBuffer;

    .line 5
    invoke-static {p1}, Lhev;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lhau;->g:J

    .line 6
    iget-wide v0, p0, Lhau;->g:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhau;->h:J

    .line 7
    iget-wide v0, p0, Lhau;->g:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhau;->i:J

    .line 8
    iget-wide v0, p0, Lhau;->i:J

    iput-wide v0, p0, Lhau;->j:J

    .line 9
    iput-boolean p2, p0, Lhau;->f:Z

    .line 10
    return-void
.end method

.method private A()B
    .locals 4

    .prologue
    .line 304
    iget-wide v0, p0, Lhau;->i:J

    iget-wide v2, p0, Lhau;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 305
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 306
    :cond_0
    iget-wide v0, p0, Lhau;->i:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lhau;->i:J

    .line 307
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->a(J)B

    move-result v0

    .line 308
    return v0
.end method

.method private final B()V
    .locals 4

    .prologue
    .line 315
    iget-wide v0, p0, Lhau;->h:J

    iget v2, p0, Lhau;->k:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhau;->h:J

    .line 316
    iget-wide v0, p0, Lhau;->h:J

    iget-wide v2, p0, Lhau;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 317
    iget v1, p0, Lhau;->m:I

    if-le v0, v1, :cond_0

    .line 318
    iget v1, p0, Lhau;->m:I

    sub-int/2addr v0, v1

    iput v0, p0, Lhau;->k:I

    .line 319
    iget-wide v0, p0, Lhau;->h:J

    iget v2, p0, Lhau;->k:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lhau;->h:J

    .line 321
    :goto_0
    return-void

    .line 320
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lhau;->k:I

    goto :goto_0
.end method

.method private final C()I
    .locals 4

    .prologue
    .line 322
    iget-wide v0, p0, Lhau;->h:J

    iget-wide v2, p0, Lhau;->i:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private x()J
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x1

    .line 186
    iget-wide v0, p0, Lhau;->i:J

    .line 187
    iget-wide v2, p0, Lhau;->h:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_9

    .line 188
    add-long v4, v0, v8

    .line 189
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->a(J)B

    move-result v0

    .line 190
    if-ltz v0, :cond_0

    .line 191
    iput-wide v4, p0, Lhau;->i:J

    .line 192
    int-to-long v0, v0

    .line 232
    :goto_0
    return-wide v0

    .line 193
    :cond_0
    iget-wide v2, p0, Lhau;->h:J

    sub-long/2addr v2, v4

    const-wide/16 v6, 0x9

    cmp-long v1, v2, v6

    if-ltz v1, :cond_9

    .line 194
    add-long v2, v4, v8

    .line 195
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 196
    shl-int/lit8 v1, v1, 0x7

    xor-int/2addr v0, v1

    if-gez v0, :cond_2

    .line 197
    xor-int/lit8 v0, v0, -0x80

    int-to-long v0, v0

    .line 230
    :cond_1
    :goto_1
    iput-wide v2, p0, Lhau;->i:J

    goto :goto_0

    .line 198
    :cond_2
    add-long v4, v2, v8

    .line 199
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v2, v3}, Lhev$d;->a(J)B

    move-result v1

    .line 200
    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_3

    .line 201
    xor-int/lit16 v0, v0, 0x3f80

    int-to-long v0, v0

    move-wide v2, v4

    goto :goto_1

    .line 202
    :cond_3
    add-long v2, v4, v8

    .line 203
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 204
    shl-int/lit8 v1, v1, 0x15

    xor-int/2addr v0, v1

    if-gez v0, :cond_4

    .line 205
    const v1, -0x1fc080

    xor-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_1

    .line 206
    :cond_4
    int-to-long v0, v0

    add-long v4, v2, v8

    .line 207
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v2, v3}, Lhev$d;->a(J)B

    move-result v2

    .line 208
    int-to-long v2, v2

    const/16 v6, 0x1c

    shl-long/2addr v2, v6

    xor-long/2addr v0, v2

    cmp-long v2, v0, v10

    if-ltz v2, :cond_5

    .line 209
    const-wide/32 v2, 0xfe03f80

    xor-long/2addr v0, v2

    move-wide v2, v4

    goto :goto_1

    .line 210
    :cond_5
    add-long v2, v4, v8

    .line 211
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 212
    int-to-long v4, v4

    const/16 v6, 0x23

    shl-long/2addr v4, v6

    xor-long/2addr v0, v4

    cmp-long v4, v0, v10

    if-gez v4, :cond_6

    .line 213
    const-wide v4, -0x7f01fc080L

    xor-long/2addr v0, v4

    goto :goto_1

    .line 214
    :cond_6
    add-long v4, v2, v8

    .line 215
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v2, v3}, Lhev$d;->a(J)B

    move-result v2

    .line 216
    int-to-long v2, v2

    const/16 v6, 0x2a

    shl-long/2addr v2, v6

    xor-long/2addr v0, v2

    cmp-long v2, v0, v10

    if-ltz v2, :cond_7

    .line 217
    const-wide v2, 0x3f80fe03f80L

    xor-long/2addr v0, v2

    move-wide v2, v4

    goto :goto_1

    .line 218
    :cond_7
    add-long v2, v4, v8

    .line 219
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 220
    int-to-long v4, v4

    const/16 v6, 0x31

    shl-long/2addr v4, v6

    xor-long/2addr v0, v4

    cmp-long v4, v0, v10

    if-gez v4, :cond_8

    .line 221
    const-wide v4, -0x1fc07f01fc080L

    xor-long/2addr v0, v4

    goto/16 :goto_1

    .line 222
    :cond_8
    add-long v4, v2, v8

    .line 223
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v2, v3}, Lhev$d;->a(J)B

    move-result v2

    .line 224
    int-to-long v2, v2

    const/16 v6, 0x38

    shl-long/2addr v2, v6

    xor-long/2addr v0, v2

    .line 225
    const-wide v2, 0xfe03f80fe03f80L

    xor-long/2addr v0, v2

    .line 226
    cmp-long v2, v0, v10

    if-gez v2, :cond_a

    .line 227
    add-long v2, v4, v8

    .line 228
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 229
    int-to-long v4, v4

    cmp-long v4, v4, v10

    if-gez v4, :cond_1

    .line 232
    :cond_9
    invoke-virtual {p0}, Lhau;->t()J

    move-result-wide v0

    goto/16 :goto_0

    :cond_a
    move-wide v2, v4

    goto/16 :goto_1
.end method

.method private y()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x4

    .line 241
    iget-wide v0, p0, Lhau;->i:J

    .line 242
    iget-wide v2, p0, Lhau;->h:J

    sub-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 243
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 244
    :cond_0
    add-long v2, v0, v4

    iput-wide v2, p0, Lhau;->i:J

    .line 246
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->a(J)B

    move-result v2

    .line 247
    and-int/lit16 v2, v2, 0xff

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    .line 249
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v4, v5}, Lhev$d;->a(J)B

    move-result v3

    .line 250
    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    const-wide/16 v4, 0x2

    add-long/2addr v4, v0

    .line 252
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v4, v5}, Lhev$d;->a(J)B

    move-result v3

    .line 253
    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    const-wide/16 v4, 0x3

    add-long/2addr v0, v4

    .line 255
    sget-object v3, Lhev;->a:Lhev$d;

    invoke-virtual {v3, v0, v1}, Lhev$d;->a(J)B

    move-result v0

    .line 256
    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v2

    .line 257
    return v0
.end method

.method private z()J
    .locals 10

    .prologue
    const-wide/16 v4, 0x8

    const-wide/16 v8, 0xff

    .line 258
    iget-wide v0, p0, Lhau;->i:J

    .line 259
    iget-wide v2, p0, Lhau;->h:J

    sub-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 260
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 261
    :cond_0
    add-long v2, v0, v4

    iput-wide v2, p0, Lhau;->i:J

    .line 263
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->a(J)B

    move-result v2

    .line 264
    int-to-long v2, v2

    and-long/2addr v2, v8

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    .line 266
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 267
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x8

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x2

    add-long/2addr v4, v0

    .line 269
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 270
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3

    add-long/2addr v4, v0

    .line 272
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 273
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x4

    add-long/2addr v4, v0

    .line 275
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 276
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x5

    add-long/2addr v4, v0

    .line 278
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 279
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x6

    add-long/2addr v4, v0

    .line 281
    sget-object v6, Lhev;->a:Lhev$d;

    invoke-virtual {v6, v4, v5}, Lhev$d;->a(J)B

    move-result v4

    .line 282
    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x30

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x7

    add-long/2addr v0, v4

    .line 284
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v0, v1}, Lhev$d;->a(J)B

    move-result v0

    .line 285
    int-to-long v0, v0

    and-long/2addr v0, v8

    const/16 v4, 0x38

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    .line 286
    return-wide v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-virtual {p0}, Lhau;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    iput v0, p0, Lhau;->l:I

    .line 19
    :goto_0
    return v0

    .line 14
    :cond_0
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    iput v0, p0, Lhau;->l:I

    .line 15
    iget v0, p0, Lhau;->l:I

    .line 16
    ushr-int/lit8 v0, v0, 0x3

    .line 17
    if-nez v0, :cond_1

    .line 18
    invoke-static {}, Lhcf;->d()Lhcf;

    move-result-object v0

    throw v0

    .line 19
    :cond_1
    iget v0, p0, Lhau;->l:I

    goto :goto_0
.end method

.method public final a(Lhbr;Lhbg;)Lhbr;
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    .line 116
    iget v1, p0, Lhau;->a:I

    iget v2, p0, Lhau;->b:I

    if-lt v1, v2, :cond_0

    .line 117
    invoke-static {}, Lhcf;->g()Lhcf;

    move-result-object v0

    throw v0

    .line 118
    :cond_0
    invoke-virtual {p0, v0}, Lhau;->c(I)I

    move-result v0

    .line 119
    iget v1, p0, Lhau;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhau;->a:I

    .line 120
    invoke-static {p1, p0, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v1

    .line 121
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lhau;->a(I)V

    .line 122
    iget v2, p0, Lhau;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lhau;->a:I

    .line 123
    invoke-virtual {p0, v0}, Lhau;->d(I)V

    .line 124
    return-object v1
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lhau;->l:I

    if-eq v0, p1, :cond_0

    .line 21
    invoke-static {}, Lhcf;->e()Lhcf;

    move-result-object v0

    throw v0

    .line 22
    :cond_0
    return-void
.end method

.method public final a(ILhde;Lhbg;)V
    .locals 2

    .prologue
    .line 96
    iget v0, p0, Lhau;->a:I

    iget v1, p0, Lhau;->b:I

    if-lt v0, v1, :cond_0

    .line 97
    invoke-static {}, Lhcf;->g()Lhcf;

    move-result-object v0

    throw v0

    .line 98
    :cond_0
    iget v0, p0, Lhau;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhau;->a:I

    .line 99
    invoke-interface {p2, p0, p3}, Lhde;->mergeFrom(Lhaq;Lhbg;)Lhde;

    .line 100
    const/4 v0, 0x4

    .line 101
    shl-int/lit8 v1, p1, 0x3

    or-int/2addr v0, v1

    .line 102
    invoke-virtual {p0, v0}, Lhau;->a(I)V

    .line 103
    iget v0, p0, Lhau;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhau;->a:I

    .line 104
    return-void
.end method

.method public final a(Lhde;Lhbg;)V
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    .line 106
    iget v1, p0, Lhau;->a:I

    iget v2, p0, Lhau;->b:I

    if-lt v1, v2, :cond_0

    .line 107
    invoke-static {}, Lhcf;->g()Lhcf;

    move-result-object v0

    throw v0

    .line 108
    :cond_0
    invoke-virtual {p0, v0}, Lhau;->c(I)I

    move-result v0

    .line 109
    iget v1, p0, Lhau;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhau;->a:I

    .line 110
    invoke-interface {p1, p0, p2}, Lhde;->mergeFrom(Lhaq;Lhbg;)Lhde;

    .line 111
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lhau;->a(I)V

    .line 112
    iget v1, p0, Lhau;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lhau;->a:I

    .line 113
    invoke-virtual {p0, v0}, Lhau;->d(I)V

    .line 114
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Lhau;->z()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final b(I)Z
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/16 v6, 0xa

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 23
    .line 24
    and-int/lit8 v2, p1, 0x7

    .line 25
    packed-switch v2, :pswitch_data_0

    .line 61
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 27
    :pswitch_0
    invoke-direct {p0}, Lhau;->C()I

    move-result v2

    if-lt v2, v6, :cond_1

    .line 29
    :goto_0
    if-ge v1, v6, :cond_0

    .line 30
    iget-wide v2, p0, Lhau;->i:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lhau;->i:J

    .line 31
    sget-object v4, Lhev;->a:Lhev$d;

    invoke-virtual {v4, v2, v3}, Lhev$d;->a(J)B

    move-result v2

    .line 32
    if-gez v2, :cond_3

    .line 33
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :cond_0
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0

    .line 37
    :cond_1
    :goto_1
    if-ge v1, v6, :cond_2

    .line 38
    invoke-direct {p0}, Lhau;->A()B

    move-result v2

    if-gez v2, :cond_3

    .line 39
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 40
    :cond_2
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0

    .line 42
    :pswitch_1
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lhau;->f(I)V

    .line 60
    :cond_3
    :goto_2
    return v0

    .line 44
    :pswitch_2
    invoke-virtual {p0}, Lhau;->s()I

    move-result v1

    invoke-virtual {p0, v1}, Lhau;->f(I)V

    goto :goto_2

    .line 47
    :cond_4
    :pswitch_3
    invoke-virtual {p0}, Lhau;->a()I

    move-result v1

    .line 48
    if-eqz v1, :cond_5

    invoke-virtual {p0, v1}, Lhau;->b(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 53
    :cond_5
    ushr-int/lit8 v1, p1, 0x3

    .line 55
    shl-int/lit8 v1, v1, 0x3

    or-int/2addr v1, v3

    .line 56
    invoke-virtual {p0, v1}, Lhau;->a(I)V

    goto :goto_2

    :pswitch_4
    move v0, v1

    .line 58
    goto :goto_2

    .line 59
    :pswitch_5
    invoke-virtual {p0, v3}, Lhau;->f(I)V

    goto :goto_2

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lhau;->y()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 287
    if-gez p1, :cond_0

    .line 288
    invoke-static {}, Lhcf;->b()Lhcf;

    move-result-object v0

    throw v0

    .line 289
    :cond_0
    invoke-virtual {p0}, Lhau;->w()I

    move-result v0

    add-int/2addr v0, p1

    .line 290
    iget v1, p0, Lhau;->m:I

    .line 291
    if-le v0, v1, :cond_1

    .line 292
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 293
    :cond_1
    iput v0, p0, Lhau;->m:I

    .line 294
    invoke-direct {p0}, Lhau;->B()V

    .line 295
    return v1
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Lhau;->x()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 296
    iput p1, p0, Lhau;->m:I

    .line 297
    invoke-direct {p0}, Lhau;->B()V

    .line 298
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Lhau;->x()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    return v0
.end method

.method public final f(I)V
    .locals 4

    .prologue
    .line 309
    if-ltz p1, :cond_0

    invoke-direct {p0}, Lhau;->C()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 310
    iget-wide v0, p0, Lhau;->i:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhau;->i:J

    .line 311
    return-void

    .line 312
    :cond_0
    if-gez p1, :cond_1

    .line 313
    invoke-static {}, Lhcf;->b()Lhcf;

    move-result-object v0

    throw v0

    .line 314
    :cond_1
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lhau;->z()J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lhau;->y()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Lhau;->x()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 9

    .prologue
    .line 70
    invoke-virtual {p0}, Lhau;->s()I

    move-result v8

    .line 71
    if-lez v8, :cond_0

    invoke-direct {p0}, Lhau;->C()I

    move-result v0

    if-gt v8, v0, :cond_0

    .line 72
    new-array v3, v8, [B

    .line 73
    iget-wide v1, p0, Lhau;->i:J

    const-wide/16 v4, 0x0

    int-to-long v6, v8

    invoke-static/range {v1 .. v7}, Lhev;->a(J[BJJ)V

    .line 74
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v3, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 75
    iget-wide v2, p0, Lhau;->i:J

    int-to-long v4, v8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lhau;->i:J

    .line 78
    :goto_0
    return-object v0

    .line 77
    :cond_0
    if-nez v8, :cond_1

    .line 78
    const-string v0, ""

    goto :goto_0

    .line 79
    :cond_1
    if-gez v8, :cond_2

    .line 80
    invoke-static {}, Lhcf;->b()Lhcf;

    move-result-object v0

    throw v0

    .line 81
    :cond_2
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public final k()Ljava/lang/String;
    .locals 9

    .prologue
    .line 82
    invoke-virtual {p0}, Lhau;->s()I

    move-result v8

    .line 83
    if-ltz v8, :cond_1

    invoke-direct {p0}, Lhau;->C()I

    move-result v0

    if-gt v8, v0, :cond_1

    .line 84
    new-array v3, v8, [B

    .line 85
    iget-wide v1, p0, Lhau;->i:J

    const-wide/16 v4, 0x0

    int-to-long v6, v8

    invoke-static/range {v1 .. v7}, Lhev;->a(J[BJJ)V

    .line 86
    invoke-static {v3}, Lhex;->a([B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lhcf;->j()Lhcf;

    move-result-object v0

    throw v0

    .line 88
    :cond_0
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v3, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 89
    iget-wide v2, p0, Lhau;->i:J

    int-to-long v4, v8

    add-long/2addr v2, v4

    iput-wide v2, p0, Lhau;->i:J

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_1
    if-nez v8, :cond_2

    .line 92
    const-string v0, ""

    goto :goto_0

    .line 93
    :cond_2
    if-gtz v8, :cond_3

    .line 94
    invoke-static {}, Lhcf;->b()Lhcf;

    move-result-object v0

    throw v0

    .line 95
    :cond_3
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public final l()Lhah;
    .locals 8

    .prologue
    .line 125
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    .line 126
    if-lez v0, :cond_0

    invoke-direct {p0}, Lhau;->C()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 127
    new-array v3, v0, [B

    .line 128
    iget-wide v1, p0, Lhau;->i:J

    const-wide/16 v4, 0x0

    int-to-long v6, v0

    invoke-static/range {v1 .. v7}, Lhev;->a(J[BJJ)V

    .line 129
    iget-wide v4, p0, Lhau;->i:J

    int-to-long v0, v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lhau;->i:J

    .line 130
    invoke-static {v3}, Lhah;->a([B)Lhah;

    move-result-object v0

    .line 132
    :goto_0
    return-object v0

    .line 131
    :cond_0
    if-nez v0, :cond_1

    .line 132
    sget-object v0, Lhah;->a:Lhah;

    goto :goto_0

    .line 133
    :cond_1
    if-gez v0, :cond_2

    .line 134
    invoke-static {}, Lhcf;->b()Lhcf;

    move-result-object v0

    throw v0

    .line 135
    :cond_2
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Lhau;->y()I

    move-result v0

    return v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0}, Lhau;->z()J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lhau;->s()I

    move-result v0

    invoke-static {v0}, Lhau;->e(I)I

    move-result v0

    return v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 141
    invoke-direct {p0}, Lhau;->x()J

    move-result-wide v0

    invoke-static {v0, v1}, Lhau;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final s()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 142
    iget-wide v0, p0, Lhau;->i:J

    .line 143
    iget-wide v2, p0, Lhau;->h:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_5

    .line 144
    add-long v4, v0, v8

    .line 145
    sget-object v2, Lhev;->a:Lhev$d;

    invoke-virtual {v2, v0, v1}, Lhev$d;->a(J)B

    move-result v0

    .line 146
    if-ltz v0, :cond_0

    .line 147
    iput-wide v4, p0, Lhau;->i:J

    .line 185
    :goto_0
    return v0

    .line 149
    :cond_0
    iget-wide v2, p0, Lhau;->h:J

    sub-long/2addr v2, v4

    const-wide/16 v6, 0x9

    cmp-long v1, v2, v6

    if-ltz v1, :cond_5

    .line 150
    add-long v2, v4, v8

    .line 151
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 152
    shl-int/lit8 v1, v1, 0x7

    xor-int/2addr v0, v1

    if-gez v0, :cond_2

    .line 153
    xor-int/lit8 v0, v0, -0x80

    .line 183
    :cond_1
    :goto_1
    iput-wide v2, p0, Lhau;->i:J

    goto :goto_0

    .line 154
    :cond_2
    add-long v4, v2, v8

    .line 155
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v2, v3}, Lhev$d;->a(J)B

    move-result v1

    .line 156
    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_3

    .line 157
    xor-int/lit16 v0, v0, 0x3f80

    move-wide v2, v4

    goto :goto_1

    .line 158
    :cond_3
    add-long v2, v4, v8

    .line 159
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 160
    shl-int/lit8 v1, v1, 0x15

    xor-int/2addr v0, v1

    if-gez v0, :cond_4

    .line 161
    const v1, -0x1fc080

    xor-int/2addr v0, v1

    goto :goto_1

    .line 162
    :cond_4
    add-long v4, v2, v8

    .line 163
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v2, v3}, Lhev$d;->a(J)B

    move-result v1

    .line 165
    shl-int/lit8 v2, v1, 0x1c

    xor-int/2addr v0, v2

    .line 166
    const v2, 0xfe03f80

    xor-int/2addr v0, v2

    .line 167
    if-gez v1, :cond_6

    add-long v2, v4, v8

    .line 169
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 170
    if-gez v1, :cond_1

    add-long v4, v2, v8

    .line 172
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v2, v3}, Lhev$d;->a(J)B

    move-result v1

    .line 173
    if-gez v1, :cond_6

    add-long v2, v4, v8

    .line 175
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 176
    if-gez v1, :cond_1

    add-long v4, v2, v8

    .line 178
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v2, v3}, Lhev$d;->a(J)B

    move-result v1

    .line 179
    if-gez v1, :cond_6

    add-long v2, v4, v8

    .line 181
    sget-object v1, Lhev;->a:Lhev$d;

    invoke-virtual {v1, v4, v5}, Lhev$d;->a(J)B

    move-result v1

    .line 182
    if-gez v1, :cond_1

    .line 185
    :cond_5
    invoke-virtual {p0}, Lhau;->t()J

    move-result-wide v0

    long-to-int v0, v0

    goto/16 :goto_0

    :cond_6
    move-wide v2, v4

    goto :goto_1
.end method

.method final t()J
    .locals 6

    .prologue
    .line 233
    const-wide/16 v2, 0x0

    .line 234
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x40

    if-ge v0, v1, :cond_1

    .line 235
    invoke-direct {p0}, Lhau;->A()B

    move-result v1

    .line 236
    and-int/lit8 v4, v1, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v0

    or-long/2addr v2, v4

    .line 237
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    .line 238
    return-wide v2

    .line 239
    :cond_0
    add-int/lit8 v0, v0, 0x7

    goto :goto_0

    .line 240
    :cond_1
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public final u()I
    .locals 2

    .prologue
    .line 299
    iget v0, p0, Lhau;->m:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 300
    const/4 v0, -0x1

    .line 301
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lhau;->m:I

    invoke-virtual {p0}, Lhau;->w()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final v()Z
    .locals 4

    .prologue
    .line 302
    iget-wide v0, p0, Lhau;->i:J

    iget-wide v2, p0, Lhau;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()I
    .locals 4

    .prologue
    .line 303
    iget-wide v0, p0, Lhau;->i:J

    iget-wide v2, p0, Lhau;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
