.class public final Lben;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field public a:Lbec;

.field public b:Lbeb;

.field public c:Lbea;

.field public d:Ljava/util/concurrent/ScheduledExecutorService;

.field public e:Ljava/util/concurrent/Executor;

.field public f:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 13
    :try_start_0
    iget-object v0, p0, Lben;->a:Lbec;

    invoke-interface {v0, p1}, Lbec;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lben;->b:Lbeb;

    if-nez v1, :cond_0

    .line 15
    const-string v0, "DialerUiTaskFragment.runTask"

    const-string v1, "task succeeded but UI is dead"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    :goto_0
    return-void

    .line 16
    :cond_0
    new-instance v1, Lbes;

    invoke-direct {v1, p0, v0}, Lbes;-><init>(Lben;Ljava/lang/Object;)V

    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    const-string v1, "DialerUiTaskFragment.runTask"

    const-string v2, "task failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    iget-object v1, p0, Lben;->c:Lbea;

    if-nez v1, :cond_1

    .line 21
    const-string v0, "DialerUiTaskFragment.runTask"

    const-string v1, "task failed but UI is dead"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 22
    :cond_1
    new-instance v1, Lbet;

    invoke-direct {v1, p0, v0}, Lbet;-><init>(Lben;Ljava/lang/Throwable;)V

    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lben;->setRetainInstance(Z)V

    .line 4
    return-void
.end method

.method public final onDetach()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 6
    const-string v0, "DialerUiTaskFragment.onDetach"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 7
    iput-object v2, p0, Lben;->b:Lbeb;

    .line 8
    iput-object v2, p0, Lben;->c:Lbea;

    .line 9
    iget-object v0, p0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 11
    iput-object v2, p0, Lben;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 12
    :cond_0
    return-void
.end method
