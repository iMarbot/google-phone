.class public final Lhgv;
.super Lhft;
.source "PG"


# instance fields
.field private a:Lhgy;

.field private b:Lhgz;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lhgv;->a:Lhgy;

    .line 4
    iput-object v1, p0, Lhgv;->b:Lhgz;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lhgv;->c:Ljava/lang/String;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lhgv;->d:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lhgv;->e:Ljava/lang/String;

    .line 8
    iput v2, p0, Lhgv;->f:I

    .line 9
    iput-boolean v2, p0, Lhgv;->g:Z

    .line 10
    iput-object v1, p0, Lhgv;->unknownFieldData:Lhfv;

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lhgv;->cachedSize:I

    .line 12
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 29
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 30
    iget-object v1, p0, Lhgv;->a:Lhgy;

    if-eqz v1, :cond_0

    .line 31
    const/4 v1, 0x1

    iget-object v2, p0, Lhgv;->a:Lhgy;

    .line 32
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_0
    iget-object v1, p0, Lhgv;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 34
    const/4 v1, 0x2

    iget-object v2, p0, Lhgv;->b:Lhgz;

    .line 35
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_1
    iget-object v1, p0, Lhgv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhgv;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 37
    const/4 v1, 0x3

    iget-object v2, p0, Lhgv;->c:Ljava/lang/String;

    .line 38
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_2
    iget-object v1, p0, Lhgv;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhgv;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 40
    const/4 v1, 0x4

    iget-object v2, p0, Lhgv;->d:Ljava/lang/String;

    .line 41
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_3
    iget-object v1, p0, Lhgv;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhgv;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 43
    const/4 v1, 0x5

    iget-object v2, p0, Lhgv;->e:Ljava/lang/String;

    .line 44
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_4
    iget v1, p0, Lhgv;->f:I

    if-eqz v1, :cond_5

    .line 46
    const/4 v1, 0x6

    iget v2, p0, Lhgv;->f:I

    .line 47
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_5
    iget-boolean v1, p0, Lhgv;->g:Z

    if-eqz v1, :cond_6

    .line 49
    const/4 v1, 0x7

    iget-boolean v2, p0, Lhgv;->g:Z

    .line 51
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 52
    add-int/2addr v0, v1

    .line 53
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 54
    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 58
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    :sswitch_0
    return-object p0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lhgv;->a:Lhgy;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    iput-object v0, p0, Lhgv;->a:Lhgy;

    .line 62
    :cond_1
    iget-object v0, p0, Lhgv;->a:Lhgy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 64
    :sswitch_2
    iget-object v0, p0, Lhgv;->b:Lhgz;

    if-nez v0, :cond_2

    .line 65
    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgv;->b:Lhgz;

    .line 66
    :cond_2
    iget-object v0, p0, Lhgv;->b:Lhgz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 68
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgv;->c:Ljava/lang/String;

    goto :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgv;->d:Ljava/lang/String;

    goto :goto_0

    .line 72
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgv;->e:Ljava/lang/String;

    goto :goto_0

    .line 75
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 76
    iput v0, p0, Lhgv;->f:I

    goto :goto_0

    .line 78
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lhgv;->g:Z

    goto :goto_0

    .line 56
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lhgv;->a:Lhgy;

    if-eqz v0, :cond_0

    .line 14
    const/4 v0, 0x1

    iget-object v1, p0, Lhgv;->a:Lhgy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 15
    :cond_0
    iget-object v0, p0, Lhgv;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    iget-object v1, p0, Lhgv;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 17
    :cond_1
    iget-object v0, p0, Lhgv;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhgv;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 18
    const/4 v0, 0x3

    iget-object v1, p0, Lhgv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 19
    :cond_2
    iget-object v0, p0, Lhgv;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhgv;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 20
    const/4 v0, 0x4

    iget-object v1, p0, Lhgv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_3
    iget-object v0, p0, Lhgv;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhgv;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 22
    const/4 v0, 0x5

    iget-object v1, p0, Lhgv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_4
    iget v0, p0, Lhgv;->f:I

    if-eqz v0, :cond_5

    .line 24
    const/4 v0, 0x6

    iget v1, p0, Lhgv;->f:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_5
    iget-boolean v0, p0, Lhgv;->g:Z

    if-eqz v0, :cond_6

    .line 26
    const/4 v0, 0x7

    iget-boolean v1, p0, Lhgv;->g:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 27
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 28
    return-void
.end method
