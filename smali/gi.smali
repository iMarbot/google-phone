.class public abstract Lgi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static A:Ljava/lang/ThreadLocal;

.field private static i:[I

.field private static j:Lge;


# instance fields
.field private B:I

.field private C:Z

.field private D:Z

.field private E:Ljava/util/ArrayList;

.field private F:Ljava/util/ArrayList;

.field public a:J

.field public b:J

.field public c:Ljava/util/ArrayList;

.field public d:Ljava/util/ArrayList;

.field public e:Lgs;

.field public f:Z

.field public g:Ljava/util/ArrayList;

.field public h:Lge;

.field private k:Ljava/lang/String;

.field private l:Landroid/animation/TimeInterpolator;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/ArrayList;

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/ArrayList;

.field private q:Ljava/util/ArrayList;

.field private r:Ljava/util/ArrayList;

.field private s:Ljava/util/ArrayList;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/ArrayList;

.field private v:Lgw;

.field private w:Lgw;

.field private x:[I

.field private y:Ljava/util/ArrayList;

.field private z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 511
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lgi;->i:[I

    .line 512
    new-instance v0, Lge;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lge;-><init>(B)V

    sput-object v0, Lgi;->j:Lge;

    .line 513
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lgi;->A:Ljava/lang/ThreadLocal;

    return-void

    .line 511
    :array_0
    .array-data 4
        0x2
        0x1
        0x3
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgi;->k:Ljava/lang/String;

    .line 3
    iput-wide v4, p0, Lgi;->a:J

    .line 4
    iput-wide v4, p0, Lgi;->b:J

    .line 5
    iput-object v1, p0, Lgi;->l:Landroid/animation/TimeInterpolator;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->c:Ljava/util/ArrayList;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    .line 8
    iput-object v1, p0, Lgi;->m:Ljava/util/ArrayList;

    .line 9
    iput-object v1, p0, Lgi;->n:Ljava/util/ArrayList;

    .line 10
    iput-object v1, p0, Lgi;->o:Ljava/util/ArrayList;

    .line 11
    iput-object v1, p0, Lgi;->p:Ljava/util/ArrayList;

    .line 12
    iput-object v1, p0, Lgi;->q:Ljava/util/ArrayList;

    .line 13
    iput-object v1, p0, Lgi;->r:Ljava/util/ArrayList;

    .line 14
    iput-object v1, p0, Lgi;->s:Ljava/util/ArrayList;

    .line 15
    iput-object v1, p0, Lgi;->t:Ljava/util/ArrayList;

    .line 16
    iput-object v1, p0, Lgi;->u:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Lgw;

    invoke-direct {v0}, Lgw;-><init>()V

    iput-object v0, p0, Lgi;->v:Lgw;

    .line 18
    new-instance v0, Lgw;

    invoke-direct {v0}, Lgw;-><init>()V

    iput-object v0, p0, Lgi;->w:Lgw;

    .line 19
    iput-object v1, p0, Lgi;->e:Lgs;

    .line 20
    sget-object v0, Lgi;->i:[I

    iput-object v0, p0, Lgi;->x:[I

    .line 21
    iput-boolean v2, p0, Lgi;->f:Z

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->g:Ljava/util/ArrayList;

    .line 23
    iput v2, p0, Lgi;->B:I

    .line 24
    iput-boolean v2, p0, Lgi;->C:Z

    .line 25
    iput-boolean v2, p0, Lgi;->D:Z

    .line 26
    iput-object v1, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->F:Ljava/util/ArrayList;

    .line 28
    sget-object v0, Lgi;->j:Lge;

    iput-object v0, p0, Lgi;->h:Lge;

    .line 29
    return-void
.end method

.method private static a(Lgw;Landroid/view/View;Lgv;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lgw;->a:Lpd;

    invoke-virtual {v0, p1, p2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 258
    if-ltz v0, :cond_0

    .line 259
    iget-object v2, p0, Lgw;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-ltz v2, :cond_5

    .line 260
    iget-object v2, p0, Lgw;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 263
    :cond_0
    :goto_0
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p1}, Lri;->s(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_1

    .line 266
    iget-object v2, p0, Lgw;->d:Lpd;

    invoke-virtual {v2, v0}, Lpd;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 267
    iget-object v2, p0, Lgw;->d:Lpd;

    invoke-virtual {v2, v0, v1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_4

    .line 270
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 271
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 272
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 273
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v2

    .line 274
    iget-object v0, p0, Lgw;->c:Lpj;

    .line 275
    iget-boolean v4, v0, Lpj;->b:Z

    if-eqz v4, :cond_2

    .line 276
    invoke-virtual {v0}, Lpj;->a()V

    .line 277
    :cond_2
    iget-object v4, v0, Lpj;->c:[J

    iget v0, v0, Lpj;->e:I

    invoke-static {v4, v0, v2, v3}, Lph;->a([JIJ)I

    move-result v0

    .line 278
    if-ltz v0, :cond_8

    .line 279
    iget-object v0, p0, Lgw;->c:Lpj;

    .line 281
    iget-object v4, v0, Lpj;->c:[J

    iget v5, v0, Lpj;->e:I

    invoke-static {v4, v5, v2, v3}, Lph;->a([JIJ)I

    move-result v4

    .line 282
    if-ltz v4, :cond_3

    iget-object v5, v0, Lpj;->d:[Ljava/lang/Object;

    aget-object v5, v5, v4

    sget-object v6, Lpj;->a:Ljava/lang/Object;

    if-ne v5, v6, :cond_7

    :cond_3
    move-object v0, v1

    .line 285
    :goto_2
    check-cast v0, Landroid/view/View;

    .line 286
    if-eqz v0, :cond_4

    .line 287
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lqy;->a(Landroid/view/View;Z)V

    .line 288
    iget-object v0, p0, Lgw;->c:Lpj;

    invoke-virtual {v0, v2, v3, v1}, Lpj;->a(JLjava/lang/Object;)V

    .line 292
    :cond_4
    :goto_3
    return-void

    .line 261
    :cond_5
    iget-object v2, p0, Lgw;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 268
    :cond_6
    iget-object v2, p0, Lgw;->d:Lpd;

    invoke-virtual {v2, v0, p1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 284
    :cond_7
    iget-object v0, v0, Lpj;->d:[Ljava/lang/Object;

    aget-object v0, v0, v4

    goto :goto_2

    .line 290
    :cond_8
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lqy;->a(Landroid/view/View;Z)V

    .line 291
    iget-object v0, p0, Lgw;->c:Lpj;

    invoke-virtual {v0, v2, v3, p1}, Lpj;->a(JLjava/lang/Object;)V

    goto :goto_3
.end method

.method private final a(Lgw;Lgw;)V
    .locals 12

    .prologue
    .line 36
    new-instance v6, Lpd;

    iget-object v0, p1, Lgw;->a:Lpd;

    invoke-direct {v6, v0}, Lpd;-><init>(Lpv;)V

    .line 37
    new-instance v7, Lpd;

    iget-object v0, p2, Lgw;->a:Lpd;

    invoke-direct {v7, v0}, Lpd;-><init>(Lpv;)V

    .line 38
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    iget-object v0, p0, Lgi;->x:[I

    array-length v0, v0

    if-ge v4, v0, :cond_7

    .line 39
    iget-object v0, p0, Lgi;->x:[I

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 41
    :pswitch_0
    invoke-virtual {v6}, Lpd;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_0

    .line 42
    invoke-virtual {v6, v2}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 43
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lgi;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    invoke-virtual {v7, v0}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    .line 45
    if-eqz v0, :cond_1

    iget-object v1, v0, Lgv;->b:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lgv;->b:Landroid/view/View;

    invoke-virtual {p0, v1}, Lgi;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    invoke-virtual {v6, v2}, Lpd;->d(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgv;

    .line 47
    iget-object v3, p0, Lgi;->y:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v1, p0, Lgi;->z:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 51
    :pswitch_1
    iget-object v8, p1, Lgw;->d:Lpd;

    iget-object v9, p2, Lgw;->d:Lpd;

    .line 52
    invoke-virtual {v8}, Lpd;->size()I

    move-result v10

    .line 53
    const/4 v0, 0x0

    move v5, v0

    :goto_2
    if-ge v5, v10, :cond_0

    .line 54
    invoke-virtual {v8, v5}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 55
    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lgi;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 56
    invoke-virtual {v8, v5}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v9, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 57
    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Lgi;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    invoke-virtual {v6, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgv;

    .line 59
    invoke-virtual {v7, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgv;

    .line 60
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 61
    iget-object v11, p0, Lgi;->y:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v2, p0, Lgi;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-virtual {v6, v0}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-virtual {v7, v1}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 67
    :pswitch_2
    iget-object v8, p1, Lgw;->b:Landroid/util/SparseArray;

    iget-object v9, p2, Lgw;->b:Landroid/util/SparseArray;

    .line 68
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v10

    .line 69
    const/4 v0, 0x0

    move v5, v0

    :goto_3
    if-ge v5, v10, :cond_0

    .line 70
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 71
    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lgi;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 73
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lgi;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    invoke-virtual {v6, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgv;

    .line 75
    invoke-virtual {v7, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgv;

    .line 76
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 77
    iget-object v11, p0, Lgi;->y:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v2, p0, Lgi;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {v6, v0}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-virtual {v7, v1}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    .line 83
    :pswitch_3
    iget-object v8, p1, Lgw;->c:Lpj;

    iget-object v9, p2, Lgw;->c:Lpj;

    .line 84
    invoke-virtual {v8}, Lpj;->b()I

    move-result v10

    .line 85
    const/4 v0, 0x0

    move v5, v0

    :goto_4
    if-ge v5, v10, :cond_0

    .line 86
    invoke-virtual {v8, v5}, Lpj;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 87
    if-eqz v0, :cond_5

    invoke-virtual {p0, v0}, Lgi;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 88
    invoke-virtual {v8, v5}, Lpj;->a(I)J

    move-result-wide v2

    .line 90
    iget-object v1, v9, Lpj;->c:[J

    iget v11, v9, Lpj;->e:I

    invoke-static {v1, v11, v2, v3}, Lph;->a([JIJ)I

    move-result v1

    .line 91
    if-ltz v1, :cond_4

    iget-object v2, v9, Lpj;->d:[Ljava/lang/Object;

    aget-object v2, v2, v1

    sget-object v3, Lpj;->a:Ljava/lang/Object;

    if-ne v2, v3, :cond_6

    .line 92
    :cond_4
    const/4 v1, 0x0

    .line 94
    :goto_5
    check-cast v1, Landroid/view/View;

    .line 95
    if-eqz v1, :cond_5

    invoke-virtual {p0, v1}, Lgi;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 96
    invoke-virtual {v6, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgv;

    .line 97
    invoke-virtual {v7, v1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgv;

    .line 98
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 99
    iget-object v11, p0, Lgi;->y:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v2, p0, Lgi;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {v6, v0}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-virtual {v7, v1}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_4

    .line 93
    :cond_6
    iget-object v2, v9, Lpj;->d:[Ljava/lang/Object;

    aget-object v1, v2, v1

    goto :goto_5

    .line 106
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    invoke-virtual {v6}, Lpd;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 107
    invoke-virtual {v6, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    .line 108
    iget-object v2, v0, Lgv;->b:Landroid/view/View;

    invoke-virtual {p0, v2}, Lgi;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 109
    iget-object v2, p0, Lgi;->y:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lgi;->z:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 112
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    invoke-virtual {v7}, Lpd;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 113
    invoke-virtual {v7, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    .line 114
    iget-object v2, v0, Lgv;->b:Landroid/view/View;

    invoke-virtual {p0, v2}, Lgi;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 115
    iget-object v2, p0, Lgi;->z:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lgi;->y:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 118
    :cond_b
    return-void

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Lgv;Lgv;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 427
    iget-object v1, p0, Lgv;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 428
    iget-object v2, p1, Lgv;->a:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 429
    if-nez v1, :cond_0

    if-eqz v2, :cond_3

    .line 430
    :cond_0
    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    .line 433
    :cond_1
    :goto_0
    return v0

    .line 432
    :cond_2
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 301
    if-nez p1, :cond_1

    .line 320
    :cond_0
    return-void

    .line 303
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 304
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 305
    new-instance v0, Lgv;

    invoke-direct {v0}, Lgv;-><init>()V

    .line 306
    iput-object p1, v0, Lgv;->b:Landroid/view/View;

    .line 307
    if-eqz p2, :cond_3

    .line 308
    invoke-virtual {p0, v0}, Lgi;->a(Lgv;)V

    .line 310
    :goto_0
    iget-object v1, v0, Lgv;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual {p0, v0}, Lgi;->c(Lgv;)V

    .line 312
    if-eqz p2, :cond_4

    .line 313
    iget-object v1, p0, Lgi;->v:Lgw;

    invoke-static {v1, p1, v0}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    .line 315
    :cond_2
    :goto_1
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 316
    check-cast p1, Landroid/view/ViewGroup;

    .line 317
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 318
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lgi;->c(Landroid/view/View;Z)V

    .line 319
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 309
    :cond_3
    invoke-virtual {p0, v0}, Lgi;->b(Lgv;)V

    goto :goto_0

    .line 314
    :cond_4
    iget-object v1, p0, Lgi;->w:Lgw;

    invoke-static {v1, p1, v0}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    goto :goto_1
.end method

.method private static f()Lpd;
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lgi;->A:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpd;

    .line 185
    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 187
    sget-object v1, Lgi;->A:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 188
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Lgv;Lgv;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(J)Lgi;
    .locals 1

    .prologue
    .line 30
    iput-wide p1, p0, Lgi;->b:J

    .line 31
    return-object p0
.end method

.method public a(Lgn;)Lgi;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 467
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 468
    :cond_0
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    return-object p0
.end method

.method public final a(Landroid/view/View;Z)Lgv;
    .locals 1

    .prologue
    .line 321
    :goto_0
    iget-object v0, p0, Lgi;->e:Lgs;

    if-eqz v0, :cond_0

    .line 322
    iget-object p0, p0, Lgi;->e:Lgs;

    goto :goto_0

    .line 323
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lgi;->v:Lgw;

    .line 324
    :goto_1
    iget-object v0, v0, Lgw;->a:Lpd;

    invoke-virtual {v0, p1}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    return-object v0

    .line 323
    :cond_1
    iget-object v0, p0, Lgi;->w:Lgw;

    goto :goto_1
.end method

.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 488
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 489
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490
    iget-wide v2, p0, Lgi;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 491
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "dur("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lgi;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    :cond_0
    iget-wide v2, p0, Lgi;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 493
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "dly("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lgi;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 494
    :cond_1
    iget-object v2, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_2

    iget-object v2, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 495
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "tgts("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 496
    iget-object v2, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    move-object v2, v0

    move v0, v1

    .line 497
    :goto_0
    iget-object v3, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 498
    if-lez v0, :cond_3

    .line 499
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 500
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 501
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move-object v2, v0

    .line 502
    :cond_5
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 503
    :goto_1
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 504
    if-lez v1, :cond_6

    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 506
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 507
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 508
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 509
    :cond_8
    return-object v0
.end method

.method final a(Landroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 386
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->y:Ljava/util/ArrayList;

    .line 387
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgi;->z:Ljava/util/ArrayList;

    .line 388
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v1, p0, Lgi;->w:Lgw;

    invoke-direct {p0, v0, v1}, Lgi;->a(Lgw;Lgw;)V

    .line 389
    invoke-static {}, Lgi;->f()Lpd;

    move-result-object v4

    .line 390
    invoke-virtual {v4}, Lpd;->size()I

    move-result v0

    .line 391
    invoke-static {p1}, Lhg;->a(Landroid/view/View;)Lhu;

    move-result-object v5

    .line 392
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_5

    .line 393
    invoke-virtual {v4, v3}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 394
    if-eqz v0, :cond_2

    .line 395
    invoke-virtual {v4, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgl;

    .line 396
    if-eqz v1, :cond_2

    iget-object v6, v1, Lgl;->a:Landroid/view/View;

    if-eqz v6, :cond_2

    iget-object v6, v1, Lgl;->d:Lhu;

    .line 397
    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 398
    iget-object v6, v1, Lgl;->c:Lgv;

    .line 399
    iget-object v7, v1, Lgl;->a:Landroid/view/View;

    .line 400
    invoke-virtual {p0, v7, v2}, Lgi;->a(Landroid/view/View;Z)Lgv;

    move-result-object v8

    .line 401
    invoke-virtual {p0, v7, v2}, Lgi;->b(Landroid/view/View;Z)Lgv;

    move-result-object v7

    .line 402
    if-nez v8, :cond_0

    if-eqz v7, :cond_3

    :cond_0
    iget-object v1, v1, Lgl;->e:Lgi;

    .line 403
    invoke-virtual {v1, v6, v7}, Lgi;->a(Lgv;Lgv;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 404
    :goto_1
    if-eqz v1, :cond_2

    .line 405
    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 406
    :cond_1
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 408
    :cond_2
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    .line 403
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 407
    :cond_4
    invoke-virtual {v4, v0}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 409
    :cond_5
    iget-object v2, p0, Lgi;->v:Lgw;

    iget-object v3, p0, Lgi;->w:Lgw;

    iget-object v4, p0, Lgi;->y:Ljava/util/ArrayList;

    iget-object v5, p0, Lgi;->z:Ljava/util/ArrayList;

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lgi;->a(Landroid/view/ViewGroup;Lgw;Lgw;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 410
    invoke-virtual {p0}, Lgi;->b()V

    .line 411
    return-void
.end method

.method protected a(Landroid/view/ViewGroup;Lgw;Lgw;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 16

    .prologue
    .line 119
    invoke-static {}, Lgi;->f()Lpd;

    move-result-object v10

    .line 120
    new-instance v11, Landroid/util/SparseIntArray;

    invoke-direct {v11}, Landroid/util/SparseIntArray;-><init>()V

    .line 121
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 122
    const/4 v2, 0x0

    move v9, v2

    :goto_0
    if-ge v9, v12, :cond_a

    .line 123
    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgv;

    .line 124
    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgv;

    .line 125
    if-eqz v2, :cond_c

    iget-object v4, v2, Lgv;->c:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 126
    const/4 v2, 0x0

    move-object v4, v2

    .line 127
    :goto_1
    if-eqz v3, :cond_0

    iget-object v2, v3, Lgv;->c:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 128
    const/4 v3, 0x0

    .line 129
    :cond_0
    if-nez v4, :cond_1

    if-eqz v3, :cond_5

    .line 130
    :cond_1
    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, Lgi;->a(Lgv;Lgv;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v2, 0x1

    .line 131
    :goto_2
    if-eqz v2, :cond_5

    .line 132
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v3}, Lgi;->a(Landroid/view/ViewGroup;Lgv;Lgv;)Landroid/animation/Animator;

    move-result-object v6

    .line 133
    if-eqz v6, :cond_5

    .line 134
    const/4 v2, 0x0

    .line 135
    if-eqz v3, :cond_9

    .line 136
    iget-object v5, v3, Lgv;->b:Landroid/view/View;

    .line 137
    invoke-virtual/range {p0 .. p0}, Lgi;->a()[Ljava/lang/String;

    move-result-object v7

    .line 138
    if-eqz v5, :cond_8

    if-eqz v7, :cond_8

    array-length v3, v7

    if-lez v3, :cond_8

    .line 139
    new-instance v4, Lgv;

    invoke-direct {v4}, Lgv;-><init>()V

    .line 140
    iput-object v5, v4, Lgv;->b:Landroid/view/View;

    .line 141
    move-object/from16 v0, p3

    iget-object v2, v0, Lgw;->a:Lpd;

    invoke-virtual {v2, v5}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgv;

    .line 142
    if-eqz v2, :cond_4

    .line 143
    const/4 v3, 0x0

    :goto_3
    array-length v8, v7

    if-ge v3, v8, :cond_4

    .line 144
    iget-object v8, v4, Lgv;->a:Ljava/util/Map;

    aget-object v13, v7, v3

    iget-object v14, v2, Lgv;->a:Ljava/util/Map;

    aget-object v15, v7, v3

    .line 145
    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .line 146
    invoke-interface {v8, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 130
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 148
    :cond_4
    invoke-virtual {v10}, Lpd;->size()I

    move-result v7

    .line 149
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    if-ge v3, v7, :cond_7

    .line 150
    invoke-virtual {v10, v3}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/Animator;

    .line 151
    invoke-virtual {v10, v2}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgl;

    .line 152
    iget-object v8, v2, Lgl;->c:Lgv;

    if-eqz v8, :cond_6

    iget-object v8, v2, Lgl;->a:Landroid/view/View;

    if-ne v8, v5, :cond_6

    iget-object v8, v2, Lgl;->b:Ljava/lang/String;

    .line 154
    move-object/from16 v0, p0

    iget-object v13, v0, Lgi;->k:Ljava/lang/String;

    .line 155
    invoke-virtual {v8, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 156
    iget-object v2, v2, Lgl;->c:Lgv;

    invoke-virtual {v2, v4}, Lgv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 157
    const/4 v2, 0x0

    move-object v7, v4

    move-object v3, v5

    move-object v8, v2

    .line 162
    :goto_5
    if-eqz v8, :cond_5

    .line 163
    new-instance v2, Lgl;

    .line 164
    move-object/from16 v0, p0

    iget-object v4, v0, Lgi;->k:Ljava/lang/String;

    .line 166
    invoke-static/range {p1 .. p1}, Lhg;->a(Landroid/view/View;)Lhu;

    move-result-object v6

    move-object/from16 v5, p0

    invoke-direct/range {v2 .. v7}, Lgl;-><init>(Landroid/view/View;Ljava/lang/String;Lgi;Lhu;Lgv;)V

    .line 167
    invoke-virtual {v10, v8, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lgi;->F:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_5
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_0

    .line 159
    :cond_6
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    :cond_7
    move-object v2, v4

    :cond_8
    move-object v7, v2

    move-object v3, v5

    move-object v8, v6

    .line 160
    goto :goto_5

    .line 161
    :cond_9
    iget-object v3, v4, Lgv;->b:Landroid/view/View;

    move-object v7, v2

    move-object v8, v6

    goto :goto_5

    .line 170
    :cond_a
    const-wide v2, 0x7fffffffffffffffL

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 171
    const/4 v2, 0x0

    move v3, v2

    :goto_6
    invoke-virtual {v11}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_b

    .line 172
    invoke-virtual {v11, v3}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    .line 173
    move-object/from16 v0, p0

    iget-object v4, v0, Lgi;->F:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/Animator;

    .line 174
    invoke-virtual {v11, v3}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    int-to-long v4, v4

    const-wide v6, 0x7fffffffffffffffL

    sub-long/2addr v4, v6

    invoke-virtual {v2}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 175
    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 176
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 177
    :cond_b
    return-void

    :cond_c
    move-object v4, v2

    goto/16 :goto_1
.end method

.method final a(Landroid/view/ViewGroup;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 224
    invoke-virtual {p0, p2}, Lgi;->a(Z)V

    .line 225
    iget-object v0, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    :cond_0
    move v1, v2

    .line 226
    :goto_0
    iget-object v0, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 227
    iget-object v0, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 228
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_1

    .line 230
    new-instance v3, Lgv;

    invoke-direct {v3}, Lgv;-><init>()V

    .line 231
    iput-object v0, v3, Lgv;->b:Landroid/view/View;

    .line 232
    if-eqz p2, :cond_2

    .line 233
    invoke-virtual {p0, v3}, Lgi;->a(Lgv;)V

    .line 235
    :goto_1
    iget-object v4, v3, Lgv;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-virtual {p0, v3}, Lgi;->c(Lgv;)V

    .line 237
    if-eqz p2, :cond_3

    .line 238
    iget-object v4, p0, Lgi;->v:Lgw;

    invoke-static {v4, v0, v3}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    .line 240
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p0, v3}, Lgi;->b(Lgv;)V

    goto :goto_1

    .line 239
    :cond_3
    iget-object v4, p0, Lgi;->w:Lgw;

    invoke-static {v4, v0, v3}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    goto :goto_2

    .line 241
    :cond_4
    :goto_3
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 242
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 243
    new-instance v1, Lgv;

    invoke-direct {v1}, Lgv;-><init>()V

    .line 244
    iput-object v0, v1, Lgv;->b:Landroid/view/View;

    .line 245
    if-eqz p2, :cond_5

    .line 246
    invoke-virtual {p0, v1}, Lgi;->a(Lgv;)V

    .line 248
    :goto_4
    iget-object v3, v1, Lgv;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    invoke-virtual {p0, v1}, Lgi;->c(Lgv;)V

    .line 250
    if-eqz p2, :cond_6

    .line 251
    iget-object v3, p0, Lgi;->v:Lgw;

    invoke-static {v3, v0, v1}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    .line 253
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 247
    :cond_5
    invoke-virtual {p0, v1}, Lgi;->b(Lgv;)V

    goto :goto_4

    .line 252
    :cond_6
    iget-object v3, p0, Lgi;->w:Lgw;

    invoke-static {v3, v0, v1}, Lgi;->a(Lgw;Landroid/view/View;Lgv;)V

    goto :goto_5

    .line 254
    :cond_7
    invoke-direct {p0, p1, p2}, Lgi;->c(Landroid/view/View;Z)V

    .line 255
    :cond_8
    return-void
.end method

.method public a(Lgm;)V
    .locals 0

    .prologue
    .line 476
    return-void
.end method

.method public abstract a(Lgv;)V
.end method

.method final a(Z)V
    .locals 1

    .prologue
    .line 293
    if-eqz p1, :cond_0

    .line 294
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v0, v0, Lgw;->a:Lpd;

    invoke-virtual {v0}, Lpd;->clear()V

    .line 295
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v0, v0, Lgw;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 296
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0}, Lpj;->c()V

    .line 300
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lgi;->w:Lgw;

    iget-object v0, v0, Lgw;->a:Lpd;

    invoke-virtual {v0}, Lpd;->clear()V

    .line 298
    iget-object v0, p0, Lgi;->w:Lgw;

    iget-object v0, v0, Lgw;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 299
    iget-object v0, p0, Lgi;->w:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0}, Lpj;->c()V

    goto :goto_0
.end method

.method final a(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 178
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 179
    iget-object v2, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    iget-object v2, p0, Lgi;->c:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lgv;Lgv;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 412
    .line 413
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    .line 414
    invoke-virtual {p0}, Lgi;->a()[Ljava/lang/String;

    move-result-object v3

    .line 415
    if-eqz v3, :cond_2

    .line 416
    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 417
    invoke-static {p1, p2, v5}, Lgi;->a(Lgv;Lgv;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    .line 426
    :goto_1
    return v0

    .line 420
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 421
    :cond_2
    iget-object v0, p1, Lgv;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    invoke-static {p1, p2, v0}, Lgi;->a(Lgv;Lgv;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 424
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(J)Lgi;
    .locals 1

    .prologue
    .line 32
    iput-wide p1, p0, Lgi;->a:J

    .line 33
    return-object p0
.end method

.method public b(Landroid/view/View;)Lgi;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    return-object p0
.end method

.method public b(Lgn;)Lgi;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 475
    :cond_0
    :goto_0
    return-object p0

    .line 472
    :cond_1
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 473
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 474
    const/4 v0, 0x0

    iput-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method final b(Landroid/view/View;Z)Lgv;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 325
    :goto_0
    iget-object v0, p0, Lgi;->e:Lgs;

    if-eqz v0, :cond_0

    .line 326
    iget-object p0, p0, Lgi;->e:Lgs;

    goto :goto_0

    .line 327
    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lgi;->y:Ljava/util/ArrayList;

    move-object v4, v0

    .line 328
    :goto_1
    if-nez v4, :cond_3

    .line 344
    :cond_1
    :goto_2
    return-object v1

    .line 327
    :cond_2
    iget-object v0, p0, Lgi;->z:Ljava/util/ArrayList;

    move-object v4, v0

    goto :goto_1

    .line 330
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 331
    const/4 v3, -0x1

    .line 332
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v5, :cond_7

    .line 333
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    .line 334
    if-eqz v0, :cond_1

    .line 336
    iget-object v0, v0, Lgv;->b:Landroid/view/View;

    if-ne v0, p1, :cond_4

    .line 341
    :goto_4
    if-ltz v2, :cond_6

    .line 342
    if-eqz p2, :cond_5

    iget-object v0, p0, Lgi;->z:Ljava/util/ArrayList;

    .line 343
    :goto_5
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgv;

    :goto_6
    move-object v1, v0

    .line 344
    goto :goto_2

    .line 339
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 342
    :cond_5
    iget-object v0, p0, Lgi;->y:Ljava/util/ArrayList;

    goto :goto_5

    :cond_6
    move-object v0, v1

    goto :goto_6

    :cond_7
    move v2, v3

    goto :goto_4
.end method

.method protected b()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 189
    invoke-virtual {p0}, Lgi;->c()V

    .line 190
    invoke-static {}, Lgi;->f()Lpd;

    move-result-object v3

    .line 191
    iget-object v0, p0, Lgi;->F:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    if-ge v2, v4, :cond_4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Landroid/animation/Animator;

    .line 192
    invoke-virtual {v3, v1}, Lpd;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 193
    invoke-virtual {p0}, Lgi;->c()V

    .line 195
    if-eqz v1, :cond_0

    .line 196
    new-instance v5, Lgj;

    invoke-direct {v5, p0, v3}, Lgj;-><init>(Lgi;Lpd;)V

    invoke-virtual {v1, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 198
    if-nez v1, :cond_1

    .line 199
    invoke-virtual {p0}, Lgi;->d()V

    goto :goto_0

    .line 201
    :cond_1
    iget-wide v6, p0, Lgi;->b:J

    .line 202
    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    .line 204
    iget-wide v6, p0, Lgi;->b:J

    .line 205
    invoke-virtual {v1, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 207
    :cond_2
    iget-wide v6, p0, Lgi;->a:J

    .line 208
    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    .line 210
    iget-wide v6, p0, Lgi;->a:J

    .line 211
    invoke-virtual {v1, v6, v7}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 214
    :cond_3
    new-instance v5, Lgk;

    invoke-direct {v5, p0}, Lgk;-><init>(Lgi;)V

    invoke-virtual {v1, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 215
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 217
    :cond_4
    iget-object v0, p0, Lgi;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 218
    invoke-virtual {p0}, Lgi;->d()V

    .line 219
    return-void
.end method

.method public abstract b(Lgv;)V
.end method

.method public c(Landroid/view/View;)Lgi;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lgi;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 223
    return-object p0
.end method

.method protected final c()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 434
    iget v0, p0, Lgi;->B:I

    if-nez v0, :cond_1

    .line 435
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 436
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 437
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 438
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 439
    :goto_0
    if-ge v2, v4, :cond_0

    .line 440
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgn;

    invoke-interface {v1}, Lgn;->c()V

    .line 441
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 442
    :cond_0
    iput-boolean v3, p0, Lgi;->D:Z

    .line 443
    :cond_1
    iget v0, p0, Lgi;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgi;->B:I

    .line 444
    return-void
.end method

.method c(Lgv;)V
    .locals 0

    .prologue
    .line 477
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 510
    invoke-virtual {p0}, Lgi;->e()Lgi;

    move-result-object v0

    return-object v0
.end method

.method protected final d()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 445
    iget v0, p0, Lgi;->B:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgi;->B:I

    .line 446
    iget v0, p0, Lgi;->B:I

    if-nez v0, :cond_5

    .line 447
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 448
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 449
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 450
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 451
    :goto_0
    if-ge v3, v4, :cond_0

    .line 452
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgn;

    invoke-interface {v1, p0}, Lgn;->a(Lgi;)V

    .line 453
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 454
    :goto_1
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0}, Lpj;->b()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 455
    iget-object v0, p0, Lgi;->v:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0, v1}, Lpj;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 456
    if-eqz v0, :cond_1

    .line 457
    invoke-static {v0, v2}, Lqy;->a(Landroid/view/View;Z)V

    .line 458
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 459
    :goto_2
    iget-object v0, p0, Lgi;->w:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0}, Lpj;->b()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 460
    iget-object v0, p0, Lgi;->w:Lgw;

    iget-object v0, v0, Lgw;->c:Lpj;

    invoke-virtual {v0, v1}, Lpj;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 461
    if-eqz v0, :cond_3

    .line 462
    invoke-static {v0, v2}, Lqy;->a(Landroid/view/View;Z)V

    .line 463
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 464
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgi;->D:Z

    .line 465
    :cond_5
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 345
    iget-boolean v0, p0, Lgi;->D:Z

    if-nez v0, :cond_3

    .line 346
    invoke-static {}, Lgi;->f()Lpd;

    move-result-object v2

    .line 347
    invoke-virtual {v2}, Lpd;->size()I

    move-result v0

    .line 348
    invoke-static {p1}, Lhg;->a(Landroid/view/View;)Lhu;

    move-result-object v3

    .line 349
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 350
    invoke-virtual {v2, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    .line 351
    iget-object v4, v0, Lgl;->a:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v0, v0, Lgl;->d:Lhu;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {v2, v1}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 354
    sget-object v4, Lfc;->a:Lfg;

    invoke-interface {v4, v0}, Lfg;->a(Landroid/animation/Animator;)V

    .line 355
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 356
    :cond_1
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 357
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 358
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 359
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 360
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    .line 361
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgn;

    invoke-interface {v1}, Lgn;->a()V

    .line 362
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 363
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgi;->C:Z

    .line 364
    :cond_3
    return-void
.end method

.method public e()Lgi;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 479
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgi;

    .line 480
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lgi;->F:Ljava/util/ArrayList;

    .line 481
    new-instance v2, Lgw;

    invoke-direct {v2}, Lgw;-><init>()V

    iput-object v2, v0, Lgi;->v:Lgw;

    .line 482
    new-instance v2, Lgw;

    invoke-direct {v2}, Lgw;-><init>()V

    iput-object v2, v0, Lgi;->w:Lgw;

    .line 483
    const/4 v2, 0x0

    iput-object v2, v0, Lgi;->y:Ljava/util/ArrayList;

    .line 484
    const/4 v2, 0x0

    iput-object v2, v0, Lgi;->z:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public e(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 365
    iget-boolean v0, p0, Lgi;->C:Z

    if-eqz v0, :cond_3

    .line 366
    iget-boolean v0, p0, Lgi;->D:Z

    if-nez v0, :cond_2

    .line 367
    invoke-static {}, Lgi;->f()Lpd;

    move-result-object v2

    .line 368
    invoke-virtual {v2}, Lpd;->size()I

    move-result v0

    .line 369
    invoke-static {p1}, Lhg;->a(Landroid/view/View;)Lhu;

    move-result-object v4

    .line 370
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 371
    invoke-virtual {v2, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    .line 372
    iget-object v5, v0, Lgl;->a:Landroid/view/View;

    if-eqz v5, :cond_0

    iget-object v0, v0, Lgl;->d:Lhu;

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {v2, v1}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 375
    sget-object v5, Lfc;->a:Lfg;

    invoke-interface {v5, v0}, Lfg;->b(Landroid/animation/Animator;)V

    .line 376
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 378
    iget-object v0, p0, Lgi;->E:Ljava/util/ArrayList;

    .line 379
    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 380
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 381
    :goto_1
    if-ge v2, v4, :cond_2

    .line 382
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgn;

    invoke-interface {v1}, Lgn;->b()V

    .line 383
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 384
    :cond_2
    iput-boolean v3, p0, Lgi;->C:Z

    .line 385
    :cond_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 478
    const-string v0, ""

    invoke-virtual {p0, v0}, Lgi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
