.class final enum Lhbn;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhbn;

.field public static final enum b:Lhbn;

.field public static final enum c:Lhbn;

.field public static final enum d:Lhbn;

.field private static synthetic f:[Lhbn;


# instance fields
.field public final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lhbn;

    const-string v1, "SCALAR"

    invoke-direct {v0, v1, v2, v2}, Lhbn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lhbn;->a:Lhbn;

    .line 6
    new-instance v0, Lhbn;

    const-string v1, "VECTOR"

    invoke-direct {v0, v1, v3, v3}, Lhbn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lhbn;->b:Lhbn;

    .line 7
    new-instance v0, Lhbn;

    const-string v1, "PACKED_VECTOR"

    invoke-direct {v0, v1, v4, v3}, Lhbn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lhbn;->c:Lhbn;

    .line 8
    new-instance v0, Lhbn;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v5, v2}, Lhbn;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lhbn;->d:Lhbn;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lhbn;

    sget-object v1, Lhbn;->a:Lhbn;

    aput-object v1, v0, v2

    sget-object v1, Lhbn;->b:Lhbn;

    aput-object v1, v0, v3

    sget-object v1, Lhbn;->c:Lhbn;

    aput-object v1, v0, v4

    sget-object v1, Lhbn;->d:Lhbn;

    aput-object v1, v0, v5

    sput-object v0, Lhbn;->f:[Lhbn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-boolean p3, p0, Lhbn;->e:Z

    .line 4
    return-void
.end method

.method public static values()[Lhbn;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhbn;->f:[Lhbn;

    invoke-virtual {v0}, [Lhbn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhbn;

    return-object v0
.end method
