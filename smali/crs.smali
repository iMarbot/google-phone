.class public final Lcrs;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field public volatile a:Z

.field private b:Ljava/util/concurrent/BlockingQueue;

.field private c:Lcrr;

.field private d:Lcrj;

.field private e:Lcsc;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Lcrr;Lcrj;Lcsc;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcrs;->a:Z

    .line 3
    iput-object p1, p0, Lcrs;->b:Ljava/util/concurrent/BlockingQueue;

    .line 4
    iput-object p2, p0, Lcrs;->c:Lcrr;

    .line 5
    iput-object p3, p0, Lcrs;->d:Lcrj;

    .line 6
    iput-object p4, p0, Lcrs;->e:Lcsc;

    .line 7
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 8
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 9
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 10
    :try_start_0
    iget-object v0, p0, Lcrs;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcru;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 16
    :try_start_1
    const-string v1, "network-queue-take"

    invoke-virtual {v0, v1}, Lcru;->a(Ljava/lang/String;)V

    .line 17
    invoke-virtual {v0}, Lcru;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    const-string v1, "network-discard-cancelled"

    invoke-virtual {v0, v1}, Lcru;->b(Ljava/lang/String;)V

    .line 19
    invoke-virtual {v0}, Lcru;->i()V
    :try_end_1
    .catch Lcse; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 51
    :catch_0
    move-exception v1

    .line 52
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 57
    iget-object v2, p0, Lcrs;->e:Lcsc;

    invoke-virtual {v2, v0, v1}, Lcsc;->a(Lcru;Lcse;)V

    .line 58
    invoke-virtual {v0}, Lcru;->i()V

    goto :goto_0

    .line 13
    :catch_1
    move-exception v0

    iget-boolean v0, p0, Lcrs;->a:Z

    if-eqz v0, :cond_0

    .line 14
    return-void

    .line 22
    :cond_1
    :try_start_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_2

    .line 24
    iget v1, v0, Lcru;->d:I

    .line 25
    invoke-static {v1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 26
    :cond_2
    iget-object v1, p0, Lcrs;->c:Lcrr;

    invoke-interface {v1, v0}, Lcrr;->a(Lcru;)Lcrt;

    move-result-object v1

    .line 27
    const-string v2, "network-http-complete"

    invoke-virtual {v0, v2}, Lcru;->a(Ljava/lang/String;)V

    .line 28
    iget-boolean v2, v1, Lcrt;->d:Z

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcru;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 29
    const-string v1, "not-modified"

    invoke-virtual {v0, v1}, Lcru;->b(Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0}, Lcru;->i()V
    :try_end_2
    .catch Lcse; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 60
    :catch_2
    move-exception v1

    .line 61
    const-string v2, "Unhandled exception %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcsf;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    new-instance v2, Lcse;

    invoke-direct {v2, v1}, Lcse;-><init>(Ljava/lang/Throwable;)V

    .line 63
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 64
    iget-object v1, p0, Lcrs;->e:Lcsc;

    invoke-virtual {v1, v0, v2}, Lcsc;->a(Lcru;Lcse;)V

    .line 65
    invoke-virtual {v0}, Lcru;->i()V

    goto :goto_0

    .line 32
    :cond_3
    :try_start_3
    invoke-virtual {v0, v1}, Lcru;->a(Lcrt;)Lcrz;

    move-result-object v1

    .line 33
    const-string v2, "network-parse-complete"

    invoke-virtual {v0, v2}, Lcru;->a(Ljava/lang/String;)V

    .line 35
    iget-boolean v2, v0, Lcru;->i:Z

    .line 36
    if-eqz v2, :cond_4

    iget-object v2, v1, Lcrz;->b:Lcrk;

    if-eqz v2, :cond_4

    .line 37
    iget-object v2, p0, Lcrs;->d:Lcrj;

    .line 39
    iget-object v3, v0, Lcru;->c:Ljava/lang/String;

    .line 40
    iget-object v4, v1, Lcrz;->b:Lcrk;

    invoke-interface {v2, v3, v4}, Lcrj;->a(Ljava/lang/String;Lcrk;)V

    .line 41
    const-string v2, "network-cache-written"

    invoke-virtual {v0, v2}, Lcru;->a(Ljava/lang/String;)V

    .line 42
    :cond_4
    invoke-virtual {v0}, Lcru;->g()V

    .line 43
    iget-object v2, p0, Lcrs;->e:Lcsc;

    invoke-virtual {v2, v0, v1}, Lcsc;->a(Lcru;Lcrz;)V

    .line 45
    iget-object v2, v0, Lcru;->e:Ljava/lang/Object;

    monitor-enter v2
    :try_end_3
    .catch Lcse; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 46
    :try_start_4
    iget-object v3, v0, Lcru;->l:Lcrw;

    .line 47
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 48
    if-eqz v3, :cond_0

    .line 49
    :try_start_5
    invoke-interface {v3, v0, v1}, Lcrw;->a(Lcru;Lcrz;)V
    :try_end_5
    .catch Lcse; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 47
    :catchall_0
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v1
    :try_end_7
    .catch Lcse; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
.end method
