.class public Lbvs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(F)F
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 101
    const/4 v0, 0x0

    .line 102
    float-to-double v2, p0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    .line 104
    :cond_0
    float-to-double v2, p0

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 105
    add-float/2addr v0, v1

    .line 106
    :cond_1
    float-to-double v2, p0

    const-wide v4, 0x3fe6666666666666L    # 0.7

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    .line 107
    add-float/2addr v0, v1

    .line 108
    :cond_2
    return v0
.end method

.method public static a(FF)F
    .locals 2

    .prologue
    .line 109
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/high16 v0, 0x40b00000    # 5.5f

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(I)F
    .locals 2

    .prologue
    .line 110
    add-int/lit8 v0, p0, -0x1

    add-int/lit8 v1, p0, -0x1

    mul-int/2addr v0, v1

    int-to-float v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Lalj;
    .locals 1

    .prologue
    .line 47
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Lalj;

    invoke-direct {v0, p0}, Lalj;-><init>(Landroid/content/Context;)V

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)Lbzg;
    .locals 1

    .prologue
    .line 92
    invoke-static {p0}, Lbvs;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Lbzv;

    invoke-direct {v0}, Lbzv;-><init>()V

    .line 94
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbzi;

    invoke-direct {v0}, Lbzi;-><init>()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZZZZZ)Lcbt;
    .locals 1

    .prologue
    .line 91
    invoke-static/range {p0 .. p5}, Lbxn;->a(Ljava/lang/String;ZZZZZ)Lbxn;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcgp;Lcgq;)Lcer;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 133
    iget v2, p1, Lcgp;->a:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    move v2, v0

    .line 134
    :goto_0
    iget-boolean v6, p1, Lcgp;->o:Z

    .line 135
    iget-boolean v3, p1, Lcgp;->l:Z

    .line 136
    iget-boolean v4, p1, Lcgp;->n:Z

    if-eqz v4, :cond_1

    if-nez v6, :cond_1

    move v5, v0

    .line 137
    :goto_1
    iget-boolean v4, p1, Lcgp;->m:Z

    .line 140
    invoke-static {p1}, Lbvs;->a(Lcgp;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p2, Lcgq;->h:Z

    if-eqz v7, :cond_2

    .line 141
    const v5, 0x7f1100ee

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move v8, v0

    move v7, v0

    move v9, v1

    move-object v1, v5

    move v5, v9

    .line 152
    :goto_2
    new-instance v0, Lcer;

    invoke-direct/range {v0 .. v8}, Lcer;-><init>(Ljava/lang/CharSequence;ZZZZZZZ)V

    return-object v0

    :cond_0
    move v2, v1

    .line 133
    goto :goto_0

    :cond_1
    move v5, v1

    .line 136
    goto :goto_1

    .line 144
    :cond_2
    iget v7, p1, Lcgp;->a:I

    const/16 v8, 0x9

    if-ne v7, v8, :cond_3

    .line 145
    const v7, 0x7f1101b4

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move v8, v0

    move v9, v1

    move-object v1, v7

    move v7, v9

    goto :goto_2

    .line 146
    :cond_3
    iget v7, p1, Lcgp;->a:I

    const/16 v8, 0xa

    if-ne v7, v8, :cond_4

    .line 147
    iget-object v7, p1, Lcgp;->d:Landroid/telecom/DisconnectCause;

    invoke-virtual {v7}, Landroid/telecom/DisconnectCause;->getLabel()Ljava/lang/CharSequence;

    move-result-object v7

    .line 148
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 149
    const v7, 0x7f11019f

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move v8, v0

    move v9, v1

    move-object v1, v7

    move v7, v9

    goto :goto_2

    .line 150
    :cond_4
    invoke-static {p2}, Lbvs;->a(Lcgq;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 151
    iget-boolean v0, p2, Lcgq;->c:Z

    move v8, v0

    move v9, v1

    move-object v1, v7

    move v7, v9

    goto :goto_2

    :cond_5
    move v8, v0

    move v9, v1

    move-object v1, v7

    move v7, v9

    goto :goto_2
.end method

.method public static a(IZI)Lcfe;
    .locals 1

    .prologue
    .line 165
    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    if-eqz p1, :cond_1

    .line 166
    :cond_0
    invoke-static {}, Lbvs;->b()Lcfe;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    .line 167
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 168
    invoke-static {}, Lbvs;->c()Lcfe;

    move-result-object v0

    goto :goto_0

    .line 169
    :cond_2
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 170
    invoke-static {}, Lbvs;->d()Lcfe;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_3
    invoke-static {}, Lbvs;->b()Lcfe;

    move-result-object v0

    goto :goto_0
.end method

.method public static a()Lcgm;
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcfx;

    invoke-direct {v0}, Lcfx;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcjk;
    .locals 1

    .prologue
    .line 245
    if-eqz p1, :cond_0

    .line 246
    invoke-static {p0}, Lcim;->b(Ljava/lang/String;)Lcim;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lciy;->b(Ljava/lang/String;)Lciy;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Z)Lcjr;
    .locals 2

    .prologue
    .line 248
    new-instance v0, Lcjn;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcjn;-><init>(ZI)V

    return-object v0
.end method

.method public static a(Lcgq;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcgq;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcgq;->e:Ljava/lang/String;

    .line 160
    :cond_0
    :goto_0
    return-object v0

    .line 155
    :cond_1
    iget-boolean v0, p0, Lcgq;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcgq;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    iget-object v0, p0, Lcgq;->a:Ljava/lang/String;

    invoke-static {v0}, Lbvs;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcgq;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 159
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcgq;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, " "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 161
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    sget-object v1, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, p0, v1}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->createTtsSpannable(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/telephony/VisualVoicemailSmsFilterSettings;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 410
    :try_start_0
    const-class v0, Landroid/telephony/TelephonyManager;

    const-string v1, "setVisualVoicemailSmsFilterSettings"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/telephony/VisualVoicemailSmsFilterSettings;

    aput-object v4, v2, v3

    .line 411
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 412
    :try_start_1
    const-string v0, "TelephonyMangerCompat.setVisualVoicemailSmsFilterSettings"

    const-string v2, "using TelephonyManager"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 414
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 415
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 416
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 423
    :goto_0
    return-object v0

    .line 417
    :catch_0
    move-exception v0

    .line 418
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    .line 420
    :try_start_3
    const-string v0, "TelephonyMangerCompat.setVisualVoicemailSmsFilterSettings"

    const-string v1, "using VisualVoicemailService"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 421
    const-class v0, Landroid/telephony/VisualVoicemailService;

    const-string v1, "setSmsFilterSettings"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/telecom/PhoneAccountHandle;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Landroid/telephony/VisualVoicemailSmsFilterSettings;

    aput-object v4, v2, v3

    .line 422
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 423
    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 424
    :catch_2
    move-exception v0

    .line 425
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 392
    :try_start_0
    const-class v0, Landroid/telephony/TelephonyManager;

    const-string v1, "sendVisualVoicemailSms"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Landroid/app/PendingIntent;

    aput-object v4, v2, v3

    .line 393
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 394
    :try_start_1
    const-string v0, "TelephonyMangerCompat.sendVisualVoicemailSms"

    const-string v2, "using TelephonyManager"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 395
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 396
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 397
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 398
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 407
    :goto_0
    return-object v0

    .line 399
    :catch_0
    move-exception v0

    .line 400
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    .line 402
    :try_start_3
    const-string v0, "TelephonyMangerCompat.sendVisualVoicemailSms"

    const-string v1, "using VisualVoicemailService"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 403
    const-class v0, Landroid/telephony/VisualVoicemailService;

    const-string v1, "sendVisualVoicemailSms"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/telecom/PhoneAccountHandle;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-class v4, Landroid/app/PendingIntent;

    aput-object v4, v2, v3

    .line 404
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 405
    const/4 v1, 0x0

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    int-to-short v4, p3

    .line 406
    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p4, v2, v3

    const/4 v3, 0x5

    aput-object p5, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 408
    :catch_2
    move-exception v0

    .line 409
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a([B)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->update([B)V

    .line 80
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 81
    invoke-static {v0}, Lbvs;->b([B)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcah;)Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2
    .line 3
    :try_start_0
    iget-object v0, p1, Lcah;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p1, Lcah;->c:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 7
    :goto_0
    if-eqz v2, :cond_7

    .line 8
    :try_start_1
    iget-object v0, p1, Lcah;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p1, Lcah;->d:Landroid/graphics/drawable/Drawable;

    .line 9
    iget-object v3, p1, Lcah;->b:Landroid/content/Context;

    iget-object v0, p1, Lcah;->d:Landroid/graphics/drawable/Drawable;

    .line 10
    instance-of v4, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v4, :cond_3

    move-object v0, v1

    .line 26
    :cond_0
    :goto_1
    iput-object v0, p1, Lcah;->e:Landroid/graphics/Bitmap;

    .line 27
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcah;->c:Landroid/net/Uri;

    aput-object v4, v0, v3

    .line 31
    :goto_2
    iget-object v0, p1, Lcah;->g:Lbvr;

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p1, Lcah;->g:Lbvr;

    iget-object v3, p1, Lcah;->d:Landroid/graphics/drawable/Drawable;

    iget-object v4, p1, Lcah;->e:Landroid/graphics/Bitmap;

    iget-object v5, p1, Lcah;->f:Ljava/lang/Object;

    invoke-interface {v0, v3, v4, v5}, Lbvr;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33
    :cond_1
    if-eqz v2, :cond_2

    .line 34
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 45
    :cond_2
    :goto_3
    return-object v1

    .line 5
    :catch_0
    move-exception v0

    .line 6
    :try_start_3
    const-string v2, "ContactsAsyncHelper.Worker.doInBackground"

    const-string v3, "error opening photo input stream"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v2, v1

    goto :goto_0

    .line 12
    :cond_3
    :try_start_4
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d018f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 13
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 15
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    .line 16
    if-le v4, v5, :cond_5

    move v3, v4

    .line 17
    :goto_4
    if-le v3, v6, :cond_0

    .line 18
    int-to-float v3, v3

    int-to-float v6, v6

    div-float/2addr v3, v6

    .line 19
    int-to-float v4, v4

    div-float/2addr v4, v3

    float-to-int v4, v4

    .line 20
    int-to-float v5, v5

    div-float v3, v5, v3

    float-to-int v3, v3

    .line 21
    if-lez v4, :cond_4

    if-gtz v3, :cond_6

    .line 22
    :cond_4
    const-string v0, "ContactsAsyncHelper.Worker.getPhotoIconWhenAppropriate"

    const-string v3, "Photo icon\'s width or height become 0."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 23
    goto :goto_1

    :cond_5
    move v3, v5

    .line 16
    goto :goto_4

    .line 24
    :cond_6
    const/4 v5, 0x1

    invoke-static {v0, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 28
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p1, Lcah;->d:Landroid/graphics/drawable/Drawable;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p1, Lcah;->e:Landroid/graphics/Bitmap;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcah;->c:Landroid/net/Uri;

    aput-object v4, v0, v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 39
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_5
    if-eqz v1, :cond_8

    .line 40
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 44
    :cond_8
    :goto_6
    throw v0

    .line 36
    :catch_1
    move-exception v0

    .line 37
    const-string v2, "ContactsAsyncHelper.Worker.doInBackground"

    const-string v3, "Unable to close input stream."

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 42
    :catch_2
    move-exception v1

    .line 43
    const-string v2, "ContactsAsyncHelper.Worker.doInBackground"

    const-string v3, "Unable to close input stream."

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 39
    :catchall_1
    move-exception v0

    goto :goto_5
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 5

    .prologue
    .line 357
    invoke-static {}, Lbvs;->g()V

    .line 358
    new-instance v0, Lcly;

    invoke-direct {v0, p0, p1}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 359
    const-string v1, "pre_o_migration_finished"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcly;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    const-string v0, "PreOMigrationHandler"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already migrated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :goto_0
    return-void

    .line 362
    :cond_0
    const-string v1, "PreOMigrationHandler"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "migrating "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    invoke-static {p0, p1}, Lbvs;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 364
    invoke-virtual {v0}, Lcly;->a()Lbdh;

    move-result-object v0

    const-string v1, "pre_o_migration_finished"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbdh;->a(Ljava/lang/String;Z)Lbdh;

    move-result-object v0

    invoke-virtual {v0}, Lbdh;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lclu;Lcnw;Lclt;)V
    .locals 5

    .prologue
    .line 265
    .line 266
    iget v0, p3, Lclt;->Q:I

    .line 267
    packed-switch v0, :pswitch_data_0

    .line 276
    const-string v0, "DefErrorCodeHandler"

    .line 277
    iget v1, p3, Lclt;->Q:I

    .line 278
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid event type "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :goto_0
    return-void

    .line 268
    :pswitch_0
    invoke-static {p2, p3}, Lbvs;->a(Lcnw;Lclt;)V

    goto :goto_0

    .line 270
    :pswitch_1
    invoke-static {p2, p3}, Lbvs;->b(Lcnw;Lclt;)V

    goto :goto_0

    .line 272
    :pswitch_2
    invoke-static {p1, p2, p3}, Lbvs;->a(Lclu;Lcnw;Lclt;)V

    goto :goto_0

    .line 274
    :pswitch_3
    invoke-static {p2, p3}, Lbvs;->c(Lcnw;Lclt;)V

    goto :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/view/TextureView;FFF)V
    .locals 0

    .prologue
    .line 250
    invoke-static {p0, p1, p2, p3}, Lbvw;->a(Landroid/view/TextureView;FFF)V

    .line 251
    return-void
.end method

.method public static a(Landroid/view/TextureView;II)V
    .locals 0

    .prologue
    .line 252
    invoke-static {p0, p1, p2}, Lbvw;->a(Landroid/view/TextureView;II)V

    .line 253
    return-void
.end method

.method public static a(Lclu;Lcnw;Lclt;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 335
    invoke-virtual {p2}, Lclt;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 346
    const-string v0, "DefErrorCodeHandler"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x23

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid notification channel event "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :goto_0
    return-void

    .line 337
    :pswitch_0
    invoke-virtual {p1, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 338
    invoke-virtual {v0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 341
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcnw;->c(I)Lcnw;

    .line 342
    invoke-virtual {p0}, Lclu;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcnw;->b(I)Lcnw;

    .line 344
    :cond_0
    invoke-virtual {p1}, Lcnw;->a()Z

    goto :goto_0

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcnw;Lclt;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 280
    invoke-virtual {p1}, Lclt;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 302
    const-string v0, "DefErrorCodeHandler"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid configuration event "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :goto_0
    return-void

    .line 282
    :pswitch_0
    invoke-virtual {p0, v1}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    .line 283
    invoke-virtual {v0, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 286
    :pswitch_1
    const/4 v0, 0x3

    .line 287
    invoke-virtual {p0, v0}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    .line 288
    invoke-virtual {v0, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 289
    invoke-virtual {v0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 290
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 293
    :pswitch_2
    invoke-virtual {p0, v1}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    .line 294
    invoke-virtual {v0, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 295
    invoke-virtual {v0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 298
    :pswitch_3
    invoke-virtual {p0, v2}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 300
    :pswitch_4
    invoke-virtual {p0, v2}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 224
    if-eqz p0, :cond_0

    .line 226
    iput-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    .line 227
    invoke-virtual {p0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    .line 51
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 52
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, p1, v0}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0, p1, p2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    return-void
.end method

.method public static a(Landroid/telecom/Call;)Z
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v0

    .line 237
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcgp;)Z
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lcgp;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcgp;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lip;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 95
    if-nez p0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p0, Lbzv;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v1

    invoke-static {v1}, Lbvs;->b(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Lcfe;
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x5

    .line 172
    invoke-static {}, Lbvs;->e()Ljava/util/Map;

    move-result-object v0

    .line 173
    const/16 v1, 0xc

    .line 174
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 175
    invoke-static {v5}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 176
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 179
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const/16 v1, 0xd

    .line 181
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 182
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    new-instance v1, Lcfe;

    new-instance v2, Lcgd;

    invoke-direct {v2, v0}, Lcgd;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2}, Lcfe;-><init>(Lcgd;)V

    return-object v1
.end method

.method public static b(Z)Lcjr;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lcjn;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcjn;-><init>(ZI)V

    return-object v0
.end method

.method public static b(Landroid/telecom/Call;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 238
    if-nez p0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-object v0

    .line 240
    :cond_1
    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telecom/Call$Details;->getGatewayInfo()Landroid/telecom/GatewayInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 241
    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getGatewayInfo()Landroid/telecom/GatewayInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/GatewayInfo;->getOriginalAddress()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :cond_2
    invoke-static {p0}, Lbvs;->c(Landroid/telecom/Call;)Landroid/net/Uri;

    move-result-object v1

    .line 243
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 72
    if-eqz p0, :cond_0

    invoke-static {}, Lapw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lbvs;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "["

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static b([B)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 82
    new-instance v1, Ljava/lang/StringBuffer;

    array-length v0, p0

    shl-int/lit8 v0, v0, 0x1

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 83
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 84
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    .line 85
    if-ge v2, v4, :cond_0

    .line 86
    const-string v3, "0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 87
    :cond_0
    invoke-static {v2, v4}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 5

    .prologue
    .line 366
    const-string v0, "PreOMigrationHandler.migrateSettings"

    const-string v1, "migrating settings"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 368
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 369
    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 370
    if-nez v0, :cond_1

    .line 371
    const-string v0, "PreOMigrationHandler.migrateSettings"

    const-string v1, "invalid PhoneAccountHandle"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    :try_start_0
    const-class v1, Landroid/telephony/TelephonyManager;

    const-string v2, "getVisualVoicemailSettings"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 374
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ReflectiveOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    const-string v1, "android.telephony.extra.VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 380
    const-string v1, "android.telephony.extra.VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 381
    const-string v2, "PreOMigrationHandler.migrateSettings"

    const/16 v3, 0x1c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "setting VVM enabled to "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-static {p0, p1, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 383
    :cond_2
    const-string v1, "android.telephony.extra.VOICEMAIL_SCRAMBLED_PIN_STRING"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    const-string v1, "android.telephony.extra.VOICEMAIL_SCRAMBLED_PIN_STRING"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 385
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 386
    const-string v1, "PreOMigrationHandler.migrateSettings"

    const-string v2, "migrating scrambled PIN"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v1

    .line 388
    invoke-virtual {v1}, Lclp;->a()Lcln;

    move-result-object v1

    .line 389
    invoke-interface {v1, p0, p1}, Lcln;->g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;

    move-result-object v1

    .line 390
    invoke-virtual {v1, v0}, Lcll;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 377
    :catch_0
    move-exception v0

    :goto_1
    const-string v0, "PreOMigrationHandler.migrateSettings"

    const-string v1, "unable to retrieve settings from system"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static b(Lcnw;Lclt;)V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 304
    invoke-virtual {p1}, Lclt;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 333
    const-string v0, "DefErrorCodeHandler"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid data channel event "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :goto_0
    return-void

    .line 305
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 307
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 309
    :pswitch_2
    const/4 v0, 0x2

    .line 310
    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 314
    :pswitch_3
    invoke-virtual {p0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 315
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 317
    :pswitch_4
    const/4 v0, 0x6

    .line 318
    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 319
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 321
    :pswitch_5
    const/4 v0, 0x4

    .line 322
    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 326
    :pswitch_6
    invoke-virtual {p0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 329
    :pswitch_7
    const/4 v0, 0x5

    .line 330
    invoke-virtual {p0, v0}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 331
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 304
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public static synthetic b(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V
    .locals 0

    .prologue
    .line 229
    invoke-static {p0}, Lbvs;->a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    return-void
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    .line 55
    return-void
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 56
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, p1, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 111
    packed-switch p0, :pswitch_data_0

    .line 113
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 112
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lbvs;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-static {p0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    .line 99
    :cond_1
    const/4 v0, 0x0

    .line 100
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 230
    const-class v0, Landroid/view/accessibility/AccessibilityManager;

    .line 231
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 232
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/telecom/Call;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 244
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static c()Lcfe;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x5

    .line 185
    invoke-static {}, Lbvs;->e()Ljava/util/Map;

    move-result-object v0

    .line 186
    const/16 v1, 0xc

    .line 187
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 188
    invoke-static {v5}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 189
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 192
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const/16 v1, 0xd

    .line 196
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 197
    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    const v3, 0x7fffffff

    .line 198
    invoke-virtual {v2, v3}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    .line 199
    invoke-virtual {v2, v5}, Lcgh;->c(I)Lcgh;

    move-result-object v2

    .line 200
    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 201
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    new-instance v1, Lcfe;

    new-instance v2, Lcgd;

    invoke-direct {v2, v0}, Lcgd;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2}, Lcfe;-><init>(Lcgd;)V

    return-object v1
.end method

.method static c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcnw;Lclt;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 348
    invoke-virtual {p1}, Lclt;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 355
    const-string v0, "DefErrorCodeHandler"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid other event "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :goto_0
    return-void

    .line 350
    :pswitch_0
    invoke-virtual {p0, v1}, Lcnw;->a(I)Lcnw;

    move-result-object v0

    .line 351
    invoke-virtual {v0, v1}, Lcnw;->c(I)Lcnw;

    move-result-object v0

    .line 352
    invoke-virtual {v0, v1}, Lcnw;->b(I)Lcnw;

    move-result-object v0

    .line 353
    invoke-virtual {v0}, Lcnw;->a()Z

    goto :goto_0

    .line 348
    nop

    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method public static c(Z)V
    .locals 2

    .prologue
    .line 258
    if-nez p0, :cond_0

    .line 259
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Expected condition to be true"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 260
    :cond_0
    return-void
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/16 v0, 0xf

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 233
    const-class v0, Landroid/view/accessibility/AccessibilityManager;

    .line 234
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 235
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    return v0
.end method

.method public static d()Lcfe;
    .locals 6

    .prologue
    const/4 v3, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x5

    .line 203
    invoke-static {}, Lbvs;->e()Ljava/util/Map;

    move-result-object v0

    .line 204
    const/16 v1, 0xd

    .line 205
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 206
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 209
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const/16 v1, 0xc

    .line 211
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 212
    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    .line 213
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    new-instance v1, Lcfe;

    new-instance v2, Lcgd;

    invoke-direct {v2, v0}, Lcgd;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v2}, Lcfe;-><init>(Lcgd;)V

    return-object v1
.end method

.method public static d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    packed-switch p0, :pswitch_data_0

    .line 132
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 116
    :pswitch_0
    const-string v0, "INVALID"

    goto :goto_0

    .line 117
    :pswitch_1
    const-string v0, "NEW"

    goto :goto_0

    .line 118
    :pswitch_2
    const-string v0, "IDLE"

    goto :goto_0

    .line 119
    :pswitch_3
    const-string v0, "ACTIVE"

    goto :goto_0

    .line 120
    :pswitch_4
    const-string v0, "INCOMING"

    goto :goto_0

    .line 121
    :pswitch_5
    const-string v0, "CALL_WAITING"

    goto :goto_0

    .line 122
    :pswitch_6
    const-string v0, "DIALING"

    goto :goto_0

    .line 123
    :pswitch_7
    const-string v0, "PULLING"

    goto :goto_0

    .line 124
    :pswitch_8
    const-string v0, "REDIALING"

    goto :goto_0

    .line 125
    :pswitch_9
    const-string v0, "ONHOLD"

    goto :goto_0

    .line 126
    :pswitch_a
    const-string v0, "DISCONNECTING"

    goto :goto_0

    .line 127
    :pswitch_b
    const-string v0, "DISCONNECTED"

    goto :goto_0

    .line 128
    :pswitch_c
    const-string v0, "CONFERENCED"

    goto :goto_0

    .line 129
    :pswitch_d
    const-string v0, "SELECT_PHONE_ACCOUNT"

    goto :goto_0

    .line 130
    :pswitch_e
    const-string v0, "CONNECTING"

    goto :goto_0

    .line 131
    :pswitch_f
    const-string v0, "BLOCKED"

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_7
    .end packed-switch
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 256
    invoke-static {p0}, Lbsw;->k(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lbvs;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Ljava/util/Map;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 216
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 217
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcgh;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v6}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Lcgg;->a(I)Lcgh;

    move-result-object v2

    invoke-virtual {v2}, Lcgh;->a()Lcgg;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    return-object v0
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    invoke-static {p0}, Lbvs;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public static e(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 254
    if-eq p0, v0, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 257
    const-string v0, "android.permission.CAMERA"

    invoke-static {p0, v0}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 261
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbvs;->c(Z)V

    .line 262
    return-void
.end method

.method public static f(I)Z
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()V
    .locals 2

    .prologue
    .line 263
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbvs;->c(Z)V

    .line 264
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lcah;

    invoke-direct {p0, p1}, Lbvs;->a(Lcah;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
