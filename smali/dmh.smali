.class final Ldmh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Ldml;

.field private b:J

.field private c:Lbln;


# direct methods
.method constructor <init>(Ldml;JLbln;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldml;

    iput-object v0, p0, Ldmh;->a:Ldml;

    .line 3
    iput-wide p2, p0, Ldmh;->b:J

    .line 4
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbln;

    iput-object v0, p0, Ldmh;->c:Lbln;

    .line 5
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 6
    .line 7
    iget-object v0, p0, Ldmh;->a:Ldml;

    invoke-virtual {v0}, Ldml;->a()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;

    move-result-object v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 15
    :goto_0
    return-object v0

    .line 10
    :cond_0
    iget-wide v2, p0, Ldmh;->b:J

    new-instance v1, Lcom/google/android/rcs/client/enrichedcall/CallComposerData;

    iget-object v4, p0, Ldmh;->c:Lbln;

    .line 11
    invoke-virtual {v4}, Lbln;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Ldmh;->c:Lbln;

    invoke-virtual {v5}, Lbln;->c()Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Ldmh;->c:Lbln;

    invoke-virtual {v6}, Lbln;->d()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v4, v5, v6}, Lcom/google/android/rcs/client/enrichedcall/CallComposerData;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 12
    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;->sendCallComposerData(JLcom/google/android/rcs/client/enrichedcall/CallComposerData;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    move-result-object v0

    .line 13
    const-string v1, "SendCallComposerDataWorker.sendCallComposerDataBackground"

    const-string v2, "Result of sendCallComposerData: %s"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->getCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
