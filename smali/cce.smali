.class public final Lcce;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcce;


# instance fields
.field public b:Landroid/telecom/CallAudioState;

.field private c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcce;

    invoke-direct {v0}, Lcce;-><init>()V

    sput-object v0, Lcce;->a:Lcce;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcce;->c:Ljava/util/List;

    .line 3
    new-instance v0, Landroid/telecom/CallAudioState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Landroid/telecom/CallAudioState;-><init>(ZII)V

    iput-object v0, p0, Lcce;->b:Landroid/telecom/CallAudioState;

    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 16
    const-class v0, Landroid/media/AudioManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 19
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getDevices(I)[Landroid/media/AudioDeviceInfo;

    move-result-object v6

    array-length v7, v6

    move v4, v5

    move v0, v5

    move v2, v5

    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v8, v6, v4

    .line 20
    invoke-virtual {v8}, Landroid/media/AudioDeviceInfo;->getType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 24
    :goto_1
    :pswitch_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :pswitch_1
    move v2, v1

    .line 22
    goto :goto_1

    :pswitch_2
    move v0, v1

    .line 23
    goto :goto_1

    .line 25
    :cond_0
    if-eqz v2, :cond_1

    .line 26
    const-string v0, "AudioModeProvider.getApproximatedAudioRoute"

    const-string v1, "Routing to bluetooth"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    .line 32
    :goto_2
    return v1

    .line 28
    :cond_1
    if-eqz v0, :cond_2

    .line 29
    const-string v0, "AudioModeProvider.getApproximatedAudioRoute"

    const-string v1, "Routing to headset"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    const/4 v1, 0x4

    goto :goto_2

    .line 31
    :cond_2
    const-string v0, "AudioModeProvider.getApproximatedAudioRoute"

    const-string v2, "Routing to earpiece"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lcce;->b:Landroid/telecom/CallAudioState;

    invoke-virtual {v0, p1}, Landroid/telecom/CallAudioState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    iput-object p1, p0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 6
    iget-object v0, p0, Lcce;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccf;

    .line 7
    invoke-interface {v0, p1}, Lccf;->a(Landroid/telecom/CallAudioState;)V

    goto :goto_0

    .line 9
    :cond_0
    return-void
.end method

.method public final a(Lccf;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcce;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11
    iget-object v0, p0, Lcce;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    iget-object v0, p0, Lcce;->b:Landroid/telecom/CallAudioState;

    invoke-interface {p1, v0}, Lccf;->a(Landroid/telecom/CallAudioState;)V

    .line 13
    :cond_0
    return-void
.end method

.method public final b(Lccf;)V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcce;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 15
    return-void
.end method
