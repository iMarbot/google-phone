.class public final Laky;
.super Laks;
.source "PG"


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Laks;-><init>(Landroid/content/ContentValues;)V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Laky;->c:Z

    .line 3
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4
    .line 5
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    .line 6
    const-string v1, "data1"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 7
    .line 8
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    .line 9
    const-string v1, "data5"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Laky;->c()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11
    .line 12
    iget-object v0, p0, Laks;->a:Landroid/content/ContentValues;

    .line 13
    const-string v1, "data6"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Laks;Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 14
    instance-of v2, p1, Laky;

    if-eqz v2, :cond_0

    iget-object v2, p0, Laky;->b:Lakt;

    if-eqz v2, :cond_0

    .line 15
    iget-object v2, p1, Laks;->b:Lakt;

    .line 16
    if-nez v2, :cond_1

    .line 32
    :cond_0
    :goto_0
    return v0

    .line 18
    :cond_1
    check-cast p1, Laky;

    .line 19
    invoke-direct {p0}, Laky;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1}, Laky;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 21
    invoke-direct {p0}, Laky;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p1}, Laky;->d()Z

    move-result v2

    if-nez v2, :cond_5

    .line 22
    :cond_2
    invoke-direct {p0}, Laky;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 23
    invoke-direct {p0}, Laky;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 24
    :cond_3
    invoke-direct {p1}, Laky;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 25
    invoke-direct {p1}, Laky;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 26
    goto :goto_0

    .line 27
    :cond_5
    invoke-direct {p0}, Laky;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p1}, Laky;->c()Ljava/lang/Integer;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 29
    invoke-direct {p0}, Laky;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_6

    .line 30
    invoke-direct {p0}, Laky;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1}, Laky;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    .line 32
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 33
    check-cast p1, Laks;

    invoke-virtual {p0, p1, p2}, Laky;->a(Laks;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
