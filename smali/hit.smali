.class public final Lhit;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final i:Lhit;

.field private static volatile j:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lhit;

    invoke-direct {v0}, Lhit;-><init>()V

    .line 113
    sput-object v0, Lhit;->i:Lhit;

    invoke-virtual {v0}, Lhit;->makeImmutable()V

    .line 114
    const-class v0, Lhit;

    sget-object v1, Lhit;->i:Lhit;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 115
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 109
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "h"

    aput-object v2, v0, v1

    .line 110
    const-string v1, "\u0001\u0007\u0000\u0001\u0001\u0007\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0004\u0001\u0003\u0004\u0002\u0004\u0004\u0003\u0005\u0004\u0004\u0006\u0004\u0005\u0007\u0004\u0006"

    .line 111
    sget-object v2, Lhit;->i:Lhit;

    invoke-static {v2, v1, v0}, Lhit;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 52
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 108
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 53
    :pswitch_0
    new-instance v0, Lhit;

    invoke-direct {v0}, Lhit;-><init>()V

    .line 107
    :goto_0
    :pswitch_1
    return-object v0

    .line 54
    :pswitch_2
    sget-object v0, Lhit;->i:Lhit;

    goto :goto_0

    .line 56
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[[S)V

    move-object v0, v1

    goto :goto_0

    .line 57
    :pswitch_4
    check-cast p2, Lhaq;

    .line 58
    check-cast p3, Lhbg;

    .line 59
    if-nez p3, :cond_0

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61
    :cond_0
    :try_start_0
    sget-boolean v0, Lhit;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {p0, p2, p3}, Lhit;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 63
    sget-object v0, Lhit;->i:Lhit;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 65
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 66
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 67
    sparse-switch v2, :sswitch_data_0

    .line 70
    invoke-virtual {p0, v2, p2}, Lhit;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 71
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 69
    goto :goto_1

    .line 72
    :sswitch_1
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhit;->a:I

    .line 73
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->b:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 94
    :catch_0
    move-exception v0

    .line 95
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    throw v0

    .line 75
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhit;->a:I

    .line 76
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->c:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 96
    :catch_1
    move-exception v0

    .line 97
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 98
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 78
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhit;->a:I

    .line 79
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->d:I

    goto :goto_1

    .line 81
    :sswitch_4
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lhit;->a:I

    .line 82
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->e:I

    goto :goto_1

    .line 84
    :sswitch_5
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lhit;->a:I

    .line 85
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->f:I

    goto :goto_1

    .line 87
    :sswitch_6
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lhit;->a:I

    .line 88
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->g:I

    goto :goto_1

    .line 90
    :sswitch_7
    iget v2, p0, Lhit;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lhit;->a:I

    .line 91
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhit;->h:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 100
    :cond_3
    :pswitch_5
    sget-object v0, Lhit;->i:Lhit;

    goto/16 :goto_0

    .line 101
    :pswitch_6
    sget-object v0, Lhit;->j:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lhit;

    monitor-enter v1

    .line 102
    :try_start_5
    sget-object v0, Lhit;->j:Lhdm;

    if-nez v0, :cond_4

    .line 103
    new-instance v0, Lhaa;

    sget-object v2, Lhit;->i:Lhit;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhit;->j:Lhdm;

    .line 104
    :cond_4
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 105
    :cond_5
    sget-object v0, Lhit;->j:Lhdm;

    goto/16 :goto_0

    .line 104
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 106
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 22
    iget v0, p0, Lhit;->memoizedSerializedSize:I

    .line 23
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 51
    :goto_0
    return v0

    .line 24
    :cond_0
    sget-boolean v0, Lhit;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 25
    invoke-virtual {p0}, Lhit;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhit;->memoizedSerializedSize:I

    .line 26
    iget v0, p0, Lhit;->memoizedSerializedSize:I

    goto :goto_0

    .line 27
    :cond_1
    const/4 v0, 0x0

    .line 28
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 29
    iget v0, p0, Lhit;->b:I

    .line 30
    invoke-static {v2, v0}, Lhaw;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31
    :cond_2
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 32
    iget v1, p0, Lhit;->c:I

    .line 33
    invoke-static {v3, v1}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_3
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 35
    const/4 v1, 0x3

    iget v2, p0, Lhit;->d:I

    .line 36
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    :cond_4
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 38
    iget v1, p0, Lhit;->e:I

    .line 39
    invoke-static {v4, v1}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    :cond_5
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 41
    const/4 v1, 0x5

    iget v2, p0, Lhit;->f:I

    .line 42
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_6
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 44
    const/4 v1, 0x6

    iget v2, p0, Lhit;->g:I

    .line 45
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_7
    iget v1, p0, Lhit;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 47
    const/4 v1, 0x7

    iget v2, p0, Lhit;->h:I

    .line 48
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_8
    iget-object v1, p0, Lhit;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    iput v0, p0, Lhit;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lhit;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhit;->writeToInternal(Lhaw;)V

    .line 21
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 7
    iget v0, p0, Lhit;->b:I

    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 8
    :cond_1
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 9
    iget v0, p0, Lhit;->c:I

    invoke-virtual {p1, v2, v0}, Lhaw;->b(II)V

    .line 10
    :cond_2
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 11
    const/4 v0, 0x3

    iget v1, p0, Lhit;->d:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 12
    :cond_3
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 13
    iget v0, p0, Lhit;->e:I

    invoke-virtual {p1, v3, v0}, Lhaw;->b(II)V

    .line 14
    :cond_4
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 15
    const/4 v0, 0x5

    iget v1, p0, Lhit;->f:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 16
    :cond_5
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 17
    const/4 v0, 0x6

    iget v1, p0, Lhit;->g:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 18
    :cond_6
    iget v0, p0, Lhit;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 19
    const/4 v0, 0x7

    iget v1, p0, Lhit;->h:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 20
    :cond_7
    iget-object v0, p0, Lhit;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
