.class public final Lclx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Ljava/util/Map;


# instance fields
.field public final a:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lclx;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    invoke-static {p1, v0}, Lclx;->a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lclx;->b:Ljava/util/Map;

    .line 4
    :cond_0
    sget-object v0, Lclx;->b:Ljava/util/Map;

    iput-object v0, p0, Lclx;->a:Ljava/util/Map;

    .line 5
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 26
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 27
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    if-eq v1, v3, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 28
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    if-ge v2, v0, :cond_2

    .line 29
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 30
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 31
    new-array v1, v3, [Ljava/lang/String;

    .line 32
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 33
    new-instance v2, Lcrg;

    invoke-direct {v2}, Lcrg;-><init>()V

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;[Ljava/lang/String;Lcrg;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Map;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 6
    new-instance v5, Landroid/util/ArrayMap;

    invoke-direct {v5}, Landroid/util/ArrayMap;-><init>()V

    .line 7
    :try_start_0
    invoke-static {p1}, Lclx;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/ArrayList;

    move-result-object v0

    .line 8
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v4, v2, 0x1

    .line 9
    instance-of v2, v1, Landroid/os/PersistableBundle;

    if-nez v2, :cond_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "PersistableBundle expected, got "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 23
    :catch_0
    move-exception v0

    .line 24
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 11
    :cond_0
    :try_start_1
    check-cast v1, Landroid/os/PersistableBundle;

    .line 12
    const-string v2, "feature_flag_name"

    invoke-virtual {v1, v2}, Landroid/os/PersistableBundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 13
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v7, "feature_flag_name"

    .line 14
    invoke-virtual {v1, v7}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 15
    :cond_1
    const-string v2, "mccmnc"

    invoke-virtual {v1, v2}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 16
    if-nez v7, :cond_2

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MCCMNC is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :catch_1
    move-exception v0

    goto :goto_1

    .line 18
    :cond_2
    array-length v8, v7

    move v2, v3

    :goto_2
    if-ge v2, v8, :cond_3

    aget-object v9, v7, v2

    .line 19
    invoke-interface {v5, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    .line 20
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v2, v4

    .line 21
    goto :goto_0

    .line 25
    :cond_4
    return-object v5

    :cond_5
    move v2, v4

    goto :goto_0
.end method
