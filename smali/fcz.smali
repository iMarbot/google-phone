.class final Lfcz;
.super Landroid/telecom/RemoteConnection$Callback;
.source "PG"


# instance fields
.field private synthetic a:Lfcy;


# direct methods
.method constructor <init>(Lfcy;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfcz;->a:Lfcy;

    invoke-direct {p0}, Landroid/telecom/RemoteConnection$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAddressChanged(Landroid/telecom/RemoteConnection;Landroid/net/Uri;I)V
    .locals 2

    .prologue
    .line 37
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onAddressChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2, p3}, Lfcy;->setAddress(Landroid/net/Uri;I)V

    .line 39
    return-void
.end method

.method public final onCallerDisplayNameChanged(Landroid/telecom/RemoteConnection;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 40
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onCallerDisplayNameChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2, p3}, Lfcy;->setCallerDisplayName(Ljava/lang/String;I)V

    .line 42
    return-void
.end method

.method public final onConferenceChanged(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConference;)V
    .locals 2

    .prologue
    .line 64
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onConferenceChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method public final onConferenceableConnectionsChanged(Landroid/telecom/RemoteConnection;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x60

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CircuitSwitchedConnectionWrapper.Callback.onConferenceableConnectionsChangedconnection: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", conf: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lfcz;->a:Lfcy;

    .line 48
    iget-object v0, v0, Lfcy;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 49
    if-eqz v0, :cond_2

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/RemoteConnection;

    .line 52
    iget-object v3, p0, Lfcz;->a:Lfcy;

    .line 54
    iget-object v3, v3, Lfcy;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 55
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/RemoteConnection;)Landroid/telecom/Connection;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x7a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CircuitSwitchedConnectionWrapper.Callback.onConferenceableConnectionsChanged, found "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " conferenceable connections"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    .line 61
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, v1}, Lfcy;->setConferenceableConnections(Ljava/util/List;)V

    .line 63
    :cond_2
    return-void
.end method

.method public final onConnectionCapabilitiesChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2

    .prologue
    .line 11
    const/16 v0, 0x64

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onConnectionCapabilitiesChanged, capabilities: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setConnectionCapabilities(I)V

    .line 13
    return-void
.end method

.method public final onConnectionPropertiesChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 14
    const/16 v0, 0x60

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onConnectionPropertiesChanged, properties: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setConnectionProperties(I)V

    .line 16
    return-void
.end method

.method public final onDestroyed(Landroid/telecom/RemoteConnection;)V
    .locals 2

    .prologue
    .line 17
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onDestroyed"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v0, p0, Lfcz;->a:Lfcy;

    .line 19
    iget-object v0, v0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    .line 20
    iget-object v1, p0, Lfcz;->a:Lfcy;

    .line 21
    iget-object v1, v1, Lfcy;->c:Landroid/telecom/RemoteConnection$Callback;

    .line 22
    invoke-virtual {v0, v1}, Landroid/telecom/RemoteConnection;->unregisterCallback(Landroid/telecom/RemoteConnection$Callback;)V

    .line 23
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0}, Lfcy;->destroy()V

    .line 24
    return-void
.end method

.method public final onDisconnected(Landroid/telecom/RemoteConnection;Landroid/telecom/DisconnectCause;)V
    .locals 3

    .prologue
    .line 8
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x41

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedConnectionWrapper.Callback.onDisconnected, cause: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 10
    return-void
.end method

.method public final onExtrasChanged(Landroid/telecom/RemoteConnection;Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x19
    .end annotation

    .prologue
    .line 66
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onExtrasChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->putExtras(Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method public final onPostDialChar(Landroid/telecom/RemoteConnection;C)V
    .locals 2

    .prologue
    .line 31
    const/16 v0, 0x41

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onPostDialChar, char: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setNextPostDialChar(C)V

    .line 33
    return-void
.end method

.method public final onPostDialWait(Landroid/telecom/RemoteConnection;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onPostDialWait"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setPostDialWait(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public final onRingbackRequested(Landroid/telecom/RemoteConnection;Z)V
    .locals 2

    .prologue
    .line 25
    const/16 v0, 0x4e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onRingbackRequested, ringback: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setRingbackRequested(Z)V

    .line 27
    return-void
.end method

.method public final onStateChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 3

    .prologue
    .line 2
    const-string v1, "CircuitSwitchedConnectionWrapper.Callback.onStateChanged, state: "

    .line 3
    invoke-static {p2}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 4
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lfcz;->a:Lfcy;

    .line 6
    invoke-virtual {v0, p2}, Lfcy;->a(I)V

    .line 7
    return-void

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onStatusHintsChanged(Landroid/telecom/RemoteConnection;Landroid/telecom/StatusHints;)V
    .locals 3

    .prologue
    .line 34
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x4e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CircuitSwitchedConnectionWrapper.Callback.onStatusHintsChanged, status hints: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 36
    return-void
.end method

.method public final onVideoStateChanged(Landroid/telecom/RemoteConnection;I)V
    .locals 2

    .prologue
    .line 43
    const/16 v0, 0x51

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "CircuitSwitchedConnectionWrapper.Callback.onVideoStateChanged, state: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lfcz;->a:Lfcy;

    invoke-virtual {v0, p2}, Lfcy;->setVideoState(I)V

    .line 45
    return-void
.end method
