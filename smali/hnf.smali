.class final Lhnf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Lhnp;

    const/4 v1, 0x0

    sget-object v2, Lhnp;->c:Lhnp;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lhnp;->b:Lhnp;

    aput-object v2, v0, v1

    .line 49
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lhnf;->a:Ljava/util/List;

    .line 50
    return-void
.end method

.method public static a(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Ljava/net/Socket;Ljava/lang/String;ILhnj;)Ljavax/net/ssl/SSLSocket;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1
    const-string v0, "sslSocketFactory"

    invoke-static {p0, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    const-string v0, "socket"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    const-string v0, "spec"

    invoke-static {p5, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    const/4 v0, 0x1

    invoke-virtual {p0, p2, p3, p4, v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 8
    iget-object v1, p5, Lhnj;->b:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 9
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v1

    .line 10
    const-class v2, Ljava/lang/String;

    iget-object v4, p5, Lhnj;->b:[Ljava/lang/String;

    .line 11
    invoke-static {v2, v4, v1}, Lhnr;->a(Ljava/lang/Class;[Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    :goto_0
    move-object v2, v1

    .line 23
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v1

    .line 24
    const-class v4, Ljava/lang/String;

    iget-object v5, p5, Lhnj;->c:[Ljava/lang/String;

    invoke-static {v4, v5, v1}, Lhnr;->a(Ljava/lang/Class;[Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 25
    new-instance v4, Lhnk;

    invoke-direct {v4, p5}, Lhnk;-><init>(Lhnj;)V

    .line 26
    invoke-virtual {v4, v2}, Lhnk;->a([Ljava/lang/String;)Lhnk;

    move-result-object v2

    .line 27
    invoke-virtual {v2, v1}, Lhnk;->b([Ljava/lang/String;)Lhnk;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lhnk;->a()Lhnj;

    move-result-object v1

    .line 30
    iget-object v2, v1, Lhnj;->c:[Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 31
    iget-object v1, v1, Lhnj;->b:[Ljava/lang/String;

    .line 32
    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 34
    :cond_0
    sget-object v2, Lhnd;->b:Lhnd;

    .line 37
    iget-boolean v1, p5, Lhnj;->d:Z

    .line 38
    if-eqz v1, :cond_2

    sget-object v1, Lhnf;->a:Ljava/util/List;

    .line 39
    :goto_1
    invoke-virtual {v2, v0, p3, v1}, Lhnd;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 40
    sget-object v2, Lhnf;->a:Ljava/util/List;

    .line 41
    invoke-static {v1}, Lhnp;->a(Ljava/lang/String;)Lhnp;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    sget-object v3, Lhnf;->a:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Only "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " are supported, but negotiated protocol is %s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 42
    invoke-static {v2, v3, v1}, Lgtn;->b(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 43
    if-nez p1, :cond_1

    .line 44
    sget-object p1, Lhnm;->a:Lhnm;

    .line 45
    :cond_1
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    invoke-interface {p1, p3, v1}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 46
    new-instance v1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    const-string v2, "Cannot verify hostname: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object v1, v3

    .line 38
    goto :goto_1

    .line 46
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 47
    :cond_4
    return-object v0

    :cond_5
    move-object v1, v3

    goto/16 :goto_0
.end method
