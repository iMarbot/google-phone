.class final Lanu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lano;


# direct methods
.method constructor <init>(Lano;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lanu;->a:Lano;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 2
    sget-object v0, Lbld$a;->i:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoq;

    .line 4
    if-nez v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 6
    :cond_1
    iget-object v3, p0, Lanu;->a:Lano;

    iget-object v3, v3, Lano;->o:Landroid/view/ActionMode;

    if-eqz v3, :cond_3

    iget-object v3, v0, Laoq;->O:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 7
    iget-object v1, p0, Lanu;->a:Lano;

    iput-boolean v2, v1, Lano;->p:Z

    .line 8
    iget-object v1, p0, Lanu;->a:Lano;

    iput-boolean v2, v1, Lano;->q:Z

    .line 9
    iget-object v1, p0, Lanu;->a:Lano;

    .line 10
    iget-object v1, v1, Lano;->g:Lanx;

    .line 11
    invoke-interface {v1}, Lanx;->b()V

    .line 12
    iget-object v1, v0, Laoq;->O:Ljava/lang/String;

    .line 13
    invoke-static {v1}, Lano;->a(Ljava/lang/String;)I

    move-result v1

    .line 15
    iget-object v3, p0, Lanu;->a:Lano;

    .line 16
    iget-object v3, v3, Lano;->r:Landroid/util/SparseArray;

    .line 17
    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 18
    iget-object v2, p0, Lanu;->a:Lano;

    iget-object v2, v2, Lano;->c:Landroid/app/Activity;

    invoke-static {v2}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v2

    sget-object v3, Lbkq$a;->cj:Lbkq$a;

    .line 19
    invoke-interface {v2, v3}, Lbku;->a(Lbkq$a;)V

    .line 20
    iget-object v2, p0, Lanu;->a:Lano;

    .line 21
    invoke-virtual {v2, v0, v1}, Lano;->a(Laoq;I)V

    goto :goto_0

    .line 23
    :cond_2
    iget-object v1, p0, Lanu;->a:Lano;

    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->ci:Lbkq$a;

    .line 24
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 25
    iget-object v1, p0, Lanu;->a:Lano;

    .line 26
    invoke-virtual {v1, v0}, Lano;->a(Laoq;)V

    .line 27
    iget-object v0, p0, Lanu;->a:Lano;

    invoke-virtual {v0}, Lano;->a()I

    move-result v0

    iget-object v1, p0, Lanu;->a:Lano;

    .line 28
    iget-object v1, v1, Lano;->r:Landroid/util/SparseArray;

    .line 29
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 30
    const-string v0, "mExpandCollapseListener.onClick"

    const-string v1, "getitem count %d is equal to items select count %d, check select all box"

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lanu;->a:Lano;

    .line 31
    invoke-virtual {v4}, Lano;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v2, p0, Lanu;->a:Lano;

    .line 33
    iget-object v2, v2, Lano;->r:Landroid/util/SparseArray;

    .line 34
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    .line 35
    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lanu;->a:Lano;

    .line 37
    iget-object v0, v0, Lano;->g:Lanx;

    .line 38
    invoke-interface {v0}, Lanx;->c()V

    goto/16 :goto_0

    .line 40
    :cond_3
    iget-object v3, p0, Lanu;->a:Lano;

    iget-object v3, v3, Lano;->d:Latf;

    if-eqz v3, :cond_4

    .line 41
    iget-object v3, p0, Lanu;->a:Lano;

    iget-object v3, v3, Lano;->d:Latf;

    .line 42
    invoke-virtual {v3, v8}, Latf;->a(Z)V

    .line 43
    iput-object v1, v3, Latf;->o:Latf$d;

    .line 44
    iput-object v1, v3, Latf;->k:Landroid/net/Uri;

    .line 46
    :cond_4
    iget-object v3, v0, Laoq;->F:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 47
    iget-object v1, p0, Lanu;->a:Lano;

    .line 48
    invoke-virtual {v1}, Lano;->g()Lbjf;

    move-result-object v1

    .line 49
    iget-object v3, v0, Laoq;->F:Ljava/lang/String;

    invoke-interface {v1, v3}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v1

    .line 50
    :cond_5
    if-nez v1, :cond_6

    .line 51
    sget-object v1, Lbjb;->a:Lbjb;

    .line 52
    :cond_6
    invoke-virtual {v1}, Lbjb;->a()Z

    move-result v3

    iput-boolean v3, v0, Laoq;->U:Z

    .line 53
    invoke-virtual {v1}, Lbjb;->d()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 54
    const-string v1, "mExpandCollapseListener.onClick"

    const-string v3, "%s is temporarily unavailable, requesting capabilities"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v0, Laoq;->F:Ljava/lang/String;

    .line 55
    invoke-static {v5}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 56
    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    iget-object v1, p0, Lanu;->a:Lano;

    .line 58
    invoke-virtual {v1}, Lano;->g()Lbjf;

    move-result-object v1

    .line 59
    iget-object v3, v0, Laoq;->F:Ljava/lang/String;

    invoke-interface {v1, v3}, Lbjf;->a(Ljava/lang/String;)V

    .line 60
    :cond_7
    iget-wide v4, v0, Laoq;->D:J

    iget-object v1, p0, Lanu;->a:Lano;

    .line 61
    iget-wide v6, v1, Lano;->m:J

    .line 62
    cmp-long v1, v4, v6

    if-nez v1, :cond_8

    .line 63
    invoke-virtual {v0, v2}, Laoq;->b(Z)V

    .line 64
    iget-object v0, p0, Lanu;->a:Lano;

    .line 65
    iput v9, v0, Lano;->l:I

    .line 67
    iget-object v0, p0, Lanu;->a:Lano;

    .line 68
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lano;->m:J

    goto/16 :goto_0

    .line 70
    :cond_8
    iget v1, v0, Laoq;->L:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_9

    .line 71
    iget-object v1, p0, Lanu;->a:Lano;

    iget-object v1, v1, Lano;->c:Landroid/app/Activity;

    iget-object v3, v0, Laoq;->E:[J

    invoke-static {v1, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;[J)V

    .line 72
    iget-object v1, p0, Lanu;->a:Lano;

    .line 73
    iget v1, v1, Lano;->i:I

    .line 74
    if-ne v1, v10, :cond_9

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/android/dialer/app/DialtactsActivity;

    .line 76
    iget-object v1, v1, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v1}, Larl;->a()V

    .line 77
    :cond_9
    iget-object v1, p0, Lanu;->a:Lano;

    .line 79
    iget-object v3, v0, Laoq;->O:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 80
    iget-object v3, v1, Lano;->c:Landroid/app/Activity;

    invoke-static {v3}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v3

    sget-object v4, Lbkq$a;->M:Lbkq$a;

    invoke-interface {v3, v4}, Lbku;->a(Lbkq$a;)V

    .line 81
    :cond_a
    iget v3, v1, Lano;->l:I

    .line 82
    invoke-virtual {v0, v8}, Laoq;->b(Z)V

    .line 83
    invoke-virtual {v0}, Laoq;->d()I

    move-result v4

    iput v4, v1, Lano;->l:I

    .line 84
    iget-wide v4, v0, Laoq;->D:J

    iput-wide v4, v1, Lano;->m:J

    .line 85
    if-eq v3, v9, :cond_b

    .line 86
    invoke-virtual {v1, v3}, Lano;->c(I)V

    .line 87
    :cond_b
    iget-object v0, v0, Laoq;->x:Landroid/view/View;

    .line 88
    if-nez v0, :cond_c

    move v0, v2

    .line 96
    :goto_1
    if-eqz v0, :cond_0

    .line 97
    sget v0, Lbbh;->g:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lbbh;->g:I

    goto/16 :goto_0

    .line 90
    :cond_c
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_d

    move v0, v2

    .line 91
    goto :goto_1

    .line 92
    :cond_d
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapk;

    .line 93
    if-nez v0, :cond_e

    move v0, v2

    .line 94
    goto :goto_1

    .line 95
    :cond_e
    const-string v1, "com.google.android.apps.tachyon"

    iget-object v2, p0, Lanu;->a:Lano;

    iget-object v2, v2, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Lapk;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method
