.class public Lfij;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lfid;


# direct methods
.method public constructor <init>(Lfid;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lfij;->a:Lfid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lfid;B)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lfij;-><init>(Lfid;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3
    const-string v0, "RpcClient.ApiaryListener.onRequestError"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    const/16 v0, 0x191

    if-ne p1, v0, :cond_1

    .line 5
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 6
    iget-object v0, v0, Lfid;->g:Lfin;

    .line 7
    iget-object v1, p0, Lfij;->a:Lfid;

    .line 8
    iget-object v1, v1, Lfid;->h:Ljava/lang/String;

    .line 10
    new-instance v2, Lfio;

    invoke-direct {v2, v0, v1}, Lfio;-><init>(Lfin;Ljava/lang/String;)V

    iget-object v0, v0, Lfin;->a:Ljava/util/concurrent/Executor;

    new-array v1, v3, [Ljava/lang/Void;

    .line 11
    invoke-virtual {v2, v0, v1}, Lfio;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 12
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 13
    iget v1, v0, Lfid;->j:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, Lfid;->j:I

    .line 14
    const/4 v0, 0x2

    if-ge v1, v0, :cond_1

    .line 15
    const-string v0, "RpcClient.ApiaryListener.onRequestError, retrying."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 17
    invoke-virtual {v0}, Lfid;->a()V

    .line 25
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 20
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 21
    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 23
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 24
    invoke-interface {v0}, Lfie;->a()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1
    const-string v1, "RpcClient.ApiaryListener.onRequestStarting, "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2
    return-void

    .line 1
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a([B)V
    .locals 2

    .prologue
    .line 26
    const-string v0, "RpcClient.ApiaryListener.onRequestCompleted"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 28
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 29
    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lfij;->a:Lfid;

    .line 31
    iget-object v0, v0, Lfid;->a:Lfie;

    .line 32
    invoke-interface {v0, p1}, Lfie;->a([B)V

    .line 33
    :cond_0
    return-void
.end method
