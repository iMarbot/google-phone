.class public Lcnl;
.super Lcna;
.source "PG"


# static fields
.field private static i:Ljava/util/Random;


# instance fields
.field public c:I

.field private d:Lcnj;

.field private e:[Lcmp;

.field private f:Ljava/util/Date;

.field private g:Lcms;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 142
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcnl;->i:Ljava/util/Random;

    .line 143
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 144
    const-string v0, "^<?([^>]+)>?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 145
    const-string v0, "\r?\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcna;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcnl;->h:Z

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcnl;->d:Lcnj;

    .line 4
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcnj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final h()Lcnj;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcnl;->d:Lcnj;

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcnj;

    invoke-direct {v0}, Lcnj;-><init>()V

    iput-object v0, p0, Lcnl;->d:Lcnj;

    .line 58
    :cond_0
    iget-object v0, p0, Lcnl;->d:Lcnj;

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 4

    .prologue
    .line 99
    const-string v0, "Message-ID"

    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    if-nez v0, :cond_1

    iget-boolean v1, p0, Lcnl;->h:Z

    if-nez v1, :cond_1

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    const-string v0, "<"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x18

    if-ge v0, v2, :cond_0

    .line 104
    sget-object v2, Lcnl;->i:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    and-int/lit8 v2, v2, 0x1f

    .line 105
    const-string v3, "0123456789abcdefghijklmnopqrstuv"

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 106
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v0, "@email.android.com>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    const-string v1, "Message-ID"

    invoke-virtual {p0, v1, v0}, Lcnl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Date;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    iget-object v0, p0, Lcnl;->f:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 60
    :try_start_0
    const-string v1, "Date: "

    const-string v0, "Date"

    .line 61
    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    invoke-static {v0}, Lhwl;->a(Ljava/lang/String;)Lhxx;

    move-result-object v0

    check-cast v0, Lhvm;

    .line 63
    invoke-interface {v0}, Lhvm;->a()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcnl;->f:Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :cond_0
    :goto_1
    iget-object v0, p0, Lcnl;->f:Ljava/util/Date;

    if-nez v0, :cond_1

    .line 68
    :try_start_1
    const-string v1, "Date: "

    const-string v0, "Delivery-date"

    .line 69
    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_2
    invoke-static {v0}, Lhwl;->a(Ljava/lang/String;)Lhxx;

    move-result-object v0

    check-cast v0, Lhvm;

    .line 71
    invoke-interface {v0}, Lhvm;->a()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcnl;->f:Ljava/util/Date;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 75
    :cond_1
    :goto_3
    iget-object v0, p0, Lcnl;->f:Ljava/util/Date;

    return-object v0

    .line 61
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    const-string v0, "Email Log"

    const-string v1, "Message missing Date header"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 69
    :cond_3
    :try_start_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 74
    :catch_1
    move-exception v0

    const-string v0, "Email Log"

    const-string v1, "Message also missing Delivery-Date header"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method public final a(Lcms;)V
    .locals 5

    .prologue
    .line 117
    iput-object p1, p0, Lcnl;->g:Lcms;

    .line 118
    instance-of v0, p1, Lcnc;

    if-eqz v0, :cond_1

    .line 119
    check-cast p1, Lcnc;

    .line 120
    const-string v0, "Content-Type"

    invoke-virtual {p1}, Lcnc;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcnl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v0, "MIME-Version"

    const-string v1, "1.0"

    invoke-virtual {p0, v0, v1}, Lcnl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    instance-of v0, p1, Lcno;

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "Content-Type"

    const-string v1, "%s;\n charset=utf-8"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 124
    invoke-virtual {p0}, Lcnl;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {p0, v0, v1}, Lcnl;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "Content-Transfer-Encoding"

    const-string v1, "base64"

    invoke-virtual {p0, v0, v1}, Lcnl;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    .line 6
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v0

    .line 7
    iget-object v0, v0, Lcnj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcnl;->h:Z

    .line 9
    iput-object v1, p0, Lcnl;->e:[Lcmp;

    .line 10
    iput-object v1, p0, Lcnl;->f:Ljava/util/Date;

    .line 11
    iput-object v1, p0, Lcnl;->g:Lcms;

    .line 12
    new-instance v0, Lhxq;

    invoke-direct {v0}, Lhxq;-><init>()V

    .line 13
    new-instance v1, Lhxp;

    invoke-direct {v1, p0}, Lhxp;-><init>(Lcnl;)V

    .line 14
    iput-object v1, v0, Lhxq;->a:Lhxp;

    .line 17
    new-instance v1, Lhxf;

    invoke-direct {v1, p1}, Lhxf;-><init>(Ljava/io/InputStream;)V

    .line 18
    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->g()Lhxz;

    .line 21
    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2, v1}, Lhzl;->a(Ljava/io/InputStream;)V

    .line 22
    :goto_0
    iget-object v1, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v1}, Lhzl;->b()Lhxu;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lhxu;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :pswitch_0
    iget-object v1, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v1}, Lhzl;->d()Lhxs;

    move-result-object v1

    .line 25
    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->c()Ljava/io/InputStream;

    move-result-object v2

    .line 26
    iget-object v3, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v3, v1, v2}, Lhxp;->a(Lhxs;Ljava/io/InputStream;)V

    .line 54
    :goto_1
    iget-object v1, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v1}, Lhzl;->f()Lhxu;

    goto :goto_0

    .line 28
    :pswitch_1
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->d()V

    goto :goto_1

    .line 30
    :pswitch_2
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->f()V

    goto :goto_1

    .line 32
    :pswitch_3
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->b()V

    goto :goto_1

    .line 34
    :pswitch_4
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->h()V

    goto :goto_1

    .line 37
    :pswitch_5
    iget-object v1, v0, Lhxq;->a:Lhxp;

    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->c()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhxp;->b(Ljava/io/InputStream;)V

    goto :goto_1

    .line 39
    :pswitch_6
    iget-object v1, v0, Lhxq;->a:Lhxp;

    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->e()Lhxx;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhxp;->a(Lhxx;)V

    goto :goto_1

    .line 41
    :pswitch_7
    iget-object v1, v0, Lhxq;->a:Lhxp;

    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->c()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhxp;->a(Ljava/io/InputStream;)V

    goto :goto_1

    .line 43
    :pswitch_8
    iget-object v1, v0, Lhxq;->a:Lhxp;

    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->c()Ljava/io/InputStream;

    invoke-virtual {v1}, Lhxp;->i()V

    goto :goto_1

    .line 45
    :pswitch_9
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->c()V

    goto :goto_1

    .line 47
    :pswitch_a
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->e()V

    goto :goto_1

    .line 49
    :pswitch_b
    iget-object v1, v0, Lhxq;->a:Lhxp;

    invoke-virtual {v1}, Lhxp;->a()V

    goto :goto_1

    .line 51
    :pswitch_c
    iget-object v1, v0, Lhxq;->a:Lhxp;

    iget-object v2, v0, Lhxq;->b:Lhzl;

    invoke-virtual {v2}, Lhzl;->d()Lhxs;

    invoke-virtual {v1}, Lhxp;->g()V

    goto :goto_1

    .line 36
    :pswitch_d
    return-void

    .line 23
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_3
        :pswitch_8
        :pswitch_a
        :pswitch_6
        :pswitch_2
        :pswitch_c
        :pswitch_4
        :pswitch_7
        :pswitch_5
        :pswitch_9
        :pswitch_1
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 134
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 135
    invoke-direct {p0}, Lcnl;->i()Ljava/lang/String;

    .line 136
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcnj;->a(Ljava/io/OutputStream;)V

    .line 137
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 139
    iget-object v0, p0, Lcnl;->g:Lcms;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcnl;->g:Lcms;

    invoke-interface {v0, p1}, Lcms;->a(Ljava/io/OutputStream;)V

    .line 141
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcnj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcnj;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Long;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 76
    :try_start_0
    const-string v0, "Content-Duration"

    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    const-string v0, "MimeMessage.getDuration"

    const-string v2, "message missing Content-Duration header"

    invoke-static {v0, v2}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 87
    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v2, "MimeMessage.getDuration"

    const-string v3, "cannot retrieve header: "

    invoke-static {v2, v3, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 80
    goto :goto_0

    .line 84
    :cond_0
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 86
    :catch_1
    move-exception v2

    const-string v2, "MimeMessage.getDuration"

    const-string v3, "cannot parse duration "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 87
    goto :goto_0

    .line 86
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Lcnl;->h()Lcnj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcnj;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public final c()[Lcmp;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcnl;->e:[Lcmp;

    if-nez v0, :cond_2

    .line 94
    const-string v0, "From"

    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnn;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 96
    :cond_0
    const-string v0, "Sender"

    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcnn;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    :cond_1
    invoke-static {v0}, Lcmp;->a(Ljava/lang/String;)[Lcmp;

    move-result-object v0

    iput-object v0, p0, Lcnl;->e:[Lcmp;

    .line 98
    :cond_2
    iget-object v0, p0, Lcnl;->e:[Lcmp;

    return-object v0
.end method

.method public final e()Lcms;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcnl;->g:Lcms;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "Content-Type"

    invoke-direct {p0, v0}, Lcnl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    if-nez v0, :cond_0

    .line 90
    const-string v0, "text/plain"

    .line 91
    :cond_0
    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcnl;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcnn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
