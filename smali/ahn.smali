.class public final Lahn;
.super Lahm;
.source "PG"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/util/List;

.field private d:Lahk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lahm;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lahn;->c:Ljava/util/List;

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lahn;->b:Landroid/content/Context;

    .line 5
    iget-object v0, p0, Lahn;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 6
    invoke-static {v0}, Lahk;->a(Landroid/content/SharedPreferences;)Lahk;

    move-result-object v0

    iput-object v0, p0, Lahn;->d:Lahk;

    .line 7
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lahn;->a(Z)V

    .line 8
    return-void
.end method

.method private final a(Lahk;ZZ)V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lahn;->d:Lahk;

    invoke-virtual {p1, v0}, Lahk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 10
    iput-object p1, p0, Lahn;->d:Lahk;

    .line 11
    if-eqz p2, :cond_0

    .line 13
    iget-object v0, p0, Lahn;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lahn;->d:Lahk;

    invoke-static {v0, v1}, Lahk;->a(Landroid/content/SharedPreferences;Lahk;)V

    .line 15
    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lahn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 17
    iget-object v0, p0, Lahn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 19
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 20
    iget-object v0, p0, Lahn;->d:Lahk;

    if-nez v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    iget-object v0, p0, Lahn;->d:Lahk;

    iget v0, v0, Lahk;->a:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 25
    :sswitch_0
    iget-object v0, p0, Lahn;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lahk;->a(Landroid/content/SharedPreferences;)Lahk;

    move-result-object v0

    .line 27
    invoke-direct {p0, v0, v2, v1}, Lahn;->a(Lahk;ZZ)V

    goto :goto_0

    .line 30
    :sswitch_1
    iget-object v0, p0, Lahn;->b:Landroid/content/Context;

    invoke-static {v0}, Lain;->a(Landroid/content/Context;)Lain;

    move-result-object v0

    .line 31
    new-instance v3, Lajk;

    iget-object v4, p0, Lahn;->d:Lahk;

    iget-object v4, v4, Lahk;->c:Ljava/lang/String;

    iget-object v5, p0, Lahn;->d:Lahk;

    iget-object v5, v5, Lahk;->b:Ljava/lang/String;

    iget-object v6, p0, Lahn;->d:Lahk;

    iget-object v6, v6, Lahk;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lajk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-virtual {v0, v2}, Lain;->a(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajk;

    .line 34
    invoke-virtual {v3, v0}, Lajk;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 38
    :goto_1
    if-nez v0, :cond_0

    .line 39
    const/4 v0, -0x2

    .line 40
    invoke-static {v0}, Lahk;->a(I)Lahk;

    move-result-object v0

    .line 41
    invoke-direct {p0, v0, v1, v1}, Lahn;->a(Lahk;ZZ)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 37
    goto :goto_1

    .line 22
    :sswitch_data_0
    .sparse-switch
        -0x6 -> :sswitch_0
        0x0 -> :sswitch_1
    .end sparse-switch
.end method
