.class final Leqp;
.super Ljava/lang/Object;

# interfaces
.implements Ledl;


# instance fields
.field private synthetic a:Lcom/google/android/gms/internal/zzcdq;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Ljava/util/Map;

.field private synthetic d:Leli;

.field private synthetic e:Lelh;

.field private synthetic f:Leql;


# direct methods
.method constructor <init>(Leql;Lcom/google/android/gms/internal/zzcdq;Ljava/lang/String;Ljava/util/Map;Leli;Lelh;)V
    .locals 0

    iput-object p1, p0, Leqp;->f:Leql;

    iput-object p2, p0, Leqp;->a:Lcom/google/android/gms/internal/zzcdq;

    iput-object p3, p0, Leqp;->b:Ljava/lang/String;

    iput-object p4, p0, Leqp;->c:Ljava/util/Map;

    iput-object p5, p0, Leqp;->d:Leli;

    iput-object p6, p0, Leqp;->e:Lelh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConnected(Landroid/os/Bundle;)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Leqp;->f:Leql;

    invoke-static {v0}, Leql;->a(Leql;)V

    iget-object v0, p0, Leqp;->f:Leql;

    invoke-virtual {v0}, Lejb;->q()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzcei;

    iget-object v1, p0, Leqp;->a:Lcom/google/android/gms/internal/zzcdq;

    iget-object v2, p0, Leqp;->b:Ljava/lang/String;

    iget-object v3, p0, Leqp;->c:Ljava/util/Map;

    iget-object v4, p0, Leqp;->d:Leli;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/internal/zzcei;->zza(Lcom/google/android/gms/internal/zzcee;Ljava/lang/String;Ljava/util/Map;Leli;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Leqp;->e:Lelh;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "RemoteException: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Leql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lelh;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onConnectionSuspended(I)V
    .locals 2

    iget-object v0, p0, Leqp;->e:Lelh;

    const-string v1, "Disconnected."

    invoke-static {v1}, Leql;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lelh;->a(Ljava/lang/String;)V

    return-void
.end method
