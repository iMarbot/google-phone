.class public final Lawi;
.super Lawf;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lawf;-><init>(B)V

    return-void
.end method

.method public static a(Ljava/lang/String;Lawg;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 2
    new-instance v0, Lawi;

    invoke-direct {v0}, Lawi;-><init>()V

    .line 3
    iput-object p0, v0, Lawi;->c:Ljava/lang/String;

    .line 4
    iput-object p1, v0, Lawi;->d:Lawg;

    .line 5
    iput-object p2, v0, Lawi;->e:Landroid/content/DialogInterface$OnDismissListener;

    .line 6
    return-object v0
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 7
    invoke-super {p0, p1}, Lawf;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 8
    invoke-virtual {p0}, Lawi;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p0}, Lapw;->b(Landroid/app/Activity;Landroid/app/DialogFragment;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 9
    const v1, 0x7f110296

    .line 10
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f110295

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lawi;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 11
    invoke-virtual {p0, v1, v2}, Lawi;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f110294

    iget-object v2, p0, Lawi;->d:Lawg;

    .line 12
    invoke-static {p0, v2}, Lapw;->b(Landroid/app/DialogFragment;Lawg;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 16
    return-object v0
.end method

.method public final bridge synthetic onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lawf;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public final bridge synthetic onPause()V
    .locals 0

    .prologue
    .line 17
    invoke-super {p0}, Lawf;->onPause()V

    return-void
.end method
