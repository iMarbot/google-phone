.class public final Ldnf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Map;

.field public b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

.field public c:Z

.field private d:Landroid/content/Intent;

.field private e:Landroid/content/Context;

.field private f:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.hangouts.telephony.ITeleHangoutsService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldnf;->d:Landroid/content/Intent;

    .line 3
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Ldnf;->a:Ljava/util/Map;

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    .line 5
    iput-boolean v4, p0, Ldnf;->c:Z

    .line 6
    new-instance v0, Ldng;

    invoke-direct {v0, p0}, Ldng;-><init>(Ldnf;)V

    iput-object v0, p0, Ldnf;->f:Landroid/content/ServiceConnection;

    .line 7
    iput-object p1, p0, Ldnf;->e:Landroid/content/Context;

    .line 8
    if-nez p2, :cond_0

    .line 9
    const-string v0, "No phone account specified; skipping connection"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    :goto_0
    return-void

    .line 11
    :cond_0
    sget-object v0, Ldny;->c:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 12
    const-string v0, "Binder not enabled; skipping connection"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {p2}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 15
    sget-object v0, Ldny;->j:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 17
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 18
    const-string v2, "%s != %s; skipping binding"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 20
    :cond_2
    const-string v0, "Binding to %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Ldnf;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 22
    iget-object v0, p0, Ldnf;->d:Landroid/content/Intent;

    iget-object v1, p0, Ldnf;->f:Landroid/content/ServiceConnection;

    .line 23
    invoke-virtual {p1, v0, v1, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Ldnf;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 25
    iget-boolean v0, p0, Ldnf;->c:Z

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "Unbinding from service"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnf;->c:Z

    .line 28
    iget-object v0, p0, Ldnf;->e:Landroid/content/Context;

    iget-object v1, p0, Ldnf;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 29
    :cond_0
    iget-object v0, p0, Ldnf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnh;

    .line 30
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ldnh;->cancel(Z)Z

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Ldnf;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 33
    return-void
.end method
