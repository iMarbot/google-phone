.class public abstract Lgui;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# instance fields
.field private transient a:Lgul;

.field private transient b:Lgul;

.field private transient c:Lgub;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private e()Lgub;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lgui;->c:Lgub;

    .line 12
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgui;->d()Lgub;

    move-result-object v0

    iput-object v0, p0, Lgui;->c:Lgub;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()Lgul;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lgui;->a:Lgul;

    .line 10
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgui;->b()Lgul;

    move-result-object v0

    iput-object v0, p0, Lgui;->a:Lgul;

    :cond_0
    return-object v0
.end method

.method abstract b()Lgul;
.end method

.method abstract c()Lgul;
.end method

.method public final clear()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lgui;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lgui;->e()Lgub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgub;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract d()Lgub;
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lgui;->a()Lgul;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 13
    invoke-static {p0, p1}, Lhcw;->a(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lgui;->a()Lgul;

    move-result-object v0

    invoke-static {v0}, Lgfb$a;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lgui;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 19
    .line 20
    iget-object v0, p0, Lgui;->b:Lgul;

    .line 21
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgui;->c()Lgul;

    move-result-object v0

    iput-object v0, p0, Lgui;->b:Lgul;

    .line 22
    :cond_0
    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    invoke-static {p0}, Lhcw;->b(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lgui;->e()Lgub;

    move-result-object v0

    return-object v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lguk;

    invoke-direct {v0, p0}, Lguk;-><init>(Lgui;)V

    return-object v0
.end method
