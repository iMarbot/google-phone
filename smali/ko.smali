.class public abstract Lko;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static g:Ljava/util/HashMap;


# instance fields
.field public a:Lkp;

.field private b:Lkq;

.field private c:Lkw;

.field private d:Z

.field private e:Z

.field private f:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lko;->g:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 2
    iput-boolean v0, p0, Lko;->d:Z

    .line 3
    iput-boolean v0, p0, Lko;->e:Z

    .line 4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    .line 7
    :goto_0
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method final a(Z)V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lko;->a:Lkp;

    if-nez v0, :cond_1

    .line 44
    new-instance v0, Lkp;

    invoke-direct {v0, p0}, Lkp;-><init>(Lko;)V

    iput-object v0, p0, Lko;->a:Lkp;

    .line 45
    iget-object v0, p0, Lko;->c:Lkw;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 46
    iget-object v0, p0, Lko;->c:Lkw;

    invoke-virtual {v0}, Lkw;->b()V

    .line 47
    :cond_0
    iget-object v0, p0, Lko;->a:Lkp;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lkp;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 48
    :cond_1
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 50
    iget-object v1, p0, Lko;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 51
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lko;->a:Lkp;

    .line 52
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lko;->a(Z)V

    .line 56
    :cond_0
    :goto_0
    monitor-exit v1

    .line 57
    :cond_1
    return-void

    .line 54
    :cond_2
    iget-boolean v0, p0, Lko;->e:Z

    if-nez v0, :cond_0

    .line 55
    iget-object v0, p0, Lko;->c:Lkw;

    invoke-virtual {v0}, Lkw;->c()V

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final c()Lkt;
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lko;->b:Lkq;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lko;->b:Lkq;

    invoke-interface {v0}, Lkq;->b()Lkt;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 60
    :cond_0
    iget-object v1, p0, Lko;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 61
    :try_start_0
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 62
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkt;

    monitor-exit v1

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 63
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lko;->b:Lkq;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lko;->b:Lkq;

    invoke-interface {v0}, Lkq;->a()Landroid/os/IBinder;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x1a

    .line 8
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_0

    .line 10
    new-instance v0, Lku;

    invoke-direct {v0, p0}, Lku;-><init>(Lko;)V

    iput-object v0, p0, Lko;->b:Lkq;

    .line 11
    iput-object v1, p0, Lko;->c:Lkw;

    .line 23
    :goto_0
    return-void

    .line 12
    :cond_0
    iput-object v1, p0, Lko;->b:Lkq;

    .line 13
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    sget-object v0, Lko;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkw;

    .line 16
    if-nez v0, :cond_2

    .line 17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1

    .line 18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t be here without a job id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_1
    new-instance v0, Lkr;

    invoke-direct {v0, p0, v1}, Lkr;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 20
    sget-object v2, Lko;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    :cond_2
    iput-object v0, p0, Lko;->c:Lkw;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 37
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 38
    iget-object v1, p0, Lko;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 39
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lko;->e:Z

    .line 40
    iget-object v0, p0, Lko;->c:Lkw;

    invoke-virtual {v0}, Lkw;->c()V

    .line 41
    monitor-exit v1

    .line 42
    :cond_0
    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, p0, Lko;->c:Lkw;

    invoke-virtual {v0}, Lkw;->a()V

    .line 26
    iget-object v1, p0, Lko;->f:Ljava/util/ArrayList;

    monitor-enter v1

    .line 27
    :try_start_0
    iget-object v0, p0, Lko;->f:Ljava/util/ArrayList;

    new-instance v2, Lks;

    if-eqz p1, :cond_0

    :goto_0
    invoke-direct {v2, p0, p1, p3}, Lks;-><init>(Lko;Landroid/content/Intent;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lko;->a(Z)V

    .line 29
    monitor-exit v1

    .line 30
    const/4 v0, 0x3

    .line 31
    :goto_1
    return v0

    .line 27
    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 31
    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method
