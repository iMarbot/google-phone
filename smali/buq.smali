.class public final Lbuq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwm;
.implements Lbwn;
.implements Lbwq;
.implements Lbwt;
.implements Lcdi;
.implements Lcgn;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/os/Handler;

.field public c:Lcdc;

.field public d:Lcdc;

.field public e:Lcgm;

.field public f:Z

.field private g:Lbvp$d;

.field private h:Lbvp$d;

.field private i:Lalj;

.field private j:Z

.field private k:Z

.field private l:Lcds;

.field private m:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbuq;->b:Landroid/os/Handler;

    .line 3
    iput-boolean v2, p0, Lbuq;->j:Z

    .line 4
    new-instance v0, Lbur;

    invoke-direct {v0, p0}, Lbur;-><init>(Lbuq;)V

    iput-object v0, p0, Lbuq;->m:Ljava/lang/Runnable;

    .line 5
    const-string v0, "CallCardPresenter.constructor"

    const/4 v1, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbuq;->a:Landroid/content/Context;

    .line 7
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    .line 8
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lbkc;

    invoke-interface {v0}, Lbkc;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    .line 9
    invoke-interface {v0}, Lcdu;->n()Lcdt;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcdt;->a()Lcds;

    move-result-object v0

    iput-object v0, p0, Lbuq;->l:Lcds;

    .line 11
    return-void
.end method

.method private final A()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 575
    iget-object v1, p0, Lbuq;->c:Lcdc;

    if-nez v1, :cond_1

    .line 580
    :cond_0
    :goto_0
    return v0

    .line 577
    :cond_1
    iget-object v1, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v1}, Lcdc;->j()I

    move-result v1

    invoke-static {v1}, Lbvs;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbuq;->c:Lcdc;

    .line 578
    invoke-virtual {v1}, Lcdc;->q()Landroid/telecom/GatewayInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbuq;->c:Lcdc;

    .line 579
    invoke-virtual {v1}, Lcdc;->q()Landroid/telecom/GatewayInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telecom/GatewayInfo;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lcct;Lcdc;Z)Lcdc;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 352
    .line 354
    invoke-virtual {p0, v3, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_1

    if-eq v0, p1, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-object v0

    .line 359
    :cond_1
    invoke-virtual {p0, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 361
    if-eqz v0, :cond_2

    if-ne v0, p1, :cond_0

    .line 363
    :cond_2
    if-nez p2, :cond_4

    .line 365
    const/16 v0, 0x9

    .line 366
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_3

    if-ne v0, p1, :cond_0

    .line 371
    :cond_3
    const/16 v0, 0xa

    .line 372
    invoke-virtual {p0, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 374
    if-eqz v0, :cond_4

    if-ne v0, p1, :cond_0

    .line 378
    :cond_4
    invoke-virtual {p0, v4, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 380
    if-eqz v0, :cond_5

    if-ne v0, p1, :cond_0

    .line 383
    :cond_5
    invoke-virtual {p0, v4, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lbvp$d;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 581
    iget-object v0, p1, Lbvp$d;->a:Ljava/lang/String;

    iget-object v1, p1, Lbvp$d;->b:Ljava/lang/String;

    iget-object v2, p0, Lbuq;->i:Lalj;

    .line 582
    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v0

    .line 583
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584
    iget-object v0, p1, Lbvp$d;->c:Ljava/lang/String;

    .line 585
    :cond_0
    return-object v0
.end method

.method private final a(Lcdc;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 338
    if-eqz p1, :cond_0

    .line 339
    invoke-virtual {p1, v0}, Lcdc;->d(I)Z

    move-result v1

    .line 340
    if-nez v1, :cond_0

    .line 341
    invoke-virtual {p1}, Lcdc;->j()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lbuq;->a(Lcdc;ZZ)V

    .line 342
    :cond_0
    return-void

    .line 341
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Lcdc;ZZ)V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v0

    .line 344
    new-instance v1, Lbus;

    invoke-direct {v1, p0, p2}, Lbus;-><init>(Lbuq;Z)V

    invoke-virtual {v0, p1, p3, v1}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 345
    return-void
.end method

.method private static a(Lcdc;)Z
    .locals 1

    .prologue
    .line 505
    if-eqz p0, :cond_0

    .line 506
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-boolean v0, v0, Lcdf;->b:Z

    .line 507
    if-nez v0, :cond_0

    .line 508
    iget-boolean v0, p0, Lcdc;->k:Z

    .line 509
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcdc;)Z
    .locals 1

    .prologue
    .line 510
    if-eqz p0, :cond_0

    .line 511
    iget-object v0, p0, Lcdc;->g:Lcdf;

    iget-boolean v0, v0, Lcdf;->b:Z

    .line 512
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcdc;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c(Lcdc;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 613
    if-nez p1, :cond_1

    .line 626
    :cond_0
    :goto_0
    return v0

    .line 615
    :cond_1
    iget-object v2, p0, Lbuq;->c:Lcdc;

    .line 616
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lbuq;->c:Lcdc;

    .line 617
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    :cond_2
    move v2, v1

    .line 618
    :goto_1
    if-eqz v2, :cond_0

    .line 620
    iget-object v2, p1, Lcdc;->q:Ljava/lang/String;

    .line 621
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 622
    invoke-virtual {p1}, Lcdc;->k()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 624
    iget-boolean v2, p1, Lcdc;->L:Z

    .line 625
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    .line 617
    goto :goto_1
.end method

.method private static d(Lcdc;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 627
    if-eqz p0, :cond_2

    .line 630
    iget-object v2, p0, Lcdc;->q:Ljava/lang/String;

    .line 631
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    .line 632
    :goto_0
    if-eqz v2, :cond_2

    .line 633
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_0

    .line 634
    invoke-virtual {p0}, Lcdc;->j()I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_2

    .line 635
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 631
    goto :goto_0

    :cond_2
    move v0, v1

    .line 635
    goto :goto_1
.end method

.method private final q()V
    .locals 32

    .prologue
    .line 213
    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    .line 215
    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    if-eqz v2, :cond_16

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/16 v3, 0x20

    .line 217
    invoke-virtual {v2, v3}, Lcdc;->d(I)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    iget-wide v2, v2, Lbvp$d;->n:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    :cond_0
    const/4 v15, 0x1

    .line 220
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v2}, Lcdc;->j()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    .line 221
    :goto_1
    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcdc;->d(I)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v17, 0x1

    .line 222
    :goto_2
    if-nez v17, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/high16 v3, 0x4000000

    .line 223
    invoke-virtual {v2, v3}, Lcdc;->d(I)Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->a:Landroid/content/Context;

    .line 224
    invoke-static {v2}, Lbib;->s(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v16, 0x1

    .line 225
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    iget-boolean v2, v2, Lbvp$d;->s:Z

    if-eqz v2, :cond_9

    const/16 v24, 0x1

    .line 226
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 227
    invoke-virtual {v2}, Lcdc;->t()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v3}, Lcdc;->j()I

    move-result v3

    invoke-static {v2, v3}, Lbxh;->a(II)Z

    move-result v2

    if-nez v2, :cond_a

    const/16 v19, 0x1

    .line 229
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->e:Lcgm;

    move-object/from16 v30, v0

    .line 230
    new-instance v3, Lcgp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 231
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 232
    invoke-virtual {v2}, Lcdc;->u()Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 233
    invoke-virtual {v2}, Lcdc;->F()Lcjs;

    move-result-object v2

    invoke-interface {v2}, Lcjs;->g()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 234
    invoke-virtual {v2}, Lcdc;->o()Landroid/telecom/DisconnectCause;

    move-result-object v7

    .line 235
    invoke-direct/range {p0 .. p0}, Lbuq;->z()Ljava/lang/String;

    move-result-object v8

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v2}, Lcdc;->d()Landroid/telecom/StatusHints;

    move-result-object v2

    .line 238
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/telecom/StatusHints;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v9

    if-eqz v9, :cond_b

    .line 239
    invoke-virtual {v2}, Landroid/telecom/StatusHints;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lbuq;->a:Landroid/content/Context;

    invoke-virtual {v2, v9}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 240
    if-eqz v9, :cond_b

    .line 245
    :goto_6
    invoke-direct/range {p0 .. p0}, Lbuq;->A()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 246
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v2}, Lcdc;->q()Landroid/telecom/GatewayInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/GatewayInfo;->getGatewayAddress()Landroid/net/Uri;

    move-result-object v2

    .line 247
    if-nez v2, :cond_c

    const-string v10, ""

    .line 250
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbuq;->c(Lcdc;)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 251
    iget-object v11, v2, Lcdc;->q:Ljava/lang/String;

    .line 252
    :goto_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lbuq;->c:Lcdc;

    .line 254
    iget-object v2, v12, Lcdc;->y:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 255
    const/4 v2, 0x4

    invoke-virtual {v12, v2}, Lcdc;->d(I)Z

    move-result v2

    .line 257
    iget-boolean v13, v12, Lcdc;->k:Z

    .line 258
    if-nez v13, :cond_1

    if-eqz v2, :cond_2

    .line 259
    :cond_1
    iget-object v2, v12, Lcdc;->h:Landroid/content/Context;

    const-class v13, Landroid/telecom/TelecomManager;

    .line 260
    invoke-virtual {v2, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/TelecomManager;

    invoke-virtual {v12}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/telecom/TelecomManager;->getLine1Number(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcdc;->y:Ljava/lang/String;

    .line 261
    :cond_2
    iget-object v2, v12, Lcdc;->y:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 262
    const-string v2, ""

    iput-object v2, v12, Lcdc;->y:Ljava/lang/String;

    .line 263
    :cond_3
    iget-object v12, v12, Lcdc;->y:Ljava/lang/String;

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 265
    iget-object v13, v2, Lcdc;->h:Landroid/content/Context;

    .line 266
    invoke-virtual {v2}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 267
    invoke-static {v13, v2}, Lbev;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    .line 268
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 269
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_4

    .line 270
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v13}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 272
    :cond_4
    invoke-static {v12, v2}, Lbmw;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/16 v13, 0x8

    .line 273
    invoke-virtual {v2, v13}, Lcdc;->d(I)Z

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 275
    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Lcdc;->d(I)Z

    move-result v2

    .line 276
    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/4 v14, 0x2

    .line 277
    invoke-virtual {v2, v14}, Lcdc;->d(I)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v14, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 279
    iget-object v2, v2, Lcdc;->p:Ljava/lang/String;

    .line 280
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    const/16 v18, 0x1

    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 281
    invoke-virtual {v2}, Lcdc;->p()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 283
    iget-boolean v0, v2, Lcdc;->G:Z

    move/from16 v22, v0

    .line 284
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 286
    iget-boolean v0, v2, Lcdc;->J:Z

    move/from16 v23, v0

    .line 288
    sget-object v2, Lcct;->a:Lcct;

    .line 289
    invoke-virtual {v2}, Lcct;->h()Lcdc;

    move-result-object v2

    .line 290
    sget-object v25, Lcct;->a:Lcct;

    .line 291
    invoke-virtual/range {v25 .. v25}, Lcct;->i()Lcdc;

    move-result-object v25

    .line 292
    if-eqz v2, :cond_11

    if-eqz v25, :cond_11

    move-object/from16 v0, v25

    if-eq v2, v0, :cond_11

    .line 293
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lcdc;->c(I)Z

    move-result v25

    .line 297
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->d:Lcdc;

    if-nez v2, :cond_12

    .line 298
    const/16 v26, 0x0

    .line 302
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 303
    invoke-virtual {v2}, Lcdc;->z()Z

    move-result v27

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 305
    invoke-virtual {v2}, Lcdc;->z()Z

    move-result v29

    if-eqz v29, :cond_14

    .line 307
    invoke-virtual {v2}, Lcdc;->m()Landroid/os/Bundle;

    move-result-object v2

    const-string v29, "android.telecom.extra.ASSISTED_DIALING_EXTRAS"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 309
    invoke-static {}, Lavk;->f()Lavl;

    move-result-object v29

    const-string v31, "TRANSFORMATION_INFO_ORIGINAL_NUMBER"

    .line 310
    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lavl;->a(Ljava/lang/String;)Lavl;

    move-result-object v29

    const-string v31, "TRANSFORMATION_INFO_TRANSFORMED_NUMBER"

    .line 311
    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lavl;->b(Ljava/lang/String;)Lavl;

    move-result-object v29

    const-string v31, "TRANSFORMATION_INFO_USER_HOME_COUNTRY_CODE"

    .line 312
    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lavl;->c(Ljava/lang/String;)Lavl;

    move-result-object v29

    const-string v31, "TRANSFORMATION_INFO_USER_ROAMING_COUNTRY_CODE"

    .line 313
    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 314
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lavl;->d(Ljava/lang/String;)Lavl;

    move-result-object v29

    const-string v31, "TRANSFORMED_NUMBER_COUNTRY_CALLING_CODE"

    .line 315
    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 316
    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lavl;->a(I)Lavl;

    move-result-object v2

    .line 317
    invoke-virtual {v2}, Lavl;->a()Lavk;

    move-result-object v29

    .line 320
    :goto_d
    invoke-direct/range {v3 .. v29}, Lcgp;-><init>(IZILandroid/telecom/DisconnectCause;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZZZJZZZZIZLjava/lang/String;Lavk;)V

    .line 321
    move-object/from16 v0, v30

    invoke-interface {v0, v3}, Lcgm;->a(Lcgp;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    .line 323
    if-nez v2, :cond_15

    const/4 v2, 0x0

    throw v2

    .line 217
    :cond_5
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 220
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 221
    :cond_7
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 224
    :cond_8
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 225
    :cond_9
    const/16 v24, 0x0

    goto/16 :goto_4

    .line 227
    :cond_a
    const/16 v19, 0x0

    goto/16 :goto_5

    .line 242
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_6

    .line 247
    :cond_c
    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_7

    .line 249
    :cond_d
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 252
    :cond_e
    const/4 v11, 0x0

    goto/16 :goto_8

    .line 277
    :cond_f
    const/4 v14, 0x0

    goto/16 :goto_9

    .line 280
    :cond_10
    const/16 v18, 0x0

    goto/16 :goto_a

    .line 294
    :cond_11
    const/16 v25, 0x1

    goto/16 :goto_b

    .line 299
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v2}, Lcdc;->j()I

    move-result v2

    const/16 v26, 0x3

    move/from16 v0, v26

    if-ne v2, v0, :cond_13

    .line 300
    const/16 v26, 0x2

    goto/16 :goto_c

    .line 301
    :cond_13
    const/16 v26, 0x1

    goto/16 :goto_c

    .line 319
    :cond_14
    const/16 v29, 0x0

    goto :goto_d

    .line 323
    :cond_15
    check-cast v2, Lip;

    invoke-virtual {v2}, Lip;->h()Lit;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/InCallActivity;

    .line 324
    if-eqz v2, :cond_16

    .line 325
    invoke-virtual {v2}, Lcom/android/incallui/InCallActivity;->i()V

    .line 326
    :cond_16
    return-void
.end method

.method private final r()V
    .locals 2

    .prologue
    .line 327
    .line 328
    iget-object v0, p0, Lbuq;->e:Lcgm;

    .line 329
    invoke-direct {p0}, Lbuq;->s()Z

    move-result v1

    invoke-interface {v0, v1}, Lcgm;->f(Z)V

    .line 330
    return-void
.end method

.method private final s()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 331
    iget-object v1, p0, Lbuq;->c:Lcdc;

    if-nez v1, :cond_1

    .line 333
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbuq;->c:Lcdc;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Lcdc;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lbuq;->j:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final t()V
    .locals 22

    .prologue
    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    if-nez v2, :cond_0

    .line 466
    :goto_0
    return-void

    .line 388
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    if-nez v2, :cond_1

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    invoke-static {}, Lcgq;->a()Lcgq;

    move-result-object v3

    invoke-interface {v2, v3}, Lcgm;->a(Lcgq;)V

    goto :goto_0

    .line 391
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 392
    invoke-virtual {v2}, Lcdc;->t()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lbuq;->c:Lcdc;

    invoke-virtual {v3}, Lcdc;->j()I

    move-result v3

    invoke-static {v2, v3}, Lbxh;->a(II)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v11, 0x1

    .line 393
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Lcdc;->d(I)Z

    move-result v12

    .line 394
    const/16 v18, 0x0

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 396
    iget-object v2, v2, Lcdc;->B:Lbjl;

    .line 397
    if-eqz v2, :cond_2

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 399
    iget-object v2, v2, Lcdc;->B:Lbjl;

    .line 400
    invoke-interface {v2}, Lbjl;->d()Lbln;

    move-result-object v18

    .line 401
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 402
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcdc;->d(I)Z

    move-result v2

    .line 403
    if-eqz v2, :cond_4

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->e:Lcgm;

    move-object/from16 v21, v0

    new-instance v2, Lcgq;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lbuq;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbuq;->c:Lcdc;

    const/4 v6, 0x2

    .line 405
    invoke-virtual {v5, v6}, Lcdc;->d(I)Z

    move-result v5

    .line 406
    invoke-static {v4, v5}, Lbve;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    .line 407
    invoke-direct/range {p0 .. p0}, Lbuq;->v()Z

    move-result v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->c:Lcdc;

    move-object/from16 v20, v0

    .line 408
    invoke-virtual/range {v20 .. v20}, Lcdc;->k()I

    move-result v20

    invoke-direct/range {v2 .. v20}, Lcgq;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;IZZZZZZZLjava/lang/String;Lbln;ZI)V

    .line 409
    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lcgm;->a(Lcgq;)V

    .line 463
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbuq;->k:Z

    if-eqz v2, :cond_17

    .line 464
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    invoke-direct/range {p0 .. p0}, Lbuq;->u()Lip;

    move-result-object v3

    invoke-interface {v2, v3}, Lcgm;->a(Lip;)V

    goto/16 :goto_0

    .line 392
    :cond_3
    const/4 v11, 0x0

    goto :goto_1

    .line 410
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    if-eqz v2, :cond_16

    .line 411
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "update primary display info for "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbuq;->a(Lbvp$d;)Ljava/lang/String;

    move-result-object v4

    .line 413
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 414
    iget-object v2, v2, Lcdc;->o:Ljava/lang/String;

    .line 415
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    move v7, v2

    .line 416
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 417
    iget-object v2, v2, Lcdc;->p:Ljava/lang/String;

    .line 418
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    .line 419
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lbuq;->c:Lcdc;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lbuq;->c(Lcdc;)Z

    move-result v8

    .line 420
    if-eqz v8, :cond_a

    .line 421
    const/4 v3, 0x0

    .line 431
    :goto_5
    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    iget-object v2, v2, Lbvp$d;->c:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v5, 0x1

    .line 432
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    iget-wide v14, v2, Lbvp$d;->n:J

    const-wide/16 v16, 0x1

    cmp-long v2, v14, v16

    if-nez v2, :cond_e

    const/4 v2, 0x1

    move v13, v2

    .line 433
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->e:Lcgm;

    move-object/from16 v21, v0

    new-instance v2, Lcgq;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbuq;->c:Lcdc;

    .line 434
    invoke-virtual {v6, v4}, Lcdc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 435
    move-object/from16 v0, p0

    iget-object v6, v0, Lbuq;->g:Lbvp$d;

    iget-boolean v6, v6, Lbvp$d;->r:Z

    .line 436
    if-eqz v5, :cond_f

    .line 437
    const/4 v6, 0x1

    .line 441
    :goto_8
    if-eqz v6, :cond_11

    .line 442
    move-object/from16 v0, p0

    iget-object v6, v0, Lbuq;->g:Lbvp$d;

    iget-object v6, v6, Lbvp$d;->d:Ljava/lang/String;

    .line 444
    :goto_9
    if-nez v7, :cond_5

    if-eqz v8, :cond_12

    :cond_5
    const/4 v7, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lbuq;->g:Lbvp$d;

    iget-object v8, v8, Lbvp$d;->f:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbuq;->g:Lbvp$d;

    iget v9, v9, Lbvp$d;->g:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lbuq;->g:Lbvp$d;

    iget-boolean v10, v10, Lbvp$d;->h:Z

    if-nez v12, :cond_6

    if-eqz v13, :cond_13

    :cond_6
    const/4 v12, 0x1

    :goto_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lbuq;->c:Lcdc;

    .line 446
    iget-boolean v13, v13, Lcdc;->s:Z

    .line 447
    move-object/from16 v0, p0

    iget-object v14, v0, Lbuq;->g:Lbvp$d;

    .line 449
    iget-object v14, v14, Lbvp$d;->m:Lbkm$a;

    sget-object v15, Lbkm$a;->c:Lbkm$a;

    if-ne v14, v15, :cond_14

    const/4 v14, 0x1

    .line 450
    :goto_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lbuq;->c:Lcdc;

    .line 452
    invoke-virtual {v15}, Lcdc;->n()Landroid/os/Bundle;

    move-result-object v15

    .line 453
    if-eqz v15, :cond_7

    const-string v16, "android.telecom.extra.ANSWERING_DROPS_FG_CALL"

    .line 454
    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_15

    .line 455
    :cond_7
    const/4 v15, 0x0

    .line 458
    :goto_d
    invoke-direct/range {p0 .. p0}, Lbuq;->v()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->g:Lbvp$d;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lbvp$d;->l:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lbuq;->c:Lcdc;

    move-object/from16 v20, v0

    .line 459
    invoke-virtual/range {v20 .. v20}, Lcdc;->k()I

    move-result v20

    invoke-direct/range {v2 .. v20}, Lcgq;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;IZZZZZZZLjava/lang/String;Lbln;ZI)V

    .line 460
    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lcgm;->a(Lcgq;)V

    goto/16 :goto_2

    .line 415
    :cond_8
    const/4 v2, 0x0

    move v7, v2

    goto/16 :goto_3

    .line 418
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 422
    :cond_a
    if-eqz v7, :cond_b

    .line 423
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->a:Landroid/content/Context;

    const v3, 0x7f1100d0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lbuq;->c:Lcdc;

    .line 424
    iget-object v9, v9, Lcdc;->o:Ljava/lang/String;

    .line 425
    aput-object v9, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 426
    :cond_b
    if-eqz v2, :cond_c

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->c:Lcdc;

    .line 428
    iget-object v3, v2, Lcdc;->p:Ljava/lang/String;

    goto/16 :goto_5

    .line 430
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->g:Lbvp$d;

    iget-object v3, v2, Lbvp$d;->c:Ljava/lang/String;

    goto/16 :goto_5

    .line 431
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 432
    :cond_e
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_7

    .line 438
    :cond_f
    if-eqz v6, :cond_10

    .line 439
    const/4 v6, 0x1

    goto/16 :goto_8

    .line 440
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 443
    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 444
    :cond_12
    move-object/from16 v0, p0

    iget-object v7, v0, Lbuq;->g:Lbvp$d;

    iget-object v7, v7, Lbvp$d;->e:Ljava/lang/String;

    goto/16 :goto_a

    :cond_13
    const/4 v12, 0x0

    goto/16 :goto_b

    .line 449
    :cond_14
    const/4 v14, 0x0

    goto/16 :goto_c

    .line 456
    :cond_15
    const-string v16, "android.telecom.extra.ANSWERING_DROPS_FG_CALL"

    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    goto :goto_d

    .line 462
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lbuq;->e:Lcgm;

    invoke-static {}, Lcgq;->a()Lcgq;

    move-result-object v3

    invoke-interface {v2, v3}, Lcgm;->a(Lcgq;)V

    goto/16 :goto_2

    .line 465
    :cond_17
    const-string v2, "CallCardPresenter.updatePrimaryDisplayInfo"

    const-string v3, "UI not ready, not showing location"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private final u()Lip;
    .locals 3

    .prologue
    .line 467
    invoke-direct {p0}, Lbuq;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    const/4 v0, 0x0

    .line 470
    :goto_0
    return-object v0

    .line 469
    :cond_0
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v1, "returning location fragment"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    iget-object v0, p0, Lbuq;->l:Lcds;

    iget-object v1, p0, Lbuq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcds;->b(Landroid/content/Context;)Lip;

    move-result-object v0

    goto :goto_0
.end method

.method private final v()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 471
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v3, "config_enable_emergency_location"

    .line 472
    invoke-interface {v0, v3, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 473
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "disabled by config."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 504
    :goto_0
    return v0

    .line 476
    :cond_0
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-static {v0}, Lbuq;->a(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    const-string v0, "CallCardPresenter.shouldShowLocation"

    const-string v3, "new emergency call"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 486
    :goto_1
    if-nez v0, :cond_4

    .line 487
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "shouldn\'t show location"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 488
    goto :goto_0

    .line 479
    :cond_1
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-static {v0}, Lbuq;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 480
    const-string v0, "CallCardPresenter.shouldShowLocation"

    const-string v3, "potential emergency callback"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 481
    goto :goto_1

    .line 482
    :cond_2
    iget-object v0, p0, Lbuq;->d:Lcdc;

    invoke-static {v0}, Lbuq;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 483
    const-string v0, "CallCardPresenter.shouldShowLocation"

    const-string v3, "has potential emergency callback"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 484
    goto :goto_1

    :cond_3
    move v0, v1

    .line 485
    goto :goto_1

    .line 489
    :cond_4
    invoke-direct {p0}, Lbuq;->w()Z

    move-result v0

    if-nez v0, :cond_5

    .line 490
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "no location permission."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 491
    goto :goto_0

    .line 492
    :cond_5
    invoke-direct {p0}, Lbuq;->x()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 493
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "low battery."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 494
    goto :goto_0

    .line 495
    :cond_6
    iget-object v0, p0, Lbuq;->e:Lcgm;

    if-nez v0, :cond_7

    const/4 v0, 0x0

    throw v0

    :cond_7
    check-cast v0, Lip;

    invoke-virtual {v0}, Lip;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 496
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "in multi-window mode"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 497
    goto/16 :goto_0

    .line 498
    :cond_8
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->u()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 499
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "emergency video calls not supported"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 500
    goto/16 :goto_0

    .line 501
    :cond_9
    iget-object v0, p0, Lbuq;->l:Lcds;

    iget-object v3, p0, Lbuq;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Lcds;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 502
    const-string v0, "CallCardPresenter.getLocationFragment"

    const-string v2, "can\'t get current location"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 503
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 504
    goto/16 :goto_0
.end method

.method private final w()Z
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final x()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 514
    iget-object v1, p0, Lbuq;->a:Landroid/content/Context;

    const/4 v2, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 515
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 516
    const-string v2, "status"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 517
    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 526
    :cond_0
    :goto_0
    return v0

    .line 519
    :cond_1
    const-string v2, "level"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 520
    const-string v3, "scale"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 521
    const/high16 v3, 0x42c80000    # 100.0f

    int-to-float v2, v2

    mul-float/2addr v2, v3

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 522
    iget-object v2, p0, Lbuq;->a:Landroid/content/Context;

    .line 523
    invoke-static {v2}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "min_battery_percent_for_emergency_location"

    const-wide/16 v4, 0xa

    .line 524
    invoke-interface {v2, v3, v4, v5}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 525
    const-string v4, "CallCardPresenter.isBatteryTooLowForEmergencyLocation"

    const/16 v5, 0x4b

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "percent charged: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", min required charge: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 526
    long-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final y()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 527
    iget-object v0, p0, Lbuq;->e:Lcgm;

    if-nez v0, :cond_0

    .line 558
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v0, p0, Lbuq;->d:Lcdc;

    if-nez v0, :cond_1

    .line 530
    iget-object v0, p0, Lbuq;->e:Lcgm;

    iget-boolean v1, p0, Lbuq;->j:Z

    invoke-static {v1}, Lcgr;->a(Z)Lcgr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcgm;->a(Lcgr;)V

    goto :goto_0

    .line 532
    :cond_1
    iget-object v0, p0, Lbuq;->d:Lcdc;

    .line 533
    iget-boolean v0, v0, Lcdc;->K:Z

    .line 534
    if-eqz v0, :cond_2

    .line 535
    const-string v0, "CallCardPresenter.updateSecondaryDisplayInfo"

    const-string v1, "secondary call is merge in process, clearing info"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    iget-object v0, p0, Lbuq;->e:Lcgm;

    iget-boolean v1, p0, Lbuq;->j:Z

    invoke-static {v1}, Lcgr;->a(Z)Lcgr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcgm;->a(Lcgr;)V

    goto :goto_0

    .line 538
    :cond_2
    iget-object v0, p0, Lbuq;->d:Lcdc;

    .line 539
    invoke-virtual {v0, v1}, Lcdc;->d(I)Z

    move-result v0

    .line 540
    if-eqz v0, :cond_3

    .line 541
    iget-object v9, p0, Lbuq;->e:Lcgm;

    new-instance v0, Lcgr;

    iget-object v2, p0, Lbuq;->a:Landroid/content/Context;

    iget-object v4, p0, Lbuq;->d:Lcdc;

    const/4 v5, 0x2

    .line 542
    invoke-virtual {v4, v5}, Lcdc;->d(I)Z

    move-result v4

    .line 543
    invoke-static {v2, v4}, Lbve;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v5, p0, Lbuq;->d:Lcdc;

    .line 544
    invoke-virtual {v5}, Lcdc;->E()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lbuq;->d:Lcdc;

    .line 545
    invoke-virtual {v6}, Lcdc;->u()Z

    move-result v7

    iget-boolean v8, p0, Lbuq;->j:Z

    move v6, v1

    invoke-direct/range {v0 .. v8}, Lcgr;-><init>(ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZ)V

    .line 546
    invoke-interface {v9, v0}, Lcgm;->a(Lcgr;)V

    goto :goto_0

    .line 547
    :cond_3
    iget-object v0, p0, Lbuq;->h:Lbvp$d;

    if-eqz v0, :cond_5

    .line 548
    iget-object v0, p0, Lbuq;->h:Lbvp$d;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    iget-object v0, p0, Lbuq;->h:Lbvp$d;

    invoke-direct {p0, v0}, Lbuq;->a(Lbvp$d;)Ljava/lang/String;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_4

    iget-object v2, p0, Lbuq;->h:Lbvp$d;

    iget-object v2, v2, Lbvp$d;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v7, v1

    .line 551
    :goto_1
    iget-object v2, p0, Lbuq;->e:Lcgm;

    new-instance v4, Lcgr;

    iget-object v5, p0, Lbuq;->d:Lcdc;

    .line 552
    invoke-virtual {v5, v0}, Lcdc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lbuq;->h:Lbvp$d;

    iget-object v8, v0, Lbvp$d;->e:Ljava/lang/String;

    iget-object v0, p0, Lbuq;->d:Lcdc;

    .line 553
    invoke-virtual {v0}, Lcdc;->E()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lbuq;->d:Lcdc;

    .line 554
    invoke-virtual {v0}, Lcdc;->u()Z

    move-result v11

    iget-boolean v12, p0, Lbuq;->j:Z

    move v5, v1

    move v10, v3

    invoke-direct/range {v4 .. v12}, Lcgr;-><init>(ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZ)V

    .line 555
    invoke-interface {v2, v4}, Lcgm;->a(Lcgr;)V

    goto/16 :goto_0

    :cond_4
    move v7, v3

    .line 550
    goto :goto_1

    .line 557
    :cond_5
    iget-object v0, p0, Lbuq;->e:Lcgm;

    iget-boolean v1, p0, Lbuq;->j:Z

    invoke-static {v1}, Lcgr;->a(Z)Lcgr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcgm;->a(Lcgr;)V

    goto/16 :goto_0
.end method

.method private final z()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 559
    iget-object v1, p0, Lbuq;->a:Landroid/content/Context;

    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-static {v1, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    :goto_0
    return-object v0

    .line 561
    :cond_0
    iget-object v1, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v1}, Lcdc;->d()Landroid/telecom/StatusHints;

    move-result-object v1

    .line 562
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/telecom/StatusHints;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 563
    invoke-virtual {v1}, Landroid/telecom/StatusHints;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 564
    :cond_1
    invoke-direct {p0}, Lbuq;->A()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 565
    iget-object v1, p0, Lbuq;->e:Lcgm;

    .line 566
    if-eqz v1, :cond_2

    .line 567
    iget-object v1, p0, Lbuq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 568
    :try_start_0
    iget-object v2, p0, Lbuq;->c:Lcdc;

    .line 569
    invoke-virtual {v2}, Lcdc;->q()Landroid/telecom/GatewayInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/GatewayInfo;->getGatewayProviderPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 570
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 571
    :catch_0
    move-exception v1

    .line 572
    const-string v2, "CallCardPresenter.getConnectionLabel"

    const-string v3, "gateway Application Not Found."

    invoke-static {v2, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 574
    :cond_2
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->E()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method final a(Lbvp$d;Z)V
    .locals 0

    .prologue
    .line 346
    if-eqz p2, :cond_0

    .line 347
    iput-object p1, p0, Lbuq;->g:Lbvp$d;

    .line 348
    invoke-direct {p0}, Lbuq;->t()V

    .line 351
    :goto_0
    return-void

    .line 349
    :cond_0
    iput-object p1, p0, Lbuq;->h:Lbvp$d;

    .line 350
    invoke-direct {p0}, Lbuq;->y()V

    goto :goto_0
.end method

.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 11

    .prologue
    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 76
    iget-object v0, p0, Lbuq;->e:Lcgm;

    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    const/4 v1, 0x0

    .line 79
    const/4 v0, 0x0

    .line 80
    sget-object v2, Lbwp;->b:Lbwp;

    if-ne p2, v2, :cond_11

    .line 81
    invoke-virtual {p3}, Lcct;->i()Lcdc;

    move-result-object v1

    .line 93
    :cond_2
    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "primary call: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "secondary call: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v2, p0, Lbuq;->c:Lcdc;

    .line 96
    invoke-static {v2, v1}, Lcdc;->a(Lcdc;Lcdc;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbuq;->c:Lcdc;

    invoke-static {v2, v1}, Lcdc;->b(Lcdc;Lcdc;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_3
    const/4 v2, 0x1

    .line 97
    :goto_2
    iget-object v3, p0, Lbuq;->d:Lcdc;

    .line 98
    invoke-static {v3, v0}, Lcdc;->a(Lcdc;Lcdc;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lbuq;->d:Lcdc;

    .line 99
    invoke-static {v3, v0}, Lcdc;->b(Lcdc;Lcdc;)Z

    move-result v3

    if-nez v3, :cond_16

    :cond_4
    const/4 v3, 0x1

    .line 100
    :goto_3
    iput-object v0, p0, Lbuq;->d:Lcdc;

    .line 101
    iget-object v4, p0, Lbuq;->c:Lcdc;

    .line 102
    iput-object v1, p0, Lbuq;->c:Lcdc;

    .line 103
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-eqz v0, :cond_7

    .line 104
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    iget-object v5, p0, Lbuq;->c:Lcdc;

    .line 105
    iget-object v6, v0, Lbwg;->x:Lbxf;

    iget-object v7, v0, Lbwg;->g:Landroid/content/Context;

    .line 106
    if-nez v5, :cond_17

    .line 107
    iget-object v8, v6, Lbxf;->g:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v7, v8}, Lbxf;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lbxf;->a(Landroid/content/Context;IZ)V

    .line 113
    :goto_4
    iget-object v6, v0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v6, :cond_6

    .line 114
    iget-object v0, v0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    .line 115
    iget-object v6, v0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v6}, Lbvy;->e()V

    .line 116
    iget-boolean v6, v0, Lcom/android/incallui/InCallActivity;->g:Z

    if-eqz v6, :cond_18

    if-eqz v5, :cond_18

    .line 117
    invoke-virtual {v5}, Lcdc;->j()I

    move-result v0

    const/16 v6, 0xa

    if-eq v0, v6, :cond_5

    .line 118
    invoke-virtual {v5}, Lcdc;->j()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_6

    .line 119
    :cond_5
    const-string v0, "InCallActivity.onForegroundCallChanged"

    const-string v5, "rejecting incoming call, not updating window background color"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :cond_6
    :goto_5
    iget-object v0, p0, Lbuq;->e:Lcgm;

    invoke-interface {v0}, Lcgm;->aa()V

    .line 122
    :cond_7
    if-eqz v2, :cond_8

    invoke-static {v1}, Lbuq;->d(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 123
    iget-object v0, p0, Lbuq;->e:Lcgm;

    invoke-interface {v0}, Lcgm;->Z()V

    .line 125
    :cond_8
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-eqz v0, :cond_19

    .line 126
    if-nez v2, :cond_9

    iget-object v0, p0, Lbuq;->e:Lcgm;

    .line 127
    invoke-interface {v0}, Lcgm;->Y()Z

    move-result v0

    invoke-direct {p0}, Lbuq;->s()Z

    move-result v1

    if-eq v0, v1, :cond_19

    :cond_9
    const/4 v0, 0x1

    .line 128
    :goto_6
    if-eqz v0, :cond_b

    .line 129
    if-eqz v4, :cond_a

    .line 130
    invoke-virtual {v4, p0}, Lcdc;->b(Lcdi;)V

    .line 131
    :cond_a
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0, p0}, Lcdc;->a(Lcdi;)V

    .line 132
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    iget-object v1, p0, Lbuq;->c:Lcdc;

    iget-object v5, p0, Lbuq;->c:Lcdc;

    .line 133
    invoke-virtual {v5}, Lcdc;->j()I

    .line 134
    invoke-static {v0, v1}, Lbvp;->a(Landroid/content/Context;Lcdc;)Lbvp$d;

    move-result-object v0

    iput-object v0, p0, Lbuq;->g:Lbvp$d;

    .line 135
    invoke-direct {p0}, Lbuq;->t()V

    .line 136
    iget-object v0, p0, Lbuq;->c:Lcdc;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbuq;->a(Lcdc;Z)V

    .line 137
    :cond_b
    if-eqz v4, :cond_c

    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-nez v0, :cond_c

    .line 138
    invoke-virtual {v4, p0}, Lcdc;->b(Lcdi;)V

    .line 139
    :cond_c
    if-eqz v3, :cond_d

    .line 140
    iget-object v0, p0, Lbuq;->d:Lcdc;

    if-nez v0, :cond_1a

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lbuq;->h:Lbvp$d;

    .line 142
    invoke-direct {p0}, Lbuq;->y()V

    .line 148
    :cond_d
    :goto_7
    const/4 v0, 0x2

    .line 149
    iget-object v1, p0, Lbuq;->c:Lcdc;

    if-eqz v1, :cond_1b

    .line 150
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->j()I

    move-result v0

    .line 151
    invoke-direct {p0}, Lbuq;->q()V

    .line 156
    :goto_8
    invoke-direct {p0}, Lbuq;->r()V

    .line 158
    iget-object v1, p0, Lbuq;->e:Lcgm;

    .line 159
    iget-object v3, p0, Lbuq;->c:Lcdc;

    .line 161
    if-nez v3, :cond_1c

    .line 162
    const/4 v0, 0x0

    .line 168
    :goto_9
    invoke-interface {v1, v0}, Lcgm;->a(Z)V

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbuq;->f:Z

    .line 171
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    const-string v1, "accessibility"

    .line 173
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 174
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    sget-object v0, Lbwp;->f:Lbwp;

    if-eq p1, v0, :cond_e

    sget-object v0, Lbwp;->f:Lbwp;

    if-eq p2, v0, :cond_10

    :cond_e
    sget-object v0, Lbwp;->b:Lbwp;

    if-eq p1, v0, :cond_f

    sget-object v0, Lbwp;->b:Lbwp;

    if-eq p2, v0, :cond_10

    :cond_f
    if-eqz v2, :cond_0

    .line 176
    :cond_10
    const-string v0, "CallCardPresenter.maybeSendAccessibilityEvent"

    const-string v1, "schedule accessibility announcement"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbuq;->f:Z

    .line 178
    iget-object v0, p0, Lbuq;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbuq;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 82
    :cond_11
    sget-object v2, Lbwp;->e:Lbwp;

    if-eq p2, v2, :cond_12

    sget-object v2, Lbwp;->f:Lbwp;

    if-ne p2, v2, :cond_14

    .line 83
    :cond_12
    invoke-virtual {p3}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 84
    if-nez v0, :cond_13

    .line 86
    const/16 v0, 0xd

    .line 87
    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 89
    :cond_13
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p3, v1, v2}, Lbuq;->a(Lcct;Lcdc;Z)Lcdc;

    move-result-object v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_1

    .line 90
    :cond_14
    sget-object v2, Lbwp;->c:Lbwp;

    if-ne p2, v2, :cond_2

    .line 91
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lbuq;->a(Lcct;Lcdc;Z)Lcdc;

    move-result-object v1

    .line 92
    const/4 v0, 0x1

    invoke-static {p3, v1, v0}, Lbuq;->a(Lcct;Lcdc;Z)Lcdc;

    move-result-object v0

    goto/16 :goto_1

    .line 96
    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 99
    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 109
    :cond_17
    invoke-virtual {v5}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v8

    invoke-static {v7, v8}, Lbxf;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I

    move-result v8

    .line 111
    iget-boolean v9, v5, Lcdc;->s:Z

    .line 112
    invoke-virtual {v6, v7, v8, v9}, Lbxf;->a(Landroid/content/Context;IZ)V

    goto/16 :goto_4

    .line 120
    :cond_18
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/android/incallui/InCallActivity;->a(F)V

    goto/16 :goto_5

    .line 127
    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 143
    :cond_1a
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    iget-object v1, p0, Lbuq;->d:Lcdc;

    iget-object v3, p0, Lbuq;->d:Lcdc;

    .line 144
    invoke-virtual {v3}, Lcdc;->j()I

    .line 145
    invoke-static {v0, v1}, Lbvp;->a(Landroid/content/Context;Lcdc;)Lbvp$d;

    move-result-object v0

    iput-object v0, p0, Lbuq;->h:Lbvp$d;

    .line 146
    invoke-direct {p0}, Lbuq;->y()V

    .line 147
    iget-object v0, p0, Lbuq;->d:Lcdc;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbuq;->a(Lcdc;Z)V

    goto/16 :goto_7

    .line 153
    :cond_1b
    iget-object v1, p0, Lbuq;->e:Lcgm;

    .line 154
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcgp;->a(ILjava/lang/String;)Lcgp;

    move-result-object v3

    .line 155
    invoke-interface {v1, v3}, Lcgm;->a(Lcgp;)V

    goto/16 :goto_8

    .line 163
    :cond_1c
    invoke-static {v0}, Lbvs;->b(I)Z

    move-result v3

    if-nez v3, :cond_1d

    const/16 v3, 0x9

    if-eq v0, v3, :cond_1d

    const/16 v3, 0xa

    if-ne v0, v3, :cond_1e

    :cond_1d
    const/4 v3, 0x4

    if-ne v0, v3, :cond_1f

    .line 164
    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 165
    :cond_1f
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_20

    .line 166
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 167
    :cond_20
    const/4 v0, 0x1

    goto/16 :goto_9
.end method

.method public final a(Lbwp;Lbwp;Lcdc;)V
    .locals 1

    .prologue
    .line 71
    .line 72
    sget-object v0, Lcct;->a:Lcct;

    .line 73
    invoke-virtual {p0, p1, p2, v0}, Lbuq;->a(Lbwp;Lbwp;Lcct;)V

    .line 74
    return-void
.end method

.method public final a(Lcdc;Landroid/telecom/Call$Details;)V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 180
    invoke-direct {p0}, Lbuq;->q()V

    .line 181
    invoke-virtual {p1, v1}, Lcdc;->c(I)Z

    move-result v0

    .line 182
    invoke-virtual {p2, v1}, Landroid/telecom/Call$Details;->can(I)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 183
    invoke-direct {p0}, Lbuq;->r()V

    .line 184
    :cond_0
    return-void
.end method

.method public final a(Lcgm;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 12
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    iput-object p1, p0, Lbuq;->e:Lcgm;

    .line 14
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v0

    iput-object v0, p0, Lbuq;->i:Lalj;

    .line 15
    sget-object v0, Lcct;->a:Lcct;

    .line 16
    invoke-virtual {v0}, Lcct;->j()Lcdc;

    move-result-object v2

    .line 17
    if-eqz v2, :cond_1

    .line 18
    iput-object v2, p0, Lbuq;->c:Lcdc;

    .line 19
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-static {v0}, Lbuq;->d(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lbuq;->e:Lcgm;

    invoke-interface {v0}, Lcgm;->Z()V

    .line 21
    :cond_0
    invoke-virtual {v2, p0}, Lcdc;->a(Lcdi;)V

    .line 23
    invoke-virtual {v2, v1}, Lcdc;->d(I)Z

    move-result v0

    .line 24
    if-nez v0, :cond_3

    .line 25
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0, v2, v1, v0}, Lbuq;->a(Lcdc;ZZ)V

    .line 27
    :cond_1
    :goto_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 28
    iget-object v0, v0, Lbwg;->n:Lbwp;

    .line 29
    sget-object v1, Lcct;->a:Lcct;

    .line 30
    invoke-virtual {p0, v4, v0, v1}, Lbuq;->a(Lbwp;Lbwp;Lcct;)V

    .line 31
    return-void

    .line 25
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :cond_3
    invoke-virtual {p0, v4, v1}, Lbuq;->a(Lbvp$d;Z)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 604
    iput-boolean p1, p0, Lbuq;->j:Z

    .line 605
    iget-object v0, p0, Lbuq;->e:Lcgm;

    if-nez v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 607
    :cond_0
    invoke-direct {p0}, Lbuq;->r()V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-direct {p0}, Lbuq;->t()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-nez v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-direct {p0}, Lbuq;->t()V

    .line 200
    invoke-direct {p0}, Lbuq;->q()V

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 203
    const-string v0, "CallCardPresenter.onDialerCallSessionModificationStateChange"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v1, p0, Lbuq;->e:Lcgm;

    .line 208
    iget-object v0, p0, Lbuq;->c:Lcdc;

    .line 209
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->g()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    .line 210
    :goto_1
    invoke-interface {v1, v0}, Lcgm;->a(Z)V

    .line 211
    invoke-direct {p0}, Lbuq;->q()V

    goto :goto_0

    .line 209
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 190
    const-string v0, "CallCardPresenter.onEnrichedCallSessionUpdate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 191
    invoke-direct {p0}, Lbuq;->t()V

    .line 192
    return-void
.end method

.method public final k()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 32
    const-string v2, "CallCardPresenter.onInCallScreenReady"

    const/4 v3, 0x0

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    iget-boolean v2, p0, Lbuq;->k:Z

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 34
    iget-object v0, p0, Lbuq;->i:Lalj;

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lbuq;->i:Lalj;

    const-string v2, "android.contacts.DISPLAY_ORDER"

    invoke-virtual {v0, v2}, Lalj;->a(Ljava/lang/String;)V

    .line 36
    :cond_1
    iget-object v0, p0, Lbuq;->g:Lbvp$d;

    if-eqz v0, :cond_2

    .line 37
    invoke-direct {p0}, Lbuq;->t()V

    .line 38
    :cond_2
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 39
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwt;)V

    .line 40
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwm;)V

    .line 41
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwn;)V

    .line 42
    iput-boolean v1, p0, Lbuq;->k:Z

    .line 43
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-static {v0}, Lbuq;->a(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 44
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bv:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 47
    :cond_3
    :goto_0
    invoke-direct {p0}, Lbuq;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 48
    iget-object v0, p0, Lbuq;->e:Lcgm;

    invoke-direct {p0}, Lbuq;->u()Lip;

    move-result-object v1

    invoke-interface {v0, v1}, Lcgm;->a(Lip;)V

    .line 49
    invoke-direct {p0}, Lbuq;->w()Z

    move-result v0

    if-nez v0, :cond_7

    .line 50
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bx:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 56
    :cond_4
    :goto_1
    return-void

    .line 45
    :cond_5
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-static {v0}, Lbuq;->b(Lcdc;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lbuq;->d:Lcdc;

    invoke-static {v0}, Lbuq;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 46
    :cond_6
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bw:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0

    .line 51
    :cond_7
    invoke-direct {p0}, Lbuq;->x()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 52
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->by:Lbkq$a;

    .line 53
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_1

    .line 54
    :cond_8
    iget-object v0, p0, Lbuq;->l:Lcds;

    iget-object v1, p0, Lbuq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcds;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 55
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bz:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_1
.end method

.method public final l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 57
    const-string v0, "CallCardPresenter.onInCallScreenUnready"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    iget-boolean v0, p0, Lbuq;->k:Z

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 59
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 60
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwt;)V

    .line 61
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwm;)V

    .line 62
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwn;)V

    .line 63
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0, p0}, Lcdc;->b(Lcdi;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lbuq;->l:Lcds;

    invoke-virtual {v0}, Lcds;->a()V

    .line 66
    iput-object v2, p0, Lbuq;->c:Lcdc;

    .line 67
    iput-object v2, p0, Lbuq;->g:Lbvp$d;

    .line 68
    iput-object v2, p0, Lbuq;->h:Lbvp$d;

    .line 69
    iput-boolean v3, p0, Lbuq;->k:Z

    .line 70
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lbuq;->e:Lcgm;

    .line 335
    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    check-cast v0, Lip;

    invoke-virtual {v0}, Lip;->h()Lit;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/InCallActivity;

    .line 336
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->b(Z)V

    .line 337
    return-void
.end method

.method public final n()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 586
    iget-object v0, p0, Lbuq;->d:Lcdc;

    if-nez v0, :cond_0

    .line 587
    const-string v0, "CallCardPresenter.onSecondaryInfoClicked"

    const-string v1, "secondary info clicked but no secondary call."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 598
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->do:Lbkq$a;

    iget-object v2, p0, Lbuq;->c:Lcdc;

    .line 591
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 592
    iget-object v3, p0, Lbuq;->c:Lcdc;

    .line 594
    iget-wide v4, v3, Lcdc;->M:J

    .line 595
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 596
    const-string v0, "CallCardPresenter.onSecondaryInfoClicked"

    iget-object v1, p0, Lbuq;->d:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "swapping call to foreground: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    iget-object v0, p0, Lbuq;->d:Lcdc;

    invoke-virtual {v0}, Lcdc;->C()V

    goto :goto_0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 599
    const-string v0, "CallCardPresenter.onEndCallClicked"

    iget-object v1, p0, Lbuq;->c:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "disconnecting call: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 600
    iget-object v0, p0, Lbuq;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lbuq;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->B()V

    .line 602
    :cond_0
    iget-object v0, p0, Lbuq;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->A(Landroid/content/Context;)V

    .line 603
    return-void
.end method

.method public final p()V
    .locals 4

    .prologue
    .line 609
    invoke-direct {p0}, Lbuq;->t()V

    .line 610
    iget-boolean v0, p0, Lbuq;->f:Z

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lbuq;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbuq;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 612
    :cond_0
    return-void
.end method
