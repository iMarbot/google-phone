.class public final Lcte;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldfi;


# static fields
.field private static f:Ldgn;


# instance fields
.field public final a:Lcsw;

.field public final b:Ldfh;

.field public final c:Ldfp;

.field public final d:Ldfs;

.field public e:Ldgn;

.field private g:Landroid/content/Context;

.field private h:Ldfo;

.field private i:Ljava/lang/Runnable;

.field private j:Landroid/os/Handler;

.field private k:Ldfb;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 102
    const-class v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Ldgn;->a(Ljava/lang/Class;)Ldgn;

    move-result-object v0

    .line 103
    iput-boolean v2, v0, Ldgn;->s:Z

    .line 105
    sput-object v0, Lcte;->f:Ldgn;

    .line 106
    const-class v0, Ldeh;

    invoke-static {v0}, Ldgn;->a(Ljava/lang/Class;)Ldgn;

    move-result-object v0

    .line 107
    iput-boolean v2, v0, Ldgn;->s:Z

    .line 108
    sget-object v0, Lcvx;->b:Lcvx;

    .line 109
    invoke-static {v0}, Ldgn;->a(Lcvx;)Ldgn;

    move-result-object v0

    sget-object v1, Lcsz;->d:Lcsz;

    invoke-virtual {v0, v1}, Ldgn;->a(Lcsz;)Ldgn;

    move-result-object v0

    .line 110
    invoke-virtual {v0, v2}, Ldgn;->b(Z)Ldgn;

    .line 111
    return-void
.end method

.method public constructor <init>(Lcsw;Ldfh;Ldfo;Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1
    new-instance v4, Ldfp;

    invoke-direct {v4}, Ldfp;-><init>()V

    .line 3
    iget-object v5, p1, Lcsw;->f:Ldfd;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    .line 5
    invoke-direct/range {v0 .. v6}, Lcte;-><init>(Lcsw;Ldfh;Ldfo;Ldfp;Ldfd;Landroid/content/Context;)V

    .line 6
    return-void
.end method

.method private constructor <init>(Lcsw;Ldfh;Ldfo;Ldfp;Ldfd;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ldfs;

    invoke-direct {v0}, Ldfs;-><init>()V

    iput-object v0, p0, Lcte;->d:Ldfs;

    .line 9
    new-instance v0, Lctf;

    invoke-direct {v0, p0}, Lctf;-><init>(Lcte;)V

    iput-object v0, p0, Lcte;->i:Ljava/lang/Runnable;

    .line 10
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcte;->j:Landroid/os/Handler;

    .line 11
    iput-object p1, p0, Lcte;->a:Lcsw;

    .line 12
    iput-object p2, p0, Lcte;->b:Ldfh;

    .line 13
    iput-object p3, p0, Lcte;->h:Ldfo;

    .line 14
    iput-object p4, p0, Lcte;->c:Ldfp;

    .line 15
    iput-object p6, p0, Lcte;->g:Landroid/content/Context;

    .line 17
    invoke-virtual {p6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ldfc;

    invoke-direct {v1, p4}, Ldfc;-><init>(Ldfp;)V

    .line 18
    invoke-virtual {p5, v0, v1}, Ldfd;->a(Landroid/content/Context;Ldfc;)Ldfb;

    move-result-object v0

    iput-object v0, p0, Lcte;->k:Ldfb;

    .line 19
    invoke-static {}, Ldhw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcte;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcte;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 22
    :goto_0
    iget-object v0, p0, Lcte;->k:Ldfb;

    invoke-interface {p2, v0}, Ldfh;->a(Ldfi;)V

    .line 24
    iget-object v0, p1, Lcsw;->b:Lcsy;

    .line 26
    iget-object v0, v0, Lcsy;->d:Ldgn;

    .line 28
    invoke-virtual {v0}, Ldgn;->a()Ldgn;

    move-result-object v0

    invoke-virtual {v0}, Ldgn;->c()Ldgn;

    move-result-object v0

    iput-object v0, p0, Lcte;->e:Ldgn;

    .line 30
    iget-object v1, p1, Lcsw;->g:Ljava/util/List;

    monitor-enter v1

    .line 31
    :try_start_0
    iget-object v0, p1, Lcsw;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot register already registered manager"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 21
    :cond_0
    invoke-interface {p2, p0}, Ldfh;->a(Ldfi;)V

    goto :goto_0

    .line 33
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcsw;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private a(Ljava/lang/Class;)Lctb;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lctb;

    iget-object v1, p0, Lcte;->a:Lcsw;

    iget-object v2, p0, Lcte;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p1, v2}, Lctb;-><init>(Lcsw;Lcte;Ljava/lang/Class;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lctb;
    .locals 1

    .prologue
    .line 76
    .line 77
    const-class v0, Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcte;->a(Ljava/lang/Class;)Lctb;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p1}, Lctb;->a(Ljava/lang/Object;)Lctb;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 35
    .line 36
    invoke-static {}, Ldhw;->a()V

    .line 37
    iget-object v1, p0, Lcte;->c:Ldfp;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, v1, Ldfp;->c:Z

    .line 39
    iget-object v0, v1, Ldfp;->a:Ljava/util/Set;

    invoke-static {v0}, Ldhw;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgh;

    .line 40
    invoke-interface {v0}, Ldgh;->f()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Ldgh;->g()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, Ldgh;->e()Z

    move-result v3

    if-nez v3, :cond_0

    .line 41
    invoke-interface {v0}, Ldgh;->a()V

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, v1, Ldfp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 44
    iget-object v0, p0, Lcte;->d:Ldfs;

    invoke-virtual {v0}, Ldfs;->a()V

    .line 45
    return-void
.end method

.method public final a(Ldha;)V
    .locals 2

    .prologue
    .line 82
    if-nez p1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    invoke-static {}, Ldhw;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    invoke-virtual {p0, p1}, Lcte;->b(Ldha;)Z

    move-result v0

    .line 87
    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcte;->a:Lcsw;

    invoke-virtual {v0, p1}, Lcsw;->a(Ldha;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcte;->j:Landroid/os/Handler;

    new-instance v1, Lctg;

    invoke-direct {v1, p0, p1}, Lctg;-><init>(Lcte;Ldha;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 46
    .line 47
    invoke-static {}, Ldhw;->a()V

    .line 48
    iget-object v1, p0, Lcte;->c:Ldfp;

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, v1, Ldfp;->c:Z

    .line 50
    iget-object v0, v1, Ldfp;->a:Ljava/util/Set;

    invoke-static {v0}, Ldhw;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgh;

    .line 51
    invoke-interface {v0}, Ldgh;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 52
    invoke-interface {v0}, Ldgh;->c()V

    .line 53
    iget-object v3, v1, Ldfp;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :cond_1
    iget-object v0, p0, Lcte;->d:Ldfs;

    invoke-virtual {v0}, Ldfs;->b()V

    .line 56
    return-void
.end method

.method final b(Ldha;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 92
    invoke-interface {p1}, Ldha;->d()Ldgh;

    move-result-object v1

    .line 93
    if-nez v1, :cond_0

    .line 100
    :goto_0
    return v0

    .line 95
    :cond_0
    iget-object v2, p0, Lcte;->c:Ldfp;

    invoke-virtual {v2, v1}, Ldfp;->a(Ldgh;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    iget-object v1, p0, Lcte;->d:Ldfs;

    .line 97
    iget-object v1, v1, Ldfs;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 98
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ldha;->a(Ldgh;)V

    goto :goto_0

    .line 100
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcte;->d:Ldfs;

    invoke-virtual {v0}, Ldfs;->c()V

    .line 58
    iget-object v0, p0, Lcte;->d:Ldfs;

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, v0, Ldfs;->a:Ljava/util/Set;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 60
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldha;

    .line 61
    invoke-virtual {p0, v0}, Lcte;->a(Ldha;)V

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lcte;->d:Ldfs;

    .line 64
    iget-object v0, v0, Ldfs;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 65
    iget-object v0, p0, Lcte;->c:Ldfp;

    invoke-virtual {v0}, Ldfp;->a()V

    .line 66
    iget-object v0, p0, Lcte;->b:Ldfh;

    invoke-interface {v0, p0}, Ldfh;->b(Ldfi;)V

    .line 67
    iget-object v0, p0, Lcte;->b:Ldfh;

    iget-object v1, p0, Lcte;->k:Ldfb;

    invoke-interface {v0, v1}, Ldfh;->b(Ldfi;)V

    .line 68
    iget-object v0, p0, Lcte;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcte;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 69
    iget-object v0, p0, Lcte;->a:Lcsw;

    .line 70
    iget-object v1, v0, Lcsw;->g:Ljava/util/List;

    monitor-enter v1

    .line 71
    :try_start_0
    iget-object v2, v0, Lcsw;->g:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 72
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot unregister not yet registered manager"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 73
    :cond_1
    :try_start_1
    iget-object v0, v0, Lcsw;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 74
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final d()Lctb;
    .locals 2

    .prologue
    .line 75
    const-class v0, Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcte;->a(Ljava/lang/Class;)Lctb;

    move-result-object v0

    sget-object v1, Lcte;->f:Ldgn;

    invoke-virtual {v0, v1}, Lctb;->a(Ldgn;)Lctb;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 101
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcte;->c:Ldfp;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcte;->h:Ldfo;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "{tracker="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", treeNode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
