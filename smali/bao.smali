.class public final Lbao;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbao$a;
    }
.end annotation


# static fields
.field public static final b:Lbao;

.field private static volatile c:Lhdm;


# instance fields
.field public a:Lhce;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lbao;

    invoke-direct {v0}, Lbao;-><init>()V

    .line 74
    sput-object v0, Lbao;->b:Lbao;

    invoke-virtual {v0}, Lbao;->makeImmutable()V

    .line 75
    const-class v0, Lbao;

    sget-object v1, Lbao;->b:Lbao;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 76
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    invoke-static {}, Lbao;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lbao;->a:Lhce;

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lbao$a;

    aput-object v2, v0, v1

    .line 71
    const-string v1, "\u0001\u0001\u0000\u0000\u0001\u0001\u0000\u0001\u0000\u0001\u001b"

    .line 72
    sget-object v2, Lbao;->b:Lbao;

    invoke-static {v2, v1, v0}, Lbao;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 25
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 26
    :pswitch_0
    new-instance v0, Lbao;

    invoke-direct {v0}, Lbao;-><init>()V

    .line 68
    :goto_0
    :pswitch_1
    return-object v0

    .line 27
    :pswitch_2
    sget-object v0, Lbao;->b:Lbao;

    goto :goto_0

    .line 28
    :pswitch_3
    iget-object v1, p0, Lbao;->a:Lhce;

    invoke-interface {v1}, Lhce;->b()V

    goto :goto_0

    .line 30
    :pswitch_4
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v1, v1}, Lhbr$a;-><init>(BS)V

    goto :goto_0

    .line 31
    :pswitch_5
    check-cast p2, Lhaq;

    .line 32
    check-cast p3, Lhbg;

    .line 33
    if-nez p3, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35
    :cond_0
    :try_start_0
    sget-boolean v0, Lbao;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0, p2, p3}, Lbao;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 37
    sget-object v0, Lbao;->b:Lbao;

    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 39
    :cond_1
    :goto_1
    if-nez v1, :cond_3

    .line 40
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-virtual {p0, v0, p2}, Lbao;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 45
    goto :goto_1

    .line 46
    :sswitch_1
    iget-object v0, p0, Lbao;->a:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 47
    iget-object v0, p0, Lbao;->a:Lhce;

    .line 48
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lbao;->a:Lhce;

    .line 49
    :cond_2
    iget-object v3, p0, Lbao;->a:Lhce;

    .line 50
    sget-object v0, Lbao$a;->j:Lbao$a;

    .line 52
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lbao$a;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :catchall_0
    move-exception v0

    throw v0

    .line 57
    :catch_1
    move-exception v0

    .line 58
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 59
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    :cond_3
    :pswitch_6
    sget-object v0, Lbao;->b:Lbao;

    goto :goto_0

    .line 62
    :pswitch_7
    sget-object v0, Lbao;->c:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lbao;

    monitor-enter v1

    .line 63
    :try_start_3
    sget-object v0, Lbao;->c:Lhdm;

    if-nez v0, :cond_4

    .line 64
    new-instance v0, Lhaa;

    sget-object v2, Lbao;->b:Lbao;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbao;->c:Lhdm;

    .line 65
    :cond_4
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 66
    :cond_5
    sget-object v0, Lbao;->c:Lhdm;

    goto/16 :goto_0

    .line 65
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 67
    :pswitch_8
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 12
    iget v1, p0, Lbao;->memoizedSerializedSize:I

    .line 13
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 24
    :goto_0
    return v0

    .line 14
    :cond_0
    sget-boolean v1, Lbao;->usingExperimentalRuntime:Z

    if-eqz v1, :cond_1

    .line 15
    invoke-virtual {p0}, Lbao;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbao;->memoizedSerializedSize:I

    .line 16
    iget v0, p0, Lbao;->memoizedSerializedSize:I

    goto :goto_0

    :cond_1
    move v1, v0

    move v2, v0

    .line 18
    :goto_1
    iget-object v0, p0, Lbao;->a:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 19
    const/4 v3, 0x1

    iget-object v0, p0, Lbao;->a:Lhce;

    .line 20
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v2, v0

    .line 21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 22
    :cond_2
    iget-object v0, p0, Lbao;->unknownFields:Lheq;

    invoke-virtual {v0}, Lheq;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 23
    iput v0, p0, Lbao;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    .line 4
    sget-boolean v0, Lbao;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lbao;->writeToInternal(Lhaw;)V

    .line 11
    :goto_0
    return-void

    .line 7
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbao;->a:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 8
    const/4 v2, 0x1

    iget-object v0, p0, Lbao;->a:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 10
    :cond_1
    iget-object v0, p0, Lbao;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
