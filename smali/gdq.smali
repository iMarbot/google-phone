.class public final Lgdq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Lgdw;

.field private static c:Lgdx;

.field private static d:Lgdx;

.field private static e:Ljava/lang/Object;

.field private static f:Lgpm;


# instance fields
.field public a:Lgdq;

.field private g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Ljava/util/Map;

.field private j:Ljava/util/Map;

.field private k:Ljava/util/ArrayList;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 127
    new-instance v0, Lgdw;

    const-string v1, "debug.binder.verification"

    invoke-direct {v0, v1}, Lgdw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgdq;->b:Lgdw;

    .line 128
    new-instance v0, Lgdx;

    const-string v1, "test.binder.trace"

    invoke-direct {v0, v1}, Lgdx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgdq;->c:Lgdx;

    .line 129
    new-instance v0, Lgdx;

    const-string v1, "test.binder.detail_trace"

    invoke-direct {v0, v1}, Lgdx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgdq;->d:Lgdx;

    .line 130
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgdq;->e:Ljava/lang/Object;

    .line 131
    new-instance v0, Lgpm;

    const/4 v1, 0x0

    new-instance v2, Lhcw;

    invoke-direct {v2}, Lhcw;-><init>()V

    invoke-direct {v0, v1, v2}, Lgpm;-><init>(ZLhcw;)V

    sput-object v0, Lgdq;->f:Lgpm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgdq;->i:Ljava/util/Map;

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgdq;->j:Ljava/util/Map;

    .line 15
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgdq;->k:Ljava/util/ArrayList;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgdq;-><init>(Landroid/content/Context;Lgdq;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lgdq;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgdq;->i:Ljava/util/Map;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgdq;->j:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgdq;->k:Ljava/util/ArrayList;

    .line 8
    iput-object p1, p0, Lgdq;->g:Landroid/content/Context;

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lgdq;->a:Lgdq;

    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgdq;->h:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static a(Landroid/content/Context;)Lgdq;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 102
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move v2, v3

    move-object v1, p0

    .line 105
    :goto_0
    instance-of v0, v1, Lgdt;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 106
    check-cast v0, Lgdt;

    invoke-interface {v0}, Lgdt;->a()Lgdq;

    move-result-object v0

    .line 107
    if-nez v0, :cond_1

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "BinderContext must not return null Binder: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v5

    .line 112
    :cond_1
    if-eqz v0, :cond_2

    .line 123
    :goto_1
    return-object v0

    .line 114
    :cond_2
    if-ne v1, v4, :cond_3

    const/4 v0, 0x1

    :goto_2
    or-int/2addr v0, v2

    .line 115
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_4

    .line 116
    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 117
    if-nez v1, :cond_5

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid ContextWrapper -- If this is a Robolectric test, have you called ActivityController.create()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v3

    .line 114
    goto :goto_2

    .line 119
    :cond_4
    if-nez v0, :cond_6

    move-object v1, v4

    .line 122
    :cond_5
    :goto_3
    if-nez v1, :cond_7

    .line 123
    invoke-static {v4}, Lgdq;->b(Landroid/content/Context;)Lgdq;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v1, v5

    .line 121
    goto :goto_3

    :cond_7
    move v2, v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lgdq;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    .line 99
    invoke-virtual {v0, p1}, Lgdq;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private final a()V
    .locals 2

    .prologue
    .line 56
    iget-boolean v0, p0, Lgdq;->l:Z

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lgds;

    const-string v1, "This binder is sealed for modification"

    invoke-direct {v0, v1}, Lgds;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 126
    return-void
.end method

.method public static b(Landroid/content/Context;)Lgdq;
    .locals 2

    .prologue
    .line 124
    sget-object v0, Lgdq;->f:Lgpm;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpm;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    invoke-static {p0}, Lgdq;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    .line 101
    invoke-direct {v0, p1}, Lgdq;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    const-string v0, "GetOptional"

    invoke-static {p1, v0}, Lgdq;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 45
    :try_start_0
    invoke-direct {p0, p1}, Lgdq;->c(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 46
    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    throw v0
.end method

.method private final c(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_0
    invoke-direct {p0, p1}, Lgdq;->d(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 55
    :goto_0
    return-object v0

    .line 53
    :cond_1
    iget-object p0, p0, Lgdq;->a:Lgdq;

    .line 54
    if-nez p0, :cond_0

    .line 55
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final declared-synchronized d(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 70
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lgdq;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Binder not initialized yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 73
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgdq;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_3

    .line 75
    sget-object v1, Lgdq;->e:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v1, :cond_2

    .line 97
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :cond_3
    :try_start_2
    iget-boolean v2, p0, Lgdq;->l:Z

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdq;->l:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 78
    :try_start_3
    iget-object v0, p0, Lgdq;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 79
    :goto_1
    if-ge v1, v3, :cond_5

    .line 80
    iget-object v0, p0, Lgdq;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdu;

    iget-object v4, p0, Lgdq;->g:Landroid/content/Context;

    invoke-interface {v0, v4, p1, p0}, Lgdu;->a(Landroid/content/Context;Ljava/lang/Class;Lgdq;)V

    .line 81
    sget-object v0, Lgdq;->b:Lgdw;

    invoke-static {v0}, Lhcw;->a(Lgdw;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 82
    iget-object v0, p0, Lgdq;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 83
    if-eqz v0, :cond_4

    .line 85
    :try_start_4
    iput-boolean v2, p0, Lgdq;->l:Z

    goto :goto_0

    .line 87
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 88
    :cond_5
    iput-boolean v2, p0, Lgdq;->l:Z

    .line 91
    iget-object v0, p0, Lgdq;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 92
    if-nez v0, :cond_1

    .line 93
    sget-object v1, Lgdq;->b:Lgdw;

    invoke-static {v1}, Lhcw;->a(Lgdw;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 94
    iget-object v1, p0, Lgdq;->j:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 95
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x24

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "get() called for multibound object: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :catchall_1
    move-exception v0

    iput-boolean v2, p0, Lgdq;->l:Z

    throw v0

    .line 96
    :cond_6
    iget-object v1, p0, Lgdq;->i:Ljava/util/Map;

    sget-object v2, Lgdq;->e:Ljava/lang/Object;

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lgdu;)Lgdq;
    .locals 1

    .prologue
    .line 18
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lgdq;->a()V

    .line 19
    iget-object v0, p0, Lgdq;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    monitor-exit p0

    return-object p0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 21
    const-string v0, "Get"

    invoke-static {p1, v0}, Lgdq;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 23
    :try_start_0
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    invoke-direct {p0, p1}, Lgdq;->c(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 42
    return-object v0

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    const-string v1, "Unbound type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSearched binders:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    :goto_0
    iget-object v1, p0, Lgdq;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    iget-object p0, p0, Lgdq;->a:Lgdq;

    .line 34
    if-eqz p0, :cond_1

    .line 35
    const-string v1, " ->\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v0

    throw v0

    .line 36
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 39
    const-string v2, "Binder"

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 40
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lgdq;->a()V

    .line 60
    sget-object v0, Lgdq;->b:Lgdw;

    invoke-static {v0}, Lhcw;->a(Lgdw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lgdq;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Attempt to single-bind multibound object: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 63
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgdq;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_2

    .line 65
    sget-object v1, Lgdq;->e:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 66
    new-instance v0, Lgds;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Bind call too late - someone already tried to get: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lgds;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_1
    new-instance v1, Lgdr;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Duplicate binding: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgdr;-><init>(Ljava/lang/String;)V

    throw v1

    .line 68
    :cond_2
    iget-object v0, p0, Lgdq;->i:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    monitor-exit p0

    return-void
.end method
