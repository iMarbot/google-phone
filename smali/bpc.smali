.class public final Lbpc;
.super Landroid/content/CursorLoader;
.source "PG"


# static fields
.field private static a:Landroid/net/Uri;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;

.field private d:[Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "filter_enterprise"

    .line 64
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lbpc;->a:Landroid/net/Uri;

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1
    sget-object v3, Lbnz;->c:[Ljava/lang/String;

    const-string v4, "length(data1) < 1000 AND data1 IS NOT NULL"

    const-string v6, "sort_key"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lbpc;->b:Ljava/lang/String;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lbpc;->c:Ljava/util/List;

    .line 4
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/Cursor;

    iput-object v0, p0, Lbpc;->d:[Landroid/database/Cursor;

    .line 5
    return-void
.end method

.method private static a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 2

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 45
    :goto_0
    return-object v0

    .line 36
    :cond_0
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-interface {p0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 37
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 38
    :cond_1
    const/4 v1, 0x3

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 39
    if-eqz v1, :cond_2

    .line 40
    invoke-static {p0}, Lbpc;->b(Landroid/database/Cursor;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 41
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 42
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static b(Landroid/database/Cursor;)[Ljava/lang/Object;
    .locals 5

    .prologue
    .line 46
    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    new-array v1, v0, [Ljava/lang/Object;

    .line 47
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    if-ge v0, v2, :cond_5

    .line 48
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getType(I)I

    move-result v2

    .line 49
    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 50
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    aput-object v2, v1, v0

    .line 60
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 52
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 53
    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 54
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 55
    :cond_2
    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 56
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 57
    :cond_3
    if-nez v2, :cond_4

    .line 58
    const/4 v2, 0x0

    aput-object v2, v1, v0

    goto :goto_1

    .line 59
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const/16 v3, 0x37

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown fieldType ("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") for column: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :cond_5
    return-object v1
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 6
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget-object v0, p0, Lbpc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 7
    iget-object v0, p0, Lbpc;->c:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbpe;

    .line 8
    invoke-virtual {v1}, Lbpe;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lbpc;->d:[Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object v1, v0, v6

    .line 32
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p0}, Lbpc;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lbpc;->b:Ljava/lang/String;

    .line 14
    invoke-virtual {v1}, Lbpe;->a()I

    move-result v3

    .line 15
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v1, v4, :cond_1

    .line 16
    sget-object v1, Lbpc;->a:Landroid/net/Uri;

    .line 19
    :goto_2
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 20
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "directory"

    .line 21
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "remove_duplicate_entries"

    const-string v3, "true"

    .line 22
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    const-string v3, "10"

    .line 23
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 26
    invoke-virtual {p0}, Lbpc;->getProjection()[Ljava/lang/String;

    move-result-object v2

    .line 27
    invoke-virtual {p0}, Lbpc;->getSelection()Ljava/lang/String;

    move-result-object v3

    .line 28
    invoke-virtual {p0}, Lbpc;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v4

    .line 29
    invoke-virtual {p0}, Lbpc;->getSortOrder()Ljava/lang/String;

    move-result-object v5

    .line 30
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lbpc;->d:[Landroid/database/Cursor;

    invoke-static {v0}, Lbpc;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    aput-object v0, v1, v6

    goto :goto_1

    .line 17
    :cond_1
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    goto :goto_2

    .line 33
    :cond_2
    invoke-virtual {p0}, Lbpc;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbpc;->d:[Landroid/database/Cursor;

    iget-object v2, p0, Lbpc;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lbpb;->a(Landroid/content/Context;[Landroid/database/Cursor;Ljava/util/List;)Lbpb;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lbpc;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
