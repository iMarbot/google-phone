.class public final Lbjo;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbjo$a;
    }
.end annotation


# static fields
.field public static final g:Lbjo;

.field private static volatile h:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Lbjo;

    invoke-direct {v0}, Lbjo;-><init>()V

    .line 124
    sput-object v0, Lbjo;->g:Lbjo;

    invoke-virtual {v0}, Lbjo;->makeImmutable()V

    .line 125
    const-class v0, Lbjo;

    sget-object v1, Lbjo;->g:Lbjo;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 126
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput v0, p0, Lbjo;->b:I

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lbjo;->c:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lbjo;->d:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lbjo;->e:Ljava/lang/String;

    .line 6
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 119
    sget-object v2, Lbjo$a;->e:Lhby;

    .line 120
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "f"

    aput-object v2, v0, v1

    .line 121
    const-string v1, "\u0001\u0005\u0000\u0001\u0001\u0007\u0000\u0000\u0000\u0001\u000c\u0000\u0002\u0008\u0001\u0004\u0008\u0002\u0005\u0008\u0003\u0007\u0002\u0004"

    .line 122
    sget-object v2, Lbjo;->g:Lbjo;

    invoke-static {v2, v1, v0}, Lbjo;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 60
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 117
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 61
    :pswitch_0
    new-instance v0, Lbjo;

    invoke-direct {v0}, Lbjo;-><init>()V

    .line 116
    :goto_0
    :pswitch_1
    return-object v0

    .line 62
    :pswitch_2
    sget-object v0, Lbjo;->g:Lbjo;

    goto :goto_0

    .line 64
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[I)V

    move-object v0, v1

    goto :goto_0

    .line 65
    :pswitch_4
    check-cast p2, Lhaq;

    .line 66
    check-cast p3, Lhbg;

    .line 67
    if-nez p3, :cond_0

    .line 68
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 69
    :cond_0
    :try_start_0
    sget-boolean v0, Lbjo;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {p0, p2, p3}, Lbjo;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 71
    sget-object v0, Lbjo;->g:Lbjo;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 73
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 74
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 75
    sparse-switch v2, :sswitch_data_0

    .line 78
    invoke-virtual {p0, v2, p2}, Lbjo;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 79
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 77
    goto :goto_1

    .line 80
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 81
    invoke-static {v2}, Lbjo$a;->a(I)Lbjo$a;

    move-result-object v3

    .line 82
    if-nez v3, :cond_3

    .line 83
    const/4 v3, 0x1

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 103
    :catch_0
    move-exception v0

    .line 104
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :catchall_0
    move-exception v0

    throw v0

    .line 84
    :cond_3
    :try_start_2
    iget v3, p0, Lbjo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lbjo;->a:I

    .line 85
    iput v2, p0, Lbjo;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 105
    :catch_1
    move-exception v0

    .line 106
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 107
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    :sswitch_2
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 88
    iget v3, p0, Lbjo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lbjo;->a:I

    .line 89
    iput-object v2, p0, Lbjo;->c:Ljava/lang/String;

    goto :goto_1

    .line 91
    :sswitch_3
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 92
    iget v3, p0, Lbjo;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lbjo;->a:I

    .line 93
    iput-object v2, p0, Lbjo;->d:Ljava/lang/String;

    goto :goto_1

    .line 95
    :sswitch_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 96
    iget v3, p0, Lbjo;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lbjo;->a:I

    .line 97
    iput-object v2, p0, Lbjo;->e:Ljava/lang/String;

    goto :goto_1

    .line 99
    :sswitch_5
    iget v2, p0, Lbjo;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lbjo;->a:I

    .line 100
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lbjo;->f:J
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 109
    :cond_4
    :pswitch_5
    sget-object v0, Lbjo;->g:Lbjo;

    goto/16 :goto_0

    .line 110
    :pswitch_6
    sget-object v0, Lbjo;->h:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lbjo;

    monitor-enter v1

    .line 111
    :try_start_5
    sget-object v0, Lbjo;->h:Lhdm;

    if-nez v0, :cond_5

    .line 112
    new-instance v0, Lhaa;

    sget-object v2, Lbjo;->g:Lbjo;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbjo;->h:Lhdm;

    .line 113
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 114
    :cond_6
    sget-object v0, Lbjo;->h:Lhdm;

    goto/16 :goto_0

    .line 113
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 115
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 75
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x38 -> :sswitch_5
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 30
    iget v0, p0, Lbjo;->memoizedSerializedSize:I

    .line 31
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 59
    :goto_0
    return v0

    .line 32
    :cond_0
    sget-boolean v0, Lbjo;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0}, Lbjo;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbjo;->memoizedSerializedSize:I

    .line 34
    iget v0, p0, Lbjo;->memoizedSerializedSize:I

    goto :goto_0

    .line 35
    :cond_1
    const/4 v0, 0x0

    .line 36
    iget v1, p0, Lbjo;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 37
    iget v0, p0, Lbjo;->b:I

    .line 38
    invoke-static {v2, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 39
    :cond_2
    iget v1, p0, Lbjo;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 42
    iget-object v1, p0, Lbjo;->c:Ljava/lang/String;

    .line 43
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    :cond_3
    iget v1, p0, Lbjo;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 47
    iget-object v1, p0, Lbjo;->d:Ljava/lang/String;

    .line 48
    invoke-static {v4, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_4
    iget v1, p0, Lbjo;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 50
    const/4 v1, 0x5

    .line 52
    iget-object v2, p0, Lbjo;->e:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_5
    iget v1, p0, Lbjo;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 55
    const/4 v1, 0x7

    iget-wide v2, p0, Lbjo;->f:J

    .line 56
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_6
    iget-object v1, p0, Lbjo;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    iput v0, p0, Lbjo;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 7
    sget-boolean v0, Lbjo;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p0, p1}, Lbjo;->writeToInternal(Lhaw;)V

    .line 29
    :goto_0
    return-void

    .line 10
    :cond_0
    iget v0, p0, Lbjo;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 11
    iget v0, p0, Lbjo;->b:I

    .line 12
    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 13
    :cond_1
    iget v0, p0, Lbjo;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 15
    iget-object v0, p0, Lbjo;->c:Ljava/lang/String;

    .line 16
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 17
    :cond_2
    iget v0, p0, Lbjo;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 19
    iget-object v0, p0, Lbjo;->d:Ljava/lang/String;

    .line 20
    invoke-virtual {p1, v3, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 21
    :cond_3
    iget v0, p0, Lbjo;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 22
    const/4 v0, 0x5

    .line 23
    iget-object v1, p0, Lbjo;->e:Ljava/lang/String;

    .line 24
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 25
    :cond_4
    iget v0, p0, Lbjo;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 26
    const/4 v0, 0x7

    iget-wide v2, p0, Lbjo;->f:J

    .line 27
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 28
    :cond_5
    iget-object v0, p0, Lbjo;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
