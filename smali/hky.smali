.class public Lhky;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lhla;

.field public b:Lhld;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lhla;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lhky;-><init>()V

    .line 36
    const-string v0, "helper"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhla;

    iput-object v0, p0, Lhky;->a:Lhla;

    .line 37
    return-void
.end method

.method static b(Ljava/util/List;)Lhks;
    .locals 3

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 39
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhks;

    .line 41
    iget-object v0, v0, Lhks;->a:Ljava/util/List;

    .line 42
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 44
    :cond_0
    new-instance v0, Lhks;

    invoke-direct {v0, v1}, Lhks;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lhky;->b:Lhld;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lhky;->b:Lhld;

    invoke-virtual {v0}, Lhld;->a()V

    .line 34
    :cond_0
    return-void
.end method

.method public a(Lhld;Lhkk;)V
    .locals 4

    .prologue
    .line 14
    .line 15
    iget-object v1, p2, Lhkk;->a:Lhkj;

    .line 17
    iget-object v0, p0, Lhky;->b:Lhld;

    if-ne p1, v0, :cond_0

    sget-object v0, Lhkj;->e:Lhkj;

    if-ne v1, v0, :cond_1

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    invoke-virtual {v1}, Lhkj;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported state:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :pswitch_0
    sget-object v0, Lhlb;->a:Lhlb;

    .line 30
    :goto_1
    iget-object v2, p0, Lhky;->a:Lhla;

    new-instance v3, Lhle;

    invoke-direct {v3, v0}, Lhle;-><init>(Lhlb;)V

    invoke-virtual {v2, v1, v3}, Lhla;->a(Lhkj;Lhle;)V

    goto :goto_0

    .line 23
    :pswitch_1
    invoke-static {p1}, Lhlb;->a(Lhld;)Lhlb;

    move-result-object v0

    goto :goto_1

    .line 26
    :pswitch_2
    iget-object v0, p2, Lhkk;->b:Lhlw;

    .line 27
    invoke-static {v0}, Lhlb;->a(Lhlw;)Lhlb;

    move-result-object v0

    goto :goto_1

    .line 19
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lhlw;)V
    .locals 4

    .prologue
    .line 9
    iget-object v0, p0, Lhky;->b:Lhld;

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lhky;->b:Lhld;

    invoke-virtual {v0}, Lhld;->a()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lhky;->b:Lhld;

    .line 12
    :cond_0
    iget-object v0, p0, Lhky;->a:Lhla;

    sget-object v1, Lhkj;->c:Lhkj;

    new-instance v2, Lhle;

    invoke-static {p1}, Lhlb;->a(Lhlw;)Lhlb;

    move-result-object v3

    invoke-direct {v2, v3}, Lhle;-><init>(Lhlb;)V

    invoke-virtual {v0, v1, v2}, Lhla;->a(Lhkj;Lhle;)V

    .line 13
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 2
    invoke-static {p1}, Lhky;->b(Ljava/util/List;)Lhks;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lhky;->b:Lhld;

    if-nez v1, :cond_0

    .line 4
    iget-object v1, p0, Lhky;->a:Lhla;

    sget-object v2, Lhjs;->b:Lhjs;

    invoke-virtual {v1, v0, v2}, Lhla;->a(Lhks;Lhjs;)Lhld;

    move-result-object v0

    iput-object v0, p0, Lhky;->b:Lhld;

    .line 5
    iget-object v0, p0, Lhky;->a:Lhla;

    sget-object v1, Lhkj;->a:Lhkj;

    new-instance v2, Lhle;

    iget-object v3, p0, Lhky;->b:Lhld;

    invoke-static {v3}, Lhlb;->a(Lhld;)Lhlb;

    move-result-object v3

    invoke-direct {v2, v3}, Lhle;-><init>(Lhlb;)V

    invoke-virtual {v0, v1, v2}, Lhla;->a(Lhkj;Lhle;)V

    .line 6
    iget-object v0, p0, Lhky;->b:Lhld;

    invoke-virtual {v0}, Lhld;->b()V

    .line 8
    :goto_0
    return-void

    .line 7
    :cond_0
    iget-object v1, p0, Lhky;->a:Lhla;

    iget-object v2, p0, Lhky;->b:Lhld;

    invoke-virtual {v1, v2, v0}, Lhla;->a(Lhld;Lhks;)V

    goto :goto_0
.end method
