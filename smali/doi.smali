.class public final Ldoi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbmi;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Lbml;)Ldoj;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Ldoj;

    invoke-direct {v0, p0}, Ldoj;-><init>(Lbml;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2
    .line 3
    invoke-static {p0}, Lapw;->v(Landroid/content/Context;)Lbgi;

    move-result-object v0

    invoke-interface {v0, p0}, Lbgi;->a(Landroid/content/Context;)Lbgl;

    move-result-object v0

    check-cast v0, Ldip;

    .line 4
    sget-object v1, Lbko$a;->d:Lbko$a;

    invoke-virtual {v1}, Lbko$a;->getNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ldip;->a(I)V

    .line 5
    sget-object v1, Lbko$a;->e:Lbko$a;

    invoke-virtual {v1}, Lbko$a;->getNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ldip;->a(I)V

    .line 6
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/io/InputStream;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-static {p2}, Ldol;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 127
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 128
    :try_start_1
    invoke-static {p3, v3}, Lgve;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 129
    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 135
    :cond_0
    :goto_0
    return-object v0

    .line 130
    :catch_0
    move-exception v2

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 131
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v3, :cond_1

    if-eqz v2, :cond_2

    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_2
    :try_start_5
    throw v0

    .line 134
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 131
    :catch_2
    move-exception v3

    invoke-static {v2, v3}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_1
.end method

.method public final synthetic a(Landroid/content/Context;Ljava/lang/String;)Lbmj;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0, p1, p2}, Ldoi;->b(Landroid/content/Context;Ljava/lang/String;)Ldoj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lbml;)Lbmj;
    .locals 1

    .prologue
    .line 146
    invoke-static {p1}, Ldoi;->b(Lbml;)Ldoj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 136
    invoke-static {p1}, Ldip;->a(Landroid/content/Context;)Ldip;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Ldip;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM cached_number_contacts"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public final a(Landroid/content/Context;Lbmj;)V
    .locals 6

    .prologue
    .line 97
    instance-of v0, p2, Ldoj;

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    check-cast p2, Ldoj;

    .line 100
    sget-object v1, Ldol;->a:Landroid/net/Uri;

    .line 101
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 103
    iget-object v3, p2, Ldoj;->a:Lbml;

    .line 105
    if-eqz v3, :cond_0

    sget-object v0, Lbml;->a:Lbml;

    invoke-virtual {v3, v0}, Lbml;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, v3, Lbml;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lbml;->h:Ljava/lang/String;

    .line 108
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 110
    const-string v4, "number"

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "phone_type"

    iget v4, v3, Lbml;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 112
    const-string v0, "phone_label"

    iget-object v4, v3, Lbml;->g:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "display_name"

    iget-object v4, v3, Lbml;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v4, "photo_uri"

    .line 115
    iget-object v0, v3, Lbml;->m:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, v3, Lbml;->m:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_2
    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v4, "reported"

    iget-boolean v0, v3, Lbml;->n:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 118
    const-string v0, "object_id"

    iget-object v4, v3, Lbml;->o:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "user_type"

    iget-wide v4, v3, Lbml;->p:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    const-string v0, "source_name"

    iget-object v3, p2, Ldoj;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v0, "source_type"

    iget-object v3, p2, Ldoj;->c:Lbko$a;

    invoke-virtual {v3}, Lbko$a;->getNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    const-string v0, "source_id"

    iget-wide v4, p2, Ldoj;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 123
    const-string v0, "lookup_key"

    iget-object v3, p2, Ldoj;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 107
    :cond_2
    iget-object v0, v3, Lbml;->k:Ljava/lang/String;

    goto/16 :goto_1

    .line 115
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 117
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Lbko$a;)Z
    .locals 1

    .prologue
    .line 140
    invoke-static {p1}, Ldoj;->b(Lbko$a;)Z

    move-result v0

    return v0
.end method

.method public final a(Lbko$a;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 141
    .line 142
    invoke-static {p1}, Ldoj;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    .line 142
    :cond_0
    const/4 v0, 0x0

    .line 143
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Ldoj;
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 8
    .line 9
    sget-object v0, Ldol;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Ldok;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 13
    if-nez v2, :cond_0

    .line 96
    :goto_0
    return-object v3

    .line 15
    :cond_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 16
    sget-object v0, Lbml;->a:Lbml;

    invoke-static {v0}, Ldoi;->b(Lbml;)Ldoj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 17
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 19
    :cond_1
    const/16 v0, 0x8

    .line 20
    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lbko$a;->a(I)Lbko$a;

    move-result-object v0

    .line 21
    if-nez v0, :cond_f

    .line 22
    sget-object v0, Lbko$a;->a:Lbko$a;

    move-object v1, v0

    .line 23
    :goto_1
    invoke-static {v1}, Ldoj;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24
    invoke-static {p1}, Ldhh;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 25
    invoke-static {p1}, Ldoi;->b(Landroid/content/Context;)V

    .line 26
    sget-object v0, Lbml;->a:Lbml;

    invoke-static {v0}, Ldoi;->b(Lbml;)Ldoj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 27
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 29
    :cond_2
    :try_start_2
    new-instance v4, Lbml;

    invoke-direct {v4}, Lbml;-><init>()V

    .line 31
    const/16 v0, 0x8

    .line 32
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lbko$a;->a(I)Lbko$a;

    move-result-object v0

    .line 33
    const/16 v5, 0x9

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 34
    const/4 v8, 0x7

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 35
    const/16 v9, 0xa

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 36
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    :cond_3
    move-object v0, v3

    .line 64
    :goto_2
    iput-object v0, v4, Lbml;->b:Landroid/net/Uri;

    .line 65
    const/16 v0, 0xa

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbml;->c:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbml;->d:Ljava/lang/String;

    .line 67
    const/4 v0, 0x5

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v4, Lbml;->f:I

    .line 68
    const/4 v0, 0x6

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbml;->g:Ljava/lang/String;

    .line 69
    const/4 v0, 0x4

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbml;->h:Ljava/lang/String;

    .line 70
    iput-object p2, v4, Lbml;->k:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-object v0, v4, Lbml;->i:Ljava/lang/String;

    .line 72
    const-wide/16 v8, 0x0

    iput-wide v8, v4, Lbml;->l:J

    .line 74
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 75
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 76
    if-eqz v0, :cond_c

    .line 77
    invoke-static {p2}, Ldol;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 84
    :cond_4
    :goto_3
    iput-object v3, v4, Lbml;->m:Landroid/net/Uri;

    .line 85
    const/16 v0, 0xb

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide/16 v10, 0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_e

    move v0, v6

    :goto_4
    iput-boolean v0, v4, Lbml;->n:Z

    .line 86
    const/16 v0, 0xc

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lbml;->o:Ljava/lang/String;

    .line 87
    const/16 v0, 0xd

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Lbml;->p:J

    .line 88
    invoke-static {v4}, Ldoi;->b(Lbml;)Ldoj;

    move-result-object v3

    .line 89
    const/4 v0, 0x7

    .line 90
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x9

    .line 91
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 92
    invoke-virtual {v3, v1, v0, v4, v5}, Ldoj;->a(Lbko$a;Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 38
    :cond_5
    :try_start_3
    sget-object v10, Lbko$a;->b:Lbko$a;

    if-ne v0, v10, :cond_7

    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x18

    if-lt v0, v8, :cond_6

    .line 40
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v0, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 43
    :goto_5
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v8, "directory"

    .line 44
    invoke-virtual {v0, v8, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 41
    :cond_6
    const-wide/16 v10, 0x0

    invoke-static {v10, v11, v9}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_5

    .line 47
    :cond_7
    sget-object v10, Lbko$a;->c:Lbko$a;

    if-eq v0, v10, :cond_8

    sget-object v10, Lbko$a;->d:Lbko$a;

    if-eq v0, v10, :cond_8

    sget-object v10, Lbko$a;->e:Lbko$a;

    if-ne v0, v10, :cond_a

    .line 48
    :cond_8
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 49
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v10, "encoded"

    .line 50
    invoke-virtual {v0, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 52
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 53
    const-string v9, "displayName"

    invoke-virtual {v0, v9, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 54
    :cond_9
    const-string v8, "directory"

    invoke-virtual {v0, v8, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 55
    :cond_a
    sget-object v5, Lbko$a;->f:Lbko$a;

    if-ne v0, v5, :cond_b

    .line 56
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 57
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v5, "encoded"

    .line 58
    invoke-virtual {v0, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v5, "directory"

    const-string v8, "9223372036854775807"

    .line 59
    invoke-virtual {v0, v5, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    :cond_b
    move-object v0, v3

    .line 63
    goto/16 :goto_2

    .line 78
    :cond_c
    if-eqz v5, :cond_d

    .line 80
    sget-object v0, Ldol;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_3

    .line 82
    :cond_d
    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto/16 :goto_3

    :cond_e
    move v0, v7

    .line 85
    goto/16 :goto_4

    .line 95
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_f
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final b(Landroid/content/Context;Lbmj;)Z
    .locals 1

    .prologue
    .line 144
    invoke-static {p1, p2}, Ldhh;->a(Landroid/content/Context;Lbmj;)Z

    move-result v0

    return v0
.end method
