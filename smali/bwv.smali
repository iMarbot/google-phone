.class public final Lbwv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwq;
.implements Lbwr;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbwv;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method private static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 13
    const/16 v0, 0x27

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Send InCallUi Broadcast, visible: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 15
    const-string v1, "visible"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 16
    const-string v1, "com.motorola.incallui.action.INCOMING_CALL_VISIBILITY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 17
    const-string v1, "com.motorola.incallui.permission.INCOMING_CALL_VISIBILITY_CHANGED"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 2

    .prologue
    .line 9
    if-eqz p1, :cond_0

    .line 10
    invoke-virtual {p1}, Lbwp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbwp;->a:Lbwp;

    if-ne p2, v0, :cond_0

    .line 11
    iget-object v0, p0, Lbwv;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbwv;->a(Landroid/content/Context;Z)V

    .line 12
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 4
    if-eqz p1, :cond_0

    .line 5
    sget-object v0, Lcct;->a:Lcct;

    .line 6
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lbwv;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbwv;->a(Landroid/content/Context;Z)V

    .line 8
    :cond_0
    return-void
.end method
