.class public final Lhiu;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final d:Lhiu;

.field private static volatile e:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lhiu;

    invoke-direct {v0}, Lhiu;-><init>()V

    .line 81
    sput-object v0, Lhiu;->d:Lhiu;

    invoke-virtual {v0}, Lhiu;->makeImmutable()V

    .line 82
    const-class v0, Lhiu;

    sget-object v1, Lhiu;->d:Lhiu;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 83
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 76
    sget-object v2, Lbld$a;->u:Lhby;

    .line 77
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    .line 78
    const-string v1, "\u0001\u0002\u0000\u0001\u0001\u0002\u0000\u0000\u0000\u0001\u000c\u0000\u0002\u0002\u0001"

    .line 79
    sget-object v2, Lhiu;->d:Lhiu;

    invoke-static {v2, v1, v0}, Lhiu;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 29
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 30
    :pswitch_0
    new-instance v0, Lhiu;

    invoke-direct {v0}, Lhiu;-><init>()V

    .line 73
    :goto_0
    :pswitch_1
    return-object v0

    .line 31
    :pswitch_2
    sget-object v0, Lhiu;->d:Lhiu;

    goto :goto_0

    .line 33
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[[I)V

    move-object v0, v1

    goto :goto_0

    .line 34
    :pswitch_4
    check-cast p2, Lhaq;

    .line 35
    check-cast p3, Lhbg;

    .line 36
    if-nez p3, :cond_0

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38
    :cond_0
    :try_start_0
    sget-boolean v0, Lhiu;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {p0, p2, p3}, Lhiu;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 40
    sget-object v0, Lhiu;->d:Lhiu;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 42
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 43
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 44
    sparse-switch v2, :sswitch_data_0

    .line 47
    invoke-virtual {p0, v2, p2}, Lhiu;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 48
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 46
    goto :goto_1

    .line 49
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 50
    invoke-static {v2}, Lbld$a;->a(I)Lbld$a;

    move-result-object v3

    .line 51
    if-nez v3, :cond_3

    .line 52
    const/4 v3, 0x1

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 60
    :catch_0
    move-exception v0

    .line 61
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    :catchall_0
    move-exception v0

    throw v0

    .line 53
    :cond_3
    :try_start_2
    iget v3, p0, Lhiu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhiu;->a:I

    .line 54
    iput v2, p0, Lhiu;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 62
    :catch_1
    move-exception v0

    .line 63
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 64
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56
    :sswitch_2
    :try_start_4
    iget v2, p0, Lhiu;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhiu;->a:I

    .line 57
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhiu;->c:J
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 66
    :cond_4
    :pswitch_5
    sget-object v0, Lhiu;->d:Lhiu;

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lhiu;->e:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lhiu;

    monitor-enter v1

    .line 68
    :try_start_5
    sget-object v0, Lhiu;->e:Lhdm;

    if-nez v0, :cond_5

    .line 69
    new-instance v0, Lhaa;

    sget-object v2, Lhiu;->d:Lhiu;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhiu;->e:Lhdm;

    .line 70
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 71
    :cond_6
    sget-object v0, Lhiu;->e:Lhdm;

    goto/16 :goto_0

    .line 70
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 72
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 14
    iget v0, p0, Lhiu;->memoizedSerializedSize:I

    .line 15
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 28
    :goto_0
    return v0

    .line 16
    :cond_0
    sget-boolean v0, Lhiu;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {p0}, Lhiu;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhiu;->memoizedSerializedSize:I

    .line 18
    iget v0, p0, Lhiu;->memoizedSerializedSize:I

    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    iget v1, p0, Lhiu;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 21
    iget v0, p0, Lhiu;->b:I

    .line 22
    invoke-static {v2, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23
    :cond_2
    iget v1, p0, Lhiu;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    .line 24
    iget-wide v2, p0, Lhiu;->c:J

    .line 25
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 26
    :cond_3
    iget-object v1, p0, Lhiu;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    iput v0, p0, Lhiu;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lhiu;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhiu;->writeToInternal(Lhaw;)V

    .line 13
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhiu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 7
    iget v0, p0, Lhiu;->b:I

    .line 8
    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 9
    :cond_1
    iget v0, p0, Lhiu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 10
    iget-wide v0, p0, Lhiu;->c:J

    .line 11
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 12
    :cond_2
    iget-object v0, p0, Lhiu;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
