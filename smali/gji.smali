.class public final Lgji;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:[Lgjj;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Lgix;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    invoke-static {}, Lgjj;->a()[Lgjj;

    move-result-object v0

    iput-object v0, p0, Lgji;->b:[Lgjj;

    .line 4
    iput-object v1, p0, Lgji;->c:Ljava/lang/Integer;

    .line 5
    iput-object v1, p0, Lgji;->d:Ljava/lang/Integer;

    .line 6
    iput-object v1, p0, Lgji;->a:Ljava/lang/Integer;

    .line 7
    iput-object v1, p0, Lgji;->e:Lgix;

    .line 8
    iput-object v1, p0, Lgji;->unknownFieldData:Lhfv;

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lgji;->cachedSize:I

    .line 10
    return-void
.end method

.method private a(Lhfp;)Lgji;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 48
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 51
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    :sswitch_0
    return-object p0

    .line 53
    :sswitch_1
    const/16 v0, 0xb

    .line 54
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 55
    iget-object v0, p0, Lgji;->b:[Lgjj;

    if-nez v0, :cond_2

    move v0, v1

    .line 56
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjj;

    .line 57
    if-eqz v0, :cond_1

    .line 58
    iget-object v3, p0, Lgji;->b:[Lgjj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 60
    new-instance v3, Lgjj;

    invoke-direct {v3}, Lgjj;-><init>()V

    aput-object v3, v2, v0

    .line 61
    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v7}, Lhfp;->a(Lhfz;I)V

    .line 62
    invoke-virtual {p1}, Lhfp;->a()I

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 55
    :cond_2
    iget-object v0, p0, Lgji;->b:[Lgjj;

    array-length v0, v0

    goto :goto_1

    .line 64
    :cond_3
    new-instance v3, Lgjj;

    invoke-direct {v3}, Lgjj;-><init>()V

    aput-object v3, v2, v0

    .line 65
    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v7}, Lhfp;->a(Lhfz;I)V

    .line 66
    iput-object v2, p0, Lgji;->b:[Lgjj;

    goto :goto_0

    .line 68
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 72
    packed-switch v3, :pswitch_data_0

    .line 74
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x26

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum Reason"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 79
    invoke-virtual {p0, p1, v0}, Lgji;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 75
    :pswitch_0
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgji;->c:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 82
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 83
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgji;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 86
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgji;->a:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 89
    :sswitch_5
    iget-object v0, p0, Lgji;->e:Lgix;

    if-nez v0, :cond_4

    .line 90
    new-instance v0, Lgix;

    invoke-direct {v0}, Lgix;-><init>()V

    iput-object v0, p0, Lgji;->e:Lgix;

    .line 91
    :cond_4
    iget-object v0, p0, Lgji;->e:Lgix;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 49
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x78 -> :sswitch_2
        0x80 -> :sswitch_3
        0x88 -> :sswitch_4
        0x92 -> :sswitch_5
    .end sparse-switch

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 27
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v1

    .line 28
    iget-object v0, p0, Lgji;->b:[Lgjj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgji;->b:[Lgjj;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 29
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lgji;->b:[Lgjj;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 30
    iget-object v2, p0, Lgji;->b:[Lgjj;

    aget-object v2, v2, v0

    .line 31
    if-eqz v2, :cond_0

    .line 32
    const/4 v3, 0x1

    .line 33
    invoke-static {v3, v2}, Lhfq;->c(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 34
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p0, Lgji;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 36
    const/16 v0, 0xf

    iget-object v2, p0, Lgji;->c:Ljava/lang/Integer;

    .line 37
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lhfq;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 38
    :cond_2
    iget-object v0, p0, Lgji;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 39
    const/16 v0, 0x10

    iget-object v2, p0, Lgji;->d:Ljava/lang/Integer;

    .line 40
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lhfq;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 41
    :cond_3
    iget-object v0, p0, Lgji;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 42
    const/16 v0, 0x11

    iget-object v2, p0, Lgji;->a:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lhfq;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 44
    :cond_4
    iget-object v0, p0, Lgji;->e:Lgix;

    if-eqz v0, :cond_5

    .line 45
    const/16 v0, 0x12

    iget-object v2, p0, Lgji;->e:Lgix;

    .line 46
    invoke-static {v0, v2}, Lhfq;->d(ILhfz;)I

    move-result v0

    add-int/2addr v1, v0

    .line 47
    :cond_5
    return v1
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lgji;->a(Lhfp;)Lgji;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 11
    iget-object v0, p0, Lgji;->b:[Lgjj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgji;->b:[Lgjj;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 12
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgji;->b:[Lgjj;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 13
    iget-object v1, p0, Lgji;->b:[Lgjj;

    aget-object v1, v1, v0

    .line 14
    if-eqz v1, :cond_0

    .line 15
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILhfz;)V

    .line 16
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lgji;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 18
    const/16 v0, 0xf

    iget-object v1, p0, Lgji;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_2
    iget-object v0, p0, Lgji;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 20
    const/16 v0, 0x10

    iget-object v1, p0, Lgji;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 21
    :cond_3
    iget-object v0, p0, Lgji;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 22
    const/16 v0, 0x11

    iget-object v1, p0, Lgji;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_4
    iget-object v0, p0, Lgji;->e:Lgix;

    if-eqz v0, :cond_5

    .line 24
    const/16 v0, 0x12

    iget-object v1, p0, Lgji;->e:Lgix;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 26
    return-void
.end method
