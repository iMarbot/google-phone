.class final Lbgx;
.super Lbhe;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/io/ByteArrayOutputStream;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbhe;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method final a()Lbhd;
    .locals 9

    .prologue
    .line 21
    const-string v0, ""

    .line 22
    iget-object v1, p0, Lbgx;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 23
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " accountType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 24
    :cond_0
    iget-object v1, p0, Lbgx;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 25
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " accountName"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26
    :cond_1
    iget-object v1, p0, Lbgx;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 27
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " isStarred"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    :cond_2
    iget-object v1, p0, Lbgx;->e:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 29
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " pinned"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 30
    :cond_3
    iget-object v1, p0, Lbgx;->g:Ljava/util/List;

    if-nez v1, :cond_4

    .line 31
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " phoneNumbers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    :cond_4
    iget-object v1, p0, Lbgx;->h:Ljava/util/List;

    if-nez v1, :cond_5

    .line 33
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " emails"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 35
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing required properties:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_7
    new-instance v0, Lbgw;

    iget-object v1, p0, Lbgx;->a:Ljava/lang/String;

    iget-object v2, p0, Lbgx;->b:Ljava/lang/String;

    iget-object v3, p0, Lbgx;->c:Ljava/lang/String;

    iget-object v4, p0, Lbgx;->d:Ljava/lang/Boolean;

    .line 37
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lbgx;->e:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lbgx;->f:Ljava/io/ByteArrayOutputStream;

    iget-object v7, p0, Lbgx;->g:Ljava/util/List;

    iget-object v8, p0, Lbgx;->h:Ljava/util/List;

    .line 39
    invoke-direct/range {v0 .. v8}, Lbgw;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/io/ByteArrayOutputStream;Ljava/util/List;Ljava/util/List;)V

    .line 40
    return-object v0
.end method

.method final a(I)Lbhe;
    .locals 1

    .prologue
    .line 9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbgx;->e:Ljava/lang/Integer;

    .line 10
    return-object p0
.end method

.method final a(Ljava/io/ByteArrayOutputStream;)Lbhe;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lbgx;->f:Ljava/io/ByteArrayOutputStream;

    .line 12
    return-object p0
.end method

.method final a(Ljava/lang/String;)Lbhe;
    .locals 0

    .prologue
    .line 3
    iput-object p1, p0, Lbgx;->b:Ljava/lang/String;

    .line 4
    return-object p0
.end method

.method final a(Ljava/util/List;)Lbhe;
    .locals 2

    .prologue
    .line 13
    if-nez p1, :cond_0

    .line 14
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null phoneNumbers"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_0
    iput-object p1, p0, Lbgx;->g:Ljava/util/List;

    .line 16
    return-object p0
.end method

.method final a(Z)Lbhe;
    .locals 1

    .prologue
    .line 7
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbgx;->d:Ljava/lang/Boolean;

    .line 8
    return-object p0
.end method

.method final b(Ljava/lang/String;)Lbhe;
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lbgx;->c:Ljava/lang/String;

    .line 6
    return-object p0
.end method

.method final b(Ljava/util/List;)Lbhe;
    .locals 2

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null emails"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lbgx;->h:Ljava/util/List;

    .line 20
    return-object p0
.end method
