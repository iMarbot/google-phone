.class final Larp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# instance fields
.field private synthetic a:Larn;


# direct methods
.method constructor <init>(Larn;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Larp;->a:Larn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1

    .prologue
    .line 115
    .line 116
    iget-object v0, p0, Larp;->a:Larn;

    invoke-virtual {v0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Laga;->a(Landroid/content/Context;)Landroid/content/CursorLoader;

    move-result-object v0

    .line 117
    return-object v0
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 37

    .prologue
    .line 3
    check-cast p2, Landroid/database/Cursor;

    .line 4
    move-object/from16 v0, p0

    iget-object v4, v0, Larp;->a:Larn;

    .line 5
    iget-object v0, v4, Larn;->f:Lasb;

    move-object/from16 v16, v0

    .line 7
    move-object/from16 v0, v16

    iget-boolean v4, v0, Lasb;->i:Z

    if-nez v4, :cond_11

    if-eqz p2, :cond_11

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_11

    .line 8
    invoke-static/range {p2 .. p2}, Lasb;->a(Landroid/database/Cursor;)I

    move-result v4

    move-object/from16 v0, v16

    iput v4, v0, Lasb;->c:I

    .line 9
    move-object/from16 v0, v16

    iget-boolean v4, v0, Lasb;->h:Z

    if-eqz v4, :cond_0

    .line 10
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->d:Lasb$a;

    invoke-interface {v4}, Lasb$a;->a()V

    .line 12
    :cond_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    move-object/from16 v0, v16

    iget v5, v0, Lasb;->c:I

    sub-int/2addr v4, v5

    move-object/from16 v0, v16

    iput v4, v0, Lasb;->b:I

    .line 14
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 15
    if-eqz p2, :cond_10

    .line 16
    new-instance v17, Landroid/util/LongSparseArray;

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 17
    const/4 v8, 0x0

    .line 18
    const/4 v7, 0x0

    .line 19
    const/4 v6, 0x0

    .line 20
    const/4 v15, 0x0

    .line 21
    const/4 v5, 0x0

    .line 22
    const/4 v4, 0x0

    .line 23
    const/4 v14, 0x0

    .line 24
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 25
    const-string v9, "starred"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 26
    const-string v9, "contact_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 27
    const-string v9, "photo_uri"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 28
    const-string v9, "lookup"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 29
    const-string v9, "pinned"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 30
    const-string v9, "display_name"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 31
    const-string v9, "display_name_alt"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 32
    const-string v9, "is_super_primary"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 33
    const-string v9, "data2"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    .line 34
    const-string v9, "data3"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    .line 35
    const-string v9, "data1"

    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    .line 36
    :goto_0
    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 37
    if-gtz v4, :cond_1

    const/16 v4, 0x14

    if-ge v9, v4, :cond_15

    .line 38
    :cond_1
    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 39
    move-object/from16 v0, v17

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lahc;

    .line 40
    if-eqz v4, :cond_4

    .line 41
    iget-boolean v10, v4, Lahc;->m:Z

    if-nez v10, :cond_14

    .line 42
    const/4 v10, 0x0

    iput-object v10, v4, Lahc;->e:Ljava/lang/String;

    .line 43
    const/4 v10, 0x0

    iput-object v10, v4, Lahc;->f:Ljava/lang/String;

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    .line 84
    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_13

    :cond_2
    move v10, v4

    move v9, v5

    move v5, v8

    move/from16 v36, v7

    move v7, v6

    move/from16 v6, v36

    .line 85
    :goto_2
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iput-boolean v4, v0, Lasb;->h:Z

    .line 86
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->a:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lasb;->a(Ljava/util/ArrayList;)V

    .line 87
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->e:Landroid/content/Context;

    move-object/from16 v0, v16

    iget-object v8, v0, Lasb;->a:Ljava/util/ArrayList;

    invoke-static {v4, v8}, Lbib;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 88
    invoke-virtual/range {v16 .. v16}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 89
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->e:Landroid/content/Context;

    invoke-static {v4}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v4

    invoke-virtual {v4}, Lbiu;->a()Lbis;

    move-result-object v17

    .line 90
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->a:Ljava/util/ArrayList;

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/4 v8, 0x0

    move v13, v8

    move v11, v14

    move v8, v15

    :cond_3
    :goto_3
    move/from16 v0, v18

    if-ge v13, v0, :cond_f

    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    add-int/lit8 v13, v13, 0x1

    check-cast v12, Lahc;

    .line 91
    iget-object v14, v12, Lahc;->f:Ljava/lang/String;

    if-nez v14, :cond_e

    .line 92
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 44
    :cond_4
    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 45
    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 46
    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 47
    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 48
    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 49
    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_9

    const/4 v4, 0x1

    move v10, v4

    .line 50
    :goto_4
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_a

    const/4 v4, 0x1

    .line 51
    :goto_5
    new-instance v34, Lahc;

    invoke-direct/range {v34 .. v34}, Lahc;-><init>()V

    .line 52
    move-wide/from16 v0, v30

    move-object/from16 v2, v34

    iput-wide v0, v2, Lahc;->j:J

    .line 54
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_b

    move-object v11, v12

    :goto_6
    move-object/from16 v0, v34

    iput-object v11, v0, Lahc;->b:Ljava/lang/String;

    .line 56
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    move-object v11, v13

    .line 58
    :goto_7
    move-object/from16 v0, v34

    iput-object v11, v0, Lahc;->c:Ljava/lang/String;

    .line 59
    move-object/from16 v0, v16

    iget-object v11, v0, Lasb;->g:Lalj;

    invoke-virtual {v11}, Lalj;->b()I

    move-result v11

    move-object/from16 v0, v34

    iput v11, v0, Lahc;->d:I

    .line 60
    if-eqz v29, :cond_d

    invoke-static/range {v29 .. v29}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    :goto_8
    move-object/from16 v0, v34

    iput-object v11, v0, Lahc;->g:Landroid/net/Uri;

    .line 61
    move-object/from16 v0, v32

    move-object/from16 v1, v34

    iput-object v0, v1, Lahc;->i:Ljava/lang/String;

    .line 62
    sget-object v11, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 63
    move-object/from16 v0, v32

    invoke-static {v11, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 64
    move-wide/from16 v0, v30

    invoke-static {v11, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    move-object/from16 v0, v34

    iput-object v11, v0, Lahc;->h:Landroid/net/Uri;

    .line 65
    move-object/from16 v0, v34

    iput-boolean v10, v0, Lahc;->l:Z

    .line 66
    move-object/from16 v0, v34

    iput-boolean v4, v0, Lahc;->m:Z

    .line 67
    move-object/from16 v0, p2

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 68
    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 69
    move-object/from16 v0, v16

    iget-object v13, v0, Lasb;->f:Landroid/content/res/Resources;

    .line 70
    invoke-static {v13, v4, v11}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v34

    iput-object v4, v0, Lahc;->e:Ljava/lang/String;

    .line 71
    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v34

    iput-object v4, v0, Lahc;->f:Ljava/lang/String;

    .line 72
    move/from16 v0, v33

    move-object/from16 v1, v34

    iput v0, v1, Lahc;->k:I

    .line 73
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->a:Ljava/util/ArrayList;

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    if-eqz v10, :cond_5

    .line 75
    add-int/lit8 v8, v8, 0x1

    .line 76
    :cond_5
    if-eqz v33, :cond_6

    .line 77
    add-int/lit8 v7, v7, 0x1

    .line 78
    :cond_6
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 79
    add-int/lit8 v5, v5, 0x1

    .line 80
    :cond_7
    if-eqz v29, :cond_8

    .line 81
    add-int/lit8 v6, v6, 0x1

    .line 82
    :cond_8
    move-object/from16 v0, v17

    move-wide/from16 v1, v30

    move-object/from16 v3, v34

    invoke-virtual {v0, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 83
    add-int/lit8 v9, v9, 0x1

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_1

    .line 49
    :cond_9
    const/4 v4, 0x0

    move v10, v4

    goto/16 :goto_4

    .line 50
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 54
    :cond_b
    move-object/from16 v0, v16

    iget-object v11, v0, Lasb;->f:Landroid/content/res/Resources;

    const v35, 0x7f1101f9

    move/from16 v0, v35

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_6

    .line 58
    :cond_c
    move-object/from16 v0, v16

    iget-object v11, v0, Lasb;->f:Landroid/content/res/Resources;

    const v13, 0x7f1101f9

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_7

    .line 60
    :cond_d
    const/4 v11, 0x0

    goto/16 :goto_8

    .line 93
    :cond_e
    move-object/from16 v0, v16

    iget-object v14, v0, Lasb;->e:Landroid/content/Context;

    iget-object v12, v12, Lahc;->f:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v14, v12}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 94
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 96
    :cond_f
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->e:Landroid/content/Context;

    invoke-static {v4}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v4

    .line 97
    invoke-interface/range {v4 .. v11}, Lbku;->a(IIIIIII)V

    .line 98
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    .line 99
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 100
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 101
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 102
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 103
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 104
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 105
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 106
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 107
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 108
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 109
    :cond_10
    invoke-virtual/range {v16 .. v16}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 110
    move-object/from16 v0, v16

    iget-object v4, v0, Lasb;->d:Lasb$a;

    const/4 v5, 0x0

    new-array v5, v5, [J

    invoke-interface {v4, v5}, Lasb$a;->a([J)V

    .line 111
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Larp;->a:Larn;

    move-object/from16 v0, p0

    iget-object v4, v0, Larp;->a:Larn;

    .line 112
    iget-object v4, v4, Larn;->f:Lasb;

    .line 113
    invoke-virtual {v4}, Lasb;->getCount()I

    move-result v4

    if-nez v4, :cond_12

    const/4 v4, 0x1

    :goto_9
    invoke-virtual {v5, v4}, Larn;->a(Z)V

    .line 114
    return-void

    .line 113
    :cond_12
    const/4 v4, 0x0

    goto :goto_9

    :cond_13
    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    goto/16 :goto_0

    :cond_14
    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_1

    :cond_15
    move v10, v5

    move v5, v9

    move v9, v6

    move v6, v8

    goto/16 :goto_2
.end method

.method public final onLoaderReset(Landroid/content/Loader;)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method
