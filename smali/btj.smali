.class final Lbtj;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/content/Context;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/widget/QuickContactBadge;

.field public final u:Lbsr;

.field public v:I

.field private w:Landroid/view/View;

.field private x:Z

.field private y:Lbtk;


# direct methods
.method constructor <init>(Landroid/view/View;Lbsr;Lbtk;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbtj;->p:Landroid/content/Context;

    .line 3
    const v0, 0x7f0e015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbtj;->q:Landroid/widget/TextView;

    .line 4
    const v0, 0x7f0e015b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbtj;->r:Landroid/widget/TextView;

    .line 5
    const v0, 0x7f0e0220

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbtj;->s:Landroid/widget/TextView;

    .line 6
    const v0, 0x7f0e00ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbtj;->t:Landroid/widget/QuickContactBadge;

    .line 7
    const v0, 0x7f0e0221

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbtj;->w:Landroid/view/View;

    .line 8
    iput-object p2, p0, Lbtj;->u:Lbsr;

    .line 9
    iput-object p3, p0, Lbtj;->y:Lbtk;

    .line 10
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lbtj;->x:Z

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p0}, Lbtj;->t()V

    .line 23
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-virtual {p0}, Lbtj;->u()V

    .line 22
    iget-object v0, p0, Lbtj;->y:Lbtk;

    invoke-interface {v0, p0}, Lbtk;->a(Lbtj;)V

    goto :goto_0
.end method

.method final t()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lbtj;->s:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtj;->x:Z

    .line 13
    iget-object v0, p0, Lbtj;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    return-void
.end method

.method final u()V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lbtj;->s:Landroid/widget/TextView;

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtj;->x:Z

    .line 17
    iget-object v0, p0, Lbtj;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 18
    return-void
.end method
