.class public final Lgog;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgog;


# instance fields
.field public reason:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgog;->clear()Lgog;

    .line 16
    return-void
.end method

.method public static checkMuteReasonOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum MuteReason"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkMuteReasonOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgog;->checkMuteReasonOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgog;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgog;->_emptyArray:[Lgog;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgog;->_emptyArray:[Lgog;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgog;

    sput-object v0, Lgog;->_emptyArray:[Lgog;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgog;->_emptyArray:[Lgog;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgog;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lgog;

    invoke-direct {v0}, Lgog;-><init>()V

    invoke-virtual {v0, p0}, Lgog;->mergeFrom(Lhfp;)Lgog;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgog;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lgog;

    invoke-direct {v0}, Lgog;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgog;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgog;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lgog;->reason:Ljava/lang/Integer;

    .line 18
    iput-object v0, p0, Lgog;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgog;->cachedSize:I

    .line 20
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 25
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 26
    iget-object v1, p0, Lgog;->reason:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 27
    const/4 v1, 0x1

    iget-object v2, p0, Lgog;->reason:Ljava/lang/Integer;

    .line 28
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 29
    :cond_0
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgog;
    .locals 3

    .prologue
    .line 30
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 31
    sparse-switch v0, :sswitch_data_0

    .line 33
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    :sswitch_0
    return-object p0

    .line 35
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 38
    invoke-static {v2}, Lgog;->checkMuteReasonOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgog;->reason:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 41
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 42
    invoke-virtual {p0, p1, v0}, Lgog;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 31
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lgog;->mergeFrom(Lhfp;)Lgog;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lgog;->reason:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgog;->reason:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 23
    :cond_0
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 24
    return-void
.end method
