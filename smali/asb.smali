.class public Lasb;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lars;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lasb$a;
    }
.end annotation


# static fields
.field private static k:Ljava/lang/String;


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:I

.field public c:I

.field public d:Lasb$a;

.field public e:Landroid/content/Context;

.field public f:Landroid/content/res/Resources;

.field public g:Lalj;

.field public h:Z

.field public i:Z

.field public j:Lbfo;

.field private l:Lahu$a;

.field private m:Ljava/util/Comparator;

.field private n:Lahc;

.field private o:I

.field private p:I

.field private q:I

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    const-class v0, Lasb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lasb;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lahu$a;Lasb$a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2
    iput-object v3, p0, Lasb;->a:Ljava/util/ArrayList;

    .line 3
    new-instance v0, Lasc;

    invoke-direct {v0, p0}, Lasc;-><init>(Lasb;)V

    iput-object v0, p0, Lasb;->m:Ljava/util/Comparator;

    .line 4
    iput-object v3, p0, Lasb;->n:Lahc;

    .line 5
    iput v1, p0, Lasb;->o:I

    .line 6
    iput v1, p0, Lasb;->p:I

    .line 7
    iput v1, p0, Lasb;->q:I

    .line 8
    iput-boolean v2, p0, Lasb;->h:Z

    .line 9
    iput-boolean v2, p0, Lasb;->i:Z

    .line 10
    iput-boolean v2, p0, Lasb;->r:Z

    .line 11
    iput-object p3, p0, Lasb;->d:Lasb$a;

    .line 12
    iput-object p2, p0, Lasb;->l:Lahu$a;

    .line 13
    iput-object p1, p0, Lasb;->e:Landroid/content/Context;

    .line 14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lasb;->f:Landroid/content/res/Resources;

    .line 15
    new-instance v0, Lalj;

    iget-object v1, p0, Lasb;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lalj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lasb;->g:Lalj;

    .line 16
    iput v2, p0, Lasb;->b:I

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    .line 18
    return-void
.end method

.method static a(Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 22
    if-nez p0, :cond_0

    .line 23
    const/4 v0, 0x0

    .line 29
    :goto_0
    return v0

    .line 24
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    const-string v0, "starred"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 26
    :cond_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    .line 27
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    goto :goto_0

    .line 28
    :cond_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 29
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method private final a(Z)V
    .locals 0

    .prologue
    .line 19
    iput-boolean p1, p0, Lasb;->i:Z

    .line 20
    iput-boolean p1, p0, Lasb;->r:Z

    .line 21
    return-void
.end method

.method private b(I)Lahc;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    return-object v0
.end method

.method private final c(I)V
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Lasb;->n:Lahc;

    if-eqz v0, :cond_0

    iget v0, p0, Lasb;->q:I

    .line 56
    invoke-virtual {p0, v0}, Lasb;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0, p1}, Lasb;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lasb;->d:Lasb$a;

    invoke-interface {v0}, Lasb$a;->a()V

    .line 59
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v1, p0, Lasb;->q:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 60
    iput p1, p0, Lasb;->q:I

    .line 61
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v1, p0, Lasb;->q:I

    sget-object v2, Lahc;->a:Lahc;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 62
    sget-object v0, Lahc;->a:Lahc;

    iget-object v1, p0, Lasb;->n:Lahc;

    iget-wide v2, v1, Lahc;->j:J

    iput-wide v2, v0, Lahc;->j:J

    .line 63
    iget-object v0, p0, Lasb;->d:Lasb$a;

    const/4 v1, 0x0

    new-array v1, v1, [J

    invoke-interface {v0, v1}, Lasb$a;->a([J)V

    .line 64
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lasb;->a(Z)V

    .line 94
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    .line 95
    iget-object v1, p3, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->d:Lahc;

    .line 96
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 98
    invoke-virtual {p0, v1}, Lasb;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    iput-object v0, p0, Lasb;->n:Lahc;

    .line 100
    iput v1, p0, Lasb;->o:I

    .line 101
    iput v1, p0, Lasb;->q:I

    .line 102
    iget v0, p0, Lasb;->q:I

    invoke-direct {p0, v0}, Lasb;->c(I)V

    .line 103
    :cond_0
    return-void
.end method

.method final a(Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    const/16 v7, 0x15

    const/4 v2, 0x0

    .line 66
    new-instance v3, Ljava/util/PriorityQueue;

    iget-object v0, p0, Lasb;->m:Ljava/util/Comparator;

    invoke-direct {v3, v7, v0}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 67
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 68
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    .line 69
    :goto_0
    if-ge v1, v5, :cond_3

    .line 70
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    .line 71
    iget v6, v0, Lahc;->k:I

    if-gt v6, v7, :cond_0

    iget v6, v0, Lahc;->k:I

    if-nez v6, :cond_2

    .line 72
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 73
    :cond_2
    iget v6, v0, Lahc;->k:I

    if-ltz v6, :cond_1

    .line 74
    invoke-virtual {v3, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    :cond_3
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 77
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 78
    const/4 v0, 0x1

    move v1, v0

    :goto_2
    add-int/lit8 v0, v5, 0x1

    if-ge v1, v0, :cond_6

    .line 79
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v3}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    iget v0, v0, Lahc;->k:I

    if-gt v0, v1, :cond_5

    .line 80
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    .line 81
    iput v1, v0, Lahc;->k:I

    .line 82
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_4
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 83
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 84
    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 86
    :cond_6
    :goto_4
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 87
    invoke-virtual {v3}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    .line 88
    iput v2, v0, Lahc;->k:I

    .line 89
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 91
    :cond_7
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 92
    return-void
.end method

.method final a(I)Z
    .locals 1

    .prologue
    .line 54
    if-ltz p1, :cond_0

    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public final b(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 2

    .prologue
    .line 104
    if-nez p3, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    .line 107
    iget-object v1, p3, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->d:Lahc;

    .line 108
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 109
    iget-boolean v1, p0, Lasb;->r:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lasb;->q:I

    if-eq v1, v0, :cond_0

    .line 110
    invoke-virtual {p0, v0}, Lasb;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    if-ltz v0, :cond_0

    .line 111
    invoke-direct {p0, v0}, Lasb;->c(I)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 32
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lasb;->b(I)Lahc;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lasb;->b(I)Lahc;

    move-result-object v0

    iget-wide v0, v0, Lahc;->j:J

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 38
    .line 39
    instance-of v0, p2, Larx;

    if-eqz v0, :cond_1

    .line 40
    check-cast p2, Larx;

    move-object v0, p2

    .line 41
    :goto_0
    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lasb;->e:Landroid/content/Context;

    const v2, 0x7f0400a5

    .line 43
    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Larx;

    .line 44
    :cond_0
    iget-object v1, p0, Lasb;->j:Lbfo;

    .line 45
    iput-object v1, v0, Lahu;->c:Lbfo;

    .line 46
    iget-object v1, p0, Lasb;->l:Lahu$a;

    .line 47
    iput-object v1, v0, Lahu;->a:Lahu$a;

    .line 48
    invoke-direct {p0, p1}, Lasb;->b(I)Lahc;

    move-result-object v1

    invoke-virtual {v0, v1}, Larx;->a(Lahc;)V

    .line 50
    iput p1, v0, Larx;->i:I

    .line 51
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lasb;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0, v0}, Lasb;->a(Z)V

    .line 114
    iget-boolean v1, p0, Lasb;->h:Z

    if-nez v1, :cond_5

    .line 117
    iget-object v1, p0, Lasb;->n:Lahc;

    if-eqz v1, :cond_5

    .line 118
    iget v1, p0, Lasb;->q:I

    invoke-virtual {p0, v1}, Lasb;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lasb;->q:I

    iget v2, p0, Lasb;->o:I

    if-eq v1, v2, :cond_2

    .line 119
    iget v0, p0, Lasb;->q:I

    iput v0, p0, Lasb;->p:I

    .line 120
    iget-object v0, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v1, p0, Lasb;->p:I

    iget-object v2, p0, Lasb;->n:Lahc;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lasb;->d:Lasb$a;

    invoke-interface {v0}, Lasb$a;->a()V

    .line 122
    const/4 v0, 0x1

    .line 128
    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    iget v0, p0, Lasb;->p:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_4

    .line 129
    iget-object v2, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v1, p0, Lasb;->o:I

    iget v3, p0, Lasb;->p:I

    .line 131
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 132
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 133
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v1, v0

    .line 134
    :goto_1
    if-gt v1, v3, :cond_3

    .line 135
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahc;

    .line 136
    add-int/lit8 v5, v1, 0x1

    .line 137
    iget v6, v0, Lahc;->k:I

    if-eq v6, v5, :cond_1

    .line 138
    sget-object v6, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v8, v0, Lahc;->j:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 139
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 140
    const-string v7, "pinned"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 141
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 123
    :cond_2
    iget v1, p0, Lasb;->o:I

    invoke-virtual {p0, v1}, Lasb;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v2, p0, Lasb;->q:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 125
    iget-object v1, p0, Lasb;->a:Ljava/util/ArrayList;

    iget v2, p0, Lasb;->o:I

    iget-object v3, p0, Lasb;->n:Lahc;

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 126
    iget v1, p0, Lasb;->o:I

    iput v1, p0, Lasb;->p:I

    .line 127
    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 146
    :try_start_0
    iget-object v0, p0, Lasb;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 147
    iget-object v0, p0, Lasb;->e:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->g:Lbks$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 151
    :cond_4
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lasb;->n:Lahc;

    .line 152
    :cond_5
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    :goto_3
    sget-object v1, Lasb;->k:Ljava/lang/String;

    const-string v2, "Exception thrown when pinning contacts"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 149
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public final s()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 153
    iget-object v0, p0, Lasb;->n:Lahc;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lasb;->n:Lahc;

    iget-object v0, v0, Lahc;->h:Landroid/net/Uri;

    .line 155
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 156
    const-string v2, "starred"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 157
    const-string v2, "pinned"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    iget-object v2, p0, Lasb;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lasb;->h:Z

    .line 160
    iget-object v0, p0, Lasb;->e:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbks$a;->h:Lbks$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbks$a;)V

    .line 161
    :cond_0
    return-void
.end method
