.class public final Lgpk;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpk;


# instance fields
.field public priority:Ljava/lang/Integer;

.field public resolution:Lgpl;

.field public send:Ljava/lang/Boolean;

.field public ssrc:[I

.field public ssrcsKnown:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpk;->clear()Lgpk;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpk;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpk;->_emptyArray:[Lgpk;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpk;->_emptyArray:[Lgpk;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpk;

    sput-object v0, Lgpk;->_emptyArray:[Lgpk;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpk;->_emptyArray:[Lgpk;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpk;
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lgpk;

    invoke-direct {v0}, Lgpk;-><init>()V

    invoke-virtual {v0, p0}, Lgpk;->mergeFrom(Lhfp;)Lgpk;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpk;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lgpk;

    invoke-direct {v0}, Lgpk;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpk;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpk;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    .line 11
    iput-object v1, p0, Lgpk;->send:Ljava/lang/Boolean;

    .line 12
    iput-object v1, p0, Lgpk;->resolution:Lgpl;

    .line 13
    iput-object v1, p0, Lgpk;->priority:Ljava/lang/Integer;

    .line 14
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgpk;->ssrc:[I

    .line 15
    iput-object v1, p0, Lgpk;->unknownFieldData:Lhfv;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lgpk;->cachedSize:I

    .line 17
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 33
    iget-object v2, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 34
    const/4 v2, 0x1

    iget-object v3, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    .line 35
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 36
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 37
    add-int/2addr v0, v2

    .line 38
    :cond_0
    iget-object v2, p0, Lgpk;->send:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 39
    const/4 v2, 0x2

    iget-object v3, p0, Lgpk;->send:Ljava/lang/Boolean;

    .line 40
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 41
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 42
    add-int/2addr v0, v2

    .line 43
    :cond_1
    iget-object v2, p0, Lgpk;->resolution:Lgpl;

    if-eqz v2, :cond_2

    .line 44
    const/4 v2, 0x3

    iget-object v3, p0, Lgpk;->resolution:Lgpl;

    .line 45
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 46
    :cond_2
    iget-object v2, p0, Lgpk;->priority:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 47
    const/4 v2, 0x4

    iget-object v3, p0, Lgpk;->priority:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 49
    :cond_3
    iget-object v2, p0, Lgpk;->ssrc:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgpk;->ssrc:[I

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    .line 51
    :goto_0
    iget-object v3, p0, Lgpk;->ssrc:[I

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 52
    iget-object v3, p0, Lgpk;->ssrc:[I

    aget v3, v3, v1

    .line 54
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    :cond_4
    add-int/2addr v0, v2

    .line 57
    iget-object v1, p0, Lgpk;->ssrc:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 58
    :cond_5
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpk;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 59
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 60
    sparse-switch v0, :sswitch_data_0

    .line 62
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :sswitch_0
    return-object p0

    .line 64
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    goto :goto_0

    .line 66
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgpk;->send:Ljava/lang/Boolean;

    goto :goto_0

    .line 68
    :sswitch_3
    iget-object v0, p0, Lgpk;->resolution:Lgpl;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lgpl;

    invoke-direct {v0}, Lgpl;-><init>()V

    iput-object v0, p0, Lgpk;->resolution:Lgpl;

    .line 70
    :cond_1
    iget-object v0, p0, Lgpk;->resolution:Lgpl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 73
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 74
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgpk;->priority:Ljava/lang/Integer;

    goto :goto_0

    .line 76
    :sswitch_5
    const/16 v0, 0x28

    .line 77
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 78
    iget-object v0, p0, Lgpk;->ssrc:[I

    if-nez v0, :cond_3

    move v0, v1

    .line 79
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 80
    if-eqz v0, :cond_2

    .line 81
    iget-object v3, p0, Lgpk;->ssrc:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    .line 84
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 85
    aput v3, v2, v0

    .line 86
    invoke-virtual {p1}, Lhfp;->a()I

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 78
    :cond_3
    iget-object v0, p0, Lgpk;->ssrc:[I

    array-length v0, v0

    goto :goto_1

    .line 89
    :cond_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 90
    aput v3, v2, v0

    .line 91
    iput-object v2, p0, Lgpk;->ssrc:[I

    goto :goto_0

    .line 93
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 94
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 96
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 97
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_5

    .line 99
    invoke-virtual {p1}, Lhfp;->g()I

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 102
    :cond_5
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 103
    iget-object v2, p0, Lgpk;->ssrc:[I

    if-nez v2, :cond_7

    move v2, v1

    .line 104
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 105
    if-eqz v2, :cond_6

    .line 106
    iget-object v4, p0, Lgpk;->ssrc:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 107
    :cond_6
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_8

    .line 109
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 110
    aput v4, v0, v2

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 103
    :cond_7
    iget-object v2, p0, Lgpk;->ssrc:[I

    array-length v2, v2

    goto :goto_4

    .line 112
    :cond_8
    iput-object v0, p0, Lgpk;->ssrc:[I

    .line 113
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0, p1}, Lgpk;->mergeFrom(Lhfp;)Lgpk;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 19
    const/4 v0, 0x1

    iget-object v1, p0, Lgpk;->ssrcsKnown:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 20
    :cond_0
    iget-object v0, p0, Lgpk;->send:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 21
    const/4 v0, 0x2

    iget-object v1, p0, Lgpk;->send:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 22
    :cond_1
    iget-object v0, p0, Lgpk;->resolution:Lgpl;

    if-eqz v0, :cond_2

    .line 23
    const/4 v0, 0x3

    iget-object v1, p0, Lgpk;->resolution:Lgpl;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_2
    iget-object v0, p0, Lgpk;->priority:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 25
    const/4 v0, 0x4

    iget-object v1, p0, Lgpk;->priority:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->c(II)V

    .line 26
    :cond_3
    iget-object v0, p0, Lgpk;->ssrc:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpk;->ssrc:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 27
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgpk;->ssrc:[I

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 28
    const/4 v1, 0x5

    iget-object v2, p0, Lgpk;->ssrc:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 31
    return-void
.end method
