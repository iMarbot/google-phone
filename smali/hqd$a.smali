.class public Lhqd$a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhqd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Ldmx;


# direct methods
.method public constructor <init>(Ldmx;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lhqd$a;->a:Ldmx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1
    const-string v0, "VideoShareSessionImpl.cameraStarting"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 2
    return-void
.end method

.method public a(Lhqd;)V
    .locals 2

    .prologue
    .line 7
    const-string v0, "VideoShareSessionImpl.cameraStopping"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lhqd$a;->a:Ldmx;

    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0, p1, v1}, Ldmx;->a(Lhqd;Landroid/hardware/Camera;)V

    .line 10
    return-void
.end method

.method public a(Lhqd;Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 3
    const-string v0, "VideoShareSessionImpl.cameraStarted"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lhqd$a;->a:Ldmx;

    .line 5
    invoke-virtual {v0, p1, p2}, Ldmx;->a(Lhqd;Landroid/hardware/Camera;)V

    .line 6
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "VideoShareSessionImpl.cameraStopped"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 12
    return-void
.end method
