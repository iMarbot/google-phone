.class public Lflb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2
    const/4 v2, 0x0

    const-string v3, ""

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lflb;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lflb;->a:Ljava/lang/String;

    iput-object p2, p0, Lflb;->b:Landroid/net/Uri;

    iput-object p3, p0, Lflb;->c:Ljava/lang/String;

    iput-object p4, p0, Lflb;->d:Ljava/lang/String;

    iput-boolean p5, p0, Lflb;->e:Z

    iput-boolean p6, p0, Lflb;->f:Z

    return-void
.end method

.method public static synthetic a(Lflb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lflb;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lflb;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lflb;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lflb;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lflb;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public static synthetic c(Lflb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lflb;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic d(Lflb;)Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lflb;->f:Z

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lezn;
    .locals 1

    .prologue
    .line 8
    invoke-static {p0, p1, p2}, Lezn;->a(Lflb;Ljava/lang/String;I)Lezn;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;J)Lezn;
    .locals 2

    .prologue
    .line 6
    invoke-static {p0, p1, p2, p3}, Lezn;->a(Lflb;Ljava/lang/String;J)Lezn;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lezn;
    .locals 1

    .prologue
    .line 9
    invoke-static {p0, p1, p2}, Lezn;->a(Lflb;Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)Lezn;
    .locals 1

    .prologue
    .line 7
    invoke-static {p0, p1, p2}, Lezn;->a(Lflb;Ljava/lang/String;Z)Lezn;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lflb;
    .locals 7

    .prologue
    .line 4
    iget-boolean v0, p0, Lflb;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set GServices prefix and skip GServices"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lflb;

    iget-object v1, p0, Lflb;->a:Ljava/lang/String;

    iget-object v2, p0, Lflb;->b:Landroid/net/Uri;

    iget-object v4, p0, Lflb;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lflb;->e:Z

    iget-boolean v6, p0, Lflb;->f:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lflb;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public b(Ljava/lang/String;)Lflb;
    .locals 7

    .prologue
    .line 5
    new-instance v0, Lflb;

    iget-object v1, p0, Lflb;->a:Ljava/lang/String;

    iget-object v2, p0, Lflb;->b:Landroid/net/Uri;

    iget-object v3, p0, Lflb;->c:Ljava/lang/String;

    iget-boolean v5, p0, Lflb;->e:Z

    iget-boolean v6, p0, Lflb;->f:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lflb;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 10
    iget-boolean v0, p0, Lflb;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflb;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
