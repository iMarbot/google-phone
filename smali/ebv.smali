.class public final Lebv;
.super Ljava/lang/Object;


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Lesc;

.field public synthetic c:Lebu;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Lebx;

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/ArrayList;

.field private k:Ljava/util/ArrayList;

.field private l:Ljava/util/ArrayList;

.field private m:Ljava/util/ArrayList;

.field private n:Z

.field private o:Z


# direct methods
.method constructor <init>(Lebu;Lebx;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lebv;-><init>(Lebu;[BLebx;)V

    return-void
.end method

.method constructor <init>(Lebu;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lebv;-><init>(Lebu;[BLebx;)V

    return-void
.end method

.method private constructor <init>(Lebu;[BLebx;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lebv;->c:Lebu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->a(Lebu;)I

    move-result v0

    iput v0, p0, Lebv;->d:I

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->b(Lebu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->e:Ljava/lang/String;

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->c(Lebu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->a:Ljava/lang/String;

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->d(Lebu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->f:Ljava/lang/String;

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->e(Lebu;)I

    move-result v0

    iput v0, p0, Lebv;->g:I

    iput-object v1, p0, Lebv;->i:Ljava/util/ArrayList;

    iput-object v1, p0, Lebv;->j:Ljava/util/ArrayList;

    iput-object v1, p0, Lebv;->k:Ljava/util/ArrayList;

    iput-object v1, p0, Lebv;->l:Ljava/util/ArrayList;

    iput-object v1, p0, Lebv;->m:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lebv;->n:Z

    new-instance v0, Lesc;

    invoke-direct {v0}, Lesc;-><init>()V

    iput-object v0, p0, Lebv;->b:Lesc;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lebv;->o:Z

    invoke-static {p1}, Lebu;->c(Lebu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->a:Ljava/lang/String;

    invoke-static {p1}, Lebu;->d(Lebu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebv;->f:Ljava/lang/String;

    iget-object v0, p0, Lebv;->b:Lesc;

    invoke-static {p1}, Lebu;->f(Lebu;)Lekm;

    move-result-object v1

    invoke-interface {v1}, Lekm;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lesc;->a:J

    iget-object v0, p0, Lebv;->b:Lesc;

    invoke-static {p1}, Lebu;->f(Lebu;)Lekm;

    move-result-object v1

    invoke-interface {v1}, Lekm;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lesc;->d:J

    iget-object v0, p0, Lebv;->b:Lesc;

    invoke-static {p1}, Lebu;->g(Lebu;)Leby;

    iget-object v1, p0, Lebv;->b:Lesc;

    iget-wide v2, v1, Lesc;->a:J

    .line 2
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    .line 3
    iput-wide v2, v0, Lesc;->g:J

    if-eqz p2, :cond_0

    iget-object v0, p0, Lebv;->b:Lesc;

    iput-object p2, v0, Lesc;->f:[B

    :cond_0
    iput-object p3, p0, Lebv;->h:Lebx;

    return-void
.end method


# virtual methods
.method public final a()Ledn;
    .locals 14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 4
    iget-boolean v0, p0, Lebv;->o:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "do not reuse LogEventBuilder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebv;->o:Z

    .line 5
    new-instance v12, Leca;

    new-instance v0, Lecb;

    iget-object v1, p0, Lebv;->c:Lebu;

    invoke-static {v1}, Lebu;->i(Lebu;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lebv;->c:Lebu;

    invoke-static {v2}, Lebu;->j(Lebu;)I

    move-result v2

    iget v3, p0, Lebv;->d:I

    iget-object v4, p0, Lebv;->e:Ljava/lang/String;

    iget-object v5, p0, Lebv;->a:Ljava/lang/String;

    iget-object v6, p0, Lebv;->f:Ljava/lang/String;

    iget-object v7, p0, Lebv;->c:Lebu;

    invoke-static {v7}, Lebu;->h(Lebu;)Z

    move-result v7

    iget v8, p0, Lebv;->g:I

    invoke-direct/range {v0 .. v8}, Lecb;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    iget-object v3, p0, Lebv;->b:Lesc;

    iget-object v4, p0, Lebv;->h:Lebx;

    invoke-static {v13}, Lebu;->a(Ljava/util/ArrayList;)[I

    move-result-object v6

    invoke-static {v13}, Lebu;->a(Ljava/util/ArrayList;)[I

    move-result-object v8

    iget-boolean v11, p0, Lebv;->n:Z

    move-object v1, v12

    move-object v2, v0

    move-object v5, v13

    move-object v7, v13

    move-object v9, v13

    move-object v10, v13

    invoke-direct/range {v1 .. v11}, Leca;-><init>(Lecb;Lesc;Lebx;Lebx;[I[Ljava/lang/String;[I[[B[Lezg;Z)V

    .line 6
    iget-object v0, v12, Leca;->a:Lecb;

    iget-object v1, p0, Lebv;->c:Lebu;

    invoke-static {v1}, Lebu;->k(Lebu;)Lebw;

    move-result-object v1

    iget-object v2, v0, Lecb;->b:Ljava/lang/String;

    iget v0, v0, Lecb;->a:I

    invoke-interface {v1, v2, v0}, Lebw;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lebv;->c:Lebu;

    invoke-static {v0}, Lebu;->l(Lebu;)Lebz;

    move-result-object v0

    invoke-interface {v0, v12}, Lebz;->a(Leca;)Ledn;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Letf;->a(Lcom/google/android/gms/common/api/Status;)Ledn;

    move-result-object v0

    goto :goto_0
.end method
