.class public Lhkl;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhkl$c;,
        Lhkl$f;,
        Lhkl$d;,
        Lhkl$g;,
        Lhkl$e;,
        Lhkl$b;,
        Lhkl$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field private static f:Lhlr;

.field private static g:Lhkl;

.field private static h:Ljava/util/concurrent/atomic/AtomicReference;


# instance fields
.field public b:Ljava/util/ArrayList;

.field public c:Lhkl$b;

.field public final d:Lhkl$a;

.field public final e:Lhlr;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 97
    const-class v0, Lhkl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lhkl;->a:Ljava/util/logging/Logger;

    .line 98
    new-instance v0, Lhlr;

    invoke-direct {v0}, Lhlr;-><init>()V

    sput-object v0, Lhkl;->f:Lhlr;

    .line 99
    new-instance v0, Lhkl;

    const/4 v1, 0x0

    sget-object v2, Lhkl;->f:Lhlr;

    invoke-direct {v0, v1, v2}, Lhkl;-><init>(Lhkl;Lhlr;)V

    sput-object v0, Lhkl;->g:Lhkl;

    .line 100
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lhkl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method private constructor <init>(Lhkl;Lhlr;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v1, Lhkl$f;

    .line 24
    invoke-direct {v1, p0}, Lhkl$f;-><init>(Lhkl;)V

    .line 25
    iput-object v1, p0, Lhkl;->c:Lhkl$b;

    .line 27
    if-nez v0, :cond_1

    .line 32
    :goto_0
    iput-object v0, p0, Lhkl;->d:Lhkl$a;

    .line 33
    iput-object p2, p0, Lhkl;->e:Lhlr;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lhkl;->i:I

    .line 35
    iget v0, p0, Lhkl;->i:I

    .line 36
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    .line 37
    sget-object v0, Lhkl;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.Context"

    const-string v3, "validateGeneration"

    const-string v4, "Context ancestry chain length is abnormally long. This suggests an error in application code. Length exceeded: 1000"

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    :cond_0
    return-void

    .line 29
    :cond_1
    instance-of v1, v0, Lhkl$a;

    if-eqz v1, :cond_2

    .line 30
    check-cast v0, Lhkl$a;

    goto :goto_0

    .line 31
    :cond_2
    iget-object v0, v0, Lhkl;->d:Lhkl$a;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lhkl$e;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lhkl$e;

    invoke-direct {v0, p0}, Lhkl$e;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a()Lhkl;
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lhkl;->g()Lhkl$g;

    move-result-object v0

    invoke-virtual {v0}, Lhkl$g;->a()Lhkl;

    move-result-object v0

    .line 19
    if-nez v0, :cond_0

    .line 20
    sget-object v0, Lhkl;->g:Lhkl;

    .line 21
    :cond_0
    return-object v0
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 94
    if-nez p0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    return-object p0
.end method

.method private static g()Lhkl$g;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhkl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkl$g;

    .line 2
    if-nez v0, :cond_0

    .line 3
    invoke-static {}, Lhkl;->h()Lhkl$g;

    move-result-object v0

    .line 4
    :cond_0
    return-object v0
.end method

.method private static h()Lhkl$g;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 5
    :try_start_0
    const-string v0, "io.grpc.override.ContextStorageOverride"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 6
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkl$g;

    .line 7
    sget-object v1, Lhkl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 16
    :cond_0
    :goto_0
    sget-object v0, Lhkl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkl$g;

    return-object v0

    .line 9
    :catch_0
    move-exception v5

    .line 10
    new-instance v0, Lhmd;

    invoke-direct {v0}, Lhmd;-><init>()V

    .line 11
    sget-object v1, Lhkl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v3, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    sget-object v0, Lhkl;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.Context"

    const-string v3, "createStorage"

    const-string v4, "Storage override doesn\'t exist. Using default"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 14
    :catch_1
    move-exception v0

    .line 15
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Storage override failed to initialize"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lhkl$b;)V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lhkl;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 80
    :cond_0
    monitor-enter p0

    .line 81
    :try_start_0
    iget-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 82
    iget-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 83
    iget-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkl$d;

    .line 84
    iget-object v0, v0, Lhkl$d;->a:Lhkl$b;

    .line 85
    if-ne v0, p1, :cond_4

    .line 86
    iget-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 89
    :cond_1
    iget-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    iget-object v1, p0, Lhkl;->c:Lhkl$b;

    invoke-virtual {v0, v1}, Lhkl$a;->a(Lhkl$b;)V

    .line 92
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lhkl;->b:Ljava/util/ArrayList;

    .line 93
    :cond_3
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 88
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lhkl$b;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 59
    const-string v0, "cancellationListener"

    invoke-static {p1, v0}, Lhkl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v0, "executor"

    invoke-static {p2, v0}, Lhkl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-virtual {p0}, Lhkl;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    new-instance v0, Lhkl$d;

    .line 63
    invoke-direct {v0, p0, p2, p1}, Lhkl$d;-><init>(Lhkl;Ljava/util/concurrent/Executor;Lhkl$b;)V

    .line 65
    monitor-enter p0

    .line 66
    :try_start_0
    invoke-virtual {p0}, Lhkl;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    invoke-virtual {v0}, Lhkl$d;->a()V

    .line 76
    :cond_0
    :goto_0
    monitor-exit p0

    .line 77
    :cond_1
    return-void

    .line 70
    :cond_2
    iget-object v1, p0, Lhkl;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhkl;->b:Ljava/util/ArrayList;

    .line 72
    iget-object v1, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    iget-object v1, p0, Lhkl;->c:Lhkl$b;

    sget-object v2, Lhkl$c;->a:Lhkl$c;

    invoke-virtual {v0, v1, v2}, Lhkl$a;->a(Lhkl$b;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 75
    :cond_3
    :try_start_1
    iget-object v1, p0, Lhkl;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public a(Lhkl;)V
    .locals 1

    .prologue
    .line 44
    const-string v0, "toAttach"

    invoke-static {p1, v0}, Lhkl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-static {}, Lhkl;->g()Lhkl$g;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lhkl$g;->a(Lhkl;Lhkl;)V

    .line 46
    return-void
.end method

.method b()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lhkl;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lhkl;->g()Lhkl$g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lhkl$g;->a(Lhkl;)Lhkl;

    move-result-object v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    sget-object v0, Lhkl;->g:Lhkl;

    .line 43
    :cond_0
    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    .line 50
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public e()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    return-object v0

    .line 53
    :cond_0
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    .line 54
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public f()Lhkm;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    return-object v0

    .line 57
    :cond_0
    iget-object v0, p0, Lhkl;->d:Lhkl$a;

    .line 58
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method
