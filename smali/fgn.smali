.class public final Lfgn;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic a:J

.field private synthetic b:Lflh;

.field private synthetic c:Lfgm;


# direct methods
.method public constructor <init>(Lfgm;JLflh;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfgn;->c:Lfgm;

    iput-wide p2, p0, Lfgn;->a:J

    iput-object p4, p0, Lfgn;->b:Lflh;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    :try_start_0
    iget-object v0, p0, Lfgn;->c:Lfgm;

    .line 3
    iget-object v0, v0, Lfgm;->a:Ljava/util/concurrent/CountDownLatch;

    .line 4
    iget-wide v2, p0, Lfgn;->a:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    :goto_0
    return-object v4

    .line 6
    :catch_0
    move-exception v0

    .line 7
    const-string v1, "FeedbackSender.sendFeedback"

    invoke-static {v1, v4, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lfgn;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 9
    .line 10
    iget-object v0, p0, Lfgn;->c:Lfgm;

    .line 11
    iget-object v0, v0, Lfgm;->b:Lfjz;

    .line 12
    invoke-interface {v0}, Lfjz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    const-string v0, "FeedbackSender.sendFeedback"

    const-string v1, "calling startFeedback"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lfgn;->c:Lfgm;

    .line 15
    iget-object v0, v0, Lfgm;->c:Lfle;

    .line 16
    iget-object v1, p0, Lfgn;->c:Lfgm;

    .line 17
    iget-object v1, v1, Lfgm;->b:Lfjz;

    .line 18
    invoke-virtual {v0, v1}, Lfle;->a(Lfjz;)Lfld;

    move-result-object v0

    .line 19
    iget-object v1, p0, Lfgn;->b:Lflh;

    invoke-interface {v0, v1}, Lfld;->a(Lflh;)Lfkc;

    move-result-object v0

    .line 20
    iget-object v1, p0, Lfgn;->c:Lfgm;

    invoke-virtual {v0, v1}, Lfkc;->a(Lfke;)V

    .line 23
    :goto_0
    return-void

    .line 22
    :cond_0
    const-string v0, "FeedbackSender.sendFeedback"

    const-string v1, "API not connected, skipping"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
