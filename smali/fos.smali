.class public final Lfos;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfoz;


# instance fields
.field public final callDirector:Lfnp;

.field public final callStateListener:Lfow;

.field public currentCodec:I

.field public encoderUpdateCallback:Lfpa;

.field public final finalTransform:[F

.field public final glManager:Lfpc;

.field public lastCaptureSize:Lfwo;

.field public lastCaptureTimestampMicros:J

.field public lastIsScreencast:Z

.field public lastViewRequestPixelCount:I

.field public libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

.field public mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

.field public mirrorMatrix:[F


# direct methods
.method public constructor <init>(Lfnp;ZLfpa;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfos;->lastCaptureTimestampMicros:J

    .line 3
    iput-object v2, p0, Lfos;->mirrorMatrix:[F

    .line 4
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lfos;->finalTransform:[F

    .line 5
    iput-object p1, p0, Lfos;->callDirector:Lfnp;

    .line 6
    iput-object p3, p0, Lfos;->encoderUpdateCallback:Lfpa;

    .line 7
    invoke-virtual {p1}, Lfnp;->getGlManager()Lfpc;

    move-result-object v0

    iput-object v0, p0, Lfos;->glManager:Lfpc;

    .line 8
    const/4 v0, 0x0

    iput v0, p0, Lfos;->currentCodec:I

    .line 9
    if-eqz p2, :cond_0

    .line 10
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    invoke-direct {v0, p1, p4}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;-><init>(Lfnp;I)V

    iput-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    .line 11
    :cond_0
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;-><init>()V

    iput-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    .line 12
    new-instance v0, Lfow;

    invoke-direct {v0, p0, v2}, Lfow;-><init>(Lfos;Lfmt;)V

    iput-object v0, p0, Lfos;->callStateListener:Lfow;

    .line 13
    new-instance v0, Lfot;

    invoke-direct {v0, p0, p1}, Lfot;-><init>(Lfos;Lfnp;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 14
    return-void
.end method

.method static synthetic access$100(Lfos;)Lfpc;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lfos;->glManager:Lfpc;

    return-object v0
.end method

.method static synthetic access$202(Lfos;I)I
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lfos;->lastViewRequestPixelCount:I

    return p1
.end method

.method static synthetic access$300(Lfos;)Lfwo;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfos;->lastCaptureSize:Lfwo;

    return-object v0
.end method

.method static synthetic access$400(Lfos;)Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lfos;->lastIsScreencast:Z

    return v0
.end method


# virtual methods
.method final calculateEncodeSizeForCurrentCodec()Lfwo;
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lfos;->getCurrentCodec()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 21
    invoke-virtual {p0}, Lfos;->getCurrentCodec()I

    move-result v0

    .line 23
    :goto_0
    invoke-static {v0}, Lfor;->getOutgoingVideoSpec(I)Lfwp;

    move-result-object v0

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    .line 24
    iget v1, p0, Lfos;->lastViewRequestPixelCount:I

    if-eqz v1, :cond_0

    .line 25
    iget v1, p0, Lfos;->lastViewRequestPixelCount:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 27
    :cond_0
    iget-object v1, p0, Lfos;->lastCaptureSize:Lfwo;

    invoke-static {v1, v0}, Lfwo;->a(Lfwo;I)Lfwo;

    move-result-object v0

    return-object v0

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final encodeFrame(IJZ[F)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 43
    iget-object v0, p0, Lfos;->mirrorMatrix:[F

    if-eqz v0, :cond_4

    .line 44
    iget-object v0, p0, Lfos;->mirrorMatrix:[F

    iget-object v1, p0, Lfos;->finalTransform:[F

    invoke-static {p5, v0, v1}, Lfwm;->a([F[F[F)V

    .line 45
    iget-object v5, p0, Lfos;->finalTransform:[F

    .line 46
    :goto_0
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->nativeAlignCaptureTimeMicros(J)J

    move-result-wide v2

    .line 48
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    if-eqz v0, :cond_3

    .line 49
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    move v1, p1

    move v4, p4

    .line 50
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encodeFrame(IJZ[F)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    .line 51
    :goto_1
    if-nez v0, :cond_0

    .line 52
    iget-object v1, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    invoke-virtual {v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->getCurrentCodec()I

    move-result v1

    invoke-virtual {p0, v1}, Lfos;->setCurrentCodec(I)V

    .line 53
    iget-object v1, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->sendFakeFrame(J)V

    .line 54
    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    move v1, p1

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encodeFrame(IJZ[F)Z

    .line 56
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->getCurrentCodec()I

    move-result v0

    invoke-virtual {p0, v0}, Lfos;->setCurrentCodec(I)V

    .line 57
    :cond_1
    iput-wide v2, p0, Lfos;->lastCaptureTimestampMicros:J

    .line 58
    return v6

    .line 50
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_2

    :cond_4
    move-object v5, p5

    goto :goto_0
.end method

.method public final getCurrentCodec()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lfos;->currentCodec:I

    return v0
.end method

.method public final initializeGLContext()V
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initializeGLContext()V

    .line 17
    :cond_0
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    if-eqz v0, :cond_1

    .line 18
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->initializeGLContext()V

    .line 19
    :cond_1
    return-void
.end method

.method final synthetic lambda$new$0$Encoder(Lfnp;)V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iget-object v1, p0, Lfos;->callStateListener:Lfow;

    invoke-virtual {v0, v1}, Lfnv;->addCallStateListener(Lfof;)V

    return-void
.end method

.method final synthetic lambda$release$2$Encoder()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lfos;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iget-object v1, p0, Lfos;->callStateListener:Lfow;

    invoke-virtual {v0, v1}, Lfnv;->removeCallStateListener(Lfof;)V

    return-void
.end method

.method final synthetic lambda$setCurrentCodec$1$Encoder(I)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lfos;->encoderUpdateCallback:Lfpa;

    invoke-interface {v0, p1}, Lfpa;->onCurrentCodecChanged(I)V

    return-void
.end method

.method public final release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    new-instance v0, Lfov;

    invoke-direct {v0, p0}, Lfov;-><init>(Lfos;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 67
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->release()V

    .line 69
    iput-object v1, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    .line 70
    :cond_0
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->release()V

    .line 72
    iput-object v1, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    .line 73
    :cond_1
    return-void
.end method

.method final setCurrentCodec(I)V
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lfos;->currentCodec:I

    if-eq v0, p1, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iput p1, p0, Lfos;->currentCodec:I

    .line 63
    iget-object v0, p0, Lfos;->encoderUpdateCallback:Lfpa;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lfos;->glManager:Lfpc;

    new-instance v1, Lfou;

    invoke-direct {v1, p0, p1}, Lfou;-><init>(Lfos;I)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final setFlipNeeded(Z)V
    .locals 1

    .prologue
    .line 28
    if-eqz p1, :cond_0

    .line 29
    sget-object v0, Lfwm;->b:[F

    .line 30
    :goto_0
    iput-object v0, p0, Lfos;->mirrorMatrix:[F

    .line 31
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setResolution(IIZ)V
    .locals 6

    .prologue
    .line 32
    iput-boolean p3, p0, Lfos;->lastIsScreencast:Z

    .line 33
    new-instance v0, Lfwo;

    invoke-direct {v0, p1, p2}, Lfwo;-><init>(II)V

    iput-object v0, p0, Lfos;->lastCaptureSize:Lfwo;

    .line 34
    if-eqz p3, :cond_2

    iget-object v0, p0, Lfos;->lastCaptureSize:Lfwo;

    .line 35
    :goto_0
    iget-object v1, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    iget v2, v0, Lfwo;->a:I

    iget v3, v0, Lfwo;->b:I

    invoke-virtual {v1, v2, v3, p3}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->setResolution(IIZ)V

    .line 37
    iget-object v1, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lfos;->lastCaptureTimestampMicros:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 38
    iget-wide v2, p0, Lfos;->lastCaptureTimestampMicros:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lfos;->lastCaptureTimestampMicros:J

    .line 39
    iget-object v1, p0, Lfos;->libjingleSoftwareEncoder:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;

    iget-wide v2, p0, Lfos;->lastCaptureTimestampMicros:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->sendFakeFrame(J)V

    .line 40
    :cond_0
    iget-object v1, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    if-eqz v1, :cond_1

    .line 41
    iget-object v1, p0, Lfos;->mediaCodecSimulcastEncoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    iget v2, v0, Lfwo;->a:I

    iget v0, v0, Lfwo;->b:I

    invoke-virtual {v1, v2, v0, p3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->setResolution(IIZ)V

    .line 42
    :cond_1
    return-void

    .line 34
    :cond_2
    invoke-virtual {p0}, Lfos;->calculateEncodeSizeForCurrentCodec()Lfwo;

    move-result-object v0

    goto :goto_0
.end method
