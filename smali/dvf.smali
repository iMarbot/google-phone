.class public final Ldvf;
.super Lduz;


# instance fields
.field public final a:Ldvh;

.field public b:Lcom/google/android/gms/analytics/internal/zzaz;

.field private c:Ldtt;

.field private d:Ldup;


# direct methods
.method protected constructor <init>(Ldvb;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lduz;-><init>(Ldvb;)V

    new-instance v0, Ldup;

    .line 2
    iget-object v1, p1, Ldvb;->c:Lekm;

    .line 3
    invoke-direct {v0, v1}, Ldup;-><init>(Lekm;)V

    iput-object v0, p0, Ldvf;->d:Ldup;

    new-instance v0, Ldvh;

    invoke-direct {v0, p0}, Ldvh;-><init>(Ldvf;)V

    iput-object v0, p0, Ldvf;->a:Ldvh;

    new-instance v0, Ldvg;

    invoke-direct {v0, p0, p1}, Ldvg;-><init>(Ldvf;Ldvb;)V

    iput-object v0, p0, Ldvf;->c:Ldtt;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 0

    return-void
.end method

.method public final a(Ldua;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 4
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    iget-object v0, p0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    if-nez v0, :cond_0

    move v0, v6

    .line 10
    :goto_0
    return v0

    .line 5
    :cond_0
    iget-boolean v1, p1, Ldua;->f:Z

    .line 6
    if-eqz v1, :cond_1

    invoke-static {}, Ldts;->g()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 7
    :try_start_0
    iget-object v1, p1, Ldua;->a:Ljava/util/Map;

    .line 9
    iget-wide v2, p1, Ldua;->d:J

    .line 10
    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/analytics/internal/zzaz;->zza(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0}, Ldvf;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    .line 6
    :cond_1
    invoke-static {}, Ldts;->h()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 10
    :catch_0
    move-exception v0

    const-string v0, "Failed to send hits to AnalyticsService"

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    move v0, v6

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    iget-object v0, p0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 4

    .prologue
    .line 11
    iget-object v0, p0, Ldvf;->d:Ldup;

    invoke-virtual {v0}, Ldup;->a()V

    iget-object v1, p0, Ldvf;->c:Ldtt;

    sget-object v0, Ldtc;->A:Ldtd;

    .line 12
    iget-object v0, v0, Ldtd;->a:Ljava/lang/Object;

    .line 13
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ldtt;->a(J)V

    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 14
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {p0}, Lduz;->m()V

    :try_start_0
    invoke-static {}, Leki;->a()Leki;

    invoke-virtual {p0}, Lduy;->h()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ldvf;->a:Ldvh;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Ldvf;->b:Lcom/google/android/gms/analytics/internal/zzaz;

    .line 15
    iget-object v0, p0, Lduy;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->c()Ldub;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Ldub;->d()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 14
    :catch_1
    move-exception v0

    goto :goto_0
.end method
