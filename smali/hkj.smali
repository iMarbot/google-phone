.class public final enum Lhkj;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhkj;

.field public static final enum b:Lhkj;

.field public static final enum c:Lhkj;

.field public static final enum d:Lhkj;

.field public static final enum e:Lhkj;

.field private static synthetic f:[Lhkj;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lhkj;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v2}, Lhkj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkj;->a:Lhkj;

    .line 4
    new-instance v0, Lhkj;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, Lhkj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkj;->b:Lhkj;

    .line 5
    new-instance v0, Lhkj;

    const-string v1, "TRANSIENT_FAILURE"

    invoke-direct {v0, v1, v4}, Lhkj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkj;->c:Lhkj;

    .line 6
    new-instance v0, Lhkj;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v5}, Lhkj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkj;->d:Lhkj;

    .line 7
    new-instance v0, Lhkj;

    const-string v1, "SHUTDOWN"

    invoke-direct {v0, v1, v6}, Lhkj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhkj;->e:Lhkj;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lhkj;

    sget-object v1, Lhkj;->a:Lhkj;

    aput-object v1, v0, v2

    sget-object v1, Lhkj;->b:Lhkj;

    aput-object v1, v0, v3

    sget-object v1, Lhkj;->c:Lhkj;

    aput-object v1, v0, v4

    sget-object v1, Lhkj;->d:Lhkj;

    aput-object v1, v0, v5

    sget-object v1, Lhkj;->e:Lhkj;

    aput-object v1, v0, v6

    sput-object v0, Lhkj;->f:[Lhkj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhkj;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhkj;->f:[Lhkj;

    invoke-virtual {v0}, [Lhkj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhkj;

    return-object v0
.end method
