.class public final Ldon;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# instance fields
.field private a:Ldom;

.field private b:Lhqc;


# direct methods
.method public constructor <init>(Ldom;Lhqc;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldon;->a:Ldom;

    .line 3
    iput-object p2, p0, Ldon;->b:Lhqc;

    .line 4
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 5
    .line 6
    iget-object v0, p0, Ldon;->b:Lhqc;

    .line 7
    invoke-interface {v0}, Lhqc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 9
    invoke-static {}, Lbdf;->c()V

    .line 10
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_0

    .line 11
    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "moto_sim_suggestion_allowed"

    invoke-interface {v2, v3, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 13
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.motorola.android.providers.userpreferredsim"

    const/high16 v3, 0x100000

    .line 14
    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 15
    :goto_0
    if-eqz v0, :cond_1

    .line 16
    new-instance v0, Ldop;

    invoke-direct {v0}, Ldop;-><init>()V

    .line 18
    :goto_1
    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 19
    invoke-static {v0, v1}, Lio/grpc/internal/av;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbnv;

    .line 20
    return-object v0

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17
    :cond_1
    new-instance v0, Lbny;

    invoke-direct {v0}, Lbny;-><init>()V

    goto :goto_1
.end method
