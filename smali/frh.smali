.class final Lfrh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final call:Lfnp;

.field public endpoint:Lfue;

.field public final participantCollection:Lfnf;

.field public final participantInfo:Lfvz;


# direct methods
.method constructor <init>(Lfnp;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfrh;->call:Lfnp;

    .line 3
    invoke-virtual {p1}, Lfnp;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnf;

    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    iput-object v0, p0, Lfrh;->participantCollection:Lfnf;

    .line 4
    new-instance v0, Lfvz;

    invoke-direct {v0}, Lfvz;-><init>()V

    iput-object v0, p0, Lfrh;->participantInfo:Lfvz;

    .line 5
    return-void
.end method

.method private final refreshInternalState()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Lfrh;->isLocalUser()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 33
    iget-object v0, p0, Lfrh;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getVideoCapturer()Lfwd;

    move-result-object v0

    .line 34
    iget-object v3, p0, Lfrh;->participantInfo:Lfvz;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lfwd;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 35
    :goto_0
    iput-boolean v0, v3, Lfvz;->c:Z

    .line 36
    iget-object v0, p0, Lfrh;->endpoint:Lfue;

    if-nez v0, :cond_4

    .line 37
    iget-object v0, p0, Lfrh;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getAudioCapturer()Lfvp;

    move-result-object v0

    .line 38
    iget-object v3, p0, Lfrh;->participantInfo:Lfvz;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lfvp;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 39
    :cond_1
    :goto_1
    iput-boolean v2, v3, Lfvz;->b:Z

    .line 40
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    .line 41
    iput v1, v0, Lfvz;->h:I

    .line 47
    :goto_2
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    .line 48
    iput-boolean v1, v0, Lfvz;->m:Z

    .line 49
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v1, p0, Lfrh;->participantCollection:Lfnf;

    invoke-interface {v1}, Lfnf;->getLocalParticipant()Lgnm;

    move-result-object v1

    .line 50
    iput-object v1, v0, Lfvz;->n:Lgnm;

    .line 83
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 34
    goto :goto_0

    :cond_3
    move v2, v1

    .line 38
    goto :goto_1

    .line 43
    :cond_4
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v2, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v2}, Lfue;->isAudioMuted()Z

    move-result v2

    .line 44
    iput-boolean v2, v0, Lfvz;->b:Z

    .line 45
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v2, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v2}, Lfue;->getConnectionStatus()I

    move-result v2

    .line 46
    iput v2, v0, Lfvz;->h:I

    goto :goto_2

    .line 52
    :cond_5
    iget-object v0, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v0}, Lfue;->getVideoStreamIds()Ljava/util/Set;

    move-result-object v3

    .line 53
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 54
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    .line 55
    iput-boolean v1, v0, Lfvz;->c:Z

    .line 59
    :goto_4
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v4, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v4}, Lfue;->isAudioMuted()Z

    move-result v4

    .line 60
    iput-boolean v4, v0, Lfvz;->b:Z

    .line 61
    iget-object v0, p0, Lfrh;->participantCollection:Lfnf;

    invoke-interface {v0}, Lfnf;->getLocalParticipant()Lgnm;

    move-result-object v4

    .line 62
    iget-object v0, p0, Lfrh;->participantCollection:Lfnf;

    .line 63
    invoke-interface {v0}, Lfnf;->getResources()Ljava/util/Map;

    move-result-object v0

    iget-object v5, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v5}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnm;

    .line 64
    if-eqz v4, :cond_6

    if-eqz v0, :cond_6

    .line 65
    iget-object v5, v4, Lgnm;->blockedUser:[Lgnn;

    array-length v6, v5

    move v4, v1

    :goto_5
    if-ge v4, v6, :cond_6

    aget-object v7, v5, v4

    .line 66
    iget-object v7, v7, Lgnn;->userId:Ljava/lang/String;

    iget-object v8, v0, Lgnm;->userId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 67
    iget-object v4, p0, Lfrh;->participantInfo:Lfvz;

    .line 68
    iput-boolean v2, v4, Lfvz;->m:Z

    .line 71
    :cond_6
    iget-object v2, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v2}, Lfue;->getVideoSsrcs()Ljava/util/List;

    move-result-object v2

    .line 72
    if-eqz v2, :cond_7

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 73
    :cond_7
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    if-nez v1, :cond_b

    throw v9

    .line 57
    :cond_8
    iget-object v4, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v5, p0, Lfrh;->endpoint:Lfue;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Lfue;->isVideoMuted(Ljava/lang/String;)Z

    move-result v0

    .line 58
    iput-boolean v0, v4, Lfvz;->c:Z

    goto :goto_4

    .line 70
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 74
    :cond_a
    iget-object v4, p0, Lfrh;->participantInfo:Lfvz;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    if-nez v4, :cond_b

    throw v9

    .line 75
    :cond_b
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    .line 76
    if-nez v3, :cond_c

    .line 77
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 78
    :goto_6
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 79
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    iget-object v2, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v2}, Lfue;->getConnectionStatus()I

    move-result v2

    .line 80
    iput v2, v1, Lfvz;->h:I

    .line 81
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    .line 82
    iput-object v0, v1, Lfvz;->n:Lgnm;

    goto/16 :goto_3

    :cond_c
    move-object v1, v3

    goto :goto_6
.end method


# virtual methods
.method public final getEndpoint()Lfue;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lfrh;->endpoint:Lfue;

    return-object v0
.end method

.method public final getParticipantId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    .line 7
    iget-object v0, v0, Lfvz;->a:Ljava/lang/String;

    .line 8
    return-object v0
.end method

.method public final getParticipantInfo()Lfvz;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    return-object v0
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 28
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    .line 29
    iget v1, v1, Lfvz;->h:I

    .line 30
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLocalUser()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lfrh;->endpoint:Lfue;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfrh;->endpoint:Lfue;

    invoke-virtual {v0}, Lfue;->isSelfEndpoint()Z

    move-result v0

    goto :goto_0
.end method

.method public final markDirty()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lfrh;->refreshInternalState()V

    .line 85
    iget-object v0, p0, Lfrh;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getParticipantManager()Lfri;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfri;->maybeUpdateParticipant(Lfrh;)V

    .line 86
    return-void
.end method

.method final setEndpoint(Lfue;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 11
    iput-object p1, p0, Lfrh;->endpoint:Lfue;

    .line 12
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    .line 13
    iput-object v1, v0, Lfvz;->a:Ljava/lang/String;

    .line 14
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    invoke-virtual {p1}, Lfue;->getDisplayName()Ljava/lang/String;

    if-nez v0, :cond_0

    throw v4

    .line 15
    :cond_0
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    invoke-virtual {p1}, Lfue;->getAvatarUrl()Ljava/lang/String;

    if-nez v0, :cond_1

    throw v4

    .line 16
    :cond_1
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    invoke-virtual {p1}, Lfue;->getConnectionTime()J

    move-result-wide v2

    .line 17
    iput-wide v2, v0, Lfvz;->i:J

    .line 18
    instance-of v0, p1, Lfui;

    if-eqz v0, :cond_3

    .line 19
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    move-object v0, p1

    check-cast v0, Lfui;

    invoke-virtual {v0}, Lfui;->getObfuscatedGaiaId()Ljava/lang/String;

    move-result-object v0

    .line 20
    iput-object v0, v1, Lfvz;->g:Ljava/lang/String;

    .line 24
    :cond_2
    iget-object v0, p0, Lfrh;->participantInfo:Lfvz;

    instance-of v1, p1, Lfuk;

    .line 25
    iput-boolean v1, v0, Lfvz;->l:Z

    .line 26
    invoke-direct {p0}, Lfrh;->refreshInternalState()V

    .line 27
    return-void

    .line 22
    :cond_3
    instance-of v0, p1, Lfuk;

    if-eqz v0, :cond_2

    .line 23
    iget-object v1, p0, Lfrh;->participantInfo:Lfvz;

    move-object v0, p1

    check-cast v0, Lfuk;

    invoke-virtual {v0}, Lfuk;->getPstnJid()Ljava/lang/String;

    if-nez v1, :cond_2

    throw v4
.end method
