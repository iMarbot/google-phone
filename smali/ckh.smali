.class final synthetic Lckh;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lckb;

.field private b:Ljava/lang/CharSequence;

.field private c:Lcla;

.field private d:Landroid/transition/TransitionValues;


# direct methods
.method constructor <init>(Lckb;Ljava/lang/CharSequence;Lcla;Landroid/transition/TransitionValues;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lckh;->a:Lckb;

    iput-object p2, p0, Lckh;->b:Ljava/lang/CharSequence;

    iput-object p3, p0, Lckh;->c:Lcla;

    iput-object p4, p0, Lckh;->d:Landroid/transition/TransitionValues;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1
    iget-object v1, p0, Lckh;->a:Lckb;

    iget-object v0, p0, Lckh;->b:Ljava/lang/CharSequence;

    iget-object v2, p0, Lckh;->c:Lcla;

    iget-object v3, p0, Lckh;->d:Landroid/transition/TransitionValues;

    .line 2
    invoke-virtual {v1, v0}, Lckb;->b(Ljava/lang/CharSequence;)V

    .line 3
    iget-object v0, v1, Lckb;->l:Lcku;

    .line 4
    iget-object v0, v0, Lcku;->f:Landroid/widget/TextView;

    .line 5
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 6
    iget-object v0, v1, Lckb;->l:Lcku;

    .line 7
    iget-object v4, v0, Lcku;->c:Landroid/widget/ViewAnimator;

    .line 9
    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->endTransitions(Landroid/view/ViewGroup;)V

    .line 11
    invoke-virtual {v4}, Landroid/widget/ViewAnimator;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v5, Lckp;

    invoke-direct {v5, v1, v4, v2, v3}, Lckp;-><init>(Lckb;Landroid/widget/ViewAnimator;Lcla;Landroid/transition/TransitionValues;)V

    .line 12
    invoke-virtual {v0, v5}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 13
    return-void
.end method
