.class public final enum Lclt;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum A:Lclt;

.field public static final enum B:Lclt;

.field public static final enum C:Lclt;

.field public static final enum D:Lclt;

.field public static final enum E:Lclt;

.field public static final enum F:Lclt;

.field public static final enum G:Lclt;

.field public static final enum H:Lclt;

.field public static final enum I:Lclt;

.field public static final enum J:Lclt;

.field public static final enum K:Lclt;

.field public static final enum L:Lclt;

.field public static final enum M:Lclt;

.field public static final enum N:Lclt;

.field public static final enum O:Lclt;

.field public static final enum P:Lclt;

.field private static enum R:Lclt;

.field private static synthetic S:[Lclt;

.field public static final enum a:Lclt;

.field public static final enum b:Lclt;

.field public static final enum c:Lclt;

.field public static final enum d:Lclt;

.field public static final enum e:Lclt;

.field public static final enum f:Lclt;

.field public static final enum g:Lclt;

.field public static final enum h:Lclt;

.field public static final enum i:Lclt;

.field public static final enum j:Lclt;

.field public static final enum k:Lclt;

.field public static final enum l:Lclt;

.field public static final enum m:Lclt;

.field public static final enum n:Lclt;

.field public static final enum o:Lclt;

.field public static final enum p:Lclt;

.field public static final enum q:Lclt;

.field public static final enum r:Lclt;

.field public static final enum s:Lclt;

.field public static final enum t:Lclt;

.field public static final enum u:Lclt;

.field public static final enum v:Lclt;

.field public static final enum w:Lclt;

.field public static final enum x:Lclt;

.field public static final enum y:Lclt;

.field public static final enum z:Lclt;


# instance fields
.field public final Q:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 11
    new-instance v0, Lclt;

    const-string v1, "CONFIG_REQUEST_STATUS_SUCCESS"

    invoke-direct {v0, v1, v5, v4, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->a:Lclt;

    .line 12
    new-instance v0, Lclt;

    const-string v1, "CONFIG_PIN_SET"

    invoke-direct {v0, v1, v4, v4, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->b:Lclt;

    .line 13
    new-instance v0, Lclt;

    const-string v1, "CONFIG_DEFAULT_PIN_REPLACED"

    invoke-direct {v0, v1, v3, v4, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->c:Lclt;

    .line 14
    new-instance v0, Lclt;

    const-string v1, "CONFIG_ACTIVATING"

    invoke-direct {v0, v1, v6, v4, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->d:Lclt;

    .line 15
    new-instance v0, Lclt;

    const-string v1, "CONFIG_ACTIVATING_SUBSEQUENT"

    invoke-direct {v0, v1, v7, v4, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->e:Lclt;

    .line 16
    new-instance v0, Lclt;

    const-string v1, "CONFIG_STATUS_SMS_TIME_OUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->f:Lclt;

    .line 17
    new-instance v0, Lclt;

    const-string v1, "CONFIG_SERVICE_NOT_AVAILABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->g:Lclt;

    .line 18
    new-instance v0, Lclt;

    const-string v1, "DATA_IMAP_OPERATION_STARTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->h:Lclt;

    .line 19
    new-instance v0, Lclt;

    const-string v1, "DATA_IMAP_OPERATION_COMPLETED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->i:Lclt;

    .line 20
    new-instance v0, Lclt;

    const-string v1, "DATA_INVALID_PORT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->j:Lclt;

    .line 21
    new-instance v0, Lclt;

    const-string v1, "DATA_NO_CONNECTION_CELLULAR_REQUIRED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->k:Lclt;

    .line 22
    new-instance v0, Lclt;

    const-string v1, "DATA_NO_CONNECTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->l:Lclt;

    .line 23
    new-instance v0, Lclt;

    const-string v1, "DATA_CANNOT_RESOLVE_HOST_ON_NETWORK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->m:Lclt;

    .line 24
    new-instance v0, Lclt;

    const-string v1, "DATA_ALL_SOCKET_CONNECTION_FAILED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->n:Lclt;

    .line 25
    new-instance v0, Lclt;

    const-string v1, "DATA_CANNOT_ESTABLISH_SSL_SESSION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->o:Lclt;

    .line 26
    new-instance v0, Lclt;

    const-string v1, "DATA_SSL_INVALID_HOST_NAME"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->p:Lclt;

    .line 27
    new-instance v0, Lclt;

    const-string v1, "DATA_BAD_IMAP_CREDENTIAL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->q:Lclt;

    .line 28
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_UNKNOWN_USER"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->r:Lclt;

    .line 29
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_UNKNOWN_DEVICE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->s:Lclt;

    .line 30
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_INVALID_PASSWORD"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->t:Lclt;

    .line 31
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_MAILBOX_NOT_INITIALIZED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->u:Lclt;

    .line 32
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_SERVICE_NOT_PROVISIONED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->v:Lclt;

    .line 33
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_SERVICE_NOT_ACTIVATED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->w:Lclt;

    .line 34
    new-instance v0, Lclt;

    const-string v1, "DATA_AUTH_USER_IS_BLOCKED"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->x:Lclt;

    .line 35
    new-instance v0, Lclt;

    const-string v1, "DATA_REJECTED_SERVER_RESPONSE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->y:Lclt;

    .line 36
    new-instance v0, Lclt;

    const-string v1, "DATA_INVALID_INITIAL_SERVER_RESPONSE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->z:Lclt;

    .line 37
    new-instance v0, Lclt;

    const-string v1, "DATA_IOE_ON_OPEN"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->A:Lclt;

    .line 38
    new-instance v0, Lclt;

    const-string v1, "DATA_MAILBOX_OPEN_FAILED"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->B:Lclt;

    .line 39
    new-instance v0, Lclt;

    const-string v1, "DATA_GENERIC_IMAP_IOE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->C:Lclt;

    .line 40
    new-instance v0, Lclt;

    const-string v1, "DATA_SSL_EXCEPTION"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lclt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lclt;->D:Lclt;

    .line 41
    new-instance v0, Lclt;

    const-string v1, "NOTIFICATION_IN_SERVICE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2, v6, v4}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->E:Lclt;

    .line 42
    new-instance v0, Lclt;

    const-string v1, "NOTIFICATION_SERVICE_LOST"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2, v6, v5}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->F:Lclt;

    .line 43
    new-instance v0, Lclt;

    const-string v1, "OTHER_SOURCE_REMOVED"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2, v7, v5}, Lclt;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lclt;->G:Lclt;

    .line 44
    new-instance v0, Lclt;

    const-string v1, "VVM3_NEW_USER_SETUP_FAILED"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->H:Lclt;

    .line 45
    new-instance v0, Lclt;

    const-string v1, "VVM3_VMG_DNS_FAILURE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->I:Lclt;

    .line 46
    new-instance v0, Lclt;

    const-string v1, "VVM3_SPG_DNS_FAILURE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->J:Lclt;

    .line 47
    new-instance v0, Lclt;

    const-string v1, "VVM3_VMG_CONNECTION_FAILED"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->K:Lclt;

    .line 48
    new-instance v0, Lclt;

    const-string v1, "VVM3_SPG_CONNECTION_FAILED"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->L:Lclt;

    .line 49
    new-instance v0, Lclt;

    const-string v1, "VVM3_VMG_TIMEOUT"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->M:Lclt;

    .line 50
    new-instance v0, Lclt;

    const-string v1, "VVM3_STATUS_SMS_TIMEOUT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->R:Lclt;

    .line 51
    new-instance v0, Lclt;

    const-string v1, "VVM3_SUBSCRIBER_PROVISIONED"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->N:Lclt;

    .line 52
    new-instance v0, Lclt;

    const-string v1, "VVM3_SUBSCRIBER_BLOCKED"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->O:Lclt;

    .line 53
    new-instance v0, Lclt;

    const-string v1, "VVM3_SUBSCRIBER_UNKNOWN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lclt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lclt;->P:Lclt;

    .line 54
    const/16 v0, 0x2b

    new-array v0, v0, [Lclt;

    sget-object v1, Lclt;->a:Lclt;

    aput-object v1, v0, v5

    sget-object v1, Lclt;->b:Lclt;

    aput-object v1, v0, v4

    sget-object v1, Lclt;->c:Lclt;

    aput-object v1, v0, v3

    sget-object v1, Lclt;->d:Lclt;

    aput-object v1, v0, v6

    sget-object v1, Lclt;->e:Lclt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lclt;->f:Lclt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lclt;->g:Lclt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lclt;->h:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lclt;->i:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lclt;->j:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lclt;->k:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lclt;->l:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lclt;->m:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lclt;->n:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lclt;->o:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lclt;->p:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lclt;->q:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lclt;->r:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lclt;->s:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lclt;->t:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lclt;->u:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lclt;->v:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lclt;->w:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lclt;->x:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lclt;->y:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lclt;->z:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lclt;->A:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lclt;->B:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lclt;->C:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lclt;->D:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lclt;->E:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lclt;->F:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lclt;->G:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lclt;->H:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lclt;->I:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lclt;->J:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lclt;->K:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lclt;->L:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lclt;->M:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lclt;->R:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lclt;->N:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lclt;->O:Lclt;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lclt;->P:Lclt;

    aput-object v2, v0, v1

    sput-object v0, Lclt;->S:[Lclt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    const/4 v0, 0x4

    iput v0, p0, Lclt;->Q:I

    .line 10
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 6
    iput p3, p0, Lclt;->Q:I

    .line 7
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lclt;->Q:I

    .line 4
    return-void
.end method

.method public static values()[Lclt;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lclt;->S:[Lclt;

    invoke-virtual {v0}, [Lclt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lclt;

    return-object v0
.end method
