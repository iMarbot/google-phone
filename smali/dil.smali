.class public final Ldil;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldil$a;
    }
.end annotation


# static fields
.field public static final c:Ldil;

.field private static volatile d:Lhdm;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ldil;

    invoke-direct {v0}, Ldil;-><init>()V

    .line 72
    sput-object v0, Ldil;->c:Ldil;

    invoke-virtual {v0}, Ldil;->makeImmutable()V

    .line 73
    const-class v0, Ldil;

    sget-object v1, Ldil;->c:Ldil;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 74
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 67
    sget-object v2, Ldil$a;->g:Lhby;

    .line 68
    aput-object v2, v0, v1

    .line 69
    const-string v1, "\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u000c\u0000"

    .line 70
    sget-object v2, Ldil;->c:Ldil;

    invoke-static {v2, v1, v0}, Ldil;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 23
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 65
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 24
    :pswitch_0
    new-instance v0, Ldil;

    invoke-direct {v0}, Ldil;-><init>()V

    .line 64
    :goto_0
    :pswitch_1
    return-object v0

    .line 25
    :pswitch_2
    sget-object v0, Ldil;->c:Ldil;

    goto :goto_0

    .line 27
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[C)V

    move-object v0, v1

    goto :goto_0

    .line 28
    :pswitch_4
    check-cast p2, Lhaq;

    .line 29
    check-cast p3, Lhbg;

    .line 30
    if-nez p3, :cond_0

    .line 31
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32
    :cond_0
    :try_start_0
    sget-boolean v0, Ldil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0, p2, p3}, Ldil;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 34
    sget-object v0, Ldil;->c:Ldil;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 36
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 37
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 38
    sparse-switch v2, :sswitch_data_0

    .line 41
    invoke-virtual {p0, v2, p2}, Ldil;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 42
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_1

    .line 43
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 44
    invoke-static {v2}, Ldil$a;->a(I)Ldil$a;

    move-result-object v3

    .line 45
    if-nez v3, :cond_3

    .line 46
    const/4 v3, 0x1

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 51
    :catch_0
    move-exception v0

    .line 52
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    :catchall_0
    move-exception v0

    throw v0

    .line 47
    :cond_3
    :try_start_2
    iget v3, p0, Ldil;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Ldil;->a:I

    .line 48
    iput v2, p0, Ldil;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 53
    :catch_1
    move-exception v0

    .line 54
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 55
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :cond_4
    :pswitch_5
    sget-object v0, Ldil;->c:Ldil;

    goto :goto_0

    .line 58
    :pswitch_6
    sget-object v0, Ldil;->d:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Ldil;

    monitor-enter v1

    .line 59
    :try_start_4
    sget-object v0, Ldil;->d:Lhdm;

    if-nez v0, :cond_5

    .line 60
    new-instance v0, Lhaa;

    sget-object v2, Ldil;->c:Ldil;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Ldil;->d:Lhdm;

    .line 61
    :cond_5
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 62
    :cond_6
    sget-object v0, Ldil;->d:Lhdm;

    goto :goto_0

    .line 61
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 63
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 11
    iget v0, p0, Ldil;->memoizedSerializedSize:I

    .line 12
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 22
    :goto_0
    return v0

    .line 13
    :cond_0
    sget-boolean v0, Ldil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 14
    invoke-virtual {p0}, Ldil;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Ldil;->memoizedSerializedSize:I

    .line 15
    iget v0, p0, Ldil;->memoizedSerializedSize:I

    goto :goto_0

    .line 16
    :cond_1
    const/4 v0, 0x0

    .line 17
    iget v1, p0, Ldil;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 18
    iget v0, p0, Ldil;->b:I

    .line 19
    invoke-static {v2, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20
    :cond_2
    iget-object v1, p0, Ldil;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 21
    iput v0, p0, Ldil;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Ldil;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Ldil;->writeToInternal(Lhaw;)V

    .line 10
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Ldil;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 7
    iget v0, p0, Ldil;->b:I

    .line 8
    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 9
    :cond_1
    iget-object v0, p0, Ldil;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
