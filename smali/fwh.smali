.class public Lfwh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final source:Lfsm;


# direct methods
.method public constructor <init>(Lfnp;Landroid/graphics/SurfaceTexture;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "Trying to create a renderer for a participant that doesn\'t exist!"

    .line 3
    invoke-virtual {p1}, Lfnp;->getVideoSourceManager()Lfso;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Lfso;->getSource(Ljava/lang/String;Ljava/lang/String;)Lfsm;

    move-result-object v1

    .line 4
    invoke-static {v0, v1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsm;

    iput-object v0, p0, Lfwh;->source:Lfsm;

    .line 5
    iget-object v0, p0, Lfwh;->source:Lfsm;

    invoke-virtual {v0, p2}, Lfsm;->bindToSurface(Landroid/graphics/SurfaceTexture;)V

    .line 6
    return-void
.end method
