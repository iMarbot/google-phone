.class final Lbrn;
.super Landroid/telecom/Connection$VideoProvider;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lbpz;

.field private c:Ljava/lang/String;

.field private d:Lbqt;

.field private e:Lbqw;


# direct methods
.method constructor <init>(Landroid/content/Context;Lbpz;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/Connection$VideoProvider;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbrn;->a:Landroid/content/Context;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpz;

    iput-object v0, p0, Lbrn;->b:Lbpz;

    .line 4
    return-void
.end method


# virtual methods
.method public final onRequestCameraCapabilities()V
    .locals 2

    .prologue
    .line 55
    const-string v0, "SimulatorVideoProvider.onRequestCameraCapabilities"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lbrn;->a:Landroid/content/Context;

    iget-object v1, p0, Lbrn;->c:Ljava/lang/String;

    .line 57
    invoke-static {v0, v1}, Lbqt;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/VideoProfile$CameraCapabilities;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Lbrn;->changeCameraCapabilities(Landroid/telecom/VideoProfile$CameraCapabilities;)V

    .line 59
    return-void
.end method

.method public final onRequestConnectionDataUsage()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "SimulatorVideoProvider.onRequestConnectionDataUsage"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 61
    const-wide/16 v0, 0x2800

    invoke-virtual {p0, v0, v1}, Lbrn;->setCallDataUsage(J)V

    .line 62
    return-void
.end method

.method public final onSendSessionModifyRequest(Landroid/telecom/VideoProfile;Landroid/telecom/VideoProfile;)V
    .locals 5

    .prologue
    .line 47
    const-string v0, "SimulatorVideoProvider.onSendSessionModifyRequest"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lbrn;->b:Lbpz;

    new-instance v1, Lbpt;

    const/16 v2, 0x8

    .line 49
    invoke-virtual {p1}, Landroid/telecom/VideoProfile;->getVideoState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 50
    invoke-virtual {p2}, Landroid/telecom/VideoProfile;->getVideoState()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbpt;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Lbpz;->a(Lbpt;)V

    .line 52
    return-void
.end method

.method public final onSendSessionModifyResponse(Landroid/telecom/VideoProfile;)V
    .locals 1

    .prologue
    .line 53
    const-string v0, "SimulatorVideoProvider.onSendSessionModifyResponse"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public final onSetCamera(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5
    const-string v1, "SimulatorVideoProvider.onSetCamera"

    const-string v2, "previewCameraId: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iput-object p1, p0, Lbrn;->c:Ljava/lang/String;

    .line 7
    iget-object v0, p0, Lbrn;->d:Lbqt;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lbrn;->d:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 9
    iput-object v4, p0, Lbrn;->d:Lbqt;

    .line 10
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lbrn;->e:Lbqw;

    if-eqz v0, :cond_1

    .line 11
    iget-object v0, p0, Lbrn;->e:Lbqw;

    invoke-virtual {v0}, Lbqw;->a()V

    .line 12
    iput-object v4, p0, Lbrn;->e:Lbqw;

    .line 13
    :cond_1
    return-void

    .line 5
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSetDeviceOrientation(I)V
    .locals 3

    .prologue
    .line 43
    const-string v0, "SimulatorVideoProvider.onSetDeviceOrientation"

    const/16 v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "rotation: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method public final onSetDisplaySurface(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 32
    const-string v0, "SimulatorVideoProvider.onSetDisplaySurface"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lbrn;->e:Lbqw;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lbrn;->e:Lbqw;

    invoke-virtual {v0}, Lbqw;->a()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lbrn;->e:Lbqw;

    .line 36
    :cond_0
    if-eqz p1, :cond_1

    .line 37
    new-instance v0, Lbqw;

    invoke-direct {v0, p1}, Lbqw;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lbrn;->e:Lbqw;

    .line 38
    iget-object v1, p0, Lbrn;->e:Lbqw;

    .line 39
    const-string v0, "SimulatorRemoteVideo.startVideo"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 40
    iget-boolean v0, v1, Lbqw;->b:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 41
    iget-object v0, v1, Lbqw;->a:Lbqx;

    invoke-virtual {v0}, Lbqx;->start()V

    .line 42
    :cond_1
    return-void

    .line 40
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSetPauseImage(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 63
    const-string v0, "SimulatorVideoProvider.onSetPauseImage"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public final onSetPreviewSurface(Landroid/view/Surface;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14
    const-string v0, "SimulatorVideoProvider.onSetPreviewSurface"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lbrn;->d:Lbqt;

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lbrn;->d:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 17
    iput-object v1, p0, Lbrn;->d:Lbqt;

    .line 18
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lbrn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 19
    new-instance v0, Lbqt;

    iget-object v1, p0, Lbrn;->a:Landroid/content/Context;

    iget-object v2, p0, Lbrn;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lbqt;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/Surface;)V

    iput-object v0, p0, Lbrn;->d:Lbqt;

    .line 20
    iget-object v1, p0, Lbrn;->d:Lbqt;

    .line 21
    const-string v0, "SimulatorPreviewCamera.startCamera"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    iget-boolean v0, v1, Lbqt;->e:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 23
    :try_start_0
    iget-object v0, v1, Lbqt;->a:Landroid/content/Context;

    const-class v2, Landroid/hardware/camera2/CameraManager;

    .line 24
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iget-object v2, v1, Lbqt;->b:Ljava/lang/String;

    new-instance v3, Lbqu;

    .line 25
    invoke-direct {v3, v1}, Lbqu;-><init>(Lbqt;)V

    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-virtual {v0, v2, v3, v1}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 31
    :cond_1
    return-void

    .line 22
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "camera error: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 29
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final onSetZoom(F)V
    .locals 3

    .prologue
    .line 45
    const-string v0, "SimulatorVideoProvider.onSetZoom"

    const/16 v1, 0x15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "zoom: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    return-void
.end method
