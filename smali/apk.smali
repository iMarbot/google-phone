.class public abstract Lapk;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lapk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IZ)Lapk;
    .locals 6

    .prologue
    .line 12
    new-instance v0, Laps;

    move-object v1, p0

    move v2, p4

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Laps;-><init>(Landroid/net/Uri;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method public static a(Landroid/telecom/PhoneAccountHandle;)Lapk;
    .locals 2

    .prologue
    .line 9
    new-instance v0, Lapp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lapp;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    return-object v0
.end method

.method public static a(Lbao;Lbhj;ZZ)Lapk;
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lapr;

    invoke-direct {v0, p0, p1, p2, p3}, Lapr;-><init>(Lbao;Lbhj;ZZ)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lapk;
    .locals 2

    .prologue
    .line 2
    .line 3
    new-instance v0, Lapl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lapl;-><init>(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V

    .line 4
    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Lapk;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lapn;

    invoke-direct {v0, p0, p1}, Lapn;-><init>(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lapk;
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lapm;

    invoke-direct {v0, p0}, Lapm;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lapk;
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lapk;->a(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Lapk;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lapk;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lapo;

    invoke-direct {v0, p0}, Lapo;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lapk;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lapq;

    invoke-direct {v0, p0}, Lapq;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/content/Intent;
.end method
