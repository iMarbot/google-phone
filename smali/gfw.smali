.class public final Lgfw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Lgqj;

.field private e:Ljava/io/InputStream;

.field private f:Ljava/lang/String;

.field private g:Lgft;

.field private h:Lggd;

.field private i:I

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lgqj;Lggd;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lgfw;->d:Lgqj;

    .line 3
    invoke-virtual {p1}, Lgqj;->b()I

    move-result v0

    iput v0, p0, Lgfw;->i:I

    .line 4
    invoke-virtual {p1}, Lgqj;->c()Z

    move-result v0

    iput-boolean v0, p0, Lgfw;->j:Z

    .line 5
    iput-object p2, p0, Lgfw;->h:Lggd;

    .line 6
    invoke-virtual {p2}, Lggd;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgfw;->f:Ljava/lang/String;

    .line 7
    invoke-virtual {p2}, Lggd;->e()I

    move-result v0

    .line 8
    if-gez v0, :cond_0

    move v0, v1

    :cond_0
    iput v0, p0, Lgfw;->b:I

    .line 9
    invoke-virtual {p2}, Lggd;->f()Ljava/lang/String;

    move-result-object v4

    .line 10
    iput-object v4, p0, Lgfw;->c:Ljava/lang/String;

    .line 11
    sget-object v5, Lgga;->a:Ljava/util/logging/Logger;

    .line 12
    iget-boolean v0, p0, Lgfw;->j:Z

    if-eqz v0, :cond_4

    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v5, v0}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    move v3, v1

    .line 14
    :goto_0
    if-eqz v3, :cond_8

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    const-string v1, "-------------- RESPONSE --------------"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v6, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    invoke-virtual {p2}, Lggd;->d()Ljava/lang/String;

    move-result-object v1

    .line 18
    if-eqz v1, :cond_5

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    :cond_1
    :goto_1
    sget-object v1, Lghy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v1, v0

    .line 24
    :goto_2
    invoke-virtual {p1}, Lgqj;->e()Lgfr;

    move-result-object v4

    if-eqz v3, :cond_6

    move-object v0, v1

    :goto_3
    invoke-virtual {v4, p2, v0}, Lgfr;->a(Lggd;Ljava/lang/StringBuilder;)V

    .line 25
    invoke-virtual {p2}, Lggd;->c()Ljava/lang/String;

    move-result-object v0

    .line 26
    if-nez v0, :cond_2

    .line 27
    invoke-virtual {p1}, Lgqj;->e()Lgfr;

    .line 28
    invoke-static {v2}, Lgfr;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 30
    :cond_2
    iput-object v0, p0, Lgfw;->a:Ljava/lang/String;

    .line 31
    if-nez v0, :cond_7

    :goto_4
    iput-object v2, p0, Lgfw;->g:Lgft;

    .line 32
    if-eqz v3, :cond_3

    .line 33
    sget-object v0, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    const-string v2, "com.google.api.client.http.HttpResponse"

    const-string v3, "<init>"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v2, v3, v1}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    :cond_3
    return-void

    :cond_4
    move v3, v1

    .line 12
    goto :goto_0

    .line 20
    :cond_5
    iget v1, p0, Lgfw;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    if-eqz v4, :cond_1

    .line 22
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    move-object v0, v2

    .line 24
    goto :goto_3

    .line 31
    :cond_7
    new-instance v2, Lgft;

    invoke-direct {v2, v0}, Lgft;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    move-object v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 35
    iget-boolean v0, p0, Lgfw;->k:Z

    if-nez v0, :cond_3

    .line 36
    iget-object v0, p0, Lgfw;->h:Lggd;

    invoke-virtual {v0}, Lggd;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_2

    .line 38
    :try_start_0
    iget-object v1, p0, Lgfw;->f:Ljava/lang/String;

    .line 39
    if-eqz v1, :cond_0

    const-string v2, "gzip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 41
    :cond_0
    :try_start_1
    sget-object v2, Lgga;->a:Ljava/util/logging/Logger;

    .line 42
    iget-boolean v1, p0, Lgfw;->j:Z

    if-eqz v1, :cond_1

    sget-object v1, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    new-instance v1, Lghq;

    sget-object v3, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    iget v4, p0, Lgfw;->i:I

    invoke-direct {v1, v0, v2, v3, v4}, Lghq;-><init>(Ljava/io/InputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    move-object v0, v1

    .line 44
    :cond_1
    iput-object v0, p0, Lgfw;->e:Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 50
    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfw;->k:Z

    .line 51
    :cond_3
    iget-object v0, p0, Lgfw;->e:Ljava/io/InputStream;

    return-object v0

    .line 47
    :catch_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 49
    :goto_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    .line 48
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lgfw;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 55
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lgfw;->b()V

    .line 57
    iget-object v0, p0, Lgfw;->h:Lggd;

    invoke-virtual {v0}, Lggd;->h()V

    .line 58
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lgfw;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 60
    if-nez v0, :cond_0

    .line 61
    const-string v0, ""

    .line 64
    :goto_0
    return-object v0

    .line 62
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 63
    invoke-static {v0, v1}, Lhcw;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 64
    invoke-virtual {p0}, Lgfw;->e()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lgfw;->g:Lgft;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfw;->g:Lgft;

    invoke-virtual {v0}, Lgft;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    sget-object v0, Lgha;->b:Ljava/nio/charset/Charset;

    .line 67
    :goto_0
    return-object v0

    .line 66
    :cond_1
    iget-object v0, p0, Lgfw;->g:Lgft;

    invoke-virtual {v0}, Lgft;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method
