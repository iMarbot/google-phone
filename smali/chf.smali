.class public final Lchf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;
.implements Lcdb;
.implements Lcdi;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcdc;

.field private c:Lcct;

.field private d:Lcgt;

.field private e:Ljava/util/concurrent/CountDownLatch;

.field private f:Ljava/util/concurrent/CountDownLatch;

.field private g:Landroid/telecom/PhoneAccountHandle;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcdc;Lcct;Landroid/telecom/PhoneAccountHandle;Lcgt;)V
    .locals 7

    .prologue
    .line 1
    const/16 v6, 0x1388

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lchf;-><init>(Landroid/content/Context;Lcdc;Lcct;Landroid/telecom/PhoneAccountHandle;Lcgt;I)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcdc;Lcct;Landroid/telecom/PhoneAccountHandle;Lcgt;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lchf;->e:Ljava/util/concurrent/CountDownLatch;

    .line 5
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lchf;->f:Ljava/util/concurrent/CountDownLatch;

    .line 6
    invoke-static {}, Lbdf;->b()V

    .line 7
    iput-object p1, p0, Lchf;->a:Landroid/content/Context;

    .line 8
    iput-object p2, p0, Lchf;->b:Lcdc;

    .line 9
    iput-object p3, p0, Lchf;->c:Lcct;

    .line 10
    iput-object p4, p0, Lchf;->g:Landroid/telecom/PhoneAccountHandle;

    .line 11
    iput-object p5, p0, Lchf;->d:Lcgt;

    .line 12
    const/16 v0, 0x1388

    iput v0, p0, Lchf;->i:I

    .line 14
    iget-object v0, p2, Lcdc;->c:Landroid/telecom/Call;

    invoke-static {v0}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v0

    .line 15
    iput-object v0, p0, Lchf;->h:Ljava/lang/String;

    .line 16
    invoke-virtual {p2, p0}, Lcdc;->a(Lcdi;)V

    .line 17
    invoke-virtual {p2}, Lcdc;->B()V

    .line 18
    return-void
.end method

.method private l()Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 19
    :try_start_0
    iget-object v0, p0, Lchf;->a:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const-string v0, "SwapSimWorker.doInBackground"

    const-string v1, "missing phone permission"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21
    new-instance v0, Lchg;

    invoke-direct {v0, p0}, Lchg;-><init>(Lchf;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 41
    :goto_0
    return-object v5

    .line 23
    :cond_0
    :try_start_1
    iget-object v0, p0, Lchf;->e:Ljava/util/concurrent/CountDownLatch;

    iget v1, p0, Lchf;->i:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 24
    const-string v0, "SwapSimWorker.doInBackground"

    const-string v1, "timeout waiting for call to disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 25
    new-instance v0, Lchh;

    invoke-direct {v0, p0}, Lchh;-><init>(Lchf;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 27
    :cond_1
    :try_start_2
    const-string v0, "SwapSimWorker.doInBackground"

    const-string v1, "call disconnected, redialing"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lchf;->a:Landroid/content/Context;

    const-class v1, Landroid/telecom/TelecomManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 29
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30
    const-string v2, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    iget-object v3, p0, Lchf;->g:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 31
    iget-object v2, p0, Lchf;->c:Lcct;

    invoke-virtual {v2, p0}, Lcct;->a(Lcdb;)V

    .line 32
    const-string v2, "tel"

    iget-object v3, p0, Lchf;->h:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/telecom/TelecomManager;->placeCall(Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lchf;->f:Ljava/util/concurrent/CountDownLatch;

    iget v1, p0, Lchf;->i:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 34
    const-string v0, "SwapSimWorker.doInBackground"

    const-string v1, "timeout waiting for call to dial"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 35
    :cond_2
    new-instance v0, Lchi;

    invoke-direct {v0, p0}, Lchi;-><init>(Lchf;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    :try_start_3
    const-string v1, "SwapSimWorker.doInBackground"

    const-string v2, "interrupted"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 40
    new-instance v0, Lchj;

    invoke-direct {v0, p0}, Lchj;-><init>(Lchf;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 42
    :catchall_0
    move-exception v0

    new-instance v1, Lchk;

    invoke-direct {v1, p0}, Lchk;-><init>(Lchf;)V

    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lchf;->l()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lchf;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 44
    return-void
.end method

.method public final a(Lcct;)V
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p1}, Lcct;->c()Lcdc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lchf;->f:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 47
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public final b(Lcdc;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final c(Lcdc;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final d(Lcdc;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public final e(Lcdc;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public final f(Lcdc;)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public final h(Lcdc;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method final synthetic k()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lchf;->b:Lcdc;

    invoke-virtual {v0, p0}, Lcdc;->b(Lcdi;)V

    .line 66
    iget-object v0, p0, Lchf;->c:Lcct;

    invoke-virtual {v0, p0}, Lcct;->b(Lcdb;)V

    .line 67
    iget-object v0, p0, Lchf;->d:Lcgt;

    invoke-interface {v0}, Lcgt;->a()V

    .line 68
    return-void
.end method
