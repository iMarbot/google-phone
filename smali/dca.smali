.class public final Ldca;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldar;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldca;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;IILcuh;)Ldas;
    .locals 5

    .prologue
    .line 7
    check-cast p1, Landroid/net/Uri;

    .line 8
    invoke-static {p2, p3}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    sget-object v0, Lddq;->a:Lcue;

    invoke-virtual {p4, v0}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 10
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 11
    :goto_0
    if-eqz v0, :cond_1

    .line 12
    new-instance v0, Ldas;

    new-instance v1, Ldhm;

    invoke-direct {v1, p1}, Ldhm;-><init>(Ljava/lang/Object;)V

    iget-object v2, p0, Ldca;->a:Landroid/content/Context;

    .line 13
    new-instance v3, Lcvg;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4}, Lcvg;-><init>(Landroid/content/ContentResolver;)V

    invoke-static {v2, p1, v3}, Lcve;->a(Landroid/content/Context;Landroid/net/Uri;Lcvh;)Lcve;

    move-result-object v2

    .line 14
    invoke-direct {v0, v1, v2}, Ldas;-><init>(Lcud;Lcum;)V

    .line 16
    :goto_1
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 15
    :cond_1
    const/4 v0, 0x0

    .line 16
    goto :goto_1
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4
    check-cast p1, Landroid/net/Uri;

    .line 5
    invoke-static {p1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->c(Landroid/net/Uri;)Z

    move-result v0

    .line 6
    return v0
.end method
