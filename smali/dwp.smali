.class public final Ldwp;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Z

.field private c:J

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ldwy;

    invoke-direct {v0}, Ldwy;-><init>()V

    sput-object v0, Ldwp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IZJZ)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Ldwp;->a:I

    iput-boolean p2, p0, Ldwp;->b:Z

    iput-wide p3, p0, Ldwp;->c:J

    iput-boolean p5, p0, Ldwp;->d:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Ldwp;->a:I

    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    .line 2
    iget-boolean v2, p0, Ldwp;->b:Z

    .line 3
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    .line 4
    iget-wide v2, p0, Ldwp;->c:J

    .line 5
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x4

    .line 6
    iget-boolean v2, p0, Ldwp;->d:Z

    .line 7
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
