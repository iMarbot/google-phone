.class public final enum Lhlx;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhlx;

.field public static final enum b:Lhlx;

.field public static final enum c:Lhlx;

.field public static final enum d:Lhlx;

.field public static final enum e:Lhlx;

.field public static final enum f:Lhlx;

.field public static final enum g:Lhlx;

.field public static final enum h:Lhlx;

.field public static final enum i:Lhlx;

.field public static final enum j:Lhlx;

.field public static final enum k:Lhlx;

.field public static final enum l:Lhlx;

.field public static final enum m:Lhlx;

.field public static final enum n:Lhlx;

.field public static final enum o:Lhlx;

.field public static final enum p:Lhlx;

.field public static final enum q:Lhlx;

.field private static synthetic t:[Lhlx;


# instance fields
.field public final r:I

.field public final s:[B


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 8
    new-instance v0, Lhlx;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4, v4}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->a:Lhlx;

    .line 9
    new-instance v0, Lhlx;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v5, v5}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->b:Lhlx;

    .line 10
    new-instance v0, Lhlx;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v6}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->c:Lhlx;

    .line 11
    new-instance v0, Lhlx;

    const-string v1, "INVALID_ARGUMENT"

    invoke-direct {v0, v1, v7, v7}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->d:Lhlx;

    .line 12
    new-instance v0, Lhlx;

    const-string v1, "DEADLINE_EXCEEDED"

    invoke-direct {v0, v1, v8, v8}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->e:Lhlx;

    .line 13
    new-instance v0, Lhlx;

    const-string v1, "NOT_FOUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->f:Lhlx;

    .line 14
    new-instance v0, Lhlx;

    const-string v1, "ALREADY_EXISTS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->g:Lhlx;

    .line 15
    new-instance v0, Lhlx;

    const-string v1, "PERMISSION_DENIED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->h:Lhlx;

    .line 16
    new-instance v0, Lhlx;

    const-string v1, "RESOURCE_EXHAUSTED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->i:Lhlx;

    .line 17
    new-instance v0, Lhlx;

    const-string v1, "FAILED_PRECONDITION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->j:Lhlx;

    .line 18
    new-instance v0, Lhlx;

    const-string v1, "ABORTED"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->k:Lhlx;

    .line 19
    new-instance v0, Lhlx;

    const-string v1, "OUT_OF_RANGE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->l:Lhlx;

    .line 20
    new-instance v0, Lhlx;

    const-string v1, "UNIMPLEMENTED"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->m:Lhlx;

    .line 21
    new-instance v0, Lhlx;

    const-string v1, "INTERNAL"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->n:Lhlx;

    .line 22
    new-instance v0, Lhlx;

    const-string v1, "UNAVAILABLE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->o:Lhlx;

    .line 23
    new-instance v0, Lhlx;

    const-string v1, "DATA_LOSS"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->p:Lhlx;

    .line 24
    new-instance v0, Lhlx;

    const-string v1, "UNAUTHENTICATED"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lhlx;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhlx;->q:Lhlx;

    .line 25
    const/16 v0, 0x11

    new-array v0, v0, [Lhlx;

    sget-object v1, Lhlx;->a:Lhlx;

    aput-object v1, v0, v4

    sget-object v1, Lhlx;->b:Lhlx;

    aput-object v1, v0, v5

    sget-object v1, Lhlx;->c:Lhlx;

    aput-object v1, v0, v6

    sget-object v1, Lhlx;->d:Lhlx;

    aput-object v1, v0, v7

    sget-object v1, Lhlx;->e:Lhlx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lhlx;->f:Lhlx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhlx;->g:Lhlx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhlx;->h:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhlx;->i:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhlx;->j:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhlx;->k:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhlx;->l:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhlx;->m:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhlx;->n:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhlx;->o:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhlx;->p:Lhlx;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhlx;->q:Lhlx;

    aput-object v2, v0, v1

    sput-object v0, Lhlx;->t:[Lhlx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lhlx;->r:I

    .line 4
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lgtg;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    iput-object v0, p0, Lhlx;->s:[B

    .line 5
    return-void
.end method

.method public static values()[Lhlx;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhlx;->t:[Lhlx;

    invoke-virtual {v0}, [Lhlx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhlx;

    return-object v0
.end method


# virtual methods
.method public final a()Lhlw;
    .locals 2

    .prologue
    .line 6
    sget-object v0, Lhlw;->a:Ljava/util/List;

    .line 7
    iget v1, p0, Lhlx;->r:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    return-object v0
.end method
