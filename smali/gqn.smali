.class public final Lgqn;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqn;


# instance fields
.field public invitation:Lgqs;

.field public requestHeader:Lgls;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqn;->clear()Lgqn;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqn;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqn;->_emptyArray:[Lgqn;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqn;->_emptyArray:[Lgqn;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqn;

    sput-object v0, Lgqn;->_emptyArray:[Lgqn;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqn;->_emptyArray:[Lgqn;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqn;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lgqn;

    invoke-direct {v0}, Lgqn;-><init>()V

    invoke-virtual {v0, p0}, Lgqn;->mergeFrom(Lhfp;)Lgqn;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqn;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lgqn;

    invoke-direct {v0}, Lgqn;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqn;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqn;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgqn;->requestHeader:Lgls;

    .line 11
    iput-object v0, p0, Lgqn;->invitation:Lgqs;

    .line 12
    iput-object v0, p0, Lgqn;->syncMetadata:Lgoa;

    .line 13
    iput-object v0, p0, Lgqn;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgqn;->cachedSize:I

    .line 15
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 25
    iget-object v1, p0, Lgqn;->requestHeader:Lgls;

    if-eqz v1, :cond_0

    .line 26
    const/4 v1, 0x1

    iget-object v2, p0, Lgqn;->requestHeader:Lgls;

    .line 27
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_0
    iget-object v1, p0, Lgqn;->invitation:Lgqs;

    if-eqz v1, :cond_1

    .line 29
    const/4 v1, 0x2

    iget-object v2, p0, Lgqn;->invitation:Lgqs;

    .line 30
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_1
    iget-object v1, p0, Lgqn;->syncMetadata:Lgoa;

    if-eqz v1, :cond_2

    .line 32
    const/4 v1, 0x3

    iget-object v2, p0, Lgqn;->syncMetadata:Lgoa;

    .line 33
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_2
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqn;
    .locals 1

    .prologue
    .line 35
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :sswitch_0
    return-object p0

    .line 40
    :sswitch_1
    iget-object v0, p0, Lgqn;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgqn;->requestHeader:Lgls;

    .line 42
    :cond_1
    iget-object v0, p0, Lgqn;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 44
    :sswitch_2
    iget-object v0, p0, Lgqn;->invitation:Lgqs;

    if-nez v0, :cond_2

    .line 45
    new-instance v0, Lgqs;

    invoke-direct {v0}, Lgqs;-><init>()V

    iput-object v0, p0, Lgqn;->invitation:Lgqs;

    .line 46
    :cond_2
    iget-object v0, p0, Lgqn;->invitation:Lgqs;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_3
    iget-object v0, p0, Lgqn;->syncMetadata:Lgoa;

    if-nez v0, :cond_3

    .line 49
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgqn;->syncMetadata:Lgoa;

    .line 50
    :cond_3
    iget-object v0, p0, Lgqn;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lgqn;->mergeFrom(Lhfp;)Lgqn;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lgqn;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lgqn;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lgqn;->invitation:Lgqs;

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lgqn;->invitation:Lgqs;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_1
    iget-object v0, p0, Lgqn;->syncMetadata:Lgoa;

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-object v1, p0, Lgqn;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 23
    return-void
.end method
