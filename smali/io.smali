.class public Lio;
.super Lip;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private W:I

.field private X:I

.field private Y:Z

.field private Z:I

.field public a:Z

.field private aa:Landroid/app/Dialog;

.field private ab:Z

.field private ac:Z

.field private ad:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    iput v0, p0, Lio;->W:I

    .line 3
    iput v0, p0, Lio;->X:I

    .line 4
    iput-boolean v1, p0, Lio;->Y:Z

    .line 5
    iput-boolean v1, p0, Lio;->a:Z

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lio;->Z:I

    .line 7
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lio;->X:I

    return v0
.end method

.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lio;->h()Lit;

    move-result-object v1

    invoke-virtual {p0}, Lio;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public a(Landroid/app/Dialog;I)V
    .locals 2

    .prologue
    .line 62
    packed-switch p2, :pswitch_data_0

    .line 65
    :goto_0
    return-void

    .line 63
    :pswitch_0
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 64
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 37
    iget-boolean v0, p0, Lio;->ad:Z

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio;->ac:Z

    .line 39
    :cond_0
    return-void
.end method

.method public a(Lja;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio;->ac:Z

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio;->ad:Z

    .line 10
    invoke-virtual {p1}, Lja;->a()Ljy;

    move-result-object v0

    .line 11
    invoke-virtual {v0, p0, p2}, Ljy;->a(Lip;Ljava/lang/String;)Ljy;

    .line 12
    invoke-virtual {v0}, Ljy;->a()I

    .line 13
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 14
    iget-boolean v0, p0, Lio;->ac:Z

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 16
    :cond_0
    iput-boolean v2, p0, Lio;->ac:Z

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio;->ad:Z

    .line 18
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 19
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    .line 21
    :cond_1
    iput-boolean v2, p0, Lio;->ab:Z

    .line 22
    iget v0, p0, Lio;->Z:I

    if-ltz v0, :cond_2

    .line 24
    iget-object v0, p0, Lip;->s:Ljc;

    .line 25
    iget v1, p0, Lio;->Z:I

    invoke-virtual {v0, v1, v2}, Lja;->a(II)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lio;->Z:I

    goto :goto_0

    .line 28
    :cond_2
    iget-object v0, p0, Lip;->s:Ljc;

    .line 29
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 30
    invoke-virtual {v0, p0}, Ljy;->a(Lip;)Ljy;

    .line 31
    if-eqz p1, :cond_3

    .line 32
    invoke-virtual {v0}, Ljy;->b()I

    goto :goto_0

    .line 33
    :cond_3
    invoke-virtual {v0}, Ljy;->a()I

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lip;->b()V

    .line 41
    iget-boolean v0, p0, Lio;->ad:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio;->ac:Z

    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio;->ac:Z

    .line 43
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 45
    iget v0, p0, Lio;->y:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lio;->a:Z

    .line 46
    if-eqz p1, :cond_0

    .line 47
    const-string v0, "android:style"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lio;->W:I

    .line 48
    const-string v0, "android:theme"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lio;->X:I

    .line 49
    const-string v0, "android:cancelable"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lio;->Y:Z

    .line 50
    const-string v0, "android:showsDialog"

    iget-boolean v1, p0, Lio;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lio;->a:Z

    .line 51
    const-string v0, "android:backStackId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lio;->Z:I

    .line 52
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 45
    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 2

    .prologue
    .line 53
    iget-boolean v0, p0, Lio;->a:Z

    if-nez v0, :cond_0

    .line 54
    invoke-super {p0, p1}, Lip;->c(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lio;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    .line 56
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    iget v1, p0, Lio;->W:I

    invoke-virtual {p0, v0, v1}, Lio;->a(Landroid/app/Dialog;I)V

    .line 58
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lio;->t:Liz;

    .line 60
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 61
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Lip;->d(Landroid/os/Bundle;)V

    .line 72
    iget-boolean v0, p0, Lio;->a:Z

    if-nez v0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 77
    if-eqz v0, :cond_3

    .line 78
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 79
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DialogFragment can not be attached to a container view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_2
    iget-object v1, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 81
    :cond_3
    invoke-virtual {p0}, Lio;->h()Lit;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_4

    .line 83
    iget-object v1, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 84
    :cond_4
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    iget-boolean v1, p0, Lio;->Y:Z

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 85
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 86
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 87
    if-eqz p1, :cond_0

    .line 88
    const-string v0, "android:savedDialogState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    .line 90
    iget-object v1, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    const-string v1, "android:savedDialogState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 102
    :cond_0
    iget v0, p0, Lio;->W:I

    if-eqz v0, :cond_1

    .line 103
    const-string v0, "android:style"

    iget v1, p0, Lio;->W:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    :cond_1
    iget v0, p0, Lio;->X:I

    if-eqz v0, :cond_2

    .line 105
    const-string v0, "android:theme"

    iget v1, p0, Lio;->X:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    :cond_2
    iget-boolean v0, p0, Lio;->Y:Z

    if-nez v0, :cond_3

    .line 107
    const-string v0, "android:cancelable"

    iget-boolean v1, p0, Lio;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 108
    :cond_3
    iget-boolean v0, p0, Lio;->a:Z

    if-nez v0, :cond_4

    .line 109
    const-string v0, "android:showsDialog"

    iget-boolean v1, p0, Lio;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 110
    :cond_4
    iget v0, p0, Lio;->Z:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 111
    const-string v0, "android:backStackId"

    iget v1, p0, Lio;->Z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    :cond_5
    return-void
.end method

.method public final l_()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lip;->l_()V

    .line 114
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 116
    :cond_0
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lip;->n_()V

    .line 118
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio;->ab:Z

    .line 120
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    .line 122
    :cond_0
    return-void
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lip;->o_()V

    .line 93
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio;->ab:Z

    .line 95
    iget-object v0, p0, Lio;->aa:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 96
    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lio;->ab:Z

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lio;->a(Z)V

    .line 70
    :cond_0
    return-void
.end method
