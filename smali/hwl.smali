.class public final Lhwl;
.super Lhwm;
.source "PG"


# static fields
.field private static a:Lhvk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lhwl;

    invoke-direct {v0}, Lhwl;-><init>()V

    sput-object v0, Lhwl;->a:Lhvk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6
    sget-object v0, Lhwt;->a:Lhvk;

    invoke-direct {p0, v0}, Lhwm;-><init>(Lhvk;)V

    .line 7
    const-string v0, "Content-Type"

    sget-object v1, Lhwh;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 8
    const-string v0, "Content-Length"

    sget-object v1, Lhvz;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 9
    const-string v0, "Content-Transfer-Encoding"

    sget-object v1, Lhwf;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 10
    const-string v0, "Content-Disposition"

    sget-object v1, Lhvt;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 11
    const-string v0, "Content-ID"

    sget-object v1, Lhvv;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 12
    const-string v0, "Content-MD5"

    sget-object v1, Lhwd;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 13
    const-string v0, "Content-Description"

    sget-object v1, Lhvr;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 14
    const-string v0, "Content-Language"

    sget-object v1, Lhvx;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 15
    const-string v0, "Content-Location"

    sget-object v1, Lhwb;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 16
    const-string v0, "MIME-Version"

    sget-object v1, Lhwr;->a:Lhvk;

    invoke-virtual {p0, v0, v1}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 17
    sget-object v0, Lhwj;->a:Lhvk;

    .line 18
    const-string v1, "Date"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 19
    const-string v1, "Resent-Date"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 20
    sget-object v0, Lhwp;->a:Lhvk;

    .line 21
    const-string v1, "From"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 22
    const-string v1, "Resent-From"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 23
    sget-object v0, Lhwn;->a:Lhvk;

    .line 24
    const-string v1, "Sender"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 25
    const-string v1, "Resent-Sender"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 26
    sget-object v0, Lhvp;->a:Lhvk;

    .line 27
    const-string v1, "To"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 28
    const-string v1, "Resent-To"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 29
    const-string v1, "Cc"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 30
    const-string v1, "Resent-Cc"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 31
    const-string v1, "Bcc"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 32
    const-string v1, "Resent-Bcc"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 33
    const-string v1, "Reply-To"

    invoke-virtual {p0, v1, v0}, Lhwl;->a(Ljava/lang/String;Lhvk;)V

    .line 34
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhxx;
    .locals 3

    .prologue
    .line 1
    sget-object v0, Lhvg;->b:Lhvg;

    .line 2
    invoke-static {p0}, Lhyp;->a(Ljava/lang/String;)Lhyi;

    move-result-object v1

    .line 3
    sget-object v2, Lhyg;->b:Lhyg;

    invoke-virtual {v2, v1}, Lhyg;->a(Lhyi;)Lhyf;

    move-result-object v1

    .line 4
    sget-object v2, Lhwl;->a:Lhvk;

    invoke-interface {v2, v1, v0}, Lhvk;->a(Lhxx;Lhvg;)Lhxx;

    move-result-object v0

    .line 5
    return-object v0
.end method
