.class public final Laoy;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# instance fields
.field public final a:Lapb;

.field private b:Landroid/content/Context;

.field private c:Lbmm;

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lapb;Lbmm;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Laoy;->b:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Laoy;->a:Lapb;

    .line 4
    iput-object p3, p0, Laoy;->c:Lbmm;

    .line 5
    iput-object p4, p0, Laoy;->d:Ljava/lang/String;

    .line 6
    return-void
.end method

.method public static a(Landroid/content/Context;)Laoy;
    .locals 5

    .prologue
    .line 7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 8
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 9
    new-instance v2, Laoy;

    .line 11
    new-instance v3, Laoz;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 12
    invoke-direct {v3, v4, v0}, Laoz;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;)V

    .line 13
    new-instance v0, Lbmm;

    invoke-direct {v0, p0, v1}, Lbmm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v2, p0, v3, v0, v1}, Laoy;-><init>(Landroid/content/Context;Lapb;Lbmm;Ljava/lang/String;)V

    .line 14
    return-object v2
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 15
    if-nez p1, :cond_0

    .line 16
    const-string v0, "CallLogNotificationsQueryHelper.markSingleMissedCallInCallLogAsRead"

    const-string v1, "call URI is null, unable to mark call as read"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :goto_0
    return-void

    .line 17
    :cond_0
    invoke-static {p0, p1}, Laoy;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const-string v0, "CallLogNotificationsQueryHelper.markMissedCallsInCallLogAsRead"

    const-string v1, "locked"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    :goto_0
    return-void

    .line 22
    :cond_0
    invoke-static {p0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 23
    const-string v0, "CallLogNotificationsQueryHelper.markMissedCallsInCallLogAsRead"

    const-string v1, "no permission"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 26
    const-string v1, "new"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 27
    const-string v1, "is_read"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 28
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    const-string v2, "new"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    const-string v2, " = 1 AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    const-string v2, "type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 35
    if-nez p1, :cond_2

    sget-object p1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    .line 36
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x3

    .line 37
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 38
    invoke-virtual {v2, p1, v0, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    const-string v1, "CallLogNotificationsQueryHelper.markMissedCallsInCallLogAsRead"

    const-string v2, "contacts provider update command failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;)Lbml;
    .locals 3

    .prologue
    .line 44
    if-nez p3, :cond_0

    .line 45
    iget-object p3, p0, Laoy;->d:Ljava/lang/String;

    .line 46
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 47
    :cond_1
    new-instance v0, Lbml;

    invoke-direct {v0}, Lbml;-><init>()V

    .line 48
    iput-object p1, v0, Lbml;->h:Ljava/lang/String;

    .line 49
    invoke-static {p1, p3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->i:Ljava/lang/String;

    .line 50
    invoke-static {p1, p3}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->k:Ljava/lang/String;

    .line 51
    iget-object v1, p0, Laoy;->b:Landroid/content/Context;

    const/4 v2, 0x0

    .line 52
    invoke-static {v1, p1, p2, v2}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;IZ)Ljava/lang/CharSequence;

    move-result-object v1

    .line 53
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->d:Ljava/lang/String;

    .line 54
    iget-object v1, v0, Lbml;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 64
    :goto_0
    return-object v0

    .line 56
    :cond_2
    iget-object v1, p0, Laoy;->c:Lbmm;

    invoke-virtual {v1, p1, p3}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;)Lbml;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_3

    iget-object v2, v1, Lbml;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object v0, v1

    .line 58
    goto :goto_0

    .line 59
    :cond_3
    iget-object v1, v0, Lbml;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 60
    iget-object v1, v0, Lbml;->i:Ljava/lang/String;

    iput-object v1, v0, Lbml;->d:Ljava/lang/String;

    goto :goto_0

    .line 61
    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 62
    iput-object p1, v0, Lbml;->d:Ljava/lang/String;

    goto :goto_0

    .line 63
    :cond_5
    iget-object v1, p0, Laoy;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110312

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Laoy;->a:Lapb;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lapb;->a(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
