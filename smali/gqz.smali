.class public final Lgqz;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4
    invoke-direct {p0}, Lhft;-><init>()V

    .line 6
    iput-object v0, p0, Lgqz;->a:Ljava/lang/Boolean;

    .line 7
    iput-object v0, p0, Lgqz;->b:Ljava/lang/Boolean;

    .line 8
    iput-object v0, p0, Lgqz;->c:Ljava/lang/Boolean;

    .line 9
    iput-object v0, p0, Lgqz;->d:Ljava/lang/Integer;

    .line 10
    iput-object v0, p0, Lgqz;->e:Ljava/lang/Long;

    .line 11
    iput-object v0, p0, Lgqz;->f:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgqz;->g:Ljava/lang/Boolean;

    .line 13
    iput-object v0, p0, Lgqz;->h:Ljava/lang/Long;

    .line 14
    iput-object v0, p0, Lgqz;->i:Ljava/lang/Long;

    .line 15
    iput-object v0, p0, Lgqz;->j:Ljava/lang/Long;

    .line 16
    iput-object v0, p0, Lgqz;->k:Ljava/lang/Integer;

    .line 17
    iput-object v0, p0, Lgqz;->l:Ljava/lang/Integer;

    .line 18
    iput-object v0, p0, Lgqz;->m:Ljava/lang/Boolean;

    .line 19
    iput-object v0, p0, Lgqz;->n:Ljava/lang/Boolean;

    .line 20
    iput-object v0, p0, Lgqz;->o:Ljava/lang/Boolean;

    .line 21
    iput-object v0, p0, Lgqz;->unknownFieldData:Lhfv;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lgqz;->cachedSize:I

    .line 23
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum ClientEventObservedStatus"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhfp;)Lgqz;
    .locals 3

    .prologue
    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 118
    sparse-switch v0, :sswitch_data_0

    .line 120
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    :sswitch_0
    return-object p0

    .line 122
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 124
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 126
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 130
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 131
    invoke-static {v2}, Lgqt;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqz;->d:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 135
    invoke-virtual {p0, p1, v0}, Lgqz;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 138
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 139
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqz;->e:Ljava/lang/Long;

    goto :goto_0

    .line 141
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqz;->f:Ljava/lang/String;

    goto :goto_0

    .line 143
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 146
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 147
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqz;->h:Ljava/lang/Long;

    goto :goto_0

    .line 150
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 151
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqz;->i:Ljava/lang/Long;

    goto :goto_0

    .line 154
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 155
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgqz;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 157
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 159
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 160
    invoke-static {v2}, Lgqz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqz;->k:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 163
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 164
    invoke-virtual {p0, p1, v0}, Lgqz;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 166
    :sswitch_c
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 168
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 169
    invoke-static {v2}, Lgqz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgqz;->l:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 172
    :catch_2
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 173
    invoke-virtual {p0, p1, v0}, Lgqz;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 175
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 177
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 179
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgqz;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 57
    iget-object v1, p0, Lgqz;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 58
    const/4 v1, 0x1

    iget-object v2, p0, Lgqz;->a:Ljava/lang/Boolean;

    .line 59
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 60
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 61
    add-int/2addr v0, v1

    .line 62
    :cond_0
    iget-object v1, p0, Lgqz;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 63
    const/4 v1, 0x2

    iget-object v2, p0, Lgqz;->b:Ljava/lang/Boolean;

    .line 64
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 65
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 66
    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Lgqz;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 68
    const/4 v1, 0x3

    iget-object v2, p0, Lgqz;->c:Ljava/lang/Boolean;

    .line 69
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 70
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 71
    add-int/2addr v0, v1

    .line 72
    :cond_2
    iget-object v1, p0, Lgqz;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 73
    const/4 v1, 0x4

    iget-object v2, p0, Lgqz;->d:Ljava/lang/Integer;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Lgqz;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 76
    const/4 v1, 0x5

    iget-object v2, p0, Lgqz;->e:Ljava/lang/Long;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_4
    iget-object v1, p0, Lgqz;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 79
    const/4 v1, 0x6

    iget-object v2, p0, Lgqz;->f:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_5
    iget-object v1, p0, Lgqz;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 82
    const/4 v1, 0x7

    iget-object v2, p0, Lgqz;->g:Ljava/lang/Boolean;

    .line 83
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 84
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 85
    add-int/2addr v0, v1

    .line 86
    :cond_6
    iget-object v1, p0, Lgqz;->h:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 87
    const/16 v1, 0x8

    iget-object v2, p0, Lgqz;->h:Ljava/lang/Long;

    .line 88
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_7
    iget-object v1, p0, Lgqz;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 90
    const/16 v1, 0x9

    iget-object v2, p0, Lgqz;->i:Ljava/lang/Long;

    .line 91
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_8
    iget-object v1, p0, Lgqz;->j:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 93
    const/16 v1, 0xa

    iget-object v2, p0, Lgqz;->j:Ljava/lang/Long;

    .line 94
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_9
    iget-object v1, p0, Lgqz;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 96
    const/16 v1, 0xb

    iget-object v2, p0, Lgqz;->k:Ljava/lang/Integer;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_a
    iget-object v1, p0, Lgqz;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 99
    const/16 v1, 0xc

    iget-object v2, p0, Lgqz;->l:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_b
    iget-object v1, p0, Lgqz;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 102
    const/16 v1, 0xd

    iget-object v2, p0, Lgqz;->m:Ljava/lang/Boolean;

    .line 103
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 104
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 105
    add-int/2addr v0, v1

    .line 106
    :cond_c
    iget-object v1, p0, Lgqz;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    .line 107
    const/16 v1, 0xe

    iget-object v2, p0, Lgqz;->n:Ljava/lang/Boolean;

    .line 108
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 109
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 110
    add-int/2addr v0, v1

    .line 111
    :cond_d
    iget-object v1, p0, Lgqz;->o:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    .line 112
    const/16 v1, 0xf

    iget-object v2, p0, Lgqz;->o:Ljava/lang/Boolean;

    .line 113
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 114
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 115
    add-int/2addr v0, v1

    .line 116
    :cond_e
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lgqz;->a(Lhfp;)Lgqz;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lgqz;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Lgqz;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 26
    :cond_0
    iget-object v0, p0, Lgqz;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Lgqz;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 28
    :cond_1
    iget-object v0, p0, Lgqz;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Lgqz;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 30
    :cond_2
    iget-object v0, p0, Lgqz;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 31
    const/4 v0, 0x4

    iget-object v1, p0, Lgqz;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 32
    :cond_3
    iget-object v0, p0, Lgqz;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 33
    const/4 v0, 0x5

    iget-object v1, p0, Lgqz;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 34
    :cond_4
    iget-object v0, p0, Lgqz;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 35
    const/4 v0, 0x6

    iget-object v1, p0, Lgqz;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 36
    :cond_5
    iget-object v0, p0, Lgqz;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 37
    const/4 v0, 0x7

    iget-object v1, p0, Lgqz;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 38
    :cond_6
    iget-object v0, p0, Lgqz;->h:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 39
    const/16 v0, 0x8

    iget-object v1, p0, Lgqz;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 40
    :cond_7
    iget-object v0, p0, Lgqz;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 41
    const/16 v0, 0x9

    iget-object v1, p0, Lgqz;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 42
    :cond_8
    iget-object v0, p0, Lgqz;->j:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 43
    const/16 v0, 0xa

    iget-object v1, p0, Lgqz;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 44
    :cond_9
    iget-object v0, p0, Lgqz;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 45
    const/16 v0, 0xb

    iget-object v1, p0, Lgqz;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 46
    :cond_a
    iget-object v0, p0, Lgqz;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 47
    const/16 v0, 0xc

    iget-object v1, p0, Lgqz;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 48
    :cond_b
    iget-object v0, p0, Lgqz;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 49
    const/16 v0, 0xd

    iget-object v1, p0, Lgqz;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 50
    :cond_c
    iget-object v0, p0, Lgqz;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    .line 51
    const/16 v0, 0xe

    iget-object v1, p0, Lgqz;->n:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 52
    :cond_d
    iget-object v0, p0, Lgqz;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 53
    const/16 v0, 0xf

    iget-object v1, p0, Lgqz;->o:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 54
    :cond_e
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 55
    return-void
.end method
