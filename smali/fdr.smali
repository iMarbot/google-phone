.class public final Lfdr;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lfdb;ZI)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 81
    invoke-interface {p1}, Lfdb;->e()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 82
    const-string v0, "telecom"

    .line 83
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 85
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getCapabilities()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move v0, v3

    .line 88
    :goto_0
    if-nez v0, :cond_1

    .line 89
    const-string v0, "HandoffInitiator.isHandoffPossible, can\'t make cell calls"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 156
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 87
    goto :goto_0

    .line 91
    :cond_1
    const-string v0, "HandoffInitiator.isHandoffPossible"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-interface {p1}, Lfdb;->a()Lfdd;

    move-result-object v0

    .line 93
    iget-boolean v0, v0, Lfdd;->h:Z

    .line 94
    if-eqz v0, :cond_2

    .line 95
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff not allowed for LTE fallback calls"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 96
    goto :goto_1

    .line 97
    :cond_2
    packed-switch p3, :pswitch_data_0

    .line 139
    :pswitch_0
    const/16 v0, 0x47

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "HandoffInitiator.isHandoffPossible, unknown handoff reason: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 140
    goto :goto_1

    .line 99
    :pswitch_1
    const-string v0, "manual_handoff_allowed"

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 100
    if-nez v0, :cond_8

    .line 101
    const-string v0, "HandoffInitiator.isHandoffPossible, manual handoff not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 102
    goto :goto_1

    .line 103
    :pswitch_2
    invoke-interface {p1}, Lfdb;->e()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 105
    const-string v0, "handoff_on_wifi_loss_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 106
    if-nez v0, :cond_8

    .line 107
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff on wifi loss not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 108
    goto :goto_1

    .line 110
    :cond_3
    const-string v0, "handoff_on_cell_loss_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 111
    if-nez v0, :cond_8

    .line 112
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff on cell loss not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 113
    goto :goto_1

    .line 115
    :pswitch_3
    invoke-interface {p1}, Lfdb;->a()Lfdd;

    move-result-object v0

    .line 116
    iget-object v0, v0, Lfdd;->d:Lffd;

    .line 117
    invoke-virtual {v0}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-static {p0, v0}, Lffe;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 119
    const-string v0, "HandoffInitiator.isHandoffPossible, emergency call, handoff for network optimization not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 120
    goto/16 :goto_1

    .line 121
    :cond_4
    const/16 v0, 0xa

    if-ne p3, v0, :cond_6

    .line 123
    const-string v0, "activity_handoff_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 125
    if-eqz v2, :cond_5

    const-string v0, "allowed."

    .line 126
    :goto_2
    const-string v4, "HandoffInitiator.isHandoffPossible, activity recognition handoff is %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v1

    invoke-static {v4, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 127
    goto/16 :goto_1

    .line 125
    :cond_5
    const-string v0, "not allowed."

    goto :goto_2

    .line 128
    :cond_6
    invoke-interface {p1}, Lfdb;->e()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 130
    const-string v0, "wifi_network_optimizing_handoff_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 131
    if-nez v0, :cond_8

    .line 132
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff for wifi network optimization not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 133
    goto/16 :goto_1

    .line 135
    :cond_7
    const-string v0, "cell_network_optimizing_handoff_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 136
    if-nez v0, :cond_8

    .line 137
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff for cell network optimization not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 138
    goto/16 :goto_1

    .line 141
    :cond_8
    invoke-interface {p1}, Lfdb;->a()Lfdd;

    move-result-object v0

    .line 142
    iget-object v0, v0, Lfdd;->j:Lffb;

    .line 144
    invoke-virtual {v0}, Lffb;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 146
    const-string v2, "international_handoff_allowed"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 147
    if-nez v2, :cond_9

    .line 148
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff while international not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 149
    goto/16 :goto_1

    .line 150
    :cond_9
    invoke-virtual {v0}, Lffb;->b()I

    move-result v0

    if-eq v0, v3, :cond_a

    .line 152
    const-string v0, "roaming_handoff_allowed"

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 153
    if-nez v0, :cond_a

    .line 154
    const-string v0, "HandoffInitiator.isHandoffPossible, handoff while roaming not allowed"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 155
    goto/16 :goto_1

    :cond_a
    move v0, v3

    .line 156
    goto/16 :goto_1

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfdd;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 2
    const/16 v0, 0x46

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "HandoffInitiator.handoffHangoutsToCircuitSwitched, reason: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p2, Lfdd;->e:Lfdb;

    .line 7
    invoke-static {p1, v0, v6, p3}, Lfdr;->a(Landroid/content/Context;Lfdb;ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 10
    :cond_0
    iget-object v0, p2, Lfdd;->g:Lfdk;

    .line 11
    if-eqz v0, :cond_2

    .line 12
    if-ne p3, v7, :cond_1

    .line 13
    const-string v0, "HandoffInitiator.handoffHangoutsToCircuitSwitched, notify handoff about network loss"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    iget-object v0, p2, Lfdd;->g:Lfdk;

    .line 17
    iget v1, v0, Lfdk;->c:I

    if-eq v1, v7, :cond_1

    .line 18
    iput v7, v0, Lfdk;->c:I

    .line 19
    iget-object v1, v0, Lfdk;->b:Lfdb;

    if-eqz v1, :cond_1

    .line 20
    invoke-virtual {v0, v8, v6}, Lfdk;->a(ZI)V

    .line 21
    :cond_1
    const-string v0, "HandoffInitiator.handoffHangoutsToCircuitSwitched, handoff pending, skipping"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 24
    :cond_2
    iget-object v0, p2, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 25
    const-string v1, "telecom"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 26
    new-instance v1, Lfdo;

    invoke-direct {v1, p1, v0}, Lfdo;-><init>(Landroid/content/Context;Landroid/telecom/TelecomManager;)V

    .line 27
    new-instance v0, Lfdk;

    invoke-direct {v0, p1, p2, v1, p3}, Lfdk;-><init>(Landroid/content/Context;Lfdd;Lfdn;I)V

    .line 28
    iput-object v0, v1, Lfdo;->b:Lfdk;

    .line 30
    const-string v0, "HandoffHangoutsToCircuitSwitched.startHandoff"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, v1, Lfdo;->b:Lfdk;

    .line 32
    iget-object v0, v0, Lfdk;->a:Lfdd;

    .line 34
    invoke-virtual {v1, v0}, Lfdo;->a(Lfdd;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 35
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x42

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "HandoffHangoutsToCircuitSwitched.startHandoff, got phone account: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    if-nez v2, :cond_3

    .line 37
    const-string v0, "HandoffHangoutsToCircuitSwitched.startHandoff, no phone account."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iget-object v0, v1, Lfdo;->b:Lfdk;

    const/16 v1, 0xe1

    invoke-virtual {v0, v6, v1}, Lfdk;->a(ZI)V

    goto :goto_0

    .line 41
    :cond_3
    iget-object v3, v0, Lfdd;->i:Ljava/lang/String;

    .line 43
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 44
    const-string v0, "HandoffHangoutsToCircuitSwitched.startHandoff, no handoff number"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    iget-object v0, v1, Lfdo;->b:Lfdk;

    const/16 v1, 0x140

    invoke-virtual {v0, v6, v1}, Lfdk;->a(ZI)V

    goto/16 :goto_0

    .line 47
    :cond_4
    iget-object v3, v1, Lfdo;->b:Lfdk;

    .line 48
    iget v3, v3, Lfdk;->d:I

    .line 50
    if-eq v3, v7, :cond_6

    const/4 v4, 0x4

    if-eq v3, v4, :cond_6

    .line 51
    const-string v2, "HandoffHangoutsToCircuitSwitched.startHandoff, not possible for call state: "

    .line 52
    invoke-static {v3}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v2, v6, [Ljava/lang/Object;

    .line 53
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    iget-object v0, v1, Lfdo;->b:Lfdk;

    const/16 v1, 0xe3

    invoke-virtual {v0, v6, v1}, Lfdk;->a(ZI)V

    goto/16 :goto_0

    .line 52
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 57
    :cond_6
    iget-object v0, v0, Lfdd;->b:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 59
    if-le v0, v8, :cond_7

    .line 60
    const/16 v2, 0x54

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HandoffHangoutsToCircuitSwitched.startHandoff, call count: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", fail handoff"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    iget-object v0, v1, Lfdo;->b:Lfdk;

    const/16 v1, 0xdd

    invoke-virtual {v0, v6, v1}, Lfdk;->a(ZI)V

    goto/16 :goto_0

    .line 63
    :cond_7
    iget-object v0, v1, Lfdo;->a:Landroid/content/Context;

    new-instance v3, Lfdq;

    invoke-direct {v3, v1, v2}, Lfdq;-><init>(Lfdo;Landroid/telecom/PhoneAccountHandle;)V

    invoke-static {v0, v3}, Lfem;->a(Landroid/content/Context;Lfen;)V

    goto/16 :goto_0
.end method

.method final b(Landroid/content/Context;Lfdd;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    const/16 v0, 0x46

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "HandoffInitiator.handoffCircuitSwitchedToHangouts, reason: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iget-object v0, p2, Lfdd;->e:Lfdb;

    .line 70
    invoke-static {p1, v0, v2, p3}, Lfdr;->a(Landroid/content/Context;Lfdb;ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p2, Lfdd;->g:Lfdk;

    .line 74
    if-eqz v0, :cond_1

    .line 75
    const-string v0, "HandoffInitiator.handoffCircuitSwitchedToHangouts, handoff pending, skipping"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :cond_1
    new-instance v0, Lfdj;

    invoke-direct {v0, p1}, Lfdj;-><init>(Landroid/content/Context;)V

    .line 77
    new-instance v1, Lfdk;

    invoke-direct {v1, p1, p2, v0, p3}, Lfdk;-><init>(Landroid/content/Context;Lfdd;Lfdn;I)V

    .line 78
    iput-object v1, v0, Lfdj;->a:Lfdk;

    .line 79
    invoke-virtual {v0}, Lfdj;->a()V

    goto :goto_0
.end method
