.class public final Lfoy;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field public final eglContext:Lfus;

.field public eglSurface:Lfut;

.field public final surface:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lfus;Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfoy;->eglContext:Lfus;

    .line 3
    iput-object p2, p0, Lfoy;->surface:Landroid/view/Surface;

    .line 4
    return-void
.end method


# virtual methods
.method public final finishFrameOperation()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfoy;->eglSurface:Lfut;

    invoke-interface {v0}, Lfut;->swapBuffers()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lfoy;->surface:Landroid/view/Surface;

    return-object v0
.end method

.method public final initializeGLContext()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 6
    iget-object v1, p0, Lfoy;->eglSurface:Lfut;

    if-eqz v1, :cond_0

    .line 16
    :goto_0
    return v0

    .line 8
    :cond_0
    iget-object v1, p0, Lfoy;->surface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 9
    const-string v1, "We already released this input surface. Cannot initialize GL context for it."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 11
    :cond_1
    iget-object v1, p0, Lfoy;->eglContext:Lfus;

    iget-object v2, p0, Lfoy;->surface:Landroid/view/Surface;

    invoke-interface {v1, v2}, Lfus;->createSurface(Ljava/lang/Object;)Lfut;

    move-result-object v1

    iput-object v1, p0, Lfoy;->eglSurface:Lfut;

    .line 12
    iget-object v1, p0, Lfoy;->eglSurface:Lfut;

    if-nez v1, :cond_2

    .line 13
    const-string v1, "eglCreateWindowSurface"

    invoke-static {v1}, Lfmk;->d(Ljava/lang/String;)V

    .line 14
    const-string v1, "Unable to create EGL surface for encoder input."

    invoke-static {v1}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 16
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lfoy;->eglSurface:Lfut;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Lfoy;->eglSurface:Lfut;

    invoke-interface {v0}, Lfut;->release()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lfoy;->eglSurface:Lfut;

    .line 20
    :cond_0
    iget-object v0, p0, Lfoy;->surface:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lfoy;->surface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 22
    :cond_1
    return-void
.end method

.method public final startFrameOperation(J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 23
    iget-object v1, p0, Lfoy;->eglSurface:Lfut;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfoy;->surface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 31
    :cond_0
    :goto_0
    return v0

    .line 25
    :cond_1
    iget-object v1, p0, Lfoy;->eglSurface:Lfut;

    invoke-interface {v1}, Lfut;->makeCurrent()I

    move-result v1

    if-nez v1, :cond_0

    .line 27
    iget-object v0, p0, Lfoy;->eglContext:Lfus;

    instance-of v0, v0, Lfuq;

    if-eqz v0, :cond_2

    .line 28
    invoke-static {}, Landroid/opengl/EGL14;->eglGetCurrentDisplay()Landroid/opengl/EGLDisplay;

    move-result-object v0

    const/16 v1, 0x3059

    invoke-static {v1}, Landroid/opengl/EGL14;->eglGetCurrentSurface(I)Landroid/opengl/EGLSurface;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 29
    invoke-virtual {v2, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 30
    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 31
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
