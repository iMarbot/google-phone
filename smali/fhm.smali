.class final Lfhm;
.super Lfvt;
.source "PG"


# instance fields
.field public final a:Landroid/util/ArraySet;

.field private b:Lfhn;

.field private synthetic c:Lfhl;


# direct methods
.method constructor <init>(Lfhl;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lfhm;->c:Lfhl;

    invoke-direct {p0}, Lfvt;-><init>()V

    .line 2
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lfhm;->b:Lfhn;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lfhm;->c:Lfhl;

    .line 77
    invoke-virtual {v0}, Lfhl;->e()Lfvr;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lfhm;->c:Lfhl;

    .line 80
    iget-object v0, v0, Lfhl;->a:Lfvj;

    .line 82
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 83
    invoke-interface {v0}, Lfvr;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnf;

    .line 84
    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    iget-object v1, p0, Lfhm;->b:Lfhn;

    .line 85
    invoke-interface {v0, v1}, Lfnf;->removeListener(Lfnl;)V

    .line 86
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfhm;->b:Lfhn;

    .line 87
    :cond_1
    return-void
.end method

.method public final onCallEnd(Lfvx;)V
    .locals 2

    .prologue
    .line 36
    const-string v0, "CallContextImpl.CallbackRelay.onCallEnd"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p0}, Lfhm;->a()V

    .line 38
    new-instance v0, Landroid/util/ArraySet;

    iget-object v1, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(Landroid/util/ArraySet;)V

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 39
    invoke-interface {v0, p1}, Lfhf;->a(Lfvx;)V

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method

.method public final onCallJoin(Lfvy;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CallContextImpl.CallbackRelay.onCallJoin, "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v1, p0, Lfhm;->c:Lfhl;

    .line 6
    invoke-virtual {v1}, Lfhl;->e()Lfvr;

    move-result-object v1

    .line 7
    if-eqz v1, :cond_1

    .line 8
    iget-object v1, p0, Lfhm;->b:Lfhn;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 9
    new-instance v0, Lfhn;

    .line 10
    invoke-direct {v0, p0}, Lfhn;-><init>(Lfhm;)V

    .line 11
    iput-object v0, p0, Lfhm;->b:Lfhn;

    .line 12
    iget-object v0, p0, Lfhm;->c:Lfhl;

    .line 13
    iget-object v0, v0, Lfhl;->a:Lfvj;

    .line 15
    iget-object v0, v0, Lfvj;->d:Lfnp;

    .line 16
    invoke-interface {v0}, Lfvr;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnf;

    .line 17
    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    iget-object v1, p0, Lfhm;->b:Lfhn;

    .line 18
    invoke-interface {v0, v1}, Lfnf;->addListener(Lfnl;)V

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    if-eqz p1, :cond_3

    .line 23
    new-instance v0, Lfhj;

    invoke-direct {v0}, Lfhj;-><init>()V

    .line 25
    iget-object v1, p1, Lfvy;->a:Ljava/lang/String;

    .line 26
    iput-object v1, v0, Lfhj;->a:Ljava/lang/String;

    .line 28
    iget-object v1, p1, Lfvy;->b:Ljava/lang/String;

    .line 29
    iput-object v1, v0, Lfhj;->b:Ljava/lang/String;

    move-object v1, v0

    .line 32
    :goto_0
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 33
    invoke-interface {v0, v1}, Lfhf;->a(Lfhj;)V

    goto :goto_1

    .line 35
    :cond_2
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final onFirstAudioPacket()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "CallContextImpl.CallbackRelay.onFirstAudioPacket"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 72
    invoke-interface {v0}, Lfhf;->b()V

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method public final onLogDataPrepared(Lgpn;)V
    .locals 2

    .prologue
    .line 58
    const-string v0, "CallContextImpl.CallbackRelay.onLogDataPrepared"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 60
    invoke-interface {v0, p1}, Lfhf;->a(Lgpn;)V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method

.method public final onParticipantAdded(Lfvz;)V
    .locals 3

    .prologue
    .line 42
    const-string v0, "CallContextImpl.CallbackRelay.onParticipantAdded"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    invoke-static {p1}, Lfhl;->a(Lfvz;)Lfhk;

    move-result-object v1

    .line 46
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 47
    invoke-interface {v0, v1}, Lfhf;->a(Lfhk;)V

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method

.method public final onParticipantRemoved(Lfvz;)V
    .locals 3

    .prologue
    .line 50
    const-string v0, "CallContextImpl.CallbackRelay.onParticipantRemoved"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    invoke-static {p1}, Lfhl;->a(Lfvz;)Lfhk;

    move-result-object v1

    .line 54
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 55
    invoke-interface {v0, v1}, Lfhf;->b(Lfhk;)V

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method public final onQualityNotification(Lfwb;)V
    .locals 3

    .prologue
    .line 63
    const-string v0, "CallContextImpl.CallbackRelay.onQualityNotification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lfhm;->a:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhf;

    .line 66
    iget-object v2, p1, Lfwb;->a:Lgiq;

    .line 67
    invoke-interface {v0, v2}, Lfhf;->a(Lgiq;)V

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method
