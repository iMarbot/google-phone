.class public final Lcny;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    return-void
.end method

.method public constructor <init>(Lcnt;Lcmz;Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iget-object v0, p1, Lcnt;->b:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lcny;->a:Ljava/lang/String;

    .line 8
    iget-object v0, p1, Lcnt;->c:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcny;->b:Ljava/lang/String;

    .line 10
    const-string v0, "realm"

    const-string v1, ""

    invoke-interface {p3, v0, v1}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcny;->c:Ljava/lang/String;

    .line 11
    const-string v0, "nonce"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcny;->d:Ljava/lang/String;

    .line 13
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 14
    const/16 v1, 0x8

    new-array v1, v1, [B

    .line 15
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 16
    const/4 v0, 0x2

    invoke-static {v1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 17
    iput-object v0, p0, Lcny;->f:Ljava/lang/String;

    .line 18
    const-string v0, "00000001"

    iput-object v0, p0, Lcny;->e:Ljava/lang/String;

    .line 19
    const-string v0, "auth"

    iput-object v0, p0, Lcny;->h:Ljava/lang/String;

    .line 20
    const-string v1, "imap/"

    .line 21
    iget-object v0, p2, Lcmz;->c:Ljava/lang/String;

    .line 22
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcny;->g:Ljava/lang/String;

    .line 23
    return-void

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
