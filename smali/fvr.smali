.class public interface abstract Lfvr;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract addCallbacks(Lfvt;)V
.end method

.method public abstract as(Ljava/lang/Class;)Lfvr;
.end method

.method public abstract getAudioCapturer()Lfvp;
.end method

.method public abstract getAudioController()Lfvq;
.end method

.method public abstract getCallStateInfo()Lfvu;
.end method

.method public abstract getCollections()Lfnm;
.end method

.method public abstract getParticipants()Ljava/util/Map;
.end method

.method public abstract isConnected()Z
.end method

.method public abstract isConnecting()Z
.end method

.method public abstract join(Lfvs;)V
.end method

.method public abstract leave()V
.end method

.method public abstract leaveWithAppError(II)V
.end method

.method public abstract removeCallbacks(Lfvt;)V
.end method

.method public abstract setAudioCapturer(Lfvp;)V
.end method

.method public abstract setAudioController(Lfvq;)V
.end method

.method public abstract setAudioPlayoutMute(Z)V
.end method
