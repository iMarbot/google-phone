.class public final Law;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public b:J

.field public c:I

.field public d:I

.field private e:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law;->a:J

    .line 3
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Law;->b:J

    .line 4
    const/4 v0, 0x0

    iput-object v0, p0, Law;->e:Landroid/animation/TimeInterpolator;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Law;->c:I

    .line 6
    const/4 v0, 0x1

    iput v0, p0, Law;->d:I

    .line 7
    iput-wide p1, p0, Law;->a:J

    .line 8
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Law;->b:J

    .line 9
    return-void
.end method

.method public constructor <init>(JJLandroid/animation/TimeInterpolator;)V
    .locals 3

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Law;->a:J

    .line 12
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Law;->b:J

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Law;->e:Landroid/animation/TimeInterpolator;

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Law;->c:I

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Law;->d:I

    .line 16
    iput-wide p1, p0, Law;->a:J

    .line 17
    iput-wide p3, p0, Law;->b:J

    .line 18
    iput-object p5, p0, Law;->e:Landroid/animation/TimeInterpolator;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()Landroid/animation/TimeInterpolator;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Law;->e:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Law;->e:Landroid/animation/TimeInterpolator;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lap;->a:Landroid/animation/TimeInterpolator;

    goto :goto_0
.end method

.method public final a(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 20
    .line 21
    iget-wide v0, p0, Law;->a:J

    .line 22
    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 24
    iget-wide v0, p0, Law;->b:J

    .line 25
    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 26
    invoke-virtual {p0}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 27
    instance-of v0, p1, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 28
    check-cast v0, Landroid/animation/ValueAnimator;

    .line 29
    iget v1, p0, Law;->c:I

    .line 30
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 31
    check-cast p1, Landroid/animation/ValueAnimator;

    .line 32
    iget v0, p0, Law;->d:I

    .line 33
    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 34
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 37
    const/4 v0, 0x1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 38
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 40
    check-cast p1, Law;

    .line 42
    iget-wide v2, p0, Law;->a:J

    .line 44
    iget-wide v4, p1, Law;->a:J

    .line 45
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 48
    iget-wide v2, p0, Law;->b:J

    .line 50
    iget-wide v4, p1, Law;->b:J

    .line 51
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 54
    iget v1, p0, Law;->c:I

    .line 56
    iget v2, p1, Law;->c:I

    .line 57
    if-ne v1, v2, :cond_0

    .line 60
    iget v1, p0, Law;->d:I

    .line 62
    iget v2, p1, Law;->d:I

    .line 63
    if-ne v1, v2, :cond_0

    .line 65
    invoke-virtual {p0}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 66
    .line 67
    iget-wide v0, p0, Law;->a:J

    .line 69
    iget-wide v2, p0, Law;->a:J

    .line 70
    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    .line 71
    mul-int/lit8 v0, v0, 0x1f

    .line 72
    iget-wide v2, p0, Law;->b:J

    .line 74
    iget-wide v4, p0, Law;->b:J

    .line 75
    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 76
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    mul-int/lit8 v0, v0, 0x1f

    .line 78
    iget v1, p0, Law;->c:I

    .line 79
    add-int/2addr v0, v1

    .line 80
    mul-int/lit8 v0, v0, 0x1f

    .line 81
    iget v1, p0, Law;->d:I

    .line 82
    add-int/2addr v0, v1

    .line 83
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, " delay: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-wide v2, p0, Law;->a:J

    .line 92
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 93
    const-string v1, " duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-wide v2, p0, Law;->b:J

    .line 96
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 97
    const-string v1, " interpolator: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {p0}, Law;->a()Landroid/animation/TimeInterpolator;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    const-string v1, " repeatCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget v1, p0, Law;->c:I

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    const-string v1, " repeatMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget v1, p0, Law;->d:I

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, "}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
