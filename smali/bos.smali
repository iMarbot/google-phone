.class public final Lbos;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"


# instance fields
.field public final c:Lbou;

.field public d:Z

.field public e:Landroid/view/View$OnClickListener;

.field public f:Landroid/view/View$OnClickListener;

.field private g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lbbf$a;

.field private k:Lbob;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbou;Lbob;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    iput-object p1, p0, Lbos;->g:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lbos;->c:Lbou;

    .line 4
    iput-object p3, p0, Lbos;->k:Lbob;

    .line 5
    return-void
.end method

.method private final c()V
    .locals 8

    .prologue
    .line 249
    new-instance v0, Lbpd;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    invoke-direct {v0, v1}, Lbpd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lbpd;->loadInBackground()Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    .line 250
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    :cond_0
    const-string v0, "SearchAdapter.logDirectories"

    const-string v3, "directory: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 252
    invoke-static {v2}, Lbpd;->a(Landroid/database/Cursor;)Lbpe;

    move-result-object v6

    aput-object v6, v4, v5

    .line 253
    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 256
    :goto_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 259
    :cond_1
    return-void

    .line 255
    :cond_2
    :try_start_1
    const-string v0, "SearchAdapter.logDirectories"

    const-string v3, "no directories found"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 258
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lbos;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbos;->d:Z

    if-nez v0, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 274
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0}, Lbou;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 26
    iget-object v2, p0, Lbos;->c:Lbou;

    .line 27
    invoke-virtual {v2}, Lbou;->a()I

    move-result v3

    .line 28
    if-lt p1, v3, :cond_0

    .line 29
    const-string v2, "Invalid position: %d, cursor count: %d"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 32
    :cond_0
    iget-object v4, v2, Lbou;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v3, v4

    if-lt p1, v3, :cond_2

    .line 33
    const/4 v0, 0x7

    .line 44
    :cond_1
    :goto_0
    return v0

    .line 34
    :cond_2
    invoke-virtual {v2, p1}, Lbou;->a(I)Lboc;

    move-result-object v3

    .line 35
    iget-object v4, v2, Lbou;->b:Lboc;

    if-ne v3, v4, :cond_3

    .line 36
    invoke-interface {v3}, Lboc;->a()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 37
    :cond_3
    sget-object v0, Lbou;->a:Lbov;

    if-ne v3, v0, :cond_4

    .line 38
    const/16 v0, 0x8

    goto :goto_0

    .line 39
    :cond_4
    iget-object v0, v2, Lbou;->c:Lboc;

    if-ne v3, v0, :cond_6

    .line 40
    invoke-interface {v3}, Lboc;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    goto :goto_0

    :cond_5
    const/4 v0, 0x4

    goto :goto_0

    .line 41
    :cond_6
    iget-object v0, v2, Lbou;->d:Lboc;

    if-ne v3, v0, :cond_8

    .line 42
    invoke-interface {v3}, Lboc;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x5

    goto :goto_0

    :cond_7
    const/4 v0, 0x6

    goto :goto_0

    .line 43
    :cond_8
    const-string v0, "No valid row type."

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 4

    .prologue
    const v2, 0x7f0400ad

    const/4 v3, 0x0

    .line 6
    packed-switch p2, :pswitch_data_0

    .line 25
    const/16 v0, 0x1c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid RowType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 7
    :pswitch_0
    new-instance v0, Lbod;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 8
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lbos;->k:Lbob;

    invoke-direct {v0, v1, v2}, Lbod;-><init>(Landroid/view/View;Lbob;)V

    .line 24
    :goto_0
    return-object v0

    .line 10
    :pswitch_1
    new-instance v0, Lbow;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 11
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbow;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 13
    :pswitch_2
    new-instance v0, Lboh;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 14
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04007a

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lboh;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 16
    :pswitch_3
    new-instance v0, Lbpa;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 17
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbpa;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 19
    :pswitch_4
    new-instance v0, Lbor;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 20
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400ab

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbor;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 22
    :pswitch_5
    new-instance v0, Lbot;

    iget-object v1, p0, Lbos;->g:Landroid/content/Context;

    .line 23
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040087

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lbos;->e:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lbos;->f:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, v3}, Lbot;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 6
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 10

    .prologue
    .line 45
    instance-of v0, p1, Lbod;

    if-eqz v0, :cond_10

    .line 46
    check-cast p1, Lbod;

    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, p2}, Lbou;->a(I)Lboc;

    move-result-object v4

    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    .line 47
    iget-object v2, p1, Lbod;->t:Landroid/content/Context;

    .line 48
    sget-object v0, Lbhj;->l:Lbhj;

    invoke-virtual {v0}, Lbhj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 50
    const/4 v3, 0x4

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 51
    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 52
    const/16 v6, 0x9

    .line 53
    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v8, 0x7

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 54
    invoke-static {v6, v7, v8}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 56
    invoke-virtual {v0, v5}, Lhbr$a;->i(Ljava/lang/String;)Lhbr$a;

    move-result-object v7

    const/4 v8, 0x5

    .line 57
    invoke-interface {v4, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lhbr$a;->h(J)Lhbr$a;

    move-result-object v7

    const/4 v8, 0x1

    .line 58
    invoke-virtual {v7, v8}, Lhbr$a;->l(I)Lhbr$a;

    move-result-object v7

    .line 59
    invoke-virtual {v7, v3}, Lhbr$a;->h(Ljava/lang/String;)Lhbr$a;

    move-result-object v7

    .line 60
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v8, 0x1

    .line 61
    invoke-interface {v4, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v9, 0x2

    .line 62
    invoke-interface {v4, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 63
    invoke-static {v2, v8, v9}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 64
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-virtual {v7, v2}, Lhbr$a;->k(Ljava/lang/String;)Lhbr$a;

    .line 66
    const/4 v2, 0x6

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 67
    if-eqz v2, :cond_0

    .line 68
    invoke-virtual {v0, v2}, Lhbr$a;->f(Ljava/lang/String;)Lhbr$a;

    .line 69
    :cond_0
    if-eqz v6, :cond_1

    .line 70
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhbr$a;->g(Ljava/lang/String;)Lhbr$a;

    .line 71
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 72
    invoke-virtual {v0, v5}, Lhbr$a;->j(Ljava/lang/String;)Lhbr$a;

    .line 73
    :cond_2
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbhj;

    .line 74
    iput-object v0, p1, Lbod;->w:Lbhj;

    .line 75
    invoke-interface {v4}, Lboc;->getPosition()I

    move-result v0

    iput v0, p1, Lbod;->u:I

    .line 76
    const/4 v0, 0x3

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lbod;->v:Ljava/lang/String;

    .line 77
    const/4 v0, 0x4

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 78
    iget-object v0, p1, Lbod;->t:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    const/4 v2, 0x1

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 80
    const/4 v3, 0x2

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 81
    if-nez v2, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    const-string v0, ""

    .line 85
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 86
    iget-object v0, p1, Lbod;->v:Ljava/lang/String;

    .line 88
    :goto_1
    iget-object v2, p1, Lbod;->q:Landroid/widget/TextView;

    iget-object v3, p1, Lbod;->t:Landroid/content/Context;

    invoke-static {v1, v7, v3}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v2, p1, Lbod;->r:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbib;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p1, Lbod;->t:Landroid/content/Context;

    .line 92
    const/16 v2, 0x8

    invoke-interface {v4, v2}, Lboc;->getInt(I)I

    move-result v2

    .line 93
    const/4 v3, 0x3

    invoke-interface {v4, v3}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 94
    and-int/lit8 v2, v2, 0x1

    const/4 v5, 0x1

    if-ne v2, v5, :cond_5

    .line 95
    const/4 v0, 0x1

    .line 112
    :goto_2
    iput v0, p1, Lbod;->x:I

    .line 113
    iget v0, p1, Lbod;->x:I

    packed-switch v0, :pswitch_data_0

    .line 135
    iget v0, p1, Lbod;->x:I

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid Call to action type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 83
    :cond_3
    invoke-static {v0, v2, v3}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 87
    :cond_4
    iget-object v2, p1, Lbod;->t:Landroid/content/Context;

    const v3, 0x7f1100af

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-object v6, p1, Lbod;->v:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 96
    :cond_5
    invoke-static {v0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v2

    invoke-virtual {v2}, Lbiu;->a()Lbis;

    move-result-object v2

    invoke-interface {v2, v0, v3}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 97
    const/4 v0, 0x2

    goto :goto_2

    .line 98
    :cond_6
    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 99
    invoke-interface {v2, v3}, Lbjf;->b(Ljava/lang/String;)Lbjb;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lbjb;->a()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 101
    const/4 v0, 0x3

    goto :goto_2

    .line 103
    :cond_7
    if-nez v0, :cond_a

    .line 104
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_9

    .line 105
    const/4 v0, 0x1

    .line 109
    :goto_3
    if-eqz v0, :cond_8

    .line 110
    invoke-interface {v2, v3}, Lbjf;->a(Ljava/lang/String;)V

    .line 111
    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 106
    :cond_9
    invoke-interface {v4}, Lboc;->getCount()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_a

    .line 107
    const/4 v0, 0x1

    goto :goto_3

    .line 108
    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    .line 114
    :pswitch_0
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :goto_4
    invoke-interface {v4}, Lboc;->getPosition()I

    move-result v0

    .line 138
    const/4 v1, 0x7

    invoke-interface {v4, v1}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    add-int/lit8 v2, v0, -0x1

    invoke-interface {v4, v2}, Lboc;->moveToPosition(I)Z

    .line 140
    invoke-interface {v4}, Lboc;->a()Z

    move-result v2

    if-nez v2, :cond_d

    invoke-interface {v4}, Lboc;->isBeforeFirst()Z

    move-result v2

    if-nez v2, :cond_d

    .line 141
    const/4 v2, 0x7

    invoke-interface {v4, v2}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-interface {v4, v0}, Lboc;->moveToPosition(I)Z

    .line 143
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    .line 146
    :goto_5
    if-eqz v0, :cond_f

    .line 147
    iget-object v0, p1, Lbod;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    iget-object v0, p1, Lbod;->p:Landroid/widget/QuickContactBadge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 149
    const/4 v0, 0x6

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-object v1, p1, Lbod;->t:Landroid/content/Context;

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbod;->p:Landroid/widget/QuickContactBadge;

    .line 152
    const/4 v3, 0x0

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 153
    const/4 v3, 0x7

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-static {v8, v9, v3}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 155
    const/4 v5, 0x5

    .line 156
    invoke-interface {v4, v5}, Lboc;->getLong(I)J

    move-result-wide v4

    .line 157
    if-nez v0, :cond_e

    const/4 v6, 0x0

    :goto_6
    const/4 v8, 0x1

    .line 158
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 248
    :cond_b
    :goto_7
    return-void

    .line 117
    :pswitch_1
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    iget-object v1, p1, Lbod;->t:Landroid/content/Context;

    const v2, 0x7f0200bc

    .line 119
    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 121
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    iget-object v1, p1, Lbod;->t:Landroid/content/Context;

    const v2, 0x7f110121

    .line 122
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 126
    :pswitch_2
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    iget-object v1, p1, Lbod;->t:Landroid/content/Context;

    const v2, 0x7f02017b

    .line 128
    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 129
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    iget-object v1, p1, Lbod;->t:Landroid/content/Context;

    const v2, 0x7f110122

    .line 131
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p1, Lbod;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 143
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 144
    :cond_d
    invoke-interface {v4, v0}, Lboc;->moveToPosition(I)Z

    .line 145
    const/4 v0, 0x1

    goto/16 :goto_5

    .line 157
    :cond_e
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_6

    .line 160
    :cond_f
    iget-object v0, p1, Lbod;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v0, p1, Lbod;->p:Landroid/widget/QuickContactBadge;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    goto :goto_7

    .line 163
    :cond_10
    instance-of v0, p1, Lbow;

    if-eqz v0, :cond_12

    .line 164
    check-cast p1, Lbow;

    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, p2}, Lbou;->a(I)Lboc;

    move-result-object v0

    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    .line 165
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lbow;->t:Ljava/lang/String;

    .line 166
    const/4 v2, 0x4

    invoke-interface {v0, v2}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 167
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 168
    iget-object v3, p1, Lbow;->q:Landroid/widget/TextView;

    iget-object v4, p1, Lbow;->p:Landroid/content/Context;

    invoke-static {v1, v7, v4}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v3, p1, Lbow;->r:Landroid/widget/TextView;

    iget-object v4, p1, Lbow;->p:Landroid/content/Context;

    invoke-static {v1, v2, v4}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    const/4 v1, 0x6

    invoke-interface {v0, v1}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 171
    iget-object v1, p1, Lbow;->p:Landroid/content/Context;

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbow;->s:Landroid/widget/QuickContactBadge;

    .line 172
    invoke-static {v0}, Lbow;->a(Lboc;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x5

    .line 173
    invoke-interface {v0, v4}, Lboc;->getLong(I)J

    move-result-wide v4

    .line 174
    if-nez v6, :cond_11

    const/4 v6, 0x0

    :goto_8
    const/4 v8, 0x2

    .line 175
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    goto/16 :goto_7

    .line 174
    :cond_11
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_8

    .line 177
    :cond_12
    instance-of v0, p1, Lbpa;

    if-eqz v0, :cond_18

    .line 178
    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, p2}, Lbou;->a(I)Lboc;

    move-result-object v0

    .line 179
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_14

    .line 180
    const-string v1, "SearchAdapter.onBindViewHolder"

    const-string v2, "cursor class: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 181
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 182
    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    const-string v1, "SearchAdapter.onBindViewHolder"

    const-string v2, "position: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    const-string v2, "SearchAdapter.onBindViewHolder"

    const-string v3, "query length: %s"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 185
    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    if-nez v1, :cond_13

    const-string v1, "null"

    :goto_9
    aput-object v1, v4, v5

    .line 186
    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    invoke-direct {p0}, Lbos;->c()V

    .line 188
    const-string v1, "SearchAdapter.onBindViewHolder"

    const-string v2, "directory id: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    check-cast v0, Lboc;

    .line 189
    invoke-interface {v0}, Lboc;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    .line 190
    invoke-static {v1, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null phone number reading remote contact"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_13
    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_9

    .line 192
    :cond_14
    check-cast p1, Lbpa;

    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, p2}, Lbou;->a(I)Lboc;

    move-result-object v4

    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    .line 193
    const/4 v0, 0x3

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lbpa;->t:Ljava/lang/String;

    .line 194
    const/4 v0, 0x4

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 195
    iget-object v0, p1, Lbpa;->p:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v4}, Lbpa;->a(Landroid/content/res/Resources;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 197
    iget-object v0, p1, Lbpa;->t:Ljava/lang/String;

    .line 199
    :goto_a
    iget-object v2, p1, Lbpa;->q:Landroid/widget/TextView;

    iget-object v3, p1, Lbpa;->p:Landroid/content/Context;

    invoke-static {v1, v7, v3}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v2, p1, Lbpa;->r:Landroid/widget/TextView;

    iget-object v3, p1, Lbpa;->p:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Lbib;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-static {v4}, Lbpa;->a(Lboc;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 202
    iget-object v0, p1, Lbpa;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    iget-object v0, p1, Lbpa;->s:Landroid/widget/QuickContactBadge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 204
    const/4 v0, 0x6

    invoke-interface {v4, v0}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    iget-object v1, p1, Lbpa;->p:Landroid/content/Context;

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbpa;->s:Landroid/widget/QuickContactBadge;

    .line 206
    invoke-static {v4}, Lbpa;->b(Lboc;)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x5

    .line 207
    invoke-interface {v4, v5}, Lboc;->getLong(I)J

    move-result-wide v4

    .line 208
    if-nez v0, :cond_16

    const/4 v6, 0x0

    :goto_b
    const/4 v8, 0x1

    .line 209
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    goto/16 :goto_7

    .line 198
    :cond_15
    iget-object v2, p1, Lbpa;->p:Landroid/content/Context;

    const v3, 0x7f1100af

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-object v6, p1, Lbpa;->t:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    .line 208
    :cond_16
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_b

    .line 211
    :cond_17
    iget-object v0, p1, Lbpa;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 212
    iget-object v0, p1, Lbpa;->s:Landroid/widget/QuickContactBadge;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    goto/16 :goto_7

    .line 213
    :cond_18
    instance-of v0, p1, Lboh;

    if-eqz v0, :cond_19

    .line 214
    iget-object v0, p0, Lbos;->c:Lbou;

    .line 215
    invoke-virtual {v0, p2}, Lbou;->a(I)Lboc;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    check-cast p1, Lboh;

    .line 217
    iget-object v1, p1, Lboh;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 218
    :cond_19
    instance-of v0, p1, Lbor;

    if-eqz v0, :cond_1b

    .line 219
    check-cast p1, Lbor;

    iget-object v0, p0, Lbos;->c:Lbou;

    .line 221
    iget-object v1, v0, Lbou;->e:Ljava/util/List;

    invoke-virtual {v0}, Lbou;->a()I

    move-result v2

    sub-int v2, p2, v2

    iget-object v0, v0, Lbou;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 223
    iget-object v0, p0, Lbos;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lbos;->h:Ljava/lang/String;

    :goto_c
    iget-object v2, p0, Lbos;->j:Lbbf$a;

    .line 225
    iput v1, p1, Lbor;->s:I

    .line 226
    iput p2, p1, Lbor;->t:I

    .line 227
    iput-object v0, p1, Lbor;->u:Ljava/lang/String;

    .line 228
    iput-object v2, p1, Lbor;->v:Lbbf$a;

    .line 229
    packed-switch v1, :pswitch_data_1

    .line 245
    const/16 v0, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid action: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 223
    :cond_1a
    iget-object v0, p0, Lbos;->i:Ljava/lang/String;

    goto :goto_c

    .line 230
    :pswitch_3
    iget-object v0, p1, Lbor;->r:Landroid/widget/TextView;

    const v1, 0x7f1102a2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    iget-object v0, p1, Lbor;->q:Landroid/widget/ImageView;

    const v1, 0x7f020160

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 233
    :pswitch_4
    iget-object v0, p1, Lbor;->r:Landroid/widget/TextView;

    const v1, 0x7f1102a5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 234
    iget-object v0, p1, Lbor;->q:Landroid/widget/ImageView;

    const v1, 0x7f020160

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 236
    :pswitch_5
    iget-object v0, p1, Lbor;->r:Landroid/widget/TextView;

    const v1, 0x7f1102a6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 237
    iget-object v0, p1, Lbor;->q:Landroid/widget/ImageView;

    const v1, 0x7f020179

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 239
    :pswitch_6
    iget-object v0, p1, Lbor;->r:Landroid/widget/TextView;

    const v1, 0x7f1102a8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 240
    iget-object v0, p1, Lbor;->q:Landroid/widget/ImageView;

    const v1, 0x7f020152

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 242
    :pswitch_7
    iget-object v1, p1, Lbor;->r:Landroid/widget/TextView;

    iget-object v2, p1, Lbor;->p:Landroid/content/Context;

    const v3, 0x7f1102a7

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p1, Lbor;->q:Landroid/widget/ImageView;

    const v1, 0x7f020167

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    .line 246
    :cond_1b
    instance-of v0, p1, Lbot;

    if-nez v0, :cond_b

    .line 247
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid ViewHolder: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 229
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lboc;)V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lbos;->c:Lbou;

    .line 261
    iget-object v1, v0, Lbou;->b:Lboc;

    if-ne p1, v1, :cond_1

    .line 262
    const/4 v0, 0x0

    .line 267
    :goto_0
    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lbos;->c:Lbou;

    iget-object v1, p0, Lbos;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbou;->a(Ljava/lang/String;)Z

    .line 270
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 271
    :cond_0
    return-void

    .line 263
    :cond_1
    if-eqz p1, :cond_2

    .line 264
    iput-object p1, v0, Lbou;->b:Lboc;

    .line 266
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 265
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Lbou;->b:Lboc;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lbbf$a;)V
    .locals 1

    .prologue
    .line 275
    iput-object p1, p0, Lbos;->h:Ljava/lang/String;

    .line 276
    iput-object p2, p0, Lbos;->i:Ljava/lang/String;

    .line 277
    iput-object p3, p0, Lbos;->j:Lbbf$a;

    .line 278
    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, p1}, Lbou;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 281
    :cond_0
    return-void
.end method

.method final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lbos;->c:Lbou;

    .line 283
    iget-object v1, v0, Lbou;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 284
    iput-object p1, v0, Lbou;->e:Ljava/util/List;

    .line 285
    const/4 v0, 0x1

    .line 287
    :goto_0
    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 290
    :cond_0
    return-void

    .line 286
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 302
    iput-object v0, p0, Lbos;->e:Landroid/view/View$OnClickListener;

    .line 303
    iput-object v0, p0, Lbos;->f:Landroid/view/View$OnClickListener;

    .line 304
    iget-object v0, p0, Lbos;->c:Lbou;

    invoke-virtual {v0, v1}, Lbou;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p0, v1}, Lbos;->e(I)V

    .line 306
    :cond_0
    return-void
.end method

.method public final b(Lboc;)V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lbos;->c:Lbou;

    .line 292
    iget-object v1, v0, Lbou;->c:Lboc;

    if-ne p1, v1, :cond_1

    .line 293
    const/4 v0, 0x0

    .line 298
    :goto_0
    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 301
    :cond_0
    return-void

    .line 294
    :cond_1
    if-eqz p1, :cond_2

    .line 295
    iput-object p1, v0, Lbou;->c:Lboc;

    .line 297
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 296
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Lbou;->c:Lboc;

    goto :goto_1
.end method

.method public final c(Lboc;)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lbos;->c:Lbou;

    .line 308
    iget-object v1, v0, Lbou;->d:Lboc;

    if-ne p1, v1, :cond_1

    .line 309
    const/4 v0, 0x0

    .line 314
    :goto_0
    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 317
    :cond_0
    return-void

    .line 310
    :cond_1
    if-eqz p1, :cond_2

    .line 311
    iput-object p1, v0, Lbou;->d:Lboc;

    .line 313
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 312
    :cond_2
    const/4 v1, 0x0

    iput-object v1, v0, Lbou;->d:Lboc;

    goto :goto_1
.end method
