.class public final Lbff;
.super Lcf;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final d:Lbfi;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbfi;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcf;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p3, p0, Lbff;->e:Ljava/util/List;

    .line 3
    iput-object p2, p0, Lbff;->d:Lbfi;

    .line 4
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbff;->setContentView(Landroid/view/View;)V

    .line 5
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfh;

    invoke-interface {v0}, Lbfh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lbff;->dismiss()V

    .line 51
    :cond_0
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 6
    invoke-super {p0, p1}, Lcf;->onCreate(Landroid/os/Bundle;)V

    .line 7
    const v0, 0x7f0e0224

    invoke-virtual {p0, v0}, Lbff;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 9
    invoke-virtual {p0}, Lbff;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 10
    const v2, 0x7f040037

    invoke-virtual {v1, v2, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 11
    iget-object v1, p0, Lbff;->d:Lbfi;

    invoke-virtual {v1}, Lbfi;->b()Lbfk;

    move-result-object v8

    .line 12
    invoke-virtual {p0}, Lbff;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    const v2, 0x7f0e00ef

    .line 13
    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/QuickContactBadge;

    .line 14
    invoke-virtual {v8}, Lbfk;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v8}, Lbfk;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 15
    :goto_0
    invoke-virtual {v8}, Lbfk;->a()J

    move-result-wide v4

    .line 16
    invoke-virtual {v8}, Lbfk;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v8}, Lbfk;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 17
    :goto_1
    invoke-virtual {v8}, Lbfk;->f()Ljava/lang/String;

    move-result-object v7

    .line 18
    invoke-virtual {v8}, Lbfk;->e()I

    move-result v8

    .line 19
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 20
    const v1, 0x7f0e015a

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 21
    const v2, 0x7f0e015b

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 22
    iget-object v3, p0, Lbff;->d:Lbfi;

    invoke-virtual {v3}, Lbfi;->c()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 23
    iget-object v1, p0, Lbff;->d:Lbfi;

    invoke-virtual {v1}, Lbfi;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 24
    iget-object v1, p0, Lbff;->d:Lbfi;

    invoke-virtual {v1}, Lbfi;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    :goto_2
    iget-object v1, p0, Lbff;->d:Lbfi;

    invoke-virtual {v1}, Lbfi;->e()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 28
    new-instance v1, Lbfg;

    invoke-direct {v1, p0}, Lbfg;-><init>(Lbff;)V

    invoke-virtual {v10, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    :cond_0
    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 31
    iget-object v1, p0, Lbff;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lbfh;

    .line 32
    instance-of v1, v2, Lbfm;

    if-eqz v1, :cond_4

    .line 34
    invoke-virtual {p0}, Lbff;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 35
    const v2, 0x7f040059

    invoke-virtual {v1, v2, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_1
    move-object v3, v9

    .line 14
    goto/16 :goto_0

    :cond_2
    move-object v6, v9

    .line 16
    goto :goto_1

    .line 25
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 26
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 38
    :cond_4
    invoke-virtual {p0}, Lbff;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 39
    const v4, 0x7f040089

    invoke-virtual {v1, v4, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 40
    const v1, 0x7f0e0212

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v2}, Lbfh;->a()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 41
    const v1, 0x7f0e0211

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 42
    invoke-interface {v2}, Lbfh;->b()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 43
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 48
    :cond_5
    return-void
.end method
