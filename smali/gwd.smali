.class public final Lgwd;
.super Lhft;
.source "PG"


# instance fields
.field private a:I

.field private b:Lgvz;

.field private c:Lgwa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    iput v0, p0, Lgwd;->a:I

    .line 10
    iput v0, p0, Lgwd;->a:I

    .line 11
    iput-object v1, p0, Lgwd;->b:Lgvz;

    .line 12
    iput v0, p0, Lgwd;->a:I

    .line 13
    iput-object v1, p0, Lgwd;->c:Lgwa;

    .line 14
    iput-object v1, p0, Lgwd;->unknownFieldData:Lhfv;

    .line 15
    iput v0, p0, Lgwd;->cachedSize:I

    .line 16
    return-void
.end method


# virtual methods
.method public final a()Lgvz;
    .locals 1

    .prologue
    .line 1
    iget v0, p0, Lgwd;->a:I

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lgwd;->b:Lgvz;

    .line 3
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lgwa;
    .locals 2

    .prologue
    .line 4
    iget v0, p0, Lgwd;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 5
    iget-object v0, p0, Lgwd;->c:Lgwa;

    .line 6
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 24
    iget v1, p0, Lgwd;->a:I

    if-nez v1, :cond_0

    .line 25
    const/4 v1, 0x3

    iget-object v2, p0, Lgwd;->b:Lgvz;

    .line 26
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_0
    iget v1, p0, Lgwd;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 28
    const/4 v1, 0x4

    iget-object v2, p0, Lgwd;->c:Lgwa;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 31
    .line 32
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 33
    sparse-switch v0, :sswitch_data_0

    .line 35
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    :sswitch_0
    return-object p0

    .line 37
    :sswitch_1
    iget-object v0, p0, Lgwd;->b:Lgvz;

    if-nez v0, :cond_1

    .line 38
    new-instance v0, Lgvz;

    invoke-direct {v0}, Lgvz;-><init>()V

    iput-object v0, p0, Lgwd;->b:Lgvz;

    .line 39
    :cond_1
    iget-object v0, p0, Lgwd;->b:Lgvz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lgwd;->a:I

    goto :goto_0

    .line 42
    :sswitch_2
    iget-object v0, p0, Lgwd;->c:Lgwa;

    if-nez v0, :cond_2

    .line 43
    new-instance v0, Lgwa;

    invoke-direct {v0}, Lgwa;-><init>()V

    iput-object v0, p0, Lgwd;->c:Lgwa;

    .line 44
    :cond_2
    iget-object v0, p0, Lgwd;->c:Lgwa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lgwd;->a:I

    goto :goto_0

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 17
    iget v0, p0, Lgwd;->a:I

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x3

    iget-object v1, p0, Lgwd;->b:Lgvz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    iget v0, p0, Lgwd;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 20
    const/4 v0, 0x4

    iget-object v1, p0, Lgwd;->c:Lgwa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 22
    return-void
.end method
