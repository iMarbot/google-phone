.class public final Lfss;
.super Lfta;
.source "PG"


# instance fields
.field public final context:Landroid/content/Context;

.field public final requestSender:Lfmv;


# direct methods
.method constructor <init>(JLjava/lang/String;[BILfmv;Ljava/lang/String;Lfsr;Landroid/content/Context;)V
    .locals 13

    .prologue
    .line 1
    const/4 v9, 0x0

    move-object v3, p0

    move-wide v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v3 .. v11}, Lfta;-><init>(JLjava/lang/String;[BILgga;Ljava/lang/String;Lfsr;)V

    .line 3
    const-string v2, "Expected non-null"

    move-object/from16 v0, p6

    invoke-static {v2, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    move-object/from16 v0, p9

    iput-object v0, p0, Lfss;->context:Landroid/content/Context;

    .line 6
    move-object/from16 v0, p6

    iput-object v0, p0, Lfss;->requestSender:Lfmv;

    .line 7
    return-void
.end method


# virtual methods
.method public final bridge synthetic doInBackgroundTimed()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lfss;->doInBackgroundTimed()[B

    move-result-object v0

    return-object v0
.end method

.method public final doInBackgroundTimed()[B
    .locals 5

    .prologue
    .line 8
    :try_start_0
    iget-object v0, p0, Lfss;->requestSender:Lfmv;

    iget-object v1, p0, Lfss;->apiaryUri:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfss;->path:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    :goto_0
    invoke-virtual {p0}, Lfss;->getHttpHeaders()Ljava/util/Map;

    .line 10
    invoke-interface {v0}, Lfmv;->a()[B

    move-result-object v0

    .line 13
    :goto_1
    return-object v0

    .line 8
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    iget-object v1, p0, Lfss;->apiaryUri:Ljava/lang/String;

    iget-object v2, p0, Lfss;->path:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error sending cronet request: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final getHttpHeaders()Ljava/util/Map;
    .locals 5

    .prologue
    .line 16
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 17
    const-string v2, "Authorization"

    const-string v3, "Bearer "

    iget-object v0, p0, Lfss;->authToken:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string v0, "X-Auth-Time"

    iget-object v2, p0, Lfss;->authTime:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    const-string v0, "User-Agent"

    const/4 v2, 0x0

    invoke-static {v2}, Lfst;->makeUserAgent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    return-object v1

    .line 17
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lfss;->onPostExecute([B)V

    return-void
.end method

.method public final onPostExecute([B)V
    .locals 0

    .prologue
    .line 14
    invoke-super {p0, p1}, Lfta;->onPostExecute([B)V

    .line 15
    return-void
.end method

.method public final bridge synthetic onPreExecute()V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Lfta;->onPreExecute()V

    return-void
.end method

.method public final bridge synthetic setAuthToken(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lfta;->setAuthToken(Ljava/lang/String;J)V

    return-void
.end method
