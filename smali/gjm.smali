.class public final Lgjm;
.super Lhft;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgjm$a;
    }
.end annotation


# instance fields
.field public a:Lgjm$a;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgjm;->b:Ljava/lang/String;

    .line 4
    iput-object v0, p0, Lgjm;->c:Ljava/lang/String;

    .line 5
    iput-object v0, p0, Lgjm;->d:Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lgjm;->e:Ljava/lang/Integer;

    .line 7
    iput-object v0, p0, Lgjm;->f:Ljava/lang/Integer;

    .line 8
    iput-object v0, p0, Lgjm;->a:Lgjm$a;

    .line 9
    iput-object v0, p0, Lgjm;->unknownFieldData:Lhfv;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lgjm;->cachedSize:I

    .line 11
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 26
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 27
    iget-object v1, p0, Lgjm;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28
    const/4 v1, 0x1

    iget-object v2, p0, Lgjm;->b:Ljava/lang/String;

    .line 29
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_0
    iget-object v1, p0, Lgjm;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 31
    const/4 v1, 0x2

    iget-object v2, p0, Lgjm;->c:Ljava/lang/String;

    .line 32
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_1
    iget-object v1, p0, Lgjm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 34
    const/4 v1, 0x3

    iget-object v2, p0, Lgjm;->d:Ljava/lang/String;

    .line 35
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_2
    iget-object v1, p0, Lgjm;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 37
    const/4 v1, 0x4

    iget-object v2, p0, Lgjm;->e:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_3
    iget-object v1, p0, Lgjm;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 40
    const/4 v1, 0x5

    iget-object v2, p0, Lgjm;->f:Ljava/lang/Integer;

    .line 41
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_4
    iget-object v1, p0, Lgjm;->a:Lgjm$a;

    if-eqz v1, :cond_5

    .line 43
    const/4 v1, 0x6

    iget-object v2, p0, Lgjm;->a:Lgjm$a;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 46
    .line 47
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 48
    sparse-switch v0, :sswitch_data_0

    .line 50
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    :sswitch_0
    return-object p0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjm;->b:Ljava/lang/String;

    goto :goto_0

    .line 54
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjm;->c:Ljava/lang/String;

    goto :goto_0

    .line 56
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjm;->d:Ljava/lang/String;

    goto :goto_0

    .line 59
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 60
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 63
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjm;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 66
    :sswitch_6
    iget-object v0, p0, Lgjm;->a:Lgjm$a;

    if-nez v0, :cond_1

    .line 67
    new-instance v0, Lgjm$a;

    invoke-direct {v0}, Lgjm$a;-><init>()V

    iput-object v0, p0, Lgjm;->a:Lgjm$a;

    .line 68
    :cond_1
    iget-object v0, p0, Lgjm;->a:Lgjm$a;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lgjm;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v1, p0, Lgjm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lgjm;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 15
    const/4 v0, 0x2

    iget-object v1, p0, Lgjm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 16
    :cond_1
    iget-object v0, p0, Lgjm;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 17
    const/4 v0, 0x3

    iget-object v1, p0, Lgjm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 18
    :cond_2
    iget-object v0, p0, Lgjm;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 19
    const/4 v0, 0x4

    iget-object v1, p0, Lgjm;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 20
    :cond_3
    iget-object v0, p0, Lgjm;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 21
    const/4 v0, 0x5

    iget-object v1, p0, Lgjm;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 22
    :cond_4
    iget-object v0, p0, Lgjm;->a:Lgjm$a;

    if-eqz v0, :cond_5

    .line 23
    const/4 v0, 0x6

    iget-object v1, p0, Lgjm;->a:Lgjm$a;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 24
    :cond_5
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 25
    return-void
.end method
