.class public final Lfpr;
.super Landroid/os/Handler;
.source "PG"


# static fields
.field public static final APIARY_REQUEST_OP:I = 0x7

.field public static final AUDIO_LEVELS_OP:I = 0x6

.field public static final CALL_ERROR_OP:I = 0x1

.field public static final CLOUD_HANDOFF_COMPLETED_OP:I = 0xc

.field public static final CONFERENCE_UPDATE_OP:I = 0x8

.field public static final CONNECTION_QUALITY_UPDATE_OP:I = 0xf

.field public static final ENDPOINT_EVENT_OP:I = 0x4

.field public static final FIRST_PACKET_RECEIVED_OP:I = 0xa

.field public static final MEDIA_STATE_CHANGED_OP:I = 0x2

.field public static final P2P_HANDOFF_COMPLETED_OP:I = 0xb

.field public static final REMOTE_SESSION_CONNECTED_OP:I = 0x9

.field public static final REPORT_CLIENT_DATA_MESSAGE_OP:I = 0x15

.field public static final REPORT_IMPRESSION_OP:I = 0x12

.field public static final REPORT_MEETINGS_PUSH_OP:I = 0x14

.field public static final REPORT_TIMING_OP:I = 0x16

.field public static final REPORT_TRANSPORT_EVENT_OP:I = 0x13

.field public static final REQUEST_TERMINATE_OP:I = 0xd

.field public static final SIGNIN_STATE_CHANGED_OP:I = 0x0

.field public static final STATS_OP:I = 0x3

.field public static final STREAM_REQUEST_RECEIVED_OP:I = 0xe

.field public static final UMA_EVENT_OP:I = 0x11

.field public static final VIDEO_SOURCES_OP:I = 0x5

.field public static final VOLUME_LEVEL_UPDATE_OP:I = 0x10


# instance fields
.field public callback:Lfpq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 112
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 113
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 4
    return-void
.end method

.method static getEndpointEventTypeName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    packed-switch p0, :pswitch_data_0

    .line 110
    const-string v0, "Unknown type"

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 111
    const-string v0, "Unknown type"

    :goto_0
    return-object v0

    .line 102
    :pswitch_0
    const-string v0, "LOCAL_ENDPOINT_ENTERED"

    goto :goto_0

    .line 103
    :pswitch_1
    const-string v0, "REMOTE_ENDPOINT_ENTERED"

    goto :goto_0

    .line 104
    :pswitch_2
    const-string v0, "ENDPOINT_EXITED"

    goto :goto_0

    .line 105
    :pswitch_3
    const-string v0, "ENDPOINT_CHANGED"

    goto :goto_0

    .line 106
    :pswitch_4
    const-string v0, "ENDPOINT_AUDIO_MUTE_STATE_CHANGED"

    goto :goto_0

    .line 107
    :pswitch_5
    const-string v0, "ENDPOINT_VIDEO_MUTE_STATE_CHANGED"

    goto :goto_0

    .line 108
    :pswitch_6
    const-string v0, "ENDPOINT_REMOTE_AUDIO_MUTE_REQUESTED"

    goto :goto_0

    .line 109
    :pswitch_7
    const-string v0, "REMOTE_ENDPOINT_CROPPABLE_CHANGED"

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 7
    iget-object v0, p0, Lfpr;->callback:Lfpq;

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 10
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 98
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unknown message type "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 11
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_2

    move v0, v1

    .line 12
    :goto_1
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v1, v0}, Lfpq;->handleSignedInStateUpdate(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 11
    goto :goto_1

    .line 14
    :pswitch_1
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 15
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v1, v0}, Lfpq;->handleRemoteSessionConnected(Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :pswitch_2
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget v2, p1, Landroid/os/Message;->arg1:I

    const-string v3, "str1"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lfpq;->handleCallEnd(ILjava/lang/String;)V

    goto :goto_0

    .line 19
    :pswitch_3
    iget-object v0, p0, Lfpr;->callback:Lfpq;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lfpq;->handleFirstPacketReceived(I)V

    goto :goto_0

    .line 21
    :pswitch_4
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 22
    const-string v2, "str1"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 23
    const-string v3, "str2"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-static {v1}, Lfoe;->getMediaStateName(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "handleMessage(MEDIA_STATE_CHANGED): for sessionId: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new state="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 26
    invoke-static {v3}, Lfvh;->logd(Ljava/lang/String;)V

    .line 27
    iget-object v3, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v3, v0, v1, v2}, Lfpq;->handleMediaStateChanged(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 29
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Stats;

    .line 30
    iget-object v3, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v3, v0}, Lfpq;->handleUnloggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 31
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    .line 32
    :goto_2
    if-eqz v1, :cond_3

    .line 33
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v2, v0}, Lfpq;->handleLoggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 34
    :cond_3
    instance-of v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;

    if-eqz v0, :cond_0

    .line 35
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;

    invoke-direct {v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;-><init>()V

    .line 36
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v2, v0}, Lfpq;->handleUnloggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    .line 37
    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v1, v0}, Lfpq;->handleLoggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 31
    goto :goto_2

    .line 40
    :pswitch_6
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 42
    iget v4, p1, Landroid/os/Message;->arg2:I

    .line 43
    const-string v2, "str2"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    const-string v5, "str3"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 45
    const-string v6, "str4"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 46
    const-string v7, "str5"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 47
    iget-object v0, p0, Lfpr;->callback:Lfpq;

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, [B

    invoke-interface/range {v0 .. v8}, Lfpq;->handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 49
    :pswitch_7
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/MediaSources;

    invoke-interface {v2, v1, v0}, Lfpq;->handleVideoSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/internal/MediaSources;)V

    goto/16 :goto_0

    .line 52
    :pswitch_8
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    const-string v3, "str2"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lfpq;->handleLoudestSpeakerUpdate(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 55
    :pswitch_9
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    const-string v2, "str1"

    .line 56
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "str2"

    .line 57
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, [B

    iget v6, p1, Landroid/os/Message;->arg1:I

    .line 58
    invoke-interface/range {v1 .. v6}, Lfpq;->makeApiaryRequest(JLjava/lang/String;[BI)V

    goto/16 :goto_0

    .line 60
    :pswitch_a
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v2, v3, v4, v1, v0}, Lfpq;->handleConferenceUpdate(IILjava/lang/String;[B)V

    goto/16 :goto_0

    .line 63
    :pswitch_b
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v1, v0}, Lfpq;->handleP2pHandoffCompleted([B)V

    goto/16 :goto_0

    .line 65
    :pswitch_c
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v1, v0}, Lfpq;->handleCloudHandoffCompleted([B)V

    goto/16 :goto_0

    .line 67
    :pswitch_d
    iget-object v0, p0, Lfpr;->callback:Lfpq;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lfpq;->handleRequestTerminate(I)V

    goto/16 :goto_0

    .line 69
    :pswitch_e
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v1, v0}, Lfpq;->handleStreamRequest([B)V

    goto/16 :goto_0

    .line 71
    :pswitch_f
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v1, v0}, Lfpq;->handleConnectionQualityUpdate([B)V

    goto/16 :goto_0

    .line 73
    :pswitch_10
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget v2, p1, Landroid/os/Message;->arg1:I

    const-string v3, "str1"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lfpq;->handleVolumeLevelUpdate(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 75
    :pswitch_11
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 77
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 78
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v4, v5, v1, v3}, Lfpq;->handleUmaEvent(JII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 81
    :catch_0
    move-exception v1

    const-string v1, "MD5 not available for logging UMA event: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 83
    :pswitch_12
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 84
    const-string v2, "str1"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    invoke-interface {v2, v1, v0}, Lfpq;->reportImpression(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 87
    :pswitch_13
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, Lfpq;->reportTransportEvent([BJ)V

    goto/16 :goto_0

    .line 90
    :pswitch_14
    iget-object v1, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v1, v0}, Lfpq;->handleMeetingsPush([B)V

    goto/16 :goto_0

    .line 92
    :pswitch_15
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v2, v1, v0}, Lfpq;->handleClientDataMessage(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 95
    :pswitch_16
    const-string v1, "str1"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    iget-object v2, p0, Lfpr;->callback:Lfpq;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-interface {v2, v0, v1}, Lfpq;->handleTimingLogEntry([BLjava/lang/String;)V

    goto/16 :goto_0

    .line 10
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_3
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public final setCallback(Lfpq;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lfpr;->callback:Lfpq;

    .line 6
    return-void
.end method
