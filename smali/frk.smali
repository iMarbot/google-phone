.class final Lfrk;
.super Lfnl;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfri;


# direct methods
.method constructor <init>(Lfri;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrk;->this$0:Lfri;

    invoke-direct {p0}, Lfnl;-><init>()V

    return-void
.end method


# virtual methods
.method public final onModified(Lgnm;Lgnm;)V
    .locals 7

    .prologue
    .line 2
    iget-object v0, p2, Lgnm;->participantId:Ljava/lang/String;

    iget-object v1, p0, Lfrk;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$100(Lfri;)Lfrh;

    move-result-object v1

    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lgnm;->blockedUser:[Lgnn;

    array-length v0, v0

    iget-object v1, p2, Lgnm;->blockedUser:[Lgnn;

    array-length v1, v1

    if-eq v0, v1, :cond_2

    .line 3
    iget-object v0, p0, Lfrk;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$200(Lfri;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 4
    iget-object v3, p2, Lgnm;->blockedUser:[Lgnn;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 5
    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v6

    .line 6
    iget-object v6, v6, Lfvz;->g:Ljava/lang/String;

    .line 7
    iget-object v5, v5, Lgnn;->userId:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 8
    invoke-virtual {v0}, Lfrh;->markDirty()V

    .line 9
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11
    :cond_2
    return-void
.end method

.method public final bridge synthetic onModified(Lhfz;Lhfz;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lgnm;

    check-cast p2, Lgnm;

    invoke-virtual {p0, p1, p2}, Lfrk;->onModified(Lgnm;Lgnm;)V

    return-void
.end method
