.class public Lcsd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public final c:I

.field public final d:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 8
    const/16 v0, 0x9c4

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1, v2}, Lcsd;-><init>(IIF)V

    .line 9
    return-void
.end method

.method public constructor <init>(IIF)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/16 v0, 0x9c4

    iput v0, p0, Lcsd;->a:I

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Lcsd;->c:I

    .line 13
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcsd;->d:F

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1
    iget v0, p0, Lcsd;->a:I

    return v0
.end method

.method public a(Lcse;)V
    .locals 3

    .prologue
    .line 3
    iget v0, p0, Lcsd;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcsd;->b:I

    .line 4
    iget v0, p0, Lcsd;->a:I

    int-to-float v0, v0

    iget v1, p0, Lcsd;->a:I

    int-to-float v1, v1

    iget v2, p0, Lcsd;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcsd;->a:I

    .line 5
    invoke-virtual {p0}, Lcsd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    throw p1

    .line 7
    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lcsd;->b:I

    return v0
.end method

.method protected c()Z
    .locals 2

    .prologue
    .line 15
    iget v0, p0, Lcsd;->b:I

    iget v1, p0, Lcsd;->c:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
