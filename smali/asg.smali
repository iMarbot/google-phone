.class public Lasg;
.super Laig;
.source "PG"


# instance fields
.field private n:I

.field public o:Lcom/android/dialer/widget/EmptyContentView;

.field public p:Lart;

.field public q:Landroid/view/View$OnTouchListener;

.field public r:Ljava/lang/String;

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Landroid/widget/Space;

.field private x:Lask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Laig;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 56
    .line 57
    invoke-virtual {p0}, Lasg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1100ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    if-eqz p1, :cond_1

    .line 59
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    const-string v1, "SearchFragment.checkForProhibitedPhoneNumber"

    const-string v2, "the phone number is prohibited explicitly by a rule"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    const v1, 0x7f11014d

    .line 66
    new-instance v2, Lbhy;

    invoke-direct {v2}, Lbhy;-><init>()V

    .line 67
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 68
    const-string v4, "argTitleResId"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    const-string v0, "argMessageResId"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    invoke-virtual {v2, v3}, Lbhy;->setArguments(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lasg;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "phone_prohibited_dialog"

    invoke-virtual {v2, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 74
    :cond_0
    const/4 v0, 0x1

    .line 75
    :cond_1
    return v0
.end method


# virtual methods
.method protected a()Lahd;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Larh;

    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Larh;-><init>(Landroid/content/Context;)V

    .line 78
    const/4 v1, 0x1

    iput-boolean v1, v0, Lahd;->f:Z

    .line 80
    iget-boolean v1, p0, Laig;->k:Z

    .line 82
    iput-boolean v1, v0, Laif;->w:Z

    .line 84
    iput-object p0, v0, Laif;->x:Laif$a;

    .line 85
    return-object v0
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 221
    invoke-super {p0, p1, p2}, Laig;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 222
    invoke-virtual {p0}, Lasg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 223
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 224
    new-instance v1, Landroid/widget/Space;

    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lasg;->w:Landroid/widget/Space;

    .line 225
    iget-object v1, p0, Lasg;->w:Landroid/widget/Space;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    :cond_0
    return-object v0
.end method

.method protected a(IJ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 86
    .line 87
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 88
    check-cast v0, Larh;

    .line 89
    invoke-virtual {v0, p1}, Larh;->m(I)I

    move-result v2

    .line 90
    const-string v3, "SearchFragment.onItemClick"

    const/16 v4, 0x19

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "shortcutType: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    packed-switch v2, :pswitch_data_0

    .line 158
    invoke-super {p0, p1, p2, p3}, Laig;->a(IJ)V

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 93
    :pswitch_0
    iget-object v2, v0, Lahd;->l:Ljava/lang/String;

    .line 96
    iget-object v3, p0, Laig;->j:Laie;

    .line 98
    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, Lasg;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 101
    invoke-virtual {p0, v1}, Lasg;->h(Z)Lbbf$a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 102
    invoke-virtual {v0, p1}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v4

    .line 104
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 105
    if-nez v0, :cond_1

    move v0, v1

    .line 108
    :goto_1
    invoke-virtual {v4, v0}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 110
    invoke-interface {v3, v2, v1, v0}, Laie;->a(Ljava/lang/String;ZLbbj;)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 107
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    .line 112
    :pswitch_1
    instance-of v1, p0, Lasm;

    if-eqz v1, :cond_2

    .line 113
    invoke-virtual {p0}, Lasg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->cA:Lbkq$a;

    .line 114
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 115
    :cond_2
    iget-object v1, p0, Lasg;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 116
    invoke-virtual {v0}, Larh;->e()Ljava/lang/String;

    move-result-object v0

    .line 118
    :goto_2
    invoke-static {v0}, Lbib;->b(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 119
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 117
    :cond_3
    iget-object v0, p0, Lasg;->r:Ljava/lang/String;

    goto :goto_2

    .line 121
    :pswitch_2
    instance-of v1, p0, Lasm;

    if-eqz v1, :cond_4

    .line 122
    invoke-virtual {p0}, Lasg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->cE:Lbkq$a;

    .line 123
    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 124
    :cond_4
    iget-object v1, p0, Lasg;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 125
    invoke-virtual {v0}, Larh;->e()Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_3
    invoke-static {v0}, Lbib;->c(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 129
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f110036

    .line 130
    invoke-static {v1, v0, v2}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 126
    :cond_5
    iget-object v0, p0, Lasg;->r:Ljava/lang/String;

    goto :goto_3

    .line 132
    :pswitch_3
    iget-object v1, p0, Lasg;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 133
    invoke-virtual {v0}, Larh;->e()Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_4
    invoke-static {v0}, Lbib;->a(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 134
    :cond_6
    iget-object v0, p0, Lasg;->r:Ljava/lang/String;

    goto :goto_4

    .line 138
    :pswitch_4
    iget-object v2, p0, Lasg;->r:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 139
    iget-object v0, v0, Lahd;->l:Ljava/lang/String;

    move-object v2, v0

    .line 142
    :goto_5
    iget-object v3, p0, Laig;->j:Laie;

    .line 144
    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, Lasg;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 147
    invoke-virtual {p0, v1}, Lasg;->h(Z)Lbbf$a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 148
    invoke-virtual {v0, p1}, Lhbr$a;->e(I)Lhbr$a;

    move-result-object v0

    .line 150
    iget-object v4, p0, Lahe;->e:Ljava/lang/String;

    .line 151
    if-nez v4, :cond_8

    .line 154
    :goto_6
    invoke-virtual {v0, v1}, Lhbr$a;->f(I)Lhbr$a;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 156
    const/4 v1, 0x1

    invoke-interface {v3, v2, v1, v0}, Laie;->a(Ljava/lang/String;ZLbbj;)V

    goto/16 :goto_0

    .line 140
    :cond_7
    iget-object v0, p0, Lasg;->r:Ljava/lang/String;

    move-object v2, v0

    goto :goto_5

    .line 152
    :cond_8
    iget-object v1, p0, Lahe;->e:Ljava/lang/String;

    .line 153
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_6

    .line 91
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public g()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 191
    iget-object v0, p0, Lasg;->w:Landroid/widget/Space;

    if-nez v0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->x()I

    move-result v0

    move v1, v0

    .line 194
    :goto_1
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v3, p0, Lasg;->w:Landroid/widget/Space;

    .line 195
    invoke-virtual {v3}, Landroid/widget/Space;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    .line 196
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lasg;->x:Lask;

    .line 197
    invoke-interface {v3}, Lask;->w()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lasg;->x:Lask;

    .line 198
    invoke-interface {v3}, Lask;->x()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 199
    iget-object v0, p0, Lasg;->w:Landroid/widget/Space;

    invoke-virtual {v0}, Landroid/widget/Space;->getHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 200
    iget-object v0, p0, Lasg;->w:Landroid/widget/Space;

    invoke-virtual {v0}, Landroid/widget/Space;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 201
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 202
    iget-object v1, p0, Lasg;->w:Landroid/widget/Space;

    invoke-virtual {v1, v0}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 193
    goto :goto_1
.end method

.method public final i(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 160
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 161
    iget-object v0, p0, Lasg;->x:Lask;

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lasg;->n:I

    iget v2, p0, Lasg;->s:I

    sub-int/2addr v0, v2

    move v4, v0

    .line 165
    :goto_1
    if-nez p1, :cond_1

    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->v()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 166
    :cond_1
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->w()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    move v3, v0

    .line 167
    :goto_3
    if-eqz p1, :cond_6

    .line 168
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->w()Z

    move-result v5

    .line 169
    if-eqz v5, :cond_4

    sget-object v0, Lamn;->a:Landroid/view/animation/Interpolator;

    move-object v2, v0

    .line 170
    :goto_4
    if-eqz v5, :cond_5

    iget v0, p0, Lasg;->u:I

    .line 171
    :goto_5
    invoke-virtual {p0}, Lasg;->getView()Landroid/view/View;

    move-result-object v6

    int-to-float v4, v4

    invoke-virtual {v6, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 172
    invoke-virtual {p0}, Lasg;->getView()Landroid/view/View;

    move-result-object v4

    .line 173
    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    int-to-float v3, v3

    .line 174
    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    .line 175
    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-long v6, v0

    .line 176
    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, Lasj;

    invoke-direct {v2, p0, v5}, Lasj;-><init>(Lasg;Z)V

    .line 177
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 181
    :goto_6
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->w()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 183
    :goto_7
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 186
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingStart()I

    move-result v2

    .line 187
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingEnd()I

    move-result v3

    .line 188
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v4

    .line 189
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/ListView;->setPaddingRelative(IIII)V

    goto :goto_0

    .line 163
    :cond_2
    iget v0, p0, Lasg;->s:I

    neg-int v0, v0

    move v4, v0

    goto :goto_1

    .line 166
    :cond_3
    iget v0, p0, Lasg;->n:I

    iget v2, p0, Lasg;->s:I

    sub-int/2addr v0, v2

    goto :goto_2

    .line 169
    :cond_4
    sget-object v0, Lamn;->b:Landroid/view/animation/Interpolator;

    move-object v2, v0

    goto :goto_4

    .line 170
    :cond_5
    iget v0, p0, Lasg;->v:I

    goto :goto_5

    .line 179
    :cond_6
    invoke-virtual {p0}, Lasg;->getView()Landroid/view/View;

    move-result-object v0

    int-to-float v2, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 180
    invoke-virtual {p0}, Lasg;->h()V

    goto :goto_6

    .line 181
    :cond_7
    iget v1, p0, Lasg;->t:I

    goto :goto_7

    :cond_8
    move v3, v1

    goto :goto_3
.end method

.method protected final i_()V
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-super {p0}, Lahe;->i_()V

    .line 219
    :goto_1
    invoke-virtual {p0}, Lasg;->g()V

    goto :goto_0

    .line 209
    :cond_1
    iget-object v0, p0, Lahe;->e:Ljava/lang/String;

    .line 210
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 213
    check-cast v0, Larh;

    .line 214
    invoke-virtual {v0}, Larh;->d()V

    goto :goto_1

    .line 217
    :cond_2
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 218
    invoke-virtual {v0}, Lahd;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2
    invoke-super {p0, p1}, Laig;->onAttach(Landroid/app/Activity;)V

    .line 4
    iput-boolean v2, p0, Lahe;->b:Z

    .line 6
    iput-boolean v1, p0, Lahe;->c:Z

    .line 7
    invoke-virtual {p0, v1}, Lasg;->f(Z)V

    .line 9
    iput-boolean v2, p0, Laig;->k:Z

    .line 10
    :try_start_0
    move-object v0, p1

    check-cast v0, Lart;

    move-object v1, v0

    iput-object v1, p0, Lasg;->p:Lart;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    :goto_0
    return-void

    .line 13
    :catch_0
    move-exception v1

    .line 14
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " doesn\'t implement OnListFragmentScrolledListener. Ignoring."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateAnimator(IZI)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 49
    if-eqz p3, :cond_0

    .line 50
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    .line 51
    :cond_0
    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lasg;->getView()Landroid/view/View;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Landroid/view/View;->getLayerType()I

    move-result v2

    .line 54
    new-instance v3, Lasi;

    invoke-direct {v3, v1, v2}, Lasi;-><init>(Landroid/view/View;I)V

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 55
    :cond_1
    return-object v0
.end method

.method public onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 16
    invoke-super {p0}, Laig;->onStart()V

    .line 17
    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lask;

    iput-object v0, p0, Lasg;->x:Lask;

    .line 18
    invoke-virtual {p0}, Lasg;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 19
    iget-object v0, p0, Lasg;->x:Lask;

    invoke-interface {v0}, Lask;->y()I

    move-result v0

    iput v0, p0, Lasg;->n:I

    .line 20
    const v0, 0x7f020184

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lasg;->s:I

    .line 21
    const v0, 0x7f0d01cc

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lasg;->t:I

    .line 22
    const v0, 0x7f0f000d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lasg;->u:I

    .line 23
    const v0, 0x7f0f000e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lasg;->v:I

    .line 25
    iget-object v2, p0, Lahe;->h:Landroid/widget/ListView;

    .line 27
    iget-object v0, p0, Lasg;->o:Lcom/android/dialer/widget/EmptyContentView;

    if-nez v0, :cond_0

    .line 28
    instance-of v0, p0, Lasm;

    if-eqz v0, :cond_2

    .line 29
    new-instance v0, Laur;

    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Laur;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lasg;->o:Lcom/android/dialer/widget/EmptyContentView;

    .line 32
    :goto_0
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 33
    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v3, p0, Lasg;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 35
    iget-object v0, p0, Lahe;->h:Landroid/widget/ListView;

    .line 36
    iget-object v3, p0, Lasg;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 37
    invoke-virtual {p0}, Lasg;->g()V

    .line 38
    :cond_0
    const v0, 0x7f0c000d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 39
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 40
    invoke-virtual {p0, v4}, Lasg;->b(Z)V

    .line 41
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setAccessibilityLiveRegion(I)V

    .line 42
    invoke-static {v2}, Larg;->a(Landroid/view/View;)V

    .line 43
    new-instance v0, Lash;

    invoke-direct {v0, p0}, Lash;-><init>(Lasg;)V

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 44
    iget-object v0, p0, Lasg;->q:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lasg;->q:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 46
    :cond_1
    invoke-virtual {p0, v4}, Lasg;->i(Z)V

    .line 47
    return-void

    .line 30
    :cond_2
    new-instance v0, Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {p0}, Lasg;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/dialer/widget/EmptyContentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lasg;->o:Lcom/android/dialer/widget/EmptyContentView;

    goto :goto_0
.end method
