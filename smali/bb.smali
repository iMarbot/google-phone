.class public final Lbb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# static fields
.field public static final a:Landroid/animation/TypeEvaluator;


# instance fields
.field private b:Lba$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lbb;

    invoke-direct {v0}, Lbb;-><init>()V

    sput-object v0, Lbb;->a:Landroid/animation/TypeEvaluator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lba$a;

    .line 3
    invoke-direct {v0}, Lba$a;-><init>()V

    .line 4
    iput-object v0, p0, Lbb;->b:Lba$a;

    return-void
.end method


# virtual methods
.method public final synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 5
    check-cast p2, Lba$a;

    check-cast p3, Lba$a;

    .line 6
    iget-object v0, p0, Lbb;->b:Lba$a;

    iget v1, p2, Lba$a;->a:F

    iget v2, p3, Lba$a;->a:F

    .line 7
    invoke-static {v1, v2, p1}, Lbh;->a(FFF)F

    move-result v1

    iget v2, p2, Lba$a;->b:F

    iget v3, p3, Lba$a;->b:F

    .line 8
    invoke-static {v2, v3, p1}, Lbh;->a(FFF)F

    move-result v2

    iget v3, p2, Lba$a;->c:F

    iget v4, p3, Lba$a;->c:F

    .line 9
    invoke-static {v3, v4, p1}, Lbh;->a(FFF)F

    move-result v3

    .line 10
    invoke-virtual {v0, v1, v2, v3}, Lba$a;->a(FFF)V

    .line 11
    iget-object v0, p0, Lbb;->b:Lba$a;

    .line 12
    return-object v0
.end method
