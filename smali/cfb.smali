.class public final Lcfb;
.super Lip;
.source "PG"


# instance fields
.field private W:Z

.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcfb;->W:Z

    return-void
.end method

.method public static a(Lcgr;)Lcfb;
    .locals 3

    .prologue
    .line 3
    new-instance v0, Lcfb;

    invoke-direct {v0}, Lcfb;-><init>()V

    .line 4
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 5
    const-string v2, "info"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 6
    invoke-virtual {v0, v1}, Lcfb;->f(Landroid/os/Bundle;)V

    .line 7
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 8
    const v0, 0x7f04007f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 10
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    .line 11
    const-string v1, "info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcgr;

    .line 12
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgr;

    .line 13
    const v1, 0x7f0e01f1

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 14
    iget-boolean v2, v0, Lcgr;->c:Z

    if-eqz v2, :cond_0

    .line 15
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v2

    iget-object v4, v0, Lcgr;->b:Ljava/lang/String;

    sget-object v5, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    .line 16
    invoke-virtual {v2, v4, v5}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v2

    .line 17
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->createTtsSpannable(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 19
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20
    const v1, 0x7f0e01f0

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 21
    iget-boolean v0, v0, Lcgr;->d:Z

    if-eqz v0, :cond_1

    .line 22
    const v0, 0x7f02017a

    .line 24
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 25
    new-instance v0, Lcfc;

    invoke-direct {v0, p0}, Lcfc;-><init>(Lcfb;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 26
    return-object v3

    .line 18
    :cond_0
    iget-object v2, v0, Lcgr;->b:Ljava/lang/String;

    goto :goto_0

    .line 23
    :cond_1
    const v0, 0x7f020166

    goto :goto_1
.end method

.method final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    .line 31
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 32
    if-nez v0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-boolean v0, p0, Lcfb;->W:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcfb;->a:I

    move v1, v0

    .line 36
    :goto_1
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 39
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 40
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 42
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 43
    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 34
    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcfb;->W:Z

    .line 28
    invoke-virtual {p0}, Lcfb;->a()V

    .line 29
    return-void
.end method
