.class public Lfcg;
.super Lfcb;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 5
    const/16 v0, 0x60

    const-string v1, "packetization-mode=1; level-asymmetry-allowed=1; profile-level-id=42900B"

    invoke-direct {p0, v0, v1}, Lfcg;-><init>(ILjava/lang/String;)V

    .line 6
    return-void
.end method

.method public constructor <init>(B)V
    .locals 3

    .prologue
    .line 10
    const-string v0, "VP8"

    const/16 v1, 0x60

    const v2, 0x15f90

    invoke-direct {p0, v0, v1, v2}, Lfcg;-><init>(Ljava/lang/String;II)V

    .line 11
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 7
    const-string v0, "H264"

    const/16 v1, 0x60

    invoke-direct {p0, v0, v1}, Lfcg;-><init>(Ljava/lang/String;I)V

    .line 8
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p0, v0}, Lfcg;->a([Ljava/lang/String;)V

    .line 9
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1
    const v0, 0x15f90

    invoke-direct {p0, p1, p2, v0}, Lfcb;-><init>(Ljava/lang/String;II)V

    .line 2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 3
    const/16 v0, 0x60

    const v1, 0x15f90

    invoke-direct {p0, p1, v0, v1}, Lfcb;-><init>(Ljava/lang/String;II)V

    .line 4
    return-void
.end method
