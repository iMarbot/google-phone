.class public final Lfqu;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field public final mediaMuxer:Landroid/media/MediaMuxer;

.field public videoTrackIndex:I

.field public videoTrackStartTimeUs:J


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lfqu;->videoTrackIndex:I

    .line 3
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lfqu;->videoTrackStartTimeUs:J

    .line 4
    if-nez p1, :cond_0

    move v0, v1

    .line 5
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 6
    if-eqz v0, :cond_1

    const-string v3, "webm"

    :goto_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1c

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "vclib-"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 7
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 8
    const-string v4, "Saving frames to %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    if-eqz v0, :cond_2

    .line 12
    :goto_2
    new-instance v0, Landroid/media/MediaMuxer;

    invoke-direct {v0, v3, v1}, Landroid/media/MediaMuxer;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    .line 13
    return-void

    :cond_0
    move v0, v2

    .line 4
    goto/16 :goto_0

    .line 6
    :cond_1
    const-string v3, "mp4"

    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 11
    goto :goto_2
.end method


# virtual methods
.method public final release()V
    .locals 2

    .prologue
    .line 30
    iget v0, p0, Lfqu;->videoTrackIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 37
    :goto_0
    return-void

    .line 32
    :cond_0
    :try_start_0
    iget-object v0, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->stop()V

    .line 33
    iget-object v0, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v1, "Failed to release media muxer"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final start(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 14
    iget v0, p0, Lfqu;->videoTrackIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18
    :goto_0
    return-void

    .line 16
    :cond_0
    iget-object v0, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v0

    iput v0, p0, Lfqu;->videoTrackIndex:I

    .line 17
    iget-object v0, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->start()V

    goto :goto_0
.end method

.method public final writeBuffer(Landroid/media/MediaCodec$BufferInfo;Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 19
    iget v0, p0, Lfqu;->videoTrackIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 29
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-wide v0, p0, Lfqu;->videoTrackStartTimeUs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 22
    iget-wide v0, p1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v0, p0, Lfqu;->videoTrackStartTimeUs:J

    .line 23
    :cond_1
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 24
    iget v1, p1, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iput v1, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    .line 25
    iget v1, p1, Landroid/media/MediaCodec$BufferInfo;->size:I

    iput v1, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 26
    iget v1, p1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    iput v1, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    .line 27
    iget-wide v2, p1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v4, p0, Lfqu;->videoTrackStartTimeUs:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 28
    iget-object v1, p0, Lfqu;->mediaMuxer:Landroid/media/MediaMuxer;

    iget v2, p0, Lfqu;->videoTrackIndex:I

    invoke-virtual {v1, v2, p2, v0}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    goto :goto_0
.end method
