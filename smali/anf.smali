.class public final Lanf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laov;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/support/v7/widget/RecyclerView$a;

.field public final c:Lawr;

.field private d:Landroid/app/FragmentManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/FragmentManager;Landroid/support/v7/widget/RecyclerView$a;Lawr;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lanf;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lanf;->d:Landroid/app/FragmentManager;

    .line 4
    iput-object p3, p0, Lanf;->b:Landroid/support/v7/widget/RecyclerView$a;

    .line 5
    iput-object p4, p0, Lanf;->c:Lawr;

    .line 6
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V
    .locals 7

    .prologue
    .line 7
    iget-object v0, p0, Lanf;->a:Landroid/content/Context;

    .line 8
    invoke-static {v0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->f()Z

    move-result v6

    new-instance v0, Lang;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lang;-><init>(Lanf;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V

    const/4 v1, 0x0

    .line 9
    invoke-static {p1, v6, v0, v1}, Lawc;->a(Ljava/lang/String;ZLawh;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/DialogFragment;

    move-result-object v0

    iget-object v1, p0, Lanf;->d:Landroid/app/FragmentManager;

    const-string v2, "BlockReportSpamDialog"

    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 11
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;ZLjava/lang/Integer;)V
    .locals 8

    .prologue
    .line 24
    new-instance v0, Lank;

    move-object v1, p0

    move v2, p6

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lank;-><init>(Lanf;ZLjava/lang/String;Ljava/lang/String;ILbko$a;Ljava/lang/Integer;)V

    .line 25
    new-instance v1, Lawj;

    invoke-direct {v1}, Lawj;-><init>()V

    .line 26
    iput-object p1, v1, Lawj;->c:Ljava/lang/String;

    .line 27
    iput-boolean p6, v1, Lawj;->a:Z

    .line 28
    iput-object v0, v1, Lawj;->d:Lawg;

    .line 29
    const/4 v0, 0x0

    iput-object v0, v1, Lawj;->e:Landroid/content/DialogInterface$OnDismissListener;

    .line 31
    iget-object v0, p0, Lanf;->d:Landroid/app/FragmentManager;

    const-string v2, "UnblockDialog"

    .line 32
    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V
    .locals 7

    .prologue
    .line 12
    iget-object v0, p0, Lanf;->a:Landroid/content/Context;

    .line 13
    invoke-static {v0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->a()Z

    move-result v6

    new-instance v0, Lani;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lani;-><init>(Lanf;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V

    .line 15
    new-instance v1, Lawb;

    invoke-direct {v1}, Lawb;-><init>()V

    .line 16
    iput-object p1, v1, Lawb;->c:Ljava/lang/String;

    .line 17
    iput-object v0, v1, Lawb;->d:Lawg;

    .line 18
    const/4 v0, 0x0

    iput-object v0, v1, Lawb;->e:Landroid/content/DialogInterface$OnDismissListener;

    .line 19
    iput-boolean v6, v1, Lawb;->a:Z

    .line 21
    iget-object v0, p0, Lanf;->d:Landroid/app/FragmentManager;

    const-string v2, "BlockDialog"

    .line 22
    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V
    .locals 6

    .prologue
    .line 34
    new-instance v0, Lanm;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lanm;-><init>(Lanf;Ljava/lang/String;Ljava/lang/String;ILbko$a;)V

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lawi;->a(Ljava/lang/String;Lawg;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/DialogFragment;

    move-result-object v0

    iget-object v1, p0, Lanf;->d:Landroid/app/FragmentManager;

    const-string v2, "NotSpamDialog"

    .line 35
    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 36
    return-void
.end method
