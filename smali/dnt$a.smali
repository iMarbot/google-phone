.class public final enum Ldnt$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ldnt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Ldnt$a;

.field public static final enum b:Ldnt$a;

.field public static final enum c:Ldnt$a;

.field public static final enum d:Ldnt$a;

.field public static final enum e:Ldnt$a;

.field public static final enum f:Ldnt$a;

.field public static final enum g:Ldnt$a;

.field public static final enum h:Ldnt$a;

.field public static final enum i:Ldnt$a;

.field public static final enum j:Ldnt$a;

.field public static final enum k:Ldnt$a;

.field public static final enum l:Ldnt$a;

.field public static final enum m:Ldnt$a;

.field public static final enum n:Ldnt$a;

.field public static final enum o:Ldnt$a;

.field public static final p:Lhby;

.field private static enum q:Ldnt$a;

.field private static enum r:Ldnt$a;

.field private static enum s:Ldnt$a;

.field private static enum t:Ldnt$a;

.field private static enum u:Ldnt$a;

.field private static enum v:Ldnt$a;

.field private static enum w:Ldnt$a;

.field private static synthetic y:[Ldnt$a;


# instance fields
.field private x:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    new-instance v0, Ldnt$a;

    const-string v1, "UNKNOWN_EVENT_TYPE"

    const v2, 0x186a0

    invoke-direct {v0, v1, v4, v2}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->q:Ldnt$a;

    .line 31
    new-instance v0, Ldnt$a;

    const-string v1, "SPAM_SYNC_GPRC_FAILED"

    const v2, 0x186a1

    invoke-direct {v0, v1, v5, v2}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->a:Ldnt$a;

    .line 32
    new-instance v0, Ldnt$a;

    const-string v1, "SPAM_SYNC_FAILED_TO_UPDATE_TO_BLACKLIST_VERSION"

    const v2, 0x186a2

    invoke-direct {v0, v1, v6, v2}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->b:Ldnt$a;

    .line 33
    new-instance v0, Ldnt$a;

    const-string v1, "SPAM_UPDATE_COMPLETE_LIST"

    const v2, 0x186a3

    invoke-direct {v0, v1, v7, v2}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->c:Ldnt$a;

    .line 34
    new-instance v0, Ldnt$a;

    const-string v1, "SPAM_UPDATE_DIFFERENCE_LIST"

    const v2, 0x186a4

    invoke-direct {v0, v1, v8, v2}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->d:Ldnt$a;

    .line 35
    new-instance v0, Ldnt$a;

    const-string v1, "TOTAL_REPORT_SPAM"

    const/4 v2, 0x5

    const v3, 0x186a5

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->r:Ldnt$a;

    .line 36
    new-instance v0, Ldnt$a;

    const-string v1, "TOTAL_REPORT_NOT_SPAM"

    const/4 v2, 0x6

    const v3, 0x186a6

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->s:Ldnt$a;

    .line 37
    new-instance v0, Ldnt$a;

    const-string v1, "EMPTY_SPAM_LIST_RETURNED_FROM_SERVER"

    const/4 v2, 0x7

    const v3, 0x186a7

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->e:Ldnt$a;

    .line 38
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_INVALID_CAPABILITIES_UPDATE"

    const/16 v2, 0x8

    const v3, 0x186a8

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->f:Ldnt$a;

    .line 39
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_INVALID_SESSION_STATUS_UPDATE"

    const/16 v2, 0x9

    const v3, 0x186a9

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->g:Ldnt$a;

    .line 40
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_INVALID_MESSAGE_STATUS_UPDATE"

    const/16 v2, 0xa

    const v3, 0x186aa

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->h:Ldnt$a;

    .line 41
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_NO_SESSION_FOR_MESSAGE_STATUS_UPDATE"

    const/16 v2, 0xb

    const v3, 0x186ab

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->i:Ldnt$a;

    .line 42
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_INVALID_INCOMING_CALL_COMPOSER_MESSAGE"

    const/16 v2, 0xc

    const v3, 0x186ac

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->j:Ldnt$a;

    .line 43
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_NO_SESSION_FOR_INCOMING_CALL_COMPOSER_DATA"

    const/16 v2, 0xd

    const v3, 0x186ad

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->k:Ldnt$a;

    .line 44
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_INVALID_INCOMING_POST_CALL_MESSAGE"

    const/16 v2, 0xe

    const v3, 0x186ae

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->l:Ldnt$a;

    .line 45
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_NO_SESSION_FOR_INCOMING_POST_CALL_DATA"

    const/16 v2, 0xf

    const v3, 0x186af

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->m:Ldnt$a;

    .line 46
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_START_CALL_COMPOSER_SESSION"

    const/16 v2, 0x10

    const v3, 0x186b0

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->n:Ldnt$a;

    .line 47
    new-instance v0, Ldnt$a;

    const-string v1, "ENRICHED_CALL_START_CALL_COMPOSER_SESSION_FAILED"

    const/16 v2, 0x11

    const v3, 0x186b1

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->o:Ldnt$a;

    .line 48
    new-instance v0, Ldnt$a;

    const-string v1, "CONTACTS_DUPLICATES_PROMO_SHOWN"

    const/16 v2, 0x12

    const v3, 0x186b2

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->t:Ldnt$a;

    .line 49
    new-instance v0, Ldnt$a;

    const-string v1, "CONTACTS_DUPLICATES_PROMO_EXPANDED"

    const/16 v2, 0x13

    const v3, 0x186b3

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->u:Ldnt$a;

    .line 50
    new-instance v0, Ldnt$a;

    const-string v1, "CONTACTS_DUPLICATES_PROMO_ACCEPTED"

    const/16 v2, 0x14

    const v3, 0x186b4

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->v:Ldnt$a;

    .line 51
    new-instance v0, Ldnt$a;

    const-string v1, "CONTACTS_DUPLICATES_PROMO_DISMISSED"

    const/16 v2, 0x15

    const v3, 0x186b5

    invoke-direct {v0, v1, v2, v3}, Ldnt$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldnt$a;->w:Ldnt$a;

    .line 52
    const/16 v0, 0x16

    new-array v0, v0, [Ldnt$a;

    sget-object v1, Ldnt$a;->q:Ldnt$a;

    aput-object v1, v0, v4

    sget-object v1, Ldnt$a;->a:Ldnt$a;

    aput-object v1, v0, v5

    sget-object v1, Ldnt$a;->b:Ldnt$a;

    aput-object v1, v0, v6

    sget-object v1, Ldnt$a;->c:Ldnt$a;

    aput-object v1, v0, v7

    sget-object v1, Ldnt$a;->d:Ldnt$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Ldnt$a;->r:Ldnt$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldnt$a;->s:Ldnt$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldnt$a;->e:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldnt$a;->f:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldnt$a;->g:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldnt$a;->h:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ldnt$a;->i:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ldnt$a;->j:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ldnt$a;->k:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ldnt$a;->l:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ldnt$a;->m:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ldnt$a;->n:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ldnt$a;->o:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ldnt$a;->t:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ldnt$a;->u:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Ldnt$a;->v:Ldnt$a;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Ldnt$a;->w:Ldnt$a;

    aput-object v2, v0, v1

    sput-object v0, Ldnt$a;->y:[Ldnt$a;

    .line 53
    new-instance v0, Ldnu;

    invoke-direct {v0}, Ldnu;-><init>()V

    sput-object v0, Ldnt$a;->p:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Ldnt$a;->x:I

    .line 29
    return-void
.end method

.method public static a(I)Ldnt$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 26
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Ldnt$a;->q:Ldnt$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Ldnt$a;->a:Ldnt$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Ldnt$a;->b:Ldnt$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Ldnt$a;->c:Ldnt$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Ldnt$a;->d:Ldnt$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Ldnt$a;->r:Ldnt$a;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Ldnt$a;->s:Ldnt$a;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Ldnt$a;->e:Ldnt$a;

    goto :goto_0

    .line 12
    :pswitch_8
    sget-object v0, Ldnt$a;->f:Ldnt$a;

    goto :goto_0

    .line 13
    :pswitch_9
    sget-object v0, Ldnt$a;->g:Ldnt$a;

    goto :goto_0

    .line 14
    :pswitch_a
    sget-object v0, Ldnt$a;->h:Ldnt$a;

    goto :goto_0

    .line 15
    :pswitch_b
    sget-object v0, Ldnt$a;->i:Ldnt$a;

    goto :goto_0

    .line 16
    :pswitch_c
    sget-object v0, Ldnt$a;->j:Ldnt$a;

    goto :goto_0

    .line 17
    :pswitch_d
    sget-object v0, Ldnt$a;->k:Ldnt$a;

    goto :goto_0

    .line 18
    :pswitch_e
    sget-object v0, Ldnt$a;->l:Ldnt$a;

    goto :goto_0

    .line 19
    :pswitch_f
    sget-object v0, Ldnt$a;->m:Ldnt$a;

    goto :goto_0

    .line 20
    :pswitch_10
    sget-object v0, Ldnt$a;->n:Ldnt$a;

    goto :goto_0

    .line 21
    :pswitch_11
    sget-object v0, Ldnt$a;->o:Ldnt$a;

    goto :goto_0

    .line 22
    :pswitch_12
    sget-object v0, Ldnt$a;->t:Ldnt$a;

    goto :goto_0

    .line 23
    :pswitch_13
    sget-object v0, Ldnt$a;->u:Ldnt$a;

    goto :goto_0

    .line 24
    :pswitch_14
    sget-object v0, Ldnt$a;->v:Ldnt$a;

    goto :goto_0

    .line 25
    :pswitch_15
    sget-object v0, Ldnt$a;->w:Ldnt$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x186a0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public static values()[Ldnt$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Ldnt$a;->y:[Ldnt$a;

    invoke-virtual {v0}, [Ldnt$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldnt$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Ldnt$a;->x:I

    return v0
.end method
