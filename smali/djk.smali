.class final Ldjk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Ldjn;


# instance fields
.field public a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

.field private b:Landroid/content/Context;

.field private c:Landroid/telecom/Call;

.field private d:Landroid/os/Bundle;

.field private e:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/telecom/Call;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldjk;->e:Z

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldjk;->b:Landroid/content/Context;

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    iput-object v0, p0, Ldjk;->c:Landroid/telecom/Call;

    .line 5
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Ldjk;->d:Landroid/os/Bundle;

    .line 6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "DuoFallbackServiceConnection.onDuoFinished"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 24
    iget-boolean v0, p0, Ldjk;->e:Z

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjk;->e:Z

    .line 26
    iget-object v0, p0, Ldjk;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 27
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 29
    iget-object v1, p0, Ldjk;->c:Landroid/telecom/Call;

    new-instance v2, Ldjl;

    invoke-direct {v2, p0}, Ldjl;-><init>(Ldjk;)V

    invoke-virtual {v1, v2, v0}, Landroid/telecom/Call;->registerCallback(Landroid/telecom/Call$Callback;Landroid/os/Handler;)V

    .line 30
    iget-object v0, p0, Ldjk;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->disconnect()V

    .line 31
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 7
    const-string v0, "DuoFallbackServiceConnection.onServiceConnected"

    const-string v1, "connected"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    invoke-static {p2}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;

    move-result-object v0

    .line 9
    :try_start_0
    new-instance v1, Ldjm;

    invoke-direct {v1, p0}, Ldjm;-><init>(Ldjn;)V

    iget-object v2, p0, Ldjk;->c:Landroid/telecom/Call;

    .line 10
    invoke-virtual {v2}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Ldjk;->d:Landroid/os/Bundle;

    .line 11
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;->requestHandover(Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackSource;Landroid/net/Uri;Landroid/os/Bundle;)Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    move-result-object v0

    iput-object v0, p0, Ldjk;->a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    iget-object v0, p0, Ldjk;->a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    if-nez v0, :cond_0

    .line 17
    const-string v0, "DuoFallbackServiceConnection.onServiceConnected"

    const-string v1, "null result from handover request"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v0, p0, Ldjk;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 19
    :cond_0
    :goto_0
    return-void

    .line 13
    :catch_0
    move-exception v0

    .line 14
    const-string v1, "DuoFallbackServiceConnection.onServiceConnected"

    const-string v2, "failed to request handover"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 20
    const-string v0, "DuoFallbackServiceConnection.onServiceDisconnected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Ldjk;->a:Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    .line 22
    return-void
.end method
