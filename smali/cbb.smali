.class public Lcbb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwl;
.implements Lbwm;
.implements Lbwq;
.implements Lbwt;
.implements Lccf;
.implements Lcgk;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lcgj;

.field public c:Lcdc;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Landroid/telecom/PhoneAccountHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcbb;->d:Z

    .line 20
    iput-boolean v0, p0, Lcbb;->e:Z

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcbb;->a:Landroid/content/Context;

    .line 22
    return-void
.end method

.method public static a(F)F
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1
    const/4 v0, 0x0

    .line 2
    float-to-double v2, p0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    .line 4
    :cond_0
    float-to-double v2, p0

    const-wide v4, 0x400199999999999aL    # 2.2

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 5
    add-float/2addr v0, v1

    .line 6
    :cond_1
    float-to-double v2, p0

    const-wide v4, 0x4041800000000000L    # 35.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    .line 7
    add-float/2addr v0, v1

    .line 8
    :cond_2
    float-to-double v2, p0

    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_3

    .line 9
    add-float/2addr v0, v1

    .line 10
    :cond_3
    return v0
.end method

.method public static c(Lcdc;)Z
    .locals 1

    .prologue
    .line 343
    const/high16 v0, 0x400000

    invoke-virtual {p0, v0}, Lcdc;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {p0, v0}, Lcbb;->a(Lcdc;)V

    .line 79
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 87
    const-string v1, "CallButtonPresenter.setAudioRoute"

    const-string v2, "sending new audio route: "

    .line 88
    invoke-static {p1}, Landroid/telecom/CallAudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 89
    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdr;->a(I)V

    .line 91
    return-void

    .line 88
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 354
    const-string v0, "incall_key_automatically_muted"

    iget-boolean v1, p0, Lcbb;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 355
    const-string v0, "incall_key_previous_mute_state"

    iget-boolean v1, p0, Lcbb;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 356
    return-void
.end method

.method public a(Landroid/telecom/CallAudioState;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcbb;->b:Lcgj;

    invoke-interface {v0, p1}, Lcgj;->a(Landroid/telecom/CallAudioState;)V

    .line 82
    :cond_0
    return-void
.end method

.method public a(Lbwp;Lbwp;Lcct;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 54
    sget-object v0, Lbwp;->f:Lbwp;

    if-ne p2, v0, :cond_1

    .line 55
    invoke-virtual {p3}, Lcct;->c()Lcdc;

    move-result-object v0

    iput-object v0, p0, Lcbb;->c:Lcdc;

    .line 68
    :cond_0
    :goto_0
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {p0, p2, v0}, Lcbb;->a(Lbwp;Lcdc;)V

    .line 69
    return-void

    .line 56
    :cond_1
    sget-object v0, Lbwp;->c:Lbwp;

    if-ne p2, v0, :cond_2

    .line 57
    invoke-virtual {p3}, Lcct;->h()Lcdc;

    move-result-object v0

    iput-object v0, p0, Lcbb;->c:Lcdc;

    .line 58
    sget-object v0, Lbwp;->f:Lbwp;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcbb;->c:Lcdc;

    .line 60
    iget-boolean v0, v0, Lcdc;->G:Z

    .line 61
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcbb;->p()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcbb;->p()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lbwp;->b:Lbwp;

    if-ne p2, v0, :cond_4

    .line 64
    invoke-virtual {p0}, Lcbb;->p()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 65
    invoke-virtual {p0}, Lcbb;->p()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    .line 66
    :cond_3
    invoke-virtual {p3}, Lcct;->i()Lcdc;

    move-result-object v0

    iput-object v0, p0, Lcbb;->c:Lcdc;

    goto :goto_0

    .line 67
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcbb;->c:Lcdc;

    goto :goto_0
.end method

.method public a(Lbwp;Lbwp;Lcdc;)V
    .locals 1

    .prologue
    .line 73
    .line 74
    sget-object v0, Lcct;->a:Lcct;

    .line 75
    invoke-virtual {p0, p1, p2, v0}, Lcbb;->a(Lbwp;Lbwp;Lcct;)V

    .line 76
    return-void
.end method

.method public a(Lbwp;Lcdc;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 272
    new-array v2, v0, [Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 273
    iget-object v2, p0, Lcbb;->b:Lcgj;

    if-nez v2, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    if-eqz p2, :cond_2

    .line 276
    iget-object v2, p0, Lcbb;->b:Lcgj;

    .line 277
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    .line 278
    iget-object v3, v3, Lbwg;->x:Lbxf;

    .line 280
    iget v3, v3, Lbxf;->b:I

    .line 281
    invoke-interface {v2, v3}, Lcgj;->e(I)V

    .line 283
    :cond_2
    invoke-virtual {p1}, Lbwp;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 284
    sget-object v2, Lbwp;->b:Lbwp;

    if-ne p1, v2, :cond_3

    move v2, v0

    .line 285
    :goto_1
    if-nez v2, :cond_4

    if-eqz p2, :cond_4

    .line 286
    :goto_2
    iget-object v1, p0, Lcbb;->b:Lcgj;

    invoke-interface {v1, v0}, Lcgj;->h(Z)V

    .line 287
    if-eqz p2, :cond_0

    .line 289
    invoke-virtual {p0, p2}, Lcbb;->a(Lcdc;)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 284
    goto :goto_1

    :cond_4
    move v0, v1

    .line 285
    goto :goto_2
.end method

.method public a(Lcdc;)V
    .locals 16

    .prologue
    .line 291
    invoke-virtual/range {p1 .. p1}, Lcdc;->u()Z

    move-result v9

    .line 292
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcdc;->c(I)Z

    move-result v10

    .line 293
    if-nez v10, :cond_2

    const/4 v1, 0x2

    .line 294
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcdc;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 295
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcdc;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 296
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcdc;->j()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    .line 297
    :goto_1
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v3

    .line 298
    iget-object v4, v3, Lcdr;->a:Landroid/telecom/InCallService;

    if-eqz v4, :cond_4

    .line 299
    iget-object v3, v3, Lcdr;->a:Landroid/telecom/InCallService;

    invoke-virtual {v3}, Landroid/telecom/InCallService;->canAddCall()Z

    move-result v3

    .line 301
    :goto_2
    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v3}, Lbw;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    .line 302
    :goto_3
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcdc;->c(I)Z

    move-result v11

    .line 303
    if-nez v9, :cond_6

    invoke-virtual/range {p0 .. p1}, Lcbb;->b(Lcdc;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    move v8, v4

    .line 304
    :goto_4
    if-eqz v9, :cond_7

    invoke-static/range {p1 .. p1}, Lcbb;->c(Lcdc;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    .line 305
    :goto_5
    const/16 v5, 0x40

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcdc;->c(I)Z

    move-result v12

    .line 306
    if-eqz v9, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcbb;->a:Landroid/content/Context;

    .line 307
    invoke-static {v5}, Lbvs;->d(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    move v7, v5

    .line 308
    :goto_6
    if-eqz v9, :cond_9

    .line 309
    invoke-virtual/range {p1 .. p1}, Lcdc;->j()I

    move-result v5

    const/4 v6, 0x6

    if-eq v5, v6, :cond_9

    .line 310
    invoke-virtual/range {p1 .. p1}, Lcdc;->j()I

    move-result v5

    const/16 v6, 0xd

    if-eq v5, v6, :cond_9

    const/4 v5, 0x1

    .line 311
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcbb;->o()Landroid/content/Context;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcdc;->r()Landroid/telecom/PhoneAccountHandle;

    move-result-object v13

    invoke-static {v6, v13}, Lbsp;->e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcbb;->g:Landroid/telecom/PhoneAccountHandle;

    .line 312
    move-object/from16 v0, p0

    iget-object v6, v0, Lcbb;->g:Landroid/telecom/PhoneAccountHandle;

    if-eqz v6, :cond_a

    .line 314
    move-object/from16 v0, p1

    iget-boolean v6, v0, Lcdc;->G:Z

    .line 315
    if-nez v6, :cond_a

    .line 316
    invoke-virtual/range {p1 .. p1}, Lcdc;->j()I

    move-result v6

    invoke-static {v6}, Lbvs;->c(I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 317
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v6

    .line 318
    iget-object v6, v6, Lbwg;->i:Lcct;

    .line 320
    iget-object v6, v6, Lcct;->b:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    .line 321
    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    const/4 v13, 0x1

    if-ne v6, v13, :cond_a

    const/4 v6, 0x1

    .line 322
    :goto_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcbb;->b:Lcgj;

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-interface {v13, v14, v15}, Lcgj;->a(IZ)V

    .line 323
    move-object/from16 v0, p0

    iget-object v13, v0, Lcbb;->b:Lcgj;

    const/4 v14, 0x4

    invoke-interface {v13, v14, v10}, Lcgj;->a(IZ)V

    .line 324
    move-object/from16 v0, p0

    iget-object v10, v0, Lcbb;->b:Lcgj;

    const/4 v13, 0x3

    invoke-interface {v10, v13, v1}, Lcgj;->a(IZ)V

    .line 325
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    invoke-interface {v1, v2}, Lcgj;->i(Z)V

    .line 326
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v12}, Lcgj;->a(IZ)V

    .line 327
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/16 v2, 0xe

    invoke-interface {v1, v2, v6}, Lcgj;->a(IZ)V

    .line 328
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/16 v2, 0x8

    const/4 v6, 0x1

    invoke-interface {v1, v2, v6}, Lcgj;->a(IZ)V

    .line 329
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, Lcgj;->b(IZ)V

    .line 330
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/4 v2, 0x5

    invoke-interface {v1, v2, v8}, Lcgj;->a(IZ)V

    .line 331
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/4 v2, 0x7

    invoke-interface {v1, v2, v4}, Lcgj;->a(IZ)V

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcbb;->b:Lcgj;

    const/4 v3, 0x6

    if-eqz v9, :cond_b

    if-eqz v7, :cond_b

    .line 333
    invoke-virtual/range {p1 .. p1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->j()Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    .line 334
    :goto_9
    invoke-interface {v2, v3, v1}, Lcgj;->a(IZ)V

    .line 335
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/16 v2, 0xa

    invoke-interface {v1, v2, v5}, Lcgj;->a(IZ)V

    .line 336
    if-eqz v9, :cond_1

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcbb;->b:Lcgj;

    invoke-virtual/range {p1 .. p1}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v7, :cond_c

    :cond_0
    const/4 v1, 0x1

    :goto_a
    invoke-interface {v2, v1}, Lcgj;->k(Z)V

    .line 338
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcgj;->a(IZ)V

    .line 339
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    const/16 v2, 0x9

    invoke-interface {v1, v2, v11}, Lcgj;->a(IZ)V

    .line 340
    move-object/from16 v0, p0

    iget-object v1, v0, Lcbb;->b:Lcgj;

    invoke-interface {v1}, Lcgj;->T()V

    .line 341
    return-void

    .line 295
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 296
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 300
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 301
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 303
    :cond_6
    const/4 v4, 0x0

    move v8, v4

    goto/16 :goto_4

    .line 304
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 307
    :cond_8
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_6

    .line 310
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 321
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 333
    :cond_b
    const/4 v1, 0x0

    goto :goto_9

    .line 337
    :cond_c
    const/4 v1, 0x0

    goto :goto_a
.end method

.method public a(Lcdc;Landroid/telecom/Call$Details;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0, p1}, Lcbb;->a(Lcdc;)V

    .line 72
    :cond_0
    return-void
.end method

.method public a(Lcgj;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 23
    iget-boolean v0, p0, Lcbb;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 24
    iput-object p1, p0, Lcbb;->b:Lcgj;

    .line 25
    sget-object v0, Lcce;->a:Lcce;

    .line 26
    invoke-virtual {v0, p0}, Lcce;->a(Lccf;)V

    .line 27
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 29
    invoke-virtual {v0, p0}, Lbwg;->a(Lbwt;)V

    .line 30
    invoke-virtual {v0, p0}, Lbwg;->a(Lbwm;)V

    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 33
    iget-object v2, v0, Lbwg;->b:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcaz;->a(Lcbb;)V

    .line 35
    sget-object v2, Lbwp;->a:Lbwp;

    .line 36
    iget-object v0, v0, Lbwg;->n:Lbwp;

    .line 37
    sget-object v3, Lcct;->a:Lcct;

    .line 38
    invoke-virtual {p0, v2, v0, v3}, Lcbb;->a(Lbwp;Lbwp;Lcct;)V

    .line 39
    iput-boolean v1, p0, Lcbb;->f:Z

    .line 40
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-nez v0, :cond_0

    .line 14
    :goto_0
    return-void

    .line 13
    :cond_0
    iget-object v1, p0, Lcbb;->b:Lcgj;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lcgj;->j(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 6

    .prologue
    .line 116
    const-string v0, "CallButtonPresenter"

    const-string v1, "turning on mute: %s, clicked by user: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 117
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 118
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    if-eqz p2, :cond_0

    .line 120
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    .line 121
    if-eqz p1, :cond_1

    .line 122
    sget-object v0, Lbkq$a;->ay:Lbkq$a;

    .line 123
    :goto_0
    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 125
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 126
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 128
    iget-wide v4, v3, Lcdc;->M:J

    .line 129
    invoke-interface {v1, v0, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 130
    :cond_0
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdr;->a(Z)V

    .line 131
    return-void

    .line 123
    :cond_1
    sget-object v0, Lbkq$a;->az:Lbkq$a;

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {p0, v0}, Lcbb;->a(Lcdc;)V

    .line 17
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 357
    const-string v0, "incall_key_automatically_muted"

    iget-boolean v1, p0, Lcbb;->d:Z

    .line 358
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcbb;->d:Z

    .line 359
    const-string v0, "incall_key_previous_mute_state"

    iget-boolean v1, p0, Lcbb;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcbb;->e:Z

    .line 360
    return-void
.end method

.method public b(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 132
    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 134
    :cond_0
    if-eqz p1, :cond_1

    .line 135
    const-string v0, "CallButtonPresenter"

    iget-object v1, p0, Lcbb;->c:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "putting the call on hold: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcbb;->c:Lcdc;

    .line 137
    const-string v1, "DialerCall.hold"

    const-string v2, ""

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    iget-object v0, v0, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->hold()V

    goto :goto_0

    .line 140
    :cond_1
    const-string v0, "CallButtonPresenter"

    iget-object v1, p0, Lcbb;->c:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "removing the call from hold: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->C()V

    goto :goto_0
.end method

.method public b(Lcdc;)Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p1}, Lcdc;->F()Lcjs;

    move-result-object v0

    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcjs;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcbb;->f:Z

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcbb;->b:Lcgj;

    .line 43
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 44
    sget-object v0, Lcce;->a:Lcce;

    .line 45
    invoke-virtual {v0, p0}, Lcce;->b(Lccf;)V

    .line 46
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwt;)V

    .line 47
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwm;)V

    .line 48
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcaz;->b(Lcbb;)V

    .line 49
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 50
    if-eqz p0, :cond_0

    .line 51
    iget-object v0, v0, Lbwg;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcbb;->f:Z

    .line 53
    return-void
.end method

.method public c(Z)V
    .locals 6

    .prologue
    .line 185
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dl:Lbkq$a;

    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 187
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 188
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 190
    iget-wide v4, v3, Lcdc;->M:J

    .line 191
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 192
    const-string v0, "show dialpad "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 193
    :goto_0
    invoke-virtual {p0}, Lcbb;->p()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    .line 194
    return-void

    .line 192
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()Landroid/telecom/CallAudioState;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcce;->a:Lcce;

    .line 85
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 86
    return-object v0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcbb;->f(Z)V

    .line 226
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    const/16 v0, 0x8

    .line 92
    invoke-virtual {p0}, Lcbb;->d()Landroid/telecom/CallAudioState;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 94
    const-string v0, "CallButtonPresenter"

    const-string v2, "toggling speakerphone not allowed when bluetooth supported."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcbb;->b:Lcgj;

    invoke-interface {v0, v1}, Lcgj;->a(Landroid/telecom/CallAudioState;)V

    .line 115
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {v1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 98
    const/4 v0, 0x5

    .line 99
    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->c:Lbkq$a;

    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 101
    iget-object v3, v3, Lcdc;->b:Ljava/lang/String;

    .line 102
    iget-object v4, p0, Lcbb;->c:Lcdc;

    .line 104
    iget-wide v4, v4, Lcdc;->M:J

    .line 105
    invoke-interface {v1, v2, v3, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 114
    :goto_1
    invoke-virtual {p0, v0}, Lcbb;->a(I)V

    goto :goto_0

    .line 107
    :cond_1
    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->b:Lbkq$a;

    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 109
    iget-object v3, v3, Lcdc;->b:Ljava/lang/String;

    .line 110
    iget-object v4, p0, Lcbb;->c:Lcdc;

    .line 112
    iget-wide v4, v4, Lcdc;->M:J

    .line 113
    invoke-interface {v1, v2, v3, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method public e(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 241
    const-string v1, "CallButtonPresenter.pauseVideoClicked"

    const-string v2, "%s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string v0, "pause"

    :goto_0
    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    .line 243
    if-eqz p1, :cond_1

    .line 244
    sget-object v0, Lbkq$a;->aC:Lbkq$a;

    .line 245
    :goto_1
    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 247
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 248
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 250
    iget-wide v4, v3, Lcdc;->M:J

    .line 251
    invoke-interface {v1, v0, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 252
    if-eqz p1, :cond_2

    .line 253
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcjs;->a(Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->k()V

    .line 259
    :goto_2
    iget-object v0, p0, Lcbb;->b:Lcgj;

    invoke-interface {v0, p1}, Lcgj;->k(Z)V

    .line 260
    iget-object v0, p0, Lcbb;->b:Lcgj;

    const/16 v1, 0xa

    invoke-interface {v0, v1, v6}, Lcgj;->b(IZ)V

    .line 261
    return-void

    .line 241
    :cond_0
    const-string v0, "unpause"

    goto :goto_0

    .line 245
    :cond_1
    sget-object v0, Lbkq$a;->aB:Lbkq$a;

    goto :goto_1

    .line 256
    :cond_2
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    invoke-virtual {v0}, Lcaz;->a()Z

    move-result v0

    .line 257
    invoke-virtual {p0, v0}, Lcbb;->f(Z)V

    .line 258
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcjs;->e(Landroid/content/Context;)V

    goto :goto_2
.end method

.method public f()V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 145
    :cond_0
    const-string v0, "CallButtonPresenter"

    iget-object v1, p0, Lcbb;->c:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "swapping the call: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    iget-object v1, p0, Lcbb;->c:Lcdc;

    .line 147
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 148
    invoke-virtual {v0, v1}, Lcdr;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 3

    .prologue
    .line 262
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->h()Lcaz;

    move-result-object v0

    .line 263
    invoke-virtual {v0, p1}, Lcaz;->a(Z)V

    .line 264
    invoke-virtual {v0}, Lcaz;->b()Ljava/lang/String;

    move-result-object v1

    .line 265
    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {v0}, Lcaz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    const/4 v0, 0x0

    .line 269
    :goto_0
    iget-object v2, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v2, v0}, Lcdc;->a(I)V

    .line 270
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, v1}, Lcjs;->a(Ljava/lang/String;)V

    .line 271
    :cond_0
    return-void

    .line 268
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g()V
    .locals 6

    .prologue
    .line 150
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dn:Lbkq$a;

    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 152
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 153
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 155
    iget-wide v4, v3, Lcdc;->M:J

    .line 156
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 157
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    iget-object v1, p0, Lcbb;->c:Lcdc;

    .line 158
    iget-object v1, v1, Lcdc;->e:Ljava/lang/String;

    .line 159
    invoke-virtual {v0, v1}, Lcdr;->b(Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public h()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 161
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dm:Lbkq$a;

    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 163
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 164
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 166
    iget-wide v4, v3, Lcdc;->M:J

    .line 167
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 168
    iput-boolean v6, p0, Lcbb;->d:Z

    .line 170
    sget-object v0, Lcce;->a:Lcce;

    .line 172
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 173
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v0

    iput-boolean v0, p0, Lcbb;->e:Z

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcbb;->a(ZZ)V

    .line 175
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    .line 176
    iget-object v1, v0, Lcdr;->a:Landroid/telecom/InCallService;

    if-eqz v1, :cond_0

    .line 177
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 179
    const-string v2, "add_call_mode"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    :try_start_0
    iget-object v0, v0, Lcdr;->a:Landroid/telecom/InCallService;

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    const-string v1, "TelecomAdapter.addCall"

    const-string v2, "Activity for adding calls isn\'t found."

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public i()V
    .locals 6

    .prologue
    .line 195
    const-string v0, "CallButtonPresenter.changeToVideoClicked"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bG:Lbkq$a;

    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 198
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 199
    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 201
    iget-wide v4, v3, Lcdc;->M:J

    .line 202
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 203
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcjs;->c(Landroid/content/Context;)V

    .line 204
    return-void
.end method

.method public j()V
    .locals 4

    .prologue
    .line 205
    const-string v0, "CallButtonPresenter.onEndCallClicked"

    iget-object v1, p0, Lcbb;->c:Lcdc;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "call: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcbb;->c:Lcdc;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcbb;->c:Lcdc;

    invoke-virtual {v0}, Lcdc;->B()V

    .line 208
    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcbb;->b:Lcgj;

    invoke-interface {v0}, Lcgj;->U()V

    .line 210
    return-void
.end method

.method public l()V
    .locals 7

    .prologue
    .line 211
    const-string v0, "CallButtonPresenter.swapSimClicked"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 212
    invoke-virtual {p0}, Lcbb;->o()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dS:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 213
    new-instance v0, Lchf;

    .line 214
    invoke-virtual {p0}, Lcbb;->o()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcbb;->c:Lcdc;

    .line 215
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    .line 216
    iget-object v3, v3, Lbwg;->i:Lcct;

    .line 217
    iget-object v4, p0, Lcbb;->g:Landroid/telecom/PhoneAccountHandle;

    .line 218
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v5

    const-string v6, "swapSim"

    invoke-virtual {v5, v6}, Lbwg;->a(Ljava/lang/String;)Lcgt;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lchf;-><init>(Landroid/content/Context;Lcdc;Lcct;Landroid/telecom/PhoneAccountHandle;Lcgt;)V

    .line 219
    invoke-virtual {p0}, Lcbb;->o()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    .line 220
    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    .line 221
    invoke-virtual {v1, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 222
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 223
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 224
    return-void
.end method

.method public m()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 227
    const-string v1, "CallButtonPresenter.toggleCameraClicked"

    const-string v2, ""

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    iget-object v1, p0, Lcbb;->c:Lcdc;

    if-nez v1, :cond_0

    .line 240
    :goto_0
    return-void

    .line 230
    :cond_0
    iget-object v1, p0, Lcbb;->a:Landroid/content/Context;

    invoke-static {v1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->aA:Lbkq$a;

    iget-object v3, p0, Lcbb;->c:Lcdc;

    .line 232
    iget-object v3, v3, Lcdc;->b:Ljava/lang/String;

    .line 233
    iget-object v4, p0, Lcbb;->c:Lcdc;

    .line 235
    iget-wide v4, v4, Lcdc;->M:J

    .line 236
    invoke-interface {v1, v2, v3, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 238
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1}, Lbwg;->h()Lcaz;

    move-result-object v1

    invoke-virtual {v1}, Lcaz;->a()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 239
    :cond_1
    invoke-virtual {p0, v0}, Lcbb;->d(Z)V

    goto :goto_0
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 344
    iget-boolean v0, p0, Lcbb;->d:Z

    if-eqz v0, :cond_1

    .line 345
    sget-object v0, Lcce;->a:Lcce;

    .line 347
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 348
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v0

    iget-boolean v1, p0, Lcbb;->e:Z

    if-eq v0, v1, :cond_1

    .line 349
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-nez v0, :cond_0

    .line 353
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-boolean v0, p0, Lcbb;->e:Z

    invoke-virtual {p0, v0, v2}, Lcbb;->a(ZZ)V

    .line 352
    :cond_1
    iput-boolean v2, p0, Lcbb;->d:Z

    goto :goto_0
.end method

.method public o()Landroid/content/Context;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcbb;->a:Landroid/content/Context;

    return-object v0
.end method

.method public p()Lcom/android/incallui/InCallActivity;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 362
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcbb;->b:Lcgj;

    if-nez v0, :cond_0

    throw v1

    :cond_0
    check-cast v0, Lip;

    .line 364
    if-eqz v0, :cond_1

    .line 365
    invoke-virtual {v0}, Lip;->h()Lit;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/InCallActivity;

    .line 366
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
