.class public final Lawr;
.super Landroid/content/AsyncQueryHandler;
.source "PG"


# static fields
.field public static final a:Ljava/util/Map;


# instance fields
.field public final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lawr;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 2
    iput-object p1, p0, Lawr;->b:Landroid/content/Context;

    .line 3
    return-void
.end method

.method private final a(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 99
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 61
    invoke-static {}, Lbdf;->c()V

    .line 62
    if-nez p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-object v6

    .line 64
    :cond_1
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 67
    if-eqz v0, :cond_3

    .line 68
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_2

    move-object v0, v6

    :cond_2
    move-object v6, v0

    .line 70
    goto :goto_0

    .line 71
    :cond_3
    invoke-static {p1, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 72
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0, v5, p1}, Laxd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 73
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    :try_start_0
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    .line 76
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lawr;->b:Landroid/content/Context;

    const/4 v2, 0x0

    .line 77
    invoke-static {v1, v2}, Lapw;->a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lawr;->b:Landroid/content/Context;

    .line 78
    invoke-static {v9}, Lapw;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v2, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lawr;->b:Landroid/content/Context;

    .line 79
    invoke-static {v9}, Lapw;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v2, v8

    .line 80
    invoke-static {v2}, Lapw;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v5, :cond_5

    .line 81
    :goto_1
    invoke-direct {p0, v3}, Lawr;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, " = ?"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x0

    .line 82
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 84
    if-eqz v2, :cond_4

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 85
    :cond_4
    sget-object v0, Lawr;->a:Ljava/util/Map;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 86
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    const-string v1, "FilteredNumberAsyncQueryHandler.getBlockedIdSynchronous"

    invoke-static {v1, v6, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_5
    move v3, v4

    .line 80
    goto :goto_1

    .line 88
    :cond_6
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 89
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 90
    sget-object v1, Lawr;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 92
    if-eqz v2, :cond_7

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_7
    move-object v6, v0

    .line 93
    goto/16 :goto_0

    .line 94
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 95
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_2
    if-eqz v2, :cond_8

    if-eqz v1, :cond_9

    :try_start_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_8
    :goto_3
    :try_start_7
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_9
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public final a(Lawy;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 107
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 108
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    if-eqz p1, :cond_0

    .line 110
    invoke-interface {p1, v3}, Lawy;->a(Landroid/net/Uri;)V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const/4 v0, 0x0

    new-instance v1, Lawu;

    invoke-direct {v1, p1}, Lawu;-><init>(Lawy;)V

    iget-object v2, p0, Lawr;->b:Landroid/content/Context;

    .line 113
    invoke-static {v2, v3}, Lapw;->a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;

    move-result-object v2

    .line 114
    invoke-virtual {p0, v0, v1, v2, p2}, Lawr;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public final a(Lawy;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lawr;->a(Lawy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public final a(Lawy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    .line 104
    invoke-static {v0, p3, p2, p4}, Lapw;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 105
    invoke-virtual {p0, p1, v0}, Lawr;->a(Lawy;Landroid/content/ContentValues;)V

    .line 106
    return-void
.end method

.method public final a(Lawz;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 29
    if-nez p2, :cond_1

    .line 30
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lawz;->a(Ljava/lang/Integer;)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    invoke-interface {p1, v7}, Lawz;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 35
    :cond_2
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 36
    if-eqz v0, :cond_3

    .line 37
    if-eqz p1, :cond_0

    .line 39
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_7

    .line 41
    :goto_1
    invoke-interface {p1, v7}, Lawz;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 43
    :cond_3
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 44
    const-string v0, "FilteredNumberAsyncQueryHandler.isBlockedNumber"

    const-string v2, "Device locked in FBE mode, cannot access blocked number database"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lawz;->a(Ljava/lang/Integer;)V

    goto :goto_0

    .line 47
    :cond_4
    invoke-static {p2, p3}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    iget-object v2, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v2, v0, p2}, Laxd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 49
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 50
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lawz;->a(Ljava/lang/Integer;)V

    .line 51
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 53
    :cond_5
    new-instance v2, Lawt;

    invoke-direct {v2, p0, p2, p1}, Lawt;-><init>(Lawr;Ljava/lang/String;Lawz;)V

    iget-object v3, p0, Lawr;->b:Landroid/content/Context;

    .line 54
    invoke-static {v3, v7}, Lapw;->a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lawr;->b:Landroid/content/Context;

    .line 55
    invoke-static {v5}, Lapw;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v5, p0, Lawr;->b:Landroid/content/Context;

    .line 56
    invoke-static {v5}, Lapw;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 57
    invoke-static {v4}, Lapw;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_6

    move v0, v6

    .line 58
    :goto_2
    invoke-direct {p0, v0}, Lawr;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v5, " = ?"

    invoke-virtual {v0, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/String;

    aput-object v8, v6, v1

    move-object v0, p0

    .line 59
    invoke-virtual/range {v0 .. v7}, Lawr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 57
    goto :goto_2

    :cond_7
    move-object v7, v0

    goto :goto_1
.end method

.method final a(Laxa;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 18
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19
    invoke-interface {p1, v1}, Laxa;->a(Z)V

    .line 28
    :goto_0
    return-void

    .line 21
    :cond_0
    new-instance v2, Laws;

    invoke-direct {v2, p1}, Laws;-><init>(Laxa;)V

    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    .line 22
    invoke-static {v0, v6}, Lapw;->a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    .line 23
    invoke-static {v0}, Lapw;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 24
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v5, v6

    :goto_1
    move-object v0, p0

    move-object v7, v6

    .line 27
    invoke-virtual/range {v0 .. v7}, Lawr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 26
    :cond_1
    const-string v5, "type=1"

    goto :goto_1
.end method

.method public final a(Laxb;Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 120
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 121
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0}, Lapw;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    if-eqz p1, :cond_0

    .line 123
    invoke-interface {p1, v4}, Laxb;->a(Landroid/content/ContentValues;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    const/4 v1, 0x0

    new-instance v2, Lawv;

    invoke-direct {v2, p0, p2, p1}, Lawv;-><init>(Lawr;Landroid/net/Uri;Laxb;)V

    move-object v0, p0

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lawr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Laxb;Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 116
    if-nez p2, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null id passed into unblock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    iget-object v0, p0, Lawr;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Lapw;->a(Landroid/content/Context;Ljava/lang/Integer;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lawr;->a(Laxb;Landroid/net/Uri;)V

    .line 119
    return-void
.end method

.method protected final onDeleteComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 15
    if-eqz p2, :cond_0

    .line 16
    check-cast p2, Lawx;

    invoke-virtual {p2, p3}, Lawx;->a(I)V

    .line 17
    :cond_0
    return-void
.end method

.method protected final onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 11
    if-eqz p2, :cond_0

    .line 12
    check-cast p2, Lawx;

    invoke-virtual {p2, p3}, Lawx;->a(Landroid/net/Uri;)V

    .line 13
    :cond_0
    return-void
.end method

.method protected final onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 4
    if-eqz p2, :cond_0

    .line 5
    :try_start_0
    check-cast p2, Lawx;

    invoke-virtual {p2, p3}, Lawx;->a(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_0
    if-eqz p3, :cond_1

    .line 7
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 10
    :cond_1
    return-void

    .line 8
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_2

    .line 9
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method protected final onUpdateComplete(ILjava/lang/Object;I)V
    .locals 0

    .prologue
    .line 14
    return-void
.end method
