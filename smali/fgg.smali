.class final Lfgg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfgg;->a:Landroid/content/Context;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    check-cast p1, Ljava/lang/String;

    .line 5
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lfgj;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, can\'t register"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 30
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 9
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, need Gcm token"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 11
    :cond_1
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, Gcm token needs refresh"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 14
    :cond_2
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->k(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 15
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, needs full jid"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 17
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v2}, Lfho;->l(Landroid/content/Context;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lfgg;->a:Landroid/content/Context;

    .line 18
    invoke-static {v2}, Lfmd;->x(Landroid/content/Context;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 19
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, needs full jid refreshed"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 21
    :cond_4
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->n(Landroid/content/Context;)J

    move-result-wide v0

    iget-object v2, p0, Lfgg;->a:Landroid/content/Context;

    .line 22
    invoke-static {v2}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 23
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, android id changed"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 25
    :cond_5
    iget-object v0, p0, Lfgg;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 26
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, no gaia for registered account"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 28
    :cond_6
    const-string v0, "ModuleController.shouldRegisterForIncomingCalls, don\'t need to register"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0
.end method
