.class public Lbkd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/Loader$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbkd$c;,
        Lbkd$e;,
        Lbkd$d;,
        Lbkd$a;,
        Lbkd$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:[Ljava/lang/String;


# instance fields
.field private c:Landroid/content/Context;

.field private d:I

.field private e:Lbbj;

.field private f:J

.field private g:Landroid/content/CursorLoader;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 142
    const-class v0, Lbkd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbkd;->a:Ljava/lang/String;

    .line 143
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "is_super_primary"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "data_set"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mimetype"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    sput-object v0, Lbkd;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IZLbbj;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbkd;->f:J

    .line 3
    iput-object p1, p0, Lbkd;->c:Landroid/content/Context;

    .line 4
    const/4 v0, 0x1

    iput v0, p0, Lbkd;->d:I

    .line 5
    iput-object p4, p0, Lbkd;->e:Lbbj;

    .line 6
    iput-boolean p3, p0, Lbkd;->h:Z

    .line 7
    instance-of v0, p1, Lbkd$b;

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 8
    instance-of v0, p1, Lbkd$a;

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 9
    instance-of v0, p1, Lif;

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 10
    return-void
.end method

.method private final a(I)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbkd;->c:Landroid/content/Context;

    check-cast v0, Lbkd$b;

    invoke-interface {v0, p1}, Lbkd$b;->g(I)V

    .line 58
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;IZLbbj;)V
    .locals 4

    .prologue
    .line 11
    packed-switch p2, :pswitch_data_0

    .line 14
    new-instance v0, Lbbh;

    invoke-direct {v0, p1, p4}, Lbbh;-><init>(Ljava/lang/String;Lbbj;)V

    .line 16
    iput-boolean p3, v0, Lbbh;->c:Z

    .line 20
    iget-boolean v1, p4, Lbbj;->q:Z

    .line 22
    iput-boolean v1, v0, Lbbh;->e:Z

    .line 24
    invoke-static {p0, v0}, Lbib;->a(Landroid/content/Context;Lbbh;)Landroid/content/Intent;

    move-result-object v0

    .line 25
    :goto_0
    invoke-static {p0, v0}, Lbss;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 26
    return-void

    .line 12
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string v2, "sms"

    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lbsz;Landroid/net/Uri;ZLbbj;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 27
    new-instance v7, Lbkd;

    invoke-direct {v7, p0, v6, p2, p3}, Lbkd;-><init>(Landroid/content/Context;IZLbbj;)V

    .line 29
    iget-object v0, v7, Lbkd;->c:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const-string v0, "PhoneNumberInteraction.startInteraction"

    const-string v1, "Need phone permission: CALL_PHONE"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, v7, Lbkd;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "android.permission.CALL_PHONE"

    aput-object v2, v1, v8

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lid;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 54
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v0, v7, Lbkd;->c:Landroid/content/Context;

    sget-object v1, Lbsw;->b:Ljava/util/List;

    .line 34
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 35
    array-length v0, v1

    if-lez v0, :cond_2

    .line 36
    const-string v2, "PhoneNumberInteraction.startInteraction"

    const-string v3, "Need contact permissions: "

    .line 37
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v3, v8, [Ljava/lang/Object;

    .line 38
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object v0, v7, Lbkd;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v1, v6}, Lid;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 41
    :cond_2
    iget-object v0, v7, Lbkd;->g:Landroid/content/CursorLoader;

    if-eqz v0, :cond_3

    .line 42
    iget-object v0, v7, Lbkd;->g:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->reset()V

    .line 43
    :cond_3
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 45
    const-string v1, "data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 46
    const-string v0, "data"

    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    move-object v2, p1

    .line 51
    :goto_2
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, v7, Lbkd;->c:Landroid/content/Context;

    sget-object v3, Lbkd;->b:[Ljava/lang/String;

    const-string v4, "mimetype IN (\'vnd.android.cursor.item/phone_v2\', \'vnd.android.cursor.item/sip_address\') AND data1 NOT NULL"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v7, Lbkd;->g:Landroid/content/CursorLoader;

    .line 52
    iget-object v0, v7, Lbkd;->g:Landroid/content/CursorLoader;

    invoke-virtual {v0, v8, v7}, Landroid/content/CursorLoader;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    .line 53
    iget-object v0, v7, Lbkd;->g:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->startLoading()V

    goto :goto_0

    :cond_4
    move-object v2, p1

    .line 47
    goto :goto_2

    .line 48
    :cond_5
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v2, p1

    .line 49
    goto :goto_2

    .line 50
    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x35

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Input Uri must be contact Uri or data Uri (input: \""

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Lbkd;->c:Landroid/content/Context;

    iget v1, p0, Lbkd;->d:I

    iget-boolean v2, p0, Lbkd;->h:Z

    iget-object v3, p0, Lbkd;->e:Lbbj;

    invoke-static {v0, p1, v1, v2, v3}, Lbkd;->a(Landroid/content/Context;Ljava/lang/String;IZLbbj;)V

    .line 56
    return-void
.end method


# virtual methods
.method public synthetic onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 18

    .prologue
    .line 59
    check-cast p2, Landroid/database/Cursor;

    .line 60
    if-nez p2, :cond_0

    .line 61
    const-string v2, "PhoneNumberInteraction.onLoadComplete"

    const-string v3, "null cursor"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(I)V

    .line 140
    :goto_0
    return-void

    .line 64
    :cond_0
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 65
    const/4 v3, 0x0

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Lbkd;->c:Landroid/content/Context;

    instance-of v2, v2, Lbsz;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lbkd;->c:Landroid/content/Context;

    check-cast v2, Lbsz;

    .line 69
    iget-boolean v2, v2, Lbsz;->w:Z

    .line 70
    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    .line 71
    :goto_1
    if-nez v2, :cond_3

    .line 72
    const-string v2, "PhoneNumberInteraction.onLoadComplete"

    const-string v3, "not safe to commit transaction"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 70
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 76
    :cond_3
    :try_start_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 77
    const-string v2, "contact_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 78
    const-string v2, "is_super_primary"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 79
    const-string v2, "data1"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 80
    const-string v2, "_id"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 81
    const-string v2, "account_type"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 82
    const-string v2, "data_set"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 83
    const-string v2, "data2"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 84
    const-string v2, "data3"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 85
    const-string v2, "mimetype"

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    move-object v2, v3

    .line 86
    :cond_4
    move-object/from16 v0, p0

    iget-wide v14, v0, Lbkd;->f:J

    const-wide/16 v16, -0x1

    cmp-long v3, v14, v16

    if-nez v3, :cond_5

    .line 87
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lbkd;->f:J

    .line 88
    :cond_5
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    .line 89
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    :cond_6
    new-instance v3, Lbkd$d;

    .line 91
    invoke-direct {v3}, Lbkd$d;-><init>()V

    .line 93
    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v3, Lbkd$d;->a:J

    .line 94
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v3, Lbkd$d;->b:Ljava/lang/String;

    .line 95
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v3, Lbkd$d;->c:Ljava/lang/String;

    .line 96
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v3, Lbkd$d;->d:Ljava/lang/String;

    .line 97
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    int-to-long v14, v14

    iput-wide v14, v3, Lbkd$d;->e:J

    .line 98
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v3, Lbkd$d;->f:Ljava/lang/String;

    .line 99
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v3, Lbkd$d;->g:Ljava/lang/String;

    .line 100
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 106
    if-eqz v2, :cond_8

    .line 107
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 103
    :cond_7
    const/4 v2, 0x1

    :try_start_2
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 110
    :cond_8
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbkd;->c:Landroid/content/Context;

    invoke-static {v4, v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 111
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_9

    .line 112
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :goto_2
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 113
    :cond_9
    :try_start_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 114
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbkd$d;

    .line 115
    iget-object v2, v2, Lbkd$d;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbkd;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 141
    :catchall_0
    move-exception v2

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    throw v2

    .line 118
    :cond_a
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lbkd;->c:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    .line 119
    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 120
    const-string v2, "PhoneNumberInteraction.showDisambiguationDialog"

    const-string v3, "activity finishing"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 122
    :cond_b
    invoke-virtual {v2}, Landroid/app/Activity;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 123
    const-string v2, "PhoneNumberInteraction.showDisambiguationDialog"

    const-string v3, "activity destroyed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 126
    :cond_c
    :try_start_6
    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lbkd;->d:I

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lbkd;->h:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lbkd;->e:Lbbj;

    .line 128
    new-instance v7, Lbkd$c;

    invoke-direct {v7}, Lbkd$c;-><init>()V

    .line 129
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 130
    const-string v9, "phoneList"

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 131
    const-string v4, "interactionType"

    invoke-virtual {v8, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const-string v3, "is_video_call"

    invoke-virtual {v8, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    invoke-static {v8, v6}, Lapw;->a(Landroid/os/Bundle;Lbbj;)V

    .line 134
    invoke-virtual {v7, v8}, Lbkd$c;->setArguments(Landroid/os/Bundle;)V

    .line 135
    sget-object v3, Lbkd;->a:Ljava/lang/String;

    invoke-virtual {v7, v2, v3}, Lbkd$c;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 137
    :catch_0
    move-exception v2

    .line 138
    :try_start_7
    const-string v3, "PhoneNumberInteraction.showDisambiguationDialog"

    const-string v4, "caught exception"

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2
.end method
