.class public final Lbvt;
.super Landroid/text/method/DialerKeyListener;
.source "PG"


# instance fields
.field private a:[C

.field private synthetic b:Lcom/android/incallui/DialpadFragment;


# direct methods
.method public constructor <init>(Lcom/android/incallui/DialpadFragment;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lbvt;->b:Lcom/android/incallui/DialpadFragment;

    .line 2
    invoke-direct {p0}, Landroid/text/method/DialerKeyListener;-><init>()V

    .line 3
    const/16 v0, 0xc

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    iput-object v0, p0, Lbvt;->a:[C

    .line 4
    return-void

    .line 3
    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x23s
        0x2as
    .end array-data
.end method

.method private final c(Landroid/view/KeyEvent;)C
    .locals 3

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v0

    .line 51
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getNumber()C

    move-result v1

    .line 52
    and-int/lit8 v2, v0, 0x3

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    .line 53
    :cond_0
    invoke-virtual {p0}, Lbvt;->getAcceptedChars()[C

    move-result-object v2

    invoke-virtual {p1, v2, v0}, Landroid/view/KeyEvent;->getMatch([CI)C

    move-result v0

    .line 54
    if-eqz v0, :cond_2

    :goto_0
    move v1, v0

    .line 55
    :cond_1
    int-to-char v0, v1

    return v0

    :cond_2
    move v0, v1

    .line 54
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lbvt;->c(Landroid/view/KeyEvent;)C

    move-result v1

    .line 29
    const/16 v0, 0x24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "DTMFKeyListener.onKeyDown: event \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    .line 31
    invoke-virtual {p0}, Lbvt;->getAcceptedChars()[C

    move-result-object v0

    invoke-static {v0, v1}, Lbvt;->ok([CC)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const/16 v0, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "DTMFKeyListener reading \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' from input."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lbvt;->b:Lcom/android/incallui/DialpadFragment;

    .line 34
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 35
    check-cast v0, Lbvu;

    invoke-virtual {v0, v1}, Lbvu;->a(C)V

    .line 36
    const/4 v0, 0x1

    .line 38
    :goto_0
    return v0

    .line 37
    :cond_0
    const/16 v0, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "DTMFKeyListener rejecting \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' from input."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 39
    if-nez p1, :cond_0

    move v0, v1

    .line 49
    :goto_0
    return v0

    .line 41
    :cond_0
    invoke-direct {p0, p1}, Lbvt;->c(Landroid/view/KeyEvent;)C

    move-result v0

    .line 42
    const/16 v2, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DTMFKeyListener.onKeyUp: event \'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lbvt;->getAcceptedChars()[C

    move-result-object v2

    invoke-static {v2, v0}, Lbvt;->ok([CC)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    const/16 v2, 0x19

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Stopping the tone for \'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lbvt;->b:Lcom/android/incallui/DialpadFragment;

    .line 46
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 47
    check-cast v0, Lbvu;

    invoke-virtual {v0}, Lbvu;->a()V

    move v0, v1

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final backspace(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    return v0
.end method

.method protected final getAcceptedChars()[C
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lbvt;->a:[C

    return-object v0
.end method

.method public final onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 7
    invoke-virtual {p0, p4, p2}, Lbvt;->lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I

    move-result v0

    int-to-char v1, v0

    .line 8
    invoke-virtual {p4}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/DialerKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {p0}, Lbvt;->getAcceptedChars()[C

    move-result-object v0

    invoke-static {v0, v1}, Lbvt;->ok([CC)Z

    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    const/16 v0, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "DTMFKeyListener reading \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' from input."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lbvt;->b:Lcom/android/incallui/DialpadFragment;

    .line 13
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 14
    check-cast v0, Lbvu;

    invoke-virtual {v0, v1}, Lbvu;->a(C)V

    .line 16
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    return v0

    .line 15
    :cond_0
    const/16 v0, 0x29

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "DTMFKeyListener rejecting \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' from input."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 17
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/method/DialerKeyListener;->onKeyUp(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    .line 19
    invoke-virtual {p0, p4, p2}, Lbvt;->lookup(Landroid/view/KeyEvent;Landroid/text/Spannable;)I

    move-result v0

    int-to-char v0, v0

    .line 20
    invoke-virtual {p0}, Lbvt;->getAcceptedChars()[C

    move-result-object v1

    invoke-static {v1, v0}, Lbvt;->ok([CC)Z

    move-result v1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    const/16 v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Stopping the tone for \'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lbvt;->b:Lcom/android/incallui/DialpadFragment;

    .line 24
    iget-object v0, v0, Lccj;->X:Lcck;

    .line 25
    check-cast v0, Lbvu;

    invoke-virtual {v0}, Lbvu;->a()V

    .line 26
    const/4 v0, 0x1

    .line 27
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
