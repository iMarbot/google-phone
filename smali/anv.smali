.class final Lanv;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic a:Laoq;

.field private synthetic b:Lbdd;

.field private synthetic c:J

.field private synthetic d:Lano;


# direct methods
.method constructor <init>(Lano;Laoq;Lbdd;J)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lanv;->d:Lano;

    iput-object p2, p0, Lanv;->a:Laoq;

    iput-object p3, p0, Lanv;->b:Lbdd;

    iput-wide p4, p0, Lanv;->c:J

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18

    .prologue
    .line 76
    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->a:Laoq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lanv;->d:Lano;

    .line 79
    iget-object v3, v3, Lano;->h:Lawr;

    .line 80
    move-object/from16 v0, p0

    iget-object v4, v0, Lanv;->a:Laoq;

    iget-object v4, v4, Laoq;->F:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lanv;->a:Laoq;

    iget-object v5, v5, Laoq;->K:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lawr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Laoq;->M:Ljava/lang/Integer;

    .line 81
    move-object/from16 v0, p0

    iget-object v3, v0, Lanv;->b:Lbdd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->a:Laoq;

    iget-object v2, v2, Laoq;->M:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, v3, Lbdd;->y:Z

    .line 82
    invoke-virtual/range {p0 .. p0}, Lanv;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_23

    .line 83
    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->d:Lano;

    .line 84
    iget-boolean v2, v2, Lano;->y:Z

    .line 85
    if-eqz v2, :cond_1

    .line 86
    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->a:Laoq;

    const/4 v3, 0x1

    iput-boolean v3, v2, Laoq;->S:Z

    .line 87
    move-object/from16 v0, p0

    iget-object v3, v0, Lanv;->a:Laoq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lanv;->b:Lbdd;

    .line 89
    const/4 v2, 0x0

    :goto_1
    iget-object v5, v4, Lbdd;->g:[I

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 90
    iget-object v5, v4, Lbdd;->g:[I

    aget v5, v5, v2

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    iget-object v5, v4, Lbdd;->g:[I

    aget v5, v5, v2

    const/4 v6, 0x3

    if-eq v5, v6, :cond_0

    iget-object v5, v4, Lbdd;->g:[I

    aget v5, v5, v2

    const/4 v6, 0x4

    if-eq v5, v6, :cond_0

    iget-object v5, v4, Lbdd;->g:[I

    aget v5, v5, v2

    const/4 v6, 0x5

    if-eq v5, v6, :cond_0

    iget-object v5, v4, Lbdd;->g:[I

    aget v5, v5, v2

    const/4 v6, 0x6

    if-ne v5, v6, :cond_3

    .line 91
    :cond_0
    const/4 v2, 0x1

    .line 94
    :goto_2
    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->d:Lano;

    iget-object v2, v2, Lano;->c:Landroid/app/Activity;

    .line 95
    invoke-static {v2}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lanv;->a:Laoq;

    iget-object v4, v4, Laoq;->F:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lanv;->a:Laoq;

    iget-object v5, v5, Laoq;->K:Ljava/lang/String;

    .line 96
    invoke-interface {v2, v4, v5}, Lbsd;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    iput-boolean v2, v3, Laoq;->T:Z

    .line 97
    move-object/from16 v0, p0

    iget-object v2, v0, Lanv;->b:Lbdd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lanv;->a:Laoq;

    iget-boolean v3, v3, Laoq;->T:Z

    iput-boolean v3, v2, Lbdd;->x:Z

    .line 98
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lanv;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_23

    move-object/from16 v0, p0

    iget-object v9, v0, Lanv;->d:Lano;

    move-object/from16 v0, p0

    iget-object v10, v0, Lanv;->a:Laoq;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lanv;->c:J

    move-object/from16 v0, p0

    iget-object v11, v0, Lanv;->b:Lbdd;

    .line 100
    invoke-static {}, Lbdf;->c()V

    .line 101
    iget-wide v4, v10, Laoq;->D:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 102
    const-string v2, "CallLogAdapter.loadData"

    const-string v3, "rowId of viewHolder changed after load task is issued, aborting load"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    const/4 v2, 0x0

    .line 225
    :goto_4
    if-eqz v2, :cond_23

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 226
    return-object v2

    .line 81
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 92
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 93
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 96
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 104
    :cond_6
    iget-object v2, v11, Lbdd;->B:Ljava/lang/String;

    iget-object v3, v11, Lbdd;->C:Ljava/lang/String;

    .line 105
    invoke-static {v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v12

    .line 106
    iget-object v2, v9, Lano;->e:Laqc;

    iget-object v3, v11, Lbdd;->a:Ljava/lang/CharSequence;

    .line 107
    invoke-virtual {v2, v12, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v13

    .line 108
    sget-object v5, Lbml;->a:Lbml;

    .line 109
    iget-object v2, v11, Lbdd;->a:Ljava/lang/CharSequence;

    iget v3, v11, Lbdd;->d:I

    invoke-static {v2, v3}, Lbmw;->a(Ljava/lang/CharSequence;I)Z

    move-result v2

    if-eqz v2, :cond_24

    if-nez v13, :cond_24

    .line 110
    invoke-virtual {v10}, Laoq;->d()I

    move-result v6

    .line 111
    iget-object v2, v9, Lano;->k:Laqd;

    iget-object v3, v11, Lbdd;->a:Ljava/lang/CharSequence;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v11, Lbdd;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v5, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v11, Lbdd;->e:Ljava/lang/String;

    iget-object v5, v11, Lbdd;->D:Lbml;

    int-to-long v6, v6

    iget-object v8, v9, Lano;->c:Landroid/app/Activity;

    .line 112
    invoke-static {v8}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v8

    const-string v14, "number_of_call_to_do_remote_lookup"

    const-wide/16 v16, 0x5

    .line 113
    move-wide/from16 v0, v16

    invoke-interface {v8, v14, v0, v1}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v14

    cmp-long v6, v6, v14

    if-gez v6, :cond_f

    const/4 v6, 0x1

    move v7, v6

    .line 115
    :goto_6
    new-instance v14, Laqk;

    invoke-direct {v14, v3, v4}, Laqk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v6, v2, Laqd;->a:Lbsu;

    invoke-virtual {v6, v14}, Lbsu;->a(Ljava/lang/Object;)Lbsv;

    move-result-object v15

    .line 117
    if-nez v15, :cond_10

    const/4 v8, 0x0

    .line 118
    :goto_7
    if-eqz v7, :cond_11

    .line 119
    const/4 v7, 0x1

    .line 121
    :goto_8
    if-nez v15, :cond_12

    .line 122
    iget-object v6, v2, Laqd;->a:Lbsu;

    sget-object v8, Lbml;->a:Lbml;

    invoke-virtual {v6, v14, v8}, Lbsu;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 124
    const/4 v6, 0x1

    invoke-virtual/range {v2 .. v7}, Laqd;->a(Ljava/lang/String;Ljava/lang/String;Lbml;ZI)V

    :cond_7
    :goto_9
    move-object v8, v5

    .line 136
    :goto_a
    iget-object v2, v8, Lbml;->i:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 137
    const/4 v5, 0x0

    .line 139
    :goto_b
    iget-object v2, v9, Lano;->c:Landroid/app/Activity;

    .line 140
    iget-object v3, v11, Lbdd;->a:Ljava/lang/CharSequence;

    iget v4, v11, Lbdd;->d:I

    iget-object v6, v11, Lbdd;->b:Ljava/lang/String;

    move v7, v13

    .line 141
    invoke-static/range {v2 .. v7}, Lapw;->a(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)Ljava/lang/CharSequence;

    move-result-object v2

    .line 142
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v11, Lbdd;->u:Ljava/lang/String;

    .line 143
    iget-object v2, v11, Lbdd;->u:Ljava/lang/String;

    iput-object v2, v10, Laoq;->H:Ljava/lang/String;

    .line 144
    iput-object v12, v10, Laoq;->N:Landroid/telecom/PhoneAccountHandle;

    .line 145
    iput-object v12, v11, Lbdd;->q:Landroid/telecom/PhoneAccountHandle;

    .line 146
    iget-object v2, v8, Lbml;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v8, Lbml;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 147
    :cond_8
    iget-object v2, v8, Lbml;->b:Landroid/net/Uri;

    .line 148
    iget-object v2, v8, Lbml;->d:Ljava/lang/String;

    iput-object v2, v11, Lbdd;->j:Ljava/lang/CharSequence;

    .line 149
    iget-object v2, v8, Lbml;->e:Ljava/lang/String;

    iput-object v2, v11, Lbdd;->k:Ljava/lang/CharSequence;

    .line 150
    iget-object v2, v9, Lano;->x:Lalj;

    invoke-virtual {v2}, Lalj;->b()I

    move-result v2

    iput v2, v11, Lbdd;->l:I

    .line 151
    iget v2, v8, Lbml;->f:I

    iput v2, v11, Lbdd;->m:I

    .line 152
    iget-object v2, v8, Lbml;->g:Ljava/lang/String;

    iput-object v2, v11, Lbdd;->n:Ljava/lang/CharSequence;

    .line 153
    iget-object v2, v8, Lbml;->m:Landroid/net/Uri;

    iput-object v2, v11, Lbdd;->o:Landroid/net/Uri;

    .line 154
    iget-object v2, v8, Lbml;->q:Lbko$a;

    iput-object v2, v11, Lbdd;->p:Lbko$a;

    .line 155
    iget-object v2, v8, Lbml;->o:Ljava/lang/String;

    .line 156
    iget-wide v2, v8, Lbml;->p:J

    iput-wide v2, v11, Lbdd;->v:J

    .line 157
    :cond_9
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 158
    invoke-virtual {v10}, Laoq;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v11, Lbdd;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, v8, Lbml;->j:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, v11, Lbdd;->o:Landroid/net/Uri;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, v8, Lbml;->m:Landroid/net/Uri;

    aput-object v4, v2, v3

    .line 159
    iget-object v2, v8, Lbml;->j:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 160
    iget-object v2, v8, Lbml;->j:Ljava/lang/String;

    iput-object v2, v11, Lbdd;->f:Ljava/lang/String;

    .line 161
    :cond_a
    iput-object v8, v10, Laoq;->R:Lbml;

    .line 162
    iget-object v2, v9, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 163
    iget-object v3, v11, Lbdd;->p:Lbko$a;

    sget-object v4, Lbko$a;->f:Lbko$a;

    if-eq v3, v4, :cond_b

    iget-object v3, v11, Lbdd;->p:Lbko$a;

    sget-object v4, Lbko$a;->g:Lbko$a;

    if-ne v3, v4, :cond_17

    .line 164
    :cond_b
    const-string v2, ""

    .line 168
    :goto_c
    iput-object v2, v10, Laoq;->J:Ljava/lang/String;

    .line 169
    iget-object v5, v9, Lano;->j:Laop;

    .line 170
    invoke-static {}, Lbdf;->c()V

    .line 171
    iget-object v2, v5, Laop;->a:Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    invoke-virtual {v2, v11}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v11, Lbdd;->z:Ljava/lang/CharSequence;

    .line 173
    invoke-static {v11}, Laop;->a(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 174
    iget-object v2, v5, Laop;->a:Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    invoke-virtual {v2, v11}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 175
    iget-object v2, v5, Laop;->a:Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    invoke-virtual {v2, v11}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 176
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 177
    iget-object v2, v11, Lbdd;->g:[I

    array-length v2, v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_c

    .line 178
    iget-object v2, v5, Laop;->b:Landroid/content/res/Resources;

    const v3, 0x7f110119

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v12, v11, Lbdd;->g:[I

    array-length v12, v12

    .line 179
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, v10

    invoke-virtual {v2, v3, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 180
    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 181
    :cond_c
    iget v2, v11, Lbdd;->r:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    .line 182
    iget-object v2, v5, Laop;->b:Landroid/content/res/Resources;

    const v3, 0x7f11012a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 183
    :cond_d
    iget-object v2, v5, Laop;->c:Laqc;

    iget-object v3, v11, Lbdd;->q:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v2, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v2

    .line 184
    iget-object v9, v5, Laop;->b:Landroid/content/res/Resources;

    iget-object v3, v11, Lbdd;->c:Ljava/lang/String;

    .line 186
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1a

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1a

    .line 187
    const v10, 0x7f110129

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    const/4 v2, 0x1

    aput-object v3, v12, v2

    .line 188
    invoke-virtual {v9, v10, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-static {v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v3

    .line 191
    if-nez v3, :cond_19

    .line 202
    :cond_e
    :goto_d
    iget-object v3, v11, Lbdd;->g:[I

    iget-boolean v9, v11, Lbdd;->w:Z

    .line 204
    array-length v10, v3

    if-lez v10, :cond_1d

    .line 205
    const/4 v10, 0x0

    aget v3, v3, v10

    .line 208
    :goto_e
    const/4 v10, 0x3

    if-ne v3, v10, :cond_1e

    .line 209
    const v3, 0x7f110117

    .line 217
    :goto_f
    iget-object v5, v5, Laop;->b:Landroid/content/res/Resources;

    .line 218
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x4

    new-array v9, v3, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v6, v9, v3

    const/4 v6, 0x1

    .line 219
    if-nez v4, :cond_22

    const-string v3, ""

    :goto_10
    aput-object v3, v9, v6

    const/4 v3, 0x2

    aput-object v7, v9, v3

    const/4 v3, 0x3

    aput-object v2, v9, v3

    .line 220
    invoke-static {v5, v9}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 221
    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 223
    iput-object v8, v11, Lbdd;->A:Ljava/lang/CharSequence;

    .line 224
    const/4 v2, 0x1

    goto/16 :goto_4

    .line 113
    :cond_f
    const/4 v6, 0x0

    move v7, v6

    goto/16 :goto_6

    .line 117
    :cond_10
    invoke-virtual {v15}, Lbsv;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lbml;

    move-object v8, v6

    goto/16 :goto_7

    .line 120
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_8

    .line 125
    :cond_12
    invoke-virtual {v15}, Lbsv;->b()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 126
    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Laqd;->a(Ljava/lang/String;Ljava/lang/String;Lbml;ZI)V

    .line 132
    :cond_13
    :goto_11
    sget-object v2, Lbml;->a:Lbml;

    invoke-static {v8, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object v5, v8

    goto/16 :goto_9

    .line 128
    :cond_14
    iget-object v6, v5, Lbml;->d:Ljava/lang/String;

    iget-object v14, v8, Lbml;->d:Ljava/lang/String;

    invoke-static {v6, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_15

    iget v6, v5, Lbml;->f:I

    iget v14, v8, Lbml;->f:I

    if-ne v6, v14, :cond_15

    iget-object v6, v5, Lbml;->g:Ljava/lang/String;

    iget-object v14, v8, Lbml;->g:Ljava/lang/String;

    .line 129
    invoke-static {v6, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_15

    const/4 v6, 0x1

    .line 130
    :goto_12
    if-nez v6, :cond_13

    .line 131
    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Laqd;->a(Ljava/lang/String;Ljava/lang/String;Lbml;ZI)V

    goto :goto_11

    .line 129
    :cond_15
    const/4 v6, 0x0

    goto :goto_12

    .line 138
    :cond_16
    iget-object v2, v8, Lbml;->i:Ljava/lang/String;

    invoke-static {v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    goto/16 :goto_b

    .line 165
    :cond_17
    iget v3, v11, Lbdd;->m:I

    if-nez v3, :cond_18

    iget-object v3, v11, Lbdd;->n:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 166
    const-string v2, ""

    goto/16 :goto_c

    .line 167
    :cond_18
    iget v3, v11, Lbdd;->m:I

    iget-object v4, v11, Lbdd;->n:Ljava/lang/CharSequence;

    invoke-static {v2, v3, v4}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto/16 :goto_c

    :cond_19
    move-object v2, v3

    .line 191
    goto/16 :goto_d

    .line 192
    :cond_1a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1b

    .line 193
    const v2, 0x7f110128

    .line 194
    invoke-static {v9, v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 195
    if-nez v2, :cond_e

    move-object v2, v3

    goto/16 :goto_d

    .line 196
    :cond_1b
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 197
    const v3, 0x7f11011b

    .line 198
    invoke-virtual {v9, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/CharSequence;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    .line 199
    invoke-static {v3, v9}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_d

    .line 200
    :cond_1c
    const-string v2, ""

    goto/16 :goto_d

    .line 206
    :cond_1d
    const/4 v3, 0x3

    goto/16 :goto_e

    .line 210
    :cond_1e
    const/4 v10, 0x1

    if-ne v3, v10, :cond_1f

    .line 211
    const v3, 0x7f110116

    goto/16 :goto_f

    .line 212
    :cond_1f
    const/4 v10, 0x4

    if-ne v3, v10, :cond_21

    .line 213
    if-eqz v9, :cond_20

    const v3, 0x7f11011f

    goto/16 :goto_f

    :cond_20
    const v3, 0x7f110126

    goto/16 :goto_f

    .line 214
    :cond_21
    const v3, 0x7f11011a

    goto/16 :goto_f

    :cond_22
    move-object v3, v4

    .line 219
    goto/16 :goto_10

    .line 225
    :cond_23
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_24
    move-object v8, v5

    goto/16 :goto_a
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 13

    .prologue
    const/16 v1, 0x8

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 2
    check-cast p1, Ljava/lang/Boolean;

    .line 3
    iget-object v0, p0, Lanv;->a:Laoq;

    iput-boolean v12, v0, Laoq;->w:Z

    .line 4
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    iget-object v2, p0, Lanv;->a:Laoq;

    iget-object v0, p0, Lanv;->d:Lano;

    iget-object v3, p0, Lanv;->a:Laoq;

    iget-wide v4, v3, Laoq;->D:J

    .line 7
    iget-object v0, v0, Lano;->v:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 8
    if-eqz v0, :cond_1

    .line 9
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 11
    :goto_0
    iput v0, v2, Laoq;->W:I

    .line 12
    iget-object v0, p0, Lanv;->d:Lano;

    iget-object v2, p0, Lanv;->a:Laoq;

    iget-wide v2, v2, Laoq;->D:J

    .line 13
    invoke-virtual {v0, v2, v3}, Lano;->a(J)I

    move-result v0

    .line 15
    iget-object v2, p0, Lanv;->b:Lbdd;

    iget v2, v2, Lbdd;->E:I

    if-eq v0, v2, :cond_4

    .line 16
    iget-object v2, p0, Lanv;->a:Laoq;

    iput v9, v2, Laoq;->X:I

    .line 17
    iget-object v2, p0, Lanv;->a:Laoq;

    iget-object v3, p0, Lanv;->d:Lano;

    .line 19
    if-nez v0, :cond_2

    .line 20
    iget-object v0, v3, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11009c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    :goto_1
    iput-object v0, v2, Laoq;->Y:Ljava/lang/CharSequence;

    .line 26
    :goto_2
    iget-object v10, p0, Lanv;->d:Lano;

    iget-object v11, p0, Lanv;->a:Laoq;

    iget-object v2, p0, Lanv;->b:Lbdd;

    iget-wide v4, p0, Lanv;->c:J

    .line 28
    invoke-static {}, Lbdf;->b()V

    .line 29
    iget-wide v6, v11, Laoq;->D:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    .line 30
    const-string v0, "CallLogAdapter.render"

    const-string v1, "rowId of viewHolder changed after load task is issued, aborting render"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v9

    .line 10
    goto :goto_0

    .line 21
    :cond_2
    if-ne v0, v12, :cond_3

    .line 22
    iget-object v0, v3, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11009d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 23
    :cond_3
    iget-object v0, v3, Lano;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f11009b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 25
    :cond_4
    iget-object v0, p0, Lanv;->a:Laoq;

    iput v1, v0, Laoq;->X:I

    goto :goto_2

    .line 32
    :cond_5
    iget-object v0, v11, Laoq;->q:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 33
    iget-object v3, v11, Laoq;->B:Landroid/widget/ImageView;

    .line 34
    iget-wide v4, v2, Lbdd;->v:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_9

    move v0, v9

    .line 35
    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 36
    iget-boolean v0, v10, Lano;->p:Z

    if-eqz v0, :cond_6

    iget-object v0, v11, Laoq;->O:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 37
    iget-object v0, v10, Lano;->r:Landroid/util/SparseArray;

    iget-object v3, v11, Laoq;->O:Ljava/lang/String;

    invoke-static {v3}, Lano;->a(Ljava/lang/String;)I

    move-result v3

    iget-object v4, v11, Laoq;->O:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    :cond_6
    iget-boolean v0, v10, Lano;->q:Z

    if-eqz v0, :cond_7

    iget-object v0, v11, Laoq;->O:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 39
    iget-object v0, v10, Lano;->r:Landroid/util/SparseArray;

    iget-object v3, v11, Laoq;->O:Ljava/lang/String;

    invoke-static {v3}, Lano;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->delete(I)V

    .line 40
    :cond_7
    iget-object v0, v11, Laoq;->O:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, v10, Lano;->r:Landroid/util/SparseArray;

    iget-object v3, v11, Laoq;->O:Ljava/lang/String;

    .line 41
    invoke-static {v3}, Lano;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 42
    iget-object v0, v11, Laoq;->C:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 43
    iget-object v0, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setVisibility(I)V

    .line 47
    :cond_8
    :goto_5
    iget-object v1, v10, Lano;->j:Laop;

    .line 48
    iget-object v0, v1, Laop;->a:Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    iget-object v3, v11, Laoq;->r:Lapt;

    invoke-virtual {v0, v3, v2}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a(Lapt;Lbdd;)V

    .line 49
    iget-object v3, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    .line 50
    iget-boolean v0, v2, Lbdd;->x:Z

    if-eqz v0, :cond_b

    .line 51
    iget-object v0, v1, Laop;->b:Landroid/content/res/Resources;

    const v4, 0x7f110124

    new-array v5, v12, [Ljava/lang/Object;

    .line 52
    invoke-static {v2}, Laop;->a(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v5, v9

    .line 53
    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_6
    invoke-virtual {v3, v0}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, v11, Laoq;->q:Landroid/view/View;

    iget-object v3, v2, Lbdd;->A:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 57
    invoke-static {v2}, Laop;->a(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v11, Laoq;->P:Ljava/lang/CharSequence;

    .line 58
    iget-object v0, v1, Laop;->a:Lcom/android/dialer/configprovider/SharedPrefConfigProvider;

    invoke-virtual {v0, v2}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v11, Laoq;->Q:Ljava/lang/CharSequence;

    .line 59
    iget-object v0, v2, Lbdd;->e:Ljava/lang/String;

    iput-object v0, v11, Laoq;->K:Ljava/lang/String;

    .line 61
    iget-object v0, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    iget-object v1, v11, Laoq;->R:Lbml;

    iget-object v1, v1, Lbml;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    .line 62
    iget-boolean v0, v11, Laoq;->S:Z

    if-eqz v0, :cond_c

    iget-boolean v0, v11, Laoq;->T:Z

    if-eqz v0, :cond_c

    .line 63
    iget-object v0, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    iget-object v1, v11, Laoq;->u:Landroid/content/Context;

    const v2, 0x7f020060

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    :goto_7
    iget-wide v0, v10, Lano;->m:J

    iget-wide v2, v11, Laoq;->D:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    .line 70
    invoke-virtual {v11}, Laoq;->d()I

    move-result v0

    iput v0, v10, Lano;->l:I

    .line 71
    invoke-virtual {v11, v12}, Laoq;->b(Z)V

    .line 73
    :goto_8
    iget-object v0, v11, Laoq;->s:Landroid/widget/TextView;

    iget v1, v11, Laoq;->X:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    iget-object v0, v11, Laoq;->s:Landroid/widget/TextView;

    iget-object v1, v11, Laoq;->Y:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_9
    move v0, v1

    .line 34
    goto/16 :goto_4

    .line 44
    :cond_a
    iget-object v0, v11, Laoq;->O:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 45
    iget-object v0, v11, Laoq;->C:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 46
    iget-object v0, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    invoke-virtual {v0, v9}, Lcom/android/dialer/app/calllog/DialerQuickContactBadge;->setVisibility(I)V

    goto/16 :goto_5

    .line 54
    :cond_b
    iget-object v0, v1, Laop;->b:Landroid/content/res/Resources;

    const v4, 0x7f11010c

    new-array v5, v12, [Ljava/lang/Object;

    invoke-static {v2}, Laop;->a(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 65
    :cond_c
    iget-object v0, v11, Laoq;->R:Lbml;

    iget-object v0, v0, Lbml;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v7, v11, Laoq;->H:Ljava/lang/String;

    .line 66
    :goto_9
    iget-object v0, v11, Laoq;->u:Landroid/content/Context;

    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, v11, Laoq;->p:Lcom/android/dialer/app/calllog/DialerQuickContactBadge;

    iget-object v0, v11, Laoq;->R:Lbml;

    iget-object v3, v0, Lbml;->b:Landroid/net/Uri;

    iget-object v0, v11, Laoq;->R:Lbml;

    iget-wide v4, v0, Lbml;->l:J

    iget-object v0, v11, Laoq;->R:Lbml;

    iget-object v6, v0, Lbml;->m:Landroid/net/Uri;

    .line 67
    invoke-virtual {v11}, Laoq;->u()I

    move-result v8

    .line 68
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_7

    .line 65
    :cond_d
    iget-object v0, v11, Laoq;->R:Lbml;

    iget-object v7, v0, Lbml;->d:Ljava/lang/String;

    goto :goto_9

    .line 72
    :cond_e
    invoke-virtual {v11, v9}, Laoq;->b(Z)V

    goto :goto_8
.end method
