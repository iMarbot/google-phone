.class public final Lglc;
.super Lhft;
.source "PG"


# static fields
.field private static volatile a:[Lglc;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lgwy;

.field private f:Ljava/lang/String;

.field private g:[Lgld;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 9
    iput-object v1, p0, Lglc;->b:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lglc;->c:Ljava/lang/String;

    .line 11
    iput-object v1, p0, Lglc;->d:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lglc;->e:Lgwy;

    .line 13
    iput-object v1, p0, Lglc;->f:Ljava/lang/String;

    .line 14
    invoke-static {}, Lgld;->a()[Lgld;

    move-result-object v0

    iput-object v0, p0, Lglc;->g:[Lgld;

    .line 15
    iput-object v1, p0, Lglc;->h:[B

    .line 16
    iput-object v1, p0, Lglc;->unknownFieldData:Lhfv;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lglc;->cachedSize:I

    .line 18
    return-void
.end method

.method public static a()[Lglc;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lglc;->a:[Lglc;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lglc;->a:[Lglc;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lglc;

    sput-object v0, Lglc;->a:[Lglc;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lglc;->a:[Lglc;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 39
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 40
    iget-object v1, p0, Lglc;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 41
    const/4 v1, 0x1

    iget-object v2, p0, Lglc;->b:Ljava/lang/String;

    .line 42
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    :cond_0
    iget-object v1, p0, Lglc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x2

    iget-object v2, p0, Lglc;->c:Ljava/lang/String;

    .line 45
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lglc;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x3

    iget-object v2, p0, Lglc;->d:Ljava/lang/String;

    .line 48
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_2
    iget-object v1, p0, Lglc;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 50
    const/4 v1, 0x4

    iget-object v2, p0, Lglc;->f:Ljava/lang/String;

    .line 51
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_3
    iget-object v1, p0, Lglc;->h:[B

    if-eqz v1, :cond_4

    .line 53
    const/4 v1, 0x5

    iget-object v2, p0, Lglc;->h:[B

    .line 54
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_4
    iget-object v1, p0, Lglc;->g:[Lgld;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lglc;->g:[Lgld;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 56
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lglc;->g:[Lgld;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 57
    iget-object v2, p0, Lglc;->g:[Lgld;

    aget-object v2, v2, v0

    .line 58
    if-eqz v2, :cond_5

    .line 59
    const/4 v3, 0x6

    .line 60
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 61
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 62
    :cond_7
    iget-object v1, p0, Lglc;->e:Lgwy;

    if-eqz v1, :cond_8

    .line 63
    const/4 v1, 0x7

    iget-object v2, p0, Lglc;->e:Lgwy;

    .line 64
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    .line 67
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 68
    sparse-switch v0, :sswitch_data_0

    .line 70
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglc;->b:Ljava/lang/String;

    goto :goto_0

    .line 74
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglc;->c:Ljava/lang/String;

    goto :goto_0

    .line 76
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglc;->d:Ljava/lang/String;

    goto :goto_0

    .line 78
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lglc;->f:Ljava/lang/String;

    goto :goto_0

    .line 80
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lglc;->h:[B

    goto :goto_0

    .line 82
    :sswitch_6
    const/16 v0, 0x32

    .line 83
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 84
    iget-object v0, p0, Lglc;->g:[Lgld;

    if-nez v0, :cond_2

    move v0, v1

    .line 85
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgld;

    .line 86
    if-eqz v0, :cond_1

    .line 87
    iget-object v3, p0, Lglc;->g:[Lgld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 89
    new-instance v3, Lgld;

    invoke-direct {v3}, Lgld;-><init>()V

    aput-object v3, v2, v0

    .line 90
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 91
    invoke-virtual {p1}, Lhfp;->a()I

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 84
    :cond_2
    iget-object v0, p0, Lglc;->g:[Lgld;

    array-length v0, v0

    goto :goto_1

    .line 93
    :cond_3
    new-instance v3, Lgld;

    invoke-direct {v3}, Lgld;-><init>()V

    aput-object v3, v2, v0

    .line 94
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 95
    iput-object v2, p0, Lglc;->g:[Lgld;

    goto :goto_0

    .line 97
    :sswitch_7
    iget-object v0, p0, Lglc;->e:Lgwy;

    if-nez v0, :cond_4

    .line 98
    new-instance v0, Lgwy;

    invoke-direct {v0}, Lgwy;-><init>()V

    iput-object v0, p0, Lglc;->e:Lgwy;

    .line 99
    :cond_4
    iget-object v0, p0, Lglc;->e:Lgwy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 68
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lglc;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v1, p0, Lglc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_0
    iget-object v0, p0, Lglc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x2

    iget-object v1, p0, Lglc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_1
    iget-object v0, p0, Lglc;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 24
    const/4 v0, 0x3

    iget-object v1, p0, Lglc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_2
    iget-object v0, p0, Lglc;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 26
    const/4 v0, 0x4

    iget-object v1, p0, Lglc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_3
    iget-object v0, p0, Lglc;->h:[B

    if-eqz v0, :cond_4

    .line 28
    const/4 v0, 0x5

    iget-object v1, p0, Lglc;->h:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 29
    :cond_4
    iget-object v0, p0, Lglc;->g:[Lgld;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lglc;->g:[Lgld;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 30
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lglc;->g:[Lgld;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 31
    iget-object v1, p0, Lglc;->g:[Lgld;

    aget-object v1, v1, v0

    .line 32
    if-eqz v1, :cond_5

    .line 33
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_6
    iget-object v0, p0, Lglc;->e:Lgwy;

    if-eqz v0, :cond_7

    .line 36
    const/4 v0, 0x7

    iget-object v1, p0, Lglc;->e:Lgwy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 37
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 38
    return-void
.end method
