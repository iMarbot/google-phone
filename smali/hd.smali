.class Lhd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Landroid/animation/LayoutTransition;

.field public static b:Ljava/lang/reflect/Field;

.field public static c:Z

.field public static d:Ljava/lang/reflect/Method;

.field public static e:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/animation/LayoutTransition;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 43
    sget-boolean v0, Lhd;->e:Z

    if-nez v0, :cond_0

    .line 44
    :try_start_0
    const-class v0, Landroid/animation/LayoutTransition;

    const-string v1, "cancel"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 45
    sput-object v0, Lhd;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    .line 48
    :goto_0
    sput-boolean v3, Lhd;->e:Z

    .line 49
    :cond_0
    sget-object v0, Lhd;->d:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    .line 50
    :try_start_1
    sget-object v0, Lhd;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0

    .line 55
    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    .line 53
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Lgz;
    .locals 1

    .prologue
    .line 1
    .line 2
    invoke-static {p1}, Lhe;->c(Landroid/view/View;)Lhe;

    move-result-object v0

    check-cast v0, Lgx;

    .line 3
    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;Z)V
    .locals 6

    .prologue
    const v5, 0x7f0e0048

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 4
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    if-nez v1, :cond_0

    .line 5
    new-instance v1, Lhb;

    invoke-direct {v1, p0}, Lhb;-><init>(Lhd;)V

    .line 6
    sput-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 7
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    invoke-virtual {v1, v0, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 8
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    invoke-virtual {v1, v4, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 9
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 10
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 11
    :cond_0
    if-eqz p2, :cond_4

    .line 12
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 13
    if-eqz v0, :cond_2

    .line 14
    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15
    invoke-static {v0}, Lhd;->a(Landroid/animation/LayoutTransition;)V

    .line 16
    :cond_1
    sget-object v1, Lhd;->a:Landroid/animation/LayoutTransition;

    if-eq v0, v1, :cond_2

    .line 17
    invoke-virtual {p1, v5, v0}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 18
    :cond_2
    sget-object v0, Lhd;->a:Landroid/animation/LayoutTransition;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 41
    :cond_3
    :goto_0
    return-void

    .line 20
    :cond_4
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 21
    sget-boolean v1, Lhd;->c:Z

    if-nez v1, :cond_5

    .line 22
    :try_start_0
    const-class v1, Landroid/view/ViewGroup;

    const-string v2, "mLayoutSuppressed"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 23
    sput-object v1, Lhd;->b:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 26
    :goto_1
    sput-boolean v4, Lhd;->c:Z

    .line 28
    :cond_5
    sget-object v1, Lhd;->b:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_6

    .line 29
    :try_start_1
    sget-object v1, Lhd;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    .line 30
    if-eqz v0, :cond_6

    .line 31
    sget-object v1, Lhd;->b:Ljava/lang/reflect/Field;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    .line 34
    :cond_6
    :goto_2
    if-eqz v0, :cond_7

    .line 35
    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 37
    :cond_7
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/LayoutTransition;

    .line 38
    if-eqz v0, :cond_3

    .line 39
    invoke-virtual {p1, v5, v3}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 40
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method
