.class public final Leme;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/Status;

.field public static final b:Lesq;

.field private static c:Letf;

.field private static d:Ledb;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Leme;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v0, Letf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Letf;-><init>(B)V

    sput-object v0, Leme;->c:Letf;

    new-instance v0, Lemn;

    invoke-direct {v0}, Lemn;-><init>()V

    sput-object v0, Leme;->d:Ledb;

    new-instance v0, Lesq;

    const-string v1, "Feedback.API"

    sget-object v2, Leme;->d:Ledb;

    sget-object v3, Leme;->c:Letf;

    invoke-direct {v0, v1, v2, v3}, Lesq;-><init>(Ljava/lang/String;Ledb;Letf;)V

    sput-object v0, Leme;->b:Lesq;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ledh;
    .locals 1

    new-instance v0, Ledh;

    invoke-direct {v0, p0}, Ledh;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static a(Ledj;Landroid/os/Bundle;J)Ledn;
    .locals 2

    new-instance v0, Lemr;

    invoke-direct {v0, p0, p1, p2, p3}, Lemr;-><init>(Ledj;Landroid/os/Bundle;J)V

    invoke-virtual {p0, v0}, Ledj;->a(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ledj;Lemg;)Ledn;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {p0}, Ledj;->b()Landroid/content/Context;

    move-result-object v3

    new-instance v0, Lemp;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lemp;-><init>(Ledj;Lemg;Landroid/content/Context;J)V

    invoke-virtual {p0, v0}, Ledj;->a(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ledj;Lemg;Landroid/os/Bundle;J)Ledn;
    .locals 7

    new-instance v0, Lemo;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lemo;-><init>(Ledj;Lemg;Landroid/os/Bundle;J)V

    invoke-virtual {p0, v0}, Ledj;->a(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Letf;Ljava/io/File;J)V
    .locals 7

    .prologue
    .line 1
    .line 2
    new-instance v0, Leqt;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Leqt;-><init>(Landroid/content/Context;Letf;Ljava/io/File;J)V

    invoke-static {v0}, Leme;->a(Ljava/lang/Runnable;)V

    new-instance v0, Lequ;

    invoke-direct {v0, p0, p1, p3, p4}, Lequ;-><init>(Landroid/content/Context;Letf;J)V

    invoke-static {v0}, Leme;->a(Ljava/lang/Runnable;)V

    .line 3
    return-void
.end method

.method private static a(Ljava/lang/Runnable;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "Feedback"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static b(Ledj;Lemg;)Ledn;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lemq;

    invoke-direct {v0, p0, p1}, Lemq;-><init>(Ledj;Lemg;)V

    invoke-virtual {p0, v0}, Ledj;->a(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method
