.class public final Lgpc;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpc;


# instance fields
.field public hangoutId:Ljava/lang/String;

.field public participantId:Ljava/lang/String;

.field public requestHeader:Lgls;

.field public resourceId:[Ljava/lang/String;

.field public sourceId:Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpc;->clear()Lgpc;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpc;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpc;->_emptyArray:[Lgpc;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpc;->_emptyArray:[Lgpc;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpc;

    sput-object v0, Lgpc;->_emptyArray:[Lgpc;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpc;->_emptyArray:[Lgpc;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpc;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lgpc;

    invoke-direct {v0}, Lgpc;-><init>()V

    invoke-virtual {v0, p0}, Lgpc;->mergeFrom(Lhfp;)Lgpc;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpc;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lgpc;

    invoke-direct {v0}, Lgpc;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpc;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpc;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgpc;->requestHeader:Lgls;

    .line 11
    iput-object v1, p0, Lgpc;->hangoutId:Ljava/lang/String;

    .line 12
    iput-object v1, p0, Lgpc;->participantId:Ljava/lang/String;

    .line 13
    iput-object v1, p0, Lgpc;->sourceId:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lgpc;->syncMetadata:Lgoa;

    .line 15
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgpc;->resourceId:[Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lgpc;->unknownFieldData:Lhfv;

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lgpc;->cachedSize:I

    .line 18
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 38
    iget-object v2, p0, Lgpc;->requestHeader:Lgls;

    if-eqz v2, :cond_0

    .line 39
    const/4 v2, 0x1

    iget-object v3, p0, Lgpc;->requestHeader:Lgls;

    .line 40
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41
    :cond_0
    iget-object v2, p0, Lgpc;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 42
    const/4 v2, 0x2

    iget-object v3, p0, Lgpc;->hangoutId:Ljava/lang/String;

    .line 43
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 44
    :cond_1
    iget-object v2, p0, Lgpc;->participantId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 45
    const/4 v2, 0x3

    iget-object v3, p0, Lgpc;->participantId:Ljava/lang/String;

    .line 46
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 47
    :cond_2
    iget-object v2, p0, Lgpc;->sourceId:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 48
    const/4 v2, 0x4

    iget-object v3, p0, Lgpc;->sourceId:Ljava/lang/String;

    .line 49
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 50
    :cond_3
    iget-object v2, p0, Lgpc;->syncMetadata:Lgoa;

    if-eqz v2, :cond_4

    .line 51
    const/4 v2, 0x5

    iget-object v3, p0, Lgpc;->syncMetadata:Lgoa;

    .line 52
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 53
    :cond_4
    iget-object v2, p0, Lgpc;->resourceId:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 56
    :goto_0
    iget-object v4, p0, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 57
    iget-object v4, p0, Lgpc;->resourceId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 58
    if-eqz v4, :cond_5

    .line 59
    add-int/lit8 v3, v3, 0x1

    .line 61
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 62
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_6
    add-int/2addr v0, v2

    .line 64
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 65
    :cond_7
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 69
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :sswitch_0
    return-object p0

    .line 71
    :sswitch_1
    iget-object v0, p0, Lgpc;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 72
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgpc;->requestHeader:Lgls;

    .line 73
    :cond_1
    iget-object v0, p0, Lgpc;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 75
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpc;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 77
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpc;->participantId:Ljava/lang/String;

    goto :goto_0

    .line 79
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpc;->sourceId:Ljava/lang/String;

    goto :goto_0

    .line 81
    :sswitch_5
    iget-object v0, p0, Lgpc;->syncMetadata:Lgoa;

    if-nez v0, :cond_2

    .line 82
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgpc;->syncMetadata:Lgoa;

    .line 83
    :cond_2
    iget-object v0, p0, Lgpc;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 85
    :sswitch_6
    const/16 v0, 0x32

    .line 86
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 87
    iget-object v0, p0, Lgpc;->resourceId:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 88
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 89
    if-eqz v0, :cond_3

    .line 90
    iget-object v3, p0, Lgpc;->resourceId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 92
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 93
    invoke-virtual {p1}, Lhfp;->a()I

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 87
    :cond_4
    iget-object v0, p0, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 95
    :cond_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 96
    iput-object v2, p0, Lgpc;->resourceId:[Ljava/lang/String;

    goto :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lgpc;->mergeFrom(Lhfp;)Lgpc;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lgpc;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 20
    const/4 v0, 0x1

    iget-object v1, p0, Lgpc;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_0
    iget-object v0, p0, Lgpc;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 22
    const/4 v0, 0x2

    iget-object v1, p0, Lgpc;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 23
    :cond_1
    iget-object v0, p0, Lgpc;->participantId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 24
    const/4 v0, 0x3

    iget-object v1, p0, Lgpc;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 25
    :cond_2
    iget-object v0, p0, Lgpc;->sourceId:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 26
    const/4 v0, 0x4

    iget-object v1, p0, Lgpc;->sourceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 27
    :cond_3
    iget-object v0, p0, Lgpc;->syncMetadata:Lgoa;

    if-eqz v0, :cond_4

    .line 28
    const/4 v0, 0x5

    iget-object v1, p0, Lgpc;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 29
    :cond_4
    iget-object v0, p0, Lgpc;->resourceId:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 30
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgpc;->resourceId:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 31
    iget-object v1, p0, Lgpc;->resourceId:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 32
    if-eqz v1, :cond_5

    .line 33
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_6
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 36
    return-void
.end method
