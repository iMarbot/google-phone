.class public final Lgvi;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgvi$b;,
        Lgvi$a;,
        Lgvi$c;
    }
.end annotation


# static fields
.field public static final r:Lgvi;

.field private static volatile s:Lhdm;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:I

.field public n:I

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 249
    new-instance v0, Lgvi;

    invoke-direct {v0}, Lgvi;-><init>()V

    .line 250
    sput-object v0, Lgvi;->r:Lgvi;

    invoke-virtual {v0}, Lgvi;->makeImmutable()V

    .line 251
    const-class v0, Lgvi;

    sget-object v1, Lgvi;->r:Lgvi;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 252
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lgvi;->b:Ljava/lang/String;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lgvi;->c:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lgvi;->d:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lgvi;->g:Ljava/lang/String;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lgvi;->h:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lgvi;->p:Ljava/lang/String;

    .line 8
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 238
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 239
    sget-object v2, Lgvi$c;->f:Lhby;

    .line 240
    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 241
    sget-object v2, Lgvi$a;->a:Lhby;

    .line 242
    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 243
    sget-object v2, Lgvi$b;->e:Lhby;

    .line 244
    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 245
    sget-object v2, Lbkm$a;->q:Lhby;

    .line 246
    aput-object v2, v0, v1

    .line 247
    const-string v1, "\u0001\u0010\u0000\u0001\u0001\u0012\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001\u0003\u0008\u0002\u0004\u0002\u0003\u0005\u000c\u0004\u0006\u0008\u0005\u0007\u0008\u0006\u0008\u0007\u0007\t\u0007\u0008\n\u0007\t\u000b\u0007\n\u000c\u000c\u000b\u000f\u000c\u000c\u0010\u0007\r\u0011\u0008\u000e\u0012\u000c\u000f"

    .line 248
    sget-object v2, Lgvi;->r:Lgvi;

    invoke-static {v2, v1, v0}, Lgvi;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 132
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 133
    :pswitch_0
    new-instance v0, Lgvi;

    invoke-direct {v0}, Lgvi;-><init>()V

    .line 236
    :goto_0
    :pswitch_1
    return-object v0

    .line 134
    :pswitch_2
    sget-object v0, Lgvi;->r:Lgvi;

    goto :goto_0

    .line 136
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[I)V

    move-object v0, v1

    goto :goto_0

    .line 137
    :pswitch_4
    check-cast p2, Lhaq;

    .line 138
    check-cast p3, Lhbg;

    .line 139
    if-nez p3, :cond_0

    .line 140
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 141
    :cond_0
    :try_start_0
    sget-boolean v0, Lgvi;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 142
    invoke-virtual {p0, p2, p3}, Lgvi;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 143
    sget-object v0, Lgvi;->r:Lgvi;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 145
    :cond_2
    :goto_1
    if-nez v0, :cond_7

    .line 146
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 147
    sparse-switch v2, :sswitch_data_0

    .line 150
    invoke-virtual {p0, v2, p2}, Lgvi;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 151
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 149
    goto :goto_1

    .line 152
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 153
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lgvi;->a:I

    .line 154
    iput-object v2, p0, Lgvi;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 223
    :catch_0
    move-exception v0

    .line 224
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    :catchall_0
    move-exception v0

    throw v0

    .line 156
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 157
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lgvi;->a:I

    .line 158
    iput-object v2, p0, Lgvi;->c:Ljava/lang/String;
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 225
    :catch_1
    move-exception v0

    .line 226
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 227
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 160
    :sswitch_3
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 161
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lgvi;->a:I

    .line 162
    iput-object v2, p0, Lgvi;->d:Ljava/lang/String;

    goto :goto_1

    .line 164
    :sswitch_4
    iget v2, p0, Lgvi;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lgvi;->a:I

    .line 165
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lgvi;->e:J

    goto :goto_1

    .line 167
    :sswitch_5
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 168
    invoke-static {v2}, Lgvi$c;->a(I)Lgvi$c;

    move-result-object v3

    .line 169
    if-nez v3, :cond_3

    .line 170
    const/4 v3, 0x5

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 171
    :cond_3
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lgvi;->a:I

    .line 172
    iput v2, p0, Lgvi;->f:I

    goto :goto_1

    .line 174
    :sswitch_6
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 175
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lgvi;->a:I

    .line 176
    iput-object v2, p0, Lgvi;->g:Ljava/lang/String;

    goto/16 :goto_1

    .line 178
    :sswitch_7
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 179
    iget v3, p0, Lgvi;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lgvi;->a:I

    .line 180
    iput-object v2, p0, Lgvi;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 182
    :sswitch_8
    iget v2, p0, Lgvi;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lgvi;->a:I

    .line 183
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lgvi;->i:Z

    goto/16 :goto_1

    .line 185
    :sswitch_9
    iget v2, p0, Lgvi;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lgvi;->a:I

    .line 186
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lgvi;->j:Z

    goto/16 :goto_1

    .line 188
    :sswitch_a
    iget v2, p0, Lgvi;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lgvi;->a:I

    .line 189
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lgvi;->k:Z

    goto/16 :goto_1

    .line 191
    :sswitch_b
    iget v2, p0, Lgvi;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lgvi;->a:I

    .line 192
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lgvi;->l:Z

    goto/16 :goto_1

    .line 194
    :sswitch_c
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 195
    invoke-static {v2}, Lgvi$a;->a(I)Lgvi$a;

    move-result-object v3

    .line 196
    if-nez v3, :cond_4

    .line 197
    const/16 v3, 0xc

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 198
    :cond_4
    iget v3, p0, Lgvi;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lgvi;->a:I

    .line 199
    iput v2, p0, Lgvi;->m:I

    goto/16 :goto_1

    .line 201
    :sswitch_d
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 202
    invoke-static {v2}, Lgvi$b;->a(I)Lgvi$b;

    move-result-object v3

    .line 203
    if-nez v3, :cond_5

    .line 204
    const/16 v3, 0xf

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 205
    :cond_5
    iget v3, p0, Lgvi;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lgvi;->a:I

    .line 206
    iput v2, p0, Lgvi;->n:I

    goto/16 :goto_1

    .line 208
    :sswitch_e
    iget v2, p0, Lgvi;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lgvi;->a:I

    .line 209
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lgvi;->o:Z

    goto/16 :goto_1

    .line 211
    :sswitch_f
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 212
    iget v3, p0, Lgvi;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lgvi;->a:I

    .line 213
    iput-object v2, p0, Lgvi;->p:Ljava/lang/String;

    goto/16 :goto_1

    .line 215
    :sswitch_10
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 216
    invoke-static {v2}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v3

    .line 217
    if-nez v3, :cond_6

    .line 218
    const/16 v3, 0x12

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 219
    :cond_6
    iget v3, p0, Lgvi;->a:I

    const v4, 0x8000

    or-int/2addr v3, v4

    iput v3, p0, Lgvi;->a:I

    .line 220
    iput v2, p0, Lgvi;->q:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 229
    :cond_7
    :pswitch_5
    sget-object v0, Lgvi;->r:Lgvi;

    goto/16 :goto_0

    .line 230
    :pswitch_6
    sget-object v0, Lgvi;->s:Lhdm;

    if-nez v0, :cond_9

    const-class v1, Lgvi;

    monitor-enter v1

    .line 231
    :try_start_5
    sget-object v0, Lgvi;->s:Lhdm;

    if-nez v0, :cond_8

    .line 232
    new-instance v0, Lhaa;

    sget-object v2, Lgvi;->r:Lgvi;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lgvi;->s:Lhdm;

    .line 233
    :cond_8
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 234
    :cond_9
    sget-object v0, Lgvi;->s:Lhdm;

    goto/16 :goto_0

    .line 233
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 235
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 63
    iget v0, p0, Lgvi;->memoizedSerializedSize:I

    .line 64
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 131
    :goto_0
    return v0

    .line 65
    :cond_0
    sget-boolean v0, Lgvi;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 66
    invoke-virtual {p0}, Lgvi;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lgvi;->memoizedSerializedSize:I

    .line 67
    iget v0, p0, Lgvi;->memoizedSerializedSize:I

    goto :goto_0

    .line 68
    :cond_1
    const/4 v0, 0x0

    .line 69
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 72
    iget-object v0, p0, Lgvi;->b:Ljava/lang/String;

    .line 73
    invoke-static {v2, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 74
    :cond_2
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 77
    iget-object v1, p0, Lgvi;->c:Ljava/lang/String;

    .line 78
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_3
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 80
    const/4 v1, 0x3

    .line 82
    iget-object v2, p0, Lgvi;->d:Ljava/lang/String;

    .line 83
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_4
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_5

    .line 85
    iget-wide v2, p0, Lgvi;->e:J

    .line 86
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_5
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v6, :cond_6

    .line 88
    const/4 v1, 0x5

    iget v2, p0, Lgvi;->f:I

    .line 89
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_6
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 91
    const/4 v1, 0x6

    .line 93
    iget-object v2, p0, Lgvi;->g:Ljava/lang/String;

    .line 94
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_7
    iget v1, p0, Lgvi;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 96
    const/4 v1, 0x7

    .line 98
    iget-object v2, p0, Lgvi;->h:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_8
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_9

    .line 101
    iget-boolean v1, p0, Lgvi;->i:Z

    .line 102
    invoke-static {v5, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_9
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_a

    .line 104
    const/16 v1, 0x9

    iget-boolean v2, p0, Lgvi;->j:Z

    .line 105
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_a
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_b

    .line 107
    const/16 v1, 0xa

    iget-boolean v2, p0, Lgvi;->k:Z

    .line 108
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_b
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_c

    .line 110
    const/16 v1, 0xb

    iget-boolean v2, p0, Lgvi;->l:Z

    .line 111
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_c
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_d

    .line 113
    const/16 v1, 0xc

    iget v2, p0, Lgvi;->m:I

    .line 114
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_d
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_e

    .line 116
    const/16 v1, 0xf

    iget v2, p0, Lgvi;->n:I

    .line 117
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_e
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_f

    .line 119
    iget-boolean v1, p0, Lgvi;->o:Z

    .line 120
    invoke-static {v6, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_f
    iget v1, p0, Lgvi;->a:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_10

    .line 122
    const/16 v1, 0x11

    .line 124
    iget-object v2, p0, Lgvi;->p:Ljava/lang/String;

    .line 125
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_10
    iget v1, p0, Lgvi;->a:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_11

    .line 127
    const/16 v1, 0x12

    iget v2, p0, Lgvi;->q:I

    .line 128
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_11
    iget-object v1, p0, Lgvi;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    iput v0, p0, Lgvi;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 9
    sget-boolean v0, Lgvi;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 10
    invoke-virtual {p0, p1}, Lgvi;->writeToInternal(Lhaw;)V

    .line 62
    :goto_0
    return-void

    .line 12
    :cond_0
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 14
    iget-object v0, p0, Lgvi;->b:Ljava/lang/String;

    .line 15
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 16
    :cond_1
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 18
    iget-object v0, p0, Lgvi;->c:Ljava/lang/String;

    .line 19
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 20
    :cond_2
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 21
    const/4 v0, 0x3

    .line 22
    iget-object v1, p0, Lgvi;->d:Ljava/lang/String;

    .line 23
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 24
    :cond_3
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 25
    iget-wide v0, p0, Lgvi;->e:J

    .line 26
    invoke-virtual {p1, v3, v0, v1}, Lhaw;->a(IJ)V

    .line 27
    :cond_4
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_5

    .line 28
    const/4 v0, 0x5

    iget v1, p0, Lgvi;->f:I

    .line 29
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 30
    :cond_5
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 31
    const/4 v0, 0x6

    .line 32
    iget-object v1, p0, Lgvi;->g:Ljava/lang/String;

    .line 33
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 34
    :cond_6
    iget v0, p0, Lgvi;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 35
    const/4 v0, 0x7

    .line 36
    iget-object v1, p0, Lgvi;->h:Ljava/lang/String;

    .line 37
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 38
    :cond_7
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 39
    iget-boolean v0, p0, Lgvi;->i:Z

    invoke-virtual {p1, v4, v0}, Lhaw;->a(IZ)V

    .line 40
    :cond_8
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 41
    const/16 v0, 0x9

    iget-boolean v1, p0, Lgvi;->j:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 42
    :cond_9
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 43
    const/16 v0, 0xa

    iget-boolean v1, p0, Lgvi;->k:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 44
    :cond_a
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_b

    .line 45
    const/16 v0, 0xb

    iget-boolean v1, p0, Lgvi;->l:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 46
    :cond_b
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 47
    const/16 v0, 0xc

    iget v1, p0, Lgvi;->m:I

    .line 48
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 49
    :cond_c
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_d

    .line 50
    const/16 v0, 0xf

    iget v1, p0, Lgvi;->n:I

    .line 51
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 52
    :cond_d
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_e

    .line 53
    iget-boolean v0, p0, Lgvi;->o:Z

    invoke-virtual {p1, v5, v0}, Lhaw;->a(IZ)V

    .line 54
    :cond_e
    iget v0, p0, Lgvi;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_f

    .line 55
    const/16 v0, 0x11

    .line 56
    iget-object v1, p0, Lgvi;->p:Ljava/lang/String;

    .line 57
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 58
    :cond_f
    iget v0, p0, Lgvi;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_10

    .line 59
    const/16 v0, 0x12

    iget v1, p0, Lgvi;->q:I

    .line 60
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 61
    :cond_10
    iget-object v0, p0, Lgvi;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto/16 :goto_0
.end method
