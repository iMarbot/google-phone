.class final Lfun;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;


# instance fields
.field public final synthetic this$0:Lfum;


# direct methods
.method constructor <init>(Lfum;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfun;->this$0:Lfum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lgng$g;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$000(Lfum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    const-string v0, "Resolve flow canceled, ignoring error (%s)"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    :goto_0
    return-void

    .line 5
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lgng$g;->errorCode:Ljava/lang/Integer;

    const/4 v1, -0x1

    .line 6
    invoke-static {v0, v1}, Lhcw;->a(Ljava/lang/Integer;I)I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 7
    :cond_1
    const-string v0, "Resolve flow failed (%s)"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$100(Lfum;)Lfnn;

    move-result-object v0

    invoke-interface {v0, p1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0

    .line 10
    :cond_2
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$200(Lfum;)Lfvs;

    move-result-object v0

    invoke-static {v0}, Lfum;->access$300(Lfvs;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 11
    const-string v0, "Resolve flow failed (%s)"

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$100(Lfum;)Lfnn;

    move-result-object v0

    invoke-interface {v0, p1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0

    .line 14
    :cond_3
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$400(Lfum;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 15
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lfum;->KNOCKING_TIMEOUT_MILLIS:J

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lfum;->access$402(Lfum;J)J

    .line 16
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lfun;->this$0:Lfum;

    invoke-static {v2}, Lfum;->access$400(Lfum;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    .line 17
    const-string v0, "Knocking resolve flow timed out"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$100(Lfum;)Lfnn;

    move-result-object v0

    invoke-interface {v0, p1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0

    .line 20
    :cond_5
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$500(Lfum;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfun;->this$0:Lfum;

    sget-wide v2, Lfum;->KNOCKING_POLL_DELAY_MILLIS:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lgng$g;

    invoke-virtual {p0, p1}, Lfun;->onError(Lgng$g;)V

    return-void
.end method

.method public final onSuccess(Lgng$g;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$000(Lfum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    const-string v0, "Resolve flow canceled, ignoring success (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p1, Lgng$g;->hangoutId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 26
    const-string v0, "Successfully resolved hangout (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$100(Lfum;)Lfnn;

    move-result-object v0

    invoke-interface {v0, p1}, Lfnn;->onSuccess(Lhfz;)V

    goto :goto_0

    .line 28
    :cond_1
    const-string v0, "Hangout ID missing in successful resolve response (%s)"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    const-string v0, "Hangout ID missing in successful resolve response"

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lfun;->this$0:Lfum;

    invoke-static {v0}, Lfum;->access$100(Lfum;)Lfnn;

    move-result-object v0

    invoke-interface {v0, p1}, Lfnn;->onError(Lhfz;)V

    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lgng$g;

    invoke-virtual {p0, p1}, Lfun;->onSuccess(Lgng$g;)V

    return-void
.end method
