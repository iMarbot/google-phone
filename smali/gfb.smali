.class public Lgfb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfq;
.implements Lgfv;
.implements Lggb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgfb$a;
    }
.end annotation


# static fields
.field private static f:Ljava/util/logging/Logger;


# instance fields
.field public final a:Lghd;

.field public final b:Lgga;

.field public final c:Lgfq;

.field public final d:Landroid/support/design/widget/AppBarLayout$Behavior$a;

.field public final e:Ljava/lang/String;

.field private g:Ljava/util/concurrent/locks/Lock;

.field private h:Lgfb$a;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Long;

.field private k:Ljava/lang/String;

.field private l:Ljava/util/Collection;

.field private m:Lgfv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const-class v0, Lgfb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lgfb;->f:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lgnw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    .line 3
    iget-object v0, p1, Lgnw;->a:Lgfb$a;

    invoke-static {v0}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfb$a;

    iput-object v0, p0, Lgfb;->h:Lgfb$a;

    .line 4
    iput-object v1, p0, Lgfb;->b:Lgga;

    .line 5
    iput-object v1, p0, Lgfb;->d:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    .line 6
    iget-object v0, p1, Lgnw;->d:Lgfn;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lgfb;->e:Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lgfb;->c:Lgfq;

    .line 8
    iput-object v1, p0, Lgfb;->m:Lgfv;

    .line 9
    iget-object v0, p1, Lgnw;->h:Ljava/util/Collection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lgfb;->l:Ljava/util/Collection;

    .line 10
    iget-object v0, p1, Lgnw;->e:Lghd;

    invoke-static {v0}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghd;

    iput-object v0, p0, Lgfb;->a:Lghd;

    .line 11
    return-void

    .line 6
    :cond_0
    iget-object v0, p1, Lgnw;->d:Lgfn;

    invoke-virtual {v0}, Lgfn;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 67
    :try_start_0
    iget-object v0, p0, Lgfb;->j:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 69
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    .line 70
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgfb;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lgfb;->a:Lghd;

    invoke-interface {v2}, Lghd;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 71
    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    iget-object v2, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 78
    :try_start_0
    invoke-virtual {p0}, Lgfb;->a()Lgff;

    move-result-object v2

    .line 79
    if-eqz v2, :cond_4

    .line 80
    invoke-virtual {p0, v2}, Lgfb;->a(Lgff;)Lgfb;

    .line 81
    iget-object v2, p0, Lgfb;->l:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Lgfg; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v2

    .line 87
    const/16 v3, 0x190

    .line 88
    :try_start_1
    iget v4, v2, Lgfx;->b:I

    .line 89
    if-gt v3, v4, :cond_2

    .line 90
    iget v3, v2, Lgfx;->b:I

    .line 91
    const/16 v4, 0x1f4

    if-ge v3, v4, :cond_2

    .line 93
    :goto_1
    iget-object v3, v2, Lgfg;->a:Lgfd;

    .line 94
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 95
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lgfb;->a(Ljava/lang/String;)Lgfb;

    .line 96
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lgfb;->b(Ljava/lang/Long;)Lgfb;

    .line 97
    :cond_0
    iget-object v3, p0, Lgfb;->l:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 103
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 83
    :cond_1
    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 102
    :goto_3
    return v0

    :cond_2
    move v0, v1

    .line 91
    goto :goto_1

    .line 99
    :cond_3
    if-eqz v0, :cond_4

    .line 100
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    :cond_4
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    .line 102
    goto :goto_3
.end method


# virtual methods
.method public a(Lgff;)Lgfb;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 104
    .line 106
    invoke-virtual {p0, v0}, Lgfb;->a(Ljava/lang/String;)Lgfb;

    .line 111
    invoke-virtual {p0, v0}, Lgfb;->b(Ljava/lang/Long;)Lgfb;

    .line 112
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Lgfb;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 61
    :try_start_0
    iput-object p1, p0, Lgfb;->j:Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 65
    return-object p0

    .line 64
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a(Ljava/lang/String;)Lgfb;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 55
    :try_start_0
    iput-object p1, p0, Lgfb;->i:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 59
    return-object p0

    .line 58
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public a()Lgff;
    .locals 5

    .prologue
    .line 113
    iget-object v0, p0, Lgfb;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 115
    :cond_0
    new-instance v0, Lgfc;

    iget-object v1, p0, Lgfb;->b:Lgga;

    iget-object v2, p0, Lgfb;->d:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    new-instance v3, Lgfn;

    iget-object v4, p0, Lgfb;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Lgfn;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lgfb;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lgfc;-><init>(Lgga;Landroid/support/design/widget/AppBarLayout$Behavior$a;Lgfn;Ljava/lang/String;)V

    iget-object v1, p0, Lgfb;->c:Lgfq;

    .line 116
    invoke-virtual {v0, v1}, Lgfc;->a(Lgfq;)Lgfc;

    move-result-object v0

    iget-object v1, p0, Lgfb;->m:Lgfv;

    .line 117
    invoke-virtual {v0, v1}, Lgfc;->a(Lgfv;)Lgfc;

    move-result-object v0

    invoke-virtual {v0}, Lgfc;->a()Lgff;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lgqj;)V
    .locals 4

    .prologue
    .line 12
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 13
    :try_start_0
    invoke-direct {p0}, Lgfb;->b()Ljava/lang/Long;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lgfb;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3c

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 15
    :cond_0
    invoke-direct {p0}, Lgfb;->c()Z

    .line 16
    iget-object v0, p0, Lgfb;->i:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 17
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 21
    :goto_0
    return-void

    .line 19
    :cond_1
    :try_start_1
    iget-object v0, p0, Lgfb;->h:Lgfb$a;

    iget-object v1, p0, Lgfb;->i:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lgfb$a;->a(Lgqj;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lgqj;Lgfw;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 23
    .line 26
    iget-object v0, p2, Lgfw;->d:Lgqj;

    invoke-virtual {v0}, Lgqj;->e()Lgfr;

    .line 28
    const/4 v0, 0x0

    .line 30
    if-eqz v0, :cond_6

    .line 31
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32
    const-string v3, "Bearer "

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 34
    sget-object v2, Lgfa;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    move v2, v1

    .line 37
    :goto_0
    if-nez v2, :cond_1

    .line 39
    iget v0, p2, Lgfw;->b:I

    .line 40
    const/16 v2, 0x191

    if-ne v0, v2, :cond_3

    move v0, v1

    .line 41
    :cond_1
    :goto_1
    if-eqz v0, :cond_5

    .line 42
    :try_start_0
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :try_start_1
    iget-object v0, p0, Lgfb;->i:Ljava/lang/String;

    iget-object v2, p0, Lgfb;->h:Lgfb$a;

    invoke-virtual {v2, p1}, Lgfb$a;->a(Lgqj;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhcw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    invoke-direct {p0}, Lgfb;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 45
    :cond_2
    :goto_2
    :try_start_2
    iget-object v0, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 50
    :goto_3
    return v1

    :cond_3
    move v0, v6

    .line 40
    goto :goto_1

    :cond_4
    move v1, v6

    .line 44
    goto :goto_2

    .line 47
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgfb;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 48
    :catch_0
    move-exception v5

    .line 49
    sget-object v0, Lgfb;->f:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "com.google.api.client.auth.oauth2.Credential"

    const-string v3, "handleResponse"

    const-string v4, "unable to refresh token"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move v1, v6

    .line 50
    goto :goto_3

    :cond_6
    move v2, v6

    move v0, v6

    goto :goto_0
.end method

.method public b(Ljava/lang/Long;)Lgfb;
    .locals 6

    .prologue
    .line 74
    .line 75
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 76
    :goto_0
    invoke-virtual {p0, v0}, Lgfb;->a(Ljava/lang/Long;)Lgfb;

    move-result-object v0

    return-object v0

    .line 75
    :cond_0
    iget-object v0, p0, Lgfb;->a:Lghd;

    invoke-interface {v0}, Lghd;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public initialize$51666RRD5TJMURR7DHIIUOBGD4NM6R39CLN78BR8EHQ70BQ8EHQ70KJ5E5QMASRK7CKLC___0(Lgqj;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p1, p0}, Lgqj;->a(Lgfq;)Lgqj;

    .line 52
    invoke-virtual {p1, p0}, Lgqj;->a(Lggb;)Lgqj;

    .line 53
    return-void
.end method
