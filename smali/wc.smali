.class public Lwc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lxv;


# static fields
.field public static a:Ljava/lang/reflect/Field;

.field public static b:Z

.field public static c:Ljava/lang/Class;

.field public static d:Z

.field public static e:Ljava/lang/reflect/Field;

.field public static f:Z

.field public static g:Ljava/lang/reflect/Field;

.field public static h:Z


# instance fields
.field public final synthetic i:Luw;


# direct methods
.method public constructor <init>(Luw;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lwc;->i:Luw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Z
    .locals 2

    .prologue
    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 2
    invoke-static {p0}, Lwc;->d(Landroid/content/res/Resources;)Z

    move-result v0

    .line 7
    :goto_0
    return v0

    .line 3
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 4
    invoke-static {p0}, Lwc;->c(Landroid/content/res/Resources;)Z

    move-result v0

    goto :goto_0

    .line 5
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 6
    invoke-static {p0}, Lwc;->b(Landroid/content/res/Resources;)Z

    move-result v0

    goto :goto_0

    .line 7
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 71
    sget-boolean v0, Lwc;->d:Z

    if-nez v0, :cond_0

    .line 72
    :try_start_0
    const-string v0, "android.content.res.ThemedResourceCache"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lwc;->c:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    sput-boolean v2, Lwc;->d:Z

    .line 77
    :cond_0
    sget-object v0, Lwc;->c:Ljava/lang/Class;

    if-nez v0, :cond_1

    move v0, v1

    .line 98
    :goto_1
    return v0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    const-string v3, "ResourcesFlusher"

    const-string v4, "Could not find ThemedResourceCache class"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 79
    :cond_1
    sget-boolean v0, Lwc;->f:Z

    if-nez v0, :cond_2

    .line 80
    :try_start_1
    sget-object v0, Lwc;->c:Ljava/lang/Class;

    const-string v3, "mUnthemedEntries"

    .line 81
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 82
    sput-object v0, Lwc;->e:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 86
    :goto_2
    sput-boolean v2, Lwc;->f:Z

    .line 87
    :cond_2
    sget-object v0, Lwc;->e:Ljava/lang/reflect/Field;

    if-nez v0, :cond_3

    move v0, v1

    .line 88
    goto :goto_1

    .line 84
    :catch_1
    move-exception v0

    .line 85
    const-string v3, "ResourcesFlusher"

    const-string v4, "Could not retrieve ThemedResourceCache#mUnthemedEntries field"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 89
    :cond_3
    const/4 v3, 0x0

    .line 90
    :try_start_2
    sget-object v0, Lwc;->e:Ljava/lang/reflect/Field;

    .line 91
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2

    .line 95
    :goto_3
    if-eqz v0, :cond_4

    .line 96
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    move v0, v2

    .line 97
    goto :goto_1

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v4, "ResourcesFlusher"

    const-string v5, "Could not retrieve value from ThemedResourceCache#mUnthemedEntries"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v3

    goto :goto_3

    :cond_4
    move v0, v1

    .line 98
    goto :goto_1
.end method

.method public static b(Landroid/content/res/Resources;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 8
    sget-boolean v0, Lwc;->b:Z

    if-nez v0, :cond_0

    .line 9
    :try_start_0
    const-class v0, Landroid/content/res/Resources;

    const-string v2, "mDrawableCache"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 10
    sput-object v0, Lwc;->a:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    :goto_0
    sput-boolean v1, Lwc;->b:Z

    .line 15
    :cond_0
    sget-object v0, Lwc;->a:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_1

    .line 16
    const/4 v2, 0x0

    .line 17
    :try_start_1
    sget-object v0, Lwc;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 21
    :goto_1
    if-eqz v0, :cond_1

    .line 22
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    move v0, v1

    .line 24
    :goto_2
    return v0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v2, "ResourcesFlusher"

    const-string v3, "Could not retrieve Resources#mDrawableCache field"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 19
    :catch_1
    move-exception v0

    .line 20
    const-string v3, "ResourcesFlusher"

    const-string v4, "Could not retrieve value from Resources#mDrawableCache"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v2

    goto :goto_1

    .line 24
    :cond_1
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static c(Landroid/content/res/Resources;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 25
    sget-boolean v2, Lwc;->b:Z

    if-nez v2, :cond_0

    .line 26
    :try_start_0
    const-class v2, Landroid/content/res/Resources;

    const-string v3, "mDrawableCache"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 27
    sput-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    sput-boolean v1, Lwc;->b:Z

    .line 32
    :cond_0
    const/4 v3, 0x0

    .line 33
    sget-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    if-eqz v2, :cond_2

    .line 34
    :try_start_1
    sget-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 38
    :goto_1
    if-nez v2, :cond_3

    .line 40
    :cond_1
    :goto_2
    return v0

    .line 29
    :catch_0
    move-exception v2

    .line 30
    const-string v3, "ResourcesFlusher"

    const-string v4, "Could not retrieve Resources#mDrawableCache field"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 36
    :catch_1
    move-exception v2

    .line 37
    const-string v4, "ResourcesFlusher"

    const-string v5, "Could not retrieve value from Resources#mDrawableCache"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    move-object v2, v3

    goto :goto_1

    .line 40
    :cond_3
    if-eqz v2, :cond_1

    invoke-static {v2}, Lwc;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_2
.end method

.method public static d(Landroid/content/res/Resources;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 41
    sget-boolean v2, Lwc;->h:Z

    if-nez v2, :cond_0

    .line 42
    :try_start_0
    const-class v2, Landroid/content/res/Resources;

    const-string v4, "mResourcesImpl"

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 43
    sput-object v2, Lwc;->g:Ljava/lang/reflect/Field;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    sput-boolean v1, Lwc;->h:Z

    .line 48
    :cond_0
    sget-object v2, Lwc;->g:Ljava/lang/reflect/Field;

    if-nez v2, :cond_2

    .line 70
    :cond_1
    :goto_1
    return v0

    .line 45
    :catch_0
    move-exception v2

    .line 46
    const-string v4, "ResourcesFlusher"

    const-string v5, "Could not retrieve Resources#mResourcesImpl field"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 51
    :cond_2
    :try_start_1
    sget-object v2, Lwc;->g:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    move-object v4, v2

    .line 55
    :goto_2
    if-eqz v4, :cond_1

    .line 57
    sget-boolean v2, Lwc;->b:Z

    if-nez v2, :cond_3

    .line 58
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v5, "mDrawableCache"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 59
    sput-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_2

    .line 63
    :goto_3
    sput-boolean v1, Lwc;->b:Z

    .line 65
    :cond_3
    sget-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    if-eqz v2, :cond_4

    .line 66
    :try_start_3
    sget-object v2, Lwc;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v2

    .line 70
    :goto_4
    if-eqz v2, :cond_1

    invoke-static {v2}, Lwc;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 53
    :catch_1
    move-exception v2

    .line 54
    const-string v4, "ResourcesFlusher"

    const-string v5, "Could not retrieve value from Resources#mResourcesImpl"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v3

    goto :goto_2

    .line 61
    :catch_2
    move-exception v2

    .line 62
    const-string v5, "ResourcesFlusher"

    const-string v6, "Could not retrieve ResourcesImpl#mDrawableCache field"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 68
    :catch_3
    move-exception v2

    .line 69
    const-string v4, "ResourcesFlusher"

    const-string v5, "Could not retrieve value from ResourcesImpl#mDrawableCache"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    move-object v2, v3

    goto :goto_4
.end method


# virtual methods
.method public a(Lxf;Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lwc;->i:Luw;

    invoke-virtual {v0, p1}, Luw;->b(Lxf;)V

    .line 108
    return-void
.end method

.method public a(Lxf;)Z
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lwc;->i:Luw;

    .line 102
    iget-object v0, v0, Luk;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    const/16 v1, 0x6c

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    .line 106
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
