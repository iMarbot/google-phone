.class public final enum Lgxg$a;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgxg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lgxg$a;

.field public static final enum b:Lgxg$a;

.field public static final enum c:Lgxg$a;

.field public static final enum d:Lgxg$a;

.field public static final enum e:Lgxg$a;

.field private static synthetic f:[Lgxg$a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lgxg$a;

    const-string v1, "NOT_A_NUMBER"

    invoke-direct {v0, v1, v2}, Lgxg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxg$a;->a:Lgxg$a;

    .line 4
    new-instance v0, Lgxg$a;

    const-string v1, "NO_MATCH"

    invoke-direct {v0, v1, v3}, Lgxg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxg$a;->b:Lgxg$a;

    .line 5
    new-instance v0, Lgxg$a;

    const-string v1, "SHORT_NSN_MATCH"

    invoke-direct {v0, v1, v4}, Lgxg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxg$a;->c:Lgxg$a;

    .line 6
    new-instance v0, Lgxg$a;

    const-string v1, "NSN_MATCH"

    invoke-direct {v0, v1, v5}, Lgxg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxg$a;->d:Lgxg$a;

    .line 7
    new-instance v0, Lgxg$a;

    const-string v1, "EXACT_MATCH"

    invoke-direct {v0, v1, v6}, Lgxg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxg$a;->e:Lgxg$a;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lgxg$a;

    sget-object v1, Lgxg$a;->a:Lgxg$a;

    aput-object v1, v0, v2

    sget-object v1, Lgxg$a;->b:Lgxg$a;

    aput-object v1, v0, v3

    sget-object v1, Lgxg$a;->c:Lgxg$a;

    aput-object v1, v0, v4

    sget-object v1, Lgxg$a;->d:Lgxg$a;

    aput-object v1, v0, v5

    sget-object v1, Lgxg$a;->e:Lgxg$a;

    aput-object v1, v0, v6

    sput-object v0, Lgxg$a;->f:[Lgxg$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgxg$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgxg$a;->f:[Lgxg$a;

    invoke-virtual {v0}, [Lgxg$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgxg$a;

    return-object v0
.end method
