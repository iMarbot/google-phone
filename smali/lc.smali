.class public Llc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:D

.field public b:D

.field public c:Z

.field public d:D

.field public e:D

.field public f:D

.field public g:D

.field public h:D

.field public i:D

.field public final j:Lbi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Llc;->a:D

    .line 3
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Llc;->b:D

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Llc;->c:Z

    .line 5
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Llc;->i:D

    .line 6
    new-instance v0, Lbi;

    invoke-direct {v0}, Lbi;-><init>()V

    iput-object v0, p0, Llc;->j:Lbi;

    .line 7
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Llc;->a:D

    .line 10
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Llc;->b:D

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Llc;->c:Z

    .line 12
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Llc;->i:D

    .line 13
    new-instance v0, Lbi;

    invoke-direct {v0}, Lbi;-><init>()V

    iput-object v0, p0, Llc;->j:Lbi;

    .line 14
    float-to-double v0, p1

    iput-wide v0, p0, Llc;->i:D

    .line 15
    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Llc;->i:D

    double-to-float v0, v0

    return v0
.end method

.method public a(DDJ)Lbi;
    .locals 19

    .prologue
    .line 39
    invoke-virtual/range {p0 .. p0}, Llc;->b()V

    .line 40
    move-wide/from16 v0, p5

    long-to-double v2, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    .line 41
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->i:D

    sub-double v6, p1, v4

    .line 42
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->b:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v8

    if-lez v4, :cond_0

    .line 43
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->g:D

    mul-double/2addr v4, v6

    sub-double v4, v4, p3

    move-object/from16 v0, p0

    iget-wide v8, v0, Llc;->g:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->f:D

    sub-double/2addr v8, v10

    div-double/2addr v4, v8

    sub-double v8, v6, v4

    .line 44
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->g:D

    mul-double/2addr v4, v6

    sub-double v4, v4, p3

    move-object/from16 v0, p0

    iget-wide v6, v0, Llc;->g:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->f:D

    sub-double/2addr v6, v10

    div-double v6, v4, v6

    .line 45
    const-wide v4, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->g:D

    mul-double/2addr v10, v2

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v4, v8

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->f:D

    mul-double/2addr v12, v2

    .line 46
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double/2addr v10, v6

    add-double/2addr v4, v10

    .line 47
    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->g:D

    mul-double/2addr v8, v10

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->g:D

    mul-double/2addr v12, v2

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->f:D

    mul-double/2addr v6, v10

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->f:D

    mul-double/2addr v2, v12

    .line 48
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v8

    .line 65
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Llc;->j:Lbi;

    move-object/from16 v0, p0

    iget-wide v8, v0, Llc;->i:D

    add-double/2addr v4, v8

    double-to-float v4, v4

    iput v4, v6, Lbi;->a:F

    .line 66
    move-object/from16 v0, p0

    iget-object v4, v0, Llc;->j:Lbi;

    double-to-float v2, v2

    iput v2, v4, Lbi;->b:F

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Llc;->j:Lbi;

    return-object v2

    .line 49
    :cond_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->b:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v8

    if-nez v4, :cond_1

    .line 51
    move-object/from16 v0, p0

    iget-wide v4, v0, Llc;->a:D

    mul-double/2addr v4, v6

    add-double v8, p3, v4

    .line 52
    mul-double v4, v8, v2

    add-double/2addr v4, v6

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->a:D

    neg-double v12, v12

    mul-double/2addr v12, v2

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double/2addr v4, v10

    .line 53
    mul-double v10, v8, v2

    add-double/2addr v6, v10

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->a:D

    neg-double v12, v12

    mul-double/2addr v12, v2

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double/2addr v6, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->a:D

    neg-double v10, v10

    mul-double/2addr v6, v10

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->a:D

    neg-double v12, v12

    mul-double/2addr v2, v12

    .line 54
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v2, v8

    add-double/2addr v2, v6

    .line 55
    goto :goto_0

    .line 57
    :cond_1
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget-wide v8, v0, Llc;->h:D

    div-double/2addr v4, v8

    move-object/from16 v0, p0

    iget-wide v8, v0, Llc;->b:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->a:D

    mul-double/2addr v8, v10

    mul-double/2addr v8, v6

    add-double v8, v8, p3

    mul-double/2addr v8, v4

    .line 58
    const-wide v4, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->b:D

    neg-double v10, v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->a:D

    mul-double/2addr v10, v12

    mul-double/2addr v10, v2

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->h:D

    mul-double/2addr v10, v2

    .line 59
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v10, v6

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->h:D

    mul-double/2addr v12, v2

    .line 60
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v12, v8

    add-double/2addr v10, v12

    mul-double/2addr v4, v10

    .line 61
    move-object/from16 v0, p0

    iget-wide v10, v0, Llc;->a:D

    neg-double v10, v10

    mul-double/2addr v10, v4

    move-object/from16 v0, p0

    iget-wide v12, v0, Llc;->b:D

    mul-double/2addr v10, v12

    const-wide v12, 0x4005bf0a8b145769L    # Math.E

    move-object/from16 v0, p0

    iget-wide v14, v0, Llc;->b:D

    neg-double v14, v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Llc;->a:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    mul-double/2addr v14, v2

    .line 62
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Llc;->h:D

    neg-double v14, v14

    mul-double/2addr v6, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Llc;->h:D

    mul-double/2addr v14, v2

    .line 63
    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v6, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Llc;->h:D

    mul-double/2addr v8, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Llc;->h:D

    mul-double/2addr v2, v14

    .line 64
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v8

    add-double/2addr v2, v6

    mul-double/2addr v2, v12

    add-double/2addr v2, v10

    goto/16 :goto_0
.end method

.method public a(F)Llc;
    .locals 2

    .prologue
    .line 16
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Llc;->b:D

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Llc;->c:Z

    .line 18
    return-object p0
.end method

.method public a(D)V
    .locals 5

    .prologue
    .line 68
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Llc;->d:D

    .line 69
    iget-wide v0, p0, Llc;->d:D

    const-wide v2, 0x404f400000000000L    # 62.5

    mul-double/2addr v0, v2

    iput-wide v0, p0, Llc;->e:D

    .line 70
    return-void
.end method

.method public a(FF)Z
    .locals 4

    .prologue
    .line 22
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, Llc;->e:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 23
    invoke-virtual {p0}, Llc;->a()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, Llc;->d:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 24
    const/4 v0, 0x1

    .line 25
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(F)Llc;
    .locals 2

    .prologue
    .line 19
    float-to-double v0, p1

    iput-wide v0, p0, Llc;->i:D

    .line 20
    return-object p0
.end method

.method public b()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 26
    iget-boolean v0, p0, Llc;->c:Z

    if-eqz v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 28
    :cond_0
    iget-wide v0, p0, Llc;->i:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error: Final position of the spring must be set before the animation starts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iget-wide v0, p0, Llc;->b:D

    cmpl-double v0, v0, v8

    if-lez v0, :cond_3

    .line 31
    iget-wide v0, p0, Llc;->b:D

    neg-double v0, v0

    iget-wide v2, p0, Llc;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Llc;->a:D

    iget-wide v4, p0, Llc;->b:D

    iget-wide v6, p0, Llc;->b:D

    mul-double/2addr v4, v6

    sub-double/2addr v4, v8

    .line 32
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, Llc;->f:D

    .line 33
    iget-wide v0, p0, Llc;->b:D

    neg-double v0, v0

    iget-wide v2, p0, Llc;->a:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Llc;->a:D

    iget-wide v4, p0, Llc;->b:D

    iget-wide v6, p0, Llc;->b:D

    mul-double/2addr v4, v6

    sub-double/2addr v4, v8

    .line 34
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Llc;->g:D

    .line 37
    :cond_2
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Llc;->c:Z

    goto :goto_0

    .line 35
    :cond_3
    iget-wide v0, p0, Llc;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, Llc;->b:D

    cmpg-double v0, v0, v8

    if-gez v0, :cond_2

    .line 36
    iget-wide v0, p0, Llc;->a:D

    iget-wide v2, p0, Llc;->b:D

    iget-wide v4, p0, Llc;->b:D

    mul-double/2addr v2, v4

    sub-double v2, v8, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iput-wide v0, p0, Llc;->h:D

    goto :goto_1
.end method
