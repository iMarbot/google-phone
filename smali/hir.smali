.class public final Lhir;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final f:Lhir;

.field private static volatile g:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:J

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Lhir;

    invoke-direct {v0}, Lhir;-><init>()V

    .line 91
    sput-object v0, Lhir;->f:Lhir;

    invoke-virtual {v0}, Lhir;->makeImmutable()V

    .line 92
    const-class v0, Lhir;

    sget-object v1, Lhir;->f:Lhir;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 93
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 87
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    .line 88
    const-string v1, "\u0001\u0004\u0000\u0001\u0001\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0002\u0001\u0003\u0002\u0002\u0004\u0004\u0003"

    .line 89
    sget-object v2, Lhir;->f:Lhir;

    invoke-static {v2, v1, v0}, Lhir;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 39
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 86
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 40
    :pswitch_0
    new-instance v0, Lhir;

    invoke-direct {v0}, Lhir;-><init>()V

    .line 85
    :goto_0
    :pswitch_1
    return-object v0

    .line 41
    :pswitch_2
    sget-object v0, Lhir;->f:Lhir;

    goto :goto_0

    .line 43
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[[B)V

    move-object v0, v1

    goto :goto_0

    .line 44
    :pswitch_4
    check-cast p2, Lhaq;

    .line 45
    check-cast p3, Lhbg;

    .line 46
    if-nez p3, :cond_0

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48
    :cond_0
    :try_start_0
    sget-boolean v0, Lhir;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {p0, p2, p3}, Lhir;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 50
    sget-object v0, Lhir;->f:Lhir;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 52
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 53
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 54
    sparse-switch v2, :sswitch_data_0

    .line 57
    invoke-virtual {p0, v2, p2}, Lhir;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 58
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 56
    goto :goto_1

    .line 59
    :sswitch_1
    iget v2, p0, Lhir;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhir;->a:I

    .line 60
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhir;->b:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :catchall_0
    move-exception v0

    throw v0

    .line 62
    :sswitch_2
    :try_start_2
    iget v2, p0, Lhir;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhir;->a:I

    .line 63
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhir;->c:J
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 74
    :catch_1
    move-exception v0

    .line 75
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    iget v2, p0, Lhir;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lhir;->a:I

    .line 66
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lhir;->d:J

    goto :goto_1

    .line 68
    :sswitch_4
    iget v2, p0, Lhir;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lhir;->a:I

    .line 69
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhir;->e:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 78
    :cond_3
    :pswitch_5
    sget-object v0, Lhir;->f:Lhir;

    goto/16 :goto_0

    .line 79
    :pswitch_6
    sget-object v0, Lhir;->g:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lhir;

    monitor-enter v1

    .line 80
    :try_start_5
    sget-object v0, Lhir;->g:Lhdm;

    if-nez v0, :cond_4

    .line 81
    new-instance v0, Lhaa;

    sget-object v2, Lhir;->f:Lhir;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhir;->g:Lhdm;

    .line 82
    :cond_4
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 83
    :cond_5
    sget-object v0, Lhir;->g:Lhdm;

    goto/16 :goto_0

    .line 82
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 84
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 54
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 18
    iget v0, p0, Lhir;->memoizedSerializedSize:I

    .line 19
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 38
    :goto_0
    return v0

    .line 20
    :cond_0
    sget-boolean v0, Lhir;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lhir;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhir;->memoizedSerializedSize:I

    .line 22
    iget v0, p0, Lhir;->memoizedSerializedSize:I

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    iget v1, p0, Lhir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 25
    iget v0, p0, Lhir;->b:I

    .line 26
    invoke-static {v2, v0}, Lhaw;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27
    :cond_2
    iget v1, p0, Lhir;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    .line 28
    iget-wide v2, p0, Lhir;->c:J

    .line 29
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_3
    iget v1, p0, Lhir;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_4

    .line 31
    const/4 v1, 0x3

    iget-wide v2, p0, Lhir;->d:J

    .line 32
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_4
    iget v1, p0, Lhir;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_5

    .line 34
    iget v1, p0, Lhir;->e:I

    .line 35
    invoke-static {v5, v1}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_5
    iget-object v1, p0, Lhir;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    iput v0, p0, Lhir;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3
    sget-boolean v0, Lhir;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 4
    invoke-virtual {p0, p1}, Lhir;->writeToInternal(Lhaw;)V

    .line 17
    :goto_0
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lhir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 7
    iget v0, p0, Lhir;->b:I

    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 8
    :cond_1
    iget v0, p0, Lhir;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 9
    iget-wide v0, p0, Lhir;->c:J

    .line 10
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 11
    :cond_2
    iget v0, p0, Lhir;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 12
    const/4 v0, 0x3

    iget-wide v2, p0, Lhir;->d:J

    .line 13
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 14
    :cond_3
    iget v0, p0, Lhir;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 15
    iget v0, p0, Lhir;->e:I

    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 16
    :cond_4
    iget-object v0, p0, Lhir;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
