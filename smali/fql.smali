.class public final Lfql;
.super Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field public static final NO_WAIT_TIMEOUT:I = 0x0

.field public static final POLL_PERIOD_MS:I = 0xa


# instance fields
.field public inputBuffers:[Ljava/nio/ByteBuffer;

.field public nextInputBufferIndex:I

.field public outputBuffers:[Ljava/nio/ByteBuffer;

.field public final pollMediaCodecRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lfnp;Lfqk;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;-><init>(Lfnp;Lfqk;)V

    .line 2
    new-instance v0, Lfqm;

    invoke-direct {v0, p0}, Lfqm;-><init>(Lfql;)V

    iput-object v0, p0, Lfql;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    .line 3
    const/4 v0, -0x1

    iput v0, p0, Lfql;->nextInputBufferIndex:I

    .line 4
    return-void
.end method

.method static synthetic access$000(Lfql;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfql;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lfql;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lfql;->outputBuffers:[Ljava/nio/ByteBuffer;

    return-object p1
.end method


# virtual methods
.method protected final getInputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lfql;->inputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected final getNextInputBufferIndex()I
    .locals 4

    .prologue
    .line 13
    iget v0, p0, Lfql;->nextInputBufferIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 14
    invoke-virtual {p0}, Lfql;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lfql;->nextInputBufferIndex:I

    .line 15
    :cond_0
    iget v0, p0, Lfql;->nextInputBufferIndex:I

    return v0
.end method

.method protected final getOutputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lfql;->outputBuffers:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected final onAfterInitialized()V
    .locals 2

    .prologue
    .line 5
    invoke-virtual {p0}, Lfql;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lfql;->inputBuffers:[Ljava/nio/ByteBuffer;

    .line 6
    invoke-virtual {p0}, Lfql;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lfql;->outputBuffers:[Ljava/nio/ByteBuffer;

    .line 7
    invoke-virtual {p0}, Lfql;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfql;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 8
    return-void
.end method

.method protected final onDecoderStopping()V
    .locals 2

    .prologue
    .line 9
    invoke-virtual {p0}, Lfql;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfql;->pollMediaCodecRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 10
    return-void
.end method

.method protected final usedInputBuffer(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 16
    if-eq p1, v2, :cond_0

    iget v0, p0, Lfql;->nextInputBufferIndex:I

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 18
    iput v2, p0, Lfql;->nextInputBufferIndex:I

    .line 19
    return-void

    .line 16
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
