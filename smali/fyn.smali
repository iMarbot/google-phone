.class public final Lfyn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile h:Lfyn;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Lfix;

.field private g:J


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Lfix;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfyn;->c:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lfyn;->d:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lfyn;->a:Ljava/lang/String;

    .line 5
    iput p4, p0, Lfyn;->e:I

    .line 6
    iput-object p5, p0, Lfyn;->b:Ljava/lang/Long;

    .line 7
    iput-object p6, p0, Lfyn;->f:Lfix;

    .line 9
    invoke-virtual {p6}, Lfix;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0

    .line 10
    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    iput-wide v0, p0, Lfyn;->g:J

    .line 11
    return-void
.end method

.method static a(Landroid/content/Context;)Lfyn;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lfyn;->h:Lfyn;

    if-nez v0, :cond_1

    .line 13
    const-class v1, Lfyn;

    monitor-enter v1

    .line 14
    :try_start_0
    sget-object v0, Lfyn;->h:Lfyn;

    if-nez v0, :cond_0

    .line 15
    invoke-static {p0}, Lfyn;->c(Landroid/content/Context;)Lfyn;

    move-result-object v0

    sput-object v0, Lfyn;->h:Lfyn;

    .line 16
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    :cond_1
    sget-object v0, Lfyn;->h:Lfyn;

    return-object v0

    .line 16
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static b(Landroid/content/Context;)Lgax;
    .locals 1

    .prologue
    .line 18
    invoke-static {p0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    new-instance v0, Lfyo;

    invoke-direct {v0, p0}, Lfyo;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Lfyn;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 20
    invoke-static {p0}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 21
    invoke-static {p0}, Lgcq;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 22
    const/4 v3, 0x0

    .line 24
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 25
    const/4 v5, 0x0

    .line 26
    :try_start_0
    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v3, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x14

    if-lt v5, v6, :cond_0

    .line 31
    const-string v5, "android.hardware.type.watch"

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const/4 v4, 0x2

    .line 33
    :cond_0
    invoke-static {p0}, Lfmk;->i(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v5

    .line 34
    new-instance v0, Lfyn;

    new-instance v6, Lfix;

    invoke-direct {v6, p0}, Lfix;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v6}, Lfyn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Lfix;)V

    return-object v0

    .line 29
    :catch_0
    move-exception v5

    const-string v5, "MetricStamper"

    const-string v6, "Failed to get PackageInfo for: %s"

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-static {v5, v6, v7}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhtd;)Lhtd;
    .locals 6

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    const-string v0, "MetricStamper"

    const-string v1, "Unexpected null metric to stamp, Stamping has been skipped."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    :goto_0
    return-object p1

    .line 37
    :cond_0
    new-instance v0, Lhqs;

    invoke-direct {v0}, Lhqs;-><init>()V

    iput-object v0, p1, Lhtd;->e:Lhqs;

    .line 38
    iget-object v0, p1, Lhtd;->e:Lhqs;

    iget-object v1, p0, Lfyn;->c:Ljava/lang/String;

    iput-object v1, v0, Lhqs;->a:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lhtd;->e:Lhqs;

    iget v1, p0, Lfyn;->e:I

    iput v1, v0, Lhqs;->c:I

    .line 40
    iget-object v0, p1, Lhtd;->e:Lhqs;

    iget-object v1, p0, Lfyn;->b:Ljava/lang/Long;

    iput-object v1, v0, Lhqs;->d:Ljava/lang/Long;

    .line 41
    iget-object v0, p1, Lhtd;->e:Lhqs;

    iget-object v1, p0, Lfyn;->a:Ljava/lang/String;

    iput-object v1, v0, Lhqs;->b:Ljava/lang/String;

    .line 42
    iget-object v0, p1, Lhtd;->e:Lhqs;

    iget-object v1, p0, Lfyn;->d:Ljava/lang/String;

    iput-object v1, v0, Lhqs;->e:Ljava/lang/String;

    .line 43
    new-instance v0, Lhrg;

    invoke-direct {v0}, Lhrg;-><init>()V

    iput-object v0, p1, Lhtd;->u:Lhrg;

    .line 44
    iget-object v0, p1, Lhtd;->u:Lhrg;

    iget-object v1, p0, Lfyn;->f:Lfix;

    .line 46
    invoke-virtual {v1}, Lfix;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    .line 47
    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhrg;->a:Ljava/lang/Long;

    .line 48
    iget-object v0, p1, Lhtd;->u:Lhrg;

    iget-wide v2, p0, Lfyn;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lhrg;->b:Ljava/lang/Long;

    goto :goto_0
.end method
