.class public final Lhmz;
.super Lio/grpc/internal/cn;
.source "PG"


# instance fields
.field public final o:Ljava/lang/Object;

.field public p:Ljava/util/List;

.field public q:Ljava/util/Queue;

.field public r:Z

.field public final s:Lhng;

.field public final t:Lhna;

.field public final synthetic u:Lhmx;

.field private v:I

.field private w:I

.field private x:Lhme;


# direct methods
.method public constructor <init>(Lhmx;ILio/grpc/internal/ez;Ljava/lang/Object;Lhme;Lhng;Lhna;)V
    .locals 2

    .prologue
    const v1, 0xffff

    .line 1
    iput-object p1, p0, Lhmz;->u:Lhmx;

    .line 2
    invoke-direct {p0, p2, p3}, Lio/grpc/internal/cn;-><init>(ILio/grpc/internal/ez;)V

    .line 3
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhmz;->r:Z

    .line 5
    iput v1, p0, Lhmz;->v:I

    .line 6
    iput v1, p0, Lhmz;->w:I

    .line 7
    const-string v0, "lock"

    invoke-static {p4, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lhmz;->o:Ljava/lang/Object;

    .line 8
    iput-object p5, p0, Lhmz;->x:Lhme;

    .line 9
    iput-object p6, p0, Lhmz;->s:Lhng;

    .line 10
    iput-object p7, p0, Lhmz;->t:Lhna;

    .line 11
    return-void
.end method


# virtual methods
.method public final a(Lhuh;Z)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 77
    .line 78
    iget-wide v0, p1, Lhuh;->c:J

    .line 79
    long-to-int v0, v0

    .line 80
    iget v1, p0, Lhmz;->v:I

    sub-int v0, v1, v0

    iput v0, p0, Lhmz;->v:I

    .line 81
    iget v0, p0, Lhmz;->v:I

    if-gez v0, :cond_1

    .line 82
    iget-object v0, p0, Lhmz;->x:Lhme;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 83
    iget v1, v1, Lhmx;->i:I

    .line 84
    sget-object v2, Lhns;->e:Lhns;

    invoke-virtual {v0, v1, v2}, Lhme;->a(ILhns;)V

    .line 85
    iget-object v0, p0, Lhmz;->t:Lhna;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 86
    iget v1, v1, Lhmx;->i:I

    .line 87
    sget-object v2, Lhlw;->h:Lhlw;

    const-string v5, "Received data size exceeded our receiving window size"

    invoke-virtual {v2, v5}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    new-instance v2, Lhne;

    invoke-direct {v2, p1}, Lhne;-><init>(Lhuh;)V

    .line 90
    iget-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_4

    .line 91
    iget-object v1, p0, Lio/grpc/internal/cn;->k:Lhlw;

    const-string v4, "DATA-----------------------------\n"

    iget-object v0, p0, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;

    .line 92
    invoke-static {v2, v0}, Lio/grpc/internal/eo;->a(Lio/grpc/internal/en;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_1
    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    .line 94
    invoke-interface {v2}, Lio/grpc/internal/en;->close()V

    .line 95
    iget-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    .line 96
    iget-object v0, v0, Lhlw;->m:Ljava/lang/String;

    .line 97
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_2

    if-eqz p2, :cond_0

    .line 98
    :cond_2
    iget-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    iget-object v1, p0, Lio/grpc/internal/cn;->l:Lhlh;

    invoke-virtual {p0, v0, v3, v1}, Lio/grpc/internal/cn;->b(Lhlw;ZLhlh;)V

    goto :goto_0

    .line 92
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 99
    :cond_4
    iget-boolean v0, p0, Lio/grpc/internal/cn;->n:Z

    if-nez v0, :cond_5

    .line 100
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v1, "headers not received before payload"

    .line 101
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    new-instance v1, Lhlh;

    invoke-direct {v1}, Lhlh;-><init>()V

    .line 102
    invoke-virtual {p0, v0, v3, v1}, Lio/grpc/internal/cn;->b(Lhlw;ZLhlh;)V

    goto :goto_0

    .line 105
    :cond_5
    const-string v0, "frame"

    invoke-static {v2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const/4 v1, 0x1

    .line 107
    :try_start_0
    iget-boolean v0, p0, Lio/grpc/internal/a$c;->d:Z

    if-eqz v0, :cond_6

    .line 108
    sget-object v0, Lio/grpc/internal/a;->a:Ljava/util/logging/Logger;

    .line 109
    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "io.grpc.internal.AbstractClientStream$TransportState"

    const-string v6, "inboundDataReceived"

    const-string v7, "Received data on closed stream"

    invoke-virtual {v0, v4, v5, v6, v7}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 110
    invoke-interface {v2}, Lio/grpc/internal/en;->close()V

    .line 121
    :goto_2
    if-eqz p2, :cond_0

    .line 122
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v1, "Received unexpected EOS on DATA frame from server."

    .line 123
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    .line 124
    new-instance v0, Lhlh;

    invoke-direct {v0}, Lhlh;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/cn;->l:Lhlh;

    .line 125
    iget-object v0, p0, Lio/grpc/internal/cn;->k:Lhlw;

    iget-object v1, p0, Lio/grpc/internal/cn;->l:Lhlh;

    invoke-virtual {p0, v0, v3, v1}, Lio/grpc/internal/cn;->a(Lhlw;ZLhlh;)V

    goto/16 :goto_0

    .line 114
    :cond_6
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0, v2}, Lio/grpc/internal/ax;->a(Lio/grpc/internal/en;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 116
    :catch_0
    move-exception v0

    .line 117
    :try_start_2
    invoke-virtual {p0, v0}, Lio/grpc/internal/f;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 119
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v3, :cond_7

    .line 120
    invoke-interface {v2}, Lio/grpc/internal/en;->close()V

    :cond_7
    throw v0

    .line 119
    :catchall_1
    move-exception v0

    move v3, v1

    goto :goto_3
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 74
    iget-object v1, p0, Lhmz;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 75
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 76
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 49
    invoke-static {p1}, Lhlw;->a(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lhlh;

    invoke-direct {v2}, Lhlh;-><init>()V

    .line 50
    invoke-virtual {p0, v0, v1, v2}, Lhmz;->c(Lhlw;ZLhlh;)V

    .line 51
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 61
    .line 62
    iget-object v0, p0, Lhmz;->u:Lhmx;

    .line 64
    iget-object v0, v0, Lio/grpc/internal/a;->b:Lio/grpc/internal/ce;

    .line 65
    invoke-interface {v0}, Lio/grpc/internal/ce;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lhmz;->t:Lhna;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 67
    iget v1, v1, Lhmx;->i:I

    .line 68
    sget-object v4, Lhns;->i:Lhns;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    .line 72
    :goto_0
    invoke-super {p0, p1}, Lio/grpc/internal/a$c;->a(Z)V

    .line 73
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lhmz;->t:Lhna;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 70
    iget v1, v1, Lhmx;->i:I

    move-object v4, v2

    move-object v5, v2

    .line 71
    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lio/grpc/internal/cn;->b()V

    .line 46
    return-void
.end method

.method public final b(I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 12
    iget-object v0, p0, Lhmz;->u:Lhmx;

    .line 13
    iget v0, v0, Lhmx;->i:I

    .line 14
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    move v0, v6

    :goto_0
    const-string v1, "the stream has been started with id %s"

    invoke-static {v0, v1, p1}, Lgtn;->a(ZLjava/lang/String;I)V

    .line 15
    iget-object v0, p0, Lhmz;->u:Lhmx;

    .line 16
    iput p1, v0, Lhmx;->i:I

    .line 18
    iget-object v0, p0, Lhmz;->u:Lhmx;

    .line 19
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 21
    invoke-super {v0}, Lio/grpc/internal/cn;->b()V

    .line 22
    iget-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    if-eqz v0, :cond_3

    .line 23
    iget-object v0, p0, Lhmz;->x:Lhme;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 24
    iget-boolean v1, v1, Lhmx;->k:Z

    .line 25
    iget-object v3, p0, Lhmz;->u:Lhmx;

    .line 26
    iget v3, v3, Lhmx;->i:I

    .line 27
    iget-object v5, p0, Lhmz;->p:Ljava/util/List;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lhme;->a(ZZIILjava/util/List;)V

    .line 28
    iget-object v0, p0, Lhmz;->u:Lhmx;

    .line 29
    iget-object v0, v0, Lhmx;->f:Lio/grpc/internal/ez;

    .line 31
    iput-object v8, p0, Lhmz;->p:Ljava/util/List;

    move v1, v2

    .line 33
    :goto_1
    iget-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    iget-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmy;

    .line 35
    iget-object v3, p0, Lhmz;->s:Lhng;

    iget-boolean v4, v0, Lhmy;->b:Z

    iget-object v5, p0, Lhmz;->u:Lhmx;

    .line 36
    iget v5, v5, Lhmx;->i:I

    .line 37
    iget-object v7, v0, Lhmy;->a:Lhuh;

    invoke-virtual {v3, v4, v5, v7, v2}, Lhng;->a(ZILhuh;Z)V

    .line 38
    iget-boolean v0, v0, Lhmy;->c:Z

    if-eqz v0, :cond_4

    move v0, v6

    :goto_2
    move v1, v0

    .line 40
    goto :goto_1

    :cond_0
    move v0, v2

    .line 14
    goto :goto_0

    .line 41
    :cond_1
    if-eqz v1, :cond_2

    .line 42
    iget-object v0, p0, Lhmz;->s:Lhng;

    invoke-virtual {v0}, Lhng;->a()V

    .line 43
    :cond_2
    iput-object v8, p0, Lhmz;->q:Ljava/util/Queue;

    .line 44
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method protected final b(Lhlw;ZLhlh;)V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0, p1, p2, p3}, Lhmz;->c(Lhlw;ZLhlh;)V

    .line 48
    return-void
.end method

.method public final c(I)V
    .locals 6

    .prologue
    .line 52
    iget v0, p0, Lhmz;->w:I

    sub-int/2addr v0, p1

    iput v0, p0, Lhmz;->w:I

    .line 53
    iget v0, p0, Lhmz;->w:I

    const/16 v1, 0x7fff

    if-gt v0, v1, :cond_0

    .line 54
    const v0, 0xffff

    iget v1, p0, Lhmz;->w:I

    sub-int/2addr v0, v1

    .line 55
    iget v1, p0, Lhmz;->v:I

    add-int/2addr v1, v0

    iput v1, p0, Lhmz;->v:I

    .line 56
    iget v1, p0, Lhmz;->w:I

    add-int/2addr v1, v0

    iput v1, p0, Lhmz;->w:I

    .line 57
    iget-object v1, p0, Lhmz;->x:Lhme;

    iget-object v2, p0, Lhmz;->u:Lhmx;

    .line 58
    iget v2, v2, Lhmx;->i:I

    .line 59
    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Lhme;->a(IJ)V

    .line 60
    :cond_0
    return-void
.end method

.method public final c(Lhlw;ZLhlh;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 127
    iget-boolean v0, p0, Lhmz;->r:Z

    if-eqz v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 129
    :cond_0
    iput-boolean v3, p0, Lhmz;->r:Z

    .line 130
    iget-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    if-eqz v0, :cond_3

    .line 131
    iget-object v0, p0, Lhmz;->t:Lhna;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 132
    iget-object v2, v0, Lhna;->r:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 133
    invoke-virtual {v0}, Lhna;->e()V

    .line 134
    iput-object v4, p0, Lhmz;->p:Ljava/util/List;

    .line 135
    iget-object v0, p0, Lhmz;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmy;

    .line 136
    iget-object v0, v0, Lhmy;->a:Lhuh;

    invoke-virtual {v0}, Lhuh;->i()V

    goto :goto_1

    .line 138
    :cond_1
    iput-object v4, p0, Lhmz;->q:Ljava/util/Queue;

    .line 139
    if-eqz p3, :cond_2

    :goto_2
    invoke-virtual {p0, p1, v3, p3}, Lhmz;->a(Lhlw;ZLhlh;)V

    goto :goto_0

    :cond_2
    new-instance p3, Lhlh;

    invoke-direct {p3}, Lhlh;-><init>()V

    goto :goto_2

    .line 140
    :cond_3
    iget-object v0, p0, Lhmz;->t:Lhna;

    iget-object v1, p0, Lhmz;->u:Lhmx;

    .line 141
    iget v1, v1, Lhmx;->i:I

    .line 142
    sget-object v4, Lhns;->i:Lhns;

    move-object v2, p1

    move v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    goto :goto_0
.end method
