.class public final Lffp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfka;
.implements Lfkb;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lffp$a;
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lfjz;

.field public final c:Lfmu;

.field public d:Lffs;

.field public final e:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lffq;

    invoke-direct {v0, p0}, Lffq;-><init>(Lffp;)V

    iput-object v0, p0, Lffp;->e:Landroid/content/BroadcastReceiver;

    .line 3
    iput-object p1, p0, Lffp;->a:Landroid/content/Context;

    .line 4
    new-instance v1, Lfet;

    .line 5
    invoke-direct {v1}, Lfet;-><init>()V

    .line 6
    new-instance v0, Lffp$a;

    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lffp$a;-><init>(Landroid/content/Context;)V

    .line 8
    invoke-static {v0}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffp$a;

    iput-object v0, v1, Lfet;->a:Lffp$a;

    .line 9
    iget-object v0, v1, Lfet;->a:Lffp$a;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Ljava/lang/IllegalStateException;

    const-class v1, Lffp$a;

    .line 11
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12
    :cond_0
    iget-object v0, v1, Lfet;->b:Lfkh;

    if-nez v0, :cond_1

    .line 13
    new-instance v0, Lfkh;

    invoke-direct {v0}, Lfkh;-><init>()V

    iput-object v0, v1, Lfet;->b:Lfkh;

    .line 14
    :cond_1
    iget-object v0, v1, Lfet;->c:Lfkz;

    if-nez v0, :cond_2

    .line 15
    new-instance v0, Lfkz;

    invoke-direct {v0}, Lfkz;-><init>()V

    iput-object v0, v1, Lfet;->c:Lfkz;

    .line 16
    :cond_2
    iget-object v0, v1, Lfet;->d:Lfms;

    if-nez v0, :cond_3

    .line 17
    new-instance v0, Lfms;

    invoke-direct {v0}, Lfms;-><init>()V

    iput-object v0, v1, Lfet;->d:Lfms;

    .line 18
    :cond_3
    new-instance v0, Lfes;

    .line 19
    invoke-direct {v0, v1}, Lfes;-><init>(Lfet;)V

    .line 21
    invoke-interface {v0}, Lffr;->b()Lfjx;

    move-result-object v1

    .line 22
    invoke-virtual {v1, p1}, Lfjx;->a(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_4

    .line 24
    invoke-interface {v0}, Lffr;->a()Lfjz$a;

    move-result-object v1

    .line 25
    invoke-interface {v0}, Lffr;->d()Lfml;

    move-result-object v2

    invoke-interface {v1, v2}, Lfjz$a;->a(Lfjy;)Lfjz$a;

    move-result-object v1

    .line 26
    invoke-interface {v1, p0}, Lfjz$a;->a(Lfka;)Lfjz$a;

    move-result-object v1

    .line 27
    invoke-interface {v1, p0}, Lfjz$a;->a(Lfkb;)Lfjz$a;

    move-result-object v1

    .line 28
    invoke-interface {v1}, Lfjz$a;->a()Lfjz;

    move-result-object v1

    iput-object v1, p0, Lffp;->b:Lfjz;

    .line 29
    invoke-interface {v0}, Lffr;->c()Lfmk;

    move-result-object v0

    invoke-virtual {v0}, Lfmk;->a()Lfmu;

    move-result-object v0

    iput-object v0, p0, Lffp;->c:Lfmu;

    .line 33
    :goto_0
    return-void

    .line 30
    :cond_4
    const-string v0, "UserActivityMonitor.UserActivityMonitor: Wifi call activity recognition API can not be started. Google Play service is not available."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iput-object v3, p0, Lffp;->b:Lfjz;

    .line 32
    iput-object v3, p0, Lffp;->c:Lfmu;

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    packed-switch p0, :pswitch_data_0

    .line 42
    :pswitch_0
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 35
    :pswitch_1
    const-string v0, "IN_VEHICLE"

    goto :goto_0

    .line 36
    :pswitch_2
    const-string v0, "ON_BICYCLE"

    goto :goto_0

    .line 37
    :pswitch_3
    const-string v0, "ON_FOOT"

    goto :goto_0

    .line 38
    :pswitch_4
    const-string v0, "WALKING"

    goto :goto_0

    .line 39
    :pswitch_5
    const-string v0, "STILL"

    goto :goto_0

    .line 40
    :pswitch_6
    const-string v0, "TILTING"

    goto :goto_0

    .line 41
    :pswitch_7
    const-string v0, "RUNNING"

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lffp;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    iget-object v1, p0, Lffp;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfjv;)V
    .locals 2

    .prologue
    .line 50
    const-string v0, "UserActivityMonitor.onConnectionFailed, connection to activity recognition API failed."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 45
    const-string v0, "UserActivityMonitor.onConnected, connection to activity recognition API established."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lffp;->c:Lfmu;

    iget-object v1, p0, Lffp;->b:Lfjz;

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lffp;->a()Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lfmu;->a(Lfjz;JLandroid/app/PendingIntent;)Lfkc;

    .line 47
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 48
    const-string v0, "UserActivityMonitor.onConnectionSuspended, suspended activity recognition API connection."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    return-void
.end method
