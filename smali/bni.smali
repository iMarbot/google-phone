.class final synthetic Lbni;
.super Ljava/lang/Object;

# interfaces
.implements Lbeb;


# instance fields
.field private a:Lbnh;

.field private b:Lbne;

.field private c:Lbnf;

.field private d:Landroid/app/Activity;

.field private e:Lbbh;

.field private f:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbnh;Lbne;Lbnf;Landroid/app/Activity;Lbbh;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbni;->a:Lbnh;

    iput-object p2, p0, Lbni;->b:Lbne;

    iput-object p3, p0, Lbni;->c:Lbnf;

    iput-object p4, p0, Lbni;->d:Landroid/app/Activity;

    iput-object p5, p0, Lbni;->e:Lbbh;

    iput-object p6, p0, Lbni;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1
    iget-object v0, p0, Lbni;->a:Lbnh;

    iget-object v1, p0, Lbni;->b:Lbne;

    iget-object v2, p0, Lbni;->c:Lbnf;

    iget-object v3, p0, Lbni;->d:Landroid/app/Activity;

    iget-object v5, p0, Lbni;->e:Lbbh;

    iget-object v4, p0, Lbni;->f:Ljava/lang/String;

    check-cast p1, Lbnk;

    .line 2
    iget-boolean v6, v0, Lbnh;->a:Z

    if-nez v6, :cond_0

    .line 3
    iget-object v6, p1, Lbnk;->a:Lgtm;

    invoke-virtual {v6}, Lgtm;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4
    invoke-interface {v1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->ec:Lbkq$a;

    .line 5
    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 6
    invoke-interface {v1}, Lbne;->a()Lbbh;

    move-result-object v1

    iget-object v0, p1, Lbnk;->a:Lgtm;

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 7
    iput-object v0, v1, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 8
    invoke-virtual {v2}, Lbnf;->a()V

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 10
    :cond_1
    const-class v6, Landroid/telecom/TelecomManager;

    .line 11
    invoke-virtual {v3, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/TelecomManager;

    .line 13
    iget-object v6, v5, Lbbh;->a:Landroid/net/Uri;

    .line 14
    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 15
    if-eqz v3, :cond_2

    .line 16
    invoke-interface {v1}, Lbne;->b()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ed:Lbkq$a;

    .line 17
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 19
    iput-object v3, v5, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 20
    invoke-virtual {v2}, Lbnf;->a()V

    goto :goto_0

    .line 22
    :cond_2
    iget-object v3, p1, Lbnk;->c:Lgtm;

    invoke-virtual {v3}, Lgtm;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 23
    const-string v5, "CallingAccountSelector.processPreferredAccount"

    iget-object v3, p1, Lbnk;->c:Lgtm;

    .line 24
    invoke-virtual {v3}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbnx;

    iget-object v3, v3, Lbnx;->b:Lbnw;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xf

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "SIM suggested: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    .line 25
    invoke-static {v5, v3, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    :cond_3
    iget-object v3, p1, Lbnk;->b:Lgtm;

    .line 27
    invoke-virtual {v3}, Lgtm;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p1, Lbnk;->c:Lgtm;

    .line 28
    invoke-virtual {v5}, Lgtm;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbnx;

    .line 29
    invoke-virtual/range {v0 .. v5}, Lbnh;->a(Lbne;Lbnf;Ljava/lang/String;Ljava/lang/String;Lbnx;)V

    goto :goto_0
.end method
