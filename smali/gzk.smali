.class public final enum Lgzk;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# static fields
.field public static final enum a:Lgzk;

.field public static final enum b:Lgzk;

.field public static final c:Lhby;

.field private static enum d:Lgzk;

.field private static synthetic f:[Lgzk;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lgzk;

    const-string v1, "USER_PREFERENCE_UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2}, Lgzk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzk;->d:Lgzk;

    .line 12
    new-instance v0, Lgzk;

    const-string v1, "DO_NOT_DONATE"

    invoke-direct {v0, v1, v3, v3}, Lgzk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzk;->a:Lgzk;

    .line 13
    new-instance v0, Lgzk;

    const-string v1, "DONATE"

    invoke-direct {v0, v1, v4, v4}, Lgzk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzk;->b:Lgzk;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lgzk;

    sget-object v1, Lgzk;->d:Lgzk;

    aput-object v1, v0, v2

    sget-object v1, Lgzk;->a:Lgzk;

    aput-object v1, v0, v3

    sget-object v1, Lgzk;->b:Lgzk;

    aput-object v1, v0, v4

    sput-object v0, Lgzk;->f:[Lgzk;

    .line 15
    new-instance v0, Lgzl;

    invoke-direct {v0}, Lgzl;-><init>()V

    sput-object v0, Lgzk;->c:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lgzk;->e:I

    .line 10
    return-void
.end method

.method public static a(I)Lgzk;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 7
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lgzk;->d:Lgzk;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lgzk;->a:Lgzk;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lgzk;->b:Lgzk;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static values()[Lgzk;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgzk;->f:[Lgzk;

    invoke-virtual {v0}, [Lgzk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgzk;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lgzk;->e:I

    return v0
.end method
