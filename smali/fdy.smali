.class final Lfdy;
.super Landroid/telecom/Conference;
.source "PG"

# interfaces
.implements Lfde;


# direct methods
.method private constructor <init>(Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/telecom/Conference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 2
    const/16 v0, 0x43

    invoke-virtual {p0, v0}, Lfdy;->setConnectionCapabilities(I)V

    .line 4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    .line 5
    invoke-virtual {p0}, Lfdy;->getConnectionProperties()I

    move-result v0

    .line 6
    and-int/lit8 v1, v0, 0x8

    if-nez v1, :cond_0

    .line 7
    or-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v0}, Lfdy;->setConnectionProperties(I)V

    .line 8
    :cond_0
    invoke-virtual {p0}, Lfdy;->setActive()V

    .line 9
    return-void
.end method

.method static a(Landroid/telecom/PhoneAccountHandle;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 65
    const-string v0, "HangoutsConference.createHangoutsConferenceWithNewCall"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    const-string v0, "HangoutsConference.getOrCreateHangoutsConference"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    invoke-virtual {p1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 69
    invoke-virtual {v0}, Landroid/telecom/Connection;->getConference()Landroid/telecom/Conference;

    move-result-object v0

    .line 70
    instance-of v2, v0, Lfdy;

    if-eqz v2, :cond_0

    .line 71
    check-cast v0, Lfdy;

    move-object v2, v0

    .line 79
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 80
    instance-of v1, v0, Lfdd;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 81
    check-cast v1, Lfdd;

    .line 84
    iget-object v0, v1, Lfdd;->e:Lfdb;

    .line 85
    if-eqz v0, :cond_4

    .line 87
    iget-object v0, v1, Lfdd;->e:Lfdb;

    .line 88
    invoke-interface {v0}, Lfdb;->e()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_4

    const/4 v0, 0x1

    .line 89
    :goto_2
    if-eqz v0, :cond_1

    .line 90
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x34

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "HangoutsConference.adding connection to conference: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    invoke-virtual {v1}, Lfdd;->getCallAudioState()Landroid/telecom/CallAudioState;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, v1, Lfdd;->e:Lfdb;

    .line 94
    if-eqz v0, :cond_2

    .line 96
    iget-object v0, v1, Lfdd;->e:Lfdb;

    .line 97
    check-cast v0, Lfds;

    invoke-virtual {v0, v3}, Lfds;->d(Z)V

    .line 98
    :cond_2
    invoke-virtual {v2, v1}, Lfdy;->addConnection(Landroid/telecom/Connection;)Z

    goto :goto_1

    .line 74
    :cond_3
    const-string v0, "HangoutsConference.createHangoutsConference"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    new-instance v0, Lfdy;

    invoke-direct {v0, p0}, Lfdy;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 76
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->addConference(Landroid/telecom/Conference;)V

    move-object v2, v0

    .line 77
    goto :goto_0

    :cond_4
    move v0, v3

    .line 88
    goto :goto_2

    .line 100
    :cond_5
    return-void
.end method

.method private final b()Lfdd;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lfdy;->getConnections()Ljava/util/List;

    move-result-object v0

    .line 62
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdd;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final a(Lfdd;I)V
    .locals 3

    .prologue
    .line 46
    const-string v1, "HangoutsConference.onConnectionStateChanged, state: "

    .line 47
    invoke-static {p2}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 48
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    .line 50
    invoke-virtual {p0}, Lfdy;->setActive()V

    .line 59
    :cond_0
    :goto_1
    return-void

    .line 47
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_2
    const/4 v0, 0x5

    if-ne p2, v0, :cond_3

    .line 52
    invoke-virtual {p0}, Lfdy;->setOnHold()V

    goto :goto_1

    .line 53
    :cond_3
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 54
    invoke-virtual {p0, p1}, Lfdy;->removeConnection(Landroid/telecom/Connection;)V

    .line 55
    invoke-virtual {p1, p0}, Lfdd;->b(Lfde;)V

    .line 56
    invoke-virtual {p0}, Lfdy;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p1}, Lfdd;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfdy;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 58
    invoke-virtual {p0}, Lfdy;->destroy()V

    goto :goto_1
.end method

.method public final onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 22
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HangoutsConference.onCallAudioStateChanged, "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    invoke-direct {p0}, Lfdy;->b()Lfdd;

    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v0, p1}, Lfdd;->onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V

    .line 27
    :goto_0
    return-void

    .line 26
    :cond_0
    const-string v0, "No DialerConnection found while trying to change audio state."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onConnectionAdded(Landroid/telecom/Connection;)V
    .locals 2

    .prologue
    .line 43
    const-string v0, "HangoutsConference.onConnectionAdded."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    check-cast p1, Lfdd;

    invoke-virtual {p1, p0}, Lfdd;->a(Lfde;)V

    .line 45
    return-void
.end method

.method public final onDisconnect()V
    .locals 2

    .prologue
    .line 38
    const-string v0, "HangoutsConference.onDisconnect."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p0}, Lfdy;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 40
    invoke-virtual {v0}, Landroid/telecom/Connection;->onDisconnect()V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method

.method public final onHold()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "HangoutsConference.onHold"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-virtual {p0}, Lfdy;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 30
    invoke-virtual {v0}, Landroid/telecom/Connection;->onHold()V

    goto :goto_0

    .line 32
    :cond_0
    return-void
.end method

.method public final onPlayDtmfTone(C)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 10
    const-string v0, "HangoutsConference.onPlayDtmfTone"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    invoke-direct {p0}, Lfdy;->b()Lfdd;

    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    invoke-virtual {v0, p1}, Lfdd;->onPlayDtmfTone(C)V

    .line 15
    :goto_0
    return-void

    .line 14
    :cond_0
    const-string v0, "No DialerConnection found while trying to play dtmf tone."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onStopDtmfTone()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    const-string v0, "HangoutsConference.onStopDtmfTone"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    invoke-direct {p0}, Lfdy;->b()Lfdd;

    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {v0}, Lfdd;->onStopDtmfTone()V

    .line 21
    :goto_0
    return-void

    .line 20
    :cond_0
    const-string v0, "No DialerConnection found while trying to stop dtmf tone."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onUnhold()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "HangoutsConference.onUnhold"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    invoke-virtual {p0}, Lfdy;->getConnections()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 35
    invoke-virtual {v0}, Landroid/telecom/Connection;->onUnhold()V

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method
