.class public Lgxg;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgxg$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:Ljava/util/Map;

.field private static d:Ljava/util/Set;

.field private static e:Ljava/util/Map;

.field private static f:Ljava/util/Map;

.field private static g:Ljava/util/Map;

.field private static h:Ljava/lang/String;

.field private static i:Ljava/util/regex/Pattern;

.field private static j:Ljava/util/regex/Pattern;

.field private static k:Ljava/util/regex/Pattern;

.field private static l:Ljava/util/regex/Pattern;

.field private static m:Ljava/util/regex/Pattern;

.field private static n:Ljava/util/regex/Pattern;

.field private static o:Ljava/util/regex/Pattern;

.field private static p:Ljava/lang/String;

.field private static q:Ljava/lang/String;

.field private static r:Ljava/util/regex/Pattern;

.field private static s:Ljava/util/regex/Pattern;

.field private static t:Ljava/util/regex/Pattern;

.field private static u:Lgxg;


# instance fields
.field private A:Ljava/util/Set;

.field public final c:Ljava/util/Map;

.field private v:Lgxd;

.field private w:Lgxo;

.field private x:Ljava/util/Set;

.field private y:Lgxq;

.field private z:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x39

    const/16 v7, 0x37

    const/16 v5, 0x36

    const/16 v4, 0x34

    const/16 v6, 0x2d

    .line 783
    const-class v0, Lgxg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lgxg;->a:Ljava/util/logging/Logger;

    .line 784
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 785
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lgxg;->b:Ljava/util/Map;

    .line 788
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 789
    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 791
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    .line 792
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 793
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 794
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 795
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 796
    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 797
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 798
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lgxg;->d:Ljava/util/Set;

    .line 799
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 800
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    const/16 v0, 0x31

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    const/16 v0, 0x33

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    const/16 v0, 0x35

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 808
    const/16 v0, 0x38

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    new-instance v0, Ljava/util/HashMap;

    const/16 v2, 0x28

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 811
    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    const/16 v2, 0x44

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    const/16 v2, 0x4b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x35

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    const/16 v2, 0x4d

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    const/16 v2, 0x4e

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    const/16 v2, 0x4f

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    const/16 v2, 0x50

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    const/16 v2, 0x53

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    const/16 v2, 0x54

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    const/16 v2, 0x55

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    const/16 v2, 0x56

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x38

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    const/16 v2, 0x57

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    const/16 v2, 0x58

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    const/16 v2, 0x59

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    const/16 v2, 0x5a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 837
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lgxg;->f:Ljava/util/Map;

    .line 838
    new-instance v0, Ljava/util/HashMap;

    const/16 v2, 0x64

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 839
    sget-object v2, Lgxg;->f:Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 840
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 841
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lgxg;->g:Ljava/util/Map;

    .line 842
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 843
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 844
    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x2b

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 845
    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x2a

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    const/16 v3, 0x23

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 847
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lgxg;->e:Ljava/util/Map;

    .line 848
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 849
    sget-object v0, Lgxg;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 850
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 853
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 854
    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    const v0, 0xff0d

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    const/16 v0, 0x2010

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 857
    const/16 v0, 0x2011

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    const/16 v0, 0x2012

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 859
    const/16 v0, 0x2013

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 860
    const/16 v0, 0x2014

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    const/16 v0, 0x2015

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    const/16 v0, 0x2212

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    const/16 v0, 0x2f

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    const v0, 0xff0f

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 866
    const/16 v0, 0x3000

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    const/16 v0, 0x2060

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    const/16 v0, 0x2e

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    const v0, 0xff0e

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    .line 871
    const-string v0, "[\\d]+(?:[~\u2053\u223c\uff5e][\\d]+)?"

    .line 872
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lgxg;->f:Ljava/util/Map;

    .line 874
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[, \\[\\]]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lgxg;->f:Ljava/util/Map;

    .line 875
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 876
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[, \\[\\]]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgxg;->h:Ljava/lang/String;

    .line 877
    const-string v0, "[+\uff0b]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->i:Ljava/util/regex/Pattern;

    .line 878
    const-string v0, "[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->j:Ljava/util/regex/Pattern;

    .line 879
    const-string v0, "(\\p{Nd})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->k:Ljava/util/regex/Pattern;

    .line 880
    const-string v0, "[+\uff0b\\p{Nd}]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->l:Ljava/util/regex/Pattern;

    .line 881
    const-string v0, "[\\\\/] *x"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->m:Ljava/util/regex/Pattern;

    .line 882
    const-string v0, "[[\\P{N}&&\\P{L}]&&[^#]]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->n:Ljava/util/regex/Pattern;

    .line 883
    const-string v0, "(?:.*?[A-Za-z]){3}.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->o:Ljava/util/regex/Pattern;

    .line 884
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lgxg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\p{Nd}]*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgxg;->p:Ljava/lang/String;

    .line 885
    const-string v0, "x\uff58#\uff03~\uff5e"

    .line 886
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ",;"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 887
    invoke-static {v1}, Lgxg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lgxg;->q:Ljava/lang/String;

    .line 888
    invoke-static {v0}, Lgxg;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 889
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(?:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lgxg;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")$"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x42

    .line 890
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->r:Ljava/util/regex/Pattern;

    .line 891
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lgxg;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lgxg;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x42

    .line 892
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->s:Ljava/util/regex/Pattern;

    .line 893
    const-string v0, "(\\D+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 894
    const-string v0, "(\\$\\d)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgxg;->t:Ljava/util/regex/Pattern;

    .line 895
    const-string v0, "\\(?\\$1\\)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 896
    const/4 v0, 0x0

    sput-object v0, Lgxg;->u:Lgxg;

    return-void
.end method

.method private constructor <init>(Lgxd;Ljava/util/Map;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Lgxp;

    invoke-direct {v0}, Lgxp;-><init>()V

    .line 5
    iput-object v0, p0, Lgxg;->w:Lgxo;

    .line 6
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x23

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lgxg;->x:Ljava/util/Set;

    .line 7
    new-instance v0, Lgxq;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lgxq;-><init>(I)V

    iput-object v0, p0, Lgxg;->y:Lgxq;

    .line 8
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x140

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lgxg;->z:Ljava/util/Set;

    .line 9
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgxg;->A:Ljava/util/Set;

    .line 10
    iput-object p1, p0, Lgxg;->v:Lgxd;

    .line 11
    iput-object p2, p0, Lgxg;->c:Ljava/util/Map;

    .line 12
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 13
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 14
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v5, :cond_0

    const-string v3, "001"

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 15
    iget-object v1, p0, Lgxg;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lgxg;->z:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 18
    :cond_1
    iget-object v0, p0, Lgxg;->z:Ljava/util/Set;

    const-string v1, "001"

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19
    sget-object v0, Lgxg;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 20
    :cond_2
    iget-object v1, p0, Lgxg;->x:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 21
    return-void
.end method

.method private final a(Ljava/lang/CharSequence;Lgxi;I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 387
    :goto_0
    invoke-static {p2, p3}, Lgxg;->a(Lgxi;I)Lgxk;

    move-result-object v1

    .line 389
    iget-object v0, v1, Lgxk;->b:Ljava/util/List;

    .line 390
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p2, Lgxi;->a:Lgxk;

    .line 394
    iget-object v0, v0, Lgxk;->b:Ljava/util/List;

    .line 399
    :goto_1
    iget-object v1, v1, Lgxk;->c:Ljava/util/List;

    .line 401
    sget v2, Lmg$c;->aj:I

    if-ne p3, v2, :cond_a

    .line 402
    sget v2, Lmg$c;->ah:I

    invoke-static {p2, v2}, Lgxg;->a(Lgxi;I)Lgxk;

    move-result-object v2

    invoke-static {v2}, Lgxg;->a(Lgxk;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 403
    sget p3, Lmg$c;->ai:I

    goto :goto_0

    .line 396
    :cond_0
    iget-object v0, v1, Lgxk;->b:Ljava/util/List;

    goto :goto_1

    .line 404
    :cond_1
    sget v2, Lmg$c;->ai:I

    invoke-static {p2, v2}, Lgxg;->a(Lgxi;I)Lgxk;

    move-result-object v3

    .line 405
    invoke-static {v3}, Lgxg;->a(Lgxk;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 406
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 408
    iget-object v0, v3, Lgxk;->b:Ljava/util/List;

    .line 409
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 411
    iget-object v0, p2, Lgxi;->a:Lgxk;

    .line 413
    iget-object v0, v0, Lgxk;->b:Ljava/util/List;

    .line 417
    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 418
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 419
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, v3, Lgxk;->c:Ljava/util/List;

    move-object v1, v0

    .line 428
    :goto_3
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_4

    .line 429
    sget v0, Lmg$c;->ay:I

    .line 440
    :goto_4
    return v0

    .line 416
    :cond_2
    iget-object v0, v3, Lgxk;->b:Ljava/util/List;

    goto :goto_2

    .line 423
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 425
    iget-object v1, v3, Lgxk;->c:Ljava/util/List;

    .line 426
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 427
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object v1, v0

    goto :goto_3

    .line 430
    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 431
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 432
    sget v0, Lmg$c;->av:I

    goto :goto_4

    .line 433
    :cond_5
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 434
    if-ne v0, v3, :cond_6

    .line 435
    sget v0, Lmg$c;->au:I

    goto :goto_4

    .line 436
    :cond_6
    if-le v0, v3, :cond_7

    .line 437
    sget v0, Lmg$c;->ax:I

    goto :goto_4

    .line 438
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, v3, :cond_8

    .line 439
    sget v0, Lmg$c;->az:I

    goto :goto_4

    .line 440
    :cond_8
    const/4 v0, 0x1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget v0, Lmg$c;->au:I

    goto :goto_4

    :cond_9
    sget v0, Lmg$c;->ay:I

    goto :goto_4

    :cond_a
    move-object v2, v0

    goto :goto_3
.end method

.method private a(Ljava/lang/CharSequence;Lgxi;Ljava/lang/StringBuilder;ZLgxl;)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 451
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 514
    :goto_0
    return v0

    .line 453
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 454
    const-string v0, "NonMatch"

    .line 455
    if-eqz p2, :cond_1

    .line 457
    iget-object v0, p2, Lgxi;->n:Ljava/lang/String;

    .line 461
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_6

    .line 462
    sget-object v4, Lgxg;->i:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 463
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 464
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {v3, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 465
    invoke-static {v3}, Lgxg;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 466
    sget-object v0, Lgxm;->a:Lgxm;

    .line 482
    :goto_1
    if-eqz p4, :cond_2

    .line 483
    invoke-virtual {p5, v0}, Lgxl;->a(Lgxm;)Lgxl;

    .line 484
    :cond_2
    sget-object v2, Lgxm;->d:Lgxm;

    if-eq v0, v2, :cond_9

    .line 485
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_7

    .line 486
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->c:Lgxf;

    const-string v2, "Phone number had an IDD, but after this was not long enough to be a viable phone number."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_3
    iget-object v4, p0, Lgxg;->y:Lgxq;

    invoke-virtual {v4, v0}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 468
    invoke-static {v3}, Lgxg;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 470
    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 471
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 472
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 473
    sget-object v4, Lgxg;->k:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 474
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 475
    invoke-virtual {v4, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lgxg;->b(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 476
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 477
    :cond_4
    invoke-virtual {v3, v1, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v0, v2

    .line 480
    :goto_2
    if-eqz v0, :cond_6

    sget-object v0, Lgxm;->b:Lgxm;

    goto :goto_1

    :cond_5
    move v0, v1

    .line 479
    goto :goto_2

    .line 480
    :cond_6
    sget-object v0, Lgxm;->d:Lgxm;

    goto :goto_1

    .line 487
    :cond_7
    invoke-direct {p0, v3, p3}, Lgxg;->a(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I

    move-result v0

    .line 488
    if-eqz v0, :cond_8

    .line 489
    invoke-virtual {p5, v0}, Lgxl;->a(I)Lgxl;

    goto/16 :goto_0

    .line 491
    :cond_8
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->a:Lgxf;

    const-string v2, "Country calling code supplied was not recognised."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 492
    :cond_9
    if-eqz p2, :cond_d

    .line 494
    iget v0, p2, Lgxi;->m:I

    .line 496
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 497
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 498
    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 499
    new-instance v5, Ljava/lang/StringBuilder;

    .line 500
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 502
    iget-object v2, p2, Lgxi;->a:Lgxk;

    .line 504
    const/4 v4, 0x0

    invoke-direct {p0, v5, p2, v4}, Lgxg;->a(Ljava/lang/StringBuilder;Lgxi;Ljava/lang/StringBuilder;)Z

    .line 505
    iget-object v4, p0, Lgxg;->w:Lgxo;

    invoke-interface {v4, v3, v2, v1}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, p0, Lgxg;->w:Lgxo;

    .line 506
    invoke-interface {v4, v5, v2, v1}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v2

    if-nez v2, :cond_b

    .line 507
    :cond_a
    invoke-virtual {p0, v3, p2}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;)I

    move-result v2

    sget v3, Lmg$c;->az:I

    if-ne v2, v3, :cond_d

    .line 508
    :cond_b
    invoke-virtual {p3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 509
    if-eqz p4, :cond_c

    .line 510
    sget-object v1, Lgxm;->c:Lgxm;

    invoke-virtual {p5, v1}, Lgxl;->a(Lgxm;)Lgxl;

    .line 511
    :cond_c
    invoke-virtual {p5, v0}, Lgxl;->a(I)Lgxl;

    goto/16 :goto_0

    .line 513
    :cond_d
    invoke-virtual {p5, v1}, Lgxl;->a(I)Lgxl;

    move v0, v1

    .line 514
    goto/16 :goto_0
.end method

.method private final a(Ljava/lang/String;Lgxi;)I
    .locals 1

    .prologue
    .line 274
    .line 275
    iget-object v0, p2, Lgxi;->a:Lgxk;

    .line 276
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    sget v0, Lmg$c;->as:I

    .line 330
    :goto_0
    return v0

    .line 279
    :cond_0
    iget-object v0, p2, Lgxi;->e:Lgxk;

    .line 280
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    sget v0, Lmg$c;->al:I

    goto :goto_0

    .line 283
    :cond_1
    iget-object v0, p2, Lgxi;->d:Lgxk;

    .line 284
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    sget v0, Lmg$c;->ak:I

    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p2, Lgxi;->f:Lgxk;

    .line 288
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    sget v0, Lmg$c;->am:I

    goto :goto_0

    .line 291
    :cond_3
    iget-object v0, p2, Lgxi;->h:Lgxk;

    .line 292
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 293
    sget v0, Lmg$c;->an:I

    goto :goto_0

    .line 295
    :cond_4
    iget-object v0, p2, Lgxi;->g:Lgxk;

    .line 296
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 297
    sget v0, Lmg$c;->ao:I

    goto :goto_0

    .line 299
    :cond_5
    iget-object v0, p2, Lgxi;->i:Lgxk;

    .line 300
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 301
    sget v0, Lmg$c;->ap:I

    goto :goto_0

    .line 303
    :cond_6
    iget-object v0, p2, Lgxi;->j:Lgxk;

    .line 304
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 305
    sget v0, Lmg$c;->aq:I

    goto :goto_0

    .line 307
    :cond_7
    iget-object v0, p2, Lgxi;->k:Lgxk;

    .line 308
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 309
    sget v0, Lmg$c;->ar:I

    goto :goto_0

    .line 311
    :cond_8
    iget-object v0, p2, Lgxi;->b:Lgxk;

    .line 312
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    .line 313
    if-eqz v0, :cond_b

    .line 315
    iget-boolean v0, p2, Lgxi;->t:Z

    .line 316
    if-eqz v0, :cond_9

    .line 317
    sget v0, Lmg$c;->aj:I

    goto :goto_0

    .line 319
    :cond_9
    iget-object v0, p2, Lgxi;->c:Lgxk;

    .line 320
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 321
    sget v0, Lmg$c;->aj:I

    goto :goto_0

    .line 322
    :cond_a
    sget v0, Lmg$c;->ah:I

    goto :goto_0

    .line 324
    :cond_b
    iget-boolean v0, p2, Lgxi;->t:Z

    .line 325
    if-nez v0, :cond_c

    .line 327
    iget-object v0, p2, Lgxi;->c:Lgxk;

    .line 328
    invoke-direct {p0, p1, v0}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 329
    sget v0, Lmg$c;->ai:I

    goto/16 :goto_0

    .line 330
    :cond_c
    sget v0, Lmg$c;->as:I

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 441
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v2, 0x30

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 450
    :goto_0
    return v0

    .line 443
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    .line 444
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    const/4 v0, 0x3

    if-gt v2, v0, :cond_3

    if-gt v2, v3, :cond_3

    .line 445
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 446
    iget-object v4, p0, Lgxg;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 447
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 449
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 450
    goto :goto_0
.end method

.method public static declared-synchronized a()Lgxg;
    .locals 4

    .prologue
    .line 54
    const-class v1, Lgxg;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lgxg;->u:Lgxg;

    if-nez v0, :cond_1

    .line 55
    sget-object v0, Lgxc;->a:Lgxb;

    .line 56
    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "metadataLoader could not be null."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 58
    :cond_0
    :try_start_1
    new-instance v2, Lgxd;

    invoke-direct {v2, v0}, Lgxd;-><init>(Lgxb;)V

    .line 59
    new-instance v0, Lgxg;

    .line 60
    invoke-static {}, Lhcw;->l()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lgxg;-><init>(Lgxd;Ljava/util/Map;)V

    .line 61
    invoke-static {v0}, Lgxg;->a(Lgxg;)V

    .line 62
    :cond_1
    sget-object v0, Lgxg;->u:Lgxg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private final a(ILjava/lang/String;)Lgxi;
    .locals 2

    .prologue
    .line 114
    const-string v0, "001"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lgxg;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 118
    :cond_0
    iget-object v0, p0, Lgxg;->v:Lgxd;

    invoke-virtual {v0, p1}, Lgxd;->a(I)Lgxi;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p0, p2}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lgxi;I)Lgxk;
    .locals 1

    .prologue
    .line 232
    add-int/lit8 v0, p1, -0x1

    packed-switch v0, :pswitch_data_0

    .line 264
    iget-object v0, p0, Lgxi;->a:Lgxk;

    .line 265
    :goto_0
    return-object v0

    .line 234
    :pswitch_0
    iget-object v0, p0, Lgxi;->e:Lgxk;

    goto :goto_0

    .line 237
    :pswitch_1
    iget-object v0, p0, Lgxi;->d:Lgxk;

    goto :goto_0

    .line 240
    :pswitch_2
    iget-object v0, p0, Lgxi;->c:Lgxk;

    goto :goto_0

    .line 243
    :pswitch_3
    iget-object v0, p0, Lgxi;->b:Lgxk;

    goto :goto_0

    .line 246
    :pswitch_4
    iget-object v0, p0, Lgxi;->f:Lgxk;

    goto :goto_0

    .line 249
    :pswitch_5
    iget-object v0, p0, Lgxi;->h:Lgxk;

    goto :goto_0

    .line 252
    :pswitch_6
    iget-object v0, p0, Lgxi;->g:Lgxk;

    goto :goto_0

    .line 255
    :pswitch_7
    iget-object v0, p0, Lgxi;->i:Lgxk;

    goto :goto_0

    .line 258
    :pswitch_8
    iget-object v0, p0, Lgxi;->j:Lgxk;

    goto :goto_0

    .line 261
    :pswitch_9
    iget-object v0, p0, Lgxi;->k:Lgxk;

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lgxl;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-boolean v1, p0, Lgxl;->h:Z

    .line 125
    if-eqz v1, :cond_0

    .line 126
    iget v1, p0, Lgxl;->j:I

    .line 127
    if-lez v1, :cond_0

    .line 129
    iget v1, p0, Lgxl;->j:I

    .line 130
    new-array v1, v1, [C

    .line 131
    const/16 v2, 0x30

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([CC)V

    .line 132
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    iget-wide v2, p0, Lgxl;->d:J

    .line 135
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Lgxl;Ljava/util/List;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 369
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v1

    .line 370
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 371
    invoke-virtual {p0, v0}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v3

    .line 373
    iget-boolean v4, v3, Lgxi;->w:Z

    .line 374
    if-eqz v4, :cond_1

    .line 375
    iget-object v4, p0, Lgxg;->y:Lgxq;

    .line 376
    iget-object v3, v3, Lgxi;->x:Ljava/lang/String;

    .line 377
    invoke-virtual {v4, v3}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    .line 378
    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 383
    :goto_0
    return-object v0

    .line 380
    :cond_1
    invoke-direct {p0, v1, v3}, Lgxg;->a(Ljava/lang/String;Lgxi;)I

    move-result v3

    sget v4, Lmg$c;->as:I

    if-eq v3, v4, :cond_0

    goto :goto_0

    .line 383
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/util/Map;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 42
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 43
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 44
    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 47
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;Lgxi;ILjava/lang/CharSequence;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 146
    .line 147
    iget-object v0, p2, Lgxi;->v:Ljava/util/List;

    .line 150
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lmg$c;->ae:I

    if-ne p3, v0, :cond_3

    .line 152
    :cond_0
    iget-object v0, p2, Lgxi;->u:Ljava/util/List;

    .line 158
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxh;

    .line 159
    invoke-virtual {v0}, Lgxh;->a()I

    move-result v1

    .line 160
    if-eqz v1, :cond_2

    iget-object v3, p0, Lgxg;->y:Lgxq;

    add-int/lit8 v1, v1, -0x1

    .line 162
    iget-object v4, v0, Lgxh;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 163
    invoke-virtual {v3, v1}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 164
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 165
    :cond_2
    iget-object v1, p0, Lgxg;->y:Lgxq;

    .line 166
    iget-object v3, v0, Lgxh;->a:Ljava/lang/String;

    .line 167
    invoke-virtual {v1, v3}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 168
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 173
    :goto_1
    if-nez v1, :cond_5

    .line 210
    :goto_2
    return-object p1

    .line 155
    :cond_3
    iget-object v0, p2, Lgxi;->v:Ljava/util/List;

    goto :goto_0

    .line 171
    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 176
    :cond_5
    iget-object v0, v1, Lgxh;->b:Ljava/lang/String;

    .line 178
    iget-object v2, p0, Lgxg;->y:Lgxq;

    .line 180
    iget-object v3, v1, Lgxh;->a:Ljava/lang/String;

    .line 181
    invoke-virtual {v2, v3}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 182
    sget v3, Lmg$c;->ae:I

    if-ne p3, v3, :cond_9

    if-eqz p4, :cond_9

    .line 183
    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_9

    .line 185
    iget-object v3, v1, Lgxh;->e:Ljava/lang/String;

    .line 186
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_9

    .line 188
    iget-object v1, v1, Lgxh;->e:Ljava/lang/String;

    .line 190
    const-string v3, "$CC"

    invoke-virtual {v1, v3, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 191
    sget-object v3, Lgxg;->t:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 192
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    :cond_6
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    :goto_3
    sget v1, Lmg$c;->af:I

    if-ne p3, v1, :cond_8

    .line 205
    sget-object v1, Lgxg;->j:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 206
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 207
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    :cond_7
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_8
    move-object p1, v0

    .line 210
    goto :goto_2

    .line 195
    :cond_9
    iget-object v1, v1, Lgxh;->d:Ljava/lang/String;

    .line 197
    sget v3, Lmg$c;->ae:I

    if-ne p3, v3, :cond_6

    if-eqz v1, :cond_6

    .line 198
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    .line 199
    sget-object v3, Lgxg;->t:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 201
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    sget-object v0, Lgxg;->o:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sget-object v1, Lgxg;->g:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/util/Map;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    :goto_0
    return-object p0

    .line 29
    :cond_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-static {p0}, Lgxg;->b(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v0, v1}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(IILjava/lang/StringBuilder;)V
    .locals 3

    .prologue
    const/16 v1, 0x2b

    const/4 v2, 0x0

    .line 137
    add-int/lit8 v0, p1, -0x1

    packed-switch v0, :pswitch_data_0

    .line 145
    :goto_0
    :pswitch_0
    return-void

    .line 138
    :pswitch_1
    invoke-virtual {p2, v2, p0}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 140
    :pswitch_2
    const-string v0, " "

    invoke-virtual {p2, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 142
    :pswitch_3
    const-string v0, "-"

    invoke-virtual {p2, v2, v0}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tel:"

    .line 143
    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static declared-synchronized a(Lgxg;)V
    .locals 2

    .prologue
    .line 49
    const-class v0, Lgxg;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lgxg;->u:Lgxg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    monitor-exit v0

    return-void

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Lgxl;Lgxi;ILjava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 211
    .line 212
    iget-boolean v0, p0, Lgxl;->e:Z

    .line 213
    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lgxl;->f:Ljava/lang/String;

    .line 215
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 216
    sget v0, Lmg$c;->af:I

    if-ne p2, v0, :cond_1

    .line 217
    const-string v0, ";ext="

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 219
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget-boolean v0, p1, Lgxi;->p:Z

    .line 222
    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p1, Lgxi;->q:Ljava/lang/String;

    .line 225
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 227
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 228
    :cond_2
    const-string v0, " ext. "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 230
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;Lgxl;)V
    .locals 4

    .prologue
    const/16 v3, 0x30

    const/4 v1, 0x1

    .line 564
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v3, :cond_1

    .line 565
    invoke-virtual {p1, v1}, Lgxl;->a(Z)Lgxl;

    move v0, v1

    .line 567
    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 568
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 570
    :cond_0
    if-eq v0, v1, :cond_1

    .line 571
    invoke-virtual {p1, v0}, Lgxl;->b(I)Lgxl;

    .line 572
    :cond_1
    return-void
.end method

.method public static a(II)Z
    .locals 2

    .prologue
    .line 63
    sget v0, Lmg$c;->ah:I

    if-eq p0, v0, :cond_0

    sget v0, Lmg$c;->aj:I

    if-eq p0, v0, :cond_0

    sget-object v0, Lgxg;->d:Ljava/util/Set;

    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lmg$c;->ai:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    .line 64
    :cond_1
    const/4 v0, 0x0

    .line 65
    goto :goto_0
.end method

.method private static a(Lgxk;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 51
    invoke-virtual {p0}, Lgxk;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 52
    iget-object v0, p0, Lgxk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 53
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 22
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 23
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0

    .line 24
    :cond_0
    sget-object v0, Lgxg;->s:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lgxk;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 334
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 336
    iget-object v2, p2, Lgxk;->b:Ljava/util/List;

    .line 338
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lgxg;->w:Lgxo;

    invoke-interface {v1, p1, p2, v0}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;Lgxi;Ljava/lang/StringBuilder;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 515
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 517
    iget-object v3, p2, Lgxi;->r:Ljava/lang/String;

    .line 519
    if-eqz v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 550
    :cond_0
    :goto_0
    return v0

    .line 521
    :cond_1
    iget-object v4, p0, Lgxg;->y:Lgxq;

    invoke-virtual {v4, v3}, Lgxq;->a(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 522
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 524
    iget-object v4, p2, Lgxi;->a:Lgxk;

    .line 526
    iget-object v5, p0, Lgxg;->w:Lgxo;

    invoke-interface {v5, p1, v4, v0}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v5

    .line 527
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    .line 529
    iget-object v7, p2, Lgxi;->s:Ljava/lang/String;

    .line 531
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_2

    .line 532
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_5

    .line 533
    :cond_2
    if-eqz v5, :cond_3

    iget-object v2, p0, Lgxg;->w:Lgxo;

    .line 534
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 535
    invoke-interface {v2, v5, v4, v0}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 537
    :cond_3
    if-eqz p3, :cond_4

    if-lez v6, :cond_4

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 538
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539
    :cond_4
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move v0, v1

    .line 540
    goto :goto_0

    .line 541
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 542
    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v0, v2, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 543
    if-eqz v5, :cond_6

    iget-object v2, p0, Lgxg;->w:Lgxo;

    .line 544
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v4, v0}, Lgxo;->a(Ljava/lang/CharSequence;Lgxk;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 546
    :cond_6
    if-eqz p3, :cond_7

    if-le v6, v1, :cond_7

    .line 547
    invoke-virtual {v3, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 549
    goto/16 :goto_0
.end method

.method private b(Lgxl;Ljava/lang/CharSequence;)Lgxg$a;
    .locals 6

    .prologue
    .line 756
    :try_start_0
    const-string v0, "ZZ"

    invoke-virtual {p0, p2, v0}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v0

    .line 757
    invoke-virtual {p0, p1, v0}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 775
    :cond_0
    :goto_0
    return-object v0

    .line 758
    :catch_0
    move-exception v0

    .line 760
    iget-object v0, v0, Lgxe;->a:Lgxf;

    .line 761
    sget-object v1, Lgxf;->a:Lgxf;

    if-ne v0, v1, :cond_2

    .line 763
    iget v0, p1, Lgxl;->b:I

    .line 764
    invoke-virtual {p0, v0}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 765
    :try_start_1
    const-string v1, "ZZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 766
    invoke-virtual {p0, p2, v0}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v0

    .line 767
    invoke-virtual {p0, p1, v0}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;

    move-result-object v0

    .line 768
    sget-object v1, Lgxg$a;->e:Lgxg$a;

    if-ne v0, v1, :cond_0

    .line 769
    sget-object v0, Lgxg$a;->d:Lgxg$a;

    goto :goto_0

    .line 771
    :cond_1
    new-instance v5, Lgxl;

    invoke-direct {v5}, Lgxl;-><init>()V

    .line 772
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V

    .line 773
    invoke-virtual {p0, p1, v5}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;
    :try_end_1
    .catch Lgxe; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    .line 775
    :cond_2
    sget-object v0, Lgxg$a;->a:Lgxg$a;

    goto :goto_0
.end method

.method private static b(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 31
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 33
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 34
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 35
    const/16 v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Character;->digit(CI)I

    move-result v2

    .line 36
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]|int|anexo|\uff49\uff4e\uff54)[:\\.\uff0e]?[ \u00a0\\t,-]*(\\p{Nd}{1,7})"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#?|[- ]+(\\p{Nd}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{1,5})#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 551
    sget-object v0, Lgxg;->r:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 552
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgxg;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    :goto_0
    if-gt v0, v2, :cond_1

    .line 554
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 555
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 556
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 559
    :goto_1
    return-object v0

    .line 558
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 559
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private static b(Lgxl;Lgxl;)Z
    .locals 4

    .prologue
    .line 728
    .line 729
    iget-wide v0, p0, Lgxl;->d:J

    .line 730
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 732
    iget-wide v2, p1, Lgxl;->d:J

    .line 733
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 734
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 735
    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 736
    :goto_0
    return v0

    .line 735
    :cond_1
    const/4 v0, 0x0

    .line 736
    goto :goto_0
.end method

.method private final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 66
    if-eqz p1, :cond_0

    iget-object v0, p0, Lgxg;->z:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(Lgxl;)Lgxl;
    .locals 4

    .prologue
    .line 675
    new-instance v0, Lgxl;

    invoke-direct {v0}, Lgxl;-><init>()V

    .line 677
    iget v1, p0, Lgxl;->b:I

    .line 678
    invoke-virtual {v0, v1}, Lgxl;->a(I)Lgxl;

    .line 680
    iget-wide v2, p0, Lgxl;->d:J

    .line 681
    invoke-virtual {v0, v2, v3}, Lgxl;->a(J)Lgxl;

    .line 683
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 684
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 686
    iget-object v1, p0, Lgxl;->f:Ljava/lang/String;

    .line 687
    invoke-virtual {v0, v1}, Lgxl;->a(Ljava/lang/String;)Lgxl;

    .line 689
    :cond_0
    iget-boolean v1, p0, Lgxl;->h:Z

    .line 690
    if-eqz v1, :cond_1

    .line 691
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgxl;->a(Z)Lgxl;

    .line 693
    iget v1, p0, Lgxl;->j:I

    .line 694
    invoke-virtual {v0, v1}, Lgxl;->b(I)Lgxl;

    .line 695
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Lgxi;)I
    .locals 1

    .prologue
    .line 386
    sget v0, Lmg$c;->as:I

    invoke-direct {p0, p1, p2, v0}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;I)I

    move-result v0

    return v0
.end method

.method public final a(Lgxl;Lgxl;)Lgxg$a;
    .locals 5

    .prologue
    .line 696
    invoke-static {p1}, Lgxg;->f(Lgxl;)Lgxl;

    move-result-object v0

    .line 697
    invoke-static {p2}, Lgxg;->f(Lgxl;)Lgxl;

    move-result-object v1

    .line 699
    iget-boolean v2, v0, Lgxl;->e:Z

    .line 700
    if-eqz v2, :cond_0

    .line 701
    iget-boolean v2, v1, Lgxl;->e:Z

    .line 702
    if-eqz v2, :cond_0

    .line 704
    iget-object v2, v0, Lgxl;->f:Ljava/lang/String;

    .line 706
    iget-object v3, v1, Lgxl;->f:Ljava/lang/String;

    .line 707
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 708
    sget-object v0, Lgxg$a;->b:Lgxg$a;

    .line 727
    :goto_0
    return-object v0

    .line 710
    :cond_0
    iget v2, v0, Lgxl;->b:I

    .line 713
    iget v3, v1, Lgxl;->b:I

    .line 715
    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 716
    invoke-virtual {v0, v1}, Lgxl;->a(Lgxl;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 717
    sget-object v0, Lgxg$a;->e:Lgxg$a;

    goto :goto_0

    .line 718
    :cond_1
    if-ne v2, v3, :cond_2

    .line 719
    invoke-static {v0, v1}, Lgxg;->b(Lgxl;Lgxl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 720
    sget-object v0, Lgxg$a;->c:Lgxg$a;

    goto :goto_0

    .line 721
    :cond_2
    sget-object v0, Lgxg$a;->b:Lgxg$a;

    goto :goto_0

    .line 722
    :cond_3
    invoke-virtual {v0, v3}, Lgxl;->a(I)Lgxl;

    .line 723
    invoke-virtual {v0, v1}, Lgxl;->a(Lgxl;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 724
    sget-object v0, Lgxg$a;->d:Lgxg$a;

    goto :goto_0

    .line 725
    :cond_4
    invoke-static {v0, v1}, Lgxg;->b(Lgxl;Lgxl;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 726
    sget-object v0, Lgxg$a;->c:Lgxg$a;

    goto :goto_0

    .line 727
    :cond_5
    sget-object v0, Lgxg$a;->b:Lgxg$a;

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lgxg$a;
    .locals 12

    .prologue
    .line 737
    :try_start_0
    const-string v0, "ZZ"

    invoke-virtual {p0, p1, v0}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v0

    .line 738
    invoke-direct {p0, v0, p2}, Lgxg;->b(Lgxl;Ljava/lang/CharSequence;)Lgxg$a;
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 755
    :goto_0
    return-object v0

    .line 739
    :catch_0
    move-exception v0

    .line 741
    iget-object v0, v0, Lgxe;->a:Lgxf;

    .line 742
    sget-object v1, Lgxf;->a:Lgxf;

    if-ne v0, v1, :cond_0

    .line 743
    :try_start_1
    const-string v0, "ZZ"

    invoke-virtual {p0, p2, v0}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;

    move-result-object v0

    .line 744
    invoke-direct {p0, v0, p1}, Lgxg;->b(Lgxl;Ljava/lang/CharSequence;)Lgxg$a;
    :try_end_1
    .catch Lgxe; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 745
    :catch_1
    move-exception v0

    .line 747
    iget-object v0, v0, Lgxe;->a:Lgxf;

    .line 748
    sget-object v1, Lgxf;->a:Lgxf;

    if-ne v0, v1, :cond_0

    .line 749
    :try_start_2
    new-instance v5, Lgxl;

    invoke-direct {v5}, Lgxl;-><init>()V

    .line 750
    new-instance v11, Lgxl;

    invoke-direct {v11}, Lgxl;-><init>()V

    .line 751
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V

    .line 752
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p0

    move-object v7, p2

    invoke-virtual/range {v6 .. v11}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V

    .line 753
    invoke-virtual {p0, v5, v11}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;
    :try_end_2
    .catch Lgxe; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    .line 755
    :cond_0
    sget-object v0, Lgxg$a;->a:Lgxg$a;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lgxi;
    .locals 1

    .prologue
    .line 331
    invoke-direct {p0, p1}, Lgxg;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    const/4 v0, 0x0

    .line 333
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgxg;->v:Lgxd;

    invoke-virtual {v0, p1}, Lgxd;->a(Ljava/lang/String;)Lgxi;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)Lgxl;
    .locals 6

    .prologue
    .line 560
    new-instance v5, Lgxl;

    invoke-direct {v5}, Lgxl;-><init>()V

    .line 562
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lgxg;->a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V

    .line 563
    return-object v5
.end method

.method public final a(Lgxl;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 68
    .line 69
    iget-wide v0, p1, Lgxl;->d:J

    .line 70
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 71
    iget-boolean v0, p1, Lgxl;->k:Z

    .line 72
    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p1, Lgxl;->l:Ljava/lang/String;

    .line 76
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 100
    :goto_0
    return-object v0

    .line 78
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 80
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 82
    iget v1, p1, Lgxl;->b:I

    .line 84
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v2

    .line 85
    sget v3, Lmg$c;->ac:I

    if-ne p2, v3, :cond_1

    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    sget v2, Lmg$c;->ac:I

    invoke-static {v1, v2, v0}, Lgxg;->a(IILjava/lang/StringBuilder;)V

    .line 100
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0, v1}, Lgxg;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 92
    :cond_2
    invoke-virtual {p0, v1}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-direct {p0, v1, v3}, Lgxg;->a(ILjava/lang/String;)Lgxi;

    move-result-object v3

    .line 96
    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, p2, v4}, Lgxg;->a(Ljava/lang/String;Lgxi;ILjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 97
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-static {p1, v3, p2, v0}, Lgxg;->a(Lgxl;Lgxi;ILjava/lang/StringBuilder;)V

    .line 99
    invoke-static {v1, p2, v0}, Lgxg;->a(IILjava/lang/StringBuilder;)V

    goto :goto_1
.end method

.method public final a(Lgxl;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 101
    .line 102
    iget v1, p1, Lgxl;->b:I

    .line 104
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {p0, v1}, Lgxg;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    :goto_0
    return-object v0

    .line 107
    :cond_0
    invoke-virtual {p0, v1}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 108
    invoke-direct {p0, v1, v2}, Lgxg;->a(ILjava/lang/String;)Lgxi;

    move-result-object v2

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x14

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 110
    sget v4, Lmg$c;->ae:I

    invoke-direct {p0, v0, v2, v4, p2}, Lgxg;->a(Ljava/lang/String;Lgxi;ILjava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    sget v0, Lmg$c;->ae:I

    invoke-static {p1, v2, v0, v3}, Lgxg;->a(Lgxl;Lgxi;ILjava/lang/StringBuilder;)V

    .line 112
    sget v0, Lmg$c;->ae:I

    invoke-static {v1, v0, v3}, Lgxg;->a(IILjava/lang/StringBuilder;)V

    .line 113
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;ZZLgxl;)V
    .locals 10

    .prologue
    .line 573
    if-nez p1, :cond_0

    .line 574
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->b:Lgxf;

    const-string v2, "The phone number supplied was null."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 575
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0xfa

    if-le v0, v1, :cond_1

    .line 576
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->e:Lgxf;

    const-string v2, "The string supplied was too long to parse."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 577
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 578
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 580
    const-string v0, ";phone-context="

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 581
    if-ltz v3, :cond_6

    .line 582
    add-int/lit8 v0, v3, 0xf

    .line 583
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_2

    .line 584
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2b

    if-ne v4, v5, :cond_2

    .line 585
    const/16 v4, 0x3b

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 586
    if-lez v4, :cond_4

    .line 587
    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    :cond_2
    :goto_0
    const-string v0, "tel:"

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 590
    if-ltz v0, :cond_5

    .line 591
    add-int/lit8 v0, v0, 0x4

    .line 592
    :goto_1
    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    :goto_2
    const-string v0, ";isub="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 608
    if-lez v0, :cond_3

    .line 609
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 610
    :cond_3
    invoke-static {v1}, Lgxg;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 611
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->b:Lgxf;

    const-string v2, "The string supplied did not seem to be a phone number."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 588
    :cond_4
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 591
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 595
    :cond_6
    sget-object v0, Lgxg;->l:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 596
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 597
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-interface {v2, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 598
    sget-object v3, Lgxg;->n:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 599
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 600
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    invoke-interface {v0, v4, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 601
    :cond_7
    sget-object v3, Lgxg;->m:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 602
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 603
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    invoke-interface {v0, v4, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 606
    :cond_8
    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 605
    :cond_9
    const-string v0, ""

    goto :goto_3

    .line 612
    :cond_a
    if-eqz p4, :cond_d

    .line 613
    invoke-direct {p0, p2}, Lgxg;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 614
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lgxg;->i:Ljava/util/regex/Pattern;

    .line 615
    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v0

    if-nez v0, :cond_c

    .line 616
    :cond_b
    const/4 v0, 0x0

    .line 618
    :goto_4
    if-nez v0, :cond_d

    .line 619
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->a:Lgxf;

    const-string v2, "Missing or invalid default region."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 617
    :cond_c
    const/4 v0, 0x1

    goto :goto_4

    .line 620
    :cond_d
    if-eqz p3, :cond_e

    .line 621
    invoke-virtual {p5, v2}, Lgxl;->b(Ljava/lang/String;)Lgxl;

    .line 622
    :cond_e
    invoke-static {v1}, Lgxg;->b(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 623
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_f

    .line 624
    invoke-virtual {p5, v0}, Lgxl;->a(Ljava/lang/String;)Lgxl;

    .line 625
    :cond_f
    invoke-virtual {p0, p2}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v2

    .line 626
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, p0

    move v4, p3

    move-object v5, p5

    .line 627
    :try_start_0
    invoke-direct/range {v0 .. v5}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;Ljava/lang/StringBuilder;ZLgxl;)I
    :try_end_0
    .catch Lgxe; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 641
    :cond_10
    if-eqz v0, :cond_13

    .line 642
    invoke-virtual {p0, v0}, Lgxg;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 643
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 644
    invoke-direct {p0, v0, v1}, Lgxg;->a(ILjava/lang/String;)Lgxi;

    move-result-object v2

    .line 656
    :cond_11
    :goto_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_15

    .line 657
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->d:Lgxf;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 629
    :catch_0
    move-exception v0

    .line 630
    sget-object v4, Lgxg;->i:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 632
    iget-object v5, v0, Lgxe;->a:Lgxf;

    .line 633
    sget-object v6, Lgxf;->a:Lgxf;

    if-ne v5, v6, :cond_12

    .line 634
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->lookingAt()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 635
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v5

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    move v8, p3

    move-object v9, p5

    invoke-direct/range {v4 .. v9}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;Ljava/lang/StringBuilder;ZLgxl;)I

    move-result v0

    .line 636
    if-nez v0, :cond_10

    .line 637
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->a:Lgxf;

    const-string v2, "Could not interpret numbers after plus-sign."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 638
    :cond_12
    new-instance v1, Lgxe;

    .line 639
    iget-object v2, v0, Lgxe;->a:Lgxf;

    .line 640
    invoke-virtual {v0}, Lgxe;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v1

    .line 646
    :cond_13
    invoke-static {v1}, Lgxg;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 647
    if-eqz p2, :cond_14

    .line 649
    iget v0, v2, Lgxi;->m:I

    .line 651
    invoke-virtual {p5, v0}, Lgxl;->a(I)Lgxl;

    goto :goto_5

    .line 652
    :cond_14
    if-eqz p3, :cond_11

    .line 654
    const/4 v0, 0x0

    iput-boolean v0, p5, Lgxl;->m:Z

    .line 655
    sget-object v0, Lgxm;->e:Lgxm;

    iput-object v0, p5, Lgxl;->n:Lgxm;

    goto :goto_5

    .line 658
    :cond_15
    if-eqz v2, :cond_17

    .line 659
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 661
    invoke-direct {p0, v0, v2, v1}, Lgxg;->a(Ljava/lang/StringBuilder;Lgxi;Ljava/lang/StringBuilder;)Z

    .line 662
    invoke-virtual {p0, v0, v2}, Lgxg;->a(Ljava/lang/CharSequence;Lgxi;)I

    move-result v2

    .line 663
    sget v4, Lmg$c;->ax:I

    if-eq v2, v4, :cond_17

    sget v4, Lmg$c;->av:I

    if-eq v2, v4, :cond_17

    sget v4, Lmg$c;->ay:I

    if-eq v2, v4, :cond_17

    .line 665
    if-eqz p3, :cond_16

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_16

    .line 666
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v1}, Lgxl;->c(Ljava/lang/String;)Lgxl;

    :cond_16
    move-object v3, v0

    .line 667
    :cond_17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 668
    const/4 v1, 0x2

    if-ge v0, v1, :cond_18

    .line 669
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->d:Lgxf;

    const-string v2, "The string supplied is too short to be a phone number."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 670
    :cond_18
    const/16 v1, 0x11

    if-le v0, v1, :cond_19

    .line 671
    new-instance v0, Lgxe;

    sget-object v1, Lgxf;->e:Lgxf;

    const-string v2, "The string supplied is too long to be a phone number."

    invoke-direct {v0, v1, v2}, Lgxe;-><init>(Lgxf;Ljava/lang/String;)V

    throw v0

    .line 672
    :cond_19
    invoke-static {v3, p5}, Lgxg;->a(Ljava/lang/CharSequence;Lgxl;)V

    .line 673
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p5, v0, v1}, Lgxl;->a(J)Lgxl;

    .line 674
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lgxg;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lgxl;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 343
    .line 344
    iget v1, p1, Lgxl;->b:I

    .line 346
    invoke-direct {p0, v1, p2}, Lgxg;->a(ILjava/lang/String;)Lgxi;

    move-result-object v2

    .line 347
    if-eqz v2, :cond_1

    const-string v3, "001"

    .line 348
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 350
    invoke-virtual {p0, p2}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v3

    .line 351
    if-nez v3, :cond_0

    .line 352
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid region code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    iget v3, v3, Lgxi;->m:I

    .line 355
    if-eq v1, v3, :cond_2

    .line 358
    :cond_1
    :goto_0
    return v0

    .line 357
    :cond_2
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-direct {p0, v1, v2}, Lgxg;->a(Ljava/lang/String;Lgxi;)I

    move-result v1

    sget v2, Lmg$c;->as:I

    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lgxl;)I
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p0, p1}, Lgxg;->d(Lgxl;)Ljava/lang/String;

    move-result-object v0

    .line 268
    iget v1, p1, Lgxl;->b:I

    .line 269
    invoke-direct {p0, v1, v0}, Lgxg;->a(ILjava/lang/String;)Lgxi;

    move-result-object v0

    .line 270
    if-nez v0, :cond_0

    .line 271
    sget v0, Lmg$c;->as:I

    .line 273
    :goto_0
    return v0

    .line 272
    :cond_0
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-direct {p0, v1, v0}, Lgxg;->a(Ljava/lang/String;Lgxi;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lgxg;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 385
    if-nez v0, :cond_0

    const-string v0, "ZZ"

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final c(Lgxl;)Z
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0, p1}, Lgxg;->d(Lgxl;)Ljava/lang/String;

    move-result-object v0

    .line 342
    invoke-virtual {p0, p1, v0}, Lgxg;->a(Lgxl;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d(Lgxl;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 359
    .line 360
    iget v1, p1, Lgxl;->b:I

    .line 362
    iget-object v0, p0, Lgxg;->c:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 363
    if-nez v0, :cond_0

    .line 364
    sget-object v0, Lgxg;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing/invalid country_code ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 365
    const/4 v0, 0x0

    .line 368
    :goto_0
    return-object v0

    .line 366
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 367
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 368
    :cond_1
    invoke-direct {p0, p1, v0}, Lgxg;->a(Lgxl;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Lgxl;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 776
    invoke-virtual {p0, p1}, Lgxg;->d(Lgxl;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgxg;->a(Ljava/lang/String;)Lgxi;

    move-result-object v1

    .line 777
    if-nez v1, :cond_1

    .line 782
    :cond_0
    :goto_0
    return v0

    .line 779
    :cond_1
    invoke-static {p1}, Lgxg;->a(Lgxl;)Ljava/lang/String;

    move-result-object v2

    .line 781
    iget-object v1, v1, Lgxi;->l:Lgxk;

    .line 782
    invoke-direct {p0, v2, v1}, Lgxg;->a(Ljava/lang/String;Lgxk;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
