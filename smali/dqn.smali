.class public final Ldqn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbsd;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ldqz;

.field private c:Ldqb;

.field private d:Ldqu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldqz;Ldqu;Ldqb;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Ldqn;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Ldqn;->b:Ldqz;

    .line 4
    iput-object p3, p0, Ldqn;->d:Ldqu;

    .line 5
    iput-object p4, p0, Ldqn;->c:Ldqb;

    .line 6
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 7
    packed-switch p0, :pswitch_data_0

    .line 15
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 8
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 9
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 10
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 11
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 12
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 13
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 14
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 7
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V
    .locals 4

    .prologue
    .line 101
    new-instance v0, Lgvg;

    invoke-direct {v0}, Lgvg;-><init>()V

    .line 102
    iput-object p1, v0, Lgvg;->d:Ljava/lang/String;

    .line 103
    invoke-static {p3}, Ldqn;->a(I)I

    move-result v1

    iput v1, v0, Lgvg;->f:I

    .line 104
    invoke-virtual {p4}, Lbkz$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->k:I

    .line 105
    invoke-virtual {p5}, Lbkm$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->m:I

    .line 106
    invoke-virtual {p6}, Lbko$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->n:I

    .line 107
    iget-object v1, p0, Ldqn;->c:Ldqb;

    .line 108
    iget-object v2, v1, Ldqb;->a:Lbdi;

    new-instance v3, Ldqg;

    invoke-direct {v3, v1, v0, p2}, Ldqg;-><init>(Ldqb;Lgvg;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v0}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 109
    invoke-static {}, Ldqr;->a()V

    .line 110
    return-void
.end method

.method private final b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V
    .locals 4

    .prologue
    .line 115
    new-instance v0, Lgvg;

    invoke-direct {v0}, Lgvg;-><init>()V

    .line 116
    iput-object p1, v0, Lgvg;->d:Ljava/lang/String;

    .line 117
    invoke-static {p3}, Ldqn;->a(I)I

    move-result v1

    iput v1, v0, Lgvg;->f:I

    .line 118
    invoke-virtual {p4}, Lbkz$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->k:I

    .line 119
    invoke-virtual {p5}, Lbkm$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->m:I

    .line 120
    invoke-virtual {p6}, Lbko$a;->getNumber()I

    move-result v1

    iput v1, v0, Lgvg;->n:I

    .line 121
    iget-object v1, p0, Ldqn;->c:Ldqb;

    .line 122
    iget-object v2, v1, Ldqb;->a:Lbdi;

    new-instance v3, Ldqh;

    invoke-direct {v3, v1, v0, p2}, Ldqh;-><init>(Ldqb;Lgvg;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v0}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 123
    invoke-static {}, Ldqr;->a()V

    .line 124
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V
    .locals 7

    .prologue
    .line 95
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const/4 v3, 0x1

    sget-object v6, Lbko$a;->a:Lbko$a;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Ldqn;->a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V

    .line 97
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbko$a;)V
    .locals 7

    .prologue
    .line 98
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v5, Lbkm$a;->a:Lbkm$a;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldqn;->a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V

    .line 100
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lbse;)V
    .locals 2

    .prologue
    .line 28
    if-nez p2, :cond_0

    iget-object v0, p0, Ldqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 29
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Lbse;->a(Z)V

    .line 38
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-static {p1}, Ldqr;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p3, v0}, Lbse;->a(Z)V

    goto :goto_0

    .line 36
    :cond_2
    new-instance v0, Ldqo;

    invoke-direct {v0, p1, p3}, Ldqo;-><init>(Ljava/lang/String;Lbse;)V

    .line 37
    iget-object v1, p0, Ldqn;->c:Ldqb;

    invoke-virtual {v1, v0, p1, p2}, Ldqb;->a(Ldqk;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Ldqn;->b:Ldqz;

    invoke-virtual {v0}, Ldqz;->a()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 86
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    .line 88
    :cond_0
    invoke-static {p1}, Ldqr;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 91
    :cond_1
    iget-object v0, p0, Ldqn;->d:Ldqu;

    invoke-virtual {v0, p1, p2}, Ldqu;->a(Ljava/lang/String;Ljava/lang/String;)Ldqv;

    move-result-object v0

    .line 92
    iget-object v1, v0, Ldqv;->b:Ldqw;

    invoke-static {p1, v1}, Ldqr;->a(Ljava/lang/String;Ldqw;)V

    .line 93
    iget-boolean v1, v0, Ldqv;->a:Z

    invoke-static {p1, v1}, Ldqr;->a(Ljava/lang/String;Z)V

    .line 94
    invoke-virtual {v0}, Ldqv;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V
    .locals 7

    .prologue
    .line 111
    const/4 v3, 0x1

    sget-object v6, Lbko$a;->a:Lbko$a;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Ldqn;->b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V

    .line 112
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbko$a;)V
    .locals 7

    .prologue
    .line 113
    sget-object v5, Lbkm$a;->a:Lbkm$a;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Ldqn;->b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;Lbko$a;)V

    .line 114
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lbse;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 39
    if-nez p2, :cond_0

    iget-object v0, p0, Ldqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 40
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-interface {p3, v1}, Lbse;->a(Z)V

    .line 54
    :goto_0
    return-void

    .line 44
    :cond_1
    sget-object v0, Ldqr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqw;

    .line 45
    if-nez v0, :cond_2

    .line 46
    const/4 v0, 0x0

    .line 49
    :goto_1
    if-eqz v0, :cond_4

    .line 50
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p3, v0}, Lbse;->a(Z)V

    goto :goto_0

    .line 47
    :cond_2
    sget-object v2, Ldqw;->c:Ldqw;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 52
    :cond_4
    new-instance v0, Ldqp;

    invoke-direct {v0, p1, p3}, Ldqp;-><init>(Ljava/lang/String;Lbse;)V

    .line 53
    iget-object v1, p0, Ldqn;->c:Ldqb;

    invoke-virtual {v1, v0, p1, p2}, Ldqb;->a(Ldql;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldqn;->b:Ldqz;

    .line 18
    invoke-virtual {v0}, Ldqz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldny;->f:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 19
    :goto_0
    return v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 24
    sget-object v0, Ldny;->g:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 25
    return v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Lbse;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 55
    if-nez p2, :cond_0

    iget-object v0, p0, Ldqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 56
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-interface {p3, v1}, Lbse;->a(Z)V

    .line 70
    :goto_0
    return-void

    .line 60
    :cond_1
    sget-object v0, Ldqr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqw;

    .line 61
    if-nez v0, :cond_2

    .line 62
    const/4 v0, 0x0

    .line 65
    :goto_1
    if-eqz v0, :cond_4

    .line 66
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p3, v0}, Lbse;->a(Z)V

    goto :goto_0

    .line 63
    :cond_2
    sget-object v2, Ldqw;->b:Ldqw;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 68
    :cond_4
    new-instance v0, Ldqq;

    invoke-direct {v0, p1, p3}, Ldqq;-><init>(Ljava/lang/String;Lbse;)V

    .line 69
    iget-object v1, p0, Ldqn;->c:Ldqb;

    invoke-virtual {v1, v0, p1, p2}, Ldqb;->a(Ldql;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 26
    sget-object v0, Ldny;->h:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 27
    return v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Lbse;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 71
    if-nez p2, :cond_0

    iget-object v0, p0, Ldqn;->a:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 72
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    invoke-interface {p3, v4}, Lbse;->a(Z)V

    .line 85
    :goto_0
    return-void

    .line 76
    :cond_1
    sget-object v0, Ldqr;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 78
    if-eqz v0, :cond_2

    .line 79
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p3, v0}, Lbse;->a(Z)V

    goto :goto_0

    .line 81
    :cond_2
    new-instance v0, Ldqj;

    invoke-direct {v0, p1, p3}, Ldqj;-><init>(Ljava/lang/String;Lbse;)V

    .line 82
    iget-object v1, p0, Ldqn;->c:Ldqb;

    .line 83
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 84
    iget-object v2, v1, Ldqb;->a:Lbdi;

    new-instance v3, Ldqe;

    invoke-direct {v3, v1, p1, p2, v0}, Ldqe;-><init>(Ldqb;Ljava/lang/String;Ljava/lang/String;Ldqj;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v0}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 20
    sget-object v0, Ldny;->i:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 21
    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 22
    sget-object v0, Ldny;->d:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 23
    return v0
.end method
