.class final Lfyx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lfyw;


# direct methods
.method constructor <init>(Lfyw;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfyx;->a:Lfyw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2
    iget-object v0, p0, Lfyx;->a:Lfyw;

    .line 3
    iget-object v0, v0, Lfyw;->d:Landroid/content/SharedPreferences;

    .line 4
    invoke-static {v0}, Lfyw;->a(Landroid/content/SharedPreferences;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5
    iget-object v0, p0, Lfyx;->a:Lfyw;

    .line 7
    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 8
    invoke-static {v2}, Lcom/google/android/libraries/performance/primes/metriccapture/PackageStatsCapture;->getPackageStats(Landroid/content/Context;)Landroid/content/pm/PackageStats;

    move-result-object v2

    .line 9
    if-eqz v2, :cond_3

    .line 10
    new-instance v3, Lhtd;

    invoke-direct {v3}, Lhtd;-><init>()V

    .line 12
    invoke-static {v2}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    new-instance v4, Lhsg;

    invoke-direct {v4}, Lhsg;-><init>()V

    .line 14
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->cacheSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->a:Ljava/lang/Long;

    .line 15
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->codeSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->b:Ljava/lang/Long;

    .line 16
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->dataSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->c:Ljava/lang/Long;

    .line 17
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->externalCacheSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->d:Ljava/lang/Long;

    .line 18
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->externalCodeSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->e:Ljava/lang/Long;

    .line 19
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->externalDataSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->f:Ljava/lang/Long;

    .line 20
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->externalMediaSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v4, Lhsg;->g:Ljava/lang/Long;

    .line 21
    iget-wide v6, v2, Landroid/content/pm/PackageStats;->externalObbSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v4, Lhsg;->h:Ljava/lang/Long;

    .line 23
    iput-object v4, v3, Lhtd;->i:Lhsg;

    .line 24
    iget-boolean v2, v0, Lfyw;->e:Z

    if-eqz v2, :cond_0

    .line 26
    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 27
    invoke-static {v2}, Lfmk;->g(Landroid/content/Context;)[Lhsh;

    move-result-object v2

    .line 28
    if-eqz v2, :cond_0

    .line 29
    iget-object v4, v3, Lhtd;->i:Lhsg;

    iput-object v2, v4, Lhsg;->i:[Lhsh;

    .line 30
    :cond_0
    const-string v2, "PackageMetricService"

    iget-object v4, v3, Lhtd;->i:Lhsg;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "pkgMetric: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    invoke-virtual {v0, v3}, Lfyw;->a(Lhtd;)V

    .line 32
    iget-object v0, v0, Lfyw;->d:Landroid/content/SharedPreferences;

    invoke-static {}, Lfmk;->i()J

    move-result-wide v2

    .line 33
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "primes.packageMetric.lastSendTime"

    invoke-interface {v0, v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 34
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 35
    :goto_0
    if-eqz v0, :cond_1

    .line 36
    const-string v0, "PackageMetricService"

    const-string v2, "Failure storing timestamp persistently"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 34
    goto :goto_0

    .line 38
    :cond_3
    const-string v0, "PackageMetricService"

    const-string v2, "PackageStats capture failed."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
