.class public final Lzd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:Landroid/graphics/PorterDuff$Mode;

.field private static d:Lzd;

.field private static e:Lzf;

.field private static f:[I

.field private static g:[I

.field private static h:[I

.field private static i:[I

.field private static j:[I

.field private static k:[I


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/util/WeakHashMap;

.field private l:Ljava/util/WeakHashMap;

.field private m:Lpd;

.field private n:Lpw;

.field private o:Landroid/util/TypedValue;

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 312
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 313
    new-instance v0, Lzf;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lzf;-><init>(I)V

    sput-object v0, Lzd;->e:Lzf;

    .line 314
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lzd;->f:[I

    .line 315
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lzd;->g:[I

    .line 316
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lzd;->h:[I

    .line 317
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lzd;->i:[I

    .line 318
    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lzd;->j:[I

    .line 319
    new-array v0, v2, [I

    fill-array-data v0, :array_5

    sput-object v0, Lzd;->k:[I

    return-void

    .line 314
    :array_0
    .array-data 4
        0x7f020050
        0x7f02004e
        0x7f020000
    .end array-data

    .line 315
    :array_1
    .array-data 4
        0x7f020016
        0x7f02003e
        0x7f02001d
        0x7f020018
        0x7f020019
        0x7f02001c
        0x7f02001b
    .end array-data

    .line 316
    :array_2
    .array-data 4
        0x7f02004d
        0x7f02004f
        0x7f02000f
        0x7f020046
        0x7f020047
        0x7f020049
        0x7f02004b
        0x7f020048
        0x7f02004a
        0x7f02004c
    .end array-data

    .line 317
    :array_3
    .array-data 4
        0x7f020034
        0x7f02000d
        0x7f020033
    .end array-data

    .line 318
    :array_4
    .array-data 4
        0x7f020044
        0x7f020051
    .end array-data

    .line 319
    :array_5
    .array-data 4
        0x7f020003
        0x7f020008
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lzd;->a:Ljava/lang/Object;

    .line 3
    new-instance v0, Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    iput-object v0, p0, Lzd;->b:Ljava/util/WeakHashMap;

    .line 4
    return-void
.end method

.method private static a(Landroid/util/TypedValue;)J
    .locals 4

    .prologue
    .line 54
    iget v0, p0, Landroid/util/TypedValue;->assetCookie:I

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/util/TypedValue;->data:I

    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .locals 3

    .prologue
    .line 299
    sget-object v0, Lzd;->e:Lzf;

    .line 300
    invoke-static {p0, p1}, Lzf;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    .line 302
    if-nez v0, :cond_0

    .line 303
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v1, p0, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 304
    sget-object v0, Lzd;->e:Lzf;

    .line 305
    invoke-static {p0, p1}, Lzf;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lzf;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    move-object v0, v1

    .line 307
    :cond_0
    return-object v0
.end method

.method private final a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 136
    iget-object v3, p0, Lzd;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 137
    :try_start_0
    iget-object v0, p0, Lzd;->b:Ljava/util/WeakHashMap;

    .line 138
    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 139
    if-nez v0, :cond_0

    .line 140
    monitor-exit v3

    move-object v0, v2

    .line 153
    :goto_0
    return-object v0

    .line 141
    :cond_0
    invoke-virtual {v0, p2, p3}, Lpj;->a(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 142
    if-eqz v1, :cond_2

    .line 143
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;

    .line 144
    if-eqz v1, :cond_1

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    monitor-exit v3

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 147
    :cond_1
    :try_start_1
    iget-object v1, v0, Lpj;->c:[J

    iget v4, v0, Lpj;->e:I

    invoke-static {v1, v4, p2, p3}, Lph;->a([JIJ)I

    move-result v1

    .line 148
    if-ltz v1, :cond_2

    .line 149
    iget-object v4, v0, Lpj;->d:[Ljava/lang/Object;

    aget-object v4, v4, v1

    sget-object v5, Lpj;->a:Ljava/lang/Object;

    if-eq v4, v5, :cond_2

    .line 150
    iget-object v4, v0, Lpj;->d:[Ljava/lang/Object;

    sget-object v5, Lpj;->a:Ljava/lang/Object;

    aput-object v5, v4, v1

    .line 151
    const/4 v1, 0x1

    iput-boolean v1, v0, Lpj;->b:Z

    .line 152
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 153
    goto :goto_0
.end method

.method public static a()Lzd;
    .locals 3

    .prologue
    .line 5
    sget-object v0, Lzd;->d:Lzd;

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lzd;

    invoke-direct {v0}, Lzd;-><init>()V

    .line 7
    sput-object v0, Lzd;->d:Lzd;

    .line 8
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-ge v1, v2, :cond_0

    .line 9
    const-string v1, "vector"

    new-instance v2, Lzh;

    invoke-direct {v2}, Lzh;-><init>()V

    invoke-direct {v0, v1, v2}, Lzd;->a(Ljava/lang/String;Lzg;)V

    .line 10
    const-string v1, "animated-vector"

    new-instance v2, Lze;

    invoke-direct {v2}, Lze;-><init>()V

    invoke-direct {v0, v1, v2}, Lzd;->a(Ljava/lang/String;Lzg;)V

    .line 11
    :cond_0
    sget-object v0, Lzd;->d:Lzd;

    return-object v0
.end method

.method private static a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .prologue
    .line 308
    invoke-static {p0}, Laba;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 310
    :cond_0
    if-nez p2, :cond_1

    sget-object p2, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    :cond_1
    invoke-static {p1, p2}, Lzd;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 311
    return-void
.end method

.method static a(Landroid/graphics/drawable/Drawable;Ladl;[I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 285
    invoke-static {p0}, Laba;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-boolean v0, p1, Ladl;->d:Z

    if-nez v0, :cond_2

    iget-boolean v0, p1, Ladl;->c:Z

    if-eqz v0, :cond_7

    .line 289
    :cond_2
    iget-boolean v0, p1, Ladl;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p1, Ladl;->a:Landroid/content/res/ColorStateList;

    :goto_1
    iget-boolean v2, p1, Ladl;->c:Z

    if-eqz v2, :cond_5

    iget-object v2, p1, Ladl;->b:Landroid/graphics/PorterDuff$Mode;

    .line 290
    :goto_2
    if-eqz v0, :cond_3

    if-nez v2, :cond_6

    .line 294
    :cond_3
    :goto_3
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 296
    :goto_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-gt v0, v1, :cond_0

    .line 297
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 289
    goto :goto_1

    :cond_5
    sget-object v2, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    goto :goto_2

    .line 292
    :cond_6
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    .line 293
    invoke-static {v0, v2}, Lzd;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    goto :goto_3

    .line 295
    :cond_7
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_4
.end method

.method private final a(Ljava/lang/String;Lzg;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lzd;->m:Lpd;

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lzd;->m:Lpd;

    .line 197
    :cond_0
    iget-object v0, p0, Lzd;->m:Lpd;

    invoke-virtual {v0, p1, p2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    return-void
.end method

.method static a(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z
    .locals 7

    .prologue
    const v2, 0x1010031

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 165
    sget-object v5, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 169
    sget-object v4, Lzd;->f:[I

    invoke-static {v4, p1}, Lzd;->a([II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    const v2, 0x7f010091

    move v4, v2

    move-object v6, v5

    move v5, v0

    move v2, v3

    .line 186
    :goto_0
    if-eqz v5, :cond_6

    .line 187
    invoke-static {p2}, Laba;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 189
    :cond_0
    invoke-static {p0, v4}, Ladj;->a(Landroid/content/Context;I)I

    move-result v1

    .line 190
    invoke-static {v1, v6}, Lzd;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 191
    if-eq v2, v3, :cond_1

    .line 192
    invoke-virtual {p2, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 194
    :cond_1
    :goto_1
    return v0

    .line 172
    :cond_2
    sget-object v4, Lzd;->h:[I

    invoke-static {v4, p1}, Lzd;->a([II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 173
    const v2, 0x7f010092

    move v4, v2

    move-object v6, v5

    move v5, v0

    move v2, v3

    .line 174
    goto :goto_0

    .line 175
    :cond_3
    sget-object v4, Lzd;->i:[I

    invoke-static {v4, p1}, Lzd;->a([II)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 178
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move v5, v0

    move-object v6, v4

    move v4, v2

    move v2, v3

    goto :goto_0

    .line 179
    :cond_4
    const v4, 0x7f020028

    if-ne p1, v4, :cond_5

    .line 180
    const v4, 0x1010030

    .line 182
    const v2, 0x42233333    # 40.8f

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object v6, v5

    move v5, v0

    goto :goto_0

    .line 183
    :cond_5
    const v4, 0x7f020011

    if-ne p1, v4, :cond_7

    move v4, v2

    move-object v6, v5

    move v5, v0

    move v2, v3

    .line 185
    goto :goto_0

    :cond_6
    move v0, v1

    .line 194
    goto :goto_1

    :cond_7
    move v2, v3

    move v4, v1

    move-object v6, v5

    move v5, v1

    goto :goto_0
.end method

.method private final a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
    .locals 4

    .prologue
    .line 154
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_1

    .line 156
    iget-object v2, p0, Lzd;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 157
    :try_start_0
    iget-object v0, p0, Lzd;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpj;

    .line 158
    if-nez v0, :cond_0

    .line 159
    new-instance v0, Lpj;

    invoke-direct {v0}, Lpj;-><init>()V

    .line 160
    iget-object v3, p0, Lzd;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :cond_0
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p2, p3, v3}, Lpj;->a(JLjava/lang/Object;)V

    .line 162
    monitor-exit v2

    .line 163
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 199
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 200
    if-ne v3, p1, :cond_1

    .line 201
    const/4 v0, 0x1

    .line 203
    :cond_0
    return v0

    .line 202
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 9

    .prologue
    const/4 v1, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 272
    new-array v0, v1, [[I

    .line 273
    new-array v1, v1, [I

    .line 274
    const v2, 0x7f010093

    invoke-static {p0, v2}, Ladj;->a(Landroid/content/Context;I)I

    move-result v2

    .line 275
    const v3, 0x7f010094

    invoke-static {p0, v3}, Ladj;->c(Landroid/content/Context;I)I

    move-result v3

    .line 276
    sget-object v4, Ladj;->a:[I

    aput-object v4, v0, v5

    .line 277
    aput v3, v1, v5

    .line 278
    sget-object v3, Ladj;->c:[I

    aput-object v3, v0, v6

    .line 279
    invoke-static {v2, p1}, Lmt;->a(II)I

    move-result v3

    aput v3, v1, v6

    .line 280
    sget-object v3, Ladj;->b:[I

    aput-object v3, v0, v7

    .line 281
    invoke-static {v2, p1}, Lmt;->a(II)I

    move-result v2

    aput v2, v1, v7

    .line 282
    sget-object v2, Ladj;->e:[I

    aput-object v2, v0, v8

    .line 283
    aput p1, v1, v8

    .line 284
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method


# virtual methods
.method final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 96
    iget-object v0, p0, Lzd;->m:Lpd;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lzd;->m:Lpd;

    invoke-virtual {v0}, Lpd;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 97
    iget-object v0, p0, Lzd;->n:Lpw;

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lzd;->n:Lpw;

    invoke-virtual {v0, p2}, Lpw;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    const-string v2, "appcompat_skip_skip"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lzd;->m:Lpd;

    .line 100
    invoke-virtual {v2, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_0
    move-object v0, v1

    .line 135
    :cond_1
    :goto_0
    return-object v0

    .line 103
    :cond_2
    new-instance v0, Lpw;

    invoke-direct {v0}, Lpw;-><init>()V

    iput-object v0, p0, Lzd;->n:Lpw;

    .line 104
    :cond_3
    iget-object v0, p0, Lzd;->o:Landroid/util/TypedValue;

    if-nez v0, :cond_4

    .line 105
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Lzd;->o:Landroid/util/TypedValue;

    .line 106
    :cond_4
    iget-object v2, p0, Lzd;->o:Landroid/util/TypedValue;

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p2, v2, v7}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 109
    invoke-static {v2}, Lzd;->a(Landroid/util/TypedValue;)J

    move-result-wide v4

    .line 110
    invoke-direct {p0, p1, v4, v5}, Lzd;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 111
    if-eqz v1, :cond_5

    move-object v0, v1

    .line 112
    goto :goto_0

    .line 113
    :cond_5
    iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    if-eqz v3, :cond_8

    iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, ".xml"

    invoke-virtual {v3, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 114
    :try_start_0
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    .line 115
    invoke-static {v3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v6

    .line 116
    :cond_6
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v8, :cond_7

    if-ne v0, v7, :cond_6

    .line 117
    :cond_7
    if-eq v0, v8, :cond_9

    .line 118
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "No start tag found"

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    const-string v2, "AppCompatDrawableManag"

    const-string v3, "Exception while inflating drawable"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_8
    move-object v0, v1

    .line 132
    :goto_1
    if-nez v0, :cond_1

    .line 133
    iget-object v1, p0, Lzd;->n:Lpw;

    const-string v2, "appcompat_skip_skip"

    invoke-virtual {v1, p2, v2}, Lpw;->b(ILjava/lang/Object;)V

    goto :goto_0

    .line 119
    :cond_9
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 120
    iget-object v7, p0, Lzd;->n:Lpw;

    invoke-virtual {v7, p2, v0}, Lpw;->b(ILjava/lang/Object;)V

    .line 121
    iget-object v7, p0, Lzd;->m:Lpd;

    invoke-virtual {v7, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzg;

    .line 122
    if-eqz v0, :cond_a

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    .line 125
    invoke-interface {v0, p1, v3, v6, v7}, Lzg;->a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 126
    :cond_a
    if-eqz v1, :cond_b

    .line 127
    iget v0, v2, Landroid/util/TypedValue;->changingConfigurations:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 128
    invoke-direct {p0, p1, v4, v5, v1}, Lzd;->a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_b
    move-object v0, v1

    .line 129
    goto :goto_1

    :cond_c
    move-object v0, v1

    .line 135
    goto/16 :goto_0
.end method

.method public final a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 12
    .line 13
    iget-boolean v0, p0, Lzd;->p:Z

    if-nez v0, :cond_3

    .line 14
    iput-boolean v2, p0, Lzd;->p:Z

    .line 15
    const v0, 0x7f020052

    .line 16
    invoke-virtual {p0, p1, v0, v1}, Lzd;->a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    instance-of v3, v0, Leu;

    if-nez v3, :cond_0

    const-string v3, "android.graphics.drawable.VectorDrawable"

    .line 20
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 21
    :goto_0
    if-nez v0, :cond_3

    .line 22
    :cond_1
    iput-boolean v1, p0, Lzd;->p:Z

    .line 23
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    .line 20
    goto :goto_0

    .line 24
    :cond_3
    invoke-virtual {p0, p1, p2}, Lzd;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 25
    if-nez v0, :cond_6

    .line 27
    iget-object v0, p0, Lzd;->o:Landroid/util/TypedValue;

    if-nez v0, :cond_4

    .line 28
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Lzd;->o:Landroid/util/TypedValue;

    .line 29
    :cond_4
    iget-object v3, p0, Lzd;->o:Landroid/util/TypedValue;

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2, v3, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 31
    invoke-static {v3}, Lzd;->a(Landroid/util/TypedValue;)J

    move-result-wide v4

    .line 32
    invoke-direct {p0, p1, v4, v5}, Lzd;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 33
    if-nez v0, :cond_6

    .line 34
    const v6, 0x7f02000e

    if-ne p2, v6, :cond_5

    .line 35
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    const v7, 0x7f02000d

    .line 37
    invoke-virtual {p0, p1, v7, v1}, Lzd;->a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 38
    aput-object v7, v6, v1

    const v7, 0x7f02000f

    .line 40
    invoke-virtual {p0, p1, v7, v1}, Lzd;->a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 41
    aput-object v1, v6, v2

    invoke-direct {v0, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 42
    :cond_5
    if-eqz v0, :cond_6

    .line 43
    iget v1, v3, Landroid/util/TypedValue;->changingConfigurations:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    .line 44
    invoke-direct {p0, p1, v4, v5, v0}, Lzd;->a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z

    .line 47
    :cond_6
    if-nez v0, :cond_7

    .line 48
    invoke-static {p1, p2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 49
    :cond_7
    if-eqz v0, :cond_8

    .line 50
    invoke-virtual {p0, p1, p2, p3, v0}, Lzd;->a(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 51
    :cond_8
    if-eqz v0, :cond_9

    .line 52
    invoke-static {v0}, Laba;->a(Landroid/graphics/drawable/Drawable;)V

    .line 53
    :cond_9
    return-object v0
.end method

.method final a(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const v7, 0x102000f

    const v6, 0x102000d

    const/high16 v1, 0x1020000

    const v5, 0x7f010092

    const v4, 0x7f010091

    .line 55
    invoke-virtual {p0, p1, p2}, Lzd;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_3

    .line 57
    invoke-static {p4}, Laba;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    .line 59
    :cond_0
    invoke-static {p4}, Lbw;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p4

    .line 60
    invoke-static {p4, v0}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 62
    const/4 v0, 0x0

    .line 63
    const v1, 0x7f020042

    if-ne p2, v1, :cond_1

    .line 64
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    .line 67
    :cond_1
    if-eqz v0, :cond_2

    .line 68
    invoke-static {p4, v0}, Lbw;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    .line 95
    :cond_2
    :goto_0
    return-object p4

    .line 69
    :cond_3
    const v0, 0x7f02003f

    if-ne p2, v0, :cond_4

    move-object v0, p4

    .line 70
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 71
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 72
    invoke-static {p1, v4}, Ladj;->a(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 73
    invoke-static {v1, v2, v3}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 74
    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 75
    invoke-static {p1, v4}, Ladj;->a(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 76
    invoke-static {v1, v2, v3}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 77
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 78
    invoke-static {p1, v5}, Ladj;->a(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 79
    invoke-static {v0, v1, v2}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 80
    :cond_4
    const v0, 0x7f020036

    if-eq p2, v0, :cond_5

    const v0, 0x7f020035

    if-eq p2, v0, :cond_5

    const v0, 0x7f020037

    if-ne p2, v0, :cond_6

    :cond_5
    move-object v0, p4

    .line 81
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 82
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 83
    invoke-static {p1, v4}, Ladj;->c(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 84
    invoke-static {v1, v2, v3}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 85
    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 86
    invoke-static {p1, v5}, Ladj;->a(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 87
    invoke-static {v1, v2, v3}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    .line 88
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 89
    invoke-static {p1, v5}, Ladj;->a(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Lzd;->c:Landroid/graphics/PorterDuff$Mode;

    .line 90
    invoke-static {v0, v1, v2}, Lzd;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 92
    :cond_6
    invoke-static {p1, p2, p4}, Lzd;->a(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 93
    if-nez v0, :cond_2

    if-eqz p3, :cond_2

    .line 94
    const/4 p4, 0x0

    goto :goto_0
.end method

.method final b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 8

    .prologue
    const/4 v2, 0x3

    const v7, 0x7f010095

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 204
    .line 205
    iget-object v0, p0, Lzd;->l:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_5

    .line 206
    iget-object v0, p0, Lzd;->l:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpw;

    .line 207
    if-eqz v0, :cond_4

    invoke-virtual {v0, p2}, Lpw;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/ColorStateList;

    .line 210
    :goto_0
    if-nez v0, :cond_3

    .line 211
    const v1, 0x7f020012

    if-ne p2, v1, :cond_6

    .line 212
    const v0, 0x7f0c00ff

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 262
    :goto_1
    if-eqz v1, :cond_2

    .line 264
    iget-object v0, p0, Lzd;->l:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    .line 265
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lzd;->l:Ljava/util/WeakHashMap;

    .line 266
    :cond_0
    iget-object v0, p0, Lzd;->l:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpw;

    .line 267
    if-nez v0, :cond_1

    .line 268
    new-instance v0, Lpw;

    invoke-direct {v0}, Lpw;-><init>()V

    .line 269
    iget-object v2, p0, Lzd;->l:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    :cond_1
    invoke-virtual {v0, p2, v1}, Lpw;->b(ILjava/lang/Object;)V

    :cond_2
    move-object v0, v1

    .line 271
    :cond_3
    return-object v0

    .line 207
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_6
    const v1, 0x7f020043

    if-ne p2, v1, :cond_7

    .line 214
    const v0, 0x7f0c0102

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto :goto_1

    .line 215
    :cond_7
    const v1, 0x7f020042

    if-ne p2, v1, :cond_9

    .line 217
    new-array v0, v2, [[I

    .line 218
    new-array v2, v2, [I

    .line 219
    invoke-static {p1, v7}, Ladj;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 220
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 221
    sget-object v3, Ladj;->a:[I

    aput-object v3, v0, v4

    .line 222
    aget-object v3, v0, v4

    invoke-virtual {v1, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    aput v3, v2, v4

    .line 223
    sget-object v3, Ladj;->d:[I

    aput-object v3, v0, v5

    .line 224
    const v3, 0x7f010092

    invoke-static {p1, v3}, Ladj;->a(Landroid/content/Context;I)I

    move-result v3

    aput v3, v2, v5

    .line 225
    sget-object v3, Ladj;->e:[I

    aput-object v3, v0, v6

    .line 226
    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    aput v1, v2, v6

    .line 234
    :goto_2
    new-instance v1, Landroid/content/res/ColorStateList;

    invoke-direct {v1, v0, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto :goto_1

    .line 228
    :cond_8
    sget-object v1, Ladj;->a:[I

    aput-object v1, v0, v4

    .line 229
    invoke-static {p1, v7}, Ladj;->c(Landroid/content/Context;I)I

    move-result v1

    aput v1, v2, v4

    .line 230
    sget-object v1, Ladj;->d:[I

    aput-object v1, v0, v5

    .line 231
    const v1, 0x7f010092

    invoke-static {p1, v1}, Ladj;->a(Landroid/content/Context;I)I

    move-result v1

    aput v1, v2, v5

    .line 232
    sget-object v1, Ladj;->e:[I

    aput-object v1, v0, v6

    .line 233
    invoke-static {p1, v7}, Ladj;->a(Landroid/content/Context;I)I

    move-result v1

    aput v1, v2, v6

    goto :goto_2

    .line 236
    :cond_9
    const v1, 0x7f020007

    if-ne p2, v1, :cond_a

    .line 238
    const v0, 0x7f010094

    .line 239
    invoke-static {p1, v0}, Ladj;->a(Landroid/content/Context;I)I

    move-result v0

    .line 240
    invoke-static {p1, v0}, Lzd;->c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 242
    :cond_a
    const v1, 0x7f020002

    if-ne p2, v1, :cond_b

    .line 244
    invoke-static {p1, v4}, Lzd;->c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 246
    :cond_b
    const v1, 0x7f020006

    if-ne p2, v1, :cond_c

    .line 248
    const v0, 0x7f010090

    .line 249
    invoke-static {p1, v0}, Ladj;->a(Landroid/content/Context;I)I

    move-result v0

    .line 250
    invoke-static {p1, v0}, Lzd;->c(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 252
    :cond_c
    const v1, 0x7f020040

    if-eq p2, v1, :cond_d

    const v1, 0x7f020041

    if-ne p2, v1, :cond_e

    .line 253
    :cond_d
    const v0, 0x7f0c0101

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 254
    :cond_e
    sget-object v1, Lzd;->g:[I

    invoke-static {v1, p2}, Lzd;->a([II)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 255
    const v0, 0x7f010091

    invoke-static {p1, v0}, Ladj;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 256
    :cond_f
    sget-object v1, Lzd;->j:[I

    invoke-static {v1, p2}, Lzd;->a([II)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 257
    const v0, 0x7f0c00fe

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 258
    :cond_10
    sget-object v1, Lzd;->k:[I

    invoke-static {v1, p2}, Lzd;->a([II)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 259
    const v0, 0x7f0c00fd

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    .line 260
    :cond_11
    const v1, 0x7f02003d

    if-ne p2, v1, :cond_12

    .line 261
    const v0, 0x7f0c0100

    invoke-static {p1, v0}, Lvy;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    goto/16 :goto_1

    :cond_12
    move-object v1, v0

    goto/16 :goto_1
.end method
