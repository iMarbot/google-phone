.class public Lbvw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(F)F
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 3
    const/4 v0, 0x0

    .line 4
    float-to-double v2, p0

    const-wide v4, 0x3fa999999999999aL    # 0.05

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    .line 6
    :cond_0
    float-to-double v2, p0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    .line 7
    add-float/2addr v0, v1

    .line 8
    :cond_1
    float-to-double v2, p0

    const-wide v4, 0x3fc999999999999aL    # 0.2

    cmpl-double v2, v2, v4

    if-lez v2, :cond_2

    .line 9
    add-float/2addr v0, v1

    .line 10
    :cond_2
    float-to-double v2, p0

    const-wide v4, 0x3fd999999999999aL    # 0.4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_3

    .line 11
    add-float/2addr v0, v1

    .line 12
    :cond_3
    float-to-double v2, p0

    const-wide v4, 0x3fe999999999999aL    # 0.8

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    .line 13
    add-float/2addr v0, v1

    .line 14
    :cond_4
    float-to-double v2, p0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    cmpl-double v2, v2, v4

    if-lez v2, :cond_5

    .line 15
    add-float/2addr v0, v1

    .line 16
    :cond_5
    return v0
.end method

.method public static a(Landroid/content/Context;Lcgp;Lcgq;)Lcet;
    .locals 7

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 17
    iget-object v0, p1, Lcgp;->f:Landroid/graphics/drawable/Drawable;

    .line 19
    iget-boolean v3, p1, Lcgp;->j:Z

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    .line 20
    const v0, 0x7f02015a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 21
    :cond_0
    iget v3, p1, Lcgp;->a:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    iget v3, p1, Lcgp;->a:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 22
    :cond_1
    iget-object v3, p1, Lcgp;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 23
    iget-object v1, p1, Lcgp;->h:Ljava/lang/String;

    move v6, v2

    move-object v2, v1

    move v1, v6

    .line 43
    :goto_0
    new-instance v3, Lcet;

    invoke-direct {v3, v2, v0, v1}, Lcet;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V

    return-object v3

    .line 25
    :cond_2
    invoke-static {p0, p1}, Lbvw;->a(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 26
    invoke-static {p2, v1}, Lbvw;->a(Lcgq;Z)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 27
    new-array v4, v5, [Ljava/lang/CharSequence;

    aput-object v3, v4, v2

    const-string v2, " "

    aput-object v2, v4, v1

    const/4 v2, 0x2

    iget-object v3, p2, Lcgq;->a:Ljava/lang/String;

    invoke-static {v3}, Lbvw;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 28
    :cond_3
    iget v3, p1, Lcgp;->c:I

    invoke-static {v3}, Lbvs;->e(I)Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p1, Lcgp;->c:I

    .line 29
    invoke-static {v3}, Lbvs;->f(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 30
    :cond_4
    invoke-static {p0, p1}, Lbvw;->c(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 31
    :cond_5
    iget v3, p1, Lcgp;->a:I

    const/16 v4, 0xf

    if-ne v3, v4, :cond_6

    .line 32
    const v2, 0x7f1101c6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 33
    :cond_6
    iget v3, p1, Lcgp;->a:I

    const/4 v4, 0x6

    if-eq v3, v4, :cond_7

    iget v3, p1, Lcgp;->a:I

    const/16 v4, 0xd

    if-ne v3, v4, :cond_8

    .line 34
    :cond_7
    invoke-static {p0, p1}, Lbvw;->b(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 35
    :cond_8
    iget v3, p1, Lcgp;->a:I

    if-ne v3, v5, :cond_9

    iget-boolean v3, p1, Lcgp;->r:Z

    if-eqz v3, :cond_9

    .line 36
    const v2, 0x7f1101c3

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 37
    :cond_9
    iget v3, p1, Lcgp;->a:I

    if-ne v3, v5, :cond_a

    .line 38
    invoke-static {p2, v2}, Lbvw;->a(Lcgq;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 39
    iget-object v2, p2, Lcgq;->a:Ljava/lang/String;

    invoke-static {v2}, Lbvw;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    .line 40
    :cond_a
    iget v2, p1, Lcgp;->a:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    iget-object v2, p1, Lcgp;->w:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 41
    iget-object v2, p1, Lcgp;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 42
    :cond_b
    invoke-static {p1}, Lbvw;->a(Lcgp;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_0

    :cond_c
    move-object v2, v3

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;IZ)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const v2, 0x7f1100f2

    const v1, 0x7f1100f0

    .line 64
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 65
    if-eqz p2, :cond_0

    .line 66
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    :goto_0
    return-object v0

    .line 67
    :cond_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_1
    if-eqz p2, :cond_2

    .line 69
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 55
    iget-boolean v0, p1, Lcgp;->b:Z

    if-eqz v0, :cond_0

    .line 56
    iget v0, p1, Lcgp;->c:I

    iget-boolean v1, p1, Lcgp;->j:Z

    invoke-static {p0, v0, v1}, Lbvw;->a(Landroid/content/Context;IZ)Ljava/lang/CharSequence;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 57
    :cond_0
    iget-boolean v0, p1, Lcgp;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcgp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    iget-object v0, p1, Lcgp;->e:Ljava/lang/String;

    goto :goto_0

    .line 59
    :cond_1
    invoke-static {p1}, Lbvw;->b(Lcgp;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    const v0, 0x7f1100ef

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcgp;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_2
    iget-boolean v0, p1, Lcgp;->l:Z

    if-eqz v0, :cond_3

    .line 62
    const v0, 0x7f1100f3

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_3
    const v0, 0x7f1100f1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcgp;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcgp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    invoke-static {p0}, Lbvw;->b(Lcgp;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcgp;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcgp;->k:Z

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    iget-object v0, p0, Lcgp;->e:Ljava/lang/String;

    .line 88
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    sget-object v1, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, p0, v1}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->createTtsSpannable(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    if-nez p0, :cond_0

    .line 99
    const-string v0, "AUDIO"

    .line 128
    :goto_0
    return-object v0

    .line 100
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 101
    const-string v0, "MUTE"

    goto :goto_0

    .line 102
    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    .line 103
    const-string v0, "DIALPAD"

    goto :goto_0

    .line 104
    :cond_2
    const/4 v0, 0x3

    if-ne p0, v0, :cond_3

    .line 105
    const-string v0, "HOLD"

    goto :goto_0

    .line 106
    :cond_3
    const/4 v0, 0x4

    if-ne p0, v0, :cond_4

    .line 107
    const-string v0, "SWAP"

    goto :goto_0

    .line 108
    :cond_4
    const/4 v0, 0x5

    if-ne p0, v0, :cond_5

    .line 109
    const-string v0, "UPGRADE_TO_VIDEO"

    goto :goto_0

    .line 110
    :cond_5
    const/4 v0, 0x7

    if-ne p0, v0, :cond_6

    .line 111
    const-string v0, "DOWNGRADE_TO_AUDIO"

    goto :goto_0

    .line 112
    :cond_6
    const/4 v0, 0x6

    if-ne p0, v0, :cond_7

    .line 113
    const-string v0, "SWITCH_CAMERA"

    goto :goto_0

    .line 114
    :cond_7
    const/16 v0, 0x8

    if-ne p0, v0, :cond_8

    .line 115
    const-string v0, "ADD_CALL"

    goto :goto_0

    .line 116
    :cond_8
    const/16 v0, 0x9

    if-ne p0, v0, :cond_9

    .line 117
    const-string v0, "MERGE"

    goto :goto_0

    .line 118
    :cond_9
    const/16 v0, 0xa

    if-ne p0, v0, :cond_a

    .line 119
    const-string v0, "PAUSE_VIDEO"

    goto :goto_0

    .line 120
    :cond_a
    const/16 v0, 0xb

    if-ne p0, v0, :cond_b

    .line 121
    const-string v0, "MANAGE_VIDEO_CONFERENCE"

    goto :goto_0

    .line 122
    :cond_b
    const/16 v0, 0xc

    if-ne p0, v0, :cond_c

    .line 123
    const-string v0, "MANAGE_VOICE_CONFERENCE"

    goto :goto_0

    .line 124
    :cond_c
    const/16 v0, 0xd

    if-ne p0, v0, :cond_d

    .line 125
    const-string v0, "SWITCH_TO_SECONDARY"

    goto :goto_0

    .line 126
    :cond_d
    const/16 v0, 0xe

    if-ne p0, v0, :cond_e

    .line 127
    const-string v0, "SWAP_SIM"

    goto :goto_0

    .line 128
    :cond_e
    const/16 v0, 0x1b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "INVALID_BUTTON: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/TextureView;FFF)V
    .locals 11

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v9, 0x40000000    # 2.0f

    .line 129
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    int-to-float v2, v0

    .line 130
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    int-to-float v3, v0

    .line 131
    div-float v4, v2, v3

    .line 132
    div-float v0, p1, p2

    .line 135
    cmpl-float v0, v4, v0

    if-lez v0, :cond_3

    .line 136
    div-float v0, v2, p1

    .line 137
    mul-float/2addr v0, p2

    .line 138
    div-float/2addr v0, v3

    .line 143
    :goto_0
    const/high16 v5, 0x42b40000    # 90.0f

    cmpl-float v5, p3, v5

    if-eqz v5, :cond_0

    const/high16 v5, 0x43870000    # 270.0f

    cmpl-float v5, p3, v5

    if-nez v5, :cond_1

    .line 146
    :cond_0
    div-float v5, v3, v2

    mul-float/2addr v0, v5

    .line 147
    mul-float/2addr v4, v1

    .line 148
    neg-float v1, v0

    .line 149
    neg-float v0, v4

    .line 150
    :cond_1
    const-string v4, "VideoScale.scaleVideoAndFillView"

    const-string v5, "view: %f x %f, video: %f x %f scale: %f x %f, rotation: %f"

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 151
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 152
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    .line 153
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    .line 154
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    .line 155
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    .line 156
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    .line 157
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    .line 158
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 160
    div-float v5, v2, v9

    div-float v6, v3, v9

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 161
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-eqz v0, :cond_2

    .line 162
    div-float v0, v2, v9

    div-float v1, v3, v9

    invoke-virtual {v4, p3, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 163
    :cond_2
    invoke-virtual {p0, v4}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 164
    return-void

    .line 140
    :cond_3
    div-float v0, v3, p2

    .line 141
    mul-float/2addr v0, p1

    .line 142
    div-float/2addr v0, v2

    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_0
.end method

.method public static a(Landroid/view/TextureView;II)V
    .locals 11

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 165
    invoke-virtual {p0}, Landroid/view/TextureView;->getWidth()I

    move-result v2

    .line 166
    invoke-virtual {p0}, Landroid/view/TextureView;->getHeight()I

    move-result v3

    .line 169
    if-le v2, v3, :cond_1

    .line 170
    mul-int v1, v3, p1

    mul-int v4, v2, p2

    if-le v1, v4, :cond_0

    .line 171
    mul-int v1, v2, p2

    div-int/2addr v1, p1

    .line 172
    int-to-float v1, v1

    int-to-float v4, v3

    div-float/2addr v1, v4

    .line 183
    :goto_0
    const-string v4, "VideoScale.scaleVideoMaintainingAspectRatio"

    const-string v5, "view: %d x %d, video: %d x %d scale: %f x %f"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 184
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    .line 186
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    .line 187
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    .line 188
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    .line 189
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    .line 190
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 192
    int-to-float v2, v2

    div-float/2addr v2, v9

    int-to-float v3, v3

    div-float/2addr v3, v9

    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 193
    invoke-virtual {p0, v4}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 194
    return-void

    .line 173
    :cond_0
    mul-int v1, v3, p1

    mul-int v4, v2, p2

    if-ge v1, v4, :cond_3

    .line 174
    mul-int v1, v3, p1

    div-int/2addr v1, p2

    .line 175
    int-to-float v1, v1

    int-to-float v4, v2

    div-float/2addr v1, v4

    .line 176
    goto :goto_0

    .line 177
    :cond_1
    mul-int v1, v3, p1

    mul-int v4, v2, p2

    if-le v1, v4, :cond_2

    .line 178
    mul-int v1, v2, p2

    div-int/2addr v1, p1

    .line 179
    int-to-float v1, v1

    int-to-float v4, v3

    div-float/2addr v1, v4

    move v10, v1

    move v1, v0

    move v0, v10

    .line 180
    goto :goto_0

    :cond_2
    mul-int v1, v3, p1

    mul-int v4, v2, p2

    if-ge v1, v4, :cond_3

    .line 181
    mul-int v1, v3, p1

    div-int/2addr v1, p2

    .line 182
    int-to-float v1, v1

    int-to-float v4, v2

    div-float/2addr v1, v4

    move v10, v1

    move v1, v0

    move v0, v10

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public static a(Lcgq;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-boolean v1, p0, Lcgq;->c:Z

    if-eqz v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    iget-object v1, p0, Lcgq;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    if-nez p1, :cond_0

    .line 50
    :cond_2
    iget-boolean v1, p0, Lcgq;->i:Z

    if-eqz v1, :cond_3

    if-eqz p1, :cond_0

    .line 52
    :cond_3
    iget-object v1, p0, Lcgq;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 71
    iget-object v0, p1, Lcgp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcgp;->j:Z

    if-nez v0, :cond_0

    .line 72
    const v0, 0x7f1101a0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p1, Lcgp;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    .line 73
    :cond_0
    iget-boolean v0, p1, Lcgp;->b:Z

    if-eqz v0, :cond_2

    .line 74
    iget-boolean v0, p1, Lcgp;->j:Z

    if-eqz v0, :cond_1

    .line 75
    const v0, 0x7f1101cc

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_1
    const v0, 0x7f1101ca

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_2
    iget-boolean v0, p1, Lcgp;->v:Z

    if-eqz v0, :cond_3

    .line 78
    const-string v0, "TopRow.getLabelForDialing"

    const-string v1, "using assisted dialing label."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    iget-object v0, p1, Lcgp;->x:Lavk;

    .line 80
    invoke-virtual {v0}, Lavk;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    const v1, 0x7f1101a2

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    iget-object v0, p1, Lcgp;->x:Lavk;

    .line 82
    invoke-virtual {v0}, Lavk;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    .line 83
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_3
    const v0, 0x7f1101a1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcgp;)Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcgp;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcgp;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lcgp;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 89
    iget v0, p1, Lcgp;->c:I

    packed-switch v0, :pswitch_data_0

    .line 95
    invoke-static {}, Lbdf;->a()V

    .line 96
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 90
    :pswitch_0
    const v0, 0x7f1101cb

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91
    :pswitch_1
    const v0, 0x7f1101c7

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 92
    :pswitch_2
    const v0, 0x7f1101c8

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :pswitch_3
    const v0, 0x7f1101c9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :pswitch_4
    iget v0, p1, Lcgp;->c:I

    iget-boolean v1, p1, Lcgp;->j:Z

    invoke-static {p0, v0, v1}, Lbvw;->a(Landroid/content/Context;IZ)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/telecom/Call;)Lcdc;
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return-object v0
.end method
