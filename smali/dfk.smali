.class public final Ldfk;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field public final a:Ldez;

.field public final b:Ldfo;

.field public c:Lcte;

.field public d:Landroid/app/Fragment;

.field private e:Ljava/util/HashSet;

.field private f:Ldfk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ldez;

    invoke-direct {v0}, Ldez;-><init>()V

    invoke-direct {p0, v0}, Ldfk;-><init>(Ldez;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Ldez;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 4
    new-instance v0, Ldfl;

    invoke-direct {v0, p0}, Ldfl;-><init>(Ldfk;)V

    iput-object v0, p0, Ldfk;->b:Ldfo;

    .line 5
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldfk;->e:Ljava/util/HashSet;

    .line 6
    iput-object p1, p0, Ldfk;->a:Ldez;

    .line 7
    return-void
.end method

.method private final a()V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Ldfk;->f:Ldfk;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Ldfk;->f:Ldfk;

    .line 19
    iget-object v0, v0, Ldfk;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Ldfk;->f:Ldfk;

    .line 21
    :cond_0
    return-void
.end method


# virtual methods
.method final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 8
    invoke-direct {p0}, Ldfk;->a()V

    .line 9
    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v0

    .line 10
    iget-object v0, v0, Lcsw;->e:Ldfm;

    .line 12
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldfm;->a(Landroid/app/FragmentManager;Landroid/app/Fragment;)Ldfk;

    move-result-object v0

    iput-object v0, p0, Ldfk;->f:Ldfk;

    .line 13
    iget-object v0, p0, Ldfk;->f:Ldfk;

    if-eq v0, p0, :cond_0

    .line 14
    iget-object v0, p0, Ldfk;->f:Ldfk;

    .line 15
    iget-object v0, v0, Ldfk;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 16
    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 23
    :try_start_0
    invoke-virtual {p0, p1}, Ldfk;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    :cond_0
    :goto_0
    return-void

    .line 25
    :catch_0
    move-exception v0

    .line 26
    const-string v1, "RMFragment"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    const-string v1, "RMFragment"

    const-string v2, "Unable to register fragment with root"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 39
    iget-object v0, p0, Ldfk;->a:Ldez;

    invoke-virtual {v0}, Ldez;->c()V

    .line 40
    invoke-direct {p0}, Ldfk;->a()V

    .line 41
    return-void
.end method

.method public final onDetach()V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 30
    invoke-direct {p0}, Ldfk;->a()V

    .line 31
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 33
    iget-object v0, p0, Ldfk;->a:Ldez;

    invoke-virtual {v0}, Ldez;->a()V

    .line 34
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 36
    iget-object v0, p0, Ldfk;->a:Ldez;

    invoke-virtual {v0}, Ldez;->b()V

    .line 37
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Ldfk;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 46
    :goto_0
    if-eqz v0, :cond_1

    .line 47
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "{parent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :cond_1
    iget-object v0, p0, Ldfk;->d:Landroid/app/Fragment;

    goto :goto_1
.end method
