.class final Lclg;
.super Lal;
.source "PG"


# instance fields
.field private synthetic a:Lclf;


# direct methods
.method constructor <init>(Lclf;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lclg;->a:Lclf;

    invoke-direct {p0, p2}, Lal;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)F
    .locals 3

    .prologue
    .line 29
    check-cast p1, Landroid/view/WindowManager$LayoutParams;

    .line 30
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 31
    iget-object v1, p0, Lclg;->a:Lclf;

    .line 32
    iget v1, v1, Lclf;->h:I

    .line 33
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 34
    iget-object v1, p0, Lclg;->a:Lclf;

    .line 35
    invoke-static {p1}, Lclf;->a(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    iget-object v1, p0, Lclg;->a:Lclf;

    .line 38
    iget-object v1, v1, Lclf;->a:Landroid/content/Context;

    .line 39
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 40
    sub-int v0, v1, v0

    .line 41
    :cond_0
    int-to-float v0, v0

    iget-object v1, p0, Lclg;->a:Lclf;

    .line 42
    iget v1, v1, Lclf;->d:I

    .line 43
    int-to-float v1, v1

    iget-object v2, p0, Lclg;->a:Lclf;

    .line 44
    iget v2, v2, Lclf;->f:I

    .line 45
    int-to-float v2, v2

    .line 46
    invoke-static {v0, v1, v2}, Lclf;->a(FFF)F

    move-result v0

    .line 47
    return v0
.end method

.method public final synthetic a(Ljava/lang/Object;F)V
    .locals 5

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2
    check-cast p1, Landroid/view/WindowManager$LayoutParams;

    .line 3
    iget-object v3, p0, Lclg;->a:Lclf;

    .line 4
    iget-object v3, v3, Lclf;->a:Landroid/content/Context;

    .line 5
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 6
    iget-object v4, p0, Lclg;->a:Lclf;

    .line 7
    iget-object v4, v4, Lclf;->c:Lckb;

    .line 9
    iget-object v4, v4, Lckb;->n:Ljava/lang/Integer;

    .line 11
    if-nez v4, :cond_3

    .line 12
    div-int/lit8 v4, v3, 0x2

    int-to-float v4, v4

    cmpl-float v4, p2, v4

    if-lez v4, :cond_2

    .line 14
    :cond_0
    :goto_0
    iget-object v1, p0, Lclg;->a:Lclf;

    .line 15
    iget v1, v1, Lclf;->h:I

    .line 16
    div-int/lit8 v1, v1, 0x2

    .line 18
    if-eqz v0, :cond_4

    int-to-float v3, v3

    sub-float/2addr v3, p2

    int-to-float v1, v1

    sub-float v1, v3, v1

    :goto_1
    float-to-int v1, v1

    iput v1, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 19
    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    or-int/lit8 v0, v0, 0x30

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 20
    iget-object v0, p0, Lclg;->a:Lclf;

    .line 21
    iget-object v0, v0, Lclf;->c:Lckb;

    .line 22
    invoke-virtual {v0}, Lckb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lclg;->a:Lclf;

    .line 24
    iget-object v0, v0, Lclf;->b:Landroid/view/WindowManager;

    .line 25
    iget-object v1, p0, Lclg;->a:Lclf;

    .line 26
    iget-object v1, v1, Lclf;->c:Lckb;

    .line 27
    invoke-virtual {v1}, Lckb;->e()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 28
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 12
    goto :goto_0

    .line 13
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    and-int/lit8 v4, v4, 0x5

    if-eq v4, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 18
    :cond_4
    int-to-float v1, v1

    sub-float v1, p2, v1

    goto :goto_1

    .line 19
    :cond_5
    const/4 v0, 0x3

    goto :goto_2
.end method
