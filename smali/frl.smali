.class final Lfrl;
.super Lfof;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfri;


# direct methods
.method private constructor <init>(Lfri;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrl;->this$0:Lfri;

    invoke-direct {p0}, Lfof;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfri;Lfmt;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lfrl;-><init>(Lfri;)V

    return-void
.end method

.method private final onEndpointConnectingOrConnected(Lfue;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 9
    invoke-virtual {p1}, Lfue;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$100(Lfri;)Lfrh;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v1

    .line 13
    iput-boolean v3, v1, Lfvz;->d:Z

    .line 14
    invoke-virtual {p1}, Lfue;->isAllowedToInvite()Z

    move-result v2

    .line 15
    iput-boolean v2, v1, Lfvz;->e:Z

    .line 16
    invoke-virtual {p1}, Lfue;->isAllowedToKick()Z

    move-result v2

    .line 17
    iput-boolean v2, v1, Lfvz;->f:Z

    move-object v1, v0

    .line 22
    :goto_0
    invoke-virtual {v1, p1}, Lfrh;->setEndpoint(Lfue;)V

    .line 23
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0, v1}, Lfri;->access$600(Lfri;Lfrh;)V

    .line 24
    const-string v0, "Participant joined: %s"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$700(Lfri;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 26
    :try_start_0
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$200(Lfri;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$800(Lfri;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 28
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$900(Lfri;)V

    .line 29
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1000(Lfri;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 31
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1100(Lfri;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrm;

    .line 32
    invoke-virtual {v1}, Lfrh;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    invoke-virtual {v0, v1}, Lfrm;->onParticipantAdded(Lfrh;)V

    goto :goto_1

    .line 19
    :cond_1
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$200(Lfri;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 20
    if-nez v0, :cond_3

    .line 21
    new-instance v0, Lfrh;

    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$500(Lfri;)Lfnp;

    move-result-object v1

    invoke-direct {v0, v1}, Lfrh;-><init>(Lfnp;)V

    move-object v1, v0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 35
    :cond_2
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1200(Lfri;)V

    .line 36
    return-void

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private final onEndpointDisconnected(Lfue;Lfuh;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$700(Lfri;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 38
    :try_start_0
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$200(Lfri;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 39
    if-nez v0, :cond_0

    .line 40
    monitor-exit v1

    .line 51
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v2

    invoke-virtual {p2}, Lfuh;->getPstnErrorCode()I

    if-nez v2, :cond_1

    const/4 v0, 0x0

    throw v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 42
    :cond_1
    :try_start_1
    iget-object v2, p0, Lfrl;->this$0:Lfri;

    invoke-static {v2}, Lfri;->access$1300(Lfri;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v2, p0, Lfrl;->this$0:Lfri;

    invoke-static {v2}, Lfri;->access$900(Lfri;)V

    .line 44
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$1000(Lfri;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 46
    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$1100(Lfri;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrm;

    .line 47
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 48
    invoke-virtual {v1, v0}, Lfrm;->onParticipantRemoved(Lfrh;)V

    goto :goto_1

    .line 50
    :cond_3
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1200(Lfri;)V

    goto :goto_0
.end method

.method private final onEndpointModified(Lfue;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$200(Lfri;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Lfrh;->markDirty()V

    .line 55
    :cond_0
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1200(Lfri;)V

    .line 56
    return-void
.end method


# virtual methods
.method public final onCallEnded(Lfoe;)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1600(Lfri;)Lfnv;

    move-result-object v0

    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$1500(Lfri;)Lfrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfnv;->removeCallStateListener(Lfof;)V

    .line 65
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$700(Lfri;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lfri;->access$1002(Lfri;Z)Z

    .line 67
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lfri;->access$1702(Lfri;Z)Z

    .line 68
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onEndpointEvent(Lfue;Lfuf;)V
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0, p1, p2}, Lfri;->access$300(Lfri;Lfue;Lfuf;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    invoke-direct {p0, p1}, Lfrl;->onEndpointConnectingOrConnected(Lfue;)V

    .line 8
    :cond_0
    :goto_0
    return-void

    .line 4
    :cond_1
    instance-of v0, p2, Lfuh;

    if-eqz v0, :cond_2

    .line 5
    check-cast p2, Lfuh;

    invoke-direct {p0, p1, p2}, Lfrl;->onEndpointDisconnected(Lfue;Lfuh;)V

    goto :goto_0

    .line 6
    :cond_2
    invoke-static {p2}, Lfri;->access$400(Lfuf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    invoke-direct {p0, p1}, Lfrl;->onEndpointModified(Lfue;)V

    goto :goto_0
.end method

.method public final onLoudestSpeakerUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1400(Lfri;)Lfrh;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lfrl;->this$0:Lfri;

    iget-object v2, p0, Lfrl;->this$0:Lfri;

    invoke-virtual {v2, p2}, Lfri;->getParticipant(Ljava/lang/String;)Lfrh;

    move-result-object v2

    invoke-static {v1, v2}, Lfri;->access$1402(Lfri;Lfrh;)Lfrh;

    .line 59
    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$1400(Lfri;)Lfrh;

    move-result-object v1

    if-eq v1, v0, :cond_0

    .line 60
    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1, v0}, Lfri;->access$600(Lfri;Lfrh;)V

    .line 61
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    iget-object v1, p0, Lfrl;->this$0:Lfri;

    invoke-static {v1}, Lfri;->access$1400(Lfri;)Lfrh;

    move-result-object v1

    invoke-static {v0, v1}, Lfri;->access$600(Lfri;Lfrh;)V

    .line 62
    iget-object v0, p0, Lfrl;->this$0:Lfri;

    invoke-static {v0}, Lfri;->access$1200(Lfri;)V

    .line 63
    :cond_0
    return-void
.end method
