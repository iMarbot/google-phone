.class public final Lddf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcuk;


# instance fields
.field private b:Lcuk;

.field private c:Z


# direct methods
.method public constructor <init>(Lcuk;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lddf;->b:Lcuk;

    .line 3
    iput-boolean p2, p0, Lddf;->c:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcwz;II)Lcwz;
    .locals 4

    .prologue
    .line 5
    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v0

    .line 6
    iget-object v1, v0, Lcsw;->a:Lcxl;

    .line 8
    invoke-interface {p2}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 10
    invoke-static {v1, v0, p3, p4}, Lddd;->a(Lcxl;Landroid/graphics/drawable/Drawable;II)Lcwz;

    move-result-object v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    iget-boolean v1, p0, Lddf;->c:Z

    if-eqz v1, :cond_1

    .line 13
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to convert "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to a Bitmap"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 15
    :cond_0
    iget-object v0, p0, Lddf;->b:Lcuk;

    .line 16
    invoke-interface {v0, p1, v1, p3, p4}, Lcuk;->a(Landroid/content/Context;Lcwz;II)Lcwz;

    move-result-object v0

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 18
    invoke-interface {v0}, Lcwz;->d()V

    .line 28
    :cond_1
    :goto_0
    return-object p2

    .line 20
    :cond_2
    invoke-interface {v0}, Lcwz;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v2

    .line 24
    iget-object v2, v2, Lcsw;->a:Lcxl;

    .line 25
    invoke-static {v1, v2, v0}, Lddi;->a(Landroid/content/res/Resources;Lcxl;Landroid/graphics/Bitmap;)Lddi;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Ljava/security/MessageDigest;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lddf;->b:Lcuk;

    invoke-interface {v0, p1}, Lcuk;->a(Ljava/security/MessageDigest;)V

    .line 35
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 29
    instance-of v0, p1, Lddf;

    if-eqz v0, :cond_0

    .line 30
    check-cast p1, Lddf;

    .line 31
    iget-object v0, p0, Lddf;->b:Lcuk;

    iget-object v1, p1, Lddf;->b:Lcuk;

    invoke-interface {v0, v1}, Lcuk;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 32
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lddf;->b:Lcuk;

    invoke-interface {v0}, Lcuk;->hashCode()I

    move-result v0

    return v0
.end method
