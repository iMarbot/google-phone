.class public final Larn;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lasb$a;
.implements Lcom/android/dialer/widget/EmptyContentView$a;
.implements Lib;


# instance fields
.field public final a:Lpj;

.field public final b:Lpj;

.field public c:I

.field public d:Laie;

.field public e:Lart;

.field public f:Lasb;

.field public g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

.field private h:Lahu$a;

.field private i:Landroid/app/LoaderManager$LoaderCallbacks;

.field private j:Larr;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Lcom/android/dialer/widget/EmptyContentView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Lpj;

    invoke-direct {v0}, Lpj;-><init>()V

    iput-object v0, p0, Larn;->a:Lpj;

    .line 3
    new-instance v0, Lpj;

    invoke-direct {v0}, Lpj;-><init>()V

    iput-object v0, p0, Larn;->b:Lpj;

    .line 4
    new-instance v0, Lahu$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lahu$a;-><init>(Larn;B)V

    iput-object v0, p0, Larn;->h:Lahu$a;

    .line 5
    new-instance v0, Larp;

    invoke-direct {v0, p0}, Larp;-><init>(Larn;)V

    iput-object v0, p0, Larn;->i:Landroid/app/LoaderManager$LoaderCallbacks;

    .line 6
    new-instance v0, Larr;

    .line 7
    invoke-direct {v0, p0}, Larr;-><init>(Larn;)V

    .line 8
    iput-object v0, p0, Larn;->j:Larr;

    return-void
.end method

.method static a([JJ)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 101
    move v0, v1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 102
    aget-wide v2, p0, v0

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 103
    const/4 v1, 0x1

    .line 105
    :cond_0
    return v1

    .line 104
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 110
    .line 111
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getFirstVisiblePosition()I

    move-result v2

    move v0, v1

    .line 112
    :goto_0
    iget-object v3, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 113
    iget-object v3, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v3, v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 114
    add-int v4, v2, v0

    .line 115
    iget-object v5, p0, Larn;->f:Lasb;

    invoke-virtual {v5, v4}, Lasb;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    iget-object v5, p0, Larn;->f:Lasb;

    invoke-virtual {v5, v4}, Lasb;->getItemId(I)J

    move-result-wide v4

    .line 117
    iget-object v6, p0, Larn;->a:Lpj;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v5, v7}, Lpj;->a(JLjava/lang/Object;)V

    .line 118
    iget-object v6, p0, Larn;->b:Lpj;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v4, v5, v3}, Lpj;->a(JLjava/lang/Object;)V

    .line 119
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    iget-object v0, p0, Larn;->a:Lpj;

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lpj;->a(JLjava/lang/Object;)V

    .line 121
    return-void
.end method

.method final a(Z)V
    .locals 5

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 60
    iget-object v2, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v2}, Lcom/android/dialer/widget/EmptyContentView;->getVisibility()I

    move-result v4

    .line 61
    if-eqz p1, :cond_1

    move v3, v1

    .line 62
    :goto_0
    if-eqz p1, :cond_2

    move v2, v0

    .line 63
    :goto_1
    if-eq v4, v3, :cond_0

    .line 64
    iget-object v0, p0, Larn;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 65
    if-eqz p1, :cond_3

    const/4 v1, -0x2

    :goto_2
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 66
    iget-object v1, p0, Larn;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, v2}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setVisibility(I)V

    .line 69
    :cond_0
    return-void

    :cond_1
    move v3, v0

    .line 61
    goto :goto_0

    :cond_2
    move v2, v1

    .line 62
    goto :goto_1

    .line 65
    :cond_3
    const/4 v1, -0x1

    goto :goto_2
.end method

.method public final varargs a([J)V
    .locals 3

    .prologue
    .line 106
    .line 107
    iget-object v0, p0, Larn;->a:Lpj;

    invoke-virtual {v0}, Lpj;->b()I

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    const/4 v1, 0x1

    new-instance v2, Laro;

    invoke-direct {v2, p0, p1}, Laro;-><init>(Larn;[J)V

    invoke-static {v0, v1, v2}, Lbib;->a(Landroid/view/View;ZLjava/lang/Runnable;)V

    .line 109
    :cond_0
    return-void
.end method

.method public final h_()V
    .locals 5

    .prologue
    .line 122
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 123
    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Larn;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lbsw;->b:Ljava/util/List;

    .line 127
    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 128
    array-length v2, v1

    if-lez v2, :cond_2

    .line 129
    const-string v2, "OldSpeedDialFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 130
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 131
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0

    .line 130
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 133
    :cond_2
    check-cast v0, Larq;

    invoke-interface {v0}, Larq;->t()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 9
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 10
    new-instance v0, Lasb;

    .line 11
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Larn;->h:Lahu$a;

    invoke-direct {v0, v1, v2, p0}, Lasb;-><init>(Landroid/content/Context;Lahu$a;Lasb$a;)V

    iput-object v0, p0, Larn;->f:Lasb;

    .line 12
    iget-object v0, p0, Larn;->f:Lasb;

    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    .line 13
    iput-object v1, v0, Lasb;->j:Lbfo;

    .line 14
    invoke-virtual {p0}, Larn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Larn;->c:I

    .line 15
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    const v0, 0x7f0400b7

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Larn;->k:Landroid/view/View;

    .line 31
    iget-object v0, p0, Larn;->k:Landroid/view/View;

    const v1, 0x7f0e0262

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/list/PhoneFavoriteListView;

    iput-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 32
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 33
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setVerticalScrollBarEnabled(Z)V

    .line 34
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setVerticalScrollbarPosition(I)V

    .line 35
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setScrollBarStyle(I)V

    .line 36
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 37
    iget-object v0, v0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    .line 38
    iget-object v1, p0, Larn;->f:Lasb;

    invoke-virtual {v0, v1}, Larj;->a(Lars;)V

    .line 40
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e019f

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 41
    iget-object v1, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 42
    iput-object v0, v1, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    .line 43
    iget-object v0, v1, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, v1, Lcom/android/dialer/app/list/PhoneFavoriteListView;->g:Landroid/view/View;

    .line 44
    iget-object v0, p0, Larn;->k:Landroid/view/View;

    const v1, 0x7f0e00d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/EmptyContentView;

    iput-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    .line 45
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f02008e

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 46
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    .line 47
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 48
    iget-object v0, p0, Larn;->k:Landroid/view/View;

    const v1, 0x7f0e0261

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Larn;->l:Landroid/view/View;

    .line 49
    new-instance v0, Landroid/view/animation/LayoutAnimationController;

    .line 50
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;)V

    .line 51
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/LayoutAnimationController;->setDelay(F)V

    .line 52
    iget-object v1, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v1, v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 53
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    iget-object v1, p0, Larn;->f:Lasb;

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 54
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    iget-object v1, p0, Larn;->j:Larr;

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 55
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setFastScrollEnabled(Z)V

    .line 56
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setFastScrollAlwaysVisible(Z)V

    .line 57
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->setAccessibilityLiveRegion(I)V

    .line 58
    iget-object v0, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-static {v0}, Larg;->a(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Larn;->k:Landroid/view/View;

    return-object v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Larn;->f:Lasb;

    invoke-virtual {v0}, Lasb;->getCount()I

    move-result v0

    .line 98
    if-gt p3, v0, :cond_0

    .line 99
    const-string v0, "OldSpeedDialFragment.onItemClick"

    const/16 v1, 0x59

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "event for unexpected position. The position "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is before \"all\" section. Ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    :cond_0
    return-void
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 135
    if-ne p1, v1, :cond_0

    .line 136
    array-length v0, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Lbsw;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 138
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 16
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 17
    iget-object v0, p0, Larn;->f:Lasb;

    if-eqz v0, :cond_0

    .line 18
    iget-object v0, p0, Larn;->f:Lasb;

    .line 19
    iget-object v1, v0, Lasb;->g:Lalj;

    const-string v2, "android.contacts.DISPLAY_ORDER"

    invoke-virtual {v1, v2}, Lalj;->a(Ljava/lang/String;)V

    .line 20
    iget-object v0, v0, Lasb;->g:Lalj;

    const-string v1, "android.contacts.SORT_ORDER"

    invoke-virtual {v0, v1}, Lalj;->a(Ljava/lang/String;)V

    .line 21
    :cond_0
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 22
    invoke-virtual {p0}, Larn;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    if-nez v0, :cond_1

    .line 23
    invoke-virtual {p0}, Larn;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Larn;->i:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 25
    :goto_0
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f1102e6

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 26
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f1102e7

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 29
    :goto_1
    return-void

    .line 24
    :cond_1
    invoke-virtual {p0}, Larn;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Loader;->forceLoad()V

    goto :goto_0

    .line 27
    :cond_2
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110260

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 28
    iget-object v0, p0, Larn;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110262

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    goto :goto_1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 70
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 71
    invoke-virtual {p0}, Larn;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 72
    :try_start_0
    move-object v0, v2

    check-cast v0, Lart;

    move-object v1, v0

    iput-object v1, p0, Larn;->e:Lart;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :try_start_1
    move-object v0, v2

    check-cast v0, Lars;

    move-object v1, v0

    .line 78
    iget-object v3, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 79
    iget-object v3, v3, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    .line 80
    invoke-virtual {v3, v1}, Larj;->a(Lars;)V

    .line 81
    move-object v0, v2

    check-cast v0, Larq;

    move-object v1, v0

    iget-object v3, p0, Larn;->g:Lcom/android/dialer/app/list/PhoneFavoriteListView;

    .line 82
    iget-object v3, v3, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    .line 83
    invoke-interface {v1, v3}, Larq;->a(Larj;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 88
    :try_start_2
    move-object v0, v2

    check-cast v0, Laie;

    move-object v1, v0

    iput-object v1, p0, Larn;->d:Laie;
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_2

    .line 93
    invoke-static {v2}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {p0}, Larn;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Larn;->i:Landroid/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 96
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    .line 76
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " must implement OnListFragmentScrolledListener"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :catch_1
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    .line 87
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " must implement OnDragDropListener and HostInterface"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :catch_2
    move-exception v1

    new-instance v1, Ljava/lang/ClassCastException;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " must implement PhoneFavoritesFragment.listener"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_0
    invoke-virtual {p0, v4}, Larn;->a(Z)V

    goto :goto_0
.end method
