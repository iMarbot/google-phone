.class public final enum Lgzt;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# static fields
.field public static final enum a:Lgzt;

.field public static final enum b:Lgzt;

.field public static final enum c:Lgzt;

.field public static final enum d:Lgzt;

.field public static final enum e:Lgzt;

.field public static final enum f:Lgzt;

.field public static final enum g:Lgzt;

.field public static final enum h:Lgzt;

.field public static final i:Lhby;

.field private static synthetic k:[Lgzt;


# instance fields
.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    new-instance v0, Lgzt;

    const-string v1, "TRANSCRIPTION_STATUS_UNSPECIFIED"

    invoke-direct {v0, v1, v4, v4}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->a:Lgzt;

    .line 17
    new-instance v0, Lgzt;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v5, v5}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->b:Lgzt;

    .line 18
    new-instance v0, Lgzt;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v6, v6}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->c:Lgzt;

    .line 19
    new-instance v0, Lgzt;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v7, v7}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->d:Lgzt;

    .line 20
    new-instance v0, Lgzt;

    const-string v1, "FAILED_RETRY"

    invoke-direct {v0, v1, v8, v8}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->e:Lgzt;

    .line 21
    new-instance v0, Lgzt;

    const-string v1, "FAILED_NO_RETRY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->f:Lgzt;

    .line 22
    new-instance v0, Lgzt;

    const-string v1, "FAILED_LANGUAGE_NOT_SUPPORTED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->g:Lgzt;

    .line 23
    new-instance v0, Lgzt;

    const-string v1, "FAILED_NO_SPEECH_DETECTED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lgzt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgzt;->h:Lgzt;

    .line 24
    const/16 v0, 0x8

    new-array v0, v0, [Lgzt;

    sget-object v1, Lgzt;->a:Lgzt;

    aput-object v1, v0, v4

    sget-object v1, Lgzt;->b:Lgzt;

    aput-object v1, v0, v5

    sget-object v1, Lgzt;->c:Lgzt;

    aput-object v1, v0, v6

    sget-object v1, Lgzt;->d:Lgzt;

    aput-object v1, v0, v7

    sget-object v1, Lgzt;->e:Lgzt;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgzt;->f:Lgzt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgzt;->g:Lgzt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgzt;->h:Lgzt;

    aput-object v2, v0, v1

    sput-object v0, Lgzt;->k:[Lgzt;

    .line 25
    new-instance v0, Lgzu;

    invoke-direct {v0}, Lgzu;-><init>()V

    sput-object v0, Lgzt;->i:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lgzt;->j:I

    .line 15
    return-void
.end method

.method public static a(I)Lgzt;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 12
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lgzt;->a:Lgzt;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lgzt;->b:Lgzt;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lgzt;->c:Lgzt;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lgzt;->d:Lgzt;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lgzt;->e:Lgzt;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lgzt;->f:Lgzt;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lgzt;->g:Lgzt;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Lgzt;->h:Lgzt;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static values()[Lgzt;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgzt;->k:[Lgzt;

    invoke-virtual {v0}, [Lgzt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgzt;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lgzt;->j:I

    return v0
.end method
