.class public final Layu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field private synthetic a:Lazb;

.field private synthetic b:F

.field private synthetic c:Layq;


# direct methods
.method public constructor <init>(Layq;Lazb;F)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Layu;->c:Layq;

    iput-object p2, p0, Layu;->a:Lazb;

    iput p3, p0, Layu;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    iget-object v0, p0, Layu;->c:Layq;

    .line 3
    iput-boolean v6, v0, Layq;->m:Z

    .line 5
    iget-object v0, p0, Layu;->c:Layq;

    .line 6
    iget-object v0, v0, Layq;->j:Landroid/hardware/Camera;

    .line 7
    if-eq v0, p2, :cond_0

    .line 8
    iget-object v0, p0, Layu;->a:Lazb;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lazb;->e(I)V

    .line 36
    :goto_0
    return-void

    .line 10
    :cond_0
    if-nez p1, :cond_1

    .line 11
    iget-object v0, p0, Layu;->a:Lazb;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lazb;->e(I)V

    goto :goto_0

    .line 13
    :cond_1
    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    .line 14
    iget-object v1, p0, Layu;->c:Layq;

    .line 15
    iget v1, v1, Layq;->k:I

    .line 16
    const/16 v2, 0x5a

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Layu;->c:Layq;

    .line 17
    iget v1, v1, Layq;->k:I

    .line 18
    const/16 v2, 0x10e

    if-ne v1, v2, :cond_3

    .line 19
    :cond_2
    iget v1, v0, Landroid/hardware/Camera$Size;->height:I

    .line 20
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    .line 23
    :goto_1
    const-string v0, "CameraManager.onPictureTaken"

    array-length v3, p1

    const/16 v4, 0x25

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "taken picture size: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    iget-object v0, p0, Layu;->c:Layq;

    .line 25
    iget-object v0, v0, Layq;->f:Laze;

    .line 26
    invoke-virtual {v0}, Laze;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v6

    new-instance v0, Lazh;

    iget v3, p0, Layu;->b:F

    iget-object v4, p0, Layu;->c:Layq;

    .line 29
    iget-object v4, v4, Layq;->f:Laze;

    .line 30
    invoke-virtual {v4}, Laze;->c()Landroid/content/Context;

    move-result-object v5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lazh;-><init>(IIF[BLandroid/content/Context;)V

    .line 31
    invoke-virtual {v6, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Layv;

    iget-object v2, p0, Layu;->a:Lazb;

    invoke-direct {v1, v2}, Layv;-><init>(Lazb;)V

    .line 32
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Layw;

    iget-object v2, p0, Layu;->a:Lazb;

    invoke-direct {v1, v2}, Layw;-><init>(Lazb;)V

    .line 33
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 35
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 21
    :cond_3
    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    .line 22
    iget v2, v0, Landroid/hardware/Camera$Size;->height:I

    goto :goto_1
.end method
