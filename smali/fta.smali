.class Lfta;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfsw;


# instance fields
.field public final apiaryUri:Ljava/lang/String;

.field public authTime:Ljava/lang/String;

.field public authToken:Ljava/lang/String;

.field public final httpTransport:Lgga;

.field public final listener:Lfsr;

.field public final path:Ljava/lang/String;

.field public final requestBytes:[B

.field public final requestId:J

.field public final timeoutInMillis:I


# direct methods
.method constructor <init>(JLjava/lang/String;[BILgga;Ljava/lang/String;Lfsr;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lfta;->requestId:J

    .line 3
    iput-object p3, p0, Lfta;->path:Ljava/lang/String;

    .line 4
    iput-object p4, p0, Lfta;->requestBytes:[B

    .line 5
    iput p5, p0, Lfta;->timeoutInMillis:I

    .line 6
    iput-object p6, p0, Lfta;->httpTransport:Lgga;

    .line 7
    iput-object p7, p0, Lfta;->apiaryUri:Ljava/lang/String;

    .line 8
    iput-object p8, p0, Lfta;->listener:Lfsr;

    .line 9
    return-void
.end method

.method public static createHttpRequestInitializer(Ljava/lang/String;Ljava/lang/String;I)Lgfv;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lgfi;

    invoke-direct {v0}, Lgfi;-><init>()V

    .line 17
    invoke-virtual {v0, p0}, Lgfi;->b(Ljava/lang/String;)Lgfi;

    .line 18
    new-instance v1, Lftb;

    invoke-direct {v1, p1, p0, v0, p2}, Lftb;-><init>(Ljava/lang/String;Ljava/lang/String;Lgfi;I)V

    return-object v1
.end method

.method private handleProtoResponse(Lfsx;)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 38
    .line 39
    :try_start_0
    invoke-virtual {p1}, Lfsx;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 40
    :try_start_1
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 41
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 42
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    .line 43
    :goto_0
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 44
    int-to-byte v1, v1

    .line 45
    invoke-virtual {v4, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 46
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    goto :goto_0

    .line 48
    :cond_0
    const-string v1, "X-Goog-Safety-Encoding"

    .line 49
    invoke-virtual {p1, v1}, Lfsx;->getFirstHeaderStringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    const-string v3, "base64"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3}, Landroid/util/Base64;->decode([BI)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 52
    if-eqz v2, :cond_1

    .line 53
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 69
    :cond_1
    :goto_1
    return-object v0

    .line 57
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 58
    if-eqz v2, :cond_1

    .line 59
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 63
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 64
    :goto_2
    :try_start_5
    const-string v3, "Error processing apiary response"

    invoke-static {v3, v1}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 65
    if-eqz v2, :cond_1

    .line 66
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    .line 70
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_3

    .line 71
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 74
    :cond_3
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_4

    .line 70
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 63
    :catch_5
    move-exception v1

    goto :goto_2
.end method

.method static headerToLowerCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic doInBackgroundTimed()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lfta;->doInBackgroundTimed()[B

    move-result-object v0

    return-object v0
.end method

.method public doInBackgroundTimed()[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 19
    new-instance v1, Lgfi;

    invoke-direct {v1}, Lgfi;-><init>()V

    .line 20
    iget-object v2, p0, Lfta;->authToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lgfi;->b(Ljava/lang/String;)Lgfi;

    .line 21
    iget-object v1, p0, Lfta;->authToken:Ljava/lang/String;

    iget-object v2, p0, Lfta;->authTime:Ljava/lang/String;

    iget v3, p0, Lfta;->timeoutInMillis:I

    .line 22
    invoke-static {v1, v2, v3}, Lfta;->createHttpRequestInitializer(Ljava/lang/String;Ljava/lang/String;I)Lgfv;

    move-result-object v1

    .line 23
    iget-object v2, p0, Lfta;->httpTransport:Lgga;

    invoke-virtual {v2, v1}, Lgga;->a(Lgfv;)Lgfu;

    move-result-object v1

    .line 24
    new-instance v2, Lfsv;

    iget-object v3, p0, Lfta;->requestBytes:[B

    invoke-direct {v2, v3}, Lfsv;-><init>([B)V

    .line 25
    :try_start_0
    new-instance v3, Ljava/net/URL;

    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lfta;->apiaryUri:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lfta;->path:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    new-instance v4, Lgfn;

    invoke-direct {v4, v3}, Lgfn;-><init>(Ljava/net/URL;)V

    .line 31
    :try_start_1
    invoke-virtual {v1, v4, v2}, Lgfu;->a(Lgfn;Lgfo;)Lgqj;

    move-result-object v1

    .line 32
    new-instance v2, Lfsx;

    .line 33
    invoke-virtual {v1}, Lgqj;->i()Lgfw;

    move-result-object v1

    invoke-direct {v2, v1}, Lfsx;-><init>(Lgfw;)V

    .line 34
    invoke-direct {p0, v2}, Lfta;->handleProtoResponse(Lfsx;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    .line 27
    :catch_0
    move-exception v1

    .line 28
    const-string v2, "Error processing request url"

    invoke-static {v2, v1}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 35
    :catch_1
    move-exception v1

    .line 36
    const-string v2, "Error making apiary request"

    invoke-static {v2, v1}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 80
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lfta;->onPostExecute([B)V

    return-void
.end method

.method public onPostExecute([B)V
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lfta;->listener:Lfsr;

    if-eqz v0, :cond_0

    .line 76
    if-nez p1, :cond_1

    .line 77
    iget-object v0, p0, Lfta;->listener:Lfsr;

    iget-wide v2, p0, Lfta;->requestId:J

    invoke-interface {v0, v2, v3}, Lfsr;->onRequestError(J)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lfta;->listener:Lfsr;

    iget-wide v2, p0, Lfta;->requestId:J

    invoke-interface {v0, v2, v3, p1}, Lfsr;->onRequestCompleted(J[B)V

    goto :goto_0
.end method

.method public onPreExecute()V
    .locals 4

    .prologue
    .line 13
    iget-object v0, p0, Lfta;->listener:Lfsr;

    iget-wide v2, p0, Lfta;->requestId:J

    iget-object v1, p0, Lfta;->path:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, Lfsr;->onRequestStarting(JLjava/lang/String;)V

    .line 14
    return-void
.end method

.method public setAuthToken(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 10
    iput-object p1, p0, Lfta;->authToken:Ljava/lang/String;

    .line 11
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    const-string v0, "none"

    :goto_0
    iput-object v0, p0, Lfta;->authTime:Ljava/lang/String;

    .line 12
    return-void

    .line 11
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
