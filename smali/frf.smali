.class final Lfrf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;


# instance fields
.field public final synthetic this$0:Lfrd;


# direct methods
.method constructor <init>(Lfrd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfrf;->this$0:Lfrd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lgoi$b;)V
    .locals 3

    .prologue
    .line 4
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed to update media source: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lfrf;->this$0:Lfrd;

    invoke-virtual {v0}, Lfrd;->maybeSendUpdate()V

    .line 6
    return-void
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lgoi$b;

    invoke-virtual {p0, p1}, Lfrf;->onError(Lgoi$b;)V

    return-void
.end method

.method public final onSuccess(Lgoi$b;)V
    .locals 1

    .prologue
    .line 2
    const-string v0, "Successfully updated media source."

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 7
    check-cast p1, Lgoi$b;

    invoke-virtual {p0, p1}, Lfrf;->onSuccess(Lgoi$b;)V

    return-void
.end method
