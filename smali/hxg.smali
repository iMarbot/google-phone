.class public final Lhxg;
.super Lhxo;
.source "PG"


# instance fields
.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;J)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lhxo;-><init>(Ljava/io/InputStream;)V

    .line 2
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Limit may not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-wide p2, p0, Lhxg;->b:J

    .line 5
    return-void
.end method

.method private final a()V
    .locals 4

    .prologue
    .line 6
    iget-wide v0, p0, Lhxg;->a:J

    iget-wide v2, p0, Lhxg;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 7
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input stream limit exceeded"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    return-void
.end method

.method private final b()I
    .locals 6

    .prologue
    .line 17
    const-wide/32 v0, 0x7fffffff

    iget-wide v2, p0, Lhxg;->b:J

    iget-wide v4, p0, Lhxg;->a:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final read()I
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lhxg;->a()V

    .line 10
    invoke-super {p0}, Lhxo;->read()I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lhxg;->a()V

    .line 12
    invoke-direct {p0}, Lhxg;->b()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 13
    invoke-super {p0, p1, p2, v0}, Lhxo;->read([BII)I

    move-result v0

    return v0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0}, Lhxg;->a()V

    .line 15
    invoke-direct {p0}, Lhxg;->b()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 16
    invoke-super {p0, v0, v1}, Lhxo;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
