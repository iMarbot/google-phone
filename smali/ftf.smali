.class final Lftf;
.super Lfmx;
.source "PG"


# instance fields
.field public final synthetic this$0:Lftc;


# direct methods
.method private constructor <init>(Lftc;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lftf;->this$0:Lftc;

    invoke-direct {p0}, Lfmx;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lftc;Lfte;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lftf;-><init>(Lftc;)V

    return-void
.end method


# virtual methods
.method protected final varargs doInBackgroundTimed([Ljava/lang/Void;)Landroid/util/Pair;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2
    :try_start_0
    const-string v1, "AuthenticationTask.doInBackgroundTimed"

    invoke-static {v1}, Lfvh;->logd(Ljava/lang/String;)V

    .line 3
    iget-object v1, p0, Lftf;->this$0:Lftc;

    invoke-static {v1}, Lftc;->access$200(Lftc;)Lfmy;

    move-result-object v1

    iget-object v2, p0, Lftf;->this$0:Lftc;

    invoke-static {v2}, Lftc;->access$100(Lftc;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "oauth2:https://www.googleapis.com/auth/hangouts "

    invoke-interface {v1, v2, v3}, Lfmy;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4
    const-string v2, "Got authToken for hangouts"

    invoke-static {v2}, Lfvh;->logd(Ljava/lang/String;)V

    .line 5
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Ldwj; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldwe; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 13
    :goto_0
    return-object v0

    .line 6
    :catch_0
    move-exception v1

    .line 7
    const-string v2, "Got authException"

    invoke-static {v2, v1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 8
    invoke-virtual {v1}, Ldwj;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 9
    :catch_1
    move-exception v1

    .line 10
    const-string v2, "Error in getToken"

    invoke-static {v2, v1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 13
    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method protected final bridge synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lftf;->doInBackgroundTimed([Ljava/lang/Void;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected final onPostExecute(Landroid/util/Pair;)V
    .locals 4

    .prologue
    .line 14
    const-string v0, "AuthenticationTask.onPostExecute"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 15
    invoke-virtual {p0}, Lftf;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    const-string v0, "AuthenticationTask cancelled"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 25
    :goto_0
    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lftf;->this$0:Lftc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lftc;->access$302(Lftc;Lftf;)Lftf;

    .line 19
    if-nez p1, :cond_1

    .line 20
    iget-object v0, p0, Lftf;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$1100(Lftc;)Lfvt;

    move-result-object v0

    invoke-virtual {v0}, Lfvt;->onAuthError()V

    goto :goto_0

    .line 21
    :cond_1
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 22
    iget-object v1, p0, Lftf;->this$0:Lftc;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lftc;->access$002(Lftc;Ljava/lang/String;)Ljava/lang/String;

    .line 23
    iget-object v0, p0, Lftf;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$700(Lftc;)Lfsq;

    move-result-object v0

    iget-object v1, p0, Lftf;->this$0:Lftc;

    invoke-static {v1}, Lftc;->access$000(Lftc;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lfsq;->setAuthToken(Ljava/lang/String;J)V

    goto :goto_0

    .line 24
    :cond_2
    iget-object v0, p0, Lftf;->this$0:Lftc;

    invoke-static {v0}, Lftc;->access$1100(Lftc;)Lfvt;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lfvt;->onAuthUserActionRequired(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0, p1}, Lftf;->onPostExecute(Landroid/util/Pair;)V

    return-void
.end method
