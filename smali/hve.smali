.class public Lhve;
.super Ljava/io/InputStream;
.source "PG"


# static fields
.field private static a:[I


# instance fields
.field private b:[B

.field private c:Ljava/io/InputStream;

.field private d:[B

.field private e:Lhyh;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Lhvg;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x100

    const/4 v0, 0x0

    .line 133
    new-array v1, v4, [I

    sput-object v1, Lhve;->a:[I

    move v1, v0

    .line 134
    :goto_0
    if-ge v1, v4, :cond_0

    .line 135
    sget-object v2, Lhve;->a:[I

    const/4 v3, -0x1

    aput v3, v2, v1

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_0
    :goto_1
    sget-object v1, Lhvf;->a:[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 138
    sget-object v1, Lhve;->a:[I

    sget-object v2, Lhvf;->a:[B

    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    aput v0, v1, v2

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 140
    :cond_1
    return-void
.end method

.method private constructor <init>(ILjava/io/InputStream;Lhvg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 4
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lhve;->b:[B

    .line 5
    iput v1, p0, Lhve;->f:I

    .line 6
    iput v1, p0, Lhve;->g:I

    .line 7
    iput-boolean v1, p0, Lhve;->h:Z

    .line 8
    if-nez p2, :cond_0

    .line 9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 10
    :cond_0
    const/16 v0, 0x600

    new-array v0, v0, [B

    iput-object v0, p0, Lhve;->d:[B

    .line 11
    new-instance v0, Lhyh;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lhyh;-><init>(I)V

    iput-object v0, p0, Lhve;->e:Lhyh;

    .line 12
    iput-object p2, p0, Lhve;->c:Ljava/io/InputStream;

    .line 13
    iput-object p3, p0, Lhve;->j:Lhvg;

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lhvg;)V
    .locals 1

    .prologue
    .line 1
    const/16 v0, 0x600

    invoke-direct {p0, v0, p1, p2}, Lhve;-><init>(ILjava/io/InputStream;Lhvg;)V

    .line 2
    return-void
.end method

.method private final a([BII)I
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 43
    .line 44
    add-int v5, p2, p3

    .line 46
    iget-object v0, p0, Lhve;->e:Lhyh;

    .line 47
    iget v0, v0, Lhyh;->b:I

    .line 48
    if-lez v0, :cond_14

    .line 49
    iget-object v0, p0, Lhve;->e:Lhyh;

    .line 50
    iget v0, v0, Lhyh;->b:I

    .line 51
    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 52
    iget-object v2, p0, Lhve;->e:Lhyh;

    .line 53
    iget-object v2, v2, Lhyh;->a:[B

    .line 54
    invoke-static {v2, v1, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    iget-object v2, p0, Lhve;->e:Lhyh;

    invoke-virtual {v2, v1, v0}, Lhyh;->a(II)V

    .line 56
    add-int/2addr v0, p2

    .line 57
    :goto_0
    iget-boolean v2, p0, Lhve;->i:Z

    if-eqz v2, :cond_1

    .line 58
    if-ne v0, p2, :cond_0

    move v0, v4

    .line 132
    :goto_1
    return v0

    .line 58
    :cond_0
    sub-int/2addr v0, p2

    goto :goto_1

    :cond_1
    move v2, v1

    move v3, v0

    move v0, v1

    .line 61
    :cond_2
    if-ge v3, v5, :cond_13

    .line 62
    :cond_3
    :goto_2
    iget v6, p0, Lhve;->f:I

    iget v7, p0, Lhve;->g:I

    if-ne v6, v7, :cond_8

    .line 63
    iget-object v6, p0, Lhve;->c:Ljava/io/InputStream;

    iget-object v7, p0, Lhve;->d:[B

    iget-object v8, p0, Lhve;->d:[B

    array-length v8, v8

    invoke-virtual {v6, v7, v1, v8}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    .line 64
    if-ne v6, v4, :cond_6

    .line 65
    iput-boolean v10, p0, Lhve;->i:Z

    .line 66
    if-eqz v0, :cond_4

    .line 68
    iget-object v1, p0, Lhve;->j:Lhvg;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "dropping "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " sextet(s)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lhvg;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 69
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected end of BASE64 stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_4
    if-ne v3, p2, :cond_5

    move v0, v4

    goto :goto_1

    :cond_5
    sub-int v0, v3, p2

    goto :goto_1

    .line 71
    :cond_6
    if-lez v6, :cond_3

    .line 72
    iput v1, p0, Lhve;->f:I

    .line 73
    iput v6, p0, Lhve;->g:I

    goto :goto_2

    .line 108
    :cond_7
    shl-int/lit8 v2, v2, 0x6

    or-int/2addr v2, v7

    .line 109
    add-int/lit8 v0, v0, 0x1

    .line 110
    const/4 v6, 0x4

    if-ne v0, v6, :cond_8

    .line 112
    ushr-int/lit8 v0, v2, 0x10

    int-to-byte v0, v0

    .line 113
    ushr-int/lit8 v6, v2, 0x8

    int-to-byte v6, v6

    .line 114
    int-to-byte v7, v2

    .line 115
    add-int/lit8 v8, v5, -0x2

    if-ge v3, v8, :cond_10

    .line 116
    add-int/lit8 v8, v3, 0x1

    aput-byte v0, p1, v3

    .line 117
    add-int/lit8 v3, v8, 0x1

    aput-byte v6, p1, v8

    .line 118
    add-int/lit8 v0, v3, 0x1

    aput-byte v7, p1, v3

    move v3, v0

    move v0, v1

    .line 75
    :cond_8
    iget v6, p0, Lhve;->f:I

    iget v7, p0, Lhve;->g:I

    if-ge v6, v7, :cond_2

    if-ge v3, v5, :cond_2

    .line 76
    iget-object v6, p0, Lhve;->d:[B

    iget v7, p0, Lhve;->f:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lhve;->f:I

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    .line 77
    const/16 v7, 0x3d

    if-ne v6, v7, :cond_f

    .line 79
    iput-boolean v10, p0, Lhve;->i:Z

    .line 80
    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 81
    ushr-int/lit8 v0, v2, 0x4

    int-to-byte v1, v0

    .line 82
    if-ge v3, v5, :cond_a

    .line 83
    add-int/lit8 v0, v3, 0x1

    aput-byte v1, p1, v3

    move v3, v0

    .line 102
    :cond_9
    :goto_3
    sub-int v0, v3, p2

    goto/16 :goto_1

    .line 84
    :cond_a
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v1}, Lhyh;->a(I)V

    goto :goto_3

    .line 85
    :cond_b
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    .line 86
    ushr-int/lit8 v0, v2, 0xa

    int-to-byte v1, v0

    .line 87
    ushr-int/lit8 v0, v2, 0x2

    int-to-byte v2, v0

    .line 88
    add-int/lit8 v0, v5, -0x1

    if-ge v3, v0, :cond_c

    .line 89
    add-int/lit8 v0, v3, 0x1

    aput-byte v1, p1, v3

    .line 90
    add-int/lit8 v3, v0, 0x1

    aput-byte v2, p1, v0

    goto :goto_3

    .line 91
    :cond_c
    if-ge v3, v5, :cond_d

    .line 92
    add-int/lit8 v0, v3, 0x1

    aput-byte v1, p1, v3

    .line 93
    iget-object v1, p0, Lhve;->e:Lhyh;

    invoke-virtual {v1, v2}, Lhyh;->a(I)V

    move v3, v0

    goto :goto_3

    .line 94
    :cond_d
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v1}, Lhyh;->a(I)V

    .line 95
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v2}, Lhyh;->a(I)V

    goto :goto_3

    .line 98
    :cond_e
    iget-object v1, p0, Lhve;->j:Lhvg;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "dropping "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " sextet(s)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lhvg;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 99
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected padding character"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_f
    sget-object v7, Lhve;->a:[I

    aget v7, v7, v6

    .line 104
    if-gez v7, :cond_7

    .line 105
    const/16 v7, 0xd

    if-eq v6, v7, :cond_8

    const/16 v7, 0xa

    if-eq v6, v7, :cond_8

    const/16 v7, 0x20

    if-eq v6, v7, :cond_8

    .line 106
    iget-object v7, p0, Lhve;->j:Lhvg;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected base64 byte: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-byte v6, v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Lhvg;->a()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 107
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected base64 byte"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_10
    add-int/lit8 v1, v5, -0x1

    if-ge v3, v1, :cond_11

    .line 120
    add-int/lit8 v1, v3, 0x1

    aput-byte v0, p1, v3

    .line 121
    aput-byte v6, p1, v1

    .line 122
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v7}, Lhyh;->a(I)V

    .line 130
    :goto_4
    sub-int v0, v5, p2

    goto/16 :goto_1

    .line 123
    :cond_11
    if-ge v3, v5, :cond_12

    .line 124
    aput-byte v0, p1, v3

    .line 125
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v6}, Lhyh;->a(I)V

    .line 126
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v7}, Lhyh;->a(I)V

    goto :goto_4

    .line 127
    :cond_12
    iget-object v1, p0, Lhve;->e:Lhyh;

    invoke-virtual {v1, v0}, Lhyh;->a(I)V

    .line 128
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v6}, Lhyh;->a(I)V

    .line 129
    iget-object v0, p0, Lhve;->e:Lhyh;

    invoke-virtual {v0, v7}, Lhyh;->a(I)V

    goto :goto_4

    .line 132
    :cond_13
    sub-int v0, v5, p2

    goto/16 :goto_1

    :cond_14
    move v0, p2

    goto/16 :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lhve;->h:Z

    if-eqz v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhve;->h:Z

    goto :goto_0
.end method

.method public read()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 15
    iget-boolean v1, p0, Lhve;->h:Z

    if-eqz v1, :cond_0

    .line 16
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream has been closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iget-object v1, p0, Lhve;->b:[B

    invoke-direct {p0, v1, v2, v3}, Lhve;->a([BII)I

    move-result v1

    .line 18
    if-ne v1, v0, :cond_1

    .line 21
    :goto_0
    return v0

    .line 20
    :cond_1
    if-ne v1, v3, :cond_0

    .line 21
    iget-object v0, p0, Lhve;->b:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([B)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 23
    iget-boolean v1, p0, Lhve;->h:Z

    if-eqz v1, :cond_0

    .line 24
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream has been closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_0
    if-nez p1, :cond_1

    .line 26
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27
    :cond_1
    array-length v1, p1

    if-nez v1, :cond_2

    .line 29
    :goto_0
    return v0

    :cond_2
    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lhve;->a([BII)I

    move-result v0

    goto :goto_0
.end method

.method public read([BII)I
    .locals 2

    .prologue
    .line 30
    iget-boolean v0, p0, Lhve;->h:Z

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream has been closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    if-nez p1, :cond_1

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34
    :cond_1
    if-ltz p2, :cond_2

    if-ltz p3, :cond_2

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_3

    .line 35
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 36
    :cond_3
    if-nez p3, :cond_4

    .line 37
    const/4 v0, 0x0

    .line 38
    :goto_0
    return v0

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lhve;->a([BII)I

    move-result v0

    goto :goto_0
.end method
