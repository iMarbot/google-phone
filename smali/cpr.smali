.class public final Lcpr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field public final a:Ljava/util/Queue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    return-void
.end method

.method private a(Lcpi;)Lcph;
    .locals 3

    .prologue
    .line 25
    invoke-static {}, Lbvs;->f()V

    .line 26
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcph;

    .line 27
    invoke-interface {v0}, Lcph;->e()Lcpi;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcpi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 3

    .prologue
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 4
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcph;

    .line 5
    invoke-static {v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lcph;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/content/Context;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lbvs;->c(Z)V

    .line 9
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 10
    invoke-static {p1, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/os/Bundle;)Lcph;

    move-result-object v2

    .line 11
    invoke-interface {v2, v0}, Lcph;->a(Landroid/os/Bundle;)V

    .line 12
    invoke-virtual {p0, v2}, Lcpr;->a(Lcph;)Z

    goto :goto_0

    .line 14
    :cond_0
    return-void
.end method

.method public final a(Lcph;)Z
    .locals 2

    .prologue
    .line 15
    invoke-interface {p1}, Lcph;->e()Lcpi;

    move-result-object v0

    iget v0, v0, Lcpi;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Task id was not set to a valid value before adding."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 17
    :cond_0
    invoke-interface {p1}, Lcph;->e()Lcpi;

    move-result-object v0

    iget v0, v0, Lcpi;->a:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    .line 18
    invoke-interface {p1}, Lcph;->e()Lcpi;

    move-result-object v0

    invoke-direct {p0, v0}, Lcpr;->a(Lcpi;)Lcph;

    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    invoke-interface {v0, p1}, Lcph;->a(Lcph;)V

    .line 21
    const-string v0, "TaskQueue.add"

    const-string v1, "duplicated task added"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0

    .line 23
    :cond_1
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 24
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcpr;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
