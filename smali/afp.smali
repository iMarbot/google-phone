.class final Lafp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Landroid/view/View;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lafp;->e:Landroid/view/View;

    .line 3
    return-void
.end method

.method private final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    iget-object v0, p0, Lafp;->e:Landroid/view/View;

    iget v1, p0, Lafp;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setLeft(I)V

    .line 17
    iget-object v0, p0, Lafp;->e:Landroid/view/View;

    iget v1, p0, Lafp;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setTop(I)V

    .line 18
    iget-object v0, p0, Lafp;->e:Landroid/view/View;

    iget v1, p0, Lafp;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setRight(I)V

    .line 19
    iget-object v0, p0, Lafp;->e:Landroid/view/View;

    iget v1, p0, Lafp;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBottom(I)V

    .line 20
    iput v2, p0, Lafp;->f:I

    .line 21
    iput v2, p0, Lafp;->g:I

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/PointF;)V
    .locals 2

    .prologue
    .line 4
    iget v0, p1, Landroid/graphics/PointF;->x:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lafp;->a:I

    .line 5
    iget v0, p1, Landroid/graphics/PointF;->y:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lafp;->b:I

    .line 6
    iget v0, p0, Lafp;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lafp;->f:I

    .line 7
    iget v0, p0, Lafp;->f:I

    iget v1, p0, Lafp;->g:I

    if-ne v0, v1, :cond_0

    .line 8
    invoke-direct {p0}, Lafp;->a()V

    .line 9
    :cond_0
    return-void
.end method

.method public final b(Landroid/graphics/PointF;)V
    .locals 2

    .prologue
    .line 10
    iget v0, p1, Landroid/graphics/PointF;->x:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lafp;->c:I

    .line 11
    iget v0, p1, Landroid/graphics/PointF;->y:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lafp;->d:I

    .line 12
    iget v0, p0, Lafp;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lafp;->g:I

    .line 13
    iget v0, p0, Lafp;->f:I

    iget v1, p0, Lafp;->g:I

    if-ne v0, v1, :cond_0

    .line 14
    invoke-direct {p0}, Lafp;->a()V

    .line 15
    :cond_0
    return-void
.end method
