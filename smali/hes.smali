.class public final Lhes;
.super Ljava/util/AbstractList;
.source "PG"

# interfaces
.implements Lhcn;
.implements Ljava/util/RandomAccess;


# instance fields
.field public final a:Lhcn;


# direct methods
.method public constructor <init>(Lhcn;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2
    iput-object p1, p0, Lhes;->a:Lhcn;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lhes;->a:Lhcn;

    invoke-interface {v0, p1}, Lhcn;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lhah;)V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lhes;->a:Lhcn;

    invoke-interface {v0}, Lhcn;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lhcn;
    .locals 0

    .prologue
    .line 10
    return-object p0
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    .line 12
    iget-object v0, p0, Lhes;->a:Lhcn;

    invoke-interface {v0, p1}, Lhcn;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 13
    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lheu;

    invoke-direct {v0, p0}, Lheu;-><init>(Lhes;)V

    return-object v0
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lhet;

    invoke-direct {v0, p0, p1}, Lhet;-><init>(Lhes;I)V

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lhes;->a:Lhcn;

    invoke-interface {v0}, Lhcn;->size()I

    move-result v0

    return v0
.end method
