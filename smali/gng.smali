.class public Lgng;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgng$e;,
        Lgng$d;,
        Lgng$c;,
        Lgng$f;,
        Lgng$b;,
        Lgng$g;,
        Lgng$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;

.field public b:Ljava/lang/Object;

.field public final c:Ljava/lang/Object;

.field public d:Landroid/content/Context;

.field public e:Lgel;

.field public volatile f:Z

.field public final g:Landroid/content/ServiceConnection;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Landroid/content/Context;Lgel;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgng;->c:Ljava/lang/Object;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgng;->f:Z

    .line 12
    new-instance v0, Lgej;

    invoke-direct {v0, p0}, Lgej;-><init>(Lgng;)V

    iput-object v0, p0, Lgng;->g:Landroid/content/ServiceConnection;

    .line 13
    iput-object p1, p0, Lgng;->a:Ljava/lang/Class;

    .line 14
    iput-object p2, p0, Lgng;->d:Landroid/content/Context;

    .line 15
    iput-object p3, p0, Lgng;->e:Lgel;

    .line 16
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 69
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredClasses()[Ljava/lang/Class;

    move-result-object v4

    .line 70
    :goto_0
    array-length v1, v4

    if-ge v0, v1, :cond_2

    .line 71
    aget-object v1, v4, v0

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Stub"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    :try_start_0
    aget-object v1, v4, v0

    const-string v3, "asInterface"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/os/IBinder;

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76
    :goto_1
    return-object v0

    .line 73
    :catch_0
    move-exception v1

    move-object v3, v1

    .line 74
    const-string v5, "RcsClientLib"

    const-string v6, "Error while getting stub: "

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v5, v1, v3}, Lhcw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v0, v2

    .line 76
    goto :goto_1
.end method

.method public static synthetic a(Lgng;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lgng;->a(Landroid/os/IBinder;)V

    return-void
.end method

.method public static checkRoleOrThrow(I)I
    .locals 3

    .prologue
    .line 2
    packed-switch p0, :pswitch_data_0

    .line 4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Role"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3
    :pswitch_0
    return p0

    .line 2
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkRoleOrThrow([I)[I
    .locals 3

    .prologue
    .line 5
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 6
    invoke-static {v2}, Lgng;->checkRoleOrThrow(I)I

    .line 7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "com.google.android.rcs.service.service.JibeService"

    return-object v0
.end method

.method public a(Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 62
    iget-object v1, p0, Lgng;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lgng;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, Lgng;->a(Ljava/lang/Class;Landroid/os/IBinder;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lgng;->b:Ljava/lang/Object;

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    iget-object v0, p0, Lgng;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgng;->c(Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 67
    :cond_0
    sget-object v0, Lgem;->b:Lgem;

    invoke-virtual {p0, v0}, Lgng;->a(Lgem;)V

    goto :goto_0
.end method

.method public a(Lgem;)V
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lgng;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    invoke-virtual {p0}, Lgng;->d()V

    .line 55
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    iget-boolean v0, p0, Lgng;->f:Z

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lgng;->e:Lgel;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lgel;->a(Ljava/lang/String;Lgem;)V

    .line 58
    :cond_0
    return-void

    .line 55
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.apps.messaging"

    .line 29
    invoke-virtual {p0}, Lgng;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 31
    return-object v0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgng;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lgng;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lgen;

    invoke-direct {v0}, Lgen;-><init>()V

    throw v0

    .line 49
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lgng;->f:Z

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lgng;->e:Lgel;

    invoke-interface {v0, p1}, Lgel;->a(Ljava/lang/String;)V

    .line 52
    :cond_0
    return-void
.end method

.method public connect()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 18
    iget-object v0, p0, Lgng;->e:Lgel;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgng;->f:Z

    .line 19
    iget-object v0, p0, Lgng;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgng;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 20
    const-string v2, "expected_version"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 21
    iget-object v2, p0, Lgng;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.apps.messaging"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    iget-object v2, p0, Lgng;->d:Landroid/content/Context;

    const-string v3, "com.google.android.rcs.service.JibeService"

    invoke-virtual {p0, v3}, Lgng;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 23
    :cond_0
    iget-object v2, p0, Lgng;->d:Landroid/content/Context;

    iget-object v3, p0, Lgng;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 24
    if-nez v0, :cond_1

    iget-boolean v1, p0, Lgng;->f:Z

    if-eqz v1, :cond_1

    .line 25
    iget-object v1, p0, Lgng;->e:Lgel;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lgem;->a:Lgem;

    invoke-interface {v1, v2, v3}, Lgel;->a(Ljava/lang/String;Lgem;)V

    .line 26
    :cond_1
    return v0

    .line 18
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 6

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lgng;->b:Ljava/lang/Object;

    .line 78
    :try_start_0
    iget-object v0, p0, Lgng;->d:Landroid/content/Context;

    iget-object v1, p0, Lgng;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    const-string v1, "RcsClientLib"

    iget-object v2, p0, Lgng;->a:Ljava/lang/Class;

    .line 82
    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected error when trying to unbind "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-static {v1, v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lgng;->f:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lgng;->e:Lgel;

    invoke-interface {v0, p1}, Lgel;->b(Ljava/lang/String;)V

    .line 61
    :cond_0
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 33
    iget-object v1, p0, Lgng;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 34
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lgng;->f:Z

    .line 35
    invoke-virtual {p0}, Lgng;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lgng;->d()V

    .line 37
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getServiceListener()Lgel;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgng;->e:Lgel;

    return-object v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 38
    iget-object v1, p0, Lgng;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 39
    :try_start_0
    iget-object v0, p0, Lgng;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x0

    monitor-exit v1

    .line 42
    :goto_0
    return v0

    .line 41
    :cond_0
    iget-object v0, p0, Lgng;->b:Ljava/lang/Object;

    check-cast v0, Landroid/os/IInterface;

    .line 42
    invoke-interface {v0}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0}, Landroid/os/IBinder;->pingBinder()Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setServiceListener(Lgel;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lgng;->e:Lgel;

    .line 46
    return-void
.end method
