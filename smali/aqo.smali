.class public final Laqo;
.super Landroid/app/ListFragment;
.source "PG"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lbub;


# instance fields
.field public a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Laql;

.field private f:Lbua;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    return-void
.end method

.method private final b()V
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Laqo;->f:Lbua;

    .line 132
    iget-boolean v0, v0, Lbua;->a:Z

    .line 133
    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Laqo;->d:Landroid/widget/TextView;

    const v1, 0x7f110053

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 135
    :cond_2
    iget-object v0, p0, Laqo;->d:Landroid/widget/TextView;

    const v1, 0x7f110052

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Laqo;->b()V

    .line 128
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 3
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 5
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 6
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const v2, 0x7f040023

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 7
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const v2, 0x7f040021

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 8
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e00eb

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 9
    new-instance v1, Lbkg;

    invoke-virtual {p0}, Laqo;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 10
    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    .line 11
    iput-object v2, v1, Lbkg;->e:Ljava/lang/Character;

    .line 13
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c0009

    invoke-static {v2, v3}, Lid;->c(Landroid/content/Context;I)I

    move-result v2

    .line 15
    iput v2, v1, Lbkg;->d:I

    .line 16
    const/4 v2, 0x1

    .line 17
    iput-boolean v2, v1, Lbkg;->c:Z

    .line 18
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    iget-object v0, p0, Laqo;->e:Laql;

    if-nez v0, :cond_0

    .line 21
    invoke-virtual {p0}, Laqo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 23
    new-instance v2, Laql;

    new-instance v3, Lbmm;

    .line 24
    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lbmm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 25
    invoke-static {v0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, Laql;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;Lbmm;Lbfo;)V

    .line 26
    iput-object v2, p0, Laqo;->e:Laql;

    .line 27
    :cond_0
    iget-object v0, p0, Laqo;->e:Laql;

    invoke-virtual {p0, v0}, Laqo;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 28
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqo;->c:Landroid/widget/TextView;

    .line 29
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e6

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqo;->b:Landroid/view/View;

    .line 30
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e9

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqo;->a:Landroid/view/View;

    .line 33
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqo;->g:Landroid/view/View;

    .line 34
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e00ed

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laqo;->h:Landroid/view/View;

    .line 35
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e5

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00ea

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e00de

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laqo;->d:Landroid/widget/TextView;

    .line 39
    new-instance v0, Lbua;

    invoke-virtual {p0}, Laqo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbua;-><init>(Landroid/content/Context;Lbub;)V

    iput-object v0, p0, Laqo;->f:Lbua;

    .line 40
    iget-object v0, p0, Laqo;->f:Lbua;

    invoke-virtual {v0}, Lbua;->a()V

    .line 41
    invoke-direct {p0}, Laqo;->b()V

    .line 42
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0e00f3

    const/4 v3, 0x0

    .line 88
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;

    .line 89
    if-nez v0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 92
    const v2, 0x7f0e00ea

    if-ne v1, v2, :cond_3

    .line 95
    invoke-virtual {v0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "blocked_search"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lard;

    .line 96
    if-nez v1, :cond_2

    .line 97
    new-instance v1, Lard;

    invoke-direct {v1}, Lard;-><init>()V

    .line 98
    invoke-virtual {v1, v3}, Lard;->setHasOptionsMenu(Z)V

    .line 99
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lard;->e(Z)V

    .line 100
    invoke-virtual {v1, v3}, Lard;->g(Z)V

    .line 101
    :cond_2
    invoke-virtual {v0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 102
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "blocked_search"

    .line 103
    invoke-virtual {v2, v4, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 104
    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 106
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lblb$a;->t:Lblb$a;

    invoke-interface {v1, v2, v0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto :goto_0

    .line 108
    :cond_3
    const v2, 0x7f0e00e5

    if-ne v1, v2, :cond_5

    .line 111
    invoke-virtual {v0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "view_numbers_to_import"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Laqt;

    .line 112
    if-nez v1, :cond_4

    .line 113
    new-instance v1, Laqt;

    invoke-direct {v1}, Laqt;-><init>()V

    .line 114
    :cond_4
    invoke-virtual {v0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "view_numbers_to_import"

    .line 116
    invoke-virtual {v0, v4, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 117
    invoke-virtual {v0, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 120
    :cond_5
    const v2, 0x7f0e00e4

    if-ne v1, v2, :cond_6

    .line 121
    new-instance v1, Laqp;

    invoke-direct {v1, p0}, Laqp;-><init>(Laqo;)V

    invoke-static {v0, v1}, Laxd;->a(Landroid/content/Context;Laxj;)V

    goto/16 :goto_0

    .line 122
    :cond_6
    const v2, 0x7f0e00e9

    if-ne v1, v2, :cond_0

    .line 123
    invoke-virtual {p1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 124
    new-instance v1, Lawo;

    invoke-virtual {p0}, Laqo;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lawo;-><init>(Landroid/content/Context;)V

    new-instance v2, Laqq;

    invoke-direct {v2, p0, v0}, Laqq;-><init>(Laqo;Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;)V

    .line 125
    invoke-virtual {v1, v2}, Lawo;->a(Lawp;)Z

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Laqo;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 48
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 82
    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "country_iso"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "number"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "normalized_number"

    aput-object v1, v3, v0

    .line 83
    new-instance v0, Landroid/content/CursorLoader;

    .line 84
    invoke-virtual {p0}, Laqo;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lbgt;->a:Landroid/net/Uri;

    const-string v4, "type=1"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 81
    const v0, 0x7f040022

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Laqo;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 44
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 45
    return-void
.end method

.method public final synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 137
    check-cast p2, Landroid/database/Cursor;

    .line 138
    iget-object v0, p0, Laqo;->e:Laql;

    invoke-virtual {v0, p2}, Laql;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 139
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 140
    :cond_0
    iget-object v0, p0, Laqo;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Laqo;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onLoaderReset(Landroid/content/Loader;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Laqo;->e:Laql;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laql;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 87
    return-void
.end method

.method public final onResume()V
    .locals 9

    .prologue
    const v8, 0x7f0e00ea

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 49
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 50
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Luh;

    .line 51
    invoke-virtual {v0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 53
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 54
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0c0071

    invoke-static {v2, v3}, Lid;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 55
    invoke-virtual {v0, v1}, Ltv;->b(Landroid/graphics/drawable/Drawable;)V

    .line 56
    invoke-virtual {v0, v5}, Ltv;->d(Z)V

    .line 57
    invoke-virtual {v0, v6}, Ltv;->b(Z)V

    .line 58
    invoke-virtual {v0, v6}, Ltv;->a(Z)V

    .line 59
    invoke-virtual {v0, v6}, Ltv;->c(Z)V

    .line 60
    const v1, 0x7f1101ea

    invoke-virtual {v0, v1}, Ltv;->b(I)V

    .line 61
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Laqo;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Laqo;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 65
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Laqo;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Laqo;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 68
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    invoke-virtual {p0}, Laqo;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f0e00e5

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Laqo;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Laqo;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    :goto_0
    invoke-static {}, Lapw;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 76
    invoke-virtual {p0}, Laqo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Laxd;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Laqo;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 79
    :goto_1
    iget-object v0, p0, Laqo;->f:Lbua;

    invoke-virtual {v0}, Lbua;->a()V

    .line 80
    return-void

    .line 73
    :cond_0
    invoke-virtual {p0}, Laqo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Laxh;

    invoke-direct {v1, p0}, Laxh;-><init>(Laqo;)V

    .line 74
    invoke-static {v0, v1}, Laxd;->a(Landroid/content/Context;Laxh;)V

    goto :goto_0

    .line 78
    :cond_1
    iget-object v0, p0, Laqo;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
