.class final Lcxe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcun;
.implements Lcvl;
.implements Lcvm;


# instance fields
.field private a:Lcvn;

.field private b:Lcvm;

.field private c:I

.field private d:Lcvj;

.field private e:Ljava/lang/Object;

.field private volatile f:Ldas;

.field private g:Lcvk;


# direct methods
.method public constructor <init>(Lcvn;Lcvm;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcxe;->a:Lcvn;

    .line 3
    iput-object p2, p0, Lcxe;->b:Lcvm;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcxe;->b:Lcvm;

    iget-object v1, p0, Lcxe;->f:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    invoke-interface {v1}, Lcum;->c()Lctw;

    move-result-object v1

    invoke-interface {v0, p1, p2, p3, v1}, Lcvm;->a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V

    .line 73
    return-void
.end method

.method public final a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V
    .locals 6

    .prologue
    .line 70
    iget-object v0, p0, Lcxe;->b:Lcvm;

    iget-object v1, p0, Lcxe;->f:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    invoke-interface {v1}, Lcum;->c()Lctw;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcvm;->a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V

    .line 71
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcxe;->b:Lcvm;

    iget-object v1, p0, Lcxe;->g:Lcvk;

    iget-object v2, p0, Lcxe;->f:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    iget-object v3, p0, Lcxe;->f:Ldas;

    iget-object v3, v3, Ldas;->c:Lcum;

    invoke-interface {v3}, Lcum;->c()Lctw;

    move-result-object v3

    invoke-interface {v0, v1, p1, v2, v3}, Lcvm;->a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V

    .line 68
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lcxe;->a:Lcvn;

    .line 58
    iget-object v0, v0, Lcvn;->p:Lcvx;

    .line 60
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcxe;->f:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    invoke-interface {v1}, Lcum;->c()Lctw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcvx;->a(Lctw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iput-object p1, p0, Lcxe;->e:Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lcxe;->b:Lcvm;

    invoke-interface {v0}, Lcvm;->c()V

    .line 66
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcxe;->b:Lcvm;

    iget-object v1, p0, Lcxe;->f:Ldas;

    iget-object v1, v1, Ldas;->a:Lcud;

    iget-object v2, p0, Lcxe;->f:Ldas;

    iget-object v3, v2, Ldas;->c:Lcum;

    iget-object v2, p0, Lcxe;->f:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    .line 64
    invoke-interface {v2}, Lcum;->c()Lctw;

    move-result-object v4

    iget-object v5, p0, Lcxe;->g:Lcvk;

    move-object v2, p1

    .line 65
    invoke-interface/range {v0 .. v5}, Lcvm;->a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 5
    iget-object v0, p0, Lcxe;->e:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lcxe;->e:Ljava/lang/Object;

    .line 7
    iput-object v10, p0, Lcxe;->e:Ljava/lang/Object;

    .line 9
    invoke-static {}, Ldhs;->a()J

    move-result-wide v4

    .line 10
    :try_start_0
    iget-object v3, p0, Lcxe;->a:Lcvn;

    .line 11
    iget-object v3, v3, Lcvn;->c:Lcsy;

    .line 12
    iget-object v3, v3, Lcsy;->c:Lcta;

    .line 14
    iget-object v3, v3, Lcta;->a:Ldfx;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v3, v6}, Ldfx;->a(Ljava/lang/Class;)Lctz;

    move-result-object v3

    .line 15
    if-eqz v3, :cond_2

    .line 19
    new-instance v6, Lcyd;

    iget-object v7, p0, Lcxe;->a:Lcvn;

    .line 21
    iget-object v7, v7, Lcvn;->i:Lcuh;

    .line 22
    invoke-direct {v6, v3, v0, v7}, Lcyd;-><init>(Lctz;Ljava/lang/Object;Lcuh;)V

    .line 23
    new-instance v7, Lcvk;

    iget-object v8, p0, Lcxe;->f:Ldas;

    iget-object v8, v8, Ldas;->a:Lcud;

    iget-object v9, p0, Lcxe;->a:Lcvn;

    .line 24
    iget-object v9, v9, Lcvn;->n:Lcud;

    .line 25
    invoke-direct {v7, v8, v9}, Lcvk;-><init>(Lcud;Lcud;)V

    iput-object v7, p0, Lcxe;->g:Lcvk;

    .line 26
    iget-object v7, p0, Lcxe;->a:Lcvn;

    invoke-virtual {v7}, Lcvn;->a()Lcyb;

    move-result-object v7

    iget-object v8, p0, Lcxe;->g:Lcvk;

    invoke-interface {v7, v8, v6}, Lcyb;->a(Lcud;Lcyd;)V

    .line 27
    const-string v6, "SourceGenerator"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 28
    iget-object v6, p0, Lcxe;->g:Lcvk;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 29
    invoke-static {v4, v5}, Ldhs;->a(J)D

    move-result-wide v4

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x5f

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Finished encoding source to cache, key: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", encoder: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", duration: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :cond_0
    iget-object v0, p0, Lcxe;->f:Ldas;

    iget-object v0, v0, Ldas;->c:Lcum;

    invoke-interface {v0}, Lcum;->a()V

    .line 33
    new-instance v0, Lcvj;

    iget-object v3, p0, Lcxe;->f:Ldas;

    iget-object v3, v3, Ldas;->a:Lcud;

    .line 34
    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcxe;->a:Lcvn;

    invoke-direct {v0, v3, v4, p0}, Lcvj;-><init>(Ljava/util/List;Lcvn;Lcvm;)V

    iput-object v0, p0, Lcxe;->d:Lcvj;

    .line 35
    :cond_1
    iget-object v0, p0, Lcxe;->d:Lcvj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcxe;->d:Lcvj;

    invoke-virtual {v0}, Lcvj;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 52
    :goto_0
    return v0

    .line 17
    :cond_2
    :try_start_1
    new-instance v1, Lip$b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lip$b;-><init>(Ljava/lang/Class;B)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 32
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcxe;->f:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    invoke-interface {v1}, Lcum;->a()V

    throw v0

    .line 37
    :cond_3
    iput-object v10, p0, Lcxe;->d:Lcvj;

    .line 38
    iput-object v10, p0, Lcxe;->f:Ldas;

    .line 40
    :cond_4
    :goto_1
    if-nez v2, :cond_6

    .line 41
    iget v0, p0, Lcxe;->c:I

    iget-object v3, p0, Lcxe;->a:Lcvn;

    invoke-virtual {v3}, Lcvn;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 42
    iget-object v0, p0, Lcxe;->a:Lcvn;

    invoke-virtual {v0}, Lcvn;->b()Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lcxe;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcxe;->c:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldas;

    iput-object v0, p0, Lcxe;->f:Ldas;

    .line 43
    iget-object v0, p0, Lcxe;->f:Ldas;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcxe;->a:Lcvn;

    .line 45
    iget-object v0, v0, Lcvn;->p:Lcvx;

    .line 46
    iget-object v3, p0, Lcxe;->f:Ldas;

    iget-object v3, v3, Ldas;->c:Lcum;

    invoke-interface {v3}, Lcum;->c()Lctw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcvx;->a(Lctw;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcxe;->a:Lcvn;

    iget-object v3, p0, Lcxe;->f:Ldas;

    iget-object v3, v3, Ldas;->c:Lcum;

    .line 47
    invoke-interface {v3}, Lcum;->d()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcvn;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 49
    :cond_5
    iget-object v0, p0, Lcxe;->f:Ldas;

    iget-object v0, v0, Ldas;->c:Lcum;

    iget-object v2, p0, Lcxe;->a:Lcvn;

    .line 50
    iget-object v2, v2, Lcvn;->o:Lcsz;

    .line 51
    invoke-interface {v0, v2, p0}, Lcum;->a(Lcsz;Lcun;)V

    move v2, v1

    goto :goto_1

    :cond_6
    move v0, v2

    .line 52
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcxe;->f:Ldas;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    iget-object v0, v0, Ldas;->c:Lcum;

    invoke-interface {v0}, Lcum;->b()V

    .line 56
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
