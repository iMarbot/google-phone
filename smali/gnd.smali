.class public final Lgnd;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnd;


# instance fields
.field public action:Ljava/lang/Integer;

.field public announcementId:Ljava/lang/String;

.field public announcementType:Ljava/lang/Integer;

.field public body:Ljava/lang/String;

.field public hangoutId:Ljava/lang/String;

.field public lifeTime:Ljava/lang/Integer;

.field public locale:Ljava/lang/String;

.field public recipientId:[Ljava/lang/String;

.field public senderId:Ljava/lang/String;

.field public subjectId:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lhft;-><init>()V

    .line 22
    invoke-virtual {p0}, Lgnd;->clear()Lgnd;

    .line 23
    return-void
.end method

.method public static checkActionOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Action"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkActionOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgnd;->checkActionOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static checkLifeTimeOrThrow(I)I
    .locals 3

    .prologue
    .line 8
    packed-switch p0, :pswitch_data_0

    .line 10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum LifeTime"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :pswitch_0
    return p0

    .line 8
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkLifeTimeOrThrow([I)[I
    .locals 3

    .prologue
    .line 11
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 12
    invoke-static {v2}, Lgnd;->checkLifeTimeOrThrow(I)I

    .line 13
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 14
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgnd;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lgnd;->_emptyArray:[Lgnd;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lgnd;->_emptyArray:[Lgnd;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lgnd;

    sput-object v0, Lgnd;->_emptyArray:[Lgnd;

    .line 19
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lgnd;->_emptyArray:[Lgnd;

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnd;
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lgnd;

    invoke-direct {v0}, Lgnd;-><init>()V

    invoke-virtual {v0, p0}, Lgnd;->mergeFrom(Lhfp;)Lgnd;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnd;
    .locals 1

    .prologue
    .line 169
    new-instance v0, Lgnd;

    invoke-direct {v0}, Lgnd;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnd;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnd;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    iput-object v1, p0, Lgnd;->hangoutId:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lgnd;->announcementId:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lgnd;->announcementType:Ljava/lang/Integer;

    .line 27
    iput-object v1, p0, Lgnd;->senderId:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lgnd;->subjectId:Ljava/lang/String;

    .line 29
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgnd;->recipientId:[Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lgnd;->locale:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lgnd;->action:Ljava/lang/Integer;

    .line 32
    iput-object v1, p0, Lgnd;->title:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lgnd;->body:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lgnd;->lifeTime:Ljava/lang/Integer;

    .line 35
    iput-object v1, p0, Lgnd;->unknownFieldData:Lhfv;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lgnd;->cachedSize:I

    .line 37
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 67
    iget-object v2, p0, Lgnd;->hangoutId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 68
    const/4 v2, 0x1

    iget-object v3, p0, Lgnd;->hangoutId:Ljava/lang/String;

    .line 69
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 70
    :cond_0
    iget-object v2, p0, Lgnd;->announcementId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 71
    const/4 v2, 0x2

    iget-object v3, p0, Lgnd;->announcementId:Ljava/lang/String;

    .line 72
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_1
    iget-object v2, p0, Lgnd;->announcementType:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 74
    const/4 v2, 0x3

    iget-object v3, p0, Lgnd;->announcementType:Ljava/lang/Integer;

    .line 75
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 76
    :cond_2
    iget-object v2, p0, Lgnd;->senderId:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 77
    const/4 v2, 0x4

    iget-object v3, p0, Lgnd;->senderId:Ljava/lang/String;

    .line 78
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 79
    :cond_3
    iget-object v2, p0, Lgnd;->subjectId:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 80
    const/4 v2, 0x5

    iget-object v3, p0, Lgnd;->subjectId:Ljava/lang/String;

    .line 81
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_4
    iget-object v2, p0, Lgnd;->recipientId:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgnd;->recipientId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 85
    :goto_0
    iget-object v4, p0, Lgnd;->recipientId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 86
    iget-object v4, p0, Lgnd;->recipientId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 87
    if-eqz v4, :cond_5

    .line 88
    add-int/lit8 v3, v3, 0x1

    .line 90
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 91
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    :cond_6
    add-int/2addr v0, v2

    .line 93
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 94
    :cond_7
    iget-object v1, p0, Lgnd;->locale:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 95
    const/4 v1, 0x7

    iget-object v2, p0, Lgnd;->locale:Ljava/lang/String;

    .line 96
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_8
    iget-object v1, p0, Lgnd;->action:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 98
    const/16 v1, 0x8

    iget-object v2, p0, Lgnd;->action:Ljava/lang/Integer;

    .line 99
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_9
    iget-object v1, p0, Lgnd;->title:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 101
    const/16 v1, 0x9

    iget-object v2, p0, Lgnd;->title:Ljava/lang/String;

    .line 102
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_a
    iget-object v1, p0, Lgnd;->body:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 104
    const/16 v1, 0xa

    iget-object v2, p0, Lgnd;->body:Ljava/lang/String;

    .line 105
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_b
    iget-object v1, p0, Lgnd;->lifeTime:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 107
    const/16 v1, 0xb

    iget-object v2, p0, Lgnd;->lifeTime:Ljava/lang/Integer;

    .line 108
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_c
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 111
    sparse-switch v0, :sswitch_data_0

    .line 113
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :sswitch_0
    return-object p0

    .line 115
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 117
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->announcementId:Ljava/lang/String;

    goto :goto_0

    .line 119
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 121
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 122
    invoke-static {v3}, Lgnc;->checkAnnouncementTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgnd;->announcementType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 126
    invoke-virtual {p0, p1, v0}, Lgnd;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->senderId:Ljava/lang/String;

    goto :goto_0

    .line 130
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->subjectId:Ljava/lang/String;

    goto :goto_0

    .line 132
    :sswitch_6
    const/16 v0, 0x32

    .line 133
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 134
    iget-object v0, p0, Lgnd;->recipientId:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 135
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 136
    if-eqz v0, :cond_1

    .line 137
    iget-object v3, p0, Lgnd;->recipientId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 139
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 140
    invoke-virtual {p1}, Lhfp;->a()I

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 134
    :cond_2
    iget-object v0, p0, Lgnd;->recipientId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    .line 142
    :cond_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 143
    iput-object v2, p0, Lgnd;->recipientId:[Ljava/lang/String;

    goto :goto_0

    .line 145
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->locale:Ljava/lang/String;

    goto :goto_0

    .line 147
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 149
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 150
    invoke-static {v3}, Lgnd;->checkActionOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgnd;->action:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 153
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 154
    invoke-virtual {p0, p1, v0}, Lgnd;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 156
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 158
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnd;->body:Ljava/lang/String;

    goto/16 :goto_0

    .line 160
    :sswitch_b
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 162
    :try_start_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 163
    invoke-static {v3}, Lgnd;->checkLifeTimeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgnd;->lifeTime:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 166
    :catch_2
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 167
    invoke-virtual {p0, p1, v0}, Lgnd;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lgnd;->mergeFrom(Lhfp;)Lgnd;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lgnd;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-object v1, p0, Lgnd;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 40
    :cond_0
    iget-object v0, p0, Lgnd;->announcementId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x2

    iget-object v1, p0, Lgnd;->announcementId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 42
    :cond_1
    iget-object v0, p0, Lgnd;->announcementType:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 43
    const/4 v0, 0x3

    iget-object v1, p0, Lgnd;->announcementType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 44
    :cond_2
    iget-object v0, p0, Lgnd;->senderId:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 45
    const/4 v0, 0x4

    iget-object v1, p0, Lgnd;->senderId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 46
    :cond_3
    iget-object v0, p0, Lgnd;->subjectId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 47
    const/4 v0, 0x5

    iget-object v1, p0, Lgnd;->subjectId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 48
    :cond_4
    iget-object v0, p0, Lgnd;->recipientId:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgnd;->recipientId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 49
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgnd;->recipientId:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 50
    iget-object v1, p0, Lgnd;->recipientId:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 51
    if-eqz v1, :cond_5

    .line 52
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 53
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_6
    iget-object v0, p0, Lgnd;->locale:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 55
    const/4 v0, 0x7

    iget-object v1, p0, Lgnd;->locale:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 56
    :cond_7
    iget-object v0, p0, Lgnd;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 57
    const/16 v0, 0x8

    iget-object v1, p0, Lgnd;->action:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 58
    :cond_8
    iget-object v0, p0, Lgnd;->title:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 59
    const/16 v0, 0x9

    iget-object v1, p0, Lgnd;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 60
    :cond_9
    iget-object v0, p0, Lgnd;->body:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 61
    const/16 v0, 0xa

    iget-object v1, p0, Lgnd;->body:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 62
    :cond_a
    iget-object v0, p0, Lgnd;->lifeTime:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 63
    const/16 v0, 0xb

    iget-object v1, p0, Lgnd;->lifeTime:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 64
    :cond_b
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 65
    return-void
.end method
