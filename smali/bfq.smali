.class public final Lbfq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbfq;

.field public static final b:Lbfq;

.field public static final c:Lbfq;

.field public static final d:Lbfq;


# instance fields
.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:F

.field public i:F

.field public j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lbfq;

    invoke-direct {v0}, Lbfq;-><init>()V

    sput-object v0, Lbfq;->a:Lbfq;

    .line 24
    new-instance v0, Lbfq;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v4, v1}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    sput-object v0, Lbfq;->b:Lbfq;

    .line 25
    new-instance v0, Lbfq;

    invoke-direct {v0, v2, v2, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Lbfq;->c:Lbfq;

    .line 26
    new-instance v0, Lbfq;

    invoke-direct {v0, v2, v2, v4, v3}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    sput-object v0, Lbfq;->d:Lbfq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput v0, p0, Lbfq;->g:I

    .line 3
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbfq;->h:F

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lbfq;->i:F

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfq;->j:Z

    .line 6
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IFFZ)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Lbfq;->g:I

    .line 13
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbfq;->h:F

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lbfq;->i:F

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfq;->j:Z

    .line 16
    iput-object p1, p0, Lbfq;->e:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lbfq;->f:Ljava/lang/String;

    .line 18
    iput p3, p0, Lbfq;->g:I

    .line 19
    iput p4, p0, Lbfq;->h:F

    .line 20
    iput p5, p0, Lbfq;->i:F

    .line 21
    iput-boolean p6, p0, Lbfq;->j:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 7

    .prologue
    .line 9
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IFFZ)V

    .line 10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 7
    const/4 v3, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;IFFZ)V

    .line 8
    return-void
.end method
