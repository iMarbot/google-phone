.class public final Lbpa;
.super Landroid/support/v7/widget/RecyclerView$r;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final p:Landroid/content/Context;

.field public final q:Landroid/widget/TextView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/QuickContactBadge;

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$r;-><init>(Landroid/view/View;)V

    .line 2
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3
    const v0, 0x7f0e00d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lbpa;->s:Landroid/widget/QuickContactBadge;

    .line 4
    const v0, 0x7f0e0255

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbpa;->q:Landroid/widget/TextView;

    .line 5
    const v0, 0x7f0e0256

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbpa;->r:Landroid/widget/TextView;

    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbpa;->p:Landroid/content/Context;

    .line 7
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 18
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 19
    if-nez v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 20
    const-string v0, ""

    .line 21
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lboc;)Z
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v0, 0x1

    .line 8
    invoke-interface {p0}, Lboc;->getPosition()I

    move-result v1

    .line 9
    invoke-interface {p0, v4}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 10
    add-int/lit8 v3, v1, -0x1

    invoke-interface {p0, v3}, Lboc;->moveToPosition(I)Z

    .line 11
    invoke-interface {p0}, Lboc;->a()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {p0}, Lboc;->isBeforeFirst()Z

    move-result v3

    if-nez v3, :cond_1

    .line 12
    invoke-interface {p0, v4}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 13
    invoke-interface {p0, v1}, Lboc;->moveToPosition(I)Z

    .line 14
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 16
    :goto_0
    return v0

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 15
    :cond_1
    invoke-interface {p0, v1}, Lboc;->moveToPosition(I)Z

    goto :goto_0
.end method

.method public static b(Lboc;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lboc;->getLong(I)J

    move-result-wide v0

    .line 23
    const/4 v2, 0x7

    invoke-interface {p0, v2}, Lboc;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    .line 26
    invoke-interface {p0}, Lboc;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 27
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 29
    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lbpa;->p:Landroid/content/Context;

    new-instance v1, Lbbh;

    iget-object v2, p0, Lbpa;->t:Ljava/lang/String;

    sget-object v3, Lbbf$a;->g:Lbbf$a;

    invoke-direct {v1, v2, v3}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    invoke-static {v0, v1}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 31
    return-void
.end method
