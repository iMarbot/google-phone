.class public final Lgkz;
.super Lhft;
.source "PG"


# instance fields
.field public a:Lgkt;

.field private b:Lglt;

.field private c:Ljava/lang/Boolean;

.field private d:[Lgkg;

.field private e:Lgkq;

.field private f:Lgkk;

.field private g:Lgkl;

.field private h:Lglo;

.field private i:[Lgki;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Lgkm;

.field private m:Lglw;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Boolean;

.field private p:Ljava/lang/Integer;

.field private q:[Lglm;

.field private r:Ljava/lang/Integer;

.field private s:Lgwx;

.field private t:[Lgkw;

.field private u:Lgmc;

.field private v:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgkz;->b:Lglt;

    .line 4
    iput-object v1, p0, Lgkz;->a:Lgkt;

    .line 5
    iput-object v1, p0, Lgkz;->c:Ljava/lang/Boolean;

    .line 6
    invoke-static {}, Lgkg;->a()[Lgkg;

    move-result-object v0

    iput-object v0, p0, Lgkz;->d:[Lgkg;

    .line 7
    iput-object v1, p0, Lgkz;->e:Lgkq;

    .line 8
    iput-object v1, p0, Lgkz;->f:Lgkk;

    .line 9
    iput-object v1, p0, Lgkz;->g:Lgkl;

    .line 10
    iput-object v1, p0, Lgkz;->h:Lglo;

    .line 11
    invoke-static {}, Lgki;->a()[Lgki;

    move-result-object v0

    iput-object v0, p0, Lgkz;->i:[Lgki;

    .line 12
    iput-object v1, p0, Lgkz;->j:Ljava/lang/Boolean;

    .line 13
    iput-object v1, p0, Lgkz;->k:Ljava/lang/Boolean;

    .line 14
    iput-object v1, p0, Lgkz;->l:Lgkm;

    .line 15
    iput-object v1, p0, Lgkz;->m:Lglw;

    .line 16
    iput-object v1, p0, Lgkz;->n:Ljava/lang/Boolean;

    .line 17
    iput-object v1, p0, Lgkz;->o:Ljava/lang/Boolean;

    .line 18
    iput-object v1, p0, Lgkz;->p:Ljava/lang/Integer;

    .line 19
    invoke-static {}, Lglm;->a()[Lglm;

    move-result-object v0

    iput-object v0, p0, Lgkz;->q:[Lglm;

    .line 20
    iput-object v1, p0, Lgkz;->r:Ljava/lang/Integer;

    .line 21
    iput-object v1, p0, Lgkz;->s:Lgwx;

    .line 22
    invoke-static {}, Lgkw;->a()[Lgkw;

    move-result-object v0

    iput-object v0, p0, Lgkz;->t:[Lgkw;

    .line 23
    iput-object v1, p0, Lgkz;->u:Lgmc;

    .line 24
    iput-object v1, p0, Lgkz;->v:Ljava/lang/Integer;

    .line 25
    iput-object v1, p0, Lgkz;->unknownFieldData:Lhfv;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lgkz;->cachedSize:I

    .line 27
    return-void
.end method

.method private a(Lhfp;)Lgkz;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 185
    sparse-switch v0, :sswitch_data_0

    .line 187
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    :sswitch_0
    return-object p0

    .line 189
    :sswitch_1
    iget-object v0, p0, Lgkz;->b:Lglt;

    if-nez v0, :cond_1

    .line 190
    new-instance v0, Lglt;

    invoke-direct {v0}, Lglt;-><init>()V

    iput-object v0, p0, Lgkz;->b:Lglt;

    .line 191
    :cond_1
    iget-object v0, p0, Lgkz;->b:Lglt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 193
    :sswitch_2
    iget-object v0, p0, Lgkz;->a:Lgkt;

    if-nez v0, :cond_2

    .line 194
    new-instance v0, Lgkt;

    invoke-direct {v0}, Lgkt;-><init>()V

    iput-object v0, p0, Lgkz;->a:Lgkt;

    .line 195
    :cond_2
    iget-object v0, p0, Lgkz;->a:Lgkt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 197
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkz;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 199
    :sswitch_4
    const/16 v0, 0x22

    .line 200
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 201
    iget-object v0, p0, Lgkz;->d:[Lgkg;

    if-nez v0, :cond_4

    move v0, v1

    .line 202
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgkg;

    .line 203
    if-eqz v0, :cond_3

    .line 204
    iget-object v3, p0, Lgkz;->d:[Lgkg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 206
    new-instance v3, Lgkg;

    invoke-direct {v3}, Lgkg;-><init>()V

    aput-object v3, v2, v0

    .line 207
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 208
    invoke-virtual {p1}, Lhfp;->a()I

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 201
    :cond_4
    iget-object v0, p0, Lgkz;->d:[Lgkg;

    array-length v0, v0

    goto :goto_1

    .line 210
    :cond_5
    new-instance v3, Lgkg;

    invoke-direct {v3}, Lgkg;-><init>()V

    aput-object v3, v2, v0

    .line 211
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 212
    iput-object v2, p0, Lgkz;->d:[Lgkg;

    goto :goto_0

    .line 214
    :sswitch_5
    iget-object v0, p0, Lgkz;->e:Lgkq;

    if-nez v0, :cond_6

    .line 215
    new-instance v0, Lgkq;

    invoke-direct {v0}, Lgkq;-><init>()V

    iput-object v0, p0, Lgkz;->e:Lgkq;

    .line 216
    :cond_6
    iget-object v0, p0, Lgkz;->e:Lgkq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 218
    :sswitch_6
    iget-object v0, p0, Lgkz;->f:Lgkk;

    if-nez v0, :cond_7

    .line 219
    new-instance v0, Lgkk;

    invoke-direct {v0}, Lgkk;-><init>()V

    iput-object v0, p0, Lgkz;->f:Lgkk;

    .line 220
    :cond_7
    iget-object v0, p0, Lgkz;->f:Lgkk;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 222
    :sswitch_7
    iget-object v0, p0, Lgkz;->h:Lglo;

    if-nez v0, :cond_8

    .line 223
    new-instance v0, Lglo;

    invoke-direct {v0}, Lglo;-><init>()V

    iput-object v0, p0, Lgkz;->h:Lglo;

    .line 224
    :cond_8
    iget-object v0, p0, Lgkz;->h:Lglo;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 226
    :sswitch_8
    const/16 v0, 0x42

    .line 227
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 228
    iget-object v0, p0, Lgkz;->i:[Lgki;

    if-nez v0, :cond_a

    move v0, v1

    .line 229
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgki;

    .line 230
    if-eqz v0, :cond_9

    .line 231
    iget-object v3, p0, Lgkz;->i:[Lgki;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 232
    :cond_9
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_b

    .line 233
    new-instance v3, Lgki;

    invoke-direct {v3}, Lgki;-><init>()V

    aput-object v3, v2, v0

    .line 234
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 235
    invoke-virtual {p1}, Lhfp;->a()I

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 228
    :cond_a
    iget-object v0, p0, Lgkz;->i:[Lgki;

    array-length v0, v0

    goto :goto_3

    .line 237
    :cond_b
    new-instance v3, Lgki;

    invoke-direct {v3}, Lgki;-><init>()V

    aput-object v3, v2, v0

    .line 238
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 239
    iput-object v2, p0, Lgkz;->i:[Lgki;

    goto/16 :goto_0

    .line 241
    :sswitch_9
    iget-object v0, p0, Lgkz;->g:Lgkl;

    if-nez v0, :cond_c

    .line 242
    new-instance v0, Lgkl;

    invoke-direct {v0}, Lgkl;-><init>()V

    iput-object v0, p0, Lgkz;->g:Lgkl;

    .line 243
    :cond_c
    iget-object v0, p0, Lgkz;->g:Lgkl;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 245
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkz;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 247
    :sswitch_b
    iget-object v0, p0, Lgkz;->l:Lgkm;

    if-nez v0, :cond_d

    .line 248
    new-instance v0, Lgkm;

    invoke-direct {v0}, Lgkm;-><init>()V

    iput-object v0, p0, Lgkz;->l:Lgkm;

    .line 249
    :cond_d
    iget-object v0, p0, Lgkz;->l:Lgkm;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 251
    :sswitch_c
    iget-object v0, p0, Lgkz;->m:Lglw;

    if-nez v0, :cond_e

    .line 252
    new-instance v0, Lglw;

    invoke-direct {v0}, Lglw;-><init>()V

    iput-object v0, p0, Lgkz;->m:Lglw;

    .line 253
    :cond_e
    iget-object v0, p0, Lgkz;->m:Lglw;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 255
    :sswitch_d
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkz;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 257
    :sswitch_e
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkz;->n:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 259
    :sswitch_f
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgkz;->o:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 261
    :sswitch_10
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 263
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 264
    invoke-static {v3}, Lhcw;->r(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgkz;->p:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 267
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 268
    invoke-virtual {p0, p1, v0}, Lgkz;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 270
    :sswitch_11
    const/16 v0, 0x8a

    .line 271
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 272
    iget-object v0, p0, Lgkz;->q:[Lglm;

    if-nez v0, :cond_10

    move v0, v1

    .line 273
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lglm;

    .line 274
    if-eqz v0, :cond_f

    .line 275
    iget-object v3, p0, Lgkz;->q:[Lglm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    :cond_f
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_11

    .line 277
    new-instance v3, Lglm;

    invoke-direct {v3}, Lglm;-><init>()V

    aput-object v3, v2, v0

    .line 278
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 279
    invoke-virtual {p1}, Lhfp;->a()I

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 272
    :cond_10
    iget-object v0, p0, Lgkz;->q:[Lglm;

    array-length v0, v0

    goto :goto_5

    .line 281
    :cond_11
    new-instance v3, Lglm;

    invoke-direct {v3}, Lglm;-><init>()V

    aput-object v3, v2, v0

    .line 282
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 283
    iput-object v2, p0, Lgkz;->q:[Lglm;

    goto/16 :goto_0

    .line 285
    :sswitch_12
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 287
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 289
    packed-switch v3, :pswitch_data_0

    .line 291
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const/16 v5, 0x2f

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " is not a valid enum AccountAgeGroup"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 295
    :catch_1
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 296
    invoke-virtual {p0, p1, v0}, Lgkz;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 292
    :pswitch_0
    :try_start_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgkz;->r:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 298
    :sswitch_13
    iget-object v0, p0, Lgkz;->s:Lgwx;

    if-nez v0, :cond_12

    .line 299
    new-instance v0, Lgwx;

    invoke-direct {v0}, Lgwx;-><init>()V

    iput-object v0, p0, Lgkz;->s:Lgwx;

    .line 300
    :cond_12
    iget-object v0, p0, Lgkz;->s:Lgwx;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 302
    :sswitch_14
    const/16 v0, 0xa2

    .line 303
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 304
    iget-object v0, p0, Lgkz;->t:[Lgkw;

    if-nez v0, :cond_14

    move v0, v1

    .line 305
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lgkw;

    .line 306
    if-eqz v0, :cond_13

    .line 307
    iget-object v3, p0, Lgkz;->t:[Lgkw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 308
    :cond_13
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    .line 309
    new-instance v3, Lgkw;

    invoke-direct {v3}, Lgkw;-><init>()V

    aput-object v3, v2, v0

    .line 310
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 311
    invoke-virtual {p1}, Lhfp;->a()I

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 304
    :cond_14
    iget-object v0, p0, Lgkz;->t:[Lgkw;

    array-length v0, v0

    goto :goto_7

    .line 313
    :cond_15
    new-instance v3, Lgkw;

    invoke-direct {v3}, Lgkw;-><init>()V

    aput-object v3, v2, v0

    .line 314
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 315
    iput-object v2, p0, Lgkz;->t:[Lgkw;

    goto/16 :goto_0

    .line 317
    :sswitch_15
    iget-object v0, p0, Lgkz;->u:Lgmc;

    if-nez v0, :cond_16

    .line 318
    new-instance v0, Lgmc;

    invoke-direct {v0}, Lgmc;-><init>()V

    iput-object v0, p0, Lgkz;->u:Lgmc;

    .line 319
    :cond_16
    iget-object v0, p0, Lgkz;->u:Lgmc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 321
    :sswitch_16
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 323
    :try_start_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 324
    invoke-static {v3}, Lhcw;->h(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgkz;->v:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 327
    :catch_2
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 328
    invoke-virtual {p0, p1, v0}, Lgkz;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 185
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x90 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xb2 -> :sswitch_15
        0xc0 -> :sswitch_16
    .end sparse-switch

    .line 289
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 91
    iget-object v2, p0, Lgkz;->b:Lglt;

    if-eqz v2, :cond_0

    .line 92
    const/4 v2, 0x1

    iget-object v3, p0, Lgkz;->b:Lglt;

    .line 93
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 94
    :cond_0
    iget-object v2, p0, Lgkz;->a:Lgkt;

    if-eqz v2, :cond_1

    .line 95
    const/4 v2, 0x2

    iget-object v3, p0, Lgkz;->a:Lgkt;

    .line 96
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_1
    iget-object v2, p0, Lgkz;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 98
    const/4 v2, 0x3

    iget-object v3, p0, Lgkz;->c:Ljava/lang/Boolean;

    .line 99
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 100
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 101
    add-int/2addr v0, v2

    .line 102
    :cond_2
    iget-object v2, p0, Lgkz;->d:[Lgkg;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lgkz;->d:[Lgkg;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 103
    :goto_0
    iget-object v3, p0, Lgkz;->d:[Lgkg;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 104
    iget-object v3, p0, Lgkz;->d:[Lgkg;

    aget-object v3, v3, v0

    .line 105
    if-eqz v3, :cond_3

    .line 106
    const/4 v4, 0x4

    .line 107
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 108
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 109
    :cond_5
    iget-object v2, p0, Lgkz;->e:Lgkq;

    if-eqz v2, :cond_6

    .line 110
    const/4 v2, 0x5

    iget-object v3, p0, Lgkz;->e:Lgkq;

    .line 111
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_6
    iget-object v2, p0, Lgkz;->f:Lgkk;

    if-eqz v2, :cond_7

    .line 113
    const/4 v2, 0x6

    iget-object v3, p0, Lgkz;->f:Lgkk;

    .line 114
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 115
    :cond_7
    iget-object v2, p0, Lgkz;->h:Lglo;

    if-eqz v2, :cond_8

    .line 116
    const/4 v2, 0x7

    iget-object v3, p0, Lgkz;->h:Lglo;

    .line 117
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 118
    :cond_8
    iget-object v2, p0, Lgkz;->i:[Lgki;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lgkz;->i:[Lgki;

    array-length v2, v2

    if-lez v2, :cond_b

    move v2, v0

    move v0, v1

    .line 119
    :goto_1
    iget-object v3, p0, Lgkz;->i:[Lgki;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 120
    iget-object v3, p0, Lgkz;->i:[Lgki;

    aget-object v3, v3, v0

    .line 121
    if-eqz v3, :cond_9

    .line 122
    const/16 v4, 0x8

    .line 123
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 124
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    move v0, v2

    .line 125
    :cond_b
    iget-object v2, p0, Lgkz;->g:Lgkl;

    if-eqz v2, :cond_c

    .line 126
    const/16 v2, 0x9

    iget-object v3, p0, Lgkz;->g:Lgkl;

    .line 127
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_c
    iget-object v2, p0, Lgkz;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    .line 129
    const/16 v2, 0xa

    iget-object v3, p0, Lgkz;->j:Ljava/lang/Boolean;

    .line 130
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 131
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 132
    add-int/2addr v0, v2

    .line 133
    :cond_d
    iget-object v2, p0, Lgkz;->l:Lgkm;

    if-eqz v2, :cond_e

    .line 134
    const/16 v2, 0xb

    iget-object v3, p0, Lgkz;->l:Lgkm;

    .line 135
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    :cond_e
    iget-object v2, p0, Lgkz;->m:Lglw;

    if-eqz v2, :cond_f

    .line 137
    const/16 v2, 0xc

    iget-object v3, p0, Lgkz;->m:Lglw;

    .line 138
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    :cond_f
    iget-object v2, p0, Lgkz;->k:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    .line 140
    const/16 v2, 0xd

    iget-object v3, p0, Lgkz;->k:Ljava/lang/Boolean;

    .line 141
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 142
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 143
    add-int/2addr v0, v2

    .line 144
    :cond_10
    iget-object v2, p0, Lgkz;->n:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    .line 145
    const/16 v2, 0xe

    iget-object v3, p0, Lgkz;->n:Ljava/lang/Boolean;

    .line 146
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 147
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 148
    add-int/2addr v0, v2

    .line 149
    :cond_11
    iget-object v2, p0, Lgkz;->o:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    .line 150
    const/16 v2, 0xf

    iget-object v3, p0, Lgkz;->o:Ljava/lang/Boolean;

    .line 151
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 152
    invoke-static {v2}, Lhfq;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 153
    add-int/2addr v0, v2

    .line 154
    :cond_12
    iget-object v2, p0, Lgkz;->p:Ljava/lang/Integer;

    if-eqz v2, :cond_13

    .line 155
    const/16 v2, 0x10

    iget-object v3, p0, Lgkz;->p:Ljava/lang/Integer;

    .line 156
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_13
    iget-object v2, p0, Lgkz;->q:[Lglm;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lgkz;->q:[Lglm;

    array-length v2, v2

    if-lez v2, :cond_16

    move v2, v0

    move v0, v1

    .line 158
    :goto_2
    iget-object v3, p0, Lgkz;->q:[Lglm;

    array-length v3, v3

    if-ge v0, v3, :cond_15

    .line 159
    iget-object v3, p0, Lgkz;->q:[Lglm;

    aget-object v3, v3, v0

    .line 160
    if-eqz v3, :cond_14

    .line 161
    const/16 v4, 0x11

    .line 162
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 163
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_15
    move v0, v2

    .line 164
    :cond_16
    iget-object v2, p0, Lgkz;->r:Ljava/lang/Integer;

    if-eqz v2, :cond_17

    .line 165
    const/16 v2, 0x12

    iget-object v3, p0, Lgkz;->r:Ljava/lang/Integer;

    .line 166
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 167
    :cond_17
    iget-object v2, p0, Lgkz;->s:Lgwx;

    if-eqz v2, :cond_18

    .line 168
    const/16 v2, 0x13

    iget-object v3, p0, Lgkz;->s:Lgwx;

    .line 169
    invoke-static {v2, v3}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 170
    :cond_18
    iget-object v2, p0, Lgkz;->t:[Lgkw;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lgkz;->t:[Lgkw;

    array-length v2, v2

    if-lez v2, :cond_1a

    .line 171
    :goto_3
    iget-object v2, p0, Lgkz;->t:[Lgkw;

    array-length v2, v2

    if-ge v1, v2, :cond_1a

    .line 172
    iget-object v2, p0, Lgkz;->t:[Lgkw;

    aget-object v2, v2, v1

    .line 173
    if-eqz v2, :cond_19

    .line 174
    const/16 v3, 0x14

    .line 175
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 176
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 177
    :cond_1a
    iget-object v1, p0, Lgkz;->u:Lgmc;

    if-eqz v1, :cond_1b

    .line 178
    const/16 v1, 0x16

    iget-object v2, p0, Lgkz;->u:Lgmc;

    .line 179
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_1b
    iget-object v1, p0, Lgkz;->v:Ljava/lang/Integer;

    if-eqz v1, :cond_1c

    .line 181
    const/16 v1, 0x18

    iget-object v2, p0, Lgkz;->v:Ljava/lang/Integer;

    .line 182
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_1c
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lgkz;->a(Lhfp;)Lgkz;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 28
    iget-object v0, p0, Lgkz;->b:Lglt;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v2, p0, Lgkz;->b:Lglt;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lgkz;->a:Lgkt;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v2, p0, Lgkz;->a:Lgkt;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_1
    iget-object v0, p0, Lgkz;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 33
    const/4 v0, 0x3

    iget-object v2, p0, Lgkz;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 34
    :cond_2
    iget-object v0, p0, Lgkz;->d:[Lgkg;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgkz;->d:[Lgkg;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 35
    :goto_0
    iget-object v2, p0, Lgkz;->d:[Lgkg;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 36
    iget-object v2, p0, Lgkz;->d:[Lgkg;

    aget-object v2, v2, v0

    .line 37
    if-eqz v2, :cond_3

    .line 38
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 39
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_4
    iget-object v0, p0, Lgkz;->e:Lgkq;

    if-eqz v0, :cond_5

    .line 41
    const/4 v0, 0x5

    iget-object v2, p0, Lgkz;->e:Lgkq;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_5
    iget-object v0, p0, Lgkz;->f:Lgkk;

    if-eqz v0, :cond_6

    .line 43
    const/4 v0, 0x6

    iget-object v2, p0, Lgkz;->f:Lgkk;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_6
    iget-object v0, p0, Lgkz;->h:Lglo;

    if-eqz v0, :cond_7

    .line 45
    const/4 v0, 0x7

    iget-object v2, p0, Lgkz;->h:Lglo;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 46
    :cond_7
    iget-object v0, p0, Lgkz;->i:[Lgki;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgkz;->i:[Lgki;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 47
    :goto_1
    iget-object v2, p0, Lgkz;->i:[Lgki;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 48
    iget-object v2, p0, Lgkz;->i:[Lgki;

    aget-object v2, v2, v0

    .line 49
    if-eqz v2, :cond_8

    .line 50
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 51
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    :cond_9
    iget-object v0, p0, Lgkz;->g:Lgkl;

    if-eqz v0, :cond_a

    .line 53
    const/16 v0, 0x9

    iget-object v2, p0, Lgkz;->g:Lgkl;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 54
    :cond_a
    iget-object v0, p0, Lgkz;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 55
    const/16 v0, 0xa

    iget-object v2, p0, Lgkz;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 56
    :cond_b
    iget-object v0, p0, Lgkz;->l:Lgkm;

    if-eqz v0, :cond_c

    .line 57
    const/16 v0, 0xb

    iget-object v2, p0, Lgkz;->l:Lgkm;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 58
    :cond_c
    iget-object v0, p0, Lgkz;->m:Lglw;

    if-eqz v0, :cond_d

    .line 59
    const/16 v0, 0xc

    iget-object v2, p0, Lgkz;->m:Lglw;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 60
    :cond_d
    iget-object v0, p0, Lgkz;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 61
    const/16 v0, 0xd

    iget-object v2, p0, Lgkz;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 62
    :cond_e
    iget-object v0, p0, Lgkz;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 63
    const/16 v0, 0xe

    iget-object v2, p0, Lgkz;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 64
    :cond_f
    iget-object v0, p0, Lgkz;->o:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 65
    const/16 v0, 0xf

    iget-object v2, p0, Lgkz;->o:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(IZ)V

    .line 66
    :cond_10
    iget-object v0, p0, Lgkz;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 67
    const/16 v0, 0x10

    iget-object v2, p0, Lgkz;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 68
    :cond_11
    iget-object v0, p0, Lgkz;->q:[Lglm;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgkz;->q:[Lglm;

    array-length v0, v0

    if-lez v0, :cond_13

    move v0, v1

    .line 69
    :goto_2
    iget-object v2, p0, Lgkz;->q:[Lglm;

    array-length v2, v2

    if-ge v0, v2, :cond_13

    .line 70
    iget-object v2, p0, Lgkz;->q:[Lglm;

    aget-object v2, v2, v0

    .line 71
    if-eqz v2, :cond_12

    .line 72
    const/16 v3, 0x11

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 73
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 74
    :cond_13
    iget-object v0, p0, Lgkz;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    .line 75
    const/16 v0, 0x12

    iget-object v2, p0, Lgkz;->r:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 76
    :cond_14
    iget-object v0, p0, Lgkz;->s:Lgwx;

    if-eqz v0, :cond_15

    .line 77
    const/16 v0, 0x13

    iget-object v2, p0, Lgkz;->s:Lgwx;

    invoke-virtual {p1, v0, v2}, Lhfq;->b(ILhfz;)V

    .line 78
    :cond_15
    iget-object v0, p0, Lgkz;->t:[Lgkw;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lgkz;->t:[Lgkw;

    array-length v0, v0

    if-lez v0, :cond_17

    .line 79
    :goto_3
    iget-object v0, p0, Lgkz;->t:[Lgkw;

    array-length v0, v0

    if-ge v1, v0, :cond_17

    .line 80
    iget-object v0, p0, Lgkz;->t:[Lgkw;

    aget-object v0, v0, v1

    .line 81
    if-eqz v0, :cond_16

    .line 82
    const/16 v2, 0x14

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 83
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 84
    :cond_17
    iget-object v0, p0, Lgkz;->u:Lgmc;

    if-eqz v0, :cond_18

    .line 85
    const/16 v0, 0x16

    iget-object v1, p0, Lgkz;->u:Lgmc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 86
    :cond_18
    iget-object v0, p0, Lgkz;->v:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 87
    const/16 v0, 0x18

    iget-object v1, p0, Lgkz;->v:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 88
    :cond_19
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 89
    return-void
.end method
