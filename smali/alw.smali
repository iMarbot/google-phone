.class public final Lalw;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field public a:Ljava/util/List;

.field public b:Z

.field public c:Z

.field public d:Lamd;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(IZILjava/util/List;Lamd;Ljava/lang/String;Ljava/util/List;)Lalw;
    .locals 4

    .prologue
    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    if-eqz p3, :cond_0

    .line 5
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 6
    :cond_0
    new-instance v1, Lalw;

    invoke-direct {v1}, Lalw;-><init>()V

    .line 7
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 8
    const-string v3, "title_res_id"

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 9
    const-string v3, "can_set_default"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 10
    if-eqz p2, :cond_1

    .line 11
    const-string v3, "set_default_res_id"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 12
    :cond_1
    const-string v3, "account_handles"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 13
    const-string v0, "listener"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 14
    const-string v0, "call_id"

    invoke-virtual {v2, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    if-eqz p6, :cond_2

    .line 16
    const-string v0, "hints"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 17
    :cond_2
    invoke-virtual {v1, v2}, Lalw;->setArguments(Landroid/os/Bundle;)V

    .line 19
    iput-object p4, v1, Lalw;->d:Lamd;

    .line 20
    return-object v1
.end method

.method public static a(Ljava/util/List;Lamd;Ljava/lang/String;)Lalw;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2
    const v0, 0x7f1102a9

    move v2, v1

    move-object v3, p0

    move-object v4, p1

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lalw;->a(IZILjava/util/List;Lamd;Ljava/lang/String;Ljava/util/List;)Lalw;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "call_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 57
    iget-boolean v0, p0, Lalw;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lalw;->d:Lamd;

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "extra_call_id"

    invoke-virtual {p0}, Lalw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lalw;->d:Lamd;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lamd;->onReceiveResult(ILandroid/os/Bundle;)V

    .line 61
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 62
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    .line 24
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title_res_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 25
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "can_set_default"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 26
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "account_handles"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lalw;->a:Ljava/util/List;

    .line 27
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "listener"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lamd;

    iput-object v0, p0, Lalw;->d:Lamd;

    .line 28
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "hints"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lalw;->e:Ljava/util/List;

    .line 29
    if-eqz p1, :cond_0

    .line 30
    const-string v0, "is_default_checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lalw;->c:Z

    .line 31
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lalw;->b:Z

    .line 32
    new-instance v0, Lalz;

    invoke-direct {v0, p0}, Lalz;-><init>(Lalw;)V

    .line 33
    new-instance v3, Lama;

    invoke-direct {v3, p0}, Lama;-><init>(Lalw;)V

    .line 34
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lalw;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v5, Lamb;

    .line 36
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0400af

    iget-object v8, p0, Lalw;->a:Ljava/util/List;

    iget-object v9, p0, Lalw;->e:Ljava/util/List;

    invoke-direct {v5, v6, v7, v8, v9}, Lamb;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/util/List;)V

    .line 38
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 39
    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    .line 41
    if-eqz v2, :cond_1

    .line 43
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003c

    const/4 v2, 0x0

    .line 44
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 45
    const v1, 0x7f0e0163

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 46
    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 47
    iget-boolean v2, p0, Lalw;->c:Z

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 48
    const v2, 0x7f0e0164

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 50
    invoke-virtual {p0}, Lalw;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "set_default_res_id"

    const v6, 0x7f1102b1

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 51
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 52
    new-instance v4, Lalx;

    invoke-direct {v4, v1}, Lalx;-><init>(Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    new-instance v2, Laly;

    invoke-direct {v2, v1}, Laly;-><init>(Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    invoke-virtual {p0, v3}, Lalw;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {v5}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 56
    :cond_1
    return-object v5
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 22
    const-string v0, "is_default_checked"

    iget-boolean v1, p0, Lalw;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 23
    return-void
.end method
