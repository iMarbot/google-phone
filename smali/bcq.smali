.class final Lbcq;
.super Lbcr;
.source "PG"


# instance fields
.field private a:I

.field private b:J

.field private c:Lamg;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:I

.field private q:I

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I


# direct methods
.method constructor <init>(IJLamg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZII)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lbcr;-><init>()V

    .line 2
    iput p1, p0, Lbcq;->a:I

    .line 3
    iput-wide p2, p0, Lbcq;->b:J

    .line 4
    iput-object p4, p0, Lbcq;->c:Lamg;

    .line 5
    iput-object p5, p0, Lbcq;->d:Ljava/lang/String;

    .line 6
    iput-object p6, p0, Lbcq;->e:Ljava/lang/String;

    .line 7
    iput-object p7, p0, Lbcq;->f:Ljava/lang/String;

    .line 8
    iput-wide p8, p0, Lbcq;->g:J

    .line 9
    iput-object p10, p0, Lbcq;->h:Ljava/lang/String;

    .line 10
    iput-object p11, p0, Lbcq;->i:Ljava/lang/String;

    .line 11
    iput-boolean p12, p0, Lbcq;->j:Z

    .line 12
    iput-boolean p13, p0, Lbcq;->k:Z

    .line 13
    move-object/from16 v0, p14

    iput-object v0, p0, Lbcq;->l:Ljava/lang/String;

    .line 14
    move-object/from16 v0, p15

    iput-object v0, p0, Lbcq;->m:Ljava/lang/String;

    .line 15
    move-object/from16 v0, p16

    iput-object v0, p0, Lbcq;->n:Ljava/lang/String;

    .line 16
    move-object/from16 v0, p17

    iput-object v0, p0, Lbcq;->o:Ljava/lang/String;

    .line 17
    move/from16 v0, p18

    iput v0, p0, Lbcq;->p:I

    .line 18
    move/from16 v0, p19

    iput v0, p0, Lbcq;->q:I

    .line 19
    move/from16 v0, p20

    iput-boolean v0, p0, Lbcq;->r:Z

    .line 20
    move/from16 v0, p21

    iput-boolean v0, p0, Lbcq;->s:Z

    .line 21
    move/from16 v0, p22

    iput v0, p0, Lbcq;->t:I

    .line 22
    move/from16 v0, p23

    iput v0, p0, Lbcq;->u:I

    .line 23
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lbcq;->a:I

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lbcq;->b:J

    return-wide v0
.end method

.method public final c()Lamg;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbcq;->c:Lamg;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbcq;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lbcq;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    instance-of v2, p1, Lbcr;

    if-eqz v2, :cond_c

    .line 49
    check-cast p1, Lbcr;

    .line 50
    iget v2, p0, Lbcq;->a:I

    invoke-virtual {p1}, Lbcr;->a()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lbcq;->b:J

    .line 51
    invoke-virtual {p1}, Lbcr;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbcq;->c:Lamg;

    .line 52
    invoke-virtual {p1}, Lbcr;->c()Lamg;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbcq;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 53
    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lbcq;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 54
    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Lbcq;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 55
    invoke-virtual {p1}, Lbcr;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-wide v2, p0, Lbcq;->g:J

    .line 56
    invoke-virtual {p1}, Lbcr;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lbcq;->h:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 57
    invoke-virtual {p1}, Lbcr;->h()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Lbcq;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 58
    invoke-virtual {p1}, Lbcr;->i()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-boolean v2, p0, Lbcq;->j:Z

    .line 59
    invoke-virtual {p1}, Lbcr;->j()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbcq;->k:Z

    .line 60
    invoke-virtual {p1}, Lbcr;->k()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lbcq;->l:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 61
    invoke-virtual {p1}, Lbcr;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Lbcq;->m:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 62
    invoke-virtual {p1}, Lbcr;->m()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget-object v2, p0, Lbcq;->n:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 63
    invoke-virtual {p1}, Lbcr;->n()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_8
    iget-object v2, p0, Lbcq;->o:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 64
    invoke-virtual {p1}, Lbcr;->o()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_9
    iget v2, p0, Lbcq;->p:I

    .line 65
    invoke-virtual {p1}, Lbcr;->p()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lbcq;->q:I

    .line 66
    invoke-virtual {p1}, Lbcr;->q()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbcq;->r:Z

    .line 67
    invoke-virtual {p1}, Lbcr;->r()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lbcq;->s:Z

    .line 68
    invoke-virtual {p1}, Lbcr;->s()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lbcq;->t:I

    .line 69
    invoke-virtual {p1}, Lbcr;->t()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lbcq;->u:I

    .line 70
    invoke-virtual {p1}, Lbcr;->u()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 71
    goto/16 :goto_0

    .line 53
    :cond_3
    iget-object v2, p0, Lbcq;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    .line 54
    :cond_4
    iget-object v2, p0, Lbcq;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_2

    .line 55
    :cond_5
    iget-object v2, p0, Lbcq;->f:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 57
    :cond_6
    iget-object v2, p0, Lbcq;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_4

    .line 58
    :cond_7
    iget-object v2, p0, Lbcq;->i:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_5

    .line 61
    :cond_8
    iget-object v2, p0, Lbcq;->l:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_6

    .line 62
    :cond_9
    iget-object v2, p0, Lbcq;->m:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_7

    .line 63
    :cond_a
    iget-object v2, p0, Lbcq;->n:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_8

    .line 64
    :cond_b
    iget-object v2, p0, Lbcq;->o:Ljava/lang/String;

    invoke-virtual {p1}, Lbcr;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    :cond_c
    move v0, v1

    .line 72
    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lbcq;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Lbcq;->g:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lbcq;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 10

    .prologue
    const/16 v9, 0x20

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    const v8, 0xf4243

    .line 73
    iget v0, p0, Lbcq;->a:I

    xor-int/2addr v0, v8

    .line 74
    mul-int/2addr v0, v8

    .line 75
    iget-wide v4, p0, Lbcq;->b:J

    ushr-long/2addr v4, v9

    iget-wide v6, p0, Lbcq;->b:J

    xor-long/2addr v4, v6

    long-to-int v4, v4

    xor-int/2addr v0, v4

    .line 76
    mul-int/2addr v0, v8

    .line 77
    iget-object v4, p0, Lbcq;->c:Lamg;

    invoke-virtual {v4}, Lamg;->hashCode()I

    move-result v4

    xor-int/2addr v0, v4

    .line 78
    mul-int v4, v0, v8

    .line 79
    iget-object v0, p0, Lbcq;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v4

    .line 80
    mul-int v4, v0, v8

    .line 81
    iget-object v0, p0, Lbcq;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v4

    .line 82
    mul-int v4, v0, v8

    .line 83
    iget-object v0, p0, Lbcq;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v4

    .line 84
    mul-int/2addr v0, v8

    .line 85
    iget-wide v4, p0, Lbcq;->g:J

    ushr-long/2addr v4, v9

    iget-wide v6, p0, Lbcq;->g:J

    xor-long/2addr v4, v6

    long-to-int v4, v4

    xor-int/2addr v0, v4

    .line 86
    mul-int v4, v0, v8

    .line 87
    iget-object v0, p0, Lbcq;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v4

    .line 88
    mul-int v4, v0, v8

    .line 89
    iget-object v0, p0, Lbcq;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v4

    .line 90
    mul-int v4, v0, v8

    .line 91
    iget-boolean v0, p0, Lbcq;->j:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    xor-int/2addr v0, v4

    .line 92
    mul-int v4, v0, v8

    .line 93
    iget-boolean v0, p0, Lbcq;->k:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    xor-int/2addr v0, v4

    .line 94
    mul-int v4, v0, v8

    .line 95
    iget-object v0, p0, Lbcq;->l:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    xor-int/2addr v0, v4

    .line 96
    mul-int v4, v0, v8

    .line 97
    iget-object v0, p0, Lbcq;->m:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    xor-int/2addr v0, v4

    .line 98
    mul-int v4, v0, v8

    .line 99
    iget-object v0, p0, Lbcq;->n:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    xor-int/2addr v0, v4

    .line 100
    mul-int/2addr v0, v8

    .line 101
    iget-object v4, p0, Lbcq;->o:Ljava/lang/String;

    if-nez v4, :cond_a

    :goto_a
    xor-int/2addr v0, v1

    .line 102
    mul-int/2addr v0, v8

    .line 103
    iget v1, p0, Lbcq;->p:I

    xor-int/2addr v0, v1

    .line 104
    mul-int/2addr v0, v8

    .line 105
    iget v1, p0, Lbcq;->q:I

    xor-int/2addr v0, v1

    .line 106
    mul-int v1, v0, v8

    .line 107
    iget-boolean v0, p0, Lbcq;->r:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    xor-int/2addr v0, v1

    .line 108
    mul-int/2addr v0, v8

    .line 109
    iget-boolean v1, p0, Lbcq;->s:Z

    if-eqz v1, :cond_c

    :goto_c
    xor-int/2addr v0, v2

    .line 110
    mul-int/2addr v0, v8

    .line 111
    iget v1, p0, Lbcq;->t:I

    xor-int/2addr v0, v1

    .line 112
    mul-int/2addr v0, v8

    .line 113
    iget v1, p0, Lbcq;->u:I

    xor-int/2addr v0, v1

    .line 114
    return v0

    .line 79
    :cond_0
    iget-object v0, p0, Lbcq;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, p0, Lbcq;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 83
    :cond_2
    iget-object v0, p0, Lbcq;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 87
    :cond_3
    iget-object v0, p0, Lbcq;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 89
    :cond_4
    iget-object v0, p0, Lbcq;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    move v0, v3

    .line 91
    goto :goto_5

    :cond_6
    move v0, v3

    .line 93
    goto :goto_6

    .line 95
    :cond_7
    iget-object v0, p0, Lbcq;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 97
    :cond_8
    iget-object v0, p0, Lbcq;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 99
    :cond_9
    iget-object v0, p0, Lbcq;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 101
    :cond_a
    iget-object v1, p0, Lbcq;->o:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_b
    move v0, v3

    .line 107
    goto :goto_b

    :cond_c
    move v2, v3

    .line 109
    goto :goto_c
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbcq;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lbcq;->j:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lbcq;->k:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lbcq;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbcq;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lbcq;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbcq;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lbcq;->p:I

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lbcq;->q:I

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lbcq;->r:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lbcq;->s:Z

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lbcq;->t:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 27

    .prologue
    .line 45
    move-object/from16 v0, p0

    iget v2, v0, Lbcq;->a:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbcq;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lbcq;->c:Lamg;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lbcq;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbcq;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbcq;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lbcq;->g:J

    move-object/from16 v0, p0

    iget-object v9, v0, Lbcq;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbcq;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lbcq;->j:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lbcq;->k:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lbcq;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcq;->m:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcq;->n:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbcq;->o:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbcq;->p:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbcq;->q:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbcq;->r:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbcq;->s:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbcq;->t:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lbcq;->u:I

    move/from16 v24, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v25

    add-int/lit16 v0, v0, 0x19d

    move/from16 v25, v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    new-instance v26, Ljava/lang/StringBuilder;

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v25, "CoalescedRow{id="

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v25, ", timestamp="

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", number="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", formattedNumber="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", photoUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", photoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lookupUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", numberTypeLabel="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isRead="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isNew="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", geocodedLocation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", phoneAccountComponentName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", phoneAccountId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", phoneAccountLabel="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", phoneAccountColor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", features="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isBusiness="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isVoicemail="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", callType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", numberCalls="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lbcq;->u:I

    return v0
.end method
