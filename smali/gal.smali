.class final Lgal;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfxb;


# instance fields
.field private a:Lfxe;

.field private b:Lgax;

.field private c:Lgax;


# direct methods
.method constructor <init>(Lfxe;Lgax;Lgax;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lgal;->a:Lfxe;

    .line 3
    iget-object v0, p0, Lgal;->a:Lfxe;

    invoke-virtual {v0, p0}, Lfxe;->a(Lfwt;)V

    .line 4
    iput-object p2, p0, Lgal;->b:Lgax;

    .line 5
    iput-object p3, p0, Lgal;->c:Lgax;

    .line 6
    return-void
.end method

.method private static a(Lgak;)J
    .locals 2

    .prologue
    .line 127
    .line 128
    iget-boolean v0, p0, Lgak;->b:Z

    .line 129
    if-eqz v0, :cond_0

    .line 131
    iget-wide v0, p0, Lgak;->c:J

    .line 135
    :goto_0
    return-wide v0

    .line 134
    :cond_0
    iget-wide v0, p0, Lgak;->e:J

    goto :goto_0
.end method

.method private final a(Lgak;JJLjava/lang/String;)V
    .locals 6

    .prologue
    .line 136
    cmp-long v0, p4, p2

    if-ltz v0, :cond_0

    .line 137
    iget-object v0, p0, Lgal;->b:Lgax;

    .line 138
    invoke-interface {v0}, Lgax;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgba;

    new-instance v1, Lgaz;

    invoke-direct {v1, p2, p3, p4, p5}, Lgaz;-><init>(JJ)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 140
    iget-object v2, p1, Lgak;->j:Lfyt;

    .line 141
    invoke-static {v2}, Lfyt;->a(Lfyt;)Ljava/lang/String;

    move-result-object v5

    move-object v2, p6

    .line 142
    invoke-virtual/range {v0 .. v5}, Lgba;->a(Lgaz;Ljava/lang/String;ZLhrz;Ljava/lang/String;)V

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Landroid/app/Activity;)V
    .locals 18

    .prologue
    .line 7
    sget-object v2, Lgat;->a:Lgat;

    .line 9
    iget-boolean v2, v2, Lgat;->c:Z

    .line 10
    if-eqz v2, :cond_1

    .line 12
    move-object/from16 v0, p0

    iget-object v2, v0, Lgal;->a:Lfxe;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lfxe;->b(Lfwt;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 14
    :cond_1
    sget-object v3, Lgak;->a:Lgak;

    .line 17
    iget-wide v4, v3, Lgak;->h:J

    .line 18
    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_0

    .line 19
    move-object/from16 v0, p0

    iget-object v2, v0, Lgal;->a:Lfxe;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lfxe;->b(Lfwt;)V

    .line 21
    move-object/from16 v0, p0

    iget-object v2, v0, Lgal;->b:Lgax;

    invoke-interface {v2}, Lgax;->a()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v3}, Lgal;->a(Lgak;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-gtz v2, :cond_5

    .line 46
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lgal;->c:Lgax;

    invoke-interface {v2}, Lgax;->a()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v3}, Lgal;->a(Lgak;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_0

    .line 48
    invoke-static {v3}, Lgal;->a(Lgak;)J

    move-result-wide v6

    .line 50
    iget-wide v14, v3, Lgak;->h:J

    .line 52
    cmp-long v2, v14, v6

    if-ltz v2, :cond_0

    .line 54
    iget-boolean v5, v3, Lgak;->b:Z

    .line 56
    if-eqz v5, :cond_8

    const-string v2, "Cold startup"

    .line 57
    :goto_2
    new-instance v4, Lgam;

    invoke-direct {v4, v2, v6, v7}, Lgam;-><init>(Ljava/lang/String;J)V

    .line 58
    if-eqz v5, :cond_b

    .line 59
    const-string v5, "App create"

    .line 61
    iget-wide v6, v3, Lgak;->c:J

    .line 63
    iget-wide v8, v3, Lgak;->d:J

    .line 64
    invoke-virtual/range {v4 .. v9}, Lgam;->a(Ljava/lang/String;JJ)J

    .line 67
    iget-object v2, v3, Lgak;->k:Ljava/lang/String;

    .line 68
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v5, ": onCreate"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 70
    :goto_3
    iget-wide v6, v3, Lgak;->d:J

    .line 73
    iget-wide v8, v3, Lgak;->f:J

    .line 74
    invoke-virtual/range {v4 .. v9}, Lgam;->a(Ljava/lang/String;JJ)J

    move-result-wide v8

    .line 76
    iget-wide v6, v3, Lgak;->e:J

    .line 77
    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-lez v2, :cond_3

    .line 80
    iget-object v2, v3, Lgak;->k:Ljava/lang/String;

    .line 81
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v5, ": init"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 83
    :goto_4
    iget-wide v10, v3, Lgak;->d:J

    .line 86
    iget-wide v12, v3, Lgak;->e:J

    .line 88
    iget-wide v6, v4, Lgam;->b:J

    const-wide/16 v16, 0x1

    add-long v6, v6, v16

    iput-wide v6, v4, Lgam;->b:J

    invoke-virtual/range {v4 .. v13}, Lgam;->a(Ljava/lang/String;JJJJ)J

    .line 103
    :cond_3
    :goto_5
    iget-object v2, v3, Lgak;->k:Ljava/lang/String;

    .line 104
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v5, ": onDraw"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 106
    :goto_6
    iget-wide v6, v3, Lgak;->f:J

    move-wide v8, v14

    .line 108
    invoke-virtual/range {v4 .. v9}, Lgam;->a(Ljava/lang/String;JJ)J

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lgal;->c:Lgax;

    .line 110
    invoke-interface {v2}, Lgax;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgbb;

    .line 112
    new-instance v5, Lhsq;

    invoke-direct {v5}, Lhsq;-><init>()V

    .line 113
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lhsq;->a:Ljava/lang/Long;

    .line 114
    iget-object v6, v4, Lgam;->a:Ljava/util/ArrayList;

    iget-object v4, v4, Lgam;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lhsy;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lhsy;

    iput-object v4, v5, Lhsq;->b:[Lhsy;

    .line 117
    iget-object v3, v3, Lgak;->j:Lfyt;

    .line 118
    invoke-static {v3}, Lfyt;->a(Lfyt;)Ljava/lang/String;

    move-result-object v3

    .line 119
    const-string v4, "TraceService"

    const-string v6, "Recording trace %d: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v5, Lhsq;->a:Ljava/lang/Long;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v5, Lhsq;->b:[Lhsy;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget-object v9, v9, Lhsy;->a:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v4, v6, v7}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    new-instance v4, Lhtd;

    invoke-direct {v4}, Lhtd;-><init>()V

    .line 121
    iput-object v5, v4, Lhtd;->o:Lhsq;

    .line 122
    if-eqz v3, :cond_4

    .line 123
    new-instance v5, Lhqo;

    invoke-direct {v5}, Lhqo;-><init>()V

    iput-object v5, v4, Lhtd;->s:Lhqo;

    .line 124
    iget-object v5, v4, Lhtd;->s:Lhqo;

    iput-object v3, v5, Lhqo;->a:Ljava/lang/String;

    .line 125
    :cond_4
    invoke-virtual {v2, v4}, Lgbb;->a(Lhtd;)V

    goto/16 :goto_0

    .line 24
    :cond_5
    iget-wide v6, v3, Lgak;->h:J

    .line 27
    iget-boolean v9, v3, Lgak;->b:Z

    .line 29
    invoke-static {v3}, Lgal;->a(Lgak;)J

    move-result-wide v4

    .line 30
    if-eqz v9, :cond_6

    const-string v8, "Cold startup"

    :goto_7
    move-object/from16 v2, p0

    .line 31
    invoke-direct/range {v2 .. v8}, Lgal;->a(Lgak;JJLjava/lang/String;)V

    .line 33
    iget-wide v6, v3, Lgak;->i:J

    .line 35
    if-eqz v9, :cond_7

    const-string v8, "Cold startup interactive"

    :goto_8
    move-object/from16 v2, p0

    .line 36
    invoke-direct/range {v2 .. v8}, Lgal;->a(Lgak;JJLjava/lang/String;)V

    .line 38
    iget-wide v4, v3, Lgak;->g:J

    .line 41
    iget-wide v6, v3, Lgak;->h:J

    .line 43
    if-nez v9, :cond_2

    const-wide/16 v8, 0x0

    cmp-long v2, v4, v8

    if-eqz v2, :cond_2

    .line 44
    const-string v8, "Warm startup activity onStart"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lgal;->a(Lgak;JJLjava/lang/String;)V

    goto/16 :goto_1

    .line 30
    :cond_6
    const-string v8, "Warm startup"

    goto :goto_7

    .line 35
    :cond_7
    const-string v8, "Warm startup interactive"

    goto :goto_8

    .line 56
    :cond_8
    const-string v2, "Warm startup"

    goto/16 :goto_2

    .line 68
    :cond_9
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 81
    :cond_a
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 93
    :cond_b
    iget-object v2, v3, Lgak;->k:Ljava/lang/String;

    .line 94
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v5, ": onCreate"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    :goto_9
    iget-wide v6, v3, Lgak;->e:J

    .line 99
    iget-wide v8, v3, Lgak;->f:J

    .line 100
    invoke-virtual/range {v4 .. v9}, Lgam;->a(Ljava/lang/String;JJ)J

    goto/16 :goto_5

    .line 94
    :cond_c
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    .line 104
    :cond_d
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_6
.end method
