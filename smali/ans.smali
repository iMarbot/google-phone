.class final Lans;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Landroid/util/SparseArray;

.field private synthetic b:Lano;


# direct methods
.method constructor <init>(Lano;Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lans;->b:Lano;

    iput-object p2, p0, Lans;->a:Landroid/util/SparseArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2
    const-string v0, "CallLogAdapter.showDeleteSelectedItemsDialog"

    iget-object v1, p0, Lans;->a:Landroid/util/SparseArray;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onClick, these items to delete "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lans;->b:Lano;

    iget-object v1, p0, Lans;->a:Landroid/util/SparseArray;

    .line 4
    invoke-virtual {v0, v1}, Lano;->a(Landroid/util/SparseArray;)V

    .line 5
    iget-object v0, p0, Lans;->b:Lano;

    iget-object v0, v0, Lano;->o:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 6
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 7
    iget-object v0, p0, Lans;->b:Lano;

    iget-object v0, v0, Lano;->c:Landroid/app/Activity;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cp:Lbkq$a;

    .line 8
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 9
    return-void
.end method
