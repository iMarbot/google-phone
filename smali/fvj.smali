.class public final Lfvj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static h:Z


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lfmz;

.field public c:Lfvv;

.field public d:Lfnp;

.field public e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

.field public final f:Landroid/content/ServiceConnection;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lfvj;->h:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfvk;

    invoke-direct {v0, p0}, Lfvk;-><init>(Lfvj;)V

    iput-object v0, p0, Lfvj;->f:Landroid/content/ServiceConnection;

    .line 3
    iput-object p1, p0, Lfvj;->a:Landroid/content/Context;

    .line 5
    new-instance v0, Lfon;

    invoke-direct {v0}, Lfon;-><init>()V

    .line 6
    invoke-virtual {v0, p1}, Lfon;->init(Landroid/content/Context;)Z

    move-result v0

    .line 7
    iput-boolean v0, p0, Lfvj;->g:Z

    .line 8
    iget-boolean v0, p0, Lfvj;->g:Z

    if-nez v0, :cond_0

    .line 9
    const-string v0, "Hangouts not supported by this device"

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 11
    :cond_0
    sget-boolean v0, Lfvj;->h:Z

    if-nez v0, :cond_1

    .line 12
    iget-object v0, p0, Lfvj;->a:Landroid/content/Context;

    invoke-static {v0}, Lgdq;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    const-class v1, Lfmz;

    new-instance v2, Lfmz;

    invoke-direct {v2}, Lfmz;-><init>()V

    .line 13
    invoke-virtual {v0, v1, v2}, Lgdq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 14
    const/4 v0, 0x1

    sput-boolean v0, Lfvj;->h:Z

    .line 15
    :cond_1
    invoke-static {p1}, Lgdq;->a(Landroid/content/Context;)Lgdq;

    move-result-object v0

    const-class v1, Lfmz;

    invoke-virtual {v0, v1}, Lgdq;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    iput-object v0, p0, Lfvj;->b:Lfmz;

    .line 16
    new-instance v0, Lfvv;

    invoke-direct {v0, p1}, Lfvv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfvj;->c:Lfvv;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfvj;->d:Lfnp;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lfvj;->d:Lfnp;

    invoke-virtual {v0}, Lfnp;->leave()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lfvj;->d:Lfnp;

    .line 23
    :cond_0
    invoke-virtual {p0}, Lfvj;->b()V

    .line 24
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    if-eqz v0, :cond_0

    .line 26
    :try_start_0
    iget-object v0, p0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->unbindFromCall()V

    .line 27
    iget-object v0, p0, Lfvj;->a:Landroid/content/Context;

    iget-object v1, p0, Lfvj;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lfvj;->e:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 31
    const-string v1, "Error disconnecting CallService"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final finalize()V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Lfvj;->a()V

    .line 19
    return-void
.end method
