.class public final enum Lfbq;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static enum a:Lfbq;

.field private static enum b:Lfbq;

.field private static enum c:Lfbq;

.field private static enum d:Lfbq;

.field private static enum e:Lfbq;

.field private static enum f:Lfbq;

.field private static enum g:Lfbq;

.field private static enum h:Lfbq;

.field private static enum i:Lfbq;

.field private static enum j:Lfbq;

.field private static enum k:Lfbq;

.field private static enum l:Lfbq;

.field private static enum m:Lfbq;

.field private static enum n:Lfbq;

.field private static synthetic o:[Lfbq;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lfbq;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->a:Lfbq;

    .line 5
    new-instance v0, Lfbq;

    const-string v1, "ACCOUNT_DISABLED"

    invoke-direct {v0, v1, v4}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->b:Lfbq;

    .line 6
    new-instance v0, Lfbq;

    const-string v1, "BAD_USERNAME"

    invoke-direct {v0, v1, v5}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->c:Lfbq;

    .line 7
    new-instance v0, Lfbq;

    const-string v1, "BAD_REQUEST"

    invoke-direct {v0, v1, v6}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->d:Lfbq;

    .line 8
    new-instance v0, Lfbq;

    const-string v1, "LOGIN_FAIL"

    invoke-direct {v0, v1, v7}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->e:Lfbq;

    .line 9
    new-instance v0, Lfbq;

    const-string v1, "SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->f:Lfbq;

    .line 10
    new-instance v0, Lfbq;

    const-string v1, "MISSING_APPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->g:Lfbq;

    .line 11
    new-instance v0, Lfbq;

    const-string v1, "NO_GMAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->h:Lfbq;

    .line 12
    new-instance v0, Lfbq;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->i:Lfbq;

    .line 13
    new-instance v0, Lfbq;

    const-string v1, "CAPTCHA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->j:Lfbq;

    .line 14
    new-instance v0, Lfbq;

    const-string v1, "CANCELLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->k:Lfbq;

    .line 15
    new-instance v0, Lfbq;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->l:Lfbq;

    .line 16
    new-instance v0, Lfbq;

    const-string v1, "OAUTH_MIGRATION_REQUIRED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->m:Lfbq;

    .line 17
    new-instance v0, Lfbq;

    const-string v1, "DMAGENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lfbq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfbq;->n:Lfbq;

    .line 18
    const/16 v0, 0xe

    new-array v0, v0, [Lfbq;

    sget-object v1, Lfbq;->a:Lfbq;

    aput-object v1, v0, v3

    sget-object v1, Lfbq;->b:Lfbq;

    aput-object v1, v0, v4

    sget-object v1, Lfbq;->c:Lfbq;

    aput-object v1, v0, v5

    sget-object v1, Lfbq;->d:Lfbq;

    aput-object v1, v0, v6

    sget-object v1, Lfbq;->e:Lfbq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lfbq;->f:Lfbq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfbq;->g:Lfbq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lfbq;->h:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lfbq;->i:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lfbq;->j:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lfbq;->k:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lfbq;->l:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lfbq;->m:Lfbq;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lfbq;->n:Lfbq;

    aput-object v2, v0, v1

    sput-object v0, Lfbq;->o:[Lfbq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lfbq;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lfbq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfbq;

    return-object v0
.end method

.method public static values()[Lfbq;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lfbq;->o:[Lfbq;

    invoke-virtual {v0}, [Lfbq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfbq;

    return-object v0
.end method
