.class public final enum Lgxf;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lgxf;

.field public static final enum b:Lgxf;

.field public static final enum c:Lgxf;

.field public static final enum d:Lgxf;

.field public static final enum e:Lgxf;

.field private static synthetic f:[Lgxf;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lgxf;

    const-string v1, "INVALID_COUNTRY_CODE"

    invoke-direct {v0, v1, v2}, Lgxf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxf;->a:Lgxf;

    .line 4
    new-instance v0, Lgxf;

    const-string v1, "NOT_A_NUMBER"

    invoke-direct {v0, v1, v3}, Lgxf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxf;->b:Lgxf;

    .line 5
    new-instance v0, Lgxf;

    const-string v1, "TOO_SHORT_AFTER_IDD"

    invoke-direct {v0, v1, v4}, Lgxf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxf;->c:Lgxf;

    .line 6
    new-instance v0, Lgxf;

    const-string v1, "TOO_SHORT_NSN"

    invoke-direct {v0, v1, v5}, Lgxf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxf;->d:Lgxf;

    .line 7
    new-instance v0, Lgxf;

    const-string v1, "TOO_LONG"

    invoke-direct {v0, v1, v6}, Lgxf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgxf;->e:Lgxf;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lgxf;

    sget-object v1, Lgxf;->a:Lgxf;

    aput-object v1, v0, v2

    sget-object v1, Lgxf;->b:Lgxf;

    aput-object v1, v0, v3

    sget-object v1, Lgxf;->c:Lgxf;

    aput-object v1, v0, v4

    sget-object v1, Lgxf;->d:Lgxf;

    aput-object v1, v0, v5

    sget-object v1, Lgxf;->e:Lgxf;

    aput-object v1, v0, v6

    sput-object v0, Lgxf;->f:[Lgxf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgxf;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgxf;->f:[Lgxf;

    invoke-virtual {v0}, [Lgxf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgxf;

    return-object v0
.end method
