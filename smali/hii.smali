.class public final Lhii;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhii$a;
    }
.end annotation


# static fields
.field public static final k:Lhii;

.field private static volatile l:Lhdm;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 177
    new-instance v0, Lhii;

    invoke-direct {v0}, Lhii;-><init>()V

    .line 178
    sput-object v0, Lhii;->k:Lhii;

    invoke-virtual {v0}, Lhii;->makeImmutable()V

    .line 179
    const-class v0, Lhii;

    sget-object v1, Lhii;->k:Lhii;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 180
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const-string v0, ""

    iput-object v0, p0, Lhii;->b:Ljava/lang/String;

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lhii;->c:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lhii;->d:Ljava/lang/String;

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lhii;->e:Ljava/lang/String;

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lhii;->f:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lhii;->h:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lhii;->j:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 172
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 173
    sget-object v2, Lhii$a;->f:Lhby;

    .line 174
    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "j"

    aput-object v2, v0, v1

    .line 175
    const-string v1, "\u0001\t\u0000\u0001\u0001\t\u0000\u0000\u0000\u0001\u0008\u0000\u0002\u0008\u0001\u0003\u0008\u0002\u0004\u0008\u0003\u0005\u0008\u0004\u0006\u000c\u0005\u0007\u0008\u0006\u0008\u0004\u0007\t\u0008\u0008"

    .line 176
    sget-object v2, Lhii;->k:Lhii;

    invoke-static {v2, v1, v0}, Lhii;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 98
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 171
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 99
    :pswitch_0
    new-instance v0, Lhii;

    invoke-direct {v0}, Lhii;-><init>()V

    .line 170
    :goto_0
    :pswitch_1
    return-object v0

    .line 100
    :pswitch_2
    sget-object v0, Lhii;->k:Lhii;

    goto :goto_0

    .line 102
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v2, v0}, Lhbr$a;-><init>(B[[[[[[B)V

    move-object v0, v1

    goto :goto_0

    .line 103
    :pswitch_4
    check-cast p2, Lhaq;

    .line 104
    check-cast p3, Lhbg;

    .line 105
    if-nez p3, :cond_0

    .line 106
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 107
    :cond_0
    :try_start_0
    sget-boolean v0, Lhii;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 108
    invoke-virtual {p0, p2, p3}, Lhii;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 109
    sget-object v0, Lhii;->k:Lhii;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 111
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    .line 112
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 113
    sparse-switch v2, :sswitch_data_0

    .line 116
    invoke-virtual {p0, v2, p2}, Lhii;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 117
    goto :goto_1

    :sswitch_0
    move v0, v1

    .line 115
    goto :goto_1

    .line 118
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 119
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lhii;->a:I

    .line 120
    iput-object v2, p0, Lhii;->b:Ljava/lang/String;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 157
    :catch_0
    move-exception v0

    .line 158
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 162
    :catchall_0
    move-exception v0

    throw v0

    .line 122
    :sswitch_2
    :try_start_2
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 123
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lhii;->a:I

    .line 124
    iput-object v2, p0, Lhii;->c:Ljava/lang/String;
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 159
    :catch_1
    move-exception v0

    .line 160
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 161
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 126
    :sswitch_3
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 127
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lhii;->a:I

    .line 128
    iput-object v2, p0, Lhii;->d:Ljava/lang/String;

    goto :goto_1

    .line 130
    :sswitch_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 131
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lhii;->a:I

    .line 132
    iput-object v2, p0, Lhii;->e:Ljava/lang/String;

    goto :goto_1

    .line 134
    :sswitch_5
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 135
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lhii;->a:I

    .line 136
    iput-object v2, p0, Lhii;->f:Ljava/lang/String;

    goto :goto_1

    .line 138
    :sswitch_6
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 139
    invoke-static {v2}, Lhii$a;->a(I)Lhii$a;

    move-result-object v3

    .line 140
    if-nez v3, :cond_3

    .line 141
    const/4 v3, 0x6

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 142
    :cond_3
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lhii;->a:I

    .line 143
    iput v2, p0, Lhii;->g:I

    goto/16 :goto_1

    .line 145
    :sswitch_7
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 146
    iget v3, p0, Lhii;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lhii;->a:I

    .line 147
    iput-object v2, p0, Lhii;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 149
    :sswitch_8
    iget v2, p0, Lhii;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lhii;->a:I

    .line 150
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lhii;->i:I

    goto/16 :goto_1

    .line 152
    :sswitch_9
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 153
    iget v3, p0, Lhii;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lhii;->a:I

    .line 154
    iput-object v2, p0, Lhii;->j:Ljava/lang/String;
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 163
    :cond_4
    :pswitch_5
    sget-object v0, Lhii;->k:Lhii;

    goto/16 :goto_0

    .line 164
    :pswitch_6
    sget-object v0, Lhii;->l:Lhdm;

    if-nez v0, :cond_6

    const-class v1, Lhii;

    monitor-enter v1

    .line 165
    :try_start_5
    sget-object v0, Lhii;->l:Lhdm;

    if-nez v0, :cond_5

    .line 166
    new-instance v0, Lhaa;

    sget-object v2, Lhii;->k:Lhii;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhii;->l:Lhdm;

    .line 167
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 168
    :cond_6
    sget-object v0, Lhii;->l:Lhdm;

    goto/16 :goto_0

    .line 167
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 169
    :pswitch_7
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 113
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 48
    iget v0, p0, Lhii;->memoizedSerializedSize:I

    .line 49
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 97
    :goto_0
    return v0

    .line 50
    :cond_0
    sget-boolean v0, Lhii;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {p0}, Lhii;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhii;->memoizedSerializedSize:I

    .line 52
    iget v0, p0, Lhii;->memoizedSerializedSize:I

    goto :goto_0

    .line 53
    :cond_1
    const/4 v0, 0x0

    .line 54
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 57
    iget-object v0, p0, Lhii;->b:Ljava/lang/String;

    .line 58
    invoke-static {v2, v0}, Lhaw;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 59
    :cond_2
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_3

    .line 62
    iget-object v1, p0, Lhii;->c:Ljava/lang/String;

    .line 63
    invoke-static {v3, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_3
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_4

    .line 65
    const/4 v1, 0x3

    .line 67
    iget-object v2, p0, Lhii;->d:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_4
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_5

    .line 72
    iget-object v1, p0, Lhii;->e:Ljava/lang/String;

    .line 73
    invoke-static {v4, v1}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_5
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    .line 75
    const/4 v1, 0x5

    .line 77
    iget-object v2, p0, Lhii;->f:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_6
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 80
    const/4 v1, 0x6

    iget v2, p0, Lhii;->g:I

    .line 81
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_7
    iget v1, p0, Lhii;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    .line 83
    const/4 v1, 0x7

    .line 85
    iget-object v2, p0, Lhii;->h:Ljava/lang/String;

    .line 86
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_8
    iget v1, p0, Lhii;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_9

    .line 88
    iget v1, p0, Lhii;->i:I

    .line 89
    invoke-static {v5, v1}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_9
    iget v1, p0, Lhii;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_a

    .line 91
    const/16 v1, 0x9

    .line 93
    iget-object v2, p0, Lhii;->j:Ljava/lang/String;

    .line 94
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_a
    iget-object v1, p0, Lhii;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    iput v0, p0, Lhii;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 10
    sget-boolean v0, Lhii;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 11
    invoke-virtual {p0, p1}, Lhii;->writeToInternal(Lhaw;)V

    .line 47
    :goto_0
    return-void

    .line 13
    :cond_0
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 15
    iget-object v0, p0, Lhii;->b:Ljava/lang/String;

    .line 16
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 17
    :cond_1
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 19
    iget-object v0, p0, Lhii;->c:Ljava/lang/String;

    .line 20
    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 21
    :cond_2
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 22
    const/4 v0, 0x3

    .line 23
    iget-object v1, p0, Lhii;->d:Ljava/lang/String;

    .line 24
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 25
    :cond_3
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 27
    iget-object v0, p0, Lhii;->e:Ljava/lang/String;

    .line 28
    invoke-virtual {p1, v3, v0}, Lhaw;->a(ILjava/lang/String;)V

    .line 29
    :cond_4
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 30
    const/4 v0, 0x5

    .line 31
    iget-object v1, p0, Lhii;->f:Ljava/lang/String;

    .line 32
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 33
    :cond_5
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 34
    const/4 v0, 0x6

    iget v1, p0, Lhii;->g:I

    .line 35
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 36
    :cond_6
    iget v0, p0, Lhii;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 37
    const/4 v0, 0x7

    .line 38
    iget-object v1, p0, Lhii;->h:Ljava/lang/String;

    .line 39
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 40
    :cond_7
    iget v0, p0, Lhii;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 41
    iget v0, p0, Lhii;->i:I

    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 42
    :cond_8
    iget v0, p0, Lhii;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 43
    const/16 v0, 0x9

    .line 44
    iget-object v1, p0, Lhii;->j:Ljava/lang/String;

    .line 45
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 46
    :cond_9
    iget-object v0, p0, Lhii;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
