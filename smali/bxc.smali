.class public final Lbxc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwr;
.implements Lccf;
.implements Lcdb;


# instance fields
.field public final a:Landroid/content/Context;

.field private b:Lael;

.field private c:Landroid/telecom/CallAudioState;

.field private d:Landroid/app/PendingIntent;

.field private e:Landroid/app/PendingIntent;

.field private f:Landroid/app/PendingIntent;

.field private g:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lbxc;->a:Landroid/content/Context;

    .line 4
    const-string v0, "toggleSpeaker"

    invoke-direct {p0, v0}, Lbxc;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbxc;->d:Landroid/app/PendingIntent;

    .line 5
    const-string v0, "showAudioRouteSelector"

    .line 6
    invoke-direct {p0, v0}, Lbxc;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbxc;->e:Landroid/app/PendingIntent;

    .line 7
    const-string v0, "toggleMute"

    invoke-direct {p0, v0}, Lbxc;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbxc;->f:Landroid/app/PendingIntent;

    .line 8
    const-string v0, "endCall"

    invoke-direct {p0, v0}, Lbxc;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lbxc;->g:Landroid/app/PendingIntent;

    .line 9
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwr;)V

    .line 10
    sget-object v0, Lcct;->a:Lcct;

    .line 11
    invoke-virtual {v0, p0}, Lcct;->a(Lcdb;)V

    .line 12
    sget-object v0, Lcce;->a:Lcce;

    .line 13
    invoke-virtual {v0, p0}, Lcce;->a(Lccf;)V

    .line 15
    sget-object v0, Lcce;->a:Lcce;

    .line 17
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 18
    iput-object v0, p0, Lbxc;->c:Landroid/telecom/CallAudioState;

    .line 19
    return-void
.end method

.method private final a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    const-class v2, Lcom/android/incallui/ReturnToCallActionReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private final a()Ljava/util/List;
    .locals 5

    .prologue
    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 97
    new-instance v0, Lcie;

    iget-object v2, p0, Lbxc;->c:Landroid/telecom/CallAudioState;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcie;-><init>(Landroid/telecom/CallAudioState;I)V

    .line 99
    invoke-static {}, Lafj;->f()Lafk;

    move-result-object v2

    iget-object v3, p0, Lbxc;->a:Landroid/content/Context;

    iget v4, v0, Lcie;->a:I

    .line 100
    invoke-static {v3, v4}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v3

    invoke-virtual {v2, v3}, Lafk;->a(Landroid/graphics/drawable/Icon;)Lafk;

    move-result-object v2

    iget-object v3, p0, Lbxc;->a:Landroid/content/Context;

    iget v4, v0, Lcie;->c:I

    .line 101
    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lafk;->a(Ljava/lang/CharSequence;)Lafk;

    move-result-object v2

    iget-boolean v3, v0, Lcie;->e:Z

    .line 102
    invoke-virtual {v2, v3}, Lafk;->a(Z)Lafk;

    move-result-object v2

    .line 103
    iget-boolean v0, v0, Lcie;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxc;->d:Landroid/app/PendingIntent;

    :goto_0
    invoke-virtual {v2, v0}, Lafk;->a(Landroid/app/PendingIntent;)Lafk;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lafk;->a()Lafj;

    move-result-object v0

    .line 105
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-static {}, Lafj;->f()Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->a:Landroid/content/Context;

    const v3, 0x7f020155

    .line 108
    invoke-static {v2, v3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v2

    invoke-virtual {v0, v2}, Lafk;->a(Landroid/graphics/drawable/Icon;)Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->a:Landroid/content/Context;

    const v3, 0x7f1101bc

    .line 109
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lafk;->a(Ljava/lang/CharSequence;)Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->c:Landroid/telecom/CallAudioState;

    .line 110
    invoke-virtual {v2}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v2

    invoke-virtual {v0, v2}, Lafk;->a(Z)Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->f:Landroid/app/PendingIntent;

    .line 111
    invoke-virtual {v0, v2}, Lafk;->a(Landroid/app/PendingIntent;)Lafk;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lafk;->a()Lafj;

    move-result-object v0

    .line 113
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-static {}, Lafj;->f()Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->a:Landroid/content/Context;

    const v3, 0x7f02012c

    .line 116
    invoke-static {v2, v3}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v2

    invoke-virtual {v0, v2}, Lafk;->a(Landroid/graphics/drawable/Icon;)Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->a:Landroid/content/Context;

    const v3, 0x7f1101b8

    .line 117
    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lafk;->a(Ljava/lang/CharSequence;)Lafk;

    move-result-object v0

    iget-object v2, p0, Lbxc;->g:Landroid/app/PendingIntent;

    .line 118
    invoke-virtual {v0, v2}, Lafk;->a(Landroid/app/PendingIntent;)Lafk;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lafk;->a()Lafj;

    move-result-object v0

    .line 120
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    return-object v1

    .line 103
    :cond_0
    iget-object v0, p0, Lbxc;->e:Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 1
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_return_to_call_bubble"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 5

    .prologue
    .line 83
    iput-object p1, p0, Lbxc;->c:Landroid/telecom/CallAudioState;

    .line 84
    iget-object v0, p0, Lbxc;->b:Lael;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lbxc;->b:Lael;

    invoke-direct {p0}, Lbxc;->a()Ljava/util/List;

    move-result-object v1

    .line 86
    iget-object v2, v0, Lael;->e:Lafi;

    .line 87
    invoke-static {}, Lafi;->f()Lafl;

    move-result-object v3

    .line 88
    invoke-virtual {v2}, Lafi;->c()Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lafl;->a(Landroid/app/PendingIntent;)Lafl;

    move-result-object v3

    .line 89
    invoke-virtual {v2}, Lafi;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lafl;->a(I)Lafl;

    move-result-object v3

    .line 90
    invoke-virtual {v2}, Lafi;->b()Landroid/graphics/drawable/Icon;

    move-result-object v4

    invoke-virtual {v3, v4}, Lafl;->a(Landroid/graphics/drawable/Icon;)Lafl;

    move-result-object v3

    .line 91
    invoke-virtual {v2}, Lafi;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Lafl;->b(I)Lafl;

    move-result-object v3

    .line 92
    invoke-virtual {v2}, Lafi;->e()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2}, Lafl;->a(Ljava/util/List;)Lafl;

    move-result-object v2

    .line 93
    invoke-virtual {v2, v1}, Lafl;->a(Ljava/util/List;)Lafl;

    move-result-object v1

    invoke-virtual {v1}, Lafl;->a()Lafi;

    move-result-object v1

    iput-object v1, v0, Lael;->e:Lafi;

    .line 94
    invoke-virtual {v0}, Lael;->g()V

    .line 95
    :cond_0
    return-void
.end method

.method public final a(Lcct;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 20
    if-eqz p1, :cond_2

    .line 22
    iget-object v0, p0, Lbxc;->b:Lael;

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lbxc;->b:Lael;

    invoke-virtual {v0}, Lael;->b()V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-string v0, "ReturnToCallController.hide"

    const-string v1, "hide() called without calling show()"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_2
    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    invoke-static {v1}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    iget-object v1, p0, Lbxc;->b:Lael;

    if-nez v1, :cond_4

    .line 30
    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    invoke-static {v1}, Lael;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 31
    const-string v1, "ReturnToCallController.startNewBubble"

    const-string v2, "can\'t show bubble, no permission"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    :goto_1
    iput-object v0, p0, Lbxc;->b:Lael;

    goto :goto_0

    .line 33
    :cond_3
    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    .line 34
    iget-object v2, p0, Lbxc;->a:Landroid/content/Context;

    invoke-static {v2, v6, v6, v6}, Lcom/android/incallui/InCallActivity;->a(Landroid/content/Context;ZZZ)Landroid/content/Intent;

    move-result-object v2

    .line 35
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 36
    const-string v3, "RETURN_TO_CALL_BUBBLE"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 37
    invoke-static {}, Lafi;->f()Lafl;

    move-result-object v3

    iget-object v4, p0, Lbxc;->a:Landroid/content/Context;

    .line 38
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0071

    invoke-virtual {v4, v5, v0}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    invoke-virtual {v3, v0}, Lafl;->a(I)Lafl;

    move-result-object v0

    iget-object v3, p0, Lbxc;->a:Landroid/content/Context;

    const v4, 0x7f02011a

    .line 39
    invoke-static {v3, v4}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v3

    invoke-virtual {v0, v3}, Lafl;->a(Landroid/graphics/drawable/Icon;)Lafl;

    move-result-object v0

    iget-object v3, p0, Lbxc;->a:Landroid/content/Context;

    .line 40
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d01bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 41
    invoke-virtual {v0, v3}, Lafl;->b(I)Lafl;

    move-result-object v0

    iget-object v3, p0, Lbxc;->a:Landroid/content/Context;

    const/4 v4, 0x2

    .line 42
    invoke-static {v3, v4, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 43
    invoke-virtual {v0, v2}, Lafl;->a(Landroid/app/PendingIntent;)Lafl;

    move-result-object v0

    .line 44
    invoke-direct {p0}, Lbxc;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lafl;->a(Ljava/util/List;)Lafl;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lafl;->a()Lafi;

    move-result-object v2

    .line 47
    sget-object v0, Lael;->p:Lafe;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-interface {v0, v1, v3}, Lafe;->a(Landroid/content/Context;Landroid/os/Handler;)Lael;

    move-result-object v0

    .line 49
    iput-object v2, v0, Lael;->e:Lafi;

    .line 50
    invoke-virtual {v0}, Lael;->f()V

    .line 53
    new-instance v1, Lafd;

    invoke-direct {v1, p0}, Lafd;-><init>(Lbxc;)V

    .line 54
    iput-object v1, v0, Lael;->o:Lafd;

    .line 55
    invoke-virtual {v0}, Lael;->a()V

    goto :goto_1

    .line 58
    :cond_4
    iget-object v0, p0, Lbxc;->b:Lael;

    invoke-virtual {v0}, Lael;->a()V

    goto/16 :goto_0
.end method

.method public final b(Lcdc;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public final c(Lcdc;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public final d(Lcdc;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public final e(Lcdc;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final f(Lcdc;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final g(Lcdc;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public final h(Lcdc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-virtual {p1}, Lcdc;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "ReturnToCallController.onDisconnect"

    const-string v1, "being called for a parent call and do nothing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Lbxc;->b:Lael;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbxc;->b:Lael;

    .line 68
    invoke-virtual {v0}, Lael;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbxc;->a:Landroid/content/Context;

    .line 69
    invoke-static {v0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    sget-object v0, Lcct;->a:Lcct;

    .line 71
    invoke-virtual {v0}, Lcct;->h()Lcdc;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 72
    :cond_2
    iget-object v0, p0, Lbxc;->b:Lael;

    iget-object v1, p0, Lbxc;->a:Landroid/content/Context;

    const v2, 0x7f11019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lael;->a(Ljava/lang/CharSequence;)V

    .line 73
    :cond_3
    sget-object v0, Lcct;->a:Lcct;

    .line 74
    invoke-virtual {v0}, Lcct;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lbxc;->b:Lael;

    if-eqz v0, :cond_4

    .line 77
    iget-object v0, p0, Lbxc;->b:Lael;

    invoke-virtual {v0}, Lael;->c()V

    goto :goto_0

    .line 78
    :cond_4
    const-string v0, "ReturnToCallController.reset"

    const-string v1, "reset() called without calling show()"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
