.class public final Lesv;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/util/List;

.field private b:Z

.field private c:Z

.field private d:Lesu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lewr;

    invoke-direct {v0}, Lewr;-><init>()V

    sput-object v0, Lesv;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;ZZLesu;)V
    .locals 0

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object p1, p0, Lesv;->a:Ljava/util/List;

    iput-boolean p2, p0, Lesv;->b:Z

    iput-boolean p3, p0, Lesv;->c:Z

    iput-object p4, p0, Lesv;->d:Lesu;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lesv;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x2

    iget-boolean v2, p0, Lesv;->b:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    iget-boolean v2, p0, Lesv;->c:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x5

    iget-object v2, p0, Lesv;->d:Lesu;

    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
