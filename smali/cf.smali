.class public Lcf;
.super Lve;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field private d:Landroid/support/design/widget/BottomSheetBehavior;

.field private e:Landroid/support/design/widget/BottomSheetBehavior$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcf;-><init>(Landroid/content/Context;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3
    .line 4
    if-nez p2, :cond_0

    .line 5
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010167

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7
    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    .line 10
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lve;-><init>(Landroid/content/Context;I)V

    .line 11
    iput-boolean v3, p0, Lcf;->a:Z

    .line 12
    iput-boolean v3, p0, Lcf;->b:Z

    .line 13
    new-instance v0, Landroid/support/design/widget/BottomSheetBehavior$a;

    invoke-direct {v0, p0}, Landroid/support/design/widget/BottomSheetBehavior$a;-><init>(Lcf;)V

    iput-object v0, p0, Lcf;->e:Landroid/support/design/widget/BottomSheetBehavior$a;

    .line 14
    invoke-virtual {p0, v3}, Lcf;->a(I)Z

    .line 15
    return-void

    .line 8
    :cond_1
    const p2, 0x7f120178

    goto :goto_0
.end method

.method private final a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 5

    .prologue
    .line 47
    .line 48
    invoke-virtual {p0}, Lcf;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04003e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 49
    const v1, 0x7f0e0167

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CoordinatorLayout;

    .line 50
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 51
    invoke-virtual {p0}, Lcf;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 52
    :cond_0
    const v2, 0x7f0e0169

    invoke-virtual {v1, v2}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 53
    invoke-static {v2}, Landroid/support/design/widget/BottomSheetBehavior;->a(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v3

    iput-object v3, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    .line 54
    iget-object v3, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    iget-object v4, p0, Lcf;->e:Landroid/support/design/widget/BottomSheetBehavior$a;

    .line 55
    iput-object v4, v3, Landroid/support/design/widget/BottomSheetBehavior;->k:Landroid/support/design/widget/BottomSheetBehavior$a;

    .line 56
    iget-object v3, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    iget-boolean v4, p0, Lcf;->a:Z

    .line 57
    iput-boolean v4, v3, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    .line 58
    if-nez p3, :cond_1

    .line 59
    invoke-virtual {v2, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 61
    :goto_0
    const v3, 0x7f0e0168

    .line 62
    invoke-virtual {v1, v3}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcg;

    invoke-direct {v3, p0}, Lcg;-><init>(Lcf;)V

    .line 63
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    new-instance v1, Lch;

    invoke-direct {v1, p0}, Lch;-><init>(Lcf;)V

    invoke-static {v2, v1}, Lqy;->a(Landroid/view/View;Lqa;)V

    .line 65
    new-instance v1, Lci;

    invoke-direct {v1}, Lci;-><init>()V

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 66
    return-object v0

    .line 60
    :cond_1
    invoke-virtual {v2, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 18
    invoke-super {p0, p1}, Lve;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-virtual {p0}, Lcf;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 22
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 23
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 24
    :cond_0
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setLayout(II)V

    .line 25
    :cond_1
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Lve;->onStart()V

    .line 38
    iget-object v0, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 40
    :cond_0
    return-void
.end method

.method public setCancelable(Z)V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lve;->setCancelable(Z)V

    .line 31
    iget-boolean v0, p0, Lcf;->a:Z

    if-eq v0, p1, :cond_0

    .line 32
    iput-boolean p1, p0, Lcf;->a:Z

    .line 33
    iget-object v0, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcf;->d:Landroid/support/design/widget/BottomSheetBehavior;

    .line 35
    iput-boolean p1, v0, Landroid/support/design/widget/BottomSheetBehavior;->e:Z

    .line 36
    :cond_0
    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 41
    invoke-super {p0, p1}, Lve;->setCanceledOnTouchOutside(Z)V

    .line 42
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcf;->a:Z

    if-nez v0, :cond_0

    .line 43
    iput-boolean v1, p0, Lcf;->a:Z

    .line 44
    :cond_0
    iput-boolean p1, p0, Lcf;->b:Z

    .line 45
    iput-boolean v1, p0, Lcf;->c:Z

    .line 46
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, p1, v0, v0}, Lcf;->a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    invoke-super {p0, v0}, Lve;->setContentView(Landroid/view/View;)V

    .line 17
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 26
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcf;->a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    invoke-super {p0, v0}, Lve;->setContentView(Landroid/view/View;)V

    .line 27
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcf;->a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    invoke-super {p0, v0}, Lve;->setContentView(Landroid/view/View;)V

    .line 29
    return-void
.end method
