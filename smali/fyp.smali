.class final Lfyp;
.super Lfwq;
.source "PG"


# static fields
.field private static volatile h:Lfyp;


# instance fields
.field public final b:Lgax;

.field public final d:Lfxe;

.field public volatile e:Ljava/util/concurrent/ScheduledFuture;

.field public final f:Lfxb;

.field public final g:Lfxc;

.field private i:D

.field private j:Lgcr;

.field private k:Lgbw;

.field private l:I

.field private m:Ljava/util/concurrent/locks/ReentrantLock;

.field private n:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lfxe;DLgcr;Lgbw;Lgax;)V
    .locals 2

    .prologue
    .line 1
    sget v0, Lmg$c;->D:I

    invoke-direct {p0, p1, p2, p8, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 2
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lfyp;->m:Ljava/util/concurrent/locks/ReentrantLock;

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lfyp;->n:Ljava/util/concurrent/atomic/AtomicLong;

    .line 4
    new-instance v0, Lfyq;

    invoke-direct {v0, p0}, Lfyq;-><init>(Lfyp;)V

    iput-object v0, p0, Lfyp;->f:Lfxb;

    .line 5
    new-instance v0, Lfys;

    invoke-direct {v0, p0}, Lfys;-><init>(Lfyp;)V

    iput-object v0, p0, Lfyp;->g:Lfxc;

    .line 6
    iput-object p3, p0, Lfyp;->d:Lfxe;

    .line 7
    iput-wide p4, p0, Lfyp;->i:D

    .line 8
    invoke-static {p6}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcr;

    iput-object v0, p0, Lfyp;->j:Lgcr;

    .line 9
    invoke-static {p7}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbw;

    iput-object v0, p0, Lfyp;->k:Lgbw;

    .line 10
    invoke-static {p8}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgax;

    iput-object v0, p0, Lfyp;->b:Lgax;

    .line 11
    return-void
.end method

.method static declared-synchronized a(Lgdc;Landroid/app/Application;Lgax;Landroid/content/SharedPreferences;D)Lfyp;
    .locals 10

    .prologue
    .line 12
    const-class v9, Lfyp;

    monitor-enter v9

    :try_start_0
    sget-object v0, Lfyp;->h:Lfyp;

    if-nez v0, :cond_0

    .line 13
    invoke-static {p1}, Lfyn;->b(Landroid/content/Context;)Lgax;

    move-result-object v0

    invoke-interface {v0}, Lgax;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyn;

    .line 14
    iget-object v0, v0, Lfyn;->a:Ljava/lang/String;

    .line 16
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    move v1, v0

    .line 17
    :goto_0
    new-instance v0, Lfyp;

    .line 18
    invoke-static {p1}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v3

    new-instance v6, Lgcr;

    invoke-direct {v6, p3, v1}, Lgcr;-><init>(Landroid/content/SharedPreferences;I)V

    new-instance v7, Lgbw;

    invoke-direct {v7}, Lgbw;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p4

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lfyp;-><init>(Lgdc;Landroid/app/Application;Lfxe;DLgcr;Lgbw;Lgax;)V

    sput-object v0, Lfyp;->h:Lfyp;

    .line 19
    :cond_0
    sget-object v0, Lfyp;->h:Lfyp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v9

    return-object v0

    .line 16
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 12
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method private static a(Ljava/io/File;[B)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 280
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 281
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 282
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    const/4 v0, 0x1

    .line 288
    :cond_0
    :goto_0
    return v0

    .line 284
    :catch_0
    move-exception v1

    .line 285
    const-string v2, "MiniHeapDumpMetric"

    const-string v3, "Failed to write mini heap dump to file."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v1, v4}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method


# virtual methods
.method final a(I)V
    .locals 20

    .prologue
    .line 24
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->j:Lgcr;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lgcr;->a(I)V

    .line 26
    sget-object v2, Lgat;->a:Lgat;

    .line 28
    iget-boolean v2, v2, Lgat;->c:Z

    .line 29
    if-nez v2, :cond_1

    .line 30
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->n:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 31
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {}, Lfmk;->i()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 32
    :goto_0
    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lfyp;->j:Lgcr;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lfyp;->i:D

    .line 34
    iget-object v2, v3, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v6, 0x64

    if-ne v2, v6, :cond_3

    iget-wide v6, v3, Lgcr;->b:D

    iget-object v2, v3, Lgcr;->a:Ljava/util/ArrayList;

    .line 35
    invoke-static {v2}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v8, v2

    mul-double/2addr v6, v8

    iget-object v2, v3, Lgcr;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v8, v2

    cmpg-double v2, v6, v8

    if-gtz v2, :cond_3

    .line 37
    iget-object v2, v3, Lgcr;->a:Ljava/util/ArrayList;

    iget-object v3, v3, Lgcr;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Integer;

    .line 38
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 39
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    array-length v6, v2

    int-to-double v6, v6

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 40
    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 41
    move/from16 v0, p1

    if-gt v2, v0, :cond_2

    const/4 v2, 0x1

    .line 43
    :goto_1
    if-eqz v2, :cond_5

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 45
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->n:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Lfmk;->i()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 48
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 49
    invoke-static {v2}, Lfmk;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-static {v2}, Landroid/os/Debug;->dumpHprofData(Ljava/lang/String;)V

    .line 51
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lfyp;->l:I

    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 55
    invoke-static {v2}, Lfmk;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    .line 57
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 58
    invoke-static {v2}, Lfms;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v6

    .line 60
    new-instance v7, Lhsm;

    invoke-direct {v7}, Lhsm;-><init>()V

    .line 61
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->j:Lgcr;

    .line 62
    iget-object v5, v2, Lgcr;->a:Ljava/util/ArrayList;

    .line 64
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [I

    iput-object v2, v7, Lhsm;->c:[I

    .line 65
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 66
    iget-object v8, v7, Lhsm;->c:[I

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v8, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 67
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 31
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 41
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 42
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 68
    :cond_4
    :try_start_1
    new-instance v8, Lhtd;

    invoke-direct {v8}, Lhtd;-><init>()V

    .line 69
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->k:Lgbw;

    .line 70
    invoke-static {v4}, Lgbx;->a(Ljava/io/File;)Lgbx;

    move-result-object v9

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "java.lang.Class"

    aput-object v5, v3, v4

    .line 73
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 74
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 75
    invoke-static {v9, v2, v3, v4}, Lgbt;->a(Lgbx;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Lgby;

    move-result-object v4

    .line 77
    new-instance v10, Ljava/util/ArrayList;

    .line 78
    iget-object v2, v4, Lgby;->a:Lgcb;

    .line 80
    iget v2, v2, Lgcb;->a:I

    .line 81
    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    new-instance v11, Lgbz;

    invoke-direct {v11}, Lgbz;-><init>()V

    .line 84
    iget-object v2, v4, Lgby;->a:Lgcb;

    .line 85
    invoke-virtual {v2}, Lgcb;->b()Lgcc;

    move-result-object v3

    :goto_3
    invoke-virtual {v3}, Lgcc;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 87
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 88
    check-cast v2, Lgbq;

    .line 90
    iget v5, v3, Lgcc;->a:I

    .line 91
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v5, v12}, Lgbz;->a(II)I

    .line 92
    new-instance v5, Lhra;

    invoke-direct {v5}, Lhra;-><init>()V

    .line 93
    invoke-virtual {v2, v9}, Lgbq;->b(Lgbx;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lhra;->b:Ljava/lang/String;

    .line 94
    invoke-interface {v10, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 228
    :catch_0
    move-exception v2

    .line 229
    :try_start_2
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 230
    const-string v3, "MiniHeapDumpMetric"

    const-string v4, "Ran out of memory serializing heap dump"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v2, v5}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 231
    const/4 v2, 0x2

    iput v2, v7, Lhsm;->a:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 232
    :try_start_3
    new-instance v2, Lhtd;

    invoke-direct {v2}, Lhtd;-><init>()V

    .line 233
    new-instance v3, Lhso;

    invoke-direct {v3}, Lhso;-><init>()V

    iput-object v3, v2, Lhtd;->h:Lhso;

    .line 234
    iget-object v3, v2, Lhtd;->h:Lhso;

    new-instance v4, Lhsp;

    invoke-direct {v4}, Lhsp;-><init>()V

    iput-object v4, v3, Lhso;->c:Lhsp;

    .line 235
    iget-object v3, v2, Lhtd;->h:Lhso;

    iget-object v3, v3, Lhso;->c:Lhsp;

    iput-object v7, v3, Lhsp;->b:Lhsm;

    .line 236
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfyp;->a(Lhtd;)V

    .line 254
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 255
    invoke-static {v2}, Lfmk;->d(Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 258
    invoke-static {v2}, Lfmk;->d(Landroid/content/Context;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 272
    :cond_5
    :goto_5
    return-void

    .line 97
    :cond_6
    :try_start_4
    iget-object v2, v4, Lgby;->a:Lgcb;

    .line 98
    invoke-virtual {v2}, Lgcb;->b()Lgcc;

    move-result-object v5

    :goto_6
    invoke-virtual {v5}, Lgcc;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 100
    iget-object v2, v5, Lgcc;->b:Ljava/lang/Object;

    .line 101
    check-cast v2, Lgbq;

    .line 103
    iget v3, v5, Lgcc;->a:I

    .line 104
    invoke-virtual {v11, v3}, Lgbz;->b(I)I

    move-result v3

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhra;

    .line 106
    iget-object v12, v2, Lgbq;->f:Lgbq;

    .line 107
    if-eqz v12, :cond_7

    .line 109
    iget-object v2, v2, Lgbq;->f:Lgbq;

    .line 110
    invoke-virtual {v2, v9}, Lgbq;->d(Lgbx;)I

    move-result v2

    invoke-virtual {v11, v2}, Lgbz;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v3, Lhra;->a:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_6

    .line 238
    :catch_1
    move-exception v2

    .line 239
    :try_start_5
    const-string v3, "MiniHeapDumpMetric"

    const-string v4, "Error serializing heap dump"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v2, v5}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 240
    const/4 v2, 0x0

    iput v2, v7, Lhsm;->a:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 241
    :try_start_6
    new-instance v2, Lhtd;

    invoke-direct {v2}, Lhtd;-><init>()V

    .line 242
    new-instance v3, Lhso;

    invoke-direct {v3}, Lhso;-><init>()V

    iput-object v3, v2, Lhtd;->h:Lhso;

    .line 243
    iget-object v3, v2, Lhtd;->h:Lhso;

    new-instance v4, Lhsp;

    invoke-direct {v4}, Lhsp;-><init>()V

    iput-object v4, v3, Lhso;->c:Lhsp;

    .line 244
    iget-object v3, v2, Lhtd;->h:Lhso;

    iget-object v3, v3, Lhso;->c:Lhsp;

    iput-object v7, v3, Lhsp;->b:Lhsm;

    .line 245
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfyp;->a(Lhtd;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    .line 261
    :catch_2
    move-exception v2

    .line 262
    :try_start_7
    const-string v3, "MiniHeapDumpMetric"

    const-string v4, "Failed to dump hprof data"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v2, v5}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 265
    invoke-static {v2}, Lfmk;->d(Landroid/content/Context;)V

    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lfyp;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_5

    .line 111
    :cond_7
    const/4 v2, 0x0

    :try_start_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v3, Lhra;->a:Ljava/lang/Integer;
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_6

    .line 247
    :catchall_0
    move-exception v2

    :try_start_9
    new-instance v3, Lhtd;

    invoke-direct {v3}, Lhtd;-><init>()V

    .line 248
    new-instance v4, Lhso;

    invoke-direct {v4}, Lhso;-><init>()V

    iput-object v4, v3, Lhtd;->h:Lhso;

    .line 249
    iget-object v4, v3, Lhtd;->h:Lhso;

    new-instance v5, Lhsp;

    invoke-direct {v5}, Lhsp;-><init>()V

    iput-object v5, v4, Lhso;->c:Lhsp;

    .line 250
    iget-object v4, v3, Lhtd;->h:Lhso;

    iget-object v4, v4, Lhso;->c:Lhsp;

    iput-object v7, v4, Lhsp;->b:Lhsm;

    .line 251
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lfyp;->a(Lhtd;)V

    .line 252
    throw v2
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 268
    :catchall_1
    move-exception v2

    .line 269
    move-object/from16 v0, p0

    iget-object v3, v0, Lfwq;->a:Landroid/app/Application;

    .line 270
    invoke-static {v3}, Lfmk;->d(Landroid/content/Context;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v3, v0, Lfyp;->m:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 113
    :cond_8
    :try_start_a
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 114
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 115
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 117
    iget-object v2, v4, Lgby;->b:Lgcb;

    .line 118
    invoke-virtual {v2}, Lgcb;->b()Lgcc;

    move-result-object v3

    :cond_9
    :goto_7
    invoke-virtual {v3}, Lgcc;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 120
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 121
    instance-of v2, v2, Lgbr;

    if-eqz v2, :cond_a

    .line 123
    iget v2, v3, Lgcc;->a:I

    .line 124
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v11, v2, v5}, Lgbz;->a(II)I

    .line 126
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 127
    check-cast v2, Lgbr;

    .line 128
    new-instance v5, Lhrb;

    invoke-direct {v5}, Lhrb;-><init>()V

    .line 129
    iget-object v2, v2, Lgbr;->a:Lgbq;

    invoke-virtual {v2, v9}, Lgbq;->d(Lgbx;)I

    move-result v2

    invoke-virtual {v11, v2}, Lgbz;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v5, Lhrb;->a:Ljava/lang/Integer;

    .line 130
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 132
    :cond_a
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 133
    instance-of v2, v2, Lgbp;

    if-eqz v2, :cond_b

    .line 135
    iget v2, v3, Lgcc;->a:I

    .line 136
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v11, v2, v5}, Lgbz;->a(II)I

    .line 138
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 139
    check-cast v2, Lgbp;

    .line 140
    new-instance v5, Lhqt;

    invoke-direct {v5}, Lhqt;-><init>()V

    .line 141
    iget-object v2, v2, Lgbp;->a:Lgbq;

    invoke-virtual {v2, v9}, Lgbq;->d(Lgbx;)I

    move-result v2

    invoke-virtual {v11, v2}, Lgbz;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v5, Lhqt;->a:Ljava/lang/Integer;

    .line 142
    invoke-interface {v13, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 144
    :cond_b
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 145
    instance-of v2, v2, Lgbv;

    if-eqz v2, :cond_9

    .line 147
    iget v2, v3, Lgcc;->a:I

    .line 148
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v11, v2, v5}, Lgbz;->a(II)I

    .line 151
    iget-object v2, v3, Lgcc;->b:Ljava/lang/Object;

    .line 152
    check-cast v2, Lgbv;

    .line 153
    new-instance v5, Lhsr;

    invoke-direct {v5}, Lhsr;-><init>()V

    .line 155
    iget v15, v2, Lgbv;->h:I

    .line 156
    iget v0, v9, Lgbx;->b:I

    move/from16 v16, v0

    .line 157
    add-int v15, v15, v16

    add-int/lit8 v15, v15, 0x4

    add-int/lit8 v15, v15, 0x4

    .line 158
    iget-object v0, v9, Lgbx;->a:Ljava/nio/ByteBuffer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v15

    .line 159
    iput v15, v5, Lhsr;->a:I

    .line 161
    invoke-virtual {v2, v9}, Lgbv;->a(Lgbx;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v5, Lhsr;->b:Ljava/lang/Integer;

    .line 162
    invoke-interface {v14, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 165
    :cond_c
    iget-object v15, v4, Lgby;->b:Lgcb;

    .line 167
    invoke-virtual {v15}, Lgcb;->b()Lgcc;

    move-result-object v16

    :cond_d
    :goto_8
    invoke-virtual/range {v16 .. v16}, Lgcc;->a()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 169
    move-object/from16 v0, v16

    iget-object v2, v0, Lgcc;->b:Ljava/lang/Object;

    .line 170
    check-cast v2, Lgbs;

    .line 171
    instance-of v3, v2, Lgbv;

    if-nez v3, :cond_d

    .line 172
    invoke-virtual {v2, v9}, Lgbs;->a(Lgbx;)I

    move-result v3

    new-array v0, v3, [I

    move-object/from16 v17, v0

    .line 173
    const/4 v4, 0x0

    .line 174
    const/4 v3, 0x0

    move v5, v3

    :goto_9
    move-object/from16 v0, v17

    array-length v3, v0

    if-ge v5, v3, :cond_11

    .line 175
    invoke-virtual {v2, v9, v5}, Lgbs;->a(Lgbx;I)I

    move-result v18

    .line 176
    if-eqz v18, :cond_17

    .line 177
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lgcb;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgbs;

    .line 178
    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lgbz;->b(I)I

    move-result v18

    .line 179
    if-eqz v3, :cond_17

    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_17

    .line 180
    instance-of v0, v3, Lgbq;

    move/from16 v19, v0

    if-eqz v19, :cond_e

    .line 181
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v18, v18, 0x1

    aput v18, v17, v4

    .line 189
    :goto_a
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_9

    .line 182
    :cond_e
    instance-of v0, v3, Lgbr;

    move/from16 v19, v0

    if-eqz v19, :cond_f

    .line 183
    add-int/lit8 v3, v4, 0x1

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    add-int/lit8 v18, v18, 0x1

    aput v18, v17, v4

    goto :goto_a

    .line 184
    :cond_f
    instance-of v0, v3, Lgbp;

    move/from16 v19, v0

    if-eqz v19, :cond_10

    .line 185
    add-int/lit8 v3, v4, 0x1

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    add-int/lit8 v18, v18, 0x1

    aput v18, v17, v4

    goto :goto_a

    .line 186
    :cond_10
    instance-of v3, v3, Lgbv;

    if-eqz v3, :cond_17

    .line 187
    add-int/lit8 v3, v4, 0x1

    .line 188
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    add-int/lit8 v18, v18, 0x1

    aput v18, v17, v4

    goto :goto_a

    .line 190
    :cond_11
    move-object/from16 v0, v17

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v3

    .line 192
    move-object/from16 v0, v16

    iget v4, v0, Lgcc;->a:I

    .line 193
    invoke-virtual {v11, v4}, Lgbz;->b(I)I

    move-result v4

    .line 194
    if-ltz v4, :cond_d

    .line 195
    instance-of v5, v2, Lgbr;

    if-eqz v5, :cond_12

    .line 196
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhrb;

    iput-object v3, v2, Lhrb;->b:[I

    goto/16 :goto_8

    .line 197
    :cond_12
    instance-of v5, v2, Lgbq;

    if-eqz v5, :cond_13

    .line 198
    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhra;

    iput-object v3, v2, Lhra;->c:[I

    goto/16 :goto_8

    .line 199
    :cond_13
    instance-of v2, v2, Lgbp;

    if-eqz v2, :cond_d

    .line 200
    invoke-interface {v13, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhqt;

    iput-object v3, v2, Lhqt;->b:[I

    goto/16 :goto_8

    .line 202
    :cond_14
    new-instance v3, Lhsl;

    invoke-direct {v3}, Lhsl;-><init>()V

    .line 203
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lhra;

    invoke-interface {v10, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lhra;

    iput-object v2, v3, Lhsl;->a:[Lhra;

    .line 204
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lhrb;

    invoke-interface {v12, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lhrb;

    iput-object v2, v3, Lhsl;->b:[Lhrb;

    .line 205
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lhqt;

    invoke-interface {v13, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lhqt;

    iput-object v2, v3, Lhsl;->c:[Lhqt;

    .line 207
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lhsr;

    invoke-interface {v14, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lhsr;

    iput-object v2, v3, Lhsl;->d:[Lhsr;

    .line 210
    move-object/from16 v0, p0

    iget v2, v0, Lfyp;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v3, Lhsl;->e:Ljava/lang/Integer;

    .line 211
    iput-object v3, v8, Lhtd;->t:Lhsl;

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lfwq;->a:Landroid/app/Application;

    .line 214
    invoke-static {v2}, Lfyn;->b(Landroid/content/Context;)Lgax;

    move-result-object v2

    invoke-interface {v2}, Lgax;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfyn;

    invoke-virtual {v2, v8}, Lfyn;->a(Lhtd;)Lhtd;

    .line 215
    invoke-static {v8}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v2

    .line 216
    array-length v3, v2

    div-int/lit16 v3, v3, 0x3e8

    .line 217
    const/16 v4, 0x2710

    if-le v3, v4, :cond_16

    .line 218
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v7, Lhsm;->b:Ljava/lang/Integer;

    .line 219
    const-string v2, "MiniHeapDumpMetric"

    const-string v4, "Serialized heap dump too large. serializedSizeKb = %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v2, v4, v5}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 222
    :cond_15
    :goto_b
    :try_start_b
    new-instance v2, Lhtd;

    invoke-direct {v2}, Lhtd;-><init>()V

    .line 223
    new-instance v3, Lhso;

    invoke-direct {v3}, Lhso;-><init>()V

    iput-object v3, v2, Lhtd;->h:Lhso;

    .line 224
    iget-object v3, v2, Lhtd;->h:Lhso;

    new-instance v4, Lhsp;

    invoke-direct {v4}, Lhsp;-><init>()V

    iput-object v4, v3, Lhso;->c:Lhsp;

    .line 225
    iget-object v3, v2, Lhtd;->h:Lhso;

    iget-object v3, v3, Lhso;->c:Lhsp;

    iput-object v7, v3, Lhsp;->b:Lhsm;

    .line 226
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfyp;->a(Lhtd;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_4

    .line 220
    :cond_16
    :try_start_c
    invoke-static {v6, v2}, Lfyp;->a(Ljava/io/File;[B)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 221
    const-string v2, "MiniHeapDumpMetric"

    const-string v3, "Successfully saved serialized heap dump"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_b

    :cond_17
    move v3, v4

    goto/16 :goto_a
.end method

.method final c()V
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lfyp;->d()V

    .line 274
    iget-object v0, p0, Lfyp;->d:Lfxe;

    iget-object v1, p0, Lfyp;->f:Lfxb;

    invoke-virtual {v0, v1}, Lfxe;->b(Lfwt;)V

    .line 275
    iget-object v0, p0, Lfyp;->d:Lfxe;

    iget-object v1, p0, Lfyp;->g:Lfxc;

    invoke-virtual {v0, v1}, Lfxe;->b(Lfwt;)V

    .line 277
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 278
    invoke-static {v0}, Lfmk;->d(Landroid/content/Context;)V

    .line 279
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lfyp;->e:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lfyp;->e:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lfyp;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 23
    :cond_0
    return-void
.end method
