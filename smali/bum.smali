.class public final Lbum;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcbu;
.implements Lcde;


# instance fields
.field public final a:Lcbt;

.field public final b:Lcdc;

.field private c:Landroid/content/Context;

.field private d:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcbt;Lcdc;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "AnswerScreenPresenter.constructor"

    const/4 v3, 0x0

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbum;->c:Landroid/content/Context;

    .line 4
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcbt;

    iput-object v0, p0, Lbum;->a:Lcbt;

    .line 5
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    iput-object v0, p0, Lbum;->b:Lcdc;

    .line 6
    invoke-direct {p0, p3}, Lbum;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p3, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v0}, Landroid/telecom/Call;->getCannedTextResponses()Ljava/util/List;

    move-result-object v0

    .line 9
    invoke-interface {p2, v0}, Lcbt;->a(Ljava/util/List;)V

    .line 11
    :cond_0
    invoke-static {}, Lbdf;->b()V

    .line 12
    iget-object v0, p3, Lcdc;->i:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 14
    iget-object v3, v0, Lbwg;->p:Lcca;

    .line 17
    invoke-virtual {p3}, Lcdc;->j()I

    move-result v0

    const/4 v4, 0x4

    if-eq v0, v4, :cond_1

    .line 18
    const-string v0, "AnswerProximitySensor.shouldUse"

    const-string v4, "call state is not incoming"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 36
    :goto_0
    if-eqz v0, :cond_5

    .line 37
    new-instance v0, Lcbw;

    invoke-direct {v0, p1, p3, v3}, Lcbw;-><init>(Landroid/content/Context;Lcdc;Lcca;)V

    .line 39
    :goto_1
    return-void

    .line 20
    :cond_1
    invoke-static {p1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v4, "answer_proximity_sensor_enabled"

    .line 21
    invoke-interface {v0, v4, v2}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 22
    const-string v0, "AnswerProximitySensor.shouldUse"

    const-string v4, "disabled by config"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_2
    const-class v0, Landroid/os/PowerManager;

    .line 25
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/16 v4, 0x20

    .line 26
    invoke-virtual {v0, v4}, Landroid/os/PowerManager;->isWakeLockLevelSupported(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 27
    const-string v0, "AnswerProximitySensor.shouldUse"

    const-string v4, "wake lock level not supported"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 28
    goto :goto_0

    .line 30
    :cond_3
    const-class v0, Landroid/hardware/display/DisplayManager;

    .line 31
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    .line 33
    const-string v0, "AnswerProximitySensor.shouldUse"

    const-string v4, "display is already on"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 34
    goto :goto_0

    :cond_4
    move v0, v2

    .line 35
    goto :goto_0

    .line 38
    :cond_5
    invoke-virtual {v3, v2}, Lcca;->a(Z)V

    goto :goto_1
.end method

.method private final b(Lcdc;)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lbum;->c:Landroid/content/Context;

    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    .line 129
    invoke-virtual {p1, v0}, Lcdc;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    .line 129
    :cond_0
    const/4 v0, 0x0

    .line 130
    goto :goto_0
.end method

.method private final g()V
    .locals 4

    .prologue
    .line 131
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lbum;->d:J

    .line 132
    iget-object v0, p0, Lbum;->a:Lcbt;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    check-cast v0, Lip;

    invoke-virtual {v0}, Lip;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    new-instance v0, Lbun;

    invoke-direct {v0, p0}, Lbun;-><init>(Lbum;)V

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    .line 134
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcgt;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbwg;->a(Ljava/lang/String;)Lcgt;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lbum;->a:Lcbt;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    check-cast v0, Lip;

    invoke-virtual {v0}, Lip;->h()Lit;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/InCallActivity;

    .line 125
    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {v0, p1}, Lcom/android/incallui/InCallActivity;->a(F)V

    .line 127
    :cond_1
    return-void
.end method

.method public final a(Lcdc;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lbum;->b(Lcdc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lbum;->a:Lcbt;

    .line 121
    iget-object v1, p1, Lcdc;->c:Landroid/telecom/Call;

    invoke-virtual {v1}, Landroid/telecom/Call;->getCannedTextResponses()Ljava/util/List;

    move-result-object v1

    .line 122
    invoke-interface {v0, v1}, Lcbt;->a(Ljava/util/List;)V

    .line 123
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 51
    iget-object v0, p0, Lbum;->a:Lcbt;

    invoke-interface {v0}, Lcbt;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    if-eqz p1, :cond_0

    .line 53
    iget-object v0, p0, Lbum;->c:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bI:Lbkq$a;

    iget-object v2, p0, Lbum;->b:Lcdc;

    .line 55
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 56
    iget-object v3, p0, Lbum;->b:Lcdc;

    .line 58
    iget-wide v4, v3, Lcdc;->M:J

    .line 59
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 60
    iget-object v0, p0, Lbum;->b:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->h()V

    .line 72
    :goto_0
    invoke-direct {p0}, Lbum;->g()V

    .line 73
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lbum;->c:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bH:Lbkq$a;

    iget-object v2, p0, Lbum;->b:Lcdc;

    .line 63
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 64
    iget-object v3, p0, Lbum;->b:Lcdc;

    .line 66
    iget-wide v4, v3, Lcdc;->M:J

    .line 67
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 68
    iget-object v0, p0, Lbum;->b:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    iget-object v1, p0, Lbum;->c:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcjs;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 69
    :cond_1
    if-eqz p1, :cond_2

    .line 70
    iget-object v0, p0, Lbum;->b:Lcdc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcdc;->e(I)V

    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lbum;->b:Lcdc;

    invoke-virtual {v0}, Lcdc;->D()V

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 40
    iget-wide v0, p0, Lbum;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 41
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lbum;->d:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    .line 41
    :cond_0
    const/4 v0, 0x0

    .line 42
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbum;->b:Lcdc;

    .line 45
    invoke-static {}, Lbdf;->b()V

    .line 46
    iget-object v0, v0, Lcdc;->i:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lbum;->b:Lcdc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcdc;->a(ZLjava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lbum;->g()V

    .line 50
    return-void
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Lbum;->a:Lcbt;

    invoke-interface {v0}, Lcbt;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lbum;->c:Landroid/content/Context;

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bJ:Lbkq$a;

    iget-object v2, p0, Lbum;->b:Lcdc;

    .line 77
    iget-object v2, v2, Lcdc;->b:Ljava/lang/String;

    .line 78
    iget-object v3, p0, Lbum;->b:Lcdc;

    .line 80
    iget-wide v4, v3, Lcdc;->M:J

    .line 81
    invoke-interface {v0, v1, v2, v4, v5}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 82
    iget-object v0, p0, Lbum;->b:Lcdc;

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->i()V

    .line 84
    :goto_0
    invoke-direct {p0}, Lbum;->g()V

    .line 85
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lbum;->b:Lcdc;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcdc;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    const-string v0, "AnswerScreenPresenter.onAnswerAndReleaseCall"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcct;->a:Lcct;

    .line 89
    const/4 v1, 0x3

    .line 90
    invoke-virtual {v0, v1, v3}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 92
    if-nez v0, :cond_0

    .line 93
    const-string v0, "AnswerScreenPresenter.onAnswerAndReleaseCall"

    const-string v1, "activeCall == null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-virtual {p0, v3}, Lbum;->a(Z)V

    .line 99
    :goto_0
    invoke-direct {p0}, Lbum;->g()V

    .line 100
    return-void

    .line 96
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcdc;->D:Z

    .line 97
    new-instance v1, Lbuo;

    invoke-direct {v1, p0, v0}, Lbuo;-><init>(Lbum;Lcdc;)V

    invoke-virtual {v0, v1}, Lcdc;->a(Lcdi;)V

    .line 98
    invoke-virtual {v0}, Lcdc;->B()V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 101
    sget-object v0, Lcct;->a:Lcct;

    .line 103
    const/4 v1, 0x3

    .line 104
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    .line 108
    iget v1, v0, Lcdc;->E:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcdc;->E:I

    .line 109
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 110
    sget-object v0, Lcct;->a:Lcct;

    .line 112
    const/4 v1, 0x3

    .line 113
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 117
    iget v1, v0, Lcdc;->C:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcdc;->C:I

    .line 118
    :cond_0
    return-void
.end method
