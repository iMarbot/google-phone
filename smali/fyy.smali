.class public final Lfyy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static volatile a:Lfyy;

.field private static c:Lfyy;


# instance fields
.field public final b:Lfyz;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lfyy;

    new-instance v1, Lfyu;

    invoke-direct {v1}, Lfyu;-><init>()V

    invoke-direct {v0, v1}, Lfyy;-><init>(Lfyz;)V

    .line 14
    sput-object v0, Lfyy;->c:Lfyy;

    sput-object v0, Lfyy;->a:Lfyy;

    return-void
.end method

.method private constructor <init>(Lfyz;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyz;

    iput-object v0, p0, Lfyy;->b:Lfyz;

    .line 3
    return-void
.end method

.method public static declared-synchronized a(Lfwr;)Lfyy;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 4
    const-class v1, Lfyy;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lfyy;->a:Lfyy;

    .line 5
    sget-object v3, Lfyy;->c:Lfyy;

    if-eq v2, v3, :cond_0

    const/4 v0, 0x1

    .line 6
    :cond_0
    if-eqz v0, :cond_1

    .line 7
    const-string v0, "Primes"

    const-string v2, "Primes.initialize() is called more than once. This call will be ignored."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    sget-object v0, Lfyy;->a:Lfyy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 10
    :goto_0
    monitor-exit v1

    return-object v0

    .line 9
    :cond_1
    :try_start_1
    new-instance v0, Lfyy;

    invoke-virtual {p0}, Lfwr;->a()Lfyz;

    move-result-object v2

    invoke-direct {v0, v2}, Lfyy;-><init>(Lfyz;)V

    sput-object v0, Lfyy;->a:Lfyy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 11
    :catchall_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lfyt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lfyt;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
