.class public final Laho;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$SelectionBoundsAdjuster;


# static fields
.field public static final a:Ljava/util/regex/Pattern;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Landroid/graphics/drawable/Drawable;

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Landroid/widget/TextView;

.field private O:Landroid/widget/ImageView;

.field private P:Landroid/widget/TextView;

.field private Q:Landroid/widget/ImageView;

.field private R:Landroid/widget/ImageView;

.field private S:I

.field private T:I

.field private U:I

.field private V:Z

.field private W:Z

.field private aa:I

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:Landroid/graphics/Rect;

.field public final b:Laha;

.field public c:Ljava/util/ArrayList;

.field public d:Ljava/util/ArrayList;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Landroid/widget/QuickContactBadge;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Landroid/widget/ImageView;

.field public o:Landroid/content/res/ColorStateList;

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Ljava/lang/CharSequence;

.field public t:Ljava/lang/String;

.field public u:I

.field public v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 546
    const-string v0, "([\\w-\\.]+)@((?:[\\w]+\\.)+)([a-zA-Z]{2,4})|[\\w]+"

    .line 547
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Laho;->a:Ljava/util/regex/Pattern;

    .line 548
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/16 v1, 0x10

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    .line 7
    if-ne v0, v6, :cond_1

    sget v0, Lmg$c;->e:I

    .line 8
    :goto_0
    iput v0, p0, Laho;->w:I

    .line 9
    iput v5, p0, Laho;->x:I

    .line 10
    iput v5, p0, Laho;->y:I

    .line 11
    iput v5, p0, Laho;->z:I

    .line 12
    const/4 v0, 0x4

    iput v0, p0, Laho;->A:I

    .line 13
    iput v1, p0, Laho;->B:I

    .line 14
    iput v5, p0, Laho;->C:I

    .line 15
    const/16 v0, 0x30

    iput v0, p0, Laho;->H:I

    .line 16
    iput v1, p0, Laho;->I:I

    .line 17
    iput v7, p0, Laho;->L:I

    .line 18
    const/4 v0, 0x5

    iput v0, p0, Laho;->M:I

    .line 19
    iput-boolean v6, p0, Laho;->h:Z

    .line 20
    iput v5, p0, Laho;->S:I

    .line 21
    iput-boolean v5, p0, Laho;->p:Z

    .line 22
    const/high16 v0, -0x1000000

    iput v0, p0, Laho;->ab:I

    .line 23
    iput-boolean v6, p0, Laho;->r:Z

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laho;->aj:Landroid/graphics/Rect;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Laho;->u:I

    .line 26
    iput v5, p0, Laho;->v:I

    .line 27
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lagd;->a:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 28
    sget v1, Lagd;->g:I

    iget v2, p0, Laho;->x:I

    .line 29
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Laho;->x:I

    .line 30
    sget v1, Lagd;->b:I

    .line 31
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    .line 32
    sget v1, Lagd;->e:I

    iget v2, p0, Laho;->y:I

    .line 33
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->y:I

    .line 34
    sget v1, Lagd;->f:I

    iget v2, p0, Laho;->z:I

    .line 35
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->z:I

    .line 36
    sget v1, Lagd;->p:I

    iget v2, p0, Laho;->A:I

    .line 37
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->A:I

    .line 38
    sget v1, Lagd;->q:I

    iget v2, p0, Laho;->B:I

    .line 39
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->B:I

    .line 40
    sget v1, Lagd;->o:I

    iget v2, p0, Laho;->S:I

    .line 41
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->S:I

    .line 42
    sget v1, Lagd;->r:I

    iget v2, p0, Laho;->C:I

    .line 43
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->C:I

    .line 44
    sget v1, Lagd;->s:I

    iget v2, p0, Laho;->D:I

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->D:I

    .line 46
    sget v1, Lagd;->d:I

    iget v2, p0, Laho;->M:I

    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Laho;->M:I

    .line 48
    sget v1, Lagd;->h:I

    iget v2, p0, Laho;->L:I

    .line 49
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Laho;->L:I

    .line 50
    sget v1, Lagd;->i:I

    iget v2, p0, Laho;->ab:I

    .line 51
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Laho;->ab:I

    .line 52
    sget v1, Lagd;->j:I

    .line 53
    invoke-virtual {p0}, Laho;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Laho;->E:I

    .line 55
    sget v1, Lagd;->u:I

    iget v2, p0, Laho;->H:I

    .line 56
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->H:I

    .line 57
    sget v1, Lagd;->t:I

    iget v2, p0, Laho;->I:I

    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Laho;->I:I

    .line 59
    sget v1, Lagd;->l:I

    .line 60
    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    sget v2, Lagd;->n:I

    .line 61
    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    sget v3, Lagd;->m:I

    .line 62
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    sget v4, Lagd;->k:I

    .line 63
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    .line 64
    invoke-virtual {p0, v1, v2, v3, v4}, Laho;->setPaddingRelative(IIII)V

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    new-instance v0, Laha;

    invoke-direct {v0, v6}, Laha;-><init>(I)V

    iput-object v0, p0, Laho;->b:Laha;

    .line 67
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lagd;->z:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    sget v1, Lagd;->A:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Laho;->o:Landroid/content/res/ColorStateList;

    .line 69
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    invoke-virtual {p0}, Laho;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Laho;->F:I

    .line 71
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 73
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laho;->c:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laho;->d:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    .line 76
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    const v1, 0x7f0e000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    .line 77
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Laho;->H:I

    iget v3, p0, Laho;->H:I

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 79
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    .line 80
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00c9

    invoke-static {v1, v2}, Llw;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 82
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 83
    invoke-virtual {p0, v7}, Laho;->setLayoutDirection(I)V

    .line 84
    return-void

    .line 7
    :cond_1
    sget v0, Lmg$c;->d:I

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Z)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laho;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    iput-boolean p3, p0, Laho;->f:Z

    .line 3
    return-void
.end method

.method static a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 6

    .prologue
    .line 493
    move v0, p1

    move v1, p2

    .line 497
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 498
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 504
    :goto_1
    add-int/lit8 v2, p1, -0x1

    move v4, v1

    move v3, v1

    move v1, p1

    .line 507
    :goto_2
    if-ltz v2, :cond_2

    if-lez v4, :cond_2

    .line 508
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-nez v5, :cond_0

    move v1, v2

    move v3, v4

    .line 511
    :cond_0
    add-int/lit8 v4, v4, -0x1

    .line 512
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 502
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 503
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v0

    .line 515
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_4

    if-lez v3, :cond_4

    .line 516
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v2

    .line 518
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 519
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 520
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 521
    if-lez v1, :cond_5

    .line 522
    const-string v3, "..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    :cond_5
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 525
    const-string v0, "..."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, p1

    move v1, p2

    goto :goto_1
.end method

.method static final synthetic a(Laif$a;I)V
    .locals 0

    .prologue
    .line 543
    invoke-interface {p0, p1}, Laif$a;->b(I)V

    return-void
.end method

.method private static a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 328
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic b(Laif$a;I)V
    .locals 0

    .prologue
    .line 544
    invoke-interface {p0, p1}, Laif$a;->a(I)V

    return-void
.end method

.method static final synthetic c(Laif$a;I)V
    .locals 0

    .prologue
    .line 545
    invoke-interface {p0, p1}, Laif$a;->c(I)V

    return-void
.end method

.method static e()Landroid/text/TextUtils$TruncateAt;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    return-object v0
.end method


# virtual methods
.method final a()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 329
    invoke-virtual {p0}, Laho;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 331
    iget v1, p0, Laho;->S:I

    .line 332
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 333
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 334
    return-object v0
.end method

.method public final a(ILaif$a;I)V
    .locals 5

    .prologue
    const v1, 0x7f110122

    const v2, 0x7f020179

    const/4 v3, 0x1

    .line 85
    iput p1, p0, Laho;->v:I

    .line 86
    iput p3, p0, Laho;->u:I

    .line 87
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 88
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200bc

    invoke-static {v0, v1}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 89
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 90
    const v1, 0x7f110121

    .line 91
    new-instance v0, Lahp;

    invoke-direct {v0, p2, p3}, Lahp;-><init>(Laif$a;I)V

    .line 108
    :goto_0
    iget-object v3, p0, Laho;->Q:Landroid/widget/ImageView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v1, p0, Laho;->Q:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    :goto_1
    return-void

    .line 92
    :cond_0
    if-ne p1, v3, :cond_1

    iget-boolean v0, p0, Laho;->f:Z

    if-eqz v0, :cond_1

    .line 94
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 95
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 97
    new-instance v0, Lahq;

    invoke-direct {v0, p2, p3}, Lahq;-><init>(Laif$a;I)V

    goto :goto_0

    .line 98
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 99
    sget v0, Lbbh;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lbbh;->i:I

    .line 101
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Llw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 102
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 104
    new-instance v0, Lahr;

    invoke-direct {v0, p2, p3}, Lahr;-><init>(Laif$a;I)V

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final a(Landroid/database/Cursor;I)V
    .locals 6

    .prologue
    .line 446
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 447
    invoke-virtual {p0, v0}, Laho;->a(Ljava/lang/CharSequence;)V

    .line 448
    iget-object v0, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 450
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f11011e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 451
    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 452
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 529
    invoke-virtual {p0}, Laho;->b()Landroid/widget/ImageView;

    move-result-object v0

    .line 530
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 531
    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00c8

    invoke-static {v1, v2}, Llw;->c(Landroid/content/Context;I)I

    move-result v1

    .line 532
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 533
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 534
    return-void
.end method

.method final a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 392
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    .line 393
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v0, v1, :cond_0

    .line 394
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 395
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    const/4 v2, 0x0

    .line 396
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    .line 397
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 398
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    :goto_0
    return-void

    .line 400
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 453
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 454
    iget-object v0, p0, Laho;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 455
    iget-object v0, p0, Laho;->b:Laha;

    iget-object v1, p0, Laho;->e:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Laha;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 470
    :cond_0
    :goto_0
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 471
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->j:Landroid/widget/TextView;

    .line 472
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 473
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    .line 474
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    .line 475
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 476
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    iget v1, p0, Laho;->ab:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 477
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    iget v1, p0, Laho;->E:I

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 478
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 479
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 480
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 481
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    const v1, 0x7f0e0010

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 482
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 483
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 484
    :cond_1
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    .line 485
    invoke-virtual {p0, v0, p1}, Laho;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 486
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 487
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 488
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    .line 489
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 490
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 492
    :goto_1
    return-void

    .line 456
    :cond_2
    iget-object v0, p0, Laho;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Laho;->c:Ljava/util/ArrayList;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    :goto_2
    if-ge v3, v5, :cond_3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    check-cast v1, Lahs;

    .line 459
    iget-object v6, p0, Laho;->b:Laha;

    .line 461
    iget v7, v1, Lahs;->a:I

    .line 463
    iget v1, v1, Lahs;->b:I

    .line 464
    invoke-virtual {v6, v2, v7, v1}, Laha;->a(Landroid/text/SpannableString;II)V

    goto :goto_2

    :cond_3
    move-object p1, v2

    .line 467
    goto/16 :goto_0

    .line 468
    :cond_4
    iget-object p1, p0, Laho;->s:Ljava/lang/CharSequence;

    goto/16 :goto_0

    .line 491
    :cond_5
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 348
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 349
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 350
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->N:Landroid/widget/TextView;

    .line 351
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    const v1, 0x7f12010e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 352
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 353
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 354
    :cond_0
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Laho;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 359
    :cond_1
    :goto_0
    return-void

    .line 357
    :cond_2
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Laho;->N:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 436
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 437
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 444
    :cond_0
    :goto_1
    return-void

    .line 437
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 438
    :cond_2
    if-eqz p1, :cond_0

    .line 439
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    .line 440
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Laho;->addView(Landroid/view/View;)V

    .line 441
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    const v2, 0x7f020103

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 442
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 443
    iget-object v1, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Laho;->p:Z

    .line 368
    iput-boolean p1, p0, Laho;->V:Z

    .line 369
    iput-boolean p2, p0, Laho;->W:Z

    .line 370
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Laho;->removeView(Landroid/view/View;)V

    .line 372
    iput-object v1, p0, Laho;->O:Landroid/widget/ImageView;

    .line 373
    :cond_0
    iget-object v0, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    invoke-virtual {p0, v0}, Laho;->removeView(Landroid/view/View;)V

    .line 375
    iput-object v1, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 376
    :cond_1
    return-void
.end method

.method public final adjustListItemSelectionBounds(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 322
    iget-boolean v0, p0, Laho;->r:Z

    if-eqz v0, :cond_0

    .line 323
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Laho;->aj:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 324
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Laho;->aj:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 325
    iget-object v0, p0, Laho;->aj:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 326
    iget-object v0, p0, Laho;->aj:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 327
    :cond_0
    return-void
.end method

.method public final b()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    .line 362
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    invoke-virtual {p0}, Laho;->a()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 363
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 364
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Laho;->p:Z

    .line 366
    :cond_0
    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 415
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    iget-object v0, p0, Laho;->b:Laha;

    .line 419
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    if-nez v1, :cond_2

    .line 420
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Laho;->P:Landroid/widget/TextView;

    .line 421
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 422
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    .line 423
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    .line 424
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 425
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    const v2, 0x1030046

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 426
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 427
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setActivated(Z)V

    .line 428
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Laho;->addView(Landroid/view/View;)V

    .line 429
    :cond_2
    iget-object v1, p0, Laho;->P:Landroid/widget/TextView;

    .line 430
    iget-object v2, p0, Laho;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Laha;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 432
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->b(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 433
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 434
    :cond_3
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final c()Landroid/widget/TextView;
    .locals 4

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x1

    .line 377
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 378
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->k:Landroid/widget/TextView;

    .line 379
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 380
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 381
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    .line 382
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    .line 383
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 384
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    const v1, 0x7f12015c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 385
    iget v0, p0, Laho;->w:I

    sget v1, Lmg$c;->d:I

    if-ne v0, v1, :cond_1

    .line 386
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 388
    :goto_0
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 389
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    const v1, 0x7f0e000f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 390
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 391
    :cond_0
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    return-object v0

    .line 387
    :cond_1
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    iget-object v1, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_0
.end method

.method public final d()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 403
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laho;->l:Landroid/widget/TextView;

    .line 404
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 405
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    .line 406
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    .line 407
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 408
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    const v1, 0x7f12015c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 409
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 410
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 411
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    const v1, 0x7f0e000e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 412
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 413
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Laho;->addView(Landroid/view/View;)V

    .line 414
    :cond_0
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    return-object v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Laho;->q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 346
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 347
    return-void
.end method

.method protected final drawableStateChanged()V
    .locals 2

    .prologue
    .line 335
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 336
    iget-boolean v0, p0, Laho;->q:Z

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Laho;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 338
    :cond_0
    return-void
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 340
    invoke-super {p0}, Landroid/view/ViewGroup;->jumpDrawablesToCurrentState()V

    .line 341
    iget-boolean v0, p0, Laho;->q:Z

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 343
    :cond_0
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 217
    sub-int v4, p5, p3

    .line 218
    sub-int v0, p4, p2

    .line 219
    invoke-virtual {p0}, Laho;->getPaddingLeft()I

    move-result v2

    .line 220
    invoke-virtual {p0}, Laho;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 221
    invoke-static {p0}, Lbib;->a(Landroid/view/View;)Z

    move-result v5

    .line 222
    iget-boolean v3, p0, Laho;->g:Z

    if-eqz v3, :cond_23

    .line 223
    if-eqz v5, :cond_e

    move v1, v2

    .line 226
    :goto_0
    iget-object v2, p0, Laho;->N:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Laho;->N:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    .line 228
    sub-int v2, v4, v6

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Laho;->D:I

    add-int v7, v2, v3

    .line 229
    iget-object v8, p0, Laho;->N:Landroid/widget/TextView;

    .line 230
    if-eqz v5, :cond_f

    iget v2, p0, Laho;->F:I

    sub-int v2, v0, v2

    move v3, v2

    .line 231
    :goto_1
    if-eqz v5, :cond_10

    move v2, v0

    :goto_2
    add-int/2addr v6, v7

    .line 232
    invoke-virtual {v8, v3, v7, v2, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 233
    :cond_0
    if-eqz v5, :cond_11

    .line 234
    iget v2, p0, Laho;->F:I

    sub-int/2addr v0, v2

    .line 236
    :goto_3
    iget-object v2, p0, Laho;->aj:Landroid/graphics/Rect;

    add-int v3, p2, v1

    const/4 v6, 0x0

    add-int v7, p2, v0

    invoke-virtual {v2, v3, v6, v7, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 237
    add-int v2, p2, v1

    iput v2, p0, Laho;->J:I

    .line 238
    add-int v2, p2, v0

    iput v2, p0, Laho;->K:I

    .line 239
    iget-boolean v2, p0, Laho;->g:Z

    if-eqz v2, :cond_1

    .line 240
    if-eqz v5, :cond_12

    .line 241
    iget v2, p0, Laho;->y:I

    sub-int/2addr v0, v2

    .line 243
    :cond_1
    :goto_4
    iget-boolean v2, p0, Laho;->q:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Laho;->isActivated()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 244
    iget-object v2, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Laho;->aj:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 245
    :cond_2
    iget-object v2, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    if-eqz v2, :cond_13

    iget-object v2, p0, Laho;->i:Landroid/widget/QuickContactBadge;

    .line 246
    :goto_5
    iget v3, p0, Laho;->w:I

    sget v6, Lmg$c;->d:I

    if-ne v3, v6, :cond_15

    .line 247
    if-eqz v2, :cond_14

    .line 248
    iget v3, p0, Laho;->U:I

    sub-int v3, v4, v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x0

    .line 249
    iget v6, p0, Laho;->T:I

    add-int/2addr v6, v1

    iget v7, p0, Laho;->U:I

    add-int/2addr v7, v3

    invoke-virtual {v2, v1, v3, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 250
    iget v2, p0, Laho;->T:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 260
    :cond_3
    :goto_6
    iget v2, p0, Laho;->H:I

    sub-int v2, v4, v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x0

    .line 261
    if-nez v5, :cond_18

    .line 262
    iget-object v3, p0, Laho;->Q:Landroid/widget/ImageView;

    iget v6, p0, Laho;->H:I

    sub-int v6, v0, v6

    iget v7, p0, Laho;->H:I

    add-int/2addr v7, v2

    invoke-virtual {v3, v6, v2, v0, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 264
    :goto_7
    iget v2, p0, Laho;->w:I

    sget v3, Lmg$c;->d:I

    if-ne v2, v3, :cond_19

    .line 265
    iget v2, p0, Laho;->H:I

    iget v3, p0, Laho;->I:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 267
    :goto_8
    iget v2, p0, Laho;->aa:I

    iget v3, p0, Laho;->ai:I

    add-int/2addr v2, v3

    iget v3, p0, Laho;->af:I

    add-int/2addr v2, v3

    iget v3, p0, Laho;->ag:I

    add-int/2addr v2, v3

    .line 268
    sub-int v2, v4, v2

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Laho;->D:I

    add-int/2addr v3, v2

    .line 269
    const/4 v2, 0x0

    .line 270
    iget-object v4, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-static {v4}, Laho;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 271
    iget-object v2, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 272
    iget v4, p0, Laho;->w:I

    sget v6, Lmg$c;->d:I

    if-ne v4, v6, :cond_1a

    .line 273
    iget-object v4, p0, Laho;->R:Landroid/widget/ImageView;

    sub-int v6, v0, v2

    iget v7, p0, Laho;->aa:I

    add-int/2addr v7, v3

    invoke-virtual {v4, v6, v3, v0, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 275
    :cond_4
    :goto_9
    iget-object v4, p0, Laho;->j:Landroid/widget/TextView;

    invoke-static {v4}, Laho;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 278
    iget v4, p0, Laho;->w:I

    sget v6, Lmg$c;->d:I

    if-ne v4, v6, :cond_1b

    .line 279
    iget-object v4, p0, Laho;->j:Landroid/widget/TextView;

    sub-int v2, v0, v2

    iget v6, p0, Laho;->aa:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v1, v3, v2, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 281
    :cond_5
    :goto_a
    iget-object v2, p0, Laho;->j:Landroid/widget/TextView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 282
    :cond_6
    iget v2, p0, Laho;->aa:I

    add-int/2addr v3, v2

    .line 283
    :cond_7
    if-eqz v5, :cond_1c

    .line 285
    iget-object v2, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 286
    iget-object v2, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 287
    iget-object v4, p0, Laho;->n:Landroid/widget/ImageView;

    sub-int v6, v0, v2

    iget v7, p0, Laho;->ag:I

    add-int/2addr v7, v3

    invoke-virtual {v4, v6, v3, v0, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 288
    iget v4, p0, Laho;->A:I

    add-int/2addr v2, v4

    sub-int v2, v0, v2

    .line 289
    :goto_b
    iget-object v4, p0, Laho;->m:Landroid/widget/TextView;

    invoke-static {v4}, Laho;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 290
    iget-object v4, p0, Laho;->m:Landroid/widget/TextView;

    iget v6, p0, Laho;->ag:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v1, v3, v2, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 299
    :cond_8
    :goto_c
    iget-object v2, p0, Laho;->m:Landroid/widget/TextView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 300
    :cond_9
    iget v2, p0, Laho;->ag:I

    add-int/2addr v2, v3

    .line 302
    :goto_d
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    invoke-static {v3}, Laho;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 303
    if-nez v5, :cond_1d

    .line 304
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    iget v4, p0, Laho;->ai:I

    add-int/2addr v4, v2

    iget v6, p0, Laho;->ad:I

    sub-int/2addr v4, v6

    iget v6, p0, Laho;->ai:I

    add-int/2addr v6, v2

    invoke-virtual {v3, v1, v4, v0, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 305
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Laho;->z:I

    add-int/2addr v3, v4

    add-int/2addr v3, v1

    move v9, v3

    move v3, v0

    move v0, v9

    .line 311
    :goto_e
    iget-object v4, p0, Laho;->l:Landroid/widget/TextView;

    invoke-static {v4}, Laho;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 312
    if-nez v5, :cond_1e

    .line 313
    iget-object v4, p0, Laho;->l:Landroid/widget/TextView;

    iget v5, p0, Laho;->ai:I

    add-int/2addr v5, v2

    iget v6, p0, Laho;->ae:I

    sub-int/2addr v5, v6

    iget v6, p0, Laho;->ai:I

    add-int/2addr v6, v2

    invoke-virtual {v4, v0, v5, v3, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 317
    :cond_a
    :goto_f
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 318
    :cond_b
    iget v0, p0, Laho;->ai:I

    add-int/2addr v2, v0

    .line 319
    :cond_c
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 320
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    iget v4, p0, Laho;->af:I

    add-int/2addr v4, v2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 321
    :cond_d
    return-void

    .line 225
    :cond_e
    const/4 v0, 0x0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_0

    :cond_f
    move v3, v1

    .line 230
    goto/16 :goto_1

    .line 231
    :cond_10
    iget v2, p0, Laho;->F:I

    add-int/2addr v2, v1

    goto/16 :goto_2

    .line 235
    :cond_11
    iget v2, p0, Laho;->F:I

    add-int/2addr v1, v2

    goto/16 :goto_3

    .line 242
    :cond_12
    iget v2, p0, Laho;->y:I

    add-int/2addr v1, v2

    goto/16 :goto_4

    .line 245
    :cond_13
    iget-object v2, p0, Laho;->O:Landroid/widget/ImageView;

    goto/16 :goto_5

    .line 251
    :cond_14
    iget-boolean v2, p0, Laho;->V:Z

    if-eqz v2, :cond_3

    .line 252
    iget v2, p0, Laho;->T:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto/16 :goto_6

    .line 253
    :cond_15
    if-eqz v2, :cond_17

    .line 254
    iget v3, p0, Laho;->U:I

    sub-int v3, v4, v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x0

    .line 255
    iget v6, p0, Laho;->T:I

    sub-int v6, v0, v6

    iget v7, p0, Laho;->U:I

    add-int/2addr v7, v3

    invoke-virtual {v2, v6, v3, v0, v7}, Landroid/view/View;->layout(IIII)V

    .line 256
    iget v2, p0, Laho;->T:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 259
    :cond_16
    :goto_10
    iget v2, p0, Laho;->C:I

    add-int/2addr v1, v2

    goto/16 :goto_6

    .line 257
    :cond_17
    iget-boolean v2, p0, Laho;->V:Z

    if-eqz v2, :cond_16

    .line 258
    iget v2, p0, Laho;->T:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    goto :goto_10

    .line 263
    :cond_18
    iget-object v3, p0, Laho;->Q:Landroid/widget/ImageView;

    iget v6, p0, Laho;->H:I

    add-int/2addr v6, v1

    iget v7, p0, Laho;->H:I

    add-int/2addr v7, v2

    invoke-virtual {v3, v1, v2, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_7

    .line 266
    :cond_19
    iget v2, p0, Laho;->H:I

    iget v3, p0, Laho;->I:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto/16 :goto_8

    .line 274
    :cond_1a
    iget-object v4, p0, Laho;->R:Landroid/widget/ImageView;

    add-int v6, v1, v2

    iget v7, p0, Laho;->aa:I

    add-int/2addr v7, v3

    invoke-virtual {v4, v1, v3, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_9

    .line 280
    :cond_1b
    iget-object v4, p0, Laho;->j:Landroid/widget/TextView;

    add-int/2addr v2, v1

    iget v6, p0, Laho;->aa:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v0, v6}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_a

    .line 293
    :cond_1c
    iget-object v2, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-static {v2}, Laho;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 294
    iget-object v2, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 295
    iget-object v4, p0, Laho;->n:Landroid/widget/ImageView;

    add-int v6, v1, v2

    iget v7, p0, Laho;->ag:I

    add-int/2addr v7, v3

    invoke-virtual {v4, v1, v3, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 296
    iget v4, p0, Laho;->A:I

    add-int/2addr v2, v4

    add-int/2addr v2, v1

    .line 297
    :goto_11
    iget-object v4, p0, Laho;->m:Landroid/widget/TextView;

    invoke-static {v4}, Laho;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 298
    iget-object v4, p0, Laho;->m:Landroid/widget/TextView;

    iget v6, p0, Laho;->ag:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v0, v6}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_c

    .line 306
    :cond_1d
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    .line 307
    iget-object v4, p0, Laho;->k:Landroid/widget/TextView;

    iget-object v6, p0, Laho;->k:Landroid/widget/TextView;

    .line 308
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v0, v6

    iget v7, p0, Laho;->ai:I

    add-int/2addr v7, v2

    iget v8, p0, Laho;->ad:I

    sub-int/2addr v7, v8

    iget v8, p0, Laho;->ai:I

    add-int/2addr v8, v2

    .line 309
    invoke-virtual {v4, v6, v7, v0, v8}, Landroid/widget/TextView;->layout(IIII)V

    .line 310
    iget-object v4, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    iget v6, p0, Laho;->z:I

    add-int/2addr v4, v6

    sub-int/2addr v0, v4

    move v9, v3

    move v3, v0

    move v0, v9

    goto/16 :goto_e

    .line 314
    :cond_1e
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    iget-object v4, p0, Laho;->l:Landroid/widget/TextView;

    .line 315
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v3, v4

    iget v5, p0, Laho;->ai:I

    add-int/2addr v5, v2

    iget v6, p0, Laho;->ae:I

    sub-int/2addr v5, v6

    iget v6, p0, Laho;->ai:I

    add-int/2addr v6, v2

    .line 316
    invoke-virtual {v0, v4, v5, v3, v6}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_f

    :cond_1f
    move v3, v0

    move v0, v1

    goto/16 :goto_e

    :cond_20
    move v2, v3

    goto/16 :goto_d

    :cond_21
    move v2, v1

    goto :goto_11

    :cond_22
    move v2, v0

    goto/16 :goto_b

    :cond_23
    move v0, v1

    move v1, v2

    goto/16 :goto_3
.end method

.method protected final onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 113
    invoke-static {v2, p1}, Laho;->resolveSize(II)I

    move-result v4

    .line 114
    iget v5, p0, Laho;->x:I

    .line 115
    iput v2, p0, Laho;->aa:I

    .line 116
    iput v2, p0, Laho;->ac:I

    .line 117
    iput v2, p0, Laho;->ad:I

    .line 118
    iput v2, p0, Laho;->ae:I

    .line 119
    iput v2, p0, Laho;->ai:I

    .line 120
    iput v2, p0, Laho;->af:I

    .line 121
    iput v2, p0, Laho;->ag:I

    .line 122
    iput v2, p0, Laho;->ah:I

    .line 124
    iget-boolean v0, p0, Laho;->p:Z

    if-nez v0, :cond_2

    .line 126
    iget v0, p0, Laho;->S:I

    .line 127
    iput v0, p0, Laho;->U:I

    iput v0, p0, Laho;->T:I

    .line 128
    iget-boolean v0, p0, Laho;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Laho;->O:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 129
    iget-boolean v0, p0, Laho;->V:Z

    if-nez v0, :cond_0

    .line 130
    iput v2, p0, Laho;->T:I

    .line 131
    :cond_0
    iget-boolean v0, p0, Laho;->W:Z

    if-nez v0, :cond_1

    .line 132
    iput v2, p0, Laho;->U:I

    .line 133
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Laho;->p:Z

    .line 134
    :cond_2
    iget v0, p0, Laho;->T:I

    if-gtz v0, :cond_3

    iget-boolean v0, p0, Laho;->V:Z

    if-eqz v0, :cond_e

    .line 136
    :cond_3
    invoke-virtual {p0}, Laho;->getPaddingLeft()I

    move-result v0

    sub-int v0, v4, v0

    .line 137
    invoke-virtual {p0}, Laho;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Laho;->T:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v1, v3

    sub-int/2addr v0, v1

    .line 139
    :goto_0
    iget-boolean v1, p0, Laho;->g:Z

    if-eqz v1, :cond_4

    .line 140
    iget v1, p0, Laho;->F:I

    iget v3, p0, Laho;->y:I

    add-int/2addr v1, v3

    sub-int/2addr v0, v1

    .line 141
    :cond_4
    iget v1, p0, Laho;->H:I

    iget v3, p0, Laho;->I:I

    add-int/2addr v1, v3

    sub-int v1, v0, v1

    .line 142
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 144
    iget v0, p0, Laho;->w:I

    sget v3, Lmg$c;->d:I

    if-eq v0, v3, :cond_12

    .line 145
    iget v0, p0, Laho;->C:I

    sub-int v0, v1, v0

    .line 146
    :goto_1
    iget-object v3, p0, Laho;->j:Landroid/widget/TextView;

    .line 147
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 148
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 149
    invoke-virtual {v3, v0, v6}, Landroid/widget/TextView;->measure(II)V

    .line 150
    iget-object v0, p0, Laho;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Laho;->aa:I

    .line 151
    :cond_5
    iget-object v0, p0, Laho;->l:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 152
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 153
    iget v0, p0, Laho;->z:I

    sub-int v0, v1, v0

    .line 154
    iget v3, p0, Laho;->M:I

    mul-int/2addr v3, v0

    iget v6, p0, Laho;->M:I

    iget v7, p0, Laho;->L:I

    add-int/2addr v6, v7

    div-int/2addr v3, v6

    .line 155
    iget v6, p0, Laho;->L:I

    mul-int/2addr v0, v6

    iget v6, p0, Laho;->M:I

    iget v7, p0, Laho;->L:I

    add-int/2addr v6, v7

    div-int/2addr v0, v6

    .line 163
    :goto_2
    iget-object v6, p0, Laho;->l:Landroid/widget/TextView;

    invoke-static {v6}, Laho;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 164
    iget-object v6, p0, Laho;->l:Landroid/widget/TextView;

    .line 165
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 166
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 167
    invoke-virtual {v6, v3, v7}, Landroid/widget/TextView;->measure(II)V

    .line 168
    iget-object v3, p0, Laho;->l:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iput v3, p0, Laho;->ae:I

    .line 169
    :cond_6
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    invoke-static {v3}, Laho;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 170
    iget-object v3, p0, Laho;->k:Landroid/widget/TextView;

    const/high16 v6, -0x80000000

    .line 171
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 172
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 173
    invoke-virtual {v3, v0, v6}, Landroid/widget/TextView;->measure(II)V

    .line 174
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Laho;->ad:I

    .line 175
    :cond_7
    iget v0, p0, Laho;->ad:I

    iget v3, p0, Laho;->ae:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Laho;->ai:I

    .line 176
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 177
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    .line 178
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 179
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 180
    invoke-virtual {v0, v3, v6}, Landroid/widget/TextView;->measure(II)V

    .line 181
    iget-object v0, p0, Laho;->P:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Laho;->af:I

    .line 182
    :cond_8
    iget-object v0, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 183
    iget-object v0, p0, Laho;->n:Landroid/widget/ImageView;

    iget v3, p0, Laho;->B:I

    .line 184
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v6, p0, Laho;->B:I

    .line 185
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 186
    invoke-virtual {v0, v3, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 187
    iget-object v0, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Laho;->ag:I

    .line 188
    :cond_9
    iget-object v0, p0, Laho;->Q:Landroid/widget/ImageView;

    iget v3, p0, Laho;->H:I

    .line 189
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v6, p0, Laho;->H:I

    .line 190
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 191
    invoke-virtual {v0, v3, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 192
    iget-object v0, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 193
    iget-object v0, p0, Laho;->R:Landroid/widget/ImageView;

    .line 194
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 195
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 196
    invoke-virtual {v0, v3, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 197
    iget v0, p0, Laho;->aa:I

    iget-object v3, p0, Laho;->R:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Laho;->aa:I

    .line 198
    :cond_a
    iget-object v0, p0, Laho;->m:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 199
    iget-object v0, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 200
    iget-object v0, p0, Laho;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    iget v1, p0, Laho;->A:I

    sub-int v1, v0, v1

    .line 202
    :cond_b
    iget-object v0, p0, Laho;->m:Landroid/widget/TextView;

    .line 203
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 204
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 205
    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->measure(II)V

    .line 206
    iget v0, p0, Laho;->ag:I

    iget-object v1, p0, Laho;->m:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Laho;->ag:I

    .line 207
    :cond_c
    iget v0, p0, Laho;->aa:I

    iget v1, p0, Laho;->ai:I

    add-int/2addr v0, v1

    iget v1, p0, Laho;->af:I

    add-int/2addr v0, v1

    iget v1, p0, Laho;->ag:I

    add-int/2addr v0, v1

    .line 208
    iget v1, p0, Laho;->U:I

    invoke-virtual {p0}, Laho;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Laho;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 209
    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 210
    iget-object v1, p0, Laho;->N:Landroid/widget/TextView;

    if-eqz v1, :cond_d

    iget-object v1, p0, Laho;->N:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_d

    .line 211
    iget-object v1, p0, Laho;->N:Landroid/widget/TextView;

    iget v3, p0, Laho;->F:I

    .line 212
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 213
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 214
    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 215
    :cond_d
    invoke-virtual {p0, v4, v0}, Laho;->setMeasuredDimension(II)V

    .line 216
    return-void

    .line 138
    :cond_e
    invoke-virtual {p0}, Laho;->getPaddingLeft()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {p0}, Laho;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto/16 :goto_0

    :cond_f
    move v0, v1

    :goto_3
    move v3, v0

    move v0, v2

    .line 162
    goto/16 :goto_2

    .line 160
    :cond_10
    iget-object v0, p0, Laho;->k:Landroid/widget/TextView;

    invoke-static {v0}, Laho;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v1

    move v3, v2

    .line 161
    goto/16 :goto_2

    :cond_11
    move v0, v2

    goto :goto_3

    :cond_12
    move v0, v1

    goto/16 :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 535
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 536
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 537
    iget-object v3, p0, Laho;->aj:Landroid/graphics/Rect;

    float-to-int v4, v1

    float-to-int v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 538
    iget v3, p0, Laho;->J:I

    int-to-float v3, v3

    cmpl-float v3, v1, v3

    if-ltz v3, :cond_2

    iget v3, p0, Laho;->K:I

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gez v1, :cond_2

    const/4 v1, 0x0

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_2

    .line 539
    invoke-virtual {p0}, Laho;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Laho;->getTop()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    cmpg-float v1, v2, v1

    if-gez v1, :cond_2

    move v1, v0

    .line 540
    :goto_0
    if-nez v1, :cond_1

    .line 541
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 542
    :cond_1
    return v0

    .line 539
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 0

    .prologue
    .line 527
    invoke-virtual {p0}, Laho;->forceLayout()V

    .line 528
    return-void
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Laho;->G:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
