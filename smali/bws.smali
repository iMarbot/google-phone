.class final Lbws;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgt;


# instance fields
.field private a:Ljava/lang/String;

.field private synthetic b:Lbwg;


# direct methods
.method constructor <init>(Lbwg;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbws;->b:Lbwg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lbws;->a:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4
    invoke-static {}, Lbdf;->b()V

    .line 5
    iget-object v0, p0, Lbws;->b:Lbwg;

    .line 7
    invoke-static {}, Lbdf;->b()V

    .line 8
    const-string v1, "InCallPresenter.releaseInCallUiLock"

    const-string v2, "releasing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v4

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    iget-object v1, v0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 10
    iget-object v1, v0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    const-string v1, "InCallPresenter.releaseInCallUiLock"

    const-string v2, "all locks released"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v1, v0, Lbwg;->n:Lbwp;

    sget-object v2, Lbwp;->a:Lbwp;

    if-ne v1, v2, :cond_0

    .line 13
    const-string v1, "InCallPresenter.releaseInCallUiLock"

    const-string v2, "no more calls, finishing UI"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-virtual {v0}, Lbwg;->b()V

    .line 15
    invoke-virtual {v0}, Lbwg;->g()V

    .line 16
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 17
    iget-object v0, p0, Lbws;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "InCallUiLock["

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
