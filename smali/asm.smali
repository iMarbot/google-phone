.class public final Lasm;
.super Lasg;
.source "PG"

# interfaces
.implements Lcom/android/dialer/widget/EmptyContentView$a;
.implements Lib;


# instance fields
.field private n:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lasg;-><init>()V

    .line 2
    new-instance v0, Lasn;

    invoke-direct {v0, p0}, Lasn;-><init>(Lasm;)V

    iput-object v0, p0, Lasm;->n:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method protected final a()Lahd;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3
    new-instance v0, Lasl;

    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lasl;-><init>(Landroid/content/Context;)V

    .line 5
    iget-boolean v1, p0, Laig;->k:Z

    .line 7
    iput-boolean v1, v0, Laif;->w:Z

    .line 9
    iput-boolean v2, v0, Lahd;->h:Z

    .line 10
    invoke-virtual {v0, v2}, Lasl;->b(Z)V

    .line 12
    iget-object v1, p0, Lahe;->e:Ljava/lang/String;

    .line 13
    invoke-virtual {v0, v1}, Lasl;->a(Ljava/lang/String;)V

    .line 15
    iput-object p0, v0, Laif;->x:Laif$a;

    .line 16
    return-object v0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 81
    .line 82
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 83
    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 86
    check-cast v0, Lasl;

    invoke-virtual {v0, p1}, Lasl;->b(Z)V

    .line 87
    :cond_0
    invoke-super {p0, p1}, Lasg;->e(Z)V

    .line 88
    return-void
.end method

.method protected final g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.CALL_PHONE"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 47
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110262

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 48
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110261

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 49
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    .line 50
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 53
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    .line 54
    iget-object v0, p0, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v0, v2}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    goto :goto_0
.end method

.method protected final h(Z)Lbbf$a;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lbbf$a;->f:Lbbf$a;

    return-object v0
.end method

.method public final h_()V
    .locals 5

    .prologue
    .line 65
    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 66
    if-nez v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p0}, Lasm;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbsw;->a:Ljava/util/List;

    .line 70
    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 71
    array-length v0, v1

    if-lez v0, :cond_0

    .line 72
    const-string v2, "SmartDialSearchFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 73
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 74
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0

    .line 73
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 17
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 18
    invoke-super {p0, p1, p2}, Lasg;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    .line 20
    :cond_0
    iget-object v0, p0, Lahe;->g:Lahd;

    .line 21
    check-cast v0, Lasl;

    .line 22
    new-instance v2, Lbil;

    invoke-super {p0}, Lasg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, Lbil;-><init>(Landroid/content/Context;)V

    .line 24
    iput-boolean v3, v2, Lbil;->b:Z

    .line 25
    iget-object v1, v2, Lbil;->a:Lbsa;

    if-eqz v1, :cond_1

    .line 26
    iget-object v3, v2, Lbil;->a:Lbsa;

    const/4 v1, 0x0

    .line 27
    iput-boolean v1, v3, Lbsa;->d:Z

    .line 30
    :cond_1
    iget-object v1, v0, Lahd;->l:Ljava/lang/String;

    .line 31
    if-nez v1, :cond_2

    .line 32
    const-string v1, ""

    invoke-virtual {v2, v1}, Lbil;->a(Ljava/lang/String;)V

    .line 33
    iget-object v0, v0, Lasl;->z:Lbsa;

    const-string v1, ""

    .line 34
    iput-object v1, v0, Lbsa;->c:Ljava/lang/String;

    :goto_1
    move-object v0, v2

    .line 43
    goto :goto_0

    .line 37
    :cond_2
    iget-object v1, v0, Lahd;->l:Ljava/lang/String;

    .line 38
    invoke-virtual {v2, v1}, Lbil;->a(Ljava/lang/String;)V

    .line 39
    iget-object v1, v0, Lasl;->z:Lbsa;

    .line 40
    iget-object v0, v0, Lahd;->l:Ljava/lang/String;

    .line 41
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    iput-object v0, v1, Lbsa;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lasm;->g()V

    .line 79
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Lasg;->onStart()V

    .line 57
    const-string v0, "SmartDialSearchFragment.onStart"

    const-string v1, "registering smart dial update receiver"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lasm;->n:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.android.dialer.database.ACTION_SMART_DIAL_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Lasg;->onStop()V

    .line 62
    const-string v0, "SmartDialSearchFragment.onStop"

    const-string v1, "unregistering smart dial update receiver"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    invoke-virtual {p0}, Lasm;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lasm;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 64
    return-void
.end method
