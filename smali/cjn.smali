.class public final Lcjn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjr;


# instance fields
.field public a:Lcjq;

.field public b:Landroid/view/TextureView;

.field public c:Landroid/view/Surface;

.field public d:Landroid/graphics/SurfaceTexture;

.field public e:Z

.field private f:I

.field private g:Z

.field private h:Landroid/graphics/Point;

.field private i:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(ZI)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean p1, p0, Lcjn;->g:Z

    .line 3
    iput p2, p0, Lcjn;->f:I

    .line 4
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    return-object v0
.end method

.method public final a(Landroid/graphics/Point;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 9
    const-string v0, "VideoSurfaceTextureImpl.setSurfaceDimensions"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 10
    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "surfaceDimensions: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    .line 11
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iput-object p1, p0, Lcjn;->h:Landroid/graphics/Point;

    .line 13
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 14
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcjn;->g:Z

    if-eqz v0, :cond_1

    .line 15
    const-string v0, "VideoSurfaceTextureImpl.setSurfaceDimensions"

    const-string v1, "skip setting default buffer size on Pixel 2017 ODR"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :cond_0
    :goto_0
    return-void

    .line 17
    :cond_1
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    goto :goto_0
.end method

.method public final a(Landroid/view/TextureView;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 23
    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    if-ne v0, p1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 25
    :cond_0
    const-string v0, "VideoSurfaceTextureImpl.attachToTextureView"

    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    invoke-virtual {v0, v3}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 29
    :cond_1
    iput-object p1, p0, Lcjn;->b:Landroid/view/TextureView;

    .line 30
    new-instance v0, Lcjp;

    .line 31
    invoke-direct {v0, p0}, Lcjp;-><init>(Lcjn;)V

    .line 32
    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 33
    new-instance v0, Lcjo;

    .line 34
    invoke-direct {v0, p0}, Lcjo;-><init>(Lcjn;)V

    .line 35
    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 37
    const-string v1, "VideoSurfaceTextureImpl.attachToTextureView"

    const/16 v2, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "areSameSurfaces: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    iget-object v1, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 39
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 40
    iget-object v0, p0, Lcjn;->h:Landroid/graphics/Point;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcjn;->h:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcjn;->h:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lcjn;->a(II)Z

    .line 41
    invoke-virtual {p0}, Lcjn;->e()V

    .line 42
    :cond_2
    iput-boolean v4, p0, Lcjn;->e:Z

    goto :goto_0
.end method

.method public final a(Lcjq;)V
    .locals 5

    .prologue
    .line 5
    const-string v0, "VideoSurfaceTextureImpl.setDelegate"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "delegate: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    iput-object p1, p0, Lcjn;->a:Lcjq;

    .line 7
    return-void
.end method

.method final a(II)Z
    .locals 4

    .prologue
    .line 56
    const-string v0, "VideoSurfaceTextureImpl.createSurface"

    .line 57
    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "width: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 58
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 60
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 62
    :cond_0
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcjn;->h:Landroid/graphics/Point;

    return-object v0
.end method

.method public final b(Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcjn;->i:Landroid/graphics/Point;

    .line 21
    return-void
.end method

.method public final c()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcjn;->i:Landroid/graphics/Point;

    return-object v0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    const-string v0, "VideoSurfaceTextureImpl.setDoneWithSurface"

    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcjn;->e:Z

    .line 46
    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcjn;->b:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    if-eqz v0, :cond_2

    .line 49
    invoke-virtual {p0}, Lcjn;->f()V

    .line 50
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 51
    iput-object v3, p0, Lcjn;->c:Landroid/view/Surface;

    .line 52
    :cond_2
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 54
    iput-object v3, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    goto :goto_0
.end method

.method final e()V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lcjn;->a:Lcjq;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcjn;->a:Lcjq;

    invoke-interface {v0, p0}, Lcjq;->a(Lcjr;)V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    const-string v1, "VideoSurfaceTextureImpl.onSurfaceCreated"

    const-string v2, "delegate is null. "

    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method final f()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcjn;->a:Lcjq;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcjn;->a:Lcjq;

    invoke-interface {v0}, Lcjq;->a()V

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    const-string v1, "VideoSurfaceTextureImpl.onSurfaceReleased"

    const-string v2, "delegate is null. "

    invoke-virtual {p0}, Lcjn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 72
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "VideoSurfaceTextureImpl<%s%s%s%s>"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 73
    iget v0, p0, Lcjn;->f:I

    if-ne v0, v5, :cond_0

    const-string v0, "local, "

    :goto_0
    aput-object v0, v3, v4

    .line 74
    iget-object v0, p0, Lcjn;->c:Landroid/view/Surface;

    if-nez v0, :cond_1

    const-string v0, "no-surface, "

    :goto_1
    aput-object v0, v3, v5

    const/4 v4, 0x2

    .line 75
    iget-object v0, p0, Lcjn;->d:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_2

    const-string v0, "no-texture, "

    :goto_2
    aput-object v0, v3, v4

    const/4 v4, 0x3

    .line 76
    iget-object v0, p0, Lcjn;->h:Landroid/graphics/Point;

    if-nez v0, :cond_3

    .line 77
    const-string v0, "(-1 x -1)"

    .line 78
    :goto_3
    aput-object v0, v3, v4

    .line 79
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 73
    :cond_0
    const-string v0, "remote, "

    goto :goto_0

    .line 74
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 75
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 78
    :cond_3
    iget-object v0, p0, Lcjn;->h:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcjn;->h:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    const/16 v6, 0x19

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " x "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
