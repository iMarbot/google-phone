.class public final Lle;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Landroid/app/PendingIntent;

.field public f:Landroid/graphics/Bitmap;

.field public g:I

.field public h:Z

.field public i:Llf;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z

.field public m:I

.field public n:I

.field public o:Ljava/lang/String;

.field public p:I

.field public q:I

.field public r:Landroid/app/Notification;

.field public s:Ljava/util/ArrayList;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lle;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lle;->b:Ljava/util/ArrayList;

    .line 3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lle;->h:Z

    .line 4
    iput-boolean v4, p0, Lle;->l:Z

    .line 5
    iput v4, p0, Lle;->m:I

    .line 6
    iput v4, p0, Lle;->n:I

    .line 7
    iput v4, p0, Lle;->p:I

    .line 8
    iput v4, p0, Lle;->q:I

    .line 9
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lle;->r:Landroid/app/Notification;

    .line 10
    iput-object p1, p0, Lle;->a:Landroid/content/Context;

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lle;->o:Ljava/lang/String;

    .line 12
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 13
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 14
    iput v4, p0, Lle;->g:I

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lle;->s:Ljava/util/ArrayList;

    .line 16
    return-void
.end method

.method private final a(IZ)V
    .locals 3

    .prologue
    .line 36
    if-eqz p2, :cond_0

    .line 37
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method public static c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 50
    if-nez p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return-object p0

    .line 51
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Llb;

    invoke-direct {v0, p0}, Llb;-><init>(Lle;)V

    invoke-virtual {v0}, Llb;->b()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lle;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 22
    return-object p0
.end method

.method public final a(J)Lle;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 20
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Lle;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 28
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lle;
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 30
    iget-object v0, p0, Lle;->r:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 31
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lle;
    .locals 1

    .prologue
    .line 23
    invoke-static {p1}, Lle;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lle;->c:Ljava/lang/CharSequence;

    .line 24
    return-object p0
.end method

.method public final a(Llf;)Lle;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lle;->i:Llf;

    if-eq v0, p1, :cond_0

    .line 41
    iput-object p1, p0, Lle;->i:Llf;

    .line 42
    iget-object v0, p0, Lle;->i:Llf;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lle;->i:Llf;

    .line 44
    iget-object v1, v0, Llf;->a:Lle;

    if-eq v1, p0, :cond_0

    .line 45
    iput-object p0, v0, Llf;->a:Lle;

    .line 46
    iget-object v1, v0, Llf;->a:Lle;

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, v0, Llf;->a:Lle;

    invoke-virtual {v1, v0}, Lle;->a(Llf;)Lle;

    .line 48
    :cond_0
    return-object p0
.end method

.method public final a(Z)Lle;
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lle;->a(IZ)V

    .line 33
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Lle;
    .locals 1

    .prologue
    .line 25
    invoke-static {p1}, Lle;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lle;->d:Ljava/lang/CharSequence;

    .line 26
    return-object p0
.end method

.method public final b(Z)Lle;
    .locals 2

    .prologue
    .line 34
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lle;->a(IZ)V

    .line 35
    return-object p0
.end method
