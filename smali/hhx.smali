.class public final Lhhx;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhhq;

.field public c:Lhhz;

.field private d:Ljava/lang/Integer;

.field private e:Lhia;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lhhx;->a:Ljava/lang/String;

    .line 4
    iput-object v0, p0, Lhhx;->b:Lhhq;

    .line 5
    iput-object v0, p0, Lhhx;->d:Ljava/lang/Integer;

    .line 6
    iput-object v0, p0, Lhhx;->c:Lhhz;

    .line 7
    iput-object v0, p0, Lhhx;->e:Lhia;

    .line 8
    iput-object v0, p0, Lhhx;->unknownFieldData:Lhfv;

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lhhx;->cachedSize:I

    .line 10
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 24
    iget-object v1, p0, Lhhx;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25
    const/4 v1, 0x1

    iget-object v2, p0, Lhhx;->a:Ljava/lang/String;

    .line 26
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_0
    iget-object v1, p0, Lhhx;->b:Lhhq;

    if-eqz v1, :cond_1

    .line 28
    const/4 v1, 0x2

    iget-object v2, p0, Lhhx;->b:Lhhq;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_1
    iget-object v1, p0, Lhhx;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 31
    const/4 v1, 0x3

    iget-object v2, p0, Lhhx;->d:Ljava/lang/Integer;

    .line 32
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_2
    iget-object v1, p0, Lhhx;->c:Lhhz;

    if-eqz v1, :cond_3

    .line 34
    const/4 v1, 0x4

    iget-object v2, p0, Lhhx;->c:Lhhz;

    .line 35
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_3
    iget-object v1, p0, Lhhx;->e:Lhia;

    if-eqz v1, :cond_4

    .line 37
    const/4 v1, 0x5

    iget-object v2, p0, Lhhx;->e:Lhia;

    .line 38
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 40
    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :sswitch_0
    return-object p0

    .line 46
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhx;->a:Ljava/lang/String;

    goto :goto_0

    .line 48
    :sswitch_2
    iget-object v0, p0, Lhhx;->b:Lhhq;

    if-nez v0, :cond_1

    .line 49
    new-instance v0, Lhhq;

    invoke-direct {v0}, Lhhq;-><init>()V

    iput-object v0, p0, Lhhx;->b:Lhhq;

    .line 50
    :cond_1
    iget-object v0, p0, Lhhx;->b:Lhhq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 53
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 54
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhhx;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 56
    :sswitch_4
    iget-object v0, p0, Lhhx;->c:Lhhz;

    if-nez v0, :cond_2

    .line 57
    new-instance v0, Lhhz;

    invoke-direct {v0}, Lhhz;-><init>()V

    iput-object v0, p0, Lhhx;->c:Lhhz;

    .line 58
    :cond_2
    iget-object v0, p0, Lhhx;->c:Lhhz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 60
    :sswitch_5
    iget-object v0, p0, Lhhx;->e:Lhia;

    if-nez v0, :cond_3

    .line 61
    new-instance v0, Lhia;

    invoke-direct {v0}, Lhia;-><init>()V

    iput-object v0, p0, Lhhx;->e:Lhia;

    .line 62
    :cond_3
    iget-object v0, p0, Lhhx;->e:Lhia;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lhhx;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 12
    const/4 v0, 0x1

    iget-object v1, p0, Lhhx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 13
    :cond_0
    iget-object v0, p0, Lhhx;->b:Lhhq;

    if-eqz v0, :cond_1

    .line 14
    const/4 v0, 0x2

    iget-object v1, p0, Lhhx;->b:Lhhq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 15
    :cond_1
    iget-object v0, p0, Lhhx;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 16
    const/4 v0, 0x3

    iget-object v1, p0, Lhhx;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 17
    :cond_2
    iget-object v0, p0, Lhhx;->c:Lhhz;

    if-eqz v0, :cond_3

    .line 18
    const/4 v0, 0x4

    iget-object v1, p0, Lhhx;->c:Lhhz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_3
    iget-object v0, p0, Lhhx;->e:Lhia;

    if-eqz v0, :cond_4

    .line 20
    const/4 v0, 0x5

    iget-object v1, p0, Lhhx;->e:Lhia;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 21
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 22
    return-void
.end method
