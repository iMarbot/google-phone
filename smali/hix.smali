.class public final Lhix;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhix;

.field private static f:[Lhiw;


# instance fields
.field public final b:Z

.field public final c:Z

.field public final d:[Ljava/lang/String;

.field public final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 47
    const/16 v0, 0xd

    new-array v0, v0, [Lhiw;

    sget-object v1, Lhiw;->o:Lhiw;

    aput-object v1, v0, v4

    sget-object v1, Lhiw;->q:Lhiw;

    aput-object v1, v0, v3

    sget-object v1, Lhiw;->g:Lhiw;

    aput-object v1, v0, v5

    sget-object v1, Lhiw;->l:Lhiw;

    aput-object v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lhiw;->k:Lhiw;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lhiw;->m:Lhiw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhiw;->n:Lhiw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhiw;->c:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhiw;->e:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhiw;->f:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhiw;->b:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhiw;->d:Lhiw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhiw;->a:Lhiw;

    aput-object v2, v0, v1

    sput-object v0, Lhix;->f:[Lhiw;

    .line 48
    new-instance v0, Lhiy;

    invoke-direct {v0, v3}, Lhiy;-><init>(Z)V

    sget-object v1, Lhix;->f:[Lhiw;

    .line 49
    invoke-virtual {v0, v1}, Lhiy;->a([Lhiw;)Lhiy;

    move-result-object v0

    new-array v1, v6, [Lhjg;

    sget-object v2, Lhjg;->a:Lhjg;

    aput-object v2, v1, v4

    sget-object v2, Lhjg;->b:Lhjg;

    aput-object v2, v1, v3

    sget-object v2, Lhjg;->c:Lhjg;

    aput-object v2, v1, v5

    .line 50
    invoke-virtual {v0, v1}, Lhiy;->a([Lhjg;)Lhiy;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v3}, Lhiy;->a(Z)Lhiy;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lhiy;->a()Lhix;

    move-result-object v0

    sput-object v0, Lhix;->a:Lhix;

    .line 53
    new-instance v0, Lhiy;

    sget-object v1, Lhix;->a:Lhix;

    invoke-direct {v0, v1}, Lhiy;-><init>(Lhix;)V

    new-array v1, v3, [Lhjg;

    sget-object v2, Lhjg;->c:Lhjg;

    aput-object v2, v1, v4

    .line 54
    invoke-virtual {v0, v1}, Lhiy;->a([Lhjg;)Lhiy;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v3}, Lhiy;->a(Z)Lhiy;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lhiy;->a()Lhix;

    .line 57
    new-instance v0, Lhiy;

    invoke-direct {v0, v4}, Lhiy;-><init>(Z)V

    invoke-virtual {v0}, Lhiy;->a()Lhix;

    return-void
.end method

.method constructor <init>(Lhiy;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iget-boolean v0, p1, Lhiy;->a:Z

    .line 4
    iput-boolean v0, p0, Lhix;->b:Z

    .line 6
    iget-object v0, p1, Lhiy;->b:[Ljava/lang/String;

    .line 7
    iput-object v0, p0, Lhix;->d:[Ljava/lang/String;

    .line 9
    iget-object v0, p1, Lhiy;->c:[Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lhix;->e:[Ljava/lang/String;

    .line 12
    iget-boolean v0, p1, Lhiy;->d:Z

    .line 13
    iput-boolean v0, p0, Lhix;->c:Z

    .line 14
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 3

    .prologue
    .line 15
    iget-object v0, p0, Lhix;->d:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 20
    :goto_0
    return-object v0

    .line 16
    :cond_0
    iget-object v0, p0, Lhix;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [Lhiw;

    .line 17
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lhix;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 18
    iget-object v2, p0, Lhix;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Lhiw;->a(Ljava/lang/String;)Lhiw;

    move-result-object v2

    aput-object v2, v1, v0

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 20
    :cond_1
    invoke-static {v1}, Lhjh;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lhix;->e:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 26
    :goto_0
    return-object v0

    .line 22
    :cond_0
    iget-object v0, p0, Lhix;->e:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [Lhjg;

    .line 23
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lhix;->e:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 24
    iget-object v2, p0, Lhix;->e:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Lhjg;->a(Ljava/lang/String;)Lhjg;

    move-result-object v2

    aput-object v2, v1, v0

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 26
    :cond_1
    invoke-static {v1}, Lhjh;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    instance-of v2, p1, Lhix;

    if-nez v2, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v0

    .line 28
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    .line 29
    :cond_2
    check-cast p1, Lhix;

    .line 30
    iget-boolean v2, p0, Lhix;->b:Z

    iget-boolean v3, p1, Lhix;->b:Z

    if-ne v2, v3, :cond_0

    .line 31
    iget-boolean v2, p0, Lhix;->b:Z

    if-eqz v2, :cond_3

    .line 32
    iget-object v2, p0, Lhix;->d:[Ljava/lang/String;

    iget-object v3, p1, Lhix;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    iget-object v2, p0, Lhix;->e:[Ljava/lang/String;

    iget-object v3, p1, Lhix;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    iget-boolean v2, p0, Lhix;->c:Z

    iget-boolean v3, p1, Lhix;->c:Z

    if-ne v2, v3, :cond_0

    :cond_3
    move v0, v1

    .line 35
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 36
    const/16 v0, 0x11

    .line 37
    iget-boolean v1, p0, Lhix;->b:Z

    if-eqz v1, :cond_0

    .line 38
    iget-object v0, p0, Lhix;->d:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 39
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhix;->e:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhix;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 41
    :cond_0
    return v0

    .line 40
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    iget-boolean v0, p0, Lhix;->b:Z

    if-nez v0, :cond_0

    .line 43
    const-string v0, "ConnectionSpec()"

    .line 46
    :goto_0
    return-object v0

    .line 44
    :cond_0
    iget-object v0, p0, Lhix;->d:[Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lhix;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    :goto_1
    iget-object v1, p0, Lhix;->e:[Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lhix;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 46
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConnectionSpec(cipherSuites="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", tlsVersions="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", supportsTlsExtensions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lhix;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_1
    const-string v0, "[all enabled]"

    goto :goto_1

    .line 45
    :cond_2
    const-string v1, "[all enabled]"

    goto :goto_2
.end method
