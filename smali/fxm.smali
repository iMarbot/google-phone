.class final Lfxm;
.super Lfwq;
.source "PG"

# interfaces
.implements Lgaj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfxm$a;
    }
.end annotation


# static fields
.field private static volatile e:Lfxm;


# instance fields
.field public final d:I

.field private f:I

.field private g:I

.field private h:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method private constructor <init>(Lgdc;Landroid/app/Application;Lgax;Lfzo;)V
    .locals 1

    .prologue
    .line 7
    sget v0, Lmg$c;->D:I

    invoke-direct {p0, p1, p2, p3, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 9
    iget v0, p4, Lfzo;->e:I

    .line 10
    iput v0, p0, Lfxm;->f:I

    .line 12
    iget v0, p4, Lfzo;->d:I

    .line 13
    iput v0, p0, Lfxm;->g:I

    .line 15
    iget v0, p4, Lfzo;->c:I

    .line 16
    iput v0, p0, Lfxm;->d:I

    .line 17
    return-void
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Lfzo;)Lfxm;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lfxm;->e:Lfxm;

    if-nez v0, :cond_1

    .line 2
    const-class v1, Lfxm;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lfxm;->e:Lfxm;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lfxm;

    invoke-direct {v0, p0, p1, p2, p3}, Lfxm;-><init>(Lgdc;Landroid/app/Application;Lgax;Lfzo;)V

    sput-object v0, Lfxm;->e:Lfxm;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lfxm;->e:Lfxm;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized f()V
    .locals 7

    .prologue
    .line 18
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfxm;->h:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_0

    .line 19
    iget-boolean v0, p0, Lfwq;->c:Z

    .line 20
    if-nez v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lfxm;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lfxm$a;

    .line 23
    invoke-direct {v1, p0}, Lfxm$a;-><init>(Lfxm;)V

    .line 24
    iget v2, p0, Lfxm;->g:I

    int-to-long v2, v2

    iget v4, p0, Lfxm;->f:I

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 25
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lfxm;->h:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :cond_0
    monitor-exit p0

    return-void

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfxm;->h:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lfxm;->h:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lfxm;->h:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :cond_0
    monitor-exit p0

    return-void

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()V
    .locals 1

    .prologue
    .line 31
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lfxm;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lfxm;->f()V

    .line 35
    return-void
.end method
