.class public final Lvi;
.super Ltv;
.source "PG"


# instance fields
.field public a:Laao;

.field public b:Z

.field public c:Landroid/view/Window$Callback;

.field private d:Z

.field private e:Z

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/lang/Runnable;

.field private h:Landroid/support/v7/widget/Toolbar$c;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window$Callback;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ltv;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvi;->f:Ljava/util/ArrayList;

    .line 3
    new-instance v0, Lvj;

    invoke-direct {v0, p0}, Lvj;-><init>(Lvi;)V

    iput-object v0, p0, Lvi;->g:Ljava/lang/Runnable;

    .line 4
    new-instance v0, Lxy;

    invoke-direct {v0, p0}, Lxy;-><init>(Lvi;)V

    iput-object v0, p0, Lvi;->h:Landroid/support/v7/widget/Toolbar$c;

    .line 5
    new-instance v0, Laao;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Laao;-><init>(Landroid/support/v7/widget/Toolbar;Z)V

    iput-object v0, p0, Lvi;->a:Laao;

    .line 6
    new-instance v0, Lvm;

    invoke-direct {v0, p0, p3}, Lvm;-><init>(Lvi;Landroid/view/Window$Callback;)V

    iput-object v0, p0, Lvi;->c:Landroid/view/Window$Callback;

    .line 7
    iget-object v0, p0, Lvi;->a:Laao;

    iget-object v1, p0, Lvi;->c:Landroid/view/Window$Callback;

    invoke-virtual {v0, v1}, Laao;->a(Landroid/view/Window$Callback;)V

    .line 8
    iget-object v0, p0, Lvi;->h:Landroid/support/v7/widget/Toolbar$c;

    .line 9
    iput-object v0, p1, Landroid/support/v7/widget/Toolbar;->q:Landroid/support/v7/widget/Toolbar$c;

    .line 10
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0, p2}, Laao;->a(Ljava/lang/CharSequence;)V

    .line 11
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->l()I

    move-result v0

    .line 33
    iget-object v1, p0, Lvi;->a:Laao;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Laao;->a(I)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->n()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0, p1}, Lqy;->a(Landroid/view/View;F)V

    .line 22
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 12
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 13
    iget-object v1, p0, Lvi;->a:Laao;

    invoke-virtual {v1}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 14
    new-instance v1, Ltw;

    invoke-direct {v1, v3, v3}, Ltw;-><init>(II)V

    .line 15
    if-eqz v0, :cond_0

    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 17
    :cond_0
    iget-object v1, p0, Lvi;->a:Laao;

    invoke-virtual {v1, v0}, Laao;->a(Landroid/view/View;)V

    .line 18
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lvi;->a:Laao;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laao;->a(Landroid/graphics/drawable/Drawable;)V

    .line 20
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0, p1}, Laao;->b(Ljava/lang/CharSequence;)V

    .line 27
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 35
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvi;->a(II)V

    .line 36
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    invoke-virtual {p0}, Lvi;->i()Landroid/view/Menu;

    move-result-object v3

    .line 60
    if-eqz v3, :cond_0

    .line 61
    if-eqz p2, :cond_1

    .line 62
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    .line 63
    :goto_0
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/Menu;->setQwertyMode(Z)V

    .line 65
    invoke-interface {v3, p1, p2, v2}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v2

    .line 66
    :cond_0
    return v2

    .line 62
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 64
    goto :goto_1
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lvi;->d()Z

    .line 58
    :cond_0
    return v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->l()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 28
    iget-object v1, p0, Lvi;->a:Laao;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Laao;->b(Ljava/lang/CharSequence;)V

    .line 29
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0, p1}, Laao;->b(Landroid/graphics/drawable/Drawable;)V

    .line 44
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0, p1}, Laao;->a(Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 37
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvi;->a(II)V

    .line 38
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->b()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    const/16 v0, 0x8

    .line 39
    invoke-direct {p0, v0, v0}, Lvi;->a(II)V

    .line 40
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 41
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvi;->a(II)V

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->h()Z

    move-result v0

    return v0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->i()Z

    move-result v0

    return v0
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lvi;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 50
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lvi;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lqy;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    .line 69
    iget-boolean v0, p0, Lvi;->e:Z

    if-ne p1, v0, :cond_1

    .line 76
    :cond_0
    return-void

    .line 71
    :cond_1
    iput-boolean p1, p0, Lvi;->e:Z

    .line 72
    iget-object v0, p0, Lvi;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 73
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 74
    iget-object v2, p0, Lvi;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->d()V

    .line 54
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final h()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lvi;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 68
    return-void
.end method

.method final i()Landroid/view/Menu;
    .locals 3

    .prologue
    .line 77
    iget-boolean v0, p0, Lvi;->d:Z

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lvi;->a:Laao;

    new-instance v1, Lvk;

    invoke-direct {v1, p0}, Lvk;-><init>(Lvi;)V

    new-instance v2, Lvl;

    invoke-direct {v2, p0}, Lvl;-><init>(Lvi;)V

    invoke-virtual {v0, v1, v2}, Laao;->a(Lxv;Lxg;)V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lvi;->d:Z

    .line 80
    :cond_0
    iget-object v0, p0, Lvi;->a:Laao;

    invoke-virtual {v0}, Laao;->o()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method
