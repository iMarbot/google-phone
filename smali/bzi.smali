.class public final Lbzi;
.super Lbzg;
.source "PG"

# interfaces
.implements Lbzu;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ClickableViewAccessibility"
    }
.end annotation


# instance fields
.field public W:Landroid/view/View;

.field public X:Landroid/view/View;

.field public Y:Landroid/animation/Animator;

.field public Z:Landroid/animation/AnimatorSet;

.field public a:Landroid/view/View;

.field public aa:I

.field public ab:I

.field public ac:Landroid/animation/Animator;

.field public ad:Lcbg;

.field private ae:Landroid/widget/TextView;

.field private af:Landroid/widget/TextView;

.field private ag:Landroid/widget/ImageView;

.field private ah:Landroid/widget/ImageView;

.field private ai:Landroid/animation/AnimatorSet;

.field private aj:Landroid/animation/AnimatorSet;

.field private ak:F

.field private al:Landroid/animation/Animator;

.field private am:Landroid/graphics/drawable/Drawable;

.field private an:Z

.field private ao:Lbzr;

.field private ap:Lcam;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lbzg;-><init>()V

    .line 2
    iput v0, p0, Lbzi;->aa:I

    .line 3
    iput v0, p0, Lbzi;->ab:I

    return-void
.end method

.method private final X()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lbzi;->ah:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->ac()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    :cond_1
    iget-object v0, p0, Lbzi;->ah:Landroid/widget/ImageView;

    const v1, 0x7f02017b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 185
    :goto_1
    iget-object v0, p0, Lbzi;->ag:Landroid/widget/ImageView;

    .line 186
    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 187
    invoke-direct {p0}, Lbzi;->Y()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    const v0, 0x7f0d005a

    .line 190
    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 191
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    .line 192
    invoke-direct {p0}, Lbzi;->Y()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 193
    iget-object v0, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lbzi;->am:Landroid/graphics/drawable/Drawable;

    .line 194
    invoke-static {v0, v3, v1, v1}, Lbib;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 197
    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 198
    iget-object v0, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 199
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 200
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 201
    iget-object v1, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    iget-object v1, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-direct {p0}, Lbzi;->Y()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v0, p0, Lbzi;->ah:Landroid/widget/ImageView;

    const v1, 0x7f020135

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 189
    :cond_3
    const v0, 0x7f0d0059

    goto :goto_2

    .line 196
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 202
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_4
.end method

.method private final Y()Z
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbzi;->am:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final Z()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 222
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    .line 243
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 225
    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 226
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 227
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 228
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 229
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 230
    iget-object v2, p0, Lbzi;->ah:Landroid/widget/ImageView;

    .line 231
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0095

    invoke-virtual {v3, v4}, Landroid/content/Context;->getColor(I)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    .line 232
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 233
    iget-object v2, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 234
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v2

    invoke-interface {v2}, Lbzh;->af()V

    .line 236
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    iget-object v3, p0, Lbzi;->ao:Lbzr;

    .line 237
    iget-boolean v3, v3, Lbzr;->c:Z

    .line 238
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setActivated(Z)V

    .line 239
    iget-object v2, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 240
    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 241
    iget-object v2, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 242
    iget-object v2, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-direct {p0}, Lbzi;->Y()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 531
    new-array v0, v5, [Landroid/animation/PropertyValuesHolder;

    sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v2, v5, [F

    aput p1, v2, v3

    aput p2, v2, v4

    .line 532
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v2, v5, [F

    aput p1, v2, v3

    aput p2, v2, v4

    .line 533
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v0, v4

    .line 534
    invoke-static {p0, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 535
    invoke-virtual {v0, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 536
    invoke-virtual {v0, p5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 537
    return-object v0
.end method

.method private static a(Landroid/view/View;FJ)Landroid/animation/ObjectAnimator;
    .locals 4

    .prologue
    .line 503
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 504
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 505
    return-object v0
.end method

.method private final a(Landroid/animation/AnimatorSet;)V
    .locals 4

    .prologue
    .line 538
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 540
    :cond_0
    iget-object v0, p0, Lbzi;->a:Landroid/view/View;

    sget-object v1, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    .line 541
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    .line 542
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    const-wide/16 v2, 0x729

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 543
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    new-instance v1, Lbzq;

    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lbzq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 544
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 545
    return-void

    .line 540
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, p1, v1}, Lapw;->a(FFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 5
    return-void
.end method

.method private final aa()V
    .locals 14

    .prologue
    const-wide/16 v4, 0x535

    const/4 v10, 0x2

    const/4 v13, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 404
    const-string v0, "FlingUpDownMethod.startSwipeToAnswerEntryAnimation"

    const-string v1, "Swipe entry animation."

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    invoke-direct {p0}, Lbzi;->ab()V

    .line 406
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    .line 407
    iget-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v10, [F

    .line 408
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v3

    const/high16 v6, 0x43400000    # 192.0f

    invoke-static {v3, v6}, Lapw;->a(Landroid/content/Context;F)F

    move-result v3

    aput v3, v2, v11

    .line 409
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v3

    const/high16 v6, -0x3e600000    # -20.0f

    invoke-static {v3, v6}, Lapw;->a(Landroid/content/Context;F)F

    move-result v3

    aput v3, v2, v13

    .line 410
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 411
    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 412
    new-instance v1, Lsl;

    invoke-direct {v1}, Lsl;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 413
    iget-object v1, p0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v10, [F

    .line 414
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, -0x3e600000    # -20.0f

    invoke-static {v6, v7}, Lapw;->a(Landroid/content/Context;F)F

    move-result v6

    aput v6, v3, v11

    aput v12, v3, v13

    .line 415
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 416
    invoke-virtual {v7, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 417
    new-instance v1, Lsk;

    invoke-direct {v1}, Lsk;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 418
    iget-object v1, p0, Lbzi;->af:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setAlpha(F)V

    .line 419
    iget-object v1, p0, Lbzi;->af:Landroid/widget/TextView;

    new-array v2, v10, [Landroid/animation/PropertyValuesHolder;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v13, [F

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v11

    .line 420
    invoke-static {v3, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v11

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v10, [F

    .line 421
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v8

    const/high16 v9, -0x3f000000    # -8.0f

    invoke-static {v8, v9}, Lapw;->a(Landroid/content/Context;F)F

    move-result v8

    aput v8, v6, v11

    aput v12, v6, v13

    .line 422
    invoke-static {v3, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v13

    .line 423
    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 424
    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    invoke-virtual {v8, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 425
    const-wide/16 v2, 0x29b

    invoke-virtual {v8, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 426
    const-wide/16 v2, 0x14d

    invoke-virtual {v8, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 427
    iget-object v1, p0, Lbzi;->a:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v10, [F

    .line 428
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v6

    const/high16 v9, 0x43c80000    # 400.0f

    invoke-static {v6, v9}, Lapw;->a(Landroid/content/Context;F)F

    move-result v6

    aput v6, v3, v11

    .line 429
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v6

    const/high16 v9, -0x3ec00000    # -12.0f

    invoke-static {v6, v9}, Lapw;->a(Landroid/content/Context;F)F

    move-result v6

    aput v6, v3, v13

    .line 430
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 431
    const-wide/16 v2, 0x5dc

    invoke-virtual {v9, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 432
    const/high16 v1, 0x3f800000    # 1.0f

    .line 433
    invoke-static {v12, v12, v12, v1}, Lbw;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v1

    .line 434
    invoke-virtual {v9, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 435
    iget-object v1, p0, Lbzi;->a:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v10, [F

    .line 436
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v6

    const/high16 v10, -0x3ec00000    # -12.0f

    invoke-static {v6, v10}, Lapw;->a(Landroid/content/Context;F)F

    move-result v6

    aput v6, v3, v11

    aput v12, v3, v13

    .line 437
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 438
    invoke-virtual {v10, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 439
    new-instance v1, Lsk;

    invoke-direct {v1}, Lsk;-><init>()V

    invoke-virtual {v10, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 440
    iget-object v1, p0, Lbzi;->ag:Landroid/widget/ImageView;

    const v2, 0x3ea8f5c3    # 0.33f

    const v3, 0x3f8ccccd    # 1.1f

    const v6, 0x3ecccccd    # 0.4f

    const/high16 v11, 0x3f800000    # 1.0f

    .line 441
    invoke-static {v6, v12, v12, v11}, Lbw;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v6

    .line 442
    invoke-static/range {v1 .. v6}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v11

    .line 443
    iget-object v1, p0, Lbzi;->ag:Landroid/widget/ImageView;

    const v2, 0x3f8ccccd    # 1.1f

    const/high16 v3, 0x3f800000    # 1.0f

    new-instance v6, Lsk;

    invoke-direct {v6}, Lsk;-><init>()V

    .line 444
    invoke-static/range {v1 .. v6}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v1

    .line 445
    iget-object v2, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 446
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 447
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 448
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-direct {p0, v0}, Lbzi;->a(Landroid/animation/AnimatorSet;)V

    .line 449
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    new-instance v1, Lbzl;

    invoke-direct {v1, p0}, Lbzl;-><init>(Lbzi;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 450
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 451
    return-void
.end method

.method private final ab()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 510
    const-string v0, "FlingUpDownMethod.endAnimation"

    const-string v1, "End animations."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    iget-object v0, p0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 513
    iput-object v3, p0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    .line 514
    :cond_0
    iget-object v0, p0, Lbzi;->Y:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    .line 515
    iget-object v0, p0, Lbzi;->Y:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 516
    iput-object v3, p0, Lbzi;->Y:Landroid/animation/Animator;

    .line 517
    :cond_1
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    .line 518
    iget-object v0, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 519
    iput-object v3, p0, Lbzi;->ai:Landroid/animation/AnimatorSet;

    .line 520
    :cond_2
    iget-object v0, p0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_3

    .line 521
    iget-object v0, p0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 522
    iput-object v3, p0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    .line 523
    :cond_3
    iget-object v0, p0, Lbzi;->ac:Landroid/animation/Animator;

    if-eqz v0, :cond_4

    .line 524
    iget-object v0, p0, Lbzi;->ac:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 525
    iput-object v3, p0, Lbzi;->ac:Landroid/animation/Animator;

    .line 526
    :cond_4
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    if-eqz v0, :cond_5

    .line 527
    iget-object v0, p0, Lbzi;->al:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 528
    iput-object v3, p0, Lbzi;->al:Landroid/animation/Animator;

    .line 529
    :cond_5
    iget-object v0, p0, Lbzi;->ad:Lcbg;

    invoke-interface {v0}, Lcbg;->b()V

    .line 530
    return-void
.end method

.method private static b(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, p1, v1}, Lapw;->a(FFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 7
    return-void
.end method

.method private static c(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getRotation()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v0, p1, v1}, Lapw;->a(FFF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setRotation(F)V

    .line 9
    return-void
.end method


# virtual methods
.method public final T()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 153
    return-void
.end method

.method final U()Landroid/animation/Animator;
    .locals 15

    .prologue
    .line 452
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 453
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x42280000    # 42.0f

    invoke-static {v1, v2}, Lapw;->a(Landroid/content/Context;F)F

    move-result v1

    .line 454
    iget-object v2, p0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v4, v5

    const/4 v5, 0x1

    neg-float v6, v1

    aput v6, v4, v5

    .line 455
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 456
    new-instance v2, Lsk;

    invoke-direct {v2}, Lsk;-><init>()V

    invoke-virtual {v7, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 457
    const-wide/16 v2, 0x535

    invoke-virtual {v7, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 458
    iget-object v2, p0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    neg-float v1, v1

    aput v1, v4, v5

    const/4 v1, 0x1

    const/4 v5, 0x0

    aput v5, v4, v1

    .line 459
    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 460
    new-instance v1, Lsk;

    invoke-direct {v1}, Lsk;-><init>()V

    invoke-virtual {v8, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 461
    const-wide/16 v2, 0x535

    invoke-virtual {v8, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 462
    iget-object v1, p0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 463
    new-instance v1, Lsl;

    invoke-direct {v1}, Lsl;-><init>()V

    invoke-virtual {v9, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 464
    const-wide/16 v2, 0x29b

    invoke-virtual {v9, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 465
    const-wide/16 v2, 0x14d

    invoke-virtual {v9, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 466
    iget-object v1, p0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    .line 467
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, -0x3f000000    # -8.0f

    invoke-static {v5, v6}, Lapw;->a(Landroid/content/Context;F)F

    move-result v5

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 468
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 469
    new-instance v1, Lsk;

    invoke-direct {v1}, Lsk;-><init>()V

    invoke-virtual {v10, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 470
    const-wide/16 v2, 0x535

    invoke-virtual {v10, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 471
    iget-object v1, p0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 472
    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    invoke-virtual {v11, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 473
    const-wide/16 v2, 0x29b

    invoke-virtual {v11, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 474
    const v1, 0x3ecccccd    # 0.4f

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 475
    invoke-static {v1, v2, v3, v4}, Lbw;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v6

    .line 476
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x42280000    # 42.0f

    invoke-static {v1, v2}, Lapw;->a(Landroid/content/Context;F)F

    move-result v1

    .line 477
    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    neg-float v1, v1

    aput v1, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 478
    invoke-virtual {v12, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 479
    const-wide/16 v2, 0x5dc

    invoke-virtual {v12, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 480
    iget-object v1, p0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f880000    # 1.0625f

    const-wide/16 v4, 0x535

    .line 481
    invoke-static/range {v1 .. v6}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v13

    .line 482
    iget-object v1, p0, Lbzi;->a:Landroid/view/View;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 483
    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 484
    new-instance v1, Lsk;

    invoke-direct {v1}, Lsk;-><init>()V

    invoke-virtual {v14, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 485
    const-wide/16 v2, 0x535

    invoke-virtual {v14, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 486
    iget-object v1, p0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v2, 0x3f880000    # 1.0625f

    const/high16 v3, 0x3f800000    # 1.0f

    const-wide/16 v4, 0x535

    new-instance v6, Lsk;

    invoke-direct {v6}, Lsk;-><init>()V

    .line 487
    invoke-static/range {v1 .. v6}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v1

    .line 489
    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 490
    invoke-virtual {v2, v11}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 491
    invoke-virtual {v2, v12}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 492
    invoke-virtual {v2, v13}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    const-wide/16 v4, 0xa7

    .line 493
    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 495
    invoke-virtual {v0, v14}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 496
    invoke-virtual {v2, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 497
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 498
    invoke-virtual {v1, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 499
    invoke-virtual {v1, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    .line 500
    invoke-virtual {v1, v12}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 501
    invoke-direct {p0, v0}, Lbzi;->a(Landroid/animation/AnimatorSet;)V

    .line 502
    return-object v0
.end method

.method final V()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 546
    const-string v0, "FlingUpDownMethod.performAccept"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    iget-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    iget-object v0, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 549
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 550
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->ad()V

    .line 551
    return-void
.end method

.method final W()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 552
    const-string v0, "FlingUpDownMethod.performReject"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    iget-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 555
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 556
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->ae()V

    .line 557
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 54
    const v0, 0x7f0400bb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 55
    const v0, 0x7f0e0268

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzi;->a:Landroid/view/View;

    .line 56
    const v0, 0x7f0e0269

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbzi;->ag:Landroid/widget/ImageView;

    .line 57
    const v0, 0x7f0e026a

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbzi;->ah:Landroid/widget/ImageView;

    .line 58
    const v0, 0x7f0e0267

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f0e026b

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzi;->af:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0e0265

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzi;->W:Landroid/view/View;

    .line 61
    iget-object v1, p0, Lbzi;->W:Landroid/view/View;

    iget-boolean v0, p0, Lbzi;->an:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 62
    iget-object v1, p0, Lbzi;->W:Landroid/view/View;

    iget-boolean v0, p0, Lbzi;->an:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 63
    const v0, 0x7f0e0266

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzi;->X:Landroid/view/View;

    .line 64
    iget-object v1, p0, Lbzi;->X:Landroid/view/View;

    iget-boolean v0, p0, Lbzi;->an:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 65
    const v0, 0x7f0e0264

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbzj;

    invoke-direct {v1, p0}, Lbzj;-><init>(Lbzi;)V

    .line 66
    invoke-virtual {v0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lbzi;->ak:F

    .line 68
    invoke-direct {p0}, Lbzi;->X()V

    .line 69
    iget-object v0, p0, Lbzi;->ap:Lcam;

    invoke-static {v7, p0, v0}, Lbzr;->a(Landroid/view/View;Lbzu;Lcam;)Lbzr;

    move-result-object v0

    iput-object v0, p0, Lbzi;->ao:Lbzr;

    .line 70
    new-instance v2, Lcbh;

    new-instance v0, Lcbn;

    invoke-direct {v0}, Lcbn;-><init>()V

    invoke-direct {v2, v0}, Lcbh;-><init>(Lcbn;)V

    .line 71
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v1

    .line 72
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 73
    invoke-static {v1}, Lbvs;->c(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 75
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v4, "answer_hint_whitelisted_devices"

    const-string v5, "/hammerhead//bullhead//angler//shamu//gm4g//gm4g_s//AQ4501//gce_x86_phone//gm4gtkc_s//Sparkle_V//Mi-498//AQ4502//imobileiq2//A65//H940//m8_google//m0xx//A10//ctih220//Mi438S//bacon/"

    .line 76
    invoke-interface {v3, v4, v5}, Lbew;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "/"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 78
    if-eqz v0, :cond_3

    .line 80
    invoke-static {v1}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "answer_hint_answered_count"

    const/4 v4, 0x0

    .line 82
    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 84
    invoke-static {v1}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v4, "answer_hint_answered_threshold"

    const-wide/16 v8, 0x3

    invoke-interface {v3, v4, v8, v9}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 85
    const-string v3, "AnswerHintFactory.shouldShowAnswerHint"

    const-string v6, "answerCount: %d, threshold: %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 87
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    .line 88
    invoke-static {v3, v6, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    int-to-long v8, v0

    cmp-long v0, v8, v4

    if-gez v0, :cond_3

    const/4 v0, 0x1

    .line 90
    :goto_3
    if-eqz v0, :cond_4

    .line 91
    new-instance v0, Lcbi;

    const-wide/16 v2, 0x5dc

    const-wide/16 v4, 0xa7

    invoke-direct/range {v0 .. v5}, Lcbi;-><init>(Landroid/content/Context;JJ)V

    .line 96
    :goto_4
    iput-object v0, p0, Lbzi;->ad:Lcbg;

    .line 97
    iget-object v1, p0, Lbzi;->ad:Lcbg;

    const v0, 0x7f0e026c

    .line 98
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    iget-object v3, p0, Lbzi;->ae:Landroid/widget/TextView;

    .line 99
    invoke-interface {v1, p1, v0, v2, v3}, Lcbg;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;Landroid/widget/TextView;)V

    .line 100
    return-object v7

    .line 61
    :cond_0
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 64
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 89
    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 92
    :cond_4
    iget-object v0, v2, Lcbh;->a:Lcbn;

    invoke-virtual {v0, v1}, Lcbn;->a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 93
    if-eqz v2, :cond_5

    .line 94
    new-instance v0, Lcbl;

    const-wide/16 v3, 0x5dc

    const-wide/16 v5, 0xa7

    invoke-direct/range {v0 .. v6}, Lcbl;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;JJ)V

    goto :goto_4

    .line 95
    :cond_5
    new-instance v0, Lcbk;

    invoke-direct {v0}, Lcbk;-><init>()V

    goto :goto_4
.end method

.method public final a(F)V
    .locals 9

    .prologue
    const/high16 v8, 0x437f0000    # 255.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 109
    iput p1, p0, Lbzi;->ak:F

    .line 110
    iget v0, p0, Lbzi;->aa:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbzi;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget v0, p0, Lbzi;->ak:F

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1, v7}, Lapw;->b(FFF)F

    move-result v3

    .line 113
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 114
    cmpl-float v0, v3, v2

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    .line 115
    :goto_0
    iget-object v1, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 116
    iget-object v1, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 117
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v5, 0x41100000    # 9.0f

    mul-float/2addr v1, v5

    sub-float v1, v7, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 118
    iget-object v5, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-static {v5, v1}, Lbzi;->b(Landroid/view/View;F)V

    .line 119
    iget-object v5, p0, Lbzi;->af:Landroid/widget/TextView;

    iget-object v6, p0, Lbzi;->af:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getAlpha()F

    move-result v6

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Lbzi;->b(Landroid/view/View;F)V

    .line 120
    iget-object v5, p0, Lbzi;->W:Landroid/view/View;

    iget-boolean v6, p0, Lbzi;->an:Z

    if-eqz v6, :cond_4

    :goto_1
    invoke-static {v5, v1}, Lbzi;->b(Landroid/view/View;F)V

    .line 121
    iget-object v1, p0, Lbzi;->ae:Landroid/widget/TextView;

    .line 122
    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v5

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-static {v5, v2, v6}, Lapw;->a(FFF)F

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 123
    iget-object v1, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lbzi;->a(Landroid/view/View;F)V

    .line 125
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v5

    .line 126
    if-eqz v0, :cond_5

    const v1, 0x7f0c002e

    .line 127
    :goto_2
    invoke-virtual {v5, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    .line 128
    mul-float v5, v8, v4

    float-to-int v5, v5

    .line 129
    invoke-static {v1, v5}, Lmt;->b(II)I

    move-result v1

    .line 130
    iget-object v5, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 131
    iget-object v5, p0, Lbzi;->ag:Landroid/widget/ImageView;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 132
    iget-object v5, p0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 133
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v1

    invoke-interface {v1}, Lbzh;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v1

    invoke-interface {v1}, Lbzh;->U()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 134
    :cond_0
    iget-object v1, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-static {v1, v2}, Lbzi;->c(Landroid/view/View;F)V

    .line 136
    :goto_3
    invoke-direct {p0}, Lbzi;->Y()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-static {v1, v4}, Lbzi;->b(Landroid/view/View;F)V

    .line 138
    :cond_1
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, v4

    invoke-static {v7, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 139
    iget-object v2, p0, Lbzi;->ah:Landroid/widget/ImageView;

    .line 140
    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0c0095

    invoke-virtual {v2, v4}, Landroid/content/Context;->getColor(I)I

    move-result v2

    sub-float v1, v7, v1

    mul-float/2addr v1, v8

    float-to-int v1, v1

    .line 141
    invoke-static {v2, v1}, Lmt;->b(II)I

    move-result v1

    .line 142
    iget-object v2, p0, Lbzi;->ah:Landroid/widget/ImageView;

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 143
    if-eqz v0, :cond_7

    .line 144
    iget-object v0, p0, Lbzi;->a:Landroid/view/View;

    neg-float v1, v3

    .line 145
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x43160000    # 150.0f

    invoke-static {v2, v4}, Lapw;->a(Landroid/content/Context;F)F

    move-result v2

    mul-float/2addr v1, v2

    .line 146
    invoke-static {v0, v1}, Lbzi;->a(Landroid/view/View;F)V

    .line 150
    :goto_4
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0, v3}, Lbzh;->a(F)V

    .line 151
    :cond_2
    return-void

    .line 114
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 120
    goto/16 :goto_1

    .line 126
    :cond_5
    const v1, 0x7f0c0031

    goto/16 :goto_2

    .line 135
    :cond_6
    iget-object v1, p0, Lbzi;->ah:Landroid/widget/ImageView;

    const/high16 v2, 0x43070000    # 135.0f

    mul-float/2addr v2, v4

    invoke-static {v1, v2}, Lbzi;->c(Landroid/view/View;F)V

    goto :goto_3

    .line 147
    :cond_7
    iget-object v0, p0, Lbzi;->a:Landroid/view/View;

    neg-float v1, v3

    .line 148
    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-static {v2, v4}, Lapw;->a(Landroid/content/Context;F)F

    move-result v2

    mul-float/2addr v1, v2

    .line 149
    invoke-static {v0, v1}, Lbzi;->a(Landroid/view/View;F)V

    goto :goto_4
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lbzi;->am:Landroid/graphics/drawable/Drawable;

    .line 178
    invoke-direct {p0}, Lbzi;->X()V

    .line 179
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lbzg;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 102
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 103
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 205
    if-nez p1, :cond_0

    .line 206
    iget-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    const v1, 0x7f110085

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 207
    iget-object v0, p0, Lbzi;->af:Landroid/widget/TextView;

    const v1, 0x7f110089

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lbzi;->ae:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lbzi;->af:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 211
    iput-boolean p1, p0, Lbzi;->an:Z

    .line 212
    iget-object v0, p0, Lbzi;->W:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 213
    if-eqz p1, :cond_1

    .line 214
    iget-object v0, p0, Lbzi;->W:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lbzi;->X:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lbzi;->W:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    iget-object v0, p0, Lbzi;->W:Landroid/view/View;

    .line 218
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 219
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lbzk;

    invoke-direct {v1, p0}, Lbzk;-><init>(Lbzi;)V

    .line 220
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 169
    iget-object v1, p0, Lbzi;->a:Landroid/view/View;

    if-nez v1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    iget-object v1, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 172
    iget-object v2, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v2

    iget-object v3, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 173
    iget-object v3, p0, Lbzi;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-double v4, v3

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float v1, v3, v1

    float-to-double v6, v1

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v6

    .line 176
    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 10
    invoke-super {p0, p1}, Lbzg;->b(Landroid/os/Bundle;)V

    .line 11
    new-instance v0, Lcam;

    invoke-virtual {p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcam;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbzi;->ap:Lcam;

    .line 12
    return-void
.end method

.method final e(I)V
    .locals 19

    .prologue
    .line 244
    const/4 v2, 0x5

    move/from16 v0, p1

    if-eq v0, v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lbzi;->aa:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lbzi;->aa:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 247
    const-string v2, "FlingUpDownMethod.setAnimationState"

    const/16 v3, 0x45

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Animation loop has completed. Cannot switch to new state: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 249
    :cond_2
    const/4 v2, 0x5

    move/from16 v0, p1

    if-eq v0, v2, :cond_3

    const/4 v2, 0x2

    move/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 250
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lbzi;->aa:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 251
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lbzi;->ab:I

    .line 252
    const/16 p1, 0x4

    .line 253
    :cond_4
    const-string v2, "FlingUpDownMethod.setAnimationState"

    const/16 v3, 0x1c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "animation state: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lbzi;->aa:I

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lip;->I:Landroid/view/View;

    .line 258
    if-eqz v2, :cond_0

    .line 259
    invoke-virtual/range {p0 .. p0}, Lbzi;->k()Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lbzi;->aa:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_9

    .line 261
    move-object/from16 v0, p0

    iget v2, v0, Lbzi;->aa:I

    packed-switch v2, :pswitch_data_0

    .line 400
    const-string v2, "FlingUpDownMethod.updateAnimationState"

    move-object/from16 v0, p0

    iget v3, v0, Lbzi;->aa:I

    const/16 v4, 0x27

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected animation state: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 262
    :pswitch_0
    invoke-direct/range {p0 .. p0}, Lbzi;->aa()V

    goto/16 :goto_0

    .line 265
    :pswitch_1
    const-string v2, "FlingUpDownMethod.startSwipeToAnswerBounceAnimation"

    const-string v3, "Swipe bounce animation."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    .line 267
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbib;->ak(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ae:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 269
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->a:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 272
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->af:Landroid/widget/TextView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 273
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->af:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTranslationY(F)V

    goto/16 :goto_0

    .line 275
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lbzi;->U()Landroid/animation/Animator;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbzi;->Y:Landroid/animation/Animator;

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ad:Lcbg;

    invoke-interface {v2}, Lcbg;->a()V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->Y:Landroid/animation/Animator;

    new-instance v3, Lbzm;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbzm;-><init>(Lbzi;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->Y:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    .line 281
    :pswitch_2
    const-string v2, "FlingUpDownMethod.startSwipeToAnswerSwipeAnimation"

    const-string v3, "Start swipe animation."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 282
    invoke-direct/range {p0 .. p0}, Lbzi;->Z()V

    .line 283
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    goto/16 :goto_0

    .line 286
    :pswitch_3
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    .line 287
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    const/4 v4, 0x0

    sget-object v5, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    .line 288
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v6, v7

    .line 289
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v3, v4

    .line 290
    invoke-static {v2, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 291
    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ah:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 293
    const-wide/16 v6, 0x64

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ae:Landroid/widget/TextView;

    const/high16 v5, 0x3f800000    # 1.0f

    const-wide/16 v6, 0x64

    .line 295
    invoke-static {v2, v5, v6, v7}, Lbzi;->a(Landroid/view/View;FJ)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 296
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->a:Landroid/view/View;

    const/high16 v6, 0x3f800000    # 1.0f

    const-wide/16 v8, 0x64

    .line 297
    invoke-static {v2, v6, v8, v9}, Lbzi;->a(Landroid/view/View;FJ)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 298
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v7, 0x3f800000    # 1.0f

    const-wide/16 v8, 0x64

    .line 299
    invoke-static {v2, v7, v8, v9}, Lbzi;->a(Landroid/view/View;FJ)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 300
    move-object/from16 v0, p0

    iget-object v8, v0, Lbzi;->ah:Landroid/widget/ImageView;

    .line 301
    invoke-direct/range {p0 .. p0}, Lbzi;->Y()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    :goto_1
    const-wide/16 v10, 0x64

    .line 302
    invoke-static {v8, v2, v10, v11}, Lbzi;->a(Landroid/view/View;FJ)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 303
    move-object/from16 v0, p0

    iget-object v8, v0, Lbzi;->a:Landroid/view/View;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/animation/PropertyValuesHolder;

    const/4 v10, 0x0

    sget-object v11, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v12, 0x1

    new-array v12, v12, [F

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v12, v13

    .line 304
    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    sget-object v11, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v12, 0x1

    new-array v12, v12, [F

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v12, v13

    .line 305
    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v11

    aput-object v11, v9, v10

    .line 306
    invoke-static {v8, v9}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 307
    const-wide/16 v10, 0x64

    invoke-virtual {v8, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 308
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v9, v0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    .line 309
    move-object/from16 v0, p0

    iget-object v9, v0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    .line 310
    invoke-virtual {v9, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 311
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 312
    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 313
    invoke-virtual {v3, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 314
    invoke-virtual {v3, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 315
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 316
    invoke-virtual {v2, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 317
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    new-instance v3, Lbzn;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbzn;-><init>(Lbzi;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->Z:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    .line 301
    :cond_6
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_1

    .line 321
    :pswitch_4
    const-string v2, "FlingUpDownMethod.clearSwipeToAnswerUi"

    const-string v3, "Clear swipe animation."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ae:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->a:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 327
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    if-eqz v2, :cond_7

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->cancel()V

    .line 329
    :cond_7
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    .line 330
    invoke-direct/range {p0 .. p0}, Lbzi;->Z()V

    .line 331
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbib;->ak(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 332
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lbzi;->h(Z)V

    goto/16 :goto_0

    .line 334
    :cond_8
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    .line 335
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x42700000    # 60.0f

    invoke-static {v2, v3}, Lapw;->a(Landroid/content/Context;F)F

    move-result v2

    .line 336
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x41000000    # 8.0f

    invoke-static {v3, v4}, Lapw;->a(Landroid/content/Context;F)F

    move-result v9

    .line 337
    const v3, 0x3e199998    # 0.14999998f

    move-object/from16 v0, p0

    iget-object v4, v0, Lbzi;->ag:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v3, v2

    .line 339
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x10e0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    .line 341
    invoke-virtual/range {p0 .. p0}, Lbzi;->s_()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x10e0001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    .line 342
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzi;->a:Landroid/view/View;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    sget-object v7, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v12, 0x0

    const v13, 0x3f733333    # 0.95f

    aput v13, v8, v12

    .line 343
    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v12, 0x0

    const v13, 0x3f866666    # 1.05f

    aput v13, v8, v12

    .line 344
    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    aput-object v7, v5, v6

    .line 345
    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 346
    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 347
    const/4 v4, 0x2

    invoke-virtual {v12, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 348
    div-int/lit8 v4, v10, 0x2

    int-to-long v4, v4

    invoke-virtual {v12, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 349
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v12, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 350
    new-instance v4, Lbzo;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lbzo;-><init>(Lbzi;)V

    invoke-virtual {v12, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzi;->a:Landroid/view/View;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    const/4 v6, 0x0

    sget-object v7, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v8, v13

    .line 352
    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    const/4 v8, 0x1

    new-array v8, v8, [F

    const/4 v13, 0x0

    const/4 v14, 0x0

    aput v14, v8, v13

    .line 353
    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    aput-object v7, v5, v6

    .line 354
    invoke-static {v4, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 355
    div-int/lit8 v4, v10, 0x2

    int-to-long v4, v4

    invoke-virtual {v13, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 356
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v12, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    neg-float v3, v3

    aput v3, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 358
    new-instance v3, Lsl;

    invoke-direct {v3}, Lsl;-><init>()V

    invoke-virtual {v14, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 359
    int-to-long v4, v10

    invoke-virtual {v14, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 360
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->a:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    neg-float v2, v2

    aput v2, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 361
    new-instance v3, Lsl;

    invoke-direct {v3}, Lsl;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 362
    int-to-long v4, v10

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 363
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3f933333    # 1.15f

    int-to-long v6, v10

    new-instance v8, Lsl;

    invoke-direct {v8}, Lsl;-><init>()V

    .line 364
    invoke-static/range {v3 .. v8}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v15

    .line 365
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->af:Landroid/widget/TextView;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x0

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/high16 v16, 0x3f800000    # 1.0f

    aput v16, v7, v8

    .line 366
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v7, 0x1

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/16 v16, 0x0

    aput v16, v7, v8

    .line 367
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 368
    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v16

    .line 369
    int-to-long v4, v10

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 370
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v9, v5, v6

    .line 371
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 372
    new-instance v3, Lsl;

    invoke-direct {v3}, Lsl;-><init>()V

    invoke-virtual {v9, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 373
    int-to-long v4, v10

    invoke-virtual {v9, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 374
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->ae:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v17

    .line 375
    new-instance v3, Lsl;

    invoke-direct {v3}, Lsl;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 376
    int-to-long v4, v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 377
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->a:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v18

    .line 378
    new-instance v3, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v3}, Landroid/view/animation/BounceInterpolator;-><init>()V

    .line 379
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 380
    int-to-long v4, v11

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 381
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzi;->ag:Landroid/widget/ImageView;

    const v4, 0x3f933333    # 1.15f

    const/high16 v5, 0x3f800000    # 1.0f

    int-to-long v6, v10

    new-instance v8, Lsl;

    invoke-direct {v8}, Lsl;-><init>()V

    .line 382
    invoke-static/range {v3 .. v8}, Lbzi;->a(Landroid/view/View;FFJLandroid/view/animation/Interpolator;)Landroid/animation/Animator;

    move-result-object v3

    .line 383
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 384
    new-instance v5, Lsl;

    invoke-direct {v5}, Lsl;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 385
    int-to-long v6, v11

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 386
    move-object/from16 v0, p0

    iget-object v5, v0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v12}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v13}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 387
    move-object/from16 v0, p0

    iget-object v5, v0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    .line 388
    invoke-virtual {v5, v14}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 389
    invoke-virtual {v5, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 390
    invoke-virtual {v5, v15}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 391
    invoke-virtual {v5, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 392
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 393
    move-object/from16 v0, p0

    iget-object v5, v0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->aj:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->af:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    .line 396
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    new-instance v3, Lbzp;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbzp;-><init>(Lbzi;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzi;->ac:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0

    .line 402
    :cond_9
    invoke-direct/range {p0 .. p0}, Lbzi;->ab()V

    goto/16 :goto_0

    .line 261
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 154
    if-eqz p1, :cond_0

    .line 156
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 159
    :goto_0
    invoke-direct {p0}, Lbzi;->Z()V

    .line 160
    invoke-virtual {p0}, Lbzi;->a()Lbzh;

    move-result-object v0

    invoke-interface {v0}, Lbzh;->af()V

    .line 161
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    goto :goto_0
.end method

.method public final g(Z)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lbzi;->ao:Lbzr;

    .line 163
    const/4 v1, 0x0

    iput-boolean v1, v0, Lbzr;->a:Z

    .line 164
    iget-object v0, p0, Lbzi;->ad:Lcbg;

    invoke-interface {v0}, Lcbg;->c()V

    .line 165
    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p0}, Lbzi;->V()V

    .line 168
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-virtual {p0}, Lbzi;->W()V

    goto :goto_0
.end method

.method final h(Z)V
    .locals 2

    .prologue
    .line 506
    if-nez p1, :cond_0

    iget v0, p0, Lbzi;->aa:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 507
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 508
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbzi;->ac:Landroid/animation/Animator;

    .line 509
    return-void
.end method

.method public final l_()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lbzi;->ab()V

    .line 44
    iget-object v0, p0, Lbzi;->ap:Lcam;

    .line 45
    iput-boolean v2, v0, Lcam;->f:Z

    .line 47
    iget-boolean v1, v0, Lcam;->e:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcam;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    iput-boolean v2, v0, Lcam;->e:Z

    .line 49
    iget-object v1, v0, Lcam;->b:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lbzi;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Lit;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lbzi;->e(I)V

    .line 52
    :cond_1
    invoke-super {p0}, Lbzg;->l_()V

    .line 53
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lbzg;->n_()V

    .line 105
    iget-object v0, p0, Lbzi;->ao:Lbzr;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lbzi;->ao:Lbzr;

    invoke-virtual {v0}, Lbzr;->a()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lbzi;->ao:Lbzr;

    .line 108
    :cond_0
    return-void
.end method

.method public final o_()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 13
    invoke-super {p0}, Lbzg;->o_()V

    .line 14
    iget-object v2, p0, Lbzi;->ap:Lcam;

    .line 15
    iput-boolean v8, v2, Lcam;->f:Z

    .line 17
    iget-boolean v0, v2, Lcam;->e:Z

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcam;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    iput-boolean v8, v2, Lcam;->e:Z

    .line 20
    iget-object v0, v2, Lcam;->c:Lcaq;

    .line 21
    iget-boolean v0, v0, Lcaq;->c:Z

    .line 22
    if-eqz v0, :cond_1

    .line 23
    sget-object v3, Lcam;->a:[I

    .line 24
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget v5, v3, v0

    .line 25
    const/16 v6, 0x16

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "get sensor "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    iget-object v6, v2, Lcam;->b:Landroid/hardware/SensorManager;

    invoke-virtual {v6, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v5

    .line 27
    if-eqz v5, :cond_0

    .line 28
    iget-object v6, v2, Lcam;->b:Landroid/hardware/SensorManager;

    invoke-virtual {v6, v2, v5, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 29
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 35
    if-eqz v0, :cond_3

    .line 36
    iget v0, p0, Lbzi;->aa:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    iget v0, p0, Lbzi;->aa:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    .line 37
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lbzi;->ak:F

    .line 38
    invoke-direct {p0}, Lbzi;->X()V

    .line 39
    invoke-virtual {p0, v1}, Lbzi;->f(Z)V

    .line 42
    :cond_3
    :goto_1
    return-void

    .line 40
    :cond_4
    iget v0, p0, Lbzi;->aa:I

    if-ne v0, v8, :cond_3

    .line 41
    invoke-direct {p0}, Lbzi;->aa()V

    goto :goto_1
.end method
