.class public final Lgly;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lgll;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Lgka;

.field private f:[B

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgly;->a:Ljava/lang/Integer;

    .line 4
    iput-object v0, p0, Lgly;->b:Lgll;

    .line 5
    iput-object v0, p0, Lgly;->c:Ljava/lang/Long;

    .line 6
    iput-object v0, p0, Lgly;->d:Ljava/lang/Long;

    .line 7
    iput-object v0, p0, Lgly;->e:Lgka;

    .line 8
    iput-object v0, p0, Lgly;->f:[B

    .line 9
    iput-object v0, p0, Lgly;->g:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lgly;->h:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgly;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgly;->cachedSize:I

    .line 13
    return-void
.end method

.method private a(Lhfp;)Lgly;
    .locals 3

    .prologue
    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 61
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :sswitch_0
    return-object p0

    .line 63
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 65
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 66
    invoke-static {v2}, Lhcw;->g(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgly;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 70
    invoke-virtual {p0, p1, v0}, Lgly;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 72
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->f()[B

    move-result-object v0

    iput-object v0, p0, Lgly;->f:[B

    goto :goto_0

    .line 75
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 76
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgly;->c:Ljava/lang/Long;

    goto :goto_0

    .line 78
    :sswitch_4
    iget-object v0, p0, Lgly;->b:Lgll;

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Lgll;

    invoke-direct {v0}, Lgll;-><init>()V

    iput-object v0, p0, Lgly;->b:Lgll;

    .line 80
    :cond_1
    iget-object v0, p0, Lgly;->b:Lgll;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 83
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 84
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgly;->d:Ljava/lang/Long;

    goto :goto_0

    .line 86
    :sswitch_6
    iget-object v0, p0, Lgly;->e:Lgka;

    if-nez v0, :cond_2

    .line 87
    new-instance v0, Lgka;

    invoke-direct {v0}, Lgka;-><init>()V

    iput-object v0, p0, Lgly;->e:Lgka;

    .line 88
    :cond_2
    iget-object v0, p0, Lgly;->e:Lgka;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgly;->g:Ljava/lang/String;

    goto :goto_0

    .line 92
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgly;->h:Ljava/lang/String;

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 32
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 33
    iget-object v1, p0, Lgly;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x1

    iget-object v2, p0, Lgly;->a:Ljava/lang/Integer;

    .line 35
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_0
    iget-object v1, p0, Lgly;->f:[B

    if-eqz v1, :cond_1

    .line 37
    const/4 v1, 0x2

    iget-object v2, p0, Lgly;->f:[B

    .line 38
    invoke-static {v1, v2}, Lhfq;->c(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_1
    iget-object v1, p0, Lgly;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 40
    const/4 v1, 0x3

    iget-object v2, p0, Lgly;->c:Ljava/lang/Long;

    .line 41
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_2
    iget-object v1, p0, Lgly;->b:Lgll;

    if-eqz v1, :cond_3

    .line 43
    const/4 v1, 0x4

    iget-object v2, p0, Lgly;->b:Lgll;

    .line 44
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_3
    iget-object v1, p0, Lgly;->d:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 46
    const/4 v1, 0x5

    iget-object v2, p0, Lgly;->d:Ljava/lang/Long;

    .line 47
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_4
    iget-object v1, p0, Lgly;->e:Lgka;

    if-eqz v1, :cond_5

    .line 49
    const/4 v1, 0x6

    iget-object v2, p0, Lgly;->e:Lgka;

    .line 50
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_5
    iget-object v1, p0, Lgly;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 52
    const/4 v1, 0x7

    iget-object v2, p0, Lgly;->g:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_6
    iget-object v1, p0, Lgly;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 55
    const/16 v1, 0x8

    iget-object v2, p0, Lgly;->h:Ljava/lang/String;

    .line 56
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_7
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lgly;->a(Lhfp;)Lgly;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 14
    iget-object v0, p0, Lgly;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 15
    const/4 v0, 0x1

    iget-object v1, p0, Lgly;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 16
    :cond_0
    iget-object v0, p0, Lgly;->f:[B

    if-eqz v0, :cond_1

    .line 17
    const/4 v0, 0x2

    iget-object v1, p0, Lgly;->f:[B

    invoke-virtual {p1, v0, v1}, Lhfq;->a(I[B)V

    .line 18
    :cond_1
    iget-object v0, p0, Lgly;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 19
    const/4 v0, 0x3

    iget-object v1, p0, Lgly;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 20
    :cond_2
    iget-object v0, p0, Lgly;->b:Lgll;

    if-eqz v0, :cond_3

    .line 21
    const/4 v0, 0x4

    iget-object v1, p0, Lgly;->b:Lgll;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 22
    :cond_3
    iget-object v0, p0, Lgly;->d:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 23
    const/4 v0, 0x5

    iget-object v1, p0, Lgly;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->a(IJ)V

    .line 24
    :cond_4
    iget-object v0, p0, Lgly;->e:Lgka;

    if-eqz v0, :cond_5

    .line 25
    const/4 v0, 0x6

    iget-object v1, p0, Lgly;->e:Lgka;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_5
    iget-object v0, p0, Lgly;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 27
    const/4 v0, 0x7

    iget-object v1, p0, Lgly;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_6
    iget-object v0, p0, Lgly;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 29
    const/16 v0, 0x8

    iget-object v1, p0, Lgly;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 31
    return-void
.end method
