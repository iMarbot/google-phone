.class final Ledx;
.super Ljava/lang/Object;

# interfaces
.implements Lefv;


# instance fields
.field private synthetic a:Lehq;


# direct methods
.method constructor <init>(Lehq;)V
    .locals 0

    iput-object p1, p0, Ledx;->a:Lehq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 26
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 27
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 28
    iget-boolean v0, v0, Lehq;->g:Z

    .line 29
    if-eqz v0, :cond_0

    iget-object v0, p0, Ledx;->a:Lehq;

    const/4 v1, 0x0

    .line 30
    iput-boolean v1, v0, Lehq;->g:Z

    .line 31
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 33
    iget-object v1, v0, Lehq;->a:Leex;

    invoke-virtual {v1, p1, p2}, Leex;->a(IZ)V

    const/4 v1, 0x0

    iput-object v1, v0, Lehq;->f:Lecl;

    const/4 v1, 0x0

    iput-object v1, v0, Lehq;->e:Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 35
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 36
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 42
    :goto_0
    return-void

    .line 36
    :cond_0
    :try_start_1
    iget-object v0, p0, Ledx;->a:Lehq;

    const/4 v1, 0x1

    .line 37
    iput-boolean v1, v0, Lehq;->g:Z

    .line 38
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 39
    iget-object v0, v0, Lehq;->b:Leff;

    .line 40
    invoke-virtual {v0, p1}, Leff;->onConnectionSuspended(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Ledx;->a:Lehq;

    .line 41
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 42
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledx;->a:Lehq;

    .line 43
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 44
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 2
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 3
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledx;->a:Lehq;

    sget-object v1, Lecl;->a:Lecl;

    .line 4
    iput-object v1, v0, Lehq;->f:Lecl;

    .line 5
    iget-object v1, p0, Ledx;->a:Lehq;

    .line 7
    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v0, v1, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v2, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, v1, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    :cond_1
    :goto_1
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 9
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 10
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 7
    :pswitch_0
    :try_start_1
    iget-object v0, v1, Lehq;->a:Leex;

    iget-object v2, v1, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Leex;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-virtual {v1}, Lehq;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 10
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledx;->a:Lehq;

    .line 11
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 12
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 7
    :cond_2
    :try_start_2
    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget v0, v1, Lehq;->i:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-virtual {v1}, Lehq;->f()V

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    iget-object v0, v1, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    goto :goto_1

    :cond_5
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->e:Lecl;

    iget-object v2, v1, Lehq;->c:Leff;

    iget v2, v2, Leff;->l:I

    iget-object v3, v1, Lehq;->b:Leff;

    iget v3, v3, Leff;->l:I

    if-ge v2, v3, :cond_6

    iget-object v0, v1, Lehq;->f:Lecl;

    :cond_6
    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lecl;)V
    .locals 4

    .prologue
    .line 13
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 14
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 15
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 16
    iput-object p1, v0, Lehq;->f:Lecl;

    .line 17
    iget-object v1, p0, Ledx;->a:Lehq;

    .line 19
    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lehq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget v0, v1, Lehq;->i:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "CompositeGAC"

    const-string v2, "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor"

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    iput v0, v1, Lehq;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    :goto_1
    iget-object v0, p0, Ledx;->a:Lehq;

    .line 21
    iget-object v0, v0, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 22
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 19
    :pswitch_0
    :try_start_1
    iget-object v0, v1, Lehq;->a:Leex;

    iget-object v2, v1, Lehq;->d:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Leex;->a(Landroid/os/Bundle;)V

    :pswitch_1
    invoke-virtual {v1}, Lehq;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledx;->a:Lehq;

    .line 23
    iget-object v1, v1, Lehq;->h:Ljava/util/concurrent/locks/Lock;

    .line 24
    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 19
    :cond_2
    :try_start_2
    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget v0, v1, Lehq;->i:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-virtual {v1}, Lehq;->f()V

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    iget-object v0, v1, Lehq;->b:Leff;

    invoke-virtual {v0}, Leff;->c()V

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->f:Lecl;

    invoke-static {v0}, Lehq;->b(Lecl;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lehq;->c:Leff;

    invoke-virtual {v0}, Leff;->c()V

    iget-object v0, v1, Lehq;->e:Lecl;

    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V

    goto :goto_1

    :cond_5
    iget-object v0, v1, Lehq;->e:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->f:Lecl;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lehq;->e:Lecl;

    iget-object v2, v1, Lehq;->c:Leff;

    iget v2, v2, Leff;->l:I

    iget-object v3, v1, Lehq;->b:Leff;

    iget v3, v3, Leff;->l:I

    if-ge v2, v3, :cond_6

    iget-object v0, v1, Lehq;->f:Lecl;

    :cond_6
    invoke-virtual {v1, v0}, Lehq;->a(Lecl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
