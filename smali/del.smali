.class final Ldel;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lctr;

.field public final b:Ljava/util/List;

.field public final c:Lcte;

.field public d:Z

.field public e:Ldem;

.field public f:Z

.field public g:Ldem;

.field public h:Landroid/graphics/Bitmap;

.field public i:Ldem;

.field private j:Landroid/os/Handler;

.field private k:Lcxl;

.field private l:Z

.field private m:Z

.field private n:Lctb;


# direct methods
.method public constructor <init>(Lcsw;Lctr;IILcuk;Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 1
    .line 3
    iget-object v1, p1, Lcsw;->a:Lcxl;

    .line 6
    iget-object v0, p1, Lcsw;->b:Lcsy;

    invoke-virtual {v0}, Lcsy;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 7
    invoke-static {v0}, Lcsw;->c(Landroid/content/Context;)Lcte;

    move-result-object v2

    const/4 v4, 0x0

    .line 9
    iget-object v0, p1, Lcsw;->b:Lcsy;

    invoke-virtual {v0}, Lcsy;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 10
    invoke-static {v0}, Lcsw;->c(Landroid/content/Context;)Lcte;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcte;->d()Lctb;

    move-result-object v0

    sget-object v3, Lcvx;->a:Lcvx;

    .line 13
    invoke-static {v3}, Ldgn;->a(Lcvx;)Ldgn;

    move-result-object v3

    .line 14
    invoke-virtual {v3, v5}, Ldgn;->a(Z)Ldgn;

    move-result-object v3

    .line 15
    invoke-virtual {v3, v5}, Ldgn;->b(Z)Ldgn;

    move-result-object v3

    .line 16
    invoke-virtual {v3, p3, p4}, Ldgn;->a(II)Ldgn;

    move-result-object v3

    .line 17
    invoke-virtual {v0, v3}, Lctb;->a(Ldgn;)Lctb;

    move-result-object v5

    move-object v0, p0

    move-object v3, p2

    move-object v6, p5

    move-object v7, p6

    .line 19
    invoke-direct/range {v0 .. v7}, Ldel;-><init>(Lcxl;Lcte;Lctr;Landroid/os/Handler;Lctb;Lcuk;Landroid/graphics/Bitmap;)V

    .line 20
    return-void
.end method

.method private constructor <init>(Lcxl;Lcte;Lctr;Landroid/os/Handler;Lctb;Lcuk;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldel;->b:Ljava/util/List;

    .line 23
    iput-boolean v1, p0, Ldel;->d:Z

    .line 24
    iput-boolean v1, p0, Ldel;->l:Z

    .line 25
    iput-boolean v1, p0, Ldel;->m:Z

    .line 26
    iput-object p2, p0, Ldel;->c:Lcte;

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Ldeo;

    invoke-direct {v2, p0}, Ldeo;-><init>(Ldel;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 28
    iput-object p1, p0, Ldel;->k:Lcxl;

    .line 29
    iput-object v0, p0, Ldel;->j:Landroid/os/Handler;

    .line 30
    iput-object p5, p0, Ldel;->n:Lctb;

    .line 31
    iput-object p3, p0, Ldel;->a:Lctr;

    .line 32
    invoke-virtual {p0, p6, p7}, Ldel;->a(Lcuk;Landroid/graphics/Bitmap;)V

    .line 33
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ldel;->a:Lctr;

    invoke-interface {v0}, Lctr;->d()I

    move-result v0

    return v0
.end method

.method final a(Lcuk;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 34
    invoke-static {p1}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-static {p2}, Ldhh;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Ldel;->h:Landroid/graphics/Bitmap;

    .line 36
    iget-object v0, p0, Ldel;->n:Lctb;

    new-instance v1, Ldgn;

    invoke-direct {v1}, Ldgn;-><init>()V

    .line 37
    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Ldgn;->a(Lcuk;Z)Ldgn;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lctb;->a(Ldgn;)Lctb;

    move-result-object v0

    iput-object v0, p0, Ldel;->n:Lctb;

    .line 39
    return-void
.end method

.method final a(Ldem;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldel;->l:Z

    .line 71
    iget-boolean v0, p0, Ldel;->f:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Ldel;->j:Landroid/os/Handler;

    invoke-virtual {v0, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 90
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-boolean v0, p0, Ldel;->d:Z

    if-nez v0, :cond_1

    .line 75
    iput-object p1, p0, Ldel;->i:Ldem;

    goto :goto_0

    .line 78
    :cond_1
    iget-object v0, p1, Ldem;->b:Landroid/graphics/Bitmap;

    .line 79
    if-eqz v0, :cond_3

    .line 80
    invoke-virtual {p0}, Ldel;->d()V

    .line 81
    iget-object v2, p0, Ldel;->e:Ldem;

    .line 82
    iput-object p1, p0, Ldel;->e:Ldem;

    .line 83
    iget-object v0, p0, Ldel;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 84
    iget-object v0, p0, Ldel;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lden;

    .line 85
    invoke-interface {v0}, Lden;->c()V

    .line 86
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 87
    :cond_2
    if-eqz v2, :cond_3

    .line 88
    iget-object v0, p0, Ldel;->j:Landroid/os/Handler;

    invoke-virtual {v0, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 89
    :cond_3
    invoke-virtual {p0}, Ldel;->c()V

    goto :goto_0
.end method

.method final b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldel;->e:Ldem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldel;->e:Ldem;

    .line 42
    iget-object v0, v0, Ldem;->b:Landroid/graphics/Bitmap;

    .line 43
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldel;->h:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method final c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 44
    iget-boolean v0, p0, Ldel;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldel;->l:Z

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-object v0, p0, Ldel;->i:Ldem;

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Ldel;->i:Ldem;

    .line 48
    iput-object v5, p0, Ldel;->i:Ldem;

    .line 49
    invoke-virtual {p0, v0}, Ldel;->a(Ldem;)V

    goto :goto_0

    .line 51
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldel;->l:Z

    .line 52
    iget-object v0, p0, Ldel;->a:Lctr;

    invoke-interface {v0}, Lctr;->c()I

    move-result v0

    .line 53
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 54
    iget-object v2, p0, Ldel;->a:Lctr;

    invoke-interface {v2}, Lctr;->b()V

    .line 55
    new-instance v2, Ldem;

    iget-object v3, p0, Ldel;->j:Landroid/os/Handler;

    iget-object v4, p0, Ldel;->a:Lctr;

    invoke-interface {v4}, Lctr;->e()I

    move-result v4

    invoke-direct {v2, v3, v4, v0, v1}, Ldem;-><init>(Landroid/os/Handler;IJ)V

    iput-object v2, p0, Ldel;->g:Ldem;

    .line 56
    iget-object v0, p0, Ldel;->n:Lctb;

    .line 57
    new-instance v1, Ldhm;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v1, v2}, Ldhm;-><init>(Ljava/lang/Object;)V

    .line 59
    new-instance v2, Ldgn;

    invoke-direct {v2}, Ldgn;-><init>()V

    invoke-virtual {v2, v1}, Ldgn;->a(Lcud;)Ldgn;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lctb;->a(Ldgn;)Lctb;

    move-result-object v0

    iget-object v1, p0, Ldel;->a:Lctr;

    .line 61
    invoke-virtual {v0, v1}, Lctb;->a(Ljava/lang/Object;)Lctb;

    move-result-object v0

    .line 62
    iget-object v1, p0, Ldel;->g:Ldem;

    .line 63
    invoke-virtual {v0, v1, v5}, Lctb;->a(Ldha;Ldgm;)Ldha;

    goto :goto_0
.end method

.method final d()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Ldel;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Ldel;->k:Lcxl;

    iget-object v1, p0, Ldel;->h:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcxl;->a(Landroid/graphics/Bitmap;)V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Ldel;->h:Landroid/graphics/Bitmap;

    .line 69
    :cond_0
    return-void
.end method
