.class public final Lfzp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lfzp;


# instance fields
.field public final b:Z

.field public final c:F

.field public final d:Z

.field public final e:Lgde;

.field public final f:Lfxn;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lfzp;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lfzp;-><init>(Z)V

    sput-object v0, Lfzp;->a:Lfzp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfzp;-><init>(Z)V

    .line 2
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfzp;-><init>(ZLfxn;)V

    .line 6
    return-void
.end method

.method private constructor <init>(ZFLfxn;Lgde;Z)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean p1, p0, Lfzp;->b:Z

    .line 9
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lfzp;->c:F

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzp;->d:Z

    .line 11
    sget-object v0, Lgde;->a:Lgde;

    iput-object v0, p0, Lfzp;->e:Lgde;

    .line 12
    iput-object p3, p0, Lfzp;->f:Lfxn;

    .line 13
    return-void
.end method

.method private constructor <init>(ZLfxn;)V
    .locals 6

    .prologue
    .line 3
    const/high16 v2, 0x42c80000    # 100.0f

    const/4 v3, 0x0

    sget-object v4, Lgde;->a:Lgde;

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lfzp;-><init>(ZFLfxn;Lgde;Z)V

    .line 4
    return-void
.end method
