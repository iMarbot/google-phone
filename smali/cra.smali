.class public final Lcra;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhjz;


# static fields
.field public static final e:Lhlh$e;

.field public static final f:Lhlh$e;

.field public static final g:Lhlh$e;

.field public static final h:Lhlh$e;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    const-string v0, "X-Goog-Api-Key"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 16
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lcra;->e:Lhlh$e;

    .line 17
    const-string v0, "X-Android-Package"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 18
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lcra;->f:Lhlh$e;

    .line 19
    const-string v0, "X-Android-Cert"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 20
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lcra;->g:Lhlh$e;

    .line 21
    const-string v0, "authorization"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 22
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lcra;->h:Lhlh$e;

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcra;->a:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcra;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcra;->c:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcra;->d:Ljava/lang/String;

    .line 6
    return-void
.end method


# virtual methods
.method public final a(Lhlk;Lhjv;Lhjw;)Lhjx;
    .locals 3

    .prologue
    .line 7
    const-string v1, "TranscriptionClientFactory.interceptCall, intercepted "

    .line 9
    iget-object v0, p1, Lhlk;->b:Ljava/lang/String;

    .line 10
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 11
    :goto_0
    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p3, p1, p2}, Lhjw;->a(Lhlk;Lhjv;)Lhjx;

    move-result-object v0

    .line 13
    new-instance v1, Lcrb;

    invoke-direct {v1, p0, v0}, Lcrb;-><init>(Lcra;Lhjx;)V

    .line 14
    return-object v1

    .line 10
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
