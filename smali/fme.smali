.class public Lfme;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhqc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lffd;Lffj;)V
    .locals 3

    .prologue
    .line 52
    const-string v1, "ProxyNumberUtils.getProxyNumber, phoneNumber: "

    .line 53
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 54
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lffk;

    invoke-direct {v1, p0, p2}, Lffk;-><init>(Landroid/content/Context;Lffj;)V

    .line 57
    invoke-static {p0, v0, v1}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Lfhc;)V

    .line 58
    return-void

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lffd;Lffb;)Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3
    invoke-virtual {p1}, Lffd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, incoming, so no proxy"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 50
    :goto_0
    return v0

    .line 6
    :cond_0
    invoke-static {p0}, Lfho;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, not Nova account."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 8
    goto :goto_0

    .line 9
    :cond_1
    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lffe;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, emergency number"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 11
    goto :goto_0

    .line 12
    :cond_2
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 13
    const-string v2, "ProxyNumberUtils.shouldUseProxyNumber, can\'t convert to e164 format, "

    .line 14
    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v2, v1, [Ljava/lang/Object;

    .line 15
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 16
    goto :goto_0

    .line 14
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 18
    :cond_4
    const-string v0, "black_listed_from_proxy_numbers"

    const-string v3, "+1711,"

    .line 19
    invoke-static {v0, v3}, Lfmd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 20
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 21
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 22
    const-string v2, "ProxyNumberUtils.shouldUseProxyNumber, not using proxy number for blacklisted number:, "

    .line 23
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v1, [Ljava/lang/Object;

    .line 24
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 25
    goto/16 :goto_0

    .line 23
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 26
    :cond_6
    invoke-virtual {p1}, Lffd;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 27
    const-string v2, "ProxyNumberUtils.shouldUseProxyNumber, anonymized number, "

    .line 28
    invoke-virtual {p1}, Lffd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmd;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v2, v1, [Ljava/lang/Object;

    .line 29
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 30
    goto/16 :goto_0

    .line 28
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 31
    :cond_8
    invoke-virtual {p2}, Lffb;->d()I

    move-result v0

    if-ne v0, v6, :cond_9

    .line 32
    invoke-virtual {p2}, Lffb;->c()I

    move-result v0

    if-eq v0, v6, :cond_9

    .line 33
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, can\'t tell if we\'re international."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 34
    goto/16 :goto_0

    .line 35
    :cond_9
    invoke-virtual {p1}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lffe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 36
    const-string v4, "ProxyNumberUtils.shouldUseProxyNumber, calling to country: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p2}, Lffb;->c()I

    move-result v0

    invoke-static {v0}, Lfme;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 38
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, Hutch profile calling US"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 39
    goto/16 :goto_0

    .line 36
    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 40
    :cond_b
    invoke-virtual {p2}, Lffb;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 41
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, user is outside the US"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 42
    goto/16 :goto_0

    .line 43
    :cond_c
    invoke-virtual {p2}, Lffb;->b()I

    move-result v0

    if-ne v0, v2, :cond_d

    .line 44
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, on home voice network"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 45
    goto/16 :goto_0

    .line 46
    :cond_d
    invoke-virtual {p2}, Lffb;->b()I

    move-result v0

    if-ne v0, v6, :cond_e

    .line 47
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, can\'t tell if we\'re roaming."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 48
    goto/16 :goto_0

    .line 49
    :cond_e
    const-string v0, "ProxyNumberUtils.shouldUseProxyNumber, true"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 50
    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method
