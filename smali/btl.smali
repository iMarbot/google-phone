.class final Lbtl;
.super Llx;
.source "PG"


# static fields
.field private static m:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "formatted_number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "photo_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "lookup_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "geocoded_location"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "call_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "transcription"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "voicemail_uri"

    aput-object v2, v0, v1

    sput-object v0, Lbtl;->m:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1
    sget-object v2, Lbcd;->a:Landroid/net/Uri;

    sget-object v3, Lbtl;->m:[Ljava/lang/String;

    const-string v4, "call_type = ?"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x4

    .line 2
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const-string v6, "timestamp DESC"

    move-object v0, p0

    move-object v1, p1

    .line 3
    invoke-direct/range {v0 .. v6}, Llx;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    return-void
.end method

.method static a(Landroid/database/Cursor;)Lbtn;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 5
    const/4 v0, 0x3

    :try_start_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 6
    sget-object v1, Lamg;->d:Lamg;

    invoke-static {v1, v0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lamg;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    new-instance v1, Lbto;

    invoke-direct {v1, v3}, Lbto;-><init>(B)V

    .line 12
    invoke-virtual {v1, v3}, Lbto;->a(I)Lbto;

    move-result-object v1

    .line 13
    invoke-virtual {v1, v4, v5}, Lbto;->a(J)Lbto;

    move-result-object v1

    .line 14
    sget-object v2, Lamg;->d:Lamg;

    .line 15
    invoke-virtual {v1, v2}, Lbto;->a(Lamg;)Lbto;

    move-result-object v1

    .line 16
    invoke-virtual {v1, v4, v5}, Lbto;->b(J)Lbto;

    move-result-object v1

    .line 17
    invoke-virtual {v1, v4, v5}, Lbto;->c(J)Lbto;

    move-result-object v1

    .line 18
    invoke-virtual {v1, v3}, Lbto;->b(I)Lbto;

    move-result-object v1

    .line 20
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lbto;->a(I)Lbto;

    move-result-object v1

    const/4 v2, 0x1

    .line 21
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbto;->a(J)Lbto;

    move-result-object v1

    const/4 v2, 0x2

    .line 22
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbto;->a(Ljava/lang/String;)Lbto;

    move-result-object v1

    .line 23
    invoke-virtual {v1, v0}, Lbto;->a(Lamg;)Lbto;

    move-result-object v0

    const/4 v1, 0x4

    .line 24
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->b(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/4 v1, 0x5

    .line 25
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->c(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/4 v1, 0x6

    .line 26
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbto;->b(J)Lbto;

    move-result-object v0

    const/4 v1, 0x7

    .line 27
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->d(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/16 v1, 0x8

    .line 28
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbto;->c(J)Lbto;

    move-result-object v0

    const/16 v1, 0xb

    .line 29
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->e(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/16 v1, 0xc

    .line 30
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->f(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/16 v1, 0x9

    .line 31
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbto;->g(Ljava/lang/String;)Lbto;

    move-result-object v0

    const/16 v1, 0xa

    .line 32
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbto;->b(I)Lbto;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbto;->a()Lbtn;

    move-result-object v0

    .line 34
    return-object v0

    .line 10
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t parse DialerPhoneNumber bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
