.class public final Lgwe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile a:Lhlk;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 40
    invoke-static {}, Lgwe;->a()Lhlk;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lhlk;
    .locals 4

    .prologue
    .line 2
    sget-object v0, Lgwe;->a:Lhlk;

    if-nez v0, :cond_1

    .line 3
    const-class v1, Lgwe;

    monitor-enter v1

    .line 4
    :try_start_0
    sget-object v0, Lgwe;->a:Lhlk;

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lhlk$a;

    .line 7
    invoke-direct {v0}, Lhlk$a;-><init>()V

    .line 8
    const/4 v2, 0x0

    .line 10
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 12
    const/4 v2, 0x0

    .line 14
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 16
    sget-object v2, Lhlk$c;->a:Lhlk$c;

    .line 18
    iput-object v2, v0, Lhlk$a;->c:Lhlk$c;

    .line 20
    const-string v2, "scooby.SpamProtectionService"

    const-string v3, "GetSpamNumbers"

    .line 21
    invoke-static {v2, v3}, Lhlk;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 22
    iput-object v2, v0, Lhlk$a;->d:Ljava/lang/String;

    .line 26
    const/4 v2, 0x1

    iput-boolean v2, v0, Lhlk$a;->e:Z

    .line 28
    new-instance v2, Lhok;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lhok;-><init>(I)V

    .line 29
    invoke-static {v2}, Lhjy;->a(Lhok;)Lhlk$b;

    move-result-object v2

    .line 30
    iput-object v2, v0, Lhlk$a;->a:Lhlk$b;

    .line 32
    new-instance v2, Lhok;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lhok;-><init>(I)V

    .line 33
    invoke-static {v2}, Lhjy;->a(Lhok;)Lhlk$b;

    move-result-object v2

    .line 34
    iput-object v2, v0, Lhlk$a;->b:Lhlk$b;

    .line 36
    invoke-virtual {v0}, Lhlk$a;->a()Lhlk;

    move-result-object v0

    sput-object v0, Lgwe;->a:Lhlk;

    .line 37
    :cond_0
    monitor-exit v1

    .line 38
    :cond_1
    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Lhjw;)Lhon;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lhon;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhon;-><init>(Lhjw;B)V

    return-object v0
.end method
