.class public final Lcil;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/incallui/video/impl/CheckableImageButton$a;


# instance fields
.field public a:Z

.field private b:Lcgk;

.field private c:Lcjl;

.field private d:Lcom/android/incallui/video/impl/CheckableImageButton;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/android/incallui/video/impl/CheckableImageButton;Lcgk;Lcjl;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v0, 0x7f020182

    iput v0, p0, Lcil;->e:I

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgk;

    iput-object v0, p0, Lcil;->b:Lcgk;

    .line 4
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjl;

    iput-object v0, p0, Lcil;->c:Lcjl;

    .line 5
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/video/impl/CheckableImageButton;

    iput-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    .line 6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setVisibility(I)V

    .line 8
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-boolean v2, p0, Lcil;->a:Z

    invoke-virtual {v0, v2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setEnabled(Z)V

    .line 9
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-boolean v2, p0, Lcil;->f:Z

    invoke-virtual {v0, v2}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 10
    iget-object v2, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-boolean v0, p0, Lcil;->g:Z

    if-eqz v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-boolean v2, p0, Lcil;->g:Z

    if-eqz v2, :cond_0

    move-object v1, p0

    .line 12
    :cond_0
    iput-object v1, v0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    .line 13
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget v1, p0, Lcil;->e:I

    invoke-virtual {v0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setImageResource(I)V

    .line 14
    iget-object v0, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    iget-object v1, p0, Lcil;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 15
    return-void

    :cond_1
    move-object v0, p0

    .line 10
    goto :goto_0
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 9

    .prologue
    const v8, 0x7f020182

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16
    const-string v2, "SpeakerButtonController.setSupportedAudio"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "audioState: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_3

    .line 18
    iput-boolean v1, p0, Lcil;->g:Z

    .line 19
    iput-boolean v1, p0, Lcil;->f:Z

    .line 20
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_0

    .line 21
    const v0, 0x7f02012a

    iput v0, p0, Lcil;->e:I

    .line 22
    const v0, 0x7f1101a3

    .line 35
    :goto_0
    iget-object v1, p0, Lcil;->d:Lcom/android/incallui/video/impl/CheckableImageButton;

    invoke-virtual {v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcil;->h:Ljava/lang/CharSequence;

    .line 36
    invoke-virtual {p0}, Lcil;->a()V

    .line 37
    return-void

    .line 23
    :cond_0
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_1

    .line 24
    iput v8, p0, Lcil;->e:I

    .line 25
    const v0, 0x7f1101aa

    goto :goto_0

    .line 26
    :cond_1
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 27
    const v0, 0x7f02014d

    iput v0, p0, Lcil;->e:I

    .line 28
    const v0, 0x7f1101a6

    goto :goto_0

    .line 29
    :cond_2
    const v0, 0x7f020164

    iput v0, p0, Lcil;->e:I

    .line 30
    const v0, 0x7f1101a4

    goto :goto_0

    .line 31
    :cond_3
    iput-boolean v0, p0, Lcil;->g:Z

    .line 32
    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v2

    if-ne v2, v7, :cond_4

    :goto_1
    iput-boolean v0, p0, Lcil;->f:Z

    .line 33
    iput v8, p0, Lcil;->e:I

    .line 34
    const v0, 0x7f1101aa

    goto :goto_0

    :cond_4
    move v0, v1

    .line 32
    goto :goto_1
.end method

.method public final a(Lcom/android/incallui/video/impl/CheckableImageButton;Z)V
    .locals 3

    .prologue
    .line 38
    const-string v0, "SpeakerButtonController.onCheckedChanged"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object v0, p0, Lcil;->b:Lcgk;

    invoke-interface {v0}, Lcgk;->e()V

    .line 40
    iget-object v0, p0, Lcil;->c:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    .line 41
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 42
    const-string v0, "SpeakerButtonController.onClick"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcil;->b:Lcgk;

    invoke-interface {v0}, Lcgk;->k()V

    .line 44
    iget-object v0, p0, Lcil;->c:Lcjl;

    invoke-interface {v0}, Lcjl;->g()V

    .line 45
    return-void
.end method
