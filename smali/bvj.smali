.class public final Lbvj;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field public final a:Landroid/widget/ListView;

.field public final b:Ljava/util/Map;

.field public final c:Lalj;

.field public d:Ljava/util/List;

.field public e:Z

.field private f:Lbfo;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;Lbfo;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lbvj;->b:Ljava/util/Map;

    .line 3
    new-instance v0, Lbvk;

    invoke-direct {v0, p0}, Lbvk;-><init>(Lbvj;)V

    iput-object v0, p0, Lbvj;->g:Landroid/view/View$OnClickListener;

    .line 4
    new-instance v0, Lbvl;

    invoke-direct {v0, p0}, Lbvl;-><init>(Lbvj;)V

    iput-object v0, p0, Lbvj;->h:Landroid/view/View$OnClickListener;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbvj;->d:Ljava/util/List;

    .line 6
    iput-object p1, p0, Lbvj;->a:Landroid/widget/ListView;

    .line 8
    iget-object v0, p0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 9
    invoke-static {v0}, Lbvs;->a(Landroid/content/Context;)Lalj;

    move-result-object v0

    iput-object v0, p0, Lbvj;->c:Lalj;

    .line 10
    iput-object p2, p0, Lbvj;->f:Lbfo;

    .line 11
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 15
    iget-object v0, p0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    .line 16
    iget-object v0, p0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    .line 17
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sub-int v0, v3, v2

    if-gt v1, v0, :cond_0

    .line 18
    iget-object v0, p0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 19
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 20
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21
    add-int v0, v1, v2

    iget-object v1, p0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {p0, v0, v4, v1}, Lbvj;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 24
    :cond_0
    return-void

    .line 23
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lbvj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lbvj;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 14
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20

    .prologue
    .line 25
    if-nez p2, :cond_0

    .line 27
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040032

    const/4 v4, 0x0

    .line 28
    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 30
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->d:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvo;

    .line 32
    iget-object v11, v2, Lbvo;->a:Lcdc;

    .line 35
    iget-object v3, v2, Lbvo;->b:Lbvp$d;

    .line 38
    move-object/from16 v0, p0

    iget-object v4, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 39
    invoke-static {v4}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v4

    .line 41
    iget-boolean v5, v2, Lbvo;->c:Z

    .line 42
    if-nez v5, :cond_1

    .line 45
    iget-object v5, v2, Lbvo;->a:Lcdc;

    .line 48
    iget-object v2, v2, Lbvo;->a:Lcdc;

    .line 49
    invoke-virtual {v2}, Lcdc;->j()I

    move-result v2

    const/4 v6, 0x4

    if-ne v2, v6, :cond_2

    const/4 v2, 0x1

    :goto_0
    new-instance v6, Lbvn;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lbvn;-><init>(Lbvj;)V

    .line 50
    invoke-virtual {v4, v5, v2, v6}, Lbvp;->a(Lcdc;ZLbvp$e;)V

    .line 51
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbvj;->e:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x1000

    .line 52
    invoke-virtual {v11, v2}, Lcdc;->c(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v6, v2

    .line 53
    :goto_1
    const/16 v2, 0x2000

    .line 54
    invoke-virtual {v11, v2}, Lcdc;->c(I)Z

    move-result v12

    .line 55
    iget-object v2, v3, Lbvp$d;->a:Ljava/lang/String;

    iget-object v4, v3, Lbvp$d;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbvj;->c:Lalj;

    .line 56
    invoke-static {v2, v4, v5}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Lalj;)Ljava/lang/String;

    move-result-object v2

    .line 57
    iget-object v7, v3, Lbvp$d;->a:Ljava/lang/String;

    .line 58
    invoke-virtual {v11, v2}, Lcdc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v10, v3, Lbvp$d;->c:Ljava/lang/String;

    iget-object v14, v3, Lbvp$d;->l:Ljava/lang/String;

    iget-object v4, v3, Lbvp$d;->j:Landroid/net/Uri;

    .line 60
    iget v15, v11, Lcdc;->l:I

    .line 62
    const v2, 0x7f0e0143

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 63
    const v2, 0x7f0e0145

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 64
    const v5, 0x7f0e0144

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Landroid/widget/TextView;

    .line 65
    const v5, 0x7f0e0146

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Landroid/widget/TextView;

    .line 66
    const v5, 0x7f0e0148

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 67
    const v16, 0x7f0e0147

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 68
    const/16 v17, 0x8

    move/from16 v0, v17

    if-ne v15, v0, :cond_4

    .line 70
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/CharSequence;

    const/16 v17, 0x0

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lbvj;->a:Landroid/widget/ListView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v18

    .line 73
    const v19, 0x7f110242

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x1

    const-string v18, " \u2022 "

    aput-object v18, v15, v17

    invoke-static {v15}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v15

    .line 74
    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 78
    const v15, 0x7f0c006f

    invoke-virtual {v2, v15}, Landroid/content/Context;->getColor(I)I

    move-result v2

    .line 79
    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 81
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 83
    move-object/from16 v0, p0

    iget-object v15, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v15}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v15

    .line 84
    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v17, 0x7f0d0052

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v0, v2, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 85
    invoke-virtual {v2}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 104
    :goto_2
    if-eqz v12, :cond_5

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 105
    if-eqz v12, :cond_6

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    :goto_4
    if-eqz v6, :cond_7

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 109
    if-eqz v6, :cond_8

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->h:Landroid/view/View$OnClickListener;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :goto_6
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object v2, v10

    .line 113
    :goto_7
    if-eqz v4, :cond_a

    .line 114
    const/4 v7, 0x0

    .line 116
    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->f:Lbfo;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual/range {v2 .. v7}, Lbfo;->a(Landroid/widget/ImageView;Landroid/net/Uri;ZZLbfq;)V

    .line 117
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 118
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    :goto_9
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 122
    const/16 v2, 0x8

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    :goto_a
    iget-object v2, v11, Lcdc;->e:Ljava/lang/String;

    .line 130
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 131
    return-object p2

    .line 49
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 52
    :cond_3
    const/4 v2, 0x0

    move v6, v2

    goto/16 :goto_1

    .line 88
    :cond_4
    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 92
    const v15, 0x7f0c004e

    invoke-virtual {v2, v15}, Landroid/content/Context;->getColor(I)I

    move-result v2

    .line 93
    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 97
    const v15, 0x7f0c004f

    invoke-virtual {v2, v15}, Landroid/content/Context;->getColor(I)I

    move-result v2

    .line 98
    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 99
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 101
    move-object/from16 v0, p0

    iget-object v15, v0, Lbvj;->a:Landroid/widget/ListView;

    invoke-virtual {v15}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v15

    .line 102
    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v17, 0x7f0d0051

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v0, v2, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 103
    invoke-virtual {v2}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_2

    .line 104
    :cond_5
    const/16 v2, 0x8

    goto/16 :goto_3

    .line 107
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 108
    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_5

    .line 111
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_6

    :cond_9
    move-object v2, v7

    .line 112
    goto/16 :goto_7

    .line 115
    :cond_a
    new-instance v7, Lbfq;

    const/4 v5, 0x1

    invoke-direct {v7, v2, v14, v5}, Lbfq;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_8

    .line 119
    :cond_b
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 123
    :cond_c
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v2

    sget-object v3, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v2, v10, v3}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;)Ljava/lang/String;

    move-result-object v2

    .line 126
    invoke-static {v2}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 127
    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a
.end method
