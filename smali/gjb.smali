.class public final Lgjb;
.super Lhft;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgjb;->a:Ljava/lang/Integer;

    .line 4
    iput-object v1, p0, Lgjb;->b:Ljava/lang/Integer;

    .line 5
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgjb;->c:[I

    .line 6
    iput-object v1, p0, Lgjb;->unknownFieldData:Lhfv;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lgjb;->cachedSize:I

    .line 8
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 20
    iget-object v2, p0, Lgjb;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 21
    const/4 v2, 0x1

    iget-object v3, p0, Lgjb;->a:Ljava/lang/Integer;

    .line 22
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 23
    :cond_0
    iget-object v2, p0, Lgjb;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 24
    const/4 v2, 0x2

    iget-object v3, p0, Lgjb;->b:Ljava/lang/Integer;

    .line 25
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 26
    :cond_1
    iget-object v2, p0, Lgjb;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgjb;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 28
    :goto_0
    iget-object v3, p0, Lgjb;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 29
    iget-object v3, p0, Lgjb;->c:[I

    aget v3, v3, v1

    .line 31
    invoke-static {v3}, Lhfq;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 33
    :cond_2
    add-int/2addr v0, v2

    .line 34
    iget-object v1, p0, Lgjb;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 35
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 36
    .line 37
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 38
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    :sswitch_0
    return-object p0

    .line 43
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 44
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjb;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 47
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 48
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjb;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 50
    :sswitch_3
    const/16 v0, 0x18

    .line 51
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 52
    iget-object v0, p0, Lgjb;->c:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 53
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 54
    if-eqz v0, :cond_1

    .line 55
    iget-object v3, p0, Lgjb;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 58
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 59
    aput v3, v2, v0

    .line 60
    invoke-virtual {p1}, Lhfp;->a()I

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 52
    :cond_2
    iget-object v0, p0, Lgjb;->c:[I

    array-length v0, v0

    goto :goto_1

    .line 63
    :cond_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 64
    aput v3, v2, v0

    .line 65
    iput-object v2, p0, Lgjb;->c:[I

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 68
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 70
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 71
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 73
    invoke-virtual {p1}, Lhfp;->g()I

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 76
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 77
    iget-object v2, p0, Lgjb;->c:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 78
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 79
    if-eqz v2, :cond_5

    .line 80
    iget-object v4, p0, Lgjb;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 83
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 84
    aput v4, v0, v2

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 77
    :cond_6
    iget-object v2, p0, Lgjb;->c:[I

    array-length v2, v2

    goto :goto_4

    .line 86
    :cond_7
    iput-object v0, p0, Lgjb;->c:[I

    .line 87
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 9
    iget-object v0, p0, Lgjb;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 10
    const/4 v0, 0x1

    iget-object v1, p0, Lgjb;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 11
    :cond_0
    iget-object v0, p0, Lgjb;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 12
    const/4 v0, 0x2

    iget-object v1, p0, Lgjb;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 13
    :cond_1
    iget-object v0, p0, Lgjb;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgjb;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 14
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgjb;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 15
    const/4 v1, 0x3

    iget-object v2, p0, Lgjb;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lhfq;->a(II)V

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 17
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 18
    return-void
.end method
