.class final Lafs;
.super Lal;
.source "PG"


# instance fields
.field private synthetic a:Lafr;


# direct methods
.method constructor <init>(Lafr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lafs;->a:Lafr;

    invoke-direct {p0, p2}, Lal;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)F
    .locals 3

    .prologue
    .line 36
    check-cast p1, Landroid/view/WindowManager$LayoutParams;

    .line 37
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 38
    iget-object v1, p0, Lafs;->a:Lafr;

    .line 39
    iget v1, v1, Lafr;->h:I

    .line 40
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 41
    iget-object v1, p0, Lafs;->a:Lafr;

    .line 42
    iget v1, v1, Lafr;->i:I

    .line 43
    add-int/2addr v0, v1

    .line 44
    iget-object v1, p0, Lafs;->a:Lafr;

    .line 45
    invoke-static {p1}, Lafr;->a(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v1

    .line 46
    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lafs;->a:Lafr;

    .line 48
    iget-object v1, v1, Lafr;->a:Landroid/content/Context;

    .line 49
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 50
    sub-int v0, v1, v0

    .line 51
    :cond_0
    int-to-float v0, v0

    iget-object v1, p0, Lafs;->a:Lafr;

    .line 52
    iget v1, v1, Lafr;->d:I

    .line 53
    int-to-float v1, v1

    iget-object v2, p0, Lafs;->a:Lafr;

    .line 54
    iget v2, v2, Lafr;->f:I

    .line 55
    int-to-float v2, v2

    .line 56
    invoke-static {v0, v1, v2}, Lafr;->a(FFF)F

    move-result v0

    .line 57
    return v0
.end method

.method public final synthetic a(Ljava/lang/Object;F)V
    .locals 6

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    check-cast p1, Landroid/view/WindowManager$LayoutParams;

    .line 3
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x5

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 4
    :goto_0
    iget-object v4, p0, Lafs;->a:Lafr;

    .line 5
    iget-object v4, v4, Lafr;->a:Landroid/content/Context;

    .line 6
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 7
    iget-object v5, p0, Lafs;->a:Lafr;

    .line 8
    iget-object v5, v5, Lafr;->c:Lael;

    .line 10
    iget-object v5, v5, Lael;->n:Ljava/lang/Integer;

    .line 12
    if-nez v5, :cond_5

    .line 13
    div-int/lit8 v5, v4, 0x2

    int-to-float v5, v5

    cmpl-float v5, p2, v5

    if-lez v5, :cond_4

    .line 15
    :cond_0
    :goto_1
    iget-object v2, p0, Lafs;->a:Lafr;

    .line 16
    iget v2, v2, Lafr;->h:I

    .line 17
    div-int/lit8 v2, v2, 0x2

    iget-object v5, p0, Lafs;->a:Lafr;

    .line 18
    iget v5, v5, Lafr;->i:I

    .line 19
    add-int/2addr v2, v5

    .line 21
    if-eqz v1, :cond_6

    int-to-float v4, v4

    sub-float/2addr v4, p2

    int-to-float v2, v2

    sub-float v2, v4, v2

    :goto_2
    float-to-int v2, v2

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 22
    if-eqz v1, :cond_7

    move v2, v3

    :goto_3
    or-int/lit8 v2, v2, 0x30

    iput v2, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 23
    if-eq v0, v1, :cond_1

    .line 24
    iget-object v0, p0, Lafs;->a:Lafr;

    .line 25
    iget-object v0, v0, Lafr;->c:Lael;

    .line 26
    invoke-virtual {v0, v1}, Lael;->a(Z)V

    .line 27
    :cond_1
    iget-object v0, p0, Lafs;->a:Lafr;

    .line 28
    iget-object v0, v0, Lafr;->c:Lael;

    .line 29
    invoke-virtual {v0}, Lael;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 30
    iget-object v0, p0, Lafs;->a:Lafr;

    .line 31
    iget-object v0, v0, Lafr;->b:Landroid/view/WindowManager;

    .line 32
    iget-object v1, p0, Lafs;->a:Lafr;

    .line 33
    iget-object v1, v1, Lafr;->c:Lael;

    .line 34
    invoke-virtual {v1}, Lael;->e()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 3
    goto :goto_0

    :cond_4
    move v1, v2

    .line 13
    goto :goto_1

    .line 14
    :cond_5
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    and-int/lit8 v5, v5, 0x5

    if-eq v5, v3, :cond_0

    move v1, v2

    goto :goto_1

    .line 21
    :cond_6
    int-to-float v2, v2

    sub-float v2, p2, v2

    goto :goto_2

    .line 22
    :cond_7
    const/4 v2, 0x3

    goto :goto_3
.end method
