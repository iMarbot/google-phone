.class public Lhxp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/Stack;

.field public final synthetic b:Lcnl;


# direct methods
.method public constructor <init>(Lcnl;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, Lhxp;->b:Lcnl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    .line 65
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    iget-object v1, p0, Lhxp;->b:Lcnl;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    :goto_0
    return-void

    .line 3
    :cond_0
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 4
    :try_start_0
    new-instance v1, Lcnl;

    invoke-direct {v1}, Lcnl;-><init>()V

    .line 5
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnd;

    invoke-interface {v0, v1}, Lcnd;->a(Lcms;)V

    .line 6
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lhxs;Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 56
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 57
    invoke-interface {p1}, Lhxs;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcnn;->b(Ljava/io/InputStream;Ljava/lang/String;)Lcms;

    move-result-object v1

    .line 58
    :try_start_0
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnd;

    invoke-interface {v0, v1}, Lcnd;->a(Lcms;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lhxx;)V
    .locals 4

    .prologue
    .line 25
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 26
    :try_start_0
    invoke-interface {p1}, Lhxx;->d()Lhyi;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 27
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnd;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcnd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 33
    const-class v0, Lcnm;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 36
    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 37
    :cond_0
    :try_start_0
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcnm;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Class;)V
    .locals 5

    .prologue
    .line 66
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 68
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhxp;->a:Ljava/util/Stack;

    .line 69
    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Internal stack error: Expected \'"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\' found \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcnl;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 11
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 12
    return-void
.end method

.method public b(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 41
    const-class v0, Lcnm;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 44
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 13
    const-class v0, Lcnm;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 14
    :try_start_0
    new-instance v1, Lcni;

    invoke-direct {v1}, Lcni;-><init>()V

    .line 15
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnm;

    invoke-virtual {v0, v1}, Lcnm;->a(Lcmt;)V

    .line 16
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    return-void

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcmt;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 21
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 22
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 24
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 32
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 46
    const-class v0, Lcnd;

    invoke-virtual {p0, v0}, Lhxp;->a(Ljava/lang/Class;)V

    .line 47
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnd;

    .line 48
    :try_start_0
    new-instance v1, Lcnm;

    invoke-interface {v0}, Lcnd;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcnm;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-interface {v0, v1}, Lcnd;->a(Lcms;)V

    .line 50
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public h()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lhxp;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 55
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
