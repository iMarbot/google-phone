.class public final Lcwl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvp;
.implements Ldie;


# static fields
.field private static q:Lcwm;

.field private static r:Landroid/os/Handler;


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Ldig;

.field public final c:Lcwo;

.field public final d:Lcyu;

.field public e:Lcud;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Lcwz;

.field public j:Lctw;

.field public k:Z

.field public l:Z

.field public m:Ljava/util/List;

.field public n:Lcwr;

.field public o:Lcvo;

.field public volatile p:Z

.field private s:Lps;

.field private t:Lcwm;

.field private u:Lcyu;

.field private v:Lcyu;

.field private w:Lcyu;

.field private x:Lcwt;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 72
    new-instance v0, Lcwm;

    invoke-direct {v0}, Lcwm;-><init>()V

    sput-object v0, Lcwl;->q:Lcwm;

    .line 73
    new-instance v0, Landroid/os/Handler;

    .line 74
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcwn;

    invoke-direct {v2}, Lcwn;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, Lcwl;->r:Landroid/os/Handler;

    .line 75
    return-void
.end method

.method constructor <init>(Lcyu;Lcyu;Lcyu;Lcyu;Lcwo;Lps;)V
    .locals 8

    .prologue
    .line 1
    sget-object v7, Lcwl;->q:Lcwm;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcwl;-><init>(Lcyu;Lcyu;Lcyu;Lcyu;Lcwo;Lps;Lcwm;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lcyu;Lcyu;Lcyu;Lcyu;Lcwo;Lps;Lcwm;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcwl;->a:Ljava/util/List;

    .line 6
    new-instance v0, Ldih;

    invoke-direct {v0}, Ldih;-><init>()V

    .line 7
    iput-object v0, p0, Lcwl;->b:Ldig;

    .line 8
    iput-object p1, p0, Lcwl;->d:Lcyu;

    .line 9
    iput-object p2, p0, Lcwl;->u:Lcyu;

    .line 10
    iput-object p3, p0, Lcwl;->v:Lcyu;

    .line 11
    iput-object p4, p0, Lcwl;->w:Lcyu;

    .line 12
    iput-object p5, p0, Lcwl;->c:Lcwo;

    .line 13
    iput-object p6, p0, Lcwl;->s:Lps;

    .line 14
    iput-object p7, p0, Lcwl;->t:Lcwm;

    .line 15
    return-void
.end method


# virtual methods
.method public final a()Lcyu;
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcwl;->g:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcwl;->v:Lcyu;

    .line 26
    :goto_0
    return-object v0

    .line 25
    :cond_0
    iget-boolean v0, p0, Lcwl;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcwl;->w:Lcyu;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcwl;->u:Lcyu;

    goto :goto_0
.end method

.method public final a(Lcvo;)V
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcwl;->a()Lcyu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcyu;->execute(Ljava/lang/Runnable;)V

    .line 54
    return-void
.end method

.method public final a(Lcwt;)V
    .locals 2

    .prologue
    .line 50
    iput-object p1, p0, Lcwl;->x:Lcwt;

    .line 51
    sget-object v0, Lcwl;->r:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 52
    return-void
.end method

.method public final a(Lcwz;Lctw;)V
    .locals 2

    .prologue
    .line 46
    iput-object p1, p0, Lcwl;->i:Lcwz;

    .line 47
    iput-object p2, p0, Lcwl;->j:Lctw;

    .line 48
    sget-object v0, Lcwl;->r:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 49
    return-void
.end method

.method public final a(Ldgo;)V
    .locals 2

    .prologue
    .line 16
    invoke-static {}, Ldhw;->a()V

    .line 17
    iget-object v0, p0, Lcwl;->b:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 18
    iget-boolean v0, p0, Lcwl;->k:Z

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcwl;->n:Lcwr;

    iget-object v1, p0, Lcwl;->j:Lctw;

    invoke-interface {p1, v0, v1}, Ldgo;->a(Lcwz;Lctw;)V

    .line 23
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-boolean v0, p0, Lcwl;->l:Z

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lcwl;->x:Lcwt;

    invoke-interface {p1, v0}, Ldgo;->a(Lcwt;)V

    goto :goto_0

    .line 22
    :cond_1
    iget-object v0, p0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 28
    invoke-static {}, Ldhw;->a()V

    .line 29
    iget-object v0, p0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 30
    iput-object v2, p0, Lcwl;->e:Lcud;

    .line 31
    iput-object v2, p0, Lcwl;->n:Lcwr;

    .line 32
    iput-object v2, p0, Lcwl;->i:Lcwz;

    .line 33
    iget-object v0, p0, Lcwl;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcwl;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 35
    :cond_0
    iput-boolean v3, p0, Lcwl;->l:Z

    .line 36
    iput-boolean v3, p0, Lcwl;->p:Z

    .line 37
    iput-boolean v3, p0, Lcwl;->k:Z

    .line 38
    iget-object v0, p0, Lcwl;->o:Lcvo;

    .line 39
    iget-object v1, v0, Lcvo;->d:Lcvs;

    invoke-virtual {v1, v3}, Lcvs;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {v0}, Lcvo;->a()V

    .line 41
    :cond_1
    iput-object v2, p0, Lcwl;->o:Lcvo;

    .line 42
    iput-object v2, p0, Lcwl;->x:Lcwt;

    .line 43
    iput-object v2, p0, Lcwl;->j:Lctw;

    .line 44
    iget-object v0, p0, Lcwl;->s:Lps;

    invoke-interface {v0, p0}, Lps;->a(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method final b(Ldgo;)Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcwl;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwl;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    iget-object v0, p0, Lcwl;->b:Ldig;

    invoke-virtual {v0}, Ldig;->a()V

    .line 56
    iget-boolean v0, p0, Lcwl;->p:Z

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0, v3}, Lcwl;->a(Z)V

    .line 70
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received an exception without any callbacks to notify"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    iget-boolean v0, p0, Lcwl;->l:Z

    if-eqz v0, :cond_2

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already failed once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcwl;->l:Z

    .line 64
    iget-object v0, p0, Lcwl;->c:Lcwo;

    iget-object v1, p0, Lcwl;->e:Lcud;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcwo;->a(Lcud;Lcwr;)V

    .line 65
    iget-object v0, p0, Lcwl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgo;

    .line 66
    invoke-virtual {p0, v0}, Lcwl;->b(Ldgo;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 67
    iget-object v2, p0, Lcwl;->x:Lcwt;

    invoke-interface {v0, v2}, Ldgo;->a(Lcwt;)V

    goto :goto_1

    .line 69
    :cond_4
    invoke-virtual {p0, v3}, Lcwl;->a(Z)V

    goto :goto_0
.end method

.method public final u_()Ldig;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcwl;->b:Ldig;

    return-object v0
.end method
