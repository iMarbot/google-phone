.class public final enum Lcmw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcmx;


# static fields
.field public static final enum a:Lcmw;

.field public static final enum b:Lcmw;

.field public static final enum c:Lcmw;

.field public static final enum d:Lcmw;

.field public static final enum e:Lcmw;

.field private static synthetic f:[Lcmw;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lcmw;

    const-string v1, "FLAGS"

    invoke-direct {v0, v1, v2}, Lcmw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcmw;->a:Lcmw;

    .line 4
    new-instance v0, Lcmw;

    const-string v1, "ENVELOPE"

    invoke-direct {v0, v1, v3}, Lcmw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcmw;->b:Lcmw;

    .line 5
    new-instance v0, Lcmw;

    const-string v1, "STRUCTURE"

    invoke-direct {v0, v1, v4}, Lcmw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcmw;->c:Lcmw;

    .line 6
    new-instance v0, Lcmw;

    const-string v1, "BODY_SANE"

    invoke-direct {v0, v1, v5}, Lcmw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcmw;->d:Lcmw;

    .line 7
    new-instance v0, Lcmw;

    const-string v1, "BODY"

    invoke-direct {v0, v1, v6}, Lcmw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcmw;->e:Lcmw;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lcmw;

    sget-object v1, Lcmw;->a:Lcmw;

    aput-object v1, v0, v2

    sget-object v1, Lcmw;->b:Lcmw;

    aput-object v1, v0, v3

    sget-object v1, Lcmw;->c:Lcmw;

    aput-object v1, v0, v4

    sget-object v1, Lcmw;->d:Lcmw;

    aput-object v1, v0, v5

    sget-object v1, Lcmw;->e:Lcmw;

    aput-object v1, v0, v6

    sput-object v0, Lcmw;->f:[Lcmw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lcmw;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcmw;->f:[Lcmw;

    invoke-virtual {v0}, [Lcmw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcmw;

    return-object v0
.end method
