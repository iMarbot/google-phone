.class public Ldou;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:[Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:[Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:Ljava/util/Map;

.field private static g:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    const-class v0, Ldou;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldou;->a:Ljava/lang/String;

    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "https://www.googleapis.com/auth/plus.me"

    aput-object v1, v0, v3

    const-string v1, "https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    aput-object v1, v0, v4

    sput-object v0, Ldou;->b:[Ljava/lang/String;

    .line 96
    const-string v1, "oauth2:"

    const-string v0, " "

    sget-object v2, Ldou;->b:[Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Ldou;->c:Ljava/lang/String;

    .line 97
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "https://www.googleapis.com/auth/plus.contactphotos"

    aput-object v1, v0, v3

    sput-object v0, Ldou;->d:[Ljava/lang/String;

    .line 98
    const-string v1, "oauth2:"

    const-string v0, " "

    sget-object v2, Ldou;->d:[Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    sput-object v0, Ldou;->e:Ljava/lang/String;

    .line 99
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Ldou;->f:Ljava/util/Map;

    .line 100
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Ldou;->g:Ljava/util/Map;

    return-void

    .line 96
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldow;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 46
    .line 47
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 48
    const-string v0, "GET"

    .line 49
    invoke-static {p5}, Ldrn;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 50
    invoke-static {p1, p2, v0, v2}, Ldhh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ldrp;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 51
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 58
    :goto_0
    if-nez v0, :cond_0

    move-object v0, v1

    .line 78
    :goto_1
    return-object v0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    :try_start_1
    sget-object v2, Ldou;->a:Ljava/lang/String;

    const-string v3, "Error looking up phone number."

    invoke-static {v2, v3, v0}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    move-object v0, v1

    .line 56
    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0

    .line 60
    :cond_0
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v5

    .line 62
    invoke-virtual {v0}, Ldrp;->b()I

    move-result v2

    invoke-static {v2}, Ldhh;->d(I)Z

    move-result v2

    .line 63
    if-nez v2, :cond_1

    .line 65
    invoke-virtual {v0}, Ldrp;->a()J

    move-result-wide v2

    .line 66
    invoke-virtual {v0}, Ldrp;->b()I

    move-result v0

    sget-object v4, Lbkx$a;->a:Lbkx$a;

    .line 67
    invoke-interface {v5, v2, v3, v0, v4}, Lbku;->a(JILbkx$a;)V

    move-object v0, v1

    .line 68
    goto :goto_1

    .line 69
    :cond_1
    const/16 v2, 0x21c

    .line 70
    const-string v1, "window"

    .line 71
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 72
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 73
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 74
    iget v1, v3, Landroid/graphics/Point;->x:I

    .line 75
    div-int/lit8 v1, v1, 0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 77
    invoke-static {}, Ldhh;->h()Ljava/lang/String;

    move-result-object v3

    move-object v1, p3

    move-object v2, p4

    .line 78
    invoke-static/range {v0 .. v5}, Ldov;->a(Ldrp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbku;)Ldow;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 18
    .line 19
    invoke-static {}, Ldhh;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20
    sget-object v1, Ldou;->e:Ljava/lang/String;

    invoke-static {p0, p1, v1}, Ldrn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 21
    if-nez v1, :cond_0

    .line 37
    :goto_0
    return-object v0

    .line 23
    :cond_0
    invoke-static {v1}, Ldrn;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    move-object v2, v1

    .line 24
    :goto_1
    const/4 v1, 0x4

    :try_start_0
    invoke-static {v1}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 25
    const-string v1, "GET"

    invoke-static {p0, p2, v1, v2}, Ldhh;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/io/InputStream;
    :try_end_0
    .catch Ldrf; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 26
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 28
    :catch_0
    move-exception v1

    .line 29
    :try_start_1
    sget-object v3, Ldou;->a:Ljava/lang/String;

    const-string v4, "Authentication error. Already invalidated auth token and retried. Aborting lookup."

    invoke-static {v3, v4}, Lbvs;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-static {p0, p1}, Ldou;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    :try_start_2
    const-string v3, "GET"

    invoke-static {p0, p2, v3, v2}, Ldhh;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/io/InputStream;
    :try_end_2
    .catch Ldrf; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 32
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 35
    :catch_1
    move-exception v2

    :try_start_3
    sget-object v2, Ldou;->a:Ljava/lang/String;

    const-string v3, "Tried again but still got auth error during image lookup."

    invoke-static {v2, v3, v1}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 36
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    throw v0

    :cond_1
    move-object v2, v0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://www.googleapis.com/plus/v2whitelisted/people/lookup?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-static {}, Ldhh;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "&type=phone&fields=kind,items(id,metadata(objectType,plusPageType,attributions),names,phoneNumbers(value,type,formattedType,canonicalizedForm),addresses(value,type,formattedType),images(url,metadata(container)),urls(value),placeDetails)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    if-eqz p2, :cond_0

    .line 83
    const-string v1, "&includePlaces=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    const-string v1, "&callType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    if-eqz p3, :cond_1

    .line 86
    const-string v1, "incoming"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :goto_0
    :try_start_0
    const-string v1, "&id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-static {p1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 87
    :cond_1
    const-string v1, "outgoing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    sget-object v1, Ldou;->a:Ljava/lang/String;

    const-string v2, "Error encoding phone number.  UTF-8 is not supported?!"

    invoke-static {v1, v2, v0}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 92
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2
    const-class v1, Ldou;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldou;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Ldwf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 3
    sget-object v0, Ldou;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    monitor-exit v1

    return-void

    .line 2
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ldow;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 39
    sget-object v1, Ldou;->c:Ljava/lang/String;

    invoke-static {p1, p2, v1}, Ldrn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 40
    if-nez v5, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-object v0

    .line 42
    :cond_1
    invoke-static {p1, p3, p5, p6}, Ldou;->a(Landroid/content/Context;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v2

    .line 43
    if-eqz v2, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    .line 45
    invoke-direct/range {v0 .. v5}, Ldou;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldow;

    move-result-object v0

    goto :goto_0
.end method

.method private static declared-synchronized b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5
    const-class v1, Ldou;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldou;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Ldwf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 6
    sget-object v0, Ldou;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    monitor-exit v1

    return-void

    .line 5
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ldow;
    .locals 3

    .prologue
    .line 8
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 10
    :try_start_0
    invoke-direct/range {p0 .. p6}, Ldou;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ldow;
    :try_end_0
    .catch Ldrf; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 17
    :goto_0
    return-object v0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    sget-object v1, Ldou;->a:Ljava/lang/String;

    const-string v2, "Authentication error. Already invalidated auth token and retried. Aborting lookup."

    invoke-static {v1, v2}, Lbvs;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    invoke-static {p1, p2}, Ldou;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 14
    :try_start_1
    invoke-direct/range {p0 .. p6}, Ldou;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ldow;
    :try_end_1
    .catch Ldrf; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 16
    :catch_1
    move-exception v1

    sget-object v1, Ldou;->a:Ljava/lang/String;

    const-string v2, "Tried again but still got auth error during phone number lookup."

    invoke-static {v1, v2, v0}, Lbvs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 17
    const/4 v0, 0x0

    goto :goto_0
.end method
