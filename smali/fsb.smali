.class public final Lfsb;
.super Lfsm;
.source "PG"

# interfaces
.implements Lfpa;


# instance fields
.field public final call:Lfnp;

.field public final callStateListener:Lfsl;

.field public callback$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFEDIN4TJ9CDIIULJ9CHIMUIBEE1QN8KRLE9J62OR54H1M2R3CC9GM6QPR0:Lfmk;

.field public final captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

.field public captureTargets:Lfwg;

.field public final captureTargetsChangedRunnable:Ljava/lang/Runnable;

.field public encodeSurface:Landroid/view/Surface;

.field public encoder:Lfoz;

.field public final encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

.field public frameTransform:[F

.field public inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field public inputTextureName:I

.field public lastEncodeTimeMs:J

.field public mediaConnected:Z

.field public final mediaSourceScreencastHelper:Lfrd;

.field public volatile msPerFrame:J

.field public final newFrameCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field public rotationTransform:[F

.field public surfaceTextureTransform:[F


# direct methods
.method public constructor <init>(Lfnp;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 1
    invoke-virtual {p1}, Lfnp;->getParticipantManager()Lfri;

    move-result-object v0

    invoke-virtual {v0}, Lfri;->getLocalParticipant()Lfrh;

    move-result-object v0

    invoke-virtual {p1}, Lfnp;->getGlManager()Lfpc;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lfsm;-><init>(Lfrh;Lfpc;)V

    .line 2
    new-instance v0, Lfsl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfsl;-><init>(Lfsb;Lfmt;)V

    iput-object v0, p0, Lfsb;->callStateListener:Lfsl;

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lfsb;->newFrameCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lfwe;

    invoke-direct {v1}, Lfwe;-><init>()V

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    .line 5
    new-instance v0, Lfsc;

    invoke-direct {v0, p0}, Lfsc;-><init>(Lfsb;)V

    iput-object v0, p0, Lfsb;->captureTargetsChangedRunnable:Ljava/lang/Runnable;

    .line 6
    iput v2, p0, Lfsb;->inputTextureName:I

    .line 7
    iput-wide v4, p0, Lfsb;->lastEncodeTimeMs:J

    .line 8
    iput-wide v4, p0, Lfsb;->msPerFrame:J

    .line 9
    new-instance v0, Lfwg;

    invoke-direct {v0}, Lfwg;-><init>()V

    iput-object v0, p0, Lfsb;->captureTargets:Lfwg;

    .line 11
    sget-object v0, Lfwm;->a:[F

    .line 12
    iput-object v0, p0, Lfsb;->rotationTransform:[F

    .line 13
    new-array v0, v3, [F

    iput-object v0, p0, Lfsb;->frameTransform:[F

    .line 14
    new-array v0, v3, [F

    iput-object v0, p0, Lfsb;->surfaceTextureTransform:[F

    .line 15
    iput-object p1, p0, Lfsb;->call:Lfnp;

    .line 16
    new-instance v0, Lfrd;

    invoke-direct {v0, p1}, Lfrd;-><init>(Lfvr;)V

    iput-object v0, p0, Lfsb;->mediaSourceScreencastHelper:Lfrd;

    .line 17
    invoke-virtual {p1}, Lfnp;->getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    move-result-object v0

    iput-object v0, p0, Lfsb;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    .line 18
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iget-object v1, p0, Lfsb;->callStateListener:Lfsl;

    invoke-virtual {v0, v1}, Lfnv;->addCallStateListener(Lfof;)V

    .line 19
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    new-instance v1, Lfsd;

    invoke-direct {v1, p0}, Lfsd;-><init>(Lfsb;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 20
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    invoke-virtual {v0}, Lfnv;->isMediaConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfsb;->mediaConnected:Z

    .line 22
    invoke-direct {p0}, Lfsb;->maybeUpdateVideoChatVideoMuteState()V

    .line 23
    :cond_0
    return-void
.end method

.method static synthetic access$102(Lfsb;Z)Z
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Lfsb;->mediaConnected:Z

    return p1
.end method

.method static synthetic access$200(Lfsb;)V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Lfsb;->maybeUpdateVideoChatVideoMuteState()V

    return-void
.end method

.method private final initializeGlContext()V
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lfmk;->f()I

    move-result v0

    iput v0, p0, Lfsb;->inputTextureName:I

    .line 42
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lfsb;->inputTextureName:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 43
    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 44
    iget v0, v0, Lfwe;->c:I

    .line 45
    if-lez v0, :cond_0

    .line 46
    iget-object v1, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    .line 47
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 48
    iget v2, v0, Lfwe;->c:I

    .line 49
    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 50
    iget v0, v0, Lfwe;->d:I

    .line 51
    invoke-virtual {v1, v2, v0}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 52
    :cond_0
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lfsb;->encodeSurface:Landroid/view/Surface;

    .line 53
    iget-object v0, p0, Lfsb;->captureTargets:Lfwg;

    iget-object v0, v0, Lfwg;->a:Ljava/util/List;

    iget-object v1, p0, Lfsb;->encodeSurface:Landroid/view/Surface;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lfsg;

    invoke-direct {v1, p0}, Lfsg;-><init>(Lfsb;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 55
    iget-object v0, p0, Lfsb;->captureTargetsChangedRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhcw;->a(Ljava/lang/Runnable;Z)V

    .line 56
    iget-object v0, p0, Lfsb;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    iget-object v1, p0, Lfsb;->call:Lfnp;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->createEncoder(Lfnp;Lfpa;)Lfoz;

    move-result-object v0

    iput-object v0, p0, Lfsb;->encoder:Lfoz;

    .line 57
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    invoke-interface {v0}, Lfoz;->initializeGLContext()V

    .line 58
    invoke-virtual {p0}, Lfsb;->updateEncodeSize()V

    .line 59
    return-void
.end method

.method private final isLocalSourceExternallyManaged()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lfsb;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallStateInfo()Lfvu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfsb;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallStateInfo()Lfvu;

    move-result-object v0

    .line 175
    iget-object v0, v0, Lfvu;->b:Lfvs;

    .line 176
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfsb;->call:Lfnp;

    .line 177
    invoke-virtual {v0}, Lfnp;->getCallStateInfo()Lfvu;

    move-result-object v0

    .line 178
    iget-object v0, v0, Lfvu;->b:Lfvs;

    .line 181
    :cond_0
    const/4 v0, 0x0

    .line 182
    return v0
.end method

.method private final maybeUpdateVideoChatVideoMuteState()V
    .locals 2

    .prologue
    .line 97
    iget-boolean v0, p0, Lfsb;->mediaConnected:Z

    if-nez v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lfsb;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iget-boolean v1, p0, Lfsb;->isVideoMuted:Z

    invoke-virtual {v0, v1}, Lfnv;->publishVideoMuteState(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final bindToSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfsb;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-super {p0, p1}, Lfsm;->bindToSurface(Landroid/graphics/SurfaceTexture;)V

    .line 39
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    invoke-virtual {v0, p0}, Lfpc;->addVideoSource(Lfsm;)V

    goto :goto_0
.end method

.method public final getCapabilities()Lfwf;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Lfwf;

    invoke-direct {v0}, Lfwf;-><init>()V

    .line 103
    invoke-virtual {p0}, Lfsb;->getCurrentCodec()I

    move-result v1

    invoke-static {v1}, Lfor;->getOutgoingVideoSpec(I)Lfwp;

    .line 104
    invoke-static {}, Lfor;->getMaxOutgoingVideoSpec()Lfwp;

    .line 105
    return-object v0
.end method

.method final getCurrentCodec()I
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    invoke-interface {v0}, Lfoz;->getCurrentCodec()I

    move-result v0

    .line 140
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDebugName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "InputCapture"

    return-object v0
.end method

.method public final getTextureName()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lfsb;->inputTextureName:I

    return v0
.end method

.method final getTransformationMatrix()[F
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lfsb;->frameTransform:[F

    return-object v0
.end method

.method public final isExternalTexture()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method final synthetic lambda$initializeGlContext$4$VideoInputSurfaceImpl(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lfsb;->newFrameCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 195
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    invoke-virtual {v0, p0}, Lfpc;->notifyFrame(Lfsm;)V

    .line 196
    return-void
.end method

.method final synthetic lambda$new$0$VideoInputSurfaceImpl()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lfsb;->callback$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFEDIN4TJ9CDIIULJ9CHIMUIBEE1QN8KRLE9J62OR54H1M2R3CC9GM6QPR0:Lfmk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfsb;->captureTargets:Lfwg;

    iget-object v0, v0, Lfwg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    invoke-static {}, Lfmk;->d()V

    .line 211
    :cond_0
    return-void
.end method

.method final synthetic lambda$new$1$VideoInputSurfaceImpl()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Lfsb;->initializeGlContext()V

    return-void
.end method

.method final synthetic lambda$onCurrentCodecChanged$8$VideoInputSurfaceImpl()V
    .locals 0

    .prologue
    .line 183
    invoke-virtual {p0}, Lfsb;->getCapabilities()Lfwf;

    invoke-static {}, Lfmk;->e()V

    return-void
.end method

.method final synthetic lambda$release$2$VideoInputSurfaceImpl()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    invoke-interface {v0}, Lfoz;->release()V

    .line 200
    :cond_0
    iget-object v0, p0, Lfsb;->encodeSurface:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lfsb;->encodeSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 202
    :cond_1
    iget-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 204
    :cond_2
    iget v0, p0, Lfsb;->inputTextureName:I

    if-eqz v0, :cond_3

    .line 205
    iget v0, p0, Lfsb;->inputTextureName:I

    invoke-static {v0}, Lfmk;->c(I)V

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lfsb;->inputTextureName:I

    .line 207
    :cond_3
    return-void
.end method

.method final synthetic lambda$setCaptureFormat$5$VideoInputSurfaceImpl(Z)V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lfsb;->isLocalSourceExternallyManaged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lfsb;->mediaSourceScreencastHelper:Lfrd;

    invoke-virtual {v0, p1}, Lfrd;->setIsScreencast(Z)V

    .line 193
    :cond_0
    return-void
.end method

.method final synthetic lambda$setCaptureFormat$6$VideoInputSurfaceImpl()V
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lfsb;->updateEncodeSize()V

    .line 189
    iget-object v0, p0, Lfsb;->captureTargetsChangedRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhcw;->a(Ljava/lang/Runnable;Z)V

    .line 190
    return-void
.end method

.method final synthetic lambda$setMirrorLocalRendering$7$VideoInputSurfaceImpl(Z)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    invoke-interface {v0, p1}, Lfoz;->setFlipNeeded(Z)V

    .line 186
    :cond_0
    invoke-virtual {p0}, Lfsb;->updateEncodeSize()V

    .line 187
    return-void
.end method

.method final synthetic lambda$unbind$3$VideoInputSurfaceImpl()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lfsb;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method public final onCurrentCodecChanged(I)V
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lfsb;->updateEncodeSize()V

    .line 171
    iget-object v0, p0, Lfsb;->callback$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFEDIN4TJ9CDIIULJ9CHIMUIBEE1QN8KRLE9J62OR54H1M2R3CC9GM6QPR0:Lfmk;

    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Lfsk;

    invoke-direct {v0, p0}, Lfsk;-><init>(Lfsb;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 173
    :cond_0
    return-void
.end method

.method public final processFrame()Z
    .locals 14

    .prologue
    const-wide/16 v12, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 60
    const-string v0, "Attempted to processFrame without initializing."

    iget-object v1, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-static {v0, v1}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 62
    const-wide/16 v0, 0x0

    .line 63
    iget-object v2, p0, Lfsb;->newFrameCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v2

    .line 64
    if-lez v2, :cond_0

    .line 65
    :try_start_0
    iget-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 66
    iget-object v0, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lfsb;->surfaceTextureTransform:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 67
    iget-object v0, p0, Lfsb;->surfaceTextureTransform:[F

    iget-object v1, p0, Lfsb;->rotationTransform:[F

    iget-object v3, p0, Lfsb;->frameTransform:[F

    invoke-static {v0, v1, v3}, Lfwm;->a([F[F[F)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    .line 73
    if-le v2, v4, :cond_0

    .line 74
    add-int/lit8 v3, v2, -0x1

    const/16 v5, 0x23

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Skipped encoding "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " frames"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfvh;->logd(Ljava/lang/String;)V

    .line 75
    :cond_0
    iget-boolean v3, p0, Lfsb;->isVideoMuted:Z

    if-nez v3, :cond_1

    iget v3, p0, Lfsb;->inputTextureName:I

    if-nez v3, :cond_5

    :cond_1
    move v7, v6

    move-wide v2, v0

    move v0, v6

    .line 82
    :goto_0
    if-eqz v0, :cond_2

    .line 83
    iput-wide v10, p0, Lfsb;->lastEncodeTimeMs:J

    .line 84
    iget-object v0, p0, Lfsb;->encoder:Lfoz;

    iget v1, p0, Lfsb;->inputTextureName:I

    iget-object v5, p0, Lfsb;->frameTransform:[F

    invoke-interface/range {v0 .. v5}, Lfoz;->encodeFrame(IJZ[F)Z

    move-result v0

    if-nez v0, :cond_2

    .line 85
    const-string v0, "Failed to encode frame."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 86
    :cond_2
    iget-wide v0, p0, Lfsb;->msPerFrame:J

    cmp-long v0, v0, v12

    if-eqz v0, :cond_3

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v10

    .line 88
    iget-object v2, p0, Lfsb;->glManager:Lfpc;

    const-wide/16 v8, 0x1

    iget-wide v10, p0, Lfsb;->msPerFrame:J

    sub-long v0, v10, v0

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-virtual {v2, p0, v0, v1}, Lfpc;->notifyFrameDelayed(Lfsm;J)V

    .line 89
    :cond_3
    if-lez v7, :cond_4

    move v6, v4

    :cond_4
    :goto_1
    return v6

    .line 69
    :catch_0
    move-exception v0

    .line 70
    const-string v1, "Failed to updateTexImage"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 78
    :cond_5
    iget-wide v8, p0, Lfsb;->msPerFrame:J

    cmp-long v3, v8, v12

    if-nez v3, :cond_6

    move v7, v2

    move-wide v2, v0

    move v0, v4

    .line 79
    goto :goto_0

    .line 80
    :cond_6
    iget-wide v0, p0, Lfsb;->lastEncodeTimeMs:J

    iget-wide v8, p0, Lfsb;->msPerFrame:J

    add-long/2addr v0, v8

    cmp-long v0, v10, v0

    if-ltz v0, :cond_7

    move v0, v4

    .line 81
    :goto_2
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v8

    move v7, v2

    move-wide v2, v8

    goto :goto_0

    :cond_7
    move v0, v6

    .line 80
    goto :goto_2
.end method

.method public final release()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lfsb;->call:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iget-object v1, p0, Lfsb;->callStateListener:Lfsl;

    invoke-virtual {v0, v1}, Lfnv;->removeCallStateListener(Lfof;)V

    .line 25
    iget-object v0, p0, Lfsb;->mediaSourceScreencastHelper:Lfrd;

    invoke-virtual {v0}, Lfrd;->release()V

    .line 26
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    new-instance v1, Lfse;

    invoke-direct {v1, p0}, Lfse;-><init>(Lfsb;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 27
    return-void
.end method

.method final resetToDefaultState()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lfsb;->setFrameRate(I)V

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfsb;->setMirrorLocalRendering(Z)V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfsb;->setCallback$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TPMASJMD5HMABQMD5I6ARQ9DPO7AT2JELP6COB3CKI46OBCDHH62ORB7CKLC___0(Lfmk;)V

    .line 34
    new-instance v0, Lfwe;

    invoke-direct {v0}, Lfwe;-><init>()V

    invoke-virtual {p0, v0}, Lfsb;->setCaptureFormat(Lfwe;)V

    .line 35
    return-void
.end method

.method public final setCallback$51666RRD5TJMURR7DHIIUOBECHP6UQB45TM6IOJIC5P6IPBJ5TK62RJ7DTQN8SPFEPKM8PBF5TPMASJMD5HMABQMD5I6ARQ9DPO7AT2JELP6COB3CKI46OBCDHH62ORB7CKLC___0(Lfmk;)V
    .locals 2

    .prologue
    .line 135
    iput-object p1, p0, Lfsb;->callback$9HHMUR9FCTNMUPRCCKNM2RJ4E9NMIP1FDHKM4SJ1E9KMASPFD1GMSPRFELQ76BRMD5I6ARPFEDIN4TJ9CDIIULJ9CHIMUIBEE1QN8KRLE9J62OR54H1M2R3CC9GM6QPR0:Lfmk;

    .line 136
    iget-object v0, p0, Lfsb;->captureTargetsChangedRunnable:Ljava/lang/Runnable;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhcw;->a(Ljava/lang/Runnable;Z)V

    .line 137
    return-void
.end method

.method public final setCaptureFormat(Lfwe;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 107
    invoke-virtual {p1, v0}, Lfwe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const-string v0, "Changing capture format from %s to %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    .line 109
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    .line 110
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 113
    iget-boolean v0, p1, Lfwe;->h:Z

    .line 115
    new-instance v1, Lfsh;

    invoke-direct {v1, p0, v0}, Lfsh;-><init>(Lfsb;Z)V

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 116
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    new-instance v1, Lfsi;

    invoke-direct {v1, p0}, Lfsi;-><init>(Lfsb;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 117
    invoke-virtual {p1}, Lfwe;->a()Lfwe;

    move-result-object v0

    .line 119
    iput v3, v0, Lfwe;->g:I

    .line 121
    iget v1, p1, Lfwe;->a:I

    .line 123
    iget v2, p1, Lfwe;->b:I

    .line 124
    invoke-virtual {v0, v1, v2}, Lfwe;->a(II)Lfwe;

    .line 125
    iget-object v1, p0, Lfsb;->videoFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 128
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lfsb;->captureTargetsChangedRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v4}, Lhcw;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0
.end method

.method public final setFrameRate(I)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 130
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lfsb;->msPerFrame:J

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    const/16 v0, 0x3e8

    div-int/2addr v0, p1

    int-to-long v0, v0

    iput-wide v0, p0, Lfsb;->msPerFrame:J

    goto :goto_0
.end method

.method public final setMirrorLocalRendering(Z)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    new-instance v1, Lfsj;

    invoke-direct {v1, p0, p1}, Lfsj;-><init>(Lfsb;Z)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 134
    return-void
.end method

.method public final setMuted(Z)V
    .locals 0

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lfsb;->setVideoMute(Z)V

    .line 95
    invoke-direct {p0}, Lfsb;->maybeUpdateVideoChatVideoMuteState()V

    .line 96
    return-void
.end method

.method public final unbind()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    new-instance v1, Lfsf;

    invoke-direct {v1, p0}, Lfsf;-><init>(Lfsb;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 29
    iget-object v0, p0, Lfsb;->glManager:Lfpc;

    invoke-virtual {v0, p0}, Lfpc;->removeVideoSource(Lfsm;)V

    .line 30
    return-void
.end method

.method final updateEncodeSize()V
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, Lfsb;->captureFormat:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwe;

    .line 145
    iget v1, v0, Lfwe;->a:I

    .line 146
    if-eqz v1, :cond_0

    .line 147
    iget v1, v0, Lfwe;->b:I

    .line 148
    if-nez v1, :cond_1

    .line 149
    :cond_0
    const-string v0, "Ignoring capture size area of 0"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 151
    :cond_1
    new-instance v1, Lfwo;

    .line 152
    iget v2, v0, Lfwe;->a:I

    .line 154
    iget v3, v0, Lfwe;->b:I

    .line 155
    invoke-direct {v1, v2, v3}, Lfwo;-><init>(II)V

    .line 156
    iget-object v2, p0, Lfsb;->inputSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 158
    iget v3, v0, Lfwe;->c:I

    .line 160
    iget v4, v0, Lfwe;->d:I

    .line 161
    invoke-virtual {v2, v3, v4}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 162
    iget-object v2, p0, Lfsb;->encoder:Lfoz;

    if-eqz v2, :cond_2

    .line 163
    iget-object v2, p0, Lfsb;->encoder:Lfoz;

    iget v3, v1, Lfwo;->a:I

    iget v1, v1, Lfwo;->b:I

    .line 164
    iget-boolean v4, v0, Lfwe;->h:Z

    .line 165
    invoke-interface {v2, v3, v1, v4}, Lfoz;->setResolution(IIZ)V

    .line 167
    :cond_2
    iget v0, v0, Lfwe;->g:I

    .line 168
    invoke-static {v0}, Lfwm;->a(I)[F

    move-result-object v0

    iput-object v0, p0, Lfsb;->rotationTransform:[F

    goto :goto_0
.end method
