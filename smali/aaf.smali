.class public final Laaf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laaj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c(Laai;)Ladb;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Laai;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Ladb;

    return-object v0
.end method


# virtual methods
.method public final a(Laai;)F
    .locals 2

    .prologue
    .line 38
    .line 39
    invoke-static {p1}, Laaf;->c(Laai;)Ladb;

    move-result-object v0

    .line 40
    iget v0, v0, Ladb;->a:F

    .line 41
    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public final a(Laai;Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2
    new-instance v0, Ladb;

    invoke-direct {v0, p3, p4}, Ladb;-><init>(Landroid/content/res/ColorStateList;F)V

    .line 3
    invoke-virtual {p1, v0}, Laai;->a(Landroid/graphics/drawable/Drawable;)V

    .line 4
    invoke-virtual {p1}, Laai;->d()Landroid/view/View;

    move-result-object v0

    .line 5
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 6
    invoke-virtual {v0, p5}, Landroid/view/View;->setElevation(F)V

    .line 8
    invoke-static {p1}, Laaf;->c(Laai;)Ladb;

    move-result-object v0

    .line 9
    invoke-virtual {p1}, Laai;->b()Z

    move-result v1

    invoke-virtual {p1}, Laai;->c()Z

    move-result v2

    .line 11
    iget v3, v0, Ladb;->b:F

    cmpl-float v3, p6, v3

    if-nez v3, :cond_0

    iget-boolean v3, v0, Ladb;->c:Z

    if-ne v3, v1, :cond_0

    iget-boolean v3, v0, Ladb;->d:Z

    if-eq v3, v2, :cond_1

    .line 12
    :cond_0
    iput p6, v0, Ladb;->b:F

    .line 13
    iput-boolean v1, v0, Ladb;->c:Z

    .line 14
    iput-boolean v2, v0, Ladb;->d:Z

    .line 15
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ladb;->a(Landroid/graphics/Rect;)V

    .line 16
    invoke-virtual {v0}, Ladb;->invalidateSelf()V

    .line 18
    :cond_1
    invoke-virtual {p1}, Laai;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 19
    invoke-virtual {p1, v4, v4, v4, v4}, Laai;->a(IIII)V

    .line 36
    :goto_0
    return-void

    .line 22
    :cond_2
    invoke-static {p1}, Laaf;->c(Laai;)Ladb;

    move-result-object v0

    .line 23
    iget v0, v0, Ladb;->b:F

    .line 26
    invoke-static {p1}, Laaf;->c(Laai;)Ladb;

    move-result-object v1

    .line 27
    iget v1, v1, Ladb;->a:F

    .line 30
    invoke-virtual {p1}, Laai;->c()Z

    move-result v2

    invoke-static {v0, v1, v2}, Ladc;->b(FFZ)F

    move-result v2

    float-to-double v2, v2

    .line 31
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 33
    invoke-virtual {p1}, Laai;->c()Z

    move-result v3

    invoke-static {v0, v1, v3}, Ladc;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 35
    invoke-virtual {p1, v2, v0, v2, v0}, Laai;->a(IIII)V

    goto :goto_0
.end method

.method public final b(Laai;)F
    .locals 2

    .prologue
    .line 42
    .line 43
    invoke-static {p1}, Laaf;->c(Laai;)Ladb;

    move-result-object v0

    .line 44
    iget v0, v0, Ladb;->a:F

    .line 45
    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method
