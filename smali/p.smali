.class public final Lp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:Ljava/lang/ThreadLocal;


# instance fields
.field public final a:Lpv;

.field public final b:Ljava/util/ArrayList;

.field public c:J

.field public d:Z

.field private f:Lbh;

.field private g:Lr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lp;->e:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lpv;

    invoke-direct {v0}, Lpv;-><init>()V

    iput-object v0, p0, Lp;->a:Lpv;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lp;->b:Ljava/util/ArrayList;

    .line 4
    new-instance v0, Lbh;

    invoke-direct {v0, p0}, Lbh;-><init>(Lp;)V

    iput-object v0, p0, Lp;->f:Lbh;

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lp;->c:J

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lp;->d:Z

    .line 7
    return-void
.end method

.method public static a()Lp;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lp;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 9
    sget-object v0, Lp;->e:Ljava/lang/ThreadLocal;

    new-instance v1, Lp;

    invoke-direct {v1}, Lp;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 10
    :cond_0
    sget-object v0, Lp;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp;

    return-object v0
.end method


# virtual methods
.method public final b()Lr;
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lp;->g:Lr;

    if-nez v0, :cond_0

    .line 12
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 13
    new-instance v0, Lu;

    iget-object v1, p0, Lp;->f:Lbh;

    invoke-direct {v0, v1}, Lu;-><init>(Lbh;)V

    iput-object v0, p0, Lp;->g:Lr;

    .line 15
    :cond_0
    :goto_0
    iget-object v0, p0, Lp;->g:Lr;

    return-object v0

    .line 14
    :cond_1
    new-instance v0, Ls;

    iget-object v1, p0, Lp;->f:Lbh;

    invoke-direct {v0, v1}, Ls;-><init>(Lbh;)V

    iput-object v0, p0, Lp;->g:Lr;

    goto :goto_0
.end method
