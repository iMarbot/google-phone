.class public final Ldtc;
.super Ljava/lang/Object;


# static fields
.field public static A:Ldtd;

.field public static B:Ldtd;

.field public static C:Ldtd;

.field public static D:Ldtd;

.field public static E:Ldtd;

.field public static a:Ldtd;

.field public static b:Ldtd;

.field public static c:Ldtd;

.field public static d:Ldtd;

.field public static e:Ldtd;

.field public static f:Ldtd;

.field public static g:Ldtd;

.field public static h:Ldtd;

.field public static i:Ldtd;

.field public static j:Ldtd;

.field public static k:Ldtd;

.field public static l:Ldtd;

.field public static m:Ldtd;

.field public static n:Ldtd;

.field public static o:Ldtd;

.field public static p:Ldtd;

.field public static q:Ldtd;

.field public static r:Ldtd;

.field public static s:Ldtd;

.field public static t:Ldtd;

.field public static u:Ldtd;

.field public static v:Ldtd;

.field public static w:Ldtd;

.field public static x:Ldtd;

.field public static y:Ldtd;

.field public static z:Ldtd;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/32 v12, 0x5265c00

    const/16 v10, 0x2000

    const/16 v9, 0x14

    const/4 v8, 0x0

    const-wide/16 v6, 0x1388

    .line 1
    const-string v0, "analytics.service_enabled"

    invoke-static {v0, v8, v8}, Ldtd;->a(Ljava/lang/String;ZZ)Ldtd;

    const-string v0, "analytics.service_client_enabled"

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;ZZ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->a:Ldtd;

    const-string v0, "analytics.log_tag"

    const-string v1, "GAv4"

    const-string v2, "GAv4-SVC"

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->b:Ldtd;

    const-string v0, "analytics.max_tokens"

    const-wide/16 v2, 0x3c

    const-wide/16 v4, 0x3c

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.tokens_per_sec"

    .line 2
    new-instance v1, Ldtd;

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v0, v2}, Lepp;->a(Ljava/lang/String;Ljava/lang/Float;)Lepp;

    move-result-object v0

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ldtd;-><init>(Lepp;Ljava/lang/Object;)V

    .line 3
    const-string v0, "analytics.max_stored_hits"

    const/16 v1, 0x7d0

    const/16 v2, 0x4e20

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->c:Ldtd;

    const-string v0, "analytics.max_stored_hits_per_app"

    const/16 v1, 0x7d0

    const/16 v2, 0x7d0

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    const-string v0, "analytics.max_stored_properties_per_app"

    const/16 v1, 0x64

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->d:Ldtd;

    const-string v0, "analytics.local_dispatch_millis"

    const-wide/32 v2, 0x1b7740

    const-wide/32 v4, 0x1d4c0

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->e:Ldtd;

    const-string v0, "analytics.initial_local_dispatch_millis"

    invoke-static {v0, v6, v7, v6, v7}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->f:Ldtd;

    const-string v0, "analytics.min_local_dispatch_millis"

    const-wide/32 v2, 0x1d4c0

    const-wide/32 v4, 0x1d4c0

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.max_local_dispatch_millis"

    const-wide/32 v2, 0x6ddd00

    const-wide/32 v4, 0x6ddd00

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.dispatch_alarm_millis"

    const-wide/32 v2, 0x6ddd00

    const-wide/32 v4, 0x6ddd00

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->g:Ldtd;

    const-string v0, "analytics.max_dispatch_alarm_millis"

    const-wide/32 v2, 0x1ee6280

    const-wide/32 v4, 0x1ee6280

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->h:Ldtd;

    const-string v0, "analytics.max_hits_per_dispatch"

    invoke-static {v0, v9, v9}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->i:Ldtd;

    const-string v0, "analytics.max_hits_per_batch"

    invoke-static {v0, v9, v9}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->j:Ldtd;

    const-string v0, "analytics.insecure_host"

    const-string v1, "http://www.google-analytics.com"

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->k:Ldtd;

    const-string v0, "analytics.secure_host"

    const-string v1, "https://ssl.google-analytics.com"

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->l:Ldtd;

    const-string v0, "analytics.simple_endpoint"

    const-string v1, "/collect"

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->m:Ldtd;

    const-string v0, "analytics.batching_endpoint"

    const-string v1, "/batch"

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->n:Ldtd;

    const-string v0, "analytics.max_get_length"

    const/16 v1, 0x7f4

    const/16 v2, 0x7f4

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->o:Ldtd;

    const-string v0, "analytics.batching_strategy.k"

    sget-object v1, Ldti;->b:Ldti;

    invoke-virtual {v1}, Ldti;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ldti;->b:Ldti;

    invoke-virtual {v2}, Ldti;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->p:Ldtd;

    const-string v0, "analytics.compression_strategy.k"

    sget-object v1, Ldto;->a:Ldto;

    invoke-virtual {v1}, Ldto;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->q:Ldtd;

    const-string v0, "analytics.max_hits_per_request.k"

    invoke-static {v0, v9, v9}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    const-string v0, "analytics.max_hit_length.k"

    invoke-static {v0, v10, v10}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->r:Ldtd;

    const-string v0, "analytics.max_post_length.k"

    invoke-static {v0, v10, v10}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->s:Ldtd;

    const-string v0, "analytics.max_batch_post_length"

    invoke-static {v0, v10, v10}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->t:Ldtd;

    const-string v0, "analytics.fallback_responses.k"

    const-string v1, "404,502"

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->u:Ldtd;

    const-string v0, "analytics.batch_retry_interval.seconds.k"

    const/16 v1, 0xe10

    const/16 v2, 0xe10

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->v:Ldtd;

    const-string v0, "analytics.service_monitor_interval"

    invoke-static {v0, v12, v13, v12, v13}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.http_connection.connect_timeout_millis"

    const v1, 0xea60

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->w:Ldtd;

    const-string v0, "analytics.http_connection.read_timeout_millis"

    const v1, 0xee48

    const v2, 0xee48

    invoke-static {v0, v1, v2}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->x:Ldtd;

    const-string v0, "analytics.campaigns.time_limit"

    invoke-static {v0, v12, v13, v12, v13}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->y:Ldtd;

    const-string v0, "analytics.first_party_experiment_id"

    const-string v1, ""

    invoke-static {v0, v1, v1}, Ldtd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldtd;

    const-string v0, "analytics.first_party_experiment_variant"

    invoke-static {v0, v8, v8}, Ldtd;->a(Ljava/lang/String;II)Ldtd;

    const-string v0, "analytics.test.disable_receiver"

    invoke-static {v0, v8, v8}, Ldtd;->a(Ljava/lang/String;ZZ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->z:Ldtd;

    const-string v0, "analytics.service_client.idle_disconnect_millis"

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x2710

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->A:Ldtd;

    const-string v0, "analytics.service_client.connect_timeout_millis"

    invoke-static {v0, v6, v7, v6, v7}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->B:Ldtd;

    const-string v0, "analytics.service_client.second_connect_delay_millis"

    invoke-static {v0, v6, v7, v6, v7}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.service_client.unexpected_reconnect_millis"

    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0xea60

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    const-string v0, "analytics.service_client.reconnect_throttle_millis"

    const-wide/32 v2, 0x1b7740

    const-wide/32 v4, 0x1b7740

    invoke-static {v0, v2, v3, v4, v5}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->C:Ldtd;

    const-string v0, "analytics.monitoring.sample_period_millis"

    invoke-static {v0, v12, v13, v12, v13}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->D:Ldtd;

    const-string v0, "analytics.initialization_warning_threshold"

    invoke-static {v0, v6, v7, v6, v7}, Ldtd;->a(Ljava/lang/String;JJ)Ldtd;

    move-result-object v0

    sput-object v0, Ldtc;->E:Ldtd;

    return-void
.end method
