.class public final Lgou;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgou;


# instance fields
.field public hangoutId:Ljava/lang/String;

.field public mediaType:Ljava/lang/Integer;

.field public muteRequest:Lgov;

.field public muteState:Lgow;

.field public participantId:Ljava/lang/String;

.field public playLevelRequest:Lgox;

.field public sourceId:Ljava/lang/String;

.field public videoDetails:Lgoy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgou;->clear()Lgou;

    .line 16
    return-void
.end method

.method public static checkDefaultSourceIdOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x2f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum DefaultSourceId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkDefaultSourceIdOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgou;->checkDefaultSourceIdOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgou;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgou;->_emptyArray:[Lgou;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgou;->_emptyArray:[Lgou;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgou;

    sput-object v0, Lgou;->_emptyArray:[Lgou;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgou;->_emptyArray:[Lgou;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgou;
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lgou;

    invoke-direct {v0}, Lgou;-><init>()V

    invoke-virtual {v0, p0}, Lgou;->mergeFrom(Lhfp;)Lgou;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgou;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lgou;

    invoke-direct {v0}, Lgou;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgou;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgou;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lgou;->hangoutId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lgou;->participantId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lgou;->sourceId:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lgou;->mediaType:Ljava/lang/Integer;

    .line 21
    iput-object v0, p0, Lgou;->muteState:Lgow;

    .line 22
    iput-object v0, p0, Lgou;->muteRequest:Lgov;

    .line 23
    iput-object v0, p0, Lgou;->videoDetails:Lgoy;

    .line 24
    iput-object v0, p0, Lgou;->playLevelRequest:Lgox;

    .line 25
    iput-object v0, p0, Lgou;->unknownFieldData:Lhfv;

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lgou;->cachedSize:I

    .line 27
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 47
    iget-object v1, p0, Lgou;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 48
    const/4 v1, 0x1

    iget-object v2, p0, Lgou;->hangoutId:Ljava/lang/String;

    .line 49
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_0
    iget-object v1, p0, Lgou;->participantId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 51
    const/4 v1, 0x2

    iget-object v2, p0, Lgou;->participantId:Ljava/lang/String;

    .line 52
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    :cond_1
    iget-object v1, p0, Lgou;->sourceId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 54
    const/4 v1, 0x3

    iget-object v2, p0, Lgou;->sourceId:Ljava/lang/String;

    .line 55
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_2
    iget-object v1, p0, Lgou;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 57
    const/4 v1, 0x4

    iget-object v2, p0, Lgou;->mediaType:Ljava/lang/Integer;

    .line 58
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    :cond_3
    iget-object v1, p0, Lgou;->muteState:Lgow;

    if-eqz v1, :cond_4

    .line 60
    const/4 v1, 0x5

    iget-object v2, p0, Lgou;->muteState:Lgow;

    .line 61
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_4
    iget-object v1, p0, Lgou;->muteRequest:Lgov;

    if-eqz v1, :cond_5

    .line 63
    const/4 v1, 0x6

    iget-object v2, p0, Lgou;->muteRequest:Lgov;

    .line 64
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_5
    iget-object v1, p0, Lgou;->videoDetails:Lgoy;

    if-eqz v1, :cond_6

    .line 66
    const/4 v1, 0x7

    iget-object v2, p0, Lgou;->videoDetails:Lgoy;

    .line 67
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_6
    iget-object v1, p0, Lgou;->playLevelRequest:Lgox;

    if-eqz v1, :cond_7

    .line 69
    const/16 v1, 0x8

    iget-object v2, p0, Lgou;->playLevelRequest:Lgox;

    .line 70
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_7
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgou;
    .locals 3

    .prologue
    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 73
    sparse-switch v0, :sswitch_data_0

    .line 75
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    :sswitch_0
    return-object p0

    .line 77
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgou;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 79
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgou;->participantId:Ljava/lang/String;

    goto :goto_0

    .line 81
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgou;->sourceId:Ljava/lang/String;

    goto :goto_0

    .line 83
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 85
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 86
    invoke-static {v2}, Lgoi;->checkMediaTypeOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgou;->mediaType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 90
    invoke-virtual {p0, p1, v0}, Lgou;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 92
    :sswitch_5
    iget-object v0, p0, Lgou;->muteState:Lgow;

    if-nez v0, :cond_1

    .line 93
    new-instance v0, Lgow;

    invoke-direct {v0}, Lgow;-><init>()V

    iput-object v0, p0, Lgou;->muteState:Lgow;

    .line 94
    :cond_1
    iget-object v0, p0, Lgou;->muteState:Lgow;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 96
    :sswitch_6
    iget-object v0, p0, Lgou;->muteRequest:Lgov;

    if-nez v0, :cond_2

    .line 97
    new-instance v0, Lgov;

    invoke-direct {v0}, Lgov;-><init>()V

    iput-object v0, p0, Lgou;->muteRequest:Lgov;

    .line 98
    :cond_2
    iget-object v0, p0, Lgou;->muteRequest:Lgov;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 100
    :sswitch_7
    iget-object v0, p0, Lgou;->videoDetails:Lgoy;

    if-nez v0, :cond_3

    .line 101
    new-instance v0, Lgoy;

    invoke-direct {v0}, Lgoy;-><init>()V

    iput-object v0, p0, Lgou;->videoDetails:Lgoy;

    .line 102
    :cond_3
    iget-object v0, p0, Lgou;->videoDetails:Lgoy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 104
    :sswitch_8
    iget-object v0, p0, Lgou;->playLevelRequest:Lgox;

    if-nez v0, :cond_4

    .line 105
    new-instance v0, Lgox;

    invoke-direct {v0}, Lgox;-><init>()V

    iput-object v0, p0, Lgou;->playLevelRequest:Lgox;

    .line 106
    :cond_4
    iget-object v0, p0, Lgou;->playLevelRequest:Lgox;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lgou;->mergeFrom(Lhfp;)Lgou;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lgou;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lgou;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lgou;->participantId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v1, p0, Lgou;->participantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_1
    iget-object v0, p0, Lgou;->sourceId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 33
    const/4 v0, 0x3

    iget-object v1, p0, Lgou;->sourceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 34
    :cond_2
    iget-object v0, p0, Lgou;->mediaType:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 35
    const/4 v0, 0x4

    iget-object v1, p0, Lgou;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_3
    iget-object v0, p0, Lgou;->muteState:Lgow;

    if-eqz v0, :cond_4

    .line 37
    const/4 v0, 0x5

    iget-object v1, p0, Lgou;->muteState:Lgow;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 38
    :cond_4
    iget-object v0, p0, Lgou;->muteRequest:Lgov;

    if-eqz v0, :cond_5

    .line 39
    const/4 v0, 0x6

    iget-object v1, p0, Lgou;->muteRequest:Lgov;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_5
    iget-object v0, p0, Lgou;->videoDetails:Lgoy;

    if-eqz v0, :cond_6

    .line 41
    const/4 v0, 0x7

    iget-object v1, p0, Lgou;->videoDetails:Lgoy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_6
    iget-object v0, p0, Lgou;->playLevelRequest:Lgox;

    if-eqz v0, :cond_7

    .line 43
    const/16 v0, 0x8

    iget-object v1, p0, Lgou;->playLevelRequest:Lgox;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 45
    return-void
.end method
