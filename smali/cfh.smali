.class public abstract Lcfh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcff;
.implements Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;


# instance fields
.field public final a:Lcgk;

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;


# direct methods
.method protected constructor <init>(Lcgk;III)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-object p1, p0, Lcfh;->a:Lcgk;

    .line 4
    iput p2, p0, Lcfh;->b:I

    .line 5
    iput p3, p0, Lcfh;->c:I

    .line 6
    iput p4, p0, Lcfh;->d:I

    .line 7
    return-void
.end method


# virtual methods
.method public a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 23
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-static {v0}, Lbvs;->b(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    .line 24
    iput-object p1, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    .line 25
    if-eqz p1, :cond_0

    .line 26
    iget-boolean v0, p0, Lcfh;->e:Z

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 27
    iget-boolean v0, p0, Lcfh;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 28
    iget-boolean v0, p0, Lcfh;->g:Z

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setChecked(Z)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    iput-object p0, p1, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    .line 33
    invoke-virtual {p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v0, p0, Lcfh;->g:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcfh;->c:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 34
    invoke-virtual {p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 35
    invoke-virtual {p1, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a(Z)V

    .line 36
    :cond_0
    return-void

    .line 27
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 33
    :cond_2
    iget v0, p0, Lcfh;->d:I

    goto :goto_1
.end method

.method public final a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;Z)V
    .locals 3

    .prologue
    .line 37
    iget-object v1, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    .line 38
    invoke-virtual {v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p2, :cond_0

    iget v0, p0, Lcfh;->c:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {p0, p2}, Lcfh;->d(Z)V

    .line 41
    return-void

    .line 38
    :cond_0
    iget v0, p0, Lcfh;->d:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 9
    iput-boolean p1, p0, Lcfh;->e:Z

    .line 10
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 12
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lcfh;->e:Z

    return v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 14
    iput-boolean p1, p0, Lcfh;->f:Z

    .line 15
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 16
    iget-object v1, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 17
    :cond_0
    return-void

    .line 16
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcfh;->f:Z

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcfh;->b:I

    return v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 18
    iput-boolean p1, p0, Lcfh;->g:Z

    .line 19
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcfh;->h:Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    invoke-virtual {v0, p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setChecked(Z)V

    .line 21
    :cond_0
    return-void
.end method

.method protected abstract d(Z)V
.end method
