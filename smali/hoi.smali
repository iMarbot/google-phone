.class public final Lhoi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static volatile a:Lhbg;

.field public static final b:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    sput-object v0, Lhoi;->a:Lhbg;

    .line 14
    new-instance v0, Lhoj;

    invoke-direct {v0}, Lhoj;-><init>()V

    sput-object v0, Lhoi;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 3
    invoke-static {p0}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    invoke-static {p1}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    const/16 v0, 0x2000

    new-array v2, v0, [B

    .line 6
    const-wide/16 v0, 0x0

    .line 7
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 8
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 9
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 10
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    return-wide v0
.end method

.method public static a(Lhdd;)Lhlk$b;
    .locals 2

    .prologue
    .line 1
    invoke-interface {p0}, Lhdd;->getParserForType()Lhdm;

    move-result-object v0

    .line 2
    new-instance v1, Lhlk$d;

    invoke-direct {v1, p0, v0}, Lhlk$d;-><init>(Lhdd;Lhdm;)V

    return-object v1
.end method
