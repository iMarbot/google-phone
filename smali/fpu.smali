.class final Lfpu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;
.implements Ljava/lang/Runnable;


# instance fields
.field public authToken:Ljava/lang/String;

.field public final credentials:Lfmy;

.field public final logData:Lgpn;

.field public mesiClient:Lfnj;

.field public final rtcClient:Lhgi;

.field public final synthetic this$0:Lfpt;


# direct methods
.method constructor <init>(Lfpt;Lfmy;Lgpn;Lhgi;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfpu;->this$0:Lfpt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lfpu;->credentials:Lfmy;

    .line 3
    iput-object p3, p0, Lfpu;->logData:Lgpn;

    .line 4
    iput-object p4, p0, Lfpu;->rtcClient:Lhgi;

    .line 5
    return-void
.end method

.method private final onTaskDone()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lfpu;->credentials:Lfmy;

    iget-object v1, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v1}, Lfpt;->access$100(Lfpt;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfpu;->authToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lfmy;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lfpu;->mesiClient:Lfnj;

    invoke-interface {v0}, Lfnj;->release()V

    .line 39
    return-void
.end method


# virtual methods
.method public final onError(Lgoi$a;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v0}, Lfpt;->access$000(Lfpt;)Lfuv;

    move-result-object v0

    const/16 v1, 0xdb7

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 34
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "LogData upload failed!\n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lfpu;->onTaskDone()V

    .line 36
    return-void
.end method

.method public final bridge synthetic onError(Lhfz;)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lgoi$a;

    invoke-virtual {p0, p1}, Lfpu;->onError(Lgoi$a;)V

    return-void
.end method

.method public final onSuccess(Lgoi$a;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v0}, Lfpt;->access$000(Lfpt;)Lfuv;

    move-result-object v0

    const/16 v1, 0xdb8

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 30
    const-string v0, "LogData upload succeeded."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lfpu;->onTaskDone()V

    .line 32
    return-void
.end method

.method public final bridge synthetic onSuccess(Lhfz;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lgoi$a;

    invoke-virtual {p0, p1}, Lfpu;->onSuccess(Lgoi$a;)V

    return-void
.end method

.method public final run()V
    .locals 9

    .prologue
    .line 6
    iget-object v0, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v0}, Lfpt;->access$000(Lfpt;)Lfuv;

    move-result-object v0

    const/16 v1, 0xdb5

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 7
    const-string v0, "Beginning LogData upload."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 8
    :try_start_0
    iget-object v0, p0, Lfpu;->credentials:Lfmy;

    iget-object v1, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v1}, Lfpt;->access$100(Lfpt;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "oauth2:https://www.googleapis.com/auth/hangouts "

    invoke-interface {v0, v1, v2}, Lfmy;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpu;->authToken:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ldwe; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    new-instance v0, Lgkh;

    invoke-direct {v0}, Lgkh;-><init>()V

    .line 15
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgkh;->a:Ljava/lang/Integer;

    .line 16
    iget-object v1, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v1}, Lfpt;->access$100(Lfpt;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lfvs;->b(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgkh;->d:Ljava/lang/Long;

    .line 17
    new-instance v1, Lgke;

    invoke-direct {v1}, Lgke;-><init>()V

    .line 18
    iget-object v2, p0, Lfpu;->this$0:Lfpt;

    iget-object v3, p0, Lfpu;->authToken:Ljava/lang/String;

    iget-object v4, p0, Lfpu;->rtcClient:Lhgi;

    invoke-virtual {v2, v3, v0, v1, v4}, Lfpt;->createMesiClient(Ljava/lang/String;Lgkh;Lgke;Lhgi;)Lfnj;

    move-result-object v0

    iput-object v0, p0, Lfpu;->mesiClient:Lfnj;

    .line 19
    new-instance v2, Lgot;

    invoke-direct {v2}, Lgot;-><init>()V

    .line 20
    iget-object v0, p0, Lfpu;->logData:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lfpu;->logData:Lgpn;

    iget-object v0, v0, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v0, v0, Lgit;->a:Ljava/lang/String;

    iput-object v0, v2, Lgot;->sessionId:Ljava/lang/String;

    .line 22
    :cond_0
    iget-object v0, p0, Lfpu;->logData:Lgpn;

    iput-object v0, v2, Lgot;->logData:Lgpn;

    .line 23
    iget-object v0, v2, Lgot;->logData:Lgpn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 24
    iget-object v0, p0, Lfpu;->mesiClient:Lfnj;

    const-string v1, "media_sessions/log"

    const-class v3, Lgoi$a;

    .line 25
    invoke-static {}, Lfpt;->access$200()I

    move-result v5

    .line 26
    invoke-static {}, Lfpt;->access$300()J

    move-result-wide v6

    const/4 v8, 0x5

    move-object v4, p0

    .line 27
    invoke-interface/range {v0 .. v8}, Lfnj;->executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V

    .line 28
    :goto_0
    return-void

    .line 10
    :catch_0
    move-exception v0

    .line 11
    :goto_1
    iget-object v1, p0, Lfpu;->this$0:Lfpt;

    invoke-static {v1}, Lfpt;->access$000(Lfpt;)Lfuv;

    move-result-object v1

    const/16 v2, 0xdb6

    invoke-virtual {v1, v2}, Lfuv;->report(I)V

    .line 12
    const-string v1, "LogData upload failed to get credentials for user"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 10
    :catch_1
    move-exception v0

    goto :goto_1
.end method
