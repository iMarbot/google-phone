.class public abstract Lhah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final a:Lhah;

.field private static c:Lhak;


# instance fields
.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Lhao;

    sget-object v1, Lhbu;->b:[B

    invoke-direct {v0, v1}, Lhao;-><init>([B)V

    sput-object v0, Lhah;->a:Lhah;

    .line 91
    const/4 v0, 0x1

    .line 92
    :try_start_0
    const-string v1, "android.content.Context"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Lhap;

    .line 97
    invoke-direct {v0}, Lhap;-><init>()V

    .line 100
    :goto_1
    sput-object v0, Lhah;->c:Lhak;

    .line 101
    return-void

    .line 95
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_0
    new-instance v0, Lhai;

    .line 99
    invoke-direct {v0}, Lhai;-><init>()V

    goto :goto_1
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lhah;->b:I

    .line 3
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lhah;
    .locals 3

    .prologue
    .line 8
    .line 9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    const/16 v0, 0x100

    .line 11
    :goto_0
    invoke-static {p0, v0}, Lhah;->a(Ljava/io/InputStream;I)Lhah;

    move-result-object v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 14
    shl-int/lit8 v0, v0, 0x1

    const/16 v2, 0x2000

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 16
    :cond_0
    invoke-static {v1}, Lhah;->a(Ljava/lang/Iterable;)Lhah;

    move-result-object v0

    .line 17
    return-object v0
.end method

.method private static a(Ljava/io/InputStream;I)Lhah;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 18
    new-array v2, p1, [B

    move v0, v1

    .line 20
    :goto_0
    if-ge v0, p1, :cond_0

    .line 21
    sub-int v3, p1, v0

    invoke-virtual {p0, v2, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 22
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 23
    add-int/2addr v0, v3

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    if-nez v0, :cond_1

    .line 26
    const/4 v0, 0x0

    .line 27
    :goto_1
    return-object v0

    :cond_1
    invoke-static {v2, v1, v0}, Lhah;->a([BII)Lhah;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Iterable;)Lhah;
    .locals 2

    .prologue
    .line 28
    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    sget-object v0, Lhah;->a:Lhah;

    .line 31
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1, v0}, Lhah;->a(Ljava/util/Iterator;I)Lhah;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lhah;
    .locals 2

    .prologue
    .line 7
    new-instance v0, Lhao;

    sget-object v1, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lhao;-><init>([B)V

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;I)Lhah;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 32
    if-gtz p1, :cond_0

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "length (%s) must be >= 1"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    if-ne p1, v2, :cond_1

    .line 35
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhah;

    .line 45
    :goto_0
    return-object v0

    .line 36
    :cond_1
    ushr-int/lit8 v0, p1, 0x1

    .line 37
    invoke-static {p0, v0}, Lhah;->a(Ljava/util/Iterator;I)Lhah;

    move-result-object v1

    .line 38
    sub-int v0, p1, v0

    invoke-static {p0, v0}, Lhah;->a(Ljava/util/Iterator;I)Lhah;

    move-result-object v0

    .line 40
    const v2, 0x7fffffff

    invoke-virtual {v1}, Lhah;->a()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Lhah;->a()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 41
    new-instance v2, Ljava/lang/IllegalArgumentException;

    .line 42
    invoke-virtual {v1}, Lhah;->a()I

    move-result v1

    invoke-virtual {v0}, Lhah;->a()I

    move-result v0

    const/16 v3, 0x35

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "ByteString would be too long: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 43
    :cond_2
    invoke-static {v1, v0}, Lhdv;->a(Lhah;Lhah;)Lhah;

    move-result-object v0

    goto :goto_0
.end method

.method static a([B)Lhah;
    .locals 1

    .prologue
    .line 5
    new-instance v0, Lhao;

    invoke-direct {v0, p0}, Lhao;-><init>([B)V

    return-object v0
.end method

.method public static a([BII)Lhah;
    .locals 2

    .prologue
    .line 4
    new-instance v0, Lhao;

    sget-object v1, Lhah;->c:Lhak;

    invoke-interface {v1, p0, p1, p2}, Lhak;->a([BII)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lhao;-><init>([B)V

    return-object v0
.end method

.method static b([BII)Lhah;
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lhaj;

    invoke-direct {v0, p0, p1, p2}, Lhaj;-><init>([BII)V

    return-object v0
.end method

.method static b(I)Lham;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lham;

    .line 69
    invoke-direct {v0, p0}, Lham;-><init>(I)V

    .line 70
    return-object v0
.end method

.method static b(II)V
    .locals 3

    .prologue
    .line 71
    add-int/lit8 v0, p0, 0x1

    sub-int v0, p1, v0

    or-int/2addr v0, p0

    if-gez v0, :cond_1

    .line 72
    if-gez p0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/16 v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Index < 0: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Index > length: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_1
    return-void
.end method

.method static c(III)I
    .locals 3

    .prologue
    .line 76
    sub-int v0, p1, p0

    .line 77
    or-int v1, p0, p1

    or-int/2addr v1, v0

    sub-int v2, p2, p1

    or-int/2addr v1, v2

    if-gez v1, :cond_2

    .line 78
    if-gez p0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const/16 v1, 0x20

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Beginning index: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    if-ge p1, p0, :cond_1

    .line 81
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const/16 v1, 0x42

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Beginning index larger than ending index: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const/16 v1, 0x25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "End index: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_2
    return v0
.end method


# virtual methods
.method public abstract a(I)B
.end method

.method public abstract a()I
.end method

.method protected abstract a(III)I
.end method

.method public abstract a(II)Lhah;
.end method

.method protected abstract a(Ljava/nio/charset/Charset;)Ljava/lang/String;
.end method

.method abstract a(Lhag;)V
.end method

.method public final a([BIII)V
    .locals 2

    .prologue
    .line 46
    add-int v0, p2, p4

    invoke-virtual {p0}, Lhah;->a()I

    move-result v1

    invoke-static {p2, v0, v1}, Lhah;->c(III)I

    .line 47
    add-int v0, p3, p4

    array-length v1, p1

    invoke-static {p3, v0, v1}, Lhah;->c(III)I

    .line 48
    if-lez p4, :cond_0

    .line 49
    invoke-virtual {p0, p1, p2, p3, p4}, Lhah;->b([BIII)V

    .line 50
    :cond_0
    return-void
.end method

.method protected abstract b(III)I
.end method

.method protected abstract b([BIII)V
.end method

.method public final b()[B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-virtual {p0}, Lhah;->a()I

    move-result v1

    .line 52
    if-nez v1, :cond_0

    .line 53
    sget-object v0, Lhbu;->b:[B

    .line 56
    :goto_0
    return-object v0

    .line 54
    :cond_0
    new-array v0, v1, [B

    .line 55
    invoke-virtual {p0, v0, v2, v2, v1}, Lhah;->b([BIII)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lhbu;->a:Ljava/nio/charset/Charset;

    .line 58
    invoke-virtual {p0}, Lhah;->a()I

    move-result v1

    if-nez v1, :cond_0

    const-string v0, ""

    .line 59
    :goto_0
    return-object v0

    .line 58
    :cond_0
    invoke-virtual {p0, v0}, Lhah;->a(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lhaq;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.end method

.method protected abstract f()I
.end method

.method protected abstract g()Z
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 60
    iget v0, p0, Lhah;->b:I

    .line 61
    if-nez v0, :cond_1

    .line 62
    invoke-virtual {p0}, Lhah;->a()I

    move-result v0

    .line 63
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, Lhah;->b(III)I

    move-result v0

    .line 64
    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 66
    :cond_0
    iput v0, p0, Lhah;->b:I

    .line 67
    :cond_1
    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 87
    .line 88
    new-instance v0, Lhal;

    invoke-direct {v0, p0}, Lhal;-><init>(Lhah;)V

    .line 89
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 84
    const-string v0, "<ByteString@%s size=%d>"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 85
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lhah;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 86
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
