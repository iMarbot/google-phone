.class public Lip;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Lk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lip$a;,
        Lip$c;,
        Lip$b;,
        Lip$d;
    }
.end annotation


# static fields
.field private static a:Lpv;

.field public static final b:Ljava/lang/Object;


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:Landroid/view/ViewGroup;

.field public I:Landroid/view/View;

.field public J:Landroid/view/View;

.field public K:Z

.field public L:Z

.field public M:Lkz;

.field public N:Z

.field public O:Z

.field public P:Lip$a;

.field public Q:Z

.field public R:Z

.field public S:F

.field public T:Landroid/view/LayoutInflater;

.field public U:Z

.field public V:Ll;

.field public c:I

.field public d:Landroid/os/Bundle;

.field public e:Landroid/util/SparseArray;

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Landroid/os/Bundle;

.field public i:Lip;

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:I

.field public s:Ljc;

.field public t:Liz;

.field public u:Ljc;

.field public v:Ljr;

.field public w:Lip;

.field public x:I

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 505
    new-instance v0, Lpv;

    invoke-direct {v0}, Lpv;-><init>()V

    sput-object v0, Lip;->a:Lpv;

    .line 506
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lip;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lip;->c:I

    .line 3
    iput v1, p0, Lip;->f:I

    .line 4
    iput v1, p0, Lip;->j:I

    .line 5
    iput-boolean v2, p0, Lip;->F:Z

    .line 6
    iput-boolean v2, p0, Lip;->L:Z

    .line 7
    new-instance v0, Ll;

    invoke-direct {v0, p0}, Ll;-><init>(Lk;)V

    iput-object v0, p0, Lip;->V:Ll;

    .line 8
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lip;
    .locals 4

    .prologue
    .line 9
    :try_start_0
    sget-object v0, Lip;->a:Lpv;

    invoke-virtual {v0, p1}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 10
    if-nez v0, :cond_0

    .line 11
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 12
    sget-object v1, Lip;->a:Lpv;

    invoke-virtual {v1, p1, v0}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 14
    if-eqz p2, :cond_1

    .line 15
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 16
    invoke-virtual {v0, p2}, Lip;->f(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 17
    :cond_1
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Lip$b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lip$b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 20
    :catch_1
    move-exception v0

    .line 21
    new-instance v1, Lip$b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lip$b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 22
    :catch_2
    move-exception v0

    .line 23
    new-instance v1, Lip$b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lip$b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 24
    :catch_3
    move-exception v0

    .line 25
    new-instance v1, Lip$b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": could not find Fragment constructor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lip$b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 26
    :catch_4
    move-exception v0

    .line 27
    new-instance v1, Lip$b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": calling Fragment constructor caused an exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lip$b;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 338
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment has not been attached yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    new-instance v0, Ljc;

    invoke-direct {v0}, Ljc;-><init>()V

    iput-object v0, p0, Lip;->u:Ljc;

    .line 341
    iget-object v0, p0, Lip;->u:Ljc;

    iget-object v1, p0, Lip;->t:Liz;

    new-instance v2, Lir;

    invoke-direct {v2, p0}, Lir;-><init>(Lip;)V

    invoke-virtual {v0, v1, v2, p0}, Ljc;->a(Liz;Lix;Lip;)V

    .line 342
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 226
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 28
    :try_start_0
    sget-object v0, Lip;->a:Lpv;

    invoke-virtual {v0, p1}, Lpv;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 29
    if-nez v0, :cond_0

    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 31
    sget-object v1, Lip;->a:Lpv;

    invoke-virtual {v1, p1, v0}, Lpv;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    const-class v1, Lip;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 34
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public static o()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public static p()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public static q()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method public static u()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 259
    const/4 v0, 0x0

    .line 266
    :goto_0
    return-object v0

    .line 260
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 261
    iget-object v0, v0, Lip$a;->i:Ljava/lang/Object;

    .line 262
    sget-object v1, Lip;->b:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 263
    invoke-virtual {p0}, Lip;->z()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 265
    iget-object v0, v0, Lip$a;->i:Ljava/lang/Object;

    goto :goto_0
.end method

.method final B()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 267
    iget-object v1, p0, Lip;->P:Lip$a;

    if-nez v1, :cond_1

    .line 272
    :goto_0
    if-eqz v0, :cond_0

    .line 273
    invoke-interface {v0}, Lip$c;->a()V

    .line 274
    :cond_0
    return-void

    .line 269
    :cond_1
    iget-object v1, p0, Lip;->P:Lip$a;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lip$a;->l:Z

    .line 270
    iget-object v1, p0, Lip;->P:Lip$a;

    iget-object v1, v1, Lip$a;->m:Lip$c;

    .line 271
    iget-object v2, p0, Lip;->P:Lip$a;

    iput-object v0, v2, Lip$a;->m:Lip$c;

    move-object v0, v1

    goto :goto_0
.end method

.method final C()V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 369
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->i()Z

    .line 370
    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lip;->c:I

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 372
    invoke-virtual {p0}, Lip;->o_()V

    .line 373
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 374
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_1
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->n()V

    .line 377
    :cond_2
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_3

    .line 378
    iget-object v0, p0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->f()V

    .line 379
    :cond_3
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->b:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 380
    return-void
.end method

.method final D()V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 383
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->i()Z

    .line 384
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lip;->c:I

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 386
    invoke-virtual {p0}, Lip;->r()V

    .line 387
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 388
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 389
    :cond_1
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_2

    .line 390
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->o()V

    .line 391
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->i()Z

    .line 392
    :cond_2
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->c:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 393
    return-void
.end method

.method final E()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 400
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->d:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 401
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lip;->u:Ljc;

    .line 403
    invoke-virtual {v0, v2}, Ljc;->b(I)V

    .line 404
    :cond_0
    iput v2, p0, Lip;->c:I

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 406
    invoke-virtual {p0}, Lip;->s()V

    .line 407
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 408
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :cond_1
    return-void
.end method

.method final F()V
    .locals 3

    .prologue
    .line 410
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->e:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 411
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->p()V

    .line 413
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lip;->c:I

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 415
    invoke-virtual {p0}, Lip;->l_()V

    .line 416
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 417
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 418
    :cond_1
    return-void
.end method

.method final G()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 419
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lip;->u:Ljc;

    .line 421
    invoke-virtual {v0, v1}, Ljc;->b(I)V

    .line 422
    :cond_0
    iput v1, p0, Lip;->c:I

    .line 423
    iget-boolean v0, p0, Lip;->N:Z

    if-eqz v0, :cond_2

    .line 424
    iput-boolean v3, p0, Lip;->N:Z

    .line 425
    iget-boolean v0, p0, Lip;->O:Z

    if-nez v0, :cond_1

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->O:Z

    .line 427
    iget-object v0, p0, Lip;->t:Liz;

    iget-object v1, p0, Lip;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lip;->N:Z

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v0

    iput-object v0, p0, Lip;->M:Lkz;

    .line 428
    :cond_1
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lip;->t:Liz;

    .line 430
    iget-boolean v0, v0, Liz;->f:Z

    .line 431
    if-eqz v0, :cond_3

    .line 432
    iget-object v0, p0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->d()V

    .line 434
    :cond_2
    :goto_0
    return-void

    .line 433
    :cond_3
    iget-object v0, p0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->c()V

    goto :goto_0
.end method

.method final H()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 435
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->f:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 436
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->q()V

    .line 438
    :cond_0
    iput v2, p0, Lip;->c:I

    .line 439
    iput-boolean v2, p0, Lip;->G:Z

    .line 440
    iput-boolean v2, p0, Lip;->U:Z

    .line 441
    invoke-virtual {p0}, Lip;->t()V

    .line 442
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 443
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lip;->u:Ljc;

    .line 445
    return-void
.end method

.method final I()Lip$a;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 455
    new-instance v0, Lip$a;

    invoke-direct {v0}, Lip$a;-><init>()V

    iput-object v0, p0, Lip;->P:Lip$a;

    .line 456
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    return-object v0
.end method

.method final J()I
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 458
    const/4 v0, 0x0

    .line 459
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget v0, v0, Lip$a;->d:I

    goto :goto_0
.end method

.method final K()I
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 466
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget v0, v0, Lip$a;->e:I

    goto :goto_0
.end method

.method final L()I
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 474
    const/4 v0, 0x0

    .line 475
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget v0, v0, Lip$a;->f:I

    goto :goto_0
.end method

.method final M()Llo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 476
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 478
    :cond_0
    return-object v1
.end method

.method final N()Llo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 479
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 481
    :cond_0
    return-object v1
.end method

.method final O()Landroid/view/View;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 483
    const/4 v0, 0x0

    .line 484
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget-object v0, v0, Lip$a;->a:Landroid/view/View;

    goto :goto_0
.end method

.method final P()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 490
    const/4 v0, 0x0

    .line 491
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget-object v0, v0, Lip$a;->b:Landroid/animation/Animator;

    goto :goto_0
.end method

.method final Q()I
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x0

    .line 494
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget v0, v0, Lip$a;->c:I

    goto :goto_0
.end method

.method final R()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 497
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 499
    :cond_0
    return v1
.end method

.method final S()Z
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 501
    const/4 v0, 0x0

    .line 502
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    iget-boolean v0, v0, Lip$a;->n:Z

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(II)V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 472
    :goto_0
    return-void

    .line 469
    :cond_0
    invoke-virtual {p0}, Lip;->I()Lip$a;

    .line 470
    iget-object v0, p0, Lip;->P:Lip$a;

    iput p1, v0, Lip$a;->e:I

    .line 471
    iget-object v0, p0, Lip;->P:Lip$a;

    iput p2, v0, Lip$a;->f:I

    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method final a(ILip;)V
    .locals 2

    .prologue
    .line 35
    iput p1, p0, Lip;->f:I

    .line 36
    if-eqz p2, :cond_0

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lip;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lip;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lip;->g:Ljava/lang/String;

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lip;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lip;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(I[Ljava/lang/String;[I)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method final a(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 487
    invoke-virtual {p0}, Lip;->I()Lip$a;

    move-result-object v0

    iput-object p1, v0, Lip$a;->b:Landroid/animation/Animator;

    .line 488
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 166
    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 155
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 158
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 161
    :goto_0
    if-eqz v0, :cond_0

    .line 162
    const/4 v1, 0x0

    iput-boolean v1, p0, Lip;->G:Z

    .line 163
    invoke-virtual {p0, v0}, Lip;->a(Landroid/app/Activity;)V

    .line 164
    :cond_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lip;->t:Liz;

    .line 159
    iget-object v0, v0, Liz;->a:Landroid/app/Activity;

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 114
    .line 115
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, p1, v1, v2}, Liz;->a(Lip;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 118
    return-void
.end method

.method public final a(Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 147
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 150
    :goto_0
    if-eqz v0, :cond_0

    .line 151
    const/4 v1, 0x0

    iput-boolean v1, p0, Lip;->G:Z

    .line 152
    invoke-virtual {p0, v0, p1, p2}, Lip;->a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 153
    :cond_0
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lip;->t:Liz;

    .line 148
    iget-object v0, v0, Liz;->a:Landroid/app/Activity;

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method final a(Lip$c;)V
    .locals 3

    .prologue
    .line 446
    invoke-virtual {p0}, Lip;->I()Lip$a;

    .line 447
    iget-object v0, p0, Lip;->P:Lip$a;

    iget-object v0, v0, Lip$a;->m:Lip$c;

    if-ne p1, v0, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lip;->P:Lip$a;

    iget-object v0, v0, Lip$a;->m:Lip$c;

    if-eqz v0, :cond_2

    .line 450
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to set a replacement startPostponedEnterTransition on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :cond_2
    if-eqz p1, :cond_0

    .line 452
    invoke-interface {p1}, Lip$c;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 275
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 276
    iget v0, p0, Lip;->x:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 277
    const-string v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 278
    iget v0, p0, Lip;->y:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 279
    const-string v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->z:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lip;->c:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 281
    const-string v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lip;->f:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 282
    const-string v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->g:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 283
    const-string v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lip;->r:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 284
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 285
    const-string v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 286
    const-string v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->n:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 287
    const-string v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->o:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 288
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->A:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 289
    const-string v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->B:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 290
    const-string v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->F:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 291
    const-string v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->E:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 292
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->C:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 293
    const-string v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->D:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 294
    const-string v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lip;->L:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 295
    iget-object v0, p0, Lip;->s:Ljc;

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lip;->s:Ljc;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    if-eqz v0, :cond_1

    .line 299
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lip;->t:Liz;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 301
    :cond_1
    iget-object v0, p0, Lip;->w:Lip;

    if-eqz v0, :cond_2

    .line 302
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lip;->w:Lip;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 304
    :cond_2
    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 305
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->h:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 306
    :cond_3
    iget-object v0, p0, Lip;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 307
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 309
    :cond_4
    iget-object v0, p0, Lip;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    .line 310
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lip;->e:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 312
    :cond_5
    iget-object v0, p0, Lip;->i:Lip;

    if-eqz v0, :cond_6

    .line 313
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->i:Lip;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 314
    const-string v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 315
    iget v0, p0, Lip;->k:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 316
    :cond_6
    invoke-virtual {p0}, Lip;->J()I

    move-result v0

    if-eqz v0, :cond_7

    .line 317
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Lip;->J()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 318
    :cond_7
    iget-object v0, p0, Lip;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    .line 319
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->H:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 320
    :cond_8
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 321
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 322
    :cond_9
    iget-object v0, p0, Lip;->J:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 323
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lip;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 324
    :cond_a
    invoke-virtual {p0}, Lip;->O()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 325
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 326
    const-string v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lip;->O()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 328
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 329
    const-string v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lip;->Q()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 331
    :cond_b
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_c

    .line 332
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lip;->M:Lkz;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lkz;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 334
    :cond_c
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_d

    .line 335
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Child "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lip;->u:Ljc;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 336
    iget-object v0, p0, Lip;->u:Ljc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Ljc;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 337
    :cond_d
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    invoke-virtual {v0, p0, p1, p2}, Liz;->a(Lip;[Ljava/lang/String;I)V

    .line 128
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lip;->t:Liz;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lip;->t:Liz;

    invoke-virtual {v0, p1}, Liz;->a(Ljava/lang/String;)Z

    move-result v0

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 355
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->q:Z

    .line 356
    invoke-virtual {p0, p1, p2, p3}, Lip;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 221
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 169
    iput-boolean v0, p0, Lip;->G:Z

    .line 170
    invoke-virtual {p0, p1}, Lip;->h(Landroid/os/Bundle;)V

    .line 171
    iget-object v1, p0, Lip;->u:Ljc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lip;->u:Ljc;

    .line 173
    iget v1, v1, Ljc;->e:I

    if-lez v1, :cond_1

    .line 174
    :goto_0
    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->l()V

    .line 176
    :cond_0
    return-void

    .line 173
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 485
    invoke-virtual {p0}, Lip;->I()Lip$a;

    move-result-object v0

    iput-object p1, v0, Lip$a;->a:Landroid/view/View;

    .line 486
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lip;->F:Z

    if-eq v0, p1, :cond_0

    .line 95
    iput-boolean p1, p0, Lip;->F:Z

    .line 96
    iget-boolean v0, p0, Lip;->E:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lip;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-boolean v0, p0, Lip;->A:Z

    .line 98
    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lip;->t:Liz;

    invoke-virtual {v0}, Liz;->d()V

    .line 100
    :cond_0
    return-void
.end method

.method public final b_(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 2

    .prologue
    .line 133
    .line 134
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    invoke-virtual {v0}, Liz;->c()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 137
    invoke-virtual {p0}, Lip;->j()Lja;

    .line 138
    iget-object v1, p0, Lip;->u:Ljc;

    .line 140
    invoke-static {v0, v1}, Lqi;->b(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V

    .line 142
    return-object v0
.end method

.method final c(I)V
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 463
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {p0}, Lip;->I()Lip$a;

    move-result-object v0

    iput p1, v0, Lip$a;->d:I

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 101
    iget-boolean v0, p0, Lip;->L:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget v0, p0, Lip;->c:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lip;->s:Ljc;

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lip;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lip;->s:Ljc;

    invoke-virtual {v0, p0}, Ljc;->b(Lip;)V

    .line 104
    :cond_0
    iput-boolean p1, p0, Lip;->L:Z

    .line 105
    iget v0, p0, Lip;->c:I

    if-ge v0, v1, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lip;->K:Z

    .line 106
    return-void

    .line 105
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d(I)V
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0}, Lip;->I()Lip$a;

    move-result-object v0

    iput p1, v0, Lip$a;->c:I

    .line 496
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 189
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method final e(Z)V
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Lip;->I()Lip$a;

    move-result-object v0

    iput-boolean p1, v0, Lip$a;->n:Z

    .line 504
    return-void
.end method

.method public f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lip;->f:I

    if-ltz v0, :cond_1

    .line 55
    iget-object v0, p0, Lip;->s:Ljc;

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 59
    :goto_0
    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment already active and state has been saved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    iget-object v0, p0, Lip;->s:Ljc;

    .line 58
    iget-boolean v0, v0, Ljc;->i:Z

    goto :goto_0

    .line 61
    :cond_1
    iput-object p1, p0, Lip;->h:Landroid/os/Bundle;

    .line 62
    return-void
.end method

.method final g(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lip;->c(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 144
    iput-object v0, p0, Lip;->T:Landroid/view/LayoutInflater;

    .line 145
    iget-object v0, p0, Lip;->T:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public final h()Lit;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    .line 66
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    .line 67
    iget-object v0, v0, Liz;->a:Landroid/app/Activity;

    .line 68
    check-cast v0, Lit;

    goto :goto_0
.end method

.method final h(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 177
    if-eqz p1, :cond_1

    .line 178
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_1

    .line 180
    iget-object v1, p0, Lip;->u:Ljc;

    if-nez v1, :cond_0

    .line 181
    invoke-direct {p0}, Lip;->a()V

    .line 182
    :cond_0
    iget-object v1, p0, Lip;->u:Ljc;

    iget-object v2, p0, Lip;->v:Ljr;

    invoke-virtual {v1, v0, v2}, Ljc;->a(Landroid/os/Parcelable;Ljr;)V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lip;->v:Ljr;

    .line 184
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->l()V

    .line 185
    :cond_1
    return-void
.end method

.method public final i()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    .line 72
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 73
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method final i(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 343
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 345
    :cond_0
    iput v1, p0, Lip;->c:I

    .line 346
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 347
    invoke-virtual {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 348
    iput-boolean v1, p0, Lip;->U:Z

    .line 349
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 350
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351
    :cond_1
    iget-object v0, p0, Lip;->V:Ll;

    sget-object v1, Li;->a:Li;

    invoke-virtual {v0, v1}, Ll;->a(Li;)V

    .line 352
    return-void
.end method

.method public final j()Lja;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lip;->u:Ljc;

    if-nez v0, :cond_0

    .line 78
    invoke-direct {p0}, Lip;->a()V

    .line 79
    iget v0, p0, Lip;->c:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    .line 80
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->o()V

    .line 87
    :cond_0
    :goto_0
    iget-object v0, p0, Lip;->u:Ljc;

    return-object v0

    .line 81
    :cond_1
    iget v0, p0, Lip;->c:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    .line 82
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->n()V

    goto :goto_0

    .line 83
    :cond_2
    iget v0, p0, Lip;->c:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    .line 84
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->m()V

    goto :goto_0

    .line 85
    :cond_3
    iget v0, p0, Lip;->c:I

    if-lez v0, :cond_0

    .line 86
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->l()V

    goto :goto_0
.end method

.method final j(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 357
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 359
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lip;->c:I

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lip;->G:Z

    .line 361
    invoke-virtual {p0, p1}, Lip;->d(Landroid/os/Bundle;)V

    .line 362
    iget-boolean v0, p0, Lip;->G:Z

    if-nez v0, :cond_1

    .line 363
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_1
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->m()V

    .line 366
    :cond_2
    return-void
.end method

.method final k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 395
    iget-object v0, p0, Lip;->u:Ljc;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->j()Landroid/os/Parcelable;

    move-result-object v0

    .line 397
    if-eqz v0, :cond_0

    .line 398
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 399
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lip;->t:Liz;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lip;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lip;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-boolean v0, p0, Lip;->A:Z

    .line 91
    if-nez v0, :cond_0

    iget-object v0, p0, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lip;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l_()V
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 208
    return-void
.end method

.method public final n()Lkx;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 107
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lip;->M:Lkz;

    .line 113
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_1

    .line 110
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_1
    iput-boolean v3, p0, Lip;->O:Z

    .line 112
    iget-object v0, p0, Lip;->t:Liz;

    iget-object v1, p0, Lip;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lip;->N:Z

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v0

    iput-object v0, p0, Lip;->M:Lkz;

    .line 113
    iget-object v0, p0, Lip;->M:Lkz;

    goto :goto_0
.end method

.method public n_()V
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 212
    return-void
.end method

.method public o_()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 190
    iput-boolean v1, p0, Lip;->G:Z

    .line 191
    iget-boolean v0, p0, Lip;->N:Z

    if-nez v0, :cond_0

    .line 192
    iput-boolean v1, p0, Lip;->N:Z

    .line 193
    iget-boolean v0, p0, Lip;->O:Z

    if-nez v0, :cond_1

    .line 194
    iput-boolean v1, p0, Lip;->O:Z

    .line 195
    iget-object v0, p0, Lip;->t:Liz;

    iget-object v1, p0, Lip;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lip;->N:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v0

    iput-object v0, p0, Lip;->M:Lkz;

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->b()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 204
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lip;->h()Lit;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lit;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 224
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 210
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 200
    return-void
.end method

.method final r_()Z
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lip;->r:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lip;->G:Z

    .line 206
    return-void
.end method

.method public s_()Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    .line 63
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    .line 64
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 119
    .line 120
    iget-object v0, p0, Lip;->t:Liz;

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    iget-object v0, p0, Lip;->t:Liz;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, p2, v1}, Liz;->a(Lip;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 123
    return-void
.end method

.method public t()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 213
    iput-boolean v1, p0, Lip;->G:Z

    .line 214
    iget-boolean v0, p0, Lip;->O:Z

    if-nez v0, :cond_0

    .line 215
    iput-boolean v1, p0, Lip;->O:Z

    .line 216
    iget-object v0, p0, Lip;->t:Liz;

    iget-object v1, p0, Lip;->g:Ljava/lang/String;

    iget-boolean v2, p0, Lip;->N:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v0

    iput-object v0, p0, Lip;->M:Lkz;

    .line 217
    :cond_0
    iget-object v0, p0, Lip;->M:Lkz;

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->g()V

    .line 219
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 42
    invoke-static {p0, v0}, Lbw;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 43
    iget v1, p0, Lip;->f:I

    if-ltz v1, :cond_0

    .line 44
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    iget v1, p0, Lip;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    :cond_0
    iget v1, p0, Lip;->x:I

    if-eqz v1, :cond_1

    .line 47
    const-string v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget v1, p0, Lip;->x:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    :cond_1
    iget-object v1, p0, Lip;->z:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 50
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget-object v1, p0, Lip;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 231
    :goto_0
    return-object v1

    .line 229
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    goto :goto_0
.end method

.method public final w()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 233
    const/4 v0, 0x0

    .line 239
    :goto_0
    return-object v0

    .line 234
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 235
    iget-object v0, v0, Lip$a;->g:Ljava/lang/Object;

    .line 236
    sget-object v1, Lip;->b:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lip;->v()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 238
    iget-object v0, v0, Lip$a;->g:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final x()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 240
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-object v1

    .line 242
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    goto :goto_0
.end method

.method public final y()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 252
    :goto_0
    return-object v0

    .line 247
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 248
    iget-object v0, v0, Lip$a;->h:Ljava/lang/Object;

    .line 249
    sget-object v1, Lip;->b:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lip;->x()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lip;->P:Lip$a;

    .line 251
    iget-object v0, v0, Lip$a;->h:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final z()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lip;->P:Lip$a;

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-object v1

    .line 255
    :cond_0
    iget-object v0, p0, Lip;->P:Lip$a;

    goto :goto_0
.end method
