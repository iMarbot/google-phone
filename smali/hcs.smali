.class final Lhcs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhea;


# static fields
.field private static c:Lhdc;


# instance fields
.field private a:Lhdc;

.field private b:Lhcv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lhct;

    invoke-direct {v0}, Lhct;-><init>()V

    sput-object v0, Lhcs;->c:Lhdc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhcv;->c:Lhcv;

    invoke-direct {p0, v0}, Lhcs;-><init>(Lhcv;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lhcv;)V
    .locals 4

    .prologue
    .line 3
    .line 4
    new-instance v0, Lhcu;

    const/4 v1, 0x2

    new-array v1, v1, [Lhdc;

    const/4 v2, 0x0

    .line 5
    sget-object v3, Lhbq;->a:Lhbq;

    .line 6
    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {}, Lhcs;->a()Lhdc;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lhcu;-><init>([Lhdc;)V

    .line 7
    invoke-direct {p0, v0, p1}, Lhcs;-><init>(Lhdc;Lhcv;)V

    .line 8
    return-void
.end method

.method private constructor <init>(Lhdc;Lhcv;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, "messageInfoFactory"

    invoke-static {p1, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdc;

    iput-object v0, p0, Lhcs;->a:Lhdc;

    .line 11
    const-string v0, "mode"

    invoke-static {p2, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcv;

    iput-object v0, p0, Lhcs;->b:Lhcv;

    .line 12
    return-void
.end method

.method private static a()Lhdc;
    .locals 3

    .prologue
    .line 102
    :try_start_0
    const-string v0, "com.google.protobuf.DescriptorMessageInfoFactory"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 103
    const-string v1, "getInstance"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdc;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lhcs;->c:Lhdc;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Lhdb;)Lhdz;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 35
    const-class v0, Lhbr;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    invoke-static {p1}, Lhcs;->a(Lhdb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    sget-object v2, Lhdk;->b:Lhdj;

    .line 39
    sget-object v3, Lhco;->b:Lhco;

    .line 40
    sget-object v4, Lheb;->c:Lhep;

    .line 41
    sget-object v5, Lhbi;->a:Lhbh;

    .line 42
    sget-object v6, Lhda;->b:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 43
    invoke-static/range {v0 .. v6}, Lhdg;->a(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 45
    :cond_0
    sget-object v2, Lhdk;->b:Lhdj;

    .line 46
    sget-object v3, Lhco;->b:Lhco;

    .line 47
    sget-object v4, Lheb;->c:Lhep;

    .line 49
    sget-object v6, Lhda;->b:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 50
    invoke-static/range {v0 .. v6}, Lhdg;->a(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_1
    invoke-static {p1}, Lhcs;->a(Lhdb;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    sget-object v2, Lhdk;->a:Lhdj;

    .line 55
    sget-object v3, Lhco;->a:Lhco;

    .line 56
    sget-object v4, Lheb;->a:Lhep;

    .line 57
    invoke-static {}, Lhbi;->a()Lhbh;

    move-result-object v5

    .line 58
    sget-object v6, Lhda;->a:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 59
    invoke-static/range {v0 .. v6}, Lhdg;->a(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_2
    sget-object v2, Lhdk;->a:Lhdj;

    .line 62
    sget-object v3, Lhco;->a:Lhco;

    .line 63
    sget-object v4, Lheb;->b:Lhep;

    .line 65
    sget-object v6, Lhda;->a:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 66
    invoke-static/range {v0 .. v6}, Lhdg;->a(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lhdb;)Z
    .locals 2

    .prologue
    .line 101
    invoke-interface {p0}, Lhdb;->a()Lhdo;

    move-result-object v0

    sget-object v1, Lhdo;->a:Lhdo;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/Class;Lhdb;)Lhdz;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 68
    const-class v0, Lhbr;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    invoke-static {p1}, Lhcs;->a(Lhdb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    sget-object v2, Lhdk;->b:Lhdj;

    .line 72
    sget-object v3, Lhco;->b:Lhco;

    .line 73
    sget-object v4, Lheb;->c:Lhep;

    .line 74
    sget-object v5, Lhbi;->a:Lhbh;

    .line 75
    sget-object v6, Lhda;->b:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 76
    invoke-static/range {v0 .. v6}, Lhdg;->b(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 78
    :cond_0
    sget-object v2, Lhdk;->b:Lhdj;

    .line 79
    sget-object v3, Lhco;->b:Lhco;

    .line 80
    sget-object v4, Lheb;->c:Lhep;

    .line 82
    sget-object v6, Lhda;->b:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 83
    invoke-static/range {v0 .. v6}, Lhdg;->b(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {p1}, Lhcs;->a(Lhdb;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    sget-object v2, Lhdk;->a:Lhdj;

    .line 88
    sget-object v3, Lhco;->a:Lhco;

    .line 89
    sget-object v4, Lheb;->a:Lhep;

    .line 90
    invoke-static {}, Lhbi;->a()Lhbh;

    move-result-object v5

    .line 91
    sget-object v6, Lhda;->a:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 92
    invoke-static/range {v0 .. v6}, Lhdg;->b(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_2
    sget-object v2, Lhdk;->a:Lhdj;

    .line 95
    sget-object v3, Lhco;->a:Lhco;

    .line 96
    sget-object v4, Lheb;->b:Lhep;

    .line 98
    sget-object v6, Lhda;->a:Lhcz;

    move-object v0, p0

    move-object v1, p1

    .line 99
    invoke-static/range {v0 .. v6}, Lhdg;->b(Ljava/lang/Class;Lhdb;Lhdj;Lhco;Lhep;Lhbh;Lhcz;)Lhdg;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lhdz;
    .locals 3

    .prologue
    .line 13
    invoke-static {p1}, Lheb;->a(Ljava/lang/Class;)V

    .line 14
    iget-object v0, p0, Lhcs;->a:Lhdc;

    invoke-interface {v0, p1}, Lhdc;->b(Ljava/lang/Class;)Lhdb;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lhdb;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16
    const-class v1, Lhbr;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18
    sget-object v1, Lheb;->c:Lhep;

    .line 19
    sget-object v2, Lhbi;->a:Lhbh;

    .line 21
    invoke-interface {v0}, Lhdb;->c()Lhdd;

    move-result-object v0

    .line 22
    invoke-static {p1, v1, v2, v0}, Lhdh;->a(Ljava/lang/Class;Lhep;Lhbh;Lhdd;)Lhdh;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 24
    :cond_0
    sget-object v1, Lheb;->a:Lhep;

    .line 25
    invoke-static {}, Lhbi;->a()Lhbh;

    move-result-object v2

    .line 26
    invoke-interface {v0}, Lhdb;->c()Lhdd;

    move-result-object v0

    .line 27
    invoke-static {p1, v1, v2, v0}, Lhdh;->a(Ljava/lang/Class;Lhep;Lhbh;Lhdd;)Lhdh;

    move-result-object v0

    goto :goto_0

    .line 28
    :cond_1
    iget-object v1, p0, Lhcs;->b:Lhcv;

    invoke-virtual {v1}, Lhcv;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 31
    invoke-interface {v0}, Lhdb;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 32
    invoke-static {p1, v0}, Lhcs;->a(Ljava/lang/Class;Lhdb;)Lhdz;

    move-result-object v0

    goto :goto_0

    .line 29
    :pswitch_0
    invoke-static {p1, v0}, Lhcs;->a(Ljava/lang/Class;Lhdb;)Lhdz;

    move-result-object v0

    goto :goto_0

    .line 30
    :pswitch_1
    invoke-static {p1, v0}, Lhcs;->b(Ljava/lang/Class;Lhdb;)Lhdz;

    move-result-object v0

    goto :goto_0

    .line 33
    :cond_2
    invoke-static {p1, v0}, Lhcs;->b(Ljava/lang/Class;Lhdb;)Lhdz;

    move-result-object v0

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
