.class public final Lazn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Landroid/graphics/Matrix;

.field public g:Lazr;

.field public h:I

.field public i:I

.field public j:Z

.field public k:Ljava/util/List;

.field public l:Ljava/util/List;

.field public m:Ljava/lang/String;

.field public n:Landroid/hardware/Camera$Parameters;

.field public o:Landroid/os/Handler;

.field public p:Lazo;


# direct methods
.method public constructor <init>(Lazo;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lazn;->a:I

    .line 3
    new-instance v0, Lazp;

    invoke-direct {v0, p0, p2}, Lazp;-><init>(Lazn;Landroid/os/Looper;)V

    iput-object v0, p0, Lazn;->o:Landroid/os/Handler;

    .line 4
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazn;->f:Landroid/graphics/Matrix;

    .line 5
    iput-object p1, p0, Lazn;->p:Lazo;

    .line 6
    return-void
.end method

.method private static a(III)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 77
    if-le p0, p2, :cond_1

    .line 81
    :goto_1
    return p2

    :cond_0
    move v0, v1

    .line 76
    goto :goto_0

    .line 79
    :cond_1
    if-gez p0, :cond_2

    move p2, v1

    .line 80
    goto :goto_1

    :cond_2
    move p2, p0

    .line 81
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 82
    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-boolean v0, p0, Lazn;->b:Z

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lazn;->g:Lazr;

    invoke-virtual {v0}, Lazr;->b()V

    .line 59
    iput-object v1, p0, Lazn;->k:Ljava/util/List;

    .line 60
    iput-object v1, p0, Lazn;->l:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/high16 v6, 0x44fa0000    # 2000.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 7
    iget v0, p0, Lazn;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Lazn;->i:I

    if-eqz v0, :cond_0

    .line 8
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 9
    iget-boolean v0, p0, Lazn;->j:Z

    iget v3, p0, Lazn;->h:I

    iget v4, p0, Lazn;->i:I

    .line 10
    if-eqz v0, :cond_1

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 11
    int-to-float v0, v3

    div-float/2addr v0, v6

    int-to-float v1, v4

    div-float/2addr v1, v6

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 12
    int-to-float v0, v3

    div-float/2addr v0, v5

    int-to-float v1, v4

    div-float/2addr v1, v5

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 13
    iget-object v0, p0, Lazn;->f:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 14
    iget-object v0, p0, Lazn;->g:Lazr;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lazn;->b:Z

    .line 15
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 10
    goto :goto_0

    .line 14
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(IIFIIIILandroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 62
    int-to-float v1, p1

    mul-float/2addr v1, p3

    float-to-int v2, v1

    .line 63
    int-to-float v1, p2

    mul-float/2addr v1, p3

    float-to-int v3, v1

    .line 64
    sub-int v1, p6, v2

    .line 65
    if-lez v1, :cond_1

    div-int/lit8 v4, v2, 0x2

    sub-int v4, p4, v4

    invoke-static {v4, v0, v1}, Lazn;->a(III)I

    move-result v1

    .line 66
    :goto_0
    sub-int v4, p7, v3

    .line 67
    if-lez v4, :cond_0

    div-int/lit8 v5, v3, 0x2

    sub-int v5, p5, v5

    invoke-static {v5, v0, v4}, Lazn;->a(III)I

    move-result v0

    .line 68
    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v1

    int-to-float v6, v0

    add-int/2addr v1, v2

    int-to-float v1, v1

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-direct {v4, v5, v6, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 69
    iget-object v0, p0, Lazn;->f:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 71
    iget v0, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->left:I

    .line 72
    iget v0, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->top:I

    .line 73
    iget v0, v4, Landroid/graphics/RectF;->right:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->right:I

    .line 74
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p8, Landroid/graphics/Rect;->bottom:I

    .line 75
    return-void

    :cond_1
    move v1, v0

    .line 65
    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 16
    iget v0, p0, Lazn;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 17
    if-eqz p1, :cond_1

    .line 18
    iput v2, p0, Lazn;->a:I

    .line 20
    :goto_0
    invoke-virtual {p0}, Lazn;->d()V

    .line 29
    :cond_0
    :goto_1
    return-void

    .line 19
    :cond_1
    iput v3, p0, Lazn;->a:I

    goto :goto_0

    .line 22
    :cond_2
    iget v0, p0, Lazn;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 23
    if-eqz p1, :cond_3

    .line 24
    iput v2, p0, Lazn;->a:I

    .line 26
    :goto_2
    invoke-virtual {p0}, Lazn;->d()V

    .line 27
    iget-object v0, p0, Lazn;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lazn;->o:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 25
    :cond_3
    iput v3, p0, Lazn;->a:I

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lazn;->a:I

    .line 31
    invoke-direct {p0}, Lazn;->e()V

    .line 32
    invoke-virtual {p0}, Lazn;->d()V

    .line 33
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lazn;->e()V

    .line 35
    iget-object v0, p0, Lazn;->p:Lazo;

    invoke-interface {v0}, Lazo;->f()V

    .line 36
    iput v1, p0, Lazn;->a:I

    .line 37
    invoke-virtual {p0}, Lazn;->d()V

    .line 38
    iget-object v0, p0, Lazn;->o:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 39
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    iget-boolean v0, p0, Lazn;->b:Z

    if-nez v0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-object v0, p0, Lazn;->g:Lazr;

    .line 43
    iget v1, p0, Lazn;->a:I

    if-nez v1, :cond_3

    .line 44
    iget-object v1, p0, Lazn;->k:Ljava/util/List;

    if-nez v1, :cond_2

    .line 45
    invoke-interface {v0}, Lazm;->b()V

    goto :goto_0

    .line 46
    :cond_2
    invoke-interface {v0}, Lazm;->a()V

    goto :goto_0

    .line 47
    :cond_3
    iget v1, p0, Lazn;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    iget v1, p0, Lazn;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 48
    :cond_4
    invoke-interface {v0}, Lazm;->a()V

    goto :goto_0

    .line 49
    :cond_5
    const-string v1, "continuous-picture"

    iget-object v2, p0, Lazn;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 50
    invoke-interface {v0, v3}, Lazm;->a(Z)V

    goto :goto_0

    .line 51
    :cond_6
    iget v1, p0, Lazn;->a:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_7

    .line 52
    invoke-interface {v0, v3}, Lazm;->a(Z)V

    goto :goto_0

    .line 53
    :cond_7
    iget v1, p0, Lazn;->a:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 54
    invoke-interface {v0, v3}, Lazm;->b(Z)V

    goto :goto_0
.end method
