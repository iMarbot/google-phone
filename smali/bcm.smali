.class public final Lbcm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbcg;


# instance fields
.field private a:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/Set;)Ljava/util/Set;
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 241
    new-instance v7, Landroid/util/ArraySet;

    invoke-direct {v7}, Landroid/util/ArraySet;-><init>()V

    .line 242
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 243
    const-string v1, "?"

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 244
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "_id in ("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 245
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 247
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v6

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 248
    add-int/lit8 v0, v1, 0x1

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    move v1, v0

    .line 249
    goto :goto_0

    .line 251
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v8, "_id"

    aput-object v8, v2, v6

    .line 252
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 254
    if-nez v2, :cond_2

    .line 255
    :try_start_0
    const-string v0, "SystemCallLogDataSource.getIdsFromSystemCallLog"

    const-string v1, "null cursor"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v7

    .line 265
    :goto_1
    return-object v0

    .line 259
    :cond_2
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 261
    :cond_3
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_3

    .line 264
    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v0, v7

    .line 265
    goto :goto_1

    .line 266
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 267
    :catchall_0
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_6

    if-eqz v5, :cond_7

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_6
    :goto_3
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v5, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Ljava/util/Set;Lbci;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 204
    .line 205
    invoke-static {p0, p1}, Lbcm;->a(Landroid/content/Context;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 206
    const-string v3, "SystemCallLogDataSource.handleDeletes"

    const-string v4, "found %d matching entries in system call log"

    new-array v5, v1, [Ljava/lang/Object;

    .line 207
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 208
    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 210
    invoke-interface {v3, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 211
    invoke-interface {v3, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 212
    const-string v0, "SystemCallLogDataSource.handleDeletes"

    const-string v4, "found %d call log entries to remove"

    new-array v5, v1, [Ljava/lang/Object;

    .line 213
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    .line 214
    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 217
    iget-object v0, p2, Lbci;->a:Landroid/util/ArrayMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    const-string v6, "Can\'t delete row scheduled for insert"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 218
    iget-object v0, p2, Lbci;->b:Landroid/util/ArrayMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    const-string v6, "Can\'t delete row scheduled for update"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 219
    iget-object v0, p2, Lbci;->c:Landroid/util/ArraySet;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_3
    const-string v6, "Can\'t delete row already scheduled for delete"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v0, p2, Lbci;->c:Landroid/util/ArraySet;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move v0, v2

    .line 217
    goto :goto_1

    :cond_1
    move v0, v2

    .line 218
    goto :goto_2

    :cond_2
    move v0, v2

    .line 219
    goto :goto_3

    .line 222
    :cond_3
    return-void
.end method

.method private static c(Landroid/content/Context;)Ljava/util/Set;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 223
    new-instance v6, Landroid/util/ArraySet;

    invoke-direct {v6}, Landroid/util/ArraySet;-><init>()V

    .line 225
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbcd;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    .line 226
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 228
    if-nez v2, :cond_1

    .line 229
    :try_start_0
    const-string v0, "SystemCallLogDataSource.getAnnotatedCallLogIds"

    const-string v1, "null cursor"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 231
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    .line 240
    :goto_0
    return-object v0

    .line 233
    :cond_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 235
    :cond_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-nez v1, :cond_2

    .line 237
    :cond_3
    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v6

    .line 240
    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 239
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v3, :cond_6

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v3, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/List;)Landroid/content/ContentValues;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 164
    .line 165
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 166
    const-string v3, "call_type"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 167
    const-string v3, "call_type"

    .line 168
    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 169
    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 168
    goto :goto_1

    .line 171
    :cond_2
    new-instance v0, Lbcp;

    invoke-direct {v0, p1}, Lbcp;-><init>(Ljava/util/List;)V

    const-string v2, "timestamp"

    .line 172
    invoke-virtual {v0, v2}, Lbcp;->a(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "new"

    .line 173
    invoke-virtual {v0, v2}, Lbcp;->a(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "number_type_label"

    .line 174
    invoke-virtual {v0, v2}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "name"

    .line 175
    invoke-virtual {v0, v2}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v2

    const-string v3, "number"

    .line 177
    iget-object v4, v2, Lbcp;->b:Landroid/content/ContentValues;

    iget-object v0, v2, Lbcp;->a:Ljava/util/List;

    .line 178
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 179
    invoke-virtual {v4, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 181
    const-string v0, "formatted_number"

    .line 182
    invoke-virtual {v2, v0}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "photo_uri"

    .line 183
    invoke-virtual {v0, v2}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "photo_id"

    .line 184
    invoke-virtual {v0, v2}, Lbcp;->a(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "lookup_uri"

    .line 185
    invoke-virtual {v0, v2}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "geocoded_location"

    .line 186
    invoke-virtual {v0, v2}, Lbcp;->b(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "phone_account_component_name"

    .line 187
    invoke-virtual {v0, v2}, Lbcp;->c(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "phone_account_id"

    .line 188
    invoke-virtual {v0, v2}, Lbcp;->c(Ljava/lang/String;)Lbcp;

    move-result-object v0

    const-string v2, "phone_account_label"

    .line 189
    invoke-virtual {v0, v2}, Lbcp;->c(Ljava/lang/String;)Lbcp;

    move-result-object v2

    const-string v3, "phone_account_color"

    .line 191
    iget-object v0, v2, Lbcp;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 192
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    .line 193
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 194
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 195
    invoke-static {v5, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    const-string v7, "Values different for "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v6, v0, v7}, Lbdf;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 197
    :cond_4
    iget-object v0, v2, Lbcp;->b:Landroid/content/ContentValues;

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 199
    const-string v0, "call_type"

    .line 200
    invoke-virtual {v2, v0}, Lbcp;->a(Ljava/lang/String;)Lbcp;

    move-result-object v0

    .line 202
    iget-object v0, v0, Lbcp;->b:Landroid/content/ContentValues;

    .line 203
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lbch;)V
    .locals 5

    .prologue
    .line 2
    invoke-static {}, Lbdf;->b()V

    .line 3
    const-string v0, "SystemCallLogDataSource.registerContentObservers"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    invoke-static {p1}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    const-string v0, "SystemCallLogDataSource.registerContentObservers"

    const-string v1, "no call log permissions"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    :goto_0
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    const/4 v2, 0x1

    new-instance v3, Lbcn;

    .line 9
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, v4, p1, p2}, Lbcn;-><init>(Landroid/os/Handler;Landroid/content/Context;Lbch;)V

    .line 10
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lbci;)V
    .locals 57

    .prologue
    .line 17
    invoke-static {}, Lbdf;->c()V

    .line 18
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lbcm;->a:Ljava/lang/Long;

    .line 19
    const-string v4, "android.permission.READ_CALL_LOG"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 20
    const-string v4, "SystemCallLogDataSource.fill"

    const-string v5, "no call log permissions"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    :goto_0
    return-void

    .line 22
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lbci;->a()Z

    move-result v4

    invoke-static {v4}, Lbdf;->a(Z)V

    .line 23
    invoke-static/range {p1 .. p1}, Lbcm;->c(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v10

    .line 24
    const-string v4, "SystemCallLogDataSource.fill"

    const-string v5, "found %d existing annotated call log ids"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 25
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 26
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-static/range {p1 .. p1}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v4

    .line 30
    invoke-virtual {v4}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "systemCallLogLastTimestampProcessed"

    const-wide/16 v6, 0x0

    .line 31
    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 32
    new-instance v11, Lbmv;

    .line 33
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v4

    invoke-direct {v11, v4}, Lbmv;-><init>(Lgxg;)V

    .line 35
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    const/16 v6, 0x16

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "date"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "last_modified"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "number"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "type"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "countryiso"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "name"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "formatted_number"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "photo_uri"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string v8, "photo_id"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "lookup_uri"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string v8, "numbertype"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "numberlabel"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string v8, "duration"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "transcription"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string v8, "voicemail_uri"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "is_read"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    const-string v8, "new"

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "geocoded_location"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    const-string v8, "subscription_component_name"

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string v8, "subscription_id"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    const-string v8, "features"

    aput-object v8, v6, v7

    const-string v7, "last_modified > ?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 36
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v8, v9

    const-string v9, "last_modified DESC LIMIT 1000"

    .line 37
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 38
    const/4 v5, 0x0

    .line 39
    if-nez v6, :cond_2

    .line 40
    :try_start_0
    const-string v4, "SystemCallLogDataSource.handleInsertsAndUpdates"

    const-string v7, "null cursor"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v7, v8}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 155
    :cond_1
    :goto_1
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v10, v1}, Lbcm;->a(Landroid/content/Context;Ljava/util/Set;Lbci;)V

    goto/16 :goto_0

    .line 43
    :cond_2
    :try_start_1
    const-string v4, "SystemCallLogDataSource.handleInsertsAndUpdates"

    const-string v7, "found %d entries to insert/update"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 44
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v8, v9

    .line 45
    invoke-static {v4, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 47
    const-string v4, "_id"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 48
    const-string v4, "date"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 49
    const-string v4, "last_modified"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 50
    const-string v9, "number"

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 51
    const-string v12, "type"

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 52
    const-string v13, "countryiso"

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 53
    const-string v14, "name"

    invoke-interface {v6, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 54
    const-string v15, "formatted_number"

    .line 55
    invoke-interface {v6, v15}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 56
    const-string v16, "photo_uri"

    move-object/from16 v0, v16

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 57
    const-string v17, "photo_id"

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 58
    const-string v18, "lookup_uri"

    move-object/from16 v0, v18

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 59
    const-string v19, "numbertype"

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 60
    const-string v20, "numberlabel"

    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    .line 61
    const-string v21, "duration"

    move-object/from16 v0, v21

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v21

    .line 62
    const-string v22, "transcription"

    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 63
    const-string v23, "voicemail_uri"

    move-object/from16 v0, v23

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 64
    const-string v24, "is_read"

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v24

    .line 65
    const-string v25, "new"

    move-object/from16 v0, v25

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v25

    .line 66
    const-string v26, "geocoded_location"

    move-object/from16 v0, v26

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    .line 67
    const-string v27, "subscription_component_name"

    .line 68
    move-object/from16 v0, v27

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    .line 69
    const-string v28, "subscription_id"

    move-object/from16 v0, v28

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v28

    .line 70
    const-string v29, "features"

    move-object/from16 v0, v29

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v29

    .line 71
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lbcm;->a:Ljava/lang/Long;

    .line 72
    :cond_3
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 73
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    .line 74
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 75
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v34

    move/from16 v0, v34

    int-to-long v0, v0

    move-wide/from16 v34, v0

    .line 76
    invoke-interface {v6, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 77
    invoke-interface {v6, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    .line 78
    invoke-interface {v6, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v38

    .line 79
    move/from16 v0, v16

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v39

    .line 80
    move/from16 v0, v17

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 81
    move/from16 v0, v18

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v42

    .line 82
    move/from16 v0, v19

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v43

    .line 83
    move/from16 v0, v20

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 84
    move/from16 v0, v21

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v45

    .line 85
    move/from16 v0, v22

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v46

    .line 86
    move/from16 v0, v23

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    .line 87
    move/from16 v0, v24

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v48

    .line 88
    move/from16 v0, v25

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v49

    .line 89
    move/from16 v0, v26

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 90
    move/from16 v0, v27

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v51

    .line 91
    move/from16 v0, v28

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v52

    .line 92
    move/from16 v0, v29

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v53

    .line 93
    new-instance v54, Landroid/content/ContentValues;

    invoke-direct/range {v54 .. v54}, Landroid/content/ContentValues;-><init>()V

    .line 94
    const-string v55, "timestamp"

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 95
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_9

    .line 97
    move-object/from16 v0, v36

    invoke-virtual {v11, v4, v0}, Lbmv;->a(Ljava/lang/String;Ljava/lang/String;)Lamg;

    move-result-object v4

    invoke-virtual {v4}, Lamg;->toByteArray()[B

    move-result-object v4

    .line 98
    const-string v32, "number"

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 104
    :goto_2
    const-string v4, "call_type"

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 105
    const-string v4, "name"

    move-object/from16 v0, v54

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v4, "formatted_number"

    move-object/from16 v0, v54

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v4, "photo_uri"

    move-object/from16 v0, v54

    move-object/from16 v1, v39

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v4, "photo_id"

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 109
    const-string v4, "lookup_uri"

    move-object/from16 v0, v54

    move-object/from16 v1, v42

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    if-nez v43, :cond_4

    if-eqz v44, :cond_5

    .line 111
    :cond_4
    const-string v4, "number_type_label"

    .line 112
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    move-object/from16 v0, v32

    move/from16 v1, v43

    move-object/from16 v2, v44

    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v32

    .line 113
    invoke-interface/range {v32 .. v32}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v32

    .line 114
    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_5
    const-string v4, "is_read"

    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    const-string v4, "new"

    invoke-static/range {v49 .. v49}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 117
    const-string v4, "geocoded_location"

    move-object/from16 v0, v54

    move-object/from16 v1, v50

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v4, "phone_account_component_name"

    move-object/from16 v0, v54

    move-object/from16 v1, v51

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v4, "phone_account_id"

    move-object/from16 v0, v54

    move-object/from16 v1, v52

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static/range {v51 .. v52}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 123
    if-eqz v4, :cond_7

    .line 124
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lapw;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v32

    .line 125
    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v33

    if-nez v33, :cond_7

    .line 126
    const-string v33, "phone_account_label"

    move-object/from16 v0, v54

    move-object/from16 v1, v33

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lapw;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)I

    move-result v4

    .line 128
    if-nez v4, :cond_6

    .line 130
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v32, 0x7f0c006e

    .line 131
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v33

    move/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v4

    .line 132
    :cond_6
    const-string v32, "phone_account_color"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    :cond_7
    const-string v4, "features"

    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    const-string v4, "duration"

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    const-string v4, "transcription"

    move-object/from16 v0, v54

    move-object/from16 v1, v46

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v4, "voicemail_uri"

    move-object/from16 v0, v54

    move-object/from16 v1, v47

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 139
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->a:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v4, 0x1

    :goto_3
    const-string v32, "Can\'t update row scheduled for insert"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 140
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->c:Landroid/util/ArraySet;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    const/4 v4, 0x1

    :goto_4
    const-string v32, "Can\'t delete row scheduled for delete"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 141
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->b:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    .line 142
    if-eqz v4, :cond_d

    .line 143
    move-object/from16 v0, v54

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 151
    :goto_5
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    if-nez v4, :cond_3

    .line 152
    :cond_8
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 100
    :cond_9
    :try_start_2
    const-string v4, "number"

    .line 101
    sget-object v32, Lamg;->d:Lamg;

    .line 102
    invoke-virtual/range {v32 .. v32}, Lamg;->toByteArray()[B

    move-result-object v32

    .line 103
    move-object/from16 v0, v54

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_2

    .line 153
    :catch_0
    move-exception v4

    :try_start_3
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 154
    :catchall_0
    move-exception v5

    move-object/from16 v56, v5

    move-object v5, v4

    move-object/from16 v4, v56

    :goto_6
    if-eqz v6, :cond_a

    if-eqz v5, :cond_12

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_a
    :goto_7
    throw v4

    .line 139
    :cond_b
    const/4 v4, 0x0

    goto :goto_3

    .line 140
    :cond_c
    const/4 v4, 0x0

    goto :goto_4

    .line 144
    :cond_d
    :try_start_5
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->b:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v54

    invoke-virtual {v4, v0, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 154
    :catchall_1
    move-exception v4

    goto :goto_6

    .line 147
    :cond_e
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->a:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    const/4 v4, 0x1

    :goto_8
    const-string v32, "Can\'t insert row already scheduled for insert"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 148
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->b:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    const/4 v4, 0x1

    :goto_9
    const-string v32, "Can\'t insert row scheduled for update"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 149
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->c:Landroid/util/ArraySet;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    const/4 v4, 0x1

    :goto_a
    const-string v32, "Can\'t insert row scheduled for delete"

    const/16 v33, 0x0

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-static {v4, v0, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 150
    move-object/from16 v0, p2

    iget-object v4, v0, Lbci;->a:Landroid/util/ArrayMap;

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v54

    invoke-virtual {v4, v0, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_5

    .line 147
    :cond_f
    const/4 v4, 0x0

    goto :goto_8

    .line 148
    :cond_10
    const/4 v4, 0x0

    goto :goto_9

    .line 149
    :cond_11
    const/4 v4, 0x0

    goto :goto_a

    .line 154
    :catch_1
    move-exception v6

    invoke-static {v5, v6}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    :cond_12
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_7
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 12
    invoke-static {}, Lbdf;->c()V

    .line 13
    invoke-static {p1}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "systemCallLogLastTimestampProcessed"

    .line 15
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 16
    :goto_0
    return v0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lbcm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 158
    invoke-static {p1}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 160
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "systemCallLogLastTimestampProcessed"

    iget-object v2, p0, Lbcm;->a:Ljava/lang/Long;

    .line 161
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 163
    :cond_0
    return-void
.end method
