.class public final Lfgs;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lfgs;->a:Ljava/lang/String;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lfgs;->b:Ljava/lang/String;

    .line 5
    iput-boolean v1, p0, Lfgs;->c:Z

    .line 6
    iput-boolean v1, p0, Lfgs;->d:Z

    .line 7
    iput-boolean v1, p0, Lfgs;->e:Z

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lfgs;->f:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lfgs;->g:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lfgs;->h:Ljava/lang/String;

    .line 11
    iput-boolean v1, p0, Lfgs;->i:Z

    .line 12
    iput v1, p0, Lfgs;->j:I

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lfgs;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lfgs;->cachedSize:I

    .line 15
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 39
    iget-object v1, p0, Lfgs;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfgs;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    const/4 v1, 0x1

    iget-object v2, p0, Lfgs;->a:Ljava/lang/String;

    .line 41
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42
    :cond_0
    iget-object v1, p0, Lfgs;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfgs;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-object v2, p0, Lfgs;->b:Ljava/lang/String;

    .line 44
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45
    :cond_1
    iget-boolean v1, p0, Lfgs;->c:Z

    if-eqz v1, :cond_2

    .line 46
    const/4 v1, 0x3

    iget-boolean v2, p0, Lfgs;->c:Z

    .line 48
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 49
    add-int/2addr v0, v1

    .line 50
    :cond_2
    iget-boolean v1, p0, Lfgs;->d:Z

    if-eqz v1, :cond_3

    .line 51
    const/4 v1, 0x4

    iget-boolean v2, p0, Lfgs;->d:Z

    .line 53
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 54
    add-int/2addr v0, v1

    .line 55
    :cond_3
    iget-boolean v1, p0, Lfgs;->e:Z

    if-eqz v1, :cond_4

    .line 56
    const/4 v1, 0x5

    iget-boolean v2, p0, Lfgs;->e:Z

    .line 58
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 59
    add-int/2addr v0, v1

    .line 60
    :cond_4
    iget-object v1, p0, Lfgs;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfgs;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 61
    const/4 v1, 0x6

    iget-object v2, p0, Lfgs;->f:Ljava/lang/String;

    .line 62
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_5
    iget-object v1, p0, Lfgs;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lfgs;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 64
    const/4 v1, 0x7

    iget-object v2, p0, Lfgs;->g:Ljava/lang/String;

    .line 65
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_6
    iget-object v1, p0, Lfgs;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lfgs;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 67
    const/16 v1, 0x8

    iget-object v2, p0, Lfgs;->h:Ljava/lang/String;

    .line 68
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_7
    iget-boolean v1, p0, Lfgs;->i:Z

    if-eqz v1, :cond_8

    .line 70
    const/16 v1, 0x9

    iget-boolean v2, p0, Lfgs;->i:Z

    .line 72
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 73
    add-int/2addr v0, v1

    .line 74
    :cond_8
    iget v1, p0, Lfgs;->j:I

    if-eqz v1, :cond_9

    .line 75
    const/16 v1, 0xa

    iget v2, p0, Lfgs;->j:I

    .line 76
    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 78
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 80
    sparse-switch v0, :sswitch_data_0

    .line 82
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 84
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgs;->a:Ljava/lang/String;

    goto :goto_0

    .line 86
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgs;->b:Ljava/lang/String;

    goto :goto_0

    .line 88
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfgs;->c:Z

    goto :goto_0

    .line 90
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfgs;->d:Z

    goto :goto_0

    .line 92
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfgs;->e:Z

    goto :goto_0

    .line 94
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgs;->f:Ljava/lang/String;

    goto :goto_0

    .line 96
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgs;->g:Ljava/lang/String;

    goto :goto_0

    .line 98
    :sswitch_8
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfgs;->h:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_9
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    iput-boolean v0, p0, Lfgs;->i:Z

    goto :goto_0

    .line 103
    :sswitch_a
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 104
    iput v0, p0, Lfgs;->j:I

    goto :goto_0

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lfgs;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfgs;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    const/4 v0, 0x1

    iget-object v1, p0, Lfgs;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 18
    :cond_0
    iget-object v0, p0, Lfgs;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfgs;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 19
    const/4 v0, 0x2

    iget-object v1, p0, Lfgs;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 20
    :cond_1
    iget-boolean v0, p0, Lfgs;->c:Z

    if-eqz v0, :cond_2

    .line 21
    const/4 v0, 0x3

    iget-boolean v1, p0, Lfgs;->c:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 22
    :cond_2
    iget-boolean v0, p0, Lfgs;->d:Z

    if-eqz v0, :cond_3

    .line 23
    const/4 v0, 0x4

    iget-boolean v1, p0, Lfgs;->d:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 24
    :cond_3
    iget-boolean v0, p0, Lfgs;->e:Z

    if-eqz v0, :cond_4

    .line 25
    const/4 v0, 0x5

    iget-boolean v1, p0, Lfgs;->e:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 26
    :cond_4
    iget-object v0, p0, Lfgs;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfgs;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 27
    const/4 v0, 0x6

    iget-object v1, p0, Lfgs;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 28
    :cond_5
    iget-object v0, p0, Lfgs;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfgs;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 29
    const/4 v0, 0x7

    iget-object v1, p0, Lfgs;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 30
    :cond_6
    iget-object v0, p0, Lfgs;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfgs;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 31
    const/16 v0, 0x8

    iget-object v1, p0, Lfgs;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 32
    :cond_7
    iget-boolean v0, p0, Lfgs;->i:Z

    if-eqz v0, :cond_8

    .line 33
    const/16 v0, 0x9

    iget-boolean v1, p0, Lfgs;->i:Z

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 34
    :cond_8
    iget v0, p0, Lfgs;->j:I

    if-eqz v0, :cond_9

    .line 35
    const/16 v0, 0xa

    iget v1, p0, Lfgs;->j:I

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 36
    :cond_9
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 37
    return-void
.end method
