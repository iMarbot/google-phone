.class final Lfoc;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field public mConnected:Z

.field public final synthetic this$0:Lfnv;


# direct methods
.method private constructor <init>(Lfnv;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lfoc;->this$0:Lfnv;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfoc;->mConnected:Z

    return-void
.end method

.method synthetic constructor <init>(Lfnv;Lfny;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lfoc;-><init>(Lfnv;)V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onReceive$0$CallManager$NetworkStateReceiver()V
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lfoc;->mConnected:Z

    if-nez v0, :cond_0

    .line 18
    const-string v0, "We still don\'t have a connection after 10 seconds. Terminate the call"

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lfoc;->this$0:Lfnv;

    invoke-static {v0}, Lfnv;->access$700(Lfnv;)V

    .line 20
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 3
    invoke-static {}, Lhcw;->b()V

    .line 4
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 5
    if-nez v0, :cond_1

    .line 16
    :cond_0
    :goto_0
    return-void

    .line 7
    :cond_1
    iget-object v0, p0, Lfoc;->this$0:Lfnv;

    invoke-virtual {v0}, Lfnv;->isPreparingOrInCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lfoc;->this$0:Lfnv;

    invoke-static {v0}, Lfnv;->access$600(Lfnv;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 10
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfoc;->mConnected:Z

    goto :goto_0

    .line 12
    :cond_2
    iget-boolean v0, p0, Lfoc;->mConnected:Z

    if-eqz v0, :cond_0

    .line 13
    const-string v0, "We lost our connection. Give it some time to recover then  terminate the call if it can\'t."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfoc;->mConnected:Z

    .line 15
    new-instance v0, Lfod;

    invoke-direct {v0, p0}, Lfod;-><init>(Lfoc;)V

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
