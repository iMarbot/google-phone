.class public final Lgqi;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgqi;


# instance fields
.field public appPushNotification:Lgpu;

.field public captionsUpdate:Lgpr;

.field public commonAnnouncementsUpdate:Lgpt;

.field public hangoutBroadcastsUpdate:Lgpp;

.field public hangoutParticipantsUpdate:Lgpy;

.field public hangoutsUpdate:Lgqa;

.field public mediaSessionsUpdate:Lgqe;

.field public mediaSourcesUpdate:Lgqf;

.field public mediaStreamsUpdate:Lgqh;

.field public meetingsUpdate:Lhhg;

.field public pushEventsUpdate:Lgpz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgqi;->clear()Lgqi;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgqi;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgqi;->_emptyArray:[Lgqi;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgqi;->_emptyArray:[Lgqi;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgqi;

    sput-object v0, Lgqi;->_emptyArray:[Lgqi;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgqi;->_emptyArray:[Lgqi;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgqi;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    invoke-virtual {v0, p0}, Lgqi;->mergeFrom(Lhfp;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgqi;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lgqi;

    invoke-direct {v0}, Lgqi;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgqi;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgqi;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    .line 11
    iput-object v0, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    .line 12
    iput-object v0, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    .line 13
    iput-object v0, p0, Lgqi;->appPushNotification:Lgpu;

    .line 14
    iput-object v0, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    .line 15
    iput-object v0, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    .line 16
    iput-object v0, p0, Lgqi;->hangoutsUpdate:Lgqa;

    .line 17
    iput-object v0, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    .line 18
    iput-object v0, p0, Lgqi;->pushEventsUpdate:Lgpz;

    .line 19
    iput-object v0, p0, Lgqi;->captionsUpdate:Lgpr;

    .line 20
    iput-object v0, p0, Lgqi;->meetingsUpdate:Lhhg;

    .line 21
    iput-object v0, p0, Lgqi;->unknownFieldData:Lhfv;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lgqi;->cachedSize:I

    .line 23
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 48
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 49
    iget-object v1, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    if-eqz v1, :cond_0

    .line 50
    const/4 v1, 0x1

    iget-object v2, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    .line 51
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_0
    iget-object v1, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    if-eqz v1, :cond_1

    .line 53
    const/4 v1, 0x2

    iget-object v2, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    .line 54
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_1
    iget-object v1, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    if-eqz v1, :cond_2

    .line 56
    const/4 v1, 0x3

    iget-object v2, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    .line 57
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58
    :cond_2
    iget-object v1, p0, Lgqi;->appPushNotification:Lgpu;

    if-eqz v1, :cond_3

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, Lgqi;->appPushNotification:Lgpu;

    .line 60
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_3
    iget-object v1, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    if-eqz v1, :cond_4

    .line 62
    const/4 v1, 0x5

    iget-object v2, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    .line 63
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_4
    iget-object v1, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    if-eqz v1, :cond_5

    .line 65
    const/4 v1, 0x7

    iget-object v2, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    .line 66
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_5
    iget-object v1, p0, Lgqi;->hangoutsUpdate:Lgqa;

    if-eqz v1, :cond_6

    .line 68
    const/16 v1, 0x8

    iget-object v2, p0, Lgqi;->hangoutsUpdate:Lgqa;

    .line 69
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_6
    iget-object v1, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    if-eqz v1, :cond_7

    .line 71
    const/16 v1, 0x9

    iget-object v2, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    .line 72
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_7
    iget-object v1, p0, Lgqi;->pushEventsUpdate:Lgpz;

    if-eqz v1, :cond_8

    .line 74
    const/16 v1, 0xa

    iget-object v2, p0, Lgqi;->pushEventsUpdate:Lgpz;

    .line 75
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_8
    iget-object v1, p0, Lgqi;->captionsUpdate:Lgpr;

    if-eqz v1, :cond_9

    .line 77
    const/16 v1, 0xb

    iget-object v2, p0, Lgqi;->captionsUpdate:Lgpr;

    .line 78
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_9
    iget-object v1, p0, Lgqi;->meetingsUpdate:Lhhg;

    if-eqz v1, :cond_a

    .line 80
    const/16 v1, 0xd

    iget-object v2, p0, Lgqi;->meetingsUpdate:Lhhg;

    .line 81
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_a
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgqi;
    .locals 1

    .prologue
    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 84
    sparse-switch v0, :sswitch_data_0

    .line 86
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    iget-object v0, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    if-nez v0, :cond_1

    .line 89
    new-instance v0, Lgpy;

    invoke-direct {v0}, Lgpy;-><init>()V

    iput-object v0, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    .line 90
    :cond_1
    iget-object v0, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 92
    :sswitch_2
    iget-object v0, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    if-nez v0, :cond_2

    .line 93
    new-instance v0, Lgqe;

    invoke-direct {v0}, Lgqe;-><init>()V

    iput-object v0, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    .line 94
    :cond_2
    iget-object v0, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 96
    :sswitch_3
    iget-object v0, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    if-nez v0, :cond_3

    .line 97
    new-instance v0, Lgqh;

    invoke-direct {v0}, Lgqh;-><init>()V

    iput-object v0, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    .line 98
    :cond_3
    iget-object v0, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 100
    :sswitch_4
    iget-object v0, p0, Lgqi;->appPushNotification:Lgpu;

    if-nez v0, :cond_4

    .line 101
    new-instance v0, Lgpu;

    invoke-direct {v0}, Lgpu;-><init>()V

    iput-object v0, p0, Lgqi;->appPushNotification:Lgpu;

    .line 102
    :cond_4
    iget-object v0, p0, Lgqi;->appPushNotification:Lgpu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 104
    :sswitch_5
    iget-object v0, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    if-nez v0, :cond_5

    .line 105
    new-instance v0, Lgqf;

    invoke-direct {v0}, Lgqf;-><init>()V

    iput-object v0, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    .line 106
    :cond_5
    iget-object v0, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 108
    :sswitch_6
    iget-object v0, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    if-nez v0, :cond_6

    .line 109
    new-instance v0, Lgpp;

    invoke-direct {v0}, Lgpp;-><init>()V

    iput-object v0, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    .line 110
    :cond_6
    iget-object v0, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 112
    :sswitch_7
    iget-object v0, p0, Lgqi;->hangoutsUpdate:Lgqa;

    if-nez v0, :cond_7

    .line 113
    new-instance v0, Lgqa;

    invoke-direct {v0}, Lgqa;-><init>()V

    iput-object v0, p0, Lgqi;->hangoutsUpdate:Lgqa;

    .line 114
    :cond_7
    iget-object v0, p0, Lgqi;->hangoutsUpdate:Lgqa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 116
    :sswitch_8
    iget-object v0, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    if-nez v0, :cond_8

    .line 117
    new-instance v0, Lgpt;

    invoke-direct {v0}, Lgpt;-><init>()V

    iput-object v0, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    .line 118
    :cond_8
    iget-object v0, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 120
    :sswitch_9
    iget-object v0, p0, Lgqi;->pushEventsUpdate:Lgpz;

    if-nez v0, :cond_9

    .line 121
    new-instance v0, Lgpz;

    invoke-direct {v0}, Lgpz;-><init>()V

    iput-object v0, p0, Lgqi;->pushEventsUpdate:Lgpz;

    .line 122
    :cond_9
    iget-object v0, p0, Lgqi;->pushEventsUpdate:Lgpz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 124
    :sswitch_a
    iget-object v0, p0, Lgqi;->captionsUpdate:Lgpr;

    if-nez v0, :cond_a

    .line 125
    new-instance v0, Lgpr;

    invoke-direct {v0}, Lgpr;-><init>()V

    iput-object v0, p0, Lgqi;->captionsUpdate:Lgpr;

    .line 126
    :cond_a
    iget-object v0, p0, Lgqi;->captionsUpdate:Lgpr;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 128
    :sswitch_b
    iget-object v0, p0, Lgqi;->meetingsUpdate:Lhhg;

    if-nez v0, :cond_b

    .line 129
    new-instance v0, Lhhg;

    invoke-direct {v0}, Lhhg;-><init>()V

    iput-object v0, p0, Lgqi;->meetingsUpdate:Lhhg;

    .line 130
    :cond_b
    iget-object v0, p0, Lgqi;->meetingsUpdate:Lhhg;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lgqi;->mergeFrom(Lhfp;)Lgqi;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    iget-object v1, p0, Lgqi;->hangoutParticipantsUpdate:Lgpy;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_0
    iget-object v0, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Lgqi;->mediaSessionsUpdate:Lgqe;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 28
    :cond_1
    iget-object v0, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Lgqi;->mediaStreamsUpdate:Lgqh;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_2
    iget-object v0, p0, Lgqi;->appPushNotification:Lgpu;

    if-eqz v0, :cond_3

    .line 31
    const/4 v0, 0x4

    iget-object v1, p0, Lgqi;->appPushNotification:Lgpu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_3
    iget-object v0, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    if-eqz v0, :cond_4

    .line 33
    const/4 v0, 0x5

    iget-object v1, p0, Lgqi;->mediaSourcesUpdate:Lgqf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 34
    :cond_4
    iget-object v0, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    if-eqz v0, :cond_5

    .line 35
    const/4 v0, 0x7

    iget-object v1, p0, Lgqi;->hangoutBroadcastsUpdate:Lgpp;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 36
    :cond_5
    iget-object v0, p0, Lgqi;->hangoutsUpdate:Lgqa;

    if-eqz v0, :cond_6

    .line 37
    const/16 v0, 0x8

    iget-object v1, p0, Lgqi;->hangoutsUpdate:Lgqa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 38
    :cond_6
    iget-object v0, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    if-eqz v0, :cond_7

    .line 39
    const/16 v0, 0x9

    iget-object v1, p0, Lgqi;->commonAnnouncementsUpdate:Lgpt;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 40
    :cond_7
    iget-object v0, p0, Lgqi;->pushEventsUpdate:Lgpz;

    if-eqz v0, :cond_8

    .line 41
    const/16 v0, 0xa

    iget-object v1, p0, Lgqi;->pushEventsUpdate:Lgpz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 42
    :cond_8
    iget-object v0, p0, Lgqi;->captionsUpdate:Lgpr;

    if-eqz v0, :cond_9

    .line 43
    const/16 v0, 0xb

    iget-object v1, p0, Lgqi;->captionsUpdate:Lgpr;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 44
    :cond_9
    iget-object v0, p0, Lgqi;->meetingsUpdate:Lhhg;

    if-eqz v0, :cond_a

    .line 45
    const/16 v0, 0xd

    iget-object v1, p0, Lgqi;->meetingsUpdate:Lhhg;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 46
    :cond_a
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 47
    return-void
.end method
