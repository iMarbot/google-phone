.class public final Lbvu;
.super Lcck;
.source "PG"

# interfaces
.implements Lbwq;


# instance fields
.field private b:Lcdc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcck;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 23
    iget-object v0, p0, Lbvu;->b:Lcdc;

    if-eqz v0, :cond_0

    .line 24
    const-string v0, "stopping remote tone"

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lcdr;->a()Lcdr;

    iget-object v0, p0, Lbvu;->b:Lcdc;

    .line 26
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 28
    invoke-static {v0}, Lcdr;->a(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    invoke-virtual {v1}, Landroid/telecom/Call;->stopDtmfTone()V

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    const-string v1, "TelecomAdapter.stopDtmfTone"

    const-string v2, "call not in call list "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(C)V
    .locals 4

    .prologue
    .line 5
    const/16 v0, 0x15

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Processing dtmf key "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->is12Key(C)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbvu;->b:Lcdc;

    if-eqz v0, :cond_3

    .line 7
    const/16 v0, 0x2e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "updating display and sending dtmf tone for \'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcck;->a:Lccl;

    .line 10
    check-cast v0, Lbvv;

    .line 11
    if-eqz v0, :cond_0

    .line 12
    invoke-interface {v0, p1}, Lbvv;->a(C)V

    .line 13
    :cond_0
    invoke-static {}, Lcdr;->a()Lcdr;

    iget-object v0, p0, Lbvu;->b:Lcdc;

    .line 14
    iget-object v0, v0, Lcdc;->e:Ljava/lang/String;

    .line 16
    invoke-static {v0}, Lcdr;->a(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v1

    .line 17
    if-eqz v1, :cond_1

    .line 18
    invoke-virtual {v1, p1}, Landroid/telecom/Call;->playDtmfTone(C)V

    .line 22
    :goto_0
    return-void

    .line 19
    :cond_1
    const-string v1, "TelecomAdapter.playDtmfTone"

    const-string v2, "call not in call list "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 21
    :cond_3
    const/16 v0, 0x1d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "ignoring dtmf request for \'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 3

    .prologue
    .line 2
    invoke-virtual {p3}, Lcct;->a()Lcdc;

    move-result-object v0

    iput-object v0, p0, Lbvu;->b:Lcdc;

    .line 3
    iget-object v0, p0, Lbvu;->b:Lcdc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialpadPresenter mCall = "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    return-void
.end method

.method public final synthetic a(Lccl;)V
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lbvv;

    .line 34
    invoke-super {p0, p1}, Lcck;->a(Lccl;)V

    .line 35
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 36
    return-void
.end method

.method public final synthetic b(Lccl;)V
    .locals 1

    .prologue
    .line 37
    check-cast p1, Lbvv;

    .line 38
    invoke-super {p0, p1}, Lcck;->b(Lccl;)V

    .line 39
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 41
    sget-object v0, Lcct;->a:Lcct;

    .line 42
    invoke-virtual {v0}, Lcct;->a()Lcdc;

    move-result-object v0

    iput-object v0, p0, Lbvu;->b:Lcdc;

    .line 43
    return-void
.end method
