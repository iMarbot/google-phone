.class public final Lhxw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhxw;

.field public static final b:Lhxw;

.field public static final c:Lhxw;

.field public static final d:Lhxw;


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Lhxw;

    const-string v1, "Body part ended prematurely. Boundary detected in header or EOF reached."

    invoke-direct {v0, v1}, Lhxw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhxw;->a:Lhxw;

    .line 13
    new-instance v0, Lhxw;

    const-string v1, "Unexpected end of headers detected. Higher level boundary detected or EOF reached."

    invoke-direct {v0, v1}, Lhxw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhxw;->b:Lhxw;

    .line 14
    new-instance v0, Lhxw;

    const-string v1, "Invalid header encountered"

    invoke-direct {v0, v1}, Lhxw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhxw;->c:Lhxw;

    .line 15
    new-instance v0, Lhxw;

    const-string v1, "Obsolete header encountered"

    invoke-direct {v0, v1}, Lhxw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhxw;->d:Lhxw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lhxw;->e:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5
    if-nez p1, :cond_1

    .line 10
    :cond_0
    :goto_0
    return v0

    .line 6
    :cond_1
    if-ne p0, p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 7
    :cond_2
    instance-of v1, p1, Lhxw;

    if-eqz v1, :cond_0

    .line 8
    check-cast p1, Lhxw;

    .line 9
    iget-object v0, p0, Lhxw;->e:Ljava/lang/String;

    iget-object v1, p1, Lhxw;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lhxw;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lhxw;->e:Ljava/lang/String;

    return-object v0
.end method
