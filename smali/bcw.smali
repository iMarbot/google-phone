.class public final Lbcw;
.super Lip;
.source "PG"

# interfaces
.implements Lbbq;
.implements Lky;


# instance fields
.field private W:Landroid/support/v7/widget/RecyclerView;

.field private a:Lbdy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    const-string v0, "NewCallLogFragment.NewCallLogFragment"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    const-string v0, "NewCallLogFragment.onCreateView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 34
    const v0, 0x7f04008c

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    const v0, 0x7f0e021d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lbcw;->W:Landroid/support/v7/widget/RecyclerView;

    .line 36
    invoke-virtual {p0}, Lbcw;->n()Lkx;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, p0}, Lkx;->b(ILandroid/os/Bundle;Lky;)Lly;

    .line 37
    return-object v1
.end method

.method public final a()Lly;
    .locals 2

    .prologue
    .line 41
    const-string v0, "NewCallLogFragment.onCreateLoader"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 42
    new-instance v0, Lbct;

    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbct;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final synthetic a(Lly;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 46
    check-cast p2, Landroid/database/Cursor;

    .line 47
    const-string v0, "NewCallLogFragment.onLoadFinished"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 48
    if-nez p2, :cond_0

    .line 49
    const-string v0, "NewCallLogFragment.onLoadFinished"

    const-string v1, "null cursor"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lbcw;->W:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Labq;

    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Labq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 52
    iget-object v0, p0, Lbcw;->W:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lbcv;

    sget-object v2, Lbcx;->a:Lbsr;

    invoke-direct {v1, p2, v2}, Lbcv;-><init>(Landroid/database/Cursor;Lbsr;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 4
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 5
    const-string v0, "NewCallLogFragment.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbo;->a(Landroid/content/Context;)Lbbo;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lbbo;->a()Lbch;

    move-result-object v1

    .line 8
    invoke-virtual {v1, p0}, Lbch;->a(Lbbq;)V

    .line 10
    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    .line 12
    invoke-virtual {p0}, Lbcw;->h()Lit;

    move-result-object v2

    invoke-virtual {v2}, Lit;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "NewCallLogFragment.refreshAnnotatedCallLog"

    .line 13
    invoke-virtual {v0}, Lbbo;->b()Lbbt;

    move-result-object v0

    .line 14
    invoke-virtual {v1, v2, v3, v0}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lbcw;->a:Lbdy;

    .line 16
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 38
    const-string v0, "NewCallLogFragment.invalidateUi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lbcw;->a:Lbdy;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-interface {v0, v1, v2, v3}, Lbdy;->a(Ljava/lang/Object;J)V

    .line 40
    return-void
.end method

.method public final k_()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "NewCallLogFragment.onLoaderReset"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lbcw;->W:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 45
    return-void
.end method

.method public final o_()V
    .locals 1

    .prologue
    .line 17
    invoke-super {p0}, Lip;->o_()V

    .line 18
    const-string v0, "NewCallLogFragment.onStart"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 20
    invoke-super {p0}, Lip;->r()V

    .line 21
    const-string v0, "NewCallLogFragment.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbo;->a(Landroid/content/Context;)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->a()Lbch;

    move-result-object v0

    .line 23
    invoke-virtual {v0, p0}, Lbch;->a(Lbbq;)V

    .line 25
    const-string v0, "NewCallLogFragment.checkAnnotatedCallLogDirtyAndRefreshIfNecessary"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lbcw;->a:Lbdy;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-interface {v0, v1, v2, v3}, Lbdy;->a(Ljava/lang/Object;J)V

    .line 27
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lip;->s()V

    .line 29
    const-string v0, "NewCallLogFragment.onPause"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lbcw;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbo;->a(Landroid/content/Context;)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->a()Lbch;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lbch;->b()V

    .line 32
    return-void
.end method
