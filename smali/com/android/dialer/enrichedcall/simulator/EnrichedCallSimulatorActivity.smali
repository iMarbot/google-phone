.class public Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;
.super Luh;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbjj;


# instance fields
.field private f:Landroid/widget/Button;

.field private g:Lbjr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method

.method private final g()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g:Lbjr;

    .line 37
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v1

    invoke-virtual {v1}, Lbjd;->a()Lbjf;

    move-result-object v1

    .line 38
    invoke-interface {v1}, Lbjf;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbjr;->a(Ljava/util/List;)V

    .line 39
    iget-object v0, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g:Lbjr;

    .line 40
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 41
    return-void
.end method


# virtual methods
.method public final m_()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "EnrichedCallSimulatorActivity.onEnrichedCallStateChanged"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g()V

    .line 31
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 33
    const-string v0, "EnrichedCallSimulatorActivity.onClick"

    const-string v1, "refreshing sessions"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    invoke-direct {p0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g()V

    .line 35
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2
    const-string v0, "EnrichedCallSimulatorActivity.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 4
    const v0, 0x7f04005f

    invoke-virtual {p0, v0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->setContentView(I)V

    .line 5
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 6
    const v1, 0x7f11016c

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 7
    const v0, 0x7f0e01b2

    invoke-virtual {p0, v0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->f:Landroid/widget/Button;

    .line 8
    iget-object v0, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    const v0, 0x7f0e01b1

    invoke-virtual {p0, v0}, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 10
    new-instance v1, Labq;

    invoke-direct {v1, p0}, Labq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 11
    new-instance v1, Lbjr;

    invoke-direct {v1}, Lbjr;-><init>()V

    iput-object v1, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g:Lbjr;

    .line 12
    iget-object v1, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g:Lbjr;

    .line 13
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v2

    invoke-virtual {v2}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 14
    invoke-interface {v2}, Lbjf;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbjr;->a(Ljava/util/List;)V

    .line 15
    iget-object v1, p0, Lcom/android/dialer/enrichedcall/simulator/EnrichedCallSimulatorActivity;->g:Lbjr;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 16
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "EnrichedCallSimulatorActivity.onPause"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 24
    invoke-super {p0}, Luh;->onPause()V

    .line 26
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 27
    invoke-interface {v0, p0}, Lbjf;->b(Lbjj;)V

    .line 28
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "EnrichedCallSimulatorActivity.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 18
    invoke-super {p0}, Luh;->onResume()V

    .line 20
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 21
    invoke-interface {v0, p0}, Lbjf;->a(Lbjj;)V

    .line 22
    return-void
.end method
