.class public final Lcom/android/dialer/shortcuts/PeriodicJobService;
.super Landroid/app/job/JobService;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x19
.end annotation


# static fields
.field private static a:J


# instance fields
.field private b:Lbpn;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x18

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/dialer/shortcuts/PeriodicJobService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v4, 0x1

    .line 2
    invoke-static {}, Lbdf;->b()V

    .line 3
    const-string v0, "PeriodicJobService.schedulePeriodicJob"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 6
    invoke-virtual {v0, v5}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 7
    const-string v0, "PeriodicJobService.schedulePeriodicJob"

    const-string v1, "job already scheduled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/android/dialer/shortcuts/PeriodicJobService;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v5, v2}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    sget-wide v2, Lcom/android/dialer/shortcuts/PeriodicJobService;->a:J

    .line 10
    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 11
    invoke-virtual {v1, v4}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 12
    invoke-virtual {v1, v4}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 13
    invoke-virtual {v1, v4}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 17
    invoke-static {}, Lbdf;->b()V

    .line 18
    const-string v0, "PeriodicJobService.cancelJob"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 19
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 20
    return-void
.end method


# virtual methods
.method public final onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21
    invoke-static {}, Lbdf;->b()V

    .line 22
    const-string v2, "PeriodicJobService.onStartJob"

    invoke-static {v2}, Lapw;->b(Ljava/lang/String;)V

    .line 23
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-lt v2, v3, :cond_0

    .line 24
    new-instance v2, Lbpn;

    invoke-direct {v2, p0}, Lbpn;-><init>(Landroid/app/job/JobService;)V

    iput-object v2, p0, Lcom/android/dialer/shortcuts/PeriodicJobService;->b:Lbpn;

    new-array v3, v0, [Landroid/app/job/JobParameters;

    aput-object p1, v3, v1

    invoke-virtual {v2, v3}, Lbpn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 28
    :goto_0
    return v0

    .line 25
    :cond_0
    const-string v0, "PeriodicJobService.onStartJob"

    const-string v2, "not running on NMR1, cancelling job"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    invoke-static {p0}, Lcom/android/dialer/shortcuts/PeriodicJobService;->b(Landroid/content/Context;)V

    move v0, v1

    .line 27
    goto :goto_0
.end method

.method public final onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-static {}, Lbdf;->b()V

    .line 30
    const-string v0, "PeriodicJobService.onStopJob"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/android/dialer/shortcuts/PeriodicJobService;->b:Lbpn;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/android/dialer/shortcuts/PeriodicJobService;->b:Lbpn;

    invoke-virtual {v0, v1}, Lbpn;->cancel(Z)Z

    .line 33
    :cond_0
    return v1
.end method
