.class public Lcom/android/dialer/shortcuts/CallContactActivity;
.super Lbsz;
.source "PG"

# interfaces
.implements Lbkd$a;
.implements Lbkd$b;
.implements Lif;


# instance fields
.field private f:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbsz;-><init>()V

    return-void
.end method

.method private final f()V
    .locals 3

    .prologue
    .line 14
    sget-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {v0, v1}, Lhbr$a;->c(Z)Lhbr$a;

    move-result-object v0

    sget-object v1, Lbbf$a;->n:Lbbf$a;

    .line 17
    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbbf$a;)Lhbr$a;

    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbbj;

    .line 19
    iget-object v1, p0, Lcom/android/dialer/shortcuts/CallContactActivity;->f:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {p0, v1, v2, v0}, Lbkd;->a(Lbsz;Landroid/net/Uri;ZLbbj;)V

    .line 20
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->finish()V

    .line 22
    return-void
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 23
    packed-switch p1, :pswitch_data_0

    .line 26
    :goto_0
    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->finish()V

    .line 27
    return-void

    .line 24
    :pswitch_0
    const v0, 0x7f110138

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    invoke-super {p0, p1}, Lbsz;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const-string v0, "com.android.dialer.shortcuts.CALL_CONTACT"

    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    invoke-static {p0}, Lbib;->J(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5
    const-string v0, "CallContactActivity.onCreate"

    const-string v1, "shortcut clicked"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/shortcuts/CallContactActivity;->f:Landroid/net/Uri;

    .line 7
    invoke-direct {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->f()V

    .line 10
    :cond_0
    :goto_0
    return-void

    .line 8
    :cond_1
    const-string v0, "CallContactActivity.onCreate"

    const-string v1, "dynamic shortcuts disabled"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lbsz;->onDestroy()V

    .line 12
    const-string v0, "CallContactActivity.onDestroy"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 13
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 36
    packed-switch p1, :pswitch_data_0

    .line 43
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported request code: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :pswitch_0
    array-length v0, p3

    if-lez v0, :cond_0

    aget v0, p3, v1

    if-nez v0, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->f()V

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    const v0, 0x7f11013a

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 41
    invoke-virtual {p0}, Lcom/android/dialer/shortcuts/CallContactActivity;->finish()V

    goto :goto_0

    .line 36
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0, p1}, Lbsz;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 32
    if-nez p1, :cond_0

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v0, "uri_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/dialer/shortcuts/CallContactActivity;->f:Landroid/net/Uri;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0, p1}, Lbsz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 29
    const-string v0, "uri_key"

    iget-object v1, p0, Lcom/android/dialer/shortcuts/CallContactActivity;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 30
    return-void
.end method
