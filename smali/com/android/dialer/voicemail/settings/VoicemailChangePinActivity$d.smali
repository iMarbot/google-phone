.class public enum Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "d"
.end annotation


# static fields
.field public static final enum a:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field public static final enum b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field public static final enum c:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field public static final enum d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field public static final enum e:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field private static synthetic f:[Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    const-string v1, "Initial"

    invoke-direct {v0, v1, v2}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->a:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 10
    new-instance v0, Lbtr;

    const-string v1, "EnterOldPin"

    invoke-direct {v0, v1, v3}, Lbtr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 11
    new-instance v0, Lbts;

    const-string v1, "VerifyOldPin"

    invoke-direct {v0, v1, v4}, Lbts;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->c:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 12
    new-instance v0, Lbtu;

    const-string v1, "EnterNewPin"

    invoke-direct {v0, v1, v5}, Lbtu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 13
    new-instance v0, Lbtv;

    const-string v1, "ConfirmNewPin"

    invoke-direct {v0, v1, v6}, Lbtv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->e:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 14
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    sget-object v1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->a:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->c:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->e:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->f:[Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->f:[Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {v0}, [Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;I)V
    .locals 0

    .prologue
    .line 5
    return-void
.end method

.method public b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public c(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 0

    .prologue
    .line 6
    return-void
.end method

.method public d(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 0

    .prologue
    .line 7
    return-void
.end method
