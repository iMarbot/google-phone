.class public Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$c;,
        Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$a;,
        Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;,
        Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;
    }
.end annotation


# instance fields
.field public a:Lcll;

.field public b:I

.field public c:I

.field public d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Landroid/widget/TextView;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/EditText;

.field public k:Landroid/widget/Button;

.field private l:Landroid/telecom/PhoneAccountHandle;

.field private m:Lbdy;

.field private n:Landroid/app/ProgressDialog;

.field private o:Landroid/widget/Button;

.field private p:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->a:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 3
    new-instance v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$a;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 4
    invoke-direct {v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$a;-><init>(Ljava/lang/ref/WeakReference;)V

    .line 5
    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->p:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {v0, p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->c(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 74
    invoke-interface {v0, p0, p1}, Lcln;->g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcll;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 75
    :cond_0
    const/4 v0, 0x0

    .line 76
    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 103
    const-string v0, "VmChangePinActivity"

    const/16 v1, 0x1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Change PIN result: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 109
    :goto_0
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->p:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 110
    return-void

    .line 108
    :cond_0
    const-string v0, "VmChangePinActivity"

    const-string v1, "Dialog not visible, not dismissing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 78
    iput-object p1, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 79
    if-eq v0, p1, :cond_0

    .line 80
    invoke-virtual {v0, p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 81
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {v0, p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {v0, p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 84
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 3

    .prologue
    .line 85
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    .line 87
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 90
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    .line 92
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 93
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    const v1, 0x7f11033f

    invoke-virtual {p0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->n:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 95
    new-instance v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;

    .line 96
    invoke-direct {v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    iput-object v1, v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;->a:Lcll;

    .line 99
    iput-object p1, v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;->b:Ljava/lang/String;

    .line 100
    iput-object p2, v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$b;->c:Ljava/lang/String;

    .line 101
    iget-object v1, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->m:Lbdy;

    invoke-interface {v1, v0}, Lbdy;->a(Ljava/lang/Object;)V

    .line 102
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {v0, p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 69
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e0282

    if-ne v0, v1, :cond_1

    .line 54
    invoke-direct {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a()V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e027d

    if-ne v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 6
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 8
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "phone_account_handle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->l:Landroid/telecom/PhoneAccountHandle;

    .line 10
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 12
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->l:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v0, v1, v2}, Lcln;->g(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcll;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    .line 13
    const v0, 0x7f0400c7

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->setContentView(I)V

    .line 14
    const v0, 0x7f1100c4

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->setTitle(I)V

    .line 16
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    invoke-virtual {v0}, Lcll;->b()Lclm;

    move-result-object v0

    .line 17
    iget v1, v0, Lclm;->a:I

    iput v1, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->b:I

    .line 18
    iget v0, v0, Lclm;->b:I

    iput v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    .line 19
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 20
    const v0, 0x7f0e027d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->o:Landroid/widget/Button;

    .line 21
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    const v0, 0x7f0e0282

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    .line 23
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    const v0, 0x7f0e0281

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    .line 25
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 26
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 27
    iget v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    iget v5, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->c:I

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 29
    :cond_0
    const v0, 0x7f0e027e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->g:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0e027f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->h:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f0e0280

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 33
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "changePin"

    new-instance v3, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$c;

    .line 36
    invoke-direct {v3}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$c;-><init>()V

    .line 37
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lbtp;

    invoke-direct {v1, p0}, Lbtp;-><init>(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 38
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Lbtq;

    invoke-direct {v1, p0}, Lbtq;-><init>(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V

    .line 39
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->m:Lbdy;

    .line 41
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->l:Landroid/telecom/PhoneAccountHandle;

    invoke-static {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    invoke-virtual {v0}, Lcll;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    .line 43
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->c:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    .line 45
    :goto_0
    return-void

    .line 44
    :cond_1
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 62
    iget-object v1, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    :goto_0
    return v0

    .line 64
    :cond_0
    if-eqz p2, :cond_1

    const/4 v1, 0x6

    if-eq p2, v1, :cond_1

    const/4 v1, 0x5

    if-ne p2, v1, :cond_2

    .line 65
    :cond_1
    invoke-direct {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a()V

    goto :goto_0

    .line 67
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 58
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->onBackPressed()V

    .line 60
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 47
    iget-object v0, p0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    .line 48
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
