.class public Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance v0, Lbtg;

    invoke-direct {v0}, Lbtg;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->d:Landroid/view/View$OnClickListener;

    .line 3
    new-instance v0, Lbth;

    invoke-direct {v0}, Lbth;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->e:Landroid/view/View$OnClickListener;

    .line 4
    new-instance v0, Lbti;

    invoke-direct {v0}, Lbti;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->f:Landroid/view/View$OnClickListener;

    .line 5
    const-string v0, "NewVoicemailMediaPlayer"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 6
    const-string v0, "layout_inflater"

    .line 7
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 8
    const v1, 0x7f040090

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 9
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 10
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 11
    const-string v0, "NewVoicemailMediaPlayer.onFinishInflate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 13
    const v0, 0x7f0e0222

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->a:Landroid/widget/Button;

    .line 14
    const v0, 0x7f0e0223

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->b:Landroid/widget/Button;

    .line 15
    const v0, 0x7f0e0199

    invoke-virtual {p0, v0}, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->c:Landroid/widget/Button;

    .line 17
    iget-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->a:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    iget-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->b:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    iget-object v0, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->c:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/dialer/voicemail/listui/NewVoicemailMediaPlayer;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    return-void
.end method
