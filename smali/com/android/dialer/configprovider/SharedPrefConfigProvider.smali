.class public Lcom/android/dialer/configprovider/SharedPrefConfigProvider;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbew;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/configprovider/SharedPrefConfigProvider$Service;
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/content/res/Resources;

.field public final c:Laqc;

.field public final d:Ljava/util/Calendar;

.field public e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Laqc;)V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e:Ljava/util/ArrayList;

    .line 6
    iput-object p1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    .line 7
    iput-object p2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    .line 8
    iput-object p3, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->c:Laqc;

    .line 9
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d:Ljava/util/Calendar;

    .line 10
    return-void
.end method

.method public static c(Lbdd;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 105
    iget-object v2, p0, Lbdd;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v2, p0, Lbdd;->p:Lbko$a;

    sget-object v3, Lbko$a;->g:Lbko$a;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 108
    goto :goto_0

    .line 109
    :cond_2
    iget-object v2, p0, Lbdd;->j:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 111
    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 1

    .prologue
    .line 2
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public a(Lbdd;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 83
    iget-object v0, p1, Lbdd;->g:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 84
    invoke-virtual {p0, p1}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 85
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Lbss;->a(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 121
    invoke-static {p1, p2}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v1, 0x7f110345

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    const v2, 0x10010

    .line 124
    invoke-virtual {p0, p1, p2}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    :goto_1
    or-int/2addr v0, v2

    .line 125
    invoke-static {v1, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public a(Lapt;Lbdd;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11
    iget-object v0, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a()V

    .line 12
    iget-object v0, p2, Lbdd;->g:[I

    array-length v4, v0

    move v3, v2

    move v0, v2

    .line 14
    :goto_0
    if-ge v3, v4, :cond_2

    if-ge v3, v7, :cond_2

    .line 15
    iget-object v5, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget-object v6, p2, Lbdd;->g:[I

    aget v6, v6, v3

    invoke-virtual {v5, v6}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(I)V

    .line 16
    if-nez v3, :cond_0

    .line 17
    iget-object v0, p2, Lbdd;->g:[I

    aget v0, v0, v3

    if-ne v0, v8, :cond_1

    move v0, v1

    .line 18
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 17
    goto :goto_1

    .line 19
    :cond_2
    iget-object v5, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget v3, p2, Lbdd;->r:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    move v3, v1

    :goto_2
    invoke-virtual {v5, v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(Z)V

    .line 20
    iget-object v5, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget v3, p2, Lbdd;->r:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v8, :cond_6

    move v3, v1

    :goto_3
    invoke-virtual {v5, v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b(Z)V

    .line 21
    iget-object v3, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget-object v5, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    iget v6, p2, Lbdd;->r:I

    .line 22
    invoke-static {v5, v6}, Lbib;->a(Landroid/content/Context;I)Z

    move-result v5

    .line 23
    invoke-virtual {v3, v5}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c(Z)V

    .line 24
    iget-object v3, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->requestLayout()V

    .line 25
    iget-object v3, p1, Lapt;->b:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v3, v2}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->setVisibility(I)V

    .line 26
    if-le v4, v7, :cond_7

    .line 27
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 29
    :goto_4
    invoke-virtual {p0, p1, v3, p2}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a(Lapt;Ljava/lang/Integer;Lbdd;)V

    .line 30
    iget-object v3, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->c:Laqc;

    iget-object v4, p2, Lbdd;->q:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v3, v4}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v3

    .line 31
    iget-object v4, p2, Lbdd;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 32
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 33
    iget-object v4, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v5, 0x7f1100a3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v2

    iget-object v3, p2, Lbdd;->c:Ljava/lang/String;

    aput-object v3, v6, v1

    .line 34
    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 36
    :goto_5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 37
    iget-object v3, p1, Lapt;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 38
    iget-object v3, p1, Lapt;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->c:Laqc;

    iget-object v3, p2, Lbdd;->q:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v3}, Laqc;->b(Landroid/telecom/PhoneAccountHandle;)I

    move-result v1

    .line 40
    if-nez v1, :cond_9

    .line 41
    iget-object v1, p1, Lapt;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c006e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 46
    :goto_6
    iget-object v1, p2, Lbdd;->u:Ljava/lang/String;

    .line 47
    invoke-virtual {p2}, Lbdd;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 49
    iget-object v2, p1, Lapt;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 52
    :goto_7
    iget-object v2, p1, Lapt;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    if-eqz v0, :cond_4

    .line 54
    iget-object v0, p1, Lapt;->e:Landroid/widget/TextView;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 55
    const-string v1, ""

    .line 56
    const-string v0, ""

    .line 57
    iget-object v2, p2, Lbdd;->s:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 58
    iget-object v1, p2, Lbdd;->s:Ljava/lang/String;

    .line 59
    iget v2, p2, Lbdd;->t:I

    if-ne v2, v7, :cond_3

    .line 60
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f110389

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    :cond_3
    :goto_8
    iget-object v2, p1, Lapt;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, p1, Lapt;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :cond_4
    iget-boolean v0, p2, Lbdd;->w:Z

    if-eqz v0, :cond_d

    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 73
    :goto_9
    iget-object v1, p1, Lapt;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 74
    iget-object v1, p1, Lapt;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 75
    iget-object v1, p1, Lapt;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 76
    iget-object v1, p1, Lapt;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 77
    iget-object v1, p1, Lapt;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    .line 78
    iget-boolean v0, p2, Lbdd;->w:Z

    if-eqz v0, :cond_e

    const v0, 0x7f0c0035

    .line 79
    :goto_a
    invoke-static {v2, v0}, Llw;->c(Landroid/content/Context;I)I

    move-result v0

    .line 80
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 81
    return-void

    :cond_5
    move v3, v2

    .line 19
    goto/16 :goto_2

    :cond_6
    move v3, v2

    .line 20
    goto/16 :goto_3

    .line 28
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 35
    :cond_8
    iget-object v3, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v4, 0x7f1100a2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p2, Lbdd;->c:Ljava/lang/String;

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 43
    :cond_9
    iget-object v3, p1, Lapt;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    .line 45
    :cond_a
    iget-object v1, p1, Lapt;->g:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 50
    :cond_b
    invoke-virtual {p2}, Lbdd;->a()Ljava/lang/CharSequence;

    move-result-object v1

    .line 51
    iget-object v3, p1, Lapt;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextDirection(I)V

    goto/16 :goto_7

    .line 61
    :cond_c
    iget v2, p2, Lbdd;->t:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_8

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f11038b

    .line 67
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 62
    :pswitch_2
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f11038d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 64
    :pswitch_3
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f11038c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 69
    :pswitch_4
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f11038a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 72
    :cond_d
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto/16 :goto_9

    .line 78
    :cond_e
    const v0, 0x7f0c0039

    goto :goto_a

    :cond_f
    move-object v1, v3

    goto/16 :goto_5

    .line 61
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lapt;Ljava/lang/Integer;Lbdd;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 131
    iget-object v0, p3, Lbdd;->z:Ljava/lang/CharSequence;

    .line 132
    if-eqz p2, :cond_0

    .line 133
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v2, 0x7f11009e

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_0
    iget-object v1, p3, Lbdd;->g:[I

    aget v1, v1, v6

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    iget-wide v2, p3, Lbdd;->i:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 136
    iget-object v1, p1, Lapt;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v3, 0x7f110344

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v6

    .line 137
    invoke-virtual {p0, p3}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->f(Lbdd;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 138
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v1, p1, Lapt;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 3
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0
.end method

.method public b(Lbdd;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 89
    iget-boolean v0, p1, Lbdd;->x:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v1, 0x7f1102e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 104
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    iget-boolean v0, p1, Lbdd;->y:Z

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v1, 0x7f11005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_2
    const/4 v0, 0x0

    .line 94
    iget-object v1, p1, Lbdd;->a:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Lbdd;->a:Ljava/lang/CharSequence;

    .line 95
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->c:Laqc;

    iget-object v2, p1, Lbdd;->q:Landroid/telecom/PhoneAccountHandle;

    iget-object v3, p1, Lbdd;->a:Ljava/lang/CharSequence;

    .line 96
    invoke-virtual {v1, v2, v3}, Laqc;->a(Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 97
    invoke-static {p1}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->c(Lbdd;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 98
    iget-object v0, p1, Lbdd;->f:Ljava/lang/String;

    .line 102
    :cond_3
    :goto_1
    iget-object v1, p1, Lbdd;->j:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v0, p1, Lbdd;->u:Ljava/lang/String;

    goto :goto_0

    .line 99
    :cond_4
    iget v1, p1, Lbdd;->m:I

    if-nez v1, :cond_5

    iget-object v1, p1, Lbdd;->n:Ljava/lang/CharSequence;

    .line 100
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 101
    :cond_5
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    iget v1, p1, Lbdd;->m:I

    iget-object v2, p1, Lbdd;->n:Ljava/lang/CharSequence;

    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public b(J)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 126
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d:Ljava/util/Calendar;

    invoke-virtual {p0}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 127
    iget-object v1, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 128
    iget-object v2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d:Ljava/util/Calendar;

    invoke-virtual {v2, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 129
    iget-object v2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->d:Ljava/util/Calendar;

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lbdd;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 112
    iget-object v0, p1, Lbdd;->g:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 113
    invoke-virtual {p0, p1}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->e(Lbdd;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-wide v0, p1, Lbdd;->h:J

    .line 115
    invoke-virtual {p0}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    const/high16 v6, 0x40000

    .line 116
    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Lbdd;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 117
    iget-object v0, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v1, 0x7f110343

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p1, Lbdd;->h:J

    .line 118
    invoke-virtual {p0, v4, v5}, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->a:Landroid/content/Context;

    iget-wide v4, p1, Lbdd;->h:J

    .line 119
    invoke-static {v3, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 120
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Lbdd;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v0, 0x63

    .line 142
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p1, Lbdd;->i:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    .line 143
    iget-wide v4, p1, Lbdd;->i:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 144
    cmp-long v6, v2, v0

    if-lez v6, :cond_0

    .line 146
    :goto_0
    iget-object v2, p0, Lcom/android/dialer/configprovider/SharedPrefConfigProvider;->b:Landroid/content/res/Resources;

    const v3, 0x7f110346

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-virtual {v2, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method
