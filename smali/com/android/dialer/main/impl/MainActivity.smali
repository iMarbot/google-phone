.class public final Lcom/android/dialer/main/impl/MainActivity;
.super Luh;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/main/impl/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    .line 3
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    .line 4
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 5
    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 30
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 6
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 7
    const-string v0, "MainActivity.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 8
    const v0, 0x7f040088

    invoke-virtual {p0, v0}, Lcom/android/dialer/main/impl/MainActivity;->setContentView(I)V

    .line 10
    const v0, 0x7f0e020f

    invoke-virtual {p0, v0}, Lcom/android/dialer/main/impl/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    const v0, 0x7f0e020e

    invoke-virtual {p0, v0}, Lcom/android/dialer/main/impl/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 12
    new-instance v1, Lblk;

    invoke-virtual {p0}, Lcom/android/dialer/main/impl/MainActivity;->c()Lja;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lblk;-><init>(Landroid/content/Context;Lja;)V

    .line 13
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lqv;)V

    .line 14
    const v1, 0x7f0e020d

    invoke-virtual {p0, v1}, Lcom/android/dialer/main/impl/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TabLayout;

    .line 15
    invoke-virtual {v1, v0}, Landroid/support/design/widget/TabLayout;->a(Landroid/support/v4/view/ViewPager;)V

    .line 16
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/android/dialer/main/impl/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 17
    const v1, 0x1030237

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 19
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v1

    invoke-virtual {v1, v0}, Luj;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 20
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/main/impl/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f130005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 22
    const/4 v0, 0x1

    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 23
    const-string v1, "Not yet implemented"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 24
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e02a7

    if-ne v1, v2, :cond_1

    .line 28
    :cond_0
    :goto_0
    return v0

    .line 26
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e02a8

    if-eq v1, v2, :cond_0

    .line 28
    invoke-super {p0, p1}, Luh;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
