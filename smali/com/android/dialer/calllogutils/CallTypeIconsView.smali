.class public Lcom/android/dialer/calllogutils/CallTypeIconsView;
.super Landroid/view/View;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/calllogutils/CallTypeIconsView$a;
    }
.end annotation


# static fields
.field private static c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

.field private static d:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;


# instance fields
.field public a:Z

.field private b:Z

.field private e:Ljava/util/List;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->e:Ljava/util/List;

    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lbde;->a:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 7
    sget v1, Lbde;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b:Z

    .line 8
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 9
    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    invoke-direct {v0, p1, v2}, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    .line 11
    :cond_0
    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->d:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b:Z

    if-eqz v0, :cond_1

    .line 12
    new-instance v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->d:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    .line 13
    :cond_1
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)I
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int/2addr v0, p2

    .line 71
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p1, p2, v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 72
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 73
    return v0
.end method

.method private final b(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->d:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    .line 44
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 50
    :pswitch_0
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->c:Landroid/graphics/drawable/Drawable;

    :goto_1
    return-object v0

    .line 43
    :cond_0
    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 46
    :pswitch_2
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 47
    :pswitch_3
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 48
    :pswitch_4
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 49
    :pswitch_5
    iget-object v0, v0, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    iget-object v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 15
    iput v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    .line 16
    iput v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    .line 17
    invoke-virtual {p0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->invalidate()V

    .line 18
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    invoke-direct {p0, p1}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 21
    iget v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sget-object v3, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget v3, v3, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    .line 22
    iget v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    .line 23
    invoke-virtual {p0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->invalidate()V

    .line 24
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a:Z

    .line 26
    if-eqz p1, :cond_0

    .line 27
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sget-object v2, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget v2, v2, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    .line 28
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    .line 29
    invoke-virtual {p0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->invalidate()V

    .line 30
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->f:Z

    .line 32
    if-eqz p1, :cond_0

    .line 33
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sget-object v2, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget v2, v2, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    .line 34
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    .line 35
    invoke-virtual {p0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->invalidate()V

    .line 36
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->g:Z

    .line 38
    if-eqz p1, :cond_0

    .line 39
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sget-object v2, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget v2, v2, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    .line 40
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    sget-object v1, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    iget-object v1, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    .line 41
    invoke-virtual {p0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->invalidate()V

    .line 42
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 53
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->d:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    move-object v1, v0

    .line 55
    :goto_0
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->f:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->g:Z

    if-nez v0, :cond_3

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 57
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    add-int/2addr v5, v2

    .line 59
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v0, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 60
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 61
    iget v0, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int/2addr v0, v5

    move v2, v0

    .line 62
    goto :goto_1

    .line 53
    :cond_1
    sget-object v0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c:Lcom/android/dialer/calllogutils/CallTypeIconsView$a;

    move-object v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 63
    :cond_3
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a:Z

    if-eqz v0, :cond_4

    .line 64
    iget-object v0, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->f:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)I

    move-result v0

    iget v2, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int v3, v0, v2

    .line 65
    :cond_4
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->f:Z

    if-eqz v0, :cond_5

    .line 66
    iget-object v0, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)I

    move-result v0

    iget v2, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->i:I

    add-int v3, v0, v2

    .line 67
    :cond_5
    iget-boolean v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->g:Z

    if-eqz v0, :cond_6

    .line 68
    iget-object v0, v1, Lcom/android/dialer/calllogutils/CallTypeIconsView$a;->h:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0, v3}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)I

    .line 69
    :cond_6
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->h:I

    iget v1, p0, Lcom/android/dialer/calllogutils/CallTypeIconsView;->i:I

    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->setMeasuredDimension(II)V

    .line 52
    return-void
.end method
