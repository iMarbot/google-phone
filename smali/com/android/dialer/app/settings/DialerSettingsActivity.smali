.class public Lcom/android/dialer/app/settings/DialerSettingsActivity;
.super Laso;
.source "PG"


# annotations
.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation


# instance fields
.field private b:Z

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Laso;-><init>()V

    return-void
.end method

.method private final b()Landroid/telecom/PhoneAccountHandle;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 118
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 120
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 121
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v5

    .line 122
    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 123
    const-string v5, "DialerSettingsActivity.getSoleSimAccount"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x11

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is a SIM account"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    if-eqz v2, :cond_1

    .line 128
    :goto_1
    return-object v3

    :cond_0
    move-object v1, v2

    :cond_1
    move-object v2, v1

    .line 127
    goto :goto_0

    :cond_2
    move-object v3, v2

    .line 128
    goto :goto_1
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 144
    .line 145
    iget-boolean v0, p0, Laso;->a:Z

    .line 146
    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-super {p0}, Laso;->onBackPressed()V

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 8

    .prologue
    const v7, 0x7f110383

    const/high16 v6, 0x4000000

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 21
    iput-object p1, p0, Lcom/android/dialer/app/settings/DialerSettingsActivity;->c:Ljava/util/List;

    .line 23
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 24
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    .line 25
    :goto_0
    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 27
    const v1, 0x7f110158

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 28
    const-class v1, Lasp;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 29
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_0
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 31
    const v1, 0x7f1102d3

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 32
    const-class v1, Lasr;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 33
    const-wide/32 v4, 0x7f0e003b

    iput-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    .line 34
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 36
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.telecom.action.SHOW_RESPOND_VIA_SMS_SETTINGS"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    const v4, 0x7f110299

    iput v4, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 38
    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 39
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    const-string v0, "phone"

    .line 41
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 43
    const-class v1, Landroid/os/UserManager;

    invoke-virtual {p0, v1}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/os/UserManager;->isSystemUser()Z

    move-result v1

    .line 45
    if-eqz v1, :cond_8

    invoke-static {v0}, Lbev;->a(Landroid/telephony/TelephonyManager;)I

    move-result v4

    if-gt v4, v2, :cond_8

    .line 46
    new-instance v2, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v2}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 47
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.telecom.action.SHOW_CALL_SETTINGS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 49
    const v5, 0x7f1100ac

    iput v5, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 50
    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 51
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_1
    :goto_1
    invoke-static {p0}, Lapw;->o(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 60
    new-instance v2, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v2}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 61
    const v4, 0x7f1101ea

    iput v4, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 62
    invoke-static {p0}, Lapw;->m(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 63
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-static {p0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/dialer/app/settings/DialerSettingsActivity;->b:Z

    .line 66
    :cond_2
    if-nez v1, :cond_a

    .line 67
    const-string v2, "DialerSettingsActivity.addVoicemailSettings"

    const-string v4, "user not primary user"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :goto_2
    if-eqz v1, :cond_4

    .line 96
    invoke-static {v0}, Lbev;->b(Landroid/telephony/TelephonyManager;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 97
    invoke-static {v0}, Lbev;->c(Landroid/telephony/TelephonyManager;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 98
    :cond_3
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 99
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.telecom.action.SHOW_CALL_ACCESSIBILITY_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    const v2, 0x7f11002b

    iput v2, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 101
    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 102
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_4
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lapw;->a(Lbew;)Z

    move-result v0

    .line 106
    const-string v1, "DialerSettingsActivity.onBuildHeaders"

    const/16 v2, 0x26

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "showing assisted dialing header: "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    if-eqz v0, :cond_5

    .line 108
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 109
    const v1, 0x7f110046

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 110
    const-class v1, Lavm;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 111
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_5
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 114
    const v1, 0x7f11002a

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 115
    const-class v1, Lamh;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 116
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_6
    return-void

    :cond_7
    move v0, v3

    .line 24
    goto/16 :goto_0

    .line 52
    :cond_8
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-ge v2, v4, :cond_9

    if-eqz v1, :cond_1

    .line 53
    :cond_9
    new-instance v2, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v2}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 54
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.telecom.action.CHANGE_PHONE_ACCOUNTS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 56
    const v5, 0x7f11026d

    iput v5, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 57
    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 58
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 69
    :cond_a
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    if-ge v2, v4, :cond_b

    .line 70
    const-string v2, "DialerSettingsActivity.addVoicemailSettings"

    const-string v4, "Dialer voicemail settings not supported by system"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 72
    :cond_b
    const-string v2, "DialerSettingsActivity.addVoicemailSettings"

    const-string v4, "adding voicemail settings"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    new-instance v2, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v2}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 74
    iput v7, v2, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 75
    invoke-direct {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->b()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 76
    if-nez v4, :cond_c

    .line 77
    const-string v4, "DialerSettingsActivity.addVoicemailSettings"

    const-string v5, "showing multi-SIM voicemail settings"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    const-class v4, Lasq;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 79
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 80
    const-string v5, "target_fragment"

    const-class v6, Lbtw;

    .line 81
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    .line 82
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v5, "phone_account_handle_key"

    const-string v6, "phone_account_handle"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v5, "arguments"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 85
    const-string v5, "target_title_res"

    invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    iput-object v4, v2, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    .line 87
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 89
    :cond_c
    const-string v5, "DialerSettingsActivity.addVoicemailSettings"

    const-string v6, "showing single-SIM voicemail settings"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    const-class v5, Lbtw;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 91
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 92
    const-string v6, "phone_account_handle"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 93
    iput-object v5, v2, Landroid/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    .line 94
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2
    const-string v0, "DialerSettingsActivity.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 3
    invoke-super {p0, p1}, Laso;->onCreate(Landroid/os/Bundle;)V

    .line 4
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 9
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/dialer/app/settings/DialerSettingsActivity;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 10
    iget-object v0, p0, Lcom/android/dialer/app/settings/DialerSettingsActivity;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 11
    iget-object v3, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 12
    const-string v2, "DialerSettingsActivity.onCreate"

    const-string v3, "switching to header: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    .line 16
    :cond_1
    return-void

    .line 12
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 4

    .prologue
    .line 130
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0e003b

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 131
    invoke-static {p0}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1102fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 134
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 136
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SOUND_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 139
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-super {p0, p1, p2}, Laso;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 140
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->onBackPressed()V

    .line 142
    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0}, Laso;->onResume()V

    .line 18
    iget-boolean v0, p0, Lcom/android/dialer/app/settings/DialerSettingsActivity;->b:Z

    invoke-static {p0}, Lapw;->j(Landroid/content/Context;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->invalidateHeaders()V

    .line 20
    :cond_0
    return-void
.end method
