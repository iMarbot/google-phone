.class public Lcom/android/dialer/app/widget/SearchEditTextLayout;
.super Landroid/widget/FrameLayout;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/app/widget/SearchEditTextLayout$a;
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroid/view/View;

.field public d:Landroid/widget/EditText;

.field public e:Landroid/view/View;

.field public f:Lcom/android/dialer/app/widget/SearchEditTextLayout$a;

.field public g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:F

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    iput-boolean v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 3
    iput-boolean v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 4
    return-void
.end method

.method private final a()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    new-instance v1, Lauz;

    invoke-direct {v1, p0}, Lauz;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 87
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 88
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 89
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 91
    iget v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->h:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 92
    iget v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->i:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 93
    iget v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->j:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 94
    iget v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->k:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 95
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->requestLayout()V

    .line 96
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    if-eqz p1, :cond_0

    .line 31
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setAlpha(F)V

    .line 32
    invoke-virtual {p0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setVisibility(I)V

    .line 33
    iput-boolean v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setAlpha(F)V

    .line 35
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setVisibility(I)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 38
    invoke-virtual {p0, v6}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c(Z)V

    .line 39
    if-eqz p1, :cond_1

    .line 40
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    const/16 v2, 0xc8

    invoke-static {v0, v1, v2}, Lamn;->a(Landroid/view/View;Landroid/view/View;I)V

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    .line 42
    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(F)V

    .line 43
    invoke-direct {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a()V

    .line 48
    :goto_0
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getPaddingTop()I

    move-result v0

    .line 49
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getPaddingStart()I

    move-result v1

    .line 50
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getPaddingBottom()I

    move-result v2

    .line 51
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getPaddingEnd()I

    move-result v3

    .line 52
    const v4, 0x7f020184

    invoke-virtual {p0, v4}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setBackgroundResource(I)V

    .line 53
    invoke-virtual {p0, v5}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setElevation(F)V

    .line 54
    invoke-virtual {p0, v1, v0, v3, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setPaddingRelative(IIII)V

    .line 55
    if-eqz p2, :cond_0

    .line 56
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 57
    :cond_0
    iput-boolean v6, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 58
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 46
    invoke-virtual {p0, v5}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(F)V

    .line 47
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 41
    nop

    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x0
    .end array-data
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0, v3}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c(Z)V

    .line 60
    if-eqz p1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    const/16 v2, 0xc8

    invoke-static {v0, v1, v2}, Lamn;->a(Landroid/view/View;Landroid/view/View;I)V

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->r:Landroid/animation/ValueAnimator;

    .line 63
    invoke-direct {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a()V

    .line 68
    :goto_0
    iput-boolean v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 69
    iget v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->l:F

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setElevation(F)V

    .line 70
    const v0, 0x7f020183

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->setBackgroundResource(I)V

    .line 71
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 66
    invoke-virtual {p0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(F)V

    .line 67
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 62
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final c(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 72
    if-eqz p1, :cond_0

    move v2, v1

    .line 73
    :goto_0
    if-eqz p1, :cond_1

    .line 74
    :goto_1
    iget-object v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->n:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 75
    iget-object v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->o:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-boolean v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->g:Z

    if-eqz v3, :cond_2

    .line 77
    iget-object v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->p:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 79
    :goto_2
    iget-object v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->q:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v2, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 81
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    :goto_3
    return-void

    :cond_0
    move v2, v0

    .line 72
    goto :goto_0

    :cond_1
    move v0, v1

    .line 73
    goto :goto_1

    .line 78
    :cond_2
    iget-object v3, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->p:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const v2, 0x7f0e0254

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 6
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->h:I

    .line 7
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->i:I

    .line 8
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v1, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->j:I

    .line 9
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->k:I

    .line 10
    invoke-virtual {p0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->getElevation()F

    move-result v0

    iput v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->l:F

    .line 11
    const v0, 0x7f0e0258

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    .line 12
    const v0, 0x7f0e0251

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    .line 13
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->m:Landroid/view/View;

    const v1, 0x7f0e0253

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    .line 14
    const v0, 0x7f0e0259

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->n:Landroid/view/View;

    .line 15
    const v0, 0x7f0e025a

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->o:Landroid/view/View;

    .line 16
    const v0, 0x7f0e025b

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->p:Landroid/view/View;

    .line 17
    const v0, 0x7f0e025c

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->q:Landroid/view/View;

    .line 18
    invoke-virtual {p0, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->e:Landroid/view/View;

    .line 19
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    new-instance v1, Laut;

    invoke-direct {v1, p0}, Laut;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 20
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c:Landroid/view/View;

    new-instance v1, Laus;

    invoke-direct {v1, p0}, Laus;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 21
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    new-instance v1, Lauu;

    invoke-direct {v1}, Lauu;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 22
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    new-instance v1, Lauv;

    invoke-direct {v1, p0}, Lauv;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    iget-object v0, p0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->d:Landroid/widget/EditText;

    new-instance v1, Lauw;

    invoke-direct {v1, p0}, Lauw;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 24
    invoke-virtual {p0, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Laux;

    invoke-direct {v1, p0}, Laux;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    const v0, 0x7f0e0252

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lauy;

    invoke-direct {v1, p0}, Lauy;-><init>(Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    .line 27
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 29
    return-void
.end method
