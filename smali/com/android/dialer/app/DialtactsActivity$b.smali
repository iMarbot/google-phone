.class public final Lcom/android/dialer/app/DialtactsActivity$b;
.super Landroid/widget/PopupMenu;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/app/DialtactsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field private synthetic a:Lcom/android/dialer/app/DialtactsActivity;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/DialtactsActivity;Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 2
    const v0, 0x800005

    invoke-direct {p0, p2, p3, v0}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 3
    return-void
.end method


# virtual methods
.method public final show()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity$b;->getMenu()Landroid/view/Menu;

    move-result-object v3

    .line 5
    const v0, 0x7f0e02a3

    invoke-interface {v3, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 6
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 7
    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 9
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 10
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 12
    iget-object v0, v0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 14
    iget-object v5, v0, Larl;->c:Lari;

    iget-object v0, v0, Larl;->c:Lari;

    .line 15
    invoke-virtual {v0, v2}, Lari;->d(I)I

    move-result v0

    invoke-virtual {v5, v0}, Lari;->a(I)Landroid/app/Fragment;

    move-result-object v0

    .line 16
    instance-of v5, v0, Larn;

    if-eqz v5, :cond_1

    .line 17
    check-cast v0, Larn;

    .line 18
    iget-object v5, v0, Larn;->f:Lasb;

    if-eqz v5, :cond_0

    .line 19
    iget-object v0, v0, Larn;->f:Lasb;

    .line 20
    iget v0, v0, Lasb;->b:I

    .line 21
    if-lez v0, :cond_0

    move v0, v1

    .line 24
    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    .line 25
    :goto_1
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 26
    const v0, 0x7f0e02a2

    invoke-interface {v3, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v4, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    .line 27
    invoke-static {v4}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v4

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 28
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity$b;->a:Lcom/android/dialer/app/DialtactsActivity;

    invoke-virtual {v0}, Lcom/android/dialer/app/DialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 29
    const v0, 0x7f0e02a5

    invoke-interface {v3, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 31
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lbkc;

    invoke-interface {v0}, Lbkc;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpv;

    .line 32
    invoke-interface {v0}, Lbpv;->k()Lbpu;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbpu;->a()Lbps;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lbps;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 35
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 36
    invoke-virtual {v0, v4}, Lbps;->a(Landroid/content/Context;)Landroid/view/ActionProvider;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;

    .line 38
    :goto_2
    invoke-static {v4}, Lbli;->a(Landroid/content/Context;)Lbli;

    move-result-object v0

    invoke-virtual {v0}, Lbli;->a()Lblh;

    move-result-object v0

    .line 39
    const v1, 0x7f0e02a6

    invoke-interface {v3, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 40
    invoke-virtual {v0}, Lblh;->a()Z

    move-result v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 41
    invoke-super {p0}, Landroid/widget/PopupMenu;->show()V

    .line 42
    return-void

    :cond_0
    move v0, v2

    .line 22
    goto :goto_0

    :cond_1
    move v0, v2

    .line 23
    goto :goto_0

    :cond_2
    move v0, v2

    .line 24
    goto :goto_1

    .line 37
    :cond_3
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method
