.class public Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;
.super Larx;
.source "PG"


# instance fields
.field public d:Lahc;

.field private j:F

.field private k:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Larx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00da

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->j:F

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lahc;)V
    .locals 2

    .prologue
    .line 13
    invoke-super {p0, p1}, Larx;->a(Lahc;)V

    .line 14
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->k:Landroid/widget/ImageButton;

    new-instance v1, Larw;

    invoke-direct {v1, p0}, Larw;-><init>(Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->d:Lahc;

    .line 17
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->getWidth()I

    move-result v0

    return v0
.end method

.method protected final b(Lahc;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p1}, Lahc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-super {p0}, Larx;->onFinishInflate()V

    .line 6
    const v0, 0x7f0e0012

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 8
    const v0, 0x7f0e0245

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 9
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 10
    const v0, 0x7f0e0242

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->k:Landroid/widget/ImageButton;

    .line 11
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 18
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 19
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->j:F

    int-to-float v2, v1

    mul-float/2addr v0, v2

    float-to-int v2, v0

    .line 20
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->getChildCount()I

    move-result v3

    .line 21
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 22
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 23
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 24
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 25
    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p0, v1, v2}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->setMeasuredDimension(II)V

    .line 28
    return-void
.end method
