.class public Lcom/android/dialer/app/list/PhoneFavoriteListView;
.super Landroid/widget/GridView;
.source "PG"

# interfaces
.implements Lark;
.implements Lars;


# static fields
.field private static i:Ljava/lang/String;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Landroid/os/Handler;

.field public e:Landroid/graphics/Bitmap;

.field public f:Landroid/widget/ImageView;

.field public g:Landroid/view/View;

.field public h:Larj;

.field private j:[I

.field private k:F

.field private l:Ljava/lang/Runnable;

.field private m:Z

.field private n:I

.field private o:Landroid/animation/AnimatorListenerAdapter;

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-class v0, Lcom/android/dialer/app/list/PhoneFavoriteListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    .line 7
    new-instance v0, Laru;

    invoke-direct {v0, p0}, Laru;-><init>(Lcom/android/dialer/app/list/PhoneFavoriteListView;)V

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->l:Ljava/lang/Runnable;

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->m:Z

    .line 9
    new-instance v0, Larv;

    invoke-direct {v0, p0}, Larv;-><init>(Lcom/android/dialer/app/list/PhoneFavoriteListView;)V

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->o:Landroid/animation/AnimatorListenerAdapter;

    .line 10
    new-instance v0, Larj;

    invoke-direct {v0, p0}, Larj;-><init>(Lark;)V

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    .line 11
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->p:I

    .line 12
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->k:F

    .line 13
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    invoke-virtual {v0, p0}, Larj;->a(Lars;)V

    .line 14
    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 101
    invoke-virtual {p0, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 102
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 104
    if-eqz v1, :cond_0

    .line 105
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->destroyDrawingCache()V

    .line 111
    invoke-virtual {p0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 112
    return-object v0

    .line 107
    :catch_0
    move-exception v1

    .line 108
    sget-object v2, Lcom/android/dialer/app/list/PhoneFavoriteListView;->i:Ljava/lang/String;

    const-string v3, "Failed to copy bitmap from Drawing cache"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private final a()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->d:Landroid/os/Handler;

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 113
    iget-object v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    invoke-virtual {p0, v2}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getLocationOnScreen([I)V

    .line 114
    iget-object v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    aget v2, v2, v0

    sub-int v3, p1, v2

    .line 115
    iget-object v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    const/4 v4, 0x1

    aget v2, v2, v4

    sub-int v4, p2, v2

    .line 117
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildCount()I

    move-result v5

    move v2, v0

    .line 118
    :goto_0
    if-ge v2, v5, :cond_1

    .line 119
    invoke-virtual {p0, v2}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    if-lt v4, v6, :cond_0

    .line 121
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    if-gt v4, v6, :cond_0

    .line 122
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v3, v6, :cond_0

    .line 123
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    if-gt v3, v6, :cond_0

    .line 128
    :goto_1
    instance-of v2, v0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;

    if-nez v2, :cond_2

    .line 130
    :goto_2
    return-object v1

    .line 125
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 126
    goto :goto_1

    .line 130
    :cond_2
    check-cast v0, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;

    move-object v1, v0

    goto :goto_2
.end method

.method public final a(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 67
    invoke-static {p3}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->a(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->e:Landroid/graphics/Bitmap;

    .line 68
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    invoke-virtual {p3, v0}, Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;->getLocationOnScreen([I)V

    .line 71
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    aget v0, v0, v2

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    .line 72
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    aget v0, v0, v3

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    .line 73
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->q:I

    .line 74
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->r:I

    .line 75
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 76
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    .line 77
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    aget v1, v1, v3

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    .line 78
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 79
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 81
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 82
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    goto :goto_0
.end method

.method public final b(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 85
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->q:I

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    .line 86
    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->r:I

    sub-int v0, p2, v0

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->j:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    .line 87
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->s:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 89
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->t:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 90
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/widget/GridView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 16
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->k:F

    .line 17
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    .line 22
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 23
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 24
    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v4, v5

    .line 60
    :cond_1
    :goto_1
    return v4

    .line 25
    :pswitch_0
    const-string v2, "PHONE_FAVORITE_TILE"

    invoke-virtual {p1}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 27
    iget-object v6, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    .line 30
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_5

    .line 31
    iget-object v2, v6, Larj;->c:[I

    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 32
    iget-object v2, v6, Larj;->c:[I

    aget v2, v2, v4

    add-int/2addr v1, v2

    .line 33
    iget-object v2, v6, Larj;->c:[I

    aget v2, v2, v5

    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 34
    :goto_2
    iget-object v0, v6, Larj;->b:Lark;

    .line 35
    invoke-interface {v0, v2, v1}, Lark;->a(II)Lcom/android/dialer/app/list/PhoneFavoriteSquareTileView;

    move-result-object v7

    .line 36
    if-nez v7, :cond_2

    move v0, v4

    .line 42
    :goto_3
    if-nez v0, :cond_0

    goto :goto_1

    :cond_2
    move v3, v4

    .line 38
    :goto_4
    iget-object v0, v6, Larj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 39
    iget-object v0, v6, Larj;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lars;

    invoke-interface {v0, v2, v1, v7}, Lars;->a(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V

    .line 40
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_3
    move v0, v5

    .line 41
    goto :goto_3

    .line 44
    :pswitch_1
    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->c:I

    .line 45
    iget-object v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    invoke-virtual {v2, p0, v1, v0}, Larj;->a(Landroid/view/View;II)V

    .line 46
    iget-boolean v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->m:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->c:I

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->n:I

    sub-int/2addr v0, v1

    .line 47
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40800000    # 4.0f

    iget v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->k:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 48
    iput-boolean v5, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->m:Z

    .line 49
    invoke-direct {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->a()V

    .line 50
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->l:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 51
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 52
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getTop()I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->a:I

    .line 53
    invoke-virtual {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->getBottom()I

    move-result v1

    sub-int v0, v1, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->b:I

    goto/16 :goto_0

    .line 55
    :pswitch_3
    invoke-direct {p0}, Lcom/android/dialer/app/list/PhoneFavoriteListView;->a()V

    .line 56
    iget-object v3, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->d:Landroid/os/Handler;

    iget-object v6, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->l:Ljava/lang/Runnable;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 57
    iput-boolean v4, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->m:Z

    .line 58
    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 59
    :cond_4
    iget-object v2, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->h:Larj;

    invoke-virtual {v2, v1, v0, v4}, Larj;->a(IIZ)V

    goto/16 :goto_0

    :cond_5
    move v2, v1

    move v1, v0

    goto/16 :goto_2

    .line 24
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->n:I

    .line 20
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 93
    iget-object v0, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->f:Landroid/widget/ImageView;

    .line 94
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->p:I

    int-to-long v2, v1

    .line 96
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/app/list/PhoneFavoriteListView;->o:Landroid/animation/AnimatorListenerAdapter;

    .line 97
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 99
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method
