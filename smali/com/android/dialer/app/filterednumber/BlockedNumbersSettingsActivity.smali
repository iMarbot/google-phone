.class public Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;
.super Luh;
.source "PG"

# interfaces
.implements Lask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 23
    :goto_0
    return-void

    .line 22
    :cond_0
    invoke-super {p0}, Luh;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const v0, 0x7f040025

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->setContentView(I)V

    .line 4
    if-nez p1, :cond_1

    .line 7
    invoke-virtual {p0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "blocked_management"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Laqo;

    .line 8
    if-nez v0, :cond_0

    .line 9
    new-instance v0, Laqo;

    invoke-direct {v0}, Laqo;-><init>()V

    .line 10
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0e00f3

    const-string v3, "blocked_management"

    .line 12
    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 14
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->s:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 15
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 16
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/android/dialer/app/filterednumber/BlockedNumbersSettingsActivity;->onBackPressed()V

    .line 18
    const/4 v0, 0x1

    .line 19
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method
