.class public Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;
.super Landroid/app/job/JobService;
.source "PG"


# static fields
.field public static a:Landroid/app/job/JobInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const/16 v1, 0xcd

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 3
    const-string v0, "VoicemailNotificationJobService.scheduleJob"

    const-string v1, "job canceled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 3

    .prologue
    .line 5
    const-string v0, "VoicemailNotificationJobService.onStartJob"

    const-string v1, "updating notification"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    new-instance v0, Lapy;

    invoke-direct {v0, p0, p1}, Lapy;-><init>(Lcom/android/dialer/app/calllog/VoicemailNotificationJobService;Landroid/app/job/JobParameters;)V

    invoke-static {p0, v0}, Lapw;->a(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 7
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    return v0
.end method
