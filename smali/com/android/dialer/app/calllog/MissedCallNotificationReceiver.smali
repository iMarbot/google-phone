.class public Lcom/android/dialer/app/calllog/MissedCallNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3
    const-string v1, "android.telecom.action.SHOW_MISSED_CALLS_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 16
    :goto_0
    return-void

    .line 5
    :cond_0
    const-string v0, "android.telecom.extra.NOTIFICATION_COUNT"

    const/4 v1, -0x1

    .line 6
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 7
    const-string v1, "android.telecom.extra.NOTIFICATION_PHONE_NUMBER"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 8
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/MissedCallNotificationReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v2

    .line 9
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v3

    .line 10
    invoke-virtual {v3}, Lbed;->a()Lbef;

    move-result-object v3

    .line 11
    invoke-static {p1}, Lbdx;->a(Landroid/content/Context;)Lbdx;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v3

    new-instance v4, Lbbu;

    invoke-direct {v4, p1, v0, v2}, Lbbu;-><init>(Landroid/content/Context;ILandroid/content/BroadcastReceiver$PendingResult;)V

    .line 12
    invoke-interface {v3, v4}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v3

    new-instance v4, Lbbz;

    invoke-direct {v4, v2}, Lbbz;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 13
    invoke-interface {v3, v4}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v2

    .line 14
    invoke-interface {v2}, Lbdz;->a()Lbdy;

    move-result-object v2

    new-instance v3, Lpr;

    .line 15
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v0, v1}, Lpr;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
