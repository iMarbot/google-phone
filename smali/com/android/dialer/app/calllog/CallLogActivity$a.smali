.class public final Lcom/android/dialer/app/calllog/CallLogActivity$a;
.super Lic;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/app/calllog/CallLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field private synthetic b:Lcom/android/dialer/app/calllog/CallLogActivity;


# direct methods
.method public constructor <init>(Lcom/android/dialer/app/calllog/CallLogActivity;Landroid/app/FragmentManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 2
    invoke-direct {p0, p2}, Lic;-><init>(Landroid/app/FragmentManager;)V

    .line 3
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/app/Fragment;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 8
    invoke-virtual {v0, p1}, Lcom/android/dialer/app/calllog/CallLogActivity;->e(I)I

    move-result v0

    .line 9
    packed-switch v0, :pswitch_data_0

    .line 12
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "No fragment at position "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :pswitch_0
    new-instance v0, Laoj;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Laoj;-><init>(IZ)V

    .line 11
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Laoj;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v2}, Laoj;-><init>(IZ)V

    goto :goto_0

    .line 9
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 13
    invoke-super {p0, p1, p2}, Lic;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoj;

    .line 14
    iget-object v1, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 15
    invoke-virtual {v1, p2}, Lcom/android/dialer/app/calllog/CallLogActivity;->e(I)I

    move-result v1

    .line 16
    packed-switch v1, :pswitch_data_0

    .line 25
    const/16 v0, 0x1d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid position: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 17
    :pswitch_0
    iget-object v1, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 18
    iput-object v0, v1, Lcom/android/dialer/app/calllog/CallLogActivity;->f:Laoj;

    .line 26
    :goto_0
    return-object v0

    .line 21
    :pswitch_1
    iget-object v1, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 22
    iput-object v0, v1, Lcom/android/dialer/app/calllog/CallLogActivity;->g:Laoj;

    goto :goto_0

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    return v0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 5
    invoke-virtual {v0, p1}, Lcom/android/dialer/app/calllog/CallLogActivity;->e(I)I

    move-result v0

    .line 6
    int-to-long v0, v0

    return-wide v0
.end method

.method public final c(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity$a;->b:Lcom/android/dialer/app/calllog/CallLogActivity;

    .line 28
    iget-object v0, v0, Lcom/android/dialer/app/calllog/CallLogActivity;->h:[Ljava/lang/String;

    .line 29
    aget-object v0, v0, p1

    return-object v0
.end method
