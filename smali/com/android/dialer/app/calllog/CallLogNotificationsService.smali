.class public Lcom/android/dialer/app/calllog/CallLogNotificationsService;
.super Landroid/app/IntentService;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/app/calllog/CallLogNotificationsService$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const-string v0, "CallLogNotificationsService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 2
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 3
    const-string v0, "CallLogNotificationsService.markAllNewVoicemailsAsOld"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    const-string v1, "com.android.dialer.calllog.ACTION_MARK_ALL_NEW_VOICEMAILS_AS_OLD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 6
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 7
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 8
    const-string v0, "CallLogNotificationsService.cancelAllMissedCalls"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 9
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lcom/android/dialer/app/calllog/CallLogNotificationsService$a;

    .line 11
    invoke-direct {v1}, Lcom/android/dialer/app/calllog/CallLogNotificationsService$a;-><init>()V

    .line 12
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 13
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 14
    invoke-interface {v0, p0}, Lbdy;->a(Ljava/lang/Object;)V

    .line 15
    return-void
.end method

.method public static c(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 17
    const-string v1, "com.android.dialer.calllog.ACTION_CANCEL_ALL_MISSED_CALLS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 18
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static d(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    const-string v0, "CallLogNotificationsService.cancelAllMissedCallsBackground"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lbdf;->c()V

    .line 69
    const/4 v0, 0x0

    invoke-static {p0, v0}, Laoy;->b(Landroid/content/Context;Landroid/net/Uri;)V

    .line 70
    invoke-static {p0}, Lbdx;->b(Landroid/content/Context;)V

    .line 71
    invoke-static {p0}, Lbsp;->b(Landroid/content/Context;)V

    .line 72
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 19
    if-nez p1, :cond_0

    .line 20
    const-string v0, "CallLogNotificationsService.onHandleIntent"

    const-string v2, "could not handle null intent"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    :goto_0
    return-void

    .line 22
    :cond_0
    const-string v0, "android.permission.READ_CALL_LOG"

    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "android.permission.WRITE_CALL_LOG"

    .line 23
    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 24
    :cond_1
    const-string v0, "CallLogNotificationsService.onHandleIntent"

    const-string v2, "no READ_CALL_LOG permission"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 27
    const-string v4, "CallLogNotificationsService.onHandleIntent"

    const-string v5, "action: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 64
    const-string v2, "CallLogNotificationsService.onHandleIntent"

    const-string v4, "no handler for action: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 27
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 28
    :sswitch_0
    const-string v4, "com.android.dialer.calllog.ACTION_MARK_ALL_NEW_VOICEMAILS_AS_OLD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    goto :goto_2

    :sswitch_1
    const-string v4, "com.android.dialer.calllog.ACTION_MARK_SINGLE_NEW_VOICEMAIL_AS_OLD "

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    goto :goto_2

    :sswitch_2
    const-string v4, "com.android.dialer.calllog.ACTION_LEGACY_VOICEMAIL_DISMISSED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v4, "com.android.dialer.calllog.ACTION_CANCEL_ALL_MISSED_CALLS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v4, "com.android.dialer.calllog.ACTION_CANCEL_SINGLE_MISSED_CALL"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v4, "com.android.dialer.calllog.CALL_BACK_FROM_MISSED_CALL_NOTIFICATION"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x5

    goto :goto_2

    .line 30
    :pswitch_0
    new-instance v0, Laqa;

    invoke-direct {v0, p0}, Laqa;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 31
    invoke-static {p0}, Lapv;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 33
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 35
    if-nez v0, :cond_5

    .line 36
    const-string v3, "VoicemailQueryHandler.markSingleNewVoicemailAsRead"

    const-string v4, "voicemail URI is null"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    :goto_4
    const-string v3, "VisualVoicemailNotifier.cancelSingleVoicemailNotification"

    invoke-static {v3}, Lapw;->b(Ljava/lang/String;)V

    .line 41
    if-nez v0, :cond_6

    .line 42
    const-string v0, "VisualVoicemailNotifier.cancelSingleVoicemailNotification"

    const-string v2, "uri is null"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 38
    :cond_5
    new-instance v3, Laqb;

    invoke-direct {v3, p0, v0}, Laqb;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-static {v3}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_4

    .line 45
    :cond_6
    invoke-static {v0}, Lapv;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {p0, v0, v2}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 48
    :pswitch_2
    const-string v0, "PHONE_ACCOUNT_HANDLE"

    .line 49
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 50
    invoke-static {p0, v0, v2}, Lcom/android/dialer/app/voicemail/LegacyVoicemailNotificationReceiver;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    goto/16 :goto_0

    .line 52
    :pswitch_3
    invoke-static {p0}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 54
    :pswitch_4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 55
    invoke-static {p0, v0}, Laoy;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 56
    invoke-static {p0, v0}, Lbdx;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 57
    invoke-static {p0}, Lbsp;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 59
    :pswitch_5
    invoke-static {p0}, Lbdx;->a(Landroid/content/Context;)Lbdx;

    move-result-object v0

    const-string v1, "android.telecom.extra.NOTIFICATION_PHONE_NUMBER"

    .line 60
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lbdx;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 64
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 28
    nop

    :sswitch_data_0
    .sparse-switch
        -0x686db24c -> :sswitch_4
        -0x3a7e3b50 -> :sswitch_1
        -0xdde8d87 -> :sswitch_5
        0x26a354f8 -> :sswitch_3
        0x29caccd9 -> :sswitch_2
        0x7c6cd13c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
