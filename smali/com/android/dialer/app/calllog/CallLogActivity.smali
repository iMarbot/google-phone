.class public Lcom/android/dialer/app/calllog/CallLogActivity;
.super Lbsz;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/app/calllog/CallLogActivity$a;
    }
.end annotation


# instance fields
.field public f:Laoj;

.field public g:Laoj;

.field public h:[Ljava/lang/String;

.field private i:Landroid/support/v4/view/ViewPager;

.field private j:Lcom/android/contacts/common/list/ViewPagerTabs;

.field private k:Lcom/android/dialer/app/calllog/CallLogActivity$a;

.field private l:Z

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbsz;-><init>()V

    return-void
.end method

.method private final f()V
    .locals 2

    .prologue
    .line 89
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->i:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 90
    return-void
.end method

.method private final f(I)V
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->m:I

    if-ne p1, v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/dialer/app/calllog/CallLogActivity;->e(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 101
    const/16 v0, 0x1d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid position: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 97
    :pswitch_0
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->f:Laoj;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->f:Laoj;

    invoke-virtual {v0}, Laoj;->h()V

    goto :goto_0

    .line 99
    :pswitch_1
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->g:Laoj;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->g:Laoj;

    invoke-virtual {v0}, Laoj;->h()V

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->j:Lcom/android/contacts/common/list/ViewPagerTabs;

    .line 88
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->j:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(IFI)V

    .line 80
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/android/dialer/app/calllog/CallLogActivity;->f(I)V

    .line 82
    iput p1, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->m:I

    .line 83
    iget-boolean v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->l:Z

    if-eqz v0, :cond_0

    .line 84
    invoke-direct {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->f()V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->j:Lcom/android/contacts/common/list/ViewPagerTabs;

    invoke-virtual {v0, p1}, Lcom/android/contacts/common/list/ViewPagerTabs;->b(I)V

    .line 86
    return-void
.end method

.method final e(I)I
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lbib;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    rsub-int/lit8 p1, p1, 0x1

    .line 93
    :cond_0
    return p1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 106
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 107
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "has_enriched_call_data"

    const/4 v1, 0x0

    .line 108
    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "phone_number"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    const v1, 0x7f0e0117

    invoke-virtual {p0, v1}, Lcom/android/dialer/app/calllog/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f110163

    invoke-virtual {p0, v2}, Lcom/android/dialer/app/calllog/CallLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-static {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    const v2, 0x7f110334

    new-instance v3, Lann;

    invoke-direct {v3, p0, v0}, Lann;-><init>(Lcom/android/dialer/app/calllog/CallLogActivity;Ljava/lang/String;)V

    .line 111
    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->c(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lbo;->a()V

    .line 114
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lbsz;->onActivityResult(IILandroid/content/Intent;)V

    .line 115
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lbld$a;->f:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 104
    invoke-super {p0}, Lbsz;->onBackPressed()V

    .line 105
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2
    invoke-super {p0, p1}, Lbsz;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const v1, 0x7f04002b

    invoke-virtual {p0, v1}, Lcom/android/dialer/app/calllog/CallLogActivity;->setContentView(I)V

    .line 4
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 6
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v1

    invoke-virtual {v1}, Luj;->a()Ltv;

    move-result-object v1

    .line 8
    invoke-virtual {v1, v2}, Ltv;->a(Z)V

    .line 9
    invoke-virtual {v1, v2}, Ltv;->b(Z)V

    .line 10
    invoke-virtual {v1, v2}, Ltv;->c(Z)V

    .line 11
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ltv;->a(F)V

    .line 13
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    const-string v3, "android.provider.extra.CALL_TYPE_FILTER"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 16
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    move v1, v2

    .line 18
    :goto_0
    iput v1, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->m:I

    .line 19
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->h:[Ljava/lang/String;

    .line 20
    iget-object v3, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->h:[Ljava/lang/String;

    const v4, 0x7f110099

    invoke-virtual {p0, v4}, Lcom/android/dialer/app/calllog/CallLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 21
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->h:[Ljava/lang/String;

    const v3, 0x7f1100a0

    invoke-virtual {p0, v3}, Lcom/android/dialer/app/calllog/CallLogActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 22
    const v0, 0x7f0e0119

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/calllog/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    .line 23
    new-instance v0, Lcom/android/dialer/app/calllog/CallLogActivity$a;

    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/dialer/app/calllog/CallLogActivity$a;-><init>(Lcom/android/dialer/app/calllog/CallLogActivity;Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->k:Lcom/android/dialer/app/calllog/CallLogActivity$a;

    .line 24
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->k:Lcom/android/dialer/app/calllog/CallLogActivity$a;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(Lqv;)V

    .line 25
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->c(I)V

    .line 26
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    .line 27
    iput-object p0, v0, Landroid/support/v4/view/ViewPager;->d:Landroid/support/v4/view/ViewPager$f;

    .line 28
    const v0, 0x7f0e0118

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/calllog/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/contacts/common/list/ViewPagerTabs;

    iput-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->j:Lcom/android/contacts/common/list/ViewPagerTabs;

    .line 29
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->j:Lcom/android/contacts/common/list/ViewPagerTabs;

    iget-object v2, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(Landroid/support/v4/view/ViewPager;)V

    .line 30
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->b(I)V

    .line 31
    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 51
    const v1, 0x7f130002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 63
    .line 64
    iget-boolean v1, p0, Lbsz;->w:Z

    .line 65
    if-nez v1, :cond_0

    .line 78
    :goto_0
    return v0

    .line 67
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 68
    sget-object v1, Lbld$a;->t:Lbld$a;

    invoke-static {v1}, Lbly;->a(Lbld$a;)V

    .line 69
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/dialer/app/DialtactsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 71
    invoke-virtual {p0, v1}, Lcom/android/dialer/app/calllog/CallLogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 73
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e029e

    if-ne v1, v2, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 75
    new-instance v2, Lapf;

    invoke-direct {v2}, Lapf;-><init>()V

    .line 76
    const-string v3, "deleteCallLog"

    invoke-virtual {v2, v1, v3}, Lapf;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_2
    invoke-super {p0, p1}, Lbsz;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->l:Z

    .line 41
    invoke-super {p0}, Lbsz;->onPause()V

    .line 42
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 53
    const v2, 0x7f0e029e

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 54
    iget-object v2, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->f:Laoj;

    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 55
    iget-object v2, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->f:Laoj;

    .line 56
    iget-object v2, v2, Laoj;->c:Lano;

    .line 58
    if-eqz v2, :cond_0

    .line 59
    iget-boolean v4, v2, Lano;->w:Z

    if-nez v4, :cond_2

    .line 60
    invoke-virtual {v2}, Lano;->a()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    .line 61
    :goto_0
    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 62
    :cond_1
    return v1

    :cond_2
    move v2, v0

    .line 60
    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lbib;->B(Landroid/content/Context;)V

    .line 33
    sget-boolean v0, Lbly;->c:Z

    .line 34
    if-nez v0, :cond_0

    .line 35
    invoke-static {}, Lbly;->a()V

    .line 36
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->l:Z

    .line 37
    invoke-super {p0}, Lbsz;->onResume()V

    .line 38
    invoke-direct {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->f()V

    .line 39
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/android/dialer/app/calllog/CallLogActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->m:I

    .line 45
    iget-object v0, p0, Lcom/android/dialer/app/calllog/CallLogActivity;->i:Landroid/support/v4/view/ViewPager;

    .line 46
    iget v0, v0, Landroid/support/v4/view/ViewPager;->c:I

    .line 47
    invoke-direct {p0, v0}, Lcom/android/dialer/app/calllog/CallLogActivity;->f(I)V

    .line 48
    :cond_0
    invoke-super {p0}, Lbsz;->onStop()V

    .line 49
    return-void
.end method
