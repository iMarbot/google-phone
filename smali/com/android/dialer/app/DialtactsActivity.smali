.class public Lcom/android/dialer/app/DialtactsActivity;
.super Lbsz;
.source "PG"

# interfaces
.implements Laie;
.implements Landroid/support/v4/view/ViewPager$f;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lany;
.implements Laom;
.implements Larq;
.implements Lars;
.implements Lart;
.implements Lask;
.implements Lauq;
.implements Lbgc$a;
.implements Lbia;
.implements Lbkd$a;
.implements Lbkd$b;
.implements Lboi$a;
.implements Lcom/android/dialer/dialpadview/DialpadFragment$a;
.implements Lcom/android/dialer/dialpadview/DialpadFragment$b;
.implements Lif;


# annotations
.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/app/DialtactsActivity$a;,
        Lcom/android/dialer/app/DialtactsActivity$b;
    }
.end annotation


# static fields
.field private static x:J

.field private static y:Lgtm;


# instance fields
.field private A:Landroid/view/animation/Animation;

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:Landroid/widget/PopupMenu;

.field private L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

.field private M:Landroid/view/View;

.field private N:Ljava/lang/String;

.field private O:Lbgl;

.field private P:Z

.field private Q:J

.field private R:Z

.field private S:Z

.field private T:Lamr;

.field private U:Lamr;

.field private V:Landroid/text/TextWatcher;

.field private W:Landroid/view/View$OnClickListener;

.field private X:I

.field private Y:Ljava/lang/String;

.field public f:Lcom/android/dialer/dialpadview/DialpadFragment;

.field public g:Landroid/support/design/widget/CoordinatorLayout;

.field public h:Lasd;

.field public i:Lasm;

.field public j:Lboi;

.field public k:Larl;

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Landroid/widget/EditText;

.field public p:Ljava/lang/String;

.field public q:Larj;

.field public r:Laun;

.field public s:Lbud;

.field public t:Lblw;

.field public u:Lblt;

.field public v:I

.field private z:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 911
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/dialer/app/DialtactsActivity;->x:J

    .line 912
    sget-object v0, Lgsz;->a:Lgsz;

    .line 913
    sput-object v0, Lcom/android/dialer/app/DialtactsActivity;->y:Lgtm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lbsz;-><init>()V

    .line 2
    new-instance v0, Lamz;

    invoke-direct {v0, p0}, Lamz;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->T:Lamr;

    .line 3
    new-instance v0, Lana;

    invoke-direct {v0, p0}, Lana;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->U:Lamr;

    .line 4
    new-instance v0, Lanb;

    invoke-direct {v0, p0}, Lanb;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->V:Landroid/text/TextWatcher;

    .line 5
    new-instance v0, Lanc;

    invoke-direct {v0, p0}, Lanc;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->W:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private final E()Ltv;
    .locals 1

    .prologue
    .line 118
    .line 119
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltv;

    return-object v0
.end method

.method private final F()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 483
    const/4 v0, 0x0

    .line 484
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    if-eqz v1, :cond_2

    .line 485
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 488
    :cond_0
    :goto_0
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v0, v4, v3

    if-eqz v0, :cond_3

    .line 489
    invoke-virtual {v0}, Lasg;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v2

    .line 490
    if-eqz v0, :cond_4

    .line 491
    invoke-virtual {v0, v2}, Lasg;->i(Z)V

    .line 503
    :cond_1
    :goto_2
    return-void

    .line 486
    :cond_2
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    if-eqz v1, :cond_0

    .line 487
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    goto :goto_0

    :cond_3
    move v1, v3

    .line 489
    goto :goto_1

    .line 492
    :cond_4
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    if-eqz v0, :cond_1

    .line 493
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 494
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d004c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 495
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f020184

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 497
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 498
    if-eqz v0, :cond_5

    sub-int v0, v2, v4

    .line 500
    :goto_3
    iget-boolean v5, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 501
    if-eqz v5, :cond_6

    .line 502
    :goto_4
    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    invoke-virtual {v2, v0, v3, v1}, Lboi;->a(III)V

    goto :goto_2

    :cond_5
    move v0, v3

    .line 498
    goto :goto_3

    .line 501
    :cond_6
    sub-int v3, v2, v4

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 6
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/DialtactsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7
    const-string v1, "ACTION_SHOW_TAB"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 8
    const-string v1, "EXTRA_SHOW_TAB"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 9
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "intent"

    .line 10
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 11
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "DialtactsActivity"

    .line 12
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "EXTRA_SHOW_TAB"

    .line 13
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 16
    return-object v0
.end method

.method private final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 519
    .line 520
    const-string v0, "android.intent.action.CALL_BUTTON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 521
    if-eqz v0, :cond_1

    .line 522
    invoke-static {p0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    .line 523
    if-eqz v0, :cond_1

    .line 524
    invoke-static {p0, v2}, Lbsp;->a(Landroid/content/Context;Z)V

    move v0, v1

    .line 527
    :goto_0
    if-eqz v0, :cond_2

    .line 528
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->finish()V

    .line 567
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 526
    goto :goto_0

    .line 530
    :cond_2
    const-string v0, "ACTION_SHOW_TAB"

    .line 531
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 533
    invoke-static {p0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    .line 534
    if-eqz v0, :cond_5

    .line 535
    invoke-static {p1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 536
    :goto_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 537
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 538
    const-string v4, "android.intent.action.DIAL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.android.phone.action.TOUCH_DIALER"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_3
    move v3, v1

    .line 545
    :goto_3
    if-eqz v3, :cond_8

    move v3, v1

    .line 546
    :goto_4
    invoke-static {p1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Landroid/content/Intent;)Z

    move-result v4

    .line 547
    if-nez v0, :cond_4

    if-nez v3, :cond_4

    if-eqz v4, :cond_9

    .line 548
    :cond_4
    const-string v5, "DialtactsActivity.displayFragment"

    const-string v6, "show dialpad fragment (showDialpadChooser: %b, isDialIntent: %b, isAddCallIntent: %b)"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    .line 549
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v2

    .line 550
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v3, 0x2

    .line 551
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v7, v3

    .line 552
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    invoke-direct {p0, v2}, Lcom/android/dialer/app/DialtactsActivity;->e(Z)V

    .line 554
    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 555
    iput-boolean v1, v2, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    .line 556
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 557
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->H:Z

    goto :goto_1

    :cond_5
    move v0, v2

    .line 535
    goto :goto_2

    .line 540
    :cond_6
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 541
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 542
    if-eqz v3, :cond_7

    const-string v4, "tel"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v1

    .line 543
    goto :goto_3

    :cond_7
    move v3, v2

    .line 544
    goto :goto_3

    :cond_8
    move v3, v2

    .line 545
    goto :goto_4

    .line 558
    :cond_9
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->S:Z

    if-eqz v0, :cond_0

    .line 560
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 561
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "last_tab"

    .line 562
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 563
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    if-eqz v1, :cond_a

    .line 564
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v1, v0}, Larl;->c(I)V

    .line 565
    invoke-static {v0}, Lbly;->b(I)V

    goto/16 :goto_1

    .line 566
    :cond_a
    invoke-static {v2}, Lbly;->b(I)V

    goto/16 :goto_1
.end method

.method private final e(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 368
    const-string v0, "DialtactActivity.showDialpadFragment"

    const-string v1, "animate: %b"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_0

    .line 370
    const-string v0, "DialtactsActivity.showDialpadFragment"

    const-string v1, "dialpad already shown"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 401
    :goto_0
    return-void

    .line 372
    :cond_0
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    if-eqz v0, :cond_1

    .line 373
    const-string v0, "DialtactsActivity.showDialpadFragment"

    const-string v1, "state already saved"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 375
    :cond_1
    iput-boolean v4, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 376
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, v5}, Larl;->setUserVisibleHint(Z)V

    .line 377
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-nez v1, :cond_2

    .line 379
    new-instance v1, Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-direct {v1}, Lcom/android/dialer/dialpadview/DialpadFragment;-><init>()V

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 380
    const v1, 0x7f0e019b

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    const-string v3, "dialpad"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 382
    :goto_1
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 383
    iput-boolean p1, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    .line 384
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lblb$a;->b:Lblb$a;

    invoke-interface {v1, v2, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 385
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 387
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 388
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 389
    const/4 v1, 0x0

    .line 390
    invoke-virtual {v0, v1, v4}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 391
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->o()V

    .line 392
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 393
    iget-object v1, v0, Laun;->a:Lauq;

    invoke-interface {v1}, Lauq;->j()Z

    move-result v1

    const/16 v2, 0x12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "isInSearchUi "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, v0, Laun;->a:Lauq;

    invoke-interface {v1}, Lauq;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 395
    invoke-virtual {v0, v4, v4}, Laun;->a(ZZ)V

    .line 399
    :goto_2
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 400
    const v0, 0x7f1101d2

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->setTitle(I)V

    goto/16 :goto_0

    .line 381
    :cond_2
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 396
    :cond_3
    iget-object v1, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    iget-object v0, v0, Laun;->d:Lamq;

    .line 397
    const/16 v2, 0xc8

    invoke-static {v1, v2, v0}, Lamn;->a(Landroid/view/View;ILamq;)V

    .line 398
    iput-boolean v4, v1, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    goto :goto_2
.end method


# virtual methods
.method public final A()V
    .locals 0

    .prologue
    .line 889
    return-void
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 903
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->R:Z

    return v0
.end method

.method public final C()Lbbf$a;
    .locals 1

    .prologue
    .line 904
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_0

    .line 905
    sget-object v0, Lbbf$a;->c:Lbbf$a;

    .line 907
    :goto_0
    return-object v0

    .line 906
    :cond_0
    sget-object v0, Lbbf$a;->g:Lbbf$a;

    goto :goto_0
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Lbss;->b(Landroid/view/View;)V

    .line 909
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    .line 910
    return-void
.end method

.method public a(Landroid/view/View;)Lcom/android/dialer/app/DialtactsActivity$b;
    .locals 2

    .prologue
    .line 508
    new-instance v0, Lcom/android/dialer/app/DialtactsActivity$b;

    invoke-direct {v0, p0, p0, p1}, Lcom/android/dialer/app/DialtactsActivity$b;-><init>(Lcom/android/dialer/app/DialtactsActivity;Landroid/content/Context;Landroid/view/View;)V

    .line 509
    const v1, 0x7f130004

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/DialtactsActivity$b;->inflate(I)V

    .line 510
    invoke-virtual {v0, p0}, Lcom/android/dialer/app/DialtactsActivity$b;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 511
    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 865
    return-void
.end method

.method public final a(IFI)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 839
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 840
    iget v0, v0, Larl;->f:I

    .line 842
    invoke-static {}, Lbib;->i()Z

    move-result v1

    .line 843
    if-nez v1, :cond_1

    if-nez v0, :cond_1

    iget-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->G:Z

    if-nez v2, :cond_1

    .line 844
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    invoke-virtual {v0, p2}, Lbud;->a(F)V

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->G:Z

    if-nez v1, :cond_2

    .line 846
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    sub-float v1, v3, p2

    invoke-virtual {v0, v1}, Lbud;->a(F)V

    goto :goto_0

    .line 847
    :cond_2
    if-eqz v0, :cond_0

    .line 848
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    invoke-virtual {v0, v3}, Lbud;->a(F)V

    goto :goto_0
.end method

.method public final a(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 2

    .prologue
    .line 796
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Larl;->a(Z)V

    .line 797
    return-void
.end method

.method public final a(Landroid/net/Uri;ZLbbj;)V
    .locals 1

    .prologue
    .line 819
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    .line 820
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p3}, Lbkd;->a(Lbsz;Landroid/net/Uri;ZLbbj;)V

    .line 821
    return-void
.end method

.method public final a(Larj;)V
    .locals 1

    .prologue
    .line 802
    iput-object p1, p0, Lcom/android/dialer/app/DialtactsActivity;->q:Larj;

    .line 803
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 804
    iget-object v0, v0, Larl;->d:Lcom/android/dialer/app/list/RemoveView;

    .line 806
    iput-object p1, v0, Lcom/android/dialer/app/list/RemoveView;->a:Larj;

    .line 807
    return-void
.end method

.method public final a(Lbic;)V
    .locals 4

    .prologue
    .line 402
    new-instance v0, Laob;

    invoke-direct {v0}, Laob;-><init>()V

    new-instance v0, Laoc;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 403
    new-instance v1, Lamw;

    invoke-direct {v1, p1}, Lamw;-><init>(Lbic;)V

    .line 404
    invoke-direct {v0, p0, v1}, Laoc;-><init>(Landroid/content/Context;Laoe;)V

    .line 406
    invoke-static {}, Lbdf;->b()V

    .line 407
    new-instance v1, Laod;

    iget-object v2, v0, Laoc;->b:Laoe;

    invoke-direct {v1, v2}, Laod;-><init>(Laoe;)V

    const/4 v2, 0x1

    new-array v2, v2, [Laoc;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Laod;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 409
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 732
    iput-object p1, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    .line 733
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 735
    iput-object p1, v0, Lasg;->r:Ljava/lang/String;

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    .line 738
    iput-object p1, v0, Lboi;->b:Ljava/lang/String;

    .line 739
    :cond_1
    sget-object v0, Lbsa;->a:Lbry;

    .line 740
    invoke-static {p1, v0}, Lbsa;->a(Ljava/lang/String;Lbry;)Ljava/lang/String;

    move-result-object v0

    .line 741
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 742
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->isVisible()Z

    move-result v1

    if-nez v1, :cond_4

    .line 743
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 744
    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->J:Ljava/lang/String;

    .line 770
    :cond_3
    :goto_0
    return-void

    .line 746
    :cond_4
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 747
    :cond_5
    :try_start_0
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 748
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 749
    const-string v2, "01189998819991197253"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 750
    iget-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    if-nez v0, :cond_6

    .line 751
    new-instance v0, Lbif;

    new-instance v2, Lbij;

    invoke-direct {v2, v1}, Lbij;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    invoke-direct {v0, v2}, Lbif;-><init>(Lbij;)V

    iput-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    .line 752
    :cond_6
    iget-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    .line 753
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_7

    .line 754
    const v1, -0xffff01

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 755
    const/high16 v2, -0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 756
    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    .line 757
    invoke-static {v3, v4}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    .line 758
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    new-instance v2, Lbig;

    invoke-direct {v2, v0}, Lbig;-><init>(Lbif;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 759
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    new-instance v2, Lbih;

    invoke-direct {v2, v0}, Lbih;-><init>(Lbif;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 760
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 761
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 762
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 763
    :cond_7
    iget-object v1, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 764
    iget-object v0, v0, Lbif;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 766
    :cond_8
    iget-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    if-eqz v0, :cond_3

    .line 767
    iget-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    invoke-virtual {v0}, Lbif;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;ZLbbj;)V
    .locals 2

    .prologue
    .line 822
    if-nez p1, :cond_0

    .line 823
    const-string p1, ""

    .line 824
    :cond_0
    new-instance v0, Lbbh;

    invoke-direct {v0, p1, p3}, Lbbh;-><init>(Ljava/lang/String;Lbbj;)V

    .line 826
    iput-boolean p2, v0, Lbbh;->c:Z

    .line 830
    iget-boolean v1, p3, Lbbj;->q:Z

    .line 832
    iput-boolean v1, v0, Lbbh;->e:Z

    .line 834
    invoke-static {p0, v0}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 835
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    .line 836
    return-void
.end method

.method public final a(ZLjava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 575
    const-string v0, "DialtactsActivity.enterSearchUi"

    const-string v1, "smart dial: %b"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    :cond_0
    const-string v0, "DialtactsActivity.enterSearchUi"

    const-string v1, "not entering search UI (mStateSaved: %b, isDestroyed: %b)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v5, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    .line 578
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v4

    .line 579
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 580
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 642
    :goto_0
    return-void

    .line 582
    :cond_1
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 583
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    if-eqz v0, :cond_6

    .line 584
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    invoke-virtual {v5, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 587
    :cond_2
    :goto_1
    iput-boolean v4, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    .line 588
    iput-boolean v4, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    .line 589
    iput-boolean v4, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    .line 591
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_new_search_fragment"

    invoke-interface {v0, v1, v4}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v6

    .line 592
    if-eqz v6, :cond_7

    .line 593
    const-string v0, "new_search"

    .line 594
    iput-boolean v3, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    move-object v2, v0

    .line 600
    :goto_2
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 601
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 602
    const/4 v1, 0x0

    .line 603
    invoke-virtual {v0, v1, v3}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 604
    if-eqz p3, :cond_9

    .line 605
    const/high16 v0, 0x10b0000

    invoke-virtual {v5, v0, v4}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    .line 607
    :goto_3
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 608
    if-nez v1, :cond_d

    .line 609
    if-eqz v6, :cond_b

    .line 611
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 612
    if-nez v0, :cond_a

    move v0, v3

    .line 613
    :goto_4
    new-instance v1, Lboi;

    invoke-direct {v1}, Lboi;-><init>()V

    .line 614
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 615
    const-string v8, "use_zero_suggest"

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 616
    invoke-virtual {v1, v7}, Lboi;->setArguments(Landroid/os/Bundle;)V

    .line 625
    :goto_5
    const v0, 0x7f0e019c

    invoke-virtual {v5, v0, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 627
    :goto_6
    invoke-virtual {v1, v4}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 628
    if-nez v6, :cond_3

    move-object v0, v1

    .line 629
    check-cast v0, Lasg;

    .line 630
    invoke-virtual {v0, v3}, Lasg;->e(Z)V

    .line 631
    :cond_3
    if-nez p1, :cond_e

    if-nez v6, :cond_e

    .line 632
    check-cast v1, Lasg;

    invoke-virtual {v1, p2}, Lasg;->a(Ljava/lang/String;)V

    .line 635
    :cond_4
    :goto_7
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    .line 636
    if-eqz p3, :cond_5

    .line 637
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 638
    :cond_5
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, v4}, Larl;->setUserVisibleHint(Z)V

    .line 639
    if-eqz p1, :cond_f

    .line 640
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->h:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 585
    :cond_6
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    if-eqz v0, :cond_2

    .line 586
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    invoke-virtual {v5, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_1

    .line 595
    :cond_7
    if-eqz p1, :cond_8

    .line 596
    const-string v0, "smartdial"

    .line 597
    iput-boolean v3, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    move-object v2, v0

    goto/16 :goto_2

    .line 598
    :cond_8
    const-string v0, "search"

    .line 599
    iput-boolean v3, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    move-object v2, v0

    goto/16 :goto_2

    .line 606
    :cond_9
    invoke-virtual {v5, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    goto/16 :goto_3

    :cond_a
    move v0, v4

    .line 612
    goto :goto_4

    .line 619
    :cond_b
    if-eqz p1, :cond_c

    .line 620
    new-instance v1, Lasm;

    invoke-direct {v1}, Lasm;-><init>()V

    goto :goto_5

    .line 621
    :cond_c
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->d(Landroid/content/Context;)Laqv;

    move-result-object v0

    invoke-interface {v0}, Laqv;->a()Lasd;

    move-result-object v1

    move-object v0, v1

    .line 622
    check-cast v0, Lasg;

    new-instance v7, Lamx;

    invoke-direct {v7, p0}, Lamx;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    .line 624
    iput-object v7, v0, Lasg;->q:Landroid/view/View$OnTouchListener;

    goto/16 :goto_5

    .line 626
    :cond_d
    invoke-virtual {v5, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_6

    .line 633
    :cond_e
    if-eqz v6, :cond_4

    .line 634
    check-cast v1, Lboi;

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->C()Lbbf$a;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Lboi;->a(Ljava/lang/String;Lbbf$a;)V

    goto/16 :goto_7

    .line 641
    :cond_f
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->g:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto/16 :goto_0
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 423
    const-string v0, "DialtactsActivity.hideDialpadFragment"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    if-eqz p2, :cond_2

    .line 427
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 429
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    .line 431
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setImportantForAccessibility(I)V

    .line 432
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a()V

    .line 433
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 435
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    .line 437
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setImportantForAccessibility(I)V

    .line 438
    :cond_2
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_0

    .line 440
    iput-boolean v4, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 441
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 442
    iput-boolean p1, v0, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    .line 443
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, v5}, Larl;->setUserVisibleHint(Z)V

    .line 444
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->c()V

    .line 445
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->F()V

    .line 446
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->z()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lbud;->a(IZ)V

    .line 447
    if-eqz p1, :cond_6

    .line 448
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 450
    :goto_1
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 451
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v0, Laun;->a:Lauq;

    .line 452
    invoke-interface {v2}, Lauq;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, v0, Laun;->a:Lauq;

    .line 453
    invoke-interface {v2}, Lauq;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 455
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 456
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    iget-object v3, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 458
    iget-boolean v3, v3, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 459
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 460
    iget-object v1, v0, Laun;->a:Lauq;

    invoke-interface {v1}, Lauq;->j()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 461
    iget-object v1, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 462
    iget-boolean v1, v1, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 463
    if-eqz v1, :cond_3

    .line 464
    iget-object v1, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v1, v5}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(Z)V

    .line 465
    :cond_3
    iget-object v1, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 466
    iget-boolean v1, v1, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 467
    if-nez v1, :cond_4

    .line 468
    iget-object v1, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v1, v4, v4}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(ZZ)V

    .line 469
    :cond_4
    invoke-virtual {v0, v4, v5}, Laun;->a(ZZ)V

    .line 470
    :cond_5
    const v0, 0x7f1101d1

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->setTitle(I)V

    goto/16 :goto_0

    .line 449
    :cond_6
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->i()V

    goto :goto_1
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 850
    .line 851
    iget v0, p0, Lcom/android/dialer/app/DialtactsActivity;->v:I

    if-ne v0, v3, :cond_0

    .line 852
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->b()V

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 854
    iget v0, v0, Larl;->f:I

    .line 856
    iget v1, p0, Lcom/android/dialer/app/DialtactsActivity;->v:I

    if-eq v0, v1, :cond_1

    .line 857
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 858
    iget-object v1, v1, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 860
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 861
    :cond_1
    const-string v1, "DialtactsActivity.onPageSelected"

    const-string v2, "tabIndex: %d"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 862
    iput v0, p0, Lcom/android/dialer/app/DialtactsActivity;->v:I

    .line 863
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/dialer/app/DialtactsActivity;->Q:J

    .line 864
    return-void
.end method

.method public final b(IILcom/android/dialer/app/list/PhoneFavoriteSquareTileView;)V
    .locals 0

    .prologue
    .line 798
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 813
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 815
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 816
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    invoke-virtual {v0, p1}, Lbud;->a(Z)V

    .line 818
    :cond_1
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 901
    iput-boolean p1, p0, Lcom/android/dialer/app/DialtactsActivity;->R:Z

    .line 902
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 18
    sget-object v0, Lbsy;->a:Lbsy;

    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lbsy;->a(II)V

    .line 20
    :cond_0
    invoke-super {p0, p1}, Lbsz;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 791
    invoke-static {p1}, Lbly;->a(I)V

    .line 792
    if-ne p1, v1, :cond_0

    .line 793
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 794
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Lbss;->b(Landroid/view/View;)V

    .line 795
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 290
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/settings/DialerSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->startActivity(Landroid/content/Intent;)V

    .line 292
    return-void
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 879
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->E()Ltv;

    move-result-object v0

    invoke-virtual {v0, p1}, Ltv;->c(I)V

    .line 880
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 410
    const-string v0, "DialtactsActivity.onDialpadShown"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 413
    iget-boolean v0, v0, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    .line 414
    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 418
    :goto_0
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->F()V

    .line 419
    return-void

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 417
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment$DialpadSlidingRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment$DialpadSlidingRelativeLayout;->setYFraction(F)V

    goto :goto_0
.end method

.method public final g(I)V
    .locals 2

    .prologue
    .line 890
    packed-switch p1, :pswitch_data_0

    .line 892
    const/16 v0, 0x29

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "PhoneNumberInteraction error: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    .line 893
    :pswitch_0
    return-void

    .line 890
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    .line 421
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 473
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 477
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 479
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 481
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 482
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 506
    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 507
    const v0, 0x7f110132

    return v0
.end method

.method public final n()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 643
    const-string v0, "DialtactsActivity.exitSearchUi"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    if-eqz v0, :cond_1

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 646
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 647
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_2

    .line 648
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a()V

    .line 650
    :cond_2
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    .line 651
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    .line 652
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    .line 653
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 654
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->isShown()Z

    move-result v0

    .line 655
    if-eqz v0, :cond_b

    .line 656
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->z()I

    move-result v0

    if-eq v0, v6, :cond_b

    .line 657
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    new-instance v3, Land;

    invoke-direct {v3, p0}, Land;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    .line 658
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 659
    invoke-virtual {v0, v3, v2}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 674
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 675
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    if-eqz v3, :cond_4

    .line 676
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 677
    :cond_4
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    if-eqz v3, :cond_5

    .line 678
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 679
    :cond_5
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    if-eqz v3, :cond_6

    .line 680
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 681
    :cond_6
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 682
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 683
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_8

    .line 684
    :cond_7
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->c()V

    .line 685
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, v2}, Larl;->setUserVisibleHint(Z)V

    .line 686
    :cond_8
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 687
    iget v0, v0, Larl;->f:I

    .line 688
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->b(I)V

    .line 689
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 690
    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 692
    iget-boolean v4, v4, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 693
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v4, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 695
    iget-boolean v4, v4, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 696
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v2

    .line 697
    iget-object v3, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 698
    iget-boolean v3, v3, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 699
    if-eqz v3, :cond_9

    .line 700
    iget-object v3, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v3, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b(Z)V

    .line 701
    :cond_9
    iget-object v2, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 702
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 703
    if-eqz v2, :cond_a

    .line 704
    iget-object v2, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 705
    const/16 v3, 0xc8

    invoke-static {v2, v3}, Lamn;->b(Landroid/view/View;I)V

    .line 706
    iput-boolean v1, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 707
    :cond_a
    invoke-virtual {v0, v1, v1}, Laun;->a(ZZ)V

    goto/16 :goto_0

    .line 661
    :cond_b
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 662
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->isShown()Z

    move-result v0

    .line 663
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 664
    iget-object v3, v0, Larl;->e:Landroid/app/Fragment;

    instance-of v3, v3, Lapu;

    if-eqz v3, :cond_c

    iget-object v0, v0, Larl;->e:Landroid/app/Fragment;

    check-cast v0, Lapu;

    .line 665
    invoke-virtual {v0}, Lapu;->j()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 668
    :goto_2
    if-eqz v0, :cond_3

    .line 669
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 670
    iget v0, v0, Larl;->f:I

    .line 671
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(IFI)V

    .line 672
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    new-instance v3, Lamy;

    invoke-direct {v3, p0}, Lamy;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    const-wide/16 v4, 0x12c

    .line 673
    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 667
    goto :goto_2
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/dialer/app/DialtactsActivity;->a(ZLjava/lang/String;Z)V

    .line 731
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 336
    const-string v0, "DialtactsActivity.onActivityResult"

    const-string v1, "requestCode:%d, resultCode:%d"

    new-array v2, v7, [Ljava/lang/Object;

    .line 337
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 338
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 339
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    if-ne p1, v5, :cond_3

    .line 341
    if-ne p2, v6, :cond_2

    .line 342
    const-string v0, "android.speech.extra.RESULTS"

    .line 343
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 345
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->Y:Ljava/lang/String;

    .line 366
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lbsz;->onActivityResult(IILandroid/content/Intent;)V

    .line 367
    return-void

    .line 346
    :cond_1
    const-string v0, "DialtactsActivity.onActivityResult"

    const-string v1, "voice search - nothing heard"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 348
    :cond_2
    const-string v0, "DialtactsActivity.onActivityResult"

    const-string v1, "voice search failed"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 349
    :cond_3
    if-ne p1, v7, :cond_5

    .line 350
    if-ne p2, v5, :cond_4

    .line 351
    const-string v0, "DialtactsActivity.onActivityResult"

    const-string v1, "returned from call composer, error occurred"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    const v0, 0x7f11006c

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "contact_name"

    .line 353
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 354
    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/app/DialtactsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v1, v0, v4}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Lbo;->a()V

    goto :goto_0

    .line 357
    :cond_4
    const-string v0, "DialtactsActivity.onActivityResult"

    const-string v1, "returned from call composer, no error"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    :cond_5
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 359
    if-ne p2, v6, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "has_enriched_call_data"

    .line 360
    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, "phone_number"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    const v2, 0x7f110163

    invoke-virtual {p0, v2}, Lcom/android/dialer/app/DialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-static {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->a(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    const v2, 0x7f110334

    new-instance v3, Lamv;

    invoke-direct {v3, p0, v0}, Lamv;-><init>(Lcom/android/dialer/app/DialtactsActivity;Ljava/lang/String;)V

    .line 363
    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->a(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 364
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/Snackbar;->c(I)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 365
    invoke-virtual {v0}, Lbo;->a()V

    goto/16 :goto_0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 4

    .prologue
    .line 259
    const-string v0, "DialtactsActivity.onAttachFragment"

    const-string v1, "fragment: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    instance-of v0, p1, Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 261
    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 281
    :cond_0
    :goto_0
    instance-of v0, p1, Lasg;

    if-eqz v0, :cond_1

    .line 282
    check-cast p1, Lasg;

    .line 283
    new-instance v0, Laih;

    invoke-direct {v0, p0, p1}, Laih;-><init>(Lcom/android/dialer/app/DialtactsActivity;Lasg;)V

    .line 284
    invoke-static {}, Lbdf;->b()V

    .line 285
    iput-object v0, p1, Laig;->m:Laih;

    .line 286
    new-instance v0, Laii;

    invoke-direct {v0, p0, p1}, Laii;-><init>(Lcom/android/dialer/app/DialtactsActivity;Lasg;)V

    .line 287
    invoke-static {}, Lbdf;->b()V

    .line 288
    iget-object v1, p1, Laig;->l:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    :cond_1
    return-void

    .line 262
    :cond_2
    instance-of v0, p1, Lasm;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 263
    check-cast v0, Lasm;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 264
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 265
    iput-object p0, v0, Laig;->j:Laie;

    .line 266
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    .line 268
    iput-object v1, v0, Lasg;->r:Ljava/lang/String;

    goto :goto_0

    .line 270
    :cond_3
    instance-of v0, p1, Lasg;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 271
    check-cast v0, Lasd;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    .line 272
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->h:Lasd;

    .line 273
    iput-object p0, v0, Laig;->j:Laie;

    goto :goto_0

    .line 275
    :cond_4
    instance-of v0, p1, Larl;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 276
    check-cast v0, Larl;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 277
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, p0}, Larl;->a(Landroid/support/v4/view/ViewPager$f;)V

    goto :goto_0

    .line 278
    :cond_5
    instance-of v0, p1, Lboi;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 279
    check-cast v0, Lboi;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->j:Lboi;

    .line 280
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->F()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 709
    sget-object v0, Lbld$a;->f:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 710
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    if-eqz v0, :cond_1

    .line 722
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_2

    .line 713
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 714
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 715
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    goto :goto_0

    .line 716
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 717
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->C:Z

    if-eqz v0, :cond_3

    .line 718
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Lbss;->b(Landroid/view/View;)V

    .line 719
    sget-object v0, Lbld$a;->r:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    goto :goto_0

    .line 720
    :cond_3
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    goto :goto_0

    .line 721
    :cond_4
    invoke-super {p0}, Lbsz;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 294
    const v1, 0x7f0e019d

    if-ne v0, v1, :cond_1

    .line 295
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-nez v0, :cond_0

    .line 296
    const-string v0, "DialtactsActivity.onClick"

    const-string v1, "floating action button clicked, going to show dialpad"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    sget-object v0, Lbld$a;->n:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 298
    iput-boolean v3, p0, Lcom/android/dialer/app/DialtactsActivity;->H:Z

    .line 299
    invoke-direct {p0, v4}, Lcom/android/dialer/app/DialtactsActivity;->e(Z)V

    .line 300
    invoke-static {}, Lbib;->d()V

    .line 312
    :goto_0
    return-void

    .line 301
    :cond_0
    const-string v0, "DialtactsActivity.onClick"

    const-string v1, "floating action button clicked, but dialpad is already showing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 302
    :cond_1
    const v1, 0x7f0e025b

    if-ne v0, v1, :cond_2

    .line 303
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/app/DialtactsActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    const v0, 0x7f110341

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 307
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 309
    :cond_2
    const v1, 0x7f0e025c

    if-ne v0, v1, :cond_3

    .line 310
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->K:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0

    .line 311
    :cond_3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected onClick event from "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 723
    invoke-super {p0, p1}, Lbsz;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 724
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v1, :cond_1

    .line 725
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->C:Z

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 726
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 727
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->C:Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 21
    const-string v0, "DialtactsActivity.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 22
    invoke-super {p0, p1}, Lbsz;->onCreate(Landroid/os/Bundle;)V

    .line 23
    iput-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    .line 24
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "last_tab_enabled"

    invoke-interface {v0, v1, v3}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->S:Z

    .line 25
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 26
    const v1, 0x7f0d004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/app/DialtactsActivity;->X:I

    .line 27
    const v0, 0x7f040057

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->E()Ltv;

    move-result-object v0

    .line 30
    const v1, 0x7f0400ae

    invoke-virtual {v0, v1}, Ltv;->a(I)V

    .line 31
    invoke-virtual {v0, v2}, Ltv;->d(Z)V

    .line 32
    invoke-virtual {v0, v4}, Ltv;->b(Landroid/graphics/drawable/Drawable;)V

    .line 33
    invoke-virtual {v0}, Ltv;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0e0257

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/app/widget/SearchEditTextLayout;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 34
    new-instance v0, Laun;

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-direct {v0, p0, v1}, Laun;-><init>(Lauq;Lcom/android/dialer/app/widget/SearchEditTextLayout;)V

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 35
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v1, 0x7f0e0253

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    .line 36
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->V:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 37
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 38
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v1, 0x7f0e025b

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->M:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v1, 0x7f0e0258

    .line 40
    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->W:Landroid/view/View$OnClickListener;

    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v1, 0x7f0e0252

    .line 43
    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lams;

    invoke-direct {v1, p0}, Lams;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->G:Z

    .line 47
    iput v3, p0, Lcom/android/dialer/app/DialtactsActivity;->v:I

    .line 48
    const v0, 0x7f0e019d

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    .line 49
    invoke-virtual {v0, p0}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    new-instance v1, Lbud;

    invoke-direct {v1, p0, v0}, Lbud;-><init>(Landroid/app/Activity;Landroid/support/design/widget/FloatingActionButton;)V

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 51
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v4, 0x7f0e025c

    .line 52
    invoke-virtual {v1, v4}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 53
    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    invoke-virtual {p0, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/view/View;)Lcom/android/dialer/app/DialtactsActivity$b;

    move-result-object v4

    iput-object v4, p0, Lcom/android/dialer/app/DialtactsActivity;->K:Landroid/widget/PopupMenu;

    .line 55
    iget-object v4, p0, Lcom/android/dialer/app/DialtactsActivity;->K:Landroid/widget/PopupMenu;

    invoke-virtual {v4}, Landroid/widget/PopupMenu;->getDragToOpenListener()Landroid/view/View$OnTouchListener;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 56
    if-nez p1, :cond_3

    .line 57
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0e019c

    new-instance v4, Larl;

    invoke-direct {v4}, Larl;-><init>()V

    const-string v5, "favorites"

    .line 59
    invoke-virtual {v1, v2, v4, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 92
    :cond_0
    :goto_1
    invoke-static {}, Lbib;->i()Z

    move-result v2

    .line 93
    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->G:Z

    if-eqz v1, :cond_9

    .line 95
    if-eqz v2, :cond_7

    const v1, 0x7f05000f

    .line 96
    :goto_2
    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->z:Landroid/view/animation/Animation;

    .line 98
    if-eqz v2, :cond_8

    const v1, 0x7f050012

    .line 99
    :goto_3
    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    .line 102
    :goto_4
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->z:Landroid/view/animation/Animation;

    sget-object v2, Lamn;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 103
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    sget-object v2, Lamn;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 104
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->z:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->T:Lamr;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 105
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->U:Lamr;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 106
    const v1, 0x7f0e019a

    invoke-virtual {p0, v1}, Lcom/android/dialer/app/DialtactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CoordinatorLayout;

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    .line 107
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    new-instance v2, Lcom/android/dialer/app/DialtactsActivity$a;

    .line 108
    invoke-direct {v2, p0}, Lcom/android/dialer/app/DialtactsActivity$a;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    .line 109
    invoke-virtual {v1, v2}, Landroid/support/design/widget/CoordinatorLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 110
    new-instance v1, Lamt;

    invoke-direct {v1, p0}, Lamt;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    invoke-static {v0, v1}, Lbib;->a(Landroid/view/View;Lbtc;)V

    .line 111
    invoke-static {p0}, Lapw;->v(Landroid/content/Context;)Lbgi;

    move-result-object v0

    invoke-interface {v0, p0}, Lbgi;->a(Landroid/content/Context;)Lbgl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->O:Lbgl;

    .line 112
    invoke-static {p0}, Lbsb;->a(Landroid/content/Context;)V

    .line 113
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->y(Landroid/content/Context;)Lblw;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->t:Lblw;

    .line 114
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->x(Landroid/content/Context;)Lblt;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->u:Lblt;

    .line 115
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_new_search_fragment"

    invoke-interface {v0, v1, v3}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;->F()V

    .line 117
    :cond_1
    return-void

    :cond_2
    move v0, v3

    .line 46
    goto/16 :goto_0

    .line 61
    :cond_3
    const-string v1, "search_query"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    .line 62
    const-string v1, "dialpad_query"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    .line 63
    const-string v1, "in_regular_search_ui"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    .line 64
    const-string v1, "in_dialpad_search_ui"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    .line 65
    const-string v1, "in_new_search_ui"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    .line 66
    const-string v1, "first_launch"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    .line 67
    const-string v1, "was_configuration_change"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->P:Z

    .line 68
    const-string v1, "is_dialpad_shown"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    .line 69
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    const-string v4, "fab_visible"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v4}, Lbud;->a(Z)V

    .line 70
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 71
    const-string v4, "key_actionbar_is_slid_up"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v1, Laun;->c:Z

    .line 72
    const-string v4, "key_actionbar_is_faded_out"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 73
    if-eqz v4, :cond_5

    .line 74
    iget-object v2, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 75
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 76
    if-nez v2, :cond_4

    .line 77
    iget-object v2, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v2, v3}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(Z)V

    .line 82
    :cond_4
    :goto_5
    const-string v2, "key_actionbar_is_expanded"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 83
    if-eqz v2, :cond_6

    .line 84
    iget-object v2, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 85
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 86
    if-nez v2, :cond_0

    .line 87
    iget-object v1, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v1, v3, v3}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(ZZ)V

    goto/16 :goto_1

    .line 78
    :cond_5
    iget-object v4, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 79
    iget-boolean v4, v4, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 80
    if-eqz v4, :cond_4

    .line 81
    iget-object v4, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v4, v2}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a(Z)V

    goto :goto_5

    .line 88
    :cond_6
    iget-object v2, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 89
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 90
    if-eqz v2, :cond_0

    .line 91
    iget-object v1, v1, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-virtual {v1, v3}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b(Z)V

    goto/16 :goto_1

    .line 95
    :cond_7
    const v1, 0x7f050010

    goto/16 :goto_2

    .line 98
    :cond_8
    const v1, 0x7f050013

    goto/16 :goto_3

    .line 100
    :cond_9
    const v1, 0x7f05000e

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->z:Landroid/view/animation/Animation;

    .line 101
    const v1, 0x7f050011

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    goto/16 :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 512
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->J:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->J:Ljava/lang/String;

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    if-eqz v0, :cond_1

    .line 516
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 517
    iget-boolean v1, v0, Laun;->c:Z

    invoke-virtual {v0, v1, v2}, Laun;->a(ZZ)V

    .line 518
    :cond_1
    return v2
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 313
    .line 314
    iget-boolean v1, p0, Lbsz;->w:Z

    .line 315
    if-nez v1, :cond_0

    .line 335
    :goto_0
    return v0

    .line 317
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 318
    const v2, 0x7f0e02a2

    if-ne v1, v2, :cond_2

    .line 319
    sget-object v0, Lbld$a;->s:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 320
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/calllog/CallLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->startActivity(Landroid/content/Intent;)V

    .line 335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 322
    :cond_2
    const v2, 0x7f0e02a3

    if-ne v1, v2, :cond_3

    .line 323
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 324
    new-instance v2, Lagu;

    invoke-direct {v2}, Lagu;-><init>()V

    .line 325
    const-string v3, "clearFrequents"

    invoke-virtual {v2, v1, v3}, Lagu;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 326
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lblb$a;->l:Lblb$a;

    invoke-interface {v1, v2, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto :goto_0

    .line 328
    :cond_3
    const v2, 0x7f0e02a4

    if-ne v1, v2, :cond_4

    .line 329
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->f()V

    .line 330
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lblb$a;->j:Lblb$a;

    invoke-interface {v1, v2, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    goto :goto_0

    .line 332
    :cond_4
    const v2, 0x7f0e02a6

    if-ne v1, v2, :cond_1

    .line 333
    invoke-static {p0}, Lbli;->a(Landroid/content/Context;)Lbli;

    move-result-object v1

    invoke-virtual {v1}, Lbli;->a()Lblh;

    move-result-object v1

    invoke-virtual {v1, p0}, Lblh;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 568
    const-string v0, "DialtactsActivity.onNewIntent"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 569
    invoke-virtual {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->setIntent(Landroid/content/Intent;)V

    .line 570
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    .line 571
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    .line 572
    invoke-direct {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/content/Intent;)V

    .line 573
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->invalidateOptionsMenu()V

    .line 574
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 208
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    if-eqz v0, :cond_1

    .line 210
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 212
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    .line 213
    iput-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->F:Z

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 215
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->i()V

    .line 216
    :cond_2
    invoke-super {p0}, Lbsz;->onPause()V

    .line 217
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 5

    .prologue
    .line 894
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Permissions requested unexpectedly: %d/%s/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 895
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 896
    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 897
    invoke-static {p3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 898
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 899
    invoke-static {v0}, Lbdf;->a(Ljava/lang/String;)V

    .line 900
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Lbsz;->onRestart()V

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->E:Z

    .line 207
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    const-string v0, "DialtactsActivity.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 122
    invoke-super {p0}, Lbsz;->onResume()V

    .line 123
    invoke-static {p0}, Lbib;->B(Landroid/content/Context;)V

    .line 124
    sget-boolean v0, Lbly;->c:Z

    .line 125
    if-nez v0, :cond_0

    .line 126
    invoke-static {}, Lbly;->a()V

    .line 127
    :cond_0
    iput-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    .line 128
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    if-eqz v0, :cond_9

    .line 129
    const-string v0, "DialtactsActivity.onResume"

    const-string v3, "mFirstLaunch true, displaying fragment"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/content/Intent;)V

    .line 141
    :goto_0
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    const-string v0, "DialtactsActivity.onResume"

    const-string v3, "mDialpadFragment attached but not hidden, forcing hide"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->Y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 145
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    invoke-virtual {v0}, Laun;->a()V

    .line 146
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->o:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->Y:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iput-object v6, p0, Lcom/android/dialer/app/DialtactsActivity;->Y:Ljava/lang/String;

    .line 148
    :cond_2
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->E:Z

    if-eqz v0, :cond_4

    .line 149
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_3

    .line 150
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lblb$a;->b:Lblb$a;

    invoke-interface {v0, v3, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 151
    :cond_3
    iput-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->E:Z

    .line 153
    :cond_4
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 154
    sget-object v0, Lcom/android/dialer/app/DialtactsActivity;->y:Lgtm;

    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 155
    sget-object v0, Lcom/android/dialer/app/DialtactsActivity;->y:Lgtm;

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 162
    :goto_1
    iput-boolean v0, v3, Lcom/android/dialer/app/widget/SearchEditTextLayout;->g:Z

    .line 163
    iget-boolean v0, v3, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    invoke-virtual {v3, v0}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->c(Z)V

    .line 164
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->M:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->P:Z

    if-nez v0, :cond_5

    .line 166
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->O:Lbgl;

    .line 167
    iget-object v3, v0, Lbgl;->a:Landroid/content/Context;

    invoke-static {v3}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 168
    new-instance v3, Lbgq;

    .line 169
    invoke-direct {v3, v0}, Lbgq;-><init>(Lbgl;)V

    .line 170
    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v3, v0}, Lbgq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    :cond_5
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_e

    .line 172
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 173
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 175
    invoke-virtual {v0, v6, v1}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 178
    :goto_2
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    if-eqz v0, :cond_8

    .line 179
    const-string v0, "vnd.android.cursor.dir/calls"

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 180
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_f

    const-string v3, "android.provider.extra.CALL_TYPE_FILTER"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_f

    .line 182
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Larl;->c(I)V

    .line 183
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bo:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 194
    :cond_6
    :goto_3
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CLEAR_NEW_VOICEMAILS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 195
    const-string v0, "DialtactsActivity.onResume"

    const-string v1, "clearing all new voicemails"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    invoke-static {p0}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->a(Landroid/content/Context;)V

    .line 197
    :cond_7
    new-instance v0, Lamu;

    invoke-direct {v0, p0}, Lamu;-><init>(Lcom/android/dialer/app/DialtactsActivity;)V

    const-wide/16 v4, 0x3e8

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/Runnable;J)V

    .line 198
    :cond_8
    iput-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    .line 200
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    const v1, 0x7f0e025a

    invoke-virtual {v0, v1}, Lcom/android/dialer/app/widget/SearchEditTextLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 201
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    .line 202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/dialer/app/DialtactsActivity;->Q:J

    .line 203
    new-instance v0, Lblu;

    invoke-direct {v0}, Lblu;-><init>()V

    .line 204
    return-void

    .line 132
    :cond_9
    invoke-static {p0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    .line 133
    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->H:Z

    if-eqz v0, :cond_a

    .line 134
    const-string v0, "DialtactsActivity.onResume"

    const-string v3, "phone not in use, hiding dialpad fragment"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-virtual {p0, v2, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 136
    iput-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->H:Z

    goto/16 :goto_0

    .line 137
    :cond_a
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v0, :cond_b

    .line 138
    const-string v0, "DialtactsActivity.onResume"

    const-string v3, "showing dialpad on resume"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 139
    invoke-direct {p0, v2}, Lcom/android/dialer/app/DialtactsActivity;->e(Z)V

    goto/16 :goto_0

    .line 140
    :cond_b
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->g:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {p0, v0}, Lbib;->a(Landroid/app/Activity;Landroid/view/View;)V

    goto/16 :goto_0

    .line 156
    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 158
    const/high16 v5, 0x10000

    .line 159
    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_d

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    move v0, v1

    goto/16 :goto_1

    :cond_d
    move v0, v2

    goto/16 :goto_1

    .line 177
    :cond_e
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->z()I

    move-result v3

    invoke-virtual {v0, v3, v2}, Lbud;->a(IZ)V

    goto/16 :goto_2

    .line 184
    :cond_f
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0, v1}, Larl;->c(I)V

    goto/16 :goto_3

    .line 185
    :cond_10
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_SHOW_TAB"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 187
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_SHOW_TAB"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 188
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 189
    iget-object v1, v1, Larl;->c:Lari;

    invoke-virtual {v1}, Lari;->b()I

    move-result v1

    .line 190
    if-ge v0, v1, :cond_6

    .line 191
    invoke-virtual {p0, v2, v2}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 192
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    .line 193
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v1, v0}, Larl;->c(I)V

    goto/16 :goto_3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 236
    const-string v0, "DialtactsActivity.onSaveInstanceState"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 237
    invoke-super {p0, p1}, Lbsz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 238
    const-string v0, "search_query"

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v0, "dialpad_query"

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v0, "in_regular_search_ui"

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 241
    const-string v0, "in_dialpad_search_ui"

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 242
    const-string v0, "in_new_search_ui"

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 243
    const-string v0, "first_launch"

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 244
    const-string v0, "is_dialpad_shown"

    iget-boolean v1, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 245
    const-string v0, "fab_visible"

    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->s:Lbud;

    .line 246
    iget-object v1, v1, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v1}, Landroid/support/design/widget/FloatingActionButton;->isShown()Z

    move-result v1

    .line 247
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 248
    const-string v0, "was_configuration_change"

    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 249
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 250
    const-string v1, "key_actionbar_is_slid_up"

    iget-boolean v2, v0, Laun;->c:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    const-string v1, "key_actionbar_is_faded_out"

    iget-object v2, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 252
    iget-boolean v2, v2, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 253
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 254
    const-string v1, "key_actionbar_is_expanded"

    iget-object v0, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 255
    iget-boolean v0, v0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->a:Z

    .line 256
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->B:Z

    .line 258
    return-void
.end method

.method protected onStop()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 218
    invoke-super {p0}, Lbsz;->onStop()V

    .line 219
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/android/dialer/app/DialtactsActivity;->Q:J

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/android/dialer/app/DialtactsActivity;->x:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    move v0, v1

    .line 220
    :goto_0
    iget-object v3, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 222
    iget v3, v3, Larl;->f:I

    .line 223
    if-ne v3, v1, :cond_2

    .line 224
    :goto_1
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Landroid/app/KeyguardManager;

    .line 226
    invoke-virtual {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->b()V

    .line 228
    :cond_0
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 230
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_tab"

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 232
    iget v2, v2, Larl;->f:I

    .line 233
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 234
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 235
    return-void

    :cond_1
    move v0, v2

    .line 219
    goto :goto_0

    :cond_2
    move v1, v2

    .line 223
    goto :goto_1
.end method

.method public final p()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 771
    iget-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->l:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/dialer/app/DialtactsActivity;->i:Lasm;

    .line 773
    iget-object v3, v2, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    if-eqz v3, :cond_2

    iget-object v2, v2, Lasm;->o:Lcom/android/dialer/widget/EmptyContentView;

    .line 774
    iget-object v3, v2, Lcom/android/dialer/widget/EmptyContentView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/android/dialer/widget/EmptyContentView;->b:Landroid/widget/TextView;

    .line 775
    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/android/dialer/widget/EmptyContentView;->c:Landroid/widget/TextView;

    .line 776
    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    .line 777
    :goto_0
    if-eqz v2, :cond_2

    move v2, v0

    .line 778
    :goto_1
    if-nez v2, :cond_3

    .line 779
    sget-object v1, Lbld$a;->o:Lbld$a;

    invoke-static {v1}, Lbly;->a(Lbld$a;)V

    .line 780
    invoke-virtual {p0, v0, v0}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 782
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 776
    goto :goto_0

    :cond_2
    move v2, v1

    .line 777
    goto :goto_1

    :cond_3
    move v0, v1

    .line 782
    goto :goto_2
.end method

.method public final q()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 783
    iget-boolean v2, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    if-eqz v2, :cond_1

    .line 784
    sget-object v2, Lbld$a;->o:Lbld$a;

    invoke-static {v2}, Lbly;->a(Lbld$a;)V

    .line 785
    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/app/DialtactsActivity;->a(ZZ)V

    .line 786
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->N:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 787
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    .line 790
    :cond_0
    :goto_0
    return v0

    .line 789
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->L:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    invoke-static {p0, v0}, Lapw;->a(Landroid/content/Context;Landroid/view/View;)V

    move v0, v1

    .line 790
    goto :goto_0
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 799
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Larl;->a(Z)V

    .line 800
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 801
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 808
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Larl;->c(I)V

    .line 810
    :cond_0
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 811
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->e(Z)V

    .line 812
    return-void
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->r:Laun;

    .line 867
    iget-boolean v1, v0, Laun;->c:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Laun;->b:Lcom/android/dialer/app/widget/SearchEditTextLayout;

    .line 868
    iget-boolean v0, v0, Lcom/android/dialer/app/widget/SearchEditTextLayout;->b:Z

    .line 869
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 870
    :goto_0
    return v0

    .line 869
    :cond_0
    const/4 v0, 0x0

    .line 870
    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 871
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->n:Z

    return v0
.end method

.method public final w_()V
    .locals 0

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->n()V

    .line 838
    return-void
.end method

.method public final x()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 872
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    if-eqz v1, :cond_0

    .line 873
    iget-object v1, p0, Lcom/android/dialer/app/DialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 874
    iget-object v2, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    if-nez v2, :cond_1

    .line 878
    :cond_0
    :goto_0
    return v0

    .line 876
    :cond_1
    iget-object v0, v1, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 881
    iget v0, p0, Lcom/android/dialer/app/DialtactsActivity;->X:I

    return v0
.end method

.method public final z()I
    .locals 1

    .prologue
    .line 882
    iget-boolean v0, p0, Lcom/android/dialer/app/DialtactsActivity;->G:Z

    if-nez v0, :cond_0

    .line 883
    invoke-virtual {p0}, Lcom/android/dialer/app/DialtactsActivity;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    .line 885
    iget v0, v0, Larl;->f:I

    .line 886
    if-nez v0, :cond_0

    .line 887
    const/4 v0, 0x0

    .line 888
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
