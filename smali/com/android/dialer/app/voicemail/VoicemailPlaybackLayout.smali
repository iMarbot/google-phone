.class public Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Laoi;
.implements Latf$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Laoq;

.field public c:Latf;

.field public d:Landroid/net/Uri;

.field public e:Z

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/widget/SeekBar;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/widget/ImageButton;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    const-class v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 4
    new-instance v0, Lasy;

    invoke-direct {v0, p0}, Lasy;-><init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->f:Landroid/view/View$OnClickListener;

    .line 5
    new-instance v0, Lasz;

    invoke-direct {v0, p0}, Lasz;-><init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->g:Landroid/view/View$OnClickListener;

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->e:Z

    .line 7
    new-instance v0, Latc;

    invoke-direct {v0, p0}, Latc;-><init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->h:Landroid/view/View$OnClickListener;

    .line 8
    new-instance v0, Latd;

    invoke-direct {v0, p0}, Latd;-><init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;)V

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->p:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 9
    iput-object p1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    .line 10
    const-string v0, "layout_inflater"

    .line 11
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 12
    const v1, 0x7f0400c9

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 13
    return-void
.end method

.method private final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v0, 0x63

    .line 105
    div-int/lit16 v2, p0, 0x3e8

    .line 106
    div-int/lit8 v1, v2, 0x3c

    .line 107
    mul-int/lit8 v3, v1, 0x3c

    sub-int/2addr v2, v3

    .line 108
    if-le v1, v0, :cond_0

    .line 110
    :goto_0
    const-string v1, "%02d:%02d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v0

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 101
    iget-object v1, v0, Latf;->w:Latf$c;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, v0, Latf;->w:Latf$c;

    iget-object v0, v0, Latf;->k:Landroid/net/Uri;

    invoke-interface {v1, v0}, Latf$c;->a(Landroid/net/Uri;)V

    .line 103
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 72
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 73
    iget-object v2, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 74
    iget-object v2, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 76
    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->o:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method

.method public final a(ILjava/util/concurrent/ScheduledExecutorService;)V
    .locals 8

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->e:Z

    .line 39
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    const v1, 0x7f0200ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 40
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    invoke-virtual {v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->a()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 43
    :cond_0
    new-instance v0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;-><init>(Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;ILjava/util/concurrent/ScheduledExecutorService;)V

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 44
    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 45
    iget-object v7, v1, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->b:Ljava/lang/Object;

    monitor-enter v7

    .line 46
    :try_start_0
    invoke-virtual {v1}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->b()V

    .line 47
    iget-object v0, v1, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->a:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x21

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 48
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, v1, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 49
    monitor-exit v7

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Latf;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->c:Latf;

    .line 15
    iput-object p2, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->d:Landroid/net/Uri;

    .line 16
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 61
    if-eqz p1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    const v1, 0x7f020181

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 63
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    const v2, 0x7f110386

    .line 64
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    const v1, 0x7f02017f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 67
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    const v2, 0x7f110387

    .line 68
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->e:Z

    .line 51
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    const v1, 0x7f0200bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 52
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    invoke-virtual {v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->a()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    .line 55
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->q:Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;

    invoke-virtual {v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout$a;->a()V

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->h()V

    .line 59
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    const v1, 0x7f110381

    invoke-direct {p0, v1}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->h()V

    .line 82
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    const v1, 0x7f11037c

    invoke-direct {p0, v1}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 85
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    const v1, 0x7f11037d

    invoke-direct {p0, v1}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 89
    invoke-virtual {p0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j()V

    .line 90
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 91
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 92
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 93
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 94
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 95
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 97
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 98
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 99
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 18
    const v0, 0x7f0e028c

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    .line 19
    const v0, 0x7f0e028f

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    .line 20
    const v0, 0x7f0e028e

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    .line 21
    const v0, 0x7f0e0290

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->l:Landroid/widget/ImageButton;

    .line 22
    const v0, 0x7f0e028a

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    .line 23
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->m:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAccessibilityLiveRegion(I)V

    .line 24
    const v0, 0x7f0e028b

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->n:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0e028d

    invoke-virtual {p0, v0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->o:Landroid/widget/TextView;

    .line 26
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->i:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->p:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 27
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->j:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->k:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->l:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->n:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->o:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    invoke-virtual {p0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020100

    iget-object v2, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->r:Landroid/graphics/drawable/Drawable;

    .line 35
    invoke-virtual {p0}, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020101

    iget-object v2, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->a:Landroid/content/Context;

    .line 36
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/app/voicemail/VoicemailPlaybackLayout;->s:Landroid/graphics/drawable/Drawable;

    .line 37
    return-void
.end method
