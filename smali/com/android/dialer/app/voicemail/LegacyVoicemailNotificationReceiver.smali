.class public Lcom/android/dialer/app/voicemail/LegacyVoicemailNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lbdg;

    .line 47
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v1

    invoke-virtual {v1}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lbdg;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/content/SharedPreferences;)V

    .line 48
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 2

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/android/dialer/app/voicemail/LegacyVoicemailNotificationReceiver;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lbdg;->a()Lbdh;

    move-result-object v0

    const-string v1, "legacy_voicemail_dismissed"

    .line 43
    invoke-virtual {v0, v1, p2}, Lbdh;->a(Ljava/lang/String;Z)Lbdh;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lbdh;->a()V

    .line 45
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v8, 0x0

    .line 2
    const-string v0, "android.telephony.action.SHOW_VOICEMAIL_NOTIFICATION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.android.voicemail.VoicemailClient.ACTION_SHOW_LEGACY_VOICEMAIL"

    .line 3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    :goto_0
    return-void

    .line 5
    :cond_0
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const-string v1, "received legacy voicemail notification"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    sget v2, Landroid/os/Build$VERSION;->PREVIEW_SDK_INT:I

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "SDK not finalized: SDK_INT="

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", PREVIEW_SDK_INT="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RELEASE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 9
    :cond_1
    const-string v0, "android.telephony.extra.PHONE_ACCOUNT_HANDLE"

    .line 10
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 11
    const-string v0, "android.telephony.extra.NOTIFICATION_COUNT"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 12
    sget-object v0, Lbev;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 13
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const/16 v3, 0x10

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "isRefresh: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-static {p1, v1}, Lcom/android/dialer/app/voicemail/LegacyVoicemailNotificationReceiver;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;

    move-result-object v0

    .line 15
    if-eqz v6, :cond_2

    .line 16
    const-string v3, "legacy_voicemail_dismissed"

    invoke-virtual {v0, v3, v8}, Lbdg;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 17
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const-string v1, "notification dismissed, ignoring refresh"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 19
    :cond_2
    invoke-static {p1, v1, v8}, Lcom/android/dialer/app/voicemail/LegacyVoicemailNotificationReceiver;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 20
    :cond_3
    if-ne v2, v5, :cond_4

    .line 21
    const/4 v2, 0x1

    .line 22
    :cond_4
    if-nez v2, :cond_5

    .line 23
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const-string v2, "clearing notification"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-static {p1, v1}, Lazq;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto/16 :goto_0

    .line 26
    :cond_5
    const-string v0, "is_legacy_mode"

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 27
    invoke-static {p1}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 28
    invoke-static {p1}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 30
    invoke-interface {v0, p1, v1}, Lcln;->e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const-string v1, "visual voicemail is activated, ignoring notification"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 33
    :cond_6
    const-string v0, "android.telephony.extra.VOICEMAIL_NUMBER"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 34
    const-string v0, "android.telephony.extra.CALL_VOICEMAIL_INTENT"

    .line 35
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    .line 36
    const-string v0, "android.telephony.extra.LAUNCH_VOICEMAIL_SETTINGS_INTENT"

    .line 37
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/app/PendingIntent;

    .line 38
    const-string v0, "LegacyVoicemailNotificationReceiver.onReceive"

    const-string v7, "sending notification"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, p1

    .line 39
    invoke-static/range {v0 .. v6}, Lazq;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;ILjava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Z)V

    goto/16 :goto_0
.end method
