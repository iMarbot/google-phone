.class final Lcom/android/dialer/calldetails/CallDetailsActivity$a;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/calldetails/CallDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private synthetic b:Lcom/android/dialer/calldetails/CallDetailsActivity;


# direct methods
.method constructor <init>(Lcom/android/dialer/calldetails/CallDetailsActivity;)V
    .locals 6

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    iget-object v0, p1, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 6
    iget-object v0, v0, Lbao;->a:Lhce;

    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 9
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    :cond_0
    iget-wide v4, v0, Lbao$a;->b:J

    .line 12
    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 14
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->a:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 37
    .line 38
    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    invoke-virtual {v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "_id IN ("

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 41
    return-object v5
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 16
    .line 17
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 18
    const-string v0, "phone_number"

    iget-object v2, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    .line 19
    iget-object v2, v2, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    .line 21
    iget-object v2, v2, Lbhj;->f:Ljava/lang/String;

    .line 22
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    .line 24
    iget-object v0, v0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 26
    iget-object v0, v0, Lbao;->a:Lhce;

    .line 27
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao$a;

    .line 29
    iget-object v0, v0, Lbao$a;->h:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    .line 30
    if-lez v0, :cond_0

    .line 31
    const-string v0, "has_enriched_call_data"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/android/dialer/calldetails/CallDetailsActivity;->setResult(ILandroid/content/Intent;)V

    .line 35
    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity$a;->b:Lcom/android/dialer/calldetails/CallDetailsActivity;

    invoke-virtual {v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->finish()V

    .line 36
    return-void
.end method
