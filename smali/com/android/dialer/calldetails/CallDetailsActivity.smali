.class public Lcom/android/dialer/calldetails/CallDetailsActivity;
.super Luh;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/Toolbar$c;
.implements Lbau;
.implements Lbaw;
.implements Lbji;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/calldetails/CallDetailsActivity$a;
    }
.end annotation


# instance fields
.field public f:Lbao;

.field public g:Lbhj;

.field private h:Lban;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbao;Lbhj;ZZ)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/calldetails/CallDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    const-string v1, "contact"

    invoke-static {v0, v1, p2}, Lbib;->b(Landroid/content/Intent;Ljava/lang/String;Lhdd;)V

    .line 6
    const-string v1, "call_details_entries"

    invoke-static {v0, v1, p1}, Lbib;->b(Landroid/content/Intent;Ljava/lang/String;Lhdd;)V

    .line 7
    const-string v1, "can_report_caller_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 8
    const-string v1, "can_support_assisted_dialing"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 9
    return-object v0
.end method

.method private final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 46
    const-string v0, "contact"

    .line 47
    sget-object v1, Lbhj;->l:Lbhj;

    .line 48
    invoke-static {p1, v0, v1}, Lbib;->a(Landroid/content/Intent;Ljava/lang/String;Lhdd;)Lhdd;

    move-result-object v0

    check-cast v0, Lbhj;

    iput-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    .line 49
    const-string v0, "call_details_entries"

    .line 50
    sget-object v1, Lbao;->b:Lbao;

    .line 51
    invoke-static {p1, v0, v1}, Lbib;->a(Landroid/content/Intent;Ljava/lang/String;Lhdd;)Lhdd;

    move-result-object v0

    check-cast v0, Lbao;

    iput-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 52
    new-instance v0, Lban;

    iget-object v2, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    iget-object v1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 54
    iget-object v3, v1, Lbao;->a:Lhce;

    move-object v1, p0

    move-object v4, p0

    move-object v5, p0

    .line 55
    invoke-direct/range {v0 .. v5}, Lban;-><init>(Landroid/content/Context;Lbhj;Ljava/util/List;Lbaw;Lbau;)V

    iput-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->h:Lban;

    .line 56
    const v0, 0x7f0e010b

    invoke-virtual {p0, v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 57
    new-instance v1, Labq;

    invoke-direct {v1, p0}, Labq;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 58
    iget-object v1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->h:Lban;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 59
    invoke-static {v0}, Lbly;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 70
    .line 71
    new-instance v0, Lbax;

    invoke-direct {v0}, Lbax;-><init>()V

    .line 72
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 73
    const-string v2, "number"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0, v1}, Lbax;->setArguments(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbax;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 139
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dK:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 141
    invoke-virtual {p0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "can_support_assisted_dialing"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 142
    new-instance v2, Lbbh;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v3, Lbbf$a;->k:Lbbf$a;

    invoke-direct {v2, v0, v3}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 143
    if-eqz v1, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 145
    iput-boolean v0, v2, Lbbh;->e:Z

    .line 146
    :cond_0
    invoke-static {p0, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 147
    return-void

    .line 142
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e000c

    if-ne v1, v2, :cond_0

    .line 62
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v2, Lbkq$a;->H:Lbkq$a;

    invoke-interface {v1, v2}, Lbku;->a(Lbkq$a;)V

    .line 63
    invoke-static {}, Lbdj;->a()Lbdi;

    move-result-object v1

    new-instance v2, Lcom/android/dialer/calldetails/CallDetailsActivity$a;

    invoke-direct {v2, p0}, Lcom/android/dialer/calldetails/CallDetailsActivity$a;-><init>(Lcom/android/dialer/calldetails/CallDetailsActivity;)V

    new-array v3, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lbdi;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 65
    const/4 v0, 0x1

    .line 66
    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dI:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 123
    new-instance v0, Lbbh;

    sget-object v1, Lbbf$a;->k:Lbbf$a;

    invoke-direct {v0, p1, v1}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    const/4 v1, 0x1

    .line 125
    iput-boolean v1, v0, Lbbh;->c:Z

    .line 127
    invoke-static {p0, v0}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 128
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 129
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dJ:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 130
    invoke-static {p0}, Lbiu;->a(Landroid/content/Context;)Lbiu;

    move-result-object v0

    invoke-virtual {v0}, Lbiu;->a()Lbis;

    move-result-object v0

    .line 131
    invoke-interface {v0, p0, p1}, Lbis;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    invoke-virtual {p0, p1}, Lcom/android/dialer/calldetails/CallDetailsActivity;->b(Ljava/lang/String;)V

    .line 138
    :goto_0
    return-void

    .line 134
    :cond_0
    :try_start_0
    invoke-interface {v0, p0, p1}, Lbis;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/calldetails/CallDetailsActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    const v0, 0x7f110033

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "can_report_caller_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 8

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    .line 81
    iget-object v0, v0, Lbhj;->f:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 83
    if-nez v0, :cond_0

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    move-object v4, v0

    .line 93
    :goto_0
    iget-object v5, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->h:Lban;

    iget-object v0, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    .line 95
    iget-object v0, v0, Lbhj;->f:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    .line 98
    if-nez v0, :cond_2

    move-object v0, v1

    .line 116
    :goto_1
    iget-object v0, v0, Lbao;->a:Lhce;

    .line 118
    iput-object v0, v5, Lban;->c:Ljava/util/List;

    .line 120
    iget-object v0, v5, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 121
    return-void

    .line 86
    :cond_0
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v2

    .line 87
    invoke-virtual {v2}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 88
    invoke-interface {v2, v0, v1}, Lbjf;->b(Ljava/lang/String;Lbao;)Ljava/util/Map;

    move-result-object v0

    .line 89
    if-nez v0, :cond_1

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_1
    move-object v4, v0

    .line 91
    goto :goto_0

    .line 100
    :cond_2
    sget-object v0, Lbao;->b:Lbao;

    invoke-virtual {v0}, Lbao;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 103
    iget-object v1, v1, Lbao;->a:Lhce;

    .line 104
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbao$a;

    .line 105
    sget-object v2, Lbao$a;->j:Lbao$a;

    invoke-virtual {v2}, Lbao$a;->createBuilder()Lhbr$a;

    move-result-object v2

    check-cast v2, Lhbr$a;

    .line 106
    invoke-virtual {v2, v1}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    move-result-object v2

    check-cast v2, Lhbr$a;

    .line 107
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 108
    if-eqz v3, :cond_3

    .line 109
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-virtual {v2, v1}, Lhbr$a;->a(Ljava/lang/Iterable;)Lhbr$a;

    .line 110
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 111
    invoke-virtual {v2}, Lhbr$a;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v3

    .line 112
    :cond_3
    invoke-virtual {v2}, Lhbr$a;->build()Lhbr;

    move-result-object v1

    check-cast v1, Lbao$a;

    invoke-virtual {v0, v1}, Lhbr$a;->a(Lbao$a;)Lhbr$a;

    goto :goto_2

    .line 114
    :cond_4
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lbao;

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lbld$a;->f:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 68
    invoke-super {p0}, Luh;->onBackPressed()V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 10
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 11
    const v0, 0x7f040028

    invoke-virtual {p0, v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->setContentView(I)V

    .line 12
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 13
    const v1, 0x7f130001

    .line 15
    new-instance v2, Lwm;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lwm;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 18
    iput-object p0, v0, Landroid/support/v7/widget/Toolbar;->q:Landroid/support/v7/widget/Toolbar$c;

    .line 19
    const v1, 0x7f11006f

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 20
    new-instance v1, Lbam;

    invoke-direct {v1, p0}, Lbam;-><init>(Lcom/android/dialer/calldetails/CallDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View$OnClickListener;)V

    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->a(Landroid/content/Intent;)V

    .line 22
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0, p1}, Luh;->onNewIntent(Landroid/content/Intent;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/android/dialer/calldetails/CallDetailsActivity;->a(Landroid/content/Intent;)V

    .line 45
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Luh;->onPause()V

    .line 39
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 41
    invoke-interface {v0, p0}, Lbjf;->b(Lbji;)V

    .line 42
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Luh;->onResume()V

    .line 24
    invoke-static {p0}, Lbib;->B(Landroid/content/Context;)V

    .line 25
    sget-boolean v0, Lbly;->c:Z

    .line 26
    if-nez v0, :cond_0

    .line 27
    invoke-static {}, Lbly;->a()V

    .line 28
    :cond_0
    const v0, 0x7f0e010b

    invoke-virtual {p0, v0}, Lcom/android/dialer/calldetails/CallDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p0, v0}, Lbib;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 29
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 31
    invoke-interface {v0, p0}, Lbjf;->a(Lbji;)V

    .line 32
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->g:Lbhj;

    .line 35
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 36
    iget-object v2, p0, Lcom/android/dialer/calldetails/CallDetailsActivity;->f:Lbao;

    invoke-interface {v0, v1, v2}, Lbjf;->a(Ljava/lang/String;Lbao;)V

    .line 37
    return-void
.end method
