.class public Lcom/android/dialer/widget/DialerToolbar;
.super Landroid/support/v7/widget/Toolbar;
.source "PG"


# instance fields
.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    const v0, 0x7f04004c

    invoke-static {p1, v0, p0}, Lcom/android/dialer/widget/DialerToolbar;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3
    const v0, 0x7f0e0046

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/widget/DialerToolbar;->w:Landroid/widget/TextView;

    .line 4
    const v0, 0x7f0e0172

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/widget/DialerToolbar;->x:Landroid/widget/TextView;

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->setElevation(F)V

    .line 6
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->setBackgroundColor(I)V

    .line 7
    const v0, 0x7f02013a

    .line 8
    invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lvy;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->b(Landroid/graphics/drawable/Drawable;)V

    .line 9
    const v0, 0x7f1102fd

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->c(I)V

    .line 10
    new-instance v0, Lbuc;

    invoke-direct {v0, p1}, Lbuc;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->a(Landroid/view/View$OnClickListener;)V

    .line 12
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getPaddingStart()I

    move-result v0

    .line 13
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getPaddingTop()I

    move-result v1

    .line 14
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d01ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 15
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getPaddingBottom()I

    move-result v3

    .line 16
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/dialer/widget/DialerToolbar;->setPaddingRelative(IIII)V

    .line 17
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/dialer/widget/DialerToolbar;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 21
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/android/dialer/widget/DialerToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/dialer/widget/DialerToolbar;->a(Ljava/lang/CharSequence;)V

    .line 19
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 22
    if-eqz p1, :cond_0

    .line 23
    iget-object v0, p0, Lcom/android/dialer/widget/DialerToolbar;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    iget-object v0, p0, Lcom/android/dialer/widget/DialerToolbar;->x:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 25
    :cond_0
    return-void
.end method
