.class public Lcom/android/dialer/postcall/PostCallActivity;
.super Luh;
.source "PG"

# interfaces
.implements Lbue$a;


# instance fields
.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "message"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    iget-boolean v0, p0, Lcom/android/dialer/postcall/PostCallActivity;->f:Z

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "PostCallActivity.onMessageFragmentSendMessage"

    const-string v2, "sending post call Rcs."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 50
    invoke-interface {v0, v1, p1}, Lbjf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, v1}, Lbib;->i(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->finish()V

    .line 71
    :goto_0
    return-void

    .line 53
    :cond_0
    const-string v0, "android.permission.SEND_SMS"

    invoke-static {p0, v0}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "PostCallActivity.sendMessage"

    const-string v3, "Sending post call SMS."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p1}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    .line 58
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 59
    invoke-static {p0, v1}, Lbib;->i(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->finish()V

    goto :goto_0

    .line 61
    :cond_1
    const-string v0, "android.permission.SEND_SMS"

    invoke-static {p0, v0}, Lbsw;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "android.permission.SEND_SMS"

    .line 62
    invoke-virtual {p0, v0}, Lcom/android/dialer/postcall/PostCallActivity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    :cond_2
    const-string v0, "PostCallActivity.sendMessage"

    const-string v1, "Requesting SMS_SEND permission."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.SEND_SMS"

    aput-object v1, v0, v4

    invoke-virtual {p0, v0, v5}, Lcom/android/dialer/postcall/PostCallActivity;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_0

    .line 65
    :cond_3
    const-string v0, "PostCallActivity.sendMessage"

    const-string v1, "Permission permanently denied, sending to settings."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    const-string v2, "package:"

    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0, v1}, Lcom/android/dialer/postcall/PostCallActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 69
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const v0, 0x7f0400aa

    invoke-virtual {p0, v0}, Lcom/android/dialer/postcall/PostCallActivity;->setContentView(I)V

    .line 4
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/android/dialer/postcall/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/DialerToolbar;

    const v3, 0x7f110272

    invoke-virtual {v0, v3}, Lcom/android/dialer/widget/DialerToolbar;->b(I)V

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "rcs_post_call"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/postcall/PostCallActivity;->f:Z

    .line 6
    const-string v0, "PostCallActivity.onCreate"

    const-string v3, "useRcs: %b"

    new-array v4, v1, [Ljava/lang/Object;

    iget-boolean v5, p0, Lcom/android/dialer/postcall/PostCallActivity;->f:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-boolean v0, p0, Lcom/android/dialer/postcall/PostCallActivity;->f:Z

    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0016

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 10
    :goto_0
    new-array v3, v6, [Ljava/lang/String;

    const v4, 0x7f110273

    .line 11
    invoke-virtual {p0, v4}, Lcom/android/dialer/postcall/PostCallActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    const v4, 0x7f110274

    .line 12
    invoke-virtual {p0, v4}, Lcom/android/dialer/postcall/PostCallActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    const v5, 0x7f110275

    .line 13
    invoke-virtual {p0, v5}, Lcom/android/dialer/postcall/PostCallActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 14
    invoke-static {}, Lbue;->a()Lbuf;

    move-result-object v4

    .line 16
    iput v0, v4, Lbuf;->c:I

    .line 17
    iput-boolean v1, v4, Lbuf;->b:Z

    .line 21
    array-length v0, v3

    if-lez v0, :cond_1

    array-length v0, v3

    if-gt v0, v6, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 22
    iput-object v3, v4, Lbuf;->a:[Ljava/lang/String;

    .line 25
    new-instance v0, Lbue;

    invoke-direct {v0}, Lbue;-><init>()V

    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 27
    const-string v2, "char_limit"

    .line 28
    iget v3, v4, Lbuf;->c:I

    .line 29
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 30
    const-string v2, "show_send_icon"

    .line 31
    iget-boolean v3, v4, Lbuf;->b:Z

    .line 32
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 33
    const-string v2, "message_list"

    .line 34
    iget-object v3, v4, Lbuf;->a:[Ljava/lang/String;

    .line 35
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1}, Lbue;->f(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->c()Lja;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    const v2, 0x7f0e01d8

    .line 41
    invoke-virtual {v1, v2, v0}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Ljy;->a()I

    .line 43
    return-void

    .line 9
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 21
    goto :goto_1
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    array-length v0, p2

    if-lez v0, :cond_0

    aget-object v0, p2, v2

    const-string v1, "android.permission.SEND_SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    aget-object v0, p2, v2

    invoke-static {p0, v0}, Lbsw;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 74
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    array-length v0, p3

    if-lez v0, :cond_1

    aget v0, p3, v2

    if-nez v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/android/dialer/postcall/PostCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/dialer/postcall/PostCallActivity;->a(Ljava/lang/String;)V

    .line 76
    :cond_1
    return-void
.end method
