.class public final Lcom/android/dialer/dialpadview/DialpadFragment;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/android/dialer/dialpadview/DialpadKeyButton$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/dialpadview/DialpadFragment$a;,
        Lcom/android/dialer/dialpadview/DialpadFragment$DialpadSlidingRelativeLayout;,
        Lcom/android/dialer/dialpadview/DialpadFragment$b;
    }
.end annotation


# static fields
.field private static i:Lgtm;


# instance fields
.field public a:Lcom/android/dialer/dialpadview/DialpadView;

.field public b:Landroid/widget/EditText;

.field public c:Lbud;

.field public d:Landroid/support/design/widget/FloatingActionButton;

.field public e:Lbif;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field private j:Ljava/lang/Object;

.field private k:Ljava/util/HashSet;

.field private l:Lcom/android/dialer/dialpadview/DialpadFragment$b;

.field private m:I

.field private n:Landroid/view/View;

.field private o:Landroid/widget/PopupMenu;

.field private p:Landroid/view/View;

.field private q:Landroid/media/ToneGenerator;

.field private r:Landroid/widget/ListView;

.field private s:Lbhw;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Lbhu;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lbdy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 658
    sget-object v0, Lgsz;->a:Lgsz;

    .line 659
    sput-object v0, Lcom/android/dialer/dialpadview/DialpadFragment;->i:Lgtm;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->j:Ljava/lang/Object;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->f:Ljava/lang/String;

    .line 5
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    .line 6
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    .line 7
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    return-void
.end method

.method private final a(C)V
    .locals 3

    .prologue
    .line 457
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 458
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    if-ne p1, v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 460
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 461
    :cond_0
    return-void
.end method

.method private final a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 309
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 335
    :goto_1
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 336
    new-instance v0, Landroid/view/KeyEvent;

    invoke-direct {v0, v3, p1}, Landroid/view/KeyEvent;-><init>(II)V

    .line 337
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1, p1, v0}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 338
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 339
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 340
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 312
    :pswitch_0
    invoke-direct {p0, v4, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 314
    :pswitch_1
    const/4 v0, 0x2

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 316
    :pswitch_2
    const/4 v0, 0x3

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 318
    :pswitch_3
    const/4 v0, 0x4

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 320
    :pswitch_4
    const/4 v0, 0x5

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 322
    :pswitch_5
    const/4 v0, 0x6

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 324
    :pswitch_6
    const/4 v0, 0x7

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 326
    :pswitch_7
    const/16 v0, 0x8

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 328
    :pswitch_8
    const/16 v0, 0x9

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 330
    :pswitch_9
    invoke-direct {p0, v3, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 332
    :pswitch_a
    const/16 v0, 0xb

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 334
    :pswitch_b
    const/16 v0, 0xa

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_1

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_a
    .end packed-switch
.end method

.method private final a(II)V
    .locals 4

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->u:Z

    if-nez v0, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 512
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    .line 513
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 516
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    if-nez v0, :cond_2

    .line 517
    const-string v0, "DialpadFragment.playTone"

    const/16 v2, 0x29

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "mToneGenerator == null, tone: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 518
    monitor-exit v1

    goto :goto_0

    .line 520
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 519
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    invoke-virtual {v0, p1, p2}, Landroid/media/ToneGenerator;->startTone(II)Z

    .line 520
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8
    if-nez p0, :cond_1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 10
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 11
    const-string v2, "android.intent.action.DIAL"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    :cond_2
    const-string v1, "add_call_mode"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private final b(C)V
    .locals 7

    .prologue
    const/16 v6, 0x2c

    const/4 v4, -0x1

    const/16 v5, 0x3b

    const/4 v2, 0x0

    .line 596
    if-eq p1, v5, :cond_0

    if-eq p1, v6, :cond_0

    .line 597
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not expected for anything other than PAUSE & WAIT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 599
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v3

    .line 600
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 601
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 602
    if-ne v1, v4, :cond_1

    .line 603
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    move v1, v0

    .line 604
    :cond_1
    iget-object v3, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 606
    if-eq p1, v5, :cond_2

    if-eq p1, v6, :cond_2

    .line 607
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Should not be called for anything other than PAUSE & WAIT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_2
    if-eq v1, v4, :cond_3

    if-ge v0, v1, :cond_5

    .line 620
    :cond_3
    :goto_0
    if-eqz v2, :cond_4

    .line 621
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v1, v0, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 622
    if-eq v1, v0, :cond_4

    .line 623
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 624
    :cond_4
    return-void

    .line 610
    :cond_5
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gt v1, v4, :cond_3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gt v0, v4, :cond_3

    .line 612
    if-eqz v1, :cond_3

    .line 614
    if-ne p1, v5, :cond_6

    .line 615
    add-int/lit8 v4, v1, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    if-eq v4, v5, :cond_3

    .line 617
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-le v4, v0, :cond_6

    invoke-interface {v3, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    if-eq v4, v5, :cond_3

    .line 619
    :cond_6
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->f()Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractNetworkPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 155
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->extractPostDialPortion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 163
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 165
    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v1, v2, v3, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 166
    invoke-virtual {p0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->afterTextChanged(Landroid/text/Editable;)V

    .line 167
    :cond_0
    return-void

    .line 158
    :cond_1
    invoke-static {v2, p2, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 160
    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final b(Z)V
    .locals 1

    .prologue
    .line 579
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lbsp;->a(Landroid/content/Context;Z)V

    .line 580
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 581
    return-void
.end method

.method private final b(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 112
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    if-nez v0, :cond_0

    move v0, v6

    .line 144
    :goto_0
    return v0

    .line 114
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string v1, "android.intent.action.DIAL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_6

    .line 118
    const-string v1, "tel"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 119
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 120
    iput-boolean v7, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    .line 122
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->replaceUnicodeDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-direct {p0, v0, v3}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 125
    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    .line 127
    goto :goto_0

    .line 128
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    .line 129
    const-string v1, "vnd.android.cursor.item/person"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "vnd.android.cursor.item/phone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 131
    :cond_4
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 133
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "number"

    aput-object v4, v2, v6

    const-string v4, "number_key"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    .line 134
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_6

    .line 136
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    .line 138
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 140
    goto/16 :goto_0

    .line 141
    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    move v0, v6

    .line 144
    goto/16 :goto_0

    .line 143
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private final e()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadFragment;->i:Lgtm;

    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadFragment;->i:Lgtm;

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 110
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final g()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final h()V
    .locals 1

    .prologue
    .line 462
    const-string v0, "DialpadFragment.hideAndClearDialpad"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 463
    const-class v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    invoke-interface {v0}, Lcom/android/dialer/dialpadview/DialpadFragment$a;->h()V

    .line 464
    return-void
.end method

.method private final i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 465
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466
    sget-object v0, Lbld$a;->p:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 469
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->e()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 470
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    new-instance v0, Lbbh;

    const-string v1, ""

    sget-object v2, Lbbf$a;->c:Lbbf$a;

    invoke-direct {v0, v1, v2}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    invoke-virtual {v0}, Lbbh;->a()Landroid/content/Intent;

    move-result-object v0

    .line 473
    const-string v1, "com.android.phone.extra.SEND_EMPTY_FLASH"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 475
    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->startActivity(Landroid/content/Intent;)V

    .line 504
    :goto_0
    return-void

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 477
    sget-object v0, Lbld$a;->g:Lbld$a;

    invoke-static {v0}, Lbly;->b(Lbld$a;)V

    .line 478
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 479
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    .line 481
    :cond_1
    const/16 v0, 0x1a

    const/16 v1, 0x96

    invoke-direct {p0, v0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(II)V

    goto :goto_0

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 484
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->t:Ljava/lang/String;

    .line 485
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->t:Ljava/lang/String;

    .line 486
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 487
    sget-object v0, Lbld$a;->p:Lbld$a;

    invoke-static {v0}, Lbly;->a(Lbld$a;)V

    .line 488
    const-string v0, "DialpadFragment.handleDialButtonPressed"

    const-string v1, "The phone number is prohibited explicitly by a rule."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 489
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 490
    const v0, 0x7f11014d

    .line 493
    new-instance v1, Lbhy;

    invoke-direct {v1}, Lbhy;-><init>()V

    .line 494
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 495
    const-string v3, "argTitleResId"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 496
    const-string v3, "argMessageResId"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 497
    invoke-virtual {v1, v2}, Lbhy;->setArguments(Landroid/os/Bundle;)V

    .line 500
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "phone_prohibited_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 501
    :cond_3
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a()V

    goto/16 :goto_0

    .line 502
    :cond_4
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lbbh;

    sget-object v3, Lbbf$a;->c:Lbbf$a;

    invoke-direct {v2, v0, v3}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    invoke-static {v1, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 503
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->h()V

    goto/16 :goto_0
.end method

.method private final j()V
    .locals 4

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->u:Z

    if-nez v0, :cond_0

    .line 528
    :goto_0
    return-void

    .line 523
    :cond_0
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 524
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    if-nez v0, :cond_1

    .line 525
    const-string v0, "DialpadFragment.stopTone"

    const-string v2, "mToneGenerator == null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 526
    monitor-exit v1

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 527
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->stopTone()V

    .line 528
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private final k()Z
    .locals 1

    .prologue
    .line 582
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final l()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 630
    .line 631
    :try_start_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "voicemail"

    invoke-static {v2, v3}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 632
    if-nez v2, :cond_2

    .line 633
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->e()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 639
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 633
    goto :goto_0

    .line 635
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v2}, Lbsp;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v2

    .line 636
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 638
    :catch_0
    move-exception v0

    const-string v0, "DialpadFragment.isVoicemailAvailable"

    const-string v2, "SecurityException is thrown. Maybe privilege isn\'t sufficient."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 639
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 507
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 347
    if-eqz p2, :cond_d

    .line 348
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 349
    const v1, 0x7f0e018c

    if-ne v0, v1, :cond_1

    .line 350
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    .line 374
    :goto_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 379
    :cond_0
    :goto_1
    return-void

    .line 351
    :cond_1
    const v1, 0x7f0e017a

    if-ne v0, v1, :cond_2

    .line 352
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 353
    :cond_2
    const v1, 0x7f0e017b

    if-ne v0, v1, :cond_3

    .line 354
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 355
    :cond_3
    const v1, 0x7f0e017c

    if-ne v0, v1, :cond_4

    .line 356
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 357
    :cond_4
    const v1, 0x7f0e017d

    if-ne v0, v1, :cond_5

    .line 358
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 359
    :cond_5
    const v1, 0x7f0e017e

    if-ne v0, v1, :cond_6

    .line 360
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 361
    :cond_6
    const v1, 0x7f0e017f

    if-ne v0, v1, :cond_7

    .line 362
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 363
    :cond_7
    const v1, 0x7f0e0180

    if-ne v0, v1, :cond_8

    .line 364
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 365
    :cond_8
    const v1, 0x7f0e0181

    if-ne v0, v1, :cond_9

    .line 366
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 367
    :cond_9
    const v1, 0x7f0e0182

    if-ne v0, v1, :cond_a

    .line 368
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 369
    :cond_a
    const v1, 0x7f0e018e

    if-ne v0, v1, :cond_b

    .line 370
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 371
    :cond_b
    const v1, 0x7f0e018f

    if-ne v0, v1, :cond_c

    .line 372
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 373
    :cond_c
    const-string v0, "DialpadFragment.onPressed"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected onTouch(ACTION_DOWN) event from: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 376
    :cond_d
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 377
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->j()V

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 146
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadView;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 152
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v1, v0, Lcom/android/dialer/dialpadview/DialpadView;->e:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 150
    iget-object v1, v0, Lcom/android/dialer/dialpadview/DialpadView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 529
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    if-eqz p1, :cond_5

    .line 534
    const-string v0, "DialpadFragment.showDialpadChooser"

    const-string v1, "Showing dialpad chooser!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 535
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0, v6}, Lcom/android/dialer/dialpadview/DialpadView;->setVisibility(I)V

    .line 537
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_3

    .line 538
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 539
    :cond_3
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 540
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 542
    invoke-virtual {v0, v5, v4}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 543
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 544
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->s:Lbhw;

    if-nez v0, :cond_4

    .line 545
    new-instance v0, Lbhw;

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lbhw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->s:Lbhw;

    .line 546
    :cond_4
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->s:Lbhw;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 547
    :cond_5
    const-string v0, "DialpadFragment.showDialpadChooser"

    const-string v1, "Displaying normal Dialer UI."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 548
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    if-eqz v0, :cond_7

    .line 549
    const-string v0, "DialpadFragment.showDialpadChooser"

    const-string v1, "mDialpadView not null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 550
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0, v3}, Lcom/android/dialer/dialpadview/DialpadView;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 552
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 554
    invoke-virtual {v0, v5, v4}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 558
    :goto_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 559
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->isShown()Z

    move-result v0

    .line 560
    if-nez v0, :cond_6

    .line 561
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 562
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 564
    invoke-virtual {v0, v5, v4}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 565
    :cond_6
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 556
    :cond_7
    const-string v0, "DialpadFragment.showDialpadChooser"

    const-string v1, "mDialpadView null"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 557
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_1
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-static {v0, v1, v2}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;Landroid/widget/EditText;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    iput-boolean v3, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    .line 35
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->l:Lcom/android/dialer/dialpadview/DialpadFragment$b;

    if-eqz v0, :cond_2

    .line 37
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->l:Lcom/android/dialer/dialpadview/DialpadFragment$b;

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment$b;->a(Ljava/lang/String;)V

    .line 38
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->c()V

    .line 39
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->w:Z

    .line 17
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 625
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 629
    :goto_0
    return-void

    .line 627
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 628
    :goto_1
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 627
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 380
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 381
    const v1, 0x7f0e0185

    if-ne v0, v1, :cond_1

    .line 382
    invoke-virtual {p1, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 383
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->i()V

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    const v1, 0x7f0e0199

    if-ne v0, v1, :cond_2

    .line 385
    const/16 v0, 0x43

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    goto :goto_0

    .line 386
    :cond_2
    const v1, 0x7f0e0198

    if-ne v0, v1, :cond_3

    .line 387
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    goto :goto_0

    .line 389
    :cond_3
    const v1, 0x7f0e0197

    if-ne v0, v1, :cond_4

    .line 390
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0

    .line 391
    :cond_4
    const-string v0, "DialpadFragment.onClick"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected event from: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 40
    const-string v0, "DialpadFragment.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    .line 44
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1100ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->t:Ljava/lang/String;

    .line 45
    if-eqz p1, :cond_0

    .line 46
    const-string v0, "pref_digits_filled_by_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->m:I

    .line 48
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->v:Lbhu;

    if-nez v0, :cond_1

    .line 49
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 50
    new-instance v1, Lbhu;

    .line 51
    invoke-direct {v1, p0}, Lbhu;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    .line 52
    iput-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->v:Lbhu;

    .line 53
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->v:Lbhu;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "DialpadFragment.initPhoneNumberFormattingTextWatcher"

    new-instance v3, Lbib;

    .line 58
    invoke-direct {v3}, Lbib;-><init>()V

    .line 59
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lbhq;

    invoke-direct {v1, p0}, Lbhq;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    .line 60
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->z:Lbdy;

    .line 62
    return-void

    .line 42
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f0e018c

    const/16 v5, 0xc

    const/4 v2, 0x0

    .line 63
    const-string v0, "DialpadFragment.onCreateView"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 64
    const v0, 0x7f040050

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 65
    invoke-virtual {v3}, Landroid/view/View;->buildLayer()V

    .line 66
    const v0, 0x7f0e0190

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadView;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 67
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->a(Z)V

    .line 68
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 69
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadView;->b:Landroid/widget/EditText;

    .line 70
    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    .line 71
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    sget-object v1, Lbiq;->a:Lbiq;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 72
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 74
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 75
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setElegantTextHeight(Z)V

    .line 77
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->z:Lbdy;

    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    .line 78
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_1

    .line 81
    new-array v4, v5, [I

    fill-array-data v4, :array_0

    move v1, v2

    .line 82
    :goto_0
    if-ge v1, v5, :cond_0

    aget v0, v4, v1

    .line 83
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 85
    iput-object p0, v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;->b:Lcom/android/dialer/dialpadview/DialpadKeyButton$a;

    .line 86
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 88
    invoke-virtual {v0, p0}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 89
    const v0, 0x7f0e0182

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 90
    invoke-virtual {v0, p0}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 92
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadView;->c:Landroid/widget/ImageButton;

    .line 93
    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->p:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->p:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 97
    :cond_2
    const v0, 0x7f0e00a9

    .line 98
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbhr;

    invoke-direct {v1, p0}, Lbhr;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    .line 99
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 100
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 101
    const v0, 0x7f0e0183

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    .line 102
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 103
    const v0, 0x7f0e0185

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->d:Landroid/support/design/widget/FloatingActionButton;

    .line 104
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->d:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    new-instance v0, Lbud;

    .line 106
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->d:Landroid/support/design/widget/FloatingActionButton;

    invoke-direct {v0, v1, v2}, Lbud;-><init>(Landroid/app/Activity;Landroid/support/design/widget/FloatingActionButton;)V

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 107
    return-object v3

    .line 81
    :array_0
    .array-data 4
        0x7f0e018c
        0x7f0e017a
        0x7f0e017b
        0x7f0e017c
        0x7f0e017d
        0x7f0e017e
        0x7f0e017f
        0x7f0e0180
        0x7f0e0181
        0x7f0e018f
        0x7f0e0182
        0x7f0e018e
    .end array-data
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 302
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    .line 304
    invoke-virtual {v0}, Lbif;->a()V

    .line 305
    iput-object v1, v0, Lbif;->a:Lbij;

    .line 306
    iput-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->e:Lbif;

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->v:Lbhu;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 308
    return-void
.end method

.method public final onHiddenChanged(Z)V
    .locals 4

    .prologue
    .line 641
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 642
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 645
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    if-eqz v0, :cond_2

    .line 646
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->a()V

    .line 647
    :cond_2
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lbhs;

    invoke-direct {v3, p0}, Lbhs;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    .line 648
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->h:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->m:I

    int-to-long v0, v0

    .line 649
    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 650
    const-class v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    invoke-interface {v0}, Lcom/android/dialer/dialpadview/DialpadFragment$a;->g()V

    .line 651
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 648
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 652
    :cond_4
    if-eqz p1, :cond_0

    .line 653
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 654
    iget-object v0, v0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 655
    const/4 v1, 0x0

    .line 656
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 568
    .line 569
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhx;

    .line 570
    iget v0, v0, Lbhx;->c:I

    .line 571
    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    .line 572
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(Z)V

    .line 578
    :goto_0
    return-void

    .line 573
    :cond_0
    const/16 v1, 0x66

    if-ne v0, v1, :cond_1

    .line 574
    invoke-direct {p0, v4}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(Z)V

    goto :goto_0

    .line 575
    :cond_1
    const/16 v1, 0x67

    if-ne v0, v1, :cond_2

    .line 576
    invoke-virtual {p0, v4}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    goto :goto_0

    .line 577
    :cond_2
    const-string v1, "DialpadFragment.onItemClick"

    const/16 v2, 0x1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected itemId: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 342
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e0198

    if-ne v0, v1, :cond_0

    .line 343
    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 344
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->i()V

    .line 345
    const/4 v0, 0x1

    .line 346
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 393
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 394
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 395
    const v4, 0x7f0e0199

    if-ne v3, v4, :cond_1

    .line 396
    invoke-interface {v2}, Landroid/text/Editable;->clear()V

    .line 456
    :cond_0
    :goto_0
    return v0

    .line 398
    :cond_1
    const v2, 0x7f0e018c

    if-ne v3, v2, :cond_9

    .line 399
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    const-string v3, "1"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 400
    :cond_2
    const/16 v2, 0x31

    invoke-direct {p0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(C)V

    .line 402
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lapw;->r(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 404
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "voicemail"

    .line 405
    invoke-static {v3, v4}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 406
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 408
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_4

    if-nez v3, :cond_4

    move v2, v0

    .line 409
    :goto_1
    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->l()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 412
    :cond_3
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lbbf$a;->c:Lbbf$a;

    invoke-static {v2, v3}, Lbbh;->a(Landroid/telecom/PhoneAccountHandle;Lbbf$a;)Lbbh;

    move-result-object v2

    .line 413
    invoke-static {v1, v2}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 414
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->h()V

    goto :goto_0

    :cond_4
    move v2, v1

    .line 408
    goto :goto_1

    .line 416
    :cond_5
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    .line 419
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_6

    move v2, v0

    .line 420
    :goto_2
    if-eqz v2, :cond_7

    .line 421
    const v2, 0x7f11014e

    .line 424
    new-instance v3, Lbhy;

    invoke-direct {v3}, Lbhy;-><init>()V

    .line 425
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 426
    const-string v5, "argTitleResId"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 427
    const-string v1, "argMessageResId"

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 428
    invoke-virtual {v3, v4}, Lbhy;->setArguments(Landroid/os/Bundle;)V

    .line 431
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "voicemail_request_during_airplane_mode"

    invoke-virtual {v3, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v2, v1

    .line 419
    goto :goto_2

    .line 433
    :cond_7
    const v2, 0x7f11014f

    .line 436
    new-instance v3, Lbhy;

    invoke-direct {v3}, Lbhy;-><init>()V

    .line 437
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 438
    const-string v5, "argTitleResId"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 439
    const-string v1, "argMessageResId"

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 440
    invoke-virtual {v3, v4}, Lbhy;->setArguments(Landroid/os/Bundle;)V

    .line 443
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "voicemail_not_ready"

    invoke-virtual {v3, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 445
    goto/16 :goto_0

    .line 446
    :cond_9
    const v2, 0x7f0e0182

    if-ne v3, v2, :cond_b

    .line 447
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 448
    const/16 v1, 0x30

    invoke-direct {p0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(C)V

    .line 449
    :cond_a
    const/16 v1, 0x51

    invoke-direct {p0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(I)V

    .line 450
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->j()V

    .line 451
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 453
    :cond_b
    const v2, 0x7f0e0198

    if-ne v3, v2, :cond_c

    .line 454
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setCursorVisible(Z)V

    move v0, v1

    .line 455
    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 456
    goto/16 :goto_0
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 583
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 584
    const v1, 0x7f0e029f

    if-ne v0, v1, :cond_0

    .line 585
    const/16 v0, 0x2c

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(C)V

    .line 595
    :goto_0
    return v10

    .line 587
    :cond_0
    const v1, 0x7f0e02a0

    if-ne v0, v1, :cond_1

    .line 588
    const/16 v0, 0x3b

    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(C)V

    goto :goto_0

    .line 590
    :cond_1
    const v1, 0x7f0e02a1

    if-ne v0, v1, :cond_2

    .line 591
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 592
    const-wide/16 v2, -0x1

    move-object v5, v4

    move-object v7, v6

    move-object v8, v4

    move-object v9, v4

    move-object v11, v4

    invoke-static/range {v1 .. v11}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a(Landroid/app/Activity;JLandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/telecom/PhoneAccountHandle;)V

    .line 593
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->h()V

    goto :goto_0

    .line 595
    :cond_2
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 285
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->j()V

    .line 286
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 287
    const-string v0, ""

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->f:Ljava/lang/String;

    .line 288
    invoke-static {}, Lbib;->a()V

    .line 289
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 290
    return-void
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    const-string v0, "DialpadFragment.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 184
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 185
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 186
    const v0, 0x7f020133

    .line 187
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lbib;->u(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 188
    const v0, 0x7f020102

    .line 189
    :cond_0
    iget-object v4, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->c:Lbud;

    .line 190
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f110111

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 192
    iget v6, v4, Lbud;->b:I

    if-eq v6, v0, :cond_1

    .line 193
    iget-object v6, v4, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v6, v0}, Landroid/support/design/widget/FloatingActionButton;->setImageResource(I)V

    .line 194
    iget-object v6, v4, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 195
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x106000b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v5}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 196
    invoke-virtual {v6, v5}, Landroid/support/design/widget/FloatingActionButton;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 197
    iput v0, v4, Lbud;->b:I

    .line 198
    :cond_1
    iget-object v0, v4, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    iget-object v0, v4, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, v3}, Landroid/support/design/widget/FloatingActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 200
    :cond_2
    const-class v0, Lcom/android/dialer/dialpadview/DialpadFragment$b;

    .line 201
    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment$b;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->l:Lcom/android/dialer/dialpadview/DialpadFragment$b;

    .line 202
    const-string v0, "Dialpad.onResume"

    .line 203
    new-instance v3, Lalv;

    invoke-direct {v3, v0}, Lalv;-><init>(Ljava/lang/String;)V

    .line 206
    const-string v0, ""

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->f:Ljava/lang/String;

    .line 207
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    const-class v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    invoke-static {p0, v0}, Lapw;->b(Landroid/app/Fragment;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadFragment$a;

    new-instance v4, Lbic;

    invoke-direct {v4, p0}, Lbic;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;)V

    .line 209
    invoke-interface {v0, v4}, Lcom/android/dialer/dialpadview/DialpadFragment$a;->a(Lbic;)V

    .line 210
    :cond_3
    const-string v0, "qloc"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 212
    const-string v4, "dtmf_tone"

    .line 213
    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->u:Z

    .line 214
    const-string v0, "dtwd"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 215
    const-string v0, "hptc"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 217
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 218
    const-string v4, "DialpadFragment.configureScreenFromIntent"

    const-string v5, "action: %s"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->g()Z

    move-result v4

    if-nez v4, :cond_6

    .line 220
    const-string v0, "DialpadFragment.configureScreenFromIntent"

    const-string v4, "Screen configuration is requested before onCreateView() is called. Ignored"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    :goto_1
    const-string v0, "fdin"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 240
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 241
    const-string v0, "DialpadFragment.onResume"

    const-string v4, "phone not in use"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    invoke-virtual {p0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    .line 243
    :cond_4
    const-string v0, "hnt"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->c()V

    .line 245
    const-string v0, "bes"

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 246
    const-string v0, "DialpadFragment"

    .line 247
    const-string v0, ""

    invoke-virtual {v3, v0}, Lalv;->a(Ljava/lang/String;)V

    .line 248
    iget-object v0, v3, Lalv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 249
    iget-object v0, v3, Lalv;->b:Ljava/util/ArrayList;

    iget-object v6, v3, Lalv;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 250
    sub-long/2addr v6, v4

    .line 251
    const-wide/16 v8, 0x32

    cmp-long v0, v6, v8

    if-ltz v0, :cond_a

    .line 252
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    iget-object v0, v3, Lalv;->a:Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string v0, ","

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 256
    const-string v0, ": "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :goto_2
    iget-object v0, v3, Lalv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 259
    iget-object v0, v3, Lalv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 260
    iget-object v0, v3, Lalv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    const-string v0, ","

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    sub-long v4, v6, v4

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 263
    const-string v0, " "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    add-int/lit8 v1, v1, 0x1

    move-wide v4, v6

    goto :goto_2

    :cond_5
    move v0, v2

    .line 213
    goto/16 :goto_0

    .line 222
    :cond_6
    invoke-static {v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 223
    const-string v0, "DialpadFragment.configureScreenFromIntent"

    const-string v4, "Add call mode"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    invoke-virtual {p0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    .line 226
    iput-boolean v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    goto/16 :goto_1

    .line 228
    :cond_7
    invoke-direct {p0, v0}, Lcom/android/dialer/dialpadview/DialpadFragment;->b(Landroid/content/Intent;)Z

    move-result v0

    .line 229
    iget-boolean v4, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    if-eqz v4, :cond_8

    if-nez v0, :cond_9

    :cond_8
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 230
    const-string v0, "DialpadFragment.configureScreenFromIntent"

    const-string v4, "Dialpad chooser mode"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    invoke-virtual {p0, v1}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    .line 233
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    goto/16 :goto_1

    .line 235
    :cond_9
    const-string v0, "DialpadFragment.configureScreenFromIntent"

    const-string v4, "Nothing to show"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    invoke-virtual {p0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Z)V

    .line 238
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->g:Z

    goto/16 :goto_1

    .line 267
    :cond_a
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 268
    iget-object v0, v0, Lcom/android/dialer/dialpadview/DialpadView;->d:Landroid/view/View;

    .line 269
    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    .line 270
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    .line 271
    new-instance v1, Lbht;

    .line 272
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, p0, v3, v0}, Lbht;-><init>(Lcom/android/dialer/dialpadview/DialpadFragment;Landroid/content/Context;Landroid/view/View;)V

    .line 273
    const v0, 0x7f130003

    invoke-virtual {v1, v0}, Landroid/widget/PopupMenu;->inflate(I)V

    .line 274
    invoke-virtual {v1, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 276
    iput-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    .line 277
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->o:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getDragToOpenListener()Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 278
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x4

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 280
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    if-eqz v0, :cond_b

    .line 281
    invoke-virtual {p0, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->onHiddenChanged(Z)V

    .line 282
    :cond_b
    iput-boolean v2, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    .line 283
    return-void

    :cond_c
    move v0, v2

    .line 279
    goto :goto_3
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 299
    const-string v0, "pref_digits_filled_by_intent"

    iget-boolean v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 300
    return-void
.end method

.method public final onStart()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 168
    const-string v0, "DialpadFragment.onStart"

    const-string v1, "first launch: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->y:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 171
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 173
    :try_start_1
    new-instance v0, Landroid/media/ToneGenerator;

    const/16 v4, 0x8

    const/16 v5, 0x50

    invoke-direct {v0, v4, v5}, Landroid/media/ToneGenerator;-><init>(II)V

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 180
    const-wide/16 v2, 0x32

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 181
    const-string v2, "DialpadFragment.onStart"

    const/16 v3, 0x35

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Time for ToneGenerator creation: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    :cond_1
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    :try_start_3
    const-string v4, "DialpadFragment.onStart"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x36

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Exception caught while creating local tone generator: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 291
    const-string v0, "DialpadFragment.onStop"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 292
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 293
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    invoke-virtual {v0}, Landroid/media/ToneGenerator;->release()V

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->q:Landroid/media/ToneGenerator;

    .line 297
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 18
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->w:Z

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 22
    iget-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->w:Z

    .line 23
    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 24
    iget-object v1, v1, Lcom/android/dialer/dialpadview/DialpadView;->d:Landroid/view/View;

    .line 25
    iput-object v1, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    .line 26
    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    invoke-static {v0, v2}, Lamn;->b(Landroid/view/View;I)V

    .line 29
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadFragment;->n:Landroid/view/View;

    invoke-static {v0, v2}, Lamn;->a(Landroid/view/View;I)V

    goto :goto_0
.end method
