.class public Lcom/android/dialer/dialpadview/DialpadView;
.super Landroid/widget/LinearLayout;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/dialpadview/DialpadView$a;
    }
.end annotation


# static fields
.field public static final a:[I

.field private static h:Ljava/lang/String;


# instance fields
.field public b:Landroid/widget/EditText;

.field public c:Landroid/widget/ImageButton;

.field public d:Landroid/view/View;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/widget/TextView;

.field private i:Landroid/util/AttributeSet;

.field private j:Landroid/content/res/ColorStateList;

.field private k:Lcom/android/dialer/dialpadview/DialpadView$a;

.field private l:[Ljava/lang/String;

.field private m:[Ljava/lang/String;

.field private n:Z

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222
    const-class v0, Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/dialer/dialpadview/DialpadView;->h:Ljava/lang/String;

    .line 223
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    return-void

    :array_0
    .array-data 4
        0x7f0e0182
        0x7f0e018c
        0x7f0e017a
        0x7f0e017b
        0x7f0e017c
        0x7f0e017d
        0x7f0e017e
        0x7f0e017f
        0x7f0e0180
        0x7f0e0181
        0x7f0e018f
        0x7f0e018e
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/dialer/dialpadview/DialpadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/dialer/dialpadview/DialpadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    iput-object p2, p0, Lcom/android/dialer/dialpadview/DialpadView;->i:Landroid/util/AttributeSet;

    .line 7
    sget-object v1, Lbik;->a:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 8
    sget v2, Lbik;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/dialer/dialpadview/DialpadView;->j:Landroid/content/res/ColorStateList;

    .line 9
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 11
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0121

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/dialer/dialpadview/DialpadView;->o:I

    .line 13
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->n:Z

    .line 14
    invoke-static {}, Lbhl;->b()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->l:[Ljava/lang/String;

    .line 15
    invoke-static {p1}, Lbhl;->b(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->m:[Ljava/lang/String;

    .line 16
    new-instance v0, Lcom/android/dialer/dialpadview/DialpadView$a;

    .line 17
    invoke-direct {v0, p0}, Lcom/android/dialer/dialpadview/DialpadView$a;-><init>(Lcom/android/dialer/dialpadview/DialpadView;)V

    .line 18
    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->k:Lcom/android/dialer/dialpadview/DialpadView$a;

    .line 19
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const v13, 0x7f0e017b

    const v12, 0x7f0e017a

    const/16 v4, 0x14a

    const/16 v3, 0x129

    const/16 v2, 0x108

    .line 108
    new-instance v6, Lbie;

    invoke-direct {v6}, Lbie;-><init>()V

    .line 109
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    array-length v0, v0

    if-ge v1, v0, :cond_38

    .line 110
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v0, v0, v1

    .line 111
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 112
    iget-boolean v5, p0, Lcom/android/dialer/dialpadview/DialpadView;->n:Z

    if-eqz v5, :cond_c

    .line 113
    if-ne v0, v13, :cond_1

    .line 114
    const/16 v0, 0x21

    .line 181
    :goto_1
    int-to-double v8, v0

    const-wide v10, 0x3fe51eb851eb851fL    # 0.66

    mul-double/2addr v8, v10

    double-to-int v7, v8

    .line 182
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v0, v0, v1

    .line 183
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 184
    iget-boolean v5, p0, Lcom/android/dialer/dialpadview/DialpadView;->n:Z

    if-eqz v5, :cond_29

    .line 185
    const v5, 0x7f0e018c

    if-eq v0, v5, :cond_0

    const v5, 0x7f0e017c

    if-eq v0, v5, :cond_0

    const v5, 0x7f0e017f

    if-eq v0, v5, :cond_0

    const v5, 0x7f0e018f

    if-ne v0, v5, :cond_25

    :cond_0
    move v0, v2

    .line 205
    :goto_2
    int-to-double v8, v0

    const-wide v10, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v8, v10

    double-to-int v8, v8

    .line 206
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 207
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v9

    .line 208
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v5

    if-eqz v5, :cond_37

    .line 209
    iget-boolean v5, p0, Lcom/android/dialer/dialpadview/DialpadView;->n:Z

    if-eqz v5, :cond_36

    const/4 v5, -0x1

    :goto_3
    iget v10, p0, Lcom/android/dialer/dialpadview/DialpadView;->o:I

    mul-int/2addr v5, v10

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setTranslationX(F)V

    .line 210
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 213
    :goto_4
    sget-object v0, Lamn;->c:Landroid/view/animation/Interpolator;

    .line 214
    invoke-virtual {v9, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v10, v7

    .line 215
    invoke-virtual {v0, v10, v11}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v8, v8

    .line 216
    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 217
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 219
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 115
    :cond_1
    const v5, 0x7f0e017e

    if-ne v0, v5, :cond_2

    .line 116
    const/16 v0, 0x42

    goto :goto_1

    .line 117
    :cond_2
    const v5, 0x7f0e0181

    if-ne v0, v5, :cond_3

    .line 118
    const/16 v0, 0x63

    goto/16 :goto_1

    .line 119
    :cond_3
    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_4

    .line 120
    const/16 v0, 0x84

    goto/16 :goto_1

    .line 121
    :cond_4
    if-ne v0, v12, :cond_5

    .line 122
    const/16 v0, 0xa5

    goto/16 :goto_1

    .line 123
    :cond_5
    const v5, 0x7f0e017d

    if-ne v0, v5, :cond_6

    .line 124
    const/16 v0, 0xc6

    goto/16 :goto_1

    .line 125
    :cond_6
    const v5, 0x7f0e0180

    if-ne v0, v5, :cond_7

    .line 126
    const/16 v0, 0xe7

    goto/16 :goto_1

    .line 127
    :cond_7
    const v5, 0x7f0e0182

    if-ne v0, v5, :cond_8

    move v0, v2

    .line 128
    goto/16 :goto_1

    .line 129
    :cond_8
    const v5, 0x7f0e018c

    if-ne v0, v5, :cond_9

    move v0, v3

    .line 130
    goto/16 :goto_1

    .line 131
    :cond_9
    const v5, 0x7f0e017c

    if-ne v0, v5, :cond_a

    move v0, v4

    .line 132
    goto/16 :goto_1

    .line 133
    :cond_a
    const v5, 0x7f0e017f

    if-eq v0, v5, :cond_b

    const v5, 0x7f0e018f

    if-ne v0, v5, :cond_24

    .line 134
    :cond_b
    const/16 v0, 0x16b

    goto/16 :goto_1

    .line 135
    :cond_c
    const v5, 0x7f0e018c

    if-ne v0, v5, :cond_d

    .line 136
    const/16 v0, 0x21

    goto/16 :goto_1

    .line 137
    :cond_d
    const v5, 0x7f0e017c

    if-ne v0, v5, :cond_e

    .line 138
    const/16 v0, 0x42

    goto/16 :goto_1

    .line 139
    :cond_e
    const v5, 0x7f0e017f

    if-ne v0, v5, :cond_f

    .line 140
    const/16 v0, 0x63

    goto/16 :goto_1

    .line 141
    :cond_f
    const v5, 0x7f0e018f

    if-ne v0, v5, :cond_10

    .line 142
    const/16 v0, 0x84

    goto/16 :goto_1

    .line 143
    :cond_10
    if-ne v0, v12, :cond_11

    .line 144
    const/16 v0, 0xa5

    goto/16 :goto_1

    .line 145
    :cond_11
    const v5, 0x7f0e017d

    if-ne v0, v5, :cond_12

    .line 146
    const/16 v0, 0xc6

    goto/16 :goto_1

    .line 147
    :cond_12
    const v5, 0x7f0e0180

    if-ne v0, v5, :cond_13

    .line 148
    const/16 v0, 0xe7

    goto/16 :goto_1

    .line 149
    :cond_13
    const v5, 0x7f0e0182

    if-ne v0, v5, :cond_14

    move v0, v2

    .line 150
    goto/16 :goto_1

    .line 151
    :cond_14
    if-ne v0, v13, :cond_15

    move v0, v3

    .line 152
    goto/16 :goto_1

    .line 153
    :cond_15
    const v5, 0x7f0e017e

    if-ne v0, v5, :cond_16

    move v0, v4

    .line 154
    goto/16 :goto_1

    .line 155
    :cond_16
    const v5, 0x7f0e0181

    if-eq v0, v5, :cond_17

    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_24

    .line 156
    :cond_17
    const/16 v0, 0x16b

    goto/16 :goto_1

    .line 157
    :cond_18
    const v5, 0x7f0e018c

    if-ne v0, v5, :cond_19

    .line 158
    const/16 v0, 0x21

    goto/16 :goto_1

    .line 159
    :cond_19
    if-ne v0, v12, :cond_1a

    .line 160
    const/16 v0, 0x42

    goto/16 :goto_1

    .line 161
    :cond_1a
    if-ne v0, v13, :cond_1b

    .line 162
    const/16 v0, 0x63

    goto/16 :goto_1

    .line 163
    :cond_1b
    const v5, 0x7f0e017c

    if-ne v0, v5, :cond_1c

    .line 164
    const/16 v0, 0x84

    goto/16 :goto_1

    .line 165
    :cond_1c
    const v5, 0x7f0e017d

    if-ne v0, v5, :cond_1d

    .line 166
    const/16 v0, 0xa5

    goto/16 :goto_1

    .line 167
    :cond_1d
    const v5, 0x7f0e017e

    if-ne v0, v5, :cond_1e

    .line 168
    const/16 v0, 0xc6

    goto/16 :goto_1

    .line 169
    :cond_1e
    const v5, 0x7f0e017f

    if-ne v0, v5, :cond_1f

    .line 170
    const/16 v0, 0xe7

    goto/16 :goto_1

    .line 171
    :cond_1f
    const v5, 0x7f0e0180

    if-ne v0, v5, :cond_20

    move v0, v2

    .line 172
    goto/16 :goto_1

    .line 173
    :cond_20
    const v5, 0x7f0e0181

    if-ne v0, v5, :cond_21

    move v0, v3

    .line 174
    goto/16 :goto_1

    .line 175
    :cond_21
    const v5, 0x7f0e018f

    if-ne v0, v5, :cond_22

    move v0, v4

    .line 176
    goto/16 :goto_1

    .line 177
    :cond_22
    const v5, 0x7f0e0182

    if-eq v0, v5, :cond_23

    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_24

    .line 178
    :cond_23
    const/16 v0, 0x16b

    goto/16 :goto_1

    .line 179
    :cond_24
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->h:Ljava/lang/String;

    const-string v5, "Attempted to get animation delay for invalid key button id."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v5, v7}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 180
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 187
    :cond_25
    if-eq v0, v12, :cond_26

    const v5, 0x7f0e017d

    if-eq v0, v5, :cond_26

    const v5, 0x7f0e0180

    if-eq v0, v5, :cond_26

    const v5, 0x7f0e0182

    if-ne v0, v5, :cond_27

    :cond_26
    move v0, v3

    .line 188
    goto/16 :goto_2

    .line 189
    :cond_27
    if-eq v0, v13, :cond_28

    const v5, 0x7f0e017e

    if-eq v0, v5, :cond_28

    const v5, 0x7f0e0181

    if-eq v0, v5, :cond_28

    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_35

    :cond_28
    move v0, v4

    .line 190
    goto/16 :goto_2

    .line 191
    :cond_29
    const v5, 0x7f0e018c

    if-eq v0, v5, :cond_2a

    const v5, 0x7f0e017c

    if-eq v0, v5, :cond_2a

    const v5, 0x7f0e017f

    if-eq v0, v5, :cond_2a

    const v5, 0x7f0e018f

    if-ne v0, v5, :cond_2b

    :cond_2a
    move v0, v4

    .line 192
    goto/16 :goto_2

    .line 193
    :cond_2b
    if-eq v0, v12, :cond_2c

    const v5, 0x7f0e017d

    if-eq v0, v5, :cond_2c

    const v5, 0x7f0e0180

    if-eq v0, v5, :cond_2c

    const v5, 0x7f0e0182

    if-ne v0, v5, :cond_2d

    :cond_2c
    move v0, v3

    .line 194
    goto/16 :goto_2

    .line 195
    :cond_2d
    if-eq v0, v13, :cond_2e

    const v5, 0x7f0e017e

    if-eq v0, v5, :cond_2e

    const v5, 0x7f0e0181

    if-eq v0, v5, :cond_2e

    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_35

    :cond_2e
    move v0, v2

    .line 196
    goto/16 :goto_2

    .line 197
    :cond_2f
    const v5, 0x7f0e018c

    if-eq v0, v5, :cond_30

    if-eq v0, v12, :cond_30

    if-eq v0, v13, :cond_30

    const v5, 0x7f0e017c

    if-eq v0, v5, :cond_30

    const v5, 0x7f0e017d

    if-eq v0, v5, :cond_30

    const v5, 0x7f0e017e

    if-ne v0, v5, :cond_31

    :cond_30
    move v0, v4

    .line 198
    goto/16 :goto_2

    .line 199
    :cond_31
    const v5, 0x7f0e017f

    if-eq v0, v5, :cond_32

    const v5, 0x7f0e0180

    if-eq v0, v5, :cond_32

    const v5, 0x7f0e0181

    if-ne v0, v5, :cond_33

    :cond_32
    move v0, v3

    .line 200
    goto/16 :goto_2

    .line 201
    :cond_33
    const v5, 0x7f0e018f

    if-eq v0, v5, :cond_34

    const v5, 0x7f0e0182

    if-eq v0, v5, :cond_34

    const v5, 0x7f0e018e

    if-ne v0, v5, :cond_35

    :cond_34
    move v0, v2

    .line 202
    goto/16 :goto_2

    .line 203
    :cond_35
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->h:Ljava/lang/String;

    const-string v5, "Attempted to get animation duration for invalid key button id."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v5, v8}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 209
    :cond_36
    const/4 v5, 0x1

    goto/16 :goto_3

    .line 211
    :cond_37
    iget v5, p0, Lcom/android/dialer/dialpadview/DialpadView;->o:I

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setTranslationY(F)V

    .line 212
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_4

    .line 220
    :cond_38
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 97
    const v0, 0x7f0e0199

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 98
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 99
    const v0, 0x7f0e0197

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 100
    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 101
    const v0, 0x7f0e0198

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 102
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setClickable(Z)V

    .line 103
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setLongClickable(Z)V

    .line 104
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 105
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 106
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 100
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method final b()Z
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadView;->k:Lcom/android/dialer/dialpadview/DialpadView$a;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 22
    return-void
.end method

.method protected onFinishInflate()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 23
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 25
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 26
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 27
    const-string v1, "fa"

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 28
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lapw;->t(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/DecimalFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    move-object v3, v0

    :goto_0
    move v4, v5

    .line 30
    :goto_1
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    array-length v0, v0

    if-ge v4, v0, :cond_8

    .line 31
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 32
    const v1, 0x7f0e0187

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 33
    sget-object v2, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v2, v2, v4

    const v6, 0x7f0e018e

    if-ne v2, v6, :cond_4

    .line 34
    const v2, 0x7f110150

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    move-object v7, v2

    .line 49
    :goto_2
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v9, 0x7f020061

    .line 50
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x15

    if-lt v10, v11, :cond_6

    .line 51
    invoke-virtual {v2, v9}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 53
    :goto_3
    check-cast v2, Landroid/graphics/drawable/RippleDrawable;

    .line 54
    iget-object v9, p0, Lcom/android/dialer/dialpadview/DialpadView;->j:Landroid/content/res/ColorStateList;

    if-eqz v9, :cond_0

    .line 55
    iget-object v9, p0, Lcom/android/dialer/dialpadview/DialpadView;->j:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v9}, Landroid/graphics/drawable/RippleDrawable;->setColor(Landroid/content/res/ColorStateList;)V

    .line 56
    :cond_0
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setElegantTextHeight(Z)V

    .line 58
    invoke-virtual {v0, v6}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {v0, v2}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 60
    const v1, 0x7f0e0189

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 61
    const v2, 0x7f0e018a

    .line 62
    invoke-virtual {v0, v2}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    if-eqz v1, :cond_1

    .line 64
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadView;->l:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 66
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadView;->m:[Ljava/lang/String;

    if-nez v2, :cond_7

    .line 67
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    :cond_2
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 29
    :cond_3
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/DecimalFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_0

    .line 36
    :cond_4
    sget-object v2, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    aget v2, v2, v4

    const v6, 0x7f0e018f

    if-ne v2, v6, :cond_5

    .line 37
    const v2, 0x7f110151

    invoke-virtual {v8, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    move-object v7, v2

    .line 38
    goto :goto_2

    .line 39
    :cond_5
    int-to-long v6, v4

    invoke-virtual {v3, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 40
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadView;->l:[Ljava/lang/String;

    aget-object v7, v2, v4

    .line 41
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    .line 42
    new-instance v9, Landroid/text/style/TtsSpan$VerbatimBuilder;

    invoke-direct {v9, v7}, Landroid/text/style/TtsSpan$VerbatimBuilder;-><init>(Ljava/lang/String;)V

    .line 43
    invoke-virtual {v9}, Landroid/text/style/TtsSpan$VerbatimBuilder;->build()Landroid/text/style/TtsSpan;

    move-result-object v9

    .line 44
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    .line 45
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v11

    const/16 v11, 0x21

    .line 46
    invoke-interface {v2, v9, v10, v7, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    move-object v7, v6

    move-object v6, v2

    .line 47
    goto/16 :goto_2

    .line 52
    :cond_6
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto/16 :goto_3

    .line 68
    :cond_7
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    iget-object v2, p0, Lcom/android/dialer/dialpadview/DialpadView;->m:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 72
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    iget-object v6, p0, Lcom/android/dialer/dialpadview/DialpadView;->i:Landroid/util/AttributeSet;

    sget-object v7, Lbik;->a:[I

    .line 73
    invoke-virtual {v2, v6, v7, v5, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 74
    sget v6, Lbik;->c:I

    .line 75
    invoke-virtual {v2, v6, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 76
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 77
    int-to-float v2, v6

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 78
    int-to-float v1, v6

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_4

    .line 80
    :cond_8
    const v0, 0x7f0e018c

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 81
    const v1, 0x7f11012e

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->a(Ljava/lang/CharSequence;)V

    .line 82
    const v0, 0x7f0e0182

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 83
    const v1, 0x7f110115

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->a(Ljava/lang/CharSequence;)V

    .line 84
    const v0, 0x7f0e0198

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->b:Landroid/widget/EditText;

    .line 85
    const v0, 0x7f0e0199

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->c:Landroid/widget/ImageButton;

    .line 86
    const v0, 0x7f0e0197

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->d:Landroid/view/View;

    .line 87
    const v0, 0x7f0e0191

    invoke-virtual {p0, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->e:Landroid/view/ViewGroup;

    .line 88
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0e0193

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->f:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0e0194

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->g:Landroid/widget/TextView;

    .line 91
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 92
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 93
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView;->b:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelected(Z)V

    .line 94
    :cond_9
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadView;->k:Lcom/android/dialer/dialpadview/DialpadView$a;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 95
    invoke-virtual {p0}, Lcom/android/dialer/dialpadview/DialpadView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/dialer/dialpadview/DialpadView;->k:Lcom/android/dialer/dialpadview/DialpadView$a;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 96
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    return v0
.end method
