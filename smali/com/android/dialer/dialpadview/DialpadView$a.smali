.class final Lcom/android/dialer/dialpadview/DialpadView$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/dialpadview/DialpadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private synthetic a:Lcom/android/dialer/dialpadview/DialpadView;


# direct methods
.method constructor <init>(Lcom/android/dialer/dialpadview/DialpadView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a()V
    .locals 10

    .prologue
    const v9, 0x7f0e0186

    const/16 v8, 0x9

    const/4 v1, 0x0

    .line 75
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 76
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 77
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    move v2, v1

    move v4, v1

    .line 79
    :goto_1
    if-gt v2, v8, :cond_1

    .line 80
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 81
    sget-object v3, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 82
    aget v3, v3, v2

    invoke-virtual {v0, v3}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 83
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 84
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 85
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v3

    goto :goto_1

    :cond_0
    move v0, v1

    .line 77
    goto :goto_0

    :cond_1
    move v3, v1

    .line 86
    :goto_2
    if-gt v3, v8, :cond_2

    .line 87
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 88
    sget-object v1, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 89
    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 90
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 91
    const v1, 0x7f0e0187

    .line 92
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/dialer/dialpadview/DialpadTextView;

    .line 94
    invoke-virtual {v1}, Lcom/android/dialer/dialpadview/DialpadTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 95
    const v5, 0x7f0e0188

    .line 96
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 97
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    .line 98
    invoke-virtual {v1}, Lcom/android/dialer/dialpadview/DialpadTextView;->getHeight()I

    move-result v1

    sub-int v1, v4, v1

    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v1, v7

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int/2addr v1, v2

    invoke-direct {v5, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 99
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 101
    :cond_2
    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 10

    .prologue
    const v9, 0x7f0e0186

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 4
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 7
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 8
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 9
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 10
    sget-object v1, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 11
    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 13
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 14
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    move v1, v2

    .line 16
    :goto_0
    sget-object v0, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 17
    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 18
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 19
    sget-object v5, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 20
    aget v5, v5, v1

    invoke-virtual {v0, v5}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 21
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 22
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    if-eq v4, v0, :cond_0

    move v0, v2

    .line 44
    :goto_1
    if-nez v0, :cond_6

    .line 74
    :goto_2
    return v2

    .line 24
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 26
    goto :goto_1

    .line 27
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 28
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 29
    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 30
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 31
    sget-object v1, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 32
    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 33
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 34
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    move v1, v2

    .line 35
    :goto_4
    const/16 v0, 0x9

    if-gt v1, v0, :cond_5

    .line 36
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 37
    sget-object v5, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 38
    aget v5, v5, v1

    invoke-virtual {v0, v5}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 39
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 40
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    if-eq v4, v0, :cond_4

    move v0, v2

    .line 41
    goto :goto_1

    :cond_3
    move v0, v3

    .line 29
    goto :goto_3

    .line 42
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_5
    move v0, v3

    .line 43
    goto :goto_1

    .line 47
    :cond_6
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 48
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 49
    if-eqz v0, :cond_8

    .line 51
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    .line 52
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->b()Z

    move-result v0

    .line 53
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 55
    sget-object v4, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 56
    array-length v5, v4

    move v1, v3

    move v2, v3

    :goto_5
    if-ge v1, v5, :cond_7

    aget v0, v4, v1

    .line 57
    iget-object v6, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v6, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 58
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 59
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 60
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 61
    :cond_7
    sget-object v4, Lcom/android/dialer/dialpadview/DialpadView;->a:[I

    .line 62
    array-length v5, v4

    move v1, v3

    :goto_6
    if-ge v1, v5, :cond_9

    aget v0, v4, v1

    .line 63
    iget-object v6, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v6, v0}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 64
    invoke-virtual {v0, v9}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 65
    const v6, 0x7f0e018b

    .line 66
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 67
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    .line 68
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    sub-int v0, v2, v0

    const/4 v8, -0x1

    invoke-direct {v7, v0, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 69
    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 72
    :cond_8
    invoke-direct {p0}, Lcom/android/dialer/dialpadview/DialpadView$a;->a()V

    .line 73
    :cond_9
    iget-object v0, p0, Lcom/android/dialer/dialpadview/DialpadView$a;->a:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    move v2, v3

    .line 74
    goto/16 :goto_2
.end method
