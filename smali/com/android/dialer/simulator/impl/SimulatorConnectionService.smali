.class public Lcom/android/dialer/simulator/impl/SimulatorConnectionService;
.super Landroid/telecom/ConnectionService;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;
    }
.end annotation


# static fields
.field public static a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

.field private static b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/ConnectionService;-><init>()V

    return-void
.end method

.method public static a(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V
    .locals 2

    .prologue
    .line 2
    sget-object v1, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    return-void
.end method

.method public static b(Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;)V
    .locals 2

    .prologue
    .line 4
    sget-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5
    return-void
.end method


# virtual methods
.method public onConference(Landroid/telecom/Connection;Landroid/telecom/Connection;)V
    .locals 5

    .prologue
    .line 44
    const-string v0, "SimulatorConnectionService.onConference"

    .line 45
    invoke-static {p1}, Lbib;->a(Landroid/telecom/Connection;)Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-static {p2}, Lbib;->a(Landroid/telecom/Connection;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "connection1: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", connection2: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 47
    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    sget-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;

    move-object v2, p1

    .line 49
    check-cast v2, Lbpz;

    move-object v0, p2

    check-cast v0, Lbpz;

    invoke-interface {v1, v2, v0}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;->a(Lbpz;Lbpz;)V

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 6
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onCreate()V

    .line 7
    sput-object p0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    .line 8
    return-void
.end method

.method public onCreateIncomingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 27
    const-string v1, "SimulatorConnectionService.onCreateIncomingConnection"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 28
    invoke-static {p2}, Lbib;->a(Landroid/telecom/ConnectionRequest;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 29
    const-string v1, "SimulatorConnectionService.onCreateIncomingConnection"

    const-string v2, "incoming call not from simulator, unregistering"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    const-string v1, "Unregistering simulator, got a real incoming call"

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 32
    invoke-static {p0}, Lbib;->Y(Landroid/content/Context;)V

    .line 43
    :goto_0
    return-object v0

    .line 34
    :cond_0
    new-instance v1, Lbpz;

    invoke-direct {v1, p0, p2}, Lbpz;-><init>(Landroid/content/Context;Landroid/telecom/ConnectionRequest;)V

    .line 35
    invoke-virtual {v1}, Lbpz;->setRinging()V

    .line 37
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "incoming_number"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 38
    const-string v3, "tel"

    invoke-static {v3, v2, v0}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 39
    invoke-virtual {v1, v0, v4}, Lbpz;->setAddress(Landroid/net/Uri;I)V

    .line 40
    sget-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;

    .line 41
    invoke-interface {v0, v1}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;->a(Lbpz;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 43
    goto :goto_0
.end method

.method public onCreateOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 13
    const-string v0, "SimulatorConnectionService.onCreateOutgoingConnection"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 14
    invoke-static {p2}, Lbib;->a(Landroid/telecom/ConnectionRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    const-string v0, "SimulatorConnectionService.onCreateOutgoingConnection"

    const-string v1, "outgoing call not from simulator, unregistering"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    const-string v0, "Unregistering simulator, making a real phone call"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 18
    invoke-static {p0}, Lbib;->Y(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    .line 26
    :goto_0
    return-object v0

    .line 20
    :cond_0
    new-instance v1, Lbpz;

    invoke-direct {v1, p0, p2}, Lbpz;-><init>(Landroid/content/Context;Landroid/telecom/ConnectionRequest;)V

    .line 21
    invoke-virtual {v1}, Lbpz;->setDialing()V

    .line 22
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lbpz;->setAddress(Landroid/net/Uri;I)V

    .line 23
    sget-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;

    .line 24
    invoke-interface {v0, v1}, Lcom/android/dialer/simulator/impl/SimulatorConnectionService$a;->b(Lbpz;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 26
    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "SimulatorConnectionService.onDestroy"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/android/dialer/simulator/impl/SimulatorConnectionService;->a:Lcom/android/dialer/simulator/impl/SimulatorConnectionService;

    .line 11
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onDestroy()V

    .line 12
    return-void
.end method
