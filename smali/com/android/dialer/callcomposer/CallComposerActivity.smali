.class public Lcom/android/dialer/callcomposer/CallComposerActivity;
.super Luh;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;
.implements Landroid/view/View$OnClickListener;
.implements Laya$a;
.implements Lbjj;


# instance fields
.field private A:Lcom/android/dialer/widget/DialerToolbar;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/ProgressBar;

.field private D:Lcom/android/dialer/widget/LockableViewPager;

.field private E:Layb;

.field private F:Lbdy;

.field private G:Z

.field private H:Z

.field private I:I

.field public f:Landroid/view/View;

.field public g:Landroid/widget/ImageView;

.field public h:Landroid/widget/ImageView;

.field public i:Landroid/widget/ImageView;

.field public j:Landroid/widget/FrameLayout;

.field public k:Landroid/widget/LinearLayout;

.field public l:Lsk;

.field public m:Z

.field public n:Z

.field public o:Z

.field private p:Landroid/os/Handler;

.field private q:Ljava/lang/Runnable;

.field private r:Ljava/lang/Runnable;

.field private s:I

.field private t:Z

.field private u:Lbhj;

.field private v:Ljava/lang/Long;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/QuickContactBadge;

.field private z:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    .line 2
    invoke-static {}, Lapw;->i()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    .line 3
    new-instance v0, Laxp;

    invoke-direct {v0, p0}, Laxp;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->q:Ljava/lang/Runnable;

    .line 4
    new-instance v0, Laxq;

    invoke-direct {v0, p0}, Laxq;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->r:Ljava/lang/Runnable;

    .line 5
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    .line 6
    iput-boolean v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->m:Z

    .line 7
    iput-boolean v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->o:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lbhj;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/callcomposer/CallComposerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 9
    const-string v1, "CALL_COMPOSER_CONTACT"

    invoke-static {v0, v1, p1}, Lbib;->b(Landroid/content/Intent;Ljava/lang/String;Lhdd;)V

    .line 10
    return-object v0
.end method

.method private final a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 312
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CALL_COMPOSER_CONTACT_BASE64"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const-string v0, "CALL_COMPOSER_CONTACT_BASE64"

    .line 314
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 316
    :try_start_0
    sget-object v1, Lbhj;->l:Lbhj;

    invoke-static {v1, v0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lbhj;

    .line 317
    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :goto_0
    invoke-static {p0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->y:Landroid/widget/QuickContactBadge;

    .line 326
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 327
    iget v0, v0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 328
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 329
    iget-object v0, v0, Lbhj;->d:Ljava/lang/String;

    .line 330
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_1
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 332
    iget-wide v4, v0, Lbhj;->b:J

    .line 333
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 334
    iget v0, v0, Lbhj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v11, :cond_2

    .line 335
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 336
    iget-object v0, v0, Lbhj;->c:Ljava/lang/String;

    .line 337
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    :goto_2
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 339
    iget-object v7, v0, Lbhj;->e:Ljava/lang/String;

    .line 340
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 342
    iget v8, v0, Lbhj;->i:I

    .line 343
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 344
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->w:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 345
    iget-object v1, v1, Lbhj;->e:Ljava/lang/String;

    .line 346
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 348
    iget-object v1, v1, Lbhj;->e:Ljava/lang/String;

    .line 349
    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/DialerToolbar;->a(Ljava/lang/CharSequence;)V

    .line 350
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 351
    iget-object v0, v0, Lbhj;->g:Ljava/lang/String;

    .line 352
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 353
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 355
    iget-object v0, v0, Lbhj;->h:Ljava/lang/String;

    .line 356
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 358
    iget-object v0, v0, Lbhj;->g:Ljava/lang/String;

    .line 368
    :goto_3
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    invoke-virtual {v1, v0}, Lcom/android/dialer/widget/DialerToolbar;->b(Ljava/lang/CharSequence;)V

    .line 373
    :goto_4
    return-void

    .line 319
    :catch_0
    move-exception v0

    .line 320
    invoke-virtual {v0}, Lhcf;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0

    .line 321
    :cond_0
    const-string v0, "CALL_COMPOSER_CONTACT"

    .line 322
    sget-object v1, Lbhj;->l:Lbhj;

    .line 323
    invoke-static {p1, v0, v1}, Lbib;->a(Landroid/content/Intent;Ljava/lang/String;Lhdd;)Lhdd;

    move-result-object v0

    check-cast v0, Lbhj;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    goto/16 :goto_0

    :cond_1
    move-object v3, v9

    .line 330
    goto :goto_1

    :cond_2
    move-object v6, v9

    .line 337
    goto :goto_2

    .line 360
    :cond_3
    const v0, 0x7f1100af

    new-array v1, v11, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 362
    iget-object v2, v2, Lbhj;->h:Ljava/lang/String;

    .line 363
    aput-object v2, v1, v10

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 365
    iget-object v3, v3, Lbhj;->g:Ljava/lang/String;

    .line 366
    aput-object v3, v1, v2

    .line 367
    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 371
    :cond_4
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private final c(Z)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 392
    invoke-static {p0}, Lbib;->ak(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 393
    iput-boolean p1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->o:Z

    .line 394
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 402
    :cond_1
    :goto_0
    return-void

    .line 396
    :cond_2
    iget-boolean v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->o:Z

    if-eq v1, p1, :cond_1

    .line 397
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v2, v1, 0x2

    .line 398
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v3, v1, 0x2

    .line 399
    if-eqz p1, :cond_3

    move v4, v2

    .line 400
    :goto_1
    if-eqz p1, :cond_4

    move v5, v0

    .line 401
    :goto_2
    iget-object v7, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    const/4 v8, 0x1

    new-instance v0, Laxr;

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Laxr;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;IIIIZ)V

    invoke-static {v7, v8, v0}, Lbib;->a(Landroid/view/View;ZLjava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    move v4, v0

    .line 399
    goto :goto_1

    :cond_4
    move v5, v2

    .line 400
    goto :goto_2
.end method

.method private final e(I)V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f333333    # 0.7f

    .line 403
    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->g:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 404
    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->h:Landroid/widget/ImageView;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 405
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->i:Landroid/widget/ImageView;

    const/4 v3, 0x2

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 406
    return-void

    :cond_0
    move v0, v2

    .line 403
    goto :goto_0

    :cond_1
    move v0, v2

    .line 404
    goto :goto_1

    :cond_2
    move v1, v2

    .line 405
    goto :goto_2
.end method

.method private final l()V
    .locals 6

    .prologue
    .line 95
    .line 96
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lbjf;->b(J)Lbjl;

    move-result-object v0

    .line 98
    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    invoke-interface {v0}, Lbjl;->c()I

    move-result v0

    .line 101
    const-string v1, "CallComposerActivity.refreshUiForCallComposerState"

    const-string v2, "state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 102
    invoke-static {v0}, Lbib;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 103
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->q:Ljava/lang/Runnable;

    .line 106
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "ec_session_started_timeout"

    const-wide/16 v4, 0x2710

    invoke-interface {v2, v3, v4, v5}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 107
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 108
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->G:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->m()V

    goto :goto_0

    .line 110
    :pswitch_2
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->q:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 111
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->G:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->g()V

    goto :goto_0

    .line 113
    :pswitch_3
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->t:Z

    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->h()V

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->k()V

    goto :goto_0

    .line 117
    :pswitch_4
    iget v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->s:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->r:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 119
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->h()V

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private final m()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->C:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    const/4 v1, 0x1

    .line 190
    iput-boolean v1, v0, Lcom/android/dialer/widget/LockableViewPager;->f:Z

    .line 191
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 253
    const-string v0, "com.google.android.apps.messaging"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 254
    return-object p1
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public final a(Laya;)V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    iget v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    invoke-virtual {v0, v1, v2}, Layb;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 311
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-virtual {p1}, Laya;->U()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->c(Z)V

    goto :goto_0
.end method

.method public final a(Lblo;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 192
    invoke-virtual {p1}, Lblo;->a()Lbln;

    move-result-object v1

    .line 193
    const-string v0, "CallComposerActivity.placeRCSCall"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "placing enriched call, data: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v2, Lbkq$a;->as:Lbkq$a;

    invoke-interface {v0, v2}, Lbku;->a(Lbkq$a;)V

    .line 196
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 197
    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, v1}, Lbjf;->a(JLbln;)V

    .line 199
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v0

    invoke-virtual {v0}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 200
    const-string v0, "is_first_call_compose"

    invoke-interface {v2, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {v1}, Lbln;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f110193

    .line 202
    :goto_0
    invoke-static {p0, v0, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 203
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d01b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 204
    const/16 v4, 0x51

    invoke-virtual {v0, v4, v5, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 205
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 206
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "is_first_call_compose"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 207
    :cond_0
    invoke-virtual {v1}, Lbln;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v0

    const-string v1, "enable_delayed_ec_images"

    invoke-interface {v0, v1, v10}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    invoke-static {p0}, Lbsp;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->r:Ljava/lang/Runnable;

    .line 211
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v2

    const-string v3, "ec_image_upload_timeout"

    const-wide/16 v4, 0x3a98

    invoke-interface {v2, v3, v4, v5}, Lbew;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 212
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 213
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 215
    iget-object v0, v0, Lbhj;->e:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 218
    iget-object v1, v1, Lbhj;->g:Ljava/lang/String;

    .line 219
    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 221
    iget-object v2, v2, Lbhj;->h:Ljava/lang/String;

    .line 222
    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 224
    iget-object v3, v3, Lbhj;->d:Ljava/lang/String;

    .line 225
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lbib;->e(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f11006d

    .line 226
    invoke-virtual {p0, v4}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 228
    iget-object v5, v5, Lbhj;->c:Ljava/lang/String;

    .line 229
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    .line 230
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 232
    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/android/incallui/callpending/CallPendingActivity;

    invoke-direct {v8, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    const-string v9, "extra_name"

    invoke-virtual {v8, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v0, "extra_number"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const-string v0, "extra_label"

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string v0, "extra_lookup_key"

    invoke-virtual {v8, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v0, "extra_call_pending_label"

    invoke-virtual {v8, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string v0, "extra_photo_uri"

    invoke-virtual {v8, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 239
    const-string v0, "extra_session_id"

    invoke-virtual {v8, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 241
    invoke-virtual {p0, v8}, Lcom/android/dialer/callcomposer/CallComposerActivity;->startActivity(Landroid/content/Intent;)V

    .line 242
    iput-boolean v10, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->t:Z

    .line 244
    :goto_1
    return-void

    .line 201
    :cond_1
    const v0, 0x7f1101f4

    goto/16 :goto_0

    .line 243
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->h()V

    goto :goto_1
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 255
    if-ne p1, v2, :cond_1

    .line 256
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->B:Landroid/widget/TextView;

    const v1, 0x7f1102ad

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 258
    :goto_0
    iget v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    if-ne v0, v2, :cond_0

    .line 259
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-static {p0, v0}, Lapw;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 260
    :cond_0
    iput p1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    .line 261
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v0, v1, p1}, Layb;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laya;

    .line 262
    invoke-virtual {v0}, Laya;->U()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->c(Z)V

    .line 263
    invoke-direct {p0, p1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->e(I)V

    .line 264
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->B:Landroid/widget/TextView;

    const v1, 0x7f1102b4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 375
    iput-boolean p1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->n:Z

    .line 376
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v0}, Lcom/android/dialer/widget/LockableViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 377
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 379
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    invoke-virtual {v1, v4}, Lcom/android/dialer/widget/DialerToolbar;->setVisibility(I)V

    .line 380
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->z:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 389
    :goto_0
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v1, v0}, Lcom/android/dialer/widget/LockableViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 390
    return-void

    .line 381
    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 382
    :cond_1
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    invoke-virtual {v2}, Lcom/android/dialer/widget/DialerToolbar;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 383
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    invoke-virtual {v1, v3}, Lcom/android/dialer/widget/DialerToolbar;->setVisibility(I)V

    .line 384
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->z:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 386
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0095

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 387
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    invoke-virtual {v1, v4}, Lcom/android/dialer/widget/DialerToolbar;->setVisibility(I)V

    .line 388
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->z:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    .line 136
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 137
    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lbjf;->b(J)Lbjl;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lbjl;->c()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 139
    :goto_0
    if-nez v0, :cond_2

    .line 140
    iput-boolean v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->G:Z

    .line 141
    invoke-direct {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->m()V

    .line 142
    const-string v0, "CallComposerActivity.onClick"

    const-string v1, "sendAndCall pressed, but the session isn\'t ready"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->at:Lbkq$a;

    .line 144
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 187
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 138
    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 147
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    iget v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    .line 148
    invoke-virtual {v0, v1, v3}, Layb;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laya;

    .line 149
    invoke-static {}, Lbln;->f()Lblo;

    move-result-object v3

    .line 150
    instance-of v1, v0, Layn;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 151
    check-cast v1, Layn;

    .line 152
    invoke-virtual {v1}, Layn;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lblo;->a(Ljava/lang/String;)Lblo;

    .line 153
    invoke-virtual {p0, v3}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Lblo;)V

    .line 154
    :cond_3
    instance-of v1, v0, Layg;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 155
    check-cast v1, Layg;

    .line 157
    iget-boolean v4, v1, Layg;->X:Z

    .line 158
    if-nez v4, :cond_5

    .line 159
    iget-object v4, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->F:Lbdy;

    .line 161
    iget-object v1, v1, Layg;->W:Layl;

    .line 162
    invoke-virtual {v1}, Layl;->a()Landroid/net/Uri;

    move-result-object v1

    .line 163
    invoke-interface {v4, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 180
    :cond_4
    :goto_2
    instance-of v1, v0, Layc;

    if-eqz v1, :cond_0

    .line 181
    check-cast v0, Layc;

    .line 182
    new-instance v1, Laye;

    invoke-direct {v1, p0, v3, v0}, Laye;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;Lblo;Layc;)V

    .line 183
    iget-boolean v3, v0, Layc;->X:Z

    if-eqz v3, :cond_6

    .line 184
    iget-object v3, v0, Layc;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 185
    iput-object v1, v0, Layc;->Y:Laye;

    goto :goto_1

    .line 165
    :cond_5
    invoke-static {}, Lcom/android/dialer/constants/Constants;->a()Lcom/android/dialer/constants/Constants;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/dialer/constants/Constants;->c()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    .line 167
    iget-object v6, v1, Layg;->W:Layl;

    .line 169
    iget-object v6, v6, Layl;->b:Ljava/lang/String;

    .line 170
    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 171
    invoke-static {p0, v4, v5}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 173
    invoke-virtual {p0, v4}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 175
    iget-object v1, v1, Layg;->W:Layl;

    .line 177
    iget-object v1, v1, Layl;->c:Ljava/lang/String;

    .line 178
    invoke-virtual {v3, v4, v1}, Lblo;->a(Landroid/net/Uri;Ljava/lang/String;)Lblo;

    .line 179
    invoke-virtual {p0, v3}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Lblo;)V

    goto :goto_2

    .line 186
    :cond_6
    iget-object v0, v0, Layc;->W:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Laye;->a(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 245
    new-instance v0, Lbbh;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 247
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 248
    sget-object v2, Lbbf$a;->o:Lbbf$a;

    invoke-direct {v0, v1, v2}, Lbbh;-><init>(Ljava/lang/String;Lbbf$a;)V

    .line 249
    invoke-static {p0, v0}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 250
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->setResult(I)V

    .line 251
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->finish()V

    .line 252
    return-void
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->n:Z

    return v0
.end method

.method public final k()V
    .locals 4

    .prologue
    .line 407
    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "contact_name"

    iget-object v3, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 409
    iget-object v3, v3, Lbhj;->e:Ljava/lang/String;

    .line 410
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 411
    invoke-virtual {p0, v0, v1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->setResult(ILandroid/content/Intent;)V

    .line 412
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->finish()V

    .line 413
    return-void
.end method

.method public final m_()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->l()V

    .line 94
    return-void
.end method

.method public onBackPressed()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 274
    const-string v0, "CallComposerActivity.onBackPressed"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 275
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->o:Z

    if-nez v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    iget v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    invoke-virtual {v0, v1, v2}, Layb;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laya;

    invoke-virtual {v0}, Laya;->V()V

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->H:Z

    if-nez v0, :cond_0

    .line 279
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 280
    invoke-interface {v0, p0}, Lbjf;->b(Lbjj;)V

    .line 282
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lbjf;->b(J)Lbjl;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 285
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lbjf;->a(J)V

    .line 288
    :cond_2
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    .line 289
    :goto_1
    new-array v1, v4, [F

    const/4 v2, 0x0

    aput v2, v1, v6

    int-to-float v0, v0

    aput v0, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 290
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->l:Lsk;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 291
    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 292
    new-instance v1, Laxx;

    invoke-direct {v1, p0}, Laxx;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 293
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->i()Z

    move-result v1

    if-nez v1, :cond_4

    .line 294
    const v1, 0x106000d

    invoke-static {p0, v1}, Llw;->c(Landroid/content/Context;I)I

    move-result v1

    .line 295
    const v2, 0x7f0c002f

    invoke-static {p0, v2}, Llw;->c(Landroid/content/Context;I)I

    move-result v2

    .line 296
    new-instance v3, Landroid/animation/ArgbEvaluator;

    invoke-direct {v3}, Landroid/animation/ArgbEvaluator;-><init>()V

    new-array v4, v4, [Ljava/lang/Object;

    .line 297
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 298
    iget-object v2, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->l:Lsk;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 299
    invoke-virtual {v1, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 300
    new-instance v2, Laxy;

    invoke-direct {v2, p0}, Laxy;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 301
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 302
    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 303
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 306
    :goto_2
    iput-boolean v5, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->H:Z

    goto/16 :goto_0

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    goto :goto_1

    .line 305
    :cond_4
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 124
    const-string v0, "CallComposerActivity.onClick"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->g:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/dialer/widget/LockableViewPager;->a(IZ)V

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->h:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v0, v2, v2}, Lcom/android/dialer/widget/LockableViewPager;->a(IZ)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->i:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/android/dialer/widget/LockableViewPager;->a(IZ)V

    goto :goto_0

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    if-ne p1, v0, :cond_3

    .line 132
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->g()V

    goto :goto_0

    .line 133
    :cond_3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "View on click not implemented: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 11
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 12
    const v0, 0x7f040027

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->setContentView(I)V

    .line 13
    const v0, 0x7f0e00d5

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->w:Landroid/widget/TextView;

    .line 14
    const v0, 0x7f0e0100

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->x:Landroid/widget/TextView;

    .line 15
    const v0, 0x7f0e0101

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->y:Landroid/widget/QuickContactBadge;

    .line 16
    const v0, 0x7f0e0105

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->g:Landroid/widget/ImageView;

    .line 17
    const v0, 0x7f0e0106

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->h:Landroid/widget/ImageView;

    .line 18
    const v0, 0x7f0e0107

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->i:Landroid/widget/ImageView;

    .line 19
    const v0, 0x7f0e00ff

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->z:Landroid/widget/RelativeLayout;

    .line 20
    const v0, 0x7f0e0102

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/LockableViewPager;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    .line 21
    const v0, 0x7f0e00fd

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->j:Landroid/widget/FrameLayout;

    .line 22
    const v0, 0x7f0e00fe

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    .line 23
    const v0, 0x7f0e010a

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/DialerToolbar;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->A:Lcom/android/dialer/widget/DialerToolbar;

    .line 24
    const v0, 0x7f0e0108

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    .line 25
    const v0, 0x7f0e0109

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->B:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0e0103

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->C:Landroid/widget/ProgressBar;

    .line 27
    new-instance v0, Lsk;

    invoke-direct {v0}, Lsk;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->l:Lsk;

    .line 28
    new-instance v0, Layb;

    .line 29
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->c()Lja;

    move-result-object v1

    .line 30
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Layb;-><init>(Lja;I)V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    .line 31
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->E:Layb;

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/LockableViewPager;->a(Lqv;)V

    .line 32
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v0, p0}, Lcom/android/dialer/widget/LockableViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 33
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Landroid/content/Intent;)V

    .line 38
    if-eqz p1, :cond_0

    .line 39
    const-string v0, "entrance_animation_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->m:Z

    .line 40
    const-string v0, "send_and_call_ready_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->G:Z

    .line 41
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    const-string v1, "view_pager_state_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/LockableViewPager;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 42
    const-string v0, "current_index_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    .line 43
    const-string v0, "session_id_key"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    .line 44
    iget v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->b(I)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->k:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    new-instance v2, Laxs;

    invoke-direct {v2, p0}, Laxs;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    invoke-static {v0, v1, v2}, Lbib;->a(Landroid/view/View;ZLjava/lang/Runnable;)V

    .line 46
    iget v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    invoke-direct {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->e(I)V

    .line 48
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "copyAndResizeImageToSend"

    new-instance v3, Layf;

    .line 51
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Layf;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Laxt;

    invoke-direct {v1, p0}, Laxt;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    .line 53
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Laxu;

    invoke-direct {v1, p0}, Laxu;-><init>(Lcom/android/dialer/callcomposer/CallComposerActivity;)V

    .line 54
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->F:Lbdy;

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Luh;->onDestroy()V

    .line 89
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 90
    invoke-interface {v0, p0}, Lbjf;->b(Lbjj;)V

    .line 91
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0, p1}, Luh;->onNewIntent(Landroid/content/Intent;)V

    .line 122
    invoke-direct {p0, p1}, Lcom/android/dialer/callcomposer/CallComposerActivity;->a(Landroid/content/Intent;)V

    .line 123
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    .line 57
    invoke-super {p0}, Luh;->onResume()V

    .line 59
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 60
    invoke-interface {v0, p0}, Lbjf;->a(Lbjj;)V

    .line 61
    iget-boolean v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->t:Z

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->r:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 63
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->setResult(I)V

    .line 64
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->finish()V

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    .line 83
    const-string v0, "CallComposerActivity.onResume"

    const-string v1, "failed to create call composer session"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->k()V

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/android/dialer/callcomposer/CallComposerActivity;->l()V

    .line 86
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 66
    const-string v0, "CallComposerActivity.onResume"

    const-string v1, "creating new session"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 70
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 71
    invoke-interface {v0, v1}, Lbjf;->c(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    goto :goto_0

    .line 73
    :cond_2
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lbjf;->b(J)Lbjl;

    move-result-object v0

    if-nez v0, :cond_3

    .line 75
    const-string v0, "CallComposerActivity.onResume"

    const-string v1, "session closed while activity paused, creating new"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    invoke-static {p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->u:Lbhj;

    .line 79
    iget-object v1, v1, Lbhj;->f:Ljava/lang/String;

    .line 80
    invoke-interface {v0, v1}, Lbjf;->c(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    goto :goto_0

    .line 81
    :cond_3
    const-string v0, "CallComposerActivity.onResume"

    const-string v1, "session still open, using old"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 267
    invoke-super {p0, p1}, Luh;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 268
    const-string v0, "view_pager_state_key"

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->D:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v1}, Lcom/android/dialer/widget/LockableViewPager;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 269
    const-string v0, "entrance_animation_key"

    iget-boolean v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 270
    const-string v0, "send_and_call_ready_key"

    iget-boolean v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->G:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 271
    const-string v0, "current_index_key"

    iget v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->I:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 272
    const-string v0, "session_id_key"

    iget-object v1, p0, Lcom/android/dialer/callcomposer/CallComposerActivity;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 273
    return-void
.end method
