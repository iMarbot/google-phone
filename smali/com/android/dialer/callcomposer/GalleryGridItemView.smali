.class public Lcom/android/dialer/callcomposer/GalleryGridItemView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field public final a:Layl;

.field public b:Landroid/widget/ImageView;

.field public c:Ljava/lang/String;

.field public d:Z

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance v0, Layl;

    invoke-direct {v0}, Layl;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->a:Layl;

    .line 3
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->d:Z

    .line 24
    iget-object v1, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->f:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 25
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 5
    const v0, 0x7f0e00a4

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    .line 6
    const v0, 0x7f0e00b8

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->e:Landroid/view/View;

    .line 7
    const v0, 0x7f0e01e4

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->f:Landroid/view/View;

    .line 8
    iget-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClipToOutline(Z)V

    .line 9
    iget-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 10
    iget-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 11
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0, p1, p1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 13
    return-void
.end method

.method public setSelected(Z)V
    .locals 2

    .prologue
    .line 14
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d014f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 17
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->setPadding(IIII)V

    .line 22
    :goto_0
    return-void

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/callcomposer/GalleryGridItemView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d014e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 21
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/android/dialer/callcomposer/GalleryGridItemView;->setPadding(IIII)V

    goto :goto_0
.end method
