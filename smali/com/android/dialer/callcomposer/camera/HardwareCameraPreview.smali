.class public Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;
.super Landroid/view/TextureView;
.source "PG"

# interfaces
.implements Lazf;


# instance fields
.field public a:Laze;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance v0, Laze;

    invoke-direct {v0, p0}, Laze;-><init>(Lazf;)V

    iput-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 3
    new-instance v0, Lazg;

    invoke-direct {v0, p0}, Lazg;-><init>(Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;)V

    invoke-virtual {p0, v0}, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 4
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 25
    return-object p0
.end method

.method public final a(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 28
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    invoke-virtual {v0}, Laze;->a()V

    .line 6
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    .line 14
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 15
    invoke-virtual {v0}, Laze;->b()V

    .line 16
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    .line 11
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    invoke-virtual {v0}, Layq;->c()V

    .line 12
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    invoke-virtual {v0, p1}, Laze;->a(I)I

    move-result v0

    .line 22
    iget-object v1, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    invoke-virtual {v1, v0, p2}, Laze;->a(II)I

    move-result v1

    .line 23
    invoke-super {p0, v0, v1}, Landroid/view/TextureView;->onMeasure(II)V

    .line 24
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/view/TextureView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 18
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    .line 19
    invoke-virtual {v0}, Laze;->b()V

    .line 20
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 7
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 8
    iget-object v0, p0, Lcom/android/dialer/callcomposer/camera/HardwareCameraPreview;->a:Laze;

    invoke-virtual {v0, p2}, Laze;->b(I)V

    .line 9
    return-void
.end method
