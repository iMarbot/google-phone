.class public Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# static fields
.field private static a:Landroid/content/UriMatcher;


# instance fields
.field private b:Lbbv;

.field private c:Lbby;

.field private d:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 139
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 140
    sput-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lbcc;->a:Ljava/lang/String;

    const-string v2, "AnnotatedCallLog"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 141
    sget-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lbcc;->a:Ljava/lang/String;

    const-string v2, "AnnotatedCallLog/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 142
    sget-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    sget-object v1, Lbcc;->a:Ljava/lang/String;

    const-string v2, "CoalescedAnnotatedCallLog"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    const/16 v0, 0x18

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 137
    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbce;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 138
    return-void
.end method

.method private final a()Z
    .locals 1

    .prologue
    .line 3
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 106
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Landroid/content/ContentProviderResult;

    .line 107
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 134
    :goto_0
    return-object v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    invoke-virtual {v0}, Lbbv;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 111
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move v2, v3

    .line 112
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 113
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 114
    sget-object v5, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    .line 115
    packed-switch v5, :pswitch_data_0

    .line 118
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown uri: "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 132
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 117
    :pswitch_0
    :try_start_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "coalesced call log does not support applyBatch"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :pswitch_1
    invoke-virtual {v0, p0, v1, v2}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    move-result-object v5

    .line 120
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->isInsert()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, v5, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 122
    new-instance v0, Landroid/content/OperationApplicationException;

    const-string v1, "error inserting row"

    invoke-direct {v0, v1}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_1
    iget-object v0, v5, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 124
    const-string v0, "AnnotatedCallLogContentProvider.applyBatch"

    const-string v6, "update or delete failed, possibly because row got cleaned up"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    :cond_2
    aput-object v5, v1, v2

    .line 126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 127
    :cond_3
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->d:Ljava/lang/ThreadLocal;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 129
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 133
    sget-object v0, Lbcd;->a:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(Landroid/net/Uri;)V

    move-object v0, v1

    .line 134
    goto/16 :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    invoke-virtual {v0}, Lbbv;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 69
    sget-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 70
    packed-switch v0, :pswitch_data_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown uri: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :pswitch_0
    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "Do not specify selection when deleting by ID"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 73
    if-nez p3, :cond_2

    move v0, v1

    :goto_1
    const-string v4, "Do not specify selection args when deleting by ID"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 74
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 75
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    const-string v6, "error parsing id from uri %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v6, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 76
    invoke-static {v4, v5}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(J)Ljava/lang/String;

    move-result-object p2

    .line 80
    :pswitch_1
    const-string v0, "AnnotatedCallLog"

    invoke-virtual {v3, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 81
    if-lez v0, :cond_4

    .line 82
    invoke-direct {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    invoke-direct {p0, p1}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(Landroid/net/Uri;)V

    .line 85
    :cond_0
    :goto_3
    return v0

    :cond_1
    move v0, v2

    .line 72
    goto :goto_0

    :cond_2
    move v0, v2

    .line 73
    goto :goto_1

    :cond_3
    move v0, v2

    .line 75
    goto :goto_2

    .line 78
    :pswitch_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "coalesced call log does not support deleting"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_4
    const-string v1, "AnnotatedCallLogContentProvider.delete"

    const-string v3, "no rows deleted"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "vnd.android.cursor.item/annotated_call_log"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 42
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    invoke-virtual {v0}, Lbbv;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 43
    sget-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 44
    packed-switch v0, :pswitch_data_0

    .line 57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown uri: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 41
    goto :goto_0

    .line 45
    :pswitch_0
    const-string v0, "_id"

    .line 46
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    const-string v5, "You must specify an _ID when inserting"

    new-array v6, v2, [Ljava/lang/Object;

    .line 47
    invoke-static {v0, v5, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :cond_1
    :goto_2
    const-string v0, "AnnotatedCallLog"

    invoke-virtual {v4, v0, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 59
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 60
    const-string v0, "AnnotatedCallLogContentProvider.insert"

    const-string v4, "error inserting row with id: %d"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "_id"

    .line 61
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v1, v2

    .line 62
    invoke-static {v0, v4, v1}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 67
    :cond_2
    :goto_3
    return-object v0

    :cond_3
    move v0, v2

    .line 46
    goto :goto_1

    .line 49
    :pswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 50
    const-string v0, "_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    .line 51
    if-eqz v6, :cond_4

    .line 52
    invoke-virtual {v6, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v1

    :goto_4
    const-string v7, "_ID from values %d does not match ID from URI: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v6, v8, v2

    aput-object p1, v8, v1

    .line 53
    invoke-static {v0, v7, v8}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 54
    if-nez v6, :cond_1

    .line 55
    const-string v0, "_id"

    invoke-virtual {p2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_2

    :cond_5
    move v0, v2

    .line 52
    goto :goto_4

    .line 56
    :pswitch_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "coalesced call log does not support inserting"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_6
    sget-object v0, Lbcd;->a:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 65
    invoke-direct {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 66
    invoke-direct {p0, v0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(Landroid/net/Uri;)V

    goto :goto_3

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 4
    new-instance v0, Lbbv;

    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x3e7

    invoke-direct {v0, v1, v2}, Lbbv;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbw;->a(Landroid/content/Context;)Lbbw;

    move-result-object v0

    invoke-virtual {v0}, Lbbw;->a()Lbby;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->c:Lbby;

    .line 6
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 7
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    invoke-virtual {v0}, Lbbv;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 8
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 9
    const-string v2, "AnnotatedCallLog"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 10
    sget-object v2, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 11
    packed-switch v2, :pswitch_data_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown uri: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 12
    :pswitch_0
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    const/16 v4, 0x18

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "_id="

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :pswitch_1
    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 14
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lbcd;->a:Landroid/net/Uri;

    .line 18
    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 36
    :cond_0
    :goto_0
    return-object v0

    .line 19
    :cond_1
    const-string v1, "AnnotatedCallLogContentProvider.query"

    const-string v2, "cursor was null"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 21
    :pswitch_2
    sget-object v2, Lbce;->b:[Ljava/lang/String;

    if-ne p2, v2, :cond_2

    move v2, v4

    :goto_1
    const-string v3, "only ALL_COLUMNS projection supported for coalesced call log"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 22
    if-nez p3, :cond_3

    move v2, v4

    :goto_2
    const-string v3, "selection not supported for coalesced call log"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 23
    if-nez p4, :cond_4

    move v2, v4

    :goto_3
    const-string v3, "selection args not supported for coalesced call log"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 24
    if-nez p5, :cond_5

    move v2, v4

    :goto_4
    const-string v3, "sort order not supported for coalesced call log"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 25
    const-string v2, "%s != ?"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v6, "call_type"

    aput-object v6, v3, v8

    .line 26
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v2, 0x4

    .line 27
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    const-string v7, "timestamp DESC"

    move-object v2, v5

    move-object v6, v5

    .line 28
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->c:Lbby;

    invoke-virtual {v0, v2}, Lbby;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lbce;->a:Landroid/net/Uri;

    .line 33
    invoke-interface {v0, v1, v3}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 35
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    move v2, v8

    .line 21
    goto :goto_1

    :cond_3
    move v2, v8

    .line 22
    goto :goto_2

    :cond_4
    move v2, v8

    .line 23
    goto :goto_3

    :cond_5
    move v2, v8

    .line 24
    goto :goto_4

    .line 37
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    :catchall_0
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_6

    if-eqz v5, :cond_7

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_6
    :goto_6
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v5, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_5

    .line 11
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 87
    iget-object v0, p0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->b:Lbbv;

    invoke-virtual {v0}, Lbbv;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 88
    sget-object v0, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 89
    packed-switch v0, :pswitch_data_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown uri: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 86
    goto :goto_0

    .line 91
    :pswitch_0
    const-string v0, "_id"

    .line 92
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v4, "Do not specify _ID when updating by ID"

    new-array v5, v2, [Ljava/lang/Object;

    .line 93
    invoke-static {v0, v4, v5}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 94
    if-nez p3, :cond_3

    move v0, v1

    :goto_2
    const-string v4, "Do not specify selection when updating by ID"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 95
    if-nez p4, :cond_4

    :goto_3
    const-string v0, "Do not specify selection args when updating by ID"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v4}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 96
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(J)Ljava/lang/String;

    move-result-object p3

    .line 100
    :pswitch_1
    const-string v0, "AnnotatedCallLog"

    invoke-virtual {v3, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 101
    if-lez v0, :cond_5

    .line 102
    invoke-direct {p0}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    invoke-direct {p0, p1}, Lcom/android/dialer/calllog/database/AnnotatedCallLogContentProvider;->a(Landroid/net/Uri;)V

    .line 105
    :cond_1
    :goto_4
    return v0

    :cond_2
    move v0, v2

    .line 92
    goto :goto_1

    :cond_3
    move v0, v2

    .line 94
    goto :goto_2

    :cond_4
    move v1, v2

    .line 95
    goto :goto_3

    .line 98
    :pswitch_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "coalesced call log does not support updating"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_5
    const-string v1, "AnnotatedCallLogContentProvider.update"

    const-string v3, "no rows updated"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
