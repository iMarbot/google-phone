.class public Lcom/android/dialer/precall/impl/PreCallActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private a:Lbnp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1b
    .end annotation

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 3
    new-instance v0, Lbnp;

    invoke-direct {v0, p0}, Lbnp;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    .line 4
    iget-object v1, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    invoke-virtual {p0}, Lcom/android/dialer/precall/impl/PreCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 5
    const-string v2, "PreCallCoordinatorImpl.onCreate"

    invoke-static {v2}, Lapw;->b(Ljava/lang/String;)V

    .line 6
    if-eqz p1, :cond_1

    .line 7
    const-string v0, "current_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lbnp;->d:I

    .line 8
    const-string v0, "extra_call_intent_builder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbbh;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbh;

    iput-object v0, v1, Lbnp;->b:Lbbh;

    .line 10
    :goto_0
    const-class v0, Landroid/app/KeyguardManager;

    invoke-virtual {p0, v0}, Lcom/android/dialer/precall/impl/PreCallActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1b

    if-ge v0, v1, :cond_2

    .line 12
    invoke-virtual {p0}, Lcom/android/dialer/precall/impl/PreCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 14
    :cond_0
    :goto_1
    return-void

    .line 9
    :cond_1
    const-string v2, "extra_call_intent_builder"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbbh;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbh;

    iput-object v0, v1, Lbnp;->b:Lbbh;

    goto :goto_0

    .line 13
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/dialer/precall/impl/PreCallActivity;->setShowWhenLocked(Z)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 26
    iget-object v0, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    .line 27
    iget-object v1, v0, Lbnp;->e:Lbnb;

    if-eqz v1, :cond_0

    .line 28
    iget-object v1, v0, Lbnp;->e:Lbnb;

    invoke-interface {v1}, Lbnb;->a()V

    .line 29
    :cond_0
    iput-object v2, v0, Lbnp;->e:Lbnb;

    .line 30
    iput-object v2, v0, Lbnp;->f:Lbnf;

    .line 31
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 16
    iget-object v1, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    .line 17
    const-string v0, "current_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lbnp;->d:I

    .line 18
    const-string v0, "extra_call_intent_builder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbbh;

    iput-object v0, v1, Lbnp;->b:Lbbh;

    .line 19
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 21
    iget-object v0, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    .line 22
    iget-object v1, v0, Lbnp;->a:Landroid/app/Activity;

    invoke-static {v1}, Lbnc;->a(Landroid/content/Context;)Lbnc;

    move-result-object v1

    invoke-virtual {v1}, Lbnc;->a()Lbna;

    move-result-object v1

    invoke-virtual {v1}, Lbna;->a()Lgue;

    move-result-object v1

    iput-object v1, v0, Lbnp;->c:Lgue;

    .line 23
    invoke-virtual {v0}, Lbnp;->e()V

    .line 24
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lcom/android/dialer/precall/impl/PreCallActivity;->a:Lbnp;

    .line 34
    const-string v1, "current_action"

    iget v2, v0, Lbnp;->d:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    const-string v1, "extra_call_intent_builder"

    iget-object v0, v0, Lbnp;->b:Lbbh;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 36
    return-void
.end method
