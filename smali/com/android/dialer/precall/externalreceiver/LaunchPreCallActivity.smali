.class public Lcom/android/dialer/precall/externalreceiver/LaunchPreCallActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dQ:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 4
    invoke-virtual {p0}, Lcom/android/dialer/precall/externalreceiver/LaunchPreCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v1

    .line 5
    invoke-virtual {p0}, Lcom/android/dialer/precall/externalreceiver/LaunchPreCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 6
    new-instance v3, Lbbh;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    sget-object v4, Lbbf$a;->m:Lbbf$a;

    invoke-direct {v3, v0, v4}, Lbbh;-><init>(Landroid/net/Uri;Lbbf$a;)V

    .line 7
    const-string v0, "phone_account_handle"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 8
    if-nez v0, :cond_0

    .line 9
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 12
    :cond_0
    iput-object v0, v3, Lbbh;->b:Landroid/telecom/PhoneAccountHandle;

    .line 14
    const-string v0, "is_video_call"

    .line 15
    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 16
    iput-boolean v0, v3, Lbbh;->c:Z

    .line 18
    const-string v0, "call_subject"

    .line 19
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 20
    iput-object v0, v3, Lbbh;->d:Ljava/lang/String;

    .line 22
    const-string v0, "allow_assisted_dial"

    const-string v4, "assisted_dialing_default_precall_state"

    .line 23
    invoke-interface {v1, v4, v5}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 24
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 26
    iput-boolean v0, v3, Lbbh;->e:Z

    .line 27
    invoke-static {p0, v3}, Lbib;->b(Landroid/content/Context;Lbbh;)V

    .line 28
    invoke-virtual {p0}, Lcom/android/dialer/precall/externalreceiver/LaunchPreCallActivity;->finish()V

    .line 29
    return-void
.end method
