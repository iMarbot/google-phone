.class public final Lcom/android/dialer/about/LicenseMenuActivity;
.super Luh;
.source "PG"

# interfaces
.implements Lky;


# instance fields
.field private f:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lly;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Laml;

    invoke-direct {v0, p0}, Laml;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final synthetic a(Lly;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 27
    check-cast p2, Ljava/util/List;

    .line 28
    iget-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 29
    iget-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 30
    iget-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 31
    return-void
.end method

.method public final k_()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 25
    iget-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 26
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const v0, 0x7f040083

    invoke-virtual {p0, v0}, Lcom/android/dialer/about/LicenseMenuActivity;->setContentView(I)V

    .line 5
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 6
    if-eqz v0, :cond_0

    .line 8
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 9
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv;->b(Z)V

    .line 10
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f040082

    const v2, 0x7f0e01f4

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    .line 11
    invoke-virtual {p0}, Lcom/android/dialer/about/LicenseMenuActivity;->d()Lkx;

    move-result-object v0

    const v1, 0xd431

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lkx;->a(ILandroid/os/Bundle;Lky;)Lly;

    .line 12
    const v0, 0x7f0e01f5

    invoke-virtual {p0, v0}, Lcom/android/dialer/about/LicenseMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 13
    iget-object v1, p0, Lcom/android/dialer/about/LicenseMenuActivity;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 14
    new-instance v1, Lamm;

    invoke-direct {v1, p0}, Lamm;-><init>(Lcom/android/dialer/about/LicenseMenuActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 15
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Luh;->onDestroy()V

    .line 21
    invoke-virtual {p0}, Lcom/android/dialer/about/LicenseMenuActivity;->d()Lkx;

    move-result-object v0

    const v1, 0xd431

    invoke-virtual {v0, v1}, Lkx;->a(I)V

    .line 22
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 16
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 17
    invoke-virtual {p0}, Lcom/android/dialer/about/LicenseMenuActivity;->finish()V

    .line 18
    const/4 v0, 0x1

    .line 19
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Luh;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
