.class public final Lcom/android/dialer/location/CountryDetector;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/dialer/location/CountryDetector$LocationChangedReceiver;
    }
.end annotation


# static fields
.field private static e:Lcom/android/dialer/location/CountryDetector;


# instance fields
.field public final a:Landroid/telephony/TelephonyManager;

.field public final b:Lbkl;

.field public final c:Landroid/location/Geocoder;

.field public final d:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Landroid/location/LocationManager;Lbkl;Landroid/location/Geocoder;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/android/dialer/location/CountryDetector;->a:Landroid/telephony/TelephonyManager;

    .line 3
    iput-object p4, p0, Lcom/android/dialer/location/CountryDetector;->b:Lbkl;

    .line 4
    iput-object p1, p0, Lcom/android/dialer/location/CountryDetector;->d:Landroid/content/Context;

    .line 5
    iput-object p5, p0, Lcom/android/dialer/location/CountryDetector;->c:Landroid/location/Geocoder;

    .line 6
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    invoke-static {p1}, Lbsw;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 9
    const-string v0, "CountryDetector.registerForLocationUpdates"

    const-string v1, "no location permissions, not registering for location updates"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    :cond_0
    :goto_0
    return-void

    .line 11
    :cond_1
    const-string v0, "CountryDetector.registerForLocationUpdates"

    const-string v1, "registering for location updates"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/location/CountryDetector$LocationChangedReceiver;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 13
    const/high16 v1, 0x8000000

    .line 14
    invoke-static {p1, v3, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 15
    const-string v1, "passive"

    const-wide/32 v2, 0x2932e00

    const v4, 0x459c4000    # 5000.0f

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/android/dialer/location/CountryDetector;
    .locals 7

    .prologue
    .line 17
    const-class v6, Lcom/android/dialer/location/CountryDetector;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lcom/android/dialer/location/CountryDetector;->e:Lcom/android/dialer/location/CountryDetector;

    if-nez v0, :cond_0

    .line 18
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 19
    new-instance v0, Lcom/android/dialer/location/CountryDetector;

    const-string v2, "phone"

    .line 20
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    const-string v3, "location"

    .line 21
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    sget-object v4, Lbkh;->a:Lbkl;

    new-instance v5, Landroid/location/Geocoder;

    invoke-direct {v5, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v5}, Lcom/android/dialer/location/CountryDetector;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Landroid/location/LocationManager;Lbkl;Landroid/location/Geocoder;)V

    sput-object v0, Lcom/android/dialer/location/CountryDetector;->e:Lcom/android/dialer/location/CountryDetector;

    .line 22
    :cond_0
    sget-object v0, Lcom/android/dialer/location/CountryDetector;->e:Lcom/android/dialer/location/CountryDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method
