.class public Lcom/android/dialer/location/CountryDetector$LocationChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dialer/location/CountryDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocationChangedReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2
    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    :goto_0
    return-void

    .line 5
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 6
    invoke-static {p1}, Lcom/android/dialer/location/CountryDetector;->a(Landroid/content/Context;)Lcom/android/dialer/location/CountryDetector;

    move-result-object v1

    .line 7
    iget-object v1, v1, Lcom/android/dialer/location/CountryDetector;->c:Landroid/location/Geocoder;

    .line 10
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v2

    .line 11
    invoke-virtual {v2}, Lbed;->a()Lbef;

    move-result-object v2

    new-instance v3, Lbkk;

    invoke-direct {v3, v1}, Lbkk;-><init>(Landroid/location/Geocoder;)V

    .line 12
    invoke-virtual {v2, v3}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Lbki;

    invoke-direct {v2, p1}, Lbki;-><init>(Landroid/content/Context;)V

    .line 13
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v1

    sget-object v2, Lbkj;->a:Lbea;

    .line 14
    invoke-interface {v1, v2}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v1

    .line 15
    invoke-interface {v1}, Lbdz;->a()Lbdy;

    move-result-object v1

    .line 16
    invoke-interface {v1, v0}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
