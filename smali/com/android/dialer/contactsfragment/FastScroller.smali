.class public Lcom/android/dialer/contactsfragment/FastScroller;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field public a:Lbga;

.field public b:Labq;

.field public c:Landroid/view/View;

.field public d:Z

.field private e:I

.field private f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->e:I

    .line 4
    return-void
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 42
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 43
    iget-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 44
    iget-object v1, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    .line 45
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    .line 46
    invoke-virtual {p0}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v3

    sub-int/2addr v3, v0

    div-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    sub-float v4, p1, v4

    float-to-int v4, v4

    invoke-static {v5, v3, v4}, Lcom/android/dialer/contactsfragment/FastScroller;->a(III)I

    move-result v3

    int-to-float v3, v3

    .line 47
    invoke-virtual {v2, v3}, Landroid/view/View;->setY(F)V

    .line 48
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    .line 49
    invoke-virtual {p0}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v3

    sub-int/2addr v3, v1

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    int-to-float v1, v1

    sub-float v1, p1, v1

    float-to-int v1, v1

    .line 50
    invoke-static {v5, v0, v1}, Lcom/android/dialer/contactsfragment/FastScroller;->a(III)I

    move-result v0

    int-to-float v0, v0

    .line 51
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setY(F)V

    .line 52
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 5
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 6
    const v0, 0x7f0e01ca

    invoke-virtual {p0, v0}, Lcom/android/dialer/contactsfragment/FastScroller;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    .line 7
    const v0, 0x7f0e01cb

    invoke-virtual {p0, v0}, Lcom/android/dialer/contactsfragment/FastScroller;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    .line 8
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 9
    iget-boolean v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->d:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/dialer/contactsfragment/FastScroller;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/android/dialer/contactsfragment/FastScroller;->e:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v0

    if-lez v2, :cond_0

    .line 10
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 40
    :goto_0
    return v0

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 40
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 12
    :pswitch_0
    iput-boolean v1, p0, Lcom/android/dialer/contactsfragment/FastScroller;->d:Z

    .line 13
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 14
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 15
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/dialer/contactsfragment/FastScroller;->a(F)V

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 17
    iget-object v3, p0, Lcom/android/dialer/contactsfragment/FastScroller;->a:Lbga;

    invoke-virtual {v3}, Lbga;->a()I

    move-result v3

    .line 19
    iget-object v4, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    cmpl-float v4, v4, v0

    if-nez v4, :cond_2

    .line 24
    :goto_1
    int-to-float v2, v3

    mul-float/2addr v0, v2

    .line 25
    add-int/lit8 v2, v3, -0x1

    float-to-int v0, v0

    invoke-static {v5, v2, v0}, Lcom/android/dialer/contactsfragment/FastScroller;->a(III)I

    move-result v0

    .line 26
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->b:Labq;

    .line 27
    iput v0, v2, Labq;->b:I

    .line 28
    iput v5, v2, Labq;->c:I

    .line 29
    iget-object v3, v2, Labq;->d:Labu;

    if-eqz v3, :cond_1

    .line 30
    iget-object v3, v2, Labq;->d:Labu;

    .line 31
    const/4 v4, -0x1

    iput v4, v3, Labu;->a:I

    .line 32
    :cond_1
    invoke-virtual {v2}, Labq;->i()V

    .line 33
    iget-object v2, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/dialer/contactsfragment/FastScroller;->a:Lbga;

    invoke-virtual {v3, v0}, Lbga;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    iget-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->a:Lbga;

    invoke-virtual {v0}, Lbga;->b()V

    move v0, v1

    .line 35
    goto :goto_0

    .line 21
    :cond_2
    iget-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    iget-object v4, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v0, v4

    invoke-virtual {p0}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_3

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    .line 23
    :cond_3
    invoke-virtual {p0}, Lcom/android/dialer/contactsfragment/FastScroller;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    goto :goto_1

    .line 36
    :pswitch_2
    iput-boolean v5, p0, Lcom/android/dialer/contactsfragment/FastScroller;->d:Z

    .line 37
    iget-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->f:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 38
    iget-object v0, p0, Lcom/android/dialer/contactsfragment/FastScroller;->c:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    move v0, v1

    .line 39
    goto/16 :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
