.class public Lcom/android/incallui/spam/SpamNotificationActivity;
.super Lit;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/spam/SpamNotificationActivity$a;,
        Lcom/android/incallui/spam/SpamNotificationActivity$b;
    }
.end annotation


# instance fields
.field public final f:Landroid/content/DialogInterface$OnDismissListener;

.field private g:Lawr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lit;-><init>()V

    .line 2
    new-instance v0, Lchu;

    invoke-direct {v0, p0}, Lchu;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity;)V

    iput-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity;->f:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 5
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 6
    const-string v1, "vnd.android.cursor.dir/raw_contact"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 7
    const-string v1, "phone"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8
    return-object v0
.end method

.method static a(Landroid/content/Context;Landroid/os/Bundle;Lbkq$a;)V
    .locals 6

    .prologue
    .line 10
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "call_id"

    .line 11
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "call_start_time_millis"

    const-wide/16 v4, 0x0

    .line 12
    invoke-virtual {p1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 13
    invoke-interface {v0, p2, v1, v2, v3}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 14
    return-void
.end method

.method private final a(Lbkq$a;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Landroid/content/Context;Landroid/os/Bundle;Lbkq$a;)V

    .line 111
    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    invoke-static {p0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final e()V
    .locals 2

    .prologue
    .line 106
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot start this activity with given action because dialogs are not enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    return-void
.end method

.method private final f()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "call_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lbkm$a;)V
    .locals 3

    .prologue
    .line 69
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-static {p1}, Lcom/android/incallui/spam/SpamNotificationActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lchv;

    invoke-direct {v1, p0, p1, p2}, Lchv;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity;Ljava/lang/String;Lbkm$a;)V

    iget-object v2, p0, Lcom/android/incallui/spam/SpamNotificationActivity;->f:Landroid/content/DialogInterface$OnDismissListener;

    .line 72
    invoke-static {v0, v1, v2}, Lawi;->a(Ljava/lang/String;Lawg;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/DialogFragment;

    move-result-object v0

    .line 73
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "NotSpamDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/spam/SpamNotificationActivity;->c(Ljava/lang/String;Lbkm$a;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZLbkm$a;)V
    .locals 6

    .prologue
    .line 85
    if-eqz p2, :cond_0

    .line 86
    sget-object v0, Lbkq$a;->r:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 87
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    .line 89
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 90
    const/4 v3, 0x1

    sget-object v4, Lbkz$a;->b:Lbkz$a;

    move-object v1, p1

    move-object v5, p3

    .line 91
    invoke-interface/range {v0 .. v5}, Lbsd;->a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V

    .line 92
    :cond_0
    sget-object v0, Lbkq$a;->n:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 93
    iget-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity;->g:Lawr;

    const/4 v1, 0x0

    .line 94
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-virtual {v0, v1, p1, v2}, Lawr;->a(Lawy;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->finish()V

    .line 97
    return-void
.end method

.method public final b(Ljava/lang/String;Lbkm$a;)V
    .locals 2

    .prologue
    .line 76
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    invoke-interface {v0}, Lbsd;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    new-instance v0, Lchw;

    invoke-direct {v0, p0, p1, p2}, Lchw;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity;Ljava/lang/String;Lbkm$a;)V

    .line 79
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 80
    invoke-static {p0, v1, v0}, Lapw;->a(Landroid/content/Context;Landroid/app/FragmentManager;Lawp;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    invoke-interface {v0}, Lawp;->a()V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Ljava/lang/String;ZLbkm$a;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Lbkm$a;)V
    .locals 6

    .prologue
    .line 98
    sget-object v0, Lbkq$a;->s:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 99
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    .line 101
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 102
    const/4 v3, 0x1

    sget-object v4, Lbkz$a;->b:Lbkz$a;

    move-object v1, p1

    move-object v5, p2

    .line 103
    invoke-interface/range {v0 .. v5}, Lbsd;->b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V

    .line 104
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->finish()V

    .line 105
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 15
    const-string v0, "SpamNotifications"

    const-string v1, "onCreate"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p0, v3}, Lcom/android/incallui/spam/SpamNotificationActivity;->setFinishOnTouchOutside(Z)V

    .line 18
    new-instance v0, Lawr;

    invoke-direct {v0, p0}, Lawr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity;->g:Lawr;

    .line 20
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notification_tag"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 21
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notification_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 22
    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 23
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity;->g:Lawr;

    .line 65
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->finish()V

    .line 67
    :cond_0
    invoke-super {p0}, Lit;->onPause()V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 24
    const-string v1, "SpamNotifications"

    const-string v2, "onResume"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-super {p0}, Lit;->onResume()V

    .line 26
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 27
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "phone_number"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "is_spam"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 30
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "contact_lookup_result_type"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 31
    invoke-static {v4}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v4

    .line 32
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const/4 v1, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_1
    return-void

    .line 32
    :sswitch_0
    const-string v6, "com.android.incallui.spam.ACTION_ADD_TO_CONTACTS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_SPAM"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v0, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_NOT_SPAM"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v0, "com.android.incallui.spam.ACTION_SHOW_DIALOG"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 33
    :pswitch_0
    sget-object v0, Lbkq$a;->q:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 34
    invoke-static {v2}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 35
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->finish()V

    goto :goto_1

    .line 37
    :pswitch_1
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->e()V

    .line 38
    invoke-virtual {p0, v2, v4}, Lcom/android/incallui/spam/SpamNotificationActivity;->b(Ljava/lang/String;Lbkm$a;)V

    goto :goto_1

    .line 40
    :pswitch_2
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->e()V

    .line 41
    invoke-virtual {p0, v2, v4}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Ljava/lang/String;Lbkm$a;)V

    goto :goto_1

    .line 43
    :pswitch_3
    if-eqz v3, :cond_1

    .line 45
    sget-object v0, Lbkq$a;->o:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 46
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/android/incallui/spam/SpamNotificationActivity$b;

    invoke-direct {v1}, Lcom/android/incallui/spam/SpamNotificationActivity$b;-><init>()V

    .line 49
    invoke-virtual {v1, v0}, Lcom/android/incallui/spam/SpamNotificationActivity$b;->f(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->c()Lja;

    move-result-object v0

    const-string v2, "FirstTimeSpamDialog"

    invoke-virtual {v1, v0, v2}, Lio;->a(Lja;Ljava/lang/String;)V

    goto :goto_1

    .line 55
    :cond_1
    sget-object v0, Lbkq$a;->p:Lbkq$a;

    invoke-direct {p0, v0}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Lbkq$a;)V

    .line 56
    invoke-direct {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->f()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/android/incallui/spam/SpamNotificationActivity$a;

    invoke-direct {v1}, Lcom/android/incallui/spam/SpamNotificationActivity$a;-><init>()V

    .line 59
    invoke-virtual {v1, v0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->f(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity;->c()Lja;

    move-result-object v0

    const-string v2, "FirstTimeNonSpamDialog"

    invoke-virtual {v1, v0, v2}, Lio;->a(Lja;Ljava/lang/String;)V

    goto :goto_1

    .line 32
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4d038271 -> :sswitch_2
        -0x22e129a5 -> :sswitch_3
        -0x1c4a177d -> :sswitch_1
        0x549923ea -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
