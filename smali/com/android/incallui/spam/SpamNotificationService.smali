.class public Lcom/android/incallui/spam/SpamNotificationService;
.super Landroid/app/Service;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private final a(Landroid/content/Intent;Lbkq$a;)V
    .locals 6

    .prologue
    .line 24
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "service_call_id"

    .line 25
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "service_call_start_time_millis"

    const-wide/16 v4, 0x0

    .line 26
    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 27
    invoke-interface {v0, p2, v1, v2, v3}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 28
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 3
    if-nez p1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationService;->stopSelf()V

    .line 23
    :goto_0
    return v8

    .line 6
    :cond_0
    const-string v1, "service_phone_number"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 7
    const-string v2, "service_notification_tag"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 8
    const-string v2, "service_notification_id"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 9
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 10
    const-string v5, "service_contact_lookup_result_type"

    .line 11
    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v5

    .line 12
    invoke-static {p0, v4, v6}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 13
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const/4 v4, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_1
    move v0, v4

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 22
    :goto_2
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationService;->stopSelf()V

    goto :goto_0

    .line 13
    :sswitch_0
    const-string v7, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_SPAM"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    :sswitch_1
    const-string v0, "com.android.incallui.spam.ACTION_MARK_NUMBER_AS_NOT_SPAM"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    goto :goto_1

    .line 14
    :pswitch_0
    sget-object v0, Lbkq$a;->v:Lbkq$a;

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/spam/SpamNotificationService;->a(Landroid/content/Intent;Lbkq$a;)V

    .line 15
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    sget-object v4, Lbkz$a;->b:Lbkz$a;

    .line 16
    invoke-interface/range {v0 .. v5}, Lbsd;->a(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V

    .line 17
    new-instance v0, Lawr;

    invoke-direct {v0, p0}, Lawr;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lawr;->a(Lawy;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 19
    :pswitch_1
    sget-object v0, Lbkq$a;->w:Lbkq$a;

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/spam/SpamNotificationService;->a(Landroid/content/Intent;Lbkq$a;)V

    .line 20
    invoke-static {p0}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v0

    sget-object v4, Lbkz$a;->b:Lbkz$a;

    .line 21
    invoke-interface/range {v0 .. v5}, Lbsd;->b(Ljava/lang/String;Ljava/lang/String;ILbkz$a;Lbkm$a;)V

    goto :goto_2

    .line 13
    :sswitch_data_0
    .sparse-switch
        -0x4d038271 -> :sswitch_1
        -0x1c4a177d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
