.class public final Lcom/android/incallui/spam/SpamNotificationActivity$a;
.super Lio;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/spam/SpamNotificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public W:Z

.field private X:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lio;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 19
    invoke-super {p0, p1}, Lio;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 21
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->h()Lit;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/spam/SpamNotificationActivity;

    .line 23
    iget-object v1, p0, Lip;->h:Landroid/os/Bundle;

    .line 24
    const-string v2, "phone_number"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27
    iget-object v2, p0, Lip;->h:Landroid/os/Bundle;

    .line 28
    const-string v3, "contact_lookup_result_type"

    invoke-virtual {v2, v3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 29
    invoke-static {v2}, Lbkm$a;->a(I)Lbkm$a;

    move-result-object v2

    .line 30
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->h()Lit;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f11021b

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 32
    invoke-static {v1}, Lcom/android/incallui/spam/SpamNotificationActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 33
    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 34
    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f1102dd

    .line 35
    invoke-virtual {p0, v4}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->b_(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f1102d5

    .line 36
    invoke-virtual {p0, v4}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->b_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcia;

    invoke-direct {v5, p0}, Lcia;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity$a;)V

    .line 37
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f1102d9

    .line 38
    invoke-virtual {p0, v4}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->b_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lchz;

    invoke-direct {v5, p0, v1}, Lchz;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity$a;Ljava/lang/String;)V

    .line 39
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f1102da

    .line 40
    invoke-virtual {p0, v4}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->b_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lchy;

    invoke-direct {v5, p0, v0, v1, v2}, Lchy;-><init>(Lcom/android/incallui/spam/SpamNotificationActivity$a;Lcom/android/incallui/spam/SpamNotificationActivity;Ljava/lang/String;Lbkm$a;)V

    .line 41
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 43
    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-super {p0, p1}, Lio;->a(Landroid/content/Context;)V

    .line 17
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity$a;->X:Landroid/content/Context;

    .line 18
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 6
    invoke-super {p0, p1}, Lio;->onDismiss(Landroid/content/DialogInterface;)V

    .line 7
    iget-object v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity$a;->X:Landroid/content/Context;

    .line 9
    iget-object v1, p0, Lip;->h:Landroid/os/Bundle;

    .line 10
    sget-object v2, Lbkq$a;->u:Lbkq$a;

    .line 12
    invoke-static {v0, v1, v2}, Lcom/android/incallui/spam/SpamNotificationActivity;->a(Landroid/content/Context;Landroid/os/Bundle;Lbkq$a;)V

    .line 13
    iget-boolean v0, p0, Lcom/android/incallui/spam/SpamNotificationActivity$a;->W:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->h()Lit;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Lit;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 14
    invoke-virtual {p0}, Lcom/android/incallui/spam/SpamNotificationActivity$a;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Lit;->finish()V

    .line 15
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 2
    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio;->a(Z)V

    .line 4
    invoke-super {p0}, Lio;->s()V

    .line 5
    return-void
.end method
