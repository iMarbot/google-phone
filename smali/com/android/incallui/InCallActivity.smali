.class public Lcom/android/incallui/InCallActivity;
.super Lbxg;
.source "PG"

# interfaces
.implements Lcbv;
.implements Lccb;
.implements Lcgl;
.implements Lcgo;
.implements Lcjm;


# instance fields
.field public final f:Lbvy;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field private k:[I

.field private l:Landroid/graphics/drawable/GradientDrawable;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lbxg;-><init>()V

    .line 2
    new-instance v0, Lbvy;

    invoke-direct {v0, p0}, Lbvy;-><init>(Lcom/android/incallui/InCallActivity;)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 3
    return-void
.end method

.method public static a(Landroid/content/Context;ZZZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 5
    const/high16 v1, 0x10040000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 6
    const-class v1, Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 7
    invoke-static {v0, p1, p2, p3}, Lbvy;->a(Landroid/content/Intent;ZZZ)V

    .line 8
    return-object v0
.end method

.method private final c(Ljy;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 643
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->g:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 649
    :goto_0
    return v0

    .line 645
    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->l()Lcbt;

    move-result-object v0

    .line 646
    if-eqz v0, :cond_2

    .line 647
    if-nez v0, :cond_1

    const/4 v0, 0x0

    throw v0

    :cond_1
    check-cast v0, Lip;

    invoke-virtual {p1, v0}, Ljy;->a(Lip;)Ljy;

    .line 648
    :cond_2
    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->g:Z

    .line 649
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final k()V
    .locals 12

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->j:Z

    if-nez v0, :cond_0

    .line 496
    const-string v0, "InCallActivity.showMainInCallFragment"

    const-string v1, "not visible yet/anymore"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 642
    :goto_0
    return-void

    .line 498
    :cond_0
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->o:Z

    if-eqz v0, :cond_1

    .line 499
    const-string v0, "InCallActivity.showMainInCallFragment"

    const-string v1, "already in method, bailing"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 501
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->o:Z

    .line 503
    sget-object v0, Lcct;->a:Lcct;

    .line 504
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v1

    .line 505
    if-eqz v1, :cond_4

    .line 506
    const-string v0, "InCallActivity.getShouldShowAnswerUi"

    const-string v2, "found incoming call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 507
    new-instance v0, Lcaw;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcaw;-><init>(ZLcdc;)V

    .line 526
    :goto_1
    sget-object v1, Lcct;->a:Lcct;

    .line 527
    invoke-virtual {v1}, Lcct;->j()Lcdc;

    move-result-object v2

    .line 528
    if-nez v2, :cond_9

    .line 529
    const-string v1, "InCallActivity.getShouldShowVideoUi"

    const-string v2, "null call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 530
    new-instance v1, Lcaw;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcaw;-><init>(ZLcdc;)V

    .line 539
    :goto_2
    const-string v2, "InCallActivity.showMainInCallFragment"

    const-string v3, "shouldShowAnswerUi: %b, shouldShowVideoUi: %b, didShowAnswerScreen: %b, didShowInCallScreen: %b, didShowVideoCallScreen: %b"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-boolean v6, v0, Lcaw;->a:Z

    .line 540
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-boolean v6, v1, Lcaw;->a:Z

    .line 541
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/android/incallui/InCallActivity;->g:Z

    .line 542
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-boolean v6, p0, Lcom/android/incallui/InCallActivity;->h:Z

    .line 543
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-boolean v6, p0, Lcom/android/incallui/InCallActivity;->i:Z

    .line 544
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    .line 545
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 546
    iget-boolean v2, v1, Lcaw;->a:Z

    invoke-virtual {p0, v2}, Lcom/android/incallui/InCallActivity;->e(Z)V

    .line 547
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v2

    invoke-virtual {v2}, Lja;->a()Ljy;

    move-result-object v8

    .line 548
    iget-boolean v2, v0, Lcaw;->a:Z

    if-eqz v2, :cond_18

    .line 549
    invoke-virtual {p0, v8}, Lcom/android/incallui/InCallActivity;->a(Ljy;)Z

    move-result v7

    .line 550
    invoke-virtual {p0, v8}, Lcom/android/incallui/InCallActivity;->b(Ljy;)Z

    move-result v6

    .line 551
    iget-object v5, v0, Lcaw;->b:Lcdc;

    .line 552
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->g:Z

    if-eqz v0, :cond_c

    if-nez v5, :cond_c

    .line 553
    const/4 v0, 0x0

    :goto_3
    move v1, v6

    move v2, v7

    .line 638
    :goto_4
    if-nez v2, :cond_2

    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    .line 639
    :cond_2
    invoke-virtual {v8}, Ljy;->c()V

    .line 640
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->n:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 641
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->o:Z

    goto/16 :goto_0

    .line 508
    :cond_4
    sget-object v0, Lcct;->a:Lcct;

    .line 509
    invoke-virtual {v0}, Lcct;->l()Lcdc;

    move-result-object v1

    .line 510
    if-eqz v1, :cond_5

    .line 511
    const-string v0, "InCallActivity.getShouldShowAnswerUi"

    const-string v2, "found video upgrade request"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    new-instance v0, Lcaw;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcaw;-><init>(ZLcdc;)V

    goto/16 :goto_1

    .line 513
    :cond_5
    sget-object v0, Lcct;->a:Lcct;

    .line 514
    invoke-virtual {v0}, Lcct;->j()Lcdc;

    move-result-object v0

    .line 515
    if-nez v0, :cond_6

    .line 516
    sget-object v0, Lcct;->a:Lcct;

    .line 518
    const/16 v1, 0x8

    .line 519
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 521
    :cond_6
    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->g:Z

    if-eqz v1, :cond_8

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcdc;->j()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_8

    .line 522
    :cond_7
    const-string v1, "InCallActivity.getShouldShowAnswerUi"

    const-string v2, "found disconnecting incoming call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    new-instance v1, Lcaw;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v0}, Lcaw;-><init>(ZLcdc;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 524
    :cond_8
    new-instance v0, Lcaw;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcaw;-><init>(ZLcdc;)V

    goto/16 :goto_1

    .line 531
    :cond_9
    invoke-virtual {v2}, Lcdc;->u()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 532
    const-string v1, "InCallActivity.getShouldShowVideoUi"

    const-string v3, "found video call"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 533
    new-instance v1, Lcaw;

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcaw;-><init>(ZLcdc;)V

    goto/16 :goto_2

    .line 534
    :cond_a
    invoke-virtual {v2}, Lcdc;->w()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 535
    const-string v1, "InCallActivity.getShouldShowVideoUi"

    const-string v3, "upgrading to video"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    new-instance v1, Lcaw;

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcaw;-><init>(ZLcdc;)V

    goto/16 :goto_2

    .line 537
    :cond_b
    new-instance v1, Lcaw;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcaw;-><init>(ZLcdc;)V

    goto/16 :goto_2

    .line 554
    :cond_c
    if-eqz v5, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "didShowAnswerScreen was false but call was still null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 555
    invoke-virtual {v5}, Lcdc;->v()Z

    move-result v2

    .line 556
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->g:Z

    if-eqz v0, :cond_f

    .line 557
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->l()Lcbt;

    move-result-object v0

    .line 558
    invoke-interface {v0}, Lcbt;->T()Ljava/lang/String;

    move-result-object v1

    .line 559
    iget-object v3, v5, Lcdc;->e:Ljava/lang/String;

    .line 560
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 561
    invoke-interface {v0}, Lcbt;->ac()Z

    move-result v1

    invoke-virtual {v5}, Lcdc;->u()Z

    move-result v3

    if-ne v1, v3, :cond_e

    .line 562
    invoke-interface {v0}, Lcbt;->U()Z

    move-result v1

    if-ne v1, v2, :cond_e

    .line 563
    invoke-interface {v0}, Lcbt;->a()Z

    move-result v1

    if-nez v1, :cond_e

    .line 564
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 554
    :cond_d
    const/4 v0, 0x0

    goto :goto_5

    .line 565
    :cond_e
    invoke-interface {v0}, Lcbt;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 566
    const-string v0, "InCallActivity.showAnswerScreenFragment"

    const-string v1, "answer fragment exists but has been accepted/rejected and timed out"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 568
    :goto_6
    invoke-direct {p0, v8}, Lcom/android/incallui/InCallActivity;->c(Ljy;)Z

    .line 571
    :cond_f
    iget-object v0, v5, Lcdc;->e:Ljava/lang/String;

    .line 573
    invoke-virtual {v5}, Lcdc;->u()Z

    move-result v1

    .line 574
    invoke-virtual {v5}, Lcdc;->F()Lcjs;

    move-result-object v3

    invoke-interface {v3}, Lcjs;->b()Z

    move-result v3

    .line 576
    sget-object v4, Lcct;->a:Lcct;

    .line 578
    const/4 v9, 0x3

    .line 579
    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Lcct;->a(II)Lcdc;

    move-result-object v4

    .line 580
    if-nez v4, :cond_11

    .line 581
    const-string v4, "InCallActivity.shouldAllowAnswerAndRelease"

    const-string v5, "no active call"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v5, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 582
    const/4 v4, 0x0

    .line 593
    :goto_7
    sget-object v5, Lcct;->a:Lcct;

    .line 595
    const/16 v9, 0x8

    .line 596
    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Lcct;->a(II)Lcdc;

    move-result-object v5

    .line 597
    if-eqz v5, :cond_16

    const/4 v5, 0x1

    .line 598
    :goto_8
    invoke-static/range {v0 .. v5}, Lbvs;->a(Ljava/lang/String;ZZZZZ)Lcbt;

    move-result-object v0

    .line 599
    const v1, 0x7f0e01f2

    if-nez v0, :cond_17

    const/4 v0, 0x0

    throw v0

    .line 567
    :cond_10
    const-string v0, "InCallActivity.showAnswerScreenFragment"

    const-string v1, "answer fragment exists but arguments do not match"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6

    .line 583
    :cond_11
    const-class v4, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v4}, Lcom/android/incallui/InCallActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    const/4 v9, 0x2

    if-ne v4, v9, :cond_12

    .line 584
    const-string v4, "InCallActivity.shouldAllowAnswerAndRelease"

    const-string v5, "PHONE_TYPE_CDMA not supported"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v5, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 585
    const/4 v4, 0x0

    goto :goto_7

    .line 586
    :cond_12
    invoke-virtual {v5}, Lcdc;->u()Z

    move-result v4

    if-nez v4, :cond_13

    invoke-virtual {v5}, Lcdc;->v()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 587
    :cond_13
    const-string v4, "InCallActivity.shouldAllowAnswerAndRelease"

    const-string v5, "video call"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v5, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 588
    const/4 v4, 0x0

    goto :goto_7

    .line 589
    :cond_14
    invoke-static {p0}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v4

    const-string v5, "answer_and_release_enabled"

    const/4 v9, 0x1

    invoke-interface {v4, v5, v9}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_15

    .line 590
    const-string v4, "InCallActivity.shouldAllowAnswerAndRelease"

    const-string v5, "disabled by config"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v5, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 591
    const/4 v4, 0x0

    goto :goto_7

    .line 592
    :cond_15
    const/4 v4, 0x1

    goto :goto_7

    .line 597
    :cond_16
    const/4 v5, 0x0

    goto :goto_8

    .line 599
    :cond_17
    check-cast v0, Lip;

    const-string v2, "tag_answer_screen"

    invoke-virtual {v8, v1, v0, v2}, Ljy;->a(ILip;Ljava/lang/String;)Ljy;

    .line 600
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->o:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->g:Z

    .line 602
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 604
    :cond_18
    iget-boolean v0, v1, Lcaw;->a:Z

    if-eqz v0, :cond_1b

    .line 605
    invoke-virtual {p0, v8}, Lcom/android/incallui/InCallActivity;->a(Ljy;)Z

    move-result v2

    .line 606
    iget-object v0, v1, Lcaw;->b:Lcdc;

    .line 607
    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->i:Z

    if-eqz v1, :cond_1a

    .line 608
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->m()Lcjk;

    move-result-object v1

    .line 609
    invoke-interface {v1}, Lcjk;->g()Ljava/lang/String;

    move-result-object v1

    .line 610
    iget-object v3, v0, Lcdc;->e:Ljava/lang/String;

    .line 611
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 612
    const/4 v0, 0x0

    .line 626
    :goto_9
    invoke-direct {p0, v8}, Lcom/android/incallui/InCallActivity;->c(Ljy;)Z

    move-result v1

    move v11, v1

    move v1, v0

    move v0, v11

    goto/16 :goto_4

    .line 613
    :cond_19
    const-string v1, "InCallActivity.showVideoCallScreenFragment"

    const-string v3, "video call fragment exists but arguments do not match"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 614
    invoke-virtual {p0, v8}, Lcom/android/incallui/InCallActivity;->b(Ljy;)Z

    .line 615
    :cond_1a
    const-string v1, "InCallActivity.showVideoCallScreenFragment"

    const-string v3, "call: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 618
    iget-object v1, v0, Lcdc;->e:Ljava/lang/String;

    .line 619
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->c()Z

    move-result v0

    .line 620
    invoke-static {v1, v0}, Lbvs;->a(Ljava/lang/String;Z)Lcjk;

    move-result-object v0

    .line 621
    const v1, 0x7f0e01f2

    invoke-interface {v0}, Lcjk;->f()Lip;

    move-result-object v0

    const-string v3, "tag_video_call_screen"

    invoke-virtual {v8, v1, v0, v3}, Ljy;->a(ILip;Ljava/lang/String;)Ljy;

    .line 622
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->n:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 623
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->i:Z

    .line 624
    const/4 v0, 0x1

    goto :goto_9

    .line 628
    :cond_1b
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->h:Z

    if-eqz v0, :cond_1c

    .line 629
    const/4 v0, 0x0

    .line 636
    :goto_a
    invoke-virtual {p0, v8}, Lcom/android/incallui/InCallActivity;->b(Ljy;)Z

    move-result v2

    .line 637
    invoke-direct {p0, v8}, Lcom/android/incallui/InCallActivity;->c(Ljy;)Z

    move-result v1

    move v11, v1

    move v1, v2

    move v2, v0

    move v0, v11

    goto/16 :goto_4

    .line 630
    :cond_1c
    invoke-static {}, Lbvs;->a()Lcgm;

    move-result-object v0

    .line 631
    const v1, 0x7f0e01f2

    check-cast v0, Lip;

    const-string v2, "tag_in_call_screen"

    invoke-virtual {v8, v1, v0, v2}, Ljy;->a(ILip;Ljava/lang/String;)Ljy;

    .line 632
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lblb$a;->n:Lblb$a;

    invoke-interface {v0, v1, p0}, Lbku;->a(Lblb$a;Landroid/app/Activity;)V

    .line 633
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->h:Z

    .line 634
    const/4 v0, 0x1

    goto :goto_a
.end method

.method private l()Lcbt;
    .locals 2

    .prologue
    .line 664
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_answer_screen"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcbt;

    return-object v0
.end method

.method private m()Lcjk;
    .locals 2

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_video_call_screen"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcjk;

    return-object v0
.end method


# virtual methods
.method public final a(Lcbt;)Lcbu;
    .locals 3

    .prologue
    .line 461
    sget-object v0, Lcct;->a:Lcct;

    .line 462
    invoke-interface {p1}, Lcbt;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 463
    if-nez v0, :cond_0

    .line 464
    const-string v0, "InCallActivity.onPrimaryCallStateChanged"

    const-string v1, "call doesn\'t exist, using stub"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 465
    new-instance v0, Lbup;

    invoke-direct {v0}, Lbup;-><init>()V

    .line 469
    :goto_0
    return-object v0

    .line 466
    :cond_0
    new-instance v0, Lbum;

    .line 467
    sget-object v1, Lcct;->a:Lcct;

    .line 468
    invoke-interface {p1}, Lcbt;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lbum;-><init>(Landroid/content/Context;Lcbt;Lcdc;)V

    goto :goto_0
.end method

.method public final a(Lcjk;)Lcjl;
    .locals 2

    .prologue
    .line 472
    sget-object v0, Lcct;->a:Lcct;

    .line 473
    invoke-interface {p1}, Lcjk;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcct;->a(Ljava/lang/String;)Lcdc;

    move-result-object v0

    .line 474
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v1

    invoke-interface {v1}, Lcjs;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 475
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcjs;->a(Landroid/content/Context;Lcjk;)Lcjl;

    move-result-object v0

    .line 476
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbxh;

    invoke-direct {v0}, Lbxh;-><init>()V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 8

    .prologue
    const/high16 v7, 0x66000000

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 386
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 387
    iget-object v0, v0, Lbwg;->x:Lbxf;

    .line 389
    invoke-static {p0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 391
    iget v2, v0, Lbxf;->f:I

    .line 394
    iget v1, v0, Lbxf;->f:I

    .line 397
    iget v0, v0, Lbxf;->f:I

    .line 408
    :goto_0
    const/4 v5, 0x0

    cmpg-float v5, p1, v5

    if-gez v5, :cond_0

    .line 409
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 410
    invoke-static {v2, v7, v5}, Lmt;->a(IIF)I

    move-result v2

    .line 411
    invoke-static {v1, v7, v5}, Lmt;->a(IIF)I

    move-result v1

    .line 412
    invoke-static {v0, v7, v5}, Lmt;->a(IIF)I

    move-result v0

    .line 414
    :cond_0
    iget-object v5, p0, Lcom/android/incallui/InCallActivity;->l:Landroid/graphics/drawable/GradientDrawable;

    if-nez v5, :cond_4

    .line 415
    const/4 v5, 0x3

    new-array v5, v5, [I

    aput v2, v5, v4

    aput v1, v5, v3

    aput v0, v5, v6

    iput-object v5, p0, Lcom/android/incallui/InCallActivity;->k:[I

    .line 416
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->k:[I

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->l:Landroid/graphics/drawable/GradientDrawable;

    .line 429
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 430
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->l:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 431
    :cond_2
    return-void

    .line 400
    :cond_3
    iget v2, v0, Lbxf;->c:I

    .line 403
    iget v1, v0, Lbxf;->d:I

    .line 406
    iget v0, v0, Lbxf;->e:I

    goto :goto_0

    .line 418
    :cond_4
    iget-object v5, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aget v5, v5, v4

    if-eq v5, v2, :cond_7

    .line 419
    iget-object v5, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aput v2, v5, v4

    move v2, v3

    .line 421
    :goto_2
    iget-object v4, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aget v4, v4, v3

    if-eq v4, v1, :cond_5

    .line 422
    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aput v1, v2, v3

    move v2, v3

    .line 424
    :cond_5
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aget v1, v1, v6

    if-eq v1, v0, :cond_6

    .line 425
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->k:[I

    aput v0, v1, v6

    .line 427
    :goto_3
    if-eqz v3, :cond_1

    .line 428
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->l:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->k:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_3

    :cond_7
    move v2, v4

    goto :goto_2
.end method

.method public final a(Ljy;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 650
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->h:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 656
    :goto_0
    return v0

    .line 652
    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->j()Lcgm;

    move-result-object v0

    .line 653
    if-eqz v0, :cond_2

    .line 654
    if-nez v0, :cond_1

    const/4 v0, 0x0

    throw v0

    :cond_1
    check-cast v0, Lip;

    invoke-virtual {p1, v0}, Ljy;->a(Lip;)Ljy;

    .line 655
    :cond_2
    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->h:Z

    .line 656
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(ZZ)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 345
    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 346
    invoke-virtual {v2}, Lbvy;->c()Z

    move-result v3

    .line 347
    const-string v4, "InCallActivityCommon.showDialpadFragment"

    const-string v5, "show: %b, animate: %b, isDialpadVisible: %b"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 348
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v0

    .line 349
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x2

    .line 350
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    .line 351
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    if-ne p1, v3, :cond_1

    .line 383
    :goto_0
    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->j()Lcgm;

    move-result-object v1

    invoke-interface {v1, p1}, Lcgm;->g(Z)V

    .line 385
    :cond_0
    return v0

    .line 354
    :cond_1
    iget-object v3, v2, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v3}, Lcom/android/incallui/InCallActivity;->f()Lja;

    move-result-object v3

    .line 355
    if-nez v3, :cond_2

    .line 356
    const-string v1, "InCallActivityCommon.showDialpadFragment"

    const-string v2, "unable to show or hide dialpad fragment"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    :cond_2
    if-nez p2, :cond_5

    .line 359
    if-eqz p1, :cond_4

    .line 360
    invoke-virtual {v2, v3}, Lbvy;->a(Lja;)V

    .line 373
    :goto_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 374
    iget-object v0, v0, Lbwg;->o:Lbxb;

    .line 376
    if-eqz v0, :cond_3

    .line 378
    iput-boolean p1, v0, Lbxb;->f:Z

    .line 379
    invoke-virtual {v0}, Lbxb;->a()V

    .line 380
    :cond_3
    iput v1, v2, Lbvy;->l:I

    move v0, v1

    .line 381
    goto :goto_0

    .line 361
    :cond_4
    invoke-virtual {v2}, Lbvy;->b()V

    goto :goto_1

    .line 362
    :cond_5
    if-eqz p1, :cond_6

    .line 363
    invoke-virtual {v2, v3}, Lbvy;->a(Lja;)V

    .line 364
    invoke-virtual {v2}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 366
    iget-object v0, v0, Lip;->I:Landroid/view/View;

    .line 367
    const v3, 0x7f0e0190

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadView;

    .line 368
    invoke-virtual {v0}, Lcom/android/dialer/dialpadview/DialpadView;->a()V

    .line 369
    :cond_6
    invoke-virtual {v2}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 371
    iget-object v3, v0, Lip;->I:Landroid/view/View;

    .line 372
    if-eqz p1, :cond_7

    iget-object v0, v2, Lbvy;->h:Landroid/view/animation/Animation;

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    :cond_7
    iget-object v0, v2, Lbvy;->i:Landroid/view/animation/Animation;

    goto :goto_2
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 342
    if-eqz p1, :cond_0

    .line 343
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/incallui/ManageConferenceActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->startActivity(Landroid/content/Intent;)V

    .line 344
    :cond_0
    return-void
.end method

.method public final b(Ljy;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 657
    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->i:Z

    if-nez v1, :cond_0

    .line 663
    :goto_0
    return v0

    .line 659
    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->m()Lcjk;

    move-result-object v1

    .line 660
    if-eqz v1, :cond_1

    .line 661
    invoke-interface {v1}, Lcjk;->f()Lip;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljy;->a(Lip;)Ljy;

    .line 662
    :cond_1
    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->i:Z

    .line 663
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v0, p1}, Lbvy;->a(Z)V

    .line 433
    return-void
.end method

.method public final d(Z)V
    .locals 6

    .prologue
    .line 445
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 446
    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const-class v2, Landroid/app/ActivityManager;

    invoke-virtual {v0, v2}, Lcom/android/incallui/InCallActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v2

    .line 447
    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getTaskId()I

    move-result v3

    .line 448
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 449
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 450
    :try_start_0
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v4

    iget v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    if-ne v4, v3, :cond_0

    .line 451
    invoke-virtual {v0, p1}, Landroid/app/ActivityManager$AppTask;->setExcludeFromRecents(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 453
    :catch_0
    move-exception v0

    .line 454
    const-string v4, "InCallActivityCommon.setExcludeFromRecents"

    const-string v5, "RuntimeException when excluding task from recents."

    invoke-static {v4, v5, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 456
    :cond_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 670
    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->n:Z

    if-eqz v1, :cond_1

    .line 671
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 672
    iput-boolean v3, p0, Lcom/android/incallui/InCallActivity;->n:Z

    .line 683
    :cond_0
    :goto_0
    return v0

    .line 674
    :cond_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 675
    iget-object v1, v1, Lbwg;->p:Lcca;

    .line 677
    iget-boolean v1, v1, Lcca;->b:Z

    .line 678
    if-nez v1, :cond_2

    .line 679
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 680
    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->n:Z

    .line 681
    const-string v1, "InCallActivity.dispatchTouchEvent"

    const-string v2, "touchDownWhenPseudoScreenOff"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 683
    :cond_2
    invoke-super {p0, p1}, Lbxg;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 434
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->j:Z

    if-eqz v0, :cond_1

    .line 435
    const-string v0, "InCallActivity.dismissPendingDialogs"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->a()V

    .line 437
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->l()Lcbt;

    move-result-object v0

    .line 438
    if-eqz v0, :cond_0

    .line 439
    invoke-interface {v0}, Lcbt;->V()V

    .line 440
    :cond_0
    iput-boolean v3, p0, Lcom/android/incallui/InCallActivity;->p:Z

    .line 444
    :goto_0
    return-void

    .line 442
    :cond_1
    const-string v0, "InCallActivity.dismissPendingDialogs"

    const-string v1, "defer actions since activity is not visible"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->p:Z

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->q:Z

    if-ne v0, p1, :cond_0

    .line 494
    :goto_0
    return-void

    .line 488
    :cond_0
    iput-boolean p1, p0, Lcom/android/incallui/InCallActivity;->q:Z

    .line 489
    if-nez p1, :cond_1

    .line 490
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->setRequestedOrientation(I)V

    .line 493
    :goto_1
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v0, p1}, Lbvy;->b(Z)V

    goto :goto_0

    .line 491
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->setRequestedOrientation(I)V

    goto :goto_1
.end method

.method public final f()Lja;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 457
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->j()Lcgm;

    move-result-object v0

    .line 458
    if-eqz v0, :cond_1

    .line 459
    if-nez v0, :cond_0

    throw v1

    :cond_0
    check-cast v0, Lip;

    invoke-virtual {v0}, Lip;->j()Lja;

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 667
    const-string v1, "InCallActivity.onPseudoScreenStateChanged"

    const/16 v2, 0xb

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "isOn: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 668
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->m:Landroid/view/View;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 669
    return-void
.end method

.method protected final f_()V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Lbxg;->f_()V

    .line 10
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->p:Z

    if-eqz v0, :cond_0

    .line 11
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->e()V

    .line 12
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    .line 209
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 210
    if-nez v0, :cond_1

    .line 211
    const-string v0, "InCallActivity.shouldCloseActivityOnFinish"

    const-string v3, "allowing activity to be closed because it\'s not visible"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 226
    :goto_0
    if-eqz v0, :cond_0

    .line 227
    invoke-super {p0}, Lbxg;->finishAndRemoveTask()V

    .line 228
    :cond_0
    return-void

    .line 213
    :cond_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 214
    invoke-static {}, Lbdf;->b()V

    .line 215
    iget-object v3, v0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 221
    :goto_1
    if-eqz v0, :cond_4

    .line 222
    const-string v0, "InCallActivity.shouldCloseActivityOnFinish"

    const-string v1, "in call ui is locked, not closing activity"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 223
    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, v0, Lbwg;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgt;

    .line 218
    const-string v4, "InCallPresenter.isInCallUiLocked"

    const-string v5, "still locked by %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 220
    goto :goto_1

    .line 224
    :cond_4
    const-string v0, "InCallActivity.shouldCloseActivityOnFinish"

    const-string v3, "activity is visible and has no locks, allowing activity to close"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 225
    goto :goto_0
.end method

.method public final g()Lcgn;
    .locals 1

    .prologue
    .line 470
    new-instance v0, Lbuq;

    invoke-direct {v0, p0}, Lbuq;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final h()Lcgk;
    .locals 1

    .prologue
    .line 471
    new-instance v0, Lcbb;

    invoke-direct {v0, p0}, Lcbb;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 477
    const-string v0, "InCallActivity.onPrimaryCallStateChanged"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 478
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->k()V

    .line 479
    return-void
.end method

.method public final j()Lcgm;
    .locals 2

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_in_call_screen"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcgm;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 238
    const-string v0, "InCallActivity.onBackPressed"

    const-string v3, ""

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-object v3, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->i:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 240
    :goto_0
    const-string v4, "InCallActivityCommon.onBackPressed"

    const-string v5, ""

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    iget-object v4, v3, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 242
    iget-boolean v4, v4, Lcom/android/incallui/InCallActivity;->j:Z

    .line 243
    if-nez v4, :cond_4

    .line 257
    :cond_1
    :goto_1
    if-nez v2, :cond_2

    .line 258
    invoke-super {p0}, Lbxg;->onBackPressed()V

    .line 259
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 239
    goto :goto_0

    .line 245
    :cond_4
    if-eqz v0, :cond_1

    .line 247
    invoke-virtual {v3}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 248
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 249
    iget-object v0, v3, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0, v1, v2}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    goto :goto_1

    .line 251
    :cond_5
    sget-object v0, Lcct;->a:Lcct;

    .line 252
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_6

    .line 254
    const-string v0, "InCallActivityCommon.onBackPressed"

    const-string v3, "consume Back press for an incoming call"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    move v2, v1

    .line 256
    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 13
    const-string v0, "InCallActivity.onCreate"

    const-string v1, ""

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 15
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "RETURN_TO_CALL_BUBBLE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cQ:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 17
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "RETURN_TO_CALL_BUBBLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 18
    :cond_0
    if-eqz p1, :cond_1

    .line 19
    const-string v0, "did_show_answer_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->g:Z

    .line 20
    const-string v0, "did_show_in_call_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->h:Z

    .line 21
    const-string v0, "did_show_video_call_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/InCallActivity;->i:Z

    .line 22
    :cond_1
    iget-object v5, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 24
    const v1, 0x88000

    .line 25
    sget-object v0, Lbvy;->a:Lgtm;

    invoke-virtual {v0}, Lgtm;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 26
    sget-object v0, Lbvy;->a:Lgtm;

    invoke-virtual {v0}, Lgtm;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 32
    :goto_0
    if-eq v0, v4, :cond_b

    .line 33
    const v0, 0x288000

    .line 34
    :goto_1
    iget-object v1, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->addFlags(I)V

    .line 35
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const v1, 0x7f040080

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallActivity;->setContentView(I)V

    .line 36
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbvy;->a(Landroid/content/Intent;)V

    .line 37
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 38
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_6

    move v0, v2

    .line 39
    :goto_2
    invoke-static {}, Lbib;->i()Z

    move-result v1

    .line 40
    if-eqz v0, :cond_9

    .line 41
    iget-object v6, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 42
    if-eqz v1, :cond_7

    const v0, 0x7f05000f

    .line 43
    :goto_3
    invoke-static {v6, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, v5, Lbvy;->h:Landroid/view/animation/Animation;

    .line 44
    iget-object v6, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 45
    if-eqz v1, :cond_8

    const v0, 0x7f050012

    .line 46
    :goto_4
    invoke-static {v6, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, v5, Lbvy;->i:Landroid/view/animation/Animation;

    .line 51
    :goto_5
    iget-object v0, v5, Lbvy;->h:Landroid/view/animation/Animation;

    sget-object v1, Lamn;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 52
    iget-object v0, v5, Lbvy;->i:Landroid/view/animation/Animation;

    sget-object v1, Lamn;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 53
    iget-object v0, v5, Lbvy;->i:Landroid/view/animation/Animation;

    new-instance v1, Lbwb;

    invoke-direct {v1, v5}, Lbwb;-><init>(Lbvy;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 54
    if-eqz p1, :cond_3

    iget v0, v5, Lbvy;->l:I

    if-ne v0, v2, :cond_3

    .line 55
    const-string v0, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    const-string v0, "InCallActivity.show_dialpad"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 57
    if-eqz v0, :cond_a

    move v0, v4

    :goto_6
    iput v0, v5, Lbvy;->l:I

    .line 58
    iput-boolean v3, v5, Lbvy;->j:Z

    .line 59
    :cond_2
    const-string v0, "InCallActivity.dialpad_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lbvy;->k:Ljava/lang/String;

    .line 60
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 61
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "tag_select_account_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lalw;

    .line 62
    if-eqz v0, :cond_3

    .line 63
    iget-object v1, v5, Lbvy;->n:Lamd;

    .line 64
    iput-object v1, v0, Lalw;->d:Lamd;

    .line 65
    :cond_3
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 66
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_international_call_on_wifi"

    .line 67
    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcif;

    .line 68
    if-eqz v0, :cond_4

    .line 69
    const-string v1, "InCallActivityCommon.onCreate"

    const-string v2, "international fragment exists attaching callback"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object v1, v5, Lbvy;->o:Lcii;

    invoke-virtual {v0, v1}, Lcif;->a(Lcii;)V

    .line 71
    :cond_4
    new-instance v0, Lbwf;

    iget-object v1, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-direct {v0, v1}, Lbwf;-><init>(Landroid/content/Context;)V

    iput-object v0, v5, Lbvy;->g:Lbwf;

    .line 72
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x600

    .line 74
    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 75
    const v0, 0x7f0e01f3

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallActivity;->m:Landroid/view/View;

    .line 77
    new-instance v0, Landroid/content/Intent;

    const-string v1, "dialer.intent.action.CALL_PENDING_ACTIVITY_FINISH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 79
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "CallList.onCallAdded_To_InCallActivity.onCreate_Incoming"

    .line 80
    invoke-interface {v0, v1}, Lbku;->b(Ljava/lang/String;)V

    .line 81
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    const-string v1, "CallList.onCallAdded_To_InCallActivity.onCreate_Outgoing"

    .line 82
    invoke-interface {v0, v1}, Lbku;->b(Ljava/lang/String;)V

    .line 83
    return-void

    .line 27
    :cond_5
    sget-object v0, Lcce;->a:Lcce;

    .line 29
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 30
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v0

    goto/16 :goto_0

    :cond_6
    move v0, v3

    .line 38
    goto/16 :goto_2

    .line 42
    :cond_7
    const v0, 0x7f050010

    goto/16 :goto_3

    .line 45
    :cond_8
    const v0, 0x7f050013

    goto/16 :goto_4

    .line 47
    :cond_9
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const v1, 0x7f05000e

    .line 48
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, v5, Lbvy;->h:Landroid/view/animation/Animation;

    .line 49
    iget-object v0, v5, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const v1, 0x7f050011

    .line 50
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, v5, Lbvy;->i:Landroid/view/animation/Animation;

    goto/16 :goto_5

    .line 57
    :cond_a
    const/4 v0, 0x3

    goto/16 :goto_6

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 201
    const-string v0, "InCallActivity.onDestroy"

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    invoke-super {p0}, Lbxg;->onDestroy()V

    .line 203
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 204
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1, v0}, Lbwg;->a(Lcom/android/incallui/InCallActivity;)V

    .line 205
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->e()V

    .line 206
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 275
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 276
    sparse-switch p1, :sswitch_data_0

    .line 329
    :cond_0
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_a

    .line 330
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    aput-object p2, v3, v1

    .line 331
    invoke-virtual {v0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 332
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/android/incallui/DialpadFragment;->l()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 334
    const-string v3, "Notifying dtmf key down."

    invoke-static {v0, v3}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    iget-object v3, v0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    if-eqz v3, :cond_8

    .line 336
    iget-object v0, v0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    invoke-virtual {v0, p2}, Lbvt;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 340
    :goto_0
    if-eqz v0, :cond_a

    move v0, v1

    .line 341
    :goto_1
    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Lbxg;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    .line 277
    :sswitch_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 278
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 279
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v3

    .line 280
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "incomingCall: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    if-eqz v3, :cond_4

    .line 282
    invoke-virtual {v3, v2}, Lcdc;->e(I)V

    :cond_3
    :goto_2
    move v0, v1

    .line 315
    goto :goto_1

    .line 285
    :cond_4
    const/4 v3, 0x3

    .line 286
    invoke-virtual {v0, v3, v2}, Lcct;->a(II)Lcdc;

    move-result-object v3

    .line 288
    if-eqz v3, :cond_6

    .line 289
    const/4 v4, 0x4

    .line 290
    invoke-virtual {v3, v4}, Lcdc;->c(I)Z

    move-result v4

    .line 292
    invoke-virtual {v3, v9}, Lcdc;->c(I)Z

    move-result v5

    .line 293
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x2d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "activeCall: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", canMerge: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", canSwap: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 294
    if-eqz v4, :cond_5

    .line 295
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    .line 296
    iget-object v3, v3, Lcdc;->e:Ljava/lang/String;

    .line 297
    invoke-virtual {v0, v3}, Lcdr;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 299
    :cond_5
    if-eqz v5, :cond_6

    .line 300
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    .line 301
    iget-object v3, v3, Lcdc;->e:Ljava/lang/String;

    .line 302
    invoke-virtual {v0, v3}, Lcdr;->c(Ljava/lang/String;)V

    goto :goto_2

    .line 306
    :cond_6
    invoke-virtual {v0, v9, v2}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_3

    .line 309
    invoke-virtual {v0, v1}, Lcdc;->c(I)Z

    move-result v3

    .line 310
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1a

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "heldCall: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", canHold: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 311
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v4

    if-ne v4, v9, :cond_3

    if-eqz v3, :cond_3

    .line 312
    invoke-virtual {v0}, Lcdc;->C()V

    goto/16 :goto_2

    :sswitch_2
    move v0, v1

    .line 316
    goto/16 :goto_1

    .line 318
    :sswitch_3
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v3

    .line 319
    sget-object v0, Lcce;->a:Lcce;

    .line 321
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 322
    invoke-virtual {v0}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Lcdr;->a(Z)V

    move v0, v1

    .line 323
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 322
    goto :goto_3

    .line 324
    :sswitch_4
    invoke-static {}, Lapw;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 325
    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 327
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "View dump:"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 328
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 338
    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 339
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 340
    goto/16 :goto_1

    .line 276
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_2
        0x4c -> :sswitch_4
        0x5b -> :sswitch_3
        0xa4 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 265
    iget-object v2, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 266
    invoke-virtual {v2}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v2

    .line 267
    if-eqz v2, :cond_2

    .line 268
    invoke-virtual {v2}, Lcom/android/incallui/DialpadFragment;->l()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 269
    invoke-virtual {v2, p2}, Lcom/android/incallui/DialpadFragment;->a(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    .line 274
    :goto_0
    if-nez v2, :cond_0

    invoke-super {p0, p1, p2}, Lbxg;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    .line 271
    :cond_2
    const/4 v2, 0x5

    if-ne p1, v2, :cond_3

    move v2, v1

    .line 272
    goto :goto_0

    :cond_3
    move v2, v0

    .line 273
    goto :goto_0
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 2

    .prologue
    .line 480
    invoke-super {p0, p1}, Lbxg;->onMultiWindowModeChanged(Z)V

    .line 481
    if-nez p1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 483
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v1}, Lbvy;->c()Z

    move-result v1

    .line 484
    invoke-virtual {v0, v1}, Lbvy;->c(Z)V

    .line 485
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 229
    const-string v0, "InCallActivity.onNewIntent"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-boolean v0, p0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 232
    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbvy;->a(Landroid/content/Intent;Z)V

    .line 234
    const-string v0, "InCallActivity.onNewIntent"

    const-string v1, "Restarting InCallActivity to force screen on."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->recreate()V

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    invoke-virtual {v0, p1, v3}, Lbvy;->a(Landroid/content/Intent;Z)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 260
    const-string v0, "InCallActivity.onOptionsItemSelected"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "item: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->onBackPressed()V

    .line 263
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbxg;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 166
    const-string v0, "InCallActivity.onPause"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    invoke-super {p0}, Lbxg;->onPause()V

    .line 168
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 169
    invoke-virtual {v0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/incallui/DialpadFragment;->a(Landroid/view/KeyEvent;)Z

    .line 172
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1, v3}, Lbwg;->a(Z)V

    .line 173
    iget-object v1, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1}, Lcom/android/incallui/InCallActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v1, v0}, Lbwg;->a(Lcom/android/incallui/InCallActivity;)V

    .line 175
    :cond_1
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 176
    iget-object v0, v0, Lbwg;->p:Lcca;

    .line 178
    iget-object v0, v0, Lcca;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method

.method protected onResume()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    const-string v0, "InCallActivity.onResume"

    const-string v3, ""

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Lit;->onResume()V

    .line 121
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 122
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    invoke-virtual {v3}, Lbwg;->f()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 123
    const-string v3, "InCallActivityCommon.onResume"

    const-string v4, "InCallPresenter is ready for tear down, not sending updates"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    :goto_0
    iget v3, v0, Lbvy;->l:I

    if-eq v3, v1, :cond_1

    .line 127
    iget v3, v0, Lbvy;->l:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 128
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lbwg;->a(ZZ)V

    .line 129
    iget-object v3, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    iget-boolean v4, v0, Lbvy;->j:Z

    invoke-virtual {v3, v1, v4}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    .line 130
    iput-boolean v2, v0, Lbvy;->j:Z

    .line 131
    invoke-virtual {v0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v3

    .line 132
    if-eqz v3, :cond_0

    .line 133
    iget-object v4, v0, Lbvy;->k:Ljava/lang/String;

    .line 134
    iget-object v3, v3, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    invoke-static {v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const/4 v3, 0x0

    iput-object v3, v0, Lbvy;->k:Ljava/lang/String;

    .line 140
    :cond_0
    :goto_1
    iput v1, v0, Lbvy;->l:I

    .line 141
    :cond_1
    invoke-virtual {v0}, Lbvy;->c()Z

    move-result v3

    invoke-virtual {v0, v3}, Lbvy;->c(Z)V

    .line 142
    iget-boolean v3, v0, Lbvy;->c:Z

    if-eqz v3, :cond_2

    .line 143
    iget-object v3, v0, Lbvy;->d:Ljava/lang/String;

    iget-object v4, v0, Lbvy;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lbvy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_2
    sget-object v3, Lcct;->a:Lcct;

    .line 145
    iget-object v0, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 146
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "InCallActivity.for_full_screen_intent"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 148
    iget-object v0, v3, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 150
    iget-object v5, v0, Lcdc;->d:Lcgu;

    .line 152
    iget-wide v6, v5, Lcgu;->h:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    .line 153
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, v5, Lcgu;->h:J

    .line 154
    iget-boolean v0, v5, Lcgu;->a:Z

    if-eqz v0, :cond_6

    if-nez v4, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, v5, Lcgu;->i:Z

    goto :goto_2

    .line 124
    :cond_4
    invoke-virtual {v0}, Lbvy;->e()V

    .line 125
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    invoke-virtual {v3, v1}, Lbwg;->a(Z)V

    goto/16 :goto_0

    .line 137
    :cond_5
    const-string v3, "InCallActivityCommon.onResume"

    const-string v4, "force hide dialpad"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    invoke-virtual {v0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 139
    iget-object v3, v0, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    invoke-virtual {v3, v2, v2}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    goto :goto_1

    :cond_6
    move v0, v2

    .line 154
    goto :goto_3

    .line 156
    :cond_7
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 157
    iget-object v0, v0, Lbwg;->p:Lcca;

    .line 160
    iget-object v1, v0, Lcca;->a:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    iget-boolean v0, v0, Lcca;->b:Z

    .line 163
    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallActivity;->f(Z)V

    .line 164
    new-instance v0, Lbvx;

    invoke-direct {v0, p0}, Lbvx;-><init>(Lcom/android/incallui/InCallActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/Runnable;J)V

    .line 165
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    const-string v0, "InCallActivity.onSaveInstanceState"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 86
    const-string v1, "InCallActivity.show_dialpad"

    invoke-virtual {v0}, Lbvy;->c()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 87
    invoke-virtual {v0}, Lbvy;->d()Lcom/android/incallui/DialpadFragment;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    const-string v1, "InCallActivity.dialpad_text"

    .line 90
    iget-object v0, v0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    const-string v0, "did_show_answer_screen"

    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    const-string v0, "did_show_in_call_screen"

    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 94
    const-string v0, "did_show_video_call_screen"

    iget-boolean v1, p0, Lcom/android/incallui/InCallActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 95
    invoke-super {p0, p1}, Lit;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    iput-boolean v3, p0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 97
    return-void
.end method

.method protected onStart()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    const-string v0, "InCallActivity.onStart"

    const-string v3, ""

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    invoke-super {p0}, Lit;->onStart()V

    .line 100
    iput-boolean v1, p0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 101
    invoke-direct {p0}, Lcom/android/incallui/InCallActivity;->k()V

    .line 102
    iget-object v3, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 103
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    iget-object v4, v3, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 104
    if-nez v4, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "registerActivity cannot be called with null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    iget-object v5, v0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eq v5, v4, :cond_1

    .line 107
    const-string v5, "InCallPresenter.setActivity"

    const-string v6, "Setting a second activity before destroying the first."

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    :cond_1
    invoke-virtual {v0, v4}, Lbwg;->b(Lcom/android/incallui/InCallActivity;)V

    .line 109
    iget-object v0, v3, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    .line 110
    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->getRequestedOrientation()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    move v0, v1

    .line 111
    :goto_0
    invoke-virtual {v3, v0}, Lbvy;->b(Z)V

    .line 112
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 113
    invoke-virtual {v0, v1}, Lbwg;->b(Z)V

    .line 114
    invoke-virtual {v0}, Lbwg;->i()V

    .line 115
    invoke-static {p0}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116
    invoke-virtual {p0}, Lcom/android/incallui/InCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 117
    invoke-virtual {p0, v2, v2}, Lcom/android/incallui/InCallActivity;->a(ZZ)Z

    .line 118
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 110
    goto :goto_0
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 180
    const-string v0, "InCallActivity.onStop"

    const-string v1, ""

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    iput-boolean v3, p0, Lcom/android/incallui/InCallActivity;->j:Z

    .line 182
    invoke-super {p0}, Lbxg;->onStop()V

    .line 183
    iget-object v1, p0, Lcom/android/incallui/InCallActivity;->f:Lbvy;

    .line 184
    iget-boolean v0, v1, Lbvy;->m:Z

    if-nez v0, :cond_0

    iget-object v0, v1, Lbvy;->b:Lcom/android/incallui/InCallActivity;

    const-class v2, Landroid/app/KeyguardManager;

    .line 185
    invoke-virtual {v0, v2}, Lcom/android/incallui/InCallActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    sget-object v0, Lcct;->a:Lcct;

    .line 188
    const/16 v2, 0xc

    .line 189
    invoke-virtual {v0, v2, v3}, Lcct;->a(II)Lcdc;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0}, Lcdc;->B()V

    .line 193
    :cond_0
    invoke-virtual {v1, v3}, Lbvy;->b(Z)V

    .line 194
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0}, Lbwg;->e()V

    .line 195
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 196
    invoke-virtual {v0, v3}, Lbwg;->b(Z)V

    .line 197
    iget-boolean v0, v1, Lbvy;->m:Z

    if-nez v0, :cond_1

    .line 198
    iget-object v0, v1, Lbvy;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, v1, Lbvy;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 200
    :cond_1
    return-void
.end method
