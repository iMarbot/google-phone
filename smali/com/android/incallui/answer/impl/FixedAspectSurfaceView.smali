.class public Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;
.super Landroid/view/SurfaceView;
.source "PG"


# instance fields
.field private a:F

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v2, Lbyk;->a:[I

    invoke-virtual {v0, p2, v2, v1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 4
    sget v0, Lbyk;->c:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->c:Z

    .line 5
    sget v0, Lbyk;->d:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->b:Z

    .line 6
    iget-boolean v0, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->c:Z

    iget-boolean v3, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->b:Z

    if-eq v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Must either scale width or height"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7
    sget v0, Lbyk;->b:I

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->a(F)V

    .line 8
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 9
    return-void

    :cond_0
    move v0, v1

    .line 6
    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 10
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Aspect ratio must be positive"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11
    iput p1, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->a:F

    .line 12
    invoke-virtual {p0}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->requestLayout()V

    .line 13
    return-void

    :cond_0
    move v0, v1

    .line 10
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 15
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 16
    iget-boolean v2, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->b:Z

    if-eqz v2, :cond_1

    .line 17
    int-to-float v1, v0

    iget v2, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 20
    :cond_0
    :goto_0
    invoke-static {v1, p1, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v1

    .line 21
    invoke-static {v0, p2, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v0

    .line 22
    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->setMeasuredDimension(II)V

    .line 23
    return-void

    .line 18
    :cond_1
    iget-boolean v2, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->c:Z

    if-eqz v2, :cond_0

    .line 19
    int-to-float v0, v1

    iget v2, p0, Lcom/android/incallui/answer/impl/FixedAspectSurfaceView;->a:F

    div-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0
.end method
