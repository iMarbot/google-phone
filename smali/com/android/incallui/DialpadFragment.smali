.class public final Lcom/android/incallui/DialpadFragment;
.super Lccj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lbvv;
.implements Lcom/android/dialer/dialpadview/DialpadKeyButton$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/DialpadFragment$DialpadSlidingLinearLayout;
    }
.end annotation


# static fields
.field private static Y:Ljava/util/Map;


# instance fields
.field public W:Lbvt;

.field private Z:[I

.field public a:Landroid/widget/EditText;

.field private aa:Lcom/android/dialer/dialpadview/DialpadView;

.field private ab:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 84
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 85
    sput-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e018c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x34

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e017f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e0180

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e0181

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x39

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e0182

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e018e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    const v1, 0x7f0e018f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lccj;-><init>()V

    .line 2
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/incallui/DialpadFragment;->Z:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0e0182
        0x7f0e018c
        0x7f0e017a
        0x7f0e017b
        0x7f0e017c
        0x7f0e017d
        0x7f0e017e
        0x7f0e017f
        0x7f0e0180
        0x7f0e0181
        0x7f0e018f
        0x7f0e018e
    .end array-data
.end method


# virtual methods
.method public final synthetic T()Lcck;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lbvu;

    invoke-direct {v0}, Lbvu;-><init>()V

    .line 83
    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 21
    const v0, 0x7f04007e

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 22
    const v0, 0x7f0e0190

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadView;

    iput-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    .line 23
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    invoke-virtual {v0, v2}, Lcom/android/dialer/dialpadview/DialpadView;->a(Z)V

    .line 24
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    const v1, 0x7f0c0093

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->setBackgroundResource(I)V

    .line 25
    const v0, 0x7f0e0198

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    .line 26
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Lbvt;

    .line 28
    invoke-direct {v0, p0}, Lbvt;-><init>(Lcom/android/incallui/DialpadFragment;)V

    .line 29
    iput-object v0, p0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    .line 30
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 31
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setLongClickable(Z)V

    .line 32
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setElegantTextHeight(Z)V

    move v1, v2

    .line 34
    :goto_0
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->Z:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    iget-object v4, p0, Lcom/android/incallui/DialpadFragment;->Z:[I

    aget v4, v4, v1

    invoke-virtual {v0, v4}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 36
    invoke-virtual {v0, p0}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 37
    invoke-virtual {v0, p0}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    iput-object p0, v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;->b:Lcom/android/dialer/dialpadview/DialpadKeyButton$a;

    .line 40
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    const v1, 0x7f0e0196

    invoke-virtual {v0, v1}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 42
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 43
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-object v3
.end method

.method public final a(C)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Editable;->append(C)Landroid/text/Editable;

    .line 64
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 69
    if-eqz p2, :cond_0

    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/android/incallui/DialpadFragment;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dp:Lbkq$a;

    .line 71
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 72
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onPressed: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lccj;->X:Lcck;

    .line 75
    check-cast v0, Lbvu;

    sget-object v1, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-virtual {v0, v1}, Lbvu;->a(C)V

    .line 76
    :cond_0
    if-nez p2, :cond_1

    .line 77
    const/16 v0, 0x10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "onPressed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lccj;->X:Lcck;

    .line 80
    check-cast v0, Lbvu;

    invoke-virtual {v0}, Lbvu;->a()V

    .line 81
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 65
    const-string v0, "Notifying dtmf key up."

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    invoke-virtual {v0, p1}, Lbvt;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/DialpadFragment;->W:Lbvt;

    .line 60
    invoke-super {p0}, Lccj;->n_()V

    .line 61
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e0196

    if-ne v0, v1, :cond_0

    .line 4
    invoke-virtual {p0}, Lcom/android/incallui/DialpadFragment;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dr:Lbkq$a;

    .line 5
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 6
    invoke-virtual {p0}, Lcom/android/incallui/DialpadFragment;->h()Lit;

    move-result-object v0

    invoke-virtual {v0}, Lit;->onBackPressed()V

    .line 7
    :cond_0
    return-void
.end method

.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 8
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x22

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "onKey:  keyCode "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    const/16 v0, 0x17

    if-eq p2, v0, :cond_0

    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 11
    sget-object v0, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 13
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 15
    iget-object v0, p0, Lccj;->X:Lcck;

    .line 16
    check-cast v0, Lbvu;

    sget-object v2, Lcom/android/incallui/DialpadFragment;->Y:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-virtual {v0, v1}, Lbvu;->a(C)V

    goto :goto_0

    .line 18
    :pswitch_1
    iget-object v0, p0, Lccj;->X:Lcck;

    .line 19
    check-cast v0, Lbvu;

    invoke-virtual {v0}, Lbvu;->a()V

    goto :goto_0

    .line 12
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 45
    invoke-super {p0}, Lccj;->r()V

    .line 47
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 48
    iget-object v0, v0, Lbwg;->x:Lbxf;

    .line 50
    iget v2, v0, Lbxf;->a:I

    .line 52
    iget v0, p0, Lcom/android/incallui/DialpadFragment;->ab:I

    if-eq v0, v2, :cond_1

    .line 53
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->Z:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/android/incallui/DialpadFragment;->aa:Lcom/android/dialer/dialpadview/DialpadView;

    iget-object v3, p0, Lcom/android/incallui/DialpadFragment;->Z:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Lcom/android/dialer/dialpadview/DialpadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/dialpadview/DialpadKeyButton;

    .line 55
    const v3, 0x7f0e0187

    invoke-virtual {v0, v3}, Lcom/android/dialer/dialpadview/DialpadKeyButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_0
    iput v2, p0, Lcom/android/incallui/DialpadFragment;->ab:I

    .line 58
    :cond_1
    return-void
.end method
