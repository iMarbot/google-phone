.class public Lcom/android/incallui/InCallServiceImpl;
.super Landroid/telecom/InCallService;
.source "PG"


# instance fields
.field private a:Lbxc;

.field private b:Lbww;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/InCallService;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 84
    invoke-virtual {p0}, Lcom/android/incallui/InCallServiceImpl;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 85
    invoke-static {v2}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v0

    .line 86
    sget-object v1, Lcce;->a:Lcce;

    .line 88
    new-instance v3, Landroid/telecom/CallAudioState;

    .line 89
    invoke-static {p0}, Lcce;->a(Landroid/content/Context;)I

    move-result v4

    const/16 v5, 0xf

    invoke-direct {v3, v10, v4, v5}, Landroid/telecom/CallAudioState;-><init>(ZII)V

    .line 90
    invoke-virtual {v1, v3}, Lcce;->a(Landroid/telecom/CallAudioState;)V

    .line 91
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 92
    sget-object v3, Lcct;->a:Lcct;

    .line 93
    new-instance v4, Lcdj;

    invoke-direct {v4}, Lcdj;-><init>()V

    new-instance v5, Lbxd;

    invoke-direct {v5, v2, v0}, Lbxd;-><init>(Landroid/content/Context;Lbvp;)V

    new-instance v6, Lcaj;

    invoke-direct {v6, v2, v0}, Lcaj;-><init>(Landroid/content/Context;Lbvp;)V

    new-instance v7, Lbxb;

    .line 94
    sget-object v8, Lcce;->a:Lcce;

    .line 95
    new-instance v9, Lbui;

    invoke-direct {v9, v2}, Lbui;-><init>(Landroid/content/Context;)V

    invoke-direct {v7, v2, v8, v9}, Lbxb;-><init>(Landroid/content/Context;Lcce;Lbui;)V

    new-instance v8, Lawr;

    invoke-direct {v8, v2}, Lawr;-><init>(Landroid/content/Context;)V

    .line 97
    iget-boolean v9, v1, Lbwg;->q:Z

    if-eqz v9, :cond_1

    .line 98
    const-string v0, "InCallPresenter.setUp"

    const-string v4, "New service connection replacing existing one."

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    if-ne v2, v0, :cond_0

    iget-object v0, v1, Lbwg;->i:Lcct;

    if-eq v3, v0, :cond_3

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 102
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 103
    iput-object v2, v1, Lbwg;->g:Landroid/content/Context;

    .line 104
    iput-object v0, v1, Lbwg;->f:Lbvp;

    .line 105
    iput-object v5, v1, Lbwg;->d:Lbxd;

    .line 106
    iput-object v6, v1, Lbwg;->e:Lcaj;

    .line 107
    iget-object v0, v1, Lbwg;->d:Lbxd;

    invoke-virtual {v1, v0}, Lbwg;->a(Lbwq;)V

    .line 108
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v0

    iget-object v5, v1, Lbwg;->d:Lbxd;

    .line 110
    invoke-interface {v0, v5}, Lbjf;->a(Lbjj;)V

    .line 111
    iput-object v7, v1, Lbwg;->o:Lbxb;

    .line 112
    iget-object v0, v1, Lbwg;->o:Lbxb;

    invoke-virtual {v1, v0}, Lbwg;->a(Lbwq;)V

    .line 113
    iget-object v0, v1, Lbwg;->x:Lbxf;

    if-nez v0, :cond_2

    .line 114
    new-instance v0, Lbxf;

    new-instance v5, Lbwu;

    iget-object v6, v1, Lbwg;->g:Landroid/content/Context;

    .line 115
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6}, Lbwu;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v0, v5}, Lbxf;-><init>(Lalr;)V

    iput-object v0, v1, Lbwg;->x:Lbxf;

    .line 116
    :cond_2
    iput-object v3, v1, Lbwg;->i:Lcct;

    .line 117
    iput-object v4, v1, Lbwg;->j:Lcdj;

    .line 118
    iget-object v0, v1, Lbwg;->e:Lcaj;

    invoke-virtual {v4, v0}, Lcdj;->a(Lcdl;)V

    .line 119
    iget-object v0, v1, Lbwg;->w:Lcdl;

    invoke-virtual {v4, v0}, Lcdj;->a(Lcdl;)V

    .line 120
    iput-boolean v11, v1, Lbwg;->q:Z

    .line 121
    iget-object v0, v1, Lbwg;->i:Lcct;

    invoke-virtual {v0, v1}, Lcct;->a(Lcdb;)V

    .line 122
    new-instance v0, Lchr;

    .line 123
    invoke-static {v2}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v3

    invoke-virtual {v3}, Lbed;->a()Lbef;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lchr;-><init>(Landroid/content/Context;Lbef;)V

    iput-object v0, v1, Lbwg;->s:Lcdb;

    .line 124
    iget-object v0, v1, Lbwg;->i:Lcct;

    iget-object v3, v1, Lbwg;->s:Lcdb;

    invoke-virtual {v0, v3}, Lcct;->a(Lcdb;)V

    .line 125
    invoke-static {}, Lbxl;->a()Lbxl;

    move-result-object v3

    .line 126
    const-string v0, "VideoPauseController.setUp"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 127
    invoke-static {v1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwg;

    iput-object v0, v3, Lbxl;->a:Lbwg;

    .line 128
    iget-object v0, v3, Lbxl;->a:Lbwg;

    invoke-virtual {v0, v3}, Lbwg;->a(Lbwq;)V

    .line 129
    iget-object v0, v3, Lbxl;->a:Lbwg;

    invoke-virtual {v0, v3}, Lbwg;->a(Lbwt;)V

    .line 130
    iput-object v8, v1, Lbwg;->r:Lawr;

    .line 131
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    const-class v3, Landroid/telephony/TelephonyManager;

    .line 132
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v3, v1, Lbwg;->u:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    .line 133
    invoke-virtual {v0, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 134
    sget-object v0, Lcce;->a:Lcce;

    .line 135
    invoke-virtual {v0, v1}, Lcce;->a(Lccf;)V

    .line 136
    iget-object v0, v1, Lbwg;->y:Lbwv;

    if-nez v0, :cond_3

    .line 137
    new-instance v0, Lbwv;

    invoke-direct {v0, v2}, Lbwv;-><init>(Landroid/content/Context;)V

    iput-object v0, v1, Lbwg;->y:Lbwv;

    .line 138
    iget-object v0, v1, Lbwg;->y:Lbwv;

    invoke-virtual {v1, v0}, Lbwg;->a(Lbwr;)V

    .line 139
    iget-object v0, v1, Lbwg;->y:Lbwv;

    invoke-virtual {v1, v0}, Lbwg;->a(Lbwq;)V

    .line 140
    :cond_3
    invoke-static {}, Lbwg;->a()Lbwg;

    .line 141
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v3

    .line 142
    if-eqz p1, :cond_4

    iget-object v0, v3, Lbwg;->k:Lcom/android/incallui/InCallActivity;

    if-eqz v0, :cond_7

    .line 155
    :cond_4
    :goto_0
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    .line 156
    iput-object p0, v0, Lcdr;->a:Landroid/telecom/InCallService;

    .line 157
    invoke-static {p0}, Lbxc;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 158
    new-instance v0, Lbxc;

    invoke-direct {v0, p0}, Lbxc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->a:Lbxc;

    .line 159
    :cond_5
    invoke-static {p0}, Lbww;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 160
    new-instance v0, Lbww;

    .line 161
    invoke-static {v2}, Lbvp;->a(Landroid/content/Context;)Lbvp;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbww;-><init>(Landroid/content/Context;Lbvp;)V

    iput-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->b:Lbww;

    .line 162
    :cond_6
    invoke-super {p0, p1}, Landroid/telecom/InCallService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    .line 163
    return-object v0

    .line 144
    :cond_7
    const-string v0, "android.telecom.extra.OUTGOING_CALL_EXTRAS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 145
    if-eqz v1, :cond_4

    .line 146
    const-string v0, "selectPhoneAccountAccounts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 147
    const-string v0, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    .line 148
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 149
    const-string v4, "touchPoint"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    .line 150
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v4

    invoke-virtual {v4, v11, v0}, Lbwg;->a(ZLandroid/telecom/PhoneAccountHandle;)V

    .line 151
    iget-object v0, v3, Lbwg;->g:Landroid/content/Context;

    .line 152
    invoke-static {v0, v10, v11, v10}, Lcom/android/incallui/InCallActivity;->a(Landroid/content/Context;ZZZ)Landroid/content/Intent;

    move-result-object v0

    .line 153
    const-string v4, "touchPoint"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 154
    iget-object v1, v3, Lbwg;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBringToForeground(Z)V
    .locals 4

    .prologue
    .line 5
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 6
    const-string v1, "InCallPresenter.onBringToForeground"

    const-string v2, "Bringing UI to foreground."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-virtual {v0, p1}, Lbwg;->c(Z)V

    .line 8
    return-void
.end method

.method public onCallAdded(Landroid/telecom/Call;)V
    .locals 14

    .prologue
    const/16 v4, 0x40

    const/4 v10, 0x0

    .line 9
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 10
    new-instance v5, Lcgu;

    invoke-direct {v5, p1}, Lcgu;-><init>(Landroid/telecom/Call;)V

    .line 12
    invoke-virtual {p1}, Landroid/telecom/Call;->getState()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    move v0, v10

    .line 29
    :goto_0
    if-eqz v0, :cond_6

    .line 31
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 32
    invoke-static {p1}, Lbvs;->b(Landroid/telecom/Call;)Ljava/lang/String;

    move-result-object v7

    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 34
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 35
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 36
    new-instance v4, Lbwj;

    invoke-direct {v4, v1, v2, v5, p1}, Lbwj;-><init>(Lbwg;Ljava/util/concurrent/atomic/AtomicBoolean;Lcgu;Landroid/telecom/Call;)V

    .line 37
    const-wide/16 v12, 0x3e8

    invoke-virtual {v3, v4, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 38
    new-instance v0, Lbwk;

    move-object v6, p1

    invoke-direct/range {v0 .. v9}, Lbwk;-><init>(Lbwg;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/Handler;Ljava/lang/Runnable;Lcgu;Landroid/telecom/Call;Ljava/lang/String;J)V

    .line 39
    iget-object v2, v1, Lbwg;->r:Lawr;

    invoke-virtual {v2, v0, v7, v11}, Lawr;->a(Lawz;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {v1, v10, v0}, Lbwg;->a(ZLandroid/telecom/PhoneAccountHandle;)V

    .line 46
    iget-object v0, v1, Lbwg;->m:Landroid/telecom/Call$Callback;

    invoke-virtual {p1, v0}, Landroid/telecom/Call;->registerCallback(Landroid/telecom/Call$Callback;)V

    .line 47
    return-void

    .line 14
    :cond_0
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 15
    const-string v0, "InCallPresenter.shouldAttemptBlocking"

    const-string v2, "not attempting to block incoming call because user is locked"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v10

    .line 16
    goto :goto_0

    .line 17
    :cond_1
    invoke-static {p1}, Lbvs;->a(Landroid/telecom/Call;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18
    const-string v0, "InCallPresenter.shouldAttemptBlocking"

    const-string v2, "Not attempting to block incoming emergency call"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v10

    .line 19
    goto :goto_0

    .line 20
    :cond_2
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Laxd;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 21
    const-string v0, "InCallPresenter.shouldAttemptBlocking"

    const-string v2, "Not attempting to block incoming call due to recent emergency call"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v10

    .line 22
    goto :goto_0

    .line 23
    :cond_3
    invoke-virtual {p1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v10

    .line 24
    goto :goto_0

    .line 25
    :cond_4
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    invoke-static {v0}, Lapw;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 26
    const-string v0, "InCallPresenter.shouldAttemptBlocking"

    const-string v2, "not attempting to block incoming call because framework blocking is in use"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v10

    .line 27
    goto/16 :goto_0

    .line 28
    :cond_5
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 41
    :cond_6
    invoke-virtual {p1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 42
    iget-object v0, v1, Lbwg;->j:Lcdj;

    invoke-virtual {v0, p1}, Lcdj;->a(Landroid/telecom/Call;)V

    goto :goto_1

    .line 43
    :cond_7
    invoke-virtual {v5}, Lcgu;->a()V

    .line 44
    iget-object v0, v1, Lbwg;->i:Lcct;

    iget-object v2, v1, Lbwg;->g:Landroid/content/Context;

    invoke-virtual {v0, v2, p1, v5}, Lcct;->a(Landroid/content/Context;Landroid/telecom/Call;Lcgu;)V

    goto :goto_1
.end method

.method public onCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lcce;->a:Lcce;

    .line 3
    invoke-virtual {v0, p1}, Lcce;->a(Landroid/telecom/CallAudioState;)V

    .line 4
    return-void
.end method

.method public onCallRemoved(Landroid/telecom/Call;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 48
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v4

    .line 49
    invoke-virtual {p1}, Landroid/telecom/Call;->getDetails()Landroid/telecom/Call$Details;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/telecom/Call$Details;->hasProperty(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, v4, Lbwg;->j:Lcdj;

    invoke-virtual {v0, p1}, Lcdj;->b(Landroid/telecom/Call;)V

    .line 78
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v5, v4, Lbwg;->i:Lcct;

    iget-object v6, v4, Lbwg;->g:Landroid/content/Context;

    .line 52
    iget-object v0, v5, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, v5, Lcct;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 54
    invoke-virtual {v0}, Lcdc;->x()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 55
    invoke-static {v6}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v1

    invoke-virtual {v1}, Lbjd;->a()Lbjf;

    move-result-object v1

    .line 56
    invoke-interface {v1, v0}, Lbjf;->b(Lbjg;)V

    .line 57
    invoke-interface {v1, v0}, Lbjf;->b(Lbjj;)V

    .line 59
    iget-object v1, v0, Lcdc;->g:Lcdf;

    .line 60
    if-eqz v1, :cond_1

    .line 61
    iget-object v1, v0, Lcdc;->g:Lcdf;

    .line 62
    iget-boolean v1, v1, Lcdf;->g:Z

    if-nez v1, :cond_1

    .line 63
    invoke-static {v6}, Lcct;->a(Landroid/content/Context;)Lcdm;

    move-result-object v1

    invoke-interface {v1, v0}, Lcdm;->a(Lcdc;)V

    .line 65
    iget-object v1, v0, Lcdc;->g:Lcdf;

    .line 66
    iput-boolean v2, v1, Lcdf;->g:Z

    .line 67
    :cond_1
    invoke-virtual {v5, v0}, Lcct;->b(Lcdc;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    const-string v2, "CallList.onCallRemoved"

    const-string v6, "Removing call not previously disconnected "

    .line 70
    iget-object v1, v0, Lcdc;->e:Ljava/lang/String;

    .line 71
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v6, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-array v6, v3, [Ljava/lang/Object;

    .line 72
    invoke-static {v2, v1, v6}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :cond_2
    iget-object v0, v0, Lcdc;->j:Lcdg;

    invoke-virtual {v0}, Lcdg;->a()V

    .line 75
    :cond_3
    invoke-virtual {v5}, Lcct;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 76
    sput v3, Lcdc;->a:I

    .line 77
    :cond_4
    iget-object v0, v4, Lbwg;->m:Landroid/telecom/Call$Callback;

    invoke-virtual {p1, v0}, Landroid/telecom/Call;->unregisterCallback(Landroid/telecom/Call$Callback;)V

    goto :goto_0

    :cond_5
    move v1, v3

    .line 54
    goto :goto_1

    .line 71
    :cond_6
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onCanAddCallChanged(Z)V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 80
    iget-object v0, v0, Lbwg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwl;

    .line 81
    invoke-interface {v0}, Lbwl;->a()V

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 164
    invoke-super {p0, p1}, Landroid/telecom/InCallService;->onUnbind(Landroid/content/Intent;)Z

    .line 165
    invoke-static {}, Lbwg;->a()Lbwg;

    .line 166
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lbwg;->a(ZLandroid/telecom/PhoneAccountHandle;)V

    .line 168
    const-string v0, "tearDown"

    invoke-static {p0, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 170
    iget-object v2, v1, Lbwg;->i:Lcct;

    .line 171
    iget-object v0, v2, Lcct;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdc;

    .line 172
    invoke-virtual {v0}, Lcdc;->j()I

    move-result v4

    .line 173
    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    if-eqz v4, :cond_0

    if-eq v4, v8, :cond_0

    .line 174
    invoke-virtual {v0, v8}, Lcdc;->b(I)V

    .line 175
    new-instance v4, Landroid/telecom/DisconnectCause;

    invoke-direct {v4, v6}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v0, v4}, Lcdc;->a(Landroid/telecom/DisconnectCause;)V

    .line 176
    invoke-virtual {v2, v0}, Lcct;->b(Lcdc;)Z

    goto :goto_0

    .line 178
    :cond_1
    invoke-virtual {v2}, Lcct;->n()V

    .line 179
    iput-boolean v6, v1, Lbwg;->q:Z

    .line 180
    iget-object v0, v1, Lbwg;->g:Landroid/content/Context;

    const-class v2, Landroid/telephony/TelephonyManager;

    .line 181
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v2, v1, Lbwg;->u:Landroid/telephony/PhoneStateListener;

    .line 182
    invoke-virtual {v0, v2, v6}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 183
    invoke-virtual {v1}, Lbwg;->g()V

    .line 184
    invoke-static {}, Lbxl;->a()Lbxl;

    move-result-object v0

    .line 185
    const-string v2, "VideoPauseController.tearDown"

    invoke-static {v2}, Lapw;->b(Ljava/lang/String;)V

    .line 186
    iget-object v2, v0, Lbxl;->a:Lbwg;

    invoke-virtual {v2, v0}, Lbwg;->b(Lbwq;)V

    .line 187
    iget-object v2, v0, Lbxl;->a:Lbwg;

    invoke-virtual {v2, v0}, Lbwg;->b(Lbwt;)V

    .line 189
    iput-object v7, v0, Lbxl;->a:Lbwg;

    .line 190
    iput-object v7, v0, Lbxl;->b:Lcdc;

    .line 191
    iput v6, v0, Lbxl;->c:I

    .line 192
    iput-boolean v6, v0, Lbxl;->d:Z

    .line 193
    iput-boolean v6, v0, Lbxl;->e:Z

    .line 194
    sget-object v0, Lcce;->a:Lcce;

    .line 195
    invoke-virtual {v0, v1}, Lcce;->b(Lccf;)V

    .line 196
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    .line 197
    iput-object v7, v0, Lcdr;->a:Landroid/telecom/InCallService;

    .line 198
    iget-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->a:Lbxc;

    if-eqz v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->a:Lbxc;

    .line 200
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbwg;->b(Lbwr;)Z

    .line 201
    sget-object v1, Lcct;->a:Lcct;

    .line 202
    invoke-virtual {v1, v0}, Lcct;->b(Lcdb;)V

    .line 203
    sget-object v1, Lcce;->a:Lcce;

    .line 204
    invoke-virtual {v1, v0}, Lcce;->b(Lccf;)V

    .line 205
    iput-object v7, p0, Lcom/android/incallui/InCallServiceImpl;->a:Lbxc;

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->b:Lbww;

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/android/incallui/InCallServiceImpl;->b:Lbww;

    .line 208
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbwg;->b(Lbwr;)Z

    .line 209
    sget-object v1, Lcct;->a:Lcct;

    .line 210
    invoke-virtual {v1, v0}, Lcct;->b(Lcdb;)V

    .line 211
    sget-object v1, Lcce;->a:Lcce;

    .line 212
    invoke-virtual {v1, v0}, Lcce;->b(Lccf;)V

    .line 213
    iput-object v7, p0, Lcom/android/incallui/InCallServiceImpl;->b:Lbww;

    .line 214
    :cond_3
    return v6
.end method
