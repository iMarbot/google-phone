.class public Lcom/android/incallui/AudioRouteSelectorActivity;
.super Lit;
.source "PG"

# interfaces
.implements Lccg$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lit;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(I)V
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdr;->a(I)V

    .line 10
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->finish()V

    .line 11
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->finish()V

    .line 13
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 3
    sget-object v0, Lcce;->a:Lcce;

    .line 5
    iget-object v0, v0, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 6
    invoke-static {v0}, Lccg;->a(Landroid/telecom/CallAudioState;)Lccg;

    move-result-object v0

    .line 7
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->c()Lja;

    move-result-object v1

    const-string v2, "AudioRouteSelectorDialogFragment"

    invoke-virtual {v0, v1, v2}, Lccg;->a(Lja;Ljava/lang/String;)V

    .line 8
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 14
    invoke-super {p0}, Lit;->onPause()V

    .line 16
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->c()Lja;

    move-result-object v0

    const-string v1, "AudioRouteSelectorDialogFragment"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lccg;

    .line 17
    if-eqz v0, :cond_0

    .line 19
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio;->a(Z)V

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 21
    invoke-virtual {p0}, Lcom/android/incallui/AudioRouteSelectorActivity;->finish()V

    .line 22
    :cond_1
    return-void
.end method
