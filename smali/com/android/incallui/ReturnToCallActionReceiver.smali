.class public Lcom/android/incallui/ReturnToCallActionReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a()Lcdc;
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 73
    iget-object v1, v0, Lbwg;->i:Lcct;

    .line 75
    if-eqz v1, :cond_1

    .line 76
    invoke-virtual {v1}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 78
    invoke-virtual {v1}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 79
    :cond_0
    if-eqz v0, :cond_1

    .line 81
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v0, 0x0

    .line 2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const/4 v5, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 69
    const-string v1, "Invalid intent action: "

    .line 70
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_1
    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 2
    :sswitch_0
    const-string v8, "toggleSpeaker"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v5, v4

    goto :goto_0

    :sswitch_1
    const-string v8, "showAudioRouteSelector"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v5, v3

    goto :goto_0

    :sswitch_2
    const-string v8, "toggleMute"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0

    :sswitch_3
    const-string v8, "endCall"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v5, 0x3

    goto :goto_0

    .line 4
    :pswitch_0
    sget-object v3, Lcce;->a:Lcce;

    .line 6
    iget-object v3, v3, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 8
    invoke-virtual {v3}, Landroid/telecom/CallAudioState;->getSupportedRouteMask()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-ne v5, v6, :cond_1

    .line 9
    const-string v5, "ReturnToCallActionReceiver.toggleSpeaker"

    const-string v6, "toggleSpeaker() called when bluetooth available. Probably should have shown audio route selector"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    :cond_1
    invoke-static {}, Lcom/android/incallui/ReturnToCallActionReceiver;->a()Lcdc;

    move-result-object v4

    .line 11
    invoke-virtual {v3}, Landroid/telecom/CallAudioState;->getRoute()I

    move-result v3

    if-ne v3, v2, :cond_5

    .line 12
    const/4 v2, 0x5

    .line 13
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v5

    sget-object v6, Lbkq$a;->bY:Lbkq$a;

    .line 14
    if-eqz v4, :cond_4

    .line 15
    iget-object v3, v4, Lcdc;->b:Ljava/lang/String;

    .line 17
    :goto_2
    if-eqz v4, :cond_2

    .line 18
    iget-wide v0, v4, Lcdc;->M:J

    .line 20
    :cond_2
    invoke-interface {v5, v6, v3, v0, v1}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    move v0, v2

    .line 30
    :goto_3
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcdr;->a(I)V

    .line 68
    :cond_3
    :goto_4
    return-void

    .line 16
    :cond_4
    const-string v3, ""

    goto :goto_2

    .line 22
    :cond_5
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v5

    sget-object v6, Lbkq$a;->bX:Lbkq$a;

    .line 23
    if-eqz v4, :cond_7

    .line 24
    iget-object v3, v4, Lcdc;->b:Ljava/lang/String;

    .line 26
    :goto_5
    if-eqz v4, :cond_6

    .line 27
    iget-wide v0, v4, Lcdc;->M:J

    .line 29
    :cond_6
    invoke-interface {v5, v6, v3, v0, v1}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    move v0, v2

    goto :goto_3

    .line 25
    :cond_7
    const-string v3, ""

    goto :goto_5

    .line 33
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/incallui/AudioRouteSelectorActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    const/high16 v1, 0x18000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 35
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 38
    :pswitch_2
    invoke-static {}, Lcom/android/incallui/ReturnToCallActionReceiver;->a()Lcdc;

    move-result-object v5

    .line 39
    sget-object v2, Lcce;->a:Lcce;

    .line 41
    iget-object v2, v2, Lcce;->b:Landroid/telecom/CallAudioState;

    .line 42
    invoke-virtual {v2}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v3

    .line 43
    :goto_6
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v6

    .line 44
    if-eqz v2, :cond_a

    .line 45
    sget-object v3, Lbkq$a;->bZ:Lbkq$a;

    move-object v4, v3

    .line 47
    :goto_7
    if-eqz v5, :cond_b

    .line 48
    iget-object v3, v5, Lcdc;->b:Ljava/lang/String;

    .line 50
    :goto_8
    if-eqz v5, :cond_8

    .line 51
    iget-wide v0, v5, Lcdc;->M:J

    .line 53
    :cond_8
    invoke-interface {v6, v4, v3, v0, v1}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 54
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcdr;->a(Z)V

    goto :goto_4

    :cond_9
    move v2, v4

    .line 42
    goto :goto_6

    .line 46
    :cond_a
    sget-object v3, Lbkq$a;->ca:Lbkq$a;

    move-object v4, v3

    goto :goto_7

    .line 49
    :cond_b
    const-string v3, ""

    goto :goto_8

    .line 57
    :pswitch_3
    invoke-static {}, Lcom/android/incallui/ReturnToCallActionReceiver;->a()Lcdc;

    move-result-object v3

    .line 58
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v4

    sget-object v5, Lbkq$a;->cb:Lbkq$a;

    .line 59
    if-eqz v3, :cond_d

    .line 60
    iget-object v2, v3, Lcdc;->b:Ljava/lang/String;

    .line 62
    :goto_9
    if-eqz v3, :cond_c

    .line 63
    iget-wide v0, v3, Lcdc;->M:J

    .line 65
    :cond_c
    invoke-interface {v4, v5, v2, v0, v1}, Lbku;->a(Lbkq$a;Ljava/lang/String;J)V

    .line 66
    if-eqz v3, :cond_3

    .line 67
    invoke-virtual {v3}, Lcdc;->B()V

    goto :goto_4

    .line 61
    :cond_d
    const-string v2, ""

    goto :goto_9

    .line 70
    :cond_e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2
    nop

    :sswitch_data_0
    .sparse-switch
        -0x78ed9d11 -> :sswitch_1
        -0x5fd46e27 -> :sswitch_3
        -0x32d24873 -> :sswitch_2
        0xf2c960b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
