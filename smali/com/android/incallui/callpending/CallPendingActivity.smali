.class public Lcom/android/incallui/callpending/CallPendingActivity;
.super Lit;
.source "PG"

# interfaces
.implements Lcgl;
.implements Lcgo;


# instance fields
.field private f:Landroid/content/BroadcastReceiver;

.field private g:Lcgk;

.field private h:Lcgn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lit;-><init>()V

    .line 2
    new-instance v0, Lceo;

    invoke-direct {v0, p0}, Lceo;-><init>(Lcom/android/incallui/callpending/CallPendingActivity;)V

    iput-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->f:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private final e()Lcgq;
    .locals 21

    .prologue
    .line 29
    .line 30
    invoke-static/range {p0 .. p0}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v2

    invoke-virtual {v2}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 31
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_session_id"

    const-wide/16 v6, -0x1

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 32
    invoke-interface {v2, v4, v5}, Lbjf;->b(J)Lbjl;

    move-result-object v2

    .line 33
    if-nez v2, :cond_0

    .line 34
    const-string v2, "CallPendingActivity.createPrimaryInfo"

    const-string v3, "Null session."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    const/16 v18, 0x0

    .line 37
    :goto_0
    const/4 v8, 0x0

    .line 39
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_photo_uri"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 41
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    .line 42
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 47
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_number"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 52
    new-instance v2, Lcgq;

    if-eqz v4, :cond_1

    .line 53
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_2
    const/4 v6, 0x0

    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v9, "extra_label"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 56
    const/4 v9, 0x2

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v19, "extra_lookup_key"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 59
    const/16 v19, 0x0

    const/16 v20, 0x1

    invoke-direct/range {v2 .. v20}, Lcgq;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;IZZZZZZZLjava/lang/String;Lbln;ZI)V

    .line 60
    return-object v2

    .line 36
    :cond_0
    invoke-interface {v2}, Lbjl;->d()Lbln;

    move-result-object v18

    goto :goto_0

    .line 44
    :catch_0
    move-exception v2

    .line 45
    const-string v3, "CallPendingActivity.createPrimaryInfo"

    const-string v4, "Contact photo not found"

    invoke-static {v3, v4, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 53
    :cond_1
    const/4 v5, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final g()Lcgn;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->h:Lcgn;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->h:Lcgn;

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lceq;

    invoke-direct {v0, p0}, Lceq;-><init>(Lcom/android/incallui/callpending/CallPendingActivity;)V

    iput-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->h:Lcgn;

    goto :goto_0
.end method

.method public final h()Lcgk;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->g:Lcgk;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->g:Lcgk;

    .line 63
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcep;

    invoke-direct {v0, p0}, Lcep;-><init>(Lcom/android/incallui/callpending/CallPendingActivity;)V

    iput-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->g:Lcgk;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 3
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 4
    const v0, 0x7f0400a1

    invoke-virtual {p0, v0}, Lcom/android/incallui/callpending/CallPendingActivity;->setContentView(I)V

    .line 5
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->f:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "dialer.intent.action.CALL_PENDING_ACTIVITY_FINISH"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/callpending/CallPendingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 6
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lit;->onDestroy()V

    .line 27
    iget-object v0, p0, Lcom/android/incallui/callpending/CallPendingActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/incallui/callpending/CallPendingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 28
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 14
    invoke-super {p0}, Lit;->onResume()V

    .line 17
    invoke-virtual {p0}, Lcom/android/incallui/callpending/CallPendingActivity;->c()Lja;

    move-result-object v0

    const-string v1, "tag_in_call_screen"

    invoke-virtual {v0, v1}, Lja;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    check-cast v0, Lcgm;

    .line 18
    invoke-direct {p0}, Lcom/android/incallui/callpending/CallPendingActivity;->e()Lcgq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcgm;->a(Lcgq;)V

    .line 19
    const/16 v1, 0x10

    .line 21
    invoke-virtual {p0}, Lcom/android/incallui/callpending/CallPendingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_call_pending_label"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 22
    invoke-static {v1, v2}, Lcgp;->a(ILjava/lang/String;)Lcgp;

    move-result-object v1

    .line 23
    invoke-interface {v0, v1}, Lcgm;->a(Lcgp;)V

    .line 24
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcgm;->a(Z)V

    .line 25
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 7
    invoke-super {p0}, Lit;->onStart()V

    .line 8
    invoke-static {}, Lbvs;->a()Lcgm;

    move-result-object v0

    .line 9
    invoke-virtual {p0}, Lcom/android/incallui/callpending/CallPendingActivity;->c()Lja;

    move-result-object v1

    .line 10
    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    const v2, 0x7f0e01f2

    .line 11
    check-cast v0, Lip;

    const-string v3, "tag_in_call_screen"

    invoke-virtual {v1, v2, v0, v3}, Ljy;->a(ILip;Ljava/lang/String;)Ljy;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Ljy;->a()I

    .line 13
    return-void
.end method
