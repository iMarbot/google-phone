.class public Lcom/android/incallui/NotificationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 70
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 72
    if-nez v0, :cond_1

    .line 73
    invoke-static {}, Lbxd;->a()V

    .line 74
    const-string v0, "NotificationBroadcastReceiver.answerIncomingCall"

    const-string v1, "call list is empty"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v0, p0}, Lcdc;->e(I)V

    .line 78
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 79
    invoke-virtual {v0, v2, v2}, Lbwg;->b(ZZ)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 3
    const-string v2, "NotificationBroadcastReceiver.onReceive"

    const-string v3, "Broadcast from Notification: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    const-string v0, "com.android.incallui.ACTION_ANSWER_VIDEO_INCOMING_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/android/incallui/NotificationBroadcastReceiver;->a(I)V

    .line 68
    :cond_0
    :goto_1
    return-void

    .line 3
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 6
    :cond_2
    const-string v0, "com.android.incallui.ACTION_ANSWER_VOICE_INCOMING_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7
    invoke-static {v5}, Lcom/android/incallui/NotificationBroadcastReceiver;->a(I)V

    goto :goto_1

    .line 8
    :cond_3
    const-string v0, "com.android.incallui.ACTION_DECLINE_INCOMING_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 9
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->P:Lbkq$a;

    .line 10
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 11
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 12
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 14
    if-nez v0, :cond_4

    .line 15
    invoke-static {}, Lbxd;->a()V

    .line 16
    const-string v0, "NotificationBroadcastReceiver.declineIncomingCall"

    const-string v1, "call list is empty"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 17
    :cond_4
    invoke-virtual {v0}, Lcct;->i()Lcdc;

    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Lcdc;->a(ZLjava/lang/String;)V

    goto :goto_1

    .line 21
    :cond_5
    const-string v0, "com.android.incallui.ACTION_HANG_UP_ONGOING_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 22
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 23
    iget-object v1, v0, Lbwg;->i:Lcct;

    .line 25
    if-nez v1, :cond_6

    .line 26
    invoke-static {}, Lbxd;->a()V

    .line 27
    const-string v0, "NotificationBroadcastReceiver.hangUpOngoingCall"

    const-string v1, "call list is empty"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 28
    :cond_6
    invoke-virtual {v1}, Lcct;->c()Lcdc;

    move-result-object v0

    .line 29
    if-nez v0, :cond_7

    .line 30
    invoke-virtual {v1}, Lcct;->h()Lcdc;

    move-result-object v0

    .line 31
    :cond_7
    const-string v1, "NotificationBroadcastReceiver.hangUpOngoingCall"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "disconnecting call, call: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v0}, Lcdc;->B()V

    goto/16 :goto_1

    .line 35
    :cond_8
    const-string v0, "com.android.incallui.ACTION_ACCEPT_VIDEO_UPGRADE_REQUEST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 37
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 38
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 40
    if-nez v0, :cond_9

    .line 41
    invoke-static {}, Lbxd;->a()V

    .line 42
    const-string v0, "NotificationBroadcastReceiver.acceptUpgradeRequest"

    const-string v1, "call list is empty"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 43
    :cond_9
    invoke-virtual {v0}, Lcct;->l()Lcdc;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcjs;->d(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 47
    :cond_a
    const-string v0, "com.android.incallui.ACTION_DECLINE_VIDEO_UPGRADE_REQUEST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 48
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 49
    iget-object v0, v0, Lbwg;->i:Lcct;

    .line 51
    if-nez v0, :cond_b

    .line 52
    invoke-static {}, Lbxd;->a()V

    .line 53
    const-string v0, "NotificationBroadcastReceiver.declineUpgradeRequest"

    const-string v1, "call list is empty"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 54
    :cond_b
    invoke-virtual {v0}, Lcct;->l()Lcdc;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Lcdc;->F()Lcjs;

    move-result-object v0

    invoke-interface {v0}, Lcjs;->i()V

    goto/16 :goto_1

    .line 58
    :cond_c
    const-string v0, "com.android.incallui.ACTION_PULL_EXTERNAL_CALL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 60
    const-string v0, "com.android.incallui.extra.EXTRA_NOTIFICATION_ID"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 61
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 62
    iget-object v1, v1, Lbwg;->e:Lcaj;

    .line 63
    invoke-virtual {v1, v0}, Lcaj;->a(I)V

    goto/16 :goto_1

    .line 64
    :cond_d
    const-string v0, "com.android.incallui.ACTION_TURN_ON_SPEAKER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 65
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcdr;->a(I)V

    goto/16 :goto_1

    .line 66
    :cond_e
    const-string v0, "com.android.incallui.ACTION_TURN_OFF_SPEAKER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {}, Lcdr;->a()Lcdr;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcdr;->a(I)V

    goto/16 :goto_1
.end method
