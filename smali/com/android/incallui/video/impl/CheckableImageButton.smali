.class public Lcom/android/incallui/video/impl/CheckableImageButton;
.super Landroid/widget/ImageButton;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/video/impl/CheckableImageButton$b;,
        Lcom/android/incallui/video/impl/CheckableImageButton$a;
    }
.end annotation


# static fields
.field private static b:[I


# instance fields
.field public a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/android/incallui/video/impl/CheckableImageButton;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 7
    sget-object v0, Lcik;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 8
    sget v1, Lcik;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->setChecked(Z)V

    .line 9
    sget v1, Lcik;->c:I

    .line 10
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->e:Ljava/lang/CharSequence;

    .line 11
    sget v1, Lcik;->d:I

    .line 12
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->f:Ljava/lang/CharSequence;

    .line 13
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 14
    invoke-direct {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->a()Ljava/lang/CharSequence;

    .line 15
    invoke-virtual {p0, v3}, Lcom/android/incallui/video/impl/CheckableImageButton;->setClickable(Z)V

    .line 16
    invoke-virtual {p0, v3}, Lcom/android/incallui/video/impl/CheckableImageButton;->setFocusable(Z)V

    .line 17
    return-void
.end method

.method private final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->e:Ljava/lang/CharSequence;

    .line 28
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 29
    return-object v0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private final a(Z)V
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 26
    :goto_0
    return-void

    .line 22
    :cond_0
    iput-boolean p1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->d:Z

    .line 23
    invoke-direct {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 24
    invoke-virtual {p0, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 25
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->refreshDrawableState()V

    goto :goto_0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0}, Landroid/widget/ImageButton;->drawableStateChanged()V

    .line 44
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->invalidate()V

    .line 45
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->d:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 39
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageButton;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    sget-object v1, Lcom/android/incallui/video/impl/CheckableImageButton;->b:[I

    invoke-static {v0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->mergeDrawableStates([I[I)[I

    .line 42
    :cond_0
    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 55
    check-cast p1, Lcom/android/incallui/video/impl/CheckableImageButton$b;

    .line 56
    invoke-virtual {p1}, Lcom/android/incallui/video/impl/CheckableImageButton$b;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ImageButton;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 57
    iget-boolean v0, p1, Lcom/android/incallui/video/impl/CheckableImageButton$b;->a:Z

    invoke-direct {p0, v0}, Lcom/android/incallui/video/impl/CheckableImageButton;->a(Z)V

    .line 58
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->requestLayout()V

    .line 59
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lcom/android/incallui/video/impl/CheckableImageButton$b;

    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v1

    invoke-super {p0}, Landroid/widget/ImageButton;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 61
    invoke-direct {v0, v1, v2}, Lcom/android/incallui/video/impl/CheckableImageButton$b;-><init>(ZLandroid/os/Parcelable;)V

    .line 62
    return-object v0
.end method

.method public performClick()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    .line 47
    iget-object v0, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 48
    :goto_0
    if-nez v0, :cond_2

    .line 49
    invoke-super {p0}, Landroid/widget/ImageButton;->performClick()Z

    move-result v0

    .line 54
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 47
    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->toggle()V

    .line 51
    invoke-super {p0}, Landroid/widget/ImageButton;->performClick()Z

    move-result v0

    .line 52
    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p0, v1}, Lcom/android/incallui/video/impl/CheckableImageButton;->playSoundEffect(I)V

    goto :goto_1
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/android/incallui/video/impl/CheckableImageButton;->a(Z)V

    .line 19
    return-void
.end method

.method public toggle()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 32
    :goto_0
    invoke-virtual {p0}, Lcom/android/incallui/video/impl/CheckableImageButton;->isChecked()Z

    move-result v3

    if-eq v3, v0, :cond_1

    .line 33
    iget-boolean v3, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->c:Z

    if-nez v3, :cond_1

    .line 34
    iput-boolean v1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->c:Z

    .line 35
    iget-object v1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->a:Lcom/android/incallui/video/impl/CheckableImageButton$a;

    invoke-interface {v1, p0, v0}, Lcom/android/incallui/video/impl/CheckableImageButton$a;->a(Lcom/android/incallui/video/impl/CheckableImageButton;Z)V

    .line 37
    :cond_0
    iput-boolean v2, p0, Lcom/android/incallui/video/impl/CheckableImageButton;->c:Z

    .line 38
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 31
    goto :goto_0
.end method
