.class public Lcom/android/incallui/incall/impl/InCallPaginator;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Path;

.field private f:Landroid/animation/ValueAnimator;

.field private g:Z

.field private h:F

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0, p1}, Lcom/android/incallui/incall/impl/InCallPaginator;->a(Landroid/content/Context;)V

    .line 3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5
    invoke-direct {p0, p1}, Lcom/android/incallui/incall/impl/InCallPaginator;->a(Landroid/content/Context;)V

    .line 6
    return-void
.end method

.method private final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 7
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/InCallPaginator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d019b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    .line 8
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/InCallPaginator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d019c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->b:I

    .line 9
    const v0, 0x7f0c00b0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    .line 10
    const v1, 0x7f0c00b1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    .line 11
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->c:Landroid/graphics/Paint;

    .line 12
    iget-object v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->c:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 13
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->d:Landroid/graphics/Paint;

    .line 14
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    .line 17
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 18
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setCurrentFraction(F)V

    .line 19
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    new-instance v1, Lcgc;

    invoke-direct {v1, p0}, Lcgc;-><init>(Lcom/android/incallui/incall/impl/InCallPaginator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 20
    return-void

    .line 16
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    packed-switch p1, :pswitch_data_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 60
    :pswitch_0
    iget-boolean v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->j:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 61
    :goto_1
    iget-object v3, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 62
    if-nez v0, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->g:Z

    .line 63
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 64
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 65
    :cond_1
    iput-boolean v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->j:Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 60
    goto :goto_1

    :cond_3
    move v1, v2

    .line 62
    goto :goto_2

    .line 68
    :pswitch_1
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->h:F

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setCurrentFraction(F)V

    .line 70
    iput-boolean v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->g:Z

    .line 71
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 72
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 50
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 51
    :goto_0
    iput p2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->h:F

    .line 52
    iput-boolean v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->i:Z

    .line 53
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2}, Landroid/animation/ValueAnimator;->setCurrentFraction(F)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/InCallPaginator;->invalidate()V

    .line 56
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->j:Z

    .line 58
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v12, 0x40000000    # 2.0f

    .line 21
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 22
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/InCallPaginator;->getWidth()I

    move-result v0

    div-int/lit8 v8, v0, 0x2

    .line 23
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/InCallPaginator;->getHeight()I

    move-result v0

    div-int/lit8 v9, v0, 0x2

    .line 24
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 25
    iget-object v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 26
    iget-boolean v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->g:Z

    if-eqz v1, :cond_1

    .line 27
    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->b:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v0

    add-float v4, v1, v2

    .line 28
    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float/2addr v0, v12

    sub-float v0, v13, v0

    mul-float v10, v1, v0

    .line 29
    iget v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->b:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v11, v0

    .line 30
    iget-boolean v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->i:Z

    if-eqz v0, :cond_0

    .line 31
    int-to-float v0, v8

    sub-float/2addr v0, v11

    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v1, v1

    sub-float v1, v0, v1

    .line 32
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    sub-int v2, v9, v2

    int-to-float v2, v2

    add-float v3, v1, v4

    iget v4, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    add-int/2addr v4, v9

    int-to-float v4, v4

    iget v5, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v6, v6

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Path;->addRoundRect(FFFFFFLandroid/graphics/Path$Direction;)V

    .line 33
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    int-to-float v1, v8

    add-float/2addr v1, v11

    int-to-float v2, v9

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v10, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 44
    :goto_0
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->d:Landroid/graphics/Paint;

    .line 45
    iget-object v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 46
    iget-boolean v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->i:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->h:F

    mul-float/2addr v0, v12

    sub-float v0, v13, v0

    :goto_1
    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->b:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 47
    iget-object v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->c:Landroid/graphics/Paint;

    .line 48
    int-to-float v2, v8

    add-float/2addr v0, v2

    int-to-float v2, v9

    iget v3, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 49
    return-void

    .line 35
    :cond_0
    int-to-float v0, v8

    add-float/2addr v0, v11

    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v1, v1

    add-float v3, v0, v1

    .line 36
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    sub-float v1, v3, v4

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    sub-int v2, v9, v2

    int-to-float v2, v2

    iget v4, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    add-int/2addr v4, v9

    int-to-float v4, v4

    iget v5, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v6, v6

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Path;->addRoundRect(FFFFFFLandroid/graphics/Path$Direction;)V

    .line 37
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    int-to-float v1, v8

    sub-float/2addr v1, v11

    int-to-float v2, v9

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v10, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    goto :goto_0

    .line 39
    :cond_1
    iget v1, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->b:I

    int-to-float v1, v1

    div-float/2addr v1, v12

    .line 40
    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v2, v2

    add-float/2addr v2, v1

    mul-float/2addr v0, v2

    sub-float v10, v1, v0

    .line 41
    iget v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v0, v0

    mul-float/2addr v0, v12

    add-float v11, v0, v1

    .line 42
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    int-to-float v1, v8

    sub-float/2addr v1, v11

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    sub-int v2, v9, v2

    int-to-float v2, v2

    int-to-float v3, v8

    sub-float/2addr v3, v10

    iget v4, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    add-int/2addr v4, v9

    int-to-float v4, v4

    iget v5, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v6, v6

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Path;->addRoundRect(FFFFFFLandroid/graphics/Path$Direction;)V

    .line 43
    iget-object v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->e:Landroid/graphics/Path;

    int-to-float v1, v8

    add-float/2addr v1, v10

    iget v2, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    sub-int v2, v9, v2

    int-to-float v2, v2

    int-to-float v3, v8

    add-float/2addr v3, v11

    iget v4, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    add-int/2addr v4, v9

    int-to-float v4, v4

    iget v5, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->a:I

    int-to-float v6, v6

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Path;->addRoundRect(FFFFFFLandroid/graphics/Path$Direction;)V

    goto/16 :goto_0

    .line 46
    :cond_2
    iget v0, p0, Lcom/android/incallui/incall/impl/InCallPaginator;->h:F

    mul-float/2addr v0, v12

    sub-float/2addr v0, v13

    goto/16 :goto_1
.end method
