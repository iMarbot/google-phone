.class public Lcom/android/incallui/incall/impl/CheckableLabeledButton;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;,
        Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;
    }
.end annotation


# static fields
.field private static c:[I


# instance fields
.field public a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

.field public b:Landroid/widget/ImageView;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Landroid/widget/TextView;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10

    .prologue
    const v9, 0x7f020105

    const/4 v8, -0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->f:I

    .line 4
    invoke-virtual {p0, v6}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOrientation(I)V

    .line 5
    invoke-virtual {p0, v6}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setGravity(I)V

    .line 6
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020107

    invoke-virtual {v0, v1, v7}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->i:Landroid/graphics/drawable/Drawable;

    .line 7
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9, v7}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->h:Landroid/graphics/drawable/Drawable;

    .line 8
    sget-object v0, Lcgi;->a:[I

    .line 9
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 10
    sget v1, Lcgi;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 11
    sget v2, Lcgi;->d:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 12
    sget v3, Lcgi;->b:I

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 13
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 14
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d0166

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 15
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setPadding(IIII)V

    .line 16
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d0171

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 17
    new-instance v4, Landroid/widget/ImageView;

    const v5, 0x10302d3

    invoke-direct {v4, p1, v7, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v4, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    .line 18
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v4

    .line 19
    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 20
    iput v0, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 21
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 23
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 24
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 25
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0c010b

    invoke-virtual {v1, v4, v7}, Landroid/content/res/Resources;->getColorStateList(ILandroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 26
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9, v7}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 27
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 28
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d0164

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setElevation(F)V

    .line 29
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    const v1, 0x7f060006

    .line 30
    invoke-static {p1, v1}, Landroid/animation/AnimatorInflater;->loadStateListAnimator(Landroid/content/Context;I)Landroid/animation/StateListAnimator;

    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 32
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->addView(Landroid/view/View;)V

    .line 33
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    .line 34
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    .line 35
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 36
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d0165

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 39
    iget-object v1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    const v1, 0x7f1200c2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 41
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 43
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 44
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 45
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 46
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setDuplicateParentStateEnabled(Z)V

    .line 47
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->addView(Landroid/view/View;)V

    .line 48
    invoke-virtual {p0, v6}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setFocusable(Z)V

    .line 49
    invoke-virtual {p0, v6}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setClickable(Z)V

    .line 50
    invoke-virtual {p0, v3}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setEnabled(Z)V

    .line 51
    invoke-virtual {p0, v7}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 52
    return-void
.end method

.method private final b(Z)V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isChecked()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    iput-boolean p1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->e:Z

    .line 105
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->refreshDrawableState()V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->f:I

    if-eq v0, p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    iput p1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->f:I

    .line 62
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->i:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 64
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 83
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->invalidate()V

    .line 84
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->e:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 78
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 79
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    sget-object v1, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->c:[I

    invoke-static {v0, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->mergeDrawableStates([I[I)[I

    .line 81
    :cond_0
    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 94
    check-cast p1, Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;

    .line 95
    invoke-virtual {p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 96
    iget-boolean v0, p1, Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;->a:Z

    invoke-direct {p0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->requestLayout()V

    .line 98
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 99
    new-instance v0, Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isChecked()Z

    move-result v1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 100
    invoke-direct {v0, v1, v2}, Lcom/android/incallui/incall/impl/CheckableLabeledButton$b;-><init>(ZLandroid/os/Parcelable;)V

    .line 101
    return-object v0
.end method

.method public performClick()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    .line 86
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 87
    :goto_0
    if-nez v0, :cond_2

    .line 88
    invoke-super {p0}, Landroid/widget/LinearLayout;->performClick()Z

    move-result v0

    .line 93
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 86
    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->toggle()V

    .line 90
    invoke-super {p0}, Landroid/widget/LinearLayout;->performClick()Z

    move-result v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0, v1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->playSoundEffect(I)V

    goto :goto_1
.end method

.method public refreshDrawableState()V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->refreshDrawableState()V

    .line 56
    iget-object v3, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 57
    iget-object v0, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0

    :cond_1
    move v1, v2

    .line 57
    goto :goto_1
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b(Z)V

    .line 69
    return-void
.end method

.method public toggle()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->isChecked()Z

    move-result v3

    if-eq v3, v0, :cond_1

    .line 72
    iget-boolean v3, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->d:Z

    if-nez v3, :cond_1

    .line 73
    iput-boolean v1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->d:Z

    .line 74
    iget-object v1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->a:Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;

    invoke-interface {v1, p0, v0}, Lcom/android/incallui/incall/impl/CheckableLabeledButton$a;->a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;Z)V

    .line 76
    :cond_0
    iput-boolean v2, p0, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->d:Z

    .line 77
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 70
    goto :goto_0
.end method
