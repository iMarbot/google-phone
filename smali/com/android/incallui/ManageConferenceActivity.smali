.class public Lcom/android/incallui/ManageConferenceActivity;
.super Luh;
.source "PG"


# instance fields
.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Luh;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbwg;->c(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/android/incallui/ManageConferenceActivity;->finish()V

    .line 29
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f0e00d2

    .line 2
    invoke-super {p0, p1}, Luh;->onCreate(Landroid/os/Bundle;)V

    .line 3
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    .line 4
    iput-object p0, v0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    .line 6
    invoke-virtual {p0}, Luh;->e()Luj;

    move-result-object v0

    invoke-virtual {v0}, Luj;->a()Ltv;

    move-result-object v0

    .line 7
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ltv;->b(Z)V

    .line 8
    const v0, 0x7f04001c

    invoke-virtual {p0, v0}, Lcom/android/incallui/ManageConferenceActivity;->setContentView(I)V

    .line 9
    invoke-virtual {p0}, Lcom/android/incallui/ManageConferenceActivity;->c()Lja;

    move-result-object v0

    invoke-virtual {v0, v2}, Lja;->a(I)Lip;

    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    new-instance v0, Lbvg;

    invoke-direct {v0}, Lbvg;-><init>()V

    .line 12
    invoke-virtual {p0}, Lcom/android/incallui/ManageConferenceActivity;->c()Lja;

    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lja;->a()Ljy;

    move-result-object v1

    .line 14
    invoke-virtual {v1, v2, v0}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Ljy;->a()I

    .line 16
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0}, Luh;->onDestroy()V

    .line 18
    invoke-virtual {p0}, Lcom/android/incallui/ManageConferenceActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x0

    .line 20
    iput-object v1, v0, Lbwg;->l:Lcom/android/incallui/ManageConferenceActivity;

    .line 21
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 22
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 23
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/android/incallui/ManageConferenceActivity;->onBackPressed()V

    .line 25
    const/4 v0, 0x1

    .line 26
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Luh;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Luh;->onStart()V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/ManageConferenceActivity;->f:Z

    .line 32
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Luh;->onStop()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/ManageConferenceActivity;->f:Z

    .line 35
    return-void
.end method
