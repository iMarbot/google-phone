.class public Lcom/android/newbubble/NewCheckableButton;
.super Lyz;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/newbubble/NewCheckableButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const v0, 0x1010072

    invoke-direct {p0, p1, p2, v0}, Lcom/android/newbubble/NewCheckableButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0, p1, p2, p3}, Lyz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    new-instance v0, Lcle;

    invoke-direct {v0, p0}, Lcle;-><init>(Lcom/android/newbubble/NewCheckableButton;)V

    invoke-static {p0, v0}, Lqy;->a(Landroid/view/View;Lqa;)V

    .line 7
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/android/newbubble/NewCheckableButton;->b:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 8
    iget-boolean v0, p0, Lcom/android/newbubble/NewCheckableButton;->b:Z

    if-eq v0, p1, :cond_0

    .line 9
    iput-boolean p1, p0, Lcom/android/newbubble/NewCheckableButton;->b:Z

    .line 10
    if-eqz p1, :cond_1

    .line 11
    invoke-virtual {p0}, Lcom/android/newbubble/NewCheckableButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0027

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    .line 13
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/newbubble/NewCheckableButton;->setTextColor(I)V

    .line 14
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/newbubble/NewCheckableButton;->setCompoundDrawableTintList(Landroid/content/res/ColorStateList;)V

    .line 15
    :cond_0
    return-void

    .line 12
    :cond_1
    invoke-virtual {p0}, Lcom/android/newbubble/NewCheckableButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/android/newbubble/NewCheckableButton;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/newbubble/NewCheckableButton;->setChecked(Z)V

    .line 18
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
