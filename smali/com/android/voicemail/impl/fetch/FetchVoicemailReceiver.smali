.class public Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver$a;
    }
.end annotation


# static fields
.field private static g:[Ljava/lang/String;


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Lcqk;

.field public c:Landroid/content/Context;

.field public d:Ljava/lang/String;

.field public e:Landroid/telecom/PhoneAccountHandle;

.field public f:I

.field private h:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "source_data"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subscription_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "subscription_component_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->f:I

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccountHandle;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 57
    invoke-static {}, Lbw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v4

    .line 69
    :goto_0
    return-object v0

    .line 59
    :cond_0
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 60
    invoke-virtual {v0}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 61
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 62
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-nez v6, :cond_2

    .line 63
    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 66
    :goto_2
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 64
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object v1, v3

    .line 65
    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 69
    goto :goto_0
.end method

.method static synthetic a(Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;Landroid/net/Network;Lcnw;)V
    .locals 2

    .prologue
    .line 70
    .line 71
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 72
    new-instance v1, Lcmg;

    invoke-direct {v1, p0, p1, p2}, Lcmg;-><init>(Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;Landroid/net/Network;Lcnw;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 73
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 3
    invoke-static {p1}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    invoke-interface {v0}, Lcln;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 5
    :cond_1
    const-string v0, "android.intent.action.FETCH_VOICEMAIL"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    const-string v0, "FetchVoicemailReceiver"

    const-string v1, "ACTION_FETCH_VOICEMAIL received"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    iput-object p1, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->c:Landroid/content/Context;

    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->h:Landroid/content/ContentResolver;

    .line 9
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a:Landroid/net/Uri;

    .line 10
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 11
    const-string v0, "FetchVoicemailReceiver"

    const-string v1, "android.intent.action.FETCH_VOICEMAIL intent sent with no data"

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 14
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a:Landroid/net/Uri;

    const-string v2, "source_package"

    .line 15
    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 16
    const-string v1, "FetchVoicemailReceiver"

    const-string v2, "ACTION_FETCH_VOICEMAIL from foreign pacakge "

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 18
    :cond_4
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->h:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->g:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 19
    if-nez v1, :cond_5

    .line 20
    const-string v0, "FetchVoicemailReceiver"

    const-string v1, "ACTION_FETCH_VOICEMAIL query returned null"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 22
    :cond_5
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 23
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->d:Ljava/lang/String;

    .line 24
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 26
    const-string v0, "phone"

    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 28
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    .line 29
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 30
    const-string v0, "FetchVoicemailReceiver"

    const-string v2, "Account null and no default sim found."

    invoke-static {v0, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 33
    :cond_6
    :try_start_1
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    const/4 v2, 0x2

    .line 34
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    const/4 v3, 0x1

    .line 35
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    .line 36
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 37
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    .line 38
    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 39
    if-nez v0, :cond_7

    .line 40
    const-string v0, "FetchVoicemailReceiver"

    const-string v2, "account no longer valid, cannot retrieve message"

    invoke-static {v0, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 41
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 43
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    invoke-static {p1, v0}, Lcqe;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 44
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    invoke-static {p1, v0}, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    .line 45
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    if-nez v0, :cond_8

    .line 46
    const-string v0, "FetchVoicemailReceiver"

    const-string v2, "Account not registered - cannot retrieve message."

    invoke-static {v0, v2}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 47
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 49
    :cond_8
    :try_start_3
    const-string v0, "FetchVoicemailReceiver"

    const-string v2, "Fetching voicemail with Marshmallow PhoneAccountHandle"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_9
    const-string v0, "FetchVoicemailReceiver"

    const-string v2, "Requesting network to fetch voicemail"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver$a;

    iget-object v2, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->e:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, p0, p1, v2}, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver$a;-><init>(Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    iput-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    .line 52
    iget-object v0, p0, Lcom/android/voicemail/impl/fetch/FetchVoicemailReceiver;->b:Lcqk;

    invoke-virtual {v0}, Lcqk;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 53
    :cond_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 55
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method
