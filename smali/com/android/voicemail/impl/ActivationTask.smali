.class public Lcom/android/voicemail/impl/ActivationTask;
.super Lcom/android/voicemail/impl/scheduling/BaseTask;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation

.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation


# instance fields
.field private f:Lcpg;

.field private g:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;-><init>(I)V

    .line 2
    new-instance v0, Lcpg;

    const/4 v1, 0x4

    const/16 v2, 0x1388

    invoke-direct {v0, v1, v2}, Lcpg;-><init>(II)V

    iput-object v0, p0, Lcom/android/voicemail/impl/ActivationTask;->f:Lcpg;

    .line 3
    iget-object v0, p0, Lcom/android/voicemail/impl/ActivationTask;->f:Lcpg;

    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/ActivationTask;->a(Lcpe;)Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 4
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5
    .line 7
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_provisioned"

    .line 8
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 9
    :goto_0
    if-nez v0, :cond_1

    .line 10
    const-string v0, "VvmActivationTask"

    const-string v1, "Activation requested while device is not provisioned, postponing"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 13
    const-string v0, "EXTRA_PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 14
    const-class v0, Landroid/app/job/JobScheduler;

    .line 15
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 16
    invoke-static {p0}, Lcom/android/voicemail/impl/DeviceProvisionedJobService;->a(Landroid/content/Context;)Landroid/app/job/JobInfo;

    move-result-object v2

    new-instance v3, Landroid/app/job/JobWorkItem;

    invoke-direct {v3, v1}, Landroid/app/job/JobWorkItem;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/job/JobScheduler;->enqueue(Landroid/app/job/JobInfo;Landroid/app/job/JobWorkItem;)I

    .line 22
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 8
    goto :goto_0

    .line 18
    :cond_1
    const-class v0, Lcom/android/voicemail/impl/ActivationTask;

    invoke-static {p0, v0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 19
    if-eqz p2, :cond_2

    .line 20
    const-string v1, "extra_message_data_bundle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 21
    :cond_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lclu;)V
    .locals 3

    .prologue
    .line 176
    .line 177
    invoke-static {p0, p1}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    sget-object v1, Lclt;->a:Lclt;

    .line 178
    invoke-virtual {p2, v0, v1}, Lclu;->a(Lcnw;Lclt;)V

    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.voicemail.VoicemailClient.ACTION_SHOW_LEGACY_VOICEMAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v1, "android.telephony.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 183
    const-string v1, "android.telephony.extra.NOTIFICATION_COUNT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 184
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 185
    invoke-static {p0, p1}, Lcom/android/voicemail/impl/sync/SyncTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 186
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;Lclu;)V
    .locals 2

    .prologue
    .line 169
    const-string v0, "0"

    .line 170
    iget-object v1, p2, Lcpx;->b:Ljava/lang/String;

    .line 171
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-static {p0, p1, p2}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;)V

    .line 173
    invoke-static {p0, p1, p3}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lclu;)V

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    const-string v0, "VvmActivationTask"

    const-string v1, "Visual voicemail not available for subscriber."

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 26
    .line 28
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 29
    sget-object v1, Lbkq$a;->aZ:Lbkq$a;

    .line 30
    invoke-static {v0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 31
    invoke-super {p0}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a()Landroid/content/Intent;

    move-result-object v0

    .line 32
    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0, p1, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 24
    const-string v0, "extra_message_data_bundle"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/android/voicemail/impl/ActivationTask;->g:Landroid/os/Bundle;

    .line 25
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    .line 33
    invoke-static {}, Lbvs;->g()V

    .line 36
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 37
    sget-object v1, Lbkq$a;->aX:Lbkq$a;

    .line 38
    invoke-static {v0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 40
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 42
    if-nez v2, :cond_0

    .line 43
    const-string v0, "VvmActivationTask"

    const-string v1, "null PhoneAccountHandle"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 47
    invoke-static {v0, v2}, Lbvs;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 48
    new-instance v3, Lclu;

    .line 49
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 50
    invoke-direct {v3, v0, v2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 51
    invoke-virtual {v3}, Lclu;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    const-string v0, "VvmActivationTask"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "VVM not supported on phoneAccountHandle "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 55
    invoke-static {v0, v2}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 59
    invoke-static {v0, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 60
    invoke-virtual {v3}, Lclu;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    const-string v0, "VvmActivationTask"

    const-string v1, "Setting up filter for legacy mode"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v3}, Lclu;->k()V

    .line 63
    :cond_2
    const-string v0, "VvmActivationTask"

    const-string v1, "VVM is disabled"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 67
    invoke-static {v0, v2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    .line 68
    invoke-virtual {v3}, Lclu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcnw;->a(Ljava/lang/String;)Lcnw;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcnw;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 70
    const-string v1, "VvmActivationTask"

    const-string v4, "Failed to configure content provider - "

    invoke-virtual {v3}, Lclu;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    .line 72
    :cond_4
    const-string v1, "VvmActivationTask"

    const-string v4, "VVM content provider configured - "

    invoke-virtual {v3}, Lclu;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v1, v0}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 75
    invoke-static {v0, v2}, Lcqe;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 76
    const-string v0, "VvmActivationTask"

    const-string v1, "Account is already activated"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v3}, Lclu;->k()V

    .line 79
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 80
    invoke-static {v0, v2, v3}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lclu;)V

    goto/16 :goto_0

    .line 70
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 72
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 84
    :cond_7
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 85
    invoke-static {v0, v2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    sget-object v1, Lclt;->d:Lclt;

    .line 86
    invoke-virtual {v3, v0, v1}, Lclu;->a(Lcnw;Lclt;)V

    .line 88
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 90
    const-class v1, Landroid/telephony/TelephonyManager;

    .line 91
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 92
    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    .line 94
    :goto_3
    if-nez v0, :cond_9

    .line 95
    const-string v0, "VvmActivationTask"

    const-string v1, "Service lost during activation, aborting"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 99
    invoke-static {v0, v2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v0

    sget-object v1, Lclt;->F:Lclt;

    .line 100
    invoke-virtual {v3, v0, v1}, Lclu;->a(Lcnw;Lclt;)V

    goto/16 :goto_0

    .line 93
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 102
    :cond_9
    invoke-virtual {v3}, Lclu;->k()V

    .line 103
    iget-object v0, p0, Lcom/android/voicemail/impl/ActivationTask;->f:Lcpg;

    .line 104
    iget-object v4, v0, Lcpg;->a:Lcmc;

    .line 107
    iget-object v0, v3, Lclu;->d:Lcou;

    .line 109
    iget-object v1, p0, Lcom/android/voicemail/impl/ActivationTask;->g:Landroid/os/Bundle;

    if-eqz v1, :cond_a

    .line 110
    iget-object v6, p0, Lcom/android/voicemail/impl/ActivationTask;->g:Landroid/os/Bundle;

    .line 131
    :goto_4
    new-instance v5, Lcpx;

    invoke-direct {v5, v6}, Lcpx;-><init>(Landroid/os/Bundle;)V

    .line 134
    iget-object v0, v5, Lcpx;->a:Ljava/lang/String;

    .line 137
    iget-object v1, v5, Lcpx;->b:Ljava/lang/String;

    .line 138
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "STATUS SMS received: st="

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", rc="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    iget-object v0, v5, Lcpx;->a:Ljava/lang/String;

    .line 141
    const-string v1, "R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 143
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 144
    invoke-static {v0, v2, v5, v3}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;Lclu;)V

    .line 165
    :goto_5
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 166
    sget-object v1, Lbkq$a;->aY:Lbkq$a;

    .line 167
    invoke-static {v0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    goto/16 :goto_0

    .line 111
    :cond_a
    :try_start_0
    new-instance v5, Lcpy;

    .line 112
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 113
    invoke-direct {v5, v1, v2}, Lcpy;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v1, 0x0

    .line 114
    :try_start_1
    invoke-virtual {v5}, Lcpy;->b()Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Lcou;->a(Lclu;Landroid/app/PendingIntent;)V

    .line 115
    invoke-virtual {v5}, Lcpy;->a()Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 116
    :try_start_2
    invoke-virtual {v5}, Lcpy;->close()V
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    goto :goto_4

    .line 120
    :catch_0
    move-exception v0

    sget-object v0, Lclt;->f:Lclt;

    invoke-virtual {v3, v4, v0}, Lclu;->a(Lcnw;Lclt;)V

    .line 121
    invoke-virtual {p0}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    goto/16 :goto_0

    .line 117
    :catch_1
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_6
    if-eqz v1, :cond_b

    :try_start_4
    invoke-virtual {v5}, Lcpy;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :goto_7
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 124
    :catch_2
    move-exception v0

    const-string v0, "VvmActivationTask"

    const-string v1, "Unable to send status request SMS"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    goto/16 :goto_0

    .line 118
    :catch_3
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_7

    .line 127
    :catch_4
    move-exception v0

    .line 128
    :goto_8
    const-string v1, "VvmActivationTask"

    const-string v2, "can\'t get future STATUS SMS"

    invoke-static {v1, v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    invoke-virtual {p0}, Lcom/android/voicemail/impl/ActivationTask;->c()V

    goto/16 :goto_0

    .line 118
    :cond_b
    :try_start_7
    invoke-virtual {v5}, Lcpy;->close()V
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_7

    .line 127
    :catch_5
    move-exception v0

    goto :goto_8

    .line 146
    :cond_c
    invoke-virtual {v3}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 147
    iget-object v0, v3, Lclu;->d:Lcou;

    invoke-virtual {v0}, Lcou;->a()Z

    move-result v0

    .line 148
    if-eqz v0, :cond_d

    .line 149
    const-string v0, "VvmActivationTask"

    const-string v1, "Subscriber not ready, start provisioning"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v3}, Lclu;->a()Z

    move-result v0

    invoke-static {v0}, Lbdf;->a(Z)V

    .line 152
    iget-object v0, v3, Lclu;->d:Lcou;

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcou;->a(Lcom/android/voicemail/impl/ActivationTask;Landroid/telecom/PhoneAccountHandle;Lclu;Lcnw;Lcpx;Landroid/os/Bundle;)V

    goto :goto_5

    .line 155
    :cond_d
    iget-object v0, v5, Lcpx;->a:Ljava/lang/String;

    .line 156
    const-string v1, "N"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 157
    const-string v0, "VvmActivationTask"

    const-string v1, "Subscriber new but provisioning is not supported"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 160
    invoke-static {v0, v2, v5, v3}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;Lclu;)V

    goto/16 :goto_5

    .line 161
    :cond_e
    const-string v0, "VvmActivationTask"

    const-string v1, "Subscriber not ready but provisioning is not supported"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lclt;->g:Lclt;

    invoke-virtual {v3, v4, v0}, Lclu;->a(Lcnw;Lclt;)V

    goto/16 :goto_5

    .line 127
    :catch_6
    move-exception v0

    goto :goto_8

    .line 118
    :catchall_1
    move-exception v0

    goto :goto_6
.end method
