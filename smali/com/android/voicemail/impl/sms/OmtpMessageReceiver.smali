.class public Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2
    iput-object p1, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    .line 3
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_voicemail_sms"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telephony/VisualVoicemailSms;

    .line 4
    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 5
    if-nez v2, :cond_1

    .line 6
    const-string v0, "OmtpMessageReceiver"

    const-string v1, "Received message for null phone account"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 8
    :cond_1
    const-class v1, Landroid/os/UserManager;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    invoke-virtual {v1}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 9
    const-string v1, "OmtpMessageReceiver"

    const-string v2, "Received message on locked device"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    invoke-static {p1, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telephony/VisualVoicemailSms;)V

    goto :goto_0

    .line 12
    :cond_2
    new-instance v1, Lclu;

    iget-object v3, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 13
    invoke-virtual {v1}, Lclu;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 14
    const-string v0, "OmtpMessageReceiver"

    const-string v1, "vvm config no longer valid"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16
    :cond_3
    iget-object v3, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 17
    invoke-virtual {v1}, Lclu;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 18
    invoke-static {p1, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telephony/VisualVoicemailSms;)V

    goto :goto_0

    .line 19
    :cond_4
    const-string v0, "OmtpMessageReceiver"

    const-string v1, "Received vvm message for disabled vvm source."

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 21
    :cond_5
    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSms;->getPrefix()Ljava/lang/String;

    move-result-object v3

    .line 22
    invoke-virtual {v0}, Landroid/telephony/VisualVoicemailSms;->getFields()Landroid/os/Bundle;

    move-result-object v4

    .line 23
    if-eqz v3, :cond_6

    if-nez v4, :cond_7

    .line 24
    :cond_6
    const-string v0, "OmtpMessageReceiver"

    const-string v1, "Unparsable VVM SMS received, ignoring"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 26
    :cond_7
    const-string v0, "SYNC"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 27
    new-instance v1, Lcpz;

    invoke-direct {v1, v4}, Lcpz;-><init>(Landroid/os/Bundle;)V

    .line 28
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 30
    iget-object v3, v1, Lcpz;->a:Ljava/lang/String;

    .line 31
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Received SYNC sms for "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " with event "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    iget-object v3, v1, Lcpz;->a:Ljava/lang/String;

    .line 35
    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_8
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 69
    const-string v2, "OmtpMessageReceiver"

    const-string v3, "Unrecognized sync trigger event: "

    .line 70
    iget-object v0, v1, Lcpz;->a:Ljava/lang/String;

    .line 71
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 35
    :sswitch_0
    const-string v4, "NM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v4, "MBU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v4, "GU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v0, 0x2

    goto :goto_1

    .line 36
    :pswitch_1
    const-string v0, "v"

    .line 37
    iget-object v3, v1, Lcpz;->e:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 39
    const-string v0, "OmtpMessageReceiver"

    .line 41
    iget-object v1, v1, Lcpz;->e:Ljava/lang/String;

    .line 42
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Non-voice message of type \'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' received, ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 47
    :cond_9
    iget-wide v4, v1, Lcpz;->g:J

    .line 49
    iget-object v0, v1, Lcpz;->f:Ljava/lang/String;

    .line 50
    invoke-static {v4, v5, v0}, Lclz;->a(JLjava/lang/String;)Lcov;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2}, Lcov;->a(Landroid/telecom/PhoneAccountHandle;)Lcov;

    move-result-object v0

    .line 53
    iget-object v3, v1, Lcpz;->c:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v3}, Lcov;->c(Ljava/lang/String;)Lcov;

    move-result-object v0

    .line 56
    iget v1, v1, Lcpz;->d:I

    .line 57
    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Lcov;->c(J)Lcov;

    move-result-object v0

    iget-object v1, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    .line 58
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcov;->b(Ljava/lang/String;)Lcov;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcov;->a()Lclz;

    move-result-object v1

    .line 60
    new-instance v3, Lcqd;

    iget-object v4, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcqd;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-virtual {v3, v1}, Lcqd;->a(Lclz;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    iget-object v3, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lclz;)Landroid/net/Uri;

    move-result-object v1

    .line 63
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcov;->b(J)Lcov;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcov;->a(Landroid/net/Uri;)Lcov;

    move-result-object v0

    invoke-virtual {v0}, Lcov;->a()Lclz;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lcom/android/voicemail/impl/sync/SyncOneTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lclz;)V

    goto/16 :goto_0

    .line 66
    :pswitch_2
    iget-object v0, p0, Lcom/android/voicemail/impl/sms/OmtpMessageReceiver;->a:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/android/voicemail/impl/sync/SyncTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto/16 :goto_0

    .line 71
    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 72
    :cond_b
    const-string v0, "STATUS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 73
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x18

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Received Status sms for "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-static {p1, v2, v4}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 75
    :cond_c
    const-string v5, "OmtpMessageReceiver"

    const-string v6, "Unknown prefix: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_d

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v5, v0}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, v1, Lclu;->d:Lcou;

    .line 79
    if-eqz v0, :cond_0

    .line 82
    iget-object v0, v1, Lclu;->d:Lcou;

    .line 83
    invoke-virtual {v0, v1, v3, v4}, Lcou;->a(Lclu;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 85
    const-string v0, "OmtpMessageReceiver"

    const-string v1, "Protocol recognized the SMS as STATUS, activating"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {p1, v2, v4}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 75
    :cond_d
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x8ee -> :sswitch_2
        0x9bf -> :sswitch_0
        0x12960 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
