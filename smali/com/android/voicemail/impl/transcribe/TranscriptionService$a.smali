.class public Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/voicemail/impl/transcribe/TranscriptionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;


# direct methods
.method constructor <init>(Lcom/android/voicemail/impl/transcribe/TranscriptionService;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;->a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/voicemail/impl/transcribe/TranscriptionService;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;-><init>(Lcom/android/voicemail/impl/transcribe/TranscriptionService;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/job/JobWorkItem;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {}, Lbdf;->b()V

    .line 2
    const-string v0, "TranscriptionService.Callback.onWorkCompleted"

    invoke-virtual {p1}, Landroid/app/job/JobWorkItem;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;->a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    .line 4
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    .line 6
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;->a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    .line 7
    iget-boolean v0, v0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->c:Z

    .line 8
    if-eqz v0, :cond_0

    .line 9
    const-string v0, "TranscriptionService.Callback.onWorkCompleted"

    const-string v1, "stopped"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    :goto_0
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;->a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    .line 11
    iget-object v0, v0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a:Landroid/app/job/JobParameters;

    .line 12
    invoke-virtual {v0, p1}, Landroid/app/job/JobParameters;->completeWork(Landroid/app/job/JobWorkItem;)V

    .line 13
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;->a:Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    .line 14
    invoke-virtual {v0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a()Z

    goto :goto_0
.end method
