.class public Lcom/android/voicemail/impl/transcribe/TranscriptionBackfillService;
.super Lko;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lko;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    const-string v0, "TranscriptionBackfillService.transcribeOldVoicemails"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Lcom/android/voicemail/impl/transcribe/TranscriptionBackfillService;

    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    new-instance v3, Landroid/app/job/JobInfo$Builder;

    const/16 v4, 0xcc

    invoke-direct {v3, v4, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    const/4 v0, 0x2

    .line 6
    invoke-virtual {v3, v0}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v3

    .line 7
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 8
    invoke-virtual {v3}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    .line 9
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 10
    const-string v5, "extra_account_handle"

    invoke-virtual {v4, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    new-instance v5, Landroid/app/job/JobWorkItem;

    invoke-direct {v5, v4}, Landroid/app/job/JobWorkItem;-><init>(Landroid/content/Intent;)V

    .line 12
    invoke-virtual {v0, v3, v5}, Landroid/app/job/JobScheduler;->enqueue(Landroid/app/job/JobInfo;Landroid/app/job/JobWorkItem;)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 14
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 12
    goto :goto_0

    .line 13
    :cond_1
    const-string v0, "TranscriptionBackfillService.transcribeOldVoicemails"

    const-string v1, "not supported"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 14
    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    .prologue
    .line 15
    const-string v0, "TranscriptionBackfillService.onHandleWork"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcqo;

    invoke-direct {v0, p0}, Lcqo;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-virtual {v0}, Lcqo;->a()Ljava/util/List;

    move-result-object v0

    .line 18
    const-string v1, "TranscriptionBackfillService.onHandleWork"

    .line 19
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x2a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "found "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " untranscribed voicemails"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    .line 20
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 22
    new-instance v2, Lcqm;

    invoke-direct {v2, p0, v0}, Lcqm;-><init>(Lcom/android/voicemail/impl/transcribe/TranscriptionBackfillService;Landroid/net/Uri;)V

    invoke-static {v2}, Lapw;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "TranscriptionBackfillService.onDestroy"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    invoke-super {p0}, Lko;->onDestroy()V

    .line 27
    return-void
.end method
