.class public Lcom/android/voicemail/impl/transcribe/TranscriptionService;
.super Landroid/app/job/JobService;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;
    }
.end annotation


# instance fields
.field public a:Landroid/app/job/JobParameters;

.field public b:Lcqp;

.field public c:Z

.field private d:Ljava/util/concurrent/ExecutorService;

.field private e:Lcqz;

.field private f:Lcqn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 38
    invoke-static {}, Lbdf;->b()V

    .line 39
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lcqz;Lcqn;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    .line 42
    iput-object p2, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    .line 43
    iput-object p3, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    .line 44
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Landroid/telecom/PhoneAccountHandle;Z)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1
    invoke-static {}, Lbdf;->b()V

    .line 3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-ge v0, v3, :cond_0

    .line 4
    const-string v0, "TranscriptionService.canTranscribeVoicemail"

    const-string v3, "not supported by sdk"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 16
    :goto_0
    if-nez v0, :cond_3

    move v0, v1

    .line 36
    :goto_1
    return v0

    .line 6
    :cond_0
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    .line 7
    invoke-interface {v0, p0, p2}, Lcln;->i(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 8
    const-string v0, "TranscriptionService.canTranscribeVoicemail"

    const-string v3, "hasn\'t accepted TOS"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 9
    goto :goto_0

    .line 10
    :cond_1
    const-string v3, "vvm_carrier_allows_ott_transcription_string"

    .line 11
    invoke-interface {v0, p0, p2, v3}, Lcln;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 12
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 13
    const-string v0, "TranscriptionService.canTranscribeVoicemail"

    const-string v3, "carrier doesn\'t allow transcription"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 14
    goto :goto_0

    :cond_2
    move v0, v2

    .line 15
    goto :goto_0

    .line 18
    :cond_3
    const-string v0, "TranscriptionService.scheduleNewVoicemailTranscriptionJob"

    const-string v3, "scheduling transcription"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v3, Lbkq$a;->dd:Lbkq$a;

    invoke-interface {v0, v3}, Lbku;->a(Lbkq$a;)V

    .line 20
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Lcom/android/voicemail/impl/transcribe/TranscriptionService;

    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 21
    new-instance v3, Landroid/app/job/JobInfo$Builder;

    const/16 v4, 0xcb

    invoke-direct {v3, v4, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 22
    if-eqz p3, :cond_5

    .line 24
    invoke-virtual {v3, v6, v7}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v6, v7}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0, v2}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    .line 28
    :goto_2
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 30
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 31
    const-string v5, "extra_voicemail_uri"

    invoke-virtual {v4, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 32
    if-eqz p2, :cond_4

    .line 33
    const-string v5, "extra_account_handle"

    invoke-virtual {v4, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 34
    :cond_4
    new-instance v5, Landroid/app/job/JobWorkItem;

    invoke-direct {v5, v4}, Landroid/app/job/JobWorkItem;-><init>(Landroid/content/Intent;)V

    .line 36
    invoke-virtual {v3}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Landroid/app/job/JobScheduler;->enqueue(Landroid/app/job/JobInfo;Landroid/app/job/JobWorkItem;)I

    move-result v0

    if-ne v0, v2, :cond_6

    move v0, v2

    goto/16 :goto_1

    .line 27
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    goto :goto_2

    :cond_6
    move v0, v1

    .line 36
    goto/16 :goto_1
.end method

.method private final b()Lcqn;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcqn;

    invoke-direct {v0, p0}, Lcqn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    return-object v0
.end method

.method private final c()Lcqz;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcqz;

    invoke-direct {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b()Lcqn;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcqz;-><init>(Landroid/content/Context;Lcqn;)V

    iput-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    return-object v0
.end method


# virtual methods
.method final a()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-static {}, Lbdf;->b()V

    .line 84
    iget-boolean v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->c:Z

    if-eqz v0, :cond_1

    .line 85
    const-string v0, "TranscriptionService.checkForWork"

    const-string v2, "stopped"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a:Landroid/app/job/JobParameters;

    invoke-virtual {v0}, Landroid/app/job/JobParameters;->dequeueWork()Landroid/app/job/JobWorkItem;

    move-result-object v3

    .line 88
    if-eqz v3, :cond_0

    .line 89
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    if-nez v0, :cond_3

    move v0, v6

    :goto_1
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 91
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    invoke-virtual {v0}, Lcqn;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 92
    new-instance v0, Lcqv;

    new-instance v2, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;

    invoke-direct {v2, p0, v1}, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;-><init>(Lcom/android/voicemail/impl/transcribe/TranscriptionService;B)V

    .line 93
    invoke-direct {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->c()Lcqz;

    move-result-object v4

    iget-object v5, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcqv;-><init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V

    .line 95
    :goto_2
    iput-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    .line 97
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_2

    .line 98
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    .line 100
    iget-object v1, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    move v1, v6

    .line 101
    goto :goto_0

    :cond_3
    move v0, v1

    .line 89
    goto :goto_1

    .line 94
    :cond_4
    new-instance v0, Lcqs;

    new-instance v2, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;

    invoke-direct {v2, p0, v1}, Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;-><init>(Lcom/android/voicemail/impl/transcribe/TranscriptionService;B)V

    .line 95
    invoke-direct {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->c()Lcqz;

    move-result-object v4

    iget-object v5, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcqs;-><init>(Landroid/content/Context;Lcom/android/voicemail/impl/transcribe/TranscriptionService$a;Landroid/app/job/JobWorkItem;Lcqz;Lcqn;)V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-static {}, Lbdf;->b()V

    .line 71
    const-string v0, "TranscriptionService.onDestroy"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    .line 75
    const-string v1, "TranscriptionClientFactory.shutdown"

    invoke-static {v1}, Lapw;->b(Ljava/lang/String;)V

    .line 76
    iget-object v1, v0, Lcqz;->b:Lhlf;

    invoke-virtual {v1}, Lhlf;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v0, v0, Lcqz;->b:Lhlf;

    invoke-virtual {v0}, Lhlf;->b()Lhlf;

    .line 78
    :cond_0
    iput-object v2, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->e:Lcqz;

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 81
    iput-object v2, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->d:Ljava/util/concurrent/ExecutorService;

    .line 82
    :cond_2
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-static {}, Lbdf;->b()V

    .line 46
    const-string v0, "TranscriptionService.onStartJob"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b()Lcqn;

    move-result-object v0

    invoke-virtual {v0}, Lcqn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const-string v0, "TranscriptionService.onStartJob"

    const-string v2, "transcription not enabled, exiting."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 57
    :goto_0
    return v0

    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b()Lcqn;

    move-result-object v0

    invoke-virtual {v0}, Lcqn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    const-string v0, "TranscriptionService.onStartJob"

    const-string v2, "transcription server not configured, exiting."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const-string v2, "TranscriptionService.onStartJob"

    const-string v3, "transcription server address: "

    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->f:Lcqn;

    .line 54
    invoke-virtual {v0}, Lcqn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    .line 55
    invoke-static {v2, v0, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iput-object p1, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a:Landroid/app/job/JobParameters;

    .line 57
    invoke-virtual {p0}, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->a()Z

    move-result v0

    goto :goto_0

    .line 54
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 58
    invoke-static {}, Lbdf;->b()V

    .line 59
    const-string v0, "TranscriptionService.onStopJob"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "params: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iput-boolean v4, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->c:Z

    .line 61
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dv:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 62
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "TranscriptionService.onStopJob"

    const-string v1, "cancelling active task"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, Lcom/android/voicemail/impl/transcribe/TranscriptionService;->b:Lcqp;

    .line 65
    invoke-static {}, Lbdf;->b()V

    .line 66
    const-string v1, "TranscriptionTask"

    const-string v2, "cancel"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iput-boolean v4, v0, Lcqp;->h:Z

    .line 68
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dw:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 69
    :cond_0
    return v4
.end method
