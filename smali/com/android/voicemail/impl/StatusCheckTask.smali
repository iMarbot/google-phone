.class public Lcom/android/voicemail/impl/StatusCheckTask;
.super Lcom/android/voicemail/impl/scheduling/BaseTask;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation

.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;-><init>(I)V

    .line 2
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/android/voicemail/impl/StatusCheckTask;

    invoke-static {p0, v0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 4
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 5
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 8

    .prologue
    .line 6
    .line 8
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 9
    const-class v1, Landroid/telephony/TelephonyManager;

    .line 10
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 12
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 13
    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    .line 17
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 18
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " no longer valid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 19
    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :goto_0
    return-void

    .line 21
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-eqz v0, :cond_1

    .line 22
    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    .line 24
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 25
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not in service"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 26
    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 28
    :cond_1
    new-instance v0, Lclu;

    .line 30
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 32
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 33
    invoke-direct {v0, v1, v2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 34
    invoke-virtual {v0}, Lclu;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 35
    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    .line 37
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 38
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "config no longer valid for "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 43
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 44
    invoke-static {v0, v1}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_0

    .line 46
    :cond_2
    :try_start_0
    new-instance v2, Lcpy;

    .line 47
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 49
    iget-object v3, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 50
    invoke-direct {v2, v1, v3}, Lcpy;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v1, 0x0

    .line 52
    :try_start_1
    iget-object v3, v0, Lclu;->d:Lcou;

    .line 53
    invoke-virtual {v2}, Lcpy;->b()Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcou;->b(Lclu;Landroid/app/PendingIntent;)V

    .line 54
    invoke-virtual {v2}, Lcpy;->a()Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 55
    :try_start_2
    invoke-virtual {v2}, Lcpy;->close()V
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 67
    new-instance v1, Lcpx;

    invoke-direct {v1, v0}, Lcpx;-><init>(Landroid/os/Bundle;)V

    .line 68
    const-string v2, "StatusCheckTask.onExecuteInBackgroundThread"

    .line 70
    iget-object v3, v1, Lcpx;->a:Ljava/lang/String;

    .line 73
    iget-object v4, v1, Lcpx;->b:Ljava/lang/String;

    .line 74
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1d

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "STATUS SMS received: st="

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", rc="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 75
    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v2, v1, Lcpx;->a:Ljava/lang/String;

    .line 78
    const-string v3, "R"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 79
    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    const-string v2, "subscriber ready, no activation required"

    invoke-static {v0, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 83
    sget-object v2, Lbkq$a;->bU:Lbkq$a;

    .line 84
    invoke-static {v0, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 86
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 88
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 89
    invoke-static {v0, v2, v1}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;)V

    goto/16 :goto_0

    .line 56
    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v2}, Lcpy;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 59
    :catch_1
    move-exception v0

    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    const-string v1, "timeout requesting status"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 57
    :catch_2
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_2

    .line 62
    :catch_3
    move-exception v0

    const-string v0, "StatusCheckTask.onExecuteInBackgroundThread"

    const-string v1, "Unable to send status request SMS"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 57
    :cond_3
    :try_start_7
    invoke-virtual {v2}, Lcpy;->close()V
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_2

    .line 64
    :catch_4
    move-exception v0

    .line 65
    :goto_3
    const-string v1, "StatusCheckTask.onExecuteInBackgroundThread"

    const-string v2, "can\'t get future STATUS SMS"

    invoke-static {v1, v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 90
    :cond_4
    const-string v1, "StatusCheckTask.onExecuteInBackgroundThread"

    const-string v2, "subscriber not ready, attempting reactivation"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 94
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 95
    invoke-static {v1, v2}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 98
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 99
    sget-object v2, Lbkq$a;->bV:Lbkq$a;

    .line 100
    invoke-static {v1, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 102
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 104
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 105
    invoke-static {v1, v2, v0}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 64
    :catch_5
    move-exception v0

    goto :goto_3

    :catch_6
    move-exception v0

    goto :goto_3

    .line 57
    :catchall_1
    move-exception v0

    goto :goto_1
.end method
