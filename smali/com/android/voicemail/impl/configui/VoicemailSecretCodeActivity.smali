.class public Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;
.super Landroid/preference/PreferenceActivity;
.source "PG"


# instance fields
.field private a:Landroid/preference/PreferenceActivity$Header;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 2
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onBuildHeaders(Ljava/util/List;)V

    .line 3
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    iput-object v0, p0, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->a:Landroid/preference/PreferenceActivity$Header;

    .line 4
    iget-object v0, p0, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->a:Landroid/preference/PreferenceActivity$Header;

    const-string v1, "Sync"

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 5
    iget-object v0, p0, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->a:Landroid/preference/PreferenceActivity$Header;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 7
    const-class v1, Lcme;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 8
    const-string v1, "VVM config override"

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 9
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 10
    return-void
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->a:Landroid/preference/PreferenceActivity$Header;

    if-ne p1, v0, :cond_0

    .line 12
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.provider.action.SYNC_VOICEMAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 13
    invoke-virtual {p0}, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 14
    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/configui/VoicemailSecretCodeActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 17
    :goto_0
    return-void

    .line 16
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_0
.end method
