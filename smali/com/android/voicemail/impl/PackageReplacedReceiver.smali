.class public Lcom/android/voicemail/impl/PackageReplacedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/voicemail/impl/PackageReplacedReceiver$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2
    const-string v0, "PackageReplacedReceiver.onReceive"

    const-string v1, "package replaced, starting activation"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-static {p1}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    invoke-interface {v0}, Lcln;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4
    const-string v0, "PackageReplacedReceiver.onReceive"

    const-string v1, "module disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    :cond_0
    :goto_0
    return-void

    .line 6
    :cond_1
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 7
    invoke-static {p1, v0, v3}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 9
    :cond_2
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 10
    const-string v1, "dialer_feature_version_acknowledged"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    const-string v0, "PackageReplacedReceiver.setVoicemailFeatureVersionAsync"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 13
    invoke-virtual {p0}, Lcom/android/voicemail/impl/PackageReplacedReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v0

    .line 14
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    .line 15
    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    new-instance v2, Lcom/android/voicemail/impl/PackageReplacedReceiver$a;

    invoke-direct {v2, p1}, Lcom/android/voicemail/impl/PackageReplacedReceiver$a;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-virtual {v1, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Lclv;

    invoke-direct {v2, v0}, Lclv;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 17
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v1

    new-instance v2, Lclw;

    invoke-direct {v2, v0}, Lclw;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 18
    invoke-interface {v1, v2}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 19
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 20
    invoke-interface {v0, v3}, Lbdy;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method
