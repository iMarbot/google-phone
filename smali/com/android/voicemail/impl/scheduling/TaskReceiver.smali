.class public Lcom/android/voicemail/impl/scheduling/TaskReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# static fields
.field public static final a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/voicemail/impl/scheduling/TaskReceiver;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2
    if-nez p2, :cond_0

    .line 3
    const-string v0, "VvmTaskReceiver"

    const-string v1, "null intent received"

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :goto_0
    return-void

    .line 5
    :cond_0
    const-string v0, "VvmTaskReceiver"

    const-string v1, "task received"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    sget-object v0, Lcpj;->b:Lcpj;

    .line 8
    if-eqz v0, :cond_2

    .line 9
    const-string v1, "VvmTaskReceiver"

    const-string v2, "TaskExecutor already running"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    iget-boolean v1, v0, Lcpj;->g:Z

    .line 12
    if-eqz v1, :cond_1

    .line 13
    const-string v0, "VvmTaskReceiver"

    const-string v1, "TaskExecutor is terminating, bouncing task"

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    sget-object v0, Lcom/android/voicemail/impl/scheduling/TaskReceiver;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/os/Bundle;)Lcph;

    move-result-object v1

    .line 18
    invoke-static {}, Lbvs;->f()V

    .line 20
    invoke-static {}, Lbvs;->f()V

    .line 21
    iget-object v2, v0, Lcpj;->e:Lcpr;

    .line 22
    invoke-virtual {v2, v1}, Lcpr;->a(Lcph;)Z

    .line 23
    const-string v2, "VvmTaskExecutor"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " added"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    iget-object v1, v0, Lcpj;->c:Lcpo;

    iget-object v2, v0, Lcpj;->i:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcpo;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 25
    invoke-virtual {v0}, Lcpj;->a()V

    goto :goto_0

    .line 27
    :cond_2
    const-string v0, "VvmTaskReceiver"

    const-string v1, "scheduling new job"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v0, v2, v3, v4}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a(Landroid/content/Context;Ljava/util/List;JZ)V

    goto/16 :goto_0
.end method
