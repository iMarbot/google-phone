.class public Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;
.super Landroid/app/job/JobService;
.source "PG"

# interfaces
.implements Lcpm;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field private a:Landroid/app/job/JobParameters;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method private static a([Landroid/os/Parcelable;)Ljava/util/List;
    .locals 4

    .prologue
    .line 81
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, p0, v1

    .line 83
    check-cast v0, Landroid/os/Bundle;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_0
    return-object v2
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;JZ)V
    .locals 8

    .prologue
    .line 29
    invoke-static {}, Lbvs;->f()V

    .line 30
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 31
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;

    move-result-object v1

    .line 32
    const-string v2, "TaskSchedulerJobService"

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x25

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "scheduling job with "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tasks"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    if-eqz v1, :cond_2

    .line 34
    if-eqz p4, :cond_1

    .line 36
    invoke-virtual {v1}, Landroid/app/job/JobInfo;->getTransientExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "extra_task_extras_array"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 37
    invoke-static {v1}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a([Landroid/os/Parcelable;)Ljava/util/List;

    move-result-object v1

    .line 38
    const-string v2, "TaskSchedulerJobService"

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x2b

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "merging job with "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " existing tasks"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    new-instance v2, Lcpr;

    invoke-direct {v2}, Lcpr;-><init>()V

    .line 40
    invoke-virtual {v2, p0, v1}, Lcpr;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 41
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 42
    invoke-static {p0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/os/Bundle;)Lcph;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcpr;->a(Lcph;)Z

    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {v2}, Lcpr;->a()Ljava/util/List;

    move-result-object p1

    .line 45
    :cond_1
    const-string v1, "TaskSchedulerJobService"

    const-string v2, "canceling existing job."

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 47
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 49
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 50
    const-string v3, "com.android.voicemail.impl.scheduling.TaskSchedulerJobService.NEXT_JOB_ID"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 51
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "com.android.voicemail.impl.scheduling.TaskSchedulerJobService.NEXT_JOB_ID"

    add-int/lit8 v5, v3, 0x1

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 54
    const-string v1, "extra_job_id"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 56
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "com.android.voicemail.impl.scheduling.TaskSchedulerJobService.EXPECTED_JOB_ID"

    .line 57
    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 58
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    const-string v4, "extra_task_extras_array"

    .line 60
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/os/Bundle;

    invoke-interface {p1, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/os/Parcelable;

    .line 61
    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 62
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    const/16 v4, 0xc8

    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;

    invoke-direct {v5, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v4, v5}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 63
    invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->setTransientExtras(Landroid/os/Bundle;)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    .line 64
    invoke-virtual {v1, p2, p3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 65
    invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 66
    if-eqz p4, :cond_3

    .line 67
    const-wide/16 v4, 0x0

    cmp-long v1, p2, v4

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lbvs;->c(Z)V

    .line 68
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/app/job/JobInfo$Builder;->setOverrideDeadline(J)Landroid/app/job/JobInfo$Builder;

    .line 69
    const-string v1, "TaskSchedulerJobService"

    const-string v4, "running job instantly."

    invoke-static {v1, v4}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_3
    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 71
    const-string v0, "TaskSchedulerJobService"

    const/16 v1, 0x19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "job "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scheduled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void

    .line 67
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "TaskSchedulerJobService"

    const-string v1, "finishing job"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a:Landroid/app/job/JobParameters;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a:Landroid/app/job/JobParameters;

    .line 76
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 77
    invoke-static {}, Lbvs;->f()V

    .line 78
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const/16 v1, 0xc8

    .line 79
    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    .line 79
    :cond_0
    const/4 v0, 0x0

    .line 80
    goto :goto_0
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getTransientExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "extra_job_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 3
    new-instance v0, Lcpt;

    invoke-direct {v0, p0}, Lcpt;-><init>(Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;)V

    .line 4
    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 5
    if-eq v3, v0, :cond_0

    .line 6
    const-string v1, "TaskSchedulerJobService"

    const/16 v4, 0x43

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Job "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not the last scheduled job "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", ignoring"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    :goto_0
    return v2

    .line 8
    :cond_0
    const-string v0, "TaskSchedulerJobService"

    const/16 v4, 0x14

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "starting "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iput-object p1, p0, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a:Landroid/app/job/JobParameters;

    .line 11
    invoke-static {}, Lbvs;->f()V

    .line 12
    sget-object v0, Lcpj;->b:Lcpj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbvs;->c(Z)V

    .line 13
    new-instance v0, Lcpj;

    invoke-direct {v0, p0}, Lcpj;-><init>(Landroid/content/Context;)V

    .line 14
    sput-object v0, Lcpj;->b:Lcpj;

    .line 15
    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a:Landroid/app/job/JobParameters;

    .line 16
    invoke-virtual {v2}, Landroid/app/job/JobParameters;->getTransientExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "extra_task_extras_array"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 17
    invoke-static {v2}, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a([Landroid/os/Parcelable;)Ljava/util/List;

    move-result-object v2

    .line 18
    invoke-virtual {v0, p0, v2}, Lcpj;->a(Lcpm;Ljava/util/List;)V

    move v2, v1

    .line 19
    goto :goto_0

    :cond_1
    move v0, v2

    .line 12
    goto :goto_1
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 4

    .prologue
    .line 20
    sget-object v0, Lcpj;->b:Lcpj;

    .line 22
    const-string v1, "VvmTaskExecutor"

    const-string v2, "onStopJob"

    invoke-static {v1, v2}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-virtual {v0}, Lcpj;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    iget-boolean v1, v0, Lcpj;->g:Z

    .line 25
    if-nez v1, :cond_0

    .line 26
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcpj;->a(JZ)V

    .line 27
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/voicemail/impl/scheduling/TaskSchedulerJobService;->a:Landroid/app/job/JobParameters;

    .line 28
    const/4 v0, 0x0

    return v0
.end method
