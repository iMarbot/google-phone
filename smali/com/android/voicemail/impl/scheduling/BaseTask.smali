.class public abstract Lcom/android/voicemail/impl/scheduling/BaseTask;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcph;


# annotations
.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/voicemail/impl/scheduling/BaseTask$a;
    }
.end annotation


# static fields
.field private static i:Lcom/android/voicemail/impl/scheduling/BaseTask$a;


# instance fields
.field public a:Landroid/content/Context;

.field public b:I

.field public c:Landroid/telecom/PhoneAccountHandle;

.field public d:Z

.field public volatile e:Z

.field private f:Landroid/os/Bundle;

.field private g:Ljava/util/List;

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/android/voicemail/impl/scheduling/BaseTask$a;

    invoke-direct {v0}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;-><init>()V

    sput-object v0, Lcom/android/voicemail/impl/scheduling/BaseTask;->i:Lcom/android/voicemail/impl/scheduling/BaseTask$a;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    .line 3
    iput p1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->b:I

    .line 5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 6
    iput-wide v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->h:J

    .line 7
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 23
    const-string v1, "extra_phone_account_handle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 24
    return-object v0
.end method

.method public static d()J
    .locals 2

    .prologue
    .line 17
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 18
    return-wide v0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 19
    .line 20
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 21
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v1, v2}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcpe;)Lcom/android/voicemail/impl/scheduling/BaseTask;
    .locals 1

    .prologue
    .line 8
    invoke-static {}, Lbvs;->f()V

    .line 9
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 10
    return-object p0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 14
    invoke-static {}, Lbvs;->f()V

    .line 15
    iput-wide p1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->h:J

    .line 16
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 28
    iput-object p1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->f:Landroid/os/Bundle;

    .line 30
    const-string v0, "extra_phone_account_handle"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 31
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpe;

    .line 32
    invoke-interface {v0, p0, p2}, Lcpe;->a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/os/Bundle;)V

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->f:Landroid/os/Bundle;

    const-string v1, "extra_execution_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "extra_execution_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->h:J

    .line 37
    :cond_0
    return-void
.end method

.method public a(Lcph;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpe;

    .line 54
    invoke-interface {v0}, Lcpe;->c()V

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 11
    invoke-static {}, Lbvs;->g()V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->e:Z

    .line 13
    return-void
.end method

.method public final e()Lcpi;
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcpi;

    iget v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->b:I

    iget-object v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, v1, v2}, Lcpi;-><init>(ILandroid/telecom/PhoneAccountHandle;)V

    return-object v0
.end method

.method public final f()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 26
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->f:Landroid/os/Bundle;

    const-string v1, "extra_execution_time"

    iget-wide v2, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->h:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 27
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->f:Landroid/os/Bundle;

    return-object v0
.end method

.method public final g()J
    .locals 4

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->h:J

    .line 39
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 40
    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 43
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->d:Z

    .line 44
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->e:Z

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpe;

    .line 47
    invoke-interface {v0}, Lcpe;->b()V

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpe;

    .line 50
    invoke-interface {v0}, Lcpe;->a()V

    goto :goto_1

    .line 52
    :cond_1
    return-void
.end method
