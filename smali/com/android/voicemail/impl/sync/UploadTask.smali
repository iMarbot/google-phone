.class public Lcom/android/voicemail/impl/sync/UploadTask;
.super Lcom/android/voicemail/impl/scheduling/BaseTask;
.source "PG"


# annotations
.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;-><init>(I)V

    .line 2
    new-instance v0, Lcpf;

    const/16 v1, 0x1388

    invoke-direct {v0, v1}, Lcpf;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/sync/UploadTask;->a(Lcpe;)Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 3
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/android/voicemail/impl/sync/UploadTask;

    invoke-static {p0, v0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 6
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 7
    new-instance v0, Lcqb;

    .line 8
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 9
    invoke-direct {v0, v1}, Lcqb;-><init>(Landroid/content/Context;)V

    .line 11
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 13
    if-nez v1, :cond_0

    .line 14
    const-string v0, "VvmUploadTask"

    .line 15
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->c:Landroid/telecom/PhoneAccountHandle;

    .line 16
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "null phone account for phoneAccountHandle "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    :goto_0
    return-void

    .line 18
    :cond_0
    const/4 v2, 0x0

    .line 20
    iget-object v3, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 21
    invoke-static {v3, v1}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v3

    .line 22
    invoke-virtual {v0, p0, v1, v2, v3}, Lcqb;->a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/telecom/PhoneAccountHandle;Lclz;Lcnw;)V

    goto :goto_0
.end method
