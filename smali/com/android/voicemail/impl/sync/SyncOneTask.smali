.class public Lcom/android/voicemail/impl/sync/SyncOneTask;
.super Lcom/android/voicemail/impl/scheduling/BaseTask;
.source "PG"


# annotations
.annotation build Lcom/android/dialer/proguard/UsedByReflection;
.end annotation


# instance fields
.field private f:Landroid/telecom/PhoneAccountHandle;

.field private g:Lclz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 6
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/scheduling/BaseTask;-><init>(I)V

    .line 7
    new-instance v0, Lcpg;

    const/4 v1, 0x2

    const/16 v2, 0x1388

    invoke-direct {v0, v1, v2}, Lcpg;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/sync/SyncOneTask;->a(Lcpe;)Lcom/android/voicemail/impl/scheduling/BaseTask;

    .line 8
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lclz;)V
    .locals 2

    .prologue
    .line 1
    const-class v0, Lcom/android/voicemail/impl/sync/SyncOneTask;

    invoke-static {p0, v0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 2
    const-string v1, "extra_phone_account_handle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3
    const-string v1, "extra_voicemail"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 5
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 20
    .line 21
    iget-object v0, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 22
    sget-object v1, Lbkq$a;->be:Lbkq$a;

    invoke-static {v0, v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Lbkq$a;)V

    .line 23
    invoke-super {p0}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a()Landroid/content/Intent;

    move-result-object v0

    .line 24
    const-string v1, "extra_phone_account_handle"

    iget-object v2, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->f:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 25
    const-string v1, "extra_voicemail"

    iget-object v2, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->g:Lclz;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 26
    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0, p1, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 10
    const-string v0, "extra_phone_account_handle"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->f:Landroid/telecom/PhoneAccountHandle;

    .line 11
    const-string v0, "extra_voicemail"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lclz;

    iput-object v0, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->g:Lclz;

    .line 12
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 13
    new-instance v0, Lcqb;

    .line 14
    iget-object v1, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 15
    invoke-direct {v0, v1}, Lcqb;-><init>(Landroid/content/Context;)V

    .line 16
    iget-object v1, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->f:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->g:Lclz;

    .line 17
    iget-object v3, p0, Lcom/android/voicemail/impl/scheduling/BaseTask;->a:Landroid/content/Context;

    .line 18
    iget-object v4, p0, Lcom/android/voicemail/impl/sync/SyncOneTask;->f:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v3, v4}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcqb;->a(Lcom/android/voicemail/impl/scheduling/BaseTask;Landroid/telecom/PhoneAccountHandle;Lclz;Lcnw;)V

    .line 19
    return-void
.end method
