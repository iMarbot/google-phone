.class public Lcom/android/voicemail/impl/OmtpService;
.super Landroid/telephony/VisualVoicemailService;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telephony/VisualVoicemailService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 88
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 89
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.android.voicemail.impl.is_shutting_down"

    .line 90
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 92
    return-void
.end method

.method private final a()Z
    .locals 1

    .prologue
    .line 70
    invoke-static {p0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    invoke-interface {v0}, Lcln;->a()Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 86
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 87
    invoke-virtual {v0}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v0

    return v0
.end method

.method private final a(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 71
    new-instance v1, Lclu;

    invoke-direct {v1, p0, p1}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 72
    invoke-virtual {v1}, Lclu;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    const-string v1, "VvmOmtpService"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "VVM not supported on "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    return v0

    .line 75
    :cond_0
    invoke-static {p0, p1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 76
    invoke-virtual {v1}, Lclu;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 77
    const-string v1, "VvmOmtpService"

    const-string v2, "VVM is disabled"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final b(Landroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 80
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 81
    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/OmtpService;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    const-string v1, "VvmOmtpService"

    const-string v2, "disabling SMS filter"

    invoke-static {v1, v2}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->setVisualVoicemailSmsFilterSettings(Landroid/telephony/VisualVoicemailSmsFilterSettings;)V

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method public onCellServiceConnected(Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;Landroid/telecom/PhoneAccountHandle;)V
    .locals 2

    .prologue
    .line 2
    const-string v0, "VvmOmtpService"

    const-string v1, "onCellServiceConnected"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lcom/android/voicemail/impl/OmtpService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4
    const-string v0, "VvmOmtpService"

    const-string v1, "onCellServiceConnected received when module is disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    .line 18
    :goto_0
    return-void

    .line 7
    :cond_0
    invoke-static {p0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 8
    const-string v0, "VvmOmtpService"

    const-string v1, "onCellServiceConnected: user locked"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 11
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 12
    invoke-direct {p0, p2}, Lcom/android/voicemail/impl/OmtpService;->b(Landroid/telecom/PhoneAccountHandle;)V

    .line 13
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 15
    :cond_2
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aW:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 16
    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lcom/android/voicemail/impl/ActivationTask;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0
.end method

.method public onSimRemoved(Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 39
    const-string v0, "VvmOmtpService"

    const-string v1, "onSimRemoved"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/android/voicemail/impl/OmtpService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    const-string v0, "VvmOmtpService"

    const-string v1, "onSimRemoved called when module is disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    .line 58
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-static {p0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    const-string v0, "VvmOmtpService"

    const-string v1, "onSimRemoved: user locked"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 49
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "com.android.voicemail.impl.is_shutting_down"

    const/4 v2, 0x0

    .line 50
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    const-string v0, "VvmOmtpService"

    const-string v1, "onSimRemoved: system shutting down, ignoring"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 55
    :cond_2
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aW:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 56
    invoke-static {p0, p2}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 57
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0
.end method

.method public onSmsReceived(Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;Landroid/telephony/VisualVoicemailSms;)V
    .locals 2

    .prologue
    .line 19
    const-string v0, "VvmOmtpService"

    const-string v1, "onSmsReceived"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/android/voicemail/impl/OmtpService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    const-string v0, "VvmOmtpService"

    const-string v1, "onSmsReceived received when module is disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    .line 38
    :goto_0
    return-void

    .line 24
    :cond_0
    invoke-static {p0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 25
    invoke-static {p0, p2}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Landroid/content/Context;Landroid/telephony/VisualVoicemailSms;)V

    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {p2}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 28
    const-string v0, "VvmOmtpService"

    const-string v1, "onSmsReceived received when service is disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p2}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/voicemail/impl/OmtpService;->b(Landroid/telecom/PhoneAccountHandle;)V

    .line 30
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 32
    :cond_2
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aW:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 33
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vociemailomtp.sms.sms_received"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Lcom/android/voicemail/impl/OmtpService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    const-string v1, "extra_voicemail_sms"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 36
    invoke-virtual {p0, v0}, Lcom/android/voicemail/impl/OmtpService;->sendBroadcast(Landroid/content/Intent;)V

    .line 37
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0
.end method

.method public onStopped(Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;)V
    .locals 2

    .prologue
    .line 59
    const-string v0, "VvmOmtpService"

    const-string v1, "onStopped"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/android/voicemail/impl/OmtpService;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const-string v0, "VvmOmtpService"

    const-string v1, "onStopped called when module is disabled"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    .line 69
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-static {p0}, Lcom/android/voicemail/impl/OmtpService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    const-string v0, "VvmOmtpService"

    const-string v1, "onStopped: user locked"

    invoke-static {v0, v1}, Lcmd;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailService$VisualVoicemailTask;->finish()V

    goto :goto_0

    .line 68
    :cond_1
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aW:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_0
.end method
