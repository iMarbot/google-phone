.class public Lcom/android/contacts/common/list/ViewPagerTabs;
.super Landroid/widget/HorizontalScrollView;
.source "PG"

# interfaces
.implements Landroid/support/v4/view/ViewPager$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/contacts/common/list/ViewPagerTabs$a;
    }
.end annotation


# static fields
.field private static d:Landroid/view/ViewOutlineProvider;

.field private static e:[I


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field public b:[I

.field public c:[I

.field private f:I

.field private g:Landroid/content/res/ColorStateList;

.field private h:I

.field private i:Z

.field private j:I

.field private k:I

.field private l:Laik;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lail;

    invoke-direct {v0}, Lail;-><init>()V

    sput-object v0, Lcom/android/contacts/common/list/ViewPagerTabs;->d:Landroid/view/ViewOutlineProvider;

    .line 110
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/contacts/common/list/ViewPagerTabs;->e:[I

    return-void

    :array_0
    .array-data 4
        0x1010095
        0x1010097
        0x1010098
        0x101038c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/contacts/common/list/ViewPagerTabs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/contacts/common/list/ViewPagerTabs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    iput v4, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    .line 7
    invoke-virtual {p0, v3}, Lcom/android/contacts/common/list/ViewPagerTabs;->setFillViewport(Z)V

    .line 8
    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->k:I

    .line 9
    sget-object v0, Lcom/android/contacts/common/list/ViewPagerTabs;->e:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 10
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->h:I

    .line 11
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->f:I

    .line 12
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->g:Landroid/content/res/ColorStateList;

    .line 13
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->i:Z

    .line 14
    new-instance v1, Laik;

    invoke-direct {v1, p1}, Laik;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    .line 15
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 17
    sget-object v0, Lcom/android/contacts/common/list/ViewPagerTabs;->d:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/list/ViewPagerTabs;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 18
    return-void
.end method

.method private final a(Ljava/lang/CharSequence;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 33
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->b:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->b:[I

    array-length v0, v0

    if-ge p2, v0, :cond_2

    .line 34
    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400c0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 35
    const v0, 0x7f0e00a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 36
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->b:[I

    aget v0, v0, p2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 37
    invoke-virtual {v2, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 38
    const v0, 0x7f0e01a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 39
    iget-object v3, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    aget v3, v3, p2

    if-lez v3, :cond_1

    .line 40
    iget-object v3, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    aget v3, v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 43
    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f100005

    iget-object v4, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    aget v4, v4, p2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 44
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    aget v6, v6, p2

    .line 45
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    .line 46
    invoke-virtual {v0, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    move-object v0, v1

    .line 64
    :goto_1
    new-instance v1, Laim;

    invoke-direct {v1, p0, p2}, Laim;-><init>(Lcom/android/contacts/common/list/ViewPagerTabs;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    new-instance v1, Lcom/android/contacts/common/list/ViewPagerTabs$a;

    invoke-direct {v1, p0, p2}, Lcom/android/contacts/common/list/ViewPagerTabs$a;-><init>(Lcom/android/contacts/common/list/ViewPagerTabs;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 66
    iget v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->k:I

    iget v2, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->k:I

    invoke-virtual {v0, v1, v7, v2, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 67
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v0, p2, v2}, Laik;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 68
    if-nez p2, :cond_0

    .line 69
    iput v7, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    .line 70
    invoke-virtual {v0, v8}, Landroid/view/View;->setSelected(Z)V

    .line 71
    :cond_0
    return-void

    .line 48
    :cond_1
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 49
    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f1102f1

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 52
    :cond_2
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    const v1, 0x7f02019a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 55
    iget v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->f:I

    if-lez v1, :cond_3

    .line 56
    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    iget v2, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 57
    :cond_3
    iget v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->h:I

    if-lez v1, :cond_4

    .line 58
    iget v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 59
    :cond_4
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->g:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_5

    .line 60
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 61
    :cond_5
    iget-boolean v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->i:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 62
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/android/contacts/common/list/ViewPagerTabs;->e(I)I

    move-result v0

    .line 85
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v1}, Laik;->getChildCount()I

    move-result v1

    .line 86
    if-eqz v1, :cond_0

    if-ltz v0, :cond_0

    if-lt v0, v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    .line 89
    iput v0, v1, Laik;->a:I

    .line 90
    iput p2, v1, Laik;->b:F

    .line 91
    invoke-virtual {v1}, Laik;->invalidate()V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    array-length v0, v0

    if-lt p2, v0, :cond_1

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->c:[I

    aput p1, v0, p2

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 4

    .prologue
    .line 19
    iput-object p1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->a:Landroid/support/v4/view/ViewPager;

    .line 20
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->a:Landroid/support/v4/view/ViewPager;

    .line 21
    iget-object v1, v0, Landroid/support/v4/view/ViewPager;->b:Lqv;

    .line 23
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v0}, Laik;->removeAllViews()V

    .line 24
    invoke-virtual {v1}, Lqv;->b()I

    move-result v2

    .line 25
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 26
    invoke-virtual {v1, v0}, Lqv;->c(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(Ljava/lang/CharSequence;I)V

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 93
    invoke-virtual {p0, p1}, Lcom/android/contacts/common/list/ViewPagerTabs;->e(I)I

    move-result v0

    .line 94
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v1}, Laik;->getChildCount()I

    move-result v1

    .line 95
    if-eqz v1, :cond_0

    if-ltz v0, :cond_0

    if-lt v0, v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget v2, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    if-ge v2, v1, :cond_2

    .line 98
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    iget v2, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    invoke-virtual {v1, v2}, Laik;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v1, v0}, Laik;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 100
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 101
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    .line 102
    invoke-virtual {p0, v1, v4}, Lcom/android/contacts/common/list/ViewPagerTabs;->smoothScrollTo(II)V

    .line 103
    iput v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->j:I

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v0, p1}, Laik;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v1, v0}, Laik;->removeView(Landroid/view/View;)V

    .line 75
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/contacts/common/list/ViewPagerTabs;->c(I)V

    .line 77
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->a:Landroid/support/v4/view/ViewPager;

    .line 78
    iget-object v0, v0, Landroid/support/v4/view/ViewPager;->b:Lqv;

    .line 79
    invoke-virtual {v0}, Lqv;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->a:Landroid/support/v4/view/ViewPager;

    .line 81
    iget-object v0, v0, Landroid/support/v4/view/ViewPager;->b:Lqv;

    .line 82
    invoke-virtual {v0, p1}, Lqv;->c(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/android/contacts/common/list/ViewPagerTabs;->a(Ljava/lang/CharSequence;I)V

    .line 83
    :cond_0
    return-void
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/android/contacts/common/list/ViewPagerTabs;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/android/contacts/common/list/ViewPagerTabs;->l:Laik;

    invoke-virtual {v0}, Laik;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    .line 108
    :cond_0
    return p1
.end method
