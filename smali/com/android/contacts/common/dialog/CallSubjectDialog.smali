.class public Lcom/android/contacts/common/dialog/CallSubjectDialog;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private A:I

.field private B:Landroid/view/View$OnClickListener;

.field private C:Landroid/view/View$OnClickListener;

.field private D:Landroid/widget/AdapterView$OnItemClickListener;

.field public a:I

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Landroid/widget/EditText;

.field public e:Landroid/widget/ListView;

.field public f:Landroid/content/SharedPreferences;

.field public g:Ljava/util/List;

.field public h:Ljava/lang/String;

.field public i:Landroid/telecom/PhoneAccountHandle;

.field private j:Ljava/nio/charset/Charset;

.field private k:Landroid/widget/QuickContactBadge;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:I

.field private r:Landroid/text/TextWatcher;

.field private s:Landroid/view/View$OnClickListener;

.field private t:Landroid/view/View$OnClickListener;

.field private u:J

.field private v:Landroid/net/Uri;

.field private w:Landroid/net/Uri;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 2
    const/16 v0, 0x10

    iput v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    .line 3
    new-instance v0, Lagl;

    invoke-direct {v0, p0}, Lagl;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->r:Landroid/text/TextWatcher;

    .line 4
    new-instance v0, Lagm;

    invoke-direct {v0, p0}, Lagm;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->s:Landroid/view/View$OnClickListener;

    .line 5
    new-instance v0, Lagn;

    invoke-direct {v0, p0}, Lagn;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->t:Landroid/view/View$OnClickListener;

    .line 6
    new-instance v0, Lago;

    invoke-direct {v0, p0}, Lago;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->B:Landroid/view/View$OnClickListener;

    .line 7
    new-instance v0, Lagp;

    invoke-direct {v0, p0}, Lagp;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->C:Landroid/view/View$OnClickListener;

    .line 8
    new-instance v0, Lagq;

    invoke-direct {v0, p0}, Lagq;-><init>(Lcom/android/contacts/common/dialog/CallSubjectDialog;)V

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->D:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 24
    const-string v1, "subject_history_count"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    :goto_0
    if-ge v0, v1, :cond_1

    .line 27
    const/16 v3, 0x1f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "subject_history_item"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 29
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_1
    return-object v2
.end method

.method public static a(Landroid/app/Activity;JLandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    .line 9
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 10
    const-string v1, "PHOTO_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 11
    const-string v1, "PHOTO_URI"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 12
    const-string v1, "CONTACT_URI"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    const-string v1, "NAME_OR_NUMBER"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v1, "NUMBER"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v1, "DISPLAY_NUMBER"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const-string v1, "NUMBER_LABEL"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v1, "CONTACT_TYPE"

    invoke-virtual {v0, v1, p9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 18
    const-string v1, "PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v0, v1, p10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 20
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/contacts/common/dialog/CallSubjectDialog;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 21
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 22
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->j:Ljava/nio/charset/Charset;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->j:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    array-length v0, v0

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->n:Landroid/widget/TextView;

    const v2, 0x7f1100ae

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    if-lt v0, v1, :cond_1

    .line 112
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->n:Landroid/widget/TextView;

    .line 113
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 114
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    :goto_1
    return-void

    .line 109
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->n:Landroid/widget/TextView;

    .line 116
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 117
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v4, -0x1

    const/4 v11, 0x1

    const/16 v9, 0x8

    const/4 v10, 0x0

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a:I

    .line 34
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->f:Landroid/content/SharedPreferences;

    .line 36
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 37
    if-nez v1, :cond_1

    .line 38
    const-string v0, "CallSubjectDialog.readArguments"

    const-string v1, "arguments cannot be null"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->i:Landroid/telecom/PhoneAccountHandle;

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "telecom"

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 53
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->i:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    const-string v1, "android.telecom.extra.CALL_SUBJECT_MAX_LENGTH"

    iget v2, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    .line 57
    const-string v1, "android.telecom.extra.CALL_SUBJECT_CHARACTER_ENCODING"

    .line 58
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 60
    :try_start_0
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    iput-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->j:Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->f:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->g:Ljava/util/List;

    .line 66
    const v0, 0x7f04004d

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->setContentView(I)V

    .line 67
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setLayout(II)V

    .line 68
    const v0, 0x7f0e0173

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->b:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v0, 0x7f0e0174

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->c:Landroid/view/View;

    .line 71
    const v0, 0x7f0e0101

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->k:Landroid/widget/QuickContactBadge;

    .line 72
    const v0, 0x7f0e0126

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->l:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0e0142

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->m:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0e0175

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    .line 75
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->r:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    new-array v0, v11, [Landroid/text/InputFilter;

    .line 78
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->q:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v10

    .line 79
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 80
    const v0, 0x7f0e0176

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->n:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0e0177

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->o:Landroid/view/View;

    .line 82
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v9

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 84
    const v0, 0x7f0e0108

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->p:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v0, 0x7f0e0178

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    .line 87
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->D:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->w:Landroid/net/Uri;

    if-eqz v0, :cond_5

    .line 91
    invoke-static {p0}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->k:Landroid/widget/QuickContactBadge;

    iget-object v3, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->w:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->u:J

    iget-object v6, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->v:Landroid/net/Uri;

    iget-object v7, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->x:Ljava/lang/String;

    iget v8, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->A:I

    .line 92
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 94
    :goto_3
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 96
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v1, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->m:Landroid/widget/TextView;

    .line 98
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 99
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->y:Ljava/lang/String;

    .line 101
    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :goto_5
    invoke-virtual {p0}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->a()V

    .line 105
    return-void

    .line 40
    :cond_1
    const-string v0, "PHOTO_ID"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->u:J

    .line 41
    const-string v0, "PHOTO_URI"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->v:Landroid/net/Uri;

    .line 42
    const-string v0, "CONTACT_URI"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->w:Landroid/net/Uri;

    .line 43
    const-string v0, "NAME_OR_NUMBER"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->x:Ljava/lang/String;

    .line 44
    const-string v0, "NUMBER"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->h:Ljava/lang/String;

    .line 45
    const-string v0, "DISPLAY_NUMBER"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->y:Ljava/lang/String;

    .line 46
    const-string v0, "NUMBER_LABEL"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->z:Ljava/lang/String;

    .line 47
    const-string v0, "CONTACT_TYPE"

    invoke-virtual {v1, v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->A:I

    .line 48
    const-string v0, "PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    iput-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->i:Landroid/telecom/PhoneAccountHandle;

    goto/16 :goto_0

    .line 63
    :catch_0
    move-exception v1

    const-string v1, "CallSubjectDialog.loadConfiguration"

    const-string v2, "invalid charset: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    :cond_2
    iput-object v12, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->j:Ljava/nio/charset/Charset;

    goto/16 :goto_1

    .line 63
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_4
    move v0, v10

    .line 83
    goto/16 :goto_2

    .line 93
    :cond_5
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->k:Landroid/widget/QuickContactBadge;

    invoke-virtual {v0, v9}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    goto/16 :goto_3

    .line 100
    :cond_6
    const v0, 0x7f1100af

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->z:Ljava/lang/String;

    aput-object v3, v2, v10

    iget-object v3, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->y:Ljava/lang/String;

    aput-object v3, v2, v11

    invoke-virtual {p0, v0, v2}, Lcom/android/contacts/common/dialog/CallSubjectDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 102
    :cond_7
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/android/contacts/common/dialog/CallSubjectDialog;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method
