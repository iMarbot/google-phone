.class public final Lcom/android/contacts/common/preference/SortOrderPreference;
.super Landroid/preference/ListPreference;
.source "PG"


# instance fields
.field private a:Lalj;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 2
    invoke-direct {p0}, Lcom/android/contacts/common/preference/SortOrderPreference;->a()V

    .line 3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 5
    invoke-direct {p0}, Lcom/android/contacts/common/preference/SortOrderPreference;->a()V

    .line 6
    return-void
.end method

.method private final a()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 7
    invoke-virtual {p0}, Lcom/android/contacts/common/preference/SortOrderPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    .line 8
    new-instance v0, Lalj;

    iget-object v1, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lalj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->a:Lalj;

    .line 9
    new-array v0, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    const v2, 0x7f110156

    .line 10
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    const v2, 0x7f110155

    .line 11
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 12
    invoke-virtual {p0, v0}, Lcom/android/contacts/common/preference/SortOrderPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 13
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "1"

    .line 14
    aput-object v1, v0, v3

    const-string v1, "2"

    .line 15
    aput-object v1, v0, v4

    .line 16
    invoke-virtual {p0, v0}, Lcom/android/contacts/common/preference/SortOrderPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 17
    iget-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->a:Lalj;

    invoke-virtual {v0}, Lalj;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/contacts/common/preference/SortOrderPreference;->setValue(Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method public final getSummary()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->a:Lalj;

    invoke-virtual {v0}, Lalj;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 23
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 21
    :pswitch_0
    iget-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    const v1, 0x7f110156

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 22
    :pswitch_1
    iget-object v0, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->b:Landroid/content/Context;

    const v1, 0x7f110155

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 30
    invoke-virtual {p1, v0, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 31
    return-void
.end method

.method protected final persistString(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 24
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 25
    iget-object v1, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->a:Lalj;

    invoke-virtual {v1}, Lalj;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 26
    iget-object v1, p0, Lcom/android/contacts/common/preference/SortOrderPreference;->a:Lalj;

    invoke-virtual {v1, v0}, Lalj;->a(I)V

    .line 27
    invoke-virtual {p0}, Lcom/android/contacts/common/preference/SortOrderPreference;->notifyChanged()V

    .line 28
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected final shouldPersist()Z
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    return v0
.end method
