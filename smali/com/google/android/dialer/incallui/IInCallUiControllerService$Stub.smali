.class public abstract Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/dialer/incallui/IInCallUiControllerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/dialer/incallui/IInCallUiControllerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_getVersion:I = 0x1

.field public static final TRANSACTION_showDialog:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.dialer.incallui.IInCallUiControllerService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/dialer/incallui/IInCallUiControllerService;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.dialer.incallui.IInCallUiControllerService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/dialer/incallui/IInCallUiControllerService;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/dialer/incallui/IInCallUiControllerService;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 21
    :goto_0
    return v0

    .line 12
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 20
    const/4 v0, 0x0

    goto :goto_0

    .line 13
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub;->getVersion()I

    move-result v0

    .line 14
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    move v0, v1

    .line 21
    goto :goto_0

    .line 17
    :pswitch_1
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 18
    invoke-virtual {p0, v0}, Lcom/google/android/dialer/incallui/IInCallUiControllerService$Stub;->showDialog(Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 12
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
