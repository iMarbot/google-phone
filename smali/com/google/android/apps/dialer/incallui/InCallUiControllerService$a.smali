.class public final Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwq;
.implements Lbwr;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/dialer/incallui/InCallUiControllerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private a:Lbwg;

.field private b:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Lbwg;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    .line 3
    iput-object p2, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->b:Landroid/app/PendingIntent;

    .line 4
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwq;)V

    .line 5
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    invoke-virtual {v0, p0}, Lbwg;->a(Lbwr;)V

    .line 6
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 13
    const-string v0, "evaluateShowDialog"

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    .line 15
    iget-object v0, v0, Lbwg;->n:Lbwp;

    .line 16
    sget-object v1, Lbwp;->e:Lbwp;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    .line 17
    invoke-virtual {v0}, Lbwg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    :try_start_0
    const-string v0, "evaluateShowDialog, showing dialog"

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->b()V

    .line 24
    :cond_0
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    const-string v1, "Unable to show requested dialog"

    invoke-static {p0, v1, v0}, Lbvs;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Lbwp;Lbwp;Lcct;)V
    .locals 1

    .prologue
    .line 10
    const-string v0, "onStateChanged"

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a()V

    .line 12
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 7
    const-string v0, "onUiShowing"

    invoke-static {p0, v0}, Lbvs;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a()V

    .line 9
    return-void
.end method

.method final b()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwq;)V

    .line 26
    iget-object v0, p0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService$a;->a:Lbwg;

    invoke-virtual {v0, p0}, Lbwg;->b(Lbwr;)Z

    .line 27
    sget-object v0, Lcom/google/android/apps/dialer/incallui/InCallUiControllerService;->a:Ljava/util/HashSet;

    .line 28
    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method
