.class public Lcom/google/android/apps/dialer/app/GoogleDialerApplication;
.super Ldio;
.source "PG"

# interfaces
.implements Lagy;
.implements Laqw;
.implements Lbgj;
.implements Lbkv;
.implements Lblv;
.implements Lblx;
.implements Lbmo;
.implements Lbsf;
.implements Lccn;
.implements Lcdn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ldio;-><init>()V

    return-void
.end method


# virtual methods
.method protected final c()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2
    new-instance v1, Ldpc;

    .line 3
    invoke-direct {v1}, Ldpc;-><init>()V

    .line 4
    new-instance v0, Lbka;

    invoke-direct {v0, p0}, Lbka;-><init>(Landroid/content/Context;)V

    .line 5
    invoke-static {v0}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbka;

    iput-object v0, v1, Ldpc;->a:Lbka;

    .line 6
    iget-object v0, v1, Ldpc;->a:Lbka;

    if-nez v0, :cond_0

    .line 7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-class v1, Lbka;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    iget-object v0, v1, Ldpc;->b:Ldjg;

    if-nez v0, :cond_1

    .line 9
    new-instance v0, Ldjg;

    invoke-direct {v0}, Ldjg;-><init>()V

    iput-object v0, v1, Ldpc;->b:Ldjg;

    .line 10
    :cond_1
    iget-object v0, v1, Ldpc;->c:Ldom;

    if-nez v0, :cond_2

    .line 11
    new-instance v0, Ldom;

    invoke-direct {v0}, Ldom;-><init>()V

    iput-object v0, v1, Ldpc;->c:Ldom;

    .line 12
    :cond_2
    new-instance v0, Ldpb;

    .line 13
    invoke-direct {v0, v1}, Ldpb;-><init>(Ldpc;)V

    .line 14
    return-object v0
.end method
