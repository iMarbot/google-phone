.class public Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;
.super Lcom/android/dialer/app/DialtactsActivity;
.source "PG"

# interfaces
.implements Lbgc$a;
.implements Ldjd$a;
.implements Ldni;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity$a;
    }
.end annotation


# static fields
.field public static final x:Landroid/net/Uri;

.field private static y:Z

.field private static z:Z


# instance fields
.field private A:Ldnf;

.field private B:Ldnr;

.field private C:Lbdy;

.field private D:Ldjd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    const-string v0, "https://support.google.com/phoneapp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->x:Landroid/net/Uri;

    .line 165
    sput-boolean v1, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->y:Z

    .line 166
    sput-boolean v1, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->z:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/android/dialer/app/DialtactsActivity;-><init>()V

    return-void
.end method

.method public static final synthetic a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 163
    const-string v0, "GoogleDialtactsActivity.onMenuItemClick"

    const-string v1, "fetch log failed"

    invoke-static {v0, v1, p0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.apps.dialer.GO_EXPERIENCE"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 155
    :cond_1
    const-string v0, "GoogleDialtactsActivity.checkDialerFeature"

    const-string v2, "dialer go system feature is not found!"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 156
    goto :goto_0

    .line 157
    :cond_2
    invoke-static {}, Lbw;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.apps.dialer.SUPPORTED"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    const-string v0, "GoogleDialtactsActivity.checkDialerFeature"

    const-string v2, "dialer system feature is not found!"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 162
    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/view/View;)Lcom/android/dialer/app/DialtactsActivity$b;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 114
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/view/View;)Lcom/android/dialer/app/DialtactsActivity$b;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lcom/android/dialer/app/DialtactsActivity$b;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0e0034

    const v3, 0x7f110190

    .line 117
    invoke-virtual {p0, v3}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 118
    invoke-interface {v1, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 119
    return-object v0
.end method

.method public final a(Ldnj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    iget v0, p1, Ldnj;->d:I

    if-nez v0, :cond_0

    iget-boolean v0, p1, Ldnj;->a:Z

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p1, Ldnj;->a:Z

    .line 107
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p1, Ldnj;->d:I

    .line 108
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    .line 110
    invoke-virtual {v0, v3, v3}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->f:Lcom/android/dialer/dialpadview/DialpadFragment;

    iget-object v1, p1, Ldnj;->c:Ljava/lang/String;

    iget-object v2, p1, Ldnj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/dialer/dialpadview/DialpadFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->a(Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->A:Ldnf;

    .line 95
    iget-boolean v0, v1, Ldnf;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Ldnf;->b:Lcom/google/android/apps/hangouts/telephony/ITeleHangoutsService;

    if-nez v0, :cond_1

    .line 96
    :cond_0
    const-string v0, "Connection is not open; ignoring getCallRate operation"

    invoke-static {v1, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, v1, Ldnf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    const-string v2, "Already an in-flight request for "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lbvs;->b(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_3
    new-instance v0, Ldnh;

    invoke-direct {v0, v1, p1, p0}, Ldnh;-><init>(Ldnf;Ljava/lang/String;Ldni;)V

    .line 102
    iget-object v1, v1, Ldnf;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldnh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 120
    .line 121
    iget-object v0, p0, Lcom/android/dialer/app/DialtactsActivity;->k:Larl;

    invoke-virtual {v0}, Larl;->getUserVisibleHint()Z

    move-result v0

    .line 122
    if-eqz v0, :cond_0

    .line 124
    iget v0, p0, Lcom/android/dialer/app/DialtactsActivity;->v:I

    .line 125
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    .line 125
    :cond_0
    const/4 v0, 0x0

    .line 126
    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->b(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "contacts_promo_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Ldjd;

    iput-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    if-nez v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldjd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    new-instance v0, Ldjd;

    invoke-direct {v0}, Ldjd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0e019a

    iget-object v2, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    const-string v3, "contacts_promo_fragment"

    .line 78
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    if-eqz v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    invoke-virtual {v0}, Ldjd;->a()V

    .line 82
    :cond_2
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->b(Z)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    .line 86
    if-eqz p1, :cond_0

    iget-object v1, v0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    .line 88
    iget v1, v1, Landroid/support/design/widget/BottomSheetBehavior;->f:I

    .line 89
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 90
    const-string v1, "ContactsPromoFragment.onContactsListScrolled"

    const-string v2, "Promo collapsed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, v0, Ldjd;->a:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->a(I)V

    .line 92
    :cond_0
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 131
    sget-object v0, Ldny;->s:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 133
    invoke-static {p0}, Ldhh;->k(Landroid/content/Context;)Z

    move-result v1

    .line 134
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 135
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/dialer/settings/GoogleDialerSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    invoke-virtual {p0, v0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->startActivity(Landroid/content/Intent;)V

    .line 139
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-super {p0}, Lcom/android/dialer/app/DialtactsActivity;->f()V

    goto :goto_0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 2
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11020b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 4
    return v0
.end method

.method protected final m()I
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6
    const v0, 0x7f110133

    .line 7
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f110132

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 140
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    .line 141
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->B:Ldnr;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->B:Ldnr;

    .line 143
    invoke-virtual {v0}, Ldnr;->i()V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/android/dialer/app/DialtactsActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->onAttachFragment(Landroid/app/Fragment;)V

    .line 62
    instance-of v0, p1, Larl;

    if-nez v0, :cond_0

    instance-of v0, p1, Ldjd;

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->D:Ldjd;

    invoke-virtual {v0}, Ldjd;->b()V

    .line 65
    :cond_0
    instance-of v0, p1, Ldnr;

    if-eqz v0, :cond_1

    .line 66
    check-cast p1, Ldnr;

    iput-object p1, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->B:Ldnr;

    .line 67
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8
    const-string v0, "GoogleDialtactsActivity.onCreate"

    invoke-static {p0, v0}, Ldoq;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 9
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 10
    const-class v0, Landroid/telecom/TelecomManager;

    .line 11
    invoke-virtual {p0, v0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/telecom/TelecomManager;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 13
    new-instance v3, Ldnf;

    invoke-direct {v3, p0, v0}, Ldnf;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 14
    iput-object v3, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->A:Ldnf;

    .line 15
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Ldio;

    .line 18
    iget-object v3, v0, Ldio;->a:Lfco;

    if-eqz v3, :cond_4

    iget-object v0, v0, Ldio;->a:Lfco;

    .line 19
    iget-object v0, v0, Lfco;->a:Lfgc;

    if-eqz v0, :cond_3

    move v0, v1

    .line 20
    :goto_0
    if-eqz v0, :cond_4

    move v0, v1

    .line 21
    :goto_1
    if-eqz v0, :cond_0

    .line 23
    invoke-static {p0}, Lbsw;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    const-string v0, "GoogleDialtactsActivity.ensureMicrophonePermission"

    const-string v3, "requesting microphone permission"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "android.permission.RECORD_AUDIO"

    aput-object v3, v0, v2

    const/16 v3, 0x3e9

    invoke-static {p0, v0, v3}, Lid;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 27
    :cond_0
    invoke-static {}, Lapw;->b()I

    move-result v0

    const/4 v3, 0x5

    if-eq v0, v3, :cond_2

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b000b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 29
    invoke-static {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->a(Landroid/content/Context;)Z

    move-result v1

    .line 40
    :cond_1
    :goto_2
    if-nez v1, :cond_2
	goto :cond_2

    .line 41
    new-instance v0, Lug;

    const v1, 0x7f120005

    invoke-direct {v0, p0, v1}, Lug;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f110130

    .line 43
    iget-object v2, v0, Lug;->a:Lub;

    iget-object v3, v0, Lug;->a:Lub;

    iget-object v3, v3, Lub;->a:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v2, Lub;->f:Ljava/lang/CharSequence;

    .line 45
    invoke-virtual {v0}, Lug;->a()Luf;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Luf;->show()V

    .line 48
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "fetch_log_task"

    new-instance v3, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity$a;

    .line 51
    invoke-direct {v3}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity$a;-><init>()V

    .line 52
    invoke-virtual {v0, v1, v2, v3}, Lbef;->a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Ldnb;

    invoke-direct {v1, p0}, Ldnb;-><init>(Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;)V

    .line 53
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    sget-object v1, Ldnc;->a:Lbea;

    .line 54
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->C:Lbdy;

    .line 56
    const-string v0, "GoogleDialtactsActivity.onCreate"

    invoke-static {v0}, Ldoq;->a(Ljava/lang/String;)V

    .line 57
    return-void

    :cond_3
    move v0, v2

    .line 19
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 20
    goto/16 :goto_1

    .line 30
    :cond_5
    sget-boolean v0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->y:Z

    if-nez v0, :cond_6

    .line 32
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v2

    :goto_3
    if-ge v0, v4, :cond_9

    aget-object v5, v3, v0

    .line 33
    const-string v6, "com.google.android.dialer.support"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    move v0, v1

    .line 37
    :goto_4
    sput-boolean v0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->z:Z

    .line 38
    sput-boolean v1, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->y:Z

    .line 39
    :cond_6
    sget-boolean v0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->z:Z

    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_7
    move v1, v2

    goto/16 :goto_2

    .line 35
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    move v0, v2

    .line 36
    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->A:Ldnf;

    invoke-virtual {v0}, Ldnf;->a()V

    .line 59
    invoke-super {p0}, Lcom/android/dialer/app/DialtactsActivity;->onDestroy()V

    .line 60
    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e0034

    if-ne v0, v1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/dialer/extensions/GoogleDialtactsActivity;->C:Lbdy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    .line 129
    const/4 v0, 0x1

    .line 130
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/dialer/app/DialtactsActivity;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 147
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 148
    array-length v0, p3

    if-lez v0, :cond_1

    aget v0, p3, v2

    if-nez v0, :cond_1

    .line 149
    const-string v0, "GoogleDialtactsActivity.onRequestPermissionsResult"

    const-string v1, "microphone permission granted"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    const-string v0, "GoogleDialtactsActivity.onRequestPermissionsResult"

    const-string v1, "microphone permission denied"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
