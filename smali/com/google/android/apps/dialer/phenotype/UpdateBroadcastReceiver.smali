.class public final Lcom/google/android/apps/dialer/phenotype/UpdateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2
    const-string v0, "com.google.android.gms.phenotype.UPDATE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phenotype/UpdateBroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v0

    .line 5
    invoke-static {p1}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v1

    invoke-virtual {v1}, Lbed;->a()Lbef;

    move-result-object v1

    new-instance v2, Ldoh;

    invoke-direct {v2, v0}, Ldoh;-><init>(Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 6
    invoke-static {p1, v1, v2}, Ldny;->a(Landroid/content/Context;Lbef;Lbeb;)V

    .line 7
    :cond_0
    return-void
.end method
