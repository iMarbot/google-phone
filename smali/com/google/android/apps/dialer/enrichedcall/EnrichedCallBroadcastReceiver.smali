.class public final Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field private static a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 319
    const-string v0, "^geo:-?[0-9]+(\\.[0-9]*)?,-?[0-9]+(\\.[0-9]*)?"

    .line 320
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->a:Ljava/util/regex/Pattern;

    .line 321
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static final synthetic a(Lbjl;)Z
    .locals 1

    .prologue
    .line 318
    invoke-interface {p0}, Lbjl;->e()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    .line 2
    invoke-static {p1}, Lbjd;->a(Landroid/content/Context;)Lbjd;

    move-result-object v0

    invoke-virtual {v0}, Lbjd;->a()Lbjf;

    move-result-object v2

    .line 3
    const-string v0, "dialer.rcs.intent.action.callCapabilitiesUpdate"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 6
    const-string v0, "rcs.intent.extra.userId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 7
    const-string v0, "EnrichedCallBroadcastReceiver.isValidCapabilitiesUpdate"

    const-string v1, "missing userId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-nez v0, :cond_4

    .line 17
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->f:Ldnt$a;

    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 317
    :cond_0
    :goto_1
    return-void

    .line 9
    :cond_1
    const-string v0, "rcs.intent.extra.callComposerSupported"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 10
    const-string v0, "EnrichedCallBroadcastReceiver.isValidCapabilitiesUpdate"

    const-string v1, "missing isCallComposerSupported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    const/4 v0, 0x0

    goto :goto_0

    .line 12
    :cond_2
    const-string v0, "rcs.intent.extra.postCallSupported"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 13
    const-string v0, "EnrichedCallBroadcastReceiver.isValidCapabilitiesUpdate"

    const-string v1, "missing isPostCallSupported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    const/4 v0, 0x0

    goto :goto_0

    .line 15
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 19
    :cond_4
    const-string v0, "rcs.intent.extra.callComposerSupported"

    const/4 v1, 0x0

    .line 20
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 21
    const-string v1, "rcs.intent.extra.postCallSupported"

    const/4 v3, 0x0

    .line 22
    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 23
    const-string v3, "rcs.intent.extra.videoShareSupported"

    const/4 v4, 0x0

    .line 24
    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 25
    const-string v4, "rcs.intent.extra.temporarilyOffline"

    const/4 v5, 0x0

    .line 26
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 27
    const-string v5, "rcs.intent.extra.userId"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 28
    const-string v6, "EnrichedCallBroadcastReceiver.handleCallCapabilitiesUpdate"

    const-string v7, "valid capabilities update, number: %s, callComposer: %b, postCall: %b, videoShare: %b"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 29
    invoke-static {v5}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    .line 31
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    .line 32
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    .line 33
    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    invoke-static {}, Lbjb;->f()Lbjc;

    move-result-object v6

    .line 36
    invoke-virtual {v6, v0}, Lbjc;->a(Z)Lbjc;

    move-result-object v0

    .line 37
    invoke-virtual {v0, v1}, Lbjc;->b(Z)Lbjc;

    move-result-object v0

    .line 38
    invoke-virtual {v0, v3}, Lbjc;->c(Z)Lbjc;

    move-result-object v0

    .line 39
    invoke-virtual {v0, v4}, Lbjc;->d(Z)Lbjc;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lbjc;->a()Lbjb;

    move-result-object v0

    .line 41
    invoke-interface {v2, v5, v0}, Lbjf;->a(Ljava/lang/String;Lbjb;)V

    goto/16 :goto_1

    .line 43
    :cond_5
    const-string v0, "dialer.rcs.intent.action.sessionStatusUpdate"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 46
    const-string v0, "rcs.intent.extra.sessionid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 47
    const-string v0, "EnrichedCallBroadcastReceiver.isValidSessionStatusUpdate"

    const-string v1, "missing sessionId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    const/4 v0, 0x0

    .line 62
    :goto_2
    if-nez v0, :cond_a

    .line 63
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->g:Ldnt$a;

    .line 64
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 49
    :cond_6
    const-string v0, "rcs.intent.extra.userId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 50
    const-string v0, "EnrichedCallBroadcastReceiver.isValidSessionStatusUpdate"

    const-string v1, "missing userId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const/4 v0, 0x0

    goto :goto_2

    .line 52
    :cond_7
    const-string v0, "rcs.intent.extra.status"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 53
    const-string v0, "EnrichedCallBroadcastReceiver.isValidSessionStatusUpdate"

    const-string v1, "missing session state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    const/4 v0, 0x0

    goto :goto_2

    .line 55
    :cond_8
    const-string v0, "rcs.intent.extra.status"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 56
    invoke-static {v0}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->isSessionState(I)Z

    move-result v1

    if-nez v1, :cond_9

    .line 57
    const-string v1, "EnrichedCallBroadcastReceiver.isValidSessionStatusUpdate"

    const-string v3, "invalid session state: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 58
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    .line 59
    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    const/4 v0, 0x0

    goto :goto_2

    .line 61
    :cond_9
    const/4 v0, 0x1

    goto :goto_2

    .line 66
    :cond_a
    const-string v0, "rcs.intent.extra.sessionid"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 67
    const-string v3, "rcs.intent.extra.userId"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 68
    const-string v4, "rcs.intent.extra.status"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 69
    const-string v5, "EnrichedCallBroadcastReceiver.handleSessionStatusUpdate"

    const-string v6, "valid update received for sessionId: %s, number: %s, sessionState: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 70
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    .line 71
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    .line 72
    invoke-static {v4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->sessionStateToString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 73
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    invoke-interface {v2, v0, v1, v3, v4}, Lbjf;->a(JLjava/lang/String;I)V

    goto/16 :goto_1

    .line 76
    :cond_b
    const-string v0, "dialer.rcs.intent.action.messageStatusUpdate"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 79
    const-string v0, "rcs.intent.extra.sessionid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 80
    const-string v0, "EnrichedCallBroadcastReceiver.isValidMessageStatusUpdate"

    const-string v1, "missing sessionId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    const/4 v0, 0x0

    .line 98
    :goto_3
    if-nez v0, :cond_11

    .line 99
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->h:Ldnt$a;

    .line 100
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 82
    :cond_c
    const-string v0, "rcs.intent.extra.userId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_d

    .line 83
    const-string v0, "EnrichedCallBroadcastReceiver.isValidMessageStatusUpdate"

    const-string v1, "missing userId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    const/4 v0, 0x0

    goto :goto_3

    .line 85
    :cond_d
    const-string v0, "rcs.intent.extra.messageid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    .line 86
    const-string v0, "EnrichedCallBroadcastReceiver.isValidMessageStatusUpdate"

    const-string v1, "missing messageId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    const/4 v0, 0x0

    goto :goto_3

    .line 88
    :cond_e
    const-string v0, "rcs.intent.extra.status"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 89
    const-string v0, "EnrichedCallBroadcastReceiver.isValidMessageStatusUpdate"

    const-string v1, "missing message state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    const/4 v0, 0x0

    goto :goto_3

    .line 91
    :cond_f
    const-string v0, "rcs.intent.extra.status"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 92
    invoke-static {v0}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->isMessageState(I)Z

    move-result v1

    if-nez v1, :cond_10

    .line 93
    const-string v1, "EnrichedCallBroadcastReceiver.isValidMessageStatusUpdate"

    const-string v3, "invalid message state: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 94
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    .line 95
    invoke-static {v1, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    const/4 v0, 0x0

    goto :goto_3

    .line 97
    :cond_10
    const/4 v0, 0x1

    goto :goto_3

    .line 102
    :cond_11
    const-string v0, "rcs.intent.extra.sessionid"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 103
    invoke-interface {v2, v0, v1}, Lbjf;->b(J)Lbjl;

    move-result-object v3

    if-nez v3, :cond_12

    .line 104
    const-string v2, "EnrichedCallBroadcastReceiver.handleMessageStatusUpdate"

    const-string v3, "no session for sessionId: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 105
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    .line 106
    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->i:Ldnt$a;

    .line 108
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 110
    :cond_12
    const-string v3, "rcs.intent.extra.messageid"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    const-string v4, "rcs.intent.extra.status"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 112
    const-string v5, "EnrichedCallBroadcastReceiver.handleMessageStatusUpdate"

    const-string v6, "valid update received for sessionId: %s, messageId: %s, messageState: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 113
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v3, v7, v8

    const/4 v8, 0x2

    .line 114
    invoke-static {v4}, Lcom/google/android/rcs/client/enrichedcall/DialerRcsIntents;->messageStateToString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 115
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    invoke-interface {v2, v0, v1, v3, v4}, Lbjf;->b(JLjava/lang/String;I)V

    goto/16 :goto_1

    .line 118
    :cond_13
    const-string v0, "dialer.rcs.intent.action.incomingCallComposerMessage"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 121
    const-string v0, "rcs.intent.extra.sessionid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 122
    const-string v0, "EnrichedCallBroadcastReceiver.isValidIncomingCallComposer"

    const-string v1, "missing sessionId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    const/4 v0, 0x0

    .line 125
    :goto_4
    if-nez v0, :cond_15

    .line 126
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->j:Ldnt$a;

    .line 127
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 124
    :cond_14
    const/4 v0, 0x1

    goto :goto_4

    .line 129
    :cond_15
    const-string v0, "rcs.intent.extra.sessionid"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 130
    invoke-interface {v2, v4, v5}, Lbjf;->b(J)Lbjl;

    move-result-object v1

    .line 131
    if-nez v1, :cond_16

    .line 132
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingCallComposerMessage"

    const-string v1, "no session for sessionId: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 133
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 134
    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->k:Ldnt$a;

    .line 136
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 138
    :cond_16
    const-string v0, "no-match"

    .line 139
    invoke-interface {v1}, Lbjl;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Ldke;->a:Lbjh;

    .line 140
    invoke-interface {v2, v0, v3, v6}, Lbjf;->a(Ljava/lang/String;Ljava/lang/String;Lbjh;)Lbjl;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_2e

    .line 142
    const-string v1, "EnrichedCallBroadcastReceiver.handleIncomingCallComposerMessage"

    const-string v3, "using the data from an already existing call composer for this number (session %d)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 143
    invoke-interface {v0}, Lbjl;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 144
    invoke-static {v1, v3, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    :goto_5
    invoke-interface {v0}, Lbjl;->d()Lbln;

    move-result-object v1

    .line 147
    invoke-static {}, Lbln;->f()Lblo;

    move-result-object v3

    .line 149
    invoke-virtual {v1}, Lbln;->a()Ljava/lang/String;

    move-result-object v6

    const-string v0, "rcs.intent.extra.subject"

    .line 150
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    const-string v7, "EnrichedCallBroadcastReceiver.handleIncomingSubject"

    const-string v8, "existing subject: %s, new subject: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 153
    invoke-static {v6}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 154
    invoke-static {v0}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 155
    invoke-static {v7, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    if-nez v0, :cond_18

    .line 158
    const/4 v0, 0x0

    .line 166
    :cond_17
    :goto_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_19

    .line 167
    const-string v6, "EnrichedCallBroadcastReceiver.handleIncomingSubject"

    const-string v7, "using new subject"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-virtual {v3, v0}, Lblo;->a(Ljava/lang/String;)Lblo;

    .line 176
    :goto_7
    invoke-virtual {v1}, Lbln;->e()Z

    move-result v6

    .line 177
    const-string v0, "rcs.intent.extra.importance"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 178
    const-string v0, "rcs.intent.extra.importance"

    const/4 v7, 0x0

    invoke-virtual {p2, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 181
    :goto_8
    const-string v7, "EnrichedCallBroadcastReceiver.handleIncomingImportance"

    const-string v8, "existing importance: %s, new importance: %b"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 182
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v0, v9, v10

    .line 183
    invoke-static {v7, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    if-eqz v0, :cond_1c

    .line 185
    const-string v6, "EnrichedCallBroadcastReceiver.handleIncomingImportance"

    const-string v7, "using new importance"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Lblo;->a(Z)Lblo;

    .line 191
    :goto_9
    invoke-virtual {v1}, Lbln;->b()Landroid/location/Location;

    move-result-object v6

    const-string v0, "rcs.intent.extra.location"

    .line 192
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    if-nez v0, :cond_1d

    .line 194
    const/4 v0, 0x0

    .line 209
    :goto_a
    const-string v7, "EnrichedCallBroadcastReceiver.handleIncomingLocation"

    const-string v8, "existing location: %s, new location: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 210
    invoke-static {v6}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 211
    invoke-static {v0}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 212
    invoke-static {v7, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    if-eqz v6, :cond_1f

    .line 214
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingLocation"

    const-string v7, "using existing location"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    invoke-virtual {v3, v6}, Lblo;->a(Landroid/location/Location;)Lblo;

    .line 222
    :goto_b
    const-string v0, "rcs.intent.extra.fileurl"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-virtual {v1}, Lbln;->c()Landroid/net/Uri;

    move-result-object v1

    .line 225
    if-nez v0, :cond_21

    const/4 v0, 0x0

    .line 227
    :goto_c
    const-string v6, "EnrichedCallBroadcastReceiver.handleIncomingImage"

    const-string v7, "existing image: %s, new image: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 228
    invoke-static {v1}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    .line 229
    invoke-static {v0}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 230
    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    if-eqz v0, :cond_22

    .line 232
    const-string v1, "EnrichedCallBroadcastReceiver.handleIncomingImage"

    const-string v6, "using new image"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    invoke-static {p1}, Lcsw;->c(Landroid/content/Context;)Lcte;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcte;->a(Ljava/lang/Object;)Lctb;

    move-result-object v1

    .line 235
    iget-object v6, v1, Lctb;->a:Lcte;

    const/high16 v7, -0x80000000

    const/high16 v8, -0x80000000

    .line 236
    new-instance v9, Ldgw;

    invoke-direct {v9, v6, v7, v8}, Ldgw;-><init>(Lcte;II)V

    .line 239
    const/4 v6, 0x0

    invoke-virtual {v1, v9, v6}, Lctb;->a(Ldha;Ldgm;)Ldha;

    .line 241
    const-string v1, "content_type_ignored"

    invoke-virtual {v3, v0, v1}, Lblo;->a(Landroid/net/Uri;Ljava/lang/String;)Lblo;

    .line 248
    :goto_d
    invoke-virtual {v3}, Lblo;->a()Lbln;

    move-result-object v0

    .line 249
    const-string v1, "EnrichedCallBroadcastReceiver.handleIncomingCallComposerMessage"

    const-string v3, "valid message received. sessionId: %s, multimediaData: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 250
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v0, v6, v7

    .line 251
    invoke-static {v1, v3, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    invoke-interface {v2, v4, v5, v0}, Lbjf;->b(JLbln;)V

    goto/16 :goto_1

    .line 159
    :cond_18
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x3c

    if-le v7, v8, :cond_17

    .line 160
    const-string v7, "EnrichedCallBroadcastReceiver.sanitizeIncomingSubject"

    const-string v8, "incoming subject was too long, truncating. Length: %d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    .line 162
    invoke-static {v7, v8, v9}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    const/4 v7, 0x0

    const/16 v8, 0x3c

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 170
    :cond_19
    if-eqz v6, :cond_1a

    .line 171
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingSubject"

    const-string v7, "using existing subject"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    invoke-virtual {v3, v6}, Lblo;->a(Ljava/lang/String;)Lblo;

    goto/16 :goto_7

    .line 174
    :cond_1a
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingSubject"

    const-string v6, "not using any subject"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 179
    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 188
    :cond_1c
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingImportance"

    const-string v7, "using existing importance"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    invoke-virtual {v3, v6}, Lblo;->a(Z)Lblo;

    goto/16 :goto_9

    .line 195
    :cond_1d
    sget-object v7, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 196
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-nez v7, :cond_1e

    .line 197
    const-string v7, "EnrichedCallBroadcastReceiver.sanitizeIncomingGeoUrl"

    const-string v8, "geoUrl didn\'t match expected pattern: %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 198
    invoke-static {v0}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    .line 199
    invoke-static {v7, v8, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 201
    :cond_1e
    const-string v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 202
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 203
    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v0, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 204
    new-instance v0, Landroid/location/Location;

    const-string v7, ""

    invoke-direct {v0, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v0, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 206
    invoke-virtual {v0, v10, v11}, Landroid/location/Location;->setLongitude(D)V

    goto/16 :goto_a

    .line 217
    :cond_1f
    if-eqz v0, :cond_20

    .line 218
    const-string v6, "EnrichedCallBroadcastReceiver.handleIncomingLocation"

    const-string v7, "using new location"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    invoke-virtual {v3, v0}, Lblo;->a(Landroid/location/Location;)Lblo;

    goto/16 :goto_b

    .line 221
    :cond_20
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingLocation"

    const-string v6, "not using any location"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_b

    .line 225
    :cond_21
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_c

    .line 243
    :cond_22
    if-eqz v1, :cond_23

    .line 244
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingImage"

    const-string v6, "using existing image"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    const-string v0, "content_type_ignored"

    invoke-virtual {v3, v1, v0}, Lblo;->a(Landroid/net/Uri;Ljava/lang/String;)Lblo;

    goto/16 :goto_d

    .line 247
    :cond_23
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingImage"

    const-string v1, "not using any image"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_d

    .line 254
    :cond_24
    const-string v0, "dialer.rcs.intent.action.incomingPostCallMessage"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 257
    const-string v0, "rcs.intent.extra.sessionid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 258
    const-string v0, "EnrichedCallBroadcastReceiver.isValidIncomingPostCall"

    const-string v1, "missing sessionId"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    const/4 v0, 0x0

    .line 261
    :goto_e
    if-nez v0, :cond_26

    .line 262
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->l:Ldnt$a;

    .line 263
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 260
    :cond_25
    const/4 v0, 0x1

    goto :goto_e

    .line 265
    :cond_26
    const-string v0, "rcs.intent.extra.sessionid"

    const-wide/16 v4, -0x1

    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 266
    const-string v3, "rcs.intent.extra.note"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 267
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 268
    const-string v2, "EnrichedCallBroadcastReceiver.handleIncomingPostCallNote"

    const-string v3, "ignoring null/empty note for sessionId: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 269
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    .line 270
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 272
    :cond_27
    invoke-interface {v2, v0, v1}, Lbjf;->b(J)Lbjl;

    move-result-object v4

    .line 273
    if-nez v4, :cond_28

    .line 274
    const-string v2, "EnrichedCallBroadcastReceiver.handleIncomingPostCallNote"

    const-string v3, "no sessionId for %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 275
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    .line 276
    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->m:Ldnt$a;

    .line 278
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    goto/16 :goto_1

    .line 280
    :cond_28
    invoke-interface {v4}, Lbjl;->d()Lbln;

    move-result-object v4

    invoke-virtual {v4}, Lbln;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_29

    .line 281
    const-string v2, "EnrichedCallBroadcastReceiver.handleIncomingPostCallNote"

    const-string v3, "existing note for sessionId: %d, ignoring new note"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 282
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    .line 283
    invoke-static {v2, v3, v4}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 285
    :cond_29
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x3c

    if-le v4, v5, :cond_2a

    .line 286
    const-string v4, "EnrichedCallBroadcastReceiver.handleIncomingPostCallNote"

    const-string v5, "incoming note was longer than %d characters. Length: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/16 v8, 0x3c

    .line 287
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 288
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 289
    invoke-static {v4, v5, v6}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    :cond_2a
    invoke-static {}, Lbln;->f()Lblo;

    move-result-object v4

    invoke-virtual {v4, v3}, Lblo;->a(Ljava/lang/String;)Lblo;

    move-result-object v3

    invoke-virtual {v3}, Lblo;->a()Lbln;

    move-result-object v3

    .line 291
    const-string v4, "EnrichedCallBroadcastReceiver.handleIncomingPostCallNote"

    const-string v5, "valid incoming post call note. sessionId: %d, multimediaData: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 292
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    .line 293
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v4

    invoke-interface {v2, v4, v0, v1, v3}, Lbjf;->a(Landroid/content/BroadcastReceiver$PendingResult;JLbln;)V

    goto/16 :goto_1

    .line 296
    :cond_2b
    const-string v0, "dialer.rcs.intent.action.incomingVideoShare"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    const-string v0, "rcs.intent.extra.sessionid"

    const-wide/16 v4, -0x1

    .line 299
    invoke-virtual {p2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 300
    const-string v3, "rcs.intent.extra.userId"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 301
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2c

    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_2d

    .line 302
    :cond_2c
    const-string v2, "EnrichedCallBroadcastReceiver.handleIncomingVideoShareInvite"

    const-string v4, "invalid sessionId: %d or remoteUserId: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 303
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    .line 304
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 305
    invoke-static {v2, v4, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->setResultCode(I)V

    goto/16 :goto_1

    .line 309
    :cond_2d
    const-string v4, "EnrichedCallBroadcastReceiver.handleIncomingVideoShareInvite"

    const-string v5, "sessionId: %d, remoteUserId: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 310
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 311
    invoke-static {v3}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 312
    invoke-static {v4, v5, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 313
    invoke-interface {v2, v0, v1, v3}, Lbjf;->a(JLjava/lang/String;)Z

    move-result v0

    .line 314
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    const-string v0, "EnrichedCallBroadcastReceiver.handleIncomingVideoShareInvite"

    const-string v1, "invite rejected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/dialer/enrichedcall/EnrichedCallBroadcastReceiver;->setResultCode(I)V

    goto/16 :goto_1

    :cond_2e
    move-object v0, v1

    goto/16 :goto_5
.end method
