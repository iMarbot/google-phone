.class public Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;
.super Lcom/google/android/rcs/client/videoshare/VideoShareService;
.source "PG"


# instance fields
.field private h:Landroid/util/LongSparseArray;

.field private i:Ldmw;

.field private j:Lbjy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgel;Ldmw;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/rcs/client/videoshare/VideoShareService;-><init>(Landroid/content/Context;Lgel;)V

    .line 2
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    .line 3
    new-instance v0, Lbjy;

    invoke-direct {v0, p0}, Lbjy;-><init>(Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;)V

    iput-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->j:Lbjy;

    .line 4
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmw;

    iput-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->i:Ldmw;

    .line 5
    return-void
.end method

.method private final e()Lcom/google/android/rcs/client/videoshare/IVideoShare;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 141
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/videoshare/IVideoShareAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/videoshare/IVideoShareAccessor;->get()Lcom/google/android/rcs/client/videoshare/IVideoShare;

    move-result-object v0

    .line 142
    if-nez v0, :cond_0

    .line 143
    const-string v0, "VideoShareManager.onSessionStatusUpdate"

    const-string v2, "unable to get video share binder"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 148
    :cond_0
    :goto_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    const-string v2, "VideoShareManager.onSessionStatusUpdate"

    const-string v3, "error getting video share binder"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 148
    goto :goto_0
.end method


# virtual methods
.method public final a(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-virtual {p0}, Lgng;->c()V

    .line 42
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->e()Lcom/google/android/rcs/client/videoshare/IVideoShare;

    move-result-object v4

    .line 43
    if-nez v4, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;-><init>(I)V

    .line 110
    :goto_0
    return-object v0

    .line 45
    :cond_0
    invoke-interface {v4, p1, p2}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->getRemoteMedia(J)[Lcom/google/android/rcs/client/session/Media;

    move-result-object v0

    .line 46
    new-instance v5, Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {v5, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lcom/google/android/rcs/client/session/Media;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->i:Ldmw;

    iget-object v1, p0, Lgng;->d:Landroid/content/Context;

    const/4 v3, 0x0

    .line 48
    invoke-interface {v4}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->shouldUseSecureSession()Z

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->j:Lbjy;

    .line 49
    invoke-virtual {v0, v1, v3, v6, v7}, Ldmw;->a(Landroid/content/Context;ZZLbjy;)Ldmx;
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    .line 54
    :try_start_2
    iput-wide p1, v6, Ldmx;->c:J

    .line 56
    iput-object v5, v6, Ldmx;->g:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 58
    const-string v0, "crypto"

    invoke-virtual {v5, v0}, Lcom/google/android/rcs/client/session/Media;->getParameterValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 59
    if-eqz v7, :cond_1

    array-length v0, v7

    if-nez v0, :cond_5

    .line 60
    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [Lges;

    .line 84
    :cond_2
    if-eqz v0, :cond_3

    array-length v1, v0

    if-lez v1, :cond_3

    .line 85
    const/4 v1, 0x1

    iput-boolean v1, v6, Ldmx;->d:Z

    .line 86
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 87
    iget-object v0, v0, Lges;->c:[B

    .line 88
    iput-object v0, v6, Ldmx;->e:[B

    .line 89
    :cond_3
    invoke-static {v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->matchUp(Lcom/google/android/rcs/client/session/rtp/RtpMedia;)Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    move-result-object v0

    iput-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 90
    iget-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    sget-object v1, Lgew;->a:Lgew;

    const/16 v2, 0xa0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setBandwith(Lgew;I)V

    .line 91
    iget-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    sget-object v1, Lfcj;->d:Lfcj;

    invoke-virtual {v0, v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setDirection(Lfcj;)V

    .line 92
    invoke-virtual {v6}, Ldmx;->g()V

    .line 93
    iget-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 94
    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/Media;->getSpropParameter()Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 96
    const-string v0, "VideoShareSessionImpl.setSpsPps"

    const-string v1, "sprop parameter is empty"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :cond_4
    :goto_1
    iget-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v6}, Ldmx;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalPort(I)V

    .line 105
    iget-object v0, v6, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 106
    invoke-interface {v4, p1, p2, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->acceptVideoShareSession(JLcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->succeeded()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->getSessionId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    const-string v1, "DialerVideoShareService.acceptVideoShareSession"

    const-string v2, "error accepting video share"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 113
    new-instance v1, Lgek;

    invoke-direct {v1, v0}, Lgek;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 51
    :catch_1
    move-exception v0

    .line 52
    :try_start_3
    new-instance v1, Lgek;

    invoke-direct {v1, v0}, Lgek;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 61
    :cond_5
    array-length v0, v7

    new-array v0, v0, [Lges;

    move v3, v2

    .line 62
    :goto_2
    array-length v1, v7

    if-ge v3, v1, :cond_2

    .line 63
    aget-object v1, v7, v3

    const-string v8, " "

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 64
    new-instance v1, Lges;

    invoke-direct {v1}, Lges;-><init>()V

    aput-object v1, v0, v3

    .line 65
    aget-object v1, v0, v3

    const/4 v9, 0x0

    aget-object v9, v8, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v1, Lges;->a:I

    .line 66
    const/4 v1, 0x1

    aget-object v9, v8, v1

    .line 67
    aget-object v10, v0, v3

    .line 68
    invoke-static {}, Lget;->values()[Lget;

    move-result-object v11

    move v1, v2

    .line 69
    :goto_3
    invoke-static {}, Lget;->values()[Lget;

    move-result-object v12

    array-length v12, v12

    if-ge v1, v12, :cond_7

    .line 70
    aget-object v12, v11, v1

    iget-object v12, v12, Lget;->b:Ljava/lang/String;

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 71
    aget-object v1, v11, v1

    .line 74
    :goto_4
    iput-object v1, v10, Lges;->b:Lget;

    .line 75
    const/4 v1, 0x2

    aget-object v1, v8, v1

    .line 76
    const-string v8, ":"

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 77
    aget-object v8, v0, v3

    const/4 v9, 0x0

    aget-object v9, v1, v9

    iput-object v9, v8, Lges;->d:Ljava/lang/String;

    .line 78
    const/4 v8, 0x1

    aget-object v1, v1, v8

    .line 79
    const-string v8, "|"

    invoke-static {v8}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 80
    aget-object v8, v0, v3

    const/4 v9, 0x0

    aget-object v1, v1, v9

    const/4 v9, 0x0

    invoke-static {v1, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    iput-object v1, v8, Lges;->c:[B

    .line 81
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 72
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 73
    :cond_7
    const/4 v1, 0x0

    goto :goto_4

    .line 98
    :cond_8
    const-string v1, "VideoShareSessionImpl.setSpsPps"

    const-string v2, "setting H264 sprop to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget-object v1, v6, Ldmx;->h:Lde/dreamchip/dreamstream/DreamVideo;

    invoke-virtual {v1, v0}, Lde/dreamchip/dreamstream/DreamVideo;->b(Ljava/lang/String;)I

    move-result v1

    .line 100
    if-eqz v1, :cond_4

    .line 101
    const-string v1, "VideoShareSessionImpl.setSpsPps"

    const-string v2, "unable to set sprop parameter set: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 109
    :cond_9
    invoke-virtual {v6}, Ldmx;->e()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 6

    .prologue
    .line 6
    invoke-virtual {p0}, Lgng;->c()V

    .line 7
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->e()Lcom/google/android/rcs/client/videoshare/IVideoShare;

    move-result-object v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    new-instance v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-object v0

    .line 10
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->i:Ldmw;

    iget-object v2, p0, Lgng;->d:Landroid/content/Context;

    const/4 v3, 0x1

    .line 11
    invoke-interface {v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->shouldUseSecureSession()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->j:Lbjy;

    .line 12
    invoke-virtual {v1, v2, v3, v4, v5}, Ldmw;->a(Landroid/content/Context;ZZLbjy;)Ldmx;
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 17
    :try_start_2
    sget-object v2, Lfci;->b:Lfci;

    invoke-static {v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->createCreatePreferredMedia(Lfci;)Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    move-result-object v2

    .line 18
    sget-object v3, Lfcj;->c:Lfcj;

    invoke-virtual {v2, v3}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setDirection(Lfcj;)V

    .line 19
    invoke-virtual {v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->addSupportedExtensions()V

    .line 20
    iget-boolean v3, v1, Ldmx;->d:Z

    if-eqz v3, :cond_1

    .line 21
    invoke-static {}, Ldmx;->i()[B

    move-result-object v3

    iput-object v3, v1, Ldmx;->e:[B

    .line 22
    iget-object v3, v1, Ldmx;->e:[B

    invoke-static {v2, v3}, Ldmx;->a(Lcom/google/android/rcs/client/session/rtp/RtpMedia;[B)V

    .line 23
    :cond_1
    iput-object v2, v1, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 24
    invoke-virtual {v1}, Ldmx;->g()V

    .line 25
    iget-object v2, v1, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {v1}, Ldmx;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalPort(I)V

    .line 28
    iget-object v2, v1, Ldmx;->f:Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    .line 29
    invoke-interface {v0, p1, v2}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->startVideoShareSession(Ljava/lang/String;Lcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->succeeded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 31
    invoke-virtual {v0}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->getSessionId()J

    move-result-wide v2

    .line 32
    iput-wide v2, v1, Ldmx;->c:J

    .line 33
    iget-object v2, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    .line 34
    iget-wide v4, v1, Ldmx;->c:J

    .line 35
    invoke-virtual {v2, v4, v5, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    const-string v1, "DialerVideoShareService.startVideoShareSession"

    const-string v2, "error preparing to share video"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    new-instance v1, Lgek;

    invoke-direct {v1, v0}, Lgek;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 14
    :catch_1
    move-exception v0

    .line 15
    :try_start_3
    new-instance v1, Lgek;

    invoke-direct {v1, v0}, Lgek;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 36
    :cond_2
    invoke-virtual {v1}, Ldmx;->e()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 150
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    :try_end_0
    .catch Lgek; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const-string v1, "VideoShareManager.endVideoShareSession"

    const-string v2, "ending session failed"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getSession(J)Lbjx;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    return-object v0
.end method

.method public onSessionStatusUpdate(JI)Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmx;

    .line 115
    if-nez v0, :cond_0

    move v0, v2

    .line 140
    :goto_0
    return v0

    .line 117
    :cond_0
    const/4 v1, 0x0

    .line 118
    if-ne p3, v10, :cond_4

    .line 119
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->e()Lcom/google/android/rcs/client/videoshare/IVideoShare;

    move-result-object v4

    .line 120
    if-nez v4, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    move v0, v3

    .line 122
    goto :goto_0

    .line 124
    :cond_1
    invoke-interface {v4}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->getActiveSessions()[J

    move-result-object v1

    .line 125
    invoke-static {v1}, Ljava/util/Arrays;->stream([J)Ljava/util/stream/LongStream;

    move-result-object v1

    new-instance v5, Ldmn;

    invoke-direct {v5, p1, p2}, Ldmn;-><init>(J)V

    invoke-interface {v1, v5}, Ljava/util/stream/LongStream;->filter(Ljava/util/function/LongPredicate;)Ljava/util/stream/LongStream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/LongStream;->count()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_2

    move v1, v3

    .line 126
    :goto_1
    if-nez v1, :cond_3

    .line 127
    const-string v0, "VideoShareManager.onSessionStatusUpdate"

    const-string v1, "session missing from RCS module, removing"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    move v0, v3

    .line 129
    goto :goto_0

    :cond_2
    move v1, v2

    .line 125
    goto :goto_1

    .line 130
    :cond_3
    invoke-interface {v4, p1, p2}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->getRemoteMedia(J)[Lcom/google/android/rcs/client/session/Media;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 138
    :cond_4
    invoke-virtual {v0, p3, v1}, Ldmx;->a(I[Lcom/google/android/rcs/client/session/Media;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    :cond_5
    move v0, v3

    .line 140
    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    const-string v1, "VideoShareManager.onSessionStatusUpdate"

    const-string v4, "error preparing to share session id: %d"

    new-array v5, v10, [Ljava/lang/Object;

    .line 134
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v3

    .line 135
    invoke-static {v1, v4, v5}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/dialer/enrichedcall/videoshare/DialerVideoShareService;->h:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    move v0, v3

    .line 137
    goto :goto_0
.end method
