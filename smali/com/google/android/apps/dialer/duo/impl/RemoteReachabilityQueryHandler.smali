.class public Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;
.super Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field public static final ACCEPTS_UPGRADES:Ljava/lang/String; = "ACCEPTS_UPGRADES"

.field public static final VIDEO_CALLABLE:Ljava/lang/String; = "VIDEO_CALLABLE"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lgvw;

.field private c:Lgvw;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;-><init>()V

    .line 3
    new-instance v0, Lgvw;

    invoke-direct {v0}, Lgvw;-><init>()V

    .line 4
    iput-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    .line 6
    new-instance v0, Lgvw;

    invoke-direct {v0}, Lgvw;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->c:Lgvw;

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->d:Z

    .line 9
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->a:Landroid/content/Context;

    .line 10
    return-void
.end method


# virtual methods
.method public onFinished(Ljava/util/Map;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 40
    invoke-static {}, Lbdf;->c()V

    .line 41
    if-nez p1, :cond_0

    .line 42
    const-string v0, "RemoteReachabilityQueryHandler.onFinished"

    const-string v1, "received null for result"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    .line 44
    sget-object v1, Lgup;->a:Lgui;

    .line 45
    invoke-virtual {v0, v1}, Lgvp;->a(Ljava/lang/Object;)Z

    .line 75
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v2, Lguj;

    invoke-direct {v2}, Lguj;-><init>()V

    .line 49
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 50
    check-cast v0, Ljava/util/Map$Entry;

    .line 51
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_1

    .line 52
    const-string v1, "RemoteReachabilityQueryHandler.onFinished"

    .line 53
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "key "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a String"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    .line 54
    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    .line 56
    sget-object v1, Lgup;->a:Lgui;

    .line 57
    invoke-virtual {v0, v1}, Lgvp;->a(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Landroid/os/Bundle;

    if-nez v1, :cond_2

    .line 60
    const-string v1, "RemoteReachabilityQueryHandler.onFinished"

    .line 61
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "value  "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a Bundle"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    .line 62
    invoke-static {v1, v0, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    .line 64
    sget-object v1, Lgup;->a:Lgui;

    .line 65
    invoke-virtual {v0, v1}, Lgvp;->a(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 67
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 69
    const-string v4, "VIDEO_CALLABLE"

    .line 70
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "ACCEPTS_UPGRADES"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 71
    invoke-static {v0, v4, v1}, Lbit;->a(Ljava/lang/String;ZZ)Lbit;

    move-result-object v1

    .line 72
    invoke-virtual {v2, v0, v1}, Lguj;->a(Ljava/lang/Object;Ljava/lang/Object;)Lguj;

    goto/16 :goto_1

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    invoke-virtual {v2}, Lguj;->a()Lgui;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgvp;->a(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->c:Lgvw;

    invoke-static {p2}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityService;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgvp;->a(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method onServiceConnectedForTest(Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityService;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->c:Lgvw;

    invoke-virtual {v0, p1}, Lgvp;->a(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 36
    const-string v0, "RemoteReachabilityQueryHandler.onServiceDisconnected"

    const-string v1, "disconnected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->c:Lgvw;

    invoke-virtual {v0, v3}, Lgvp;->cancel(Z)Z

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    invoke-virtual {v0, v3}, Lgvp;->cancel(Z)Z

    .line 39
    return-void
.end method

.method public query(Ljava/util/List;)Ljava/util/Map;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 11
    invoke-static {}, Lbdf;->c()V

    .line 12
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    iget-boolean v2, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->d:Z

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 14
    iput-boolean v1, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->d:Z

    .line 15
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 16
    sget-object v1, Ldjo;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 17
    iget-object v1, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 18
    const-string v0, "RemoteReachabilityQueryHandler.query"

    const-string v1, "unable to bind to reachability service"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    sget-object v0, Lgup;->a:Lgui;

    .line 31
    :goto_0
    return-object v0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->c:Lgvw;

    const-wide/16 v2, 0x3e8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 22
    invoke-virtual {v0, v2, v3, v1}, Lgvw;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityService;

    .line 23
    invoke-interface {v0, p1, p0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityService;->queryReachability(Ljava/util/List;Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;)V

    .line 24
    iget-object v0, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->b:Lgvw;

    const-wide/16 v2, 0x3e8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 25
    invoke-virtual {v0, v2, v3, v1}, Lgvw;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 26
    iget-object v1, p0, Lcom/google/android/apps/dialer/duo/impl/RemoteReachabilityQueryHandler;->a:Landroid/content/Context;

    invoke-virtual {v1, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 28
    :catch_0
    move-exception v0

    .line 29
    :goto_1
    const-string v1, "RemoteReachabilityQueryHandler.query"

    const-string v2, "cannot query reachability"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    sget-object v0, Lgup;->a:Lgui;

    goto :goto_0

    .line 28
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_1
.end method
