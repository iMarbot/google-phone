.class public Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# static fields
.field private static a:Landroid/content/UriMatcher;

.field private static b:Ljava/util/Set;


# instance fields
.field private c:[Ljava/lang/String;

.field private d:Ljava/io/File;

.field private e:Ljava/io/File;

.field private f:Ldip;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 137
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    .line 139
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.dialer.cacheprovider"

    const-string v2, "contact"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 140
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.dialer.cacheprovider"

    const-string v2, "contact/*"

    const/16 v3, 0x3e9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 141
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.dialer.cacheprovider"

    const-string v2, "photo/*"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 142
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.dialer.cacheprovider"

    const-string v2, "thumbnail/*"

    const/16 v3, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "number"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "phone_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "phone_label"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "display_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 147
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "photo_uri"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "source_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "source_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "source_id"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "lookup_key"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "reported"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "object_id"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    const-string v1, "user_type"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    .line 3
    return-void
.end method

.method private final a(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 116
    if-eqz p2, :cond_1

    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 119
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 121
    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 122
    :cond_0
    const/high16 v2, 0x30000000

    :try_start_1
    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 126
    :goto_1
    return-object v0

    .line 118
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    goto :goto_0

    .line 124
    :catch_0
    move-exception v1

    goto :goto_1

    .line 126
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid URI or phone number not provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-direct {p0, v0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method private static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create photo storage directory "

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_1
    return-void
.end method

.method private final a(Ljava/lang/String;ZZ)V
    .locals 5

    .prologue
    .line 107
    if-eqz p2, :cond_0

    .line 108
    const-string v0, "has_photo"

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v1}, Ldip;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 113
    if-eqz p3, :cond_1

    const-string v1, "1"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3e

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "UPDATE cached_number_contacts SET "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE normalized_number=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    .line 114
    invoke-virtual {v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    return-void

    .line 109
    :cond_0
    const-string v0, "has_thumbnail"

    goto :goto_0

    .line 113
    :cond_1
    const-string v1, "0"

    goto :goto_1
.end method

.method private final b(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->e:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private final b(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 127
    if-eqz p2, :cond_0

    .line 128
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 130
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    return v0

    .line 129
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method private final c(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->d:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 64
    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v0}, Ldip;->b()V

    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    aput-object v0, v1, v3

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v1}, Ldip;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 70
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b(Ljava/lang/String;Z)Z

    move-result v2

    .line 71
    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b(Ljava/lang/String;Z)Z

    move-result v0

    .line 72
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 74
    :cond_0
    const-string v0, "cached_number_contacts"

    const-string v2, "normalized_number=?"

    iget-object v3, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    .line 75
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown URI or phone number not provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 26
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 27
    packed-switch v0, :pswitch_data_0

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown URI"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v1}, Ldip;->b()V

    .line 29
    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_3

    .line 30
    const-string v0, "number"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 32
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Phone number not provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 35
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object p1, v2

    .line 61
    :cond_2
    :goto_1
    return-object p1

    .line 34
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 37
    :cond_4
    invoke-virtual {p2}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 38
    sget-object v6, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 39
    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 40
    const-string v6, "PhoneNumberCacheProvider.insert"

    const-string v7, "ignoring unsupported column for update: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v6, v0, v7}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 42
    :cond_7
    const-string v0, "normalized_number"

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v0, "time_last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v0}, Ldip;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    aput-object v1, v0, v4

    .line 47
    const-string v0, "source_type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_8

    .line 49
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lbko$a;->a(I)Lbko$a;

    move-result-object v0

    invoke-static {v0}, Ldoj;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    .line 59
    :goto_4
    if-eqz v0, :cond_2

    .line 60
    const-string v0, "cached_number_contacts"

    const/4 v1, 0x5

    invoke-virtual {v5, v0, v2, p2, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_1

    .line 52
    :cond_8
    :try_start_0
    const-string v0, "SELECT source_type FROM cached_number_contacts WHERE normalized_number=?"

    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    .line 53
    invoke-static {v5, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 54
    invoke-static {v0}, Lbko$a;->a(I)Lbko$a;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 57
    :goto_5
    if-eqz v0, :cond_9

    invoke-static {v0}, Ldoj;->a(Lbko$a;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    move v0, v3

    .line 58
    goto :goto_4

    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_5

    :cond_a
    move v0, v4

    goto :goto_4

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 4
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldip;->a(Landroid/content/Context;)Ldip;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    .line 6
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "photos/raw"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->e:Ljava/io/File;

    .line 7
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->e:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/io/File;)V

    .line 8
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "thumbnails/raw"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->d:Ljava/io/File;

    .line 9
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/io/File;)V

    .line 10
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 11

    .prologue
    const/16 v10, 0x7d0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    sget-object v2, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 85
    sparse-switch v3, :sswitch_data_0

    .line 106
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Unknown or unsupported URI"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :sswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v2}, Ldip;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 89
    iget-object v5, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    aput-object v4, v5, v1

    .line 90
    const-string v5, "cached_number_contacts"

    const-string v6, "normalized_number=?"

    iget-object v7, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    .line 91
    invoke-static {v2, v5, v6, v7}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v6

    .line 92
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_0

    move v2, v0

    .line 93
    :goto_0
    if-nez v2, :cond_1

    .line 94
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Phone number does not exist in cache"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_1
    const-string v2, "r"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 96
    :goto_1
    if-eqz v2, :cond_4

    .line 97
    if-ne v3, v10, :cond_3

    :goto_2
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 103
    :goto_3
    return-object v0

    :cond_2
    move v2, v1

    .line 95
    goto :goto_1

    :cond_3
    move v0, v1

    .line 97
    goto :goto_2

    .line 98
    :cond_4
    if-ne v3, v10, :cond_5

    move v2, v0

    .line 99
    :goto_4
    if-eqz v2, :cond_6

    .line 100
    invoke-direct {p0, v4}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 102
    :goto_5
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 103
    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v2, v1

    .line 98
    goto :goto_4

    .line 101
    :cond_6
    invoke-direct {p0, v4}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_5

    .line 104
    :cond_7
    invoke-direct {p0, v4, v2, v1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Ljava/lang/String;ZZ)V

    .line 105
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "No photo file found for number: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 85
    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0xbb8 -> :sswitch_0
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 11
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 12
    const-string v0, "PhoneNumberCacheProvider.query"

    const-string v1, "contacts permission denied"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    :cond_0
    :goto_0
    return-object v5

    .line 14
    :cond_1
    sget-object v0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_0

    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 17
    if-nez v0, :cond_2

    .line 18
    new-instance v5, Lagi;

    invoke-direct {v5, p2}, Lagi;-><init>([Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    invoke-virtual {v1}, Ldip;->b()V

    .line 20
    iget-object v1, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    aput-object v0, v1, v2

    .line 21
    iget-object v0, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->f:Ldip;

    .line 22
    invoke-virtual {v0}, Ldip;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "cached_number_contacts"

    const-string v3, "normalized_number=?"

    iget-object v4, p0, Lcom/google/android/apps/dialer/phonenumbercache/PhoneNumberCacheProvider;->c:[Ljava/lang/String;

    move-object v2, p2

    move-object v6, v5

    move-object v7, v5

    .line 23
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "The cache does not support update operations. Use insert to replace an existing phone number, if needed."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
