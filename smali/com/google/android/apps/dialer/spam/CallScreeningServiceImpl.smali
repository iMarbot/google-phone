.class public Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;
.super Landroid/telecom/CallScreeningService;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x18
.end annotation


# instance fields
.field private a:Ldqb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/CallScreeningService;-><init>()V

    return-void
.end method

.method public static a(Z)Landroid/telecom/CallScreeningService$CallResponse;
    .locals 1

    .prologue
    .line 2
    new-instance v0, Landroid/telecom/CallScreeningService$CallResponse$Builder;

    invoke-direct {v0}, Landroid/telecom/CallScreeningService$CallResponse$Builder;-><init>()V

    .line 3
    invoke-virtual {v0, p0}, Landroid/telecom/CallScreeningService$CallResponse$Builder;->setDisallowCall(Z)Landroid/telecom/CallScreeningService$CallResponse$Builder;

    .line 4
    invoke-virtual {v0, p0}, Landroid/telecom/CallScreeningService$CallResponse$Builder;->setRejectCall(Z)Landroid/telecom/CallScreeningService$CallResponse$Builder;

    .line 5
    invoke-virtual {v0}, Landroid/telecom/CallScreeningService$CallResponse$Builder;->build()Landroid/telecom/CallScreeningService$CallResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/telecom/Call$Details;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a(Z)Landroid/telecom/CallScreeningService$CallResponse;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->respondToCall(Landroid/telecom/Call$Details;Landroid/telecom/CallScreeningService$CallResponse;)V

    .line 25
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 6
    invoke-super {p0}, Landroid/telecom/CallScreeningService;->onCreate()V

    .line 7
    new-instance v0, Ldqb;

    invoke-direct {v0, p0}, Ldqb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a:Ldqb;

    .line 8
    return-void
.end method

.method public onScreenCall(Landroid/telecom/Call$Details;)V
    .locals 7

    .prologue
    .line 9
    .line 10
    invoke-virtual {p1}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v1

    .line 11
    sget-object v0, Ldny;->e:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 12
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const-string v0, "tel"

    .line 13
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 15
    :goto_0
    if-nez v0, :cond_1

    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a(Landroid/telecom/Call$Details;)V

    .line 23
    :goto_1
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 18
    :cond_1
    invoke-virtual {p1}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    .line 19
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 20
    iget-object v6, p0, Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;->a:Ldqb;

    new-instance v0, Ldqa;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Ldqa;-><init>(Lcom/google/android/apps/dialer/spam/CallScreeningServiceImpl;JLjava/lang/String;Landroid/telecom/Call$Details;)V

    .line 21
    invoke-static {p0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-virtual {v6, v0, v4, v1}, Ldqb;->a(Ldqk;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
