.class public Lcom/google/android/apps/dialer/spam/SpamJobService;
.super Landroid/app/job/JobService;
.source "PG"


# static fields
.field public static a:Z

.field private static b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/dialer/spam/SpamJobService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 39
    new-instance v0, Ldqy;

    invoke-direct {v0, p0}, Ldqy;-><init>(Lcom/google/android/apps/dialer/spam/SpamJobService;)V

    .line 40
    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 41
    const-string v1, "spam_jobs_last_updated_blacklist"

    const-wide/16 v2, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v3

    .line 3
    invoke-virtual {p0}, Lcom/google/android/apps/dialer/spam/SpamJobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 4
    sget-boolean v0, Ldny;->a:Z

    if-nez v0, :cond_0

    .line 5
    invoke-static {v4}, Ldny;->a(Landroid/content/Context;)V

    .line 6
    invoke-static {v4}, Ldny;->b(Landroid/content/Context;)V

    .line 7
    :cond_0
    invoke-static {v3}, Ldhh;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ldny;->D:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 8
    :cond_1
    invoke-static {v4, v3}, Ldhh;->b(Landroid/content/Context;I)V

    move v0, v1

    .line 35
    :goto_0
    return v0

    .line 10
    :cond_2
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v0

    const-string v5, "spam_jobs_interval"

    invoke-virtual {v0, v5}, Landroid/os/PersistableBundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 11
    invoke-static {v3}, Ldhh;->b(I)J

    move-result-wide v8

    .line 12
    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    .line 14
    invoke-static {v3}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "rescheduling "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " because interval was updated"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    invoke-static {v4, v3}, Ldhh;->c(Landroid/content/Context;I)V

    move v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_3
    sget-object v5, Lcom/google/android/apps/dialer/spam/SpamJobService;->b:Ljava/lang/Object;

    monitor-enter v5

    .line 18
    :try_start_0
    sget-boolean v0, Lcom/google/android/apps/dialer/spam/SpamJobService;->a:Z

    if-eqz v0, :cond_5

    .line 19
    const-string v0, "other job already running, returning false for: "

    .line 20
    invoke-static {v3}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 21
    :goto_1
    monitor-exit v5

    move v0, v1

    goto :goto_0

    .line 20
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 22
    :cond_5
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/google/android/apps/dialer/spam/SpamJobService;->a:Z

    .line 23
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/google/android/apps/dialer/spam/SpamJobService;->a()J

    move-result-wide v10

    sub-long/2addr v8, v10

    cmp-long v0, v8, v6

    if-gez v0, :cond_6

    .line 26
    invoke-static {v3}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x4a

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "returning false for "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " because the last update occurred within the interval."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    sput-boolean v1, Lcom/google/android/apps/dialer/spam/SpamJobService;->a:Z

    move v0, v1

    .line 28
    goto/16 :goto_0

    .line 29
    :cond_6
    const-string v0, "syncing for job: "

    .line 30
    invoke-static {v3}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    :goto_2
    new-instance v0, Ldqm;

    invoke-direct {v0, p0, p1}, Ldqm;-><init>(Lcom/google/android/apps/dialer/spam/SpamJobService;Landroid/app/job/JobParameters;)V

    .line 32
    invoke-static {v4}, Lbw;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 33
    new-instance v3, Ldqc;

    invoke-direct {v3, v4, v0}, Ldqc;-><init>(Landroid/content/Context;Ldqm;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Void;

    .line 34
    invoke-virtual {v3, v0, v1}, Ldqc;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_7
    move v0, v2

    .line 35
    goto/16 :goto_0

    .line 30
    :cond_8
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 3

    .prologue
    .line 36
    const-string v0, "onStopJob for job: "

    .line 37
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v1

    invoke-static {v1}, Ldhh;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 37
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
