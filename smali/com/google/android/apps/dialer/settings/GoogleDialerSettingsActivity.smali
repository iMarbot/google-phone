.class public Lcom/google/android/apps/dialer/settings/GoogleDialerSettingsActivity;
.super Lcom/android/dialer/app/settings/DialerSettingsActivity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/android/dialer/app/settings/DialerSettingsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 2
    invoke-super {p0, p1}, Lcom/android/dialer/app/settings/DialerSettingsActivity;->onBuildHeaders(Ljava/util/List;)V

    .line 3
    invoke-static {p0}, Ldhh;->k(Landroid/content/Context;)Z

    move-result v0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 6
    const v1, 0x7f11018b

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 7
    const-class v1, Ldpt;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 8
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    :cond_0
    sget-object v0, Ldny;->s:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 12
    const v1, 0x7f1101df

    iput v1, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    .line 13
    const-class v1, Ldpu;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 14
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    :cond_1
    return-void
.end method
