.class public interface abstract Lcom/google/android/apps/tycho/IVoiceService;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/tycho/IVoiceService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getProxyNumber(Ljava/lang/String;Lcom/google/android/apps/tycho/IProxyNumberCallback;)V
.end method

.method public abstract getVersion()I
.end method

.method public abstract isOnHomeVoiceNetwork()Z
.end method

.method public abstract onSimCallManagerChanged()V
.end method
