.class public abstract Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_requestHandover:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.apps.tachyon.telecom.ITelecomFallbackService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.apps.tachyon.telecom.ITelecomFallbackService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 20
    :goto_0
    return v0

    .line 12
    :cond_0
    if-ne p1, v2, :cond_1

    .line 13
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackSource$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackSource;

    move-result-object v3

    .line 14
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 15
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v1}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 16
    invoke-virtual {p0, v3, v0, v1}, Lcom/google/android/apps/tachyon/telecom/ITelecomFallbackService$Stub;->requestHandover(Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackSource;Landroid/net/Uri;Landroid/os/Bundle;)Lcom/google/android/apps/tachyon/telecom/IHandoverFallbackTarget;

    move-result-object v0

    .line 17
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 18
    invoke-static {p3, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    move v0, v2

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
