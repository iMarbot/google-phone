.class public abstract Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_onFinished:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.apps.tachyon.contacts.reachability.IReachabilityQueryCallback"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.apps.tachyon.contacts.reachability.IReachabilityQueryCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16
    :goto_0
    return v0

    .line 12
    :cond_0
    if-ne p1, v0, :cond_1

    .line 13
    invoke-static {p2}, Ldii;->b(Landroid/os/Parcel;)Ljava/util/HashMap;

    move-result-object v1

    .line 14
    invoke-virtual {p0, v1}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;->onFinished(Ljava/util/Map;)V

    goto :goto_0

    .line 16
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
