.class public Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.apps.tachyon.contacts.reachability.IReachabilityQueryCallback"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public onFinished(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 5
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/tachyon/contacts/reachability/IReachabilityQueryCallback$Stub$Proxy;->transactOneway(ILandroid/os/Parcel;)V

    .line 6
    return-void
.end method
