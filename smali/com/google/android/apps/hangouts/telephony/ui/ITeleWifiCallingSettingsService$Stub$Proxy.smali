.class public Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.apps.hangouts.telephony.ui.ITeleWifiCallingSettingsService"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public getTychoAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 27
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 28
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 30
    return-object v0
.end method

.method public getVersion()I
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 6
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 7
    return v1
.end method

.method public getWifiCallingAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 18
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 19
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 21
    return-object v0
.end method

.method public getWifiCallingEnabledPreference()Z
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 40
    const/16 v1, 0x9

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 41
    invoke-static {v0}, Ldii;->a(Landroid/os/Parcel;)Z

    move-result v1

    .line 42
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 43
    return v1
.end method

.method public getWifiCallingState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 9
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 12
    return-object v1
.end method

.method public setLastEmergencyDialedTimeMillisFromDarkNumber(J)V
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 36
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 37
    const/16 v1, 0x8

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadExceptionReturnVoid(ILandroid/os/Parcel;)V

    .line 38
    return-void
.end method

.method public setTychoAccount(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 32
    invoke-static {v0, p1}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    .line 33
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadExceptionReturnVoid(ILandroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method public setWifiCallingAccount(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 23
    invoke-static {v0, p1}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    .line 24
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadExceptionReturnVoid(ILandroid/os/Parcel;)V

    .line 25
    return-void
.end method

.method public setWifiCallingState(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 14
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 15
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;->transactAndReadExceptionReturnVoid(ILandroid/os/Parcel;)V

    .line 16
    return-void
.end method
