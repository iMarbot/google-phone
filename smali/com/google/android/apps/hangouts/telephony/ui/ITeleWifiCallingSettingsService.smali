.class public interface abstract Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getTychoAccount()Landroid/accounts/Account;
.end method

.method public abstract getVersion()I
.end method

.method public abstract getWifiCallingAccount()Landroid/accounts/Account;
.end method

.method public abstract getWifiCallingEnabledPreference()Z
.end method

.method public abstract getWifiCallingState()Ljava/lang/String;
.end method

.method public abstract setLastEmergencyDialedTimeMillisFromDarkNumber(J)V
.end method

.method public abstract setTychoAccount(Landroid/accounts/Account;)V
.end method

.method public abstract setWifiCallingAccount(Landroid/accounts/Account;)V
.end method

.method public abstract setWifiCallingState(Ljava/lang/String;)V
.end method
