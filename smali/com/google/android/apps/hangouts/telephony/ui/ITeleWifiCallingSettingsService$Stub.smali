.class public abstract Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_getTychoAccount:I = 0x6

.field public static final TRANSACTION_getVersion:I = 0x1

.field public static final TRANSACTION_getWifiCallingAccount:I = 0x4

.field public static final TRANSACTION_getWifiCallingEnabledPreference:I = 0x9

.field public static final TRANSACTION_getWifiCallingState:I = 0x2

.field public static final TRANSACTION_setLastEmergencyDialedTimeMillisFromDarkNumber:I = 0x8

.field public static final TRANSACTION_setTychoAccount:I = 0x7

.field public static final TRANSACTION_setWifiCallingAccount:I = 0x5

.field public static final TRANSACTION_setWifiCallingState:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.apps.hangouts.telephony.ui.ITeleWifiCallingSettingsService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.apps.hangouts.telephony.ui.ITeleWifiCallingSettingsService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 50
    :goto_0
    return v0

    .line 12
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 49
    const/4 v0, 0x0

    goto :goto_0

    .line 13
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->getVersion()I

    move-result v0

    .line 14
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_1
    move v0, v1

    .line 50
    goto :goto_0

    .line 17
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->getWifiCallingState()Ljava/lang/String;

    move-result-object v0

    .line 18
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 19
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 21
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->setWifiCallingState(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    .line 25
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->getWifiCallingAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 26
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 27
    invoke-static {p3, v0}, Ldii;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 29
    :pswitch_4
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 30
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->setWifiCallingAccount(Landroid/accounts/Account;)V

    .line 31
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    .line 33
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->getTychoAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 34
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 35
    invoke-static {p3, v0}, Ldii;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 37
    :pswitch_6
    sget-object v0, Landroid/accounts/Account;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 38
    invoke-virtual {p0, v0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->setTychoAccount(Landroid/accounts/Account;)V

    .line 39
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    .line 41
    :pswitch_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 42
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->setLastEmergencyDialedTimeMillisFromDarkNumber(J)V

    .line 43
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    .line 45
    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/apps/hangouts/telephony/ui/ITeleWifiCallingSettingsService$Stub;->getWifiCallingEnabledPreference()Z

    move-result v0

    .line 46
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 47
    invoke-static {p3, v0}, Ldii;->a(Landroid/os/Parcel;Z)V

    goto :goto_1

    .line 12
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
