.class public Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
.super Lcom/google/android/rcs/client/JibeServiceResult;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private c:J

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lgep;

    invoke-direct {v0}, Lgep;-><init>()V

    sput-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(JI)V

    .line 2
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 3
    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move-object v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(JLjava/lang/String;ILjava/lang/String;)V

    .line 4
    return-void
.end method

.method public constructor <init>(JILjava/lang/String;)V
    .locals 7

    .prologue
    .line 7
    const/4 v4, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(JLjava/lang/String;ILjava/lang/String;)V

    .line 8
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 5
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(JLjava/lang/String;ILjava/lang/String;)V

    .line 6
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/android/rcs/client/JibeServiceResult;-><init>()V

    .line 10
    iput p4, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->a:I

    .line 11
    iput-object p5, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->b:Ljava/lang/String;

    .line 12
    iput-wide p1, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->c:J

    .line 13
    iput-object p3, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->d:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/rcs/client/JibeServiceResult;-><init>()V

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->a:I

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->b:Ljava/lang/String;

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->c:J

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->d:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getMessageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionId()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->c:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->c:J

    invoke-super {p0}, Lcom/google/android/rcs/client/JibeServiceResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Session ID: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    iget-object v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25
    iget-wide v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 26
    iget-object v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    return-void
.end method
