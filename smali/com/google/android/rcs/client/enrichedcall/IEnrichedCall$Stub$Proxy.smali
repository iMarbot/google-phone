.class public Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.rcs.client.enrichedcall.IEnrichedCall"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public endCallComposerSession(J)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 30
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 31
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 32
    return-object v0
.end method

.method public getSupportedServices()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 5
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    .line 6
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 7
    return-object v0
.end method

.method public getVersion()I
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 41
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 43
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 44
    return v1
.end method

.method public requestCapabilities(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 2

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 10
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 11
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 12
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 13
    return-object v0
.end method

.method public sendCallComposerData(JLcom/google/android/rcs/client/enrichedcall/CallComposerData;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 3

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 21
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 22
    invoke-static {v0, p3}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    .line 23
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 24
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 26
    return-object v0
.end method

.method public sendPostCallNote(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 34
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 37
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 39
    return-object v0
.end method

.method public startCallComposerSession(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 15
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 16
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 17
    sget-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    .line 18
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 19
    return-object v0
.end method
