.class public Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;
.super Lcom/google/android/rcs/client/JibeServiceResult;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lgeq;

    invoke-direct {v0}, Lgeq;-><init>()V

    sput-object v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/rcs/client/JibeServiceResult;-><init>()V

    .line 3
    iget-boolean v0, p1, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;->a:Z

    .line 4
    iput-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->c:Z

    .line 6
    iget-boolean v0, p1, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;->b:Z

    .line 7
    iput-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->d:Z

    .line 9
    iget-boolean v0, p1, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;->c:Z

    .line 10
    iput-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->e:Z

    .line 11
    return-void
.end method

.method public static builder()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;

    .line 20
    invoke-direct {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;-><init>()V

    .line 21
    return-object v0
.end method


# virtual methods
.method public isCallComposerSupported()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->c:Z

    return v0
.end method

.method public isPostCallSupported()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->d:Z

    return v0
.end method

.method public isVideoShareSupported()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->e:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 16
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 17
    iget-boolean v0, p0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->e:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 18
    return-void

    :cond_0
    move v0, v2

    .line 15
    goto :goto_0

    :cond_1
    move v0, v2

    .line 16
    goto :goto_1

    :cond_2
    move v1, v2

    .line 17
    goto :goto_2
.end method
