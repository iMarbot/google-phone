.class public Lcom/google/android/rcs/client/enrichedcall/EnrichedCallService;
.super Lgng;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgel;)V
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-direct {p0, v0, p1, p2}, Lgng;-><init>(Ljava/lang/Class;Landroid/content/Context;Lgel;)V

    .line 2
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "com.google.android.rcs.service.service.EnrichedCallBindingService"

    return-object v0
.end method

.method public endCallComposerSession(J)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 5

    .prologue
    .line 45
    invoke-virtual {p0}, Lgng;->c()V

    .line 46
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 47
    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(I)V

    .line 49
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->endCallComposerSession(J)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 51
    const-string v2, "RcsClientLib"

    const-string v3, "Error while ending call composer session: "

    .line 52
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    new-instance v2, Lgek;

    const-string v3, "Error while ending call composer session: "

    .line 55
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 52
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 55
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getSupportedServices()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;
    .locals 5

    .prologue
    .line 3
    invoke-virtual {p0}, Lgng;->c()V

    .line 4
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    invoke-static {}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;->builder()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult$a;->a()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;

    move-result-object v0

    .line 7
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->getSupportedServices()Lcom/google/android/rcs/client/enrichedcall/EnrichedCallSupportedServicesResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 9
    const-string v2, "RcsClientLib"

    const-string v3, "Error while getting supported services: "

    .line 10
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 11
    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    new-instance v2, Lgek;

    const-string v3, "Error while getting supported services: "

    .line 13
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 10
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 13
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getVersion()I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 67
    :try_start_0
    invoke-virtual {p0}, Lgng;->c()V

    .line 68
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 69
    if-nez v0, :cond_0

    move v0, v1

    .line 74
    :goto_0
    return v0

    .line 71
    :cond_0
    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->getVersion()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-string v2, "RcsClientLib"

    const-string v3, "Error while getting version: "

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 74
    goto :goto_0

    .line 73
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public requestCapabilities(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 5

    .prologue
    .line 14
    invoke-virtual {p0}, Lgng;->c()V

    .line 15
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(I)V

    .line 18
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->requestCapabilities(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 19
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 20
    const-string v2, "RcsClientLib"

    const-string v3, "Error while requesting capabilities: "

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    new-instance v2, Lgek;

    const-string v3, "Error while requesting capabilities: "

    .line 22
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 20
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 22
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public sendCallComposerData(JLcom/google/android/rcs/client/enrichedcall/CallComposerData;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 5

    .prologue
    .line 34
    invoke-virtual {p0}, Lgng;->c()V

    .line 35
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(I)V

    .line 38
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->sendCallComposerData(JLcom/google/android/rcs/client/enrichedcall/CallComposerData;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 40
    const-string v2, "RcsClientLib"

    const-string v3, "Error while sending call composer data: "

    .line 41
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v2, Lgek;

    const-string v3, "Error while sending call composer data: "

    .line 44
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 41
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 44
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public sendPostCallNote(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 5

    .prologue
    .line 56
    invoke-virtual {p0}, Lgng;->c()V

    .line 57
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 58
    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(I)V

    .line 60
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->sendPostCallNote(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 62
    const-string v2, "RcsClientLib"

    const-string v3, "Error while sending post call note: "

    .line 63
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v2, Lgek;

    const-string v3, "Error while sending post call note: "

    .line 66
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 63
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 66
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public startCallComposerSession(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    .locals 5

    .prologue
    .line 23
    invoke-virtual {p0}, Lgng;->c()V

    .line 24
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;->get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v0

    .line 25
    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;-><init>(I)V

    .line 27
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;->startCallComposerSession(Ljava/lang/String;)Lcom/google/android/rcs/client/enrichedcall/EnrichedCallServiceResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 28
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 29
    const-string v2, "RcsClientLib"

    const-string v3, "Error while starting call composer session: "

    .line 30
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    :goto_1
    invoke-static {v2, v0}, Lhcw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    new-instance v2, Lgek;

    const-string v3, "Error while starting call composer session: "

    .line 33
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v2, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v2

    .line 30
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 33
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method
