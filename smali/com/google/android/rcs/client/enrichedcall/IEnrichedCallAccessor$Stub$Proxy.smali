.class public Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.rcs.client.enrichedcall.IEnrichedCallAccessor"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public get()Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCallAccessor$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/rcs/client/enrichedcall/IEnrichedCall;

    move-result-object v1

    .line 6
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 7
    return-object v1
.end method
