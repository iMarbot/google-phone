.class public Lcom/google/android/rcs/client/utils/Content;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lgey;

    invoke-direct {v0}, Lgey;-><init>()V

    sput-object v0, Lcom/google/android/rcs/client/utils/Content;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->a:Ljava/lang/String;

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 8
    if-lez v0, :cond_0

    .line 9
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    .line 10
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 12
    :goto_0
    return-void

    .line 11
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/rcs/client/utils/Content;->a:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    .line 4
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method public getContent()[B
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->a:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 17
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 18
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 20
    :cond_0
    return-void

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/google/android/rcs/client/utils/Content;->b:[B

    array-length v0, v0

    goto :goto_0
.end method
