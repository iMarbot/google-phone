.class public abstract Lcom/google/android/rcs/client/JibeServiceResult;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ERROR_CONNECTION_OFFLINE:I = 0x4

.field public static final ERROR_FILE_NOT_FOUND:I = 0x8

.field public static final ERROR_INVALID_DESTINATION:I = 0xa

.field public static final ERROR_NETWORK_FAILURE:I = 0x3

.field public static final ERROR_NOT_INITIALIZED:I = 0x2

.field public static final ERROR_NOT_SUPPORTED:I = 0x5

.field public static final ERROR_OTHER_PARTY_DOES_NOT_EXIST:I = 0x6

.field public static final ERROR_OTHER_PARTY_OFFLINE:I = 0x5

.field public static final ERROR_OTHER_PARTY_TEMPORARILY_NOT_REACHABLE:I = 0x7

.field public static final ERROR_SESSION_NOT_FOUND:I = 0x9

.field public static final ERROR_UNKNOWN:I = 0x1

.field public static final SUCCESS:I


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    return v0
.end method

.method public getCode()I
    .locals 1

    .prologue
    .line 6
    iget v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->b:Ljava/lang/String;

    return-object v0
.end method

.method public succeeded()Z
    .locals 1

    .prologue
    .line 5
    iget v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8
    iget v0, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    if-nez v0, :cond_0

    .line 9
    const-string v0, "OK"

    .line 34
    :goto_0
    return-object v0

    .line 10
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error: ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 11
    iget v1, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    packed-switch v1, :pswitch_data_0

    .line 28
    :pswitch_0
    iget v1, p0, Lcom/google/android/rcs/client/JibeServiceResult;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    :goto_1
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    iget-object v1, p0, Lcom/google/android/rcs/client/JibeServiceResult;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 31
    const-string v1, ", description: ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    iget-object v1, p0, Lcom/google/android/rcs/client/JibeServiceResult;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 12
    :pswitch_1
    const-string v1, "Unknown"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 14
    :pswitch_2
    const-string v1, "Remote user offline offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 16
    :pswitch_3
    const-string v1, "Network failure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 18
    :pswitch_4
    const-string v1, "Remote user unknown"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 20
    :pswitch_5
    const-string v1, "Remote user temporarily unreachable"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 22
    :pswitch_6
    const-string v1, "Local file not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 24
    :pswitch_7
    const-string v1, "Unable to find session"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 26
    :pswitch_8
    const-string v1, "IMS/RCS connection is offline"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
