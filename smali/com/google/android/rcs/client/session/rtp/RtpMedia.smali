.class public Lcom/google/android/rcs/client/session/rtp/RtpMedia;
.super Lcom/google/android/rcs/client/session/Media;
.source "PG"


# static fields
.field public static final EXT_JIBE_VIDEO_QUALITY_LEVEL:Ljava/lang/String; = "urn:jibe:video-quality-level"

.field public static final EXT_VIDEO_ORIENTATION:Ljava/lang/String; = "urn:3gpp:video-orientation"

.field private static a:Ljava/util/List;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 177
    sput-object v0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a:Ljava/util/List;

    new-instance v1, Lgex;

    const/4 v2, 0x1

    const-string v3, "urn:3gpp:video-orientation"

    invoke-direct {v1, v2, v3}, Lgex;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    sget-object v0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a:Ljava/util/List;

    new-instance v1, Lgex;

    const/4 v2, 0x2

    const-string v3, "urn:jibe:video-quality-level"

    invoke-direct {v1, v2, v3}, Lgex;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method

.method public constructor <init>(Lcom/google/android/rcs/client/session/Media;)V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/android/rcs/client/session/Media;-><init>(Lcom/google/android/rcs/client/session/Media;)V

    .line 11
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getFormats()[Lcom/google/android/rcs/client/session/Format;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/Format;->getFormat()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->b:I

    .line 12
    return-void
.end method

.method public constructor <init>(Lfci;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/rcs/client/session/Media;-><init>()V

    .line 2
    invoke-virtual {p0, p1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setType(Lfci;)V

    .line 3
    const-string v0, "RTP/AVP"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setProtocol(Ljava/lang/String;)V

    .line 4
    sget-object v0, Lfcj;->b:Lfcj;

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setDirection(Lfcj;)V

    .line 5
    return-void
.end method

.method public constructor <init>(Lfci;I)V
    .locals 4

    .prologue
    .line 6
    invoke-direct {p0, p1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lfci;)V

    .line 7
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/rcs/client/session/Format;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/rcs/client/session/Format;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/rcs/client/session/Format;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setFormats([Lcom/google/android/rcs/client/session/Format;)V

    .line 8
    iput p2, p0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->b:I

    .line 9
    return-void
.end method

.method private static a(I[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 123
    if-nez p1, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 125
    :cond_1
    array-length v1, p1

    if-eqz v1, :cond_0

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const/4 v0, 0x1

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 132
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 136
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private final a(Lgex;)V
    .locals 5

    .prologue
    .line 20
    const-string v0, "extmap"

    .line 21
    iget v1, p1, Lgex;->b:I

    iget-object v2, p1, Lgex;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-virtual {p0, v0, v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static createCreatePreferredMedia(Lfci;)Lcom/google/android/rcs/client/session/rtp/RtpMedia;
    .locals 11

    .prologue
    .line 94
    invoke-virtual {p0}, Lfci;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 101
    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    sget-object v0, Lfbz;->a:[Lfcg;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 102
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v6, v0, [Lcom/google/android/rcs/client/session/Format;

    .line 103
    new-instance v5, Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-direct {v5, p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lfci;)V

    .line 104
    const/16 v2, 0x60

    .line 105
    const/4 v0, 0x0

    move v3, v2

    move v2, v0

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 106
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcb;

    .line 107
    const-string v4, "rtpmap"

    .line 108
    iget-object v7, v0, Lfca;->a:Ljava/lang/String;

    .line 111
    iget v8, v0, Lfcb;->b:I

    .line 112
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x18

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "/"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 113
    invoke-virtual {v5, v4, v7}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, v0, Lfcb;->c:[Ljava/lang/String;

    .line 116
    invoke-static {v3, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 118
    const-string v4, "fmtp"

    invoke-virtual {v5, v4, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    new-instance v0, Lcom/google/android/rcs/client/session/Format;

    add-int/lit8 v4, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/rcs/client/session/Format;-><init>(Ljava/lang/String;)V

    aput-object v0, v6, v2

    .line 120
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_2

    .line 98
    :pswitch_1
    sget-object v0, Lfbz;->b:[Lfcf;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 100
    goto :goto_1

    .line 121
    :cond_1
    invoke-virtual {v5, v6}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setFormats([Lcom/google/android/rcs/client/session/Format;)V

    move-object v0, v5

    .line 122
    goto/16 :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static matchUp(Lcom/google/android/rcs/client/session/rtp/RtpMedia;)Lcom/google/android/rcs/client/session/rtp/RtpMedia;
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    .line 137
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getType()Lfci;

    move-result-object v1

    .line 138
    new-instance v0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getPayload()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lfci;I)V

    .line 139
    invoke-virtual {v0, v7}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalPort(I)V

    .line 140
    invoke-static {v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->createCreatePreferredMedia(Lfci;)Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    move-result-object v2

    .line 141
    if-nez v2, :cond_0

    .line 175
    :goto_0
    return-object v0

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getFormats()[Lcom/google/android/rcs/client/session/Format;

    move-result-object v3

    .line 144
    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 145
    invoke-virtual {v5}, Lcom/google/android/rcs/client/session/Format;->getFormat()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 146
    invoke-virtual {p0, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getCodec(I)Ljava/lang/String;

    move-result-object v5

    .line 147
    invoke-virtual {v2, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->supportsCodec(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 148
    new-instance v2, Lcom/google/android/rcs/client/session/rtp/RtpMedia;

    invoke-direct {v2, p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;-><init>(Lcom/google/android/rcs/client/session/Media;)V

    .line 149
    invoke-virtual {v2, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->selectFormat(Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v2, v7}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalPort(I)V

    .line 153
    sget-object v0, Lfcj;->b:Lfcj;

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setDirection(Lfcj;)V

    .line 154
    const-string v0, "crypto"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 155
    const-string v0, "X-Jibe-HD-Video"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 156
    const-string v0, "nortpproxy"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 157
    const-string v0, "nortpproxydt"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 158
    const-string v0, "nortpproxyorange"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 160
    const-string v0, "extmap"

    invoke-virtual {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getExtensions()Ljava/util/List;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgex;

    .line 163
    sget-object v1, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgex;

    .line 165
    iget-object v5, v0, Lgex;->a:Ljava/lang/String;

    .line 167
    iget-object v1, v1, Lgex;->a:Ljava/lang/String;

    .line 168
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 169
    invoke-direct {v2, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a(Lgex;)V

    goto :goto_2

    :cond_3
    move-object v0, v2

    .line 172
    goto/16 :goto_0

    .line 173
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 174
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setLocalPortReject()V

    goto/16 :goto_0
.end method


# virtual methods
.method public addExtensions(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 16
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgex;

    .line 17
    invoke-direct {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a(Lgex;)V

    goto :goto_0

    .line 19
    :cond_0
    return-void
.end method

.method public addSupportedExtensions()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->addExtensions(Ljava/util/List;)V

    .line 15
    return-void
.end method

.method public getCodec(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xc

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 50
    const-string v0, "rtpmap"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getParameterValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 51
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 52
    aget-object v3, v2, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 53
    aget-object v3, v2, v0

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 54
    if-gez v3, :cond_0

    .line 55
    aget-object v0, v2, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_1
    return-object v0

    .line 56
    :cond_0
    aget-object v0, v2, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 57
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getExtensions()Ljava/util/List;
    .locals 5

    .prologue
    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    const-string v1, "extmap"

    invoke-virtual {p0, v1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getParameterValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 26
    if-nez v2, :cond_1

    .line 33
    :cond_0
    return-object v0

    .line 28
    :cond_1
    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 29
    invoke-static {v4}, Lgex;->a(Ljava/lang/String;)Lgex;

    move-result-object v4

    .line 30
    if-eqz v4, :cond_2

    .line 31
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getPayload()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->b:I

    return v0
.end method

.method public getPayload(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 65
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getFormats()[Lcom/google/android/rcs/client/session/Format;

    move-result-object v3

    .line 66
    if-eqz v3, :cond_0

    array-length v0, v3

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 74
    :cond_1
    :goto_0
    return v0

    .line 68
    :cond_2
    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 69
    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/Format;->getFormat()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getCodec(I)Ljava/lang/String;

    move-result-object v5

    .line 71
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 73
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 74
    goto :goto_0
.end method

.method public getSelectedFormat()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getFormats()[Lcom/google/android/rcs/client/session/Format;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/rcs/client/session/Format;->getFormat()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getCodec(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeSecure()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "RTP/SAVP"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setProtocol(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public selectFormat(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getPayload(Ljava/lang/String;)I

    move-result v2

    .line 37
    invoke-virtual {p0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getFormats()[Lcom/google/android/rcs/client/session/Format;

    move-result-object v3

    .line 38
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 39
    invoke-virtual {v5}, Lcom/google/android/rcs/client/session/Format;->getFormat()Ljava/lang/String;

    move-result-object v5

    .line 40
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 41
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 42
    const-string v6, "fmtp"

    invoke-virtual {p0, v6, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v6, "rtpmap"

    invoke-virtual {p0, v6, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v6, "framesize"

    invoke-virtual {p0, v6, v5}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_1
    iput v2, p0, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->b:I

    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/rcs/client/session/Format;

    new-instance v3, Lcom/google/android/rcs/client/session/Format;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/android/rcs/client/session/Format;-><init>(Ljava/lang/String;)V

    aput-object v3, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->setFormats([Lcom/google/android/rcs/client/session/Format;)V

    .line 48
    return-void
.end method

.method public setDirection(Lfcj;)V
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lfcj;->b:Lfcj;

    .line 78
    iget-object v0, v0, Lfcj;->f:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 80
    sget-object v0, Lfcj;->d:Lfcj;

    .line 81
    iget-object v0, v0, Lfcj;->f:Ljava/lang/String;

    .line 82
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 83
    sget-object v0, Lfcj;->c:Lfcj;

    .line 84
    iget-object v0, v0, Lfcj;->f:Ljava/lang/String;

    .line 85
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 86
    sget-object v0, Lfcj;->e:Lfcj;

    .line 87
    iget-object v0, v0, Lfcj;->f:Ljava/lang/String;

    .line 88
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 89
    sget-object v0, Lfcj;->a:Lfcj;

    .line 90
    iget-object v0, v0, Lfcj;->f:Ljava/lang/String;

    .line 91
    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->removeParameter(Ljava/lang/String;)V

    .line 92
    invoke-super {p0, p1}, Lcom/google/android/rcs/client/session/Media;->setDirection(Lfcj;)V

    .line 93
    return-void
.end method

.method public supportsCodec(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    const-string v0, "rtpmap"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/rtp/RtpMedia;->getParameterValues(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 60
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 61
    aget-object v3, v2, v0

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    const/4 v1, 0x1

    .line 64
    :cond_0
    return v1

    .line 63
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
