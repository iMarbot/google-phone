.class public Lcom/google/android/rcs/client/session/Media;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lfci;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:[Lcom/google/android/rcs/client/session/Format;

.field private f:Lfcj;

.field private g:Ljava/util/ArrayList;

.field private h:Lgew;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 198
    new-instance v0, Lgev;

    invoke-direct {v0}, Lgev;-><init>()V

    sput-object v0, Lcom/google/android/rcs/client/session/Media;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lfcj;->a:Lfcj;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 3
    sget-object v0, Lgew;->a:Lgew;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    .line 5
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    sget-object v0, Lfcj;->a:Lfcj;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 8
    sget-object v0, Lgew;->a:Lgew;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    .line 10
    invoke-virtual {p0, p1}, Lcom/google/android/rcs/client/session/Media;->readFromParcel(Landroid/os/Parcel;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Lcom/google/android/rcs/client/session/Media;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lfcj;->a:Lfcj;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 14
    sget-object v0, Lgew;->a:Lgew;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    .line 15
    iput v3, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    .line 16
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    .line 17
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    .line 18
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 19
    iget v0, p1, Lcom/google/android/rcs/client/session/Media;->c:I

    iput v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    .line 20
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    .line 21
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/rcs/client/session/Format;

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    .line 23
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    iget-object v2, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 24
    :cond_0
    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    .line 26
    :cond_1
    return-void
.end method


# virtual methods
.method public final addParameter(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/rcs/client/session/Media;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public final addParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public clearParameters()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 112
    return-void
.end method

.method public final containsParameterValue(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v3

    .line 160
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v1, v2

    .line 155
    :goto_1
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 157
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    :cond_3
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_1

    :cond_4
    move v2, v3

    .line 160
    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public getBandwith()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    return v0
.end method

.method public getBandwithScope()Lgew;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    return-object v0
.end method

.method public getDirection()Lfcj;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    return-object v0
.end method

.method public getFormats()[Lcom/google/android/rcs/client/session/Format;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    return-object v0
.end method

.method public getLocalInterface()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPort()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    return v0
.end method

.method public getParameterNames()[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 69
    new-array v0, v0, [Ljava/lang/String;

    .line 74
    :goto_0
    return-object v0

    .line 70
    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move v1, v0

    .line 71
    :goto_1
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_1

    .line 74
    :cond_1
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getParameterValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move-object v0, v2

    .line 102
    :goto_0
    return-object v0

    .line 98
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 101
    :cond_1
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 102
    goto :goto_0
.end method

.method public getParameterValues(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    .line 105
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 106
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 107
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_1
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_1

    .line 110
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getSpropParameter()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 161
    const-string v0, "rtpmap"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/Media;->getParameterValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 163
    const-string v0, "RcsClientLib"

    const-string v2, "Media description contains no RTP mapping"

    invoke-static {v0, v2}, Lhcw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :goto_0
    return-object v1

    .line 165
    :cond_0
    const-string v2, "H264"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    const-string v0, "RcsClientLib"

    const-string v2, "Media format is not H264, no need to set SPS/PPS"

    invoke-static {v0, v2}, Lhcw;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_1
    const-string v0, "sprop-parameter-sets"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/Media;->containsParameterValue(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 169
    const-string v0, "RcsClientLib"

    const-string v2, "Media description contains no SPS/PPS information."

    invoke-static {v0, v2}, Lhcw;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_2
    const-string v0, "fmtp"

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/Media;->getParameterValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173
    const-string v0, "sprop-parameter-sets"

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 174
    if-gez v0, :cond_3

    move-object v0, v1

    .line 182
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 183
    const-string v0, "RcsClientLib"

    const-string v2, "Unable to extract sprop parameter set!"

    invoke-static {v0, v2}, Lhcw;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_3
    add-int/lit8 v0, v0, 0x14

    add-int/lit8 v3, v0, 0x1

    .line 177
    const-string v0, ";"

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 178
    if-gez v0, :cond_4

    .line 179
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 180
    :cond_4
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v1, v0

    .line 185
    goto :goto_0
.end method

.method public getType()Lfci;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    return-object v0
.end method

.method public final hasParameter(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 147
    :goto_1
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 149
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    const/4 v2, 0x1

    goto :goto_0

    .line 151
    :cond_2
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_1
.end method

.method public isLocalPortChoose()Z
    .locals 2

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    const v1, 0x7ffffffe

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocalPortReject()Z
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocalPortValid()Z
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReceiving()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    invoke-virtual {v0}, Lfcj;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 60
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 59
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isSending()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    invoke-virtual {v0}, Lfcj;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 57
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 56
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 132
    invoke-static {}, Lfci;->values()[Lfci;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    .line 136
    invoke-static {}, Lfcj;->values()[Lfcj;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 137
    invoke-static {}, Lgew;->values()[Lgew;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 141
    array-length v1, v0

    new-array v1, v1, [Lcom/google/android/rcs/client/session/Format;

    iput-object v1, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    .line 142
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    iget-object v2, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    .line 144
    return-void
.end method

.method public removeParameter(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 82
    :cond_0
    return-void

    .line 77
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 78
    if-ltz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 80
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public removeParameter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 95
    :cond_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 89
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 90
    :cond_2
    add-int/lit8 v0, v2, 0x2

    move v2, v0

    .line 91
    goto :goto_0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setBandwith(Lgew;I)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    .line 53
    iput p2, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    .line 54
    return-void
.end method

.method public setDirection(Lfcj;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    .line 49
    return-void
.end method

.method public setFormats([Lcom/google/android/rcs/client/session/Format;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    .line 46
    return-void
.end method

.method public setLocalInterface(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setLocalPort(I)V
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    .line 35
    return-void
.end method

.method public setLocalPortReject()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/rcs/client/session/Media;->setLocalPort(I)V

    .line 37
    return-void
.end method

.method public setParameterValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 114
    invoke-virtual {p0, p1, p2}, Lcom/google/android/rcs/client/session/Media;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void

    .line 116
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_2
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_0
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setType(Lfci;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    .line 29
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    const-string v1, "Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    const-string v1, ", local interface: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    iget v1, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    const-string v1, ", protocol: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    const-string v1, ", formats: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    iget-object v1, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->a:Lfci;

    invoke-virtual {v0}, Lfci;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->f:Lfcj;

    invoke-virtual {v0}, Lfcj;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->h:Lgew;

    invoke-virtual {v0}, Lgew;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget v0, p0, Lcom/google/android/rcs/client/session/Media;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->e:[Lcom/google/android/rcs/client/session/Format;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeArray([Ljava/lang/Object;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/rcs/client/session/Media;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 131
    return-void
.end method
