.class public Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/rcs/client/videoshare/IVideoShare;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.rcs.client.videoshare.IVideoShare"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public acceptVideoShareSession(JLcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    invoke-static {v0, p3}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    .line 30
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 31
    sget-object v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    .line 32
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 33
    return-object v0
.end method

.method public endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 35
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 36
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 37
    sget-object v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    .line 38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 39
    return-object v0
.end method

.method public getActiveSessions()[J
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v1

    .line 6
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 7
    return-object v1
.end method

.method public getLocalMedia(J)[Lcom/google/android/rcs/client/session/Media;
    .locals 3

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 9
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 10
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 11
    sget-object v0, Lcom/google/android/rcs/client/session/Media;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/rcs/client/session/Media;

    .line 12
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 13
    return-object v0
.end method

.method public getRemoteMedia(J)[Lcom/google/android/rcs/client/session/Media;
    .locals 3

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 15
    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 16
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 17
    sget-object v0, Lcom/google/android/rcs/client/session/Media;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/rcs/client/session/Media;

    .line 18
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 19
    return-object v0
.end method

.method public getVersion()I
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 46
    const/16 v1, 0x8

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 48
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 49
    return v1
.end method

.method public shouldUseSecureSession()Z
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 41
    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 42
    invoke-static {v0}, Ldii;->a(Landroid/os/Parcel;)Z

    move-result v1

    .line 43
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 44
    return v1
.end method

.method public startVideoShareSession(Ljava/lang/String;Lcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 21
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 22
    invoke-static {v0, p2}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    .line 23
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    .line 24
    sget-object v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    .line 25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 26
    return-object v0
.end method
