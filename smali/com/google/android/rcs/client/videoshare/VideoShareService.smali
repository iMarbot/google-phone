.class public Lcom/google/android/rcs/client/videoshare/VideoShareService;
.super Lgng;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgel;)V
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/google/android/rcs/client/videoshare/IVideoShareAccessor;

    invoke-direct {p0, v0, p1, p2}, Lgng;-><init>(Ljava/lang/Class;Landroid/content/Context;Lgel;)V

    .line 2
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    const-string v0, "com.google.android.rcs.service.service.VideoShareBindingService"

    return-object v0
.end method

.method public endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    .locals 5

    .prologue
    .line 3
    invoke-virtual {p0}, Lgng;->c()V

    .line 4
    :try_start_0
    invoke-virtual {p0}, Lgng;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/videoshare/IVideoShareAccessor;

    invoke-interface {v0}, Lcom/google/android/rcs/client/videoshare/IVideoShareAccessor;->get()Lcom/google/android/rcs/client/videoshare/IVideoShare;

    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;-><init>(I)V

    .line 7
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/rcs/client/videoshare/IVideoShare;->endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    const-string v1, "RcsClientLib"

    const-string v2, "Error while ending video share session: "

    invoke-static {v1, v2, v0}, Lhcw;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10
    new-instance v1, Lgek;

    const-string v2, "Error while ending video share session: "

    .line 11
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Lgek;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
