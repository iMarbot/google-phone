.class public abstract Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/rcs/client/videoshare/IVideoShare;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/rcs/client/videoshare/IVideoShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_acceptVideoShareSession:I = 0x5

.field public static final TRANSACTION_endVideoShareSession:I = 0x6

.field public static final TRANSACTION_getActiveSessions:I = 0x1

.field public static final TRANSACTION_getLocalMedia:I = 0x2

.field public static final TRANSACTION_getRemoteMedia:I = 0x3

.field public static final TRANSACTION_getVersion:I = 0x8

.field public static final TRANSACTION_shouldUseSecureSession:I = 0x7

.field public static final TRANSACTION_startVideoShareSession:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.rcs.client.videoshare.IVideoShare"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/rcs/client/videoshare/IVideoShare;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.rcs.client.videoshare.IVideoShare"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/rcs/client/videoshare/IVideoShare;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/rcs/client/videoshare/IVideoShare;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 53
    :goto_0
    return v0

    .line 12
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 52
    const/4 v0, 0x0

    goto :goto_0

    .line 13
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->getActiveSessions()[J

    move-result-object v0

    .line 14
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    :goto_1
    move v0, v1

    .line 53
    goto :goto_0

    .line 17
    :pswitch_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 18
    invoke-virtual {p0, v2, v3}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->getLocalMedia(J)[Lcom/google/android/rcs/client/session/Media;

    move-result-object v0

    .line 19
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 20
    invoke-virtual {p3, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 22
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 23
    invoke-virtual {p0, v2, v3}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->getRemoteMedia(J)[Lcom/google/android/rcs/client/session/Media;

    move-result-object v0

    .line 24
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 25
    invoke-virtual {p3, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 27
    :pswitch_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 28
    sget-object v0, Lcom/google/android/rcs/client/session/Media;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/session/Media;

    .line 29
    invoke-virtual {p0, v2, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->startVideoShareSession(Ljava/lang/String;Lcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v0

    .line 30
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 31
    invoke-static {p3, v0}, Ldii;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 33
    :pswitch_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 34
    sget-object v0, Lcom/google/android/rcs/client/session/Media;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Ldii;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/rcs/client/session/Media;

    .line 35
    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->acceptVideoShareSession(JLcom/google/android/rcs/client/session/Media;)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v0

    .line 36
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 37
    invoke-static {p3, v0}, Ldii;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 39
    :pswitch_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 40
    invoke-virtual {p0, v2, v3}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->endVideoShareSession(J)Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;

    move-result-object v0

    .line 41
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 42
    invoke-static {p3, v0}, Ldii;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 44
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->shouldUseSecureSession()Z

    move-result v0

    .line 45
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 46
    invoke-static {p3, v0}, Ldii;->a(Landroid/os/Parcel;Z)V

    goto :goto_1

    .line 48
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/rcs/client/videoshare/IVideoShare$Stub;->getVersion()I

    move-result v0

    .line 49
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 12
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
