.class public Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;
.super Lcom/google/android/rcs/client/JibeServiceResult;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lgez;

    invoke-direct {v0}, Lgez;-><init>()V

    sput-object v0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;-><init>(JI)V

    .line 2
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;-><init>(JILjava/lang/String;)V

    .line 4
    return-void
.end method

.method public constructor <init>(JILjava/lang/String;)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/google/android/rcs/client/JibeServiceResult;-><init>()V

    .line 6
    iput p3, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->a:I

    .line 7
    iput-object p4, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->b:Ljava/lang/String;

    .line 8
    iput-wide p1, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->c:J

    .line 9
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/rcs/client/JibeServiceResult;-><init>()V

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->a:I

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->b:Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->c:J

    .line 14
    return-void
.end method


# virtual methods
.method public getSessionId()J
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->c:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 17
    iget-object v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 18
    iget-wide v0, p0, Lcom/google/android/rcs/client/videoshare/VideoShareServiceResult;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 19
    return-void
.end method
