.class public final Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;
.super Lepr;

# interfaces
.implements Lcom/google/android/gms/common/internal/ReflectedParcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ldxc;

    invoke-direct {v0}, Ldxc;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZZZI)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->b:Z

    iput-boolean p3, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->c:Z

    const/4 v0, 0x2

    if-ge p1, v0, :cond_1

    if-eqz p4, :cond_0

    const/4 v0, 0x3

    :goto_0
    iput v0, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:I

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iput p5, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:I

    goto :goto_1
.end method

.method constructor <init>(Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1
    const/4 v1, 0x2

    .line 4
    iget-boolean v3, p1, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->a:Z

    .line 6
    iget v5, p1, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->b:I

    move-object v0, p0

    move v4, v2

    .line 7
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;-><init>(IZZZI)V

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x1

    .line 8
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v1

    .line 9
    iget-boolean v2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->b:Z

    .line 10
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v2, 0x2

    .line 11
    iget-boolean v3, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->c:Z

    .line 12
    invoke-static {p1, v2, v3}, Letf;->a(Landroid/os/Parcel;IZ)V

    .line 13
    iget v2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:I

    if-ne v2, v4, :cond_0

    .line 14
    :goto_0
    invoke-static {p1, v4, v0}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v0, 0x4

    iget v2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->d:I

    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/16 v0, 0x3e8

    iget v2, p0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;->a:I

    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    invoke-static {p1, v1}, Letf;->H(Landroid/os/Parcel;I)V

    return-void

    .line 13
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
