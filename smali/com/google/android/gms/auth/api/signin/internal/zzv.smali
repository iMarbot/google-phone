.class public final Lcom/google/android/gms/auth/api/signin/internal/zzv;
.super Lcom/google/android/gms/auth/api/signin/internal/zzq;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/api/signin/internal/zzq;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/auth/api/signin/internal/zzv;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final zzzx()V
    .locals 5

    .prologue
    .line 1
    iget-object v0, p0, Lcom/google/android/gms/auth/api/signin/internal/zzv;->a:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lecs;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    const/16 v2, 0x34

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Calling UID "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not Google Play services."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/api/signin/internal/zzv;->a:Landroid/content/Context;

    invoke-static {v0}, Ldxv;->a(Landroid/content/Context;)Ldxv;

    move-result-object v1

    invoke-virtual {v1}, Ldxv;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->c:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    if-eqz v2, :cond_1

    .line 2
    const-string v0, "defaultGoogleSignInAccount"

    invoke-virtual {v1, v0}, Ldxv;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldxv;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object v0

    .line 3
    :cond_1
    new-instance v1, Ledk;

    iget-object v3, p0, Lcom/google/android/gms/auth/api/signin/internal/zzv;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, Ledk;-><init>(Landroid/content/Context;)V

    sget-object v3, Ldwk;->a:Lesq;

    .line 4
    const-string v4, "Api must not be null"

    invoke-static {v3, v4}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "Null options are not permitted for this Api"

    invoke-static {v0, v4}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v1, Ledk;->c:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Lesq;->a()Letg;

    move-result-object v3

    invoke-virtual {v3, v0}, Letg;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v3, v1, Ledk;->b:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v3, v1, Ledk;->a:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 5
    invoke-virtual {v1}, Ledk;->b()Ledj;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ledj;->f()Lecl;

    move-result-object v0

    invoke-virtual {v0}, Lecl;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_3

    sget-object v0, Ldwk;->b:Ldxl;

    invoke-virtual {v0, v1}, Ldxl;->a(Ledj;)Ledn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    invoke-virtual {v1}, Ledj;->g()V

    return-void

    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ledj;->i()Ledn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ledj;->g()V

    throw v0
.end method
