.class public interface abstract Lcom/google/android/gms/auth/firstparty/dataservice/zzaw;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract checkAccountName(Ldye;)Ldyf;
.end method

.method public abstract checkPassword(Ldyy;)Ldyz;
.end method

.method public abstract checkRealName(Ldyl;)Ldym;
.end method

.method public abstract clearFactoryResetChallenges()V
.end method

.method public abstract clearFre()V
.end method

.method public abstract clearToken(Ldyn;)Ldyo;
.end method

.method public abstract confirmCredentials(Ldyp;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract createAccount(Ldyt;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract createPlusProfile(Ldyt;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract getAccountChangeEvents(Ldwc;)Ldwd;
.end method

.method public abstract getAccountExportData(Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract getAccountId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getAccountVisibilityRestriction(Landroid/accounts/Account;)[Ljava/lang/String;
.end method

.method public abstract getAndAdvanceOtpCounter(Ljava/lang/String;)Ldyr;
.end method

.method public abstract getDeviceManagementInfo(Landroid/accounts/Account;)Ldyq;
.end method

.method public abstract getGoogleAccountData(Landroid/accounts/Account;)Ldys;
.end method

.method public abstract getGoogleAccountId(Landroid/accounts/Account;)Ljava/lang/String;
.end method

.method public abstract getGplusInfo(Ldyu;)Ldyv;
.end method

.method public abstract getOtp(Ldyw;)Ldyx;
.end method

.method public abstract getToken(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract getTokenHandle(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract installAccountFromExportData(Ljava/lang/String;Landroid/os/Bundle;)Z
.end method

.method public abstract isTokenHandleValid(Ljava/lang/String;)Z
.end method

.method public abstract removeAccount(Ldyg;)Ldyh;
.end method

.method public abstract setAccountVisibilityRestriction(Landroid/accounts/Account;[Ljava/lang/String;)Z
.end method

.method public abstract setFreUnlocked()V
.end method

.method public abstract signIn(Ldyi;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract updateCredentials(Ldzb;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.end method

.method public abstract validateAccountCredentials(Leao;)Ldzc;
.end method

.method public abstract zza(Ldyj;)Ldyk;
.end method
