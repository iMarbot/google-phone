.class public final Lcom/google/android/gms/auth/firstparty/dataservice/zzax;
.super Lcom/google/android/gms/internal/zzee;

# interfaces
.implements Lcom/google/android/gms/auth/firstparty/dataservice/zzaw;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    const-string v0, "com.google.android.gms.auth.firstparty.dataservice.IGoogleAccountDataService"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzee;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final checkAccountName(Ldye;)Ldyf;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyf;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyf;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final checkPassword(Ldyy;)Ldyz;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyz;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyz;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final checkRealName(Ldyl;)Ldym;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldym;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldym;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final clearFactoryResetChallenges()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0x1d

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final clearFre()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final clearToken(Ldyn;)Ldyo;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x13

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyo;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final confirmCredentials(Ldyp;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final createAccount(Ldyt;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final createPlusProfile(Ldyt;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getAccountChangeEvents(Ldwc;)Ldwd;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x17

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldwd;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldwd;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getAccountExportData(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x10

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getAccountId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x19

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v1
.end method

.method public final getAccountVisibilityRestriction(Landroid/accounts/Account;)[Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x2a

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v1
.end method

.method public final getAndAdvanceOtpCounter(Ljava/lang/String;)Ldyr;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x25

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyr;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyr;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getDeviceManagementInfo(Landroid/accounts/Account;)Ldyq;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x28

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyq;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getGoogleAccountData(Landroid/accounts/Account;)Ldys;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x1e

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldys;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldys;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getGoogleAccountId(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x1f

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v1
.end method

.method public final getGplusInfo(Ldyu;)Ldyv;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyv;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyv;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getOtp(Ldyw;)Ldyx;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x18

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyx;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyx;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getToken(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x8

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final getTokenHandle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x26

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v1
.end method

.method public final installAccountFromExportData(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x11

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0}, Lerk;->a(Landroid/os/Parcel;)Z

    move-result v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return v1
.end method

.method public final isTokenHandleValid(Ljava/lang/String;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x27

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0}, Lerk;->a(Landroid/os/Parcel;)Z

    move-result v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return v1
.end method

.method public final removeAccount(Ldyg;)Ldyh;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x14

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyh;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyh;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final setAccountVisibilityRestriction(Landroid/accounts/Account;[Ljava/lang/String;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    const/16 v1, 0x29

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0}, Lerk;->a(Landroid/os/Parcel;)Z

    move-result v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return v1
.end method

.method public final setFreUnlocked()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0x2b

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final signIn(Ldyi;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x9

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final updateCredentials(Ldzb;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final validateAccountCredentials(Leao;)Ldzc;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x24

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldzc;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldzc;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final zza(Ldyj;)Ldyk;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x1b

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Ldyk;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldyk;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method
