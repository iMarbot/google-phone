.class public abstract Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;
.super Lcom/google/android/gms/internal/zzef;

# interfaces
.implements Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzef;-><init>()V

    const-string v0, "com.google.android.gms.auth.firstparty.delegate.IAuthDelegateService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.auth.firstparty.delegate.IAuthDelegateService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzef;->zza(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getSetupAccountWorkflowIntent(Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getTokenRetrievalWorkflowIntent(Lcom/google/android/gms/auth/firstparty/delegate/TokenWorkflowRequest;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_2
    sget-object v0, Leah;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leah;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getUpdateCredentialsWorkflowIntent(Leah;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_3
    sget-object v0, Leae;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leae;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getConfirmCredentialsWorkflowIntent(Leae;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_4
    sget-object v0, Leag;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leag;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getStartAddAccountSessionWorkflowIntent(Leag;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_5
    sget-object v0, Leah;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leah;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getStartUpdateCredentialsSessionWorkflowIntent(Leah;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_6
    sget-object v0, Leaf;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leaf;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/delegate/IAuthDelegateService$Stub;->getFinishSessionWorkflowIntent(Leaf;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-static {p3, v0}, Lerk;->b(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
