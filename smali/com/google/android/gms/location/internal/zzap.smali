.class public final Lcom/google/android/gms/location/internal/zzap;
.super Lcom/google/android/gms/internal/zzee;

# interfaces
.implements Lcom/google/android/gms/location/internal/zzao;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzee;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final zza(JZLandroid/app/PendingIntent;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lerk;->a(Landroid/os/Parcel;Z)V

    invoke-static {v0, p4}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x49

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Landroid/location/Location;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v1, 0x1a

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lcom/google/android/gms/location/internal/zzaj;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x43

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lesi;Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p3}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x46

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lesk;Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p3}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x48

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lesp;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/zzam;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p3}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x39

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lesv;Lcom/google/android/gms/location/internal/zzaq;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x3f

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Leto;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0x3b

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zza(Lewv;Lcom/google/android/gms/location/internal/zzam;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x4a

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzb(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x41

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzbz(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Z)V

    const/16 v1, 0xc

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzc(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x42

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzc(Landroid/location/Location;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/16 v1, 0xd

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzd(Landroid/app/PendingIntent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzd(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x44

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zze(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    invoke-static {v0, p2}, Lerk;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    const/16 v1, 0x45

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final zzii(Ljava/lang/String;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x40

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/location/ActivityRecognitionResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final zzij(Ljava/lang/String;)Landroid/location/Location;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x15

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final zzik(Ljava/lang/String;)Lcom/google/android/gms/location/LocationAvailability;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/16 v1, 0x22

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zza(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/location/LocationAvailability;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v1, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/LocationAvailability;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method
