.class public interface abstract Lcom/google/android/gms/location/internal/zzao;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract zza(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract zza(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zza(Landroid/location/Location;I)V
.end method

.method public abstract zza(Lcom/google/android/gms/location/internal/zzaj;)V
.end method

.method public abstract zza(Lesi;Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zza(Lesk;Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zza(Lesp;Landroid/app/PendingIntent;Lcom/google/android/gms/location/internal/zzam;)V
.end method

.method public abstract zza(Lesv;Lcom/google/android/gms/location/internal/zzaq;Ljava/lang/String;)V
.end method

.method public abstract zza(Leto;)V
.end method

.method public abstract zza(Lewv;Lcom/google/android/gms/location/internal/zzam;)V
.end method

.method public abstract zzb(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zzbz(Z)V
.end method

.method public abstract zzc(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zzc(Landroid/location/Location;)V
.end method

.method public abstract zzd(Landroid/app/PendingIntent;)V
.end method

.method public abstract zzd(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zze(Landroid/app/PendingIntent;Lcom/google/android/gms/common/api/internal/zzcb;)V
.end method

.method public abstract zzii(Ljava/lang/String;)Lcom/google/android/gms/location/ActivityRecognitionResult;
.end method

.method public abstract zzij(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract zzik(Ljava/lang/String;)Lcom/google/android/gms/location/LocationAvailability;
.end method
