.class public final Lcom/google/android/gms/location/places/internal/PlaceEntity;
.super Lepr;

# interfaces
.implements Lcom/google/android/gms/common/internal/ReflectedParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/os/Bundle;

.field private c:Leux;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private d:Lcom/google/android/gms/maps/model/LatLng;

.field private e:F

.field private f:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private g:Ljava/lang/String;

.field private h:Landroid/net/Uri;

.field private i:Z

.field private j:F

.field private k:I

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/util/List;

.field private s:Leuz;

.field private t:Leus;

.field private u:Ljava/lang/String;

.field private v:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leur;

    invoke-direct {v0}, Leur;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/maps/model/LatLng;FLcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;Landroid/net/Uri;ZFILeux;Leuz;Leus;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lepr;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->m:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->l:Ljava/util/List;

    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->b:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->n:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->o:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->p:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->q:Ljava/lang/String;

    if-eqz p9, :cond_1

    :goto_1
    iput-object p9, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->r:Ljava/util/List;

    iput-object p10, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->d:Lcom/google/android/gms/maps/model/LatLng;

    iput p11, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->e:F

    iput-object p12, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-eqz p13, :cond_2

    :goto_2
    iput-object p13, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->g:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->h:Landroid/net/Uri;

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->i:Z

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->j:F

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->k:I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->v:Ljava/util/Locale;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->c:Leux;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->s:Leuz;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->t:Leus;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->u:Ljava/lang/String;

    return-void

    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p9

    goto :goto_1

    :cond_2
    const-string p13, "UTC"

    goto :goto_2
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/location/places/internal/PlaceEntity;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/location/places/internal/PlaceEntity;

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v4, v4}, Letf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    invoke-static {p0}, Letf;->a(Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "placeTypes"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->m:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "locale"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "address"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "phoneNumber"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "latlng"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->d:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "viewport"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "websiteUri"

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "isPermanentlyClosed"

    iget-boolean v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    const-string v1, "priceLevel"

    iget v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lein;->a(Ljava/lang/String;Ljava/lang/Object;)Lein;

    move-result-object v0

    invoke-virtual {v0}, Lein;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v1

    const/4 v0, 0x1

    .line 2
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->a:Ljava/lang/String;

    .line 3
    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->b:Landroid/os/Bundle;

    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->c:Leux;

    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x4

    .line 4
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->d:Lcom/google/android/gms/maps/model/LatLng;

    .line 5
    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x5

    .line 6
    iget v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->e:F

    .line 7
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v0, 0x6

    .line 8
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->f:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 9
    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->g:Ljava/lang/String;

    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0x8

    .line 10
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->h:Landroid/net/Uri;

    .line 11
    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v0, 0x9

    .line 12
    iget-boolean v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->i:Z

    .line 13
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v0, 0xa

    .line 14
    iget v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->j:F

    .line 15
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/16 v0, 0xb

    .line 16
    iget v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->k:I

    .line 17
    invoke-static {p1, v0, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->l:Ljava/util/List;

    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;)V

    const/16 v2, 0xe

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->o:Ljava/lang/String;

    .line 19
    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v2, v0, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v2, 0xf

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->p:Ljava/lang/String;

    .line 21
    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v2, v0, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0x10

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->q:Ljava/lang/String;

    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0x11

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->r:Ljava/util/List;

    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/16 v2, 0x13

    .line 22
    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->n:Ljava/lang/String;

    .line 23
    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v2, v0, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0x14

    .line 24
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->m:Ljava/util/List;

    .line 25
    invoke-static {p1, v0, v2}, Letf;->a(Landroid/os/Parcel;ILjava/util/List;)V

    const/16 v0, 0x15

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->s:Leuz;

    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v0, 0x16

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->t:Leus;

    invoke-static {p1, v0, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v0, 0x17

    .line 26
    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEntity;->u:Ljava/lang/String;

    .line 27
    invoke-static {p1, v0, v2, v3}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v1}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
