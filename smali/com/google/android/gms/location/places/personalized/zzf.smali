.class public abstract Lcom/google/android/gms/location/places/personalized/zzf;
.super Lcom/google/android/gms/internal/zzef;

# interfaces
.implements Lcom/google/android/gms/location/places/personalized/zze;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzef;-><init>()V

    const-string v0, "com.google.android.gms.location.places.personalized.IAliasedPlacesCallbacks"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/location/places/personalized/zzf;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzef;->zza(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    sget-object v0, Levl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Levl;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/location/places/personalized/zzf;->zza(Levl;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v0, Levl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Levl;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/location/places/personalized/zzf;->zzb(Levl;)V

    goto :goto_1

    :pswitch_2
    sget-object v0, Levl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Levl;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/location/places/personalized/zzf;->zzc(Levl;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
