.class public interface abstract Lcom/google/android/gms/googlehelp/internal/common/zzag;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract zza(Landroid/graphics/Bitmap;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Landroid/os/Bundle;JLcom/google/android/gms/googlehelp/GoogleHelp;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/graphics/Bitmap;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Lcom/google/android/gms/googlehelp/GoogleHelp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Lemg;Landroid/os/Bundle;JLcom/google/android/gms/googlehelp/GoogleHelp;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zza(Ljava/lang/String;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zzb(Landroid/os/Bundle;JLcom/google/android/gms/googlehelp/GoogleHelp;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zzb(Lcom/google/android/gms/googlehelp/GoogleHelp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method

.method public abstract zzb(Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;)V
.end method
