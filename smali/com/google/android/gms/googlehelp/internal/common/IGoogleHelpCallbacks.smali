.class public interface abstract Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;
    }
.end annotation


# virtual methods
.method public abstract onAsyncPsbdSaved()V
.end method

.method public abstract onAsyncPsdSaved()V
.end method

.method public abstract onC2cSupportRequestFailed()V
.end method

.method public abstract onC2cSupportRequestSuccess()V
.end method

.method public abstract onChatSupportRequestFailed()V
.end method

.method public abstract onChatSupportRequestSuccess(I)V
.end method

.method public abstract onEscalationOptionsReceived([B)V
.end method

.method public abstract onEscalationOptionsRequestFailed()V
.end method

.method public abstract onGoogleHelpProcessed(Lcom/google/android/gms/googlehelp/GoogleHelp;)V
.end method

.method public abstract onPipClick()V
.end method

.method public abstract onPipInCallingAppDisabled()V
.end method

.method public abstract onPipInCallingAppHidden()V
.end method

.method public abstract onPipShown()V
.end method

.method public abstract onSuggestionsReceived([B)V
.end method

.method public abstract onSuggestionsRequestFailed()V
.end method

.method public abstract onTogglingPipProcessed(Lcom/google/android/gms/googlehelp/internal/common/TogglingData;)V
.end method
