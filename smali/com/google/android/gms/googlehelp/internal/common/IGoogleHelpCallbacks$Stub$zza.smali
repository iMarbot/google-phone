.class public final Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub$zza;
.super Lcom/google/android/gms/internal/zzee;

# interfaces
.implements Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zza"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    const-string v0, "com.google.android.gms.googlehelp.internal.common.IGoogleHelpCallbacks"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzee;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final onAsyncPsbdSaved()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzc(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onAsyncPsdSaved()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzc(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onC2cSupportRequestFailed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onC2cSupportRequestSuccess()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onChatSupportRequestFailed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onChatSupportRequestSuccess(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v1, 0x9

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onEscalationOptionsReceived([B)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    const/16 v1, 0xf

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onEscalationOptionsRequestFailed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onGoogleHelpProcessed(Lcom/google/android/gms/googlehelp/GoogleHelp;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onPipClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onPipInCallingAppDisabled()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onPipInCallingAppHidden()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onPipShown()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onSuggestionsReceived([B)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    const/16 v1, 0xd

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onSuggestionsRequestFailed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method

.method public final onTogglingPipProcessed(Lcom/google/android/gms/googlehelp/internal/common/TogglingData;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzee;->zzaw()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {v0, p1}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/internal/zzee;->zzb(ILandroid/os/Parcel;)V

    return-void
.end method
