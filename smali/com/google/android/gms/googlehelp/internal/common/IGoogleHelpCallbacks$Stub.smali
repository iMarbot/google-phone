.class public abstract Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;
.super Lcom/google/android/gms/internal/zzef;

# interfaces
.implements Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub$zza;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzef;-><init>()V

    const-string v0, "com.google.android.gms.googlehelp.internal.common.IGoogleHelpCallbacks"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.googlehelp.internal.common.IGoogleHelpCallbacks"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub$zza;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub$zza;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzef;->zza(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onGoogleHelpProcessed(Lcom/google/android/gms/googlehelp/GoogleHelp;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/googlehelp/internal/common/TogglingData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p2, v0}, Lerk;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/internal/common/TogglingData;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onTogglingPipProcessed(Lcom/google/android/gms/googlehelp/internal/common/TogglingData;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onPipShown()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onPipClick()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onPipInCallingAppHidden()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onPipInCallingAppDisabled()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onAsyncPsdSaved()V

    goto :goto_1

    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onAsyncPsbdSaved()V

    goto :goto_1

    :pswitch_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onChatSupportRequestSuccess(I)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onChatSupportRequestFailed()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_a
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onC2cSupportRequestSuccess()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_b
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onC2cSupportRequestFailed()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_c
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onSuggestionsReceived([B)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_d
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onSuggestionsRequestFailed()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_e
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onEscalationOptionsReceived([B)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/internal/common/IGoogleHelpCallbacks$Stub;->onEscalationOptionsRequestFailed()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
