.class public final Lcom/google/android/gms/dynamic/zzr;
.super Lcom/google/android/gms/dynamic/zzl;


# instance fields
.field private a:Lip;


# direct methods
.method private constructor <init>(Lip;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/zzl;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    return-void
.end method

.method public static zza(Lip;)Lcom/google/android/gms/dynamic/zzr;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Lcom/google/android/gms/dynamic/zzr;

    invoke-direct {v0, p0}, Lcom/google/android/gms/dynamic/zzr;-><init>(Lip;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 2
    iget-object v0, v0, Lip;->h:Landroid/os/Bundle;

    .line 3
    return-object v0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 5
    iget v0, v0, Lip;->x:I

    .line 6
    return v0
.end method

.method public final getRetainInstance()Z
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 11
    iget-boolean v0, v0, Lip;->C:Z

    .line 12
    return v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 14
    iget-object v0, v0, Lip;->z:Ljava/lang/String;

    .line 15
    return-object v0
.end method

.method public final getTargetRequestCode()I
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 20
    iget v0, v0, Lip;->k:I

    .line 21
    return v0
.end method

.method public final getUserVisibleHint()Z
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 23
    iget-boolean v0, v0, Lip;->L:Z

    .line 24
    return v0
.end method

.method public final getView()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 26
    iget-object v0, v0, Lip;->I:Landroid/view/View;

    .line 27
    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzaf(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final isAdded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0}, Lip;->k()Z

    move-result v0

    return v0
.end method

.method public final isDetached()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 29
    iget-boolean v0, v0, Lip;->B:Z

    .line 30
    return v0
.end method

.method public final isHidden()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 32
    iget-boolean v0, v0, Lip;->A:Z

    .line 33
    return v0
.end method

.method public final isInLayout()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 35
    iget-boolean v0, v0, Lip;->o:Z

    .line 36
    return v0
.end method

.method public final isRemoving()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 38
    iget-boolean v0, v0, Lip;->m:Z

    .line 39
    return v0
.end method

.method public final isResumed()Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 41
    iget v0, v0, Lip;->c:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    .line 41
    :cond_0
    const/4 v0, 0x0

    .line 42
    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0}, Lip;->l()Z

    move-result v0

    return v0
.end method

.method public final setHasOptionsMenu(Z)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 47
    iget-boolean v1, v0, Lip;->E:Z

    if-eq v1, p1, :cond_0

    .line 48
    iput-boolean p1, v0, Lip;->E:Z

    .line 49
    invoke-virtual {v0}, Lip;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    iget-boolean v1, v0, Lip;->A:Z

    .line 51
    if-nez v1, :cond_0

    .line 52
    iget-object v0, v0, Lip;->t:Liz;

    invoke-virtual {v0}, Liz;->d()V

    .line 53
    :cond_0
    return-void
.end method

.method public final setMenuVisibility(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0, p1}, Lip;->b(Z)V

    return-void
.end method

.method public final setRetainInstance(Z)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 55
    iput-boolean p1, v0, Lip;->C:Z

    .line 56
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0, p1}, Lip;->c(Z)V

    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0, p1}, Lip;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0, p1, p2}, Lip;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final zzaa(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 2

    .prologue
    .line 43
    invoke-static {p1}, Lcom/google/android/gms/dynamic/zzn;->zzac(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 45
    return-void
.end method

.method public final zzab(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/dynamic/zzn;->zzac(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lip;->a(Landroid/view/View;)V

    return-void
.end method

.method public final zzaup()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0}, Lip;->h()Lit;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzaf(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final zzauq()Lcom/google/android/gms/dynamic/zzk;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 8
    iget-object v0, v0, Lip;->w:Lip;

    .line 9
    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzr;->zza(Lip;)Lcom/google/android/gms/dynamic/zzr;

    move-result-object v0

    return-object v0
.end method

.method public final zzaur()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    invoke-virtual {v0}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzaf(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final zzaus()Lcom/google/android/gms/dynamic/zzk;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->a:Lip;

    .line 17
    iget-object v0, v0, Lip;->i:Lip;

    .line 18
    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzr;->zza(Lip;)Lcom/google/android/gms/dynamic/zzr;

    move-result-object v0

    return-object v0
.end method
