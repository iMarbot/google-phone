.class public interface abstract Lcom/google/android/gms/internal/zzeh;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract getAccountChangeEvents(Ldwc;)Ldwd;
.end method

.method public abstract zza(Landroid/accounts/Account;)Landroid/os/Bundle;
.end method

.method public abstract zza(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract zza(Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract zza(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract zzo(Ljava/lang/String;)Landroid/os/Bundle;
.end method
