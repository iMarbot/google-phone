.class public interface abstract Lcom/google/android/gms/maps/internal/zze;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract zza(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/IMapViewDelegate;
.end method

.method public abstract zza(Lcom/google/android/gms/dynamic/IObjectWrapper;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)Lcom/google/android/gms/maps/internal/IStreetViewPanoramaViewDelegate;
.end method

.method public abstract zzaf(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;
.end method

.method public abstract zzag(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/internal/IStreetViewPanoramaFragmentDelegate;
.end method

.method public abstract zzbae()Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;
.end method

.method public abstract zzbaf()Lcom/google/android/gms/maps/model/internal/zza;
.end method

.method public abstract zzi(Lcom/google/android/gms/dynamic/IObjectWrapper;I)V
.end method
