.class public Lcom/google/android/gms/maps/SupportMapFragment;
.super Lip;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/maps/SupportMapFragment$a;
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/maps/SupportMapFragment$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lip;-><init>()V

    new-instance v0, Lcom/google/android/gms/maps/SupportMapFragment$a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/maps/SupportMapFragment$a;-><init>(Lip;)V

    iput-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x2

    .line 14
    iget-object v1, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 15
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lelo;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lelo;-><init>(Lell;Landroid/widget/FrameLayout;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)V

    invoke-virtual {v1, p3, v0}, Lell;->a(Landroid/os/Bundle;Lels;)V

    iget-object v0, v1, Lell;->a:Lelk;

    if-nez v0, :cond_0

    .line 17
    sget-object v0, Lecn;->a:Lecn;

    .line 18
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lecp;->b(Landroid/content/Context;)I

    move-result v3

    invoke-static {v1, v3}, Lejp;->c(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3}, Lejp;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Lecp;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/widget/Button;

    invoke-direct {v3, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    const v4, 0x1020019

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setId(I)V

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v4, Lelp;

    invoke-direct {v4, v1, v0}, Lelp;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    :cond_0
    invoke-virtual {v2, v10}, Landroid/view/View;->setClickable(Z)V

    return-object v2
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 1
    invoke-super {p0, p1}, Lip;->a(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 3
    iput-object p1, v0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment$a;->a()V

    .line 4
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 5
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0, v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lip;->a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 7
    iput-object p1, v0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment$a;->a()V

    .line 8
    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "MapOptions"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 9
    new-instance v3, Lelm;

    invoke-direct {v3, v0, p1, v2, p3}, Lelm;-><init>(Lell;Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    invoke-virtual {v0, p3, v3}, Lell;->a(Landroid/os/Bundle;Lels;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 11
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 12
    new-instance v1, Leln;

    invoke-direct {v1, v0, p1}, Leln;-><init>(Lell;Landroid/os/Bundle;)V

    invoke-virtual {v0, p1, v1}, Lell;->a(Landroid/os/Bundle;Lels;)V

    .line 13
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-class v0, Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Lip;->d(Landroid/os/Bundle;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    if-eqz p1, :cond_0

    const-class v0, Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 43
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0, p1}, Lelk;->b(Landroid/os/Bundle;)V

    .line 44
    :cond_1
    :goto_0
    return-void

    .line 43
    :cond_2
    iget-object v1, v0, Lell;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lell;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final l_()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 31
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0}, Lelk;->d()V

    .line 32
    :goto_0
    invoke-super {p0}, Lip;->l_()V

    return-void

    .line 31
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lell;->a(I)V

    goto :goto_0
.end method

.method public final n_()V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 34
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0}, Lelk;->e()V

    .line 35
    :goto_0
    invoke-super {p0}, Lip;->n_()V

    return-void

    .line 34
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lell;->a(I)V

    goto :goto_0
.end method

.method public final o_()V
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lip;->o_()V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 28
    const/4 v1, 0x0

    new-instance v2, Lelq;

    invoke-direct {v2, v0}, Lelq;-><init>(Lell;)V

    invoke-virtual {v0, v1, v2}, Lell;->a(Landroid/os/Bundle;Lels;)V

    .line 29
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 40
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0}, Lelk;->g()V

    .line 41
    :cond_0
    invoke-super {p0}, Lip;->onLowMemory()V

    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 21
    invoke-super {p0}, Lip;->r()V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 22
    const/4 v1, 0x0

    new-instance v2, Lelr;

    invoke-direct {v2, v0}, Lelr;-><init>(Lell;)V

    invoke-virtual {v0, v1, v2}, Lell;->a(Landroid/os/Bundle;Lels;)V

    .line 23
    return-void
.end method

.method public final s()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 25
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0}, Lelk;->c()V

    .line 26
    :goto_0
    invoke-super {p0}, Lip;->s()V

    return-void

    .line 25
    :cond_0
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lell;->a(I)V

    goto :goto_0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment;->a:Lcom/google/android/gms/maps/SupportMapFragment$a;

    .line 37
    iget-object v1, v0, Lell;->a:Lelk;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lell;->a:Lelk;

    invoke-virtual {v0}, Lelk;->f()V

    .line 38
    :goto_0
    invoke-super {p0}, Lip;->t()V

    return-void

    .line 37
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lell;->a(I)V

    goto :goto_0
.end method
