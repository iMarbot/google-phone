.class public final Lcom/google/android/gms/maps/SupportMapFragment$a;
.super Lell;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/maps/SupportMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public d:Landroid/app/Activity;

.field public final e:Ljava/util/List;

.field private f:Lip;

.field private g:Lelt;


# direct methods
.method constructor <init>(Lip;)V
    .locals 1

    invoke-direct {p0}, Lell;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->e:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->f:Lip;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 1
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->g:Lelt;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lell;->a:Lelk;

    .line 3
    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    invoke-static {v0}, Lexi;->a(Landroid/content/Context;)I

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    invoke-static {v0}, Lexn;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/internal/zze;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->d:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/dynamic/zzn;->zzaf(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/zze;->zzaf(Lcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;

    move-result-object v0

    if-nez v0, :cond_1

    .line 5
    :cond_0
    :goto_0
    return-void

    .line 3
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->g:Lelt;

    new-instance v2, Lelk;

    iget-object v3, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->f:Lip;

    invoke-direct {v2, v3, v0}, Lelk;-><init>(Lip;Lcom/google/android/gms/maps/internal/IMapFragmentDelegate;)V

    invoke-virtual {v1, v2}, Lelt;->a(Lelk;)V

    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexj;

    .line 4
    iget-object v1, p0, Lell;->a:Lelk;

    .line 5
    check-cast v1, Lelk;

    invoke-virtual {v1, v0}, Lelk;->a(Lexj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lecq; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lip$b;

    invoke-direct {v1, v0}, Lip$b;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lecq; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Lelt;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/SupportMapFragment$a;->g:Lelt;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/SupportMapFragment$a;->a()V

    return-void
.end method
