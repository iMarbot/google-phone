.class public final Lcom/google/android/gms/common/api/internal/zzdc;
.super Lcom/google/android/gms/internal/zzeek;

# interfaces
.implements Ledl;
.implements Ledm;


# static fields
.field private static a:Ledb;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/os/Handler;

.field private d:Ledb;

.field private e:Ljava/util/Set;

.field private f:Lejm;

.field private g:Lera;

.field private h:Legl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Leqx;->a:Ledb;

    sput-object v0, Lcom/google/android/gms/common/api/internal/zzdc;->a:Ledb;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lejm;)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/api/internal/zzdc;->a:Ledb;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/api/internal/zzdc;-><init>(Landroid/content/Context;Landroid/os/Handler;Lejm;Ledb;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lejm;Ledb;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/zzeek;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzdc;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/common/api/internal/zzdc;->c:Landroid/os/Handler;

    const-string v0, "ClientSettings must not be null"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejm;

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->f:Lejm;

    .line 2
    iget-object v0, p3, Lejm;->b:Ljava/util/Set;

    .line 3
    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->e:Ljava/util/Set;

    iput-object p4, p0, Lcom/google/android/gms/common/api/internal/zzdc;->d:Ledb;

    return-void
.end method

.method public static synthetic zza(Lcom/google/android/gms/common/api/internal/zzdc;Leri;)V
    .locals 5

    .prologue
    .line 9
    .line 11
    iget-object v0, p1, Leri;->a:Lecl;

    .line 12
    invoke-virtual {v0}, Lecl;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    iget-object v0, p1, Leri;->b:Leiw;

    .line 15
    iget-object v1, v0, Leiw;->a:Lecl;

    .line 16
    invoke-virtual {v1}, Lecl;->b()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "SignInCoordinator"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Sign-in succeeded with resolve account failure: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->h:Legl;

    invoke-interface {v0, v1}, Legl;->b(Lecl;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0}, Lera;->e()V

    .line 17
    return-void

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzdc;->h:Legl;

    invoke-virtual {v0}, Leiw;->a()Lcom/google/android/gms/common/internal/zzam;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzdc;->e:Ljava/util/Set;

    invoke-interface {v1, v0, v2}, Legl;->a(Lcom/google/android/gms/common/internal/zzam;Ljava/util/Set;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzdc;->h:Legl;

    invoke-interface {v1, v0}, Legl;->b(Lecl;)V

    goto :goto_0
.end method


# virtual methods
.method public final onConnected(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0, p0}, Lera;->a(Lcom/google/android/gms/internal/zzeel;)V

    return-void
.end method

.method public final onConnectionFailed(Lecl;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->h:Legl;

    invoke-interface {v0, p1}, Legl;->b(Lecl;)V

    return-void
.end method

.method public final onConnectionSuspended(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0}, Lera;->e()V

    return-void
.end method

.method public final zza(Legl;)V
    .locals 7

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0}, Lera;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->f:Lejm;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 5
    iput-object v1, v0, Lejm;->h:Ljava/lang/Integer;

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->d:Ledb;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzdc;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzdc;->c:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/api/internal/zzdc;->f:Lejm;

    iget-object v4, p0, Lcom/google/android/gms/common/api/internal/zzdc;->f:Lejm;

    .line 7
    iget-object v4, v4, Lejm;->g:Lerb;

    move-object v5, p0

    move-object v6, p0

    .line 8
    invoke-virtual/range {v0 .. v6}, Ledb;->a(Landroid/content/Context;Landroid/os/Looper;Lejm;Ljava/lang/Object;Ledl;Ledm;)Ledd;

    move-result-object v0

    check-cast v0, Lera;

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzdc;->h:Legl;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0}, Lera;->k()V

    return-void
.end method

.method public final zzals()Lera;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    return-object v0
.end method

.method public final zzamg()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->g:Lera;

    invoke-interface {v0}, Lera;->e()V

    :cond_0
    return-void
.end method

.method public final zzb(Leri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzdc;->c:Landroid/os/Handler;

    new-instance v1, Legk;

    invoke-direct {v1, p0, p1}, Legk;-><init>(Lcom/google/android/gms/common/api/internal/zzdc;Leri;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
