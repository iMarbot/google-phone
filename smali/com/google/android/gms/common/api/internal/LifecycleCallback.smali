.class public Lcom/google/android/gms/common/api/internal/LifecycleCallback;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lefx;


# direct methods
.method public constructor <init>(Lefx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a:Lefx;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lefx;
    .locals 1

    new-instance v0, Lefw;

    invoke-direct {v0, p0}, Lefw;-><init>(Landroid/app/Activity;)V

    invoke-static {v0}, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a(Lefw;)Lefx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lefw;)Lefx;
    .locals 2

    .prologue
    .line 1
    .line 2
    iget-object v0, p0, Lefw;->a:Ljava/lang/Object;

    instance-of v0, v0, Lit;

    .line 3
    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lefw;->a:Ljava/lang/Object;

    check-cast v0, Lit;

    .line 5
    invoke-static {v0}, Lego;->a(Lit;)Lego;

    move-result-object v0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    iget-object v0, p0, Lefw;->a:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    .line 7
    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p0, Lefw;->a:Ljava/lang/Object;

    check-cast v0, Landroid/app/Activity;

    .line 9
    invoke-static {v0}, Lefy;->a(Landroid/app/Activity;)Lefy;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t get fragment for unexpected activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getChimeraLifecycleFragmentImpl(Lefw;)Lefx;
    .locals 2
    .annotation build Landroid/support/annotation/Keep;
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Method not available in SDK."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/LifecycleCallback;->a:Lefx;

    invoke-interface {v0}, Lefx;->a()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method
