.class public Lcom/google/android/libraries/hangouts/video/internal/Renderer;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field public static final RENDERER_INPUT_DIMENSIONS_KEY:Ljava/lang/String; = "sub_indims"

.field public static final RENDERER_IS_SCREENCAST_KEY:Ljava/lang/String; = "sub_screencast"

.field public static final RENDERER_OUTPUT_TEXTURE_KEY:Ljava/lang/String; = "sub_outtex"

.field public static final RENDERER_TYPE_REMOTE:I = 0x1

.field public static final RENDERER_TYPE_SELF:I = 0x0

.field public static final SELF_RENDERER_CAMERA_ROTATION_KEY:Ljava/lang/String; = "c_rotation"


# instance fields
.field public mNativeContext:J
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;->nativeInit(I)V

    .line 3
    return-void
.end method

.method private final native nativeInit(I)V
.end method

.method private final native nativeInitializeGLContext()Z
.end method

.method private final native nativeRelease()V
.end method


# virtual methods
.method public final initializeGLContext()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;->nativeInitializeGLContext()Z

    .line 8
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;->onInitializeGLContext()V

    .line 9
    return-void
.end method

.method protected final native nativeGetIntParam(Ljava/lang/String;)I
.end method

.method protected final native nativeRenderFrame(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method protected final native nativeSetIntParam(Ljava/lang/String;I)V
.end method

.method protected notifyFrameReceived()V
    .locals 0
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 12
    return-void
.end method

.method public final native notifyFrameRendered()V
.end method

.method protected onInitializeGLContext()V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method protected onRelease()V
    .locals 0

    .prologue
    .line 10
    return-void
.end method

.method public final release()V
    .locals 0

    .prologue
    .line 4
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;->onRelease()V

    .line 5
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;->nativeRelease()V

    .line 6
    return-void
.end method
