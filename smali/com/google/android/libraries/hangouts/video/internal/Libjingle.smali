.class public Lcom/google/android/libraries/hangouts/video/internal/Libjingle;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field public static final AUDIO_PLAYBACK_SAMPLING_RATE:Ljava/lang/String; = "AUDIO_PLAYBACK_SAMPLING_RATE"

.field public static final AUDIO_RECORDING_DEVICE:Ljava/lang/String; = "AUDIO_RECORDING_DEVICE"

.field public static final AUDIO_RECORDING_SAMPLING_RATE:Ljava/lang/String; = "AUDIO_RECORDING_SAMPLING_RATE"

.field public static final AVAILABLE:I = 0x1

.field public static final HAS_CAMERA_V1:I = 0x8

.field public static final HAS_VIDEO_V1:I = 0x4

.field public static final HAS_VOICE_V1:I = 0x2

.field public static final LIBJINGLE_LS_ERROR:I = 0x4

.field public static final LIBJINGLE_LS_INFO:I = 0x2

.field public static final LIBJINGLE_LS_VERBOSE:I = 0x1

.field public static final LIBJINGLE_LS_WARNING:I = 0x3

.field public static final STR1_KEY:Ljava/lang/String; = "str1"

.field public static final STR2_KEY:Ljava/lang/String; = "str2"

.field public static final STR3_KEY:Ljava/lang/String; = "str3"

.field public static final STR4_KEY:Ljava/lang/String; = "str4"

.field public static final STR5_KEY:Ljava/lang/String; = "str5"

.field public static final UNAVAILABLE:I = 0x0

.field public static final USE_DEFAULT_NETWORKS_ONLY:Ljava/lang/String; = "USE_DEFAULT_NETWORKS_ONLY"


# instance fields
.field public final context:Landroid/content/Context;

.field public gservices:Lfpo;

.field public final handler:Landroid/os/Handler;

.field public initialized:Z

.field public mNativeContext:J
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public pendingAudioPlayoutMute:Z

.field public pendingInitialAudioMute:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 191
    :try_start_0
    const-string v0, "videochat_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :goto_0
    invoke-static {}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeInit()V

    .line 197
    invoke-static {}, Lfvh;->getVCLibLogLevel()I

    move-result v0

    .line 198
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 199
    const/4 v0, 0x5

    .line 200
    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetLoggingLevel(I)V

    .line 201
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    const-string v1, "Unable to load videochat_jni.so with error"

    invoke-static {v1, v0}, Lfvh;->logw(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 195
    const-string v0, "videochat_jni_symbolized"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lfpo;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->context:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->handler:Landroid/os/Handler;

    .line 5
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->gservices:Lfpo;

    .line 6
    return-void
.end method

.method private static dispatchNativeEvent(Ljava/lang/Object;IIILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 175
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;

    .line 176
    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->mNativeContext:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, v0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3, p9}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 178
    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 179
    const-string v3, "str1"

    check-cast p4, Ljava/lang/String;

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v3, "str2"

    check-cast p5, Ljava/lang/String;

    invoke-virtual {v2, v3, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v3, "str3"

    check-cast p6, Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v3, "str4"

    check-cast p7, Ljava/lang/String;

    invoke-virtual {v2, v3, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v3, "str5"

    check-cast p8, Ljava/lang/String;

    invoke-virtual {v2, v3, p8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 185
    iget-object v0, v0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 190
    :goto_0
    return-void

    .line 187
    :cond_0
    const-string v0, "Dropping libjingle native message (id=%d) because the native client is being released."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 188
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 189
    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getCorrectedLibjingleMediaLogLevel(I)I
    .locals 0

    .prologue
    .line 14
    packed-switch p1, :pswitch_data_0

    .line 16
    :goto_0
    return p1

    .line 15
    :pswitch_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 14
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getLibjingleLogLevel(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 7
    invoke-static {p1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8
    const/4 v0, 0x1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 9
    :cond_1
    invoke-static {p1, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 11
    invoke-static {p1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 12
    goto :goto_0

    :cond_2
    move v0, v2

    .line 13
    goto :goto_0
.end method

.method private initGservicesOverrides([[Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 49
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, p1, v0

    .line 50
    aget-object v4, v3, v1

    .line 51
    aget-object v3, v3, v6

    .line 52
    const-string v5, "USE_DEFAULT_NETWORKS_ONLY"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 53
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->gservices:Lfpo;

    invoke-virtual {v3, v4, v6}, Lfpo;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 54
    if-ne v3, v6, :cond_0

    invoke-static {}, Lfmk;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 55
    :cond_1
    const-string v3, "USE_DEFAULT_NETWORKS_ONLY"

    const-string v4, "true"

    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_3
    iget-object v5, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->gservices:Lfpo;

    invoke-virtual {v5, v4}, Lfpo;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 58
    if-eqz v4, :cond_2

    .line 59
    invoke-direct {p0, v3, v4}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 61
    :cond_4
    return-void
.end method

.method public static load()V
    .locals 0

    .prologue
    .line 1
    return-void
.end method

.method public static native nativeAbort(Ljava/lang/String;)V
.end method

.method private static final native nativeAddLogComment(Ljava/lang/String;)V
.end method

.method private final native nativeBlockMedia(Ljava/lang/String;)V
.end method

.method private final native nativeBroadcastClientData([B)V
.end method

.method private final native nativeCallHangout(Ljava/lang/String;Z[BLjava/lang/String;)V
.end method

.method private final native nativeEndCall()V
.end method

.method private final native nativeEndCallAndSignOut()V
.end method

.method private static native nativeInit()V
.end method

.method private final native nativeInvitePstn(Ljava/lang/String;Ljava/lang/String;ZZZ[B)V
.end method

.method private final native nativeInviteUsers([Ljava/lang/String;Z)V
.end method

.method private final native nativePlayoutMute(Z)V
.end method

.method private final native nativePublishAudioMuteState(Z)V
.end method

.method private final native nativePublishVideoMuteState(Z)V
.end method

.method private final native nativeReinitializeAudio()V
.end method

.method private final native nativeRelease()V
.end method

.method private final native nativeRemoteMute(Ljava/lang/String;)V
.end method

.method private final native nativeRequestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V
.end method

.method private final native nativeSendDtmf(CILjava/lang/String;)V
.end method

.method private final native nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private final native nativeSetHangoutCookie([B)V
.end method

.method private final native nativeSetInitialAudioMute(Z)V
.end method

.method private static final native nativeSetLoggingLevel(I)V
.end method

.method private final native nativeSetRtcClient([B)V
.end method

.method private final native nativeSetVideoCallOptions([B)V
.end method

.method private final native nativeSetup(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V
.end method

.method private final native nativeSignIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
.end method


# virtual methods
.method public addLogComment(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 169
    invoke-static {p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeAddLogComment(Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public blockMedia(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeBlockMedia(Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 124
    :cond_0
    const-string v0, "blockMedia: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public broadcastClientData([B)V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeBroadcastClientData([B)V

    .line 174
    return-void
.end method

.method public endCallAndSignOut()V
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeEndCallAndSignOut()V

    .line 152
    :goto_0
    return-void

    .line 151
    :cond_0
    const-string v0, "endCallAndSignOut: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final native handleApiaryResponse(J[B)V
.end method

.method public final native handlePushNotification([B)V
.end method

.method public init(Ljava/lang/String;[[Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 17
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_1

    .line 18
    const-string v0, "init: already initialized"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 20
    :cond_1
    iput-boolean v10, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    .line 21
    invoke-direct {p0, p2}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initGservicesOverrides([[Ljava/lang/String;)V

    .line 22
    const-string v0, "init: call nativeSetup"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 23
    const-string v0, "vclib:videoLogging"

    .line 24
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->getLibjingleLogLevel(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->getCorrectedLibjingleMediaLogLevel(I)I

    move-result v6

    .line 25
    const-string v0, "vclib:audioLogging"

    .line 26
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->getLibjingleLogLevel(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->getCorrectedLibjingleMediaLogLevel(I)I

    move-result v7

    .line 27
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 28
    if-nez v4, :cond_2

    .line 29
    const-string v4, "en"

    .line 30
    :cond_2
    const-string v1, "Product: "

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 32
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    move v0, v10

    .line 36
    :goto_2
    if-nez v0, :cond_4

    .line 37
    const-string v0, "init: log directory (%s) creation failed. Native logs may not be saved."

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p3, v1, v9

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->context:Landroid/content/Context;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 39
    invoke-static {}, Lfmk;->c()Z

    move-result v8

    move-object v0, p0

    move-object v3, p1

    move-object v5, p3

    .line 40
    invoke-direct/range {v0 .. v8}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetup(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 41
    const-string v0, "init: nativeSetup returned"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 42
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingAudioPlayoutMute:Z

    if-eqz v0, :cond_5

    .line 43
    invoke-virtual {p0, v10}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setAudioPlayoutMute(Z)V

    .line 44
    iput-boolean v9, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingAudioPlayoutMute:Z

    .line 45
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingInitialAudioMute:Z

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0, v10}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->setInitialAudioMute(Z)V

    .line 47
    iput-boolean v9, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingInitialAudioMute:Z

    goto/16 :goto_0

    .line 30
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    move v0, v9

    .line 33
    goto :goto_2

    :catch_0
    move-exception v0

    move v0, v9

    goto :goto_2
.end method

.method public initiateHangoutCall(Lfvs;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    .line 81
    const-string v1, "Expected condition to be true"

    invoke-static {v1, v0}, Lfmw;->a(Ljava/lang/String;Z)V

    .line 84
    const/4 v0, 0x0

    .line 86
    iget-object v1, p1, Lfvs;->l:[B

    .line 89
    const/4 v2, 0x0

    .line 90
    invoke-direct {p0, p2, v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeCallHangout(Ljava/lang/String;Z[BLjava/lang/String;)V

    .line 91
    return-void
.end method

.method public invitePstn(Ljava/lang/String;Ljava/lang/String;ZZZ[B)V
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 162
    invoke-direct/range {p0 .. p6}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeInvitePstn(Ljava/lang/String;Ljava/lang/String;ZZZ[B)V

    .line 164
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v0, "invitePstn: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public inviteUsers([Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 158
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeInviteUsers([Ljava/lang/String;Z)V

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "inviteUsers: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    return v0
.end method

.method public publishAudioMuteState(Z)V
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativePublishAudioMuteState(Z)V

    .line 99
    :goto_0
    return-void

    .line 98
    :cond_0
    const-string v0, "publishAudioMuteState: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public publishVideoMuteState(Z)V
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativePublishVideoMuteState(Z)V

    .line 121
    :goto_0
    return-void

    .line 120
    :cond_0
    const-string v0, "publishVideoMuteState: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reinitializeAudio()V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeReinitializeAudio()V

    .line 107
    :goto_0
    return-void

    .line 106
    :cond_0
    const-string v0, "reinitializeAudio: not initialized"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-nez v0, :cond_0

    .line 63
    const-string v0, "release: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    .line 66
    const-string v0, "Release: call nativeRelease"

    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeRelease()V

    goto :goto_0
.end method

.method public remoteMute(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeRemoteMute(Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "remoteMute: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeRequestVideoViews([Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;)V

    .line 95
    :goto_0
    return-void

    .line 94
    :cond_0
    const-string v0, "requestVideoViews: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendDtmf(CILjava/lang/String;)V
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSendDtmf(CILjava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 167
    :cond_0
    const-string v0, "sendDtmf: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioPlayoutMute(Z)V
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativePlayoutMute(Z)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    const-string v0, "setAudioPlayoutMute: not initialized, will apply on initialization"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 111
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingAudioPlayoutMute:Z

    goto :goto_0
.end method

.method public setHangoutCookie(Lgob;)V
    .locals 1

    .prologue
    .line 171
    invoke-static {p1}, Lhfz;->toByteArray(Lhfz;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetHangoutCookie([B)V

    .line 172
    return-void
.end method

.method public setInitialAudioMute(Z)V
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetInitialAudioMute(Z)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    const-string v0, "setInitialAudioMute: not initialized, will apply on initialization"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 116
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->pendingInitialAudioMute:Z

    goto :goto_0
.end method

.method public setPlayoutSampleRate(I)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "AUDIO_PLAYBACK_SAMPLING_RATE"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public setRecordingDevice(I)V
    .locals 2

    .prologue
    .line 78
    const-string v0, "AUDIO_RECORDING_DEVICE"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public setRecordingSampleRate(I)V
    .locals 2

    .prologue
    .line 74
    const-string v0, "AUDIO_RECORDING_SAMPLING_RATE"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetGServicesOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public setRtcClient([B)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetRtcClient([B)V

    .line 73
    return-void
.end method

.method public setVideoCallOptions([B)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSetVideoCallOptions([B)V

    .line 71
    return-void
.end method

.method public signIn(Lfvs;JJ)V
    .locals 12

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 129
    iget-object v2, p1, Lfvs;->a:Ljava/lang/String;

    .line 132
    iget-object v3, p1, Lfvs;->e:Ljava/lang/String;

    .line 135
    iget-object v4, p1, Lfvs;->g:Ljava/lang/String;

    .line 138
    iget-object v5, p1, Lfvs;->j:Ljava/lang/String;

    .line 141
    iget-object v6, p1, Lfvs;->i:Ljava/lang/String;

    .line 144
    iget-object v7, p1, Lfvs;->b:Ljava/lang/String;

    move-object v1, p0

    move-wide v8, p2

    move-wide/from16 v10, p4

    .line 146
    invoke-direct/range {v1 .. v11}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeSignIn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    const-string v0, "signIn: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public terminateCall()V
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->initialized:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Libjingle;->nativeEndCall()V

    .line 156
    :goto_0
    return-void

    .line 155
    :cond_0
    const-string v0, "terminateCall: not initialized"

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0
.end method
