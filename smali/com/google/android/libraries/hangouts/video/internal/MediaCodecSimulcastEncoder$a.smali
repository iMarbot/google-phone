.class public final Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;
.super Landroid/util/LruCache;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    .line 2
    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    .line 3
    return-void
.end method

.method public static final synthetic lambda$entryRemoved$0$MediaCodecSimulcastEncoder$EncoderInputSurfaceLruCache(Lfwo;Lfoy;)V
    .locals 4

    .prologue
    .line 8
    const-string v0, "Releasing surface of size: %s x %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lfwo;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lfwo;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    invoke-virtual {p1}, Lfoy;->release()V

    .line 10
    return-void
.end method


# virtual methods
.method public final entryRemoved(ZLfwo;Lfoy;Lfoy;)V
    .locals 2

    .prologue
    .line 4
    if-eqz p1, :cond_0

    .line 5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->access$600(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Lfpc;

    move-result-object v0

    new-instance v1, Lfrb;

    invoke-direct {v1, p2, p3}, Lfrb;-><init>(Lfwo;Lfoy;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 6
    :cond_0
    return-void
.end method

.method public final bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 7
    check-cast p2, Lfwo;

    check-cast p3, Lfoy;

    check-cast p4, Lfoy;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->entryRemoved(ZLfwo;Lfoy;Lfoy;)V

    return-void
.end method
