.class public final Lcom/google/android/libraries/hangouts/video/internal/Stats$a;
.super Lcom/google/android/libraries/hangouts/video/internal/Stats;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/hangouts/video/internal/Stats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public batteryLevel:I

.field public currentCpuSpeedMHz:I

.field public isOnBattery:Z

.field public onlineCpuCores:I

.field public utilizationPerCpu:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;-><init>()V

    .line 2
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->onlineCpuCores:I

    .line 3
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->currentCpuSpeedMHz:I

    .line 4
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->utilizationPerCpu:I

    .line 5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->isOnBattery:Z

    .line 6
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->batteryLevel:I

    return-void
.end method

.method public static printLegend(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 23
    const-string v0, "  GlobalStats -- pcpu, tcpu, online cores, util per cpu, cpu freq, on battery, battery level"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final addTo(Lgiv;)V
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->onlineCpuCores:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgiv;->e:Ljava/lang/Integer;

    .line 18
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->currentCpuSpeedMHz:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgiv;->f:Ljava/lang/Integer;

    .line 19
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->utilizationPerCpu:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgiv;->g:Ljava/lang/Integer;

    .line 20
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->isOnBattery:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lgiv;->h:Ljava/lang/Boolean;

    .line 21
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->batteryLevel:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lgiv;->i:Ljava/lang/Integer;

    .line 22
    return-void
.end method

.method public final print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V
    .locals 7

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->onlineCpuCores:I

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->utilizationPerCpu:I

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->currentCpuSpeedMHz:I

    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->isOnBattery:Z

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->batteryLevel:I

    const/16 v5, 0x4c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, " -- GlobalStats -- "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public final setBatteryLevel(I)V
    .locals 0

    .prologue
    .line 15
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->batteryLevel:I

    .line 16
    return-void
.end method

.method public final setCurrentCpuSpeedMHz(I)V
    .locals 0

    .prologue
    .line 9
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->currentCpuSpeedMHz:I

    .line 10
    return-void
.end method

.method public final setIsOnBattery(Z)V
    .locals 0

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->isOnBattery:Z

    .line 14
    return-void
.end method

.method public final setOnlineCpuCores(I)V
    .locals 0

    .prologue
    .line 7
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->onlineCpuCores:I

    .line 8
    return-void
.end method

.method public final setUtilizationPerCpu(I)V
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$a;->utilizationPerCpu:I

    .line 12
    return-void
.end method
