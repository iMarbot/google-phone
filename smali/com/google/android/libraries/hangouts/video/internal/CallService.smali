.class public Lcom/google/android/libraries/hangouts/video/internal/CallService;
.super Landroid/app/Service;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/CallService$a;
    }
.end annotation


# static fields
.field public static final NOTIFICATION_ID:I = 0x1


# instance fields
.field public final client:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

.field public currentCall:Lfnp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;-><init>(Lcom/google/android/libraries/hangouts/video/internal/CallService;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService;->client:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    .line 3
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/libraries/hangouts/video/internal/CallService;Lfnp;)Lfnp;
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService;->currentCall:Lfnp;

    return-object p1
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService;->currentCall:Lfnp;

    if-nez v0, :cond_0

    .line 6
    const-string v0, "No call connected"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 8
    :goto_0
    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService;->currentCall:Lfnp;

    invoke-virtual {v0, p2}, Lfnp;->dump(Ljava/io/PrintWriter;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/CallService;->onBind(Landroid/content/Intent;)Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Lcom/google/android/libraries/hangouts/video/internal/CallService$a;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService;->client:Lcom/google/android/libraries/hangouts/video/internal/CallService$a;

    return-object v0
.end method
