.class public final Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfoz;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;,
        Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;,
        Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;
    }
.end annotation


# static fields
.field public static final CHECK_NATIVE_CONFIG_PERIOD_MS:I = 0xc8

.field public static final ENCODER_THREAD_NAME:Ljava/lang/String; = "EncoderHandlerThread"

.field public static final FAILED_RESET_RETRY_PERIOD_MS:I = 0xc8

.field public static final FORCE_KEYFRAME_FOR_SCREEN_SHARE_MS:I = 0x1388

.field public static final GL_CLEANUP_WAIT_MS:I = 0x1388

.field public static final MAX_CONSECUTIVE_HARDWARE_FAILURE_COUNT:I = 0x3

.field public static final MAX_ENCODER_COUNT:I = 0x4

.field public static final MAX_ENCODER_SURFACES_TO_CACHE:I = 0x3

.field public static final RESET_RETRY_PERIOD_MS:I = 0xc8


# instance fields
.field public final availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

.field public final callDirector:Lfnp;

.field public final checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

.field public consecutiveHardwareFailureCount:I

.field public encoderInstances:Ljava/util/List;

.field public final encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

.field public final encoderThread:Landroid/os/HandlerThread;

.field public final encoderThreadHandler:Landroid/os/Handler;

.field public encodersRunning:Z

.field public final glManager:Lfpc;

.field public initialized:Z

.field public inputIsScreencast:Z

.field public inputTextureHeight:I

.field public inputTextureWidth:I

.field public final lock:Ljava/lang/Object;

.field public final manageEncoderSurfacesDirectly:Z

.field public final maxOutstandingEncoderFrames:I

.field public nextKeyFrameTimeMs:J

.field public final releaseSharedEncoderSurfacesRunnable:Ljava/lang/Runnable;

.field public final resetEncodersRunnable:Ljava/lang/Runnable;

.field public final shouldForceKeyframes:Z

.field public final usedEncoderSurfaces:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lfnp;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;I)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    .line 21
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->usedEncoderSurfaces:Ljava/util/Map;

    .line 22
    new-instance v2, Lfqv;

    invoke-direct {v2, p0}, Lfqv;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncodersRunnable:Ljava/lang/Runnable;

    .line 23
    new-instance v2, Lfqw;

    invoke-direct {v2, p0}, Lfqw;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->releaseSharedEncoderSurfacesRunnable:Ljava/lang/Runnable;

    .line 24
    new-instance v2, Lfqz;

    invoke-direct {v2, p0}, Lfqz;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->callDirector:Lfnp;

    .line 26
    invoke-virtual {p1}, Lfnp;->getEncoderManager()Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    .line 27
    invoke-virtual {p1}, Lfnp;->getGlManager()Lfpc;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    .line 28
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->maxOutstandingEncoderFrames:I

    .line 30
    invoke-virtual {p1}, Lfnp;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "babel_hangout_force_generate_keyframes"

    .line 31
    invoke-static {v2, v3, v0}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->shouldForceKeyframes:Z

    .line 32
    const-string v2, "Will force generate keyframes in screencast mode: %b"

    new-array v3, v0, [Ljava/lang/Object;

    iget-boolean v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->shouldForceKeyframes:Z

    .line 33
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    .line 34
    invoke-static {v2, v3}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    .line 36
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    .line 37
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "EncoderHandlerThread"

    const/4 v4, -0x4

    invoke-direct {v2, v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThread:Landroid/os/HandlerThread;

    .line 38
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 39
    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    .line 40
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->manageEncoderSurfacesDirectly:Z

    .line 41
    return-void

    :cond_0
    move v0, v1

    .line 40
    goto :goto_0
.end method

.method public static synthetic access$000(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic access$100(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic access$200(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encodersRunning:Z

    return v0
.end method

.method public static synthetic access$300(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncoders()V

    return-void
.end method

.method public static synthetic access$400(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Z
    .locals 1

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkForHardwareFailure()Z

    move-result v0

    return v0
.end method

.method public static synthetic access$500(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Z
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeConfigForEncoderReset()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)Lfpc;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    return-object v0
.end method

.method private final checkForHardwareFailure()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqp;

    .line 12
    invoke-virtual {v0}, Lfqp;->hasFailed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 13
    invoke-virtual {v0}, Lfqp;->getEncodedFrameCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 14
    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->consecutiveHardwareFailureCount:I

    .line 15
    :cond_1
    invoke-virtual {v0}, Lfqp;->getNativeEncoderId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->handleHardwareFailure(J)V

    .line 16
    const/4 v0, 0x1

    .line 18
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private final checkNativeConfigForEncoderReset()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 111
    invoke-static {}, Lfmw;->d()V

    .line 112
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->fetchNativeEncoderConfig()Ljava/util/List;

    move-result-object v5

    .line 113
    if-nez v5, :cond_1

    .line 114
    const-string v0, "Native encoders have been reset."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 115
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initialized:Z

    move v3, v4

    .line 138
    :cond_0
    :goto_0
    return v3

    .line 117
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 118
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "New number of simulcast streams forcing a reset: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move v3, v4

    .line 119
    goto :goto_0

    :cond_2
    move v2, v3

    .line 120
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqp;

    .line 122
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;

    .line 123
    iget-wide v6, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->nativeEncoderId:J

    invoke-virtual {v0}, Lfqp;->getNativeEncoderId()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_3

    .line 124
    const-string v0, "Encoder setup has changed. Resetting."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move v3, v4

    .line 125
    goto :goto_0

    .line 126
    :cond_3
    iget v6, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->codecType:I

    invoke-virtual {v0}, Lfqp;->getCodecType()I

    move-result v7

    if-eq v6, v7, :cond_4

    .line 127
    const-string v0, "Encoder codec has changed. Resetting."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move v3, v4

    .line 128
    goto :goto_0

    .line 129
    :cond_4
    iget v6, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    invoke-virtual {v0}, Lfqp;->getOriginalWidth()I

    move-result v7

    if-ne v6, v7, :cond_5

    iget v6, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    .line 130
    invoke-virtual {v0}, Lfqp;->getOriginalHeight()I

    move-result v7

    if-eq v6, v7, :cond_6

    .line 131
    :cond_5
    const-string v0, "Encoder setup(resolution) has changed. Resetting."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move v3, v4

    .line 132
    goto :goto_0

    .line 133
    :cond_6
    iget-boolean v6, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->requiresKeyFrame:Z

    if-nez v6, :cond_7

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->shouldForceKeyFrame()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 134
    :cond_7
    invoke-virtual {v0}, Lfqp;->requestKeyFrame()V

    .line 135
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/16 v8, 0x1388

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->nextKeyFrameTimeMs:J

    .line 136
    :cond_8
    iget v1, v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->targetBitRate:I

    invoke-virtual {v0, v1}, Lfqp;->setBitRate(I)V

    .line 137
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method private final fetchNativeEncoderConfig()Ljava/util/List;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    invoke-static {}, Lfmw;->d()V

    .line 78
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 79
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureWidth:I

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureHeight:I

    if-lt v0, v4, :cond_1

    move v7, v1

    .line 80
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    new-instance v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;

    invoke-direct {v8}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;-><init>()V

    .line 82
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->getNativeSimulcastEncoderIds(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v0, v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v5

    .line 110
    :goto_1
    return-object v0

    :cond_1
    move v7, v2

    .line 79
    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 84
    :cond_2
    iget-object v0, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v0, v0

    const/4 v3, 0x4

    if-le v0, v3, :cond_3

    .line 85
    iget-object v0, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v0, v0

    const/16 v1, 0x39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Got a request for too many simulcast streams: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move-object v0, v5

    .line 86
    goto :goto_1

    .line 87
    :cond_3
    iget-object v0, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v0, v0

    if-ne v0, v1, :cond_4

    move v0, v1

    .line 88
    :goto_2
    new-instance v6, Ljava/util/ArrayList;

    iget-object v3, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v3, v3

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v2

    .line 89
    :goto_3
    iget-object v4, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    array-length v4, v4

    if-ge v3, v4, :cond_b

    .line 90
    new-instance v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;

    invoke-direct {v9}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;-><init>()V

    .line 91
    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    iget-object v10, v8, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$NativeSimulcastEncoderOutputParams;->nativeEncoderIds:[J

    aget-wide v10, v10, v3

    invoke-virtual {v4, v10, v11, v9}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->getEncoderConfig(JLjava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 92
    const-string v0, "Native encoder reset in the middle of a fetch operation."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    move-object v0, v5

    .line 93
    goto :goto_1

    :cond_4
    move v0, v2

    .line 87
    goto :goto_2

    .line 94
    :cond_5
    if-eqz v0, :cond_7

    iget-boolean v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputIsScreencast:Z

    if-nez v4, :cond_7

    .line 95
    new-instance v4, Lfwo;

    iget v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    iget v11, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    invoke-direct {v4, v10, v11}, Lfwo;-><init>(II)V

    .line 96
    iget v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->targetBitRate:I

    const/16 v11, 0x12c

    if-gt v10, v11, :cond_9

    .line 97
    const v10, 0x12c00

    invoke-static {v4, v10}, Lfwo;->a(Lfwo;I)Lfwo;

    move-result-object v4

    .line 100
    :cond_6
    :goto_4
    iget v10, v4, Lfwo;->a:I

    iput v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    .line 101
    iget v4, v4, Lfwo;->b:I

    iput v4, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    .line 102
    :cond_7
    iget v4, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    iget v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    if-lt v4, v10, :cond_a

    move v4, v1

    .line 103
    :goto_5
    if-eq v7, v4, :cond_8

    .line 104
    iget v4, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    .line 105
    iget v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    iput v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    .line 106
    iput v4, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    .line 107
    :cond_8
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 98
    :cond_9
    iget v10, v9, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->targetBitRate:I

    const/16 v11, 0x5dc

    if-gt v10, v11, :cond_6

    .line 99
    const v10, 0x4b000

    invoke-static {v4, v10}, Lfwo;->a(Lfwo;I)Lfwo;

    move-result-object v4

    goto :goto_4

    :cond_a
    move v4, v2

    .line 102
    goto :goto_5

    .line 109
    :cond_b
    new-instance v0, Lfra;

    invoke-direct {v0, p0}, Lfra;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;)V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v0, v6

    .line 110
    goto/16 :goto_1
.end method

.method private final getOrCreateSharedInputSurfaceForSize(Lfwo;)Lfoy;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 201
    if-nez v0, :cond_0

    .line 202
    const-string v0, "Creating persistent input surface for size: %s x %s."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p1, Lfwo;->a:I

    .line 203
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p1, Lfwo;->b:I

    .line 204
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 205
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    new-instance v0, Lfoy;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    .line 207
    invoke-virtual {v1}, Lfpc;->getEglContextWrapper()Lfus;

    move-result-object v1

    invoke-static {}, Landroid/media/MediaCodec;->createPersistentInputSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lfoy;-><init>(Lfus;Landroid/view/Surface;)V

    .line 208
    :cond_0
    return-object v0
.end method

.method private final handleHardwareFailure(J)V
    .locals 5

    .prologue
    .line 1
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->consecutiveHardwareFailureCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->consecutiveHardwareFailureCount:I

    .line 2
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->consecutiveHardwareFailureCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 3
    const-string v0, "An encoder instance has reported hardware failure. Resetting."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->stopEncoders()V

    .line 5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncodersRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 10
    :goto_0
    return-void

    .line 6
    :cond_0
    const-string v0, "An encoder instance has reported hardware failure too many times. Falling back to software encode."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initialized:Z

    .line 8
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->stopEncoders()V

    .line 9
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderManager:Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/hangouts/video/internal/EncoderManager;->notifyHardwareFailed(J)Z

    goto :goto_0
.end method

.method public static final synthetic lambda$releaseEncoderInstances$3$MediaCodecSimulcastEncoder(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 244
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 245
    invoke-virtual {v0}, Lfoy;->release()V

    goto :goto_0

    .line 247
    :cond_0
    return-void
.end method

.method private final releaseEncoderInstances(Ljava/util/List;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 218
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 221
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqp;

    .line 222
    if-eqz v0, :cond_2

    .line 223
    invoke-virtual {v0}, Lfqp;->release()Lfoy;

    move-result-object v4

    .line 224
    if-eqz v4, :cond_2

    .line 225
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->usedEncoderSurfaces:Ljava/util/Map;

    .line 226
    invoke-virtual {v0}, Lfqp;->getNativeEncoderId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfoy;

    .line 227
    new-instance v5, Lfwo;

    invoke-virtual {v0}, Lfqp;->getEncoderWidth()I

    move-result v6

    invoke-virtual {v0}, Lfqp;->getEncoderHeight()I

    move-result v0

    invoke-direct {v5, v6, v0}, Lfwo;-><init>(II)V

    .line 228
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 229
    const-string v0, "Moving surface of size: %s x %s to be available for reuse."

    new-array v4, v10, [Ljava/lang/Object;

    iget v6, v5, Lfwo;->a:I

    .line 230
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v8

    iget v6, v5, Lfwo;->b:I

    .line 231
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v9

    .line 232
    invoke-static {v0, v4}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 234
    :cond_3
    const-string v0, "Releasing surface of size: %s x %s."

    new-array v1, v10, [Ljava/lang/Object;

    iget v6, v5, Lfwo;->a:I

    .line 235
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v8

    iget v5, v5, Lfwo;->b:I

    .line 236
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v9

    .line 237
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    new-instance v1, Lfqy;

    invoke-direct {v1, v2}, Lfqy;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method private final resetEncoders()V
    .locals 20

    .prologue
    .line 152
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encodersRunning:Z

    .line 153
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->stopEncoders()V

    .line 154
    invoke-direct/range {p0 .. p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->fetchNativeEncoderConfig()Ljava/util/List;

    move-result-object v18

    .line 155
    if-nez v18, :cond_0

    .line 156
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initialized:Z

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncodersRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    :goto_0
    return-void

    .line 159
    :cond_0
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 160
    const/4 v3, -0x1

    .line 161
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_6

    .line 162
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;

    .line 163
    iget v11, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    .line 164
    iget v12, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    .line 165
    const/4 v4, -0x1

    if-ne v3, v4, :cond_7

    .line 166
    iget v3, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->codecType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 167
    const/16 v17, 0x10

    .line 169
    :goto_2
    if-lez v17, :cond_1

    .line 170
    add-int/lit8 v3, v17, -0x1

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v11, v3

    .line 171
    add-int/lit8 v3, v17, -0x1

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v12, v3

    .line 172
    shl-int/lit8 v17, v17, 0x1

    .line 173
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v3, v4, :cond_3

    .line 174
    new-instance v3, Lfqq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->callDirector:Lfnp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    .line 175
    invoke-virtual {v5}, Lfpc;->getEglContextWrapper()Lfus;

    move-result-object v5

    iget-wide v6, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->nativeEncoderId:J

    iget v8, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->codecType:I

    iget v9, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    iget v10, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->maxOutstandingEncoderFrames:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    invoke-direct/range {v3 .. v14}, Lfqq;-><init>(Lfnp;Lfus;JIIIIIILandroid/os/Handler;)V

    .line 187
    :goto_3
    iget v4, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->targetBitRate:I

    invoke-virtual {v3, v4}, Lfqp;->initializeMediaCodec(I)Z

    move-result v4

    if-nez v4, :cond_5

    .line 188
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->releaseEncoderInstances(Ljava/util/List;)V

    .line 190
    iget-wide v2, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->nativeEncoderId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->handleHardwareFailure(J)V

    goto :goto_0

    .line 168
    :cond_2
    const/16 v17, 0x2

    goto :goto_2

    .line 176
    :cond_3
    const/4 v14, 0x0

    .line 177
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->manageEncoderSurfacesDirectly:Z

    if-eqz v3, :cond_4

    .line 178
    new-instance v3, Lfwo;

    invoke-direct {v3, v11, v12}, Lfwo;-><init>(II)V

    .line 179
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->getOrCreateSharedInputSurfaceForSize(Lfwo;)Lfoy;

    move-result-object v14

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->usedEncoderSurfaces:Ljava/util/Map;

    iget-wide v6, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->nativeEncoderId:J

    .line 181
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 182
    const-string v3, "Expected non-null"

    invoke-static {v3, v14}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 183
    check-cast v3, Lfoy;

    .line 184
    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    :cond_4
    new-instance v3, Lfqs;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->callDirector:Lfnp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    .line 186
    invoke-virtual {v5}, Lfpc;->getEglContextWrapper()Lfus;

    move-result-object v5

    iget-wide v6, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->nativeEncoderId:J

    iget v8, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->codecType:I

    iget v9, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->width:I

    iget v10, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->height:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->maxOutstandingEncoderFrames:I

    iget v15, v2, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$EncoderConfigurationOutputParams;->temporalLayerCount:I

    invoke-direct/range {v3 .. v15}, Lfqs;-><init>(Lfnp;Lfus;JIIIIIILfoy;I)V

    goto :goto_3

    .line 192
    :cond_5
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    move/from16 v3, v17

    goto/16 :goto_1

    .line 194
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 195
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 196
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initialized:Z

    .line 197
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 197
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_7
    move/from16 v17, v3

    goto/16 :goto_2
.end method

.method private final shouldForceKeyFrame()Z
    .locals 4

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->shouldForceKeyframes:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputIsScreencast:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->getCurrentCodec()I

    move-result v0

    if-nez v0, :cond_0

    .line 141
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->nextKeyFrameTimeMs:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    .line 142
    goto :goto_0
.end method

.method private final stopEncoders()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncodersRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 147
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 148
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 149
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->releaseEncoderInstances(Ljava/util/List;)V

    .line 151
    return-void

    .line 149
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final encodeFrame(IJZ[F)Z
    .locals 14

    .prologue
    .line 53
    iget-object v9, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v9

    .line 54
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->initialized:Z

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    monitor-exit v9

    .line 71
    :goto_0
    return v0

    .line 56
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 57
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v8, v0

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqp;

    .line 59
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureWidth:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureHeight:I

    move v1, p1

    move-wide/from16 v4, p2

    move/from16 v6, p4

    move-object/from16 v7, p5

    .line 60
    invoke-virtual/range {v0 .. v7}, Lfqp;->encodeFrame(IIIJZ[F)Z

    move-result v0

    or-int/2addr v0, v8

    move v8, v0

    .line 61
    goto :goto_1

    .line 62
    :cond_1
    if-eqz v8, :cond_2

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->callDirector:Lfnp;

    invoke-virtual {v0}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    invoke-virtual {v0}, Lfnv;->getEncodeLatencyTracker()Lfvc;

    move-result-object v0

    .line 64
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->callDirector:Lfnp;

    .line 67
    invoke-virtual {v0}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lfnv;->getEncodeLatencyTracker()Lfvc;

    move-result-object v0

    .line 69
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, v10, v11}, Lfvc;->addStartDatapoint(Ljava/lang/Object;J)V

    .line 70
    :cond_2
    monitor-exit v9

    .line 71
    const/4 v0, 0x1

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final getCurrentCodec()I
    .locals 3

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, -0x1

    monitor-exit v1

    .line 75
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderInstances:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqp;

    invoke-virtual {v0}, Lfqp;->getCodecType()I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final initializeGLContext()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public final synthetic lambda$new$0$MediaCodecSimulcastEncoder()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->resetEncoders()V

    return-void
.end method

.method public final synthetic lambda$new$1$MediaCodecSimulcastEncoder()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->availableEncoderSurfaces:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder$a;->evictAll()V

    .line 253
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->usedEncoderSurfaces:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    const-string v0, "Did not release all used encoder surfaces. Releasing now."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->usedEncoderSurfaces:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoy;

    .line 256
    invoke-virtual {v0}, Lfoy;->release()V

    goto :goto_0

    .line 258
    :cond_0
    return-void
.end method

.method public final synthetic lambda$release$2$MediaCodecSimulcastEncoder(Ljava/util/concurrent/CountDownLatch;)V
    .locals 2

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->stopEncoders()V

    .line 249
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->glManager:Lfpc;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->releaseSharedEncoderSurfacesRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 250
    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 251
    return-void
.end method

.method public final release()V
    .locals 4

    .prologue
    .line 209
    invoke-static {}, Lfmw;->b()V

    .line 210
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 211
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    new-instance v2, Lfqx;

    invoke-direct {v2, p0, v0}, Lfqx;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 212
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 213
    const-wide/16 v2, 0x1388

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v0

    const-string v0, "GL thread interrupted unexpectedly."

    invoke-static {v0}, Lfvh;->loge(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setFlipNeeded(Z)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public final setResolution(IIZ)V
    .locals 3

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureWidth:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureHeight:I

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputIsScreencast:Z

    if-ne v0, p3, :cond_0

    .line 52
    :goto_0
    return-void

    .line 46
    :cond_0
    const/16 v0, 0x49

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Encoder setResolution with new resolution: Input: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 47
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureWidth:I

    .line 49
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputTextureHeight:I

    .line 50
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->inputIsScreencast:Z

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->encoderThreadHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecSimulcastEncoder;->checkNativeEncoderConfigRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 52
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
