.class public Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/hangouts/video/internal/Stats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AggregatePrintStats"
.end annotation


# instance fields
.field public final frameRateReceiveList:Ljava/util/ArrayList;

.field public final frameRateSendList:Ljava/util/ArrayList;

.field public mostRecentlyUsingRelayServer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;Z)Z
    .locals 0

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    return p1
.end method


# virtual methods
.method public print(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 5
    const-string v0, "Aggregate statistics"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateSendList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->access$000(Ljava/util/ArrayList;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/16 v1, 0x24

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "               send FPS: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->frameRateReceiveList:Ljava/util/ArrayList;

    .line 8
    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->access$000(Ljava/util/ArrayList;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/16 v1, 0x27

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "  Median video receive FPS: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 9
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 10
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;->mostRecentlyUsingRelayServer:Z

    const/16 v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "  using relay: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 11
    return-void
.end method
