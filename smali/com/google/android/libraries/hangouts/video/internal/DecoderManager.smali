.class public Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field public static final DECODER_STOP_TIMEOUT_SECONDS:I = 0x5

.field public static final MAX_SIMULTANEOUS_RESETS:I = 0x5


# instance fields
.field public allowedCodecFlagsOverride:I

.field public final context:Landroid/content/Context;

.field public final failedOverSsrcs:Ljava/util/Set;

.field public nativeContext:J
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public final resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field public final unsupportedResolutionSsrcs:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->failedOverSsrcs:Ljava/util/Set;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->unsupportedResolutionSsrcs:Ljava/util/Set;

    .line 4
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const/4 v3, 0x5

    const-wide/16 v4, 0x5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->allowedCodecFlagsOverride:I

    .line 6
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->context:Landroid/content/Context;

    .line 7
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->nativeInit()V

    .line 8
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->setSupportedCodecs(I)Z

    .line 9
    return-void
.end method

.method private getSupportedHardwareAcceleratedCodecs()I
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v0

    .line 19
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->allowedCodecFlagsOverride:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 20
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->allowedCodecFlagsOverride:I

    and-int/2addr v0, v1

    .line 21
    :cond_0
    return v0
.end method

.method public static getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lfrc;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;Z)I

    move-result v0

    return v0
.end method

.method public static isCodecTypeHardwareAccelerated(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v2

    .line 23
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 26
    :cond_0
    :goto_0
    return v0

    .line 24
    :pswitch_0
    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 25
    :pswitch_1
    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final native nativeInit()V
.end method

.method private native nativeNotifyHardwareFailed(I)Z
.end method

.method private native nativeNotifyResolutionNotSupported(III)Z
.end method

.method private final native nativeRelease()V
.end method

.method private final native setSupportedCodecs(I)Z
.end method


# virtual methods
.method public areDynamicResolutionChangesSupported()Z
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->context:Landroid/content/Context;

    .line 29
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "babel_hangout_hardware_decode_supports_dynamic_resolution_changes"

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_0

    const/4 v0, 0x1

    .line 30
    :goto_0
    invoke-static {v1, v2, v0}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method native consumeNextEncodedFrame(IJLjava/nio/ByteBuffer;)Z
.end method

.method public createDecoder(Lfnp;Lfqk;)Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 33
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 34
    new-instance v0, Lfql;

    invoke-direct {v0, p1, p2}, Lfql;-><init>(Lfnp;Lfqk;)V

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lfqn;

    invoke-direct {v0, p1, p2}, Lfqn;-><init>(Lfnp;Lfqk;)V

    goto :goto_0
.end method

.method native frameDecoded(IJIIJ)Z
.end method

.method native getCodecType(I)I
.end method

.method public getNativeContext()J
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->nativeContext:J

    return-wide v0
.end method

.method native getNextEncodedFrameMetadata(IZLjava/lang/Object;)Z
.end method

.method public getResetDecoderExecutor()Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-object v0
.end method

.method public hadHardwareFailed(I)Z
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->failedOverSsrcs:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public notifyHardwareFailed(I)Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->failedOverSsrcs:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->nativeNotifyHardwareFailed(I)Z

    move-result v0

    return v0
.end method

.method public notifyResolutionNotSupported(III)Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->unsupportedResolutionSsrcs:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->nativeNotifyResolutionNotSupported(III)Z

    move-result v0

    return v0
.end method

.method public notifyResolutionNowSupported(I)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->unsupportedResolutionSsrcs:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 12
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->nativeRelease()V

    .line 13
    return-void
.end method

.method public setAllowedHardwareCodecs(I)V
    .locals 1

    .prologue
    .line 15
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->allowedCodecFlagsOverride:I

    .line 16
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getSupportedHardwareAcceleratedCodecs()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->setSupportedCodecs(I)Z

    .line 17
    return-void
.end method

.method public wasDecodingHardwareUnsupportedResolution(I)Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->unsupportedResolutionSsrcs:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
