.class public final Lcom/google/android/libraries/hangouts/video/internal/CallService$a;
.super Landroid/os/Binder;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/hangouts/video/internal/CallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/internal/CallService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public final bindToCall(Lfnp;)V
    .locals 3

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;

    invoke-static {v0, p1}, Lcom/google/android/libraries/hangouts/video/internal/CallService;->access$002(Lcom/google/android/libraries/hangouts/video/internal/CallService;Lfnp;)Lfnp;

    .line 3
    invoke-virtual {p1}, Lfnp;->getCallStateInfo()Lfvu;

    move-result-object v0

    .line 4
    iget-object v0, v0, Lfvu;->b:Lfvs;

    .line 6
    const/4 v0, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    const-string v0, "No notification was specified for the call; service may be terminated unexpectedly."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 12
    :goto_0
    return-void

    .line 10
    :cond_0
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 11
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/hangouts/video/internal/CallService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public final unbindFromCall()V
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/CallService;->access$002(Lcom/google/android/libraries/hangouts/video/internal/CallService;Lfnp;)Lfnp;

    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/CallService$a;->this$0:Lcom/google/android/libraries/hangouts/video/internal/CallService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/CallService;->stopForeground(Z)V

    .line 15
    return-void
.end method
