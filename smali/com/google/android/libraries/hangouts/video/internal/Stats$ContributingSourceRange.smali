.class public Lcom/google/android/libraries/hangouts/video/internal/Stats$ContributingSourceRange;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/hangouts/video/internal/Stats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContributingSourceRange"
.end annotation


# instance fields
.field public final csrc:I

.field public final end:J

.field public final start:J


# direct methods
.method public constructor <init>(IJJ)V
    .locals 0
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$ContributingSourceRange;->csrc:I

    .line 3
    iput-wide p2, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$ContributingSourceRange;->start:J

    .line 4
    iput-wide p4, p0, Lcom/google/android/libraries/hangouts/video/internal/Stats$ContributingSourceRange;->end:J

    .line 5
    return-void
.end method
