.class public Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# instance fields
.field public final frameRate:I
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public final height:I
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public final renderer:Lcom/google/android/libraries/hangouts/video/internal/Renderer;
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public final ssrc:I
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field public final width:I
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;IIII)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->renderer:Lcom/google/android/libraries/hangouts/video/internal/Renderer;

    .line 3
    iput p2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    .line 4
    iput p3, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    .line 5
    iput p4, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    .line 6
    iput p5, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->frameRate:I

    .line 7
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8
    if-ne p1, p0, :cond_1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 10
    :cond_1
    instance-of v2, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    if-nez v2, :cond_2

    move v0, v1

    .line 11
    goto :goto_0

    .line 12
    :cond_2
    check-cast p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;

    .line 13
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->renderer:Lcom/google/android/libraries/hangouts/video/internal/Renderer;

    iget-object v3, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->renderer:Lcom/google/android/libraries/hangouts/video/internal/Renderer;

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->frameRate:I

    iget v3, p1, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->frameRate:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->ssrc:I

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->width:I

    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->height:I

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/VideoViewRequest;->frameRate:I

    const/16 v4, 0x52

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "VideoViewRequest: ssrc: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " w: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " h: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fps: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
