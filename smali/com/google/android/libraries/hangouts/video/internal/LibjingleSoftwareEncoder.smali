.class public final Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;
.super Lcom/google/android/libraries/hangouts/video/internal/Renderer;
.source "PG"

# interfaces
.implements Lfoz;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;
    }
.end annotation


# instance fields
.field public final encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

.field public lastInputTextureSize:Lfwo;

.field public lastIsScreencast:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;-><init>(I)V

    .line 2
    new-instance v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;-><init>(Lfmt;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    .line 3
    return-void
.end method


# virtual methods
.method public final encodeFrame(IJZ[F)Z
    .locals 2

    .prologue
    .line 15
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    if-eqz p4, :cond_0

    .line 16
    const v0, 0x8d65

    :goto_0
    iput v0, v1, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->inputTextureType:I

    .line 17
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    iput p1, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->inputTextureName:I

    .line 18
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    iput-object p5, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->transformMatrixUpdate:[F

    .line 19
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    iput-wide p2, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->timestampMicros:J

    .line 20
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->isFake:Z

    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->nativeRenderFrame(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    const/4 v0, 0x1

    return v0

    .line 16
    :cond_0
    const/16 v0, 0xde1

    goto :goto_0
.end method

.method public final getCurrentCodec()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public final native nativeAlignCaptureTimeMicros(J)J
.end method

.method public final sendFakeFrame(J)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    iput-wide p1, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->timestampMicros:J

    .line 24
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;->isFake:Z

    .line 25
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->encoderInputData:Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder$EncoderInputData;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->nativeRenderFrame(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 26
    return-void
.end method

.method public final setFlipNeeded(Z)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public final setResolution(IIZ)V
    .locals 3

    .prologue
    .line 5
    new-instance v0, Lfwo;

    invoke-direct {v0, p1, p2}, Lfwo;-><init>(II)V

    .line 6
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->lastInputTextureSize:Lfwo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->lastInputTextureSize:Lfwo;

    invoke-virtual {v1, v0}, Lfwo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 7
    :cond_0
    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->lastInputTextureSize:Lfwo;

    .line 8
    const-string v1, "sub_indims"

    .line 9
    iget v2, v0, Lfwo;->a:I

    shl-int/lit8 v2, v2, 0x10

    iget v0, v0, Lfwo;->b:I

    or-int/2addr v0, v2

    .line 10
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->nativeSetIntParam(Ljava/lang/String;I)V

    .line 11
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->lastIsScreencast:Z

    if-eq v0, p3, :cond_2

    .line 12
    iput-boolean p3, p0, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->lastIsScreencast:Z

    .line 13
    const-string v1, "sub_screencast"

    if-eqz p3, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/hangouts/video/internal/LibjingleSoftwareEncoder;->nativeSetIntParam(Ljava/lang/String;I)V

    .line 14
    :cond_2
    return-void

    .line 13
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
