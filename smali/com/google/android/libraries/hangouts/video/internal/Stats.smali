.class public abstract Lcom/google/android/libraries/hangouts/video/internal/Stats;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/Stats$a;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$ConnectionInfoStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$BandwidthEstimationStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoReceiverStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$VideoSenderStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceReceiverStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$ContributingSourceRange;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$VoiceSenderStats;,
        Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;
    }
.end annotation


# static fields
.field public static final CONNECTION_INFO_STATS_VIDEO:I = 0x0

.field public static final CONNECTION_INFO_STATS_VOICE:I = 0x1

.field public static final DEFAULT_FOR_REQUIRED_PROTO_FIELD:I = -0x1

.field public static final RELAY_CONNECTION_TYPE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/util/ArrayList;)F
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->calculateMedian(Ljava/util/ArrayList;)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->parseConnectionType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/libraries/hangouts/video/internal/Stats;->parseConnectionProtocol(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static calculateMedian(Ljava/util/ArrayList;)F
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 29
    if-nez v2, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 39
    :goto_0
    return v0

    .line 31
    :cond_0
    new-array v3, v2, [F

    .line 32
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 33
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v3, v1

    .line 34
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 35
    :cond_1
    invoke-static {v3}, Ljava/util/Arrays;->sort([F)V

    .line 36
    div-int/lit8 v0, v2, 0x2

    .line 37
    rem-int/lit8 v1, v2, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 38
    aget v0, v3, v0

    goto :goto_0

    .line 39
    :cond_2
    add-int/lit8 v1, v0, -0x1

    aget v1, v3, v1

    aget v0, v3, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method static createEmptyMediaProto(I)Lgiy;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, -0x1

    .line 2
    new-instance v0, Lgiy;

    invoke-direct {v0}, Lgiy;-><init>()V

    .line 3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->a:Ljava/lang/Integer;

    .line 4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->b:Ljava/lang/Integer;

    .line 5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->c:Ljava/lang/Integer;

    .line 6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->d:Ljava/lang/Integer;

    .line 7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->e:Ljava/lang/Integer;

    .line 8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->f:Ljava/lang/Integer;

    .line 9
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgiy;->g:Ljava/lang/Long;

    .line 10
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->h:Ljava/lang/Integer;

    .line 11
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lgiy;->i:Ljava/lang/Long;

    .line 12
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lgiy;->j:Ljava/lang/Integer;

    .line 13
    return-object v0
.end method

.method private static parseConnectionProtocol(Ljava/lang/String;)I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 21
    const-string v0, "udp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    .line 27
    :goto_0
    return v0

    .line 23
    :cond_0
    const-string v0, "tcp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    goto :goto_0

    .line 25
    :cond_1
    const-string v0, "ssltcp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    const/4 v0, 0x3

    goto :goto_0

    .line 27
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseConnectionType(Ljava/lang/String;)I
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 14
    const-string v0, "local"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 15
    const/4 v0, 0x1

    .line 20
    :goto_0
    return v0

    .line 16
    :cond_0
    const-string v0, "stun"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17
    const/4 v0, 0x2

    goto :goto_0

    .line 18
    :cond_1
    const-string v0, "relay"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19
    const/4 v0, 0x3

    goto :goto_0

    .line 20
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract addTo(Lgiv;)V
.end method

.method public abstract print(Ljava/io/PrintWriter;Lcom/google/android/libraries/hangouts/video/internal/Stats$AggregatePrintStats;)V
.end method
