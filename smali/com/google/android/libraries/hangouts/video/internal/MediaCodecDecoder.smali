.class public abstract Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;
    }
.end annotation


# static fields
.field public static final DEBUG_TAG:Ljava/lang/String; = "vclib_save_dec_video"

.field public static final DECODER_THREAD_NAME:Ljava/lang/String; = "DecoderHandlerThread"

.field public static final H264_SOFTWARE_DECODER_NAME:Ljava/lang/String; = "OMX.google.h264.decoder"

.field public static final MAX_CONSECUTIVE_MEDIA_CODEC_CREATION_FAILURE_COUNT:I = 0x3

.field public static final MAX_FAILED_GETNEXTINPUTBUFFER_COUNT:I = 0x64

.field public static final MAX_HARDWARE_DECODER_COUNT:I

.field public static final MAX_VP8_DRAIN_TIME_MILLIS:J

.field public static final POLL_PERIOD_MS:I = 0xa

.field public static final RESET_RETRY_PERIOD_MS:I = 0xc8

.field public static final TIMESTAMP_TO_PRESENTATION_TIME_MULTIPLIER:I = 0xb

.field public static final VP8_SOFTWARE_DECODER_NAME:Ljava/lang/String; = "OMX.google.vp8.decoder"

.field public static hardwareDecoderCount:I

.field public static final hardwareDecoderCountLock:Ljava/lang/Object;


# instance fields
.field public final callServiceCallbacks:Lfvt;

.field public volatile codecType:I

.field public consecutiveMediaCodecCreationFailureCount:I

.field public currentInputHeight:I

.field public currentInputWidth:I

.field public currentOutputFormat:Lfwe;

.field public currentResolutionSupported:Z

.field public final decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

.field public final decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

.field public final decoderThread:Landroid/os/HandlerThread;

.field public final decoderThreadHandler:Landroid/os/Handler;

.field public final decoderUpdateCallback:Lfqk;

.field public failed:Z

.field public failedGetNextInputBufferCount:I

.field public forceSoftwareMediaCodecUsage:Z

.field public frameRecorder:Lfqu;

.field public final impressionReporter:Lfuv;

.field public volatile initialized:Z

.field public isHardwareDecoder:Z

.field public final lock:Ljava/lang/Object;

.field public mediaCodec:Landroid/media/MediaCodec;

.field public final pollInputRunnable:Ljava/lang/Runnable;

.field public final presentationTimeToNtpTime:Ljava/util/Map;

.field public requireKeyframe:Z

.field public final resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field public final resetDecoderRunnable:Ljava/lang/Runnable;

.field public volatile ssrc:I

.field public final stopDecoderRunnable:Ljava/lang/Runnable;

.field public surface:Landroid/view/Surface;

.field public vp8DecoderDrainStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 529
    const-string v0, "manta"

    sget-object v1, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, 0x3

    sput v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->MAX_HARDWARE_DECODER_COUNT:I

    .line 532
    :goto_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->MAX_VP8_DRAIN_TIME_MILLIS:J

    .line 533
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCountLock:Ljava/lang/Object;

    return-void

    .line 531
    :cond_0
    const v0, 0x7fffffff

    sput v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->MAX_HARDWARE_DECODER_COUNT:I

    goto :goto_0
.end method

.method public constructor <init>(Lfnp;Lfqk;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    .line 55
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    .line 57
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    .line 59
    new-instance v0, Lfqc;

    invoke-direct {v0, p0}, Lfqc;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    .line 60
    new-instance v0, Lfqd;

    invoke-direct {v0, p0}, Lfqd;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoderRunnable:Ljava/lang/Runnable;

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    .line 63
    new-instance v0, Lfqj;

    invoke-direct {v0, p0}, Lfqj;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->pollInputRunnable:Ljava/lang/Runnable;

    .line 64
    invoke-virtual {p1}, Lfnp;->getDecoderManager()Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getResetDecoderExecutor()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 66
    invoke-virtual {p1}, Lfnp;->getImpressionReporter()Lfuv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->impressionReporter:Lfuv;

    .line 67
    invoke-virtual {p1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->callServiceCallbacks:Lfvt;

    .line 68
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    .line 69
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DecoderHandlerThread"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 71
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method protected constructor <init>(Lfnp;Lfqk;Landroid/os/HandlerThread;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    .line 35
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    .line 37
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    .line 39
    new-instance v0, Lfpw;

    invoke-direct {v0, p0}, Lfpw;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    .line 40
    new-instance v0, Lfqb;

    invoke-direct {v0, p0}, Lfqb;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoderRunnable:Ljava/lang/Runnable;

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    .line 42
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    .line 43
    new-instance v0, Lfqj;

    invoke-direct {v0, p0}, Lfqj;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->pollInputRunnable:Ljava/lang/Runnable;

    .line 44
    invoke-virtual {p1}, Lfnp;->getDecoderManager()Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getResetDecoderExecutor()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 46
    invoke-virtual {p1}, Lfnp;->getImpressionReporter()Lfuv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->impressionReporter:Lfuv;

    .line 47
    invoke-virtual {p1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->callServiceCallbacks:Lfvt;

    .line 48
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    .line 49
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    .line 50
    iput-object p4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    .line 51
    return-void
.end method

.method public static synthetic access$000(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->pollInputRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic access$100(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private cleanup(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 457
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->usedInputBuffer(I)V

    .line 458
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 459
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failedGetNextInputBufferCount:I

    .line 460
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    .line 461
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initialized:Z

    .line 462
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputWidth:I

    .line 463
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputHeight:I

    .line 464
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    .line 465
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->releaseMediaCodec(Z)V

    .line 466
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    .line 467
    return-void
.end method

.method private createBufferInfo(JII)Landroid/media/MediaCodec$BufferInfo;
    .locals 3

    .prologue
    .line 469
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 470
    iput-wide p1, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 471
    iput p3, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 472
    const/4 v1, 0x0

    iput v1, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    .line 473
    iput p4, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    .line 474
    return-object v0
.end method

.method private createMediaCodec()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->forceSoftwareMediaCodecUsage:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    .line 121
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    if-eqz v0, :cond_0

    .line 122
    sget-object v3, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCountLock:Ljava/lang/Object;

    monitor-enter v3

    .line 123
    :try_start_0
    sget v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    sget v4, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->MAX_HARDWARE_DECODER_COUNT:I

    if-lt v0, v4, :cond_3

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    .line 126
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    if-eqz v0, :cond_4

    .line 128
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 130
    invoke-virtual {v0}, Landroid/media/MediaCodec;->getCodecInfo()Landroid/media/MediaCodecInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 131
    invoke-virtual {v0}, Landroid/media/MediaCodec;->getCodecInfo()Landroid/media/MediaCodecInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lfrc;->isKnownSoftwareImplementation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    .line 134
    sget-object v3, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCountLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 135
    :try_start_2
    sget v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    .line 136
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 137
    :try_start_3
    const-string v0, "%s: createDecoderByType returned a software decoder."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 138
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 139
    invoke-static {v0, v3}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->switchToSoftwareMode(Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 150
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 120
    goto :goto_0

    .line 125
    :cond_3
    :try_start_4
    sget v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    goto :goto_1

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 136
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string v3, "%s: MediaCodec.createDecoderByType failed, "

    new-array v4, v6, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 145
    :cond_4
    :try_start_7
    const-string v0, "%s: Creating a software decoder."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getSoftwareDecoderName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 148
    :catch_1
    move-exception v0

    .line 149
    const-string v3, "%s: MediaCodec.createByCodecName failed"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private getDebugName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 468
    const-string v0, "Decoder (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMimeType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-nez v0, :cond_0

    .line 152
    const-string v0, "video/x-vnd.on2.vp8"

    .line 156
    :goto_0
    return-object v0

    .line 153
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 154
    const-string v0, "video/avc"

    goto :goto_0

    .line 155
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unknown codec type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 156
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSoftwareDecoderName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-nez v0, :cond_0

    .line 158
    const-string v0, "OMX.google.vp8.decoder"

    .line 162
    :goto_0
    return-object v0

    .line 159
    :cond_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 160
    const-string v0, "OMX.google.h264.decoder"

    goto :goto_0

    .line 161
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unknown codec type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initializeMediaCodec()Z
    .locals 7

    .prologue
    const/16 v4, 0x780

    const/16 v3, 0x280

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 169
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->createMediaCodec()V

    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    .line 171
    const-string v0, "%s: Unable to create a MediaCodec decoder."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 186
    :goto_0
    return v0

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v3}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 174
    const-string v3, "max-width"

    invoke-virtual {v0, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 175
    const-string v3, "max-height"

    invoke-virtual {v0, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 176
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {p0, v3, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    .line 177
    :try_start_0
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    iget-object v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 178
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 184
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initialized:Z

    .line 185
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->onAfterInitialized()V

    move v0, v2

    .line 186
    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 181
    :goto_1
    const-string v3, "%s: MediaCodec decoder initialization failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v3, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;)V

    move v0, v1

    .line 183
    goto :goto_0

    .line 180
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private isResolutionChange(II)Z
    .locals 1

    .prologue
    .line 317
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputWidth:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputHeight:I

    if-eq p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isResolutionSupported(II)Z
    .locals 4

    .prologue
    const/16 v3, 0x780

    const/16 v2, 0x438

    .line 318
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 319
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isResolutionSupportedLollipop(II)Z

    move-result v0

    .line 320
    :goto_0
    return v0

    :cond_0
    if-gt p1, v3, :cond_2

    if-gt p2, v3, :cond_2

    if-le p1, v2, :cond_1

    if-gt p2, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isResolutionSupportedLollipop(II)Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 321
    if-le p2, p1, :cond_1

    .line 325
    :goto_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    if-nez v1, :cond_0

    .line 326
    const-string v1, "%s: Getting Codec info when mediaCodec is null"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    :goto_1
    return v0

    .line 328
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 329
    invoke-virtual {v1}, Landroid/media/MediaCodec;->getCodecInfo()Landroid/media/MediaCodecInfo;

    move-result-object v1

    .line 330
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v1

    .line 331
    invoke-virtual {v1}, Landroid/media/MediaCodecInfo$CodecCapabilities;->getVideoCapabilities()Landroid/media/MediaCodecInfo$VideoCapabilities;

    move-result-object v1

    .line 332
    invoke-virtual {v1, p2, p1}, Landroid/media/MediaCodecInfo$VideoCapabilities;->isSizeSupported(II)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_1

    .line 335
    :catch_0
    move-exception v1

    const-string v1, "%s: Decoder failed getCapabilitiesForType for MIME type %s. Claiming unsupported size."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 336
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 337
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getMimeType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 338
    invoke-static {v1, v2}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    move v5, p2

    move p2, p1

    move p1, v5

    goto :goto_0
.end method

.method public static final synthetic lambda$releaseMediaCodec$3$MediaCodecDecoder(Landroid/media/MediaCodec;)V
    .locals 1

    .prologue
    .line 519
    :try_start_0
    invoke-virtual {p0}, Landroid/media/MediaCodec;->stop()V

    .line 520
    invoke-virtual {p0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private releaseMediaCodec(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 75
    new-instance v1, Lfqe;

    invoke-direct {v1, v0}, Lfqe;-><init>(Landroid/media/MediaCodec;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getActiveCount()I

    move-result v0

    .line 77
    const/4 v2, 0x5

    if-lt v0, v2, :cond_0

    .line 78
    const-string v2, "%s: Currently processing %d resets. Need to wait a bit to reset."

    new-array v3, v5, [Ljava/lang/Object;

    .line 79
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    .line 80
    invoke-static {v2, v3}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 82
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 83
    invoke-interface {v0, v2, v3, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 108
    :cond_1
    :goto_0
    iput-object v7, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 109
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    invoke-virtual {v0}, Lfqu;->release()V

    .line 111
    :cond_2
    iput-object v7, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    .line 112
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    if-eqz v0, :cond_3

    .line 113
    sget-object v1, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCountLock:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_1
    sget v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->hardwareDecoderCount:I

    .line 115
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    :cond_3
    :goto_1
    return-void

    .line 86
    :catch_0
    move-exception v0

    const-string v0, "%s: Decoder hung while trying to stop the codec."

    new-array v1, v5, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->impressionReporter:Lfuv;

    const/16 v1, 0xde3

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 88
    if-nez p1, :cond_1

    .line 89
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->sendHardwareMalfunctionedNotification()V

    goto :goto_0

    .line 92
    :catch_1
    move-exception v0

    :goto_2
    const-string v0, "%s: Decoder thread got interrupted while waiting for codec to be stopped/released."

    new-array v1, v5, [Ljava/lang/Object;

    .line 93
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 94
    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isTerminating()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    :cond_4
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 99
    :cond_5
    const-string v0, "%s: Failed to enqueue release of decoder within 5 seconds!"

    new-array v1, v5, [Ljava/lang/Object;

    .line 100
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 101
    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iput-boolean v5, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->notifyHardwareFailed(I)Z

    .line 104
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_6

    .line 105
    new-instance v0, Lfqf;

    invoke-direct {v0, p0}, Lfqf;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 106
    :cond_6
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->sendHardwareMalfunctionedNotification()V

    goto :goto_1

    .line 115
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 92
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method private resetDecoder()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 378
    invoke-direct {p0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoder(Z)V

    .line 379
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getCodecType(I)I

    move-result v2

    .line 380
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setCurrentCodec(I)V

    .line 381
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 382
    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initialized:Z

    .line 383
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 386
    :try_start_0
    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    move-result v3

    if-nez v3, :cond_4

    .line 387
    :cond_2
    const-string v3, "%s: Decoder skipping reset. ssrc=%s, surface=%s, isValid=%b"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 388
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 389
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    .line 390
    invoke-virtual {v6}, Landroid/view/Surface;->isValid()Z

    move-result v6

    if-eqz v6, :cond_3

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v5

    .line 391
    invoke-static {v3, v4}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    monitor-exit v2

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 390
    goto :goto_1

    .line 393
    :cond_4
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initializeMediaCodec()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->pollInputRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private sendHardwareMalfunctionedNotification()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lfqg;

    invoke-direct {v0, p0}, Lfqg;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 118
    return-void
.end method

.method private stopDecoder(Z)V
    .locals 2

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->onDecoderStopping()V

    .line 446
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->pollInputRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 448
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->cleanup(Z)V

    .line 449
    return-void
.end method


# virtual methods
.method protected createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;
    .locals 1

    .prologue
    .line 119
    invoke-static {p1}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCapabilitiesSupported()Z
    .locals 2

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 205
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentCodec()I
    .locals 2

    .prologue
    .line 195
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 196
    :try_start_0
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    monitor-exit v1

    return v0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCurrentOutputFormat()Lfwe;
    .locals 2

    .prologue
    .line 198
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    monitor-exit v1

    .line 201
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    invoke-virtual {v0}, Lfwe;->a()Lfwe;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getDecodeLatencyTracker()Lfvc;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    return-object v0
.end method

.method public getDecoderThreadHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public abstract getInputBuffer(I)Ljava/nio/ByteBuffer;
.end method

.method public getMediaCodec()Landroid/media/MediaCodec;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    return-object v0
.end method

.method public abstract getNextInputBufferIndex()I
.end method

.method public abstract getOutputBuffer(I)Ljava/nio/ByteBuffer;
.end method

.method public handleDecodedFrame(ILandroid/media/MediaCodec$BufferInfo;)V
    .locals 12

    .prologue
    .line 340
    invoke-static {}, Lfmw;->c()V

    .line 341
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->consecutiveMediaCodecCreationFailureCount:I

    .line 342
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    if-nez v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->updateOutputFormat(Landroid/media/MediaFormat;)V

    .line 345
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    .line 347
    if-eqz v0, :cond_1

    .line 348
    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lfvc;->addEndDatapoint(Ljava/lang/Object;J)V

    .line 349
    :cond_1
    iget-wide v0, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    const-wide/16 v2, 0xb

    div-long v2, v0, v2

    .line 350
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    .line 351
    iget v4, v0, Lfwe;->a:I

    .line 353
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    .line 354
    iget v5, v0, Lfwe;->b:I

    .line 356
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 357
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 361
    :goto_0
    const-wide/16 v6, 0x0

    .line 362
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    iget-wide v10, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    iget-wide v6, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 364
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->frameDecoded(IJIIJ)Z

    .line 365
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_3

    .line 366
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-interface {v0, v2, v3, v8, v9}, Lfqk;->onRenderingStarted(JJ)V

    .line 367
    :cond_3
    return-void

    .line 345
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public handleOutputFormatChange(Landroid/media/MediaFormat;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 368
    const-string v0, "%s: resolution changed. New format: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->areDynamicResolutionChangesSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 370
    const-string v0, "%s: Missed a dynamic resolution change when handling incoming frames. Resetting hw decoder now."

    new-array v1, v4, [Ljava/lang/Object;

    .line 371
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 372
    invoke-static {v0, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    .line 377
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 376
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->updateOutputFormat(Landroid/media/MediaFormat;)V

    .line 377
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isInitialized()Z
    .locals 2

    .prologue
    .line 192
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final synthetic lambda$new$1$MediaCodecDecoder()V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    return-void
.end method

.method public final synthetic lambda$new$2$MediaCodecDecoder()V
    .locals 1

    .prologue
    .line 524
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoder(Z)V

    return-void
.end method

.method public final synthetic lambda$processInput$10$MediaCodecDecoder(Z)V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    invoke-interface {v0, p1}, Lfqk;->capabilitiesChanged(Z)V

    return-void
.end method

.method public final synthetic lambda$releaseMediaCodec$4$MediaCodecDecoder()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfqk;->capabilitiesChanged(Z)V

    return-void
.end method

.method public final synthetic lambda$sendHardwareMalfunctionedNotification$5$MediaCodecDecoder()V
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->callServiceCallbacks:Lfvt;

    new-instance v1, Lfwb;

    sget-object v2, Lfwc;->b:Lfwc;

    invoke-direct {v1, v2}, Lfwb;-><init>(Lfwc;)V

    invoke-virtual {v0, v1}, Lfvt;->onQualityNotification(Lfwb;)V

    return-void
.end method

.method public final synthetic lambda$setCurrentCodec$6$MediaCodecDecoder(I)V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    invoke-interface {v0, p1}, Lfqk;->currentCodecChanged(I)V

    return-void
.end method

.method public final synthetic lambda$setSourceId$8$MediaCodecDecoder(Z)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    invoke-interface {v0, p1}, Lfqk;->capabilitiesChanged(Z)V

    return-void
.end method

.method public final synthetic lambda$setSourceId$9$MediaCodecDecoder(I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 478
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v3

    .line 479
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 480
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->wasDecodingHardwareUnsupportedResolution(I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    .line 481
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->hadHardwareFailed(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    .line 482
    const-string v0, "%s: Previous known state of decoder: resolution supported: %b, failed: %b"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 483
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-boolean v5, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    .line 484
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    iget-boolean v5, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    .line 485
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    .line 486
    invoke-static {v0, v4}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v0

    .line 488
    if-eq v0, v3, :cond_0

    .line 489
    const-string v3, "%s: Changed support capabilities. Now: %b"

    new-array v4, v6, [Ljava/lang/Object;

    .line 490
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 491
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v1

    .line 492
    invoke-static {v3, v4}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 493
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v1, :cond_0

    .line 494
    new-instance v1, Lfqa;

    invoke-direct {v1, p0, v0}, Lfqa;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Z)V

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Lfvc;

    .line 496
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lfvc;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    .line 497
    if-eqz v0, :cond_1

    .line 498
    invoke-virtual {v0}, Lfvc;->release()V

    .line 499
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    .line 500
    return-void

    :cond_2
    move v0, v2

    .line 480
    goto :goto_0
.end method

.method public final synthetic lambda$setSurface$7$MediaCodecDecoder(Landroid/view/Surface;Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 502
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/Surface;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    .line 503
    const-string v2, "%s: MediaCodec decoder surface is invalid. Stopping decoder."

    new-array v1, v1, [Ljava/lang/Object;

    .line 504
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 505
    invoke-static {v2, v1}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 506
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    .line 512
    :goto_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    .line 513
    if-eqz p2, :cond_0

    .line 514
    invoke-static {p2}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 515
    :cond_0
    return-void

    .line 507
    :cond_1
    const-string v2, "%s: MediaCodec decoder surface is set: %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 508
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    if-eqz p1, :cond_2

    move v0, v1

    .line 509
    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v1

    .line 510
    invoke-static {v2, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->surface:Landroid/view/Surface;

    goto :goto_0
.end method

.method public final synthetic lambda$switchToSoftwareMode$0$MediaCodecDecoder()V
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfqk;->capabilitiesChanged(Z)V

    return-void
.end method

.method public final synthetic lambda$updateOutputFormat$11$MediaCodecDecoder(Lfwe;)V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    invoke-interface {v0, p1}, Lfqk;->outputFormatChanged(Lfwe;)V

    return-void
.end method

.method public onAfterInitialized()V
    .locals 0

    .prologue
    .line 2
    return-void
.end method

.method public onBeforeInitialized(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 0

    .prologue
    .line 1
    return-void
.end method

.method public onDecoderStopping()V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public processInput()V
    .locals 15

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x3

    const/4 v6, 0x2

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 211
    invoke-static {}, Lfmw;->c()V

    .line 212
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getCodecType(I)I

    move-result v0

    .line 213
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-eq v1, v0, :cond_2

    .line 214
    if-ne v0, v9, :cond_0

    .line 215
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->initialized:Z

    .line 216
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    .line 316
    :cond_1
    :goto_0
    return-void

    .line 218
    :cond_2
    new-instance v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;

    invoke-direct {v7}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;-><init>()V

    .line 219
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    invoke-virtual {v0, v1, v3, v7}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->getNextEncodedFrameMetadata(IZLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    iget v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    iget v1, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isResolutionChange(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 222
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v0

    .line 223
    iget v1, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    iget v3, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    invoke-direct {p0, v1, v3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isResolutionSupported(II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    .line 224
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    if-nez v1, :cond_4

    .line 225
    const-string v1, "%s: Unsupported resolution for decode: (%d x %d)"

    new-array v3, v8, [Ljava/lang/Object;

    .line 226
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    .line 227
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    iget v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    .line 228
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 229
    invoke-static {v1, v3}, Lfvh;->logw(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    iget v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    iget v5, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->notifyResolutionNotSupported(III)Z

    .line 232
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v1

    .line 233
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v3

    if-eq v3, v0, :cond_3

    .line 234
    const-string v0, "%s: Changed support capabilities. Now: %b"

    new-array v3, v6, [Ljava/lang/Object;

    .line 235
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 236
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v12

    .line 237
    invoke-static {v0, v3}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_3

    .line 239
    new-instance v0, Lfpy;

    invoke-direct {v0, p0, v1}, Lfpy;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Z)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 240
    :cond_3
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputWidth:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputHeight:I

    if-eqz v0, :cond_5

    .line 241
    const-string v0, "%s: Dynamic resolution change detected: %d (%d x %d -> %d x %d)"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 242
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 243
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v12

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputWidth:I

    .line 244
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v6

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputHeight:I

    .line 245
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v8

    const/4 v3, 0x4

    iget v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    .line 246
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x5

    iget v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    .line 247
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    .line 248
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->areDynamicResolutionChangesSupported()Z

    move-result v0

    if-nez v0, :cond_5

    .line 250
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoder()V

    goto/16 :goto_0

    .line 231
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->notifyResolutionNowSupported(I)V

    goto :goto_1

    .line 252
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    .line 253
    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->requireKeyframe:Z

    .line 254
    iget v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    if-eqz v0, :cond_6

    iget v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    if-eqz v0, :cond_6

    .line 255
    iget v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputWidth:I

    .line 256
    iget v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentInputHeight:I

    .line 257
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentResolutionSupported:Z

    if-nez v0, :cond_7

    .line 258
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    iget-wide v2, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->timestamp:J

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->consumeNextEncodedFrame(IJLjava/nio/ByteBuffer;)Z

    goto/16 :goto_0

    .line 260
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getNextInputBufferIndex()I

    move-result v1

    .line 261
    if-ne v1, v9, :cond_8

    .line 262
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failedGetNextInputBufferCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failedGetNextInputBufferCount:I

    .line 263
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failedGetNextInputBufferCount:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    .line 264
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Too many failed getNextInputBuffer calls."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 266
    :cond_8
    iput v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failedGetNextInputBufferCount:I

    .line 267
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 268
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 269
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_9

    .line 270
    iput-wide v8, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    goto/16 :goto_0

    .line 271
    :cond_9
    iget-wide v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    sub-long v0, v8, v0

    sget-wide v4, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->MAX_VP8_DRAIN_TIME_MILLIS:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 272
    const-string v0, "%s: VP8 decoder took too long to drain frames."

    new-array v1, v12, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Waited too long for VP8 decoder to drain."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v12}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;Z)V

    goto/16 :goto_0

    .line 275
    :cond_a
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->vp8DecoderDrainStartTime:J

    .line 276
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getInputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 277
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v4, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    iget-wide v10, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->timestamp:J

    invoke-virtual {v0, v4, v10, v11, v6}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->consumeNextEncodedFrame(IJLjava/nio/ByteBuffer;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 278
    const-string v0, "%s: Unable to consume encoded frame."

    new-array v1, v12, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 281
    :cond_b
    iget-boolean v0, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->isEndOfStream:Z

    if-eqz v0, :cond_10

    .line 282
    const/4 v0, 0x4

    .line 283
    :goto_2
    iget-wide v4, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->timestamp:J

    const-wide/16 v10, 0xb

    mul-long/2addr v4, v10

    .line 284
    iget-object v10, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    if-eqz v10, :cond_e

    .line 285
    if-eqz v3, :cond_c

    .line 286
    or-int/lit8 v0, v0, 0x1

    .line 287
    :cond_c
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    iget v10, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->size:I

    .line 288
    invoke-direct {p0, v4, v5, v10, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->createBufferInfo(JII)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v10

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 289
    invoke-virtual {v3, v10, v6}, Lfqu;->writeBuffer(Landroid/media/MediaCodec$BufferInfo;Ljava/nio/ByteBuffer;)V

    move v6, v0

    .line 310
    :goto_3
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    iget v3, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->size:I

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 311
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->usedInputBuffer(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    .line 313
    if-eqz v0, :cond_d

    .line 314
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, v8, v9}, Lfvc;->addStartDatapoint(Ljava/lang/Object;J)V

    .line 315
    :cond_d
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->presentationTimeToNtpTime:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->ntpTimeMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 290
    :cond_e
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x17

    if-eq v10, v11, :cond_f

    const-string v10, "vclib_save_dec_video"

    invoke-static {v10}, Lfvh;->isDebugPropertyEnabled(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    if-eqz v3, :cond_f

    .line 291
    :try_start_0
    new-instance v3, Lfqu;

    iget v10, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    const-string v11, "decode-%d-%dx%d"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget v14, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    .line 292
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget v14, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    .line 293
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    iget v14, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    .line 294
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    .line 295
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v3, v10, v11}, Lfqu;-><init>(ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    .line 297
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getMimeType()Ljava/lang/String;

    move-result-object v3

    iget v10, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->width:I

    iget v11, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->height:I

    .line 298
    invoke-static {v3, v10, v11}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v3

    .line 299
    const-string v10, "max-width"

    const/16 v11, 0x780

    invoke-virtual {v3, v10, v11}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 300
    const-string v10, "max-height"

    const/16 v11, 0x780

    invoke-virtual {v3, v10, v11}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 301
    iget-object v10, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    invoke-virtual {v10, v3}, Lfqu;->start(Landroid/media/MediaFormat;)V

    .line 302
    or-int/lit8 v0, v0, 0x1

    .line 303
    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    iget v10, v7, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder$FrameDataOutputParams;->size:I

    .line 304
    invoke-direct {p0, v4, v5, v10, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->createBufferInfo(JII)Landroid/media/MediaCodec$BufferInfo;

    move-result-object v10

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 305
    invoke-virtual {v3, v10, v6}, Lfqu;->writeBuffer(Landroid/media/MediaCodec$BufferInfo;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v0

    .line 306
    goto/16 :goto_3

    .line 307
    :catch_0
    move-exception v3

    .line 308
    const-string v6, "Unable to create frameRecorder"

    invoke-static {v6, v3}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 309
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->frameRecorder:Lfqu;

    :cond_f
    move v6, v0

    goto/16 :goto_3

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public release()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoderRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 451
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decodeLatencyTracker:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvc;

    .line 454
    if-eqz v0, :cond_1

    .line 455
    invoke-virtual {v0}, Lfvc;->release()V

    .line 456
    :cond_1
    return-void
.end method

.method public reportCodecException(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->reportCodecException(Ljava/lang/Exception;Z)V

    .line 7
    return-void
.end method

.method protected reportCodecException(Ljava/lang/Exception;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 8
    const-string v0, "%s: MediaCodec reported exception"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->consecutiveMediaCodecCreationFailureCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->consecutiveMediaCodecCreationFailureCount:I

    .line 10
    if-nez p2, :cond_0

    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->consecutiveMediaCodecCreationFailureCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 11
    const-string v0, "%s: Attempting to reset decoder."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 19
    :goto_0
    return-void

    .line 13
    :cond_0
    if-nez p2, :cond_1

    .line 14
    const-string v0, "%s: Too many consecutive MediaCodec decoder creation failures."

    new-array v1, v3, [Ljava/lang/Object;

    .line 15
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 16
    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->switchToSoftwareMode(Z)V

    goto :goto_0

    .line 17
    :cond_1
    const-string v0, "%s: immediate software failover requested."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method setCurrentCodec(I)V
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-eq v0, p1, :cond_0

    .line 164
    iput p1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    .line 166
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, Lfqh;

    invoke-direct {v0, p0, p1}, Lfqh;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;I)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 168
    :cond_0
    return-void
.end method

.method public setSourceId(I)V
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    if-ne v0, p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    new-instance v1, Lfpx;

    invoke-direct {v1, p0, p1}, Lfpx;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 187
    new-instance v0, Lfqi;

    invoke-direct {v0, p0, p1, p2}, Lfqi;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 188
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 191
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method switchToSoftwareMode(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 20
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-ne v0, v3, :cond_1

    if-eqz p1, :cond_1

    .line 21
    const-string v0, "%s: Switching to SW H.264 MediaCodec decoders."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->forceSoftwareMediaCodecUsage:Z

    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->resetDecoderRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->codecType:I

    if-nez v0, :cond_0

    .line 25
    const-string v0, "%s: Switching to SW VP8 decoders."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentCapabilitiesSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_2

    .line 27
    new-instance v0, Lfpv;

    invoke-direct {v0, p0}, Lfpv;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;)V

    invoke-static {v0}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 28
    :cond_2
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->failed:Z

    .line 29
    invoke-direct {p0, v4}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->stopDecoder(Z)V

    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderManager:Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->ssrc:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/hangouts/video/internal/DecoderManager;->notifyHardwareFailed(I)Z

    goto :goto_0
.end method

.method protected updateOutputFormat(Landroid/media/MediaFormat;)V
    .locals 14

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 397
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->mediaCodec:Landroid/media/MediaCodec;

    .line 398
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    new-instance v7, Lfwe;

    invoke-direct {v7}, Lfwe;-><init>()V

    .line 401
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->isHardwareDecoder:Z

    if-nez v0, :cond_3

    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 402
    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 403
    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 406
    :goto_0
    invoke-virtual {v7, v1, v0}, Lfwe;->a(II)Lfwe;

    .line 407
    const-string v2, "crop-left"

    invoke-virtual {p1, v2}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 408
    const-string v2, "crop-left"

    invoke-virtual {p1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 410
    :goto_1
    const-string v4, "crop-top"

    invoke-virtual {p1, v4}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 411
    const-string v4, "crop-top"

    invoke-virtual {p1, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    .line 413
    :goto_2
    const-string v5, "crop-right"

    invoke-virtual {p1, v5}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 414
    const-string v5, "crop-right"

    invoke-virtual {p1, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    .line 416
    :goto_3
    const-string v6, "crop-bottom"

    invoke-virtual {p1, v6}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 417
    const-string v6, "crop-bottom"

    invoke-virtual {p1, v6}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v6

    .line 419
    :goto_4
    if-ltz v2, :cond_0

    if-ge v2, v1, :cond_0

    if-ltz v4, :cond_0

    if-ge v4, v0, :cond_0

    if-ltz v5, :cond_0

    if-ge v5, v1, :cond_0

    if-ltz v6, :cond_0

    if-lt v6, v0, :cond_9

    .line 420
    :cond_0
    const-string v8, "%s: Unexpected crop values: width: %d height: %d crop-left: %d crop-top: %d crop-right: %d crop-bottom: %d"

    const/4 v9, 0x7

    new-array v9, v9, [Ljava/lang/Object;

    .line 421
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v3

    .line 422
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    .line 423
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    const/4 v10, 0x3

    .line 424
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v2, 0x4

    .line 425
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v2

    const/4 v2, 0x5

    .line 426
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v2

    const/4 v2, 0x6

    .line 427
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v2

    .line 428
    invoke-static {v8, v9}, Lfvh;->loge(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 431
    add-int/lit8 v5, v1, -0x1

    .line 432
    add-int/lit8 v2, v0, -0x1

    move v4, v3

    move v6, v3

    .line 433
    :goto_5
    if-gtz v5, :cond_1

    if-lez v2, :cond_8

    .line 434
    :cond_1
    new-instance v8, Landroid/graphics/RectF;

    int-to-float v6, v6

    int-to-float v9, v1

    div-float/2addr v6, v9

    int-to-float v4, v4

    int-to-float v9, v0

    div-float/2addr v4, v9

    add-int/lit8 v9, v1, -0x1

    sub-int v5, v9, v5

    int-to-float v5, v5

    int-to-float v1, v1

    div-float v1, v5, v1

    add-int/lit8 v5, v0, -0x1

    sub-int v2, v5, v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-direct {v8, v6, v4, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v7, v8}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    .line 436
    :goto_6
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    invoke-virtual {v7, v0}, Lfwe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 437
    const-string v0, "%s: MediaCodec updating output format: %s"

    new-array v1, v12, [Ljava/lang/Object;

    .line 438
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getDebugName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v7, v1, v11

    .line 439
    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    iput-object v7, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->currentOutputFormat:Lfwe;

    .line 441
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->decoderUpdateCallback:Lfqk;

    if-eqz v0, :cond_2

    .line 442
    invoke-virtual {v7}, Lfwe;->a()Lfwe;

    move-result-object v0

    .line 443
    new-instance v1, Lfpz;

    invoke-direct {v1, p0, v0}, Lfpz;-><init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Lfwe;)V

    invoke-static {v1}, Lhcw;->a(Ljava/lang/Runnable;)V

    .line 444
    :cond_2
    return-void

    .line 404
    :cond_3
    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 405
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    :cond_4
    move v2, v3

    .line 409
    goto/16 :goto_1

    :cond_5
    move v4, v3

    .line 412
    goto/16 :goto_2

    .line 415
    :cond_6
    add-int/lit8 v5, v1, -0x1

    goto/16 :goto_3

    .line 418
    :cond_7
    add-int/lit8 v6, v0, -0x1

    goto/16 :goto_4

    .line 435
    :cond_8
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {v7, v0}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    goto :goto_6

    :cond_9
    move v13, v6

    move v6, v2

    move v2, v13

    goto :goto_5
.end method

.method public abstract usedInputBuffer(I)V
.end method
