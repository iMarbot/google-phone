.class public final Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;
.super Lcom/google/android/libraries/hangouts/video/internal/Renderer;
.source "PG"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;
    }
.end annotation


# instance fields
.field public currentOutputTexture:I

.field public final frameTransform:[F

.field public final glManager:Lfpc;

.field public hardwareOutputTexture:I

.field public hardwareSurface:Landroid/view/Surface;

.field public hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field public final hasHardwareFramesAvailable:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public isOutputTextureExternal:Z

.field public final mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

.field public final remoteVideoSource:Lfrn;

.field public final shouldRenderInHardware:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public softwareOutputTexture:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;Lfpc;Lfrn;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/Renderer;-><init>(I)V

    .line 2
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->frameTransform:[F

    .line 3
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hasHardwareFramesAvailable:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->shouldRenderInHardware:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 5
    iput-object p3, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->remoteVideoSource:Lfrn;

    .line 6
    iput-object p1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    .line 7
    iput-object p2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->glManager:Lfpc;

    .line 8
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->frameTransform:[F

    invoke-static {v0}, Lfwm;->a([F)V

    .line 9
    return-void
.end method

.method private final initializeHardwareSurface()V
    .locals 2

    .prologue
    .line 15
    const-string v0, "Created intermediate texture twice"

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-static {v0, v1}, Lfmw;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 16
    invoke-static {}, Lfmk;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareOutputTexture:I

    .line 17
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareOutputTexture:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 18
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p0}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 19
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurface:Landroid/view/Surface;

    .line 20
    return-void
.end method

.method private final releaseHardwareSurface()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->glManager:Lfpc;

    new-instance v1, Lfpl;

    invoke-direct {v1, p0}, Lfpl;-><init>(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 29
    return-void
.end method

.method private final setRendererFrameOutputDataFromDecoder(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-nez v0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->getCurrentOutputFormat()Lfwe;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 36
    iget v1, v0, Lfwe;->a:I

    .line 37
    iput v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameWidth:I

    .line 39
    iget v1, v0, Lfwe;->b:I

    .line 40
    iput v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameHeight:I

    .line 42
    iget-object v1, v0, Lfwe;->f:Landroid/graphics/RectF;

    .line 44
    if-nez v1, :cond_2

    .line 45
    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropLeft:I

    .line 46
    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropTop:I

    .line 47
    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropRight:I

    .line 48
    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropBottom:I

    goto :goto_0

    .line 49
    :cond_2
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 50
    iget v3, v0, Lfwe;->a:I

    .line 51
    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropLeft:I

    .line 52
    iget v2, v1, Landroid/graphics/RectF;->top:F

    .line 53
    iget v3, v0, Lfwe;->b:I

    .line 54
    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropTop:I

    .line 55
    iget v2, v1, Landroid/graphics/RectF;->right:F

    .line 56
    iget v3, v0, Lfwe;->a:I

    .line 57
    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropRight:I

    .line 58
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 59
    iget v0, v0, Lfwe;->b:I

    .line 60
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropBottom:I

    goto :goto_0
.end method


# virtual methods
.method public final drawTexture(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isOutputTextureExternal:Z

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hasHardwareFramesAvailable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    .line 65
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->shouldRenderInHardware:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    iget v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareOutputTexture:I

    iput v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->currentOutputTexture:I

    .line 69
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isOutputTextureExternal:Z

    .line 70
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v2, :cond_1

    .line 71
    iget-object v2, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v3, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->frameTransform:[F

    invoke-virtual {v2, v3}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 72
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->setRendererFrameOutputDataFromDecoder(Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;)V

    .line 73
    iput-boolean v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    .line 83
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isOutputTextureExternal:Z

    if-eq v0, v1, :cond_2

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->remoteVideoSource:Lfrn;

    invoke-virtual {v0}, Lfrn;->updateViewRequest()V

    .line 85
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->updatedTexture:Z

    return v0

    .line 74
    :cond_3
    iget v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->softwareOutputTexture:I

    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->currentOutputTexture:I

    .line 75
    if-eqz v0, :cond_4

    .line 76
    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->frameTransform:[F

    invoke-static {v1}, Lfwm;->a([F)V

    .line 77
    :cond_4
    iput-boolean v3, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isOutputTextureExternal:Z

    .line 78
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->nativeRenderFrame(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    iput v3, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropLeft:I

    .line 80
    iput v3, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropTop:I

    .line 81
    iget v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameWidth:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropRight:I

    .line 82
    iget v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->frameHeight:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p1, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer$RendererFrameOutputData;->cropBottom:I

    goto :goto_0
.end method

.method public final getHardwareSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurface:Landroid/view/Surface;

    return-object v0
.end method

.method public final getOutputTextureName()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->currentOutputTexture:I

    return v0
.end method

.method public final getTransformMatrix()[F
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->frameTransform:[F

    return-object v0
.end method

.method public final isExternalTexture()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->isOutputTextureExternal:Z

    return v0
.end method

.method public final synthetic lambda$releaseHardwareSurface$0$GlRemoteRenderer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 95
    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 97
    iput-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurface:Landroid/view/Surface;

    .line 98
    iget v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareOutputTexture:I

    .line 99
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareOutputTexture:I

    .line 100
    invoke-static {v0}, Lfmk;->c(I)V

    .line 101
    :cond_0
    return-void
.end method

.method public final notifyFrameReceived()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->shouldRenderInHardware:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->glManager:Lfpc;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->remoteVideoSource:Lfrn;

    invoke-virtual {v0, v1}, Lfpc;->notifyFrame(Lfsm;)V

    .line 92
    return-void
.end method

.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hasHardwareFramesAvailable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->shouldRenderInHardware:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 88
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->glManager:Lfpc;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->remoteVideoSource:Lfrn;

    invoke-virtual {v0, v1}, Lfpc;->notifyFrame(Lfsm;)V

    .line 89
    return-void
.end method

.method public final onInitializeGLContext()V
    .locals 3

    .prologue
    .line 10
    const-string v0, "sub_outtex"

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->nativeGetIntParam(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->softwareOutputTexture:I

    .line 11
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_0

    .line 12
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->initializeHardwareSurface()V

    .line 13
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    iget-object v1, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->hardwareSurface:Landroid/view/Surface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 14
    :cond_0
    return-void
.end method

.method public final onRelease()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->mediaCodecDecoder:Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/libraries/hangouts/video/internal/MediaCodecDecoder;->setSurface(Landroid/view/Surface;Ljava/lang/Runnable;)V

    .line 26
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/hangouts/video/internal/GlRemoteRenderer;->releaseHardwareSurface()V

    .line 27
    return-void
.end method
