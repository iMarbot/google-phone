.class public final Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;
.super Landroid/preference/PreferenceFragment;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 3
    const v0, 0x7f08000b

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->addPreferencesFromResource(I)V

    .line 5
    const v0, 0x7f110297

    .line 6
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    .line 7
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setPersistent(Z)V

    .line 8
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lfho;->f(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 9
    new-instance v2, Lfhr;

    invoke-direct {v2, p0}, Lfhr;-><init>(Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;)V

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 10
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getActivity()Landroid/app/Activity;

    .line 11
    const-string v2, "is_request_call_feedback_allowed"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 12
    if-nez v2, :cond_0

    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 15
    :cond_0
    const v0, 0x7f1103ca

    .line 16
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    .line 17
    const v2, 0x7f1103cb

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 19
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lfho;->e(Landroid/content/Context;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 22
    :goto_0
    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 23
    new-instance v1, Lfhs;

    invoke-direct {v1, p0}, Lfhs;-><init>(Lcom/google/android/libraries/dialer/voip/preferences/ui/WifiCallingSettingsActivity$a;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 24
    return-void

    .line 20
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 19
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
