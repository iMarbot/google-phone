.class final Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;
.super Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method private final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->C(Landroid/content/Context;)V

    .line 16
    invoke-static {p1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x47

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialerSettingsService.setNovaAccountImpl, accountName: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 17
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lfho;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lfho;->a(Landroid/content/Context;Z)V

    .line 20
    return-void
.end method


# virtual methods
.method public final getVersion()I
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a:Landroid/content/Context;

    invoke-static {v0}, Lfmd;->C(Landroid/content/Context;)V

    .line 5
    const/4 v0, 0x2

    return v0
.end method

.method public final setNovaAccountInfo(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 6
    const-string v0, "DialerSettingsService.setNovaAccountInfo"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a(Ljava/lang/String;Z)V

    .line 8
    return-void
.end method

.method public final setNovaAccountInfoV2(Ljava/lang/String;ZLcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 9
    const-string v0, "DialerSettingsService.setNovaAccountInfoV2"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;->a(Ljava/lang/String;Z)V

    .line 11
    const-string v0, "DialerSettingsService.setNovaAccountInfoV2, invoking callback"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    invoke-interface {p3}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;->novaAccountInfoSet()V

    .line 13
    return-void
.end method
