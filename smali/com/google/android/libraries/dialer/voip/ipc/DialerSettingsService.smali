.class public Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService;
.super Landroid/app/Service;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    const-string v0, "DialerSettingsService.onBind"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8
    const-string v0, "DialerSettingsServiceStub.onBind, Unknown binding action; ignoring"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    const/4 v0, 0x0

    .line 10
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService;->a:Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 2
    const-string v0, "DialerSettingsService.onCreate"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 4
    new-instance v0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/dialer/voip/ipc/DialerSettingsService;->a:Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;

    .line 5
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 11
    const-string v0, "DialerSettingsService.onDestroy"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 13
    return-void
.end method
