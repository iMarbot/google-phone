.class public Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;
.super Lcom/google/android/aidl/BaseProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Proxy"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 1
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsService"

    invoke-direct {p0, p1, v0}, Lcom/google/android/aidl/BaseProxy;-><init>(Landroid/os/IBinder;Ljava/lang/String;)V

    .line 2
    return-void
.end method


# virtual methods
.method public getVersion()I
    .locals 2

    .prologue
    .line 3
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 4
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->transactAndReadException(ILandroid/os/Parcel;)Landroid/os/Parcel;

    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 6
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 7
    return v1
.end method

.method public setNovaAccountInfo(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 10
    invoke-static {v0, p2}, Ldii;->a(Landroid/os/Parcel;Z)V

    .line 11
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->transactOneway(ILandroid/os/Parcel;)V

    .line 12
    return-void
.end method

.method public setNovaAccountInfoV2(Ljava/lang/String;ZLcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;)V
    .locals 2

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->obtainAndWriteInterfaceToken()Landroid/os/Parcel;

    move-result-object v0

    .line 14
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 15
    invoke-static {v0, p2}, Ldii;->a(Landroid/os/Parcel;Z)V

    .line 16
    invoke-static {v0, p3}, Ldii;->a(Landroid/os/Parcel;Landroid/os/IInterface;)V

    .line 17
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;->transactOneway(ILandroid/os/Parcel;)V

    .line 18
    return-void
.end method
