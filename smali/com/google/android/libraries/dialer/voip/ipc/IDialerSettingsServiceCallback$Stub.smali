.class public abstract Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_novaAccountInfoSet:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsServiceCallback"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsServiceCallback"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15
    :goto_0
    return v0

    .line 12
    :cond_0
    if-ne p1, v0, :cond_1

    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub;->novaAccountInfoSet()V

    goto :goto_0

    .line 15
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
