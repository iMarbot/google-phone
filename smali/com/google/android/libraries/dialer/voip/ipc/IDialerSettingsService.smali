.class public interface abstract Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getVersion()I
.end method

.method public abstract setNovaAccountInfo(Ljava/lang/String;Z)V
.end method

.method public abstract setNovaAccountInfoV2(Ljava/lang/String;ZLcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;)V
.end method
