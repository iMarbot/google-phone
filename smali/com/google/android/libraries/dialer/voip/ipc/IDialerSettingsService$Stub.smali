.class public abstract Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;
.super Lcom/google/android/aidl/BaseStub;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;
    }
.end annotation


# static fields
.field public static final TRANSACTION_getVersion:I = 0x1

.field public static final TRANSACTION_setNovaAccountInfo:I = 0x2

.field public static final TRANSACTION_setNovaAccountInfoV2:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/android/aidl/BaseStub;-><init>()V

    .line 2
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 3
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;
    .locals 2

    .prologue
    .line 4
    if-nez p0, :cond_0

    .line 5
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    const-string v0, "com.google.android.libraries.dialer.voip.ipc.IDialerSettingsService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;

    if-eqz v1, :cond_1

    .line 8
    check-cast v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService;

    goto :goto_0

    .line 9
    :cond_1
    new-instance v0, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;->routeToSuperOrEnforceInterface(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    :goto_0
    return v0

    .line 12
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 26
    const/4 v0, 0x0

    goto :goto_0

    .line 13
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;->getVersion()I

    move-result v1

    .line 14
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 15
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 17
    :pswitch_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 18
    invoke-static {p2}, Ldii;->a(Landroid/os/Parcel;)Z

    move-result v2

    .line 19
    invoke-virtual {p0, v1, v2}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;->setNovaAccountInfo(Ljava/lang/String;Z)V

    goto :goto_0

    .line 21
    :pswitch_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 22
    invoke-static {p2}, Ldii;->a(Landroid/os/Parcel;)Z

    move-result v2

    .line 23
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;

    move-result-object v3

    .line 24
    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsService$Stub;->setNovaAccountInfoV2(Ljava/lang/String;ZLcom/google/android/libraries/dialer/voip/ipc/IDialerSettingsServiceCallback;)V

    goto :goto_0

    .line 12
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
