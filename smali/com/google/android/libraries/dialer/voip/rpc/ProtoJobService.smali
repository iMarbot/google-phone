.class public Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;
.super Landroid/app/job/JobService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/PersistableBundle;)V
    .locals 8

    .prologue
    const/16 v3, 0x2710

    const/4 v7, 0x1

    .line 2
    const-class v0, Landroid/app/job/JobScheduler;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 4
    invoke-virtual {v0}, Landroid/app/job/JobScheduler;->getAllPendingJobs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/job/JobInfo;

    .line 5
    invoke-virtual {v1}, Landroid/app/job/JobInfo;->getId()I

    move-result v5

    if-lt v5, v3, :cond_1

    .line 6
    invoke-virtual {v1}, Landroid/app/job/JobInfo;->getId()I

    move-result v5

    const/16 v6, 0x2774

    if-ge v5, v6, :cond_1

    .line 7
    invoke-virtual {v1}, Landroid/app/job/JobInfo;->getId()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v1, v2

    :goto_1
    move v2, v1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .line 10
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11
    new-instance v3, Landroid/app/job/JobInfo$Builder;

    invoke-direct {v3, v1, v2}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 13
    const-string v2, "proto_upload_retry_backoff_millis"

    const-wide/16 v4, 0x7d0

    invoke-static {v2, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v4

    .line 16
    invoke-virtual {v3, p1}, Landroid/app/job/JobInfo$Builder;->setExtras(Landroid/os/PersistableBundle;)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 17
    invoke-virtual {v2, v7}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 18
    invoke-virtual {v2, v4, v5, v7}, Landroid/app/job/JobInfo$Builder;->setBackoffCriteria(JI)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 19
    invoke-virtual {v2, v7}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v2

    .line 20
    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v2

    .line 22
    invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 23
    const-string v0, "ProtoJobService.scheduleJob, job %d scheduled."

    new-array v2, v7, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    return-void

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 25
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v9

    .line 27
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v1

    .line 28
    const-string v0, "proto_wrapper_type_key"

    invoke-virtual {v1, v0, v10}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 29
    new-instance v0, Lfic;

    new-instance v2, Lfid;

    const-string v3, "account_name_key"

    .line 30
    invoke-virtual {v1, v3}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lfid;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v3, "proto_key_key"

    .line 31
    invoke-virtual {v1, v3}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "apiary_uri_key"

    .line 32
    invoke-virtual {v1, v4}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "path_key"

    .line 33
    invoke-virtual {v1, v5}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "proto_key"

    .line 34
    invoke-virtual {v1, v6}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Lfic;->a(Ljava/lang/String;I)Lhfz;

    move-result-object v6

    const-string v8, "timeout_key"

    .line 35
    invoke-virtual {v1, v8, v10}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lfic;-><init>(Landroid/content/Context;Lfid;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)V

    .line 36
    invoke-virtual {v9, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lfia;

    invoke-direct {v1, p0, p1}, Lfia;-><init>(Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;Landroid/app/job/JobParameters;)V

    .line 37
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Lfib;

    invoke-direct {v1, p0, p1}, Lfib;-><init>(Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;Landroid/app/job/JobParameters;)V

    .line 38
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 40
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
