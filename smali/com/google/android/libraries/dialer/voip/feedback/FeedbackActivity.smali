.class public Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;
.super Lit;
.source "PG"

# interfaces
.implements Lfgq;


# instance fields
.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lit;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lfgs;Landroid/os/Bundle;II)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "call_info_psd"

    .line 3
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "rating"

    .line 4
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notification_id"

    .line 5
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "vnd.hangouts"

    const/16 v2, 0x11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unique"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 6
    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 8
    invoke-virtual {p1}, Lfgs;->getSerializedSize()I

    move-result v1

    new-array v1, v1, [B

    .line 10
    const/4 v2, 0x0

    :try_start_0
    array-length v3, v1

    invoke-static {v1, v2, v3}, Lhfq;->a([BII)Lhfq;

    move-result-object v2

    .line 11
    invoke-virtual {p1, v2}, Lfgs;->writeTo(Lhfq;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    const-string v2, "args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 16
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, p4, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 13
    :catch_0
    move-exception v0

    .line 14
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method

.method private final a(I)V
    .locals 8

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->e()Lfgs;

    move-result-object v1

    .line 28
    new-instance v2, Lgpn;

    invoke-direct {v2}, Lgpn;-><init>()V

    .line 29
    const/16 v0, 0x3b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lgpn;->logSource:Ljava/lang/Integer;

    .line 30
    iget-object v0, v1, Lfgs;->f:Ljava/lang/String;

    iput-object v0, v2, Lgpn;->participantLogId:Ljava/lang/String;

    .line 31
    new-instance v0, Lgjf;

    invoke-direct {v0}, Lgjf;-><init>()V

    iput-object v0, v2, Lgpn;->callSurveyLogEntry:Lgjf;

    .line 32
    iget-object v0, v1, Lfgs;->g:Ljava/lang/String;

    iput-object v0, v2, Lgpn;->localSessionId:Ljava/lang/String;

    .line 33
    iget-object v3, v2, Lgpn;->callSurveyLogEntry:Lgjf;

    .line 34
    iget-object v0, v1, Lfgs;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lfgs;->g:Ljava/lang/String;

    :goto_0
    iput-object v0, v3, Lgjf;->a:Ljava/lang/String;

    .line 35
    iget-object v0, v2, Lgpn;->callSurveyLogEntry:Lgjf;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lgjf;->b:Ljava/lang/Integer;

    .line 36
    new-instance v4, Lgot;

    invoke-direct {v4}, Lgot;-><init>()V

    .line 37
    iput-object v2, v4, Lgot;->logData:Lgpn;

    .line 38
    iget-object v0, v4, Lgot;->logData:Lgpn;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lgpn;->clientSentTimestampMsec:Ljava/lang/Long;

    .line 40
    invoke-static {p0}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-static {p0}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v6

    .line 42
    invoke-static {p0, v0, v6, v7}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;J)Lgls;

    move-result-object v0

    iput-object v0, v4, Lgot;->requestHeader:Lgls;

    .line 43
    iget-object v0, v2, Lgpn;->callPerfLogEntry:Lgit;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, v2, Lgpn;->callPerfLogEntry:Lgit;

    iget-object v0, v0, Lgit;->a:Ljava/lang/String;

    iput-object v0, v4, Lgot;->sessionId:Ljava/lang/String;

    .line 45
    :cond_0
    const-string v0, "media_session_log_request_key"

    iget-object v1, v1, Lfgs;->a:Ljava/lang/String;

    .line 46
    invoke-static {p0}, Lfmd;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_sessions/log"

    const/4 v5, 0x1

    .line 47
    invoke-static {p0}, Lfmd;->l(Landroid/content/Context;)I

    move-result v6

    .line 48
    invoke-static/range {v0 .. v6}, Lfic;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhfz;II)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 49
    invoke-static {p0, v0}, Lcom/google/android/libraries/dialer/voip/rpc/ProtoJobService;->a(Landroid/content/Context;Landroid/os/PersistableBundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->finish()V

    .line 51
    return-void

    .line 34
    :cond_1
    iget-object v0, v1, Lfgs;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method private final b(Z)V
    .locals 5

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->e()Lfgs;

    move-result-object v0

    .line 114
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->f()I

    move-result v1

    iget-boolean v0, v0, Lfgs;->d:Z

    .line 115
    new-instance v2, Lfgr;

    invoke-direct {v2}, Lfgr;-><init>()V

    .line 116
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 117
    const-string v4, "rating"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v1, "should_show_audio_issues"

    invoke-virtual {v3, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    const-string v1, "was_incoming"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    invoke-virtual {v2, v3}, Lfgr;->f(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->c()Lja;

    move-result-object v1

    .line 125
    if-eqz p1, :cond_0

    .line 126
    const-string v0, "audio_issue_chooser"

    .line 128
    :goto_0
    invoke-virtual {v2, v1, v0}, Lfgr;->a(Lja;Ljava/lang/String;)V

    .line 129
    return-void

    .line 127
    :cond_0
    const-string v0, "call_issue_chooser"

    goto :goto_0
.end method

.method private final e()Lfgs;
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "args"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 131
    :try_start_0
    new-instance v1, Lfgs;

    invoke-direct {v1}, Lfgs;-><init>()V

    invoke-static {v1, v0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lfgs;
    :try_end_0
    .catch Lhfy; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    invoke-virtual {v0}, Lhfy;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->b(Ljava/lang/String;)Ljava/lang/AssertionError;

    move-result-object v0

    throw v0
.end method

.method private final f()I
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "rating"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 59
    const-string v0, "FeedbackActivity.onAudioIssueSelected"

    const-string v1, "sending feedback"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->f:Ljava/lang/String;

    .line 61
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->e()Lfgs;

    move-result-object v0

    .line 62
    iget-boolean v2, v0, Lfgs;->e:Z

    if-eqz v2, :cond_0

    .line 63
    const-string v0, "[HANDOFF] "

    .line 75
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 80
    :goto_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 84
    :goto_2
    const-string v1, "FeedbackActivity.sendFeedback"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->e()Lfgs;

    move-result-object v1

    .line 86
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "call_info_psd"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 88
    new-instance v3, Lfgm;

    invoke-direct {v3, p0}, Lfgm;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-static {}, Lfgk;->a()Lfgl;

    move-result-object v4

    new-instance v5, Lfgp;

    .line 93
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lfgp;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-static {v5}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-virtual {v4}, Lfgl;->a()Lfgo;

    move-result-object v4

    .line 97
    invoke-interface {v4}, Lfgo;->e()Lfli;

    move-result-object v4

    .line 99
    invoke-virtual {v4, v2}, Lfli;->a(Landroid/os/Bundle;)Lfli;

    move-result-object v2

    const-string v4, "rating"

    .line 100
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lfli;->a(Ljava/lang/String;Ljava/lang/String;)Lfli;

    move-result-object v2

    .line 101
    invoke-virtual {v2, v0}, Lfli;->b(Ljava/lang/String;)Lfli;

    move-result-object v0

    const-string v2, "com.google.android.talk.telephony"

    .line 102
    invoke-virtual {v0, v2}, Lfli;->c(Ljava/lang/String;)Lfli;

    move-result-object v0

    iget-object v1, v1, Lfgs;->a:Ljava/lang/String;

    .line 103
    invoke-virtual {v0, v1}, Lfli;->a(Ljava/lang/String;)Lfli;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lfli;->a()Lflh;

    move-result-object v0

    .line 106
    iget-object v1, v3, Lfgm;->d:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->v(Landroid/content/Context;)J

    move-result-wide v4

    .line 107
    const-string v1, "FeedbackSender.sendFeedback"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2e

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "options: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", timeoutMillis: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v1, v2, v6}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    new-instance v1, Lfgn;

    invoke-direct {v1, v3, v4, v5, v0}, Lfgn;-><init>(Lfgm;JLflh;)V

    new-array v0, v8, [Ljava/lang/Void;

    .line 109
    invoke-virtual {v1, v0}, Lfgn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    invoke-direct {p0, v9}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(I)V

    .line 111
    return-void

    .line 64
    :cond_0
    iget-boolean v2, v0, Lfgs;->c:Z

    if-eqz v2, :cond_3

    .line 65
    iget v2, v0, Lfgs;->j:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 66
    iget-boolean v2, v0, Lfgs;->i:Z

    if-eqz v2, :cond_1

    .line 67
    const-string v2, "[WIFI + %s] "

    new-array v3, v9, [Ljava/lang/Object;

    iget v0, v0, Lfgs;->j:I

    .line 68
    invoke-static {v0}, Lfmd;->c(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    .line 69
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 70
    :cond_1
    const-string v2, "[%s] "

    new-array v3, v9, [Ljava/lang/Object;

    iget v0, v0, Lfgs;->j:I

    .line 71
    invoke-static {v0}, Lfmd;->c(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    .line 72
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 73
    :cond_2
    const-string v0, "[WIFI] "

    goto/16 :goto_0

    .line 74
    :cond_3
    const-string v0, "[CELL] "

    goto/16 :goto_0

    .line 77
    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object p1, v1

    .line 78
    goto/16 :goto_1

    .line 79
    :cond_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_1

    .line 82
    :cond_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    if-eqz p2, :cond_0

    .line 53
    const-string v0, "FeedbackActivity.onCallIssueSelected"

    const-string v1, "good call, closing"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(I)V

    .line 58
    :goto_0
    return-void

    .line 55
    :cond_0
    const-string v0, "FeedbackActivity.onCallIssueSelected"

    const-string v1, "showing audio issues"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iput-object p1, p0, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->f:Ljava/lang/String;

    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->b(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 17
    const-string v0, "FeedbackActivity.onCreate"

    const/4 v1, 0x0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    invoke-super {p0, p1}, Lit;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const-string v0, "telephony_call_feedback"

    .line 20
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notification_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 21
    invoke-static {p0, v0, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 22
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->f()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 23
    const-string v0, "FeedbackActivity.onCreate"

    const-string v1, "rated good, doing nothing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-direct {p0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->f()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->a(I)V

    .line 26
    :goto_0
    return-void

    .line 25
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/libraries/dialer/voip/feedback/FeedbackActivity;->b(Z)V

    goto :goto_0
.end method
