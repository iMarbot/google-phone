.class public Lcom/google/android/libraries/dialer/voip/controller/RegistrationJobService;
.super Landroid/app/job/JobService;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    .line 2
    const-string v0, "RegistrationJobService.onStartJob"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/controller/RegistrationJobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    new-instance v1, Lfgj;

    invoke-direct {v1, p0}, Lfgj;-><init>(Landroid/content/Context;)V

    .line 5
    invoke-virtual {v0, v1}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    new-instance v1, Lfgh;

    invoke-direct {v1, p0, p1}, Lfgh;-><init>(Lcom/google/android/libraries/dialer/voip/controller/RegistrationJobService;Landroid/app/job/JobParameters;)V

    .line 6
    invoke-interface {v0, v1}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    new-instance v1, Lfgi;

    invoke-direct {v1, p0, p1}, Lfgi;-><init>(Lcom/google/android/libraries/dialer/voip/controller/RegistrationJobService;Landroid/app/job/JobParameters;)V

    .line 7
    invoke-interface {v0, v1}, Lbdz;->a(Lbea;)Lbdz;

    move-result-object v0

    .line 8
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    .line 10
    invoke-static {p0}, Lffe;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 11
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 12
    const/4 v0, 0x1

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2

    .prologue
    .line 13
    const-string v0, "RegistrationJobService.onStopJob"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    const/4 v0, 0x1

    return v0
.end method
