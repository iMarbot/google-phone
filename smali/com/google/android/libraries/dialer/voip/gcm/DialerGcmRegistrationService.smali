.class public Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;
.super Lflr;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;,
        Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lflr;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2
    const-string v0, "DialerGcmRegistrationService.getGcmRegistrationIdSynchronously"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-static {p0}, Lfho;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 4
    if-eqz v0, :cond_0

    invoke-static {p0}, Lfho;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    :cond_0
    const-string v0, "DialerGcmRegistrationService.getGcmRegistrationIdSynchronously, requesting token"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 6
    const/4 v1, 0x0

    .line 7
    :try_start_0
    const-string v0, "GCM"

    .line 8
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->b(Landroid/content/Context;)Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;->d()Lflv;

    move-result-object v2

    const-string v3, "566865154279"

    invoke-virtual {v2, v3, v0}, Lflv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 9
    :try_start_1
    invoke-static {p0, v0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 13
    :cond_1
    :goto_0
    return-object v0

    .line 11
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 12
    :goto_1
    const-string v2, "RegistrationIntentService.getGcmRegistrationIdSynchronously, failed"

    invoke-static {v2, v1}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 11
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14
    const-string v0, "DialerGcmRegistrationService.setGcmRegistrationId. Got new GCM registration ID: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 15
    invoke-static {p1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 16
    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    invoke-static {p0, v3}, Lfho;->c(Landroid/content/Context;Z)V

    .line 18
    invoke-static {p0}, Lfmd;->I(Landroid/content/Context;)I

    move-result v0

    invoke-static {p0, v0}, Lfho;->b(Landroid/content/Context;I)V

    .line 19
    invoke-static {p0, p1}, Lfho;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method static b(Landroid/content/Context;)Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;
    .locals 3

    .prologue
    .line 31
    new-instance v1, Lfgu;

    .line 32
    invoke-direct {v1}, Lfgu;-><init>()V

    .line 33
    new-instance v0, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 34
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-static {v0}, Lio/grpc/internal/av;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    iput-object v0, v1, Lfgu;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 36
    iget-object v0, v1, Lfgu;->a:Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    const-class v1, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$b;

    .line 38
    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " must be set"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iget-object v0, v1, Lfgu;->b:Lfmc;

    if-nez v0, :cond_1

    .line 40
    new-instance v0, Lfmc;

    invoke-direct {v0}, Lfmc;-><init>()V

    iput-object v0, v1, Lfgu;->b:Lfmc;

    .line 41
    :cond_1
    new-instance v0, Lfgt;

    .line 42
    invoke-direct {v0, v1}, Lfgt;-><init>(Lfgu;)V

    .line 43
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 21
    const-string v1, "DialerGcmRegistrationService.onRunTask, registering for GCM"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->b(Landroid/content/Context;)Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;->a()Lflu;

    move-result-object v1

    .line 23
    :try_start_0
    const-string v2, "566865154279"

    invoke-virtual {v1, v2}, Lflu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {p0, v1}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    return v0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v1, "DialerGcmRegistrationService.onRunTask. Unable to register for GCM"

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Lfls;
    .locals 1

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->b(Landroid/content/Context;)Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;->e()Lfls;

    move-result-object v0

    return-object v0
.end method
