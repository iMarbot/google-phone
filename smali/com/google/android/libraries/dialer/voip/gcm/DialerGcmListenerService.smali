.class public Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmListenerService;
.super Lend;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lend;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 2
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialerGcmListenerService.onMessageReceived, from: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", data: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4
    const-string v0, "use_service_for_incoming_hangouts_call_processing"

    invoke-static {v0, v7}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    invoke-static {p0, p2}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 8
    :cond_1
    const-string v0, "BlockingIncomingHangoutsCallController.onReceiveGcmPushIntent"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 10
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 11
    new-instance v8, Lfcq;

    invoke-direct {v8, p0, v0}, Lfcq;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 12
    const-string v0, "BlockingIncomingHangoutsCallController.onPushNotification"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, v8, Lfcq;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 14
    const-string v0, "BlockingIncomingHangoutsCallController.onPushNotification, no account, ignoring."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :cond_2
    :goto_1
    const-string v0, "BlockingIncomingHangoutsCallController.cleanup"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object v0, v8, Lfcq;->c:Lfex;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, v8, Lfcq;->c:Lfex;

    invoke-virtual {v0}, Lfex;->a()V

    .line 87
    iput-object v9, v8, Lfcq;->c:Lfex;

    goto :goto_0

    .line 15
    :cond_3
    iget-object v0, v8, Lfcq;->a:Landroid/content/Context;

    invoke-static {v0}, Lfho;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 16
    const-string v0, "BlockingIncomingHangoutsCallController.onPushNotification, not enabled, ignoring."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 17
    :cond_4
    iget-object v0, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v1, v8, Lfcq;->b:Landroid/content/Intent;

    invoke-static {v0, v1}, Lfmd;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/util/Pair;

    move-result-object v1

    .line 18
    if-eqz v1, :cond_2

    .line 19
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_8

    .line 20
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_6

    .line 21
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    iget-object v0, v0, Lfga;->b:Ljava/lang/String;

    invoke-static {v0}, Lfds;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 22
    iget-object v2, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    invoke-static {v2, v0}, Lfmd;->d(Landroid/content/Context;Lfga;)Z

    goto :goto_1

    .line 23
    :cond_5
    const-string v0, "BlockingIncomingHangoutsCallController.onPushNotification, ignoring server cancel request for answered call."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 24
    :cond_6
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 25
    const/16 v1, 0x4d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "BlockingIncomingHangoutsCallController.sendRingDowngrade, reason: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    iget-object v1, v0, Lfga;->a:Ljava/lang/String;

    if-eqz v1, :cond_7

    move v1, v2

    :goto_2
    invoke-static {v1}, Lbdf;->a(Z)V

    .line 27
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v2, v0, Lfga;->a:Ljava/lang/String;

    iget-object v3, v0, Lfga;->b:Ljava/lang/String;

    iget-wide v4, v0, Lfga;->c:J

    invoke-static/range {v1 .. v6}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JI)V

    goto/16 :goto_1

    :cond_7
    move v1, v7

    .line 26
    goto :goto_2

    .line 29
    :cond_8
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    .line 30
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x41

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "BlockingIncomingHangoutsCallController.onIncomingInvite, invite: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iput-object v0, v8, Lfcq;->e:Lfga;

    .line 32
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->b(Landroid/content/Context;)Z

    .line 33
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lfmd;->e(Landroid/content/Context;Lfga;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34
    invoke-virtual {v8}, Lfcq;->a()V

    .line 36
    const-string v0, "BlockingIncomingHangoutsCallController.checkNetworkState"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 37
    iget-object v0, v8, Lfcq;->e:Lfga;

    iget-object v1, v8, Lfcq;->d:Lfez;

    iget-object v1, v1, Lfez;->c:Lffb;

    invoke-virtual {v1}, Lffb;->e()Lfgb;

    move-result-object v1

    iput-object v1, v0, Lfga;->f:Lfgb;

    .line 38
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v3, v8, Lfcq;->d:Lfez;

    .line 39
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 42
    invoke-static {v0, v9}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_9

    move v0, v2

    .line 43
    :goto_3
    invoke-static {v1, v3, v7, v0}, Lfmd;->a(Landroid/content/Context;Lfez;ZZ)Lfew;

    move-result-object v0

    .line 44
    iget-object v1, v8, Lfcq;->e:Lfga;

    iget-boolean v3, v0, Lfew;->b:Z

    iput-boolean v3, v1, Lfga;->g:Z

    .line 45
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v3, v8, Lfcq;->e:Lfga;

    invoke-static {v1, v3}, Lfmd;->b(Landroid/content/Context;Lfga;)V

    .line 46
    iget-boolean v0, v0, Lfew;->a:Z

    if-eqz v0, :cond_a

    .line 47
    invoke-virtual {v8}, Lfcq;->c()V

    goto/16 :goto_1

    :cond_9
    move v0, v7

    .line 42
    goto :goto_3

    .line 48
    :cond_a
    const-string v0, "BlockingIncomingHangoutsCallController.onNetworkSelectionStateFetched, waiting for PSTN call"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-virtual {v8}, Lfcq;->b()Z

    move-result v0

    .line 50
    if-nez v0, :cond_b

    .line 51
    const-string v0, "BlockingIncomingHangoutsCallController.checkNetworkState, not falling back to WiFi"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 52
    :cond_b
    iget-object v0, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v1, v8, Lfcq;->e:Lfga;

    invoke-static {v0, v1}, Lfmd;->c(Landroid/content/Context;Lfga;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 53
    const-string v0, "BlockingIncomingHangoutsCallController.checkNetworkState, invite was cancelled"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 55
    :cond_c
    iget-object v0, v8, Lfcq;->e:Lfga;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x41

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "BlockingIncomingHangoutsCallController.onFallbackToWifi, invite: "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v1, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v0, v8, Lfcq;->a:Landroid/content/Context;

    iget-object v3, v8, Lfcq;->e:Lfga;

    .line 58
    const/4 v4, -0x1

    .line 59
    invoke-static {v0, v7, v4}, Lfem;->a(Landroid/content/Context;II)Lfeo;

    move-result-object v4

    .line 60
    new-instance v5, Lffa;

    invoke-direct {v5}, Lffa;-><init>()V

    .line 63
    iput-object v4, v5, Lffa;->a:Lfeo;

    .line 66
    invoke-static {v0}, Lfft;->a(Landroid/content/Context;)Lffy;

    move-result-object v0

    .line 67
    iput-object v0, v5, Lffa;->b:Lffy;

    .line 69
    iget-object v0, v3, Lfga;->f:Lfgb;

    .line 70
    invoke-static {v0}, Lffb;->a(Lfgb;)Lffb;

    move-result-object v0

    .line 71
    iput-object v0, v5, Lffa;->c:Lffb;

    .line 73
    invoke-virtual {v5}, Lffa;->a()Lfez;

    move-result-object v3

    .line 75
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 78
    invoke-static {v0, v9}, Lfds;->a(Landroid/telecom/ConnectionService;Lfds;)Lfds;

    move-result-object v0

    if-eqz v0, :cond_d

    move v0, v2

    .line 79
    :goto_4
    invoke-static {v1, v3, v2, v0}, Lfmd;->a(Landroid/content/Context;Lfez;ZZ)Lfew;

    move-result-object v0

    .line 80
    iget-object v1, v8, Lfcq;->e:Lfga;

    iget-boolean v2, v0, Lfew;->b:Z

    iput-boolean v2, v1, Lfga;->g:Z

    .line 81
    iget-boolean v0, v0, Lfew;->a:Z

    if-eqz v0, :cond_2

    .line 82
    invoke-virtual {v8}, Lfcq;->c()V

    goto/16 :goto_1

    :cond_d
    move v0, v7

    .line 78
    goto :goto_4
.end method
