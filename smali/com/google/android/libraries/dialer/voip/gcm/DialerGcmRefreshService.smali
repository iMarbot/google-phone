.class public Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRefreshService;
.super Leoq;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Leoq;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2
    const-string v0, "DialerGcmRefreshService.onTokenRefresh"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lfho;->c(Landroid/content/Context;Z)V

    .line 5
    invoke-static {p0}, Lfho;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    invoke-static {p0}, Lfho;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7
    invoke-static {p0}, Lfmd;->I(Landroid/content/Context;)I

    move-result v0

    invoke-static {p0}, Lfho;->j(Landroid/content/Context;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 8
    :cond_0
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->b(Landroid/content/Context;)Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;

    move-result-object v0

    .line 10
    invoke-interface {v0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;->c()Lflx;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lflx;->a()Lflw;

    move-result-object v1

    const-class v2, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;

    .line 12
    invoke-virtual {v1, v2}, Lflw;->a(Ljava/lang/Class;)Lflw;

    move-result-object v1

    .line 14
    const-string v2, "gcm_registration_window_start_delay_secs"

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 15
    int-to-long v2, v2

    .line 17
    const-string v4, "gcm_registration_window_end_delay_secs"

    const-wide/16 v6, 0x3c

    invoke-static {v4, v6, v7}, Lfmd;->a(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    .line 18
    int-to-long v4, v4

    .line 19
    invoke-virtual {v1, v2, v3, v4, v5}, Lflw;->a(JJ)Lflw;

    move-result-object v1

    const-string v2, "DialerGcmRegistrationService"

    .line 20
    invoke-virtual {v1, v2}, Lflw;->a(Ljava/lang/String;)Lflw;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lflw;->a()Lfly;

    move-result-object v1

    .line 22
    invoke-interface {v0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService$a;->b()Lflq;

    move-result-object v0

    invoke-virtual {v0, v1}, Lflq;->a(Lfly;)V

    .line 23
    :cond_1
    return-void
.end method
