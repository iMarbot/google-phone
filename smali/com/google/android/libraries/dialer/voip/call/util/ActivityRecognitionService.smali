.class public Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;
.super Landroid/app/IntentService;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService$a;
    }
.end annotation


# instance fields
.field private a:Lfmu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const-string v0, "ActivityRecognitionService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 2
    new-instance v0, Lfer;

    .line 3
    invoke-direct {v0}, Lfer;-><init>()V

    .line 5
    iget-object v1, v0, Lfer;->a:Lfms;

    if-nez v1, :cond_0

    .line 6
    new-instance v1, Lfms;

    invoke-direct {v1}, Lfms;-><init>()V

    iput-object v1, v0, Lfer;->a:Lfms;

    .line 7
    :cond_0
    new-instance v1, Lfeq;

    .line 8
    invoke-direct {v1, v0}, Lfeq;-><init>(Lfer;)V

    .line 10
    invoke-interface {v1}, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService$a;->a()Lfmk;

    move-result-object v0

    invoke-virtual {v0}, Lfmk;->a()Lfmu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;->a:Lfmu;

    .line 11
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;->a:Lfmu;

    invoke-virtual {v0, p1}, Lfmu;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 13
    const-string v0, "ActivityRecognitionService.onHandleIntent, intent returned result, sending broadcast"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;->a:Lfmu;

    invoke-virtual {v0, p1}, Lfmu;->a(Landroid/content/Intent;)Lfmm;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lfmm;->a()Lfmn;

    move-result-object v0

    .line 17
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.libraries.dialer.voip.call.util.user_activity_action"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 18
    const-string v2, "com.google.android.libraries.dialer.voip.call.util.user_activity_type"

    invoke-interface {v0}, Lfmn;->b()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 19
    const-string v2, "com.google.android.libraries.dialer.voip.call.util.user_activity_confidence"

    invoke-interface {v0}, Lfmn;->a()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 21
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/dialer/voip/call/util/ActivityRecognitionService;->sendBroadcast(Landroid/content/Intent;)V

    .line 24
    :goto_0
    return-void

    .line 23
    :cond_0
    const-string v0, "ActivityRecognitionService.onHandleIntent, intent didn\'t return any activity recognition result."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
