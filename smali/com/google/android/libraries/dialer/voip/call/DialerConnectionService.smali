.class public Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;
.super Landroid/telecom/ConnectionService;
.source "PG"


# static fields
.field public static a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;


# instance fields
.field private b:Lfda;

.field private c:Lfhg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/telecom/ConnectionService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 2
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3
    new-instance v1, Landroid/telecom/PhoneAccountHandle;

    const-string v2, "hangouts"

    invoke-direct {v1, v0, v2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Landroid/telecom/Connection;)Landroid/telecom/RemoteConnection;
    .locals 1

    .prologue
    .line 124
    instance-of v0, p0, Lfdd;

    if-eqz v0, :cond_0

    .line 125
    check-cast p0, Lfdd;

    .line 127
    iget-object v0, p0, Lfdd;->e:Lfdb;

    .line 128
    instance-of v0, v0, Lfct;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lfdd;->e:Lfdb;

    .line 131
    check-cast v0, Lfct;

    .line 132
    iget-object v0, v0, Lfct;->d:Landroid/telecom/RemoteConnection;

    .line 138
    :goto_0
    return-object v0

    .line 134
    :cond_0
    instance-of v0, p0, Lfcy;

    if-eqz v0, :cond_1

    .line 135
    check-cast p0, Lfcy;

    .line 136
    iget-object v0, p0, Lfcy;->a:Landroid/telecom/RemoteConnection;

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/telecom/RemoteConnection;)V
    .locals 2

    .prologue
    .line 139
    if-eqz p0, :cond_0

    .line 140
    const-string v0, "DialerConnectionService.rejectCanceledCellularCall, rejecting call"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    invoke-virtual {p0}, Landroid/telecom/RemoteConnection;->reject()V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/telecom/RemoteConnection;)Landroid/telecom/Connection;
    .locals 3

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 120
    invoke-static {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/Connection;)Landroid/telecom/RemoteConnection;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 123
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lfhg;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    if-nez v0, :cond_0

    .line 193
    invoke-static {p0}, Lfmd;->D(Landroid/content/Context;)Lfhg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    return-object v0
.end method

.method public final a(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;Lfdd;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "DialerConnectionService.makeCircuitSwitchedOutgoingConnection, "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    iget-object v2, p3, Lfdd;->d:Lffd;

    .line 150
    iget-object v3, p3, Lfdd;->j:Lffb;

    .line 151
    invoke-static {p0, v2, v3}, Lfmd;->a(Landroid/content/Context;Lffd;Lffb;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    const-string v2, "outgoing_circuit_switched_anonymyzing_call_allowed"

    invoke-static {v2, v0}, Lfmd;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 154
    if-nez v2, :cond_1

    .line 155
    const-string v0, "DialerConnectionService.makeCircuitSwitchedOutgoingConnection, anonymizing disabled by config"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->createRemoteOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/RemoteConnection;

    move-result-object v0

    .line 190
    new-instance v2, Lfct;

    new-instance v3, Lfdr;

    invoke-direct {v3}, Lfdr;-><init>()V

    invoke-direct {v2, p0, v3, v0, v1}, Lfct;-><init>(Landroid/content/Context;Lfdr;Landroid/telecom/RemoteConnection;Z)V

    invoke-virtual {p3, v2}, Lfdd;->a(Lfdb;)V

    .line 191
    return-void

    .line 157
    :cond_1
    iget-object v2, p3, Lfdd;->j:Lffb;

    .line 159
    iget-object v2, v2, Lffb;->a:Ljava/lang/String;

    .line 160
    if-eqz v2, :cond_6

    .line 161
    const-string v3, "310260"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 162
    const/4 v0, 0x2

    .line 172
    :cond_2
    :goto_1
    invoke-static {v0}, Lfmd;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 173
    const-string v0, "DialerConnectionService.makeCircuitSwitchedOutgoingConnection, not on hutch, not anonymizing"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 163
    :cond_3
    const-string v3, "310120"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 165
    const-string v0, "311580"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 166
    const/4 v0, 0x3

    goto :goto_1

    .line 167
    :cond_4
    const-string v0, "23420"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 168
    const/4 v0, 0x4

    goto :goto_1

    .line 169
    :cond_5
    const-string v0, "45403"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 170
    const/4 v0, 0x5

    goto :goto_1

    :cond_6
    move v0, v1

    .line 171
    goto :goto_1

    .line 174
    :cond_7
    const-string v0, "DialerConnectionService.makeCircuitSwitchedOutgoingConnection, anonymizing number"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    iget-object v0, p3, Lfdd;->d:Lffd;

    .line 179
    invoke-virtual {v0}, Lffd;->a()Ljava/lang/String;

    move-result-object v0

    .line 181
    iget-object v2, p3, Lfdd;->j:Lffb;

    .line 183
    iget-object v2, v2, Lffb;->b:Ljava/lang/String;

    .line 184
    invoke-static {v0, v2}, Lffe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v2, "tel"

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 186
    new-instance v0, Landroid/telecom/ConnectionRequest;

    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v0, v3, v2, v4}, Landroid/telecom/ConnectionRequest;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/net/Uri;Landroid/os/Bundle;)V

    move-object p2, v0

    .line 187
    goto/16 :goto_0
.end method

.method public onConference(Landroid/telecom/Connection;Landroid/telecom/Connection;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 99
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x41

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DialerConnectionService.onConference, connection1: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " connection2: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    invoke-static {p1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/Connection;)Landroid/telecom/RemoteConnection;

    move-result-object v0

    .line 101
    invoke-static {p2}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/telecom/Connection;)Landroid/telecom/RemoteConnection;

    move-result-object v1

    .line 102
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 103
    const-string v2, "DialerConnectionService.onConference, creating conference for remote connections"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->conferenceRemoteConnections(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection;)V

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6f

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DialerConnectionService.onConference, failed to find remote connection, remoteConnection1: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " remoteConnection2: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 4
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onCreate()V

    .line 5
    sput-object p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 6
    return-void
.end method

.method public onCreateIncomingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v10, 0x4

    const/4 v8, 0x0

    .line 34
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialerConnectionService.onCreateIncomingConnection, request: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    new-instance v6, Lffb;

    invoke-direct {v6, p0, v8}, Lffb;-><init>(Landroid/content/Context;I)V

    .line 36
    new-instance v0, Lffd;

    .line 37
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x2

    .line 39
    iget-object v5, v6, Lffb;->b:Ljava/lang/String;

    move-object v1, p0

    .line 40
    invoke-direct/range {v0 .. v5}, Lffd;-><init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v1, Lfdd;

    move-object v2, p0

    move-object v3, p2

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lfdd;-><init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;Lffd;Lffb;)V

    .line 42
    invoke-static {p0}, Lffc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 44
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Landroid/os/Bundle;)Lfga;

    move-result-object v2

    .line 45
    if-eqz v2, :cond_1

    iget-object v0, v2, Lfga;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    .line 47
    :goto_0
    iget-object v3, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->b:Lfda;

    if-nez v3, :cond_0

    .line 48
    new-instance v3, Lfda;

    invoke-direct {v3, p0}, Lfda;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->b:Lfda;

    .line 49
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->b:Lfda;

    .line 50
    invoke-virtual {v3, p2, v0}, Lfda;->a(Landroid/telecom/ConnectionRequest;Z)Z

    move-result v3

    .line 53
    iget-object v5, v1, Lfdd;->d:Lffd;

    .line 55
    iget-object v6, v1, Lfdd;->j:Lffb;

    .line 56
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    .line 57
    invoke-static {p0, v5, v6, v9}, Lfmd;->a(Landroid/content/Context;Lffd;Lffb;Landroid/os/Bundle;)Z

    move-result v5

    .line 58
    if-eqz v0, :cond_6

    .line 59
    if-eqz v3, :cond_2

    .line 60
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, duplicate Hangouts call"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    new-instance v0, Landroid/telecom/DisconnectCause;

    invoke-direct {v0, v10}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 98
    :goto_1
    return-object v1

    :cond_1
    move v0, v8

    .line 45
    goto :goto_0

    .line 62
    :cond_2
    if-eqz v5, :cond_3

    .line 63
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, blocking spam Hangout call"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    new-instance v0, Landroid/telecom/DisconnectCause;

    invoke-direct {v0, v10}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 65
    :cond_3
    invoke-static {p0, v2}, Lfmd;->c(Landroid/content/Context;Lfga;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 66
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, missed Hangouts call"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 68
    :cond_4
    invoke-static {p0}, Lffc;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 69
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, no microphone permission"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    new-instance v0, Landroid/telecom/DisconnectCause;

    invoke-direct {v0, v10}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 71
    :cond_5
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, new Hangouts call"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v0, v2, Lfga;->f:Lfgb;

    invoke-static {v0}, Lffb;->a(Lfgb;)Lffb;

    move-result-object v0

    .line 73
    iput-object v0, v1, Lfdd;->j:Lffb;

    .line 74
    iget-boolean v0, v2, Lfga;->g:Z

    .line 75
    iput-boolean v0, v1, Lfdd;->h:Z

    .line 76
    new-instance v0, Lfds;

    new-instance v3, Lfdr;

    invoke-direct {v3}, Lfdr;-><init>()V

    invoke-direct {v0, p0, v3, v4, v2}, Lfds;-><init>(Landroid/content/Context;Lfdr;Ljava/lang/String;Lfga;)V

    invoke-virtual {v1, v0}, Lfdd;->a(Lfdb;)V

    goto :goto_1

    .line 78
    :cond_6
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->createRemoteIncomingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/RemoteConnection;

    move-result-object v0

    .line 79
    if-eqz v3, :cond_7

    .line 80
    const-string v2, "DialerConnectionService.onCreateIncomingConnection, duplicate cell call"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    invoke-static {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->b(Landroid/telecom/RemoteConnection;)V

    .line 82
    new-instance v0, Landroid/telecom/DisconnectCause;

    invoke-direct {v0, v10}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_1

    .line 83
    :cond_7
    if-eqz v5, :cond_8

    .line 84
    const-string v2, "DialerConnectionService.onCreateIncomingConnection, blocking spam cell call"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-static {v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->b(Landroid/telecom/RemoteConnection;)V

    .line 86
    new-instance v0, Landroid/telecom/DisconnectCause;

    invoke-direct {v0, v10}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto/16 :goto_1

    .line 87
    :cond_8
    const-string v2, "DialerConnectionService.onCreateIncomingConnection, new cell call"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "DialerConnectionService.updateNetworkStatus, "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    new-instance v2, Lfex;

    new-instance v3, Lfdh;

    invoke-direct {v3, v1}, Lfdh;-><init>(Lfdd;)V

    invoke-direct {v2, p0, v3}, Lfex;-><init>(Landroid/content/Context;Lfey;)V

    .line 91
    invoke-virtual {v2}, Lfex;->b()V

    .line 92
    invoke-static {p0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 93
    iput-object v2, v1, Lfdd;->f:Ljava/lang/String;

    .line 94
    new-instance v2, Lfct;

    new-instance v3, Lfdr;

    invoke-direct {v3}, Lfdr;-><init>()V

    invoke-direct {v2, p0, v3, v0, v7}, Lfct;-><init>(Landroid/content/Context;Lfdr;Landroid/telecom/RemoteConnection;Z)V

    invoke-virtual {v1, v2}, Lfdd;->a(Lfdb;)V

    goto/16 :goto_1

    .line 96
    :cond_9
    const-string v0, "DialerConnectionService.onCreateIncomingConnection, lack permissions"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/16 v2, 0xa

    invoke-direct {v0, v2}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto/16 :goto_1
.end method

.method public onCreateOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 14
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialerConnectionService.onCreateOutgoingConnection, request: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    new-instance v6, Lffb;

    invoke-direct {v6, p0, v7}, Lffb;-><init>(Landroid/content/Context;I)V

    .line 16
    new-instance v0, Lffd;

    .line 17
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    .line 18
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lffe;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 19
    invoke-static {p0, v1, v6}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Lffb;)Ljava/lang/String;

    move-result-object v4

    .line 21
    iget-object v5, v6, Lffb;->b:Ljava/lang/String;

    move-object v1, p0

    .line 22
    invoke-direct/range {v0 .. v5}, Lffd;-><init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V

    .line 23
    new-instance v1, Lfdd;

    const/4 v4, 0x0

    move-object v2, p0

    move-object v3, p2

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lfdd;-><init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;Lffd;Lffb;)V

    .line 24
    invoke-static {p0}, Lffc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "DialerConnectionService.makeOutgoingCall"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    new-instance v0, Lfdf;

    invoke-direct {v0, p0, v1, p1, p2}, Lfdf;-><init>(Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;Lfdd;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)V

    .line 28
    new-instance v2, Lfex;

    invoke-direct {v2, p0, v0}, Lfex;-><init>(Landroid/content/Context;Lfey;)V

    .line 29
    invoke-virtual {v2}, Lfex;->b()V

    .line 33
    :goto_0
    return-object v1

    .line 31
    :cond_0
    const-string v0, "DialerConnectionService.onCreateOutgoingConnection, lack permissions"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/16 v2, 0xa

    invoke-direct {v0, v2}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v1, v0}, Lfdd;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7
    const-string v0, "DialerConnectionService.onDestroy"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    invoke-interface {v0}, Lfhg;->b()V

    .line 10
    iput-object v2, p0, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->c:Lfhg;

    .line 11
    :cond_0
    sput-object v2, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a:Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;

    .line 12
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onDestroy()V

    .line 13
    return-void
.end method

.method public onRemoteConferenceAdded(Landroid/telecom/RemoteConference;)V
    .locals 3

    .prologue
    .line 107
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DialerConnectionService.onRemoteConferenceAdded, conference: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    new-instance v0, Lfcw;

    .line 110
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lfcw;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/RemoteConference;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V

    .line 111
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->addConference(Landroid/telecom/Conference;)V

    .line 112
    return-void
.end method

.method public onRemoteExistingConnectionAdded(Landroid/telecom/RemoteConnection;)V
    .locals 2

    .prologue
    .line 113
    const-string v0, "DialerConnectionService.onRemoteExistingConnectionAdded"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    .line 116
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->a(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    new-instance v1, Lfcy;

    invoke-direct {v1, p1, p0}, Lfcy;-><init>(Landroid/telecom/RemoteConnection;Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;)V

    .line 117
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/dialer/voip/call/DialerConnectionService;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V

    .line 118
    :cond_0
    return-void
.end method
