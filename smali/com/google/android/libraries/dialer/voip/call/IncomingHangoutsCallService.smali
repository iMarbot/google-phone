.class public Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;
.super Lfiv;
.source "PG"


# static fields
.field private static a:Ljava/lang/Object;

.field private static b:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lfiv;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 15
    sget-object v1, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 16
    :try_start_0
    const-string v0, "IncomingHangoutsCallService.ensureWakeLock, acquiring wakelock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 18
    const-string v0, "IncomingHangoutsCallService.ensureWakeLock, initializing wakelock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 20
    const/4 v2, 0x1

    const-string v3, "incoming_hangouts_call_wake_lock"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    .line 21
    :cond_0
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 22
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2
    const-string v0, "IncomingHangoutsCallService.onReceiveGcmPushIntent"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a(Landroid/content/Context;)V

    .line 4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 6
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 7
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 8
    const-string v0, "IncomingHangoutsCallService.onReceiveFallbackToWifiIntent"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a(Landroid/content/Context;)V

    .line 10
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 12
    const-string v1, "falling_back_to_wifi"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 13
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 14
    return-void
.end method

.method private static c()V
    .locals 3

    .prologue
    .line 23
    sget-object v1, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    const-string v0, "IncomingHangoutsCallService.releaseWakeLock, releasing wakelock"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-static {}, Lhcw;->b()V

    .line 26
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    invoke-static {v0}, Lbdf;->b(Z)V

    .line 27
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 28
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lfiv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {p0}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a(Landroid/content/Context;)V

    .line 81
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Lfiv;->b()V

    .line 84
    invoke-static {}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->c()V

    .line 85
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0}, Lfiv;->onCreate()V

    .line 87
    const-string v0, "IncomingHangoutsCallService.onCreate"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 29
    const-string v0, "IncomingHangoutsCallService.onStartCommand"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 32
    const-string v0, "falling_back_to_wifi"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    const-string v0, "IncomingHangoutsCallService.onFallbackToWifi"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lfmd;->a(Landroid/os/Bundle;)Lfga;

    move-result-object v0

    .line 36
    invoke-static {p0, v0}, Lfmd;->c(Landroid/content/Context;Lfga;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 37
    const-string v0, "IncomingHangoutsCallService.onFallbackToWifi, invite was cancelled"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_0
    :goto_1
    invoke-super {p0}, Lfiv;->b()V

    .line 74
    invoke-static {}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->c()V

    .line 77
    :goto_2
    invoke-static {}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->c()V

    .line 78
    const/4 v0, 0x2

    return v0

    :cond_1
    move v0, v3

    .line 30
    goto :goto_0

    .line 39
    :cond_2
    new-instance v1, Lfeb;

    invoke-direct {v1, p0}, Lfeb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p0, v0}, Lfeb;->a(Lfiv;Lfga;)V

    goto :goto_1

    .line 42
    :cond_3
    const-string v0, "IncomingHangoutsCallService.onPushNotification"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    invoke-static {p0}, Lfho;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44
    const-string v0, "IncomingHangoutsCallService.onPushNotification, no account, ignoring."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 46
    :cond_4
    invoke-static {p0}, Lfho;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 47
    const-string v0, "IncomingHangoutsCallService.onPushNotification, not enabled, ignoring."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 49
    :cond_5
    invoke-static {p0, p1}, Lfmd;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/util/Pair;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_0

    .line 51
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_8

    .line 52
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x5

    if-ne v0, v4, :cond_6

    .line 53
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    iget-object v0, v0, Lfga;->b:Ljava/lang/String;

    invoke-static {v0}, Lfds;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    invoke-static {p0, v0}, Lfmd;->d(Landroid/content/Context;Lfga;)Z

    goto :goto_1

    .line 55
    :cond_6
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 56
    iget-object v1, v0, Lfga;->a:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_3
    invoke-static {v2}, Lbdf;->a(Z)V

    .line 57
    iget-object v2, v0, Lfga;->a:Ljava/lang/String;

    iget-object v3, v0, Lfga;->b:Ljava/lang/String;

    iget-wide v4, v0, Lfga;->c:J

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JI)V

    goto/16 :goto_1

    :cond_7
    move v2, v3

    .line 56
    goto :goto_3

    .line 60
    :cond_8
    new-instance v2, Lfeb;

    invoke-direct {v2, p0}, Lfeb;-><init>(Landroid/content/Context;)V

    .line 61
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lfga;

    .line 62
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x39

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "IncomingHangoutsCallController.onIncomingInvite, invite: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    invoke-virtual {v2, p0, v0}, Lfeb;->b(Lfiv;Lfga;)V

    .line 64
    iget-object v1, v2, Lfeb;->a:Landroid/content/Context;

    invoke-static {v1}, Lfmd;->b(Landroid/content/Context;)Z

    .line 65
    iget-object v1, v2, Lfeb;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lfmd;->e(Landroid/content/Context;Lfga;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 67
    const-string v0, "IncomingHangoutsCallController.startAsyncNetworkStatusTask"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    new-instance v0, Lfex;

    iget-object v1, v2, Lfeb;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lfex;-><init>(Landroid/content/Context;Lfey;)V

    iput-object v0, v2, Lfeb;->b:Lfex;

    .line 69
    iget-object v0, v2, Lfeb;->b:Lfex;

    invoke-virtual {v0}, Lfex;->b()V

    goto/16 :goto_1

    .line 71
    :cond_9
    invoke-virtual {v2}, Lfeb;->b()V

    goto/16 :goto_1

    .line 76
    :cond_a
    const-string v0, "IncomingHangoutsCallService.onStartCommand, service is already stopped"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2
.end method
