.class public Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallFallback$AlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    const-string v0, "IncomingHangoutsCallFallback.AlarmReceiver.onReceive"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3
    const-string v0, "invite_info"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 4
    invoke-static {v0}, Lfmd;->a(Landroid/os/Bundle;)Lfga;

    move-result-object v1

    .line 5
    invoke-static {p1, v1}, Lfmd;->c(Landroid/content/Context;Lfga;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6
    const-string v0, "IncomingHangoutsCallFallback.AlarmReceiver.onReceive, invite was cancelled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 9
    :goto_0
    return-void

    .line 8
    :cond_0
    invoke-static {p1, v0}, Lcom/google/android/libraries/dialer/voip/call/IncomingHangoutsCallService;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method
