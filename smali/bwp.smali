.class public final enum Lbwp;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lbwp;

.field public static final enum b:Lbwp;

.field public static final enum c:Lbwp;

.field public static final enum d:Lbwp;

.field public static final enum e:Lbwp;

.field public static final enum f:Lbwp;

.field private static synthetic g:[Lbwp;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lbwp;

    const-string v1, "NO_CALLS"

    invoke-direct {v0, v1, v3}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->a:Lbwp;

    .line 5
    new-instance v0, Lbwp;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v4}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->b:Lbwp;

    .line 6
    new-instance v0, Lbwp;

    const-string v1, "INCALL"

    invoke-direct {v0, v1, v5}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->c:Lbwp;

    .line 7
    new-instance v0, Lbwp;

    const-string v1, "WAITING_FOR_ACCOUNT"

    invoke-direct {v0, v1, v6}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->d:Lbwp;

    .line 8
    new-instance v0, Lbwp;

    const-string v1, "PENDING_OUTGOING"

    invoke-direct {v0, v1, v7}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->e:Lbwp;

    .line 9
    new-instance v0, Lbwp;

    const-string v1, "OUTGOING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbwp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbwp;->f:Lbwp;

    .line 10
    const/4 v0, 0x6

    new-array v0, v0, [Lbwp;

    sget-object v1, Lbwp;->a:Lbwp;

    aput-object v1, v0, v3

    sget-object v1, Lbwp;->b:Lbwp;

    aput-object v1, v0, v4

    sget-object v1, Lbwp;->c:Lbwp;

    aput-object v1, v0, v5

    sget-object v1, Lbwp;->d:Lbwp;

    aput-object v1, v0, v6

    sget-object v1, Lbwp;->e:Lbwp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbwp;->f:Lbwp;

    aput-object v2, v0, v1

    sput-object v0, Lbwp;->g:[Lbwp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lbwp;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbwp;->g:[Lbwp;

    invoke-virtual {v0}, [Lbwp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbwp;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lbwp;->b:Lbwp;

    if-eq p0, v0, :cond_0

    sget-object v0, Lbwp;->f:Lbwp;

    if-eq p0, v0, :cond_0

    sget-object v0, Lbwp;->c:Lbwp;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
