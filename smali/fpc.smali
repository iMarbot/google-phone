.class public final Lfpc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final GL_THREAD_CHECK_PERIOD:J


# instance fields
.field public final callServiceCallbacks:Lfvt;

.field public final checkGlThreadActiveRunnable:Ljava/lang/Runnable;

.field public final eglContextWrapper:Lfus;

.field public final glThread:Lfph;

.field public volatile glThreadActive:Z

.field public final impressionReporter:Lfuv;

.field public final outputRenderers:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 75
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfpc;->GL_THREAD_CHECK_PERIOD:J

    return-void
.end method

.method public constructor <init>(Lfnp;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfpc;->outputRenderers:Ljava/util/Map;

    .line 3
    iput-boolean v2, p0, Lfpc;->glThreadActive:Z

    .line 4
    new-instance v0, Lfpd;

    invoke-direct {v0, p0}, Lfpd;-><init>(Lfpc;)V

    iput-object v0, p0, Lfpc;->checkGlThreadActiveRunnable:Ljava/lang/Runnable;

    .line 5
    invoke-virtual {p1}, Lfnp;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 6
    invoke-virtual {p1}, Lfnp;->getImpressionReporter()Lfuv;

    move-result-object v1

    iput-object v1, p0, Lfpc;->impressionReporter:Lfuv;

    .line 7
    invoke-virtual {p1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v1

    iput-object v1, p0, Lfpc;->callServiceCallbacks:Lfvt;

    .line 8
    const-string v1, "video/x-vnd.on2.vp8"

    .line 9
    invoke-static {v0, v1, v2}, Lfrc;->supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 11
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "babel_hangout_enable_egl14"

    .line 12
    invoke-static {v0, v2, v1}, Lfbm;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    .line 13
    if-eqz v1, :cond_0

    const-string v0, "Using EGL14"

    :goto_0
    invoke-static {v0}, Lfvh;->logd(Ljava/lang/String;)V

    .line 14
    if-eqz v1, :cond_1

    new-instance v0, Lfuq;

    invoke-direct {v0}, Lfuq;-><init>()V

    :goto_1
    iput-object v0, p0, Lfpc;->eglContextWrapper:Lfus;

    .line 15
    new-instance v0, Lfph;

    invoke-direct {v0, p0}, Lfph;-><init>(Lfpc;)V

    iput-object v0, p0, Lfpc;->glThread:Lfph;

    .line 16
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->start()V

    .line 17
    iget-object v0, p0, Lfpc;->checkGlThreadActiveRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lfpc;->GL_THREAD_CHECK_PERIOD:J

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    .line 18
    return-void

    .line 13
    :cond_0
    const-string v0, "Using EGL10"

    goto :goto_0

    .line 14
    :cond_1
    new-instance v0, Lfuo;

    invoke-direct {v0}, Lfuo;-><init>()V

    goto :goto_1
.end method

.method static synthetic access$000(Lfpc;)Lfus;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lfpc;->eglContextWrapper:Lfus;

    return-object v0
.end method

.method static synthetic access$300(Lfpc;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lfpc;->outputRenderers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Lfpc;)Lfph;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    return-object v0
.end method

.method private final checkGlThreadActive()V
    .locals 4

    .prologue
    .line 19
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    iget-boolean v0, p0, Lfpc;->glThreadActive:Z

    if-nez v0, :cond_2

    .line 22
    iget-object v0, p0, Lfpc;->impressionReporter:Lfuv;

    const/16 v1, 0xea6

    invoke-virtual {v0, v1}, Lfuv;->report(I)V

    .line 23
    iget-object v0, p0, Lfpc;->callServiceCallbacks:Lfvt;

    new-instance v1, Lfwb;

    sget-object v2, Lfwc;->b:Lfwc;

    invoke-direct {v1, v2}, Lfwb;-><init>(Lfwc;)V

    invoke-virtual {v0, v1}, Lfvt;->onQualityNotification(Lfwb;)V

    goto :goto_0

    .line 25
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpc;->glThreadActive:Z

    .line 26
    new-instance v0, Lfpe;

    invoke-direct {v0, p0}, Lfpe;-><init>(Lfpc;)V

    invoke-virtual {p0, v0}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 27
    iget-object v0, p0, Lfpc;->checkGlThreadActiveRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lfpc;->GL_THREAD_CHECK_PERIOD:J

    invoke-static {v0, v2, v3}, Lhcw;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method


# virtual methods
.method public final addVideoSource(Lfsm;)V
    .locals 2

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Invalid videoSource"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    new-instance v0, Lfpf;

    invoke-direct {v0, p0, p1}, Lfpf;-><init>(Lfpc;Lfsm;)V

    invoke-virtual {p0, v0}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    .line 54
    return-void
.end method

.method public final getEglContextWrapper()Lfus;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lfpc;->eglContextWrapper:Lfus;

    return-object v0
.end method

.method final synthetic lambda$addVideoSource$2$GlManager(Lfsm;)V
    .locals 3

    .prologue
    .line 66
    const-string v0, "Creating output renderer for source %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    new-instance v0, Lfpk;

    invoke-direct {v0, p0, p1}, Lfpk;-><init>(Lfpc;Lfsm;)V

    .line 68
    iget-object v1, p0, Lfpc;->outputRenderers:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method final synthetic lambda$checkGlThreadActive$1$GlManager()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpc;->glThreadActive:Z

    return-void
.end method

.method final synthetic lambda$new$0$GlManager()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lfpc;->checkGlThreadActive()V

    return-void
.end method

.method final synthetic lambda$removeVideoSource$3$GlManager(Lfsm;)V
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lfpc;->outputRenderers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpk;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    const-string v1, "Destroying output renderer for source %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {v0}, Lfpk;->release()V

    .line 65
    :cond_0
    return-void
.end method

.method public final notifyFrame(Lfsm;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    :cond_0
    const-string v0, "Tried to notify frame on a dead GlManager, ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    .line 48
    invoke-virtual {v0}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v1}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final notifyFrameDelayed(Lfsm;J)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    :cond_0
    const-string v0, "Tried to notify frame on a dead GlManager, ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 44
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    .line 42
    invoke-virtual {v0}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v1}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final queueEvent(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    :cond_0
    const-string v0, "Tried to queue an event on a dead GlManager, ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 34
    :goto_0
    return-void

    .line 33
    :cond_1
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final queueEventAtFrontOfQueue(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    :cond_0
    const-string v0, "Tried to queue an event on a dead GlManager, ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 38
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final release()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->quit()V

    .line 30
    return-void
.end method

.method public final removeVideoSource(Lfsm;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isQuitting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpc;->glThread:Lfph;

    invoke-virtual {v0}, Lfph;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    :cond_0
    const-string v0, "Tried to remove rendering target on a dead GlManager, ignoring."

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_1
    new-instance v0, Lfpg;

    invoke-direct {v0, p0, p1}, Lfpg;-><init>(Lfpc;Lfsm;)V

    invoke-virtual {p0, v0}, Lfpc;->queueEventAtFrontOfQueue(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
