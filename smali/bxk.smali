.class final Lbxk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcjq;


# instance fields
.field private synthetic a:Lbxh;


# direct methods
.method constructor <init>(Lbxh;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lbxk;->a:Lbxh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 16
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 17
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 18
    if-nez v0, :cond_0

    .line 19
    const-string v0, "VideoCallPresenter.RemoteDelegate.onSurfaceReleased"

    const-string v1, "no video call"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 22
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 23
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final a(Lcjr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 3
    iget-object v0, v0, Lbxh;->b:Lcjk;

    .line 4
    if-nez v0, :cond_0

    .line 5
    const-string v0, "VideoCallPresenter.RemoteDelegate.onSurfaceCreated"

    const-string v1, "no UI"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    :goto_0
    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 8
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 9
    if-nez v0, :cond_1

    .line 10
    const-string v0, "VideoCallPresenter.RemoteDelegate.onSurfaceCreated"

    const-string v1, "no video call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 12
    :cond_1
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 13
    iget-object v0, v0, Lbxh;->d:Landroid/telecom/InCallService$VideoCall;

    .line 14
    invoke-interface {p1}, Lcjr;->a()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/telecom/InCallService$VideoCall;->setDisplaySurface(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 26
    iget-object v0, p0, Lbxk;->a:Lbxh;

    .line 28
    const-string v1, "VideoCallPresenter.onSurfaceClick"

    const-string v2, ""

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-virtual {v0}, Lbxh;->i()V

    .line 30
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 31
    iget-boolean v1, v1, Lbwg;->t:Z

    .line 32
    if-nez v1, :cond_0

    .line 33
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v0

    const/4 v1, 0x1

    .line 34
    invoke-virtual {v0, v1, v4}, Lbwg;->a(ZZ)V

    .line 39
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-static {}, Lbwg;->a()Lbwg;

    move-result-object v1

    .line 37
    invoke-virtual {v1, v4, v4}, Lbwg;->a(ZZ)V

    .line 38
    iget-object v1, v0, Lbxh;->c:Lcdc;

    invoke-virtual {v0, v1}, Lbxh;->a(Lcdc;)V

    goto :goto_0
.end method
