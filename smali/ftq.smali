.class public final Lftq;
.super Lftw;
.source "PG"

# interfaces
.implements Lfnf;


# instance fields
.field public volatile localParticipantId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lftm;Lfnj;)V
    .locals 4

    .prologue
    .line 1
    sget-object v0, Lftq;->a:Lfno;

    new-instance v1, Lfts;

    invoke-direct {v1, p2}, Lfts;-><init>(Lfnj;)V

    new-instance v2, Lftr;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lftr;-><init>(Lfmt;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lftw;-><init>(Lftm;Lfno;Lfti;Lfth;)V

    .line 2
    return-void
.end method


# virtual methods
.method public final getLocalParticipant()Lgnm;
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lftq;->localParticipantId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lftq;->getResources()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lftq;->localParticipantId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnm;

    goto :goto_0
.end method

.method public final setLocalParticipantId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3
    iput-object p1, p0, Lftq;->localParticipantId:Ljava/lang/String;

    .line 4
    return-void
.end method
