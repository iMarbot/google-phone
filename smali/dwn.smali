.class public Ldwn;
.super Ljava/lang/Object;

# interfaces
.implements Lecx;
.implements Leda;


# static fields
.field public static a:Ljava/lang/Integer;


# instance fields
.field public final b:Landroid/view/View;

.field public final c:Z

.field public final d:Ljava/util/List;

.field public e:Ldhc;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldwn;->d:Ljava/util/List;

    .line 3
    iput-object p1, p0, Ldwn;->b:Landroid/view/View;

    .line 4
    iput-boolean p2, p0, Ldwn;->c:Z

    .line 5
    return-void
.end method

.method public static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 6
    sget-object v0, Ldwn;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 7
    const-string v0, "window"

    .line 8
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 9
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 10
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 11
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 12
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Ldwn;->a:Ljava/lang/Integer;

    .line 13
    :cond_0
    sget-object v0, Ldwn;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 67
    if-gtz p0, :cond_0

    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(III)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    sub-int v0, p2, p3

    .line 57
    if-lez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    iget-boolean v0, p0, Ldwn;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 60
    goto :goto_0

    .line 61
    :cond_2
    sub-int v0, p1, p3

    .line 62
    if-gtz v0, :cond_0

    .line 64
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, -0x2

    if-ne p2, v0, :cond_3

    .line 65
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldwn;->a(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 66
    goto :goto_0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Ldwn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 20
    :cond_1
    invoke-virtual {p0}, Ldwn;->d()I

    move-result v0

    .line 21
    invoke-virtual {p0}, Ldwn;->c()I

    move-result v1

    .line 22
    invoke-virtual {p0, v0, v1}, Ldwn;->b(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    invoke-virtual {p0, v0, v1}, Ldwn;->a(II)V

    .line 25
    invoke-virtual {p0}, Ldwn;->b()V

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 14
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldwn;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Ldgz;

    .line 15
    invoke-interface {v1, p1, p2}, Ldgz;->a(II)V

    goto :goto_0

    .line 17
    :cond_0
    return-void
.end method

.method public a(Ldgz;)V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p0}, Ldwn;->d()I

    move-result v0

    .line 28
    invoke-virtual {p0}, Ldwn;->c()I

    move-result v1

    .line 29
    invoke-virtual {p0, v0, v1}, Ldwn;->b(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30
    invoke-interface {p1, v0, v1}, Ldgz;->a(II)V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Ldwn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    iget-object v0, p0, Ldwn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_2
    iget-object v0, p0, Ldwn;->e:Ldhc;

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 36
    new-instance v1, Ldhc;

    invoke-direct {v1, p0}, Ldhc;-><init>(Ldwn;)V

    iput-object v1, p0, Ldwn;->e:Ldhc;

    .line 37
    iget-object v1, p0, Ldwn;->e:Ldhc;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Ldwn;->e:Ldhc;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 44
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldwn;->e:Ldhc;

    .line 45
    iget-object v0, p0, Ldwn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 46
    return-void
.end method

.method public b(Ldgz;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldwn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public b(II)Z
    .locals 1

    .prologue
    .line 47
    invoke-static {p1}, Ldwn;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ldwn;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    add-int/2addr v1, v0

    .line 49
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 51
    :goto_0
    iget-object v2, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v2, v0, v1}, Ldwn;->a(III)I

    move-result v0

    return v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    add-int/2addr v1, v0

    .line 53
    iget-object v0, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 55
    :goto_0
    iget-object v2, p0, Ldwn;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p0, v2, v0, v1}, Ldwn;->a(III)I

    move-result v0

    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
