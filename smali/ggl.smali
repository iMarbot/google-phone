.class public Lggl;
.super Lghm;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:Landroid/support/design/widget/AppBarLayout$Behavior$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lghm;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lggl;
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lghm;->b()Lghm;

    move-result-object v0

    check-cast v0, Lggl;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lggl;
    .locals 1

    .prologue
    .line 11
    invoke-super {p0, p1, p2}, Lghm;->b(Ljava/lang/String;Ljava/lang/Object;)Lghm;

    move-result-object v0

    check-cast v0, Lggl;

    return-object v0
.end method

.method public synthetic b()Lghm;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lggl;->a()Lggl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;Ljava/lang/Object;)Lghm;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lggl;->a(Ljava/lang/String;Ljava/lang/Object;)Lggl;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lggl;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lggl;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 9
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lghm;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lggl;->a()Lggl;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lggl;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    if-eqz v0, :cond_0

    .line 3
    :try_start_0
    iget-object v0, p0, Lggl;->a:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-virtual {v0, p0}, Landroid/support/design/widget/AppBarLayout$Behavior$a;->a(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6
    :goto_0
    return-object v0

    .line 4
    :catch_0
    move-exception v0

    .line 5
    invoke-static {v0}, Lhcw;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 6
    :cond_0
    invoke-super {p0}, Lghm;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
