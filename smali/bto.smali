.class public Lbto;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field public c:Lamg;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lbto;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method public a()Lbtn;
    .locals 19

    .prologue
    .line 30
    const-string v2, ""

    .line 31
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->a:Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 32
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " id"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->b:Ljava/lang/Long;

    if-nez v3, :cond_1

    .line 34
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " timestamp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 35
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->c:Lamg;

    if-nez v3, :cond_2

    .line 36
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " number"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->g:Ljava/lang/Long;

    if-nez v3, :cond_3

    .line 38
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " photoId"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->j:Ljava/lang/Long;

    if-nez v3, :cond_4

    .line 40
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " duration"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->m:Ljava/lang/Integer;

    if-nez v3, :cond_5

    .line 42
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " callType"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 44
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Missing required properties:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_7
    new-instance v2, Lbtm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbto;->a:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbto;->b:Ljava/lang/Long;

    .line 47
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lbto;->c:Lamg;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbto;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbto;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbto;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbto;->g:Ljava/lang/Long;

    .line 48
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lbto;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbto;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbto;->j:Ljava/lang/Long;

    .line 49
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lbto;->k:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbto;->l:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbto;->m:Ljava/lang/Integer;

    move-object/from16 v18, v0

    .line 50
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 51
    invoke-direct/range {v2 .. v18}, Lbtm;-><init>(IJLamg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 52
    return-object v2
.end method

.method public a(I)Lbto;
    .locals 1

    .prologue
    .line 2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbto;->a:Ljava/lang/Integer;

    .line 3
    return-object p0
.end method

.method public a(J)Lbto;
    .locals 1

    .prologue
    .line 4
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbto;->b:Ljava/lang/Long;

    .line 5
    return-object p0
.end method

.method public a(Lamg;)Lbto;
    .locals 2

    .prologue
    .line 6
    if-nez p1, :cond_0

    .line 7
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null number"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8
    :cond_0
    iput-object p1, p0, Lbto;->c:Lamg;

    .line 9
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lbto;->d:Ljava/lang/String;

    .line 11
    return-object p0
.end method

.method public b(I)Lbto;
    .locals 1

    .prologue
    .line 28
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbto;->m:Ljava/lang/Integer;

    .line 29
    return-object p0
.end method

.method public b(J)Lbto;
    .locals 1

    .prologue
    .line 16
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbto;->g:Ljava/lang/Long;

    .line 17
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lbto;->e:Ljava/lang/String;

    .line 13
    return-object p0
.end method

.method public c(J)Lbto;
    .locals 1

    .prologue
    .line 20
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbto;->j:Ljava/lang/Long;

    .line 21
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lbto;->f:Ljava/lang/String;

    .line 15
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lbto;->h:Ljava/lang/String;

    .line 19
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lbto;->k:Ljava/lang/String;

    .line 23
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lbto;->l:Ljava/lang/String;

    .line 25
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lbto;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lbto;->i:Ljava/lang/String;

    .line 27
    return-object p0
.end method
