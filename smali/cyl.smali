.class public final Lcyl;
.super Ldht;
.source "PG"

# interfaces
.implements Lcym;


# instance fields
.field private a:Lcyn;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Ldht;-><init>(J)V

    .line 2
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lcwz;

    .line 17
    invoke-interface {p1}, Lcwz;->c()I

    move-result v0

    .line 18
    return v0
.end method

.method public final synthetic a(Lcud;)Lcwz;
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Ldht;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwz;

    return-object v0
.end method

.method public final synthetic a(Lcud;Lcwz;)Lcwz;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Ldht;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwz;

    return-object v0
.end method

.method public final a(I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 5
    const/16 v0, 0x28

    if-lt p1, v0, :cond_1

    .line 7
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ldht;->a(J)V

    .line 11
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    const/16 v0, 0x14

    if-lt p1, v0, :cond_0

    .line 10
    invoke-virtual {p0}, Lcyl;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcyl;->a(J)V

    goto :goto_0
.end method

.method public final a(Lcyn;)V
    .locals 0

    .prologue
    .line 3
    iput-object p1, p0, Lcyl;->a:Lcyn;

    .line 4
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 12
    check-cast p2, Lcwz;

    .line 13
    iget-object v0, p0, Lcyl;->a:Lcyn;

    if-eqz v0, :cond_0

    .line 14
    iget-object v0, p0, Lcyl;->a:Lcyn;

    invoke-interface {v0, p2}, Lcyn;->a(Lcwz;)V

    .line 15
    :cond_0
    return-void
.end method
