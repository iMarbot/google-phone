.class public Ledz;
.super Ljava/lang/Object;

# interfaces
.implements Lefu;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Ljava/util/Map;

.field public final c:Leex;

.field public final d:Ljava/util/concurrent/locks/Lock;

.field public final e:Ljava/util/concurrent/locks/Condition;

.field public final f:Z

.field public g:Z

.field public h:Ljava/util/Map;

.field public i:Ljava/util/Map;

.field public j:Lecl;

.field public final k:Ljava/lang/String;

.field public final l:J

.field public final m:J

.field private n:Ljava/util/Map;

.field private o:Lefj;

.field private p:Landroid/os/Looper;

.field private q:Lecp;

.field private r:Lejm;

.field private s:Z

.field private t:Ljava/util/Queue;

.field private u:Leeb;


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ledz;->k:Ljava/lang/String;

    iput-wide p2, p0, Ledz;->l:J

    iput-wide p4, p0, Ledz;->m:J

    return-void
.end method

.method static synthetic a(Ledz;)Lecl;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const v3, 0x7fffffff

    .line 40
    .line 41
    iget-object v2, p0, Ledz;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v0

    move-object v6, v1

    move v2, v0

    move-object v4, v1

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    .line 42
    iget-object v1, v0, Ledh;->b:Lesq;

    .line 44
    iget-object v0, v0, Ledh;->c:Legz;

    .line 45
    iget-object v8, p0, Ledz;->h:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecl;

    invoke-virtual {v0}, Lecl;->b()Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Ledz;->n:Ljava/util/Map;

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lecl;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Ledz;->q:Lecp;

    .line 46
    iget v8, v0, Lecl;->b:I

    .line 47
    invoke-virtual {v1, v8}, Lecp;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    :cond_1
    iget v1, v0, Lecl;->b:I

    .line 49
    const/4 v8, 0x4

    if-ne v1, v8, :cond_3

    iget-boolean v1, p0, Ledz;->s:Z

    if-eqz v1, :cond_3

    if-eqz v6, :cond_2

    if-le v5, v3, :cond_0

    :cond_2
    move v5, v3

    move-object v6, v0

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_4

    if-le v2, v3, :cond_7

    :cond_4
    move-object v1, v0

    move v0, v3

    :goto_1
    move v2, v0

    move-object v4, v1

    goto :goto_0

    :cond_5
    if-eqz v4, :cond_6

    if-eqz v6, :cond_6

    if-le v2, v5, :cond_6

    .line 50
    :goto_2
    return-object v6

    :cond_6
    move-object v6, v4

    goto :goto_2

    :cond_7
    move v0, v2

    move-object v1, v4

    goto :goto_1
.end method

.method private final a(Letf;)Lecl;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledz;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    iget-object v1, p0, Ledz;->h:Ljava/util/Map;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Ledz;->h:Ljava/util/Map;

    .line 26
    iget-object v0, v0, Ledh;->c:Legz;

    .line 27
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(Ledz;Ledy;Lecl;)Z
    .locals 2

    .prologue
    .line 33
    .line 34
    invoke-virtual {p2}, Lecl;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lecl;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ledz;->n:Ljava/util/Map;

    .line 35
    iget-object v1, p1, Ledh;->b:Lesq;

    .line 36
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledz;->q:Lecp;

    .line 37
    iget v1, p2, Lecl;->b:I

    .line 38
    invoke-virtual {v0, v1}, Lecp;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 39
    goto :goto_0
.end method

.method static synthetic b(Ledz;)V
    .locals 5

    .prologue
    .line 51
    .line 52
    iget-object v0, p0, Ledz;->r:Lejm;

    if-nez v0, :cond_0

    iget-object v0, p0, Ledz;->c:Leex;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, Leex;->d:Ljava/util/Set;

    .line 59
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Ledz;->r:Lejm;

    .line 53
    iget-object v0, v0, Lejm;->b:Ljava/util/Set;

    .line 54
    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Ledz;->r:Lejm;

    .line 55
    iget-object v2, v0, Lejm;->d:Ljava/util/Map;

    .line 56
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesq;

    .line 57
    invoke-virtual {v0}, Lesq;->c()Letf;

    move-result-object v4

    invoke-direct {p0, v4}, Ledz;->a(Letf;)Lecl;

    move-result-object v4

    .line 58
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lecl;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejo;

    iget-object v0, v0, Lejo;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ledz;->c:Leex;

    iput-object v1, v0, Leex;->d:Ljava/util/Set;

    goto :goto_0
.end method

.method static synthetic c(Ledz;)V
    .locals 2

    .prologue
    .line 60
    .line 61
    :goto_0
    iget-object v0, p0, Ledz;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ledz;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehe;

    invoke-virtual {p0, v0}, Ledz;->b(Lehe;)Lehe;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ledz;->c:Leex;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leex;->a(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method private final c(Lehe;)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x0

    .line 11
    .line 12
    iget-object v0, p1, Lehe;->a:Letf;

    .line 13
    invoke-direct {p0, v0}, Ledz;->a(Letf;)Lecl;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 14
    iget v2, v2, Lecl;->b:I

    .line 15
    if-ne v2, v6, :cond_3

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    iget-object v3, p0, Ledz;->o:Lefj;

    iget-object v4, p0, Ledz;->a:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    .line 16
    iget-object v0, v0, Ledh;->c:Legz;

    .line 17
    iget-object v4, p0, Ledz;->c:Leex;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    .line 18
    iget-object v5, v3, Lefj;->h:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 21
    :goto_0
    invoke-direct {v2, v6, v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p1, v2}, Lehe;->b(Lcom/google/android/gms/common/api/Status;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 19
    :cond_0
    iget-object v5, v0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    if-nez v5, :cond_1

    move-object v0, v1

    .line 20
    :goto_2
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 19
    :cond_1
    iget-object v0, v0, Lefk;->f:Lcom/google/android/gms/common/api/internal/zzdc;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzdc;->zzals()Lera;

    move-result-object v0

    goto :goto_2

    .line 20
    :cond_2
    iget-object v3, v3, Lefj;->e:Landroid/content/Context;

    invoke-interface {v0}, Lera;->d()Landroid/content/Intent;

    move-result-object v0

    const/high16 v5, 0x8000000

    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 21
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private f()Z
    .locals 2

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledz;->h:Ljava/util/Map;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ledz;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private final g()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Ledz;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ledz;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Ledz;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Letf;

    invoke-direct {p0, v0}, Ledz;->a(Letf;)Lecl;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lecl;->b()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :cond_3
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lecl;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ledz;->a()V

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    :goto_0
    invoke-direct {p0}, Ledz;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ledz;->c()V

    new-instance v0, Lecl;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    :goto_1
    return-object v0

    :cond_0
    iget-object v2, p0, Ledz;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Lecl;

    const/16 v1, 0xf

    invoke-direct {v0, v1, v4}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Ledz;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lecl;->a:Lecl;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ledz;->j:Lecl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ledz;->j:Lecl;

    goto :goto_1

    :cond_3
    new-instance v0, Lecl;

    const/16 v1, 0xd

    invoke-direct {v0, v1, v4}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public final a(Lehe;)Lehe;
    .locals 2

    .prologue
    .line 1
    iget-boolean v0, p0, Ledz;->s:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ledz;->c(Lehe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5
    :goto_0
    return-object p1

    .line 1
    :cond_0
    invoke-virtual {p0}, Ledz;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ledz;->t:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ledz;->c:Leex;

    iget-object v0, v0, Leex;->f:Legs;

    invoke-virtual {v0, p1}, Legs;->a(Lehk;)V

    iget-object v0, p0, Ledz;->a:Ljava/util/Map;

    .line 2
    iget-object v1, p1, Lehe;->a:Letf;

    .line 3
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    .line 4
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ledh;->a(ILehe;)Lehe;

    move-result-object p1

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Ledz;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 24
    :goto_0
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Ledz;->g:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->h:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->i:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->u:Leeb;

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->j:Lecl;

    iget-object v0, p0, Ledz;->o:Lefj;

    invoke-virtual {v0}, Lefj;->a()V

    iget-object v0, p0, Ledz;->o:Lefj;

    iget-object v1, p0, Ledz;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lefj;->a(Ljava/lang/Iterable;)Lfat;

    move-result-object v0

    new-instance v1, Leqg;

    iget-object v2, p0, Ledz;->p:Landroid/os/Looper;

    invoke-direct {v1, v2}, Leqg;-><init>(Landroid/os/Looper;)V

    new-instance v2, Leea;

    .line 23
    invoke-direct {v2, p0}, Leea;-><init>(Ledz;)V

    .line 24
    invoke-virtual {v0, v1, v2}, Lfat;->a(Ljava/util/concurrent/Executor;Lfap;)Lfat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Legj;)Z
    .locals 3

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Ledz;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ledz;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ledz;->o:Lefj;

    invoke-virtual {v0}, Lefj;->a()V

    new-instance v0, Leeb;

    invoke-direct {v0, p0, p1}, Leeb;-><init>(Ledz;Legj;)V

    iput-object v0, p0, Ledz;->u:Leeb;

    iget-object v0, p0, Ledz;->o:Lefj;

    iget-object v1, p0, Ledz;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lefj;->a(Ljava/lang/Iterable;)Lfat;

    move-result-object v0

    new-instance v1, Leqg;

    iget-object v2, p0, Ledz;->p:Landroid/os/Looper;

    invoke-direct {v1, v2}, Leqg;-><init>(Landroid/os/Looper;)V

    iget-object v2, p0, Ledz;->u:Leeb;

    invoke-virtual {v0, v1, v2}, Lfat;->a(Ljava/util/concurrent/Executor;Lfap;)Lfat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()Lecl;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ledz;->a()V

    :goto_0
    invoke-direct {p0}, Ledz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Ledz;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Lecl;

    const/16 v1, 0xf

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ledz;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lecl;->a:Lecl;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ledz;->j:Lecl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ledz;->j:Lecl;

    goto :goto_1

    :cond_2
    new-instance v0, Lecl;

    const/16 v1, 0xd

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public final b(Lehe;)Lehe;
    .locals 2

    .prologue
    .line 6
    .line 7
    iget-object v0, p1, Lehe;->a:Letf;

    .line 8
    iget-boolean v1, p0, Ledz;->s:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Ledz;->c(Lehe;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10
    :goto_0
    return-object p1

    .line 8
    :cond_0
    iget-object v1, p0, Ledz;->c:Leex;

    iget-object v1, v1, Leex;->f:Legs;

    invoke-virtual {v1, p1}, Legs;->a(Lehk;)V

    iget-object v1, p0, Ledz;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    .line 9
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ledh;->a(ILehe;)Lehe;

    move-result-object p1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Ledz;->g:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->h:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->i:Ljava/util/Map;

    iget-object v0, p0, Ledz;->u:Leeb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledz;->u:Leeb;

    invoke-virtual {v0}, Leeb;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->u:Leeb;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->j:Lecl;

    :goto_0
    iget-object v0, p0, Ledz;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ledz;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehk;->a(Legv;)V

    invoke-virtual {v0}, Ledn;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Ledz;->e:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledz;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledz;->j:Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 28
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Ledz;->o:Lefj;

    .line 29
    iget-object v1, v0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v1, v0, Lefj;->k:Landroid/os/Handler;

    iget-object v0, v0, Lefj;->k:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 30
    iget-object v0, p0, Ledz;->u:Leeb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledz;->u:Leeb;

    invoke-virtual {v0}, Leeb;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Ledz;->u:Leeb;

    :cond_0
    iget-object v0, p0, Ledz;->i:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Lpd;

    iget-object v1, p0, Ledz;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Lpd;-><init>(I)V

    iput-object v0, p0, Ledz;->i:Ljava/util/Map;

    :cond_1
    new-instance v1, Lecl;

    const/4 v0, 0x4

    invoke-direct {v1, v0}, Lecl;-><init>(I)V

    iget-object v0, p0, Ledz;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledy;

    iget-object v3, p0, Ledz;->i:Ljava/util/Map;

    .line 31
    iget-object v0, v0, Ledh;->c:Legz;

    .line 32
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Ledz;->h:Ljava/util/Map;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ledz;->h:Ljava/util/Map;

    iget-object v1, p0, Ledz;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    iget-object v0, p0, Ledz;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method
