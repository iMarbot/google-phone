.class public final Lxc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lxu;


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field public b:Lxf;

.field public c:Landroid/support/v7/view/menu/ExpandedMenuView;

.field public d:I

.field public e:Lxv;

.field public f:Lxd;

.field private g:Landroid/content/Context;

.field private h:I


# direct methods
.method private constructor <init>(II)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput p1, p0, Lxc;->d:I

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lxc;->h:I

    .line 8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lxc;-><init>(II)V

    .line 2
    iput-object p1, p0, Lxc;->g:Landroid/content/Context;

    .line 3
    iget-object v0, p0, Lxc;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lxc;->a:Landroid/view/LayoutInflater;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lxf;)V
    .locals 2

    .prologue
    .line 9
    iget v0, p0, Lxc;->h:I

    if-eqz v0, :cond_2

    .line 10
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Lxc;->h:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lxc;->g:Landroid/content/Context;

    .line 11
    iget-object v0, p0, Lxc;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lxc;->a:Landroid/view/LayoutInflater;

    .line 16
    :cond_0
    :goto_0
    iput-object p2, p0, Lxc;->b:Lxf;

    .line 17
    iget-object v0, p0, Lxc;->f:Lxd;

    if-eqz v0, :cond_1

    .line 18
    iget-object v0, p0, Lxc;->f:Lxd;

    invoke-virtual {v0}, Lxd;->notifyDataSetChanged()V

    .line 19
    :cond_1
    return-void

    .line 12
    :cond_2
    iget-object v0, p0, Lxc;->g:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 13
    iput-object p1, p0, Lxc;->g:Landroid/content/Context;

    .line 14
    iget-object v0, p0, Lxc;->a:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 15
    iget-object v0, p0, Lxc;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lxc;->a:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final a(Lxf;Z)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lxc;->e:Lxv;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lxc;->e:Lxv;

    invoke-interface {v0, p1, p2}, Lxv;->a(Lxf;Z)V

    .line 70
    :cond_0
    return-void
.end method

.method public final a(Lxv;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lxc;->e:Lxv;

    .line 26
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lxc;->f:Lxd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxc;->f:Lxd;

    invoke-virtual {v0}, Lxd;->notifyDataSetChanged()V

    .line 24
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lye;)Z
    .locals 6

    .prologue
    .line 27
    invoke-virtual {p1}, Lye;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    .line 28
    :cond_0
    new-instance v0, Lxi;

    invoke-direct {v0, p1}, Lxi;-><init>(Lxf;)V

    .line 29
    iget-object v1, v0, Lxi;->a:Lxf;

    .line 30
    new-instance v2, Lug;

    .line 31
    iget-object v3, v1, Lxf;->a:Landroid/content/Context;

    .line 32
    invoke-direct {v2, v3}, Lug;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v3, Lxc;

    .line 34
    iget-object v4, v2, Lug;->a:Lub;

    iget-object v4, v4, Lub;->a:Landroid/content/Context;

    .line 35
    const v5, 0x7f04000f

    invoke-direct {v3, v4, v5}, Lxc;-><init>(Landroid/content/Context;I)V

    iput-object v3, v0, Lxi;->c:Lxc;

    .line 36
    iget-object v3, v0, Lxi;->c:Lxc;

    .line 37
    iput-object v0, v3, Lxc;->e:Lxv;

    .line 38
    iget-object v3, v0, Lxi;->a:Lxf;

    iget-object v4, v0, Lxi;->c:Lxc;

    invoke-virtual {v3, v4}, Lxf;->a(Lxu;)V

    .line 39
    iget-object v3, v0, Lxi;->c:Lxc;

    invoke-virtual {v3}, Lxc;->b()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 40
    iget-object v4, v2, Lug;->a:Lub;

    iput-object v3, v4, Lub;->n:Landroid/widget/ListAdapter;

    .line 41
    iget-object v3, v2, Lug;->a:Lub;

    iput-object v0, v3, Lub;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 43
    iget-object v3, v1, Lxf;->h:Landroid/view/View;

    .line 45
    if-eqz v3, :cond_2

    .line 47
    iget-object v1, v2, Lug;->a:Lub;

    iput-object v3, v1, Lub;->e:Landroid/view/View;

    .line 58
    :goto_1
    iget-object v1, v2, Lug;->a:Lub;

    iput-object v0, v1, Lub;->m:Landroid/content/DialogInterface$OnKeyListener;

    .line 59
    invoke-virtual {v2}, Lug;->a()Luf;

    move-result-object v1

    iput-object v1, v0, Lxi;->b:Luf;

    .line 60
    iget-object v1, v0, Lxi;->b:Luf;

    invoke-virtual {v1, v0}, Luf;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 61
    iget-object v1, v0, Lxi;->b:Luf;

    invoke-virtual {v1}, Luf;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 62
    const/16 v2, 0x3eb

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 63
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 64
    iget-object v0, v0, Lxi;->b:Luf;

    invoke-virtual {v0}, Luf;->show()V

    .line 65
    iget-object v0, p0, Lxc;->e:Lxv;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lxc;->e:Lxv;

    invoke-interface {v0, p1}, Lxv;->a(Lxf;)Z

    .line 67
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 50
    :cond_2
    iget-object v3, v1, Lxf;->g:Landroid/graphics/drawable/Drawable;

    .line 52
    iget-object v4, v2, Lug;->a:Lub;

    iput-object v3, v4, Lub;->c:Landroid/graphics/drawable/Drawable;

    .line 55
    iget-object v1, v1, Lxf;->f:Ljava/lang/CharSequence;

    .line 56
    invoke-virtual {v2, v1}, Lug;->a(Ljava/lang/CharSequence;)Lug;

    goto :goto_1
.end method

.method public final b()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lxc;->f:Lxd;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lxd;

    invoke-direct {v0, p0}, Lxd;-><init>(Lxc;)V

    iput-object v0, p0, Lxc;->f:Lxd;

    .line 22
    :cond_0
    iget-object v0, p0, Lxc;->f:Lxd;

    return-object v0
.end method

.method public final b(Lxj;)Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lxj;)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lxc;->b:Lxf;

    iget-object v1, p0, Lxc;->f:Lxd;

    invoke-virtual {v1, p3}, Lxd;->a(I)Lxj;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lxf;->a(Landroid/view/MenuItem;Lxu;I)Z

    .line 72
    return-void
.end method
