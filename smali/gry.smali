.class public final Lgry;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Lgrz;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lgry;->b:Ljava/lang/String;

    .line 4
    iput-object v0, p0, Lgry;->c:Ljava/lang/Integer;

    .line 5
    iput-object v0, p0, Lgry;->a:Ljava/lang/Integer;

    .line 6
    iput-object v0, p0, Lgry;->d:Lgrz;

    .line 7
    iput-object v0, p0, Lgry;->e:Ljava/lang/Integer;

    .line 8
    iput-object v0, p0, Lgry;->unknownFieldData:Lhfv;

    .line 9
    const/4 v0, -0x1

    iput v0, p0, Lgry;->cachedSize:I

    .line 10
    return-void
.end method

.method private a(Lhfp;)Lgry;
    .locals 3

    .prologue
    .line 40
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 41
    sparse-switch v0, :sswitch_data_0

    .line 43
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    :sswitch_0
    return-object p0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgry;->b:Ljava/lang/String;

    goto :goto_0

    .line 48
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 49
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgry;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 51
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 53
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 54
    invoke-static {v2}, Lhcw;->t(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgry;->a:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 58
    invoke-virtual {p0, p1, v0}, Lgry;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 60
    :sswitch_4
    iget-object v0, p0, Lgry;->d:Lgrz;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lgrz;

    invoke-direct {v0}, Lgrz;-><init>()V

    iput-object v0, p0, Lgry;->d:Lgrz;

    .line 62
    :cond_1
    iget-object v0, p0, Lgry;->d:Lgrz;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 64
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 66
    :try_start_1
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 67
    invoke-static {v2}, Lgjz;->a(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgry;->e:Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 70
    :catch_1
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 71
    invoke-virtual {p0, p1, v0}, Lgry;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 24
    iget-object v1, p0, Lgry;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25
    const/4 v1, 0x1

    iget-object v2, p0, Lgry;->b:Ljava/lang/String;

    .line 26
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_0
    iget-object v1, p0, Lgry;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 28
    const/4 v1, 0x2

    iget-object v2, p0, Lgry;->c:Ljava/lang/Integer;

    .line 29
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_1
    iget-object v1, p0, Lgry;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 31
    const/4 v1, 0x3

    iget-object v2, p0, Lgry;->a:Ljava/lang/Integer;

    .line 32
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_2
    iget-object v1, p0, Lgry;->d:Lgrz;

    if-eqz v1, :cond_3

    .line 34
    const/4 v1, 0x4

    iget-object v2, p0, Lgry;->d:Lgrz;

    .line 35
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36
    :cond_3
    iget-object v1, p0, Lgry;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 37
    const/4 v1, 0x5

    iget-object v2, p0, Lgry;->e:Ljava/lang/Integer;

    .line 38
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lgry;->a(Lhfp;)Lgry;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lgry;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 12
    const/4 v0, 0x1

    iget-object v1, p0, Lgry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 13
    :cond_0
    iget-object v0, p0, Lgry;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 14
    const/4 v0, 0x2

    iget-object v1, p0, Lgry;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 15
    :cond_1
    iget-object v0, p0, Lgry;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 16
    const/4 v0, 0x3

    iget-object v1, p0, Lgry;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 17
    :cond_2
    iget-object v0, p0, Lgry;->d:Lgrz;

    if-eqz v0, :cond_3

    .line 18
    const/4 v0, 0x4

    iget-object v1, p0, Lgry;->d:Lgrz;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_3
    iget-object v0, p0, Lgry;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 20
    const/4 v0, 0x5

    iget-object v1, p0, Lgry;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 21
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 22
    return-void
.end method
