.class public final Lgmo;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmo;


# instance fields
.field public currentState:Ljava/lang/Integer;

.field public liveStartTimeMs:Ljava/lang/Long;

.field public shouldGoLive:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lhft;-><init>()V

    .line 15
    invoke-virtual {p0}, Lgmo;->clear()Lgmo;

    .line 16
    return-void
.end method

.method public static checkStatusOrThrow(I)I
    .locals 3

    .prologue
    .line 1
    packed-switch p0, :pswitch_data_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid enum Status"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2
    :pswitch_0
    return p0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static checkStatusOrThrow([I)[I
    .locals 3

    .prologue
    .line 4
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p0, v0

    .line 5
    invoke-static {v2}, Lgmo;->checkStatusOrThrow(I)I

    .line 6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7
    :cond_0
    return-object p0
.end method

.method public static emptyArray()[Lgmo;
    .locals 2

    .prologue
    .line 8
    sget-object v0, Lgmo;->_emptyArray:[Lgmo;

    if-nez v0, :cond_1

    .line 9
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 10
    :try_start_0
    sget-object v0, Lgmo;->_emptyArray:[Lgmo;

    if-nez v0, :cond_0

    .line 11
    const/4 v0, 0x0

    new-array v0, v0, [Lgmo;

    sput-object v0, Lgmo;->_emptyArray:[Lgmo;

    .line 12
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_1
    sget-object v0, Lgmo;->_emptyArray:[Lgmo;

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmo;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lgmo;

    invoke-direct {v0}, Lgmo;-><init>()V

    invoke-virtual {v0, p0}, Lgmo;->mergeFrom(Lhfp;)Lgmo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmo;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lgmo;

    invoke-direct {v0}, Lgmo;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmo;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmo;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lgmo;->currentState:Ljava/lang/Integer;

    .line 18
    iput-object v0, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    .line 19
    iput-object v0, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    .line 20
    iput-object v0, p0, Lgmo;->unknownFieldData:Lhfv;

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lgmo;->cachedSize:I

    .line 22
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 31
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 32
    iget-object v1, p0, Lgmo;->currentState:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 33
    const/4 v1, 0x3

    iget-object v2, p0, Lgmo;->currentState:Ljava/lang/Integer;

    .line 34
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 35
    :cond_0
    iget-object v1, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 36
    const/4 v1, 0x4

    iget-object v2, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    .line 37
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lhfq;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 38
    :cond_1
    iget-object v1, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 39
    const/4 v1, 0x5

    iget-object v2, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    .line 40
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 41
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 42
    add-int/2addr v0, v1

    .line 43
    :cond_2
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmo;
    .locals 3

    .prologue
    .line 44
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 45
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    :sswitch_0
    return-object p0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 51
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 52
    invoke-static {v2}, Lgmo;->checkStatusOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgmo;->currentState:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 56
    invoke-virtual {p0, p1, v0}, Lgmo;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 59
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->h()J

    move-result-wide v0

    .line 60
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    goto :goto_0

    .line 62
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    goto :goto_0

    .line 45
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lgmo;->mergeFrom(Lhfp;)Lgmo;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    .line 23
    iget-object v0, p0, Lgmo;->currentState:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x3

    iget-object v1, p0, Lgmo;->currentState:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 25
    :cond_0
    iget-object v0, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x4

    iget-object v1, p0, Lgmo;->liveStartTimeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lhfq;->b(IJ)V

    .line 27
    :cond_1
    iget-object v0, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 28
    const/4 v0, 0x5

    iget-object v1, p0, Lgmo;->shouldGoLive:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 29
    :cond_2
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 30
    return-void
.end method
