.class public Labp;
.super Landroid/view/ViewGroup;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Labp$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:F

.field public f:Z

.field public g:I

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:I

.field private j:Z

.field private k:[I

.field private l:[I

.field private m:I

.field private n:I

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Labp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Labp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 6
    iput-boolean v0, p0, Labp;->f:Z

    .line 7
    iput v6, p0, Labp;->a:I

    .line 8
    iput v1, p0, Labp;->b:I

    .line 9
    const v2, 0x800033

    iput v2, p0, Labp;->g:I

    .line 10
    sget-object v2, Lvu;->aG:[I

    invoke-static {p1, p2, v2, p3, v1}, Ladn;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ladn;

    move-result-object v2

    .line 11
    sget v3, Lvu;->aN:I

    invoke-virtual {v2, v3, v6}, Ladn;->a(II)I

    move-result v3

    .line 12
    if-ltz v3, :cond_0

    .line 13
    invoke-virtual {p0, v3}, Labp;->c(I)V

    .line 14
    :cond_0
    sget v3, Lvu;->aM:I

    invoke-virtual {v2, v3, v6}, Ladn;->a(II)I

    move-result v3

    .line 15
    if-ltz v3, :cond_1

    .line 16
    invoke-virtual {p0, v3}, Labp;->d(I)V

    .line 17
    :cond_1
    sget v3, Lvu;->aK:I

    invoke-virtual {v2, v3, v0}, Ladn;->a(IZ)Z

    move-result v3

    .line 18
    if-nez v3, :cond_2

    .line 20
    iput-boolean v3, p0, Labp;->f:Z

    .line 21
    :cond_2
    sget v3, Lvu;->aO:I

    .line 22
    iget-object v4, v2, Ladn;->b:Landroid/content/res/TypedArray;

    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {v4, v3, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    .line 23
    iput v3, p0, Labp;->e:F

    .line 24
    sget v3, Lvu;->aL:I

    .line 25
    invoke-virtual {v2, v3, v6}, Ladn;->a(II)I

    move-result v3

    iput v3, p0, Labp;->a:I

    .line 26
    sget v3, Lvu;->aR:I

    invoke-virtual {v2, v3, v1}, Ladn;->a(IZ)Z

    move-result v3

    iput-boolean v3, p0, Labp;->j:Z

    .line 27
    sget v3, Lvu;->aP:I

    invoke-virtual {v2, v3}, Ladn;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 28
    iget-object v4, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    if-eq v3, v4, :cond_3

    .line 29
    iput-object v3, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    .line 30
    if-eqz v3, :cond_4

    .line 31
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iput v4, p0, Labp;->i:I

    .line 32
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    iput v4, p0, Labp;->m:I

    .line 35
    :goto_0
    if-nez v3, :cond_5

    :goto_1
    invoke-virtual {p0, v0}, Labp;->setWillNotDraw(Z)V

    .line 36
    invoke-virtual {p0}, Labp;->requestLayout()V

    .line 37
    :cond_3
    sget v0, Lvu;->aS:I

    invoke-virtual {v2, v0, v1}, Ladn;->a(II)I

    move-result v0

    iput v0, p0, Labp;->n:I

    .line 38
    sget v0, Lvu;->aQ:I

    invoke-virtual {v2, v0, v1}, Ladn;->e(II)I

    move-result v0

    iput v0, p0, Labp;->o:I

    .line 40
    iget-object v0, v2, Ladn;->b:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void

    .line 33
    :cond_4
    iput v1, p0, Labp;->i:I

    .line 34
    iput v1, p0, Labp;->m:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 35
    goto :goto_1
.end method

.method private final a(II)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 306
    invoke-virtual {p0}, Labp;->getMeasuredWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move v7, v3

    .line 307
    :goto_0
    if-ge v7, p1, :cond_1

    .line 309
    invoke-virtual {p0, v7}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 311
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    .line 312
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Labp$a;

    .line 313
    iget v0, v6, Labp$a;->width:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 314
    iget v8, v6, Labp$a;->height:I

    .line 315
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, v6, Labp$a;->height:I

    move-object v0, p0

    move v4, p2

    move v5, v3

    .line 316
    invoke-virtual/range {v0 .. v5}, Labp;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 317
    iput v8, v6, Labp$a;->height:I

    .line 318
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 319
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Labp;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Labp;->o:I

    add-int/2addr v1, v2

    .line 102
    invoke-virtual {p0}, Labp;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Labp;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Labp;->o:I

    sub-int/2addr v2, v3

    iget v3, p0, Labp;->m:I

    add-int/2addr v3, p2

    .line 103
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 104
    iget-object v0, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 105
    return-void
.end method

.method private a(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 546
    invoke-virtual/range {p0 .. p5}, Labp;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 547
    return-void
.end method

.method private b(II)V
    .locals 29

    .prologue
    .line 320
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 321
    const/16 v18, 0x0

    .line 322
    const/16 v17, 0x0

    .line 323
    const/16 v16, 0x0

    .line 324
    const/4 v15, 0x0

    .line 325
    const/4 v14, 0x1

    .line 326
    const/4 v4, 0x0

    .line 328
    invoke-virtual/range {p0 .. p0}, Labp;->getChildCount()I

    move-result v21

    .line 330
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 331
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 332
    const/4 v10, 0x0

    .line 333
    const/4 v12, 0x0

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Labp;->k:[I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Labp;->l:[I

    if-nez v2, :cond_1

    .line 335
    :cond_0
    const/4 v2, 0x4

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Labp;->k:[I

    .line 336
    const/4 v2, 0x4

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Labp;->l:[I

    .line 337
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Labp;->k:[I

    move-object/from16 v24, v0

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Labp;->l:[I

    move-object/from16 v25, v0

    .line 339
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, -0x1

    aput v7, v24, v6

    aput v7, v24, v5

    aput v7, v24, v3

    aput v7, v24, v2

    .line 340
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, -0x1

    aput v7, v25, v6

    aput v7, v25, v5

    aput v7, v25, v3

    aput v7, v25, v2

    .line 341
    move-object/from16 v0, p0

    iget-boolean v0, v0, Labp;->f:Z

    move/from16 v26, v0

    .line 342
    move-object/from16 v0, p0

    iget-boolean v0, v0, Labp;->j:Z

    move/from16 v27, v0

    .line 343
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    move v9, v2

    .line 344
    :goto_0
    const/4 v11, 0x0

    .line 345
    const/16 v19, 0x0

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_12

    .line 347
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 349
    if-nez v3, :cond_3

    .line 350
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    move/from16 v2, v19

    .line 404
    :goto_2
    add-int/lit8 v19, v2, 0x1

    goto :goto_1

    .line 343
    :cond_2
    const/4 v2, 0x0

    move v9, v2

    goto :goto_0

    .line 352
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v5, 0x8

    if-eq v2, v5, :cond_34

    .line 353
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Labp;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 354
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    move-object/from16 v0, p0

    iget v5, v0, Labp;->i:I

    add-int/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 356
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Labp$a;

    .line 357
    iget v2, v8, Labp$a;->g:F

    add-float v13, v4, v2

    .line 358
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-ne v0, v2, :cond_8

    iget v2, v8, Labp$a;->width:I

    if-nez v2, :cond_8

    iget v2, v8, Labp$a;->g:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_8

    .line 359
    if-eqz v9, :cond_6

    .line 360
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    iget v4, v8, Labp$a;->leftMargin:I

    iget v5, v8, Labp$a;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 363
    :goto_3
    if-eqz v26, :cond_7

    .line 364
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 365
    invoke-virtual {v3, v2, v2}, Landroid/view/View;->measure(II)V

    move v7, v11

    move v11, v12

    .line 384
    :goto_4
    const/4 v2, 0x0

    .line 385
    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v4, :cond_32

    iget v4, v8, Labp$a;->height:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_32

    .line 386
    const/4 v4, 0x1

    .line 387
    const/4 v2, 0x1

    .line 388
    :goto_5
    iget v5, v8, Labp$a;->topMargin:I

    iget v6, v8, Labp$a;->bottomMargin:I

    add-int/2addr v5, v6

    .line 389
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v5

    .line 390
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v10

    move/from16 v0, v17

    invoke-static {v0, v10}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 391
    if-eqz v26, :cond_5

    .line 392
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    move-result v12

    .line 393
    const/4 v3, -0x1

    if-eq v12, v3, :cond_5

    .line 394
    iget v3, v8, Labp$a;->h:I

    if-gez v3, :cond_d

    move-object/from16 v0, p0

    iget v3, v0, Labp;->g:I

    :goto_6
    and-int/lit8 v3, v3, 0x70

    .line 395
    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, -0x2

    shr-int/lit8 v3, v3, 0x1

    .line 396
    aget v17, v24, v3

    move/from16 v0, v17

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v17

    aput v17, v24, v3

    .line 397
    aget v17, v25, v3

    sub-int v12, v6, v12

    move/from16 v0, v17

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v12

    aput v12, v25, v3

    .line 398
    :cond_5
    move/from16 v0, v18

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 399
    if-eqz v14, :cond_e

    iget v3, v8, Labp$a;->height:I

    const/4 v14, -0x1

    if-ne v3, v14, :cond_e

    const/4 v3, 0x1

    .line 400
    :goto_7
    iget v8, v8, Labp$a;->g:F

    const/4 v14, 0x0

    cmpl-float v8, v8, v14

    if-lez v8, :cond_10

    .line 401
    if-eqz v2, :cond_f

    move v2, v5

    :goto_8
    invoke-static {v15, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v5, v13

    move v6, v3

    move/from16 v8, v16

    move v3, v11

    move v11, v12

    move/from16 v28, v7

    move v7, v2

    move/from16 v2, v28

    :goto_9
    move v12, v3

    move v14, v6

    move v15, v7

    move/from16 v16, v8

    move/from16 v17, v10

    move/from16 v18, v11

    move v10, v4

    move v11, v2

    move v4, v5

    move/from16 v2, v19

    .line 403
    goto/16 :goto_2

    .line 361
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    .line 362
    iget v4, v8, Labp$a;->leftMargin:I

    add-int/2addr v4, v2

    iget v5, v8, Labp$a;->rightMargin:I

    add-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    goto/16 :goto_3

    .line 367
    :cond_7
    const/4 v12, 0x1

    move v7, v11

    move v11, v12

    goto/16 :goto_4

    .line 368
    :cond_8
    const/high16 v2, -0x80000000

    .line 369
    iget v4, v8, Labp$a;->width:I

    if-nez v4, :cond_9

    iget v4, v8, Labp$a;->g:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_9

    .line 370
    const/4 v2, 0x0

    .line 371
    const/4 v4, -0x2

    iput v4, v8, Labp$a;->width:I

    :cond_9
    move/from16 v20, v2

    .line 372
    const/4 v2, 0x0

    cmpl-float v2, v13, v2

    if-nez v2, :cond_b

    move-object/from16 v0, p0

    iget v5, v0, Labp;->d:I

    :goto_a
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v6, p2

    invoke-direct/range {v2 .. v7}, Labp;->a(Landroid/view/View;IIII)V

    .line 373
    const/high16 v2, -0x80000000

    move/from16 v0, v20

    if-eq v0, v2, :cond_a

    .line 374
    move/from16 v0, v20

    iput v0, v8, Labp$a;->width:I

    .line 375
    :cond_a
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 376
    if-eqz v9, :cond_c

    .line 377
    move-object/from16 v0, p0

    iget v4, v0, Labp;->d:I

    iget v5, v8, Labp$a;->leftMargin:I

    add-int/2addr v5, v2

    iget v6, v8, Labp$a;->rightMargin:I

    add-int/2addr v5, v6

    .line 378
    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Labp;->d:I

    .line 382
    :goto_b
    if-eqz v27, :cond_33

    .line 383
    invoke-static {v2, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v7, v11

    move v11, v12

    goto/16 :goto_4

    .line 372
    :cond_b
    const/4 v5, 0x0

    goto :goto_a

    .line 379
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Labp;->d:I

    .line 380
    add-int v5, v4, v2

    iget v6, v8, Labp$a;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v8, Labp$a;->rightMargin:I

    add-int/2addr v5, v6

    .line 381
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Labp;->d:I

    goto :goto_b

    .line 394
    :cond_d
    iget v3, v8, Labp$a;->h:I

    goto/16 :goto_6

    .line 399
    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_7

    :cond_f
    move v2, v6

    .line 401
    goto/16 :goto_8

    .line 402
    :cond_10
    if-eqz v2, :cond_11

    :goto_c
    move/from16 v0, v16

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v5, v13

    move v6, v3

    move v8, v2

    move v3, v11

    move v2, v7

    move v7, v15

    move v11, v12

    goto/16 :goto_9

    :cond_11
    move v5, v6

    goto :goto_c

    .line 405
    :cond_12
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    if-lez v2, :cond_13

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Labp;->b(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 406
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    move-object/from16 v0, p0

    iget v3, v0, Labp;->i:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 407
    :cond_13
    const/4 v2, 0x1

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_14

    const/4 v2, 0x0

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_14

    const/4 v2, 0x2

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_14

    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_31

    .line 408
    :cond_14
    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, 0x0

    aget v3, v24, v3

    const/4 v5, 0x1

    aget v5, v24, v5

    const/4 v6, 0x2

    aget v6, v24, v6

    .line 409
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 410
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 411
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 412
    const/4 v3, 0x3

    aget v3, v25, v3

    const/4 v5, 0x0

    aget v5, v25, v5

    const/4 v6, 0x1

    aget v6, v25, v6

    const/4 v7, 0x2

    aget v7, v25, v7

    .line 413
    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 414
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 415
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 416
    add-int/2addr v2, v3

    move/from16 v0, v18

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 417
    :goto_d
    if-eqz v27, :cond_19

    const/high16 v2, -0x80000000

    move/from16 v0, v22

    if-eq v0, v2, :cond_15

    if-nez v22, :cond_19

    .line 418
    :cond_15
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 419
    const/4 v3, 0x0

    :goto_e
    move/from16 v0, v21

    if-ge v3, v0, :cond_19

    .line 421
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 423
    if-nez v2, :cond_16

    .line 424
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    move v2, v3

    .line 437
    :goto_f
    add-int/lit8 v3, v2, 0x1

    goto :goto_e

    .line 426
    :cond_16
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_17

    move v2, v3

    .line 428
    goto :goto_f

    .line 430
    :cond_17
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Labp$a;

    .line 431
    if-eqz v9, :cond_18

    .line 432
    move-object/from16 v0, p0

    iget v6, v0, Labp;->d:I

    iget v7, v2, Labp$a;->leftMargin:I

    add-int/2addr v7, v11

    iget v2, v2, Labp$a;->rightMargin:I

    add-int/2addr v2, v7

    .line 433
    add-int/2addr v2, v6

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    move v2, v3

    goto :goto_f

    .line 434
    :cond_18
    move-object/from16 v0, p0

    iget v6, v0, Labp;->d:I

    .line 435
    add-int v7, v6, v11

    iget v8, v2, Labp$a;->leftMargin:I

    add-int/2addr v7, v8

    iget v2, v2, Labp$a;->rightMargin:I

    add-int/2addr v2, v7

    .line 436
    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    move v2, v3

    goto :goto_f

    .line 438
    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v6

    add-int/2addr v3, v6

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 439
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    .line 440
    invoke-virtual/range {p0 .. p0}, Labp;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 441
    const/4 v3, 0x0

    move/from16 v0, p1

    invoke-static {v2, v0, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v18

    .line 442
    const v2, 0xffffff

    and-int v2, v2, v18

    .line 443
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    sub-int v6, v2, v3

    .line 444
    if-nez v12, :cond_1a

    if-eqz v6, :cond_2a

    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-lez v2, :cond_2a

    .line 445
    :cond_1a
    move-object/from16 v0, p0

    iget v2, v0, Labp;->e:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1b

    move-object/from16 v0, p0

    iget v4, v0, Labp;->e:F

    .line 446
    :cond_1b
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, -0x1

    aput v8, v24, v7

    aput v8, v24, v5

    aput v8, v24, v3

    aput v8, v24, v2

    .line 447
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v7, 0x3

    const/4 v8, -0x1

    aput v8, v25, v7

    aput v8, v25, v5

    aput v8, v25, v3

    aput v8, v25, v2

    .line 448
    const/4 v7, -0x1

    .line 449
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 450
    const/4 v2, 0x0

    move v15, v2

    move v11, v14

    move/from16 v12, v16

    move v14, v7

    move/from16 v7, v17

    :goto_10
    move/from16 v0, v21

    if-ge v15, v0, :cond_26

    .line 452
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 454
    if-eqz v5, :cond_30

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_30

    .line 456
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Labp$a;

    .line 457
    iget v8, v2, Labp$a;->g:F

    .line 458
    const/4 v3, 0x0

    cmpl-float v3, v8, v3

    if-lez v3, :cond_2f

    .line 459
    int-to-float v3, v6

    mul-float/2addr v3, v8

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 460
    sub-float v8, v4, v8

    .line 461
    sub-int/2addr v6, v3

    .line 463
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v13

    add-int/2addr v4, v13

    iget v13, v2, Labp$a;->topMargin:I

    add-int/2addr v4, v13

    iget v13, v2, Labp$a;->bottomMargin:I

    add-int/2addr v4, v13

    iget v13, v2, Labp$a;->height:I

    .line 464
    move/from16 v0, p2

    invoke-static {v0, v4, v13}, Labp;->getChildMeasureSpec(III)I

    move-result v13

    .line 465
    iget v4, v2, Labp$a;->width:I

    if-nez v4, :cond_1c

    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v4, :cond_1f

    .line 466
    :cond_1c
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 467
    if-gez v3, :cond_1d

    .line 468
    const/4 v3, 0x0

    :cond_1d
    move-object v4, v5

    .line 471
    :goto_11
    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v3, v13}, Landroid/view/View;->measure(II)V

    .line 473
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredState()I

    move-result v3

    const/high16 v4, -0x1000000

    and-int/2addr v3, v4

    .line 474
    invoke-static {v7, v3}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v13

    move v7, v8

    move v8, v6

    .line 475
    :goto_12
    if-eqz v9, :cond_21

    .line 476
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v6, v2, Labp$a;->leftMargin:I

    add-int/2addr v4, v6

    iget v6, v2, Labp$a;->rightMargin:I

    add-int/2addr v4, v6

    .line 477
    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 481
    :goto_13
    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v3, :cond_22

    iget v3, v2, Labp$a;->height:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_22

    const/4 v3, 0x1

    .line 482
    :goto_14
    iget v4, v2, Labp$a;->topMargin:I

    iget v6, v2, Labp$a;->bottomMargin:I

    add-int/2addr v4, v6

    .line 483
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    .line 484
    invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 485
    if-eqz v3, :cond_23

    move v3, v4

    :goto_15
    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 486
    if-eqz v11, :cond_24

    iget v3, v2, Labp$a;->height:I

    const/4 v11, -0x1

    if-ne v3, v11, :cond_24

    const/4 v3, 0x1

    .line 487
    :goto_16
    if-eqz v26, :cond_1e

    .line 488
    invoke-virtual {v5}, Landroid/view/View;->getBaseline()I

    move-result v5

    .line 489
    const/4 v11, -0x1

    if-eq v5, v11, :cond_1e

    .line 490
    iget v11, v2, Labp$a;->h:I

    if-gez v11, :cond_25

    move-object/from16 v0, p0

    iget v2, v0, Labp;->g:I

    :goto_17
    and-int/lit8 v2, v2, 0x70

    .line 491
    shr-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, -0x2

    shr-int/lit8 v2, v2, 0x1

    .line 492
    aget v11, v24, v2

    invoke-static {v11, v5}, Ljava/lang/Math;->max(II)I

    move-result v11

    aput v11, v24, v2

    .line 493
    aget v11, v25, v2

    sub-int v5, v6, v5

    invoke-static {v11, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v25, v2

    :cond_1e
    move v2, v7

    move v5, v4

    move v6, v13

    move v4, v3

    move v7, v14

    move v3, v8

    .line 494
    :goto_18
    add-int/lit8 v8, v15, 0x1

    move v15, v8

    move v11, v4

    move v12, v5

    move v14, v7

    move v7, v6

    move v4, v2

    move v6, v3

    goto/16 :goto_10

    .line 471
    :cond_1f
    if-lez v3, :cond_20

    move-object v4, v5

    goto/16 :goto_11

    :cond_20
    const/4 v3, 0x0

    move-object v4, v5

    goto/16 :goto_11

    .line 478
    :cond_21
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    .line 479
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget v6, v2, Labp$a;->leftMargin:I

    add-int/2addr v4, v6

    iget v6, v2, Labp$a;->rightMargin:I

    add-int/2addr v4, v6

    .line 480
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    goto/16 :goto_13

    .line 481
    :cond_22
    const/4 v3, 0x0

    goto :goto_14

    :cond_23
    move v3, v6

    .line 485
    goto :goto_15

    .line 486
    :cond_24
    const/4 v3, 0x0

    goto :goto_16

    .line 490
    :cond_25
    iget v2, v2, Labp$a;->h:I

    goto :goto_17

    .line 495
    :cond_26
    move-object/from16 v0, p0

    iget v2, v0, Labp;->d:I

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Labp;->d:I

    .line 496
    const/4 v2, 0x1

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_27

    const/4 v2, 0x0

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_27

    const/4 v2, 0x2

    aget v2, v24, v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_27

    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_28

    .line 497
    :cond_27
    const/4 v2, 0x3

    aget v2, v24, v2

    const/4 v3, 0x0

    aget v3, v24, v3

    const/4 v4, 0x1

    aget v4, v24, v4

    const/4 v5, 0x2

    aget v5, v24, v5

    .line 498
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 499
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 500
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 501
    const/4 v3, 0x3

    aget v3, v25, v3

    const/4 v4, 0x0

    aget v4, v25, v4

    const/4 v5, 0x1

    aget v5, v25, v5

    const/4 v6, 0x2

    aget v6, v25, v6

    .line 502
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 503
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 504
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 505
    add-int/2addr v2, v3

    invoke-static {v14, v2}, Ljava/lang/Math;->max(II)I

    move-result v14

    :cond_28
    move v2, v12

    move/from16 v17, v7

    move v3, v14

    move v14, v11

    .line 523
    :goto_19
    if-nez v14, :cond_2d

    const/high16 v4, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v4, :cond_2d

    .line 525
    :goto_1a
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 526
    invoke-virtual/range {p0 .. p0}, Labp;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 527
    const/high16 v3, -0x1000000

    and-int v3, v3, v17

    or-int v3, v3, v18

    shl-int/lit8 v4, v17, 0x10

    .line 528
    move/from16 v0, p2

    invoke-static {v2, v0, v4}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v2

    .line 529
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Labp;->setMeasuredDimension(II)V

    .line 530
    if-eqz v10, :cond_2c

    .line 532
    invoke-virtual/range {p0 .. p0}, Labp;->getMeasuredHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 533
    const/4 v2, 0x0

    move v9, v2

    :goto_1b
    move/from16 v0, v21

    if-ge v9, v0, :cond_2c

    .line 535
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 537
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_29

    .line 538
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Labp$a;

    .line 539
    iget v2, v8, Labp$a;->height:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_29

    .line 540
    iget v10, v8, Labp$a;->width:I

    .line 541
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iput v2, v8, Labp$a;->width:I

    .line 542
    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    invoke-virtual/range {v2 .. v7}, Labp;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 543
    iput v10, v8, Labp$a;->width:I

    .line 544
    :cond_29
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_1b

    .line 507
    :cond_2a
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 508
    if-eqz v27, :cond_2e

    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v2, :cond_2e

    .line 509
    const/4 v2, 0x0

    move v3, v2

    :goto_1c
    move/from16 v0, v21

    if-ge v3, v0, :cond_2e

    .line 511
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 513
    if-eqz v4, :cond_2b

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v6, 0x8

    if-eq v2, v6, :cond_2b

    .line 515
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Labp$a;

    .line 516
    iget v2, v2, Labp$a;->g:F

    .line 517
    const/4 v6, 0x0

    cmpl-float v2, v2, v6

    if-lez v2, :cond_2b

    .line 518
    const/high16 v2, 0x40000000    # 2.0f

    .line 519
    invoke-static {v11, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 520
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 521
    invoke-virtual {v4, v2, v6}, Landroid/view/View;->measure(II)V

    .line 522
    :cond_2b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1c

    .line 545
    :cond_2c
    return-void

    :cond_2d
    move v2, v3

    goto/16 :goto_1a

    :cond_2e
    move v2, v12

    move v3, v5

    goto/16 :goto_19

    :cond_2f
    move v8, v6

    move v13, v7

    move v7, v4

    goto/16 :goto_12

    :cond_30
    move v2, v4

    move v3, v6

    move v5, v12

    move v4, v11

    move v6, v7

    move v7, v14

    goto/16 :goto_18

    :cond_31
    move/from16 v5, v18

    goto/16 :goto_d

    :cond_32
    move v4, v10

    goto/16 :goto_5

    :cond_33
    move v7, v11

    move v11, v12

    goto/16 :goto_4

    :cond_34
    move v2, v11

    move v3, v12

    move v5, v4

    move v6, v14

    move v7, v15

    move/from16 v8, v16

    move v4, v10

    move/from16 v11, v18

    move/from16 v10, v17

    goto/16 :goto_9
.end method

.method private b(Landroid/graphics/Canvas;I)V
    .locals 5

    .prologue
    .line 106
    iget-object v0, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Labp;->getPaddingTop()I

    move-result v1

    iget v2, p0, Labp;->o:I

    add-int/2addr v1, v2

    iget v2, p0, Labp;->i:I

    add-int/2addr v2, p2

    .line 107
    invoke-virtual {p0}, Labp;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Labp;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Labp;->o:I

    sub-int/2addr v3, v4

    .line 108
    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 109
    iget-object v0, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 110
    return-void
.end method

.method private static b(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 664
    add-int v0, p1, p3

    add-int v1, p2, p4

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 665
    return-void
.end method


# virtual methods
.method public a(Landroid/util/AttributeSet;)Labp$a;
    .locals 2

    .prologue
    .line 678
    new-instance v0, Labp$a;

    invoke-virtual {p0}, Labp;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Labp$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public b(Landroid/view/ViewGroup$LayoutParams;)Labp$a;
    .locals 1

    .prologue
    .line 684
    new-instance v0, Labp$a;

    invoke-direct {v0, p1}, Labp$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public final b(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 293
    if-nez p1, :cond_2

    .line 294
    iget v2, p0, Labp;->n:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 294
    goto :goto_0

    .line 295
    :cond_2
    invoke-virtual {p0}, Labp;->getChildCount()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 296
    iget v2, p0, Labp;->n:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 297
    :cond_3
    iget v2, p0, Labp;->n:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 299
    add-int/lit8 v2, p1, -0x1

    :goto_1
    if-ltz v2, :cond_5

    .line 300
    invoke-virtual {p0, v2}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    .line 303
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 305
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 666
    iget v0, p0, Labp;->c:I

    if-eq v0, p1, :cond_0

    .line 667
    iput p1, p0, Labp;->c:I

    .line 668
    invoke-virtual {p0}, Labp;->requestLayout()V

    .line 669
    :cond_0
    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 685
    instance-of v0, p1, Labp$a;

    return v0
.end method

.method public d()Labp$a;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 679
    iget v0, p0, Labp;->c:I

    if-nez v0, :cond_0

    .line 680
    new-instance v0, Labp$a;

    invoke-direct {v0, v2, v2}, Labp$a;-><init>(II)V

    .line 683
    :goto_0
    return-object v0

    .line 681
    :cond_0
    iget v0, p0, Labp;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 682
    new-instance v0, Labp$a;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Labp$a;-><init>(II)V

    goto :goto_0

    .line 683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 670
    iget v0, p0, Labp;->g:I

    if-eq v0, p1, :cond_1

    .line 671
    const v0, 0x800007

    and-int/2addr v0, p1

    if-nez v0, :cond_2

    .line 672
    const v0, 0x800003

    or-int/2addr v0, p1

    .line 673
    :goto_0
    and-int/lit8 v1, v0, 0x70

    if-nez v1, :cond_0

    .line 674
    or-int/lit8 v0, v0, 0x30

    .line 675
    :cond_0
    iput v0, p0, Labp;->g:I

    .line 676
    invoke-virtual {p0}, Labp;->requestLayout()V

    .line 677
    :cond_1
    return-void

    :cond_2
    move v0, p1

    goto :goto_0
.end method

.method public synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 692
    invoke-virtual {p0}, Labp;->d()Labp$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 694
    invoke-virtual {p0, p1}, Labp;->a(Landroid/util/AttributeSet;)Labp$a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 693
    invoke-virtual {p0, p1}, Labp;->b(Landroid/view/ViewGroup$LayoutParams;)Labp$a;

    move-result-object v0

    return-object v0
.end method

.method public getBaseline()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 111
    iget v1, p0, Labp;->a:I

    if-gez v1, :cond_1

    .line 112
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result v0

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    invoke-virtual {p0}, Labp;->getChildCount()I

    move-result v1

    iget v2, p0, Labp;->a:I

    if-gt v1, v2, :cond_2

    .line 114
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_2
    iget v1, p0, Labp;->a:I

    invoke-virtual {p0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 116
    invoke-virtual {v2}, Landroid/view/View;->getBaseline()I

    move-result v3

    .line 117
    if-ne v3, v0, :cond_3

    .line 118
    iget v1, p0, Labp;->a:I

    if-eqz v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_3
    iget v0, p0, Labp;->b:I

    .line 122
    iget v1, p0, Labp;->c:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_4

    .line 123
    iget v1, p0, Labp;->g:I

    and-int/lit8 v1, v1, 0x70

    .line 124
    const/16 v4, 0x30

    if-eq v1, v4, :cond_4

    .line 125
    sparse-switch v1, :sswitch_data_0

    :cond_4
    move v1, v0

    .line 129
    :goto_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Labp$a;

    .line 130
    iget v0, v0, Labp$a;->topMargin:I

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    goto :goto_0

    .line 126
    :sswitch_0
    invoke-virtual {p0}, Labp;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Labp;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Labp;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Labp;->d:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 127
    goto :goto_1

    .line 128
    :sswitch_1
    invoke-virtual {p0}, Labp;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Labp;->getTop()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p0}, Labp;->getPaddingTop()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p0}, Labp;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    iget v4, p0, Labp;->d:I

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    .line 125
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Labp;->h:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    iget v1, p0, Labp;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 48
    invoke-virtual {p0}, Labp;->getChildCount()I

    move-result v2

    move v1, v0

    .line 50
    :goto_1
    if-ge v1, v2, :cond_3

    .line 52
    invoke-virtual {p0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 54
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_2

    .line 55
    invoke-virtual {p0, v1}, Labp;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Labp$a;

    .line 57
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget v0, v0, Labp$a;->topMargin:I

    sub-int v0, v3, v0

    iget v3, p0, Labp;->m:I

    sub-int/2addr v0, v3

    .line 58
    invoke-direct {p0, p1, v0}, Labp;->a(Landroid/graphics/Canvas;I)V

    .line 59
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 60
    :cond_3
    invoke-virtual {p0, v2}, Labp;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    add-int/lit8 v0, v2, -0x1

    .line 62
    invoke-virtual {p0, v0}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 64
    if-nez v1, :cond_4

    .line 65
    invoke-virtual {p0}, Labp;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Labp;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Labp;->m:I

    sub-int/2addr v0, v1

    .line 68
    :goto_2
    invoke-direct {p0, p1, v0}, Labp;->a(Landroid/graphics/Canvas;I)V

    goto :goto_0

    .line 66
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Labp$a;

    .line 67
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, Labp$a;->bottomMargin:I

    add-int/2addr v0, v1

    goto :goto_2

    .line 72
    :cond_5
    invoke-virtual {p0}, Labp;->getChildCount()I

    move-result v2

    .line 74
    invoke-static {p0}, Laei;->a(Landroid/view/View;)Z

    move-result v3

    move v1, v0

    .line 75
    :goto_3
    if-ge v1, v2, :cond_8

    .line 77
    invoke-virtual {p0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 79
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_6

    .line 80
    invoke-virtual {p0, v1}, Labp;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Labp$a;

    .line 82
    if-eqz v3, :cond_7

    .line 83
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget v0, v0, Labp$a;->rightMargin:I

    add-int/2addr v0, v4

    .line 85
    :goto_4
    invoke-direct {p0, p1, v0}, Labp;->b(Landroid/graphics/Canvas;I)V

    .line 86
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 84
    :cond_7
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v0, v0, Labp$a;->leftMargin:I

    sub-int v0, v4, v0

    iget v4, p0, Labp;->i:I

    sub-int/2addr v0, v4

    goto :goto_4

    .line 87
    :cond_8
    invoke-virtual {p0, v2}, Labp;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    add-int/lit8 v0, v2, -0x1

    .line 89
    invoke-virtual {p0, v0}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 91
    if-nez v1, :cond_a

    .line 92
    if-eqz v3, :cond_9

    .line 93
    invoke-virtual {p0}, Labp;->getPaddingLeft()I

    move-result v0

    .line 99
    :goto_5
    invoke-direct {p0, p1, v0}, Labp;->b(Landroid/graphics/Canvas;I)V

    goto/16 :goto_0

    .line 94
    :cond_9
    invoke-virtual {p0}, Labp;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Labp;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Labp;->i:I

    sub-int/2addr v0, v1

    goto :goto_5

    .line 95
    :cond_a
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Labp$a;

    .line 96
    if-eqz v3, :cond_b

    .line 97
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v0, v0, Labp$a;->leftMargin:I

    sub-int v0, v1, v0

    iget v1, p0, Labp;->i:I

    sub-int/2addr v0, v1

    goto :goto_5

    .line 98
    :cond_b
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v0, v0, Labp$a;->rightMargin:I

    add-int/2addr v0, v1

    goto :goto_5
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 686
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 687
    const-class v0, Labp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 688
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 689
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 690
    const-class v0, Labp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 691
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 23

    .prologue
    .line 548
    move-object/from16 v0, p0

    iget v3, v0, Labp;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 550
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v8

    .line 551
    sub-int v3, p4, p2

    .line 552
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v4

    sub-int v9, v3, v4

    .line 553
    sub-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v4

    sub-int v10, v3, v4

    .line 555
    invoke-virtual/range {p0 .. p0}, Labp;->getChildCount()I

    move-result v11

    .line 557
    move-object/from16 v0, p0

    iget v3, v0, Labp;->g:I

    and-int/lit8 v3, v3, 0x70

    .line 558
    move-object/from16 v0, p0

    iget v4, v0, Labp;->g:I

    const v5, 0x800007

    and-int/2addr v5, v4

    .line 559
    sparse-switch v3, :sswitch_data_0

    .line 564
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v3

    .line 565
    :goto_0
    const/4 v7, 0x0

    move v6, v3

    :goto_1
    if-ge v7, v11, :cond_7

    .line 567
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 569
    if-nez v12, :cond_0

    move v3, v7

    .line 595
    :goto_2
    add-int/lit8 v7, v3, 0x1

    goto :goto_1

    .line 560
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v3

    add-int v3, v3, p5

    sub-int v3, v3, p3

    move-object/from16 v0, p0

    iget v4, v0, Labp;->d:I

    sub-int/2addr v3, v4

    .line 561
    goto :goto_0

    .line 562
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v3

    sub-int v4, p5, p3

    move-object/from16 v0, p0

    iget v6, v0, Labp;->d:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 563
    goto :goto_0

    .line 571
    :cond_0
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_c

    .line 572
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 573
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    .line 575
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Labp$a;

    .line 576
    iget v4, v3, Labp$a;->h:I

    .line 577
    if-gez v4, :cond_1

    move v4, v5

    .line 580
    :cond_1
    sget-object v15, Lqy;->a:Lri;

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lri;->j(Landroid/view/View;)I

    move-result v15

    .line 582
    invoke-static {v4, v15}, Lbw;->a(II)I

    move-result v4

    .line 583
    and-int/lit8 v4, v4, 0x7

    sparse-switch v4, :sswitch_data_1

    .line 588
    iget v4, v3, Labp$a;->leftMargin:I

    add-int/2addr v4, v8

    .line 589
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Labp;->b(I)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 590
    move-object/from16 v0, p0

    iget v15, v0, Labp;->m:I

    add-int/2addr v6, v15

    .line 591
    :cond_2
    iget v15, v3, Labp$a;->topMargin:I

    add-int/2addr v6, v15

    .line 592
    invoke-static {v12, v4, v6, v13, v14}, Labp;->b(Landroid/view/View;IIII)V

    .line 593
    iget v3, v3, Labp$a;->bottomMargin:I

    add-int/2addr v3, v14

    add-int/2addr v6, v3

    move v3, v7

    .line 594
    goto :goto_2

    .line 584
    :sswitch_2
    sub-int v4, v10, v13

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v8

    iget v15, v3, Labp$a;->leftMargin:I

    add-int/2addr v4, v15

    iget v15, v3, Labp$a;->rightMargin:I

    sub-int/2addr v4, v15

    .line 585
    goto :goto_3

    .line 586
    :sswitch_3
    sub-int v4, v9, v13

    iget v15, v3, Labp$a;->rightMargin:I

    sub-int/2addr v4, v15

    .line 587
    goto :goto_3

    .line 598
    :cond_3
    invoke-static/range {p0 .. p0}, Laei;->a(Landroid/view/View;)Z

    move-result v5

    .line 599
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v8

    .line 600
    sub-int v3, p5, p3

    .line 601
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v4

    sub-int v12, v3, v4

    .line 602
    sub-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v4

    sub-int v13, v3, v4

    .line 604
    invoke-virtual/range {p0 .. p0}, Labp;->getChildCount()I

    move-result v14

    .line 606
    move-object/from16 v0, p0

    iget v3, v0, Labp;->g:I

    const v4, 0x800007

    and-int/2addr v3, v4

    .line 607
    move-object/from16 v0, p0

    iget v4, v0, Labp;->g:I

    and-int/lit8 v11, v4, 0x70

    .line 608
    move-object/from16 v0, p0

    iget-boolean v15, v0, Labp;->f:Z

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Labp;->k:[I

    move-object/from16 v16, v0

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Labp;->l:[I

    move-object/from16 v17, v0

    .line 612
    sget-object v4, Lqy;->a:Lri;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lri;->j(Landroid/view/View;)I

    move-result v4

    .line 614
    invoke-static {v3, v4}, Lbw;->a(II)I

    move-result v3

    sparse-switch v3, :sswitch_data_2

    .line 619
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v9

    .line 620
    :goto_4
    const/4 v4, 0x0

    .line 621
    const/4 v3, 0x1

    .line 622
    if-eqz v5, :cond_b

    .line 623
    add-int/lit8 v4, v14, -0x1

    .line 624
    const/4 v3, -0x1

    move v5, v4

    move v4, v3

    .line 625
    :goto_5
    const/4 v10, 0x0

    :goto_6
    if-ge v10, v14, :cond_7

    .line 626
    mul-int v3, v4, v10

    add-int v18, v5, v3

    .line 628
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    .line 630
    if-nez v19, :cond_4

    move v3, v10

    .line 662
    :goto_7
    add-int/lit8 v10, v3, 0x1

    goto :goto_6

    .line 615
    :sswitch_4
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v3

    add-int v3, v3, p4

    sub-int v3, v3, p2

    move-object/from16 v0, p0

    iget v4, v0, Labp;->d:I

    sub-int v9, v3, v4

    .line 616
    goto :goto_4

    .line 617
    :sswitch_5
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v3

    sub-int v4, p4, p2

    move-object/from16 v0, p0

    iget v6, v0, Labp;->d:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    add-int v9, v3, v4

    .line 618
    goto :goto_4

    .line 632
    :cond_4
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_a

    .line 633
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v20

    .line 634
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    .line 635
    const/4 v6, -0x1

    .line 637
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Labp$a;

    .line 638
    if-eqz v15, :cond_5

    iget v7, v3, Labp$a;->height:I

    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v7, v0, :cond_5

    .line 639
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getBaseline()I

    move-result v6

    .line 640
    :cond_5
    iget v7, v3, Labp$a;->h:I

    .line 641
    if-gez v7, :cond_6

    move v7, v11

    .line 643
    :cond_6
    and-int/lit8 v7, v7, 0x70

    sparse-switch v7, :sswitch_data_3

    move v6, v8

    .line 655
    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Labp;->b(I)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 656
    move-object/from16 v0, p0

    iget v7, v0, Labp;->i:I

    add-int/2addr v7, v9

    .line 657
    :goto_9
    iget v9, v3, Labp$a;->leftMargin:I

    add-int/2addr v7, v9

    .line 658
    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v7, v6, v1, v2}, Labp;->b(Landroid/view/View;IIII)V

    .line 659
    iget v3, v3, Labp$a;->rightMargin:I

    add-int v3, v3, v20

    .line 660
    add-int v9, v7, v3

    move v3, v10

    .line 661
    goto :goto_7

    .line 644
    :sswitch_6
    iget v7, v3, Labp$a;->topMargin:I

    add-int/2addr v7, v8

    .line 645
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_9

    .line 646
    const/16 v22, 0x1

    aget v22, v16, v22

    sub-int v6, v22, v6

    add-int/2addr v6, v7

    goto :goto_8

    .line 647
    :sswitch_7
    sub-int v6, v13, v21

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v8

    iget v7, v3, Labp$a;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v3, Labp$a;->bottomMargin:I

    sub-int/2addr v6, v7

    .line 648
    goto :goto_8

    .line 649
    :sswitch_8
    sub-int v7, v12, v21

    iget v0, v3, Labp$a;->bottomMargin:I

    move/from16 v22, v0

    sub-int v7, v7, v22

    .line 650
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v6, v0, :cond_9

    .line 651
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    sub-int v6, v22, v6

    .line 652
    const/16 v22, 0x2

    aget v22, v17, v22

    sub-int v6, v22, v6

    sub-int v6, v7, v6

    .line 653
    goto :goto_8

    .line 663
    :cond_7
    return-void

    :cond_8
    move v7, v9

    goto :goto_9

    :cond_9
    move v6, v7

    goto :goto_8

    :cond_a
    move v3, v10

    goto/16 :goto_7

    :cond_b
    move v5, v4

    move v4, v3

    goto/16 :goto_5

    :cond_c
    move v3, v7

    goto/16 :goto_2

    .line 559
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch

    .line 583
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x5 -> :sswitch_3
    .end sparse-switch

    .line 614
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_5
        0x5 -> :sswitch_4
    .end sparse-switch

    .line 643
    :sswitch_data_3
    .sparse-switch
        0x10 -> :sswitch_7
        0x30 -> :sswitch_6
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .locals 27

    .prologue
    .line 131
    move-object/from16 v0, p0

    iget v3, v0, Labp;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1f

    .line 133
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 134
    const/16 v18, 0x0

    .line 135
    const/16 v17, 0x0

    .line 136
    const/16 v16, 0x0

    .line 137
    const/4 v15, 0x0

    .line 138
    const/4 v14, 0x1

    .line 139
    const/4 v5, 0x0

    .line 141
    invoke-virtual/range {p0 .. p0}, Labp;->getChildCount()I

    move-result v21

    .line 143
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v22

    .line 144
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 145
    const/4 v10, 0x0

    .line 146
    const/4 v12, 0x0

    .line 147
    move-object/from16 v0, p0

    iget v0, v0, Labp;->a:I

    move/from16 v24, v0

    .line 148
    move-object/from16 v0, p0

    iget-boolean v0, v0, Labp;->j:Z

    move/from16 v25, v0

    .line 149
    const/4 v11, 0x0

    .line 150
    const/16 v19, 0x0

    :goto_0
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_c

    .line 152
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 154
    if-nez v4, :cond_0

    .line 155
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    move/from16 v3, v19

    .line 199
    :goto_1
    add-int/lit8 v19, v3, 0x1

    goto :goto_0

    .line 157
    :cond_0
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_26

    .line 158
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Labp;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 159
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Labp;->m:I

    add-int/2addr v3, v6

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 160
    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Labp$a;

    .line 161
    iget v3, v9, Labp$a;->g:F

    add-float v13, v5, v3

    .line 162
    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-ne v0, v3, :cond_3

    iget v3, v9, Labp$a;->height:I

    if-nez v3, :cond_3

    iget v3, v9, Labp$a;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_3

    .line 163
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    .line 164
    iget v5, v9, Labp$a;->topMargin:I

    add-int/2addr v5, v3

    iget v6, v9, Labp$a;->bottomMargin:I

    add-int/2addr v5, v6

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 165
    const/4 v12, 0x1

    move v8, v11

    move v11, v12

    .line 180
    :goto_2
    if-ltz v24, :cond_2

    add-int/lit8 v3, v19, 0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_2

    .line 181
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    move-object/from16 v0, p0

    iput v3, v0, Labp;->b:I

    .line 182
    :cond_2
    move/from16 v0, v19

    move/from16 v1, v24

    if-ge v0, v1, :cond_7

    iget v3, v9, Labp$a;->g:F

    const/4 v5, 0x0

    cmpl-float v3, v3, v5

    if-lez v3, :cond_7

    .line 183
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 167
    :cond_3
    const/high16 v3, -0x80000000

    .line 168
    iget v5, v9, Labp$a;->height:I

    if-nez v5, :cond_4

    iget v5, v9, Labp$a;->g:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    .line 169
    const/4 v3, 0x0

    .line 170
    const/4 v5, -0x2

    iput v5, v9, Labp$a;->height:I

    :cond_4
    move/from16 v20, v3

    .line 171
    const/4 v6, 0x0

    const/4 v3, 0x0

    cmpl-float v3, v13, v3

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget v8, v0, Labp;->d:I

    :goto_3
    move-object/from16 v3, p0

    move/from16 v5, p1

    move/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Labp;->a(Landroid/view/View;IIII)V

    .line 172
    const/high16 v3, -0x80000000

    move/from16 v0, v20

    if-eq v0, v3, :cond_5

    .line 173
    move/from16 v0, v20

    iput v0, v9, Labp$a;->height:I

    .line 174
    :cond_5
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 175
    move-object/from16 v0, p0

    iget v5, v0, Labp;->d:I

    .line 176
    add-int v6, v5, v3

    iget v7, v9, Labp$a;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v9, Labp$a;->bottomMargin:I

    add-int/2addr v6, v7

    .line 177
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Labp;->d:I

    .line 178
    if-eqz v25, :cond_25

    .line 179
    invoke-static {v3, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v8, v11

    move v11, v12

    goto :goto_2

    .line 171
    :cond_6
    const/4 v8, 0x0

    goto :goto_3

    .line 184
    :cond_7
    const/4 v3, 0x0

    .line 185
    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_24

    iget v5, v9, Labp$a;->width:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_24

    .line 186
    const/4 v5, 0x1

    .line 187
    const/4 v3, 0x1

    .line 188
    :goto_4
    iget v6, v9, Labp$a;->leftMargin:I

    iget v7, v9, Labp$a;->rightMargin:I

    add-int/2addr v6, v7

    .line 189
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v6

    .line 190
    move/from16 v0, v18

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v12

    .line 192
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredState()I

    move-result v4

    .line 193
    move/from16 v0, v17

    invoke-static {v0, v4}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 194
    if-eqz v14, :cond_8

    iget v4, v9, Labp$a;->width:I

    const/4 v14, -0x1

    if-ne v4, v14, :cond_8

    const/4 v4, 0x1

    .line 195
    :goto_5
    iget v9, v9, Labp$a;->g:F

    const/4 v14, 0x0

    cmpl-float v9, v9, v14

    if-lez v9, :cond_a

    .line 196
    if-eqz v3, :cond_9

    move v3, v6

    :goto_6
    invoke-static {v15, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move/from16 v9, v16

    move v4, v11

    move v11, v12

    move/from16 v26, v8

    move v8, v3

    move/from16 v3, v26

    :goto_7
    move v12, v4

    move v14, v7

    move v15, v8

    move/from16 v16, v9

    move/from16 v17, v10

    move/from16 v18, v11

    move v10, v5

    move v11, v3

    move v5, v6

    move/from16 v3, v19

    .line 198
    goto/16 :goto_1

    .line 194
    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    :cond_9
    move v3, v7

    .line 196
    goto :goto_6

    .line 197
    :cond_a
    if-eqz v3, :cond_b

    :goto_8
    move/from16 v0, v16

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v6, v13

    move v7, v4

    move v9, v3

    move v4, v11

    move v3, v8

    move v8, v15

    move v11, v12

    goto :goto_7

    :cond_b
    move v6, v7

    goto :goto_8

    .line 200
    :cond_c
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    if-lez v3, :cond_d

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Labp;->b(I)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 201
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    move-object/from16 v0, p0

    iget v4, v0, Labp;->m:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 202
    :cond_d
    if-eqz v25, :cond_11

    const/high16 v3, -0x80000000

    move/from16 v0, v23

    if-eq v0, v3, :cond_e

    if-nez v23, :cond_11

    .line 203
    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 204
    const/4 v4, 0x0

    :goto_9
    move/from16 v0, v21

    if-ge v4, v0, :cond_11

    .line 206
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 208
    if-nez v3, :cond_f

    .line 209
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    move v3, v4

    .line 219
    :goto_a
    add-int/lit8 v4, v3, 0x1

    goto :goto_9

    .line 211
    :cond_f
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_10

    move v3, v4

    .line 213
    goto :goto_a

    .line 215
    :cond_10
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Labp$a;

    .line 216
    move-object/from16 v0, p0

    iget v6, v0, Labp;->d:I

    .line 217
    add-int v7, v6, v11

    iget v8, v3, Labp$a;->topMargin:I

    add-int/2addr v7, v8

    iget v3, v3, Labp$a;->bottomMargin:I

    add-int/2addr v3, v7

    .line 218
    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    move v3, v4

    goto :goto_a

    .line 220
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v6

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 221
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    .line 222
    invoke-virtual/range {p0 .. p0}, Labp;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 223
    const/4 v4, 0x0

    move/from16 v0, p2

    invoke-static {v3, v0, v4}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v19

    .line 224
    const v3, 0xffffff

    and-int v3, v3, v19

    .line 225
    move-object/from16 v0, p0

    iget v4, v0, Labp;->d:I

    sub-int v6, v3, v4

    .line 226
    if-nez v12, :cond_12

    if-eqz v6, :cond_1d

    const/4 v3, 0x0

    cmpl-float v3, v5, v3

    if-lez v3, :cond_1d

    .line 227
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Labp;->e:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_13

    move-object/from16 v0, p0

    iget v5, v0, Labp;->e:F

    .line 228
    :cond_13
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    .line 229
    const/4 v3, 0x0

    move v15, v3

    move v12, v14

    move/from16 v13, v16

    move/from16 v11, v17

    move/from16 v14, v18

    :goto_b
    move/from16 v0, v21

    if-ge v15, v0, :cond_1b

    .line 231
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 233
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_23

    .line 234
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Labp$a;

    .line 235
    iget v8, v3, Labp$a;->g:F

    .line 236
    const/4 v4, 0x0

    cmpl-float v4, v8, v4

    if-lez v4, :cond_22

    .line 237
    int-to-float v4, v6

    mul-float/2addr v4, v8

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 238
    sub-float v8, v5, v8

    .line 239
    sub-int v9, v6, v4

    .line 241
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v3, Labp$a;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Labp$a;->rightMargin:I

    add-int/2addr v5, v6

    iget v6, v3, Labp$a;->width:I

    .line 242
    move/from16 v0, p1

    invoke-static {v0, v5, v6}, Labp;->getChildMeasureSpec(III)I

    move-result v5

    .line 243
    iget v6, v3, Labp$a;->height:I

    if-nez v6, :cond_14

    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_16

    .line 244
    :cond_14
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v4, v6

    .line 245
    if-gez v4, :cond_15

    .line 246
    const/4 v4, 0x0

    :cond_15
    move-object v6, v7

    .line 249
    :goto_c
    const/high16 v16, 0x40000000    # 2.0f

    .line 250
    move/from16 v0, v16

    invoke-static {v4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 251
    invoke-virtual {v6, v5, v4}, Landroid/view/View;->measure(II)V

    .line 253
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredState()I

    move-result v4

    and-int/lit16 v4, v4, -0x100

    .line 254
    invoke-static {v11, v4}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v4

    move v5, v9

    move v6, v4

    move v4, v8

    .line 255
    :goto_d
    iget v8, v3, Labp$a;->leftMargin:I

    iget v9, v3, Labp$a;->rightMargin:I

    add-int/2addr v8, v9

    .line 256
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v8

    .line 257
    invoke-static {v14, v9}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 258
    const/high16 v14, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v14, :cond_18

    iget v14, v3, Labp$a;->width:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v14, v0, :cond_18

    const/4 v14, 0x1

    .line 259
    :goto_e
    if-eqz v14, :cond_19

    :goto_f
    invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 260
    if-eqz v12, :cond_1a

    iget v8, v3, Labp$a;->width:I

    const/4 v12, -0x1

    if-ne v8, v12, :cond_1a

    const/4 v8, 0x1

    .line 261
    :goto_10
    move-object/from16 v0, p0

    iget v12, v0, Labp;->d:I

    .line 262
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v12

    iget v13, v3, Labp$a;->topMargin:I

    add-int/2addr v7, v13

    iget v3, v3, Labp$a;->bottomMargin:I

    add-int/2addr v3, v7

    .line 263
    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    move v3, v9

    move v7, v11

    .line 264
    :goto_11
    add-int/lit8 v9, v15, 0x1

    move v15, v9

    move v12, v8

    move v13, v3

    move v11, v6

    move v14, v7

    move v6, v5

    move v5, v4

    goto/16 :goto_b

    .line 249
    :cond_16
    if-lez v4, :cond_17

    move-object v6, v7

    goto :goto_c

    :cond_17
    const/4 v4, 0x0

    move-object v6, v7

    goto :goto_c

    .line 258
    :cond_18
    const/4 v14, 0x0

    goto :goto_e

    :cond_19
    move v8, v9

    .line 259
    goto :goto_f

    .line 260
    :cond_1a
    const/4 v8, 0x0

    goto :goto_10

    .line 265
    :cond_1b
    move-object/from16 v0, p0

    iget v3, v0, Labp;->d:I

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Labp;->d:I

    move v3, v13

    move/from16 v17, v11

    move v4, v14

    move v14, v12

    .line 283
    :goto_12
    if-nez v14, :cond_20

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v22

    if-eq v0, v5, :cond_20

    .line 285
    :goto_13
    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingLeft()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Labp;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 286
    invoke-virtual/range {p0 .. p0}, Labp;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 287
    move/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Labp;->setMeasuredDimension(II)V

    .line 288
    if-eqz v10, :cond_1c

    .line 289
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, p2

    invoke-direct {v0, v1, v2}, Labp;->a(II)V

    .line 292
    :cond_1c
    :goto_14
    return-void

    .line 267
    :cond_1d
    move/from16 v0, v16

    invoke-static {v0, v15}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 268
    if-eqz v25, :cond_21

    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v3, :cond_21

    .line 269
    const/4 v3, 0x0

    move v4, v3

    :goto_15
    move/from16 v0, v21

    if-ge v4, v0, :cond_21

    .line 271
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Labp;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 273
    if-eqz v5, :cond_1e

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v6, 0x8

    if-eq v3, v6, :cond_1e

    .line 275
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Labp$a;

    .line 276
    iget v3, v3, Labp$a;->g:F

    .line 277
    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1e

    .line 279
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    .line 280
    invoke-static {v11, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 281
    invoke-virtual {v5, v3, v6}, Landroid/view/View;->measure(II)V

    .line 282
    :cond_1e
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_15

    .line 291
    :cond_1f
    invoke-direct/range {p0 .. p2}, Labp;->b(II)V

    goto :goto_14

    :cond_20
    move v3, v4

    goto :goto_13

    :cond_21
    move v3, v13

    move/from16 v4, v18

    goto/16 :goto_12

    :cond_22
    move v4, v5

    move v5, v6

    move v6, v11

    goto/16 :goto_d

    :cond_23
    move v4, v5

    move v8, v12

    move v3, v13

    move v7, v14

    move v5, v6

    move v6, v11

    goto/16 :goto_11

    :cond_24
    move v5, v10

    goto/16 :goto_4

    :cond_25
    move v8, v11

    move v11, v12

    goto/16 :goto_2

    :cond_26
    move v3, v11

    move v4, v12

    move v6, v5

    move v7, v14

    move v8, v15

    move/from16 v9, v16

    move v5, v10

    move/from16 v11, v18

    move/from16 v10, v17

    goto/16 :goto_7
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
