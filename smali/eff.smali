.class public final Leff;
.super Ljava/lang/Object;

# interfaces
.implements Lefu;
.implements Lehp;


# instance fields
.field public final a:Ljava/util/concurrent/locks/Lock;

.field public final b:Ljava/util/concurrent/locks/Condition;

.field public final c:Landroid/content/Context;

.field public final d:Lecp;

.field public final e:Lefh;

.field public final f:Ljava/util/Map;

.field public final g:Ljava/util/Map;

.field public h:Lejm;

.field public i:Ljava/util/Map;

.field public j:Ledb;

.field public volatile k:Lefe;

.field public l:I

.field public final m:Leex;

.field public final n:Lefv;

.field private o:Lecl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;Lefv;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leff;->g:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Leff;->o:Lecl;

    iput-object p1, p0, Leff;->c:Landroid/content/Context;

    iput-object p3, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    iput-object p5, p0, Leff;->d:Lecp;

    iput-object p6, p0, Leff;->f:Ljava/util/Map;

    iput-object p7, p0, Leff;->h:Lejm;

    iput-object p8, p0, Leff;->i:Ljava/util/Map;

    iput-object p9, p0, Leff;->j:Ledb;

    iput-object p2, p0, Leff;->m:Leex;

    iput-object p11, p0, Leff;->n:Lefv;

    check-cast p10, Ljava/util/ArrayList;

    invoke-virtual {p10}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    check-cast v0, Leho;

    .line 2
    iput-object p0, v0, Leho;->b:Lehp;

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lefh;

    invoke-direct {v0, p0, p4}, Lefh;-><init>(Leff;Landroid/os/Looper;)V

    iput-object v0, p0, Leff;->e:Lefh;

    invoke-interface {p3}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Leff;->b:Ljava/util/concurrent/locks/Condition;

    new-instance v0, Leew;

    invoke-direct {v0, p0}, Leew;-><init>(Leff;)V

    iput-object v0, p0, Leff;->k:Lefe;

    return-void
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lecl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 7
    invoke-virtual {p0}, Leff;->a()V

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 8
    :goto_0
    iget-object v2, p0, Leff;->k:Lefe;

    instance-of v2, v2, Leek;

    .line 9
    if-eqz v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    :try_start_0
    invoke-virtual {p0}, Leff;->c()V

    new-instance v0, Lecl;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    :goto_1
    return-object v0

    :cond_0
    iget-object v2, p0, Leff;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Lecl;

    const/16 v1, 0xf

    invoke-direct {v0, v1, v4}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Leff;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lecl;->a:Lecl;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Leff;->o:Lecl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Leff;->o:Lecl;

    goto :goto_1

    :cond_3
    new-instance v0, Lecl;

    const/16 v1, 0xd

    invoke-direct {v0, v1, v4}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public final a(Lehe;)Lehe;
    .locals 1

    invoke-virtual {p1}, Lehk;->f()V

    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0, p1}, Lefe;->a(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0}, Lefe;->c()V

    return-void
.end method

.method final a(Lecl;)V
    .locals 2

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iput-object p1, p0, Leff;->o:Lecl;

    new-instance v0, Leew;

    invoke-direct {v0, p0}, Leew;-><init>(Leff;)V

    iput-object v0, p0, Leff;->k:Lefe;

    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0}, Lefe;->a()V

    iget-object v0, p0, Leff;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lecl;Lesq;Z)V
    .locals 2

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0, p1, p2, p3}, Lefe;->a(Lecl;Lesq;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final a(Lefg;)V
    .locals 2

    iget-object v0, p0, Leff;->e:Lefh;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lefh;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Leff;->e:Lefh;

    invoke-virtual {v1, v0}, Lefh;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "mState="

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, p0, Leff;->k:Lefe;

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Leff;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesq;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v3

    invoke-virtual {v0}, Lesq;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, p0, Leff;->f:Ljava/util/Map;

    invoke-virtual {v0}, Lesq;->c()Letf;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledd;

    invoke-interface {v0, v1, p3}, Ledd;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Legj;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lecl;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4
    invoke-virtual {p0}, Leff;->a()V

    .line 5
    :goto_0
    iget-object v0, p0, Leff;->k:Lefe;

    instance-of v0, v0, Leek;

    .line 6
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Leff;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Lecl;

    const/16 v1, 0xf

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Leff;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lecl;->a:Lecl;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Leff;->o:Lecl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leff;->o:Lecl;

    goto :goto_1

    :cond_2
    new-instance v0, Lecl;

    const/16 v1, 0xd

    invoke-direct {v0, v1, v2}, Lecl;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public final b(Lehe;)Lehe;
    .locals 1

    invoke-virtual {p1}, Lehk;->f()V

    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0, p1}, Lefe;->b(Lehe;)Lehe;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0}, Lefe;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leff;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Leff;->k:Lefe;

    instance-of v0, v0, Leeh;

    return v0
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final onConnected(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0, p1}, Lefe;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final onConnectionSuspended(I)V
    .locals 2

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leff;->k:Lefe;

    invoke-interface {v0, p1}, Lefe;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leff;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
