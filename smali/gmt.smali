.class public final Lgmt;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgmt;


# instance fields
.field public currentSpeakerParticipantId:Ljava/lang/String;

.field public fullScreenMode:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgmt;->clear()Lgmt;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgmt;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgmt;->_emptyArray:[Lgmt;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgmt;->_emptyArray:[Lgmt;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgmt;

    sput-object v0, Lgmt;->_emptyArray:[Lgmt;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgmt;->_emptyArray:[Lgmt;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgmt;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lgmt;

    invoke-direct {v0}, Lgmt;-><init>()V

    invoke-virtual {v0, p0}, Lgmt;->mergeFrom(Lhfp;)Lgmt;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgmt;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lgmt;

    invoke-direct {v0}, Lgmt;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgmt;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgmt;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;

    .line 12
    iput-object v0, p0, Lgmt;->unknownFieldData:Lhfv;

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lgmt;->cachedSize:I

    .line 14
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 21
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 22
    iget-object v1, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23
    const/4 v1, 0x1

    iget-object v2, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    .line 24
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25
    :cond_0
    iget-object v1, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 26
    const/4 v1, 0x2

    iget-object v2, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;

    .line 27
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28
    :cond_1
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgmt;
    .locals 3

    .prologue
    .line 29
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 30
    sparse-switch v0, :sswitch_data_0

    .line 32
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    :sswitch_0
    return-object p0

    .line 34
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    goto :goto_0

    .line 36
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v1

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v2

    .line 39
    invoke-static {v2}, Lgmk;->checkFullScreenOrThrow(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 42
    :catch_0
    move-exception v2

    invoke-virtual {p1, v1}, Lhfp;->e(I)V

    .line 43
    invoke-virtual {p0, p1, v0}, Lgmt;->storeUnknownField(Lhfp;I)Z

    goto :goto_0

    .line 30
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lgmt;->mergeFrom(Lhfp;)Lgmt;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    iget-object v1, p0, Lgmt;->currentSpeakerParticipantId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 17
    :cond_0
    iget-object v0, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 18
    const/4 v0, 0x2

    iget-object v1, p0, Lgmt;->fullScreenMode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 19
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 20
    return-void
.end method
