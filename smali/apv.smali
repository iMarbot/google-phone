.class public Lapv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    iput-object p1, p0, Lapv;->a:Landroid/content/Context;

    .line 173
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lapv;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lapa;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x3

    .line 154
    invoke-static {p0, v0}, Lcom/android/dialer/app/DialtactsActivity;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 155
    if-eqz p1, :cond_0

    .line 156
    iget-object v1, p1, Lapa;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 157
    :cond_0
    const-string v1, "EXTRA_CLEAR_NEW_VOICEMAILS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 158
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lapa;)Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 159
    if-eqz p0, :cond_0

    iget-object v0, p0, Lapa;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapa;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 160
    :cond_0
    const/4 v0, 0x0

    .line 163
    :goto_0
    return-object v0

    .line 161
    :cond_1
    new-instance v0, Landroid/telecom/PhoneAccountHandle;

    iget-object v1, p0, Lapa;->e:Ljava/lang/String;

    .line 162
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    iget-object v2, p0, Lapa;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/telecom/PhoneAccountHandle;-><init>(Landroid/content/ComponentName;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 139
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "VisualVoicemail_"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 136
    const-string v0, "VisualVoicemailNotifier.cancelAllVoicemailNotifications"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 137
    const-string v0, "VisualVoicemailGroup"

    invoke-static {p0, v0}, Lbib;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 1
    const-string v0, "VisualVoicemailNotifier.showNotifications"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    const-string v1, "com.android.dialer.calllog.ACTION_MARK_ALL_NEW_VOICEMAILS_AS_OLD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 6
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100004

    .line 10
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 11
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-static {p0}, Lapv;->b(Landroid/content/Context;)Lle;

    move-result-object v2

    .line 14
    invoke-virtual {v2, v1}, Lle;->a(Ljava/lang/CharSequence;)Lle;

    move-result-object v1

    .line 15
    invoke-virtual {v1, p3}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    move-result-object v1

    .line 16
    invoke-virtual {v1, v0}, Lle;->a(Landroid/app/PendingIntent;)Lle;

    move-result-object v1

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, v1, Lle;->k:Z

    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-static {p0, v0}, Lapv;->a(Landroid/content/Context;Lapa;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 22
    iput-object v0, v1, Lle;->e:Landroid/app/PendingIntent;

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v0, v2, :cond_0

    .line 26
    if-eqz p4, :cond_5

    .line 27
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lle;->a(Z)Lle;

    .line 28
    const/4 v0, 0x0

    .line 29
    iput v0, v1, Lle;->q:I

    .line 33
    :goto_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapa;

    invoke-static {v0}, Lapv;->a(Lapa;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 34
    invoke-static {p0, v0}, Lbib;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    .line 35
    iput-object v0, v1, Lle;->o:Ljava/lang/String;

    .line 36
    :cond_0
    const-string v0, "GroupSummary_VisualVoicemail"

    const/4 v2, 0x1

    .line 37
    invoke-virtual {v1}, Lle;->a()Landroid/app/Notification;

    move-result-object v1

    .line 38
    invoke-static {p0, v0, v2, v1}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    .line 39
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lapa;

    .line 42
    iget-object v0, v2, Lapa;->b:Landroid/net/Uri;

    invoke-static {v0}, Lapv;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 43
    const/4 v7, 0x1

    .line 45
    invoke-static {v2}, Lapv;->a(Lapa;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    .line 46
    iget-object v0, v2, Lapa;->c:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 48
    invoke-static {p0}, Lapv;->b(Landroid/content/Context;)Lle;

    move-result-object v1

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v8, 0x7f110241

    iget-object v9, v0, Lbml;->d:Ljava/lang/String;

    .line 50
    invoke-static {v3, v8, v9}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 51
    invoke-virtual {v1, v3}, Lle;->a(Ljava/lang/CharSequence;)Lle;

    move-result-object v1

    iget-wide v8, v2, Lapa;->i:J

    .line 52
    invoke-virtual {v1, v8, v9}, Lle;->a(J)Lle;

    move-result-object v8

    .line 54
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v1, v3, :cond_6

    .line 55
    const/4 v1, 0x0

    .line 63
    :goto_2
    invoke-virtual {v8, v1}, Lle;->a(Landroid/net/Uri;)Lle;

    move-result-object v8

    .line 65
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-ge v1, v3, :cond_8

    .line 66
    const/4 v1, -0x1

    .line 77
    :goto_3
    iget-object v3, v8, Lle;->r:Landroid/app/Notification;

    iput v1, v3, Landroid/app/Notification;->defaults:I

    .line 78
    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 79
    iget-object v1, v8, Lle;->r:Landroid/app/Notification;

    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 82
    :cond_1
    iget-object v1, v2, Lapa;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 83
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->bq:Lbkq$a;

    .line 84
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 85
    iget-object v1, v2, Lapa;->g:Ljava/lang/String;

    .line 86
    invoke-virtual {v8, v1}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    move-result-object v1

    new-instance v3, Lld;

    invoke-direct {v3}, Lld;-><init>()V

    iget-object v9, v2, Lapa;->g:Ljava/lang/String;

    .line 87
    invoke-virtual {v3, v9}, Lld;->a(Ljava/lang/CharSequence;)Lld;

    move-result-object v3

    invoke-virtual {v1, v3}, Lle;->a(Llf;)Lle;

    .line 111
    :goto_4
    iget-object v1, v2, Lapa;->b:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 112
    iget-object v1, v2, Lapa;->b:Landroid/net/Uri;

    .line 114
    new-instance v3, Landroid/content/Intent;

    const-class v9, Lcom/android/dialer/app/calllog/CallLogNotificationsService;

    invoke-direct {v3, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    const-string v9, "com.android.dialer.calllog.ACTION_MARK_SINGLE_NEW_VOICEMAIL_AS_OLD "

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 117
    const/4 v1, 0x0

    const/4 v9, 0x0

    invoke-static {p0, v1, v3, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 118
    invoke-virtual {v8, v1}, Lle;->a(Landroid/app/PendingIntent;)Lle;

    .line 119
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v1, v3, :cond_3

    .line 120
    invoke-static {p0, v4}, Lbib;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v1

    .line 121
    iput-object v1, v8, Lle;->o:Ljava/lang/String;

    .line 122
    const/4 v1, 0x1

    .line 123
    iput v1, v8, Lle;->q:I

    .line 124
    :cond_3
    new-instance v1, Laqi;

    invoke-direct {v1, p0, v0}, Laqi;-><init>(Landroid/content/Context;Lbml;)V

    .line 125
    invoke-virtual {v1}, Laqi;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_4

    .line 128
    iput-object v0, v8, Lle;->f:Landroid/graphics/Bitmap;

    .line 129
    :cond_4
    invoke-static {p0, v2}, Lapv;->a(Landroid/content/Context;Lapa;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 130
    iput-object v0, v8, Lle;->e:Landroid/app/PendingIntent;

    .line 131
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bp:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 132
    invoke-virtual {v8}, Lle;->a()Landroid/app/Notification;

    move-result-object v0

    .line 133
    invoke-static {p0, v6, v7, v0}, Lbib;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    .line 31
    :cond_5
    const/4 v0, 0x2

    .line 32
    iput v0, v1, Lle;->q:I

    goto/16 :goto_0

    .line 56
    :cond_6
    if-nez v4, :cond_e

    .line 57
    const-string v1, "VisualVoicemailNotifier.getVoicemailRingtoneUri"

    const-string v3, "null handle, getting fallback"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1, v3, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    invoke-static {p0}, Lapv;->c(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 59
    if-nez v1, :cond_7

    .line 60
    const-string v1, "VisualVoicemailNotifier.getVoicemailRingtoneUri"

    const-string v3, "no fallback handle, using null (default) ringtone"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1, v3, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_7
    move-object v3, v1

    .line 62
    :goto_5
    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v3}, Landroid/telephony/TelephonyManager;->getVoicemailRingtoneUri(Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_2

    .line 67
    :cond_8
    if-nez v4, :cond_d

    .line 68
    const-string v1, "VisualVoicemailNotifier.getNotificationDefaultFlags"

    const-string v3, "null handle, getting fallback"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1, v3, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-static {p0}, Lapv;->c(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 70
    if-nez v1, :cond_9

    .line 71
    const-string v1, "VisualVoicemailNotifier.getNotificationDefaultFlags"

    const-string v3, "no fallback handle, using default vibration"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1, v3, v9}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    const/4 v1, -0x1

    goto/16 :goto_3

    :cond_9
    move-object v3, v1

    .line 73
    :goto_6
    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v3}, Landroid/telephony/TelephonyManager;->isVoicemailVibrationEnabled(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 74
    const/4 v1, 0x2

    goto/16 :goto_3

    .line 75
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 88
    :cond_b
    iget v1, v2, Lapa;->j:I

    packed-switch v1, :pswitch_data_0

    .line 109
    :pswitch_0
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->du:Lbkq$a;

    .line 110
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    goto/16 :goto_4

    .line 89
    :pswitch_1
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->ds:Lbkq$a;

    .line 90
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 91
    const v1, 0x7f11038d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    goto/16 :goto_4

    .line 93
    :pswitch_2
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->dt:Lbkq$a;

    .line 94
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 95
    const v1, 0x7f11038c

    .line 96
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {v8, v1}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    goto/16 :goto_4

    .line 99
    :pswitch_3
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->dt:Lbkq$a;

    .line 100
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 101
    const v1, 0x7f11038b

    .line 102
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-virtual {v8, v1}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    goto/16 :goto_4

    .line 105
    :pswitch_4
    invoke-static {p0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v1

    sget-object v3, Lbkq$a;->dt:Lbkq$a;

    .line 106
    invoke-interface {v1, v3}, Lbku;->a(Lbkq$a;)V

    .line 107
    const v1, 0x7f11038a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lle;->b(Ljava/lang/CharSequence;)Lle;

    goto/16 :goto_4

    .line 135
    :cond_c
    return-void

    :cond_d
    move-object v3, v4

    goto :goto_6

    :cond_e
    move-object v3, v4

    goto/16 :goto_5

    .line 88
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;)Lle;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 140
    new-instance v0, Lle;

    invoke-direct {v0, p0}, Lle;-><init>(Landroid/content/Context;)V

    const v1, 0x108007e

    .line 141
    invoke-virtual {v0, v1}, Lle;->a(I)Lle;

    move-result-object v0

    const v1, 0x7f0c0071

    .line 142
    invoke-virtual {p0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v1

    .line 143
    iput v1, v0, Lle;->m:I

    .line 145
    const-string v1, "VisualVoicemailGroup"

    .line 147
    iput-object v1, v0, Lle;->j:Ljava/lang/String;

    .line 150
    invoke-virtual {v0, v2}, Lle;->a(Z)Lle;

    move-result-object v0

    .line 151
    invoke-virtual {v0, v2}, Lle;->b(Z)Lle;

    move-result-object v0

    .line 152
    return-object v0
.end method

.method private static c(Landroid/content/Context;)Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 164
    const-string v0, "tel"

    .line 165
    invoke-static {p0, v0}, Lbsp;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 166
    if-nez v0, :cond_0

    .line 167
    invoke-static {p0}, Lbsp;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 168
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 170
    :cond_0
    return-object v0
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lapv;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Void;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lapv;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lapv;->a:Landroid/content/Context;

    .line 176
    invoke-static {v0}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v0

    invoke-interface {v0}, Lbmn;->a()Lbmi;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 178
    iget-object v1, p0, Lapv;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lbmi;->a(Landroid/content/Context;)V

    .line 179
    :cond_0
    return-object v2
.end method
