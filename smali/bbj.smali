.class public final Lbbj;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final f:Lhcc;

.field public static final k:Lhcc;

.field public static final r:Lbbj;

.field private static volatile s:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Lhca;

.field public g:I

.field public h:J

.field public i:J

.field public j:Lhca;

.field public l:Lhcd;

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lbbk;

    invoke-direct {v0}, Lbbk;-><init>()V

    sput-object v0, Lbbj;->f:Lhcc;

    .line 259
    new-instance v0, Lbbl;

    invoke-direct {v0}, Lbbl;-><init>()V

    sput-object v0, Lbbj;->k:Lhcc;

    .line 260
    new-instance v0, Lbbj;

    invoke-direct {v0}, Lbbj;-><init>()V

    .line 261
    sput-object v0, Lbbj;->r:Lbbj;

    invoke-virtual {v0}, Lbbj;->makeImmutable()V

    .line 262
    const-class v0, Lbbj;

    sget-object v1, Lbbj;->r:Lbbj;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 263
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    invoke-static {}, Lbbj;->emptyIntList()Lhca;

    move-result-object v0

    iput-object v0, p0, Lbbj;->e:Lhca;

    .line 3
    invoke-static {}, Lbbj;->emptyIntList()Lhca;

    move-result-object v0

    iput-object v0, p0, Lbbj;->j:Lhca;

    .line 4
    invoke-static {}, Lbbj;->emptyLongList()Lhcd;

    move-result-object v0

    iput-object v0, p0, Lbbj;->l:Lhcd;

    .line 5
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 249
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 250
    sget-object v2, Lbbf$a;->u:Lhby;

    .line 251
    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 252
    sget-object v2, Lbbm$a;->e:Lhby;

    .line 253
    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 254
    sget-object v2, Lbld$a;->u:Lhby;

    .line 255
    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "q"

    aput-object v2, v0, v1

    .line 256
    const-string v1, "\u0001\u000e\u0000\u0001\u0001\u000e\u0000\u0003\u0000\u0001\u000c\u0000\u0002\u0004\u0001\u0003\u0004\u0002\u0004\u001e\u0005\u0004\u0003\u0006\u0002\u0004\u0007\u0002\u0005\u0008\u001e\t\u0014\n\u0004\u0006\u000b\u0004\u0007\u000c\u0004\u0008\r\u0004\t\u000e\u0007\n"

    .line 257
    sget-object v2, Lbbj;->r:Lbbj;

    invoke-static {v2, v1, v0}, Lbbj;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 113
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 114
    :pswitch_0
    new-instance v0, Lbbj;

    invoke-direct {v0}, Lbbj;-><init>()V

    .line 247
    :goto_0
    :pswitch_1
    return-object v0

    .line 115
    :pswitch_2
    sget-object v0, Lbbj;->r:Lbbj;

    goto :goto_0

    .line 116
    :pswitch_3
    iget-object v1, p0, Lbbj;->e:Lhca;

    invoke-interface {v1}, Lhca;->b()V

    .line 117
    iget-object v1, p0, Lbbj;->j:Lhca;

    invoke-interface {v1}, Lhca;->b()V

    .line 118
    iget-object v1, p0, Lbbj;->l:Lhcd;

    invoke-interface {v1}, Lhcd;->b()V

    goto :goto_0

    .line 120
    :pswitch_4
    new-instance v0, Lhbr$a;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhbr$a;-><init>(BF)V

    goto :goto_0

    .line 121
    :pswitch_5
    check-cast p2, Lhaq;

    .line 122
    check-cast p3, Lhbg;

    .line 123
    if-nez p3, :cond_0

    .line 124
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 125
    :cond_0
    :try_start_0
    sget-boolean v0, Lbbj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {p0, p2, p3}, Lbbj;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 127
    sget-object v0, Lbbj;->r:Lbbj;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 129
    :cond_2
    :goto_1
    if-nez v0, :cond_11

    .line 130
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v1

    .line 131
    sparse-switch v1, :sswitch_data_0

    .line 134
    invoke-virtual {p0, v1, p2}, Lbbj;->parseUnknownField(ILhaq;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 135
    goto :goto_1

    :sswitch_0
    move v0, v2

    .line 133
    goto :goto_1

    .line 136
    :sswitch_1
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v1

    .line 137
    invoke-static {v1}, Lbbf$a;->a(I)Lbbf$a;

    move-result-object v3

    .line 138
    if-nez v3, :cond_3

    .line 139
    const/4 v3, 0x1

    invoke-super {p0, v3, v1}, Lhbr;->mergeVarintField(II)V
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 234
    :catch_0
    move-exception v0

    .line 235
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    :catchall_0
    move-exception v0

    throw v0

    .line 140
    :cond_3
    :try_start_2
    iget v3, p0, Lbbj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lbbj;->a:I

    .line 141
    iput v1, p0, Lbbj;->b:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 236
    :catch_1
    move-exception v0

    .line 237
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 238
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    :sswitch_2
    :try_start_4
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lbbj;->a:I

    .line 144
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->c:I

    goto :goto_1

    .line 146
    :sswitch_3
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lbbj;->a:I

    .line 147
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->d:I

    goto :goto_1

    .line 149
    :sswitch_4
    iget-object v1, p0, Lbbj;->e:Lhca;

    invoke-interface {v1}, Lhca;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 150
    iget-object v1, p0, Lbbj;->e:Lhca;

    .line 151
    invoke-static {v1}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v1

    iput-object v1, p0, Lbbj;->e:Lhca;

    .line 152
    :cond_4
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v1

    .line 153
    invoke-static {v1}, Lbbm$a;->a(I)Lbbm$a;

    move-result-object v3

    .line 154
    if-nez v3, :cond_5

    .line 155
    const/4 v3, 0x4

    invoke-super {p0, v3, v1}, Lhbr;->mergeVarintField(II)V

    goto :goto_1

    .line 156
    :cond_5
    iget-object v3, p0, Lbbj;->e:Lhca;

    invoke-interface {v3, v1}, Lhca;->d(I)V

    goto/16 :goto_1

    .line 158
    :sswitch_5
    iget-object v1, p0, Lbbj;->e:Lhca;

    invoke-interface {v1}, Lhca;->a()Z

    move-result v1

    if-nez v1, :cond_6

    .line 159
    iget-object v1, p0, Lbbj;->e:Lhca;

    .line 160
    invoke-static {v1}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v1

    iput-object v1, p0, Lbbj;->e:Lhca;

    .line 161
    :cond_6
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v1

    .line 162
    invoke-virtual {p2, v1}, Lhaq;->c(I)I

    move-result v1

    .line 163
    :goto_2
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_8

    .line 164
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v3

    .line 165
    invoke-static {v3}, Lbbm$a;->a(I)Lbbm$a;

    move-result-object v4

    .line 166
    if-nez v4, :cond_7

    .line 167
    const/4 v4, 0x4

    invoke-super {p0, v4, v3}, Lhbr;->mergeVarintField(II)V

    goto :goto_2

    .line 168
    :cond_7
    iget-object v4, p0, Lbbj;->e:Lhca;

    invoke-interface {v4, v3}, Lhca;->d(I)V

    goto :goto_2

    .line 170
    :cond_8
    invoke-virtual {p2, v1}, Lhaq;->d(I)V

    goto/16 :goto_1

    .line 172
    :sswitch_6
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lbbj;->a:I

    .line 173
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->g:I

    goto/16 :goto_1

    .line 175
    :sswitch_7
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lbbj;->a:I

    .line 176
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbbj;->h:J

    goto/16 :goto_1

    .line 178
    :sswitch_8
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lbbj;->a:I

    .line 179
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbbj;->i:J

    goto/16 :goto_1

    .line 181
    :sswitch_9
    iget-object v1, p0, Lbbj;->j:Lhca;

    invoke-interface {v1}, Lhca;->a()Z

    move-result v1

    if-nez v1, :cond_9

    .line 182
    iget-object v1, p0, Lbbj;->j:Lhca;

    .line 183
    invoke-static {v1}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v1

    iput-object v1, p0, Lbbj;->j:Lhca;

    .line 184
    :cond_9
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v1

    .line 185
    invoke-static {v1}, Lbld$a;->a(I)Lbld$a;

    move-result-object v3

    .line 186
    if-nez v3, :cond_a

    .line 187
    const/16 v3, 0x8

    invoke-super {p0, v3, v1}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 188
    :cond_a
    iget-object v3, p0, Lbbj;->j:Lhca;

    invoke-interface {v3, v1}, Lhca;->d(I)V

    goto/16 :goto_1

    .line 190
    :sswitch_a
    iget-object v1, p0, Lbbj;->j:Lhca;

    invoke-interface {v1}, Lhca;->a()Z

    move-result v1

    if-nez v1, :cond_b

    .line 191
    iget-object v1, p0, Lbbj;->j:Lhca;

    .line 192
    invoke-static {v1}, Lhbr;->mutableCopy(Lhca;)Lhca;

    move-result-object v1

    iput-object v1, p0, Lbbj;->j:Lhca;

    .line 193
    :cond_b
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v1

    .line 194
    invoke-virtual {p2, v1}, Lhaq;->c(I)I

    move-result v1

    .line 195
    :goto_3
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_d

    .line 196
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v3

    .line 197
    invoke-static {v3}, Lbld$a;->a(I)Lbld$a;

    move-result-object v4

    .line 198
    if-nez v4, :cond_c

    .line 199
    const/16 v4, 0x8

    invoke-super {p0, v4, v3}, Lhbr;->mergeVarintField(II)V

    goto :goto_3

    .line 200
    :cond_c
    iget-object v4, p0, Lbbj;->j:Lhca;

    invoke-interface {v4, v3}, Lhca;->d(I)V

    goto :goto_3

    .line 202
    :cond_d
    invoke-virtual {p2, v1}, Lhaq;->d(I)V

    goto/16 :goto_1

    .line 204
    :sswitch_b
    iget-object v1, p0, Lbbj;->l:Lhcd;

    invoke-interface {v1}, Lhcd;->a()Z

    move-result v1

    if-nez v1, :cond_e

    .line 205
    iget-object v1, p0, Lbbj;->l:Lhcd;

    .line 206
    invoke-static {v1}, Lhbr;->mutableCopy(Lhcd;)Lhcd;

    move-result-object v1

    iput-object v1, p0, Lbbj;->l:Lhcd;

    .line 207
    :cond_e
    iget-object v1, p0, Lbbj;->l:Lhcd;

    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lhcd;->a(J)V

    goto/16 :goto_1

    .line 209
    :sswitch_c
    invoke-virtual {p2}, Lhaq;->s()I

    move-result v1

    .line 210
    invoke-virtual {p2, v1}, Lhaq;->c(I)I

    move-result v1

    .line 211
    iget-object v3, p0, Lbbj;->l:Lhcd;

    invoke-interface {v3}, Lhcd;->a()Z

    move-result v3

    if-nez v3, :cond_f

    invoke-virtual {p2}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_f

    .line 212
    iget-object v3, p0, Lbbj;->l:Lhcd;

    .line 213
    invoke-static {v3}, Lhbr;->mutableCopy(Lhcd;)Lhcd;

    move-result-object v3

    iput-object v3, p0, Lbbj;->l:Lhcd;

    .line 214
    :cond_f
    :goto_4
    invoke-virtual {p2}, Lhaq;->u()I

    move-result v3

    if-lez v3, :cond_10

    .line 215
    iget-object v3, p0, Lbbj;->l:Lhcd;

    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lhcd;->a(J)V

    goto :goto_4

    .line 216
    :cond_10
    invoke-virtual {p2, v1}, Lhaq;->d(I)V

    goto/16 :goto_1

    .line 218
    :sswitch_d
    iget v1, p0, Lbbj;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lbbj;->a:I

    .line 219
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->m:I

    goto/16 :goto_1

    .line 221
    :sswitch_e
    iget v1, p0, Lbbj;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lbbj;->a:I

    .line 222
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->n:I

    goto/16 :goto_1

    .line 224
    :sswitch_f
    iget v1, p0, Lbbj;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lbbj;->a:I

    .line 225
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->o:I

    goto/16 :goto_1

    .line 227
    :sswitch_10
    iget v1, p0, Lbbj;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lbbj;->a:I

    .line 228
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v1

    iput v1, p0, Lbbj;->p:I

    goto/16 :goto_1

    .line 230
    :sswitch_11
    iget v1, p0, Lbbj;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lbbj;->a:I

    .line 231
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v1

    iput-boolean v1, p0, Lbbj;->q:Z
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 240
    :cond_11
    :pswitch_6
    sget-object v0, Lbbj;->r:Lbbj;

    goto/16 :goto_0

    .line 241
    :pswitch_7
    sget-object v0, Lbbj;->s:Lhdm;

    if-nez v0, :cond_13

    const-class v1, Lbbj;

    monitor-enter v1

    .line 242
    :try_start_5
    sget-object v0, Lbbj;->s:Lhdm;

    if-nez v0, :cond_12

    .line 243
    new-instance v0, Lhaa;

    sget-object v2, Lbbj;->r:Lbbj;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbbj;->s:Lhdm;

    .line 244
    :cond_12
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 245
    :cond_13
    sget-object v0, Lbbj;->s:Lhdm;

    goto/16 :goto_0

    .line 244
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 246
    :pswitch_8
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 131
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x38 -> :sswitch_8
        0x40 -> :sswitch_9
        0x42 -> :sswitch_a
        0x48 -> :sswitch_b
        0x4a -> :sswitch_c
        0x50 -> :sswitch_d
        0x58 -> :sswitch_e
        0x60 -> :sswitch_f
        0x68 -> :sswitch_10
        0x70 -> :sswitch_11
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 48
    iget v0, p0, Lbbj;->memoizedSerializedSize:I

    .line 49
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 112
    :goto_0
    return v0

    .line 50
    :cond_0
    sget-boolean v0, Lbbj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {p0}, Lbbj;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbbj;->memoizedSerializedSize:I

    .line 52
    iget v0, p0, Lbbj;->memoizedSerializedSize:I

    goto :goto_0

    .line 54
    :cond_1
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 55
    iget v0, p0, Lbbj;->b:I

    .line 56
    invoke-static {v3, v0}, Lhaw;->k(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 57
    :goto_1
    iget v2, p0, Lbbj;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    .line 58
    iget v2, p0, Lbbj;->c:I

    .line 59
    invoke-static {v4, v2}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_2
    iget v2, p0, Lbbj;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 61
    const/4 v2, 0x3

    iget v3, p0, Lbbj;->d:I

    .line 62
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v1

    .line 64
    :goto_2
    iget-object v4, p0, Lbbj;->e:Lhca;

    invoke-interface {v4}, Lhca;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 65
    iget-object v4, p0, Lbbj;->e:Lhca;

    .line 66
    invoke-interface {v4, v2}, Lhca;->c(I)I

    move-result v4

    invoke-static {v4}, Lhaw;->j(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 67
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 68
    :cond_4
    add-int/2addr v0, v3

    .line 69
    iget-object v2, p0, Lbbj;->e:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 70
    iget v2, p0, Lbbj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5

    .line 71
    const/4 v2, 0x5

    iget v3, p0, Lbbj;->g:I

    .line 72
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_5
    iget v2, p0, Lbbj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_6

    .line 74
    const/4 v2, 0x6

    iget-wide v4, p0, Lbbj;->h:J

    .line 75
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 76
    :cond_6
    iget v2, p0, Lbbj;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_7

    .line 77
    const/4 v2, 0x7

    iget-wide v4, p0, Lbbj;->i:J

    .line 78
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    move v2, v1

    move v3, v1

    .line 80
    :goto_3
    iget-object v4, p0, Lbbj;->j:Lhca;

    invoke-interface {v4}, Lhca;->size()I

    move-result v4

    if-ge v2, v4, :cond_8

    .line 81
    iget-object v4, p0, Lbbj;->j:Lhca;

    .line 82
    invoke-interface {v4, v2}, Lhca;->c(I)I

    move-result v4

    invoke-static {v4}, Lhaw;->j(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 84
    :cond_8
    add-int/2addr v0, v3

    .line 85
    iget-object v2, p0, Lbbj;->j:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    move v0, v1

    .line 87
    :goto_4
    iget-object v3, p0, Lbbj;->l:Lhcd;

    invoke-interface {v3}, Lhcd;->size()I

    move-result v3

    if-ge v1, v3, :cond_9

    .line 88
    iget-object v3, p0, Lbbj;->l:Lhcd;

    .line 89
    invoke-interface {v3, v1}, Lhcd;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lhaw;->d(J)I

    move-result v3

    add-int/2addr v0, v3

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 91
    :cond_9
    add-int/2addr v0, v2

    .line 93
    iget-object v1, p0, Lbbj;->l:Lhcd;

    .line 94
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 95
    iget v1, p0, Lbbj;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_a

    .line 96
    const/16 v1, 0xa

    iget v2, p0, Lbbj;->m:I

    .line 97
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_a
    iget v1, p0, Lbbj;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_b

    .line 99
    const/16 v1, 0xb

    iget v2, p0, Lbbj;->n:I

    .line 100
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_b
    iget v1, p0, Lbbj;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_c

    .line 102
    const/16 v1, 0xc

    iget v2, p0, Lbbj;->o:I

    .line 103
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_c
    iget v1, p0, Lbbj;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_d

    .line 105
    const/16 v1, 0xd

    iget v2, p0, Lbbj;->p:I

    .line 106
    invoke-static {v1, v2}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_d
    iget v1, p0, Lbbj;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_e

    .line 108
    const/16 v1, 0xe

    iget-boolean v2, p0, Lbbj;->q:Z

    .line 109
    invoke-static {v1, v2}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_e
    iget-object v1, p0, Lbbj;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Lbbj;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 6
    sget-boolean v0, Lbbj;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p0, p1}, Lbbj;->writeToInternal(Lhaw;)V

    .line 47
    :goto_0
    return-void

    .line 9
    :cond_0
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 10
    iget v0, p0, Lbbj;->b:I

    .line 11
    invoke-virtual {p1, v2, v0}, Lhaw;->b(II)V

    .line 12
    :cond_1
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 13
    iget v0, p0, Lbbj;->c:I

    invoke-virtual {p1, v3, v0}, Lhaw;->b(II)V

    .line 14
    :cond_2
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 15
    const/4 v0, 0x3

    iget v2, p0, Lbbj;->d:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    :cond_3
    move v0, v1

    .line 16
    :goto_1
    iget-object v2, p0, Lbbj;->e:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 17
    iget-object v2, p0, Lbbj;->e:Lhca;

    invoke-interface {v2, v0}, Lhca;->c(I)I

    move-result v2

    .line 18
    invoke-virtual {p1, v4, v2}, Lhaw;->b(II)V

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 20
    :cond_4
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_5

    .line 21
    const/4 v0, 0x5

    iget v2, p0, Lbbj;->g:I

    invoke-virtual {p1, v0, v2}, Lhaw;->b(II)V

    .line 22
    :cond_5
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_6

    .line 23
    const/4 v0, 0x6

    iget-wide v2, p0, Lbbj;->h:J

    .line 24
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 25
    :cond_6
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 26
    const/4 v0, 0x7

    iget-wide v2, p0, Lbbj;->i:J

    .line 27
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    :cond_7
    move v0, v1

    .line 28
    :goto_2
    iget-object v2, p0, Lbbj;->j:Lhca;

    invoke-interface {v2}, Lhca;->size()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 29
    iget-object v2, p0, Lbbj;->j:Lhca;

    invoke-interface {v2, v0}, Lhca;->c(I)I

    move-result v2

    .line 30
    invoke-virtual {p1, v5, v2}, Lhaw;->b(II)V

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 32
    :cond_8
    :goto_3
    iget-object v0, p0, Lbbj;->l:Lhcd;

    invoke-interface {v0}, Lhcd;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 33
    const/16 v0, 0x9

    iget-object v2, p0, Lbbj;->l:Lhcd;

    invoke-interface {v2, v1}, Lhcd;->a(I)J

    move-result-wide v2

    .line 34
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 35
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 36
    :cond_9
    iget v0, p0, Lbbj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 37
    const/16 v0, 0xa

    iget v1, p0, Lbbj;->m:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 38
    :cond_a
    iget v0, p0, Lbbj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_b

    .line 39
    const/16 v0, 0xb

    iget v1, p0, Lbbj;->n:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 40
    :cond_b
    iget v0, p0, Lbbj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_c

    .line 41
    const/16 v0, 0xc

    iget v1, p0, Lbbj;->o:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 42
    :cond_c
    iget v0, p0, Lbbj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_d

    .line 43
    const/16 v0, 0xd

    iget v1, p0, Lbbj;->p:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 44
    :cond_d
    iget v0, p0, Lbbj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 45
    const/16 v0, 0xe

    iget-boolean v1, p0, Lbbj;->q:Z

    invoke-virtual {p1, v0, v1}, Lhaw;->a(IZ)V

    .line 46
    :cond_e
    iget-object v0, p0, Lbbj;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto/16 :goto_0
.end method
