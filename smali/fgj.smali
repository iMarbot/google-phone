.class public final Lfgj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfgj;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4
    invoke-static {}, Lbdf;->c()V

    .line 5
    invoke-static {p0}, Lfho;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6
    const-string v1, "RegistrationWorker.canRegisterDevice, no incoming call account"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    :goto_0
    return v0

    .line 8
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9
    const-string v1, "RegistrationWorker.canRegisterDevice, no verified number"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 11
    :cond_1
    const-string v1, "RegistrationWorker.canRegisterDevice, returning true"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 12
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 13
    move-object v5, p1

    check-cast v5, Ljava/lang/String;

    .line 14
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    invoke-static {v0, v5}, Lfgj;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    :goto_0
    return-object v7

    .line 16
    :cond_0
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/dialer/voip/gcm/DialerGcmRegistrationService;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 17
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18
    const-string v0, "RegistrationWorker.doInBackground, Gcm registration failed"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 20
    :cond_1
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    .line 21
    invoke-static {v0}, Lfho;->e(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v4, 0x1

    .line 22
    :goto_1
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    iget-object v1, p0, Lfgj;->a:Landroid/content/Context;

    .line 23
    invoke-static {v1}, Lfho;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lfgj;->a:Landroid/content/Context;

    .line 24
    invoke-static {v3}, Lfho;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 25
    invoke-static/range {v0 .. v5}, Lfmd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26
    const-string v2, "RegistrationWorker.doInBackground, got registration id: "

    .line 27
    invoke-static {v1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-array v2, v6, [Ljava/lang/Object;

    .line 28
    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lfho;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    iget-object v1, p0, Lfgj;->a:Landroid/content/Context;

    invoke-static {v1}, Lfhx;->a(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lfho;->a(Landroid/content/Context;J)V

    .line 31
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    iget-object v1, p0, Lfgj;->a:Landroid/content/Context;

    .line 32
    invoke-static {v1}, Lfho;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-static {v0, v1}, Lfmd;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 34
    const-string v2, "RegistrationWorker.doInBackground, got gaia id: "

    invoke-static {v1}, Lfmd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lfmd;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lfgj;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lfho;->f(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move v4, v6

    .line 21
    goto :goto_1

    .line 27
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 34
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method
