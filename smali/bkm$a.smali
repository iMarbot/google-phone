.class public final enum Lbkm$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbkm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbkm$a;

.field public static final enum b:Lbkm$a;

.field public static final enum c:Lbkm$a;

.field public static final enum d:Lbkm$a;

.field public static final enum e:Lbkm$a;

.field public static final enum f:Lbkm$a;

.field public static final enum g:Lbkm$a;

.field public static final enum h:Lbkm$a;

.field public static final enum i:Lbkm$a;

.field public static final enum j:Lbkm$a;

.field public static final enum k:Lbkm$a;

.field public static final enum l:Lbkm$a;

.field public static final enum m:Lbkm$a;

.field public static final enum n:Lbkm$a;

.field public static final enum o:Lbkm$a;

.field public static final enum p:Lbkm$a;

.field public static final q:Lhby;

.field private static synthetic s:[Lbkm$a;


# instance fields
.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 24
    new-instance v0, Lbkm$a;

    const-string v1, "UNKNOWN_LOOKUP_RESULT_TYPE"

    invoke-direct {v0, v1, v4, v4}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->a:Lbkm$a;

    .line 25
    new-instance v0, Lbkm$a;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v5, v5}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->b:Lbkm$a;

    .line 26
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CONTACT"

    invoke-direct {v0, v1, v6, v6}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->c:Lbkm$a;

    .line 27
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE"

    invoke-direct {v0, v1, v7, v7}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->d:Lbkm$a;

    .line 28
    new-instance v0, Lbkm$a;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v8, v8}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->e:Lbkm$a;

    .line 29
    new-instance v0, Lbkm$a;

    const-string v1, "EMERGENCY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->f:Lbkm$a;

    .line 30
    new-instance v0, Lbkm$a;

    const-string v1, "VOICEMAIL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->g:Lbkm$a;

    .line 31
    new-instance v0, Lbkm$a;

    const-string v1, "REMOTE_PLACES"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->h:Lbkm$a;

    .line 32
    new-instance v0, Lbkm$a;

    const-string v1, "REMOTE_PROFILE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->i:Lbkm$a;

    .line 33
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_UNKNOWN"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->j:Lbkm$a;

    .line 34
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_DIRECTORY"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->k:Lbkm$a;

    .line 35
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_EXTENDED"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->l:Lbkm$a;

    .line 36
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_PLACES"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->m:Lbkm$a;

    .line 37
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_PROFILE"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->n:Lbkm$a;

    .line 38
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_CNAP"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->o:Lbkm$a;

    .line 39
    new-instance v0, Lbkm$a;

    const-string v1, "LOCAL_CACHE_CEQUINT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lbkm$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkm$a;->p:Lbkm$a;

    .line 40
    const/16 v0, 0x10

    new-array v0, v0, [Lbkm$a;

    sget-object v1, Lbkm$a;->a:Lbkm$a;

    aput-object v1, v0, v4

    sget-object v1, Lbkm$a;->b:Lbkm$a;

    aput-object v1, v0, v5

    sget-object v1, Lbkm$a;->c:Lbkm$a;

    aput-object v1, v0, v6

    sget-object v1, Lbkm$a;->d:Lbkm$a;

    aput-object v1, v0, v7

    sget-object v1, Lbkm$a;->e:Lbkm$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbkm$a;->f:Lbkm$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbkm$a;->g:Lbkm$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbkm$a;->h:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbkm$a;->i:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbkm$a;->j:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbkm$a;->k:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbkm$a;->l:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbkm$a;->m:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbkm$a;->n:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbkm$a;->o:Lbkm$a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbkm$a;->p:Lbkm$a;

    aput-object v2, v0, v1

    sput-object v0, Lbkm$a;->s:[Lbkm$a;

    .line 41
    new-instance v0, Lbkn;

    invoke-direct {v0}, Lbkn;-><init>()V

    sput-object v0, Lbkm$a;->q:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lbkm$a;->r:I

    .line 23
    return-void
.end method

.method public static a(I)Lbkm$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 20
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbkm$a;->a:Lbkm$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbkm$a;->b:Lbkm$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbkm$a;->c:Lbkm$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbkm$a;->d:Lbkm$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lbkm$a;->e:Lbkm$a;

    goto :goto_0

    .line 9
    :pswitch_5
    sget-object v0, Lbkm$a;->f:Lbkm$a;

    goto :goto_0

    .line 10
    :pswitch_6
    sget-object v0, Lbkm$a;->g:Lbkm$a;

    goto :goto_0

    .line 11
    :pswitch_7
    sget-object v0, Lbkm$a;->h:Lbkm$a;

    goto :goto_0

    .line 12
    :pswitch_8
    sget-object v0, Lbkm$a;->i:Lbkm$a;

    goto :goto_0

    .line 13
    :pswitch_9
    sget-object v0, Lbkm$a;->j:Lbkm$a;

    goto :goto_0

    .line 14
    :pswitch_a
    sget-object v0, Lbkm$a;->k:Lbkm$a;

    goto :goto_0

    .line 15
    :pswitch_b
    sget-object v0, Lbkm$a;->l:Lbkm$a;

    goto :goto_0

    .line 16
    :pswitch_c
    sget-object v0, Lbkm$a;->m:Lbkm$a;

    goto :goto_0

    .line 17
    :pswitch_d
    sget-object v0, Lbkm$a;->n:Lbkm$a;

    goto :goto_0

    .line 18
    :pswitch_e
    sget-object v0, Lbkm$a;->o:Lbkm$a;

    goto :goto_0

    .line 19
    :pswitch_f
    sget-object v0, Lbkm$a;->p:Lbkm$a;

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static values()[Lbkm$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbkm$a;->s:[Lbkm$a;

    invoke-virtual {v0}, [Lbkm$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbkm$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbkm$a;->r:I

    return v0
.end method
