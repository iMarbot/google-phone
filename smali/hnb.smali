.class final Lhnb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhna;


# direct methods
.method constructor <init>(Lhna;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lhnb;->a:Lhna;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 2
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 4
    iget-object v1, v1, Lhna;->b:Ljava/net/InetSocketAddress;

    if-nez v1, :cond_0

    move v0, v7

    .line 5
    :cond_0
    if-eqz v0, :cond_1

    .line 6
    iget-object v0, p0, Lhnb;->a:Lhna;

    iget-object v0, v0, Lhna;->C:Ljava/lang/Runnable;

    .line 7
    iget-object v0, p0, Lhnb;->a:Lhna;

    new-instance v1, Lhna$a;

    iget-object v2, p0, Lhnb;->a:Lhna;

    iget-object v3, p0, Lhnb;->a:Lhna;

    .line 9
    invoke-direct {v1, v2, v4}, Lhna$a;-><init>(Lhna;Lhnt;)V

    .line 10
    iput-object v1, v0, Lhna;->k:Lhna$a;

    .line 12
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 13
    iget-object v0, v0, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 14
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 15
    iget-object v1, v1, Lhna;->k:Lhna$a;

    .line 16
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 17
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 18
    iget-object v1, v0, Lhna;->g:Ljava/lang/Object;

    .line 19
    monitor-enter v1

    .line 20
    :try_start_0
    iget-object v0, p0, Lhnb;->a:Lhna;

    const v2, 0x7fffffff

    .line 21
    iput v2, v0, Lhna;->q:I

    .line 23
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 24
    invoke-virtual {v0}, Lhna;->c()Z

    .line 26
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 28
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 29
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 31
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 32
    iget-object v1, v1, Lhna;->p:Ljava/net/Socket;

    .line 33
    invoke-virtual {v0, v4, v1}, Lhme;->a(Lhnv;Ljava/net/Socket;)V

    .line 34
    iget-object v0, p0, Lhnb;->a:Lhna;

    iget-object v0, v0, Lhna;->D:Lgvw;

    invoke-virtual {v0, v4}, Lgvp;->a(Ljava/lang/Object;)Z

    .line 149
    :goto_0
    return-void

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 36
    :cond_1
    new-instance v0, Lhnc;

    invoke-direct {v0}, Lhnc;-><init>()V

    invoke-static {v0}, Lhul;->a(Lhuv;)Lhuj;

    move-result-object v6

    .line 37
    new-instance v8, Lhoa;

    invoke-direct {v8}, Lhoa;-><init>()V

    .line 38
    :try_start_2
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 39
    iget-object v0, v0, Lhna;->y:Ljava/net/InetSocketAddress;

    .line 40
    if-nez v0, :cond_3

    .line 41
    new-instance v2, Ljava/net/Socket;

    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 42
    iget-object v0, v0, Lhna;->b:Ljava/net/InetSocketAddress;

    .line 43
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 44
    iget-object v1, v1, Lhna;->b:Ljava/net/InetSocketAddress;

    .line 45
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v1

    invoke-direct {v2, v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    .line 57
    :goto_1
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 58
    iget-object v0, v0, Lhna;->n:Ljavax/net/ssl/SSLSocketFactory;

    .line 59
    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 62
    iget-object v0, v0, Lhna;->n:Ljavax/net/ssl/SSLSocketFactory;

    .line 63
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 64
    iget-object v1, v1, Lhna;->o:Ljavax/net/ssl/HostnameVerifier;

    .line 65
    iget-object v3, p0, Lhnb;->a:Lhna;

    .line 66
    iget-object v4, v3, Lhna;->c:Ljava/lang/String;

    invoke-static {v4}, Lio/grpc/internal/cf;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    .line 67
    invoke-virtual {v4}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 68
    invoke-virtual {v4}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 70
    :goto_2
    iget-object v4, p0, Lhnb;->a:Lhna;

    .line 71
    iget-object v5, v4, Lhna;->c:Ljava/lang/String;

    invoke-static {v5}, Lio/grpc/internal/cf;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v5

    .line 72
    invoke-virtual {v5}, Ljava/net/URI;->getPort()I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    .line 73
    invoke-virtual {v5}, Ljava/net/URI;->getPort()I

    move-result v4

    .line 75
    :goto_3
    iget-object v5, p0, Lhnb;->a:Lhna;

    .line 77
    iget-object v5, v5, Lhna;->s:Lhnj;

    .line 78
    invoke-static/range {v0 .. v5}, Lhnf;->a(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Ljava/net/Socket;Ljava/lang/String;ILhnj;)Ljavax/net/ssl/SSLSocket;

    move-result-object v2

    .line 79
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 80
    invoke-static {v2}, Lhul;->b(Ljava/net/Socket;)Lhuv;

    move-result-object v0

    invoke-static {v0}, Lhul;->a(Lhuv;)Lhuj;
    :try_end_2
    .catch Lhma; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 81
    :try_start_3
    invoke-static {v2}, Lhul;->a(Ljava/net/Socket;)Lhuu;

    move-result-object v0

    invoke-static {v0}, Lhul;->a(Lhuu;)Lhui;
    :try_end_3
    .catch Lhma; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v0

    .line 82
    iget-object v3, p0, Lhnb;->a:Lhna;

    new-instance v4, Lhna$a;

    iget-object v5, p0, Lhnb;->a:Lhna;

    invoke-interface {v8, v1, v7}, Lhog;->a(Lhuj;Z)Lhnt;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lhna$a;-><init>(Lhna;Lhnt;)V

    .line 83
    iput-object v4, v3, Lhna;->k:Lhna$a;

    .line 85
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 86
    iget-object v1, v1, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 87
    iget-object v3, p0, Lhnb;->a:Lhna;

    .line 88
    iget-object v3, v3, Lhna;->k:Lhna$a;

    .line 89
    invoke-interface {v1, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 124
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 125
    iget-object v1, v1, Lhna;->g:Ljava/lang/Object;

    .line 126
    monitor-enter v1

    .line 127
    :try_start_4
    iget-object v3, p0, Lhnb;->a:Lhna;

    .line 128
    iput-object v2, v3, Lhna;->p:Ljava/net/Socket;

    .line 130
    iget-object v2, p0, Lhnb;->a:Lhna;

    const v3, 0x7fffffff

    .line 131
    iput v3, v2, Lhna;->q:I

    .line 133
    iget-object v2, p0, Lhnb;->a:Lhna;

    .line 134
    invoke-virtual {v2}, Lhna;->c()Z

    .line 136
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 137
    invoke-interface {v8, v0, v7}, Lhog;->a(Lhui;Z)Lhnv;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 139
    iget-object v1, v1, Lhna;->e:Lhme;

    .line 140
    iget-object v2, p0, Lhnb;->a:Lhna;

    .line 141
    iget-object v2, v2, Lhna;->p:Ljava/net/Socket;

    .line 142
    invoke-virtual {v1, v0, v2}, Lhme;->a(Lhnv;Ljava/net/Socket;)V

    .line 143
    :try_start_5
    invoke-interface {v0}, Lhnv;->a()V

    .line 144
    new-instance v1, Lhof;

    invoke-direct {v1}, Lhof;-><init>()V

    .line 145
    invoke-interface {v0, v1}, Lhnv;->b(Lhof;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    iget-object v1, p0, Lhnb;->a:Lhna;

    invoke-virtual {v1, v0}, Lhna;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 46
    :cond_3
    :try_start_6
    iget-object v0, p0, Lhnb;->a:Lhna;

    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 47
    iget-object v1, v1, Lhna;->b:Ljava/net/InetSocketAddress;

    .line 48
    iget-object v2, p0, Lhnb;->a:Lhna;

    .line 49
    iget-object v2, v2, Lhna;->y:Ljava/net/InetSocketAddress;

    .line 50
    iget-object v3, p0, Lhnb;->a:Lhna;

    .line 51
    iget-object v3, v3, Lhna;->z:Ljava/lang/String;

    .line 52
    iget-object v4, p0, Lhnb;->a:Lhna;

    .line 53
    iget-object v4, v4, Lhna;->A:Ljava/lang/String;

    .line 55
    invoke-virtual {v0, v1, v2, v3, v4}, Lhna;->a(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v2

    goto/16 :goto_1

    .line 69
    :cond_4
    iget-object v3, v3, Lhna;->c:Ljava/lang/String;

    goto/16 :goto_2

    .line 74
    :cond_5
    iget-object v4, v4, Lhna;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {v4}, Ljava/net/InetSocketAddress;->getPort()I
    :try_end_6
    .catch Lhma; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v4

    goto/16 :goto_3

    .line 91
    :catch_1
    move-exception v0

    move-object v1, v6

    .line 92
    :goto_4
    :try_start_7
    iget-object v2, p0, Lhnb;->a:Lhna;

    const/4 v3, 0x0

    sget-object v4, Lhns;->d:Lhns;

    .line 93
    iget-object v0, v0, Lhma;->a:Lhlw;

    .line 95
    invoke-virtual {v2, v3, v4, v0}, Lhna;->a(ILhns;Lhlw;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 96
    iget-object v0, p0, Lhnb;->a:Lhna;

    new-instance v2, Lhna$a;

    iget-object v3, p0, Lhnb;->a:Lhna;

    invoke-interface {v8, v1, v7}, Lhog;->a(Lhuj;Z)Lhnt;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lhna$a;-><init>(Lhna;Lhnt;)V

    .line 97
    iput-object v2, v0, Lhna;->k:Lhna$a;

    .line 99
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 100
    iget-object v0, v0, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 101
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 102
    iget-object v1, v1, Lhna;->k:Lhna$a;

    .line 103
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 105
    :catch_2
    move-exception v0

    .line 106
    :goto_5
    :try_start_8
    iget-object v1, p0, Lhnb;->a:Lhna;

    invoke-virtual {v1, v0}, Lhna;->a(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 107
    iget-object v0, p0, Lhnb;->a:Lhna;

    new-instance v1, Lhna$a;

    iget-object v2, p0, Lhnb;->a:Lhna;

    invoke-interface {v8, v6, v7}, Lhog;->a(Lhuj;Z)Lhnt;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhna$a;-><init>(Lhna;Lhnt;)V

    .line 108
    iput-object v1, v0, Lhna;->k:Lhna$a;

    .line 110
    iget-object v0, p0, Lhnb;->a:Lhna;

    .line 111
    iget-object v0, v0, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 112
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 113
    iget-object v1, v1, Lhna;->k:Lhna$a;

    .line 114
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 116
    :catchall_1
    move-exception v0

    :goto_6
    iget-object v1, p0, Lhnb;->a:Lhna;

    new-instance v2, Lhna$a;

    iget-object v3, p0, Lhnb;->a:Lhna;

    invoke-interface {v8, v6, v7}, Lhog;->a(Lhuj;Z)Lhnt;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lhna$a;-><init>(Lhna;Lhnt;)V

    .line 117
    iput-object v2, v1, Lhna;->k:Lhna$a;

    .line 119
    iget-object v1, p0, Lhnb;->a:Lhna;

    .line 120
    iget-object v1, v1, Lhna;->i:Ljava/util/concurrent/Executor;

    .line 121
    iget-object v2, p0, Lhnb;->a:Lhna;

    .line 122
    iget-object v2, v2, Lhna;->k:Lhna$a;

    .line 123
    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    throw v0

    .line 136
    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    .line 116
    :catchall_3
    move-exception v0

    move-object v6, v1

    goto :goto_6

    .line 105
    :catch_3
    move-exception v0

    move-object v6, v1

    goto :goto_5

    .line 91
    :catch_4
    move-exception v0

    goto :goto_4
.end method
