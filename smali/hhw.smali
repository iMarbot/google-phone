.class public final Lhhw;
.super Lhft;
.source "PG"


# instance fields
.field private a:Lhhu;

.field private b:Lhhq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lhhw;->a:Lhhu;

    .line 4
    iput-object v0, p0, Lhhw;->b:Lhhq;

    .line 5
    iput-object v0, p0, Lhhw;->unknownFieldData:Lhfv;

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lhhw;->cachedSize:I

    .line 7
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 14
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 15
    iget-object v1, p0, Lhhw;->a:Lhhu;

    if-eqz v1, :cond_0

    .line 16
    const/4 v1, 0x2

    iget-object v2, p0, Lhhw;->a:Lhhu;

    .line 17
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18
    :cond_0
    iget-object v1, p0, Lhhw;->b:Lhhq;

    if-eqz v1, :cond_1

    .line 19
    const/4 v1, 0x3

    iget-object v2, p0, Lhhw;->b:Lhhq;

    .line 20
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 22
    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 24
    sparse-switch v0, :sswitch_data_0

    .line 26
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    :sswitch_0
    return-object p0

    .line 28
    :sswitch_1
    iget-object v0, p0, Lhhw;->a:Lhhu;

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Lhhu;

    invoke-direct {v0}, Lhhu;-><init>()V

    iput-object v0, p0, Lhhw;->a:Lhhu;

    .line 30
    :cond_1
    iget-object v0, p0, Lhhw;->a:Lhhu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 32
    :sswitch_2
    iget-object v0, p0, Lhhw;->b:Lhhq;

    if-nez v0, :cond_2

    .line 33
    new-instance v0, Lhhq;

    invoke-direct {v0}, Lhhq;-><init>()V

    iput-object v0, p0, Lhhw;->b:Lhhq;

    .line 34
    :cond_2
    iget-object v0, p0, Lhhw;->b:Lhhq;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 24
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lhhw;->a:Lhhu;

    if-eqz v0, :cond_0

    .line 9
    const/4 v0, 0x2

    iget-object v1, p0, Lhhw;->a:Lhhu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 10
    :cond_0
    iget-object v0, p0, Lhhw;->b:Lhhq;

    if-eqz v0, :cond_1

    .line 11
    const/4 v0, 0x3

    iget-object v1, p0, Lhhw;->b:Lhhq;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 12
    :cond_1
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 13
    return-void
.end method
