.class final Lhds;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:Lhbs;

.field public final a:[Ljava/lang/Object;

.field public b:Ljava/lang/Class;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:[I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Ljava/lang/reflect/Field;

.field private q:Lhdt;

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Lhbs;

.field private z:Lhbs;


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const v1, 0x7fffffff

    iput v1, p0, Lhds;->u:I

    .line 3
    const/high16 v1, -0x80000000

    iput v1, p0, Lhds;->v:I

    .line 4
    iput v2, p0, Lhds;->w:I

    .line 5
    iput v2, p0, Lhds;->x:I

    .line 7
    sget-object v1, Lhbs;->a:Lhbs;

    .line 8
    iput-object v1, p0, Lhds;->y:Lhbs;

    .line 10
    sget-object v1, Lhbs;->a:Lhbs;

    .line 11
    iput-object v1, p0, Lhds;->z:Lhbs;

    .line 13
    sget-object v1, Lhbs;->a:Lhbs;

    .line 14
    iput-object v1, p0, Lhds;->A:Lhbs;

    .line 15
    iput-object p1, p0, Lhds;->b:Ljava/lang/Class;

    .line 16
    new-instance v1, Lhdt;

    invoke-direct {v1, p2}, Lhdt;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lhds;->q:Lhdt;

    .line 17
    iput-object p3, p0, Lhds;->a:[Ljava/lang/Object;

    .line 19
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 20
    iput v1, p0, Lhds;->c:I

    .line 22
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 23
    iput v1, p0, Lhds;->d:I

    .line 24
    iget v1, p0, Lhds;->d:I

    if-nez v1, :cond_0

    .line 25
    iput v2, p0, Lhds;->e:I

    .line 26
    iput v2, p0, Lhds;->r:I

    .line 27
    iput v2, p0, Lhds;->f:I

    .line 28
    iput v2, p0, Lhds;->g:I

    .line 29
    iput v2, p0, Lhds;->h:I

    .line 30
    iput v2, p0, Lhds;->i:I

    .line 31
    iput-object v0, p0, Lhds;->j:[I

    .line 59
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 34
    iput v1, p0, Lhds;->e:I

    .line 36
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 37
    iput v1, p0, Lhds;->r:I

    .line 39
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 40
    iput v1, p0, Lhds;->f:I

    .line 42
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 43
    iput v1, p0, Lhds;->g:I

    .line 45
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 46
    iput v1, p0, Lhds;->h:I

    .line 48
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 49
    iput v1, p0, Lhds;->i:I

    .line 52
    iget-object v1, p0, Lhds;->q:Lhdt;

    invoke-virtual {v1}, Lhdt;->b()I

    move-result v1

    .line 54
    if-nez v1, :cond_1

    .line 57
    :goto_1
    iput-object v0, p0, Lhds;->j:[I

    .line 58
    iget v0, p0, Lhds;->e:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lhds;->r:I

    add-int/2addr v0, v1

    iput v0, p0, Lhds;->s:I

    goto :goto_0

    .line 56
    :cond_1
    new-array v0, v1, [I

    goto :goto_1
.end method

.method static a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 2

    .prologue
    .line 197
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private final a(ILhby;)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lhds;->z:Lhbs;

    .line 188
    sget-object v1, Lhbs;->a:Lhbs;

    .line 189
    if-ne v0, v1, :cond_0

    .line 190
    new-instance v0, Lhbs;

    invoke-direct {v0}, Lhbs;-><init>()V

    iput-object v0, p0, Lhds;->z:Lhbs;

    .line 191
    :cond_0
    iget-object v0, p0, Lhds;->z:Lhbs;

    invoke-virtual {v0, p1, p2}, Lhbs;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 192
    return-void
.end method

.method private final a(ILjava/lang/Class;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lhds;->y:Lhbs;

    .line 180
    sget-object v1, Lhbs;->a:Lhbs;

    .line 181
    if-ne v0, v1, :cond_0

    .line 182
    new-instance v0, Lhbs;

    invoke-direct {v0}, Lhbs;-><init>()V

    iput-object v0, p0, Lhds;->y:Lhbs;

    .line 183
    :cond_0
    iget-object v0, p0, Lhds;->y:Lhbs;

    invoke-virtual {v0, p1, p2}, Lhbs;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 184
    return-void
.end method

.method private final g()I
    .locals 7

    .prologue
    const v6, 0xd800

    .line 60
    iget-object v2, p0, Lhds;->q:Lhdt;

    .line 61
    iget-object v0, v2, Lhdt;->a:Ljava/lang/String;

    iget v1, v2, Lhdt;->b:I

    add-int/lit8 v3, v1, 0x1

    iput v3, v2, Lhdt;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 62
    if-ge v0, v6, :cond_0

    .line 70
    :goto_0
    return v0

    .line 64
    :cond_0
    and-int/lit16 v1, v0, 0x1fff

    .line 65
    const/16 v0, 0xd

    .line 66
    :goto_1
    iget-object v3, v2, Lhdt;->a:Ljava/lang/String;

    iget v4, v2, Lhdt;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v2, Lhdt;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-lt v3, v6, :cond_1

    .line 67
    and-int/lit16 v3, v3, 0x1fff

    shl-int/2addr v3, v0

    or-int/2addr v1, v3

    .line 68
    add-int/lit8 v0, v0, 0xd

    goto :goto_1

    .line 69
    :cond_1
    shl-int v0, v3, v0

    or-int/2addr v0, v1

    .line 70
    goto :goto_0
.end method

.method private final h()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lhds;->a:[Ljava/lang/Object;

    iget v1, p0, Lhds;->s:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhds;->s:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private i()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 151
    iget v1, p0, Lhds;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final j()V
    .locals 5

    .prologue
    const/16 v4, 0x36

    .line 158
    iget-object v0, p0, Lhds;->q:Lhdt;

    invoke-virtual {v0}, Lhdt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lhds;->q:Lhdt;

    .line 161
    iget v1, v1, Lhdt;->b:I

    .line 162
    iget-object v2, p0, Lhds;->q:Lhdt;

    .line 164
    iget-object v2, v2, Lhdt;->a:Ljava/lang/String;

    .line 165
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x42

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "decoder position = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not at end (length = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lhds;->a:[Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget v0, p0, Lhds;->s:I

    iget-object v1, p0, Lhds;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-eq v0, v1, :cond_1

    .line 167
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->s:I

    iget-object v2, p0, Lhds;->a:[Ljava/lang/Object;

    array-length v2, v2

    const/16 v3, 0x41

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "objectsPosition = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not at end (length = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_1
    iget-object v0, p0, Lhds;->j:[I

    if-eqz v0, :cond_2

    iget v0, p0, Lhds;->t:I

    iget-object v1, p0, Lhds;->j:[I

    array-length v1, v1

    if-eq v0, v1, :cond_2

    .line 169
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->t:I

    iget-object v2, p0, Lhds;->j:[I

    array-length v2, v2

    const/16 v3, 0x4a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "checkInitializedPosition = "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not at end (length = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_2
    iget v0, p0, Lhds;->d:I

    if-lez v0, :cond_3

    iget v0, p0, Lhds;->f:I

    iget v1, p0, Lhds;->u:I

    if-eq v0, v1, :cond_3

    .line 171
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->f:I

    iget v2, p0, Lhds;->u:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "minFieldNumber is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_3
    iget v0, p0, Lhds;->d:I

    if-lez v0, :cond_4

    iget v0, p0, Lhds;->g:I

    iget v1, p0, Lhds;->v:I

    if-eq v0, v1, :cond_4

    .line 173
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->g:I

    iget v2, p0, Lhds;->v:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "maxFieldNumber is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_4
    iget v0, p0, Lhds;->h:I

    iget v1, p0, Lhds;->w:I

    if-eq v0, v1, :cond_5

    .line 175
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->h:I

    iget v2, p0, Lhds;->w:I

    const/16 v3, 0x35

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "mapFieldCount is "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_5
    iget v0, p0, Lhds;->i:I

    iget v1, p0, Lhds;->x:I

    if-eq v0, v1, :cond_6

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lhds;->i:I

    iget v2, p0, Lhds;->x:I

    const/16 v3, 0x3a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "repeatedFieldCount is "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_6
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lhds;->q:Lhdt;

    invoke-virtual {v0}, Lhdt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 150
    :goto_0
    return v0

    .line 74
    :cond_0
    invoke-direct {p0}, Lhds;->g()I

    move-result v0

    iput v0, p0, Lhds;->k:I

    .line 75
    invoke-direct {p0}, Lhds;->g()I

    move-result v0

    iput v0, p0, Lhds;->l:I

    .line 76
    iget v0, p0, Lhds;->l:I

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lhds;->m:I

    .line 77
    iget v0, p0, Lhds;->k:I

    iget v3, p0, Lhds;->u:I

    if-ge v0, v3, :cond_1

    .line 78
    iget v0, p0, Lhds;->k:I

    iput v0, p0, Lhds;->u:I

    .line 79
    :cond_1
    iget v0, p0, Lhds;->k:I

    iget v3, p0, Lhds;->v:I

    if-le v0, v3, :cond_2

    .line 80
    iget v0, p0, Lhds;->k:I

    iput v0, p0, Lhds;->v:I

    .line 81
    :cond_2
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->j:Lhbm;

    .line 82
    iget v3, v3, Lhbm;->k:I

    .line 83
    if-ne v0, v3, :cond_7

    .line 84
    iget v0, p0, Lhds;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhds;->w:I

    .line 93
    :cond_3
    :goto_1
    iget v0, p0, Lhds;->l:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_8

    move v0, v2

    .line 94
    :goto_2
    if-eqz v0, :cond_4

    .line 95
    iget-object v0, p0, Lhds;->j:[I

    iget v3, p0, Lhds;->t:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lhds;->t:I

    iget v4, p0, Lhds;->k:I

    aput v4, v0, v3

    .line 96
    :cond_4
    invoke-virtual {p0}, Lhds;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 97
    invoke-direct {p0}, Lhds;->g()I

    move-result v0

    iput v0, p0, Lhds;->n:I

    .line 98
    iget v0, p0, Lhds;->m:I

    sget-object v1, Lhbm;->b:Lhbm;

    .line 99
    iget v1, v1, Lhbm;->k:I

    .line 100
    add-int/lit8 v1, v1, 0x33

    if-eq v0, v1, :cond_5

    iget v0, p0, Lhds;->m:I

    sget-object v1, Lhbm;->d:Lhbm;

    .line 102
    iget v1, v1, Lhbm;->k:I

    .line 103
    add-int/lit8 v1, v1, 0x33

    if-ne v0, v1, :cond_9

    .line 104
    :cond_5
    iget v1, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {p0, v1, v0}, Lhds;->a(ILjava/lang/Class;)V

    :cond_6
    :goto_3
    move v0, v2

    .line 150
    goto :goto_0

    .line 85
    :cond_7
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->e:Lhbm;

    .line 86
    iget v3, v3, Lhbm;->k:I

    .line 87
    if-lt v0, v3, :cond_3

    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->i:Lhbm;

    .line 89
    iget v3, v3, Lhbm;->k:I

    .line 90
    if-gt v0, v3, :cond_3

    .line 91
    iget v0, p0, Lhds;->x:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhds;->x:I

    goto :goto_1

    :cond_8
    move v0, v1

    .line 93
    goto :goto_2

    .line 105
    :cond_9
    iget v0, p0, Lhds;->m:I

    sget-object v1, Lhbm;->c:Lhbm;

    .line 106
    iget v1, v1, Lhbm;->k:I

    .line 107
    add-int/lit8 v1, v1, 0x33

    if-ne v0, v1, :cond_6

    .line 108
    invoke-direct {p0}, Lhds;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    iget v1, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    invoke-direct {p0, v1, v0}, Lhds;->a(ILhby;)V

    goto :goto_3

    .line 110
    :cond_a
    iget-object v3, p0, Lhds;->b:Ljava/lang/Class;

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lhds;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lhds;->p:Ljava/lang/reflect/Field;

    .line 111
    invoke-virtual {p0}, Lhds;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 112
    invoke-direct {p0}, Lhds;->g()I

    move-result v0

    iput v0, p0, Lhds;->o:I

    .line 113
    :cond_b
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->b:Lhbm;

    .line 114
    iget v3, v3, Lhbm;->k:I

    .line 115
    if-eq v0, v3, :cond_c

    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->d:Lhbm;

    .line 116
    iget v3, v3, Lhbm;->k:I

    .line 117
    if-ne v0, v3, :cond_d

    .line 118
    :cond_c
    iget v0, p0, Lhds;->k:I

    iget-object v1, p0, Lhds;->p:Ljava/lang/reflect/Field;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lhds;->a(ILjava/lang/Class;)V

    goto :goto_3

    .line 119
    :cond_d
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->f:Lhbm;

    .line 120
    iget v3, v3, Lhbm;->k:I

    .line 121
    if-eq v0, v3, :cond_e

    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->i:Lhbm;

    .line 123
    iget v3, v3, Lhbm;->k:I

    .line 124
    if-ne v0, v3, :cond_f

    .line 125
    :cond_e
    iget v1, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {p0, v1, v0}, Lhds;->a(ILjava/lang/Class;)V

    goto/16 :goto_3

    .line 126
    :cond_f
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->c:Lhbm;

    .line 127
    iget v3, v3, Lhbm;->k:I

    .line 128
    if-eq v0, v3, :cond_10

    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->g:Lhbm;

    .line 130
    iget v3, v3, Lhbm;->k:I

    .line 131
    if-eq v0, v3, :cond_10

    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->h:Lhbm;

    .line 133
    iget v3, v3, Lhbm;->k:I

    .line 134
    if-ne v0, v3, :cond_11

    .line 135
    :cond_10
    invoke-direct {p0}, Lhds;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 136
    iget v1, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    invoke-direct {p0, v1, v0}, Lhds;->a(ILhby;)V

    goto/16 :goto_3

    .line 137
    :cond_11
    iget v0, p0, Lhds;->m:I

    sget-object v3, Lhbm;->j:Lhbm;

    .line 138
    iget v3, v3, Lhbm;->k:I

    .line 139
    if-ne v0, v3, :cond_6

    .line 140
    iget v0, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v3

    .line 141
    iget-object v4, p0, Lhds;->A:Lhbs;

    .line 142
    sget-object v5, Lhbs;->a:Lhbs;

    .line 143
    if-ne v4, v5, :cond_12

    .line 144
    new-instance v4, Lhbs;

    invoke-direct {v4}, Lhbs;-><init>()V

    iput-object v4, p0, Lhds;->A:Lhbs;

    .line 145
    :cond_12
    iget-object v4, p0, Lhds;->A:Lhbs;

    invoke-virtual {v4, v0, v3}, Lhbs;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget v0, p0, Lhds;->l:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_13

    move v1, v2

    .line 148
    :cond_13
    if-eqz v1, :cond_6

    .line 149
    iget v1, p0, Lhds;->k:I

    invoke-direct {p0}, Lhds;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhby;

    invoke-direct {p0, v1, v0}, Lhds;->a(ILhby;)V

    goto/16 :goto_3
.end method

.method final b()Z
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lhds;->m:I

    sget-object v1, Lhbm;->j:Lhbm;

    .line 153
    iget v1, v1, Lhbm;->k:I

    .line 154
    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Z
    .locals 2

    .prologue
    .line 155
    invoke-direct {p0}, Lhds;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhds;->m:I

    sget-object v1, Lhbm;->d:Lhbm;

    .line 156
    iget v1, v1, Lhbm;->k:I

    .line 157
    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()Lhbs;
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lhds;->j()V

    .line 186
    iget-object v0, p0, Lhds;->y:Lhbs;

    return-object v0
.end method

.method final e()Lhbs;
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Lhds;->j()V

    .line 194
    iget-object v0, p0, Lhds;->z:Lhbs;

    return-object v0
.end method

.method final f()Lhbs;
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lhds;->j()V

    .line 196
    iget-object v0, p0, Lhds;->A:Lhbs;

    return-object v0
.end method
