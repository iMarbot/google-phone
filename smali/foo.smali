.class public final Lfoo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final CPU_FILE_PREFIX:Ljava/lang/String; = "/sys/devices/system/cpu/"

.field public static final CPU_MONITOR_THREAD_NAME:Ljava/lang/String; = "CpuMonitor"

.field public static final CURR_CPU_FREQ_FILE_SUFFIX:Ljava/lang/String; = "/cpufreq/scaling_cur_freq"

.field public static final MAX_CPU_FREQ_FILE_SUFFIX:Ljava/lang/String; = "/cpufreq/cpuinfo_max_freq"

.field public static final POLL_PERIOD_MS:I = 0x3e8

.field public static final PRESENT_CPU_COUNT:I

.field public static instance:Lfoo;


# instance fields
.field public final cpuMonitorHandler:Landroid/os/Handler;

.field public final cpuMonitorThread:Landroid/os/HandlerThread;

.field public volatile lastCpuFrequency:I

.field public volatile lastCpuUsage:I

.field public lastFrequencyPercentage:D

.field public lastProcStat:Lfoq;

.field public maxCpuFrequency:[I

.field public volatile onlineCpuCount:I

.field public final sampleCpuUtilizationRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 130
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 131
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lfoo;->PRESENT_CPU_COUNT:I

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    const-string v0, "present"

    invoke-static {v0}, Lfoo;->getCpuCountForState(Ljava/lang/String;)I

    move-result v0

    sput v0, Lfoo;->PRESENT_CPU_COUNT:I

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lfop;

    invoke-direct {v0, p0}, Lfop;-><init>(Lfoo;)V

    iput-object v0, p0, Lfoo;->sampleCpuUtilizationRunnable:Ljava/lang/Runnable;

    .line 3
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lfoo;->lastFrequencyPercentage:D

    .line 4
    new-instance v0, Lfoq;

    invoke-direct {v0, p0, v2, v2}, Lfoq;-><init>(Lfoo;II)V

    iput-object v0, p0, Lfoo;->lastProcStat:Lfoq;

    .line 5
    sget v0, Lfoo;->PRESENT_CPU_COUNT:I

    new-array v0, v0, [I

    iput-object v0, p0, Lfoo;->maxCpuFrequency:[I

    .line 6
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CpuMonitor"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lfoo;->cpuMonitorThread:Landroid/os/HandlerThread;

    .line 7
    iget-object v0, p0, Lfoo;->cpuMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 8
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lfoo;->cpuMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lfoo;->cpuMonitorHandler:Landroid/os/Handler;

    .line 9
    iget-object v0, p0, Lfoo;->cpuMonitorHandler:Landroid/os/Handler;

    iget-object v1, p0, Lfoo;->sampleCpuUtilizationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 10
    return-void
.end method

.method static synthetic access$000(Lfoo;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lfoo;->sampleCpuUtilizationRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lfoo;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lfoo;->cpuMonitorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lfoo;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lfoo;->sampleCpuUtilization()V

    return-void
.end method

.method private static getCpuCountForState(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 60
    .line 61
    const-string v0, "/sys/devices/system/cpu/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lfoo;->readFirstLineFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    const-string v2, "\\-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 64
    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 65
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 66
    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 67
    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    .line 68
    if-gtz v0, :cond_0

    move v0, v1

    :cond_0
    move v1, v0

    .line 73
    :cond_1
    :goto_1
    return v1

    .line 61
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    const-string v2, "Could not parse cpu count for state: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static getInstance()Lfoo;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lfoo;->instance:Lfoo;

    return-object v0
.end method

.method public static getPresentCpuCount()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lfoo;->PRESENT_CPU_COUNT:I

    return v0
.end method

.method private static readCpuFrequencyValueFromFile(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 99
    .line 100
    invoke-static {p0}, Lfoo;->readFirstLineFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 105
    :goto_0
    return v0

    .line 104
    :catch_0
    move-exception v2

    const-string v2, "Could not parse cpu frequency value: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lfvh;->logv(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static readFirstLineFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 74
    .line 76
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 77
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 78
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 98
    :cond_0
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v1

    const-string v1, "Could not close file reader."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_3
    const-string v3, "Could not find or read from file: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-static {v2}, Lfvh;->logd(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 85
    if-eqz v1, :cond_0

    .line 86
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 89
    :catch_2
    move-exception v1

    const-string v1, "Could not close file reader."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_1
    :try_start_5
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 91
    :catchall_0
    move-exception v0

    .line 92
    :goto_3
    if-eqz v1, :cond_2

    .line 93
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 97
    :cond_2
    :goto_4
    throw v0

    .line 96
    :catch_3
    move-exception v1

    const-string v1, "Could not close file reader."

    invoke-static {v1}, Lfvh;->logw(Ljava/lang/String;)V

    goto :goto_4

    .line 91
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    .line 84
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method private final readIdleAndRunTime()Lfoq;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 106
    .line 108
    const-string v1, "/proc/stat"

    invoke-static {v1}, Lfoo;->readFirstLineFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    if-nez v3, :cond_0

    .line 110
    new-instance v1, Lfoq;

    invoke-direct {v1, p0, v0, v0}, Lfoq;-><init>(Lfoo;II)V

    move-object v0, v1

    .line 126
    :goto_0
    return-object v0

    .line 111
    :cond_0
    const/4 v1, 0x0

    .line 112
    :try_start_0
    new-instance v2, Ljava/util/Scanner;

    invoke-direct {v2, v3}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :try_start_1
    invoke-virtual {v2}, Ljava/util/Scanner;->next()Ljava/lang/String;

    .line 114
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I

    move-result v1

    .line 115
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I

    move-result v3

    .line 116
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 117
    add-int/2addr v1, v3

    add-int/2addr v1, v4

    .line 118
    :try_start_2
    invoke-virtual {v2}, Ljava/util/Scanner;->nextInt()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    .line 119
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    .line 126
    :cond_1
    :goto_1
    new-instance v2, Lfoq;

    invoke-direct {v2, p0, v1, v0}, Lfoq;-><init>(Lfoo;II)V

    move-object v0, v2

    goto :goto_0

    .line 121
    :catch_0
    move-exception v2

    move-object v2, v1

    move v1, v0

    :goto_2
    :try_start_3
    const-string v3, "Could not parse /proc/stat"

    invoke-static {v3}, Lfvh;->logw(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 122
    if-eqz v2, :cond_1

    .line 123
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    goto :goto_1

    .line 124
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_2

    .line 125
    invoke-virtual {v2}, Ljava/util/Scanner;->close()V

    :cond_2
    throw v0

    .line 124
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 121
    :catch_1
    move-exception v1

    move v1, v0

    goto :goto_2

    :catch_2
    move-exception v3

    goto :goto_2
.end method

.method private final sampleCpuUtilization()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 24
    sget v0, Lfoo;->PRESENT_CPU_COUNT:I

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-string v0, "online"

    invoke-static {v0}, Lfoo;->getCpuCountForState(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfoo;->onlineCpuCount:I

    .line 29
    iget v0, p0, Lfoo;->onlineCpuCount:I

    iget-object v2, p0, Lfoo;->maxCpuFrequency:[I

    array-length v2, v2

    if-le v0, v2, :cond_2

    .line 30
    iget-object v0, p0, Lfoo;->maxCpuFrequency:[I

    iget v2, p0, Lfoo;->onlineCpuCount:I

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lfoo;->maxCpuFrequency:[I

    :cond_2
    move v0, v1

    move v2, v1

    move v3, v1

    .line 31
    :goto_1
    iget v4, p0, Lfoo;->onlineCpuCount:I

    if-ge v0, v4, :cond_7

    .line 32
    const/16 v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "/sys/devices/system/cpu/cpu"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 33
    iget-object v4, p0, Lfoo;->maxCpuFrequency:[I

    aget v4, v4, v0

    if-nez v4, :cond_3

    .line 34
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v4, "/cpufreq/cpuinfo_max_freq"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 35
    :goto_2
    iget-object v6, p0, Lfoo;->maxCpuFrequency:[I

    invoke-static {v4}, Lfoo;->readCpuFrequencyValueFromFile(Ljava/lang/String;)I

    move-result v4

    aput v4, v6, v0

    .line 36
    :cond_3
    iget-object v4, p0, Lfoo;->maxCpuFrequency:[I

    aget v4, v4, v0

    add-int/2addr v2, v4

    .line 37
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v4, "/cpufreq/scaling_cur_freq"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 38
    :goto_3
    invoke-static {v4}, Lfoo;->readCpuFrequencyValueFromFile(Ljava/lang/String;)I

    move-result v4

    .line 39
    add-int/2addr v3, v4

    .line 40
    if-nez v0, :cond_4

    .line 41
    iput v4, p0, Lfoo;->lastCpuFrequency:I

    .line 42
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34
    :cond_5
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 37
    :cond_6
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 43
    :cond_7
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    int-to-double v6, v3

    mul-double/2addr v4, v6

    int-to-double v2, v2

    div-double/2addr v4, v2

    .line 44
    iget-wide v2, p0, Lfoo;->lastFrequencyPercentage:D

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-lez v0, :cond_8

    .line 45
    iget-wide v2, p0, Lfoo;->lastFrequencyPercentage:D

    add-double/2addr v2, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v6

    .line 47
    :goto_4
    iput-wide v4, p0, Lfoo;->lastFrequencyPercentage:D

    .line 48
    invoke-direct {p0}, Lfoo;->readIdleAndRunTime()Lfoq;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0}, Lfoq;->getRunTime()I

    move-result v4

    iget-object v5, p0, Lfoo;->lastProcStat:Lfoq;

    invoke-virtual {v5}, Lfoq;->getRunTime()I

    move-result v5

    sub-int/2addr v4, v5

    .line 52
    invoke-virtual {v0}, Lfoq;->getIdleTime()I

    move-result v5

    iget-object v6, p0, Lfoo;->lastProcStat:Lfoq;

    invoke-virtual {v6}, Lfoq;->getIdleTime()I

    move-result v6

    sub-int/2addr v5, v6

    .line 53
    iput-object v0, p0, Lfoo;->lastProcStat:Lfoq;

    .line 54
    add-int v0, v4, v5

    .line 55
    if-nez v0, :cond_9

    .line 56
    iput v1, p0, Lfoo;->lastCpuUsage:I

    goto/16 :goto_0

    :cond_8
    move-wide v2, v4

    .line 46
    goto :goto_4

    .line 57
    :cond_9
    int-to-double v4, v4

    mul-double/2addr v2, v4

    int-to-double v4, v0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    .line 58
    const/16 v2, 0x64

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lfoo;->lastCpuUsage:I

    goto/16 :goto_0
.end method

.method public static startMonitoring()V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lfoo;->instance:Lfoo;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lfoo;

    invoke-direct {v0}, Lfoo;-><init>()V

    sput-object v0, Lfoo;->instance:Lfoo;

    .line 14
    :cond_0
    return-void
.end method


# virtual methods
.method public final getCurrentCpuFrequencyKHz()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lfoo;->lastCpuFrequency:I

    return v0
.end method

.method public final getCurrentCpuUtilization()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lfoo;->lastCpuUsage:I

    return v0
.end method

.method public final getMaxCpuFrequencyKHz()I
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lfoo;->maxCpuFrequency:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final getOnlineCpuCount()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lfoo;->onlineCpuCount:I

    return v0
.end method

.method public final stopMonitoring()V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lfoo;->cpuMonitorHandler:Landroid/os/Handler;

    iget-object v1, p0, Lfoo;->sampleCpuUtilizationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 16
    iget-object v0, p0, Lfoo;->cpuMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    iget-object v0, p0, Lfoo;->cpuMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 18
    :cond_0
    return-void
.end method
