.class final Lfri;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final addedParticipants:Ljava/util/Set;

.field public final call:Lfnp;

.field public final callManager:Lfnv;

.field public final callServiceCallbacks:Lfvt;

.field public final changedParticipants:Ljava/util/Set;

.field public focusedParticipant:Lfrh;

.field public hasCallEnded:Z

.field public final listeners:Ljava/util/List;

.field public final localParticipant:Lfrh;

.field public loudestParticipant:Lfrh;

.field public needsToUpdateFocusedParticipant:Z

.field public participantUpdateInQueue:Z

.field public final participantUpdaterTask:Ljava/lang/Runnable;

.field public final participants:Ljava/util/Map;

.field public final participantsLock:Ljava/lang/Object;

.field public final removedParticipants:Ljava/util/Set;

.field public final stateListener:Lfrl;


# direct methods
.method constructor <init>(Lfnp;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfri;->listeners:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfri;->participantsLock:Ljava/lang/Object;

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfri;->participantUpdateInQueue:Z

    .line 5
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lfri;->participants:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lfri;->addedParticipants:Ljava/util/Set;

    .line 7
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lfri;->changedParticipants:Ljava/util/Set;

    .line 8
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lfri;->removedParticipants:Ljava/util/Set;

    .line 9
    iput-object p1, p0, Lfri;->call:Lfnp;

    .line 10
    invoke-virtual {p1}, Lfnp;->getCallbacks()Lfvt;

    move-result-object v0

    iput-object v0, p0, Lfri;->callServiceCallbacks:Lfvt;

    .line 11
    invoke-virtual {p1}, Lfnp;->getCallManager()Lfnv;

    move-result-object v0

    iput-object v0, p0, Lfri;->callManager:Lfnv;

    .line 12
    invoke-virtual {p1}, Lfnp;->getCollections()Lfnm;

    move-result-object v0

    const-class v1, Lfnf;

    .line 13
    invoke-virtual {v0, v1}, Lfnm;->getCollection(Ljava/lang/Class;)Lfnk;

    move-result-object v0

    check-cast v0, Lfnf;

    new-instance v1, Lfrk;

    invoke-direct {v1, p0}, Lfrk;-><init>(Lfri;)V

    .line 14
    invoke-interface {v0, v1}, Lfnf;->addListener(Lfnl;)V

    .line 15
    new-instance v0, Lfrl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfrl;-><init>(Lfri;Lfmt;)V

    iput-object v0, p0, Lfri;->stateListener:Lfrl;

    .line 16
    iget-object v0, p0, Lfri;->callManager:Lfnv;

    iget-object v1, p0, Lfri;->stateListener:Lfrl;

    invoke-virtual {v0, v1}, Lfnv;->addCallStateListener(Lfof;)V

    .line 17
    new-instance v0, Lfrh;

    invoke-direct {v0, p1}, Lfrh;-><init>(Lfnp;)V

    iput-object v0, p0, Lfri;->localParticipant:Lfrh;

    .line 18
    new-instance v0, Lfrj;

    invoke-direct {v0, p0}, Lfrj;-><init>(Lfri;)V

    iput-object v0, p0, Lfri;->participantUpdaterTask:Ljava/lang/Runnable;

    .line 19
    return-void
.end method

.method static synthetic access$100(Lfri;)Lfrh;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lfri;->localParticipant:Lfrh;

    return-object v0
.end method

.method static synthetic access$1000(Lfri;)Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lfri;->hasCallEnded:Z

    return v0
.end method

.method static synthetic access$1002(Lfri;Z)Z
    .locals 0

    .prologue
    .line 150
    iput-boolean p1, p0, Lfri;->hasCallEnded:Z

    return p1
.end method

.method static synthetic access$1100(Lfri;)Ljava/util/List;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lfri;->listeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200(Lfri;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lfri;->updateFocusedParticipant()V

    return-void
.end method

.method static synthetic access$1300(Lfri;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfri;->removedParticipants:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1400(Lfri;)Lfrh;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lfri;->loudestParticipant:Lfrh;

    return-object v0
.end method

.method static synthetic access$1402(Lfri;Lfrh;)Lfrh;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lfri;->loudestParticipant:Lfrh;

    return-object p1
.end method

.method static synthetic access$1500(Lfri;)Lfrl;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lfri;->stateListener:Lfrl;

    return-object v0
.end method

.method static synthetic access$1600(Lfri;)Lfnv;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lfri;->callManager:Lfnv;

    return-object v0
.end method

.method static synthetic access$1702(Lfri;Z)Z
    .locals 0

    .prologue
    .line 151
    iput-boolean p1, p0, Lfri;->participantUpdateInQueue:Z

    return p1
.end method

.method static synthetic access$200(Lfri;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lfri;Lfue;Lfuf;)Z
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Lfri;->isConnectingOrConnected(Lfue;Lfuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lfuf;)Z
    .locals 1

    .prologue
    .line 136
    invoke-static {p0}, Lfri;->isEventThatModifiesEndpoint(Lfuf;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lfri;)Lfnp;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lfri;->call:Lfnp;

    return-object v0
.end method

.method static synthetic access$600(Lfri;Lfrh;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lfri;->maybeUpdateParticipantState(Lfrh;)V

    return-void
.end method

.method static synthetic access$700(Lfri;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lfri;->participantsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lfri;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lfri;->addedParticipants:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$900(Lfri;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lfri;->enqueueUpdateTask()V

    return-void
.end method

.method private final enqueueUpdateTask()V
    .locals 3

    .prologue
    .line 82
    iget-object v1, p0, Lfri;->participantsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-boolean v0, p0, Lfri;->hasCallEnded:Z

    if-eqz v0, :cond_0

    .line 84
    monitor-exit v1

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-boolean v0, p0, Lfri;->participantUpdateInQueue:Z

    if-nez v0, :cond_1

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfri;->participantUpdateInQueue:Z

    .line 87
    iget-object v0, p0, Lfri;->participantUpdaterTask:Ljava/lang/Runnable;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lhcw;->a(Ljava/lang/Runnable;Z)V

    .line 88
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private final isConnectingOrConnected(Lfue;Lfuf;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 89
    instance-of v0, p2, Lfug;

    if-nez v0, :cond_0

    instance-of v0, p2, Lfuc;

    if-eqz v0, :cond_3

    .line 90
    :cond_0
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 91
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lfue;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 91
    goto :goto_0

    :cond_3
    move v0, v1

    .line 92
    goto :goto_0
.end method

.method private static isEventThatModifiesEndpoint(Lfuf;)Z
    .locals 1

    .prologue
    .line 93
    instance-of v0, p0, Lfub;

    if-nez v0, :cond_0

    instance-of v0, p0, Lfuc;

    if-nez v0, :cond_0

    instance-of v0, p0, Lfuj;

    if-nez v0, :cond_0

    instance-of v0, p0, Lful;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final maybeUpdateParticipantState(Lfrh;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p1}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v3

    .line 72
    iget-object v0, p0, Lfri;->loudestParticipant:Lfrh;

    if-ne p1, v0, :cond_1

    move v0, v1

    .line 73
    :goto_0
    iput-boolean v0, v3, Lfvz;->j:Z

    .line 74
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    if-ne p1, v0, :cond_2

    .line 75
    :goto_1
    iput-boolean v1, v3, Lfvz;->k:Z

    .line 76
    invoke-virtual {p1}, Lfrh;->markDirty()V

    .line 77
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 72
    goto :goto_0

    :cond_2
    move v1, v2

    .line 74
    goto :goto_1
.end method

.method private final updateFocusedParticipant()V
    .locals 4

    .prologue
    .line 42
    iget-object v1, p0, Lfri;->focusedParticipant:Lfrh;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    .line 44
    iget-object v0, p0, Lfri;->loudestParticipant:Lfrh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfri;->loudestParticipant:Lfrh;

    invoke-virtual {v0}, Lfrh;->isLocalUser()Z

    move-result v0

    if-nez v0, :cond_2

    .line 45
    iget-object v0, p0, Lfri;->loudestParticipant:Lfrh;

    iput-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    .line 56
    :cond_0
    :goto_0
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lfri;->localParticipant:Lfrh;

    iput-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    .line 58
    :cond_1
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    if-eq v1, v0, :cond_5

    .line 59
    invoke-direct {p0, v1}, Lfri;->maybeUpdateParticipantState(Lfrh;)V

    .line 60
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    invoke-direct {p0, v0}, Lfri;->maybeUpdateParticipantState(Lfrh;)V

    .line 61
    iget-object v1, p0, Lfri;->participantsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lfri;->needsToUpdateFocusedParticipant:Z

    .line 63
    invoke-direct {p0}, Lfri;->enqueueUpdateTask()V

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    iget-boolean v0, p0, Lfri;->hasCallEnded:Z

    if-nez v0, :cond_5

    .line 66
    iget-object v0, p0, Lfri;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrm;

    .line 67
    iget-object v2, p0, Lfri;->focusedParticipant:Lfrh;

    invoke-virtual {v0, v2}, Lfrm;->onFocusedParticipantChanged(Lfrh;)V

    goto :goto_1

    .line 46
    :cond_2
    if-eqz v1, :cond_3

    .line 47
    invoke-virtual {v1}, Lfrh;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48
    invoke-virtual {v1}, Lfrh;->isLocalUser()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    .line 49
    invoke-virtual {v1}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    iput-object v1, p0, Lfri;->focusedParticipant:Lfrh;

    goto :goto_0

    .line 51
    :cond_3
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 52
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lfrh;->isLocalUser()Z

    move-result v3

    if-nez v3, :cond_4

    .line 53
    iput-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 69
    :cond_5
    return-void
.end method


# virtual methods
.method public final addListener(Lfrm;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfri;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public final getFocusedParticipant()Lfrh;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    return-object v0
.end method

.method public final getLocalParticipant()Lfrh;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lfri;->localParticipant:Lfrh;

    return-object v0
.end method

.method public final getParticipant(Ljava/lang/String;)Lfrh;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 41
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getParticipants()Ljava/util/List;
    .locals 4

    .prologue
    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 27
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 28
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_1
    return-object v1
.end method

.method public final getPendingParticipants()Ljava/util/List;
    .locals 4

    .prologue
    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    iget-object v0, p0, Lfri;->participants:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 34
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 35
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_1
    return-object v1
.end method

.method final synthetic lambda$new$0$ParticipantManager()V
    .locals 6

    .prologue
    .line 94
    iget-object v1, p0, Lfri;->participantsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    :try_start_0
    iget-boolean v0, p0, Lfri;->participantUpdateInQueue:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfri;->hasCallEnded:Z

    if-eqz v0, :cond_2

    .line 96
    :cond_0
    monitor-exit v1

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 97
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfri;->participantUpdateInQueue:Z

    .line 98
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v2, p0, Lfri;->addedParticipants:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 99
    new-instance v2, Ljava/util/LinkedHashSet;

    iget-object v3, p0, Lfri;->changedParticipants:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 100
    new-instance v3, Ljava/util/LinkedHashSet;

    iget-object v4, p0, Lfri;->removedParticipants:Ljava/util/Set;

    invoke-direct {v3, v4}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 101
    iget-object v4, p0, Lfri;->addedParticipants:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 102
    iget-object v4, p0, Lfri;->changedParticipants:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 103
    iget-object v4, p0, Lfri;->removedParticipants:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 104
    iget-boolean v4, p0, Lfri;->needsToUpdateFocusedParticipant:Z

    .line 105
    const/4 v5, 0x0

    iput-boolean v5, p0, Lfri;->needsToUpdateFocusedParticipant:Z

    .line 106
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    invoke-interface {v2, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 108
    invoke-interface {v2, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 109
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 110
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 111
    iget-object v5, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v5, v0}, Lfvt;->onParticipantAdded(Lfvz;)V

    goto :goto_1

    .line 106
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 112
    :cond_3
    iget-object v5, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v5, v0}, Lfvt;->onPendingParticipantAdded(Lfvz;)V

    goto :goto_1

    .line 114
    :cond_4
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 115
    iget-object v2, p0, Lfri;->participants:Ljava/util/Map;

    invoke-virtual {v0}, Lfrh;->getParticipantId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 117
    iget-object v2, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfvt;->onParticipantChanged(Lfvz;)V

    goto :goto_2

    .line 118
    :cond_6
    iget-object v2, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfvt;->onPendingParticipantChanged(Lfvz;)V

    goto :goto_2

    .line 120
    :cond_7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 121
    invoke-virtual {v0}, Lfrh;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 122
    iget-object v2, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfvt;->onParticipantRemoved(Lfvz;)V

    goto :goto_3

    .line 123
    :cond_8
    iget-object v2, p0, Lfri;->callServiceCallbacks:Lfvt;

    invoke-virtual {v0}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfvt;->onPendingParticipantRemoved(Lfvz;)V

    goto :goto_3

    .line 125
    :cond_9
    if-eqz v4, :cond_1

    .line 126
    iget-object v0, p0, Lfri;->focusedParticipant:Lfrh;

    .line 127
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object v0, p0, Lfri;->callServiceCallbacks:Lfvt;

    iget-object v1, p0, Lfri;->focusedParticipant:Lfrh;

    .line 130
    invoke-virtual {v1}, Lfrh;->getParticipantInfo()Lfvz;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lfvt;->onFocusedParticipantChanged(Lfvz;)V

    goto/16 :goto_0
.end method

.method final maybeUpdateParticipant(Lfrh;)V
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lfri;->participantsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-object v0, p0, Lfri;->changedParticipants:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-direct {p0}, Lfri;->enqueueUpdateTask()V

    .line 81
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final release()V
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lfri;->callManager:Lfnv;

    iget-object v1, p0, Lfri;->stateListener:Lfrl;

    invoke-virtual {v0, v1}, Lfnv;->removeCallStateListener(Lfof;)V

    .line 21
    return-void
.end method

.method public final removeListener(Lfrm;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfri;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 25
    return-void
.end method
