.class public final Lhya;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhxv;


# instance fields
.field public a:I

.field private b:Lhxu;

.field private c:Lhxz;

.field private d:Lhvg;

.field private e:Lhxy;

.field private f:Lhyp;

.field private g:Lhyh;

.field private h:Lhxh;

.field private i:Lhxe;

.field private j:Lhxu;

.field private k:I

.field private l:Z

.field private m:I

.field private n:Lhxx;

.field private o:Lhxs;

.field private p:Lhxn;

.field private q:Lhxj;

.field private r:[B


# direct methods
.method public constructor <init>(Lhxh;Ljava/io/InputStream;Lhxz;Lhxu;Lhxu;Lhvg;Lhxy;Lhyp;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p3, p0, Lhya;->c:Lhxz;

    .line 3
    iput-object p4, p0, Lhya;->j:Lhxu;

    .line 4
    iput-object p5, p0, Lhya;->b:Lhxu;

    .line 5
    iput-object p6, p0, Lhya;->d:Lhvg;

    .line 6
    iput-object p7, p0, Lhya;->e:Lhxy;

    .line 7
    iput-object p8, p0, Lhya;->f:Lhyp;

    .line 8
    new-instance v0, Lhyh;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lhyh;-><init>(I)V

    iput-object v0, p0, Lhya;->g:Lhyh;

    .line 9
    iput v2, p0, Lhya;->k:I

    .line 10
    iput-boolean v2, p0, Lhya;->l:Z

    .line 11
    iput v2, p0, Lhya;->m:I

    .line 12
    iput-object p1, p0, Lhya;->h:Lhxh;

    .line 13
    new-instance v0, Lhxe;

    const/16 v1, 0x1000

    .line 14
    iget v2, p3, Lhxz;->a:I

    .line 15
    invoke-direct {v0, p2, v1, v2}, Lhxe;-><init>(Ljava/io/InputStream;II)V

    iput-object v0, p0, Lhya;->i:Lhxe;

    .line 16
    new-instance v0, Lhxj;

    iget-object v1, p0, Lhya;->i:Lhxe;

    .line 17
    iget v2, p3, Lhxz;->a:I

    .line 18
    invoke-direct {v0, v1, v2}, Lhxj;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lhya;->q:Lhxj;

    .line 19
    return-void
.end method

.method private final a(Lhxu;Lhxu;Ljava/io/InputStream;)Lhxv;
    .locals 9

    .prologue
    .line 197
    iget v0, p0, Lhya;->a:I

    sget v1, Lmg$c;->bc:I

    if-ne v0, v1, :cond_0

    .line 198
    new-instance v0, Lias;

    invoke-direct {v0, p3}, Lias;-><init>(Ljava/io/InputStream;)V

    .line 203
    :goto_0
    return-object v0

    .line 200
    :cond_0
    new-instance v0, Lhya;

    iget-object v1, p0, Lhya;->h:Lhxh;

    iget-object v3, p0, Lhya;->c:Lhxz;

    iget-object v6, p0, Lhya;->d:Lhvg;

    iget-object v7, p0, Lhya;->e:Lhxy;

    iget-object v2, p0, Lhya;->f:Lhyp;

    invoke-virtual {v2}, Lhyp;->c()Lhyp;

    move-result-object v8

    move-object v2, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v8}, Lhya;-><init>(Lhxh;Ljava/io/InputStream;Lhxz;Lhxu;Lhxu;Lhvg;Lhxy;Lhyp;)V

    .line 201
    iget v1, p0, Lhya;->a:I

    .line 202
    iput v1, v0, Lhya;->a:I

    goto :goto_0
.end method

.method private final a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lhya;->o:Lhxs;

    invoke-interface {v0}, Lhxs;->c()Ljava/lang/String;

    move-result-object v0

    .line 192
    invoke-static {v0}, Lhyl;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 193
    new-instance v0, Lhve;

    iget-object v1, p0, Lhya;->d:Lhvg;

    invoke-direct {v0, p1, v1}, Lhve;-><init>(Ljava/io/InputStream;Lhvg;)V

    move-object p1, v0

    .line 196
    :cond_0
    :goto_0
    return-object p1

    .line 194
    :cond_1
    invoke-static {v0}, Lhyl;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    new-instance v0, Lhvj;

    iget-object v1, p0, Lhya;->d:Lhvg;

    invoke-direct {v0, p1, v1}, Lhvj;-><init>(Ljava/io/InputStream;Lhvg;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private static a(Lhxu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lhxu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 253
    const-string v0, "Unknown"

    .line 254
    :goto_0
    return-object v0

    .line 225
    :pswitch_0
    const-string v0, "End of stream"

    goto :goto_0

    .line 227
    :pswitch_1
    const-string v0, "Start message"

    goto :goto_0

    .line 229
    :pswitch_2
    const-string v0, "End message"

    goto :goto_0

    .line 231
    :pswitch_3
    const-string v0, "Raw entity"

    goto :goto_0

    .line 233
    :pswitch_4
    const-string v0, "Start header"

    goto :goto_0

    .line 235
    :pswitch_5
    const-string v0, "Field"

    goto :goto_0

    .line 237
    :pswitch_6
    const-string v0, "End header"

    goto :goto_0

    .line 239
    :pswitch_7
    const-string v0, "Start multipart"

    goto :goto_0

    .line 241
    :pswitch_8
    const-string v0, "End multipart"

    goto :goto_0

    .line 243
    :pswitch_9
    const-string v0, "Preamble"

    goto :goto_0

    .line 245
    :pswitch_a
    const-string v0, "Epilogue"

    goto :goto_0

    .line 247
    :pswitch_b
    const-string v0, "Start bodypart"

    goto :goto_0

    .line 249
    :pswitch_c
    const-string v0, "End bodypart"

    goto :goto_0

    .line 251
    :pswitch_d
    const-string v0, "Body"

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lhxw;)V
    .locals 4

    .prologue
    .line 23
    iget-object v0, p0, Lhya;->d:Lhvg;

    invoke-virtual {v0}, Lhvg;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    if-nez p1, :cond_0

    .line 26
    const-string v0, "Event is unexpectedly null."

    .line 29
    :goto_0
    iget-object v1, p0, Lhya;->h:Lhxh;

    if-nez v1, :cond_1

    .line 30
    const/4 v1, -0x1

    .line 33
    :goto_1
    if-gtz v1, :cond_2

    .line 37
    :goto_2
    iget-object v0, p0, Lhya;->d:Lhvg;

    invoke-virtual {v0}, Lhvg;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38
    new-instance v0, Lhyb;

    invoke-direct {v0, p1}, Lhyb;-><init>(Lhxw;)V

    throw v0

    .line 27
    :cond_0
    invoke-virtual {p1}, Lhxw;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_1
    iget-object v1, p0, Lhya;->h:Lhxh;

    invoke-interface {v1}, Lhxh;->a()I

    move-result v1

    goto :goto_1

    .line 35
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Line "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_2

    .line 39
    :cond_3
    return-void
.end method

.method private f()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 40
    iget-object v2, p0, Lhya;->c:Lhxz;

    .line 41
    iget v3, v2, Lhxz;->b:I

    .line 43
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lhya;->l:Z

    if-eqz v2, :cond_1

    .line 95
    :goto_1
    return v0

    .line 45
    :cond_1
    if-lez v3, :cond_2

    iget v2, p0, Lhya;->m:I

    if-lt v2, v3, :cond_2

    .line 46
    new-instance v0, Lhxl;

    const-string v1, "Maximum header limit exceeded"

    invoke-direct {v0, v1}, Lhxl;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    iget v2, p0, Lhya;->m:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhya;->m:I

    .line 48
    iget-object v2, p0, Lhya;->e:Lhxy;

    invoke-interface {v2}, Lhxy;->a()V

    .line 50
    iget-boolean v2, p0, Lhya;->l:Z

    if-eqz v2, :cond_3

    .line 51
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 53
    :cond_3
    iget-object v4, p0, Lhya;->q:Lhxj;

    .line 55
    :cond_4
    :goto_2
    :try_start_0
    iget-object v2, p0, Lhya;->g:Lhyh;

    .line 56
    iget v2, v2, Lhyh;->b:I

    .line 58
    if-lez v2, :cond_5

    .line 59
    iget-object v2, p0, Lhya;->e:Lhxy;

    iget-object v5, p0, Lhya;->g:Lhyh;

    invoke-interface {v2, v5}, Lhxy;->a(Lhyh;)V

    .line 60
    :cond_5
    iget-object v2, p0, Lhya;->g:Lhyh;

    .line 61
    const/4 v5, 0x0

    iput v5, v2, Lhyh;->b:I

    .line 62
    iget-object v2, p0, Lhya;->g:Lhyh;

    invoke-virtual {v4, v2}, Lhxi;->a(Lhyh;)I

    move-result v2

    const/4 v5, -0x1

    if-ne v2, v5, :cond_8

    .line 63
    sget-object v2, Lhxw;->b:Lhxw;

    invoke-direct {p0, v2}, Lhya;->a(Lhxw;)V

    .line 64
    const/4 v2, 0x1

    iput-boolean v2, p0, Lhya;->l:Z
    :try_end_0
    .catch Lhxm; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :cond_6
    :goto_3
    :try_start_1
    iget-object v2, p0, Lhya;->e:Lhxy;

    invoke-interface {v2}, Lhxy;->b()Lhyf;

    move-result-object v2

    .line 85
    if-eqz v2, :cond_0

    .line 88
    iget v4, v2, Lhyf;->b:I

    .line 90
    iget-object v5, v2, Lhyf;->c:Ljava/lang/String;

    .line 91
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v4, v5, :cond_7

    .line 92
    sget-object v4, Lhxw;->d:Lhxw;

    invoke-direct {p0, v4}, Lhya;->a(Lhxw;)V

    .line 93
    :cond_7
    iget-object v4, p0, Lhya;->f:Lhyp;

    invoke-virtual {v4, v2}, Lhyp;->a(Lhyf;)Lhxx;

    .line 94
    iput-object v2, p0, Lhya;->n:Lhxx;
    :try_end_1
    .catch Lhvc; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    .line 95
    goto :goto_1

    .line 66
    :cond_8
    :try_start_2
    iget-object v2, p0, Lhya;->g:Lhyh;

    .line 67
    iget v2, v2, Lhyh;->b:I

    .line 69
    if-lez v2, :cond_9

    iget-object v5, p0, Lhya;->g:Lhyh;

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6}, Lhyh;->b(I)B

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_9

    .line 70
    add-int/lit8 v2, v2, -0x1

    .line 71
    :cond_9
    if-lez v2, :cond_a

    iget-object v5, p0, Lhya;->g:Lhyh;

    add-int/lit8 v6, v2, -0x1

    invoke-virtual {v5, v6}, Lhyh;->b(I)B

    move-result v5

    const/16 v6, 0xd

    if-ne v5, v6, :cond_a

    .line 72
    add-int/lit8 v2, v2, -0x1

    .line 73
    :cond_a
    if-nez v2, :cond_b

    .line 74
    const/4 v2, 0x1

    iput-boolean v2, p0, Lhya;->l:Z
    :try_end_2
    .catch Lhxm; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Lhvc;

    invoke-direct {v1, v0}, Lhvc;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 76
    :cond_b
    :try_start_3
    iget v2, p0, Lhya;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhya;->k:I

    .line 77
    iget v2, p0, Lhya;->k:I

    if-le v2, v1, :cond_4

    .line 78
    iget-object v2, p0, Lhya;->g:Lhyh;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lhyh;->b(I)B
    :try_end_3
    .catch Lhxm; {:try_start_3 .. :try_end_3} :catch_0

    move-result v2

    .line 79
    const/16 v5, 0x20

    if-eq v2, v5, :cond_4

    const/16 v5, 0x9

    if-ne v2, v5, :cond_6

    goto/16 :goto_2

    .line 97
    :catch_1
    move-exception v2

    sget-object v2, Lhxw;->c:Lhxw;

    invoke-direct {p0, v2}, Lhya;->a(Lhxw;)V

    .line 98
    iget-object v2, p0, Lhya;->c:Lhxz;

    goto/16 :goto_0
.end method

.method private final g()V
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lhya;->o:Lhxs;

    invoke-interface {v0}, Lhxs;->b()Ljava/lang/String;

    move-result-object v0

    .line 168
    :try_start_0
    new-instance v1, Lhxn;

    iget-object v2, p0, Lhya;->i:Lhxe;

    iget-object v3, p0, Lhya;->c:Lhxz;

    .line 169
    const/4 v3, 0x0

    .line 170
    invoke-direct {v1, v2, v0, v3}, Lhxn;-><init>(Lhxe;Ljava/lang/String;Z)V

    iput-object v1, p0, Lhya;->p:Lhxn;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    new-instance v0, Lhxj;

    iget-object v1, p0, Lhya;->p:Lhxn;

    iget-object v2, p0, Lhya;->c:Lhxz;

    .line 175
    iget v2, v2, Lhxz;->a:I

    .line 176
    invoke-direct {v0, v1, v2}, Lhxj;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lhya;->q:Lhxj;

    .line 177
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    new-instance v1, Lhvc;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lhvc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private final h()V
    .locals 3

    .prologue
    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lhya;->p:Lhxn;

    .line 179
    new-instance v0, Lhxj;

    iget-object v1, p0, Lhya;->i:Lhxe;

    iget-object v2, p0, Lhya;->c:Lhxz;

    .line 180
    iget v2, v2, Lhxz;->a:I

    .line 181
    invoke-direct {v0, v1, v2}, Lhxj;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lhya;->q:Lhxj;

    .line 182
    return-void
.end method

.method private final i()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lhya;->q:Lhxj;

    .line 184
    iget-boolean v0, v0, Lhxj;->b:Z

    .line 185
    if-nez v0, :cond_2

    .line 186
    iget-object v0, p0, Lhya;->r:[B

    if-nez v0, :cond_0

    .line 187
    const/16 v0, 0x800

    new-array v0, v0, [B

    iput-object v0, p0, Lhya;->r:[B

    .line 188
    :cond_0
    invoke-direct {p0}, Lhya;->j()Ljava/io/InputStream;

    move-result-object v0

    .line 189
    :cond_1
    iget-object v1, p0, Lhya;->r:[B

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 190
    :cond_2
    return-void
.end method

.method private final j()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lhya;->c:Lhxz;

    .line 205
    iget-wide v2, v0, Lhxz;->d:J

    .line 207
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    .line 208
    new-instance v0, Lhxg;

    iget-object v1, p0, Lhya;->q:Lhxj;

    invoke-direct {v0, v1, v2, v3}, Lhxg;-><init>(Ljava/io/InputStream;J)V

    .line 209
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhya;->q:Lhxj;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lhxu;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lhya;->j:Lhxu;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lhya;->a:I

    .line 22
    return-void
.end method

.method public final b()Lhxv;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    iget-object v2, p0, Lhya;->j:Lhxu;

    invoke-virtual {v2}, Lhxu;->ordinal()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 162
    :pswitch_0
    iget-object v0, p0, Lhya;->j:Lhxu;

    iget-object v1, p0, Lhya;->b:Lhxu;

    if-ne v0, v1, :cond_c

    .line 163
    sget-object v0, Lhxu;->n:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    .line 166
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 102
    :pswitch_1
    sget-object v0, Lhxu;->d:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    .line 104
    :pswitch_2
    sget-object v0, Lhxu;->d:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    .line 106
    :pswitch_3
    iget-object v0, p0, Lhya;->f:Lhyp;

    invoke-virtual {v0}, Lhyp;->a()V

    .line 107
    :pswitch_4
    invoke-direct {p0}, Lhya;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lhxu;->e:Lhxu;

    :goto_2
    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    :cond_1
    sget-object v0, Lhxu;->f:Lhxu;

    goto :goto_2

    .line 109
    :pswitch_5
    iget-object v0, p0, Lhya;->f:Lhyp;

    invoke-virtual {v0}, Lhyp;->b()Lhxs;

    move-result-object v0

    iput-object v0, p0, Lhya;->o:Lhxs;

    .line 110
    iget-object v0, p0, Lhya;->o:Lhxs;

    invoke-interface {v0}, Lhxs;->a()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget v1, p0, Lhya;->a:I

    sget v2, Lmg$c;->bd:I

    if-ne v1, v2, :cond_2

    .line 112
    sget-object v0, Lhxu;->m:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    .line 113
    :cond_2
    invoke-static {v0}, Lhyl;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    sget-object v0, Lhxu;->g:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    .line 115
    invoke-direct {p0}, Lhya;->h()V

    goto :goto_0

    .line 116
    :cond_3
    iget v1, p0, Lhya;->a:I

    sget v2, Lmg$c;->bb:I

    if-eq v1, v2, :cond_5

    invoke-static {v0}, Lhyl;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 117
    sget-object v0, Lhxu;->m:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    .line 119
    iget-object v0, p0, Lhya;->p:Lhxn;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhya;->p:Lhxn;

    .line 120
    :goto_3
    invoke-direct {p0, v0}, Lhya;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 121
    sget-object v1, Lhxu;->a:Lhxu;

    sget-object v2, Lhxu;->b:Lhxu;

    invoke-direct {p0, v1, v2, v0}, Lhya;->a(Lhxu;Lhxu;Ljava/io/InputStream;)Lhxv;

    move-result-object v0

    goto :goto_1

    .line 119
    :cond_4
    iget-object v0, p0, Lhya;->i:Lhxe;

    goto :goto_3

    .line 123
    :cond_5
    sget-object v0, Lhxu;->m:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    .line 125
    :pswitch_6
    iget-object v2, p0, Lhya;->q:Lhxj;

    .line 126
    iget-boolean v2, v2, Lhxj;->a:Z

    .line 127
    if-eqz v2, :cond_6

    .line 128
    invoke-direct {p0}, Lhya;->i()V

    .line 129
    sget-object v0, Lhxu;->h:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto :goto_0

    .line 131
    :cond_6
    invoke-direct {p0}, Lhya;->g()V

    .line 132
    sget-object v2, Lhxu;->i:Lhxu;

    iput-object v2, p0, Lhya;->j:Lhxu;

    .line 133
    iget-object v2, p0, Lhya;->p:Lhxn;

    .line 134
    iget v2, v2, Lhxn;->e:I

    if-nez v2, :cond_8

    move v2, v0

    .line 136
    :goto_4
    if-eqz v2, :cond_0

    .line 137
    :pswitch_7
    invoke-direct {p0}, Lhya;->i()V

    .line 138
    iget-object v2, p0, Lhya;->p:Lhxn;

    .line 139
    iget-boolean v3, v2, Lhxn;->a:Z

    if-eqz v3, :cond_9

    iget-object v2, v2, Lhxn;->d:Lhxe;

    invoke-virtual {v2}, Lhxe;->c()Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v0

    .line 140
    :goto_5
    if-eqz v2, :cond_a

    iget-object v2, p0, Lhya;->p:Lhxn;

    .line 141
    iget-boolean v2, v2, Lhxn;->b:Z

    .line 142
    if-nez v2, :cond_a

    .line 143
    sget-object v2, Lhxw;->a:Lhxw;

    invoke-direct {p0, v2}, Lhya;->a(Lhxw;)V

    .line 152
    :cond_7
    iget-object v2, p0, Lhya;->p:Lhxn;

    .line 153
    iget-boolean v3, v2, Lhxn;->c:Z

    if-eqz v3, :cond_b

    iget-object v2, v2, Lhxn;->d:Lhxe;

    invoke-virtual {v2}, Lhxe;->c()Z

    move-result v2

    if-nez v2, :cond_b

    .line 155
    :goto_6
    invoke-direct {p0}, Lhya;->h()V

    .line 156
    sget-object v1, Lhxu;->j:Lhxu;

    iput-object v1, p0, Lhya;->j:Lhxu;

    .line 157
    if-eqz v0, :cond_0

    .line 158
    :pswitch_8
    sget-object v0, Lhxu;->h:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 134
    goto :goto_4

    :cond_9
    move v2, v1

    .line 139
    goto :goto_5

    .line 144
    :cond_a
    iget-object v2, p0, Lhya;->p:Lhxn;

    .line 145
    iget-boolean v2, v2, Lhxn;->b:Z

    .line 146
    if-nez v2, :cond_7

    .line 147
    invoke-direct {p0}, Lhya;->h()V

    .line 148
    invoke-direct {p0}, Lhya;->g()V

    .line 150
    sget-object v0, Lhxu;->k:Lhxu;

    sget-object v1, Lhxu;->l:Lhxu;

    iget-object v2, p0, Lhya;->p:Lhxn;

    invoke-direct {p0, v0, v1, v2}, Lhya;->a(Lhxu;Lhxu;Ljava/io/InputStream;)Lhxv;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    move v0, v1

    .line 153
    goto :goto_6

    .line 160
    :pswitch_9
    iget-object v0, p0, Lhya;->b:Lhxu;

    iput-object v0, p0, Lhya;->j:Lhxu;

    goto/16 :goto_0

    .line 165
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhya;->j:Lhxu;

    invoke-static {v2}, Lhya;->a(Lhxu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_7
        :pswitch_8
        :pswitch_2
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public final c()Lhxs;
    .locals 3

    .prologue
    .line 210
    .line 211
    iget-object v0, p0, Lhya;->j:Lhxu;

    .line 212
    invoke-virtual {v0}, Lhxu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 214
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhya;->j:Lhxu;

    invoke-static {v2}, Lhya;->a(Lhxu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :pswitch_1
    iget-object v0, p0, Lhya;->o:Lhxs;

    return-object v0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final d()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lhya;->j:Lhxu;

    invoke-virtual {v0}, Lhxu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 222
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhya;->j:Lhxu;

    invoke-static {v2}, Lhya;->a(Lhxu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :pswitch_1
    invoke-direct {p0}, Lhya;->j()Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Lhxx;
    .locals 3

    .prologue
    .line 215
    .line 216
    iget-object v0, p0, Lhya;->j:Lhxu;

    .line 217
    invoke-virtual {v0}, Lhxu;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 219
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhya;->j:Lhxu;

    invoke-static {v2}, Lhya;->a(Lhxu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :pswitch_0
    iget-object v0, p0, Lhya;->n:Lhxx;

    return-object v0

    .line 217
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhya;->j:Lhxu;

    invoke-static {v1}, Lhya;->a(Lhxu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhya;->o:Lhxs;

    invoke-interface {v1}, Lhxs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhya;->o:Lhxs;

    invoke-interface {v1}, Lhxs;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
