.class public final Lgot;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgot;


# instance fields
.field public logData:Lgpn;

.field public requestHeader:Lgls;

.field public sessionId:Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgot;->clear()Lgot;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgot;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgot;->_emptyArray:[Lgot;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgot;->_emptyArray:[Lgot;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgot;

    sput-object v0, Lgot;->_emptyArray:[Lgot;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgot;->_emptyArray:[Lgot;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgot;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lgot;

    invoke-direct {v0}, Lgot;-><init>()V

    invoke-virtual {v0, p0}, Lgot;->mergeFrom(Lhfp;)Lgot;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgot;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lgot;

    invoke-direct {v0}, Lgot;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgot;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgot;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgot;->requestHeader:Lgls;

    .line 11
    iput-object v0, p0, Lgot;->sessionId:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lgot;->logData:Lgpn;

    .line 13
    iput-object v0, p0, Lgot;->syncMetadata:Lgoa;

    .line 14
    iput-object v0, p0, Lgot;->unknownFieldData:Lhfv;

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lgot;->cachedSize:I

    .line 16
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 28
    iget-object v1, p0, Lgot;->requestHeader:Lgls;

    if-eqz v1, :cond_0

    .line 29
    const/4 v1, 0x1

    iget-object v2, p0, Lgot;->requestHeader:Lgls;

    .line 30
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31
    :cond_0
    iget-object v1, p0, Lgot;->sessionId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 32
    const/4 v1, 0x2

    iget-object v2, p0, Lgot;->sessionId:Ljava/lang/String;

    .line 33
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34
    :cond_1
    iget-object v1, p0, Lgot;->logData:Lgpn;

    if-eqz v1, :cond_2

    .line 35
    const/4 v1, 0x3

    iget-object v2, p0, Lgot;->logData:Lgpn;

    .line 36
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 37
    :cond_2
    iget-object v1, p0, Lgot;->syncMetadata:Lgoa;

    if-eqz v1, :cond_3

    .line 38
    const/4 v1, 0x4

    iget-object v2, p0, Lgot;->syncMetadata:Lgoa;

    .line 39
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40
    :cond_3
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgot;
    .locals 1

    .prologue
    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :sswitch_0
    return-object p0

    .line 46
    :sswitch_1
    iget-object v0, p0, Lgot;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgot;->requestHeader:Lgls;

    .line 48
    :cond_1
    iget-object v0, p0, Lgot;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 50
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgot;->sessionId:Ljava/lang/String;

    goto :goto_0

    .line 52
    :sswitch_3
    iget-object v0, p0, Lgot;->logData:Lgpn;

    if-nez v0, :cond_2

    .line 53
    new-instance v0, Lgpn;

    invoke-direct {v0}, Lgpn;-><init>()V

    iput-object v0, p0, Lgot;->logData:Lgpn;

    .line 54
    :cond_2
    iget-object v0, p0, Lgot;->logData:Lgpn;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 56
    :sswitch_4
    iget-object v0, p0, Lgot;->syncMetadata:Lgoa;

    if-nez v0, :cond_3

    .line 57
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgot;->syncMetadata:Lgoa;

    .line 58
    :cond_3
    iget-object v0, p0, Lgot;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lgot;->mergeFrom(Lhfp;)Lgot;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lgot;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x1

    iget-object v1, p0, Lgot;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 19
    :cond_0
    iget-object v0, p0, Lgot;->sessionId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 20
    const/4 v0, 0x2

    iget-object v1, p0, Lgot;->sessionId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 21
    :cond_1
    iget-object v0, p0, Lgot;->logData:Lgpn;

    if-eqz v0, :cond_2

    .line 22
    const/4 v0, 0x3

    iget-object v1, p0, Lgot;->logData:Lgpn;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_2
    iget-object v0, p0, Lgot;->syncMetadata:Lgoa;

    if-eqz v0, :cond_3

    .line 24
    const/4 v0, 0x4

    iget-object v1, p0, Lgot;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 26
    return-void
.end method
