.class final Laki;
.super Lajw;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lajw;-><init>()V

    .line 3
    return-void
.end method


# virtual methods
.method protected final a(Landroid/util/AttributeSet;Ljava/lang/String;)Lajg;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13
    const-string v0, "assistant"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-static {v1}, Lajm;->e(I)Lajg;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 15
    :cond_0
    const-string v0, "brother"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    const/4 v0, 0x2

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 17
    :cond_1
    const-string v0, "child"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18
    const/4 v0, 0x3

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 19
    :cond_2
    const-string v0, "domestic_partner"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20
    const/4 v0, 0x4

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 21
    :cond_3
    const-string v0, "father"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 22
    const/4 v0, 0x5

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 23
    :cond_4
    const-string v0, "friend"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 24
    const/4 v0, 0x6

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 25
    :cond_5
    const-string v0, "manager"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 26
    const/4 v0, 0x7

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 27
    :cond_6
    const-string v0, "mother"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 28
    const/16 v0, 0x8

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_7
    const-string v0, "parent"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 30
    const/16 v0, 0x9

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_8
    const-string v0, "partner"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 32
    const/16 v0, 0xa

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 33
    :cond_9
    const-string v0, "referred_by"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 34
    const/16 v0, 0xb

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 35
    :cond_a
    const-string v0, "relative"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 36
    const/16 v0, 0xc

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 37
    :cond_b
    const-string v0, "sister"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 38
    const/16 v0, 0xd

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 39
    :cond_c
    const-string v0, "spouse"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 40
    const/16 v0, 0xe

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    goto/16 :goto_0

    .line 41
    :cond_d
    const-string v0, "custom"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 42
    const/4 v0, 0x0

    invoke-static {v0}, Lajm;->e(I)Lajg;

    move-result-object v0

    .line 44
    iput-boolean v1, v0, Lajg;->b:Z

    .line 46
    const-string v1, "data3"

    .line 48
    iput-object v1, v0, Lajg;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 51
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    const-string v0, "relationship"

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Ljava/util/List;
    .locals 10

    .prologue
    const v6, 0x7f11028f

    .line 5
    const/4 v3, 0x0

    const-string v4, "vnd.android.cursor.item/relation"

    const-string v5, "data2"

    const/16 v7, 0x3e7

    new-instance v8, Lakh;

    invoke-direct {v8}, Lakh;-><init>()V

    new-instance v9, Lakj;

    const-string v0, "data1"

    invoke-direct {v9, v0}, Lakj;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    .line 6
    invoke-virtual/range {v0 .. v9}, Laki;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;ZLjava/lang/String;Ljava/lang/String;IILaji;Laji;)Lakt;

    move-result-object v0

    .line 7
    iget-object v1, v0, Lakt;->l:Ljava/util/List;

    new-instance v2, Lajf;

    const-string v3, "data1"

    const/16 v4, 0x2061

    invoke-direct {v2, v3, v6, v4}, Lajf;-><init>(Ljava/lang/String;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iput-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    .line 9
    iget-object v1, v0, Lakt;->m:Landroid/content/ContentValues;

    const-string v2, "data2"

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 10
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 11
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    return-object v1
.end method
