.class public final Ldny;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static A:Lezn;

.field public static B:Lezn;

.field public static C:Lezn;

.field public static D:Lezn;

.field public static E:Lezn;

.field public static F:Lezn;

.field public static G:Lezn;

.field private static H:[Ljava/lang/String;

.field private static I:Lflb;

.field private static J:Lezn;

.field public static a:Z

.field public static final b:Lflb;

.field public static c:Lezn;

.field public static d:Lezn;

.field public static e:Lezn;

.field public static f:Lezn;

.field public static g:Lezn;

.field public static h:Lezn;

.field public static i:Lezn;

.field public static j:Lezn;

.field public static k:Lezn;

.field public static l:Lezn;

.field public static m:Lezn;

.field public static n:Lezn;

.field public static o:Lezn;

.field public static p:Lezn;

.field public static q:Lezn;

.field public static r:Lezn;

.field public static s:Lezn;

.field public static t:Lezn;

.field public static u:Lezn;

.field public static v:Lezn;

.field public static w:Lezn;

.field public static x:Lezn;

.field public static y:Lezn;

.field public static z:Lezn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 51
    sput-boolean v6, Ldny;->a:Z

    .line 52
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ANDROID_DIALER"

    aput-object v1, v0, v6

    const-string v1, "SCOOBY_EVENTS"

    aput-object v1, v0, v7

    const-string v1, "SCOOBY_SPAM_REPORT_LOG"

    aput-object v1, v0, v3

    sput-object v0, Ldny;->H:[Ljava/lang/String;

    .line 53
    new-instance v0, Lflb;

    const-string v1, "dialer_phenotype_flags"

    invoke-direct {v0, v1}, Lflb;-><init>(Ljava/lang/String;)V

    const-string v1, "Dialer_"

    .line 54
    invoke-virtual {v0, v1}, Lflb;->a(Ljava/lang/String;)Lflb;

    move-result-object v0

    const-string v1, "G__"

    .line 55
    invoke-virtual {v0, v1}, Lflb;->b(Ljava/lang/String;)Lflb;

    move-result-object v0

    sput-object v0, Ldny;->b:Lflb;

    .line 56
    new-instance v0, Lflb;

    const-string v1, "dialer_phenotype_flags"

    invoke-direct {v0, v1}, Lflb;-><init>(Ljava/lang/String;)V

    const-string v1, "Dialer_"

    .line 57
    invoke-virtual {v0, v1}, Lflb;->a(Ljava/lang/String;)Lflb;

    move-result-object v0

    const-string v1, "Scooby__"

    .line 58
    invoke-virtual {v0, v1}, Lflb;->b(Ljava/lang/String;)Lflb;

    move-result-object v0

    sput-object v0, Ldny;->I:Lflb;

    .line 59
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_hangouts_integration"

    .line 60
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->c:Lezn;

    .line 61
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_voicemail_archive"

    .line 62
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    .line 63
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_voicemail_share"

    .line 64
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    .line 65
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "spam_report_checked_by_default"

    .line 66
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->d:Lezn;

    .line 67
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_spam_auto_reject"

    .line 68
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->e:Lezn;

    .line 69
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_after_call_notification"

    .line 70
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->f:Lezn;

    .line 71
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "percentage_of_spam_notifications_to_show"

    const/16 v2, 0x64

    .line 72
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->g:Lezn;

    .line 73
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "percentage_of_non_spam_notifications_to_show"

    const/16 v2, 0xa

    .line 74
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->h:Lezn;

    .line 75
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_dialogs_for_after_call_notification"

    .line 76
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->i:Lezn;

    .line 77
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "hangouts_package_name"

    const-string v2, "com.google.android.talk"

    .line 78
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->j:Lezn;

    .line 79
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_silent_crash_reporting"

    .line 80
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->k:Lezn;

    .line 81
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_primes"

    .line 82
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->l:Lezn;

    .line 83
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_primes_memory_metric"

    .line 84
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->m:Lezn;

    .line 85
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_primes_timer_metric"

    .line 86
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->n:Lezn;

    .line 87
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_primes_crash_metric"

    .line 88
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->o:Lezn;

    .line 89
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_primes_network_metric"

    .line 90
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->p:Lezn;

    .line 91
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "reverse_lookup_discovery_protected_photo_url"

    const-string v2, "https://plus.google.com/_/focus/photos/private"

    .line 92
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->q:Lezn;

    .line 93
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "reverse_lookup_additional_query_params"

    const-string v2, "includePeople=1&includeGal=1"

    .line 94
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->r:Lezn;

    .line 95
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_nearby_places_directory"

    .line 96
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->s:Lezn;

    .line 97
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "nearby_places_directory_radius_meters"

    const/16 v2, 0x3e8

    .line 98
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->t:Lezn;

    .line 99
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "nearby_places_min_query_len"

    .line 100
    invoke-virtual {v0, v1, v3}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->u:Lezn;

    .line 101
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "nearby_places_max_query_len"

    const/16 v2, 0x32

    .line 102
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->v:Lezn;

    .line 103
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "nearby_places_api_scope"

    const-string v2, "oauth2: https://www.googleapis.com/auth/android_nearbyplaces"

    .line 104
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->w:Lezn;

    .line 105
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "nearby_places_enable_personalization"

    .line 106
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->x:Lezn;

    .line 107
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "debug_display_nearby_place_distance"

    .line 108
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->y:Lezn;

    .line 109
    sget-object v0, Ldny;->b:Lflb;

    const-string v1, "enable_nearby_places_export"

    .line 110
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->z:Lezn;

    .line 111
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "spam_server_host"

    const-string v2, "telephonyspamprotect-pa.googleapis.com"

    .line 112
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;Ljava/lang/String;)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->J:Lezn;

    .line 113
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "spam_server_port"

    const/16 v2, 0x1bb

    .line 114
    invoke-virtual {v0, v1, v2}, Lflb;->a(Ljava/lang/String;I)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->A:Lezn;

    .line 115
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "spam_interval_for_wifi_job_milliseconds"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 116
    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 117
    invoke-virtual {v0, v1, v2, v3}, Lflb;->a(Ljava/lang/String;J)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->B:Lezn;

    .line 118
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "spam_interval_for_any_network_job_milliseconds"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 119
    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 120
    invoke-virtual {v0, v1, v2, v3}, Lflb;->a(Ljava/lang/String;J)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->C:Lezn;

    .line 121
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "are_spam_jobs_enabled"

    .line 122
    invoke-virtual {v0, v1, v7}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->D:Lezn;

    .line 123
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "enable_droidguard_to_fetch_spam_list"

    .line 124
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->E:Lezn;

    .line 125
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "enable_same_prefix_logging"

    .line 126
    invoke-virtual {v0, v1, v6}, Lflb;->a(Ljava/lang/String;Z)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->F:Lezn;

    .line 127
    sget-object v0, Ldny;->I:Lflb;

    const-string v1, "scooby_experiment_id"

    const-wide/16 v2, 0x0

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Lflb;->a(Ljava/lang/String;J)Lezn;

    move-result-object v0

    sput-object v0, Ldny;->G:Lezn;

    .line 129
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1
    .line 2
    sget-object v1, Lezn;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v0, v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->isDeviceProtectedStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    sput-object p0, Lezn;->b:Landroid/content/Context;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    sput-boolean v0, Lezn;->c:Z

    .line 3
    const/4 v0, 0x1

    sput-boolean v0, Ldny;->a:Z

    .line 4
    return-void

    .line 2
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Lbef;Lbeb;)V
    .locals 2

    .prologue
    .line 32
    invoke-static {p0}, Lbw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :goto_0
    return-void

    .line 34
    :cond_0
    new-instance v0, Ldnx;

    invoke-direct {v0, p0}, Ldnx;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {p1, v0}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v0

    .line 36
    invoke-interface {v0, p2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 38
    invoke-interface {v0, v1}, Lbdy;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static final synthetic a(Lfat;)V
    .locals 4

    .prologue
    .line 50
    const-string v0, "Flags.register"

    invoke-virtual {p0}, Lfat;->b()Z

    move-result v1

    const/16 v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "phenotype register status: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static final synthetic a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 49
    const-string v0, "Flags.register"

    const-string v1, "commit succeeded: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 5
    invoke-static {p0}, Ldny;->d(Landroid/content/Context;)I

    move-result v2

    .line 6
    invoke-static {p0}, Lezk;->a(Landroid/content/Context;)Lezl;

    move-result-object v3

    .line 7
    const-string v4, "com.google.android.dialer"

    sget-object v5, Ldny;->H:[Ljava/lang/String;

    .line 8
    sget-object v0, Ldil;->c:Ldil;

    invoke-virtual {v0}, Ldil;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 9
    invoke-static {}, Lapw;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 15
    invoke-static {}, Lbdf;->a()V

    .line 16
    sget-object v1, Ldil$a;->a:Ldil$a;

    .line 17
    :goto_0
    invoke-virtual {v0, v1}, Lhbr$a;->a(Ldil$a;)Lhbr$a;

    move-result-object v0

    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Ldil;

    .line 18
    invoke-virtual {v0}, Ldil;->toByteArray()[B

    move-result-object v0

    .line 20
    new-instance v1, Lfam;

    invoke-direct {v1, v4, v2, v5, v0}, Lfam;-><init>(Ljava/lang/String;I[Ljava/lang/String;[B)V

    .line 22
    new-instance v0, Lfau;

    invoke-direct {v0}, Lfau;-><init>()V

    iget-object v2, v3, Ledh;->h:Lefj;

    const/4 v4, 0x0

    iget-object v5, v3, Ledh;->g:Legm;

    .line 23
    new-instance v6, Legx;

    invoke-direct {v6, v4, v1, v0, v5}, Legx;-><init>(ILegq;Lfau;Legm;)V

    iget-object v1, v2, Lefj;->k:Landroid/os/Handler;

    iget-object v4, v2, Lefj;->k:Landroid/os/Handler;

    const/4 v5, 0x4

    new-instance v7, Legf;

    iget-object v2, v2, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-direct {v7, v6, v2, v3}, Legf;-><init>(Ledv;ILedh;)V

    invoke-virtual {v4, v5, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 25
    iget-object v0, v0, Lfau;->a:Lfbj;

    .line 27
    sget-object v1, Ldnz;->a:Lfap;

    invoke-virtual {v0, v1}, Lfat;->a(Lfap;)Lfat;

    .line 29
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v0

    invoke-virtual {v0}, Lbed;->a()Lbef;

    move-result-object v0

    sget-object v1, Ldoa;->a:Lbeb;

    .line 30
    invoke-static {p0, v0, v1}, Ldny;->a(Landroid/content/Context;Lbef;Lbeb;)V

    .line 31
    return-void

    .line 10
    :pswitch_0
    sget-object v1, Ldil$a;->b:Ldil$a;

    goto :goto_0

    .line 11
    :pswitch_1
    sget-object v1, Ldil$a;->c:Ldil$a;

    goto :goto_0

    .line 12
    :pswitch_2
    sget-object v1, Ldil$a;->d:Ldil$a;

    goto :goto_0

    .line 13
    :pswitch_3
    sget-object v1, Ldil$a;->e:Ldil$a;

    goto :goto_0

    .line 14
    :pswitch_4
    sget-object v1, Ldil$a;->f:Ldil$a;

    goto :goto_0

    .line 9
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 45
    sget-object v0, Ldny;->J:Lezn;

    invoke-virtual {v0}, Lezn;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    invoke-static {v1}, Ldrs;->a(Landroid/content/ContentResolver;)Ldrs;

    move-result-object v1

    .line 47
    invoke-virtual {v1, v0}, Ldrs;->a(Ljava/lang/String;)Ldrt;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldrt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method private static d(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 40
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "Flags.getAppVersion"

    const-string v2, "could not find own package"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    const/4 v0, -0x1

    goto :goto_0
.end method
