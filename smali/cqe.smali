.class public final Lcqe;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# static fields
.field public static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcqe;->a:Ljava/util/Set;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 4

    .prologue
    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 31
    invoke-static {p0, v0}, Lcqe;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :cond_1
    return-object v1
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    invoke-static {p0, p1}, Lcov;->b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 7
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 8
    new-instance v0, Lcly;

    invoke-direct {v0, p0, p1}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 10
    invoke-virtual {v0}, Lcly;->a()Lbdh;

    move-result-object v0

    const-string v1, "u"

    .line 11
    invoke-virtual {v0, v1, v2}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    const-string v1, "pw"

    .line 12
    invoke-virtual {v0, v1, v2}, Lbdh;->a(Ljava/lang/String;Ljava/lang/String;)Lbdh;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lbdh;->a()V

    .line 14
    new-instance v0, Lcqg;

    invoke-direct {v0, p1}, Lcqg;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 15
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcpx;)V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lcly;

    invoke-direct {v0, p0, p1}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 2
    invoke-virtual {v0}, Lcly;->a()Lbdh;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcpx;->a(Lbdh;)Lbdh;

    move-result-object v0

    invoke-virtual {v0}, Lbdh;->a()V

    .line 3
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcqe;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V

    .line 4
    new-instance v0, Lcqf;

    invoke-direct {v0, p1}, Lcqf;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    invoke-static {v0}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 5
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Z)V
    .locals 2

    .prologue
    .line 41
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-static {p0, p1}, Lcqe;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lbdg;->a()Lbdh;

    move-result-object v0

    const-string v1, "is_account_activated"

    .line 44
    invoke-virtual {v0, v1, p2}, Lbdh;->a(Ljava/lang/String;Z)Lbdh;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lbdh;->a()V

    .line 46
    return-void
.end method

.method public static a(Lclo;)V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lbdf;->b()V

    .line 36
    sget-object v0, Lcqe;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public static b(Lclo;)V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lbdf;->b()V

    .line 39
    sget-object v0, Lcqe;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 16
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    invoke-static {p0, p1}, Lcqe;->c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;

    move-result-object v1

    .line 20
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isUserUnlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    const-string v0, "is_account_activated"

    invoke-virtual {v1, v0}, Lbdg;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcly;

    invoke-direct {v0, p0, p1}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 24
    invoke-virtual {v1}, Lbdg;->a()Lbdh;

    move-result-object v2

    const-string v3, "is_account_activated"

    const-string v4, "is_account_activated"

    .line 25
    invoke-virtual {v0, v4, v5}, Lbdg;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 26
    invoke-virtual {v2, v3, v0}, Lbdh;->a(Ljava/lang/String;Z)Lbdh;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lbdh;->a()V

    .line 28
    :cond_0
    const-string v0, "is_account_activated"

    invoke-virtual {v1, v0, v5}, Lbdg;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lbdg;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lbdg;

    .line 48
    invoke-static {p0}, Lbsi;->a(Landroid/content/Context;)Lbsi;

    move-result-object v1

    invoke-virtual {v1}, Lbsi;->a()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lbdg;-><init>(Landroid/telecom/PhoneAccountHandle;Landroid/content/SharedPreferences;)V

    .line 49
    return-object v0
.end method
