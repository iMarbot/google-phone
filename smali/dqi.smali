.class final Ldqi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhow;


# instance fields
.field private synthetic a:Ldqm;

.field private synthetic b:Ldqb;


# direct methods
.method constructor <init>(Ldqb;Ldqm;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldqi;->b:Ldqb;

    iput-object p2, p0, Ldqi;->a:Ldqm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 10
    const-string v0, "SpamAsyncTaskUtil.onCompleted"

    const-string v1, "gRPC blocked list fetching completed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 12
    check-cast p1, Lgwc;

    .line 13
    const-string v0, "SpamAsyncTaskUtil.onNext"

    const-string v1, "gRPC data received from server"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-wide v2, p1, Lgwc;->a:J

    .line 15
    iget-wide v5, p1, Lgwc;->b:J

    .line 16
    iget-object v4, p1, Lgwc;->d:[Ljava/lang/String;

    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    array-length v8, v4

    move v0, v7

    :goto_0
    if-ge v0, v8, :cond_0

    aget-object v9, v4, v0

    .line 19
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22
    :cond_0
    const-string v0, "SpamAsyncTaskUtil.onNext"

    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x4a

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "updated spam list for countryIso: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " blacklist version: "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v8, v7, [Ljava/lang/Object;

    .line 24
    invoke-static {v0, v1, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    iget-object v8, p1, Lgwc;->c:Lgwd;

    .line 26
    if-eqz v8, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_3

    .line 27
    :cond_1
    const-string v0, "SpamAsyncTaskUtil.onNext"

    const-string v1, "spam list from server was empty"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 29
    iget-object v0, v0, Ldqb;->c:Landroid/content/Context;

    .line 30
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->e:Ldnt$a;

    .line 31
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    .line 32
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 33
    iget-object v0, p0, Ldqi;->a:Ldqm;

    if-eqz v0, :cond_2

    .line 34
    iget-object v0, p0, Ldqi;->a:Ldqm;

    invoke-virtual {v0, v10}, Ldqm;->a(Z)V

    .line 79
    :cond_2
    :goto_1
    return-void

    .line 36
    :cond_3
    invoke-virtual {v8}, Lgwd;->a()Lgvz;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 38
    iget-object v0, v0, Ldqb;->b:Ldqu;

    .line 40
    invoke-virtual {v8}, Lgwd;->a()Lgvz;

    move-result-object v1

    .line 42
    invoke-static {}, Lbdf;->c()V

    .line 43
    iget-object v0, v0, Ldqu;->a:Ldqt;

    invoke-virtual/range {v0 .. v6}, Ldqt;->a(Lgvz;J[Ljava/lang/String;J)Z

    move-result v0

    .line 44
    if-eqz v0, :cond_4

    .line 45
    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 46
    iget-object v0, v0, Ldqb;->c:Landroid/content/Context;

    .line 47
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->c:Ldnt$a;

    .line 48
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 49
    const-string v0, "SpamAsyncTaskUtil.onNext"

    const/16 v1, 0x4f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "updated spam list with complete list to blacklist version: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Ldqi;->a:Ldqm;

    if-eqz v0, :cond_2

    .line 51
    iget-object v0, p0, Ldqi;->a:Ldqm;

    invoke-virtual {v0, v10}, Ldqm;->a(Z)V

    goto :goto_1

    .line 53
    :cond_4
    invoke-virtual {v8}, Lgwd;->b()Lgwa;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 55
    iget-object v0, v0, Ldqb;->b:Ldqu;

    .line 57
    invoke-virtual {v8}, Lgwd;->b()Lgwa;

    move-result-object v1

    .line 59
    invoke-static {}, Lbdf;->c()V

    .line 60
    iget-object v0, v0, Ldqu;->a:Ldqt;

    invoke-virtual/range {v0 .. v6}, Ldqt;->a(Lgwa;J[Ljava/lang/String;J)Z

    move-result v0

    .line 61
    if-eqz v0, :cond_5

    .line 62
    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 63
    iget-object v0, v0, Ldqb;->c:Landroid/content/Context;

    .line 64
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->d:Ldnt$a;

    .line 65
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    .line 66
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 67
    const-string v0, "SpamAsyncTaskUtil.onNext"

    const/16 v1, 0x51

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "updated spam list with difference list to blacklist version: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Ldqi;->a:Ldqm;

    if-eqz v0, :cond_2

    .line 69
    iget-object v0, p0, Ldqi;->a:Ldqm;

    invoke-virtual {v0, v10}, Ldqm;->a(Z)V

    goto/16 :goto_1

    .line 71
    :cond_5
    const-string v0, "SpamAsyncTaskUtil.onNext"

    const/16 v1, 0x45

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "could not update spam list to blacklist version: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 73
    iget-object v0, v0, Ldqb;->c:Landroid/content/Context;

    .line 74
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->b:Ldnt$a;

    .line 75
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    .line 76
    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 77
    iget-object v0, p0, Ldqi;->a:Ldqm;

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Ldqi;->a:Ldqm;

    invoke-virtual {v0, v7}, Ldqm;->a(Z)V

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2
    const-string v0, "SpamAsyncTaskUtil.onError"

    const-string v1, "error while fetching list updates via gRPC"

    invoke-static {v0, v1, p1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3
    iget-object v0, p0, Ldqi;->b:Ldqb;

    .line 4
    iget-object v0, v0, Ldqb;->c:Landroid/content/Context;

    .line 5
    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Ldnt$a;->a:Ldnt$a;

    .line 6
    invoke-virtual {v1}, Ldnt$a;->getNumber()I

    move-result v1

    invoke-interface {v0, v1}, Lbku;->a(I)V

    .line 7
    iget-object v0, p0, Ldqi;->a:Ldqm;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Ldqi;->a:Ldqm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldqm;->a(Z)V

    .line 9
    :cond_0
    return-void
.end method
