.class public final Lcwd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcwo;
.implements Lcws;
.implements Lcyn;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Lcym;

.field public final c:Lcwg;

.field public final d:Ljava/util/Map;

.field public final e:Lcwe;

.field private f:Lcwq;

.field private g:Lcxc;

.field private h:Lcvr;

.field private i:Ljava/lang/ref/ReferenceQueue;


# direct methods
.method public constructor <init>(Lcym;Lcyc;Lcyu;Lcyu;Lcyu;Lcyu;)V
    .locals 13

    .prologue
    .line 1
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v12}, Lcwd;-><init>(Lcym;Lcyc;Lcyu;Lcyu;Lcyu;Lcyu;Ljava/util/Map;Lcwq;Ljava/util/Map;Lcwg;Lcwe;Lcxc;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Lcym;Lcyc;Lcyu;Lcyu;Lcyu;Lcyu;Ljava/util/Map;Lcwq;Ljava/util/Map;Lcwg;Lcwe;Lcxc;)V
    .locals 6

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcwd;->b:Lcym;

    .line 5
    new-instance v0, Lcvr;

    invoke-direct {v0, p2}, Lcvr;-><init>(Lcyc;)V

    iput-object v0, p0, Lcwd;->h:Lcvr;

    .line 6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    iput-object v0, p0, Lcwd;->d:Ljava/util/Map;

    .line 8
    new-instance v0, Lcwq;

    invoke-direct {v0}, Lcwq;-><init>()V

    .line 9
    iput-object v0, p0, Lcwd;->f:Lcwq;

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 11
    iput-object v0, p0, Lcwd;->a:Ljava/util/Map;

    .line 12
    new-instance v0, Lcwg;

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcwg;-><init>(Lcyu;Lcyu;Lcyu;Lcyu;Lcwo;)V

    .line 13
    iput-object v0, p0, Lcwd;->c:Lcwg;

    .line 14
    new-instance v0, Lcwe;

    iget-object v1, p0, Lcwd;->h:Lcvr;

    invoke-direct {v0, v1}, Lcwe;-><init>(Lcvr;)V

    .line 15
    iput-object v0, p0, Lcwd;->e:Lcwe;

    .line 16
    new-instance v0, Lcxc;

    invoke-direct {v0}, Lcxc;-><init>()V

    .line 17
    iput-object v0, p0, Lcwd;->g:Lcxc;

    .line 18
    invoke-interface {p1, p0}, Lcym;->a(Lcyn;)V

    .line 19
    return-void
.end method

.method public static a(Ljava/lang/String;JLcud;)V
    .locals 5

    .prologue
    .line 20
    invoke-static {p1, p2}, Ldhs;->a(J)D

    move-result-wide v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms, key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/ref/ReferenceQueue;
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcwd;->i:Ljava/lang/ref/ReferenceQueue;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lcwd;->i:Ljava/lang/ref/ReferenceQueue;

    .line 51
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    .line 52
    new-instance v1, Lcwj;

    iget-object v2, p0, Lcwd;->d:Ljava/util/Map;

    iget-object v3, p0, Lcwd;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, v2, v3}, Lcwj;-><init>(Ljava/util/Map;Ljava/lang/ref/ReferenceQueue;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcwd;->i:Ljava/lang/ref/ReferenceQueue;

    return-object v0
.end method

.method public final a(Lcud;Lcwr;)V
    .locals 3

    .prologue
    .line 22
    invoke-static {}, Ldhw;->a()V

    .line 23
    if-eqz p2, :cond_0

    .line 25
    iput-object p1, p2, Lcwr;->c:Lcud;

    .line 26
    iput-object p0, p2, Lcwr;->b:Lcws;

    .line 28
    iget-boolean v0, p2, Lcwr;->a:Z

    .line 29
    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcwd;->d:Ljava/util/Map;

    new-instance v1, Lcwk;

    invoke-virtual {p0}, Lcwd;->a()Ljava/lang/ref/ReferenceQueue;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Lcwk;-><init>(Lcud;Lcwr;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    iget-object v0, p0, Lcwd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method

.method public final a(Lcwl;Lcud;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Ldhw;->a()V

    .line 34
    iget-object v0, p0, Lcwd;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwl;

    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcwd;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    return-void
.end method

.method public final a(Lcwz;)V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Ldhw;->a()V

    .line 39
    iget-object v0, p0, Lcwd;->g:Lcxc;

    invoke-virtual {v0, p1}, Lcxc;->a(Lcwz;)V

    .line 40
    return-void
.end method

.method public final b(Lcud;Lcwr;)V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Ldhw;->a()V

    .line 42
    iget-object v0, p0, Lcwd;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-boolean v0, p2, Lcwr;->a:Z

    .line 45
    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcwd;->b:Lcym;

    invoke-interface {v0, p1, p2}, Lcym;->a(Lcud;Lcwz;)Lcwz;

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcwd;->g:Lcxc;

    invoke-virtual {v0, p2}, Lcxc;->a(Lcwz;)V

    goto :goto_0
.end method
