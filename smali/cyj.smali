.class public final Lcyj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcyb;


# instance fields
.field private a:Lcyr;

.field private b:Ljava/io/File;

.field private c:J

.field private d:Lcyf;

.field private e:Lcti;


# direct methods
.method protected constructor <init>(Ljava/io/File;J)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcyf;

    invoke-direct {v0}, Lcyf;-><init>()V

    iput-object v0, p0, Lcyj;->d:Lcyf;

    .line 3
    iput-object p1, p0, Lcyj;->b:Ljava/io/File;

    .line 4
    iput-wide p2, p0, Lcyj;->c:J

    .line 5
    new-instance v0, Lcyr;

    invoke-direct {v0}, Lcyr;-><init>()V

    iput-object v0, p0, Lcyj;->a:Lcyr;

    .line 6
    return-void
.end method

.method private final declared-synchronized a()Lcti;
    .locals 6

    .prologue
    .line 7
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcyj;->e:Lcti;

    if-nez v0, :cond_0

    .line 8
    iget-object v0, p0, Lcyj;->b:Ljava/io/File;

    const/4 v1, 0x1

    const/4 v2, 0x1

    iget-wide v4, p0, Lcyj;->c:J

    invoke-static {v0, v1, v2, v4, v5}, Lcti;->a(Ljava/io/File;IIJ)Lcti;

    move-result-object v0

    iput-object v0, p0, Lcyj;->e:Lcti;

    .line 9
    :cond_0
    iget-object v0, p0, Lcyj;->e:Lcti;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 7
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lcud;)Ljava/io/File;
    .locals 4

    .prologue
    .line 10
    iget-object v0, p0, Lcyj;->a:Lcyr;

    invoke-virtual {v0, p1}, Lcyr;->a(Lcud;)Ljava/lang/String;

    move-result-object v1

    .line 11
    const-string v0, "DiskLruCacheWrapper"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 12
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Get: Obtained: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for for Key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :try_start_0
    invoke-direct {p0}, Lcyj;->a()Lcti;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcti;->a(Ljava/lang/String;)Lctn;

    move-result-object v1

    .line 15
    if-eqz v1, :cond_1

    .line 17
    iget-object v1, v1, Lctn;->a:[Ljava/io/File;

    const/4 v2, 0x0

    aget-object v0, v1, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :cond_1
    :goto_0
    return-object v0

    .line 20
    :catch_0
    move-exception v1

    .line 21
    const-string v2, "DiskLruCacheWrapper"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    const-string v2, "DiskLruCacheWrapper"

    const-string v3, "Unable to get from disk cache"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Lcud;Lcyd;)V
    .locals 5

    .prologue
    .line 24
    iget-object v0, p0, Lcyj;->a:Lcyr;

    invoke-virtual {v0, p1}, Lcyr;->a(Lcud;)Ljava/lang/String;

    move-result-object v1

    .line 25
    iget-object v2, p0, Lcyj;->d:Lcyf;

    .line 26
    monitor-enter v2

    .line 27
    :try_start_0
    iget-object v0, v2, Lcyf;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyg;

    .line 28
    if-nez v0, :cond_0

    .line 29
    iget-object v0, v2, Lcyf;->b:Lcyh;

    invoke-virtual {v0}, Lcyh;->a()Lcyg;

    move-result-object v0

    .line 30
    iget-object v3, v2, Lcyf;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    iget v3, v0, Lcyg;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcyg;->b:I

    .line 32
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    iget-object v0, v0, Lcyg;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 34
    :try_start_1
    const-string v0, "DiskLruCacheWrapper"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Put: Obtained: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for for Key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 36
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcyj;->a()Lcti;

    move-result-object v0

    .line 37
    invoke-virtual {v0, v1}, Lcti;->a(Ljava/lang/String;)Lctn;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 38
    if-eqz v2, :cond_2

    .line 39
    iget-object v0, p0, Lcyj;->d:Lcyf;

    invoke-virtual {v0, v1}, Lcyf;->a(Ljava/lang/String;)V

    .line 59
    :goto_0
    return-void

    .line 32
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 42
    :cond_2
    const-wide/16 v2, -0x1

    :try_start_4
    invoke-virtual {v0, v1, v2, v3}, Lcti;->a(Ljava/lang/String;J)Lctl;

    move-result-object v2

    .line 44
    if-nez v2, :cond_5

    .line 45
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Had two simultaneous puts for: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    :try_start_5
    const-string v2, "DiskLruCacheWrapper"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 57
    const-string v2, "DiskLruCacheWrapper"

    const-string v3, "Unable to put to disk cache"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 58
    :cond_3
    :goto_2
    iget-object v0, p0, Lcyj;->d:Lcyf;

    invoke-virtual {v0, v1}, Lcyf;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_4
    :try_start_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 60
    :catchall_1
    move-exception v0

    iget-object v2, p0, Lcyj;->d:Lcyf;

    invoke-virtual {v2, v1}, Lcyf;->a(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_5
    const/4 v0, 0x0

    :try_start_7
    invoke-virtual {v2, v0}, Lctl;->a(I)Ljava/io/File;

    move-result-object v0

    .line 47
    invoke-virtual {p2, v0}, Lcyd;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49
    iget-object v0, v2, Lctl;->d:Lcti;

    const/4 v3, 0x1

    .line 50
    invoke-virtual {v0, v2, v3}, Lcti;->a(Lctl;Z)V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, v2, Lctl;->c:Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 52
    :cond_6
    :try_start_8
    invoke-virtual {v2}, Lctl;->b()V

    goto :goto_2

    .line 54
    :catchall_2
    move-exception v0

    invoke-virtual {v2}, Lctl;->b()V

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method
