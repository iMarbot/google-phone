.class Lur;
.super Lum;
.source "PG"


# instance fields
.field private synthetic a:Luq;


# direct methods
.method constructor <init>(Luq;Landroid/view/Window$Callback;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lur;->a:Luq;

    .line 2
    invoke-direct {p0, p1, p2}, Lum;-><init>(Luk;Landroid/view/Window$Callback;)V

    .line 3
    return-void
.end method


# virtual methods
.method final a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 9
    new-instance v4, Lwl;

    iget-object v0, p0, Lur;->a:Luq;

    iget-object v0, v0, Luq;->b:Landroid/content/Context;

    invoke-direct {v4, v0, p1}, Lwl;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    .line 10
    iget-object v5, p0, Lur;->a:Luq;

    .line 12
    iget-object v0, v5, Luw;->r:Lwf;

    if-eqz v0, :cond_0

    .line 13
    iget-object v0, v5, Luw;->r:Lwf;

    invoke-virtual {v0}, Lwf;->c()V

    .line 14
    :cond_0
    new-instance v6, Lvt;

    invoke-direct {v6, v5, v4}, Lvt;-><init>(Luw;Lwg;)V

    .line 15
    invoke-virtual {v5}, Luw;->a()Ltv;

    move-result-object v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {v0, v6}, Ltv;->a(Lwg;)Lwf;

    move-result-object v0

    iput-object v0, v5, Luw;->r:Lwf;

    .line 18
    :cond_1
    iget-object v0, v5, Luw;->r:Lwf;

    if-nez v0, :cond_6

    .line 20
    invoke-virtual {v5}, Luw;->n()V

    .line 21
    iget-object v0, v5, Luw;->r:Lwf;

    if-eqz v0, :cond_2

    .line 22
    iget-object v0, v5, Luw;->r:Lwf;

    invoke-virtual {v0}, Lwf;->c()V

    .line 24
    :cond_2
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    if-nez v0, :cond_3

    .line 25
    iget-boolean v0, v5, Luw;->l:Z

    if-eqz v0, :cond_8

    .line 26
    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    .line 27
    iget-object v0, v5, Luw;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 28
    const v8, 0x7f01004e

    invoke-virtual {v0, v8, v7, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 29
    iget v8, v7, Landroid/util/TypedValue;->resourceId:I

    if-eqz v8, :cond_7

    .line 30
    iget-object v8, v5, Luw;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    .line 31
    invoke-virtual {v8, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 32
    iget v0, v7, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 33
    new-instance v0, Lwi;

    iget-object v9, v5, Luw;->b:Landroid/content/Context;

    invoke-direct {v0, v9, v2}, Lwi;-><init>(Landroid/content/Context;I)V

    .line 34
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 37
    :goto_0
    new-instance v8, Landroid/support/v7/widget/ActionBarContextView;

    invoke-direct {v8, v0}, Landroid/support/v7/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v8, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    .line 38
    new-instance v8, Landroid/widget/PopupWindow;

    const v9, 0x7f010061

    invoke-direct {v8, v0, v3, v9}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v8, v5, Luw;->t:Landroid/widget/PopupWindow;

    .line 39
    iget-object v8, v5, Luw;->t:Landroid/widget/PopupWindow;

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lte;->a(Landroid/widget/PopupWindow;I)V

    .line 40
    iget-object v8, v5, Luw;->t:Landroid/widget/PopupWindow;

    iget-object v9, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 41
    iget-object v8, v5, Luw;->t:Landroid/widget/PopupWindow;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 42
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    const v9, 0x7f010050

    invoke-virtual {v8, v9, v7, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 43
    iget v7, v7, Landroid/util/TypedValue;->data:I

    .line 44
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 45
    invoke-static {v7, v0}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 46
    iget-object v7, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    .line 47
    iput v0, v7, Landroid/support/v7/widget/ActionBarContextView;->d:I

    .line 48
    iget-object v0, v5, Luw;->t:Landroid/widget/PopupWindow;

    const/4 v7, -0x2

    invoke-virtual {v0, v7}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 49
    new-instance v0, Luz;

    invoke-direct {v0, v5}, Luz;-><init>(Luw;)V

    iput-object v0, v5, Luw;->u:Ljava/lang/Runnable;

    .line 57
    :cond_3
    :goto_1
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    if-eqz v0, :cond_5

    .line 58
    invoke-virtual {v5}, Luw;->n()V

    .line 59
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->b()V

    .line 60
    new-instance v7, Lwj;

    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    iget-object v0, v5, Luw;->t:Landroid/widget/PopupWindow;

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    invoke-direct {v7, v8, v9, v6, v0}, Lwj;-><init>(Landroid/content/Context;Landroid/support/v7/widget/ActionBarContextView;Lwg;Z)V

    .line 61
    invoke-virtual {v7}, Lwf;->b()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v6, v7, v0}, Lwg;->a(Lwf;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 62
    invoke-virtual {v7}, Lwf;->d()V

    .line 63
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionBarContextView;->a(Lwf;)V

    .line 64
    iput-object v7, v5, Luw;->r:Lwf;

    .line 65
    invoke-virtual {v5}, Luw;->m()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 66
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarContextView;->setAlpha(F)V

    .line 67
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-static {v0}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v0

    invoke-virtual {v0, v10}, Lrv;->a(F)Lrv;

    move-result-object v0

    iput-object v0, v5, Luw;->v:Lrv;

    .line 68
    iget-object v0, v5, Luw;->v:Lrv;

    new-instance v1, Lvb;

    invoke-direct {v1, v5}, Lvb;-><init>(Luw;)V

    invoke-virtual {v0, v1}, Lrv;->a(Lrz;)Lrv;

    .line 75
    :cond_4
    :goto_3
    iget-object v0, v5, Luw;->t:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_5

    .line 76
    iget-object v0, v5, Luw;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, v5, Luw;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 78
    :cond_5
    :goto_4
    iget-object v0, v5, Luw;->r:Lwf;

    .line 79
    iput-object v0, v5, Luw;->r:Lwf;

    .line 80
    :cond_6
    iget-object v0, v5, Luw;->r:Lwf;

    .line 82
    if-eqz v0, :cond_c

    .line 83
    invoke-virtual {v4, v0}, Lwl;->b(Lwf;)Landroid/view/ActionMode;

    move-result-object v0

    .line 84
    :goto_5
    return-object v0

    .line 36
    :cond_7
    iget-object v0, v5, Luw;->b:Landroid/content/Context;

    goto/16 :goto_0

    .line 51
    :cond_8
    iget-object v0, v5, Luw;->w:Landroid/view/ViewGroup;

    const v7, 0x7f0e00bd

    .line 52
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ViewStubCompat;

    .line 53
    if-eqz v0, :cond_3

    .line 54
    invoke-virtual {v5}, Luw;->l()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 55
    iput-object v7, v0, Landroid/support/v7/widget/ViewStubCompat;->a:Landroid/view/LayoutInflater;

    .line 56
    invoke-virtual {v0}, Landroid/support/v7/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionBarContextView;

    iput-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 60
    goto :goto_2

    .line 69
    :cond_a
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v10}, Landroid/support/v7/widget/ActionBarContextView;->setAlpha(F)V

    .line 70
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionBarContextView;->setVisibility(I)V

    .line 71
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 72
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_4

    .line 73
    iget-object v0, v5, Luw;->s:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 74
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->g(Landroid/view/View;)V

    goto :goto_3

    .line 77
    :cond_b
    iput-object v3, v5, Luw;->r:Lwf;

    goto :goto_4

    :cond_c
    move-object v0, v3

    .line 84
    goto :goto_5
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lur;->a:Luq;

    .line 5
    iget-boolean v0, v0, Luq;->p:Z

    .line 6
    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p0, p1}, Lur;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    .line 8
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lum;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    goto :goto_0
.end method
