.class final Ldux;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic a:Ldub;


# direct methods
.method constructor <init>(Ldub;)V
    .locals 0

    iput-object p1, p0, Ldux;->a:Ldub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1
    iget-object v0, p0, Ldux;->a:Ldub;

    .line 2
    iget-object v1, v0, Ldub;->a:Ldvn;

    .line 4
    invoke-static {}, Ldvw;->b()V

    invoke-virtual {v1}, Lduz;->m()V

    const-string v0, "Sync dispatching local hits"

    invoke-virtual {v1, v0}, Lduy;->b(Ljava/lang/String;)V

    iget-wide v2, v1, Ldvn;->e:J

    invoke-virtual {v1}, Ldvn;->d()V

    :try_start_0
    invoke-virtual {v1}, Ldvn;->e()Z

    .line 5
    iget-object v0, v1, Lduy;->f:Ldvb;

    .line 6
    iget-object v4, v0, Ldvb;->g:Ldui;

    invoke-static {v4}, Ldvb;->a(Lduz;)V

    iget-object v0, v0, Ldvb;->g:Ldui;

    .line 7
    invoke-virtual {v0}, Ldui;->e()V

    invoke-virtual {v1}, Ldvn;->f()V

    iget-wide v4, v1, Ldvn;->e:J

    cmp-long v0, v4, v2

    if-eqz v0, :cond_0

    iget-object v0, v1, Ldvn;->c:Lduf;

    invoke-virtual {v0}, Lduf;->c()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 7
    :catch_0
    move-exception v0

    const-string v2, "Sync local dispatch failed"

    invoke-virtual {v1, v2, v0}, Lduy;->e(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1}, Ldvn;->f()V

    goto :goto_0
.end method
