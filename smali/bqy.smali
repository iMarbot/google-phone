.class final Lbqy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/view/Surface;

.field private b:F

.field private c:F

.field private d:F

.field private e:D


# direct methods
.method constructor <init>(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    iput-object v0, p0, Lbqy;->a:Landroid/view/Surface;

    .line 3
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 38
    invoke-static {}, Lbdf;->c()V

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    const-wide/16 v2, 0x21

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 40
    return-void
.end method

.method public final run()V
    .locals 9

    .prologue
    const/high16 v8, 0x41800000    # 16.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 4
    .line 5
    invoke-static {}, Lbdf;->c()V

    .line 6
    :try_start_0
    iget-object v0, p0, Lbqy;->a:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 11
    const-string v1, "SimulatorRemoteVideo.RenderThread.drawFrame"

    const-string v2, "size; %d x %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 12
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 13
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 14
    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 15
    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 17
    invoke-static {}, Lbdf;->c()V

    .line 18
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    .line 19
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    .line 20
    iget v3, p0, Lbqy;->b:F

    cmpl-float v3, v3, v6

    if-nez v3, :cond_1

    iget v3, p0, Lbqy;->c:F

    cmpl-float v3, v3, v6

    if-nez v3, :cond_1

    .line 21
    int-to-float v1, v1

    div-float/2addr v1, v7

    iput v1, p0, Lbqy;->b:F

    .line 22
    int-to-float v1, v2

    div-float/2addr v1, v7

    iput v1, p0, Lbqy;->c:F

    .line 23
    const-wide v2, 0x3fe921fb54442d18L    # 0.7853981633974483

    iput-wide v2, p0, Lbqy;->e:D

    .line 24
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float/2addr v1, v2

    iput v1, p0, Lbqy;->d:F

    .line 30
    :cond_0
    :goto_0
    invoke-static {}, Lbdf;->c()V

    .line 31
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 32
    const v2, -0xff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 34
    iget v2, p0, Lbqy;->b:F

    iget v3, p0, Lbqy;->c:F

    iget v4, p0, Lbqy;->d:F

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 35
    iget-object v1, p0, Lbqy;->a:Landroid/view/Surface;

    invoke-virtual {v1, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 36
    :goto_1
    invoke-virtual {p0}, Lbqy;->a()V

    .line 37
    return-void

    .line 8
    :catch_0
    move-exception v0

    .line 9
    const-string v1, "SimulatorRemoteVideo.RenderThread.drawFrame"

    const-string v2, "unable to lock canvas"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 25
    :cond_1
    iget v3, p0, Lbqy;->b:F

    iget-wide v4, p0, Lbqy;->e:D

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    iput v3, p0, Lbqy;->b:F

    .line 26
    iget v3, p0, Lbqy;->c:F

    iget-wide v4, p0, Lbqy;->e:D

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, v8

    add-float/2addr v3, v4

    iput v3, p0, Lbqy;->c:F

    .line 27
    iget v3, p0, Lbqy;->b:F

    iget v4, p0, Lbqy;->d:F

    add-float/2addr v3, v4

    int-to-float v1, v1

    cmpl-float v1, v3, v1

    if-gez v1, :cond_2

    iget v1, p0, Lbqy;->b:F

    iget v3, p0, Lbqy;->d:F

    sub-float/2addr v1, v3

    cmpg-float v1, v1, v6

    if-lez v1, :cond_2

    iget v1, p0, Lbqy;->c:F

    iget v3, p0, Lbqy;->d:F

    add-float/2addr v1, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gez v1, :cond_2

    iget v1, p0, Lbqy;->c:F

    iget v2, p0, Lbqy;->d:F

    sub-float/2addr v1, v2

    cmpg-float v1, v1, v6

    if-gtz v1, :cond_0

    .line 28
    :cond_2
    iget-wide v2, p0, Lbqy;->e:D

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v2, v4

    iput-wide v2, p0, Lbqy;->e:D

    goto :goto_0
.end method
