.class public Lcsw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static volatile h:Lcsw;

.field private static volatile i:Z


# instance fields
.field public final a:Lcxl;

.field public final b:Lcsy;

.field public final c:Lcta;

.field public final d:Lcxg;

.field public final e:Ldfm;

.field public final f:Ldfd;

.field public final g:Ljava/util/List;

.field private j:Lcym;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcwd;Lcym;Lcxl;Lcxg;Ldfm;Ldfd;ILdgn;Ljava/util/Map;)V
    .locals 18
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 103
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcsw;->g:Ljava/util/List;

    .line 105
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcsw;->a:Lcxl;

    .line 106
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcsw;->d:Lcxg;

    .line 107
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcsw;->j:Lcym;

    .line 108
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcsw;->e:Ldfm;

    .line 109
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcsw;->f:Ldfd;

    .line 111
    move-object/from16 v0, p9

    iget-object v3, v0, Ldgn;->p:Lcuh;

    .line 112
    sget-object v4, Ldda;->a:Lcue;

    invoke-virtual {v3, v4}, Lcuh;->a(Lcue;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lctx;

    .line 113
    new-instance v4, Lczc;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v4, v0, v1, v3}, Lczc;-><init>(Lcym;Lcxl;Lctx;)V

    .line 114
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 115
    new-instance v4, Lcta;

    invoke-direct {v4}, Lcta;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcsw;->c:Lcta;

    .line 116
    move-object/from16 v0, p0

    iget-object v4, v0, Lcsw;->c:Lcta;

    new-instance v5, Ldco;

    invoke-direct {v5}, Ldco;-><init>()V

    .line 117
    iget-object v4, v4, Lcta;->d:Ldfz;

    invoke-virtual {v4, v5}, Ldfz;->a(Lcub;)V

    .line 118
    new-instance v4, Ldda;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcsw;->c:Lcta;

    invoke-virtual {v5}, Lcta;->a()Ljava/util/List;

    move-result-object v5

    .line 119
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v4, v5, v6, v0, v1}, Ldda;-><init>(Ljava/util/List;Landroid/util/DisplayMetrics;Lcxl;Lcxg;)V

    .line 120
    new-instance v5, Ldee;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcsw;->c:Lcta;

    .line 121
    invoke-virtual {v6}, Lcta;->a()Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v5, v0, v6, v1, v2}, Ldee;-><init>(Landroid/content/Context;Ljava/util/List;Lcxl;Lcxg;)V

    .line 122
    new-instance v6, Lddq;

    move-object/from16 v0, p4

    invoke-direct {v6, v0}, Lddq;-><init>(Lcxl;)V

    .line 123
    new-instance v7, Ldcl;

    invoke-direct {v7, v4}, Ldcl;-><init>(Ldda;)V

    .line 124
    new-instance v8, Lddl;

    move-object/from16 v0, p5

    invoke-direct {v8, v4, v0}, Lddl;-><init>(Ldda;Lcxg;)V

    .line 125
    new-instance v4, Ldeb;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ldeb;-><init>(Landroid/content/Context;)V

    .line 126
    new-instance v9, Ldbf;

    invoke-direct {v9, v3}, Ldbf;-><init>(Landroid/content/res/Resources;)V

    .line 127
    new-instance v10, Ldbg;

    invoke-direct {v10, v3}, Ldbg;-><init>(Landroid/content/res/Resources;)V

    .line 128
    new-instance v11, Ldbe;

    invoke-direct {v11, v3}, Ldbe;-><init>(Landroid/content/res/Resources;)V

    .line 129
    new-instance v12, Ldci;

    invoke-direct {v12}, Ldci;-><init>()V

    .line 130
    move-object/from16 v0, p0

    iget-object v13, v0, Lcsw;->c:Lcta;

    const-class v14, Ljava/nio/ByteBuffer;

    new-instance v15, Lczo;

    invoke-direct {v15}, Lczo;-><init>()V

    .line 131
    invoke-virtual {v13, v14, v15}, Lcta;->a(Ljava/lang/Class;Lctz;)Lcta;

    move-result-object v13

    const-class v14, Ljava/io/InputStream;

    new-instance v15, Ldbh;

    move-object/from16 v0, p5

    invoke-direct {v15, v0}, Ldbh;-><init>(Lcxg;)V

    .line 132
    invoke-virtual {v13, v14, v15}, Lcta;->a(Ljava/lang/Class;Lctz;)Lcta;

    move-result-object v13

    const-string v14, "Bitmap"

    const-class v15, Ljava/nio/ByteBuffer;

    const-class v16, Landroid/graphics/Bitmap;

    .line 133
    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v7}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v13

    const-string v14, "Bitmap"

    const-class v15, Ljava/io/InputStream;

    const-class v16, Landroid/graphics/Bitmap;

    .line 134
    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v8}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v13

    const-string v14, "Bitmap"

    const-class v15, Landroid/os/ParcelFileDescriptor;

    const-class v16, Landroid/graphics/Bitmap;

    .line 135
    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v6}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v13

    const-string v14, "Bitmap"

    const-class v15, Landroid/graphics/Bitmap;

    const-class v16, Landroid/graphics/Bitmap;

    new-instance v17, Lddp;

    invoke-direct/range {v17 .. v17}, Lddp;-><init>()V

    .line 136
    invoke-virtual/range {v13 .. v17}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v13

    const-class v14, Landroid/graphics/Bitmap;

    const-class v15, Landroid/graphics/Bitmap;

    .line 137
    sget-object v16, Ldbm;->a:Ldbm;

    .line 138
    invoke-virtual/range {v13 .. v16}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v13

    const-class v14, Landroid/graphics/Bitmap;

    .line 139
    invoke-virtual {v13, v14, v12}, Lcta;->a(Ljava/lang/Class;Lcuj;)Lcta;

    move-result-object v13

    const-string v14, "BitmapDrawable"

    const-class v15, Ljava/nio/ByteBuffer;

    const-class v16, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v17, Ldcg;

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    invoke-direct {v0, v3, v1, v7}, Ldcg;-><init>(Landroid/content/res/Resources;Lcxl;Lcui;)V

    .line 140
    invoke-virtual/range {v13 .. v17}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v7

    const-string v13, "BitmapDrawable"

    const-class v14, Ljava/io/InputStream;

    const-class v15, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v16, Ldcg;

    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-direct {v0, v3, v1, v8}, Ldcg;-><init>(Landroid/content/res/Resources;Lcxl;Lcui;)V

    .line 141
    move-object/from16 v0, v16

    invoke-virtual {v7, v13, v14, v15, v0}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v7

    const-string v8, "BitmapDrawable"

    const-class v13, Landroid/os/ParcelFileDescriptor;

    const-class v14, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v15, Ldcg;

    move-object/from16 v0, p4

    invoke-direct {v15, v3, v0, v6}, Ldcg;-><init>(Landroid/content/res/Resources;Lcxl;Lcui;)V

    .line 142
    invoke-virtual {v7, v8, v13, v14, v15}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v6

    const-class v7, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v8, Ldch;

    move-object/from16 v0, p4

    invoke-direct {v8, v0, v12}, Ldch;-><init>(Lcxl;Lcuj;)V

    .line 143
    invoke-virtual {v6, v7, v8}, Lcta;->a(Ljava/lang/Class;Lcuj;)Lcta;

    move-result-object v6

    const-string v7, "Gif"

    const-class v8, Ljava/io/InputStream;

    const-class v12, Ldeh;

    new-instance v13, Lder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcsw;->c:Lcta;

    .line 144
    invoke-virtual {v14}, Lcta;->a()Ljava/util/List;

    move-result-object v14

    move-object/from16 v0, p5

    invoke-direct {v13, v14, v5, v0}, Lder;-><init>(Ljava/util/List;Lcui;Lcxg;)V

    .line 145
    invoke-virtual {v6, v7, v8, v12, v13}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v6

    const-string v7, "Gif"

    const-class v8, Ljava/nio/ByteBuffer;

    const-class v12, Ldeh;

    .line 146
    invoke-virtual {v6, v7, v8, v12, v5}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v5

    const-class v6, Ldeh;

    new-instance v7, Ldei;

    invoke-direct {v7}, Ldei;-><init>()V

    .line 147
    invoke-virtual {v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Lcuj;)Lcta;

    move-result-object v5

    const-class v6, Lctr;

    const-class v7, Lctr;

    .line 148
    sget-object v8, Ldbm;->a:Ldbm;

    .line 149
    invoke-virtual {v5, v6, v7, v8}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v5

    const-string v6, "Bitmap"

    const-class v7, Lctr;

    const-class v8, Landroid/graphics/Bitmap;

    new-instance v12, Ldep;

    move-object/from16 v0, p4

    invoke-direct {v12, v0}, Ldep;-><init>(Lcxl;)V

    .line 150
    invoke-virtual {v5, v6, v7, v8, v12}, Lcta;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v5

    const-class v6, Landroid/net/Uri;

    const-class v7, Landroid/graphics/drawable/Drawable;

    .line 151
    invoke-virtual {v5, v6, v7, v4}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v5

    const-class v6, Landroid/net/Uri;

    const-class v7, Landroid/graphics/Bitmap;

    new-instance v8, Lddk;

    move-object/from16 v0, p4

    invoke-direct {v8, v4, v0}, Lddk;-><init>(Ldeb;Lcxl;)V

    .line 152
    invoke-virtual {v5, v6, v7, v8}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v4

    new-instance v5, Lddv;

    invoke-direct {v5}, Lddv;-><init>()V

    .line 153
    invoke-virtual {v4, v5}, Lcta;->a(Lcup;)Lcta;

    move-result-object v4

    const-class v5, Ljava/io/File;

    const-class v6, Ljava/nio/ByteBuffer;

    new-instance v7, Lczr;

    invoke-direct {v7}, Lczr;-><init>()V

    .line 154
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/io/File;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Lczy;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lczy;-><init>(B)V

    .line 155
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/io/File;

    const-class v6, Ljava/io/File;

    new-instance v7, Lded;

    invoke-direct {v7}, Lded;-><init>()V

    .line 156
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v4

    const-class v5, Ljava/io/File;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    new-instance v7, Lczy;

    invoke-direct {v7}, Lczy;-><init>()V

    .line 157
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/io/File;

    const-class v6, Ljava/io/File;

    .line 158
    sget-object v7, Ldbm;->a:Ldbm;

    .line 159
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    new-instance v5, Lcuz;

    move-object/from16 v0, p5

    invoke-direct {v5, v0}, Lcuz;-><init>(Lcxg;)V

    .line 160
    invoke-virtual {v4, v5}, Lcta;->a(Lcup;)Lcta;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v6, Ljava/io/InputStream;

    .line 161
    invoke-virtual {v4, v5, v6, v9}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    .line 162
    invoke-virtual {v4, v5, v6, v11}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/Integer;

    const-class v6, Ljava/io/InputStream;

    .line 163
    invoke-virtual {v4, v5, v6, v9}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/Integer;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    .line 164
    invoke-virtual {v4, v5, v6, v11}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/Integer;

    const-class v6, Landroid/net/Uri;

    .line 165
    invoke-virtual {v4, v5, v6, v10}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v6, Landroid/net/Uri;

    .line 166
    invoke-virtual {v4, v5, v6, v10}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/String;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Lczv;

    invoke-direct {v7}, Lczv;-><init>()V

    .line 167
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/String;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbk;

    invoke-direct {v7}, Ldbk;-><init>()V

    .line 168
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/lang/String;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    new-instance v7, Ldbj;

    invoke-direct {v7}, Ldbj;-><init>()V

    .line 169
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbx;

    invoke-direct {v7}, Ldbx;-><init>()V

    .line 170
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Lczg;

    .line 171
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    invoke-direct {v7, v8}, Lczg;-><init>(Landroid/content/res/AssetManager;)V

    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    new-instance v7, Lczf;

    .line 172
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    invoke-direct {v7, v8}, Lczf;-><init>(Landroid/content/res/AssetManager;)V

    .line 173
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbz;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ldbz;-><init>(Landroid/content/Context;)V

    .line 174
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldcb;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ldcb;-><init>(Landroid/content/Context;)V

    .line 175
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbr;

    .line 176
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-direct {v7, v8}, Ldbr;-><init>(Landroid/content/ContentResolver;)V

    .line 177
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Landroid/os/ParcelFileDescriptor;

    new-instance v7, Ldbp;

    .line 178
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-direct {v7, v8}, Ldbp;-><init>(Landroid/content/ContentResolver;)V

    .line 179
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbt;

    invoke-direct {v7}, Ldbt;-><init>()V

    .line 180
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ljava/net/URL;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldcd;

    invoke-direct {v7}, Ldcd;-><init>()V

    .line 181
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Ljava/io/File;

    new-instance v7, Ldal;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ldal;-><init>(Landroid/content/Context;)V

    .line 182
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Ldad;

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Ldbv;

    invoke-direct {v7}, Ldbv;-><init>()V

    .line 183
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, [B

    const-class v6, Ljava/nio/ByteBuffer;

    new-instance v7, Lczi;

    invoke-direct {v7}, Lczi;-><init>()V

    .line 184
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, [B

    const-class v6, Ljava/io/InputStream;

    new-instance v7, Lczm;

    invoke-direct {v7}, Lczm;-><init>()V

    .line 185
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/net/Uri;

    const-class v6, Landroid/net/Uri;

    .line 186
    sget-object v7, Ldbm;->a:Ldbm;

    .line 187
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/graphics/drawable/Drawable;

    const-class v6, Landroid/graphics/drawable/Drawable;

    .line 188
    sget-object v7, Ldbm;->a:Ldbm;

    .line 189
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldat;)Lcta;

    move-result-object v4

    const-class v5, Landroid/graphics/drawable/Drawable;

    const-class v6, Landroid/graphics/drawable/Drawable;

    new-instance v7, Ldec;

    invoke-direct {v7}, Ldec;-><init>()V

    .line 190
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Lcui;)Lcta;

    move-result-object v4

    const-class v5, Landroid/graphics/Bitmap;

    const-class v6, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v7, Ldet;

    move-object/from16 v0, p4

    invoke-direct {v7, v3, v0}, Ldet;-><init>(Landroid/content/res/Resources;Lcxl;)V

    .line 191
    invoke-virtual {v4, v5, v6, v7}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldev;)Lcta;

    move-result-object v3

    const-class v4, Landroid/graphics/Bitmap;

    const-class v5, [B

    new-instance v6, Ldes;

    invoke-direct {v6}, Ldes;-><init>()V

    .line 192
    invoke-virtual {v3, v4, v5, v6}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldev;)Lcta;

    move-result-object v3

    const-class v4, Ldeh;

    const-class v5, [B

    new-instance v6, Ldeu;

    invoke-direct {v6}, Ldeu;-><init>()V

    .line 193
    invoke-virtual {v3, v4, v5, v6}, Lcta;->a(Ljava/lang/Class;Ljava/lang/Class;Ldev;)Lcta;

    .line 194
    new-instance v6, Ldgv;

    invoke-direct {v6}, Ldgv;-><init>()V

    .line 195
    new-instance v3, Lcsy;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcsw;->c:Lcta;

    move-object/from16 v4, p1

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p2

    move/from16 v10, p8

    invoke-direct/range {v3 .. v10}, Lcsy;-><init>(Landroid/content/Context;Lcta;Ldgv;Ldgn;Ljava/util/Map;Lcwd;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcsw;->b:Lcsy;

    .line 196
    return-void
.end method

.method private static a()Lcsv;
    .locals 3

    .prologue
    .line 89
    const/4 v1, 0x0

    .line 90
    :try_start_0
    const-string v0, "com.bumptech.glide.GeneratedAppGlideModuleImpl"

    .line 91
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsv;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 102
    :goto_0
    return-object v0

    .line 95
    :catch_0
    move-exception v0

    const-string v0, "Glide"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "Glide"

    const-string v2, "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    .line 97
    goto :goto_0

    .line 98
    :catch_1
    move-exception v0

    .line 99
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "GeneratedAppGlideModuleImpl is implemented incorrectly. If you\'ve manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 100
    :catch_2
    move-exception v0

    .line 101
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "GeneratedAppGlideModuleImpl is implemented incorrectly. If you\'ve manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;)Lcsw;
    .locals 3

    .prologue
    .line 1
    sget-object v0, Lcsw;->h:Lcsw;

    if-nez v0, :cond_2

    .line 2
    const-class v1, Lcsw;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lcsw;->h:Lcsw;

    if-nez v0, :cond_1

    .line 5
    sget-boolean v0, Lcsw;->i:Z

    if-eqz v0, :cond_0

    .line 6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 10
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 7
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcsw;->i:Z

    .line 8
    invoke-static {p0}, Lcsw;->d(Landroid/content/Context;)V

    .line 9
    const/4 v0, 0x0

    sput-boolean v0, Lcsw;->i:Z

    .line 10
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 11
    :cond_2
    sget-object v0, Lcsw;->h:Lcsw;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ldfm;
    .locals 1

    .prologue
    .line 197
    const-string v0, "You cannot start a load on a not yet attached View or a  Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed)."

    invoke-static {p0, v0}, Ldhh;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 198
    invoke-static {p0}, Lcsw;->a(Landroid/content/Context;)Lcsw;

    move-result-object v0

    .line 199
    iget-object v0, v0, Lcsw;->e:Ldfm;

    .line 200
    return-object v0
.end method

.method public static c(Landroid/content/Context;)Lcte;
    .locals 1

    .prologue
    .line 201
    invoke-static {p0}, Lcsw;->b(Landroid/content/Context;)Ldfm;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldfm;->a(Landroid/content/Context;)Lcte;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/content/Context;)V
    .locals 22

    .prologue
    .line 12
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    .line 13
    invoke-static {}, Lcsw;->a()Lcsv;

    move-result-object v2

    .line 14
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 15
    new-instance v3, Ldfw;

    invoke-direct {v3, v15}, Ldfw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Ldfw;->a()Ljava/util/List;

    move-result-object v16

    .line 16
    if-eqz v2, :cond_2

    .line 17
    invoke-virtual {v2}, Lcsv;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 19
    invoke-virtual {v2}, Lcsv;->a()Ljava/util/Set;

    move-result-object v3

    .line 20
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 21
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 22
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldfu;

    .line 23
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 24
    const-string v5, "Glide"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 25
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2e

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "AppGlideModule excludes manifest GlideModule: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 28
    :cond_2
    const-string v2, "Glide"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 29
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldfu;

    .line 30
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Discovered GlideModule from manifest: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 32
    :cond_3
    new-instance v17, Lcsx;

    invoke-direct/range {v17 .. v17}, Lcsx;-><init>()V

    const/4 v2, 0x0

    .line 34
    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->m:Ldfn;

    .line 37
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    .line 40
    :cond_4
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->f:Lcyu;

    if-nez v2, :cond_5

    .line 42
    invoke-static {}, Lcyu;->b()I

    move-result v3

    const-string v4, "source"

    sget-object v5, Lcyy;->b:Lcyy;

    .line 43
    new-instance v2, Lcyu;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcyu;-><init>(ILjava/lang/String;Lcyy;ZZ)V

    .line 44
    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->f:Lcyu;

    .line 45
    :cond_5
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->g:Lcyu;

    if-nez v2, :cond_6

    .line 46
    invoke-static {}, Lcyu;->a()Lcyu;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->g:Lcyu;

    .line 47
    :cond_6
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->i:Lcyo;

    if-nez v2, :cond_7

    .line 48
    new-instance v2, Lcyp;

    invoke-direct {v2, v15}, Lcyp;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v3, Lcyo;

    invoke-direct {v3, v2}, Lcyo;-><init>(Lcyp;)V

    .line 50
    move-object/from16 v0, v17

    iput-object v3, v0, Lcsx;->i:Lcyo;

    .line 51
    :cond_7
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->j:Ldfd;

    if-nez v2, :cond_8

    .line 52
    new-instance v2, Ldfd;

    invoke-direct {v2}, Ldfd;-><init>()V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->j:Ldfd;

    .line 53
    :cond_8
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->c:Lcxl;

    if-nez v2, :cond_9

    .line 54
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->i:Lcyo;

    .line 55
    iget v2, v2, Lcyo;->a:I

    .line 57
    if-lez v2, :cond_e

    .line 58
    new-instance v3, Lcxt;

    int-to-long v4, v2

    invoke-direct {v3, v4, v5}, Lcxt;-><init>(J)V

    move-object/from16 v0, v17

    iput-object v3, v0, Lcsx;->c:Lcxl;

    .line 60
    :cond_9
    :goto_3
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->d:Lcxg;

    if-nez v2, :cond_a

    .line 61
    new-instance v2, Lcxg;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcsx;->i:Lcyo;

    .line 62
    iget v3, v3, Lcyo;->c:I

    .line 63
    invoke-direct {v2, v3}, Lcxg;-><init>(I)V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->d:Lcxg;

    .line 64
    :cond_a
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->e:Lcym;

    if-nez v2, :cond_b

    .line 65
    new-instance v2, Lcyl;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcsx;->i:Lcyo;

    .line 66
    iget v3, v3, Lcyo;->b:I

    .line 67
    int-to-long v4, v3

    invoke-direct {v2, v4, v5}, Lcyl;-><init>(J)V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->e:Lcym;

    .line 68
    :cond_b
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->h:Lcyc;

    if-nez v2, :cond_c

    .line 69
    new-instance v2, Lcyk;

    invoke-direct {v2, v15}, Lcyk;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->h:Lcyc;

    .line 70
    :cond_c
    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->b:Lcwd;

    if-nez v2, :cond_d

    .line 71
    new-instance v2, Lcwd;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcsx;->e:Lcym;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcsx;->h:Lcyc;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcsx;->g:Lcyu;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcsx;->f:Lcyu;

    move-object/from16 v21, v0

    .line 72
    new-instance v3, Lcyu;

    const/4 v4, 0x0

    const v5, 0x7fffffff

    sget-wide v6, Lcyu;->a:J

    const-string v8, "source-unlimited"

    sget-object v9, Lcyy;->b:Lcyy;

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v12}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    invoke-direct/range {v3 .. v12}, Lcyu;-><init>(IIJLjava/lang/String;Lcyy;ZZLjava/util/concurrent/BlockingQueue;)V

    .line 73
    invoke-static {}, Lcyu;->b()I

    move-result v4

    .line 74
    const/4 v5, 0x4

    if-lt v4, v5, :cond_f

    const/4 v7, 0x2

    .line 75
    :goto_4
    new-instance v5, Lcyu;

    const/4 v6, 0x0

    sget-wide v8, Lcyu;->a:J

    const-string v10, "animation"

    sget-object v11, Lcyy;->b:Lcyy;

    const/4 v12, 0x1

    const/4 v13, 0x0

    new-instance v14, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v14}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    invoke-direct/range {v5 .. v14}, Lcyu;-><init>(IIJLjava/lang/String;Lcyy;ZZLjava/util/concurrent/BlockingQueue;)V

    move-object v6, v2

    move-object/from16 v7, v18

    move-object/from16 v8, v19

    move-object/from16 v9, v20

    move-object/from16 v10, v21

    move-object v11, v3

    move-object v12, v5

    .line 76
    invoke-direct/range {v6 .. v12}, Lcwd;-><init>(Lcym;Lcyc;Lcyu;Lcyu;Lcyu;Lcyu;)V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->b:Lcwd;

    .line 77
    :cond_d
    new-instance v8, Ldfm;

    move-object/from16 v0, v17

    iget-object v2, v0, Lcsx;->m:Ldfn;

    invoke-direct {v8, v2}, Ldfm;-><init>(Ldfn;)V

    .line 78
    new-instance v2, Lcsw;

    move-object/from16 v0, v17

    iget-object v4, v0, Lcsx;->b:Lcwd;

    move-object/from16 v0, v17

    iget-object v5, v0, Lcsx;->e:Lcym;

    move-object/from16 v0, v17

    iget-object v6, v0, Lcsx;->c:Lcxl;

    move-object/from16 v0, v17

    iget-object v7, v0, Lcsx;->d:Lcxg;

    move-object/from16 v0, v17

    iget-object v9, v0, Lcsx;->j:Ldfd;

    move-object/from16 v0, v17

    iget v10, v0, Lcsx;->k:I

    move-object/from16 v0, v17

    iget-object v11, v0, Lcsx;->l:Ldgn;

    .line 80
    const/4 v3, 0x1

    iput-boolean v3, v11, Ldgn;->s:Z

    .line 82
    move-object/from16 v0, v17

    iget-object v12, v0, Lcsx;->a:Ljava/util/Map;

    move-object v3, v15

    invoke-direct/range {v2 .. v12}, Lcsw;-><init>(Landroid/content/Context;Lcwd;Lcym;Lcxl;Lcxg;Ldfm;Ldfd;ILdgn;Ljava/util/Map;)V

    .line 84
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_5

    .line 59
    :cond_e
    new-instance v2, Lcxm;

    invoke-direct {v2}, Lcxm;-><init>()V

    move-object/from16 v0, v17

    iput-object v2, v0, Lcsx;->c:Lcxl;

    goto/16 :goto_3

    .line 74
    :cond_f
    const/4 v7, 0x1

    goto :goto_4

    .line 86
    :cond_10
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 87
    sput-object v2, Lcsw;->h:Lcsw;

    .line 88
    return-void
.end method


# virtual methods
.method final a(Ldha;)V
    .locals 3

    .prologue
    .line 202
    iget-object v1, p0, Lcsw;->g:Ljava/util/List;

    monitor-enter v1

    .line 203
    :try_start_0
    iget-object v0, p0, Lcsw;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcte;

    .line 204
    invoke-virtual {v0, p1}, Lcte;->b(Ldha;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    monitor-exit v1

    return-void

    .line 207
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to remove target from managers"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 216
    .line 217
    invoke-static {}, Ldhw;->a()V

    .line 218
    iget-object v0, p0, Lcsw;->j:Lcym;

    invoke-interface {v0}, Lcym;->a()V

    .line 219
    iget-object v0, p0, Lcsw;->a:Lcxl;

    invoke-interface {v0}, Lcxl;->a()V

    .line 220
    iget-object v0, p0, Lcsw;->d:Lcxg;

    invoke-virtual {v0}, Lcxg;->a()V

    .line 221
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 209
    .line 210
    invoke-static {}, Ldhw;->a()V

    .line 211
    iget-object v0, p0, Lcsw;->j:Lcym;

    invoke-interface {v0, p1}, Lcym;->a(I)V

    .line 212
    iget-object v0, p0, Lcsw;->a:Lcxl;

    invoke-interface {v0, p1}, Lcxl;->a(I)V

    .line 213
    iget-object v0, p0, Lcsw;->d:Lcxg;

    invoke-virtual {v0, p1}, Lcxg;->a(I)V

    .line 214
    return-void
.end method
