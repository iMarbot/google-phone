.class Lfkp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/Map;

.field private b:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lfkp;->a:Ljava/util/Map;

    .line 3
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    iput-object v0, p0, Lfkp;->b:Ljava/util/Map;

    .line 4
    return-void
.end method


# virtual methods
.method public a(Lfjz;)Ledj;
    .locals 1

    .prologue
    .line 18
    instance-of v0, p1, Lfkx;

    if-eqz v0, :cond_0

    .line 19
    check-cast p1, Lfkx;

    invoke-virtual {p1}, Lfkx;->e()Ledj;

    move-result-object v0

    .line 20
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lfka;)Ledl;
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lfkp;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lfkp;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledl;

    .line 12
    :goto_0
    return-object v0

    .line 10
    :cond_0
    new-instance v0, Lfkq;

    invoke-direct {v0, p1}, Lfkq;-><init>(Lfka;)V

    .line 11
    iget-object v1, p0, Lfkp;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lfkb;)Ledm;
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lfkp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    iget-object v0, p0, Lfkp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledm;

    .line 17
    :goto_0
    return-object v0

    .line 15
    :cond_0
    new-instance v0, Lfkr;

    invoke-direct {v0, p0, p1}, Lfkr;-><init>(Lfkp;Lfkb;)V

    .line 16
    iget-object v1, p0, Lfkp;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lfjy;)Lesq;
    .locals 1

    .prologue
    .line 5
    instance-of v0, p1, Lfkn;

    if-eqz v0, :cond_0

    .line 6
    check-cast p1, Lfkn;

    invoke-interface {p1}, Lfkn;->a()Lesq;

    move-result-object v0

    .line 7
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lecl;)Lfjv;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lfko;

    invoke-direct {v0, p1}, Lfko;-><init>(Lecl;)V

    return-object v0
.end method

.method public a(Ledn;)Lfkc;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lfkc;

    sget-object v1, Lfku;->a:Lfky;

    invoke-direct {v0, p1, v1}, Lfkc;-><init>(Ledn;Lfky;)V

    return-object v0
.end method

.method public b(Lfka;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lfkp;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method

.method public b(Lfkb;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lfkp;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method
