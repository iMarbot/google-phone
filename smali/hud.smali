.class public Lhud;
.super Lhuw;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhud$a;
    }
.end annotation


# static fields
.field public static final a:J

.field public static final b:J

.field public static c:Lhud;


# instance fields
.field public d:Lhud;

.field public e:J

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lhud;->a:J

    .line 59
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v2, Lhud;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lhud;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhuw;-><init>()V

    return-void
.end method

.method private static declared-synchronized a(Lhud;JZ)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 14
    const-class v1, Lhud;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhud;->c:Lhud;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lhud;

    invoke-direct {v0}, Lhud;-><init>()V

    sput-object v0, Lhud;->c:Lhud;

    .line 16
    new-instance v0, Lhud$a;

    invoke-direct {v0}, Lhud$a;-><init>()V

    invoke-virtual {v0}, Lhud$a;->start()V

    .line 17
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 18
    cmp-long v0, p1, v4

    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    .line 19
    invoke-virtual {p0}, Lhud;->c()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    add-long/2addr v4, v2

    iput-wide v4, p0, Lhud;->e:J

    .line 26
    :goto_0
    iget-wide v4, p0, Lhud;->e:J

    sub-long/2addr v4, v2

    .line 28
    sget-object v0, Lhud;->c:Lhud;

    .line 29
    :goto_1
    iget-object v6, v0, Lhud;->d:Lhud;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lhud;->d:Lhud;

    .line 30
    iget-wide v6, v6, Lhud;->e:J

    sub-long/2addr v6, v2

    .line 31
    cmp-long v6, v4, v6

    if-gez v6, :cond_6

    .line 32
    :cond_1
    iget-object v2, v0, Lhud;->d:Lhud;

    iput-object v2, p0, Lhud;->d:Lhud;

    .line 33
    iput-object p0, v0, Lhud;->d:Lhud;

    .line 34
    sget-object v2, Lhud;->c:Lhud;

    if-ne v0, v2, :cond_2

    .line 35
    const-class v0, Lhud;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :cond_2
    monitor-exit v1

    return-void

    .line 20
    :cond_3
    cmp-long v0, p1, v4

    if-eqz v0, :cond_4

    .line 21
    add-long v4, v2, p1

    :try_start_1
    iput-wide v4, p0, Lhud;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 22
    :cond_4
    if-eqz p3, :cond_5

    .line 23
    :try_start_2
    invoke-virtual {p0}, Lhud;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lhud;->e:J

    goto :goto_0

    .line 24
    :cond_5
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_6
    iget-object v0, v0, Lhud;->d:Lhud;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static declared-synchronized a(Lhud;)Z
    .locals 3

    .prologue
    .line 41
    const-class v1, Lhud;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhud;->c:Lhud;

    :goto_0
    if-eqz v0, :cond_1

    .line 42
    iget-object v2, v0, Lhud;->d:Lhud;

    if-ne v2, p0, :cond_0

    .line 43
    iget-object v2, p0, Lhud;->d:Lhud;

    iput-object v2, v0, Lhud;->d:Lhud;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lhud;->d:Lhud;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    const/4 v0, 0x0

    .line 47
    :goto_1
    monitor-exit v1

    return v0

    .line 46
    :cond_0
    :try_start_1
    iget-object v0, v0, Lhud;->d:Lhud;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-boolean v1, p0, Lhud;->f:Z

    if-nez v1, :cond_0

    .line 40
    :goto_0
    return v0

    .line 39
    :cond_0
    iput-boolean v0, p0, Lhud;->f:Z

    .line 40
    invoke-static {p0}, Lhud;->a(Lhud;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lhud;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lhud;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2
    iget-boolean v0, p0, Lhud;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalanced enter/exit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 9
    cmp-long v1, v2, v2

    if-nez v1, :cond_1

    .line 13
    :goto_0
    return-void

    .line 11
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhud;->f:Z

    .line 12
    invoke-static {p0, v2, v3, v0}, Lhud;->a(Lhud;JZ)V

    goto :goto_0
.end method

.method final a(Z)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lhud;->e()Z

    move-result v0

    .line 50
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhud;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 51
    :cond_0
    return-void
.end method

.method protected b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    .line 55
    if-eqz p1, :cond_0

    .line 56
    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 57
    :cond_0
    return-object v0
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method
