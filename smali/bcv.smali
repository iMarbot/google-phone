.class final Lbcv;
.super Landroid/support/v7/widget/RecyclerView$a;
.source "PG"


# instance fields
.field private c:Landroid/database/Cursor;

.field private d:Lbsr;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Landroid/database/Cursor;Lbsr;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$a;-><init>()V

    .line 2
    iput-object p1, p0, Lbcv;->c:Landroid/database/Cursor;

    .line 3
    iput-object p2, p0, Lbcv;->d:Lbsr;

    .line 4
    invoke-interface {p2}, Lbsr;->a()J

    move-result-wide v2

    .line 5
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 7
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 9
    invoke-static {v2, v3, v0, v1}, Lapw;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcv;->e:Ljava/lang/Integer;

    .line 11
    const/4 v0, 0x2

    .line 12
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 16
    invoke-static {v2, v3, v4, v5}, Lapw;->b(JJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcv;->f:Ljava/lang/Integer;

    .line 27
    :goto_1
    return-void

    .line 20
    :cond_1
    iput-object v6, p0, Lbcv;->f:Ljava/lang/Integer;

    goto :goto_1

    .line 22
    :cond_2
    iput-object v6, p0, Lbcv;->e:Ljava/lang/Integer;

    .line 23
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbcv;->f:Ljava/lang/Integer;

    goto :goto_1

    .line 25
    :cond_3
    iput-object v6, p0, Lbcv;->e:Ljava/lang/Integer;

    .line 26
    iput-object v6, p0, Lbcv;->f:Ljava/lang/Integer;

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Lbcv;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 99
    :cond_0
    iget-object v1, p0, Lbcv;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 100
    add-int/lit8 v0, v0, 0x1

    .line 101
    :cond_1
    iget-object v1, p0, Lbcv;->c:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lbcv;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbcv;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Lbcv;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbcv;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 94
    const/4 v0, 0x2

    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$r;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    packed-switch p2, :pswitch_data_0

    .line 37
    const/16 v0, 0x22

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Unsupported view type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 39
    throw v1

    .line 29
    :pswitch_0
    new-instance v0, Lbcu;

    .line 30
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04008d

    .line 31
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lbcu;-><init>(Landroid/view/View;)V

    .line 36
    :goto_0
    return-object v0

    .line 33
    :pswitch_1
    new-instance v0, Lbcy;

    .line 34
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04008b

    .line 35
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lbcv;->d:Lbsr;

    invoke-direct {v0, v1, v2}, Lbcy;-><init>(Landroid/view/View;Lbsr;)V

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView$r;I)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 40
    instance-of v0, p1, Lbcu;

    if-eqz v0, :cond_2

    .line 41
    check-cast p1, Lbcu;

    .line 42
    invoke-virtual {p0, p2}, Lbcv;->a(I)I

    move-result v0

    .line 43
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 44
    const v0, 0x7f110214

    invoke-virtual {p1, v0}, Lbcu;->c(I)V

    .line 90
    :goto_0
    return-void

    .line 45
    :cond_0
    if-ne v0, v8, :cond_1

    .line 46
    const v0, 0x7f110215

    invoke-virtual {p1, v0}, Lbcu;->c(I)V

    goto :goto_0

    .line 47
    :cond_1
    const/16 v1, 0x39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected view type "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 48
    :cond_2
    check-cast p1, Lbcy;

    .line 50
    iget-object v0, p0, Lbcv;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbcv;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le p2, v0, :cond_a

    move v0, v8

    .line 52
    :goto_1
    iget-object v1, p0, Lbcv;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbcv;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le p2, v1, :cond_3

    .line 53
    add-int/lit8 v0, v0, 0x1

    .line 54
    :cond_3
    iget-object v1, p0, Lbcv;->c:Landroid/database/Cursor;

    sub-int v0, p2, v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 55
    iget-object v0, p0, Lbcv;->c:Landroid/database/Cursor;

    .line 56
    invoke-static {v0}, Lbct;->a(Landroid/database/Cursor;)Lbcr;

    move-result-object v0

    .line 57
    iget-object v1, p1, Lbcy;->q:Landroid/widget/TextView;

    iget-object v2, p1, Lbcy;->p:Landroid/content/Context;

    invoke-static {v2, v0}, Lapw;->e(Landroid/content/Context;Lbcr;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v1, p1, Lbcy;->r:Landroid/widget/TextView;

    iget-object v2, p1, Lbcy;->p:Landroid/content/Context;

    iget-object v3, p1, Lbcy;->x:Lbsr;

    invoke-static {v2, v3, v0}, Lapw;->a(Landroid/content/Context;Lbsr;Lbcr;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-static {v0}, Lbcy;->a(Lbcr;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 60
    iget-object v1, p1, Lbcy;->q:Landroid/widget/TextView;

    const v2, 0x7f120206

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 61
    iget-object v1, p1, Lbcy;->r:Landroid/widget/TextView;

    const v2, 0x7f120207

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextAppearance(I)V

    .line 63
    :cond_4
    invoke-virtual {v0}, Lbcr;->u()I

    move-result v1

    if-le v1, v8, :cond_5

    .line 64
    iget-object v1, p1, Lbcy;->q:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, " (%d)"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lbcr;->u()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 66
    :cond_5
    iget-object v1, p1, Lbcy;->p:Landroid/content/Context;

    invoke-static {v1}, Lbfo;->a(Landroid/content/Context;)Lbfo;

    move-result-object v1

    iget-object v2, p1, Lbcy;->s:Landroid/widget/QuickContactBadge;

    .line 67
    invoke-virtual {v0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_7

    move-object v3, v6

    .line 68
    :goto_2
    invoke-virtual {v0}, Lbcr;->g()J

    move-result-wide v4

    .line 69
    invoke-virtual {v0}, Lbcr;->f()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_8

    .line 70
    :goto_3
    invoke-virtual {v0}, Lbcr;->d()Ljava/lang/String;

    move-result-object v7

    .line 71
    invoke-virtual/range {v1 .. v8}, Lbfo;->a(Landroid/widget/QuickContactBadge;Landroid/net/Uri;JLandroid/net/Uri;Ljava/lang/String;I)V

    .line 73
    iget-object v1, p1, Lbcy;->t:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    .line 74
    invoke-virtual {v0}, Lbcr;->q()I

    move-result v2

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    .line 75
    :goto_4
    invoke-virtual {v1, v8}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->b(Z)V

    .line 76
    iget-object v1, p1, Lbcy;->t:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    iget-object v2, p1, Lbcy;->p:Landroid/content/Context;

    .line 77
    invoke-virtual {v0}, Lbcr;->q()I

    move-result v3

    invoke-static {v2, v3}, Lbib;->a(Landroid/content/Context;I)Z

    move-result v2

    .line 78
    invoke-virtual {v1, v2}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->c(Z)V

    .line 80
    iget-object v1, p1, Lbcy;->u:Lcom/android/dialer/calllogutils/CallTypeIconsView;

    invoke-virtual {v0}, Lbcr;->t()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/dialer/calllogutils/CallTypeIconsView;->a(I)V

    .line 82
    invoke-virtual {v0}, Lbcr;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 83
    iget-object v1, p1, Lbcy;->v:Landroid/widget/TextView;

    invoke-virtual {v0}, Lbcr;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v1, p1, Lbcy;->v:Landroid/widget/TextView;

    invoke-virtual {v0}, Lbcr;->p()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v1, p1, Lbcy;->v:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    :cond_6
    iget-object v1, p1, Lbcy;->a:Landroid/view/View;

    new-instance v2, Lbcz;

    invoke-direct {v2, p1, v0}, Lbcz;-><init>(Lbcy;Lbcr;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v1, p1, Lbcy;->w:Landroid/widget/ImageView;

    iget-object v2, p1, Lbcy;->p:Landroid/content/Context;

    invoke-static {v2, v0}, Lapw;->b(Landroid/content/Context;Lbcr;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 67
    :cond_7
    invoke-virtual {v0}, Lbcr;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_2

    .line 69
    :cond_8
    invoke-virtual {v0}, Lbcr;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_3

    :cond_9
    move v8, v9

    .line 74
    goto :goto_4

    :cond_a
    move v0, v9

    goto/16 :goto_1
.end method
