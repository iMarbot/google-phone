.class public final Lcnt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcmi;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcmz;

.field public e:Lcnp;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcmi;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILandroid/net/Network;)V
    .locals 7

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcnt;->f:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcnt;->a:Lcmi;

    .line 4
    iput-object p3, p0, Lcnt;->b:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcnt;->c:Ljava/lang/String;

    .line 6
    new-instance v0, Lcmz;

    .line 7
    iget-object v2, p0, Lcnt;->a:Lcmi;

    move-object v1, p1

    move-object v3, p8

    move-object v4, p6

    move v5, p5

    move v6, p7

    .line 8
    invoke-direct/range {v0 .. v6}, Lcmz;-><init>(Landroid/content/Context;Lcmi;Landroid/net/Network;Ljava/lang/String;II)V

    iput-object v0, p0, Lcnt;->d:Lcmz;

    .line 9
    return-void
.end method

.method static a([Lcna;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, p0, v0

    .line 13
    if-eqz v1, :cond_0

    .line 14
    const/16 v1, 0x2c

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 16
    :cond_0
    iget-object v1, v4, Lcna;->b:Ljava/lang/String;

    .line 17
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    const/4 v1, 0x1

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcnp;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcnt;->e:Lcnp;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcnp;

    invoke-direct {v0, p0}, Lcnp;-><init>(Lcnt;)V

    iput-object v0, p0, Lcnt;->e:Lcnp;

    .line 23
    :cond_0
    iget-object v0, p0, Lcnt;->e:Lcnp;

    return-object v0
.end method
