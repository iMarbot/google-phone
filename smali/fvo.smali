.class final Lfvo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfnn;


# instance fields
.field private synthetic a:Lftc;

.field private synthetic b:Lfvn;


# direct methods
.method constructor <init>(Lfvn;Lftc;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfvo;->b:Lfvn;

    iput-object p2, p0, Lfvo;->a:Lftc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic onError(Lhfz;)V
    .locals 3

    .prologue
    .line 9
    check-cast p1, Lgng$a;

    .line 10
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed to create new call id: \n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logw(Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lfvo;->b:Lfvn;

    .line 12
    iget-object v0, v0, Lfvn;->a:Lfvm;

    .line 13
    invoke-virtual {v0}, Lfvm;->a()V

    .line 14
    iget-object v0, p0, Lfvo;->a:Lftc;

    invoke-virtual {v0}, Lftc;->release()V

    .line 15
    return-void
.end method

.method public final synthetic onSuccess(Lhfz;)V
    .locals 3

    .prologue
    .line 2
    check-cast p1, Lgng$a;

    .line 3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x23

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Successfully created new call id: \n"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfvh;->logv(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lfvo;->b:Lfvn;

    .line 5
    iget-object v0, v0, Lfvn;->a:Lfvm;

    .line 6
    iget-object v1, p1, Lgng$a;->resource:[Lgnj;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Lgnj;->hangoutId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfvm;->a(Ljava/lang/String;)V

    .line 7
    iget-object v0, p0, Lfvo;->a:Lftc;

    invoke-virtual {v0}, Lftc;->release()V

    .line 8
    return-void
.end method
