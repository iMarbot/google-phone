.class public final enum Lhaz$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhaz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final a:Lhby;

.field private static enum b:Lhaz$a;

.field private static enum c:Lhaz$a;

.field private static enum d:Lhaz$a;

.field private static synthetic f:[Lhaz$a;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lhaz$a;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2, v2}, Lhaz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$a;->b:Lhaz$a;

    .line 12
    new-instance v0, Lhaz$a;

    const-string v1, "CORD"

    invoke-direct {v0, v1, v3, v3}, Lhaz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$a;->c:Lhaz$a;

    .line 13
    new-instance v0, Lhaz$a;

    const-string v1, "STRING_PIECE"

    invoke-direct {v0, v1, v4, v4}, Lhaz$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lhaz$a;->d:Lhaz$a;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lhaz$a;

    sget-object v1, Lhaz$a;->b:Lhaz$a;

    aput-object v1, v0, v2

    sget-object v1, Lhaz$a;->c:Lhaz$a;

    aput-object v1, v0, v3

    sget-object v1, Lhaz$a;->d:Lhaz$a;

    aput-object v1, v0, v4

    sput-object v0, Lhaz$a;->f:[Lhaz$a;

    .line 15
    new-instance v0, Lhba;

    invoke-direct {v0}, Lhba;-><init>()V

    sput-object v0, Lhaz$a;->a:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9
    iput p3, p0, Lhaz$a;->e:I

    .line 10
    return-void
.end method

.method public static a(I)Lhaz$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 7
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lhaz$a;->b:Lhaz$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lhaz$a;->c:Lhaz$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lhaz$a;->d:Lhaz$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static values()[Lhaz$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhaz$a;->f:[Lhaz$a;

    invoke-virtual {v0}, [Lhaz$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhaz$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lhaz$a;->e:I

    return v0
.end method
