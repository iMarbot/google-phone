.class final Lfzh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private synthetic b:Lfza;


# direct methods
.method constructor <init>(Lfza;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfzh;->b:Lfza;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lfzh;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 3
    return-void
.end method


# virtual methods
.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 4
    iget-object v0, p0, Lfzh;->b:Lfza;

    .line 5
    iget-boolean v0, v0, Lfza;->e:Z

    .line 6
    if-nez v0, :cond_0

    .line 7
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    :cond_0
    :goto_0
    iget-object v0, p0, Lfzh;->b:Lfza;

    .line 13
    invoke-virtual {v0}, Lfza;->g()Z

    move-result v0

    .line 14
    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzh;->b:Lfza;

    iget-object v0, v0, Lfza;->i:Lfzp;

    .line 15
    iget-boolean v0, v0, Lfzp;->b:Z

    .line 16
    if-eqz v0, :cond_2

    .line 17
    iget-object v0, p0, Lfzh;->b:Lfza;

    .line 19
    iget-object v0, v0, Lfza;->f:Lgdc;

    .line 20
    iget-object v1, p0, Lfzh;->b:Lfza;

    .line 22
    iget-object v1, v1, Lfza;->a:Landroid/app/Application;

    .line 23
    iget-object v2, p0, Lfzh;->b:Lfza;

    .line 25
    iget-object v2, v2, Lfza;->b:Lgax;

    .line 26
    iget-object v3, p0, Lfzh;->b:Lfza;

    iget-object v3, v3, Lfza;->i:Lfzp;

    iget-object v4, p0, Lfzh;->b:Lfza;

    iget-object v4, v4, Lfza;->r:Lfzx;

    .line 28
    iget-boolean v4, v4, Lfzx;->f:Z

    .line 29
    invoke-static {v0, v1, v2, v3, v4}, Lfxo;->a(Lgdc;Landroid/app/Application;Lgax;Lfzp;Z)Lfxo;

    move-result-object v0

    iget-object v1, p0, Lfzh;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 30
    invoke-virtual {v0, v1}, Lfxo;->a(Ljava/lang/Thread$UncaughtExceptionHandler;)Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 31
    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 34
    :cond_1
    :goto_1
    return-void

    .line 10
    :catch_0
    move-exception v0

    const-string v0, "Primes"

    const-string v1, "Wait for initialization is interrupted"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 32
    :cond_2
    iget-object v0, p0, Lfzh;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lfzh;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
