.class public final Lfwe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Landroid/graphics/RectF;

.field public f:Landroid/graphics/RectF;

.field public g:I

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfwe;->e:Landroid/graphics/RectF;

    .line 3
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lfwe;->f:Landroid/graphics/RectF;

    .line 4
    return-void
.end method

.method private a(IIII)Lfwe;
    .locals 0

    .prologue
    .line 14
    iput p1, p0, Lfwe;->a:I

    .line 15
    iput p2, p0, Lfwe;->b:I

    .line 16
    iput p3, p0, Lfwe;->c:I

    .line 17
    iput p4, p0, Lfwe;->d:I

    .line 18
    return-object p0
.end method


# virtual methods
.method public final a()Lfwe;
    .locals 5

    .prologue
    .line 5
    new-instance v0, Lfwe;

    invoke-direct {v0}, Lfwe;-><init>()V

    .line 6
    iget v1, p0, Lfwe;->a:I

    iget v2, p0, Lfwe;->b:I

    iget v3, p0, Lfwe;->c:I

    iget v4, p0, Lfwe;->d:I

    invoke-direct {v0, v1, v2, v3, v4}, Lfwe;->a(IIII)Lfwe;

    .line 7
    iget-object v1, p0, Lfwe;->e:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lfwe;->a(Landroid/graphics/RectF;)Lfwe;

    .line 8
    iget-object v1, p0, Lfwe;->f:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lfwe;->b(Landroid/graphics/RectF;)Lfwe;

    .line 9
    iget v1, p0, Lfwe;->g:I

    .line 10
    iput v1, v0, Lfwe;->g:I

    .line 11
    iget-boolean v1, p0, Lfwe;->h:Z

    .line 12
    iput-boolean v1, v0, Lfwe;->h:Z

    .line 13
    return-object v0
.end method

.method public final a(II)Lfwe;
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p1, p2}, Lfwe;->a(IIII)Lfwe;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/RectF;)Lfwe;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    :cond_0
    iput-object p1, p0, Lfwe;->e:Landroid/graphics/RectF;

    .line 21
    return-object p0
.end method

.method public final b(Landroid/graphics/RectF;)Lfwe;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1, v0, v0, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    :cond_0
    iput-object p1, p0, Lfwe;->f:Landroid/graphics/RectF;

    .line 23
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    if-ne p1, p0, :cond_1

    .line 32
    :cond_0
    :goto_0
    return v0

    .line 26
    :cond_1
    instance-of v2, p1, Lfwe;

    if-nez v2, :cond_2

    move v0, v1

    .line 27
    goto :goto_0

    .line 28
    :cond_2
    check-cast p1, Lfwe;

    .line 29
    iget v2, p0, Lfwe;->a:I

    iget v3, p1, Lfwe;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lfwe;->b:I

    iget v3, p1, Lfwe;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lfwe;->c:I

    iget v3, p1, Lfwe;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lfwe;->d:I

    iget v3, p1, Lfwe;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lfwe;->e:Landroid/graphics/RectF;

    iget-object v3, p1, Lfwe;->e:Landroid/graphics/RectF;

    .line 30
    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfwe;->f:Landroid/graphics/RectF;

    iget-object v3, p1, Lfwe;->f:Landroid/graphics/RectF;

    .line 31
    invoke-virtual {v2, v3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lfwe;->g:I

    iget v3, p1, Lfwe;->g:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lfwe;->h:Z

    iget-boolean v3, p1, Lfwe;->h:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    .line 32
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 11

    .prologue
    const/16 v10, 0x78

    .line 33
    iget v0, p0, Lfwe;->a:I

    iget v1, p0, Lfwe;->b:I

    iget v2, p0, Lfwe;->c:I

    iget v3, p0, Lfwe;->d:I

    iget-object v4, p0, Lfwe;->e:Landroid/graphics/RectF;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lfwe;->f:Landroid/graphics/RectF;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lfwe;->g:I

    iget-boolean v7, p0, Lfwe;->h:Z

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit16 v8, v8, 0xa2

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "VideoFormatInfo:\n size: "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n bufferSize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n regionOfInterest: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n croppedRect: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n rotation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n screenshare: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
