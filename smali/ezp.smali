.class public abstract Lezp;
.super Ljava/lang/Object;


# instance fields
.field private a:Lezl;

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method public constructor <init>(Lezl;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lezp;->a:Lezl;

    iput-object p2, p0, Lezp;->b:Ljava/lang/String;

    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lezp;->c:J

    return-void
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract a(Leze;)V
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1
    :goto_0
    if-gtz p2, :cond_1

    const-string v2, "PhenotypeFlagCommitter"

    const-string v3, "No more attempts remaining, giving up for "

    iget-object v0, p0, Lezp;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 5
    :goto_2
    return v0

    .line 1
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lezp;->a:Lezl;

    iget-object v2, p0, Lezp;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lezp;->a()Ljava/lang/String;

    move-result-object v3

    .line 2
    new-instance v4, Lezs;

    invoke-direct {v4, v2, p1, v3}, Lezs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ledh;->a(Legq;)Lfat;

    move-result-object v2

    .line 3
    :try_start_0
    iget-wide v4, p0, Lezp;->c:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v4, v5, v0}, Lfmd;->a(Lfat;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_5

    invoke-virtual {v2}, Lfat;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leze;

    invoke-virtual {p0, v0}, Lezp;->a(Leze;)V

    iget-object v3, p0, Lezp;->a:Lezl;

    invoke-virtual {v2}, Lfat;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leze;

    iget-object v0, v0, Leze;->a:Ljava/lang/String;

    .line 4
    new-instance v2, Lezt;

    invoke-direct {v2, v0}, Lezt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ledh;->a(Legq;)Lfat;

    move-result-object v0

    .line 5
    :try_start_1
    iget-wide v2, p0, Lezp;->c:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v2, v3, v4}, Lfmd;->a(Lfat;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3

    const/4 v0, 0x1

    goto :goto_2

    .line 3
    :catch_0
    move-exception v0

    :goto_3
    const-string v2, "PhenotypeFlagCommitter"

    iget-object v3, p0, Lezp;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Retrieving snapshot for "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_2

    .line 5
    :catch_1
    move-exception v0

    :goto_4
    const-string v2, "PhenotypeFlagCommitter"

    iget-object v3, p0, Lezp;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Committing snapshot for "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed, retrying"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_4

    .line 3
    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_3
.end method
