.class public final Lesi;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:J

.field private b:Z

.field private c:Landroid/os/WorkSource;

.field private d:Ljava/lang/String;

.field private e:[I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lewx;

    invoke-direct {v0}, Lewx;-><init>()V

    sput-object v0, Lesi;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(JZLandroid/os/WorkSource;Ljava/lang/String;[IZLjava/lang/String;J)V
    .locals 1

    invoke-direct {p0}, Lepr;-><init>()V

    iput-wide p1, p0, Lesi;->a:J

    iput-boolean p3, p0, Lesi;->b:Z

    iput-object p4, p0, Lesi;->c:Landroid/os/WorkSource;

    iput-object p5, p0, Lesi;->d:Ljava/lang/String;

    iput-object p6, p0, Lesi;->e:[I

    iput-boolean p7, p0, Lesi;->f:Z

    iput-object p8, p0, Lesi;->g:Ljava/lang/String;

    iput-wide p9, p0, Lesi;->h:J

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    .line 2
    iget-wide v2, p0, Lesi;->a:J

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x2

    iget-boolean v2, p0, Lesi;->b:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lesi;->c:Landroid/os/WorkSource;

    invoke-static {p1, v1, v2, p2, v4}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x4

    iget-object v2, p0, Lesi;->d:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Lesi;->e:[I

    invoke-static {p1, v1, v2, v4}, Letf;->a(Landroid/os/Parcel;I[IZ)V

    const/4 v1, 0x6

    iget-boolean v2, p0, Lesi;->f:Z

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x7

    iget-object v2, p0, Lesi;->g:Ljava/lang/String;

    invoke-static {p1, v1, v2, v4}, Letf;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x8

    iget-wide v2, p0, Lesi;->h:J

    invoke-static {p1, v1, v2, v3}, Letf;->a(Landroid/os/Parcel;IJ)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
