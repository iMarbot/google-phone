.class public final Lcmi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public a:Lcnt;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/net/Network;

.field public final d:Lclu;

.field private e:Lcnq;

.field private f:Landroid/telecom/PhoneAccountHandle;

.field private g:Lcnw;

.field private h:Lcly;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    .locals 6

    .prologue
    .line 1
    new-instance v2, Lclu;

    invoke-direct {v2, p1, p2}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcmi;-><init>(Landroid/content/Context;Lclu;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lclu;Landroid/telecom/PhoneAccountHandle;Landroid/net/Network;Lcnw;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcmi;->b:Landroid/content/Context;

    .line 5
    iput-object p3, p0, Lcmi;->f:Landroid/telecom/PhoneAccountHandle;

    .line 6
    iput-object p4, p0, Lcmi;->c:Landroid/net/Network;

    .line 7
    iput-object p5, p0, Lcmi;->g:Lcnw;

    .line 8
    iput-object p2, p0, Lcmi;->d:Lclu;

    .line 9
    new-instance v0, Lcly;

    invoke-direct {v0, p1, p3}, Lcly;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    iput-object v0, p0, Lcmi;->h:Lcly;

    .line 11
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcnf;->a:Ljava/io/File;

    .line 12
    iget-object v0, p0, Lcmi;->h:Lcly;

    const-string v1, "u"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 13
    iget-object v0, p0, Lcmi;->h:Lcly;

    const-string v1, "pw"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 14
    iget-object v0, p0, Lcmi;->h:Lcly;

    const-string v1, "srv"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 15
    iget-object v0, p0, Lcmi;->h:Lcly;

    const-string v1, "ipt"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcly;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 17
    iget-object v1, p0, Lcmi;->d:Lclu;

    invoke-virtual {v1}, Lclu;->i()I

    move-result v5

    .line 18
    if-eqz v5, :cond_1

    .line 20
    const/4 v7, 0x1

    .line 21
    :goto_0
    new-instance v0, Lcnt;

    move-object v1, p1

    move-object v2, p0

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcnt;-><init>(Landroid/content/Context;Lcmi;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILandroid/net/Network;)V

    iput-object v0, p0, Lcmi;->a:Lcnt;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    return-void

    .line 23
    :catch_0
    move-exception v0

    .line 24
    sget-object v1, Lclt;->j:Lclt;

    invoke-virtual {p0, v1}, Lcmi;->a(Lclt;)V

    .line 25
    const-string v1, "ImapHelper"

    const-string v2, "Could not parse port number"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcop;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    new-instance v1, Lcmj;

    const-string v2, "cannot initialize ImapHelper:"

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Lcmj;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v7, v9

    move v5, v0

    goto :goto_0
.end method

.method private final a(Lcna;)Lcmm;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 122
    const-string v1, "ImapHelper"

    const-string v2, "Fetching message structure for "

    .line 123
    iget-object v0, p1, Lcna;->b:Ljava/lang/String;

    .line 124
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    new-instance v0, Lcml;

    invoke-direct {v0, p0}, Lcml;-><init>(Lcmi;)V

    .line 126
    new-instance v1, Lcmv;

    invoke-direct {v1}, Lcmv;-><init>()V

    .line 127
    const/4 v2, 0x3

    new-array v2, v2, [Lcmw;

    sget-object v3, Lcmw;->a:Lcmw;

    aput-object v3, v2, v5

    sget-object v3, Lcmw;->b:Lcmw;

    aput-object v3, v2, v6

    const/4 v3, 0x2

    sget-object v4, Lcmw;->c:Lcmw;

    aput-object v4, v2, v3

    .line 128
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 129
    invoke-virtual {v1, v2}, Lcmv;->addAll(Ljava/util/Collection;)Z

    .line 130
    iget-object v2, p0, Lcmi;->e:Lcnq;

    new-array v3, v6, [Lcna;

    aput-object p1, v3, v5

    invoke-virtual {v2, v3, v1, v0}, Lcnq;->a([Lcna;Lcmv;Lcnr;)V

    .line 132
    iget-object v0, v0, Lcml;->a:Lcmm;

    .line 133
    return-object v0

    .line 124
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static a(Lcms;)[B
    .locals 4

    .prologue
    .line 319
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 320
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 321
    :try_start_0
    invoke-interface {p0, v2}, Lcms;->a(Ljava/io/OutputStream;)V

    .line 322
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode([BI)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 323
    invoke-static {v2}, Lhuz;->a(Ljava/io/OutputStream;)V

    .line 324
    invoke-static {v1}, Lhuz;->a(Ljava/io/OutputStream;)V

    .line 325
    return-object v0

    .line 326
    :catchall_0
    move-exception v0

    invoke-static {v2}, Lhuz;->a(Ljava/io/OutputStream;)V

    .line 327
    invoke-static {v1}, Lhuz;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method private final b(Ljava/lang/String;)Lcnq;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 289
    :try_start_0
    iget-object v1, p0, Lcmi;->a:Lcnt;

    if-nez v1, :cond_0

    .line 315
    :goto_0
    return-object v0

    .line 291
    :cond_0
    new-instance v1, Lcnq;

    iget-object v2, p0, Lcmi;->a:Lcnt;

    const-string v3, "INBOX"

    invoke-direct {v1, v2, v3}, Lcnq;-><init>(Lcnt;Ljava/lang/String;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_1

    .line 293
    :try_start_1
    invoke-virtual {v1}, Lcnq;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 294
    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "Duplicated open on ImapFolder"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_1
    .catch Lcmr; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_2

    .line 304
    :catch_0
    move-exception v2

    .line 305
    const/4 v3, 0x0

    :try_start_2
    iput-object v3, v1, Lcnq;->b:Lcnp;

    .line 306
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcnq;->a(Z)V

    .line 307
    throw v2
    :try_end_2
    .catch Lcnb; {:try_start_2 .. :try_end_2} :catch_1

    .line 313
    :catch_1
    move-exception v1

    .line 314
    const-string v2, "ImapHelper"

    const-string v3, "Messaging Exception"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 295
    :cond_1
    :try_start_3
    monitor-enter v1
    :try_end_3
    .catch Lcmr; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcnb; {:try_start_3 .. :try_end_3} :catch_2

    .line 296
    :try_start_4
    iget-object v2, v1, Lcnq;->a:Lcnt;

    invoke-virtual {v2}, Lcnt;->a()Lcnp;

    move-result-object v2

    iput-object v2, v1, Lcnq;->b:Lcnp;

    .line 297
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 298
    :try_start_5
    invoke-virtual {v1}, Lcnq;->c()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 299
    :try_start_6
    invoke-virtual {v1}, Lcnq;->a()V
    :try_end_6
    .catch Lcmr; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcnb; {:try_start_6 .. :try_end_6} :catch_2

    move-object v0, v1

    .line 312
    goto :goto_0

    .line 297
    :catchall_0
    move-exception v2

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v2
    :try_end_8
    .catch Lcmr; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcnb; {:try_start_8 .. :try_end_8} :catch_2

    .line 308
    :catch_2
    move-exception v2

    .line 309
    const/4 v3, 0x0

    :try_start_9
    iput-boolean v3, v1, Lcnq;->c:Z

    .line 310
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcnq;->a(Z)V

    .line 311
    throw v2
    :try_end_9
    .catch Lcnb; {:try_start_9 .. :try_end_9} :catch_1

    .line 301
    :catch_3
    move-exception v2

    .line 302
    :try_start_a
    iget-object v3, v1, Lcnq;->b:Lcnp;

    invoke-virtual {v1, v3, v2}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v2

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 303
    :catchall_1
    move-exception v2

    :try_start_b
    invoke-virtual {v1}, Lcnq;->a()V

    throw v2
    :try_end_b
    .catch Lcmr; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcnb; {:try_start_b .. :try_end_b} :catch_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 192
    iget-object v4, p0, Lcmi;->a:Lcnt;

    invoke-virtual {v4}, Lcnt;->a()Lcnp;

    move-result-object v4

    .line 195
    :try_start_0
    iget-object v5, p0, Lcmi;->d:Lclu;

    .line 197
    iget-object v5, v5, Lclu;->d:Lcou;

    .line 198
    const-string v6, "XCHANGE_TUI_PWD PWD=%1$s OLD_PWD=%2$s"

    invoke-virtual {v5, v6}, Lcou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 199
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    invoke-static {v6, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcnp;->b(Ljava/lang/String;Z)Ljava/lang/String;

    .line 200
    invoke-virtual {v4}, Lcnp;->c()Lcoh;

    move-result-object v5

    .line 201
    invoke-virtual {v5}, Lcoh;->f()Z

    move-result v6

    if-nez v6, :cond_0

    .line 202
    new-instance v0, Lcnb;

    const/16 v1, 0x13

    const-string v3, "tagged response expected"

    invoke-direct {v0, v1, v3}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    :try_start_1
    const-string v1, "ImapHelper"

    const-string v3, "changePin: "

    invoke-static {v1, v3, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    invoke-virtual {v4}, Lcnp;->b()V

    move v0, v2

    .line 228
    :goto_0
    return v0

    .line 204
    :cond_0
    :try_start_2
    const-string v6, "OK"

    .line 205
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v6, v8}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v6

    .line 206
    if-nez v6, :cond_7

    .line 207
    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Lcoh;->c(I)Lcol;

    move-result-object v3

    invoke-virtual {v3}, Lcol;->e()Ljava/lang/String;

    move-result-object v5

    .line 208
    const-string v6, "ImapHelper"

    const-string v7, "change PIN failed: "

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v3, v7}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    const-string v3, "password too short"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 223
    :goto_2
    invoke-virtual {v4}, Lcnp;->b()V

    goto :goto_0

    .line 208
    :cond_1
    :try_start_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 229
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcnp;->b()V

    throw v0

    .line 211
    :cond_2
    :try_start_4
    const-string v0, "password too long"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 212
    goto :goto_2

    .line 213
    :cond_3
    const-string v0, "password too weak"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214
    const/4 v0, 0x3

    goto :goto_2

    .line 215
    :cond_4
    const-string v0, "old password mismatch"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 216
    const/4 v0, 0x4

    goto :goto_2

    .line 217
    :cond_5
    const-string v0, "password contains invalid characters"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 218
    const/4 v0, 0x5

    goto :goto_2

    :cond_6
    move v0, v2

    .line 219
    goto :goto_2

    .line 220
    :cond_7
    const-string v0, "ImapHelper"

    const-string v1, "change PIN succeeded"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v3

    .line 221
    goto :goto_2
.end method

.method public final a()Ljava/util/List;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 60
    :try_start_0
    const-string v3, "mode_read_write"

    invoke-direct {p0, v3}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v3

    iput-object v3, p0, Lcmi;->e:Lcnq;

    .line 61
    iget-object v3, p0, Lcmi;->e:Lcnq;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 62
    invoke-virtual {p0}, Lcmi;->e()V

    move-object v0, v1

    .line 120
    :goto_0
    return-object v0

    .line 64
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcmi;->e:Lcnq;

    .line 65
    const-string v4, "1:* NOT DELETED"

    invoke-virtual {v3, v4}, Lcnq;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 66
    invoke-virtual {v3, v4}, Lcnq;->a([Ljava/lang/String;)[Lcna;

    move-result-object v5

    .line 68
    array-length v6, v5

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_7

    aget-object v0, v5, v3

    .line 69
    invoke-direct {p0, v0}, Lcmi;->a(Lcna;)Lcmm;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_5

    .line 72
    iget-object v7, v0, Lcmm;->a:Lcna;

    .line 73
    new-instance v8, Lcmn;

    .line 74
    invoke-direct {v8, p0}, Lcmn;-><init>(Lcmi;)V

    .line 76
    iget-object v4, v0, Lcmm;->b:Lcmt;

    if-eqz v4, :cond_1

    .line 77
    new-instance v4, Lcmv;

    invoke-direct {v4}, Lcmv;-><init>()V

    .line 78
    iget-object v0, v0, Lcmm;->b:Lcmt;

    invoke-virtual {v4, v0}, Lcmv;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcmi;->e:Lcnq;

    const/4 v9, 0x1

    new-array v9, v9, [Lcna;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-virtual {v0, v9, v4, v8}, Lcnq;->a([Lcna;Lcmv;Lcnr;)V

    .line 80
    :cond_1
    invoke-virtual {v7}, Lcna;->a()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    .line 81
    invoke-virtual {v7}, Lcna;->c()[Lcmp;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_6

    array-length v4, v0

    if-lez v4, :cond_6

    .line 83
    array-length v4, v0

    if-eq v4, v13, :cond_2

    .line 84
    const-string v4, "ImapHelper"

    const-string v9, "More than one from addresses found. Using the first one."

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v4, v9, v12}, Lcop;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    :cond_2
    const/4 v4, 0x0

    aget-object v0, v0, v4

    .line 86
    iget-object v0, v0, Lcmp;->a:Ljava/lang/String;

    .line 88
    const/16 v4, 0x40

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 89
    const/4 v9, -0x1

    if-eq v4, v9, :cond_3

    .line 90
    const/4 v9, 0x0

    invoke-virtual {v0, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v4, v0

    .line 95
    :goto_2
    invoke-virtual {v7}, Lcna;->d()Ljava/util/HashSet;

    move-result-object v0

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 96
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v9, "seen"

    invoke-interface {v0, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 97
    invoke-virtual {v7}, Lcna;->b()Ljava/lang/Long;

    move-result-object v9

    .line 99
    invoke-static {v10, v11, v4}, Lclz;->a(JLjava/lang/String;)Lcov;

    move-result-object v4

    iget-object v10, p0, Lcmi;->f:Landroid/telecom/PhoneAccountHandle;

    .line 100
    invoke-virtual {v4, v10}, Lcov;->a(Landroid/telecom/PhoneAccountHandle;)Lcov;

    move-result-object v4

    iget-object v10, p0, Lcmi;->b:Landroid/content/Context;

    .line 101
    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Lcov;->b(Ljava/lang/String;)Lcov;

    move-result-object v4

    .line 103
    iget-object v7, v7, Lcna;->b:Ljava/lang/String;

    .line 104
    invoke-virtual {v4, v7}, Lcov;->c(Ljava/lang/String;)Lcov;

    move-result-object v4

    .line 105
    invoke-virtual {v4, v0}, Lcov;->a(Z)Lcov;

    move-result-object v0

    .line 107
    iget-object v4, v8, Lcmn;->a:Ljava/lang/String;

    .line 108
    invoke-virtual {v0, v4}, Lcov;->d(Ljava/lang/String;)Lcov;

    move-result-object v0

    .line 109
    if-eqz v9, :cond_4

    .line 110
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcov;->c(J)Lcov;

    .line 111
    :cond_4
    invoke-virtual {v0}, Lcov;->a()Lclz;

    move-result-object v0

    .line 112
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_6
    move-object v4, v1

    .line 92
    goto :goto_2

    .line 115
    :cond_7
    invoke-virtual {p0}, Lcmi;->e()V

    move-object v0, v2

    .line 116
    goto/16 :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    :try_start_2
    const-string v2, "ImapHelper"

    const-string v3, "Messaging Exception"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 119
    invoke-virtual {p0}, Lcmi;->e()V

    move-object v0, v1

    .line 120
    goto/16 :goto_0

    .line 121
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method public final a(Lclt;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcmi;->d:Lclu;

    iget-object v1, p0, Lcmi;->g:Lcnw;

    invoke-virtual {v0, v1, p1}, Lclu;->a(Lcnw;Lclt;)V

    .line 34
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 230
    iget-object v0, p0, Lcmi;->a:Lcnt;

    invoke-virtual {v0}, Lcnt;->a()Lcnp;

    move-result-object v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcmi;->d:Lclu;

    .line 235
    iget-object v0, v0, Lclu;->d:Lcou;

    .line 236
    const-string v2, "XCHANGE_VM_LANG LANG=%1$s"

    invoke-virtual {v0, v2}, Lcou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcnp;->b(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    invoke-virtual {v1}, Lcnp;->b()V

    .line 243
    :goto_0
    return-void

    .line 240
    :catch_0
    move-exception v0

    .line 241
    :try_start_1
    const-string v2, "ImapHelper"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcop;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    invoke-virtual {v1}, Lcnp;->b()V

    goto :goto_0

    .line 244
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcnp;->b()V

    throw v0
.end method

.method public final a(Lcow;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 134
    :try_start_0
    const-string v2, "mode_read_write"

    invoke-direct {p0, v2}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v2

    iput-object v2, p0, Lcmi;->e:Lcnq;

    .line 135
    iget-object v2, p0, Lcmi;->e:Lcnq;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 136
    invoke-virtual {p0}, Lcmi;->e()V

    .line 162
    :goto_0
    return v0

    .line 138
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcmi;->e:Lcnq;

    invoke-virtual {v2, p2}, Lcnq;->b(Ljava/lang/String;)Lcna;
    :try_end_1
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 139
    if-nez v3, :cond_1

    .line 140
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 143
    :cond_1
    :try_start_2
    const-string v4, "ImapHelper"

    const-string v5, "Fetching message body for "

    .line 144
    iget-object v2, v3, Lcna;->b:Ljava/lang/String;

    .line 145
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v2, v5}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    new-instance v2, Lcmk;

    .line 147
    invoke-direct {v2, p0}, Lcmk;-><init>(Lcmi;)V

    .line 149
    new-instance v4, Lcmv;

    invoke-direct {v4}, Lcmv;-><init>()V

    .line 150
    sget-object v5, Lcmw;->e:Lcmw;

    invoke-virtual {v4, v5}, Lcmv;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v5, p0, Lcmi;->e:Lcnq;

    const/4 v6, 0x1

    new-array v6, v6, [Lcna;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v5, v6, v4, v2}, Lcnq;->a([Lcna;Lcmv;Lcnr;)V

    .line 153
    iget-object v2, v2, Lcmk;->a:Lcmo;

    .line 155
    invoke-virtual {p1, v2}, Lcow;->a(Lcmo;)V
    :try_end_2
    .catch Lcnb; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    invoke-virtual {p0}, Lcmi;->e()V

    move v0, v1

    .line 157
    goto :goto_0

    .line 145
    :cond_2
    :try_start_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lcnb; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 159
    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method public final a(Lcqc;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 163
    :try_start_0
    const-string v2, "mode_read_write"

    invoke-direct {p0, v2}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v2

    iput-object v2, p0, Lcmi;->e:Lcnq;

    .line 164
    iget-object v2, p0, Lcmi;->e:Lcnq;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 165
    invoke-virtual {p0}, Lcmi;->e()V

    .line 190
    :goto_0
    return v0

    .line 167
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcmi;->e:Lcnq;

    invoke-virtual {v2, p2}, Lcnq;->b(Ljava/lang/String;)Lcna;
    :try_end_1
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 168
    if-nez v2, :cond_1

    .line 169
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 171
    :cond_1
    :try_start_2
    invoke-direct {p0, v2}, Lcmi;->a(Lcna;)Lcmm;

    move-result-object v3

    .line 172
    if-eqz v3, :cond_2

    .line 173
    new-instance v4, Lcmn;

    .line 174
    invoke-direct {v4, p0}, Lcmn;-><init>(Lcmi;)V

    .line 176
    iget-object v5, v3, Lcmm;->b:Lcmt;

    if-eqz v5, :cond_2

    .line 177
    new-instance v5, Lcmv;

    invoke-direct {v5}, Lcmv;-><init>()V

    .line 178
    iget-object v3, v3, Lcmm;->b:Lcmt;

    invoke-virtual {v5, v3}, Lcmv;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v3, p0, Lcmi;->e:Lcnq;

    const/4 v6, 0x1

    new-array v6, v6, [Lcna;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v3, v6, v5, v4}, Lcnq;->a([Lcna;Lcmv;Lcnr;)V

    .line 181
    iget-object v2, v4, Lcmn;->a:Ljava/lang/String;

    .line 183
    new-instance v3, Lcqd;

    iget-object v4, p1, Lcqc;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcqd;-><init>(Landroid/content/Context;)V

    .line 184
    iget-object v4, p1, Lcqc;->b:Lclz;

    invoke-virtual {v3, v4, v2}, Lcqd;->a(Lclz;Ljava/lang/String;)V
    :try_end_2
    .catch Lcnb; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 185
    :cond_2
    invoke-virtual {p0}, Lcmi;->e()V

    move v0, v1

    .line 186
    goto :goto_0

    .line 187
    :catch_0
    move-exception v1

    .line 188
    :try_start_3
    const-string v2, "ImapHelper"

    const-string v3, "Messaging Exception"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method public final a(Ljava/util/List;)Z
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "deleted"

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Lcmi;->a(Ljava/util/List;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final varargs a(Ljava/util/List;[Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 35
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 57
    :goto_0
    return v0

    .line 37
    :cond_0
    :try_start_0
    const-string v0, "mode_read_write"

    invoke-direct {p0, v0}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v0

    iput-object v0, p0, Lcmi;->e:Lcnq;

    .line 38
    iget-object v0, p0, Lcmi;->e:Lcnq;

    if-eqz v0, :cond_2

    .line 39
    iget-object v4, p0, Lcmi;->e:Lcnq;

    .line 40
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Lcna;

    move v3, v1

    .line 41
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 42
    new-instance v0, Lcnl;

    invoke-direct {v0}, Lcnl;-><init>()V

    aput-object v0, v5, v3

    .line 43
    aget-object v6, v5, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclz;

    .line 44
    iget-object v0, v0, Lclz;->g:Ljava/lang/String;

    .line 46
    iput-object v0, v6, Lcna;->b:Ljava/lang/String;

    .line 47
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 49
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {v4, v5, p2, v0}, Lcnq;->a([Lcna;[Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    invoke-virtual {p0}, Lcmi;->e()V

    move v0, v2

    .line 51
    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {p0}, Lcmi;->e()V

    move v0, v1

    .line 53
    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    :try_start_1
    const-string v2, "ImapHelper"

    const-string v3, "Messaging exception"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    invoke-virtual {p0}, Lcmi;->e()V

    move v0, v1

    .line 57
    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lcmi;->a:Lcnt;

    invoke-virtual {v0}, Lcnt;->a()Lcnp;

    move-result-object v1

    .line 247
    :try_start_0
    iget-object v0, p0, Lcmi;->d:Lclu;

    .line 249
    iget-object v0, v0, Lclu;->d:Lcou;

    .line 250
    const-string v2, "XCLOSE_NUT"

    invoke-virtual {v0, v2}, Lcou;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 251
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    invoke-virtual {v1}, Lcnp;->b()V

    .line 253
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    :try_start_1
    new-instance v2, Lcnb;

    const/16 v3, 0x13

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcnb;-><init>(ILjava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcnp;->b()V

    throw v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 257
    :try_start_0
    const-string v0, "mode_read_write"

    invoke-direct {p0, v0}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v0

    iput-object v0, p0, Lcmi;->e:Lcnq;

    .line 258
    iget-object v0, p0, Lcmi;->e:Lcnq;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 259
    invoke-virtual {p0}, Lcmi;->e()V

    .line 274
    :goto_0
    return-void

    .line 261
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcmi;->e:Lcnq;

    .line 262
    invoke-virtual {v0}, Lcnq;->d()Lcns;

    move-result-object v0

    .line 263
    if-nez v0, :cond_1

    .line 264
    const-string v0, "ImapHelper"

    const-string v1, "quota was null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    :goto_1
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 266
    :cond_1
    :try_start_2
    const-string v1, "ImapHelper"

    iget v2, v0, Lcns;->a:I

    iget v3, v0, Lcns;->b:I

    const/16 v4, 0x5c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Updating Voicemail status table with quota occupied: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " new quota total:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcop;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    iget-object v1, p0, Lcmi;->b:Landroid/content/Context;

    iget-object v2, p0, Lcmi;->f:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v1, v2}, Lcov;->a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lcnw;

    move-result-object v1

    iget v2, v0, Lcns;->a:I

    iget v0, v0, Lcns;->b:I

    invoke-virtual {v1, v2, v0}, Lcnw;->a(II)Lcnw;

    move-result-object v0

    invoke-virtual {v0}, Lcnw;->a()Z

    .line 268
    const-string v0, "ImapHelper"

    const-string v1, "Updated quota occupied and total"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcop;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lcnb; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 271
    :catch_0
    move-exception v0

    .line 272
    :try_start_3
    const-string v1, "ImapHelper"

    const-string v2, "Messaging Exception"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 273
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcmi;->a:Lcnt;

    .line 28
    iget-object v1, v0, Lcnt;->e:Lcnp;

    if-eqz v1, :cond_0

    .line 29
    iget-object v1, v0, Lcnt;->e:Lcnp;

    invoke-virtual {v1}, Lcnp;->a()V

    .line 30
    const/4 v1, 0x0

    iput-object v1, v0, Lcnt;->e:Lcnp;

    .line 31
    :cond_0
    return-void
.end method

.method public final d()Lcns;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 276
    :try_start_0
    const-string v1, "mode_read_only"

    invoke-direct {p0, v1}, Lcmi;->b(Ljava/lang/String;)Lcnq;

    move-result-object v1

    iput-object v1, p0, Lcmi;->e:Lcnq;

    .line 277
    iget-object v1, p0, Lcmi;->e:Lcnq;

    if-nez v1, :cond_0

    .line 278
    const-string v1, "ImapHelper"

    const-string v2, "Unable to open folder"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcop;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    invoke-virtual {p0}, Lcmi;->e()V

    .line 287
    :goto_0
    return-object v0

    .line 281
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcmi;->e:Lcnq;

    invoke-virtual {v1}, Lcnq;->d()Lcns;
    :try_end_1
    .catch Lcnb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 282
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 284
    :catch_0
    move-exception v1

    .line 285
    :try_start_2
    const-string v2, "ImapHelper"

    const-string v3, "Messaging Exception"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Lcop;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 286
    invoke-virtual {p0}, Lcmi;->e()V

    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcmi;->e()V

    throw v0
.end method

.method final e()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcmi;->e:Lcnq;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcmi;->e:Lcnq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcnq;->a(Z)V

    .line 318
    :cond_0
    return-void
.end method
