.class public final Lfwk;
.super Lfvt;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lfvt;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lfwk;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final onAuthError()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 74
    invoke-virtual {v0}, Lfvt;->onAuthError()V

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method public final onAuthUserActionRequired(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 70
    invoke-virtual {v0, p1}, Lfvt;->onAuthUserActionRequired(Landroid/content/Intent;)V

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public final onCallEnd(I)V
    .locals 1

    .prologue
    .line 11
    const-string v0, "Only use onCallEnd(EndCauseInfo) when forwarding."

    invoke-static {v0}, Lfmw;->a(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public final onCallEnd(Lfvx;)V
    .locals 2

    .prologue
    .line 13
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 14
    invoke-virtual {v0, p1}, Lfvt;->onCallEnd(Lfvx;)V

    goto :goto_0

    .line 16
    :cond_0
    return-void
.end method

.method public final onCallJoin(Lfvy;)V
    .locals 2

    .prologue
    .line 3
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 4
    invoke-virtual {v0, p1}, Lfvt;->onCallJoin(Lfvy;)V

    goto :goto_0

    .line 6
    :cond_0
    return-void
.end method

.method public final onClientDataMessageReceived(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 86
    invoke-virtual {v0, p1, p2}, Lfvt;->onClientDataMessageReceived(Ljava/lang/String;[B)V

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method public final onCloudMediaSessionIdAvailable(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 18
    invoke-virtual {v0, p1}, Lfvt;->onCloudMediaSessionIdAvailable(Ljava/lang/String;)V

    goto :goto_0

    .line 20
    :cond_0
    return-void
.end method

.method public final onFirstAudioPacket()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 78
    invoke-virtual {v0}, Lfvt;->onFirstAudioPacket()V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public final onFocusedParticipantChanged(Lfvz;)V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 46
    invoke-virtual {v0, p1}, Lfvt;->onFocusedParticipantChanged(Lfvz;)V

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method public final onHangoutLogRequestPrepared(Lgsi;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 58
    invoke-virtual {v0, p1}, Lfvt;->onHangoutLogRequestPrepared(Lgsi;)V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method

.method public final onInitialCallStateSynchronized(Z)V
    .locals 2

    .prologue
    .line 7
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 8
    invoke-virtual {v0, p1}, Lfvt;->onInitialCallStateSynchronized(Z)V

    goto :goto_0

    .line 10
    :cond_0
    return-void
.end method

.method public final onLogDataPrepared(Lgpn;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 54
    invoke-virtual {v0, p1}, Lfvt;->onLogDataPrepared(Lgpn;)V

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method public final onMediaStats(Lgiv;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 62
    invoke-virtual {v0, p1}, Lfvt;->onMediaStats(Lgiv;)V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method public final onMeetingsPush(Lgqi;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 82
    invoke-virtual {v0, p1}, Lfvt;->onMeetingsPush(Lgqi;)V

    goto :goto_0

    .line 84
    :cond_0
    return-void
.end method

.method public final onParticipantAdded(Lfvz;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 22
    invoke-virtual {v0, p1}, Lfvt;->onParticipantAdded(Lfvz;)V

    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method public final onParticipantChanged(Lfvz;)V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 30
    invoke-virtual {v0, p1}, Lfvt;->onParticipantChanged(Lfvz;)V

    goto :goto_0

    .line 32
    :cond_0
    return-void
.end method

.method public final onParticipantRemoved(Lfvz;)V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 26
    invoke-virtual {v0, p1}, Lfvt;->onParticipantRemoved(Lfvz;)V

    goto :goto_0

    .line 28
    :cond_0
    return-void
.end method

.method public final onPendingParticipantAdded(Lfvz;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 34
    invoke-virtual {v0, p1}, Lfvt;->onPendingParticipantAdded(Lfvz;)V

    goto :goto_0

    .line 36
    :cond_0
    return-void
.end method

.method public final onPendingParticipantChanged(Lfvz;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 42
    invoke-virtual {v0, p1}, Lfvt;->onPendingParticipantChanged(Lfvz;)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public final onPendingParticipantRemoved(Lfvz;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 38
    invoke-virtual {v0, p1}, Lfvt;->onPendingParticipantRemoved(Lfvz;)V

    goto :goto_0

    .line 40
    :cond_0
    return-void
.end method

.method public final onQualityNotification(Lfwb;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 50
    invoke-virtual {v0, p1}, Lfvt;->onQualityNotification(Lfwb;)V

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public final onVolumeLevelUpdate(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lfwk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvt;

    .line 66
    invoke-virtual {v0, p1, p2}, Lfvt;->onVolumeLevelUpdate(ILjava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method
