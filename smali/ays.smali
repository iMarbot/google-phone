.class final Lays;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private synthetic a:Layq;


# direct methods
.method constructor <init>(Layq;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lays;->a:Layq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v10, :cond_2

    .line 3
    iget-object v0, p0, Lays;->a:Layq;

    .line 4
    iget-object v0, v0, Layq;->n:Lazn;

    .line 5
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 6
    iget v3, v0, Lazn;->h:I

    if-ne v3, v1, :cond_0

    iget v3, v0, Lazn;->i:I

    if-eq v3, v2, :cond_1

    .line 7
    :cond_0
    iput v1, v0, Lazn;->h:I

    .line 8
    iput v2, v0, Lazn;->i:I

    .line 9
    invoke-virtual {v0}, Lazn;->a()V

    .line 10
    :cond_1
    iget-object v0, p0, Lays;->a:Layq;

    .line 11
    iget-object v0, v0, Layq;->n:Lazn;

    .line 13
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int v4, v1, v2

    .line 14
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    add-int v5, v1, v2

    .line 16
    iget-boolean v1, v0, Lazn;->b:Z

    if-eqz v1, :cond_2

    iget v1, v0, Lazn;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 61
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 62
    return v10

    .line 18
    :cond_3
    iget-object v1, v0, Lazn;->k:Ljava/util/List;

    if-eqz v1, :cond_5

    iget v1, v0, Lazn;->a:I

    if-eq v1, v10, :cond_4

    iget v1, v0, Lazn;->a:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    iget v1, v0, Lazn;->a:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    .line 19
    :cond_4
    invoke-virtual {v0}, Lazn;->c()V

    .line 20
    :cond_5
    iget-object v1, v0, Lazn;->g:Lazr;

    .line 21
    iget v1, v1, Lazr;->d:I

    mul-int/lit8 v1, v1, 0x2

    .line 23
    iget-object v2, v0, Lazn;->g:Lazr;

    .line 24
    iget v2, v2, Lazr;->d:I

    mul-int/lit8 v2, v2, 0x2

    .line 26
    if-eqz v1, :cond_2

    iget-object v3, v0, Lazn;->g:Lazr;

    invoke-virtual {v3}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->f()I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lazn;->g:Lazr;

    invoke-virtual {v3}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay$b;->g()I

    move-result v3

    if-eqz v3, :cond_2

    .line 28
    iget v6, v0, Lazn;->h:I

    .line 29
    iget v7, v0, Lazn;->i:I

    .line 30
    iget-boolean v3, v0, Lazn;->c:Z

    if-eqz v3, :cond_7

    .line 32
    iget-object v3, v0, Lazn;->k:Ljava/util/List;

    if-nez v3, :cond_6

    .line 33
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lazn;->k:Ljava/util/List;

    .line 34
    iget-object v3, v0, Lazn;->k:Ljava/util/List;

    new-instance v8, Landroid/hardware/Camera$Area;

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {v8, v9, v10}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_6
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v8, v0, Lazn;->k:Ljava/util/List;

    .line 36
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Area;

    iget-object v8, v8, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    .line 37
    invoke-virtual/range {v0 .. v8}, Lazn;->a(IIFIIIILandroid/graphics/Rect;)V

    .line 38
    :cond_7
    iget-boolean v3, v0, Lazn;->d:Z

    if-eqz v3, :cond_9

    .line 40
    iget-object v3, v0, Lazn;->l:Ljava/util/List;

    if-nez v3, :cond_8

    .line 41
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lazn;->l:Ljava/util/List;

    .line 42
    iget-object v3, v0, Lazn;->l:Ljava/util/List;

    new-instance v8, Landroid/hardware/Camera$Area;

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {v8, v9, v10}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_8
    const/high16 v3, 0x3fc00000    # 1.5f

    iget-object v8, v0, Lazn;->l:Ljava/util/List;

    .line 44
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Area;

    iget-object v8, v8, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;

    .line 45
    invoke-virtual/range {v0 .. v8}, Lazn;->a(IIFIIIILandroid/graphics/Rect;)V

    .line 46
    :cond_9
    iget-object v1, v0, Lazn;->g:Lazr;

    .line 47
    iput v4, v1, Lazr;->e:I

    .line 48
    iput v5, v1, Lazr;->f:I

    .line 49
    iget v2, v1, Lazr;->e:I

    iget v3, v1, Lazr;->f:I

    invoke-virtual {v1, v2, v3}, Lazr;->a(II)V

    .line 50
    iget-object v1, v0, Lazn;->p:Lazo;

    invoke-interface {v1}, Lazo;->g()V

    .line 51
    iget-boolean v1, v0, Lazn;->c:Z

    if-eqz v1, :cond_a

    .line 53
    iget-object v1, v0, Lazn;->p:Lazo;

    invoke-interface {v1}, Lazo;->e()V

    .line 54
    iput v10, v0, Lazn;->a:I

    .line 55
    invoke-virtual {v0}, Lazn;->d()V

    .line 56
    iget-object v0, v0, Lazn;->o:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 58
    :cond_a
    invoke-virtual {v0}, Lazn;->d()V

    .line 59
    iget-object v1, v0, Lazn;->o:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 60
    iget-object v0, v0, Lazn;->o:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v11, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method
