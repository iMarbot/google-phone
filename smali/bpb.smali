.class public final Lbpb;
.super Landroid/database/MergeCursor;
.source "PG"

# interfaces
.implements Lboc;


# static fields
.field private static b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    sget-object v0, Lbpb;->a:[Ljava/lang/String;

    sget-object v1, Lbpb;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 41
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    const-string v2, "directory_id"

    aput-object v2, v0, v1

    .line 43
    sput-object v0, Lbpb;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>([Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 28
    return-void
.end method

.method public static a(Landroid/content/Context;[Landroid/database/Cursor;Ljava/util/List;)Lbpb;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1
    array-length v0, p1

    .line 2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Directories (%d) and cursors (%d) must be the same size."

    new-array v4, v11, [Ljava/lang/Object;

    .line 3
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    array-length v5, p1

    .line 4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    .line 5
    invoke-static {v0, v3, v4}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 8
    :goto_1
    array-length v0, p1

    if-ge v3, v0, :cond_3

    .line 9
    aget-object v5, p1, v3

    .line 10
    if-eqz v5, :cond_0

    invoke-interface {v5}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 11
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpe;

    .line 12
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_2

    .line 13
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 21
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 2
    goto :goto_0

    .line 15
    :cond_2
    invoke-virtual {v0}, Lbpe;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lbpe;->a()I

    move-result v0

    .line 16
    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v8, Lbpb;->b:[Ljava/lang/String;

    invoke-direct {v7, v8, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 17
    new-array v8, v11, [Ljava/lang/Object;

    const v9, 0x7f110152

    new-array v10, v1, [Ljava/lang/Object;

    aput-object v6, v10, v2

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 19
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 22
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/Cursor;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    .line 24
    array-length v1, v0

    if-lez v1, :cond_4

    .line 25
    new-instance v1, Lbpb;

    invoke-direct {v1, v0}, Lbpb;-><init>([Landroid/database/Cursor;)V

    move-object v0, v1

    .line 26
    :goto_3
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-virtual {p0}, Lbpb;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lbpb;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lbpb;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public final c()J
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 30
    invoke-virtual {p0}, Lbpb;->getPosition()I

    move-result v0

    .line 31
    :cond_0
    invoke-virtual {p0}, Lbpb;->moveToPrevious()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 32
    const-string v1, "directory_id"

    invoke-virtual {p0, v1}, Lbpb;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 33
    if-eq v1, v2, :cond_0

    .line 34
    invoke-virtual {p0, v1}, Lbpb;->getInt(I)I

    move-result v1

    .line 35
    if-eq v1, v2, :cond_0

    .line 36
    invoke-virtual {p0, v0}, Lbpb;->moveToPosition(I)Z

    .line 37
    int-to-long v0, v1

    return-wide v0

    .line 38
    :cond_1
    const/16 v1, 0x2b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "No directory id for contact at: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method
