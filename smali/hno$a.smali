.class final Lhno$a;
.super Lhno;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhno;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private d:Lhnn;

.field private e:Lhnn;

.field private f:Lhnn;

.field private g:Lhnn;


# direct methods
.method public constructor <init>(Lhnn;Lhnn;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Lhnn;Lhnn;Ljava/security/Provider;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p7}, Lhno;-><init>(Ljava/security/Provider;)V

    .line 2
    iput-object p1, p0, Lhno$a;->d:Lhnn;

    .line 3
    iput-object p2, p0, Lhno$a;->e:Lhnn;

    .line 4
    iput-object p5, p0, Lhno$a;->f:Lhnn;

    .line 5
    iput-object p6, p0, Lhno$a;->g:Lhnn;

    .line 6
    return-void
.end method


# virtual methods
.method public final a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 7
    if-eqz p2, :cond_0

    .line 8
    iget-object v0, p0, Lhno$a;->d:Lhnn;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p1, v1}, Lhnn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    iget-object v0, p0, Lhno$a;->e:Lhnn;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {v0, p1, v1}, Lhnn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    :cond_0
    iget-object v0, p0, Lhno$a;->g:Lhnn;

    invoke-virtual {v0, p1}, Lhnn;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {p3}, Lhno$a;->a(Ljava/util/List;)[B

    move-result-object v1

    aput-object v1, v0, v4

    .line 12
    iget-object v1, p0, Lhno$a;->g:Lhnn;

    invoke-virtual {v1, p1, v0}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    :cond_1
    return-void
.end method

.method public final b(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 14
    iget-object v0, p0, Lhno$a;->f:Lhnn;

    invoke-virtual {v0, p1}, Lhnn;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 16
    :goto_0
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lhno$a;->f:Lhnn;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v2}, Lhnn;->b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 16
    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/String;

    sget-object v2, Lhnr;->b:Ljava/nio/charset/Charset;

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
