.class public final Lbyp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbyw;

.field public b:Z

.field public c:F

.field public d:I

.field public e:I

.field public f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

.field public g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

.field public h:Landroid/animation/Animator;

.field public i:I

.field public j:Landroid/view/View;

.field private k:Landroid/content/Context;

.field private l:Lcbo;

.field private m:Landroid/view/VelocityTracker;

.field private n:F

.field private o:F

.field private p:F

.field private q:I

.field private r:I

.field private s:Z

.field private t:I

.field private u:Z

.field private v:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method public constructor <init>(Lbyw;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lbyq;

    invoke-direct {v0, p0}, Lbyq;-><init>(Lbyp;)V

    iput-object v0, p0, Lbyp;->v:Landroid/animation/AnimatorListenerAdapter;

    .line 3
    iput-object p2, p0, Lbyp;->k:Landroid/content/Context;

    .line 4
    iput-object p1, p0, Lbyp;->a:Lbyw;

    .line 5
    invoke-virtual {p0}, Lbyp;->a()V

    .line 6
    return-void
.end method

.method private final a(FZZ)V
    .locals 13

    .prologue
    .line 178
    invoke-direct {p0}, Lbyp;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 179
    :goto_0
    invoke-direct {p0}, Lbyp;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 180
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 181
    iget v0, p0, Lbyp;->c:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_3

    .line 182
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_6

    iget-object v1, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 183
    :goto_2
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v8, v0

    .line 184
    :goto_3
    invoke-virtual {p0}, Lbyp;->e()I

    move-result v0

    int-to-float v0, v0

    div-float v3, v2, v0

    .line 185
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, v3

    .line 186
    const/4 v4, 0x0

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v12

    .line 187
    if-eqz p2, :cond_8

    if-eqz p3, :cond_8

    const/4 v0, 0x1

    move v11, v0

    .line 188
    :goto_4
    if-eqz p2, :cond_9

    if-nez p3, :cond_9

    const/4 v0, 0x1

    move v9, v0

    .line 190
    :goto_5
    iget v0, p0, Lbyp;->d:I

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_a

    .line 191
    const/4 v2, 0x0

    .line 194
    :goto_6
    if-eqz p2, :cond_b

    invoke-direct {p0}, Lbyp;->h()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    move v10, v0

    .line 195
    :goto_7
    if-eqz v1, :cond_1

    .line 196
    if-nez p2, :cond_c

    .line 199
    iget v0, v1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    .line 200
    mul-float/2addr v0, v12

    add-float/2addr v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    .line 201
    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 207
    :cond_1
    :goto_8
    if-eqz v8, :cond_2

    .line 208
    const/4 v2, 0x0

    .line 210
    iget v0, v8, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    .line 211
    mul-float v3, v12, v0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, v8

    move v4, v11

    move v5, v10

    move v7, v9

    .line 212
    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 213
    :cond_2
    iput p1, p0, Lbyp;->c:F

    .line 214
    :cond_3
    return-void

    .line 178
    :cond_4
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    goto :goto_0

    .line 179
    :cond_5
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    goto :goto_1

    .line 182
    :cond_6
    iget-object v1, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    goto :goto_2

    .line 183
    :cond_7
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v8, v0

    goto :goto_3

    .line 187
    :cond_8
    const/4 v0, 0x0

    move v11, v0

    goto :goto_4

    .line 188
    :cond_9
    const/4 v0, 0x0

    move v9, v0

    goto :goto_5

    .line 192
    :cond_a
    iget v0, p0, Lbyp;->d:I

    int-to-float v0, v0

    sub-float v0, v2, v0

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float/2addr v0, v2

    iget v2, p0, Lbyp;->i:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    goto :goto_6

    .line 194
    :cond_b
    const/4 v0, 0x0

    move v10, v0

    goto :goto_7

    .line 202
    :cond_c
    const/4 v2, 0x0

    .line 204
    iget v0, v1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    .line 205
    mul-float v3, v12, v0

    const/4 v6, 0x0

    move-object v0, p0

    move v4, v11

    move v5, v10

    move v7, v9

    .line 206
    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    goto :goto_8
.end method

.method private final a(ZFF)V
    .locals 9

    .prologue
    .line 102
    iget-boolean v0, p0, Lbyp;->b:Z

    if-eqz v0, :cond_e

    .line 105
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    if-nez v0, :cond_5

    .line 106
    const/4 v0, 0x0

    .line 118
    :cond_0
    :goto_0
    invoke-direct {p0}, Lbyp;->h()Z

    move-result v3

    .line 119
    iget v1, p0, Lbyp;->c:F

    mul-float/2addr v1, v0

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_6

    const/4 v1, 0x1

    .line 120
    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lbyp;->r:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_7

    if-eqz v1, :cond_7

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v2, v3

    .line 121
    xor-int/2addr v1, v2

    if-eqz v1, :cond_8

    const/4 v0, 0x0

    move v3, v0

    .line 122
    :goto_3
    if-nez v2, :cond_1

    if-eqz p1, :cond_9

    :cond_1
    const/4 v0, 0x1

    :goto_4
    iget v1, p0, Lbyp;->c:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a

    const/4 v1, 0x1

    move v2, v1

    .line 123
    :goto_5
    if-eqz v2, :cond_b

    iget-object v1, p0, Lbyp;->a:Lbyw;

    invoke-interface {v1}, Lbyw;->a()F

    move-result v1

    neg-float v1, v1

    .line 124
    :goto_6
    if-eqz v0, :cond_2

    const/4 v1, 0x0

    .line 125
    :cond_2
    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lbyp;->c:F

    aput v6, v4, v5

    const/4 v5, 0x1

    aput v1, v4, v5

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 126
    iget-object v4, p0, Lbyp;->l:Lcbo;

    iget v5, p0, Lbyp;->c:F

    invoke-virtual {v4, v7, v5, v1, v3}, Lcbo;->a(Landroid/animation/Animator;FFF)V

    .line 127
    new-instance v1, Lbyu;

    invoke-direct {v1, p0}, Lbyu;-><init>(Lbyp;)V

    invoke-virtual {v7, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 128
    iget-object v1, p0, Lbyp;->v:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v7, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 129
    if-nez v0, :cond_d

    .line 130
    const/high16 v0, 0x3ec00000    # 0.375f

    mul-float v4, v3, v0

    new-instance v8, Lbyv;

    invoke-direct {v8, p0, v2}, Lbyv;-><init>(Lbyp;Z)V

    .line 131
    if-eqz v2, :cond_c

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v6, v0

    .line 132
    :goto_7
    if-eqz v6, :cond_3

    .line 134
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->e:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(Landroid/animation/Animator;)V

    .line 135
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->k:Landroid/animation/Animator;

    invoke-static {v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(Landroid/animation/Animator;)V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->m:Z

    .line 137
    iget v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b:F

    iput v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->j:F

    .line 138
    invoke-virtual {v6}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b()F

    move-result v3

    .line 139
    invoke-virtual {v6, v3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b(F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 140
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a:Lcbo;

    iget v2, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b:F

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcbo;->a(Landroid/animation/Animator;FFFF)V

    .line 141
    new-instance v0, Lbzb;

    invoke-direct {v0, v6, v8, v3}, Lbzb;-><init>(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;Ljava/lang/Runnable;F)V

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 142
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 143
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v6, v0, v1}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(FZ)V

    .line 144
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->i:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->i:Landroid/view/View;

    .line 147
    invoke-virtual {v6}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->getLeft()I

    move-result v1

    iget v2, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->c:I

    add-int/2addr v1, v2

    invoke-virtual {v6}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->getTop()I

    move-result v2

    iget v5, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->d:I

    add-int/2addr v2, v5

    iget v5, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b:F

    .line 148
    invoke-static {v0, v1, v2, v5, v3}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->k:Landroid/animation/Animator;

    .line 149
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a:Lcbo;

    iget-object v1, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->k:Landroid/animation/Animator;

    iget v2, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b:F

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcbo;->a(Landroid/animation/Animator;FFFF)V

    .line 150
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->k:Landroid/animation/Animator;

    iget-object v1, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->n:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 151
    iget-object v0, v6, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->k:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 154
    :cond_3
    :goto_8
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->start()V

    .line 155
    iput-object v7, p0, Lbyp;->h:Landroid/animation/Animator;

    .line 158
    :goto_9
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_4

    .line 159
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    .line 161
    :cond_4
    return-void

    .line 107
    :cond_5
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 108
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 109
    iget-object v1, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    .line 110
    iget v2, p0, Lbyp;->n:F

    sub-float v2, p2, v2

    .line 111
    iget v3, p0, Lbyp;->o:F

    sub-float v3, p3, v3

    .line 112
    float-to-double v4, v2

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    double-to-float v4, v4

    .line 113
    mul-float/2addr v0, v2

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    div-float/2addr v0, v4

    .line 114
    iget-object v1, p0, Lbyp;->j:Landroid/view/View;

    iget-object v2, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-ne v1, v2, :cond_0

    .line 115
    neg-float v0, v0

    goto/16 :goto_0

    .line 119
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 120
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_8
    move v3, v0

    .line 121
    goto/16 :goto_3

    .line 122
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_a
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_5

    .line 123
    :cond_b
    iget-object v1, p0, Lbyp;->a:Lbyw;

    invoke-interface {v1}, Lbyw;->a()F

    move-result v1

    goto/16 :goto_6

    .line 131
    :cond_c
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v6, v0

    goto/16 :goto_7

    .line 153
    :cond_d
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbyp;->a(Z)V

    goto :goto_8

    .line 157
    :cond_e
    const/4 v0, 0x0

    iput-object v0, p0, Lbyp;->j:Landroid/view/View;

    goto :goto_9
.end method

.method private final a(Landroid/view/View;FF)Z
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 100
    sub-float v0, p2, v0

    float-to-double v2, v0

    sub-float v0, p3, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    .line 101
    iget v2, p0, Lbyp;->t:I

    div-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 256
    :cond_0
    return-void
.end method

.method private final f()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final g()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-virtual {v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final h()Z
    .locals 3

    .prologue
    .line 175
    iget v0, p0, Lbyp;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lbyp;->p:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p0}, Lbyp;->e()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ZI)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 164
    if-eqz p1, :cond_0

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v1, v0

    .line 165
    :goto_0
    if-nez v1, :cond_1

    .line 166
    const/4 v0, 0x0

    .line 171
    :goto_1
    return-object v0

    .line 164
    :cond_0
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-object v1, v0

    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v2, 0x0

    .line 168
    iget v3, v1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->b:F

    .line 169
    aput v3, v0, v2

    const/4 v2, 0x1

    int-to-float v3, p2

    aput v3, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 170
    new-instance v2, Lbyt;

    invoke-direct {v2, p0, v1, p1}, Lbyt;-><init>(Lbyp;Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;Z)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 7
    invoke-virtual {p0}, Lbyp;->c()V

    .line 8
    iget-object v1, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 9
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 10
    iget v3, v0, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    :goto_0
    move-object v0, p0

    move v5, v4

    move v7, v4

    .line 12
    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 13
    iget-object v1, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 14
    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 15
    iget v3, v0, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    :goto_1
    move-object v0, p0

    move v5, v4

    move v7, v4

    .line 17
    invoke-virtual/range {v0 .. v7}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V

    .line 18
    invoke-virtual {p0}, Lbyp;->b()V

    .line 19
    return-void

    :cond_0
    move v3, v2

    .line 11
    goto :goto_0

    :cond_1
    move v3, v2

    .line 16
    goto :goto_1
.end method

.method public final a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FFZZZZ)V
    .locals 1

    .prologue
    .line 215
    if-nez p1, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-virtual {p1}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p6, :cond_0

    .line 219
    :cond_2
    if-eqz p7, :cond_3

    .line 220
    invoke-virtual {p1, p2}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(F)V

    .line 223
    :goto_1
    invoke-virtual {p0, p1, p3, p4}, Lbyp;->a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FZ)V

    goto :goto_0

    .line 222
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, p2, p5, v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(FZZ)V

    goto :goto_1
.end method

.method final a(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;FZ)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const-wide/16 v2, -0x1

    .line 225
    .line 227
    iget v0, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->l:F

    .line 228
    div-float v0, p2, v0

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    const v1, 0x3f4ccccd    # 0.8f

    add-float/2addr v0, v1

    .line 229
    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 231
    invoke-static {v6, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 232
    invoke-virtual {p1, v0, p3}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(FZ)V

    .line 235
    iget-object v0, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->g:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(Landroid/animation/Animator;)V

    .line 236
    if-nez p3, :cond_0

    .line 237
    iput v1, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->h:F

    .line 238
    invoke-virtual {p1}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->invalidate()V

    .line 253
    :goto_0
    return-void

    .line 239
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v4, 0x0

    iget v5, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->h:F

    aput v5, v0, v4

    const/4 v4, 0x1

    aput v1, v0, v4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 240
    iput-object v4, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->g:Landroid/animation/ValueAnimator;

    .line 241
    new-instance v0, Lbze;

    invoke-direct {v0, p1}, Lbze;-><init>(Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;)V

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 242
    iget-object v0, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->o:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 243
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    .line 244
    sget-object v0, Lcbs;->a:Landroid/view/animation/Interpolator;

    .line 246
    :goto_1
    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 247
    cmp-long v0, v2, v2

    if-nez v0, :cond_2

    .line 248
    iget v0, p1, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->h:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3e4ccccc    # 0.19999999f

    div-float/2addr v0, v1

    .line 249
    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 250
    const/high16 v1, 0x43480000    # 200.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 251
    :goto_2
    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 252
    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 245
    :cond_1
    sget-object v0, Lcbs;->b:Landroid/view/animation/Interpolator;

    goto :goto_1

    :cond_2
    move-wide v0, v2

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 257
    invoke-virtual {p0}, Lbyp;->d()V

    .line 258
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, p1}, Lbyp;->a(FZZ)V

    .line 259
    iput-boolean v1, p0, Lbyp;->s:Z

    .line 260
    iget-boolean v0, p0, Lbyp;->b:Z

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyp;->b:Z

    .line 262
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 43
    iget-boolean v3, p0, Lbyp;->s:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 97
    :goto_0
    return v2

    .line 45
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 46
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 48
    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v2, v1

    .line 97
    goto :goto_0

    .line 50
    :pswitch_1
    invoke-direct {p0}, Lbyp;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-direct {p0, v0, v5, v4}, Lbyp;->a(Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 56
    :goto_2
    if-eqz v0, :cond_2

    iget-object v3, p0, Lbyp;->j:Landroid/view/View;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lbyp;->j:Landroid/view/View;

    if-eq v3, v0, :cond_5

    .line 57
    :cond_2
    iput-boolean v1, p0, Lbyp;->s:Z

    goto :goto_0

    .line 52
    :cond_3
    invoke-direct {p0}, Lbyp;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    invoke-direct {p0, v0, v5, v4}, Lbyp;->a(Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 53
    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    goto :goto_2

    .line 54
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 59
    :cond_5
    iget-object v3, p0, Lbyp;->j:Landroid/view/View;

    if-eqz v3, :cond_7

    .line 60
    invoke-virtual {p0}, Lbyp;->d()V

    .line 63
    :goto_3
    iput-boolean v1, p0, Lbyp;->b:Z

    .line 64
    iput-object v0, p0, Lbyp;->j:Landroid/view/View;

    .line 65
    iput v5, p0, Lbyp;->n:F

    .line 66
    iput v4, p0, Lbyp;->o:F

    .line 67
    iget v0, p0, Lbyp;->c:F

    iput v0, p0, Lbyp;->p:F

    .line 69
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_6

    .line 70
    iget-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 71
    :cond_6
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lbyp;->m:Landroid/view/VelocityTracker;

    .line 72
    invoke-direct {p0, p1}, Lbyp;->b(Landroid/view/MotionEvent;)V

    .line 73
    iput-boolean v2, p0, Lbyp;->s:Z

    goto :goto_1

    .line 61
    :cond_7
    iput-boolean v2, p0, Lbyp;->u:Z

    goto :goto_3

    .line 75
    :pswitch_2
    iput-boolean v1, p0, Lbyp;->s:Z

    .line 76
    invoke-direct {p0, v1, v5, v4}, Lbyp;->a(ZFF)V

    goto :goto_1

    .line 78
    :pswitch_3
    invoke-direct {p0, p1}, Lbyp;->b(Landroid/view/MotionEvent;)V

    .line 79
    iget v0, p0, Lbyp;->n:F

    sub-float v0, v5, v0

    .line 80
    iget v3, p0, Lbyp;->o:F

    sub-float v3, v4, v3

    .line 81
    float-to-double v4, v0

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    double-to-float v0, v4

    .line 82
    iget-boolean v3, p0, Lbyp;->u:Z

    if-nez v3, :cond_8

    iget v3, p0, Lbyp;->d:I

    int-to-float v3, v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_8

    .line 83
    iput-boolean v1, p0, Lbyp;->u:Z

    .line 84
    :cond_8
    iget-boolean v3, p0, Lbyp;->b:Z

    if-eqz v3, :cond_1

    .line 85
    iget-object v3, p0, Lbyp;->j:Landroid/view/View;

    iget-object v4, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-ne v3, v4, :cond_9

    .line 86
    iget v3, p0, Lbyp;->p:F

    sub-float v0, v3, v0

    .line 87
    invoke-static {v8, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 90
    :goto_4
    invoke-direct {p0, v0, v2, v2}, Lbyp;->a(FZZ)V

    goto/16 :goto_1

    .line 88
    :cond_9
    iget v3, p0, Lbyp;->p:F

    add-float/2addr v0, v3

    .line 89
    invoke-static {v8, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_4

    :pswitch_4
    move v0, v1

    .line 92
    :goto_5
    iget-object v3, p0, Lbyp;->j:Landroid/view/View;

    iget-object v6, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-ne v3, v6, :cond_b

    move v3, v1

    .line 93
    :goto_6
    invoke-direct {p0, p1}, Lbyp;->b(Landroid/view/MotionEvent;)V

    .line 94
    if-nez v0, :cond_a

    move v2, v1

    :cond_a
    invoke-direct {p0, v2, v5, v4}, Lbyp;->a(ZFF)V

    .line 95
    iget-boolean v2, p0, Lbyp;->u:Z

    if-nez v2, :cond_1

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lbyp;->a:Lbyw;

    invoke-interface {v0, v3}, Lbyw;->b(Z)V

    goto/16 :goto_1

    :cond_b
    move v3, v2

    .line 92
    goto :goto_6

    :pswitch_5
    move v0, v2

    goto :goto_5

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lbyp;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lbyp;->d:I

    .line 22
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lbyp;->r:I

    .line 23
    iget-object v0, p0, Lbyp;->k:Landroid/content/Context;

    .line 24
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbyp;->q:I

    .line 25
    iget-object v0, p0, Lbyp;->k:Landroid/content/Context;

    .line 26
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0053

    .line 27
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbyp;->i:I

    .line 28
    iget-object v0, p0, Lbyp;->k:Landroid/content/Context;

    .line 29
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbyp;->t:I

    .line 30
    iget-object v0, p0, Lbyp;->k:Landroid/content/Context;

    .line 31
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbyp;->e:I

    .line 32
    new-instance v0, Lcbo;

    iget-object v1, p0, Lbyp;->k:Landroid/content/Context;

    const v2, 0x3ecccccd    # 0.4f

    invoke-direct {v0, v1, v2}, Lcbo;-><init>(Landroid/content/Context;F)V

    iput-object v0, p0, Lbyp;->l:Lcbo;

    .line 33
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lbyp;->a:Lbyw;

    invoke-interface {v0}, Lbyw;->b()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-result-object v0

    iput-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 35
    iget-object v0, p0, Lbyp;->a:Lbyw;

    invoke-interface {v0}, Lbyw;->c()Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    move-result-object v0

    iput-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    .line 37
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lbyp;->f:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    iget-object v1, p0, Lbyp;->a:Lbyw;

    invoke-interface {v1}, Lbyw;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(Landroid/view/View;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lbyp;->g:Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/incallui/answer/impl/affordance/SwipeButtonView;->a(Landroid/view/View;)V

    .line 41
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lbyp;->h:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lbyp;->h:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 174
    :cond_0
    return-void
.end method

.method final e()I
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lbyp;->a:Lbyw;

    invoke-interface {v0}, Lbyw;->e()F

    move-result v0

    .line 177
    iget v1, p0, Lbyp;->q:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
