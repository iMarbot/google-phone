.class public Laoj;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lanw;
.implements Lanx;
.implements Laox;
.implements Lbgh;
.implements Lcom/android/dialer/widget/EmptyContentView$a;
.implements Lib;


# instance fields
.field public final a:Landroid/os/Handler;

.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Lano;

.field public d:Z

.field public e:Laow;

.field private f:Landroid/database/ContentObserver;

.field private g:Landroid/database/ContentObserver;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Labq;

.field private l:Lbgf;

.field private m:Lcom/android/dialer/widget/EmptyContentView;

.field private n:Laqd;

.field private o:Laqf;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:J

.field private u:Z

.field private v:Z

.field private w:Landroid/os/Handler;

.field private x:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1
    invoke-direct {p0, v0, v0}, Laoj;-><init>(II)V

    .line 2
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Laoj;-><init>(II)V

    .line 4
    return-void
.end method

.method private constructor <init>(II)V
    .locals 4

    .prologue
    .line 8
    const/4 v0, -0x1

    const-wide/16 v2, 0x0

    invoke-direct {p0, p1, v0, v2, v3}, Laoj;-><init>(IIJ)V

    .line 9
    return-void
.end method

.method private constructor <init>(IIJ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 10
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 11
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Laoj;->a:Landroid/os/Handler;

    .line 12
    new-instance v0, Laol;

    invoke-direct {v0, p0}, Laol;-><init>(Laoj;)V

    iput-object v0, p0, Laoj;->f:Landroid/database/ContentObserver;

    .line 13
    new-instance v0, Laol;

    invoke-direct {v0, p0}, Laol;-><init>(Laoj;)V

    iput-object v0, p0, Laoj;->g:Landroid/database/ContentObserver;

    .line 14
    new-instance v0, Laqf;

    invoke-direct {v0, p0}, Laqf;-><init>(Laoj;)V

    iput-object v0, p0, Laoj;->o:Laqf;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoj;->q:Z

    .line 16
    iput v1, p0, Laoj;->r:I

    .line 17
    iput v1, p0, Laoj;->s:I

    .line 18
    iput-wide v2, p0, Laoj;->t:J

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Laoj;->u:Z

    .line 20
    new-instance v0, Laok;

    invoke-direct {v0, p0}, Laok;-><init>(Laoj;)V

    iput-object v0, p0, Laoj;->w:Landroid/os/Handler;

    .line 21
    iput p1, p0, Laoj;->r:I

    .line 22
    iput p2, p0, Laoj;->s:I

    .line 23
    iput-wide v2, p0, Laoj;->t:J

    .line 24
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 1

    .prologue
    .line 5
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Laoj;-><init>(II)V

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoj;->u:Z

    .line 7
    return-void
.end method

.method private final a(I)V
    .locals 3

    .prologue
    .line 299
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 300
    if-nez v0, :cond_0

    .line 319
    :goto_0
    return-void

    .line 302
    :cond_0
    const-string v1, "android.permission.READ_CALL_LOG"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 303
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f11025c

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 304
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110262

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    goto :goto_0

    .line 306
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 313
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x36

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected filter type in CallLogFragment: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :pswitch_1
    const v0, 0x7f11009f

    .line 314
    :goto_1
    iget-object v1, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v1, v0}, Lcom/android/dialer/widget/EmptyContentView;->a(I)V

    .line 315
    iget-boolean v0, p0, Laoj;->u:Z

    if-nez v0, :cond_2

    .line 316
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 317
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f110098

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    goto :goto_0

    .line 309
    :pswitch_2
    const v0, 0x7f1100a4

    .line 310
    goto :goto_1

    .line 311
    :pswitch_3
    const v0, 0x7f110097

    .line 312
    goto :goto_1

    .line 318
    :cond_2
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->c(I)V

    goto :goto_0

    .line 306
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final l()V
    .locals 3

    .prologue
    .line 419
    iget-boolean v0, p0, Laoj;->v:Z

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Laoj;->j:Landroid/widget/ImageView;

    .line 421
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0200a2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 422
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 424
    iget-object v0, p0, Laoj;->c:Lano;

    .line 425
    invoke-virtual {v0}, Lano;->j()V

    .line 438
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Laoj;->j:Landroid/widget/ImageView;

    .line 427
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0200a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 428
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 430
    iget-object v0, p0, Laoj;->c:Lano;

    .line 432
    const/4 v1, 0x0

    iput-boolean v1, v0, Lano;->p:Z

    .line 433
    const/4 v1, 0x1

    iput-boolean v1, v0, Lano;->q:Z

    .line 434
    iget-object v1, v0, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 435
    invoke-virtual {v0}, Lano;->c()V

    .line 437
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/16 v1, 0x36

    const/4 v2, 0x0

    .line 248
    iget-object v0, p0, Laoj;->l:Lbgf;

    iget v3, p0, Laoj;->r:I

    iget-wide v4, p0, Laoj;->t:J

    .line 250
    invoke-virtual {v0, v1}, Lbgf;->cancelOperation(I)V

    .line 251
    iget-object v6, v0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v6}, Lbsw;->a(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 253
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 255
    const-string v8, "(type"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " != ?)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const/4 v8, 0x6

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-static {}, Lapw;->o()I

    move-result v8

    const/16 v9, 0x17

    if-lt v8, v9, :cond_0

    .line 258
    const-string v8, " AND (deleted"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = 0)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    :cond_0
    if-ltz v3, :cond_3

    .line 260
    const-string v8, " AND (type"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " = ?)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    :goto_0
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_1

    .line 265
    const-string v8, " AND (date"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " > ?)"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    :cond_1
    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 268
    iget-object v3, v0, Lbgf;->b:Landroid/content/Context;

    invoke-static {v3}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v3

    .line 269
    invoke-virtual {v3}, Lclp;->a()Lcln;

    move-result-object v3

    iget-object v4, v0, Lbgf;->b:Landroid/content/Context;

    .line 270
    invoke-interface {v3, v4, v6, v7}, Lcln;->a(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 283
    :goto_1
    iget v3, v0, Lbgf;->a:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_5

    const/16 v3, 0x3e8

    .line 284
    :goto_2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 285
    :goto_3
    iget-object v4, v0, Lbgf;->b:Landroid/content/Context;

    .line 286
    invoke-static {v4}, Lbsp;->e(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    .line 287
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v6, "limit"

    .line 288
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 289
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 291
    invoke-static {}, Lbmk;->a()[Ljava/lang/String;

    move-result-object v4

    .line 292
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v7, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    const-string v7, "date DESC"

    .line 293
    invoke-virtual/range {v0 .. v7}, Lbgf;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_4
    iget-boolean v0, p0, Laoj;->u:Z

    if-nez v0, :cond_2

    .line 297
    invoke-virtual {p0}, Laoj;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Larl;

    invoke-virtual {v0}, Larl;->a()V

    .line 298
    :cond_2
    return-void

    .line 262
    :cond_3
    const-string v8, " AND NOT "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    const-string v8, "(type = 4)"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 271
    :cond_4
    const-string v3, " AND (subscription_component_name"

    .line 272
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 273
    const-string v4, " IS NULL OR subscription_component_name"

    .line 274
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 275
    const-string v4, " NOT LIKE \'com.google.android.apps.tachyon%\' OR features"

    .line 276
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 277
    const-string v4, " & 1"

    .line 278
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 279
    const-string v4, " == 1"

    .line 280
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 281
    const-string v4, ")"

    .line 282
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 283
    :cond_5
    iget v3, v0, Lbgf;->a:I

    goto :goto_2

    :cond_6
    move-object v5, v2

    .line 284
    goto :goto_3

    .line 295
    :cond_7
    invoke-virtual {v0, v2}, Lbgf;->a(Landroid/database/Cursor;)Z

    goto :goto_4
.end method

.method protected final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 73
    const v0, 0x7f0e010b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 74
    iget-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 75
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->s:Z

    .line 76
    new-instance v0, Labq;

    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Labq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laoj;->k:Labq;

    .line 77
    iget-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Laoj;->k:Labq;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$f;)V

    .line 78
    iget-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lbly;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 79
    const v0, 0x7f0e00d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/widget/EmptyContentView;

    iput-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    .line 80
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const v1, 0x7f02008c

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->b(I)V

    .line 81
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    .line 82
    iput-object p0, v0, Lcom/android/dialer/widget/EmptyContentView;->d:Lcom/android/dialer/widget/EmptyContentView$a;

    .line 83
    const v0, 0x7f0e011f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Laoj;->x:Landroid/view/ViewGroup;

    .line 84
    new-instance v0, Laow;

    .line 85
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Laoj;->x:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, v2, p0}, Laow;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Laox;)V

    iput-object v0, p0, Laoj;->e:Laow;

    .line 86
    const v0, 0x7f0e011c

    .line 87
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laoj;->h:Landroid/view/View;

    .line 88
    const v0, 0x7f0e011e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laoj;->i:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e011d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laoj;->j:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Laoj;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Laoj;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Laoj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 396
    iget-object v5, p0, Laoj;->h:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 397
    iget-object v5, p0, Laoj;->h:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setAlpha(F)V

    .line 398
    iget-object v0, p0, Laoj;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 399
    invoke-virtual {p0}, Laoj;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Larl;

    .line 400
    iget-object v3, v0, Larl;->b:Lcom/android/contacts/common/list/ViewPagerTabs;

    if-eqz p1, :cond_4

    :goto_3
    invoke-virtual {v3, v2}, Lcom/android/contacts/common/list/ViewPagerTabs;->setVisibility(I)V

    .line 401
    iget-object v0, v0, Larl;->a:Lcom/android/dialer/app/list/DialerViewPager;

    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 402
    :cond_0
    iput-boolean v1, v0, Lcom/android/dialer/app/list/DialerViewPager;->f:Z

    .line 403
    return-void

    :cond_1
    move v0, v2

    .line 396
    goto :goto_0

    :cond_2
    move v0, v4

    .line 397
    goto :goto_1

    :cond_3
    move v4, v3

    .line 398
    goto :goto_2

    :cond_4
    move v2, v1

    .line 400
    goto :goto_3
.end method

.method public final a(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v1, p0, Laoj;->c:Lano;

    .line 50
    const/4 v2, -0x1

    iput v2, v1, Lano;->l:I

    .line 51
    iget-object v1, p0, Laoj;->c:Lano;

    .line 52
    iput-boolean v0, v1, Lano;->w:Z

    .line 53
    iget-object v1, p0, Laoj;->c:Lano;

    invoke-virtual {v1, p1}, Lano;->b(Landroid/database/Cursor;)V

    .line 54
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 55
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 56
    iget-object v1, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 57
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingStart()I

    move-result v2

    iget-object v3, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 58
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingEnd()I

    move-result v3

    .line 59
    invoke-virtual {p0}, Laoj;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0143

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 60
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/support/v7/widget/RecyclerView;->setPaddingRelative(IIII)V

    .line 61
    iget-object v0, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    .line 66
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 62
    :cond_2
    iget-object v1, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 63
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingStart()I

    move-result v2

    iget-object v3, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingEnd()I

    move-result v3

    .line 64
    invoke-virtual {v1, v2, v0, v3, v0}, Landroid/support/v7/widget/RecyclerView;->setPaddingRelative(IIII)V

    .line 65
    iget-object v1, p0, Laoj;->m:Lcom/android/dialer/widget/EmptyContentView;

    invoke-virtual {v1, v0}, Lcom/android/dialer/widget/EmptyContentView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Laoj;->v:Z

    .line 405
    iget-object v0, p0, Laoj;->j:Landroid/widget/ImageView;

    .line 406
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0200a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 407
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 408
    return-void
.end method

.method public final b(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 378
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 379
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    aput-object p0, v0, v4

    const/4 v1, 0x2

    .line 380
    invoke-virtual {p0}, Laoj;->getUserVisibleHint()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 382
    iget-object v0, p0, Laoj;->c:Lano;

    .line 384
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 385
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Laom;

    .line 386
    if-eqz p1, :cond_1

    .line 387
    iget-object v1, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v5}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 388
    iget-object v1, p0, Laoj;->x:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 389
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laoj;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    invoke-interface {v0, v3}, Laom;->c(Z)V

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-object v1, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 392
    iget-object v1, p0, Laoj;->x:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 393
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laoj;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-interface {v0, v4}, Laom;->c(Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 409
    const-string v0, "CallLogFragment.tapSelectAll"

    const-string v1, "imitating select all"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoj;->v:Z

    .line 411
    invoke-direct {p0}, Laoj;->l()V

    .line 412
    return-void
.end method

.method public final c(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method protected d()Latf;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method final e()V
    .locals 2

    .prologue
    .line 326
    iget-boolean v0, p0, Laoj;->d:Z

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Laoj;->n:Laqd;

    .line 328
    iget-object v1, v0, Laqd;->a:Lbsu;

    .line 329
    iget-object v1, v1, Lbsu;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 330
    invoke-virtual {v0}, Laqd;->b()V

    .line 331
    iget-object v0, p0, Laoj;->c:Lano;

    const/4 v1, 0x1

    .line 332
    iput-boolean v1, v0, Lano;->w:Z

    .line 333
    invoke-virtual {p0}, Laoj;->a()V

    .line 334
    iget-object v0, p0, Laoj;->l:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 335
    iget-object v0, p0, Laoj;->l:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()V

    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Laoj;->d:Z

    .line 339
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Laoj;->c:Lano;

    .line 338
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    goto :goto_0
.end method

.method final g()V
    .locals 6

    .prologue
    const-wide/32 v4, 0xea60

    const/4 v3, 0x1

    .line 359
    iget-object v0, p0, Laoj;->w:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 361
    rem-long/2addr v0, v4

    sub-long v0, v4, v0

    .line 362
    iget-object v2, p0, Laoj;->w:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 363
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Laoj;->l:Lbgf;

    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/KeyguardManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Laoj;->l:Lbgf;

    invoke-virtual {v0}, Lbgf;->c()V

    .line 367
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/dialer/app/calllog/CallLogNotificationsService;->b(Landroid/content/Context;)V

    .line 368
    :cond_0
    return-void
.end method

.method public final h_()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 340
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 341
    if-nez v0, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lbsw;->a:Ljava/util/List;

    .line 345
    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 346
    array-length v2, v1

    if-lez v2, :cond_3

    .line 347
    const-string v2, "CallLogFragment.onEmptyViewActionButtonClicked"

    const-string v3, "Requesting permissions: "

    .line 348
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-array v3, v5, [Ljava/lang/Object;

    .line 349
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    const/4 v0, 0x1

    invoke-static {p0, v1, v0}, Lhv;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    goto :goto_0

    .line 348
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 351
    :cond_3
    iget-boolean v1, p0, Laoj;->u:Z

    if-nez v1, :cond_0

    .line 352
    const-string v1, "CallLogFragment.onEmptyViewActionButtonClicked"

    const-string v2, "showing dialpad"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    check-cast v0, Laom;

    invoke-interface {v0}, Laom;->u()V

    goto :goto_0
.end method

.method public i()V
    .locals 2

    .prologue
    .line 369
    const-string v0, "CallLogFragment.onPageSelected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 370
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Laom;

    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Laom;

    invoke-virtual {p0}, Laoj;->j()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, Laom;->c(Z)V

    .line 372
    :cond_0
    return-void

    .line 371
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 373
    iget-object v2, p0, Laoj;->e:Laow;

    if-eqz v2, :cond_1

    iget-object v2, p0, Laoj;->e:Laow;

    .line 374
    iget-object v2, v2, Laow;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    .line 375
    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 374
    goto :goto_0

    :cond_1
    move v0, v1

    .line 375
    goto :goto_1
.end method

.method public k()V
    .locals 1

    .prologue
    .line 376
    const-string v0, "CallLogFragment.onPageUnselected"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 377
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v13, 0x0

    .line 95
    const-string v0, "CallLogFragment.onActivityCreated"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 96
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 98
    iget-boolean v0, p0, Laoj;->u:Z

    if-eqz v0, :cond_2

    move v10, v11

    .line 101
    :goto_0
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 102
    new-instance v2, Laqd;

    .line 103
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Luh;

    invoke-static {v0}, Laqj;->a(Luh;)Laqj;

    move-result-object v0

    .line 105
    iget-object v0, v0, Laqj;->a:Lbsu;

    .line 106
    new-instance v3, Lbmm;

    .line 107
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lbmm;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Laoj;->o:Laqf;

    invoke-direct {v2, v0, v3, v1}, Laqd;-><init>(Lbsu;Lbmm;Laqf;)V

    iput-object v2, p0, Laoj;->n:Laqd;

    .line 109
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/ActionMenuView$b;->d(Landroid/content/Context;)Laqv;

    move-result-object v0

    .line 110
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    .line 111
    if-ne v10, v12, :cond_3

    .line 112
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lany;

    move-object v5, v3

    .line 113
    :goto_1
    new-instance v6, Laqc;

    .line 114
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v6, v3}, Laqc;-><init>(Landroid/content/Context;)V

    iget-object v7, p0, Laoj;->n:Laqd;

    .line 115
    invoke-virtual {p0}, Laoj;->d()Latf;

    move-result-object v8

    new-instance v9, Lawr;

    .line 116
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v9, v3}, Lawr;-><init>(Landroid/content/Context;)V

    move-object v3, p0

    move-object v4, p0

    .line 117
    invoke-interface/range {v0 .. v10}, Laqv;->a(Landroid/app/Activity;Landroid/view/ViewGroup;Lanw;Lanx;Lany;Laqc;Laqd;Latf;Lawr;I)Lano;

    move-result-object v0

    iput-object v0, p0, Laoj;->c:Lano;

    .line 118
    iget-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Laoj;->c:Lano;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$a;)V

    .line 119
    iget-object v0, p0, Laoj;->c:Lano;

    invoke-virtual {v0}, Lano;->b()Landroid/support/v7/widget/RecyclerView$i;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Laoj;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Laoj;->c:Lano;

    invoke-virtual {v1}, Lano;->b()Landroid/support/v7/widget/RecyclerView$i;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/RecyclerView$i;)V

    .line 121
    :cond_0
    invoke-virtual {p0}, Laoj;->a()V

    .line 123
    if-eqz p1, :cond_1

    .line 124
    const-string v0, "select_all_mode_checked"

    invoke-virtual {p1, v0, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    invoke-direct {p0}, Laoj;->l()V

    .line 126
    :cond_1
    iget-object v2, p0, Laoj;->c:Lano;

    .line 127
    if-eqz p1, :cond_5

    .line 128
    const-string v0, "expanded_position"

    const/4 v1, -0x1

    .line 129
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v2, Lano;->l:I

    .line 130
    const-string v0, "expanded_row_id"

    const-wide/16 v4, -0x1

    .line 131
    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, v2, Lano;->m:J

    .line 132
    const-string v0, "action_mode_selected_items"

    .line 133
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 134
    if-eqz v3, :cond_5

    .line 135
    const-string v0, "CallLogAdapter.onRestoreInstanceState"

    const-string v1, "restored selectedItemsList:%d"

    new-array v4, v11, [Ljava/lang/Object;

    .line 136
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v13

    .line 137
    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v13

    .line 139
    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 140
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 141
    invoke-static {v0}, Lano;->a(Ljava/lang/String;)I

    move-result v4

    .line 142
    const-string v5, "CallLogAdapter.onRestoreInstanceState"

    const-string v6, "restoring selected index %d, id=%d, uri=%s "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    .line 143
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    .line 144
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    aput-object v0, v7, v12

    .line 145
    invoke-static {v5, v6, v7}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    iget-object v5, v2, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v5, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 147
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v10, v12

    .line 100
    goto/16 :goto_0

    .line 113
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 148
    :cond_4
    const-string v0, "CallLogAdapter.onRestoreInstance"

    const-string v1, "restored selectedItems %s"

    new-array v3, v11, [Ljava/lang/Object;

    iget-object v4, v2, Lano;->r:Landroid/util/SparseArray;

    .line 149
    invoke-virtual {v4}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v13

    .line 150
    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    invoke-virtual {v2}, Lano;->c()V

    .line 152
    :cond_5
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 413
    iget-boolean v0, p0, Laoj;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Laoj;->v:Z

    .line 414
    iget-boolean v0, p0, Laoj;->v:Z

    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cl:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 417
    :goto_1
    invoke-direct {p0}, Laoj;->l()V

    .line 418
    return-void

    .line 413
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 416
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cm:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 25
    const-string v0, "CallLogFragment.onCreate"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 26
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    iput-boolean v5, p0, Laoj;->d:Z

    .line 28
    if-eqz p1, :cond_0

    .line 29
    const-string v0, "filter_type"

    iget v1, p0, Laoj;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Laoj;->r:I

    .line 30
    const-string v0, "log_limit"

    iget v1, p0, Laoj;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Laoj;->s:I

    .line 31
    const-string v0, "date_limit"

    iget-wide v2, p0, Laoj;->t:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Laoj;->t:J

    .line 32
    const-string v0, "is_call_log_activity"

    iget-boolean v1, p0, Laoj;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Laoj;->u:Z

    .line 33
    const-string v0, "has_read_call_log_permission"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Laoj;->p:Z

    .line 34
    const-string v0, "refresh_data_required"

    iget-boolean v1, p0, Laoj;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Laoj;->d:Z

    .line 35
    const-string v0, "select_all_mode_checked"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Laoj;->v:Z

    .line 36
    :cond_0
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 38
    new-instance v2, Lbgf;

    iget v3, p0, Laoj;->s:I

    invoke-direct {v2, v0, v1, p0, v3}, Lbgf;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lbgh;I)V

    iput-object v2, p0, Laoj;->l:Lbgf;

    .line 39
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    sget-object v0, Landroid/provider/CallLog;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Laoj;->f:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 42
    :goto_0
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Laoj;->g:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 45
    :goto_1
    invoke-virtual {p0, v5}, Laoj;->setHasOptionsMenu(Z)V

    .line 46
    return-void

    .line 41
    :cond_1
    const-string v0, "CallLogFragment.onCreate"

    const-string v2, "call log permission not available"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :cond_2
    const-string v0, "CallLogFragment.onCreate"

    const-string v1, "contacts permission not available."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 70
    const v0, 0x7f04002d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 71
    invoke-virtual {p0, v0}, Laoj;->a(Landroid/view/View;)V

    .line 72
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 213
    const-string v0, "CallLogFragment.onDestroy"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Laoj;->c:Lano;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Laoj;->c:Lano;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lano;->b(Landroid/database/Cursor;)V

    .line 216
    :cond_0
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Laoj;->f:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 217
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Laoj;->g:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 218
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 219
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 181
    const-string v0, "CallLogFragment.onPause"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Laoj;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Laoj;->k()V

    .line 185
    :cond_0
    iget-object v0, p0, Laoj;->w:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 186
    iget-object v1, p0, Laoj;->c:Lano;

    .line 187
    invoke-virtual {v1}, Lano;->h()Lbis;

    move-result-object v0

    invoke-interface {v0, v1}, Lbis;->b(Lbix;)V

    .line 189
    iget-object v0, v1, Lano;->k:Laqd;

    .line 190
    invoke-virtual {v0}, Laqd;->b()V

    .line 191
    iget-object v0, v1, Lano;->e:Laqc;

    invoke-virtual {v0}, Laqc;->a()V

    .line 192
    iget-object v0, v1, Lano;->u:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 193
    iget-object v3, v1, Lano;->c:Landroid/app/Activity;

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;Landroid/net/Uri;Laoi;)V

    goto :goto_0

    .line 195
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 196
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 355
    if-ne p1, v1, :cond_0

    .line 356
    array-length v0, p3

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    .line 357
    iput-boolean v1, p0, Laoj;->d:Z

    .line 358
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 156
    const-string v0, "CallLogFragment.onResume"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 157
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 159
    invoke-virtual {p0}, Laoj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "android.permission.READ_CALL_LOG"

    invoke-static {v0, v1}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 160
    iget-boolean v1, p0, Laoj;->p:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 161
    const/4 v1, 0x1

    iput-boolean v1, p0, Laoj;->d:Z

    .line 162
    iget v1, p0, Laoj;->r:I

    invoke-direct {p0, v1}, Laoj;->a(I)V

    .line 163
    :cond_0
    iput-boolean v0, p0, Laoj;->p:Z

    .line 164
    iget-object v0, p0, Laoj;->c:Lano;

    .line 165
    sget-object v0, Lawr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 166
    invoke-virtual {p0}, Laoj;->e()V

    .line 167
    iget-object v0, p0, Laoj;->c:Lano;

    .line 168
    iget-object v1, v0, Lano;->c:Landroid/app/Activity;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-static {v1, v2}, Lbsw;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, v0, Lano;->k:Laqd;

    .line 170
    iget-object v2, v1, Laqd;->f:Laqg;

    if-nez v2, :cond_1

    .line 171
    iget-object v1, v1, Laqd;->d:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 172
    :cond_1
    iget-object v1, v0, Lano;->x:Lalj;

    const-string v2, "android.contacts.DISPLAY_ORDER"

    invoke-virtual {v1, v2}, Lalj;->a(Ljava/lang/String;)V

    .line 173
    iget-object v1, v0, Lano;->c:Landroid/app/Activity;

    invoke-static {v1}, Lbib;->ae(Landroid/content/Context;)Lbsd;

    move-result-object v1

    invoke-interface {v1}, Lbsd;->a()Z

    move-result v1

    iput-boolean v1, v0, Lano;->y:Z

    .line 174
    invoke-virtual {v0}, Lano;->h()Lbis;

    move-result-object v1

    invoke-interface {v1, v0}, Lbis;->a(Lbix;)V

    .line 176
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$a;->a:Landroid/support/v7/widget/RecyclerView$b;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$b;->b()V

    .line 177
    invoke-virtual {p0}, Laoj;->g()V

    .line 178
    invoke-virtual {p0}, Laoj;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    invoke-virtual {p0}, Laoj;->i()V

    .line 180
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 220
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 221
    const-string v0, "filter_type"

    iget v1, p0, Laoj;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 222
    const-string v0, "log_limit"

    iget v1, p0, Laoj;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    const-string v0, "date_limit"

    iget-wide v4, p0, Laoj;->t:J

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 224
    const-string v0, "is_call_log_activity"

    iget-boolean v1, p0, Laoj;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 225
    const-string v0, "has_read_call_log_permission"

    iget-boolean v1, p0, Laoj;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 226
    const-string v0, "refresh_data_required"

    iget-boolean v1, p0, Laoj;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    const-string v0, "select_all_mode_checked"

    iget-boolean v1, p0, Laoj;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 228
    iget-object v0, p0, Laoj;->c:Lano;

    if-eqz v0, :cond_1

    .line 229
    iget-object v3, p0, Laoj;->c:Lano;

    .line 230
    const-string v0, "expanded_position"

    iget v1, v3, Lano;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 231
    const-string v0, "expanded_row_id"

    iget-wide v4, v3, Lano;->m:J

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 232
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 233
    iget-object v0, v3, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v1, v2

    .line 234
    :goto_0
    iget-object v0, v3, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 235
    iget-object v0, v3, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 236
    iget-object v0, v3, Lano;->r:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 237
    const-string v6, "CallLogAdapter.onSaveInstanceState"

    const-string v7, "index %d, id=%d, uri=%s "

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    .line 238
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v10

    aput-object v0, v8, v11

    .line 239
    invoke-static {v6, v7, v8}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 242
    :cond_0
    const-string v0, "action_mode_selected_items"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 243
    const-string v0, "CallLogAdapter.onSaveInstanceState"

    const-string v1, "saved: %d, selectedItemsSize:%d"

    new-array v5, v11, [Ljava/lang/Object;

    .line 244
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    iget-object v2, v3, Lano;->r:Landroid/util/SparseArray;

    .line 245
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v10

    .line 246
    invoke-static {v0, v1, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 197
    const-string v0, "CallLogFragment.onStart"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 198
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 199
    const/4 v0, 0x0

    .line 200
    invoke-virtual {p0}, Laoj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lblq;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    new-instance v0, Lblq;

    invoke-direct {v0}, Lblq;-><init>()V

    .line 203
    :cond_0
    iget-object v1, p0, Laoj;->n:Laqd;

    .line 204
    iput-object v0, v1, Laqd;->e:Lblq;

    .line 205
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 206
    const-string v0, "CallLogFragment.onStop"

    invoke-static {v0}, Lapw;->b(Ljava/lang/String;)V

    .line 207
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 208
    iget-object v0, p0, Laoj;->c:Lano;

    .line 209
    invoke-virtual {v0}, Lano;->g()Lbjf;

    move-result-object v0

    invoke-interface {v0}, Lbjf;->a()V

    .line 210
    iget-object v0, p0, Laoj;->n:Laqd;

    .line 211
    invoke-virtual {v0}, Laqd;->b()V

    .line 212
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 154
    iget v0, p0, Laoj;->r:I

    invoke-direct {p0, v0}, Laoj;->a(I)V

    .line 155
    return-void
.end method

.method public setMenuVisibility(Z)V
    .locals 1

    .prologue
    .line 320
    invoke-super {p0, p1}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 321
    iget-boolean v0, p0, Laoj;->q:Z

    if-eq v0, p1, :cond_0

    .line 322
    iput-boolean p1, p0, Laoj;->q:Z

    .line 323
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Laoj;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0}, Laoj;->e()V

    .line 325
    :cond_0
    return-void
.end method
