.class public final Ldnn;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "PG"


# instance fields
.field private a:Lcjt;

.field private b:Z


# direct methods
.method public constructor <init>(Lcjt;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnn;->b:Z

    .line 3
    iput-object p1, p0, Ldnn;->a:Lcjt;

    .line 4
    return-void
.end method


# virtual methods
.method public final onAvailable(Landroid/net/Network;)V
    .locals 2

    .prologue
    .line 5
    iget-boolean v0, p0, Ldnn;->b:Z

    if-nez v0, :cond_0

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnn;->b:Z

    .line 7
    iget-object v0, p0, Ldnn;->a:Lcjt;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 8
    new-instance v1, Ldno;

    invoke-direct {v1, v0}, Ldno;-><init>(Lcjt;)V

    .line 9
    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 10
    :cond_0
    return-void
.end method

.method public final onLost(Landroid/net/Network;)V
    .locals 2

    .prologue
    .line 11
    iget-boolean v0, p0, Ldnn;->b:Z

    if-eqz v0, :cond_0

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnn;->b:Z

    .line 13
    iget-object v0, p0, Ldnn;->a:Lcjt;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 14
    new-instance v1, Ldnp;

    invoke-direct {v1, v0}, Ldnp;-><init>(Lcjt;)V

    .line 15
    invoke-static {v1}, Lapw;->b(Ljava/lang/Runnable;)V

    .line 16
    :cond_0
    return-void
.end method
