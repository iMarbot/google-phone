.class final Lhup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhui;


# instance fields
.field private a:Lhuh;

.field private b:Lhuu;

.field private c:Z


# direct methods
.method constructor <init>(Lhuu;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lhuh;

    invoke-direct {v0}, Lhuh;-><init>()V

    iput-object v0, p0, Lhup;->a:Lhuh;

    .line 3
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-object p1, p0, Lhup;->b:Lhuu;

    .line 5
    return-void
.end method

.method private a()Lhui;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 25
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iget-object v4, p0, Lhup;->a:Lhuh;

    .line 27
    iget-wide v0, v4, Lhuh;->c:J

    .line 28
    cmp-long v5, v0, v2

    if-nez v5, :cond_3

    move-wide v0, v2

    .line 34
    :cond_1
    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lhup;->b:Lhuu;

    iget-object v3, p0, Lhup;->a:Lhuh;

    invoke-interface {v2, v3, v0, v1}, Lhuu;->a_(Lhuh;J)V

    .line 35
    :cond_2
    return-object p0

    .line 29
    :cond_3
    iget-object v4, v4, Lhuh;->b:Lhur;

    iget-object v4, v4, Lhur;->g:Lhur;

    .line 30
    iget v5, v4, Lhur;->c:I

    const/16 v6, 0x2000

    if-ge v5, v6, :cond_1

    iget-boolean v5, v4, Lhur;->e:Z

    if-eqz v5, :cond_1

    .line 31
    iget v5, v4, Lhur;->c:I

    iget v4, v4, Lhur;->b:I

    sub-int v4, v5, v4

    int-to-long v4, v4

    sub-long/2addr v0, v4

    goto :goto_0
.end method


# virtual methods
.method public final a_(Lhuh;J)V
    .locals 2

    .prologue
    .line 6
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1, p2, p3}, Lhuh;->a_(Lhuh;J)V

    .line 8
    invoke-direct {p0}, Lhup;->a()Lhui;

    .line 9
    return-void
.end method

.method public final b(Ljava/lang/String;)Lhui;
    .locals 2

    .prologue
    .line 10
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 11
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1}, Lhuh;->a(Ljava/lang/String;)Lhuh;

    .line 12
    invoke-direct {p0}, Lhup;->a()Lhui;

    move-result-object v0

    return-object v0
.end method

.method public final b([B)Lhui;
    .locals 2

    .prologue
    .line 13
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1}, Lhuh;->a([B)Lhuh;

    .line 15
    invoke-direct {p0}, Lhup;->a()Lhui;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 6

    .prologue
    .line 41
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const/4 v0, 0x0

    .line 43
    :try_start_0
    iget-object v1, p0, Lhup;->a:Lhuh;

    iget-wide v2, v1, Lhuh;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 44
    iget-object v1, p0, Lhup;->b:Lhuu;

    iget-object v2, p0, Lhup;->a:Lhuh;

    iget-object v3, p0, Lhup;->a:Lhuh;

    iget-wide v4, v3, Lhuh;->c:J

    invoke-interface {v1, v2, v4, v5}, Lhuu;->a_(Lhuh;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 48
    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, Lhup;->b:Lhuu;

    invoke-interface {v1}, Lhuu;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 52
    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lhup;->c:Z

    .line 53
    if-eqz v0, :cond_0

    invoke-static {v0}, Lhuy;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 50
    :catch_0
    move-exception v1

    .line 51
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 46
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final e(I)Lhui;
    .locals 2

    .prologue
    .line 22
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1}, Lhuh;->c(I)Lhuh;

    .line 24
    invoke-direct {p0}, Lhup;->a()Lhui;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)Lhui;
    .locals 2

    .prologue
    .line 19
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1}, Lhuh;->b(I)Lhuh;

    .line 21
    invoke-direct {p0}, Lhup;->a()Lhui;

    move-result-object v0

    return-object v0
.end method

.method public final flush()V
    .locals 4

    .prologue
    .line 36
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    iget-wide v0, v0, Lhuh;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 38
    iget-object v0, p0, Lhup;->b:Lhuu;

    iget-object v1, p0, Lhup;->a:Lhuh;

    iget-object v2, p0, Lhup;->a:Lhuh;

    iget-wide v2, v2, Lhuh;->c:J

    invoke-interface {v0, v1, v2, v3}, Lhuu;->a_(Lhuh;J)V

    .line 39
    :cond_1
    iget-object v0, p0, Lhup;->b:Lhuu;

    invoke-interface {v0}, Lhuu;->flush()V

    .line 40
    return-void
.end method

.method public final g(I)Lhui;
    .locals 2

    .prologue
    .line 16
    iget-boolean v0, p0, Lhup;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iget-object v0, p0, Lhup;->a:Lhuh;

    invoke-virtual {v0, p1}, Lhuh;->a(I)Lhuh;

    .line 18
    invoke-direct {p0}, Lhup;->a()Lhui;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhup;->b:Lhuu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
