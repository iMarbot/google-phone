.class public final Lfvg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final clearcutWrapper:Lfuu;

.field public final markReporter:Lfwl;

.field public reportSent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfuu;[I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lfvg;->clearcutWrapper:Lfuu;

    .line 3
    new-instance v0, Lfwl;

    invoke-direct {v0, p3}, Lfwl;-><init>([I)V

    iput-object v0, p0, Lfvg;->markReporter:Lfwl;

    .line 4
    return-void
.end method


# virtual methods
.method public final mark(I)V
    .locals 4

    .prologue
    .line 5
    iget-object v0, p0, Lfvg;->markReporter:Lfwl;

    .line 6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lfwl;->a(IJ)V

    .line 7
    return-void
.end method

.method public final markAt(IJ)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lfvg;->markReporter:Lfwl;

    invoke-virtual {v0, p1, p2, p3}, Lfwl;->a(IJ)V

    .line 9
    return-void
.end method

.method public final report()V
    .locals 8

    .prologue
    .line 10
    iget-boolean v0, p0, Lfvg;->reportSent:Z

    if-eqz v0, :cond_0

    .line 11
    const-string v0, "Report already sent - not sending again."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 29
    :goto_0
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvg;->reportSent:Z

    .line 14
    const-string v0, "Reporting vclib marks."

    invoke-static {v0}, Lfvh;->logi(Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lfvg;->clearcutWrapper:Lfuu;

    invoke-virtual {v0}, Lfuu;->createDefaultLogRequest()Lgsi;

    move-result-object v2

    .line 16
    iget-object v3, v2, Lgsi;->a:Lgrw;

    iget-object v4, p0, Lfvg;->markReporter:Lfwl;

    .line 17
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 18
    iget-object v0, v4, Lfwl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 19
    new-instance v7, Lgsl;

    invoke-direct {v7}, Lgsl;-><init>()V

    .line 20
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iput-object v1, v7, Lgsl;->a:Ljava/lang/Integer;

    .line 21
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v7, Lgsl;->b:Ljava/lang/Long;

    .line 22
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 24
    :cond_1
    iget-object v1, v4, Lfwl;->b:Lgsj;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lgsl;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgsl;

    iput-object v0, v1, Lgsj;->a:[Lgsl;

    .line 25
    iget-object v0, v4, Lfwl;->b:Lgsj;

    .line 26
    iput-object v0, v3, Lgrw;->e:Lgsj;

    .line 27
    iget-object v0, p0, Lfvg;->clearcutWrapper:Lfuu;

    invoke-virtual {v0}, Lfuu;->getCallbacks()Lfvt;

    move-result-object v0

    invoke-virtual {v0, v2}, Lfvt;->onHangoutLogRequestPrepared(Lgsi;)V

    .line 28
    iget-object v0, p0, Lfvg;->clearcutWrapper:Lfuu;

    invoke-virtual {v0, v2}, Lfuu;->logHangoutLogRequest(Lgsi;)V

    goto :goto_0
.end method
