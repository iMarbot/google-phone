.class public final enum Lhbr$e;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhbr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "e"
.end annotation


# static fields
.field public static final enum a:Lhbr$e;

.field public static final enum b:Lhbr$e;

.field public static final enum c:Lhbr$e;

.field public static final enum d:Lhbr$e;

.field public static final enum e:Lhbr$e;

.field public static final enum f:Lhbr$e;

.field public static final enum g:Lhbr$e;

.field public static final enum h:Lhbr$e;

.field public static final enum i:Lhbr$e;

.field private static synthetic j:[Lhbr$e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3
    new-instance v0, Lhbr$e;

    const-string v1, "IS_INITIALIZED"

    invoke-direct {v0, v1, v3}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->a:Lhbr$e;

    .line 4
    new-instance v0, Lhbr$e;

    const-string v1, "GET_MEMOIZED_IS_INITIALIZED"

    invoke-direct {v0, v1, v4}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->b:Lhbr$e;

    .line 5
    new-instance v0, Lhbr$e;

    const-string v1, "SET_MEMOIZED_IS_INITIALIZED"

    invoke-direct {v0, v1, v5}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->c:Lhbr$e;

    .line 6
    new-instance v0, Lhbr$e;

    const-string v1, "MERGE_FROM_STREAM"

    invoke-direct {v0, v1, v6}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->d:Lhbr$e;

    .line 7
    new-instance v0, Lhbr$e;

    const-string v1, "MAKE_IMMUTABLE"

    invoke-direct {v0, v1, v7}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->e:Lhbr$e;

    .line 8
    new-instance v0, Lhbr$e;

    const-string v1, "NEW_MUTABLE_INSTANCE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->f:Lhbr$e;

    .line 9
    new-instance v0, Lhbr$e;

    const-string v1, "NEW_BUILDER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->g:Lhbr$e;

    .line 10
    new-instance v0, Lhbr$e;

    const-string v1, "GET_DEFAULT_INSTANCE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->h:Lhbr$e;

    .line 11
    new-instance v0, Lhbr$e;

    const-string v1, "GET_PARSER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lhbr$e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhbr$e;->i:Lhbr$e;

    .line 12
    const/16 v0, 0x9

    new-array v0, v0, [Lhbr$e;

    sget-object v1, Lhbr$e;->a:Lhbr$e;

    aput-object v1, v0, v3

    sget-object v1, Lhbr$e;->b:Lhbr$e;

    aput-object v1, v0, v4

    sget-object v1, Lhbr$e;->c:Lhbr$e;

    aput-object v1, v0, v5

    sget-object v1, Lhbr$e;->d:Lhbr$e;

    aput-object v1, v0, v6

    sget-object v1, Lhbr$e;->e:Lhbr$e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhbr$e;->f:Lhbr$e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhbr$e;->g:Lhbr$e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhbr$e;->h:Lhbr$e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhbr$e;->i:Lhbr$e;

    aput-object v2, v0, v1

    sput-object v0, Lhbr$e;->j:[Lhbr$e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhbr$e;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhbr$e;->j:[Lhbr$e;

    invoke-virtual {v0}, [Lhbr$e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhbr$e;

    return-object v0
.end method
