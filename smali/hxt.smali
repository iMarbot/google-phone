.class public final Lhxt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhxy;


# static fields
.field private static a:Ljava/util/BitSet;


# instance fields
.field private b:Lhyh;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    sput-object v0, Lhxt;->a:Ljava/util/BitSet;

    .line 47
    const/16 v0, 0x21

    :goto_0
    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    .line 48
    sget-object v1, Lhxt;->a:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    const/16 v0, 0x3b

    :goto_1
    const/16 v1, 0x7e

    if-gt v0, v1, :cond_1

    .line 51
    sget-object v1, Lhxt;->a:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53
    :cond_1
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lhyh;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lhyh;-><init>(I)V

    iput-object v0, p0, Lhxt;->b:Lhyh;

    .line 3
    iput p1, p0, Lhxt;->c:I

    .line 4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 5
    iget-object v0, p0, Lhxt;->b:Lhyh;

    .line 6
    const/4 v1, 0x0

    iput v1, v0, Lhyh;->b:I

    .line 7
    return-void
.end method

.method public final a(Lhyh;)V
    .locals 4

    .prologue
    .line 8
    if-nez p1, :cond_0

    .line 22
    :goto_0
    return-void

    .line 11
    :cond_0
    iget v0, p1, Lhyh;->b:I

    .line 13
    iget v1, p0, Lhxt;->c:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lhxt;->b:Lhyh;

    .line 14
    iget v1, v1, Lhyh;->b:I

    .line 15
    add-int/2addr v0, v1

    iget v1, p0, Lhxt;->c:I

    if-lt v0, v1, :cond_1

    .line 16
    new-instance v0, Lhxk;

    const-string v1, "Maximum header length limit exceeded"

    invoke-direct {v0, v1}, Lhxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_1
    iget-object v0, p0, Lhxt;->b:Lhyh;

    .line 18
    iget-object v1, p1, Lhyh;->a:[B

    .line 19
    const/4 v2, 0x0

    .line 20
    iget v3, p1, Lhyh;->b:I

    .line 21
    invoke-virtual {v0, v1, v2, v3}, Lhyh;->a([BII)V

    goto :goto_0
.end method

.method public final b()Lhyf;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 23
    iget-object v0, p0, Lhxt;->b:Lhyh;

    .line 24
    iget v0, v0, Lhyh;->b:I

    .line 26
    if-lez v0, :cond_1

    .line 27
    iget-object v2, p0, Lhxt;->b:Lhyh;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Lhyh;->b(I)B

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    .line 28
    add-int/lit8 v0, v0, -0x1

    .line 29
    :cond_0
    iget-object v2, p0, Lhxt;->b:Lhyh;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Lhyh;->b(I)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_1

    .line 30
    add-int/lit8 v0, v0, -0x1

    .line 31
    :cond_1
    new-instance v2, Lhyh;

    iget-object v3, p0, Lhxt;->b:Lhyh;

    .line 32
    iget-object v3, v3, Lhyh;->a:[B

    .line 33
    invoke-direct {v2, v3, v0, v1}, Lhyh;-><init>([BIZ)V

    .line 34
    sget-object v0, Lhyg;->b:Lhyg;

    invoke-virtual {v0, v2}, Lhyg;->a(Lhyi;)Lhyf;

    move-result-object v2

    .line 36
    iget-object v3, v2, Lhyf;->c:Ljava/lang/String;

    move v0, v1

    .line 38
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 39
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 40
    sget-object v4, Lhxt;->a:Ljava/util/BitSet;

    invoke-virtual {v4, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 41
    new-instance v0, Lhvc;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "MIME field name contains illegal characters: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 42
    iget-object v2, v2, Lhyf;->c:Ljava/lang/String;

    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lhvc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_3
    return-object v2
.end method
