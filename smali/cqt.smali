.class final synthetic Lcqt;
.super Ljava/lang/Object;

# interfaces
.implements Lcqr;


# instance fields
.field private a:Lcqs;


# direct methods
.method constructor <init>(Lcqs;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcqt;->a:Lcqs;

    return-void
.end method


# virtual methods
.method public final a(Lcqy;)Lcrc;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1
    iget-object v2, p0, Lcqt;->a:Lcqs;

    .line 3
    sget-object v0, Lgzp;->f:Lgzp;

    invoke-virtual {v0}, Lgzp;->createBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 4
    iget-object v3, v2, Lcqs;->f:Lhah;

    .line 5
    invoke-virtual {v0, v3}, Lhbr$a;->a(Lhah;)Lhbr$a;

    move-result-object v0

    iget-object v3, v2, Lcqs;->g:Lgzi;

    .line 6
    invoke-virtual {v0, v3}, Lhbr$a;->a(Lgzi;)Lhbr$a;

    move-result-object v3

    .line 8
    iget-object v0, v2, Lcqs;->d:Landroid/telecom/PhoneAccountHandle;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcqs;->a:Landroid/content/Context;

    .line 9
    invoke-static {v0}, Lclp;->a(Landroid/content/Context;)Lclp;

    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lclp;->a()Lcln;

    move-result-object v0

    iget-object v4, v2, Lcqs;->a:Landroid/content/Context;

    iget-object v5, v2, Lcqs;->d:Landroid/telecom/PhoneAccountHandle;

    .line 11
    invoke-interface {v0, v4, v5}, Lcln;->d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 12
    :goto_0
    if-eqz v0, :cond_3

    sget-object v0, Lgzk;->b:Lgzk;

    .line 13
    :goto_1
    invoke-virtual {v3, v0}, Lhbr$a;->a(Lgzk;)Lhbr$a;

    move-result-object v0

    .line 14
    iget-object v3, v2, Lcqs;->e:Lcqn;

    .line 15
    iget-object v3, v3, Lcqn;->a:Landroid/content/Context;

    invoke-static {v3}, Lapw;->u(Landroid/content/Context;)Lbew;

    move-result-object v3

    const-string v4, "voicemail_transcription_client_generated_voicemail_ids"

    .line 16
    invoke-interface {v3, v4, v1}, Lbew;->a(Ljava/lang/String;Z)Z

    move-result v1

    .line 17
    if-nez v1, :cond_0

    iget-object v1, v2, Lcqs;->e:Lcqn;

    .line 18
    invoke-virtual {v1}, Lcqn;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19
    :cond_0
    iget-object v1, v2, Lcqs;->f:Lhah;

    invoke-static {v1}, Lcom/android/voicemail/impl/scheduling/BaseTask$a;->a(Lhah;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhbr$a;->w(Ljava/lang/String;)Lhbr$a;

    .line 20
    :cond_1
    invoke-virtual {v0}, Lhbr$a;->build()Lhbr;

    move-result-object v0

    check-cast v0, Lgzp;

    .line 21
    invoke-virtual {p1, v0}, Lcqy;->a(Lgzp;)Lcrd;

    move-result-object v0

    .line 22
    return-object v0

    :cond_2
    move v0, v1

    .line 11
    goto :goto_0

    .line 12
    :cond_3
    sget-object v0, Lgzk;->a:Lgzk;

    goto :goto_1
.end method
