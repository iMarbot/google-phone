.class public final Lboy;
.super Landroid/content/CursorLoader;
.source "PG"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1
    .line 2
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/Context;)Lagx;

    move-result-object v0

    .line 3
    invoke-interface {v0}, Lagx;->a()Landroid/net/Uri;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 5
    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const-string v2, "3"

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 8
    sget-object v3, Lbnz;->c:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    invoke-static {p3}, Lboy;->a(Ljava/util/List;)J

    move-result-wide v0

    iput-wide v0, p0, Lboy;->a:J

    .line 10
    return-void
.end method

.method private static a(Ljava/util/List;)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x1

    .line 15
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    const-wide v0, 0x7fffffffffffffffL

    .line 23
    :goto_0
    return-wide v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    move v4, v0

    move-wide v2, v6

    :goto_1
    if-ge v4, v5, :cond_1

    .line 19
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    .line 20
    cmp-long v8, v0, v2

    if-lez v8, :cond_2

    .line 22
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_1

    .line 23
    :cond_1
    add-long v0, v2, v6

    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 11
    iget-wide v0, p0, Lboy;->a:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 12
    const-string v0, "NearbyPlacesCursorLoader.loadInBackground"

    const-string v1, "directory id not set."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    const/4 v0, 0x0

    .line 14
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lboy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v1

    iget-wide v2, p0, Lboy;->a:J

    invoke-static {v0, v1, v2, v3}, Lbox;->a(Landroid/content/Context;Landroid/database/Cursor;J)Lbox;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lboy;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
