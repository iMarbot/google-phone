.class public Lvq;
.super Ltv;
.source "PG"

# interfaces
.implements Landroid/support/v7/widget/ActionBarOverlayLayout$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvq$a;
    }
.end annotation


# static fields
.field private static o:Landroid/view/animation/Interpolator;

.field private static p:Landroid/view/animation/Interpolator;


# instance fields
.field private A:Lrz;

.field private B:Lsb;

.field public a:Landroid/content/Context;

.field public b:Landroid/support/v7/widget/ActionBarOverlayLayout;

.field public c:Landroid/support/v7/widget/ActionBarContainer;

.field public d:Laao;

.field public e:Landroid/support/v7/widget/ActionBarContextView;

.field public f:Landroid/view/View;

.field public g:Lvq$a;

.field public h:Lwf;

.field public i:Lwg;

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Lwp;

.field public n:Z

.field private q:Landroid/content/Context;

.field private r:Z

.field private s:Z

.field private t:Ljava/util/ArrayList;

.field private u:Z

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lrz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 320
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lvq;->o:Landroid/view/animation/Interpolator;

    .line 321
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lvq;->p:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Ltv;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvq;->t:Ljava/util/ArrayList;

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lvq;->v:I

    .line 5
    iput-boolean v1, p0, Lvq;->j:Z

    .line 6
    iput-boolean v1, p0, Lvq;->x:Z

    .line 7
    new-instance v0, Lvr;

    invoke-direct {v0, p0}, Lvr;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->z:Lrz;

    .line 8
    new-instance v0, Lvs;

    invoke-direct {v0, p0}, Lvs;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->A:Lrz;

    .line 9
    new-instance v0, Lsb;

    invoke-direct {v0, p0}, Lsb;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->B:Lsb;

    .line 10
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 12
    invoke-direct {p0, v0}, Lvq;->a(Landroid/view/View;)V

    .line 13
    if-nez p2, :cond_0

    .line 14
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lvq;->f:Landroid/view/View;

    .line 15
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 16
    invoke-direct {p0}, Ltv;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lvq;->t:Ljava/util/ArrayList;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lvq;->v:I

    .line 20
    iput-boolean v1, p0, Lvq;->j:Z

    .line 21
    iput-boolean v1, p0, Lvq;->x:Z

    .line 22
    new-instance v0, Lvr;

    invoke-direct {v0, p0}, Lvr;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->z:Lrz;

    .line 23
    new-instance v0, Lvs;

    invoke-direct {v0, p0}, Lvs;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->A:Lrz;

    .line 24
    new-instance v0, Lsb;

    invoke-direct {v0, p0}, Lsb;-><init>(Lvq;)V

    iput-object v0, p0, Lvq;->B:Lsb;

    .line 25
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lvq;->a(Landroid/view/View;)V

    .line 26
    return-void
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->l()I

    move-result v0

    .line 125
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 126
    const/4 v1, 0x1

    iput-boolean v1, p0, Lvq;->r:Z

    .line 127
    :cond_0
    iget-object v1, p0, Lvq;->d:Laao;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Laao;->a(I)V

    .line 128
    return-void
.end method

.method private final a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    const v0, 0x7f0e00bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    .line 28
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    .line 30
    iput-object p0, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/support/v7/widget/ActionBarOverlayLayout$a;

    .line 31
    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 32
    iget-object v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/support/v7/widget/ActionBarOverlayLayout$a;

    iget v4, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->a:I

    invoke-interface {v3, v4}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->d(I)V

    .line 33
    iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:I

    if-eqz v3, :cond_0

    .line 34
    iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:I

    .line 35
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarOverlayLayout;->onWindowSystemUiVisibilityChanged(I)V

    .line 37
    sget-object v3, Lqy;->a:Lri;

    invoke-virtual {v3, v0}, Lri;->g(Landroid/view/View;)V

    .line 38
    :cond_0
    const v0, 0x7f0e00c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 39
    instance-of v3, v0, Laao;

    if-eqz v3, :cond_2

    .line 40
    check-cast v0, Laao;

    .line 45
    :goto_0
    iput-object v0, p0, Lvq;->d:Laao;

    .line 46
    const v0, 0x7f0e00c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionBarContextView;

    iput-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    .line 47
    const v0, 0x7f0e00c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ActionBarContainer;

    iput-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    .line 48
    iget-object v0, p0, Lvq;->d:Laao;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    if-nez v0, :cond_5

    .line 49
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_2
    instance-of v3, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v3, :cond_3

    .line 42
    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->i()Laao;

    move-result-object v0

    goto :goto_0

    .line 43
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t make a decor toolbar out of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 44
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const-string v0, "null"

    goto :goto_1

    .line 50
    :cond_5
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lvq;->a:Landroid/content/Context;

    .line 51
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->l()I

    move-result v0

    .line 52
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v0, v1

    .line 53
    :goto_2
    if-eqz v0, :cond_6

    .line 54
    iput-boolean v1, p0, Lvq;->r:Z

    .line 55
    :cond_6
    iget-object v0, p0, Lvq;->a:Landroid/content/Context;

    invoke-static {v0}, Lwe;->a(Landroid/content/Context;)Lwe;

    move-result-object v0

    .line 57
    iget-object v3, v0, Lwe;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_7

    .line 59
    :cond_7
    invoke-virtual {v0}, Lwe;->b()Z

    move-result v0

    invoke-direct {p0, v0}, Lvq;->j(Z)V

    .line 60
    iget-object v0, p0, Lvq;->a:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, Lvu;->a:[I

    const v5, 0x7f01004c

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    sget v3, Lvu;->m:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 63
    iget-object v3, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    .line 64
    iget-boolean v3, v3, Landroid/support/v7/widget/ActionBarOverlayLayout;->c:Z

    .line 65
    if-nez v3, :cond_9

    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move v0, v2

    .line 52
    goto :goto_2

    .line 67
    :cond_9
    iput-boolean v1, p0, Lvq;->n:Z

    .line 68
    iget-object v3, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Z)V

    .line 69
    :cond_a
    sget v1, Lvu;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 70
    if-eqz v1, :cond_b

    .line 71
    int-to-float v1, v1

    invoke-virtual {p0, v1}, Lvq;->a(F)V

    .line 72
    :cond_b
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 73
    return-void
.end method

.method static a(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 174
    if-eqz p2, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 177
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final j(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 78
    iput-boolean p1, p0, Lvq;->u:Z

    .line 79
    iget-boolean v0, p0, Lvq;->u:Z

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v3}, Laao;->a(Ladf;)V

    .line 81
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->a(Ladf;)V

    .line 85
    :goto_0
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->m()I

    move-result v0

    .line 86
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 87
    :goto_1
    iget-object v4, p0, Lvq;->d:Laao;

    iget-boolean v3, p0, Lvq;->u:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    move v3, v1

    :goto_2
    invoke-virtual {v4, v3}, Laao;->a(Z)V

    .line 88
    iget-object v3, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, Lvq;->u:Z

    if-nez v4, :cond_3

    if-eqz v0, :cond_3

    .line 89
    :goto_3
    iput-boolean v1, v3, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Z

    .line 90
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->a(Ladf;)V

    .line 83
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v3}, Laao;->a(Ladf;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v3, v2

    .line 87
    goto :goto_2

    :cond_3
    move v1, v2

    .line 88
    goto :goto_3
.end method

.method private final k(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179
    iget-boolean v0, p0, Lvq;->l:Z

    iget-boolean v1, p0, Lvq;->w:Z

    invoke-static {v2, v0, v1}, Lvq;->a(ZZZ)Z

    move-result v0

    .line 180
    if-eqz v0, :cond_7

    .line 181
    iget-boolean v0, p0, Lvq;->x:Z

    if-nez v0, :cond_4

    .line 182
    iput-boolean v5, p0, Lvq;->x:Z

    .line 184
    iget-object v0, p0, Lvq;->m:Lwp;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lvq;->m:Lwp;

    invoke-virtual {v0}, Lwp;->b()V

    .line 186
    :cond_0
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionBarContainer;->setVisibility(I)V

    .line 187
    iget v0, p0, Lvq;->v:I

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lvq;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_5

    .line 188
    :cond_1
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/ActionBarContainer;->setTranslationY(F)V

    .line 189
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 190
    if-eqz p1, :cond_2

    .line 191
    new-array v1, v6, [I

    fill-array-data v1, :array_0

    .line 192
    iget-object v2, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 193
    aget v1, v1, v5

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 194
    :cond_2
    iget-object v1, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ActionBarContainer;->setTranslationY(F)V

    .line 195
    new-instance v1, Lwp;

    invoke-direct {v1}, Lwp;-><init>()V

    .line 196
    iget-object v2, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-static {v2}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v2

    invoke-virtual {v2, v4}, Lrv;->b(F)Lrv;

    move-result-object v2

    .line 197
    iget-object v3, p0, Lvq;->B:Lsb;

    invoke-virtual {v2, v3}, Lrv;->a(Lsb;)Lrv;

    .line 198
    invoke-virtual {v1, v2}, Lwp;->a(Lrv;)Lwp;

    .line 199
    iget-boolean v2, p0, Lvq;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lvq;->f:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 200
    iget-object v2, p0, Lvq;->f:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 201
    iget-object v0, p0, Lvq;->f:Landroid/view/View;

    invoke-static {v0}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v0

    invoke-virtual {v0, v4}, Lrv;->b(F)Lrv;

    move-result-object v0

    invoke-virtual {v1, v0}, Lwp;->a(Lrv;)Lwp;

    .line 202
    :cond_3
    sget-object v0, Lvq;->p:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Lwp;->a(Landroid/view/animation/Interpolator;)Lwp;

    .line 203
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lwp;->a(J)Lwp;

    .line 204
    iget-object v0, p0, Lvq;->A:Lrz;

    invoke-virtual {v1, v0}, Lwp;->a(Lrz;)Lwp;

    .line 205
    iput-object v1, p0, Lvq;->m:Lwp;

    .line 206
    invoke-virtual {v1}, Lwp;->a()V

    .line 213
    :goto_0
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_4

    .line 214
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    .line 215
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->g(Landroid/view/View;)V

    .line 243
    :cond_4
    :goto_1
    return-void

    .line 208
    :cond_5
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->setAlpha(F)V

    .line 209
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/ActionBarContainer;->setTranslationY(F)V

    .line 210
    iget-boolean v0, p0, Lvq;->j:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lvq;->f:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 211
    iget-object v0, p0, Lvq;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 212
    :cond_6
    iget-object v0, p0, Lvq;->A:Lrz;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lrz;->b(Landroid/view/View;)V

    goto :goto_0

    .line 217
    :cond_7
    iget-boolean v0, p0, Lvq;->x:Z

    if-eqz v0, :cond_4

    .line 218
    iput-boolean v2, p0, Lvq;->x:Z

    .line 220
    iget-object v0, p0, Lvq;->m:Lwp;

    if-eqz v0, :cond_8

    .line 221
    iget-object v0, p0, Lvq;->m:Lwp;

    invoke-virtual {v0}, Lwp;->b()V

    .line 222
    :cond_8
    iget v0, p0, Lvq;->v:I

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lvq;->y:Z

    if-nez v0, :cond_9

    if-eqz p1, :cond_c

    .line 223
    :cond_9
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->setAlpha(F)V

    .line 224
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/ActionBarContainer;->a(Z)V

    .line 225
    new-instance v1, Lwp;

    invoke-direct {v1}, Lwp;-><init>()V

    .line 226
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 227
    if-eqz p1, :cond_a

    .line 228
    new-array v2, v6, [I

    fill-array-data v2, :array_1

    .line 229
    iget-object v3, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 230
    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 231
    :cond_a
    iget-object v2, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-static {v2}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v2

    invoke-virtual {v2, v0}, Lrv;->b(F)Lrv;

    move-result-object v2

    .line 232
    iget-object v3, p0, Lvq;->B:Lsb;

    invoke-virtual {v2, v3}, Lrv;->a(Lsb;)Lrv;

    .line 233
    invoke-virtual {v1, v2}, Lwp;->a(Lrv;)Lwp;

    .line 234
    iget-boolean v2, p0, Lvq;->j:Z

    if-eqz v2, :cond_b

    iget-object v2, p0, Lvq;->f:Landroid/view/View;

    if-eqz v2, :cond_b

    .line 235
    iget-object v2, p0, Lvq;->f:Landroid/view/View;

    invoke-static {v2}, Lqy;->b(Landroid/view/View;)Lrv;

    move-result-object v2

    invoke-virtual {v2, v0}, Lrv;->b(F)Lrv;

    move-result-object v0

    invoke-virtual {v1, v0}, Lwp;->a(Lrv;)Lwp;

    .line 236
    :cond_b
    sget-object v0, Lvq;->o:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v0}, Lwp;->a(Landroid/view/animation/Interpolator;)Lwp;

    .line 237
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lwp;->a(J)Lwp;

    .line 238
    iget-object v0, p0, Lvq;->z:Lrz;

    invoke-virtual {v1, v0}, Lwp;->a(Lrz;)Lwp;

    .line 239
    iput-object v1, p0, Lvq;->m:Lwp;

    .line 240
    invoke-virtual {v1}, Lwp;->a()V

    goto/16 :goto_1

    .line 242
    :cond_c
    iget-object v0, p0, Lvq;->z:Lrz;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lrz;->b(Landroid/view/View;)V

    goto/16 :goto_1

    .line 191
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 228
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->n()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lwg;)Lwf;
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lvq;->g:Lvq$a;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lvq;->g:Lvq$a;

    invoke-virtual {v0}, Lvq$a;->c()V

    .line 147
    :cond_0
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Z)V

    .line 148
    iget-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->b()V

    .line 149
    new-instance v0, Lvq$a;

    iget-object v1, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lvq$a;-><init>(Lvq;Landroid/content/Context;Lwg;)V

    .line 150
    invoke-virtual {v0}, Lvq$a;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    iput-object v0, p0, Lvq;->g:Lvq$a;

    .line 152
    invoke-virtual {v0}, Lvq$a;->d()V

    .line 153
    iget-object v1, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ActionBarContextView;->a(Lwf;)V

    .line 154
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lvq;->i(Z)V

    .line 155
    iget-object v1, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 157
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    invoke-static {v0, p1}, Lqy;->a(Landroid/view/View;F)V

    .line 75
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0}, Lvq;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lvq;->d:Laao;

    .line 106
    invoke-virtual {v1}, Laao;->a()Landroid/view/ViewGroup;

    move-result-object v1

    const/4 v2, 0x0

    .line 107
    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lvq;->d:Laao;

    invoke-virtual {v1, v0}, Laao;->a(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lvq;->a:Landroid/content/Context;

    invoke-static {v0}, Lwe;->a(Landroid/content/Context;)Lwe;

    move-result-object v0

    invoke-virtual {v0}, Lwe;->b()Z

    move-result v0

    invoke-direct {p0, v0}, Lvq;->j(Z)V

    .line 77
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lvq;->d:Laao;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laao;->a(Landroid/graphics/drawable/Drawable;)V

    .line 304
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, p1}, Laao;->b(Ljava/lang/CharSequence;)V

    .line 121
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 110
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvq;->a(II)V

    .line 111
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 308
    iget-object v0, p0, Lvq;->g:Lvq$a;

    if-nez v0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v2

    .line 310
    :cond_1
    iget-object v0, p0, Lvq;->g:Lvq$a;

    .line 311
    iget-object v3, v0, Lvq$a;->a:Lxf;

    .line 313
    if-eqz v3, :cond_0

    .line 314
    if-eqz p2, :cond_2

    .line 315
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    .line 316
    :goto_1
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v1, :cond_3

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/Menu;->setQwertyMode(Z)V

    .line 318
    invoke-interface {v3, p1, p2, v2}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v2

    goto :goto_0

    .line 315
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 317
    goto :goto_2
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->l()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lvq;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lvq;->a(Ljava/lang/CharSequence;)V

    .line 119
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 129
    iget-object v2, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    .line 130
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 131
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 132
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ActionBarContainer;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    :cond_0
    iput-object p1, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    .line 134
    if-eqz p1, :cond_1

    .line 135
    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 136
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->b:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 137
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    iget-object v4, v2, Landroid/support/v7/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget-object v5, v2, Landroid/support/v7/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    iget-object v6, v2, Landroid/support/v7/widget/ActionBarContainer;->b:Landroid/view/View;

    .line 138
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    iget-object v7, v2, Landroid/support/v7/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 139
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 140
    :cond_1
    iget-boolean v3, v2, Landroid/support/v7/widget/ActionBarContainer;->f:Z

    if-eqz v3, :cond_4

    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->e:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_3

    :cond_2
    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ActionBarContainer;->setWillNotDraw(Z)V

    .line 141
    invoke-virtual {v2}, Landroid/support/v7/widget/ActionBarContainer;->invalidate()V

    .line 142
    return-void

    :cond_3
    move v0, v1

    .line 140
    goto :goto_0

    :cond_4
    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_5

    iget-object v3, v2, Landroid/support/v7/widget/ActionBarContainer;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, p1}, Laao;->a(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 112
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvq;->a(II)V

    .line 113
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Landroid/content/Context;
    .locals 4

    .prologue
    .line 286
    iget-object v0, p0, Lvq;->q:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 287
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 288
    iget-object v1, p0, Lvq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 289
    const v2, 0x7f01004f

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 290
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 291
    if-eqz v0, :cond_1

    .line 292
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lvq;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lvq;->q:Landroid/content/Context;

    .line 294
    :cond_0
    :goto_0
    iget-object v0, p0, Lvq;->q:Landroid/content/Context;

    return-object v0

    .line 293
    :cond_1
    iget-object v0, p0, Lvq;->a:Landroid/content/Context;

    iput-object v0, p0, Lvq;->q:Landroid/content/Context;

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 168
    if-eqz p1, :cond_0

    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    .line 169
    iget-boolean v0, v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->c:Z

    .line 170
    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(I)V

    .line 173
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    const/16 v0, 0x8

    .line 114
    invoke-direct {p0, v0, v0}, Lvq;->a(II)V

    .line 115
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lvq;->v:I

    .line 92
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 116
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1}, Lvq;->a(II)V

    .line 117
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lvq;->r:Z

    if-nez v0, :cond_0

    .line 306
    invoke-virtual {p0, p1}, Lvq;->b(Z)V

    .line 307
    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 93
    iput-boolean p1, p0, Lvq;->y:Z

    .line 94
    if-nez p1, :cond_0

    iget-object v0, p0, Lvq;->m:Lwp;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lvq;->m:Lwp;

    invoke-virtual {v0}, Lwp;->b()V

    .line 96
    :cond_0
    return-void
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    .line 97
    iget-boolean v0, p0, Lvq;->s:Z

    if-ne p1, v0, :cond_1

    .line 104
    :cond_0
    return-void

    .line 99
    :cond_1
    iput-boolean p1, p0, Lvq;->s:Z

    .line 100
    iget-object v0, p0, Lvq;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 101
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 102
    iget-object v2, p0, Lvq;->t:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lvq;->d:Laao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0}, Laao;->d()V

    .line 301
    const/4 v0, 0x1

    .line 302
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 0

    .prologue
    .line 158
    iput-boolean p1, p0, Lvq;->j:Z

    .line 159
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lvq;->l:Z

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lvq;->l:Z

    .line 162
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lvq;->k(Z)V

    .line 163
    :cond_0
    return-void
.end method

.method public final i(Z)V
    .locals 10

    .prologue
    const-wide/16 v8, 0xc8

    const-wide/16 v6, 0x64

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 244
    if-eqz p1, :cond_3

    .line 246
    iget-boolean v0, p0, Lvq;->w:Z

    if-nez v0, :cond_1

    .line 247
    const/4 v0, 0x1

    iput-boolean v0, p0, Lvq;->w:Z

    .line 248
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 249
    invoke-static {}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a()V

    .line 250
    :cond_0
    invoke-direct {p0, v2}, Lvq;->k(Z)V

    .line 259
    :cond_1
    :goto_0
    iget-object v0, p0, Lvq;->c:Landroid/support/v7/widget/ActionBarContainer;

    .line 260
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->q(Landroid/view/View;)Z

    move-result v0

    .line 261
    if-eqz v0, :cond_7

    .line 262
    if-eqz p1, :cond_5

    .line 263
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v3, v6, v7}, Laao;->a(IJ)Lrv;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v1, v2, v8, v9}, Landroid/support/v7/widget/ActionBarContextView;->a(IJ)Lrv;

    move-result-object v1

    .line 267
    :goto_1
    new-instance v4, Lwp;

    invoke-direct {v4}, Lwp;-><init>()V

    .line 269
    iget-object v2, v4, Lwp;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v0, v0, Lrv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_6

    .line 272
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v2

    .line 275
    :goto_2
    iget-object v0, v1, Lrv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_2

    .line 276
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 277
    :cond_2
    iget-object v0, v4, Lwp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    invoke-virtual {v4}, Lwp;->a()V

    .line 285
    :goto_3
    return-void

    .line 253
    :cond_3
    iget-boolean v0, p0, Lvq;->w:Z

    if-eqz v0, :cond_1

    .line 254
    iput-boolean v2, p0, Lvq;->w:Z

    .line 255
    iget-object v0, p0, Lvq;->b:Landroid/support/v7/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_4

    .line 256
    invoke-static {}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a()V

    .line 257
    :cond_4
    invoke-direct {p0, v2}, Lvq;->k(Z)V

    goto :goto_0

    .line 265
    :cond_5
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v2, v8, v9}, Laao;->a(IJ)Lrv;

    move-result-object v1

    .line 266
    iget-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v4, v6, v7}, Landroid/support/v7/widget/ActionBarContextView;->a(IJ)Lrv;

    move-result-object v0

    goto :goto_1

    .line 273
    :cond_6
    const-wide/16 v2, 0x0

    goto :goto_2

    .line 280
    :cond_7
    if-eqz p1, :cond_8

    .line 281
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v3}, Laao;->b(I)V

    .line 282
    iget-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionBarContextView;->setVisibility(I)V

    goto :goto_3

    .line 283
    :cond_8
    iget-object v0, p0, Lvq;->d:Laao;

    invoke-virtual {v0, v2}, Laao;->b(I)V

    .line 284
    iget-object v0, p0, Lvq;->e:Landroid/support/v7/widget/ActionBarContextView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/ActionBarContextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 164
    iget-boolean v0, p0, Lvq;->l:Z

    if-nez v0, :cond_0

    .line 165
    iput-boolean v1, p0, Lvq;->l:Z

    .line 166
    invoke-direct {p0, v1}, Lvq;->k(Z)V

    .line 167
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lvq;->m:Lwp;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lvq;->m:Lwp;

    invoke-virtual {v0}, Lwp;->b()V

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lvq;->m:Lwp;

    .line 298
    :cond_0
    return-void
.end method
