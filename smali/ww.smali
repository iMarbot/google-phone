.class final Lww;
.super Lxr;
.source "PG"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lxu;


# instance fields
.field private A:Landroid/widget/PopupWindow$OnDismissListener;

.field public final a:Landroid/os/Handler;

.field public final b:Ljava/util/List;

.field public final c:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public d:Landroid/view/View;

.field public e:Landroid/view/ViewTreeObserver;

.field public f:Z

.field private h:Landroid/content/Context;

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:Ljava/util/List;

.field private n:Landroid/view/View$OnAttachStateChangeListener;

.field private o:Lacg;

.field private p:I

.field private q:I

.field private r:Landroid/view/View;

.field private s:I

.field private t:Z

.field private u:Z

.field private v:I

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Lxv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;IIZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lxr;-><init>()V

    .line 2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lww;->m:Ljava/util/List;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lww;->b:Ljava/util/List;

    .line 4
    new-instance v0, Lwx;

    invoke-direct {v0, p0}, Lwx;-><init>(Lww;)V

    iput-object v0, p0, Lww;->c:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 5
    new-instance v0, Lwy;

    invoke-direct {v0, p0}, Lwy;-><init>(Lww;)V

    iput-object v0, p0, Lww;->n:Landroid/view/View$OnAttachStateChangeListener;

    .line 6
    new-instance v0, Lwz;

    invoke-direct {v0, p0}, Lwz;-><init>(Lww;)V

    iput-object v0, p0, Lww;->o:Lacg;

    .line 7
    iput v1, p0, Lww;->p:I

    .line 8
    iput v1, p0, Lww;->q:I

    .line 9
    iput-object p1, p0, Lww;->h:Landroid/content/Context;

    .line 10
    iput-object p2, p0, Lww;->r:Landroid/view/View;

    .line 11
    iput p3, p0, Lww;->j:I

    .line 12
    iput p4, p0, Lww;->k:I

    .line 13
    iput-boolean p5, p0, Lww;->l:Z

    .line 14
    iput-boolean v1, p0, Lww;->x:Z

    .line 15
    invoke-direct {p0}, Lww;->g()I

    move-result v0

    iput v0, p0, Lww;->s:I

    .line 16
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    const v2, 0x7f0d0017

    .line 18
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 19
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lww;->i:I

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lww;->a:Landroid/os/Handler;

    .line 21
    return-void
.end method

.method private final c(Lxf;)V
    .locals 12

    .prologue
    .line 63
    iget-object v0, p0, Lww;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 64
    new-instance v0, Lxe;

    iget-boolean v1, p0, Lww;->l:Z

    invoke-direct {v0, p1, v6, v1}, Lxe;-><init>(Lxf;Landroid/view/LayoutInflater;Z)V

    .line 65
    invoke-virtual {p0}, Lww;->d()Z

    move-result v1

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lww;->x:Z

    if-eqz v1, :cond_4

    .line 66
    const/4 v1, 0x1

    .line 67
    iput-boolean v1, v0, Lxe;->b:Z

    .line 72
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iget-object v2, p0, Lww;->h:Landroid/content/Context;

    iget v3, p0, Lww;->i:I

    invoke-static {v0, v1, v2, v3}, Lww;->a(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)I

    move-result v7

    .line 74
    new-instance v8, Lach;

    iget-object v1, p0, Lww;->h:Landroid/content/Context;

    const/4 v2, 0x0

    iget v3, p0, Lww;->j:I

    iget v4, p0, Lww;->k:I

    invoke-direct {v8, v1, v2, v3, v4}, Lach;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 75
    iget-object v1, p0, Lww;->o:Lacg;

    .line 76
    iput-object v1, v8, Lach;->b:Lacg;

    .line 78
    iput-object p0, v8, Labw;->n:Landroid/widget/AdapterView$OnItemClickListener;

    .line 79
    invoke-virtual {v8, p0}, Lach;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 80
    iget-object v1, p0, Lww;->r:Landroid/view/View;

    .line 81
    iput-object v1, v8, Labw;->m:Landroid/view/View;

    .line 82
    iget v1, p0, Lww;->q:I

    .line 83
    iput v1, v8, Labw;->j:I

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Lach;->a(Z)V

    .line 85
    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Lach;->c(I)V

    .line 88
    invoke-virtual {v8, v0}, Lach;->a(Landroid/widget/ListAdapter;)V

    .line 89
    invoke-virtual {v8, v7}, Lach;->b(I)V

    .line 90
    iget v0, p0, Lww;->q:I

    .line 91
    iput v0, v8, Labw;->j:I

    .line 92
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 93
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    iget-object v1, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 95
    iget-object v3, v0, Lxb;->b:Lxf;

    .line 96
    const/4 v1, 0x0

    invoke-virtual {v3}, Lxf;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    .line 97
    invoke-virtual {v3, v2}, Lxf;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 98
    invoke-interface {v1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v5

    if-ne p1, v5, :cond_5

    move-object v5, v1

    .line 103
    :goto_2
    if-nez v5, :cond_7

    .line 104
    const/4 v1, 0x0

    :goto_3
    move-object v3, v1

    move-object v4, v0

    .line 133
    :goto_4
    if-eqz v3, :cond_15

    .line 135
    sget-object v0, Lach;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    .line 136
    :try_start_0
    sget-object v0, Lach;->a:Ljava/lang/reflect/Method;

    iget-object v1, v8, Lach;->s:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_1
    :goto_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    .line 141
    iget-object v0, v8, Lach;->s:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setEnterTransition(Landroid/transition/Transition;)V

    .line 143
    :cond_2
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    iget-object v1, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 144
    iget-object v0, v0, Lxb;->a:Lach;

    .line 145
    iget-object v0, v0, Labw;->e:Labb;

    .line 147
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 148
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getLocationOnScreen([I)V

    .line 149
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 150
    iget-object v5, p0, Lww;->d:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 151
    iget v5, p0, Lww;->s:I

    const/4 v9, 0x1

    if-ne v5, v9, :cond_e

    .line 152
    const/4 v5, 0x0

    aget v1, v1, v5

    invoke-virtual {v0}, Landroid/widget/ListView;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v0, v7

    .line 153
    iget v1, v2, Landroid/graphics/Rect;->right:I

    if-gt v0, v1, :cond_f

    .line 154
    const/4 v0, 0x1

    move v1, v0

    .line 160
    :goto_6
    const/4 v0, 0x1

    if-ne v1, v0, :cond_10

    const/4 v0, 0x1

    .line 161
    :goto_7
    iput v1, p0, Lww;->s:I

    .line 162
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1a

    if-lt v1, v2, :cond_11

    .line 164
    iput-object v3, v8, Labw;->m:Landroid/view/View;

    .line 165
    const/4 v2, 0x0

    .line 166
    const/4 v1, 0x0

    .line 173
    :goto_8
    iget v5, p0, Lww;->q:I

    and-int/lit8 v5, v5, 0x5

    const/4 v9, 0x5

    if-ne v5, v9, :cond_13

    .line 174
    if-eqz v0, :cond_12

    .line 175
    add-int v0, v2, v7

    .line 181
    :goto_9
    iput v0, v8, Labw;->g:I

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, v8, Labw;->i:Z

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, v8, Labw;->h:Z

    .line 185
    invoke-virtual {v8, v1}, Lach;->a(I)V

    .line 197
    :goto_a
    new-instance v0, Lxb;

    iget v1, p0, Lww;->s:I

    invoke-direct {v0, v8, p1, v1}, Lxb;-><init>(Lach;Lxf;I)V

    .line 198
    iget-object v1, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-virtual {v8}, Lach;->b()V

    .line 201
    iget-object v2, v8, Labw;->e:Labb;

    .line 203
    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 204
    if-nez v4, :cond_3

    iget-boolean v0, p0, Lww;->y:Z

    if-eqz v0, :cond_3

    .line 205
    iget-object v0, p1, Lxf;->f:Ljava/lang/CharSequence;

    .line 206
    if-eqz v0, :cond_3

    .line 207
    const v0, 0x7f040011

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 208
    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 209
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 211
    iget-object v3, p1, Lxf;->f:Ljava/lang/CharSequence;

    .line 212
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 214
    invoke-virtual {v8}, Lach;->b()V

    .line 215
    :cond_3
    return-void

    .line 69
    :cond_4
    invoke-virtual {p0}, Lww;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    invoke-static {p1}, Lxr;->b(Lxf;)Z

    move-result v1

    .line 71
    iput-boolean v1, v0, Lxe;->b:Z

    goto/16 :goto_0

    .line 100
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 101
    :cond_6
    const/4 v1, 0x0

    move-object v5, v1

    goto/16 :goto_2

    .line 106
    :cond_7
    iget-object v1, v0, Lxb;->a:Lach;

    .line 107
    iget-object v9, v1, Labw;->e:Labb;

    .line 109
    invoke-virtual {v9}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 110
    instance-of v2, v1, Landroid/widget/HeaderViewListAdapter;

    if-eqz v2, :cond_8

    .line 111
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    .line 112
    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v2

    .line 113
    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lxe;

    .line 117
    :goto_b
    const/4 v4, -0x1

    .line 118
    const/4 v3, 0x0

    invoke-virtual {v1}, Lxe;->getCount()I

    move-result v10

    :goto_c
    if-ge v3, v10, :cond_18

    .line 119
    invoke-virtual {v1, v3}, Lxe;->a(I)Lxj;

    move-result-object v11

    if-ne v5, v11, :cond_9

    move v1, v3

    .line 123
    :goto_d
    const/4 v3, -0x1

    if-ne v1, v3, :cond_a

    .line 124
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 115
    :cond_8
    const/4 v2, 0x0

    .line 116
    check-cast v1, Lxe;

    goto :goto_b

    .line 122
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 125
    :cond_a
    add-int/2addr v1, v2

    .line 126
    invoke-virtual {v9}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    .line 127
    if-ltz v1, :cond_b

    invoke-virtual {v9}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_c

    .line 128
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 129
    :cond_c
    invoke-virtual {v9, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_3

    .line 131
    :cond_d
    const/4 v0, 0x0

    .line 132
    const/4 v1, 0x0

    move-object v3, v1

    move-object v4, v0

    goto/16 :goto_4

    .line 155
    :cond_e
    const/4 v0, 0x0

    aget v0, v1, v0

    sub-int/2addr v0, v7

    .line 156
    if-gez v0, :cond_f

    .line 157
    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_6

    .line 158
    :cond_f
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_6

    .line 160
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 167
    :cond_11
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 168
    iget-object v2, p0, Lww;->r:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 169
    const/4 v2, 0x2

    new-array v5, v2, [I

    .line 170
    invoke-virtual {v3, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 171
    const/4 v2, 0x0

    aget v2, v5, v2

    const/4 v9, 0x0

    aget v9, v1, v9

    sub-int/2addr v2, v9

    .line 172
    const/4 v9, 0x1

    aget v5, v5, v9

    const/4 v9, 0x1

    aget v1, v1, v9

    sub-int v1, v5, v1

    goto/16 :goto_8

    .line 176
    :cond_12
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int v0, v2, v0

    goto/16 :goto_9

    .line 177
    :cond_13
    if-eqz v0, :cond_14

    .line 178
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v2

    goto/16 :goto_9

    .line 179
    :cond_14
    sub-int v0, v2, v7

    goto/16 :goto_9

    .line 187
    :cond_15
    iget-boolean v0, p0, Lww;->t:Z

    if-eqz v0, :cond_16

    .line 188
    iget v0, p0, Lww;->v:I

    .line 189
    iput v0, v8, Labw;->g:I

    .line 190
    :cond_16
    iget-boolean v0, p0, Lww;->u:Z

    if-eqz v0, :cond_17

    .line 191
    iget v0, p0, Lww;->w:I

    invoke-virtual {v8, v0}, Lach;->a(I)V

    .line 193
    :cond_17
    iget-object v0, p0, Lxr;->g:Landroid/graphics/Rect;

    .line 196
    iput-object v0, v8, Labw;->q:Landroid/graphics/Rect;

    goto/16 :goto_a

    :catch_0
    move-exception v0

    goto/16 :goto_5

    :cond_18
    move v1, v4

    goto/16 :goto_d
.end method

.method private final g()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 54
    iget-object v1, p0, Lww;->r:Landroid/view/View;

    .line 55
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v1}, Lri;->j(Landroid/view/View;)I

    move-result v1

    .line 57
    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 296
    iget v0, p0, Lww;->p:I

    if-eq v0, p1, :cond_0

    .line 297
    iput p1, p0, Lww;->p:I

    .line 298
    iget-object v0, p0, Lww;->r:Landroid/view/View;

    .line 300
    sget-object v1, Lqy;->a:Lri;

    invoke-virtual {v1, v0}, Lri;->j(Landroid/view/View;)I

    move-result v0

    .line 301
    invoke-static {p1, v0}, Lbw;->a(II)I

    move-result v0

    iput v0, p0, Lww;->q:I

    .line 302
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lww;->r:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 304
    iput-object p1, p0, Lww;->r:Landroid/view/View;

    .line 305
    iget v0, p0, Lww;->p:I

    iget-object v1, p0, Lww;->r:Landroid/view/View;

    .line 307
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v1}, Lri;->j(Landroid/view/View;)I

    move-result v1

    .line 308
    invoke-static {v0, v1}, Lbw;->a(II)I

    move-result v0

    iput v0, p0, Lww;->q:I

    .line 309
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lww;->A:Landroid/widget/PopupWindow$OnDismissListener;

    .line 311
    return-void
.end method

.method public final a(Lxf;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lww;->h:Landroid/content/Context;

    invoke-virtual {p1, p0, v0}, Lxf;->a(Lxu;Landroid/content/Context;)V

    .line 59
    invoke-virtual {p0}, Lww;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-direct {p0, p1}, Lww;->c(Lxf;)V

    .line 62
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lww;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lxf;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 254
    .line 255
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    .line 256
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 257
    iget-object v0, v0, Lxb;->b:Lxf;

    if-ne p1, v0, :cond_1

    .line 262
    :goto_1
    if-gez v1, :cond_3

    .line 294
    :cond_0
    :goto_2
    return-void

    .line 259
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    :cond_2
    const/4 v0, -0x1

    move v1, v0

    goto :goto_1

    .line 264
    :cond_3
    add-int/lit8 v0, v1, 0x1

    .line 265
    iget-object v3, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 266
    iget-object v3, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 267
    iget-object v0, v0, Lxb;->b:Lxf;

    invoke-virtual {v0, v2}, Lxf;->a(Z)V

    .line 268
    :cond_4
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 269
    iget-object v1, v0, Lxb;->b:Lxf;

    invoke-virtual {v1, p0}, Lxf;->b(Lxu;)V

    .line 270
    iget-boolean v1, p0, Lww;->f:Z

    if-eqz v1, :cond_6

    .line 271
    iget-object v1, v0, Lxb;->a:Lach;

    .line 272
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_5

    .line 273
    iget-object v1, v1, Lach;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setExitTransition(Landroid/transition/Transition;)V

    .line 274
    :cond_5
    iget-object v1, v0, Lxb;->a:Lach;

    .line 275
    iget-object v1, v1, Labw;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 276
    :cond_6
    iget-object v0, v0, Lxb;->a:Lach;

    invoke-virtual {v0}, Lach;->c()V

    .line 277
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 278
    if-lez v1, :cond_a

    .line 279
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    iget v0, v0, Lxb;->c:I

    iput v0, p0, Lww;->s:I

    .line 281
    :goto_3
    if-nez v1, :cond_b

    .line 282
    invoke-virtual {p0}, Lww;->c()V

    .line 283
    iget-object v0, p0, Lww;->z:Lxv;

    if-eqz v0, :cond_7

    .line 284
    iget-object v0, p0, Lww;->z:Lxv;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lxv;->a(Lxf;Z)V

    .line 285
    :cond_7
    iget-object v0, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_9

    .line 286
    iget-object v0, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 287
    iget-object v0, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lww;->c:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 288
    :cond_8
    iput-object v5, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    .line 289
    :cond_9
    iget-object v0, p0, Lww;->d:Landroid/view/View;

    iget-object v1, p0, Lww;->n:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 290
    iget-object v0, p0, Lww;->A:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    goto/16 :goto_2

    .line 280
    :cond_a
    invoke-direct {p0}, Lww;->g()I

    move-result v0

    iput v0, p0, Lww;->s:I

    goto :goto_3

    .line 291
    :cond_b
    if-eqz p2, :cond_0

    .line 292
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 293
    iget-object v0, v0, Lxb;->b:Lxf;

    invoke-virtual {v0, v2}, Lxf;->a(Z)V

    goto/16 :goto_2
.end method

.method public final a(Lxv;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lww;->z:Lxv;

    .line 239
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 233
    iget-object v0, v0, Lxb;->a:Lach;

    .line 234
    iget-object v0, v0, Labw;->e:Labb;

    .line 235
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-static {v0}, Lww;->a(Landroid/widget/ListAdapter;)Lxe;

    move-result-object v0

    invoke-virtual {v0}, Lxe;->notifyDataSetChanged()V

    goto :goto_0

    .line 237
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lye;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 240
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 241
    iget-object v3, v0, Lxb;->b:Lxf;

    if-ne p1, v3, :cond_0

    .line 243
    iget-object v0, v0, Lxb;->a:Lach;

    .line 244
    iget-object v0, v0, Labw;->e:Labb;

    .line 245
    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    move v0, v1

    .line 253
    :goto_0
    return v0

    .line 248
    :cond_1
    invoke-virtual {p1}, Lye;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 249
    invoke-virtual {p0, p1}, Lww;->a(Lxf;)V

    .line 250
    iget-object v0, p0, Lww;->z:Lxv;

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lww;->z:Lxv;

    invoke-interface {v0, p1}, Lxv;->a(Lxf;)Z

    :cond_2
    move v0, v1

    .line 252
    goto :goto_0

    .line 253
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lww;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    iget-object v0, p0, Lww;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxf;

    .line 27
    invoke-direct {p0, v0}, Lww;->c(Lxf;)V

    goto :goto_1

    .line 29
    :cond_2
    iget-object v0, p0, Lww;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 30
    iget-object v0, p0, Lww;->r:Landroid/view/View;

    iput-object v0, p0, Lww;->d:Landroid/view/View;

    .line 31
    iget-object v0, p0, Lww;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 33
    :goto_2
    iget-object v1, p0, Lww;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iput-object v1, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    .line 34
    if-eqz v0, :cond_3

    .line 35
    iget-object v0, p0, Lww;->e:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Lww;->c:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 36
    :cond_3
    iget-object v0, p0, Lww;->d:Landroid/view/View;

    iget-object v1, p0, Lww;->n:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto :goto_0

    .line 32
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x1

    iput-boolean v0, p0, Lww;->t:Z

    .line 318
    iput p1, p0, Lww;->v:I

    .line 319
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, Lww;->x:Z

    .line 23
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 39
    if-lez v1, :cond_1

    .line 40
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    new-array v2, v1, [Lxb;

    .line 41
    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lxb;

    .line 42
    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_1

    .line 43
    aget-object v2, v0, v1

    .line 44
    iget-object v3, v2, Lxb;->a:Lach;

    .line 45
    iget-object v3, v3, Labw;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    .line 46
    if-eqz v3, :cond_0

    .line 47
    iget-object v2, v2, Lxb;->a:Lach;

    invoke-virtual {v2}, Lach;->c()V

    .line 48
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lww;->u:Z

    .line 321
    iput p1, p0, Lww;->w:I

    .line 322
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 323
    iput-boolean p1, p0, Lww;->y:Z

    .line 324
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    iget-object v0, v0, Lxb;->a:Lach;

    .line 217
    iget-object v0, v0, Labw;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 218
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final e()Landroid/widget/ListView;
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 316
    :goto_0
    return-object v0

    .line 312
    :cond_0
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    iget-object v1, p0, Lww;->b:Ljava/util/List;

    .line 313
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 314
    iget-object v0, v0, Lxb;->a:Lach;

    .line 315
    iget-object v0, v0, Labw;->e:Labb;

    goto :goto_0
.end method

.method protected final f()Z
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x0

    return v0
.end method

.method public final onDismiss()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 219
    const/4 v1, 0x0

    .line 220
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_2

    .line 221
    iget-object v0, p0, Lww;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxb;

    .line 222
    iget-object v5, v0, Lxb;->a:Lach;

    .line 223
    iget-object v5, v5, Labw;->s:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v5

    .line 224
    if-nez v5, :cond_1

    .line 228
    :goto_1
    if-eqz v0, :cond_0

    .line 229
    iget-object v0, v0, Lxb;->b:Lxf;

    invoke-virtual {v0, v3}, Lxf;->a(Z)V

    .line 230
    :cond_0
    return-void

    .line 227
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 51
    invoke-virtual {p0}, Lww;->c()V

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
