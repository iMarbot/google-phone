.class public final enum Lgem;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lgem;

.field public static final enum b:Lgem;

.field private static enum c:Lgem;

.field private static enum d:Lgem;

.field private static enum e:Lgem;

.field private static enum f:Lgem;

.field private static synthetic g:[Lgem;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3
    new-instance v0, Lgem;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->a:Lgem;

    new-instance v0, Lgem;

    const-string v1, "INTERNAL_ERROR"

    invoke-direct {v0, v1, v4}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->b:Lgem;

    new-instance v0, Lgem;

    const-string v1, "AUTHENTICATION_FAILED"

    invoke-direct {v0, v1, v5}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->c:Lgem;

    new-instance v0, Lgem;

    const-string v1, "NETWORK_DISCONNECTED"

    invoke-direct {v0, v1, v6}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->d:Lgem;

    new-instance v0, Lgem;

    const-string v1, "USER_CANCELED"

    invoke-direct {v0, v1, v7}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->e:Lgem;

    new-instance v0, Lgem;

    const-string v1, "NO_SIM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgem;->f:Lgem;

    .line 4
    const/4 v0, 0x6

    new-array v0, v0, [Lgem;

    sget-object v1, Lgem;->a:Lgem;

    aput-object v1, v0, v3

    sget-object v1, Lgem;->b:Lgem;

    aput-object v1, v0, v4

    sget-object v1, Lgem;->c:Lgem;

    aput-object v1, v0, v5

    sget-object v1, Lgem;->d:Lgem;

    aput-object v1, v0, v6

    sget-object v1, Lgem;->e:Lgem;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgem;->f:Lgem;

    aput-object v2, v0, v1

    sput-object v0, Lgem;->g:[Lgem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lgem;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgem;->g:[Lgem;

    invoke-virtual {v0}, [Lgem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgem;

    return-object v0
.end method
