.class final Lcmn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcnr;


# instance fields
.field public a:Ljava/lang/String;

.field private synthetic b:Lcmi;


# direct methods
.method constructor <init>(Lcmi;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcmn;->b:Lcmi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcna;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2
    const-string v1, "ImapHelper"

    const-string v2, "Fetched transcription for "

    .line 3
    iget-object v0, p1, Lcna;->b:Ljava/lang/String;

    .line 4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcop;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcmn;->b:Lcmi;

    invoke-virtual {p1}, Lcna;->e()Lcms;

    move-result-object v1

    .line 6
    invoke-static {v1}, Lcmi;->a(Lcms;)[B

    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    iput-object v0, p0, Lcmn;->a:Ljava/lang/String;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 14
    :goto_1
    return-void

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "ImapHelper"

    const-string v2, "Messaging Exception:"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcop;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 12
    :catch_1
    move-exception v0

    .line 13
    const-string v1, "ImapHelper"

    const-string v2, "IO Exception:"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcop;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
