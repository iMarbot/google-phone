.class public Lbsp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:Z

.field private static b:Lbsq;

.field private static c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    sput-boolean v0, Lbsp;->a:Z

    .line 207
    new-instance v0, Lbsq;

    invoke-direct {v0}, Lbsq;-><init>()V

    sput-object v0, Lbsp;->b:Lbsq;

    .line 208
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lbsp;->c:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 19
    invoke-static {p0}, Lbsp;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    :try_start_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getAdnUriForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 23
    :goto_0
    return-object v0

    .line 22
    :catch_0
    move-exception v0

    const-string v0, "TelecomUtil"

    const-string v1, "TelecomManager.getAdnUriForPhoneAccount called without permission."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;
    .locals 1

    .prologue
    .line 31
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getDefaultOutgoingPhoneAccount(Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 7
    invoke-static {p0}, Lbsp;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8
    :try_start_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->silenceRinger()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    :cond_0
    :goto_0
    return-void

    .line 11
    :catch_0
    move-exception v0

    const-string v0, "TelecomUtil"

    const-string v1, "TelecomManager.silenceRinger called without permission."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 1
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    :try_start_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->showInCallScreen(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    :cond_0
    :goto_0
    return-void

    .line 5
    :catch_0
    move-exception v0

    const-string v0, "TelecomUtil"

    const-string v1, "TelecomManager.showInCallScreen called without permission."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    .line 76
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v3

    .line 78
    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v3

    .line 79
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 80
    if-eqz v2, :cond_2

    .line 82
    sput-boolean v1, Lbsp;->a:Z

    .line 91
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const-string v2, "android.permission.CALL_PHONE"

    .line 93
    invoke-static {p0, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    .line 94
    :goto_1
    if-eqz v2, :cond_4

    :cond_1
    move v2, v0

    .line 95
    :goto_2
    if-eqz v2, :cond_5

    .line 96
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/telecom/TelecomManager;->placeCall(Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 98
    :goto_3
    return v0

    .line 84
    :cond_2
    sget-boolean v3, Lbsp;->a:Z

    .line 85
    if-nez v3, :cond_0

    .line 86
    const-string v3, "TelecomUtil"

    const-string v4, "Dialer is not currently set to be default dialer"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    sput-boolean v0, Lbsp;->a:Z

    goto :goto_0

    :cond_3
    move v2, v1

    .line 93
    goto :goto_1

    :cond_4
    move v2, v1

    .line 94
    goto :goto_2

    :cond_5
    move v0, v1

    .line 98
    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    :goto_0
    return v0

    .line 61
    :cond_0
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62
    sget-object v2, Lbsp;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    sget-object v0, Lbsp;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 65
    :cond_1
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 66
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/telecom/TelecomManager;->isVoiceMailNumber(Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Z

    move-result v0

    .line 67
    :cond_2
    sget-object v2, Lbsp;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-static {p0}, Lbsp;->j(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    if-nez p2, :cond_1

    .line 26
    :try_start_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/telecom/TelecomManager;->handleMmi(Ljava/lang/String;)Z

    move-result v0

    .line 30
    :cond_0
    :goto_0
    return v0

    .line 27
    :cond_1
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/telecom/TelecomManager;->handleMmi(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 29
    :catch_0
    move-exception v1

    const-string v1, "TelecomUtil"

    const-string v2, "TelecomManager.handleMmi called without permission."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 13
    invoke-static {p0}, Lbsp;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    :try_start_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->cancelMissedCallsNotification()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    :cond_0
    :goto_0
    return-void

    .line 17
    :catch_0
    move-exception v0

    const-string v0, "TelecomUtil"

    const-string v1, "TelecomManager.cancelMissedCalls called without permission."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Lgtm;
    .locals 4

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lgsz;->a:Lgsz;

    .line 47
    :goto_0
    return-object v0

    .line 41
    :cond_0
    const-class v0, Landroid/telephony/SubscriptionManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionManager;

    .line 42
    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 43
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getIccId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    invoke-static {v0}, Lgtm;->a(Ljava/lang/Object;)Lgtm;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_2
    sget-object v0, Lgsz;->a:Lgsz;

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Ljava/util/List;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/telecom/TelecomManager;->getVoiceMailNumber(Landroid/telecom/PhoneAccountHandle;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 48
    .line 49
    invoke-static {p0}, Lbsp;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    .line 52
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->isInManagedCall()Z

    move-result v0

    .line 58
    :goto_0
    return v0

    .line 55
    :cond_0
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->isInCall()Z

    move-result v0

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x0

    .line 58
    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 99
    invoke-static {p0}, Lbsp;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI_WITH_VOICEMAIL:Landroid/net/Uri;

    .line 102
    :goto_0
    return-object v0

    .line 101
    :cond_0
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccountHandle;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 196
    if-nez p1, :cond_0

    move-object v1, v2

    .line 205
    :goto_0
    return-object v1

    .line 198
    :cond_0
    const-class v0, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    .line 199
    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 200
    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v4

    .line 201
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 202
    invoke-virtual {v1, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 205
    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 103
    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v3

    .line 108
    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 110
    if-eqz v2, :cond_3

    .line 112
    sput-boolean v0, Lbsp;->a:Z

    .line 121
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const-string v2, "com.android.voicemail.permission.READ_VOICEMAIL"

    .line 124
    invoke-static {p0, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    .line 125
    :goto_1
    if-eqz v2, :cond_2

    const-string v2, "com.android.voicemail.permission.WRITE_VOICEMAIL"

    .line 128
    invoke-static {p0, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_5

    move v2, v1

    .line 129
    :goto_2
    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 130
    :cond_2
    return v0

    .line 114
    :cond_3
    sget-boolean v3, Lbsp;->a:Z

    .line 115
    if-nez v3, :cond_0

    .line 116
    const-string v3, "TelecomUtil"

    const-string v4, "Dialer is not currently set to be default dialer"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    sput-boolean v1, Lbsp;->a:Z

    goto :goto_0

    :cond_4
    move v2, v0

    .line 124
    goto :goto_1

    :cond_5
    move v2, v0

    .line 128
    goto :goto_2
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 155
    .line 158
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v3

    .line 160
    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 162
    if-eqz v2, :cond_3

    .line 164
    sput-boolean v0, Lbsp;->a:Z

    .line 173
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const-string v2, "android.permission.READ_PHONE_STATE"

    .line 175
    invoke-static {p0, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    .line 176
    :goto_1
    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    .line 166
    :cond_3
    sget-boolean v3, Lbsp;->a:Z

    .line 167
    if-nez v3, :cond_0

    .line 168
    const-string v3, "TelecomUtil"

    const-string v4, "Dialer is not currently set to be default dialer"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    sput-boolean v1, Lbsp;->a:Z

    goto :goto_0

    :cond_4
    move v2, v0

    .line 175
    goto :goto_1
.end method

.method static h(Landroid/content/Context;)Landroid/telecom/TelecomManager;
    .locals 1

    .prologue
    .line 177
    const-string v0, "telecom"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    return-object v0
.end method

.method public static i(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    .line 180
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 184
    if-eqz v0, :cond_1

    .line 186
    sput-boolean v3, Lbsp;->a:Z

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    sget-boolean v1, Lbsp;->a:Z

    .line 189
    if-nez v1, :cond_0

    .line 190
    const-string v1, "TelecomUtil"

    const-string v2, "Dialer is not currently set to be default dialer"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    const/4 v1, 0x1

    .line 192
    sput-boolean v1, Lbsp;->a:Z

    goto :goto_0
.end method

.method private static j(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 131
    .line 134
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 135
    invoke-static {p0}, Lbsp;->h(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v3

    .line 136
    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v3

    .line 137
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    .line 138
    if-eqz v2, :cond_3

    .line 140
    sput-boolean v0, Lbsp;->a:Z

    .line 149
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    const-string v2, "android.permission.MODIFY_PHONE_STATE"

    .line 152
    invoke-static {p0, v2}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    .line 153
    :goto_1
    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 154
    :cond_2
    return v0

    .line 142
    :cond_3
    sget-boolean v3, Lbsp;->a:Z

    .line 143
    if-nez v3, :cond_0

    .line 144
    const-string v3, "TelecomUtil"

    const-string v4, "Dialer is not currently set to be default dialer"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    sput-boolean v1, Lbsp;->a:Z

    goto :goto_0

    :cond_4
    move v2, v0

    .line 152
    goto :goto_1
.end method
