.class final Lfqm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic this$0:Lfql;


# direct methods
.method constructor <init>(Lfql;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfqm;->this$0:Lfql;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2
    iget-object v0, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v0}, Lfql;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfqm;->this$0:Lfql;

    invoke-static {v1}, Lfql;->access$000(Lfql;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4
    iget-object v2, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v2}, Lfql;->getMediaCodec()Landroid/media/MediaCodec;

    move-result-object v2

    .line 5
    if-nez v2, :cond_0

    .line 22
    :goto_0
    return-void

    .line 7
    :cond_0
    :try_start_0
    new-instance v3, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v3}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 8
    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v4

    .line 9
    if-ltz v4, :cond_2

    .line 10
    iget-object v2, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v2, v4, v3}, Lfql;->handleDecodedFrame(ILandroid/media/MediaCodec$BufferInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :cond_1
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 20
    const-wide/16 v2, 0xa

    sub-long v0, v2, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 21
    iget-object v2, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v2}, Lfql;->getDecoderThreadHandler()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lfqm;->this$0:Lfql;

    invoke-static {v3}, Lfql;->access$000(Lfql;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 11
    :cond_2
    const/4 v3, -0x2

    if-ne v4, v3, :cond_3

    .line 12
    :try_start_1
    iget-object v3, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v2

    invoke-virtual {v3, v2}, Lfql;->handleOutputFormatChange(Landroid/media/MediaFormat;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 16
    :catch_0
    move-exception v0

    .line 17
    iget-object v1, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v1, v0}, Lfql;->reportCodecException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 13
    :cond_3
    const/4 v3, -0x3

    if-ne v4, v3, :cond_1

    .line 14
    :try_start_2
    iget-object v3, p0, Lfqm;->this$0:Lfql;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {v3, v2}, Lfql;->access$102(Lfql;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method
