.class public final Lfeo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>(IIILjava/lang/String;II)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lfeo;->a:I

    .line 3
    iput p2, p0, Lfeo;->b:I

    .line 4
    iput p3, p0, Lfeo;->d:I

    .line 5
    iput-object p4, p0, Lfeo;->e:Ljava/lang/String;

    .line 6
    iput p5, p0, Lfeo;->c:I

    .line 7
    iput p6, p0, Lfeo;->f:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 10
    iget v0, p0, Lfeo;->a:I

    if-nez v0, :cond_0

    iget v0, p0, Lfeo;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lfeo;->b:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11
    if-ne p0, p1, :cond_1

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 13
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 14
    goto :goto_0

    .line 15
    :cond_3
    check-cast p1, Lfeo;

    .line 16
    iget v2, p0, Lfeo;->a:I

    iget v3, p1, Lfeo;->a:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lfeo;->b:I

    iget v3, p1, Lfeo;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lfeo;->d:I

    iget v3, p1, Lfeo;->d:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lfeo;->e:Ljava/lang/String;

    iget-object v3, p1, Lfeo;->e:Ljava/lang/String;

    .line 17
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lfeo;->c:I

    iget v3, p1, Lfeo;->c:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lfeo;->f:I

    iget v3, p1, Lfeo;->f:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    .line 18
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 19
    iget v0, p0, Lfeo;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 20
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeo;->b:I

    add-int/2addr v0, v1

    .line 21
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeo;->d:I

    add-int/2addr v0, v1

    .line 22
    iget-object v1, p0, Lfeo;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfeo;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 24
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeo;->c:I

    add-int/2addr v0, v1

    .line 25
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfeo;->f:I

    add-int/2addr v0, v1

    .line 26
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 9
    iget v0, p0, Lfeo;->a:I

    iget v1, p0, Lfeo;->b:I

    iget v2, p0, Lfeo;->d:I

    iget-object v3, p0, Lfeo;->e:Ljava/lang/String;

    iget v4, p0, Lfeo;->c:I

    iget v5, p0, Lfeo;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit16 v6, v6, 0xd2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "CellState.serviceState: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", CellState.signalLevelPercent: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CellState.phoneType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CellState.networkOperator: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CellState.networkType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CellState.systemId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
