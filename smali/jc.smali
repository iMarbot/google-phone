.class final Ljc;
.super Lja;
.source "PG"

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;


# static fields
.field private static E:Landroid/view/animation/Interpolator;

.field private static F:Landroid/view/animation/Interpolator;

.field public static a:Z

.field private static t:Ljava/lang/reflect/Field;


# instance fields
.field private A:Landroid/os/Bundle;

.field private B:Landroid/util/SparseArray;

.field private C:Ljava/util/ArrayList;

.field private D:Ljava/lang/Runnable;

.field public final b:Ljava/util/ArrayList;

.field public c:Landroid/util/SparseArray;

.field public d:Ljava/util/ArrayList;

.field public e:I

.field public f:Liz;

.field public g:Lix;

.field public h:Lip;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljr;

.field private l:Ljava/util/ArrayList;

.field private m:Z

.field private n:I

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/ArrayList;

.field private q:Ljava/util/ArrayList;

.field private r:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private s:Lip;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Ljava/util/ArrayList;

.field private y:Ljava/util/ArrayList;

.field private z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x40200000    # 2.5f

    const/high16 v1, 0x3fc00000    # 1.5f

    .line 1790
    const/4 v0, 0x0

    sput-boolean v0, Ljc;->a:Z

    .line 1791
    const/4 v0, 0x0

    sput-object v0, Ljc;->t:Ljava/lang/reflect/Field;

    .line 1792
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Ljc;->E:Landroid/view/animation/Interpolator;

    .line 1793
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Ljc;->F:Landroid/view/animation/Interpolator;

    .line 1794
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 1795
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lja;-><init>()V

    .line 2
    iput v1, p0, Ljc;->n:I

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    .line 4
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 5
    iput v1, p0, Ljc;->e:I

    .line 6
    iput-object v2, p0, Ljc;->A:Landroid/os/Bundle;

    .line 7
    iput-object v2, p0, Ljc;->B:Landroid/util/SparseArray;

    .line 8
    new-instance v0, Ljd;

    invoke-direct {v0, p0}, Ljd;-><init>(Ljc;)V

    iput-object v0, p0, Ljc;->D:Ljava/lang/Runnable;

    .line 9
    return-void
.end method

.method private final a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILpf;)I
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 994
    .line 995
    add-int/lit8 v0, p4, -0x1

    move v6, v0

    move v4, p4

    :goto_0
    if-lt v6, p3, :cond_6

    .line 996
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 997
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move v2, v3

    .line 999
    :goto_1
    iget-object v1, v0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 1000
    iget-object v1, v0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lij;

    .line 1001
    invoke-static {v1}, Lii;->b(Lij;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v5

    .line 1005
    :goto_2
    if-eqz v1, :cond_4

    add-int/lit8 v1, v6, 0x1

    .line 1006
    invoke-virtual {v0, p1, v1, p4}, Lii;->a(Ljava/util/ArrayList;II)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v5

    .line 1007
    :goto_3
    if-eqz v1, :cond_7

    .line 1008
    iget-object v1, p0, Ljc;->C:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1009
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ljc;->C:Ljava/util/ArrayList;

    .line 1010
    :cond_0
    new-instance v1, Ljq;

    invoke-direct {v1, v0, v7}, Ljq;-><init>(Lii;Z)V

    .line 1011
    iget-object v2, p0, Ljc;->C:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    invoke-virtual {v0, v1}, Lii;->a(Lip$c;)V

    .line 1013
    if-eqz v7, :cond_5

    .line 1014
    invoke-virtual {v0}, Lii;->e()V

    .line 1016
    :goto_4
    add-int/lit8 v1, v4, -0x1

    .line 1017
    if-eq v6, v1, :cond_1

    .line 1018
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1019
    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1020
    :cond_1
    invoke-direct {p0, p5}, Ljc;->b(Lpf;)V

    move v0, v1

    .line 1021
    :goto_5
    add-int/lit8 v1, v6, -0x1

    move v6, v1

    move v4, v0

    goto :goto_0

    .line 1003
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v3

    .line 1004
    goto :goto_2

    :cond_4
    move v1, v3

    .line 1006
    goto :goto_3

    .line 1015
    :cond_5
    invoke-virtual {v0, v3}, Lii;->a(Z)V

    goto :goto_4

    .line 1022
    :cond_6
    return v4

    :cond_7
    move v0, v4

    goto :goto_5
.end method

.method private static a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;
    .locals 4

    .prologue
    .line 304
    const/4 v1, 0x0

    .line 305
    :try_start_0
    sget-object v0, Ljc;->t:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 306
    const-class v0, Landroid/view/animation/Animation;

    const-string v2, "mListener"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 307
    sput-object v0, Ljc;->t:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 308
    :cond_0
    sget-object v0, Ljc;->t:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation$AnimationListener;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 315
    :goto_0
    return-object v0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    const-string v2, "FragmentManager"

    const-string v3, "No field with the name mListener is found in Animation class"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 312
    goto :goto_0

    .line 313
    :catch_1
    move-exception v0

    .line 314
    const-string v2, "FragmentManager"

    const-string v3, "Cannot access Animation\'s mListener field"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0
.end method

.method private static a(FF)Ljl;
    .locals 4

    .prologue
    .line 195
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 196
    sget-object v1, Ljc;->F:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 197
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 198
    new-instance v1, Ljl;

    .line 199
    invoke-direct {v1, v0}, Ljl;-><init>(Landroid/view/animation/Animation;)V

    .line 200
    return-object v1
.end method

.method private static a(FFFF)Ljl;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 183
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 184
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 185
    sget-object v1, Ljc;->E:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 186
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 187
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 188
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 189
    sget-object v1, Ljc;->F:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 190
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 191
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 192
    new-instance v0, Ljl;

    .line 193
    invoke-direct {v0, v9}, Ljl;-><init>(Landroid/view/animation/Animation;)V

    .line 194
    return-object v0
.end method

.method private a(Lip;IZI)Ljl;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const v9, 0x3f79999a    # 0.975f

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    .line 201
    invoke-virtual {p1}, Lip;->J()I

    move-result v4

    .line 202
    invoke-static {}, Lip;->p()Landroid/view/animation/Animation;

    .line 203
    invoke-static {}, Lip;->q()Landroid/animation/Animator;

    .line 204
    if-eqz v4, :cond_3

    .line 205
    iget-object v1, p0, Ljc;->f:Liz;

    .line 206
    iget-object v1, v1, Liz;->b:Landroid/content/Context;

    .line 207
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    const-string v3, "anim"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 209
    const/4 v3, 0x0

    .line 210
    if-eqz v5, :cond_1

    .line 211
    :try_start_0
    iget-object v1, p0, Ljc;->f:Liz;

    .line 212
    iget-object v1, v1, Liz;->b:Landroid/content/Context;

    .line 213
    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 214
    if-eqz v6, :cond_0

    .line 215
    new-instance v1, Ljl;

    .line 216
    invoke-direct {v1, v6}, Ljl;-><init>(Landroid/view/animation/Animation;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    .line 267
    :goto_0
    return-object v0

    :cond_0
    move v1, v0

    .line 223
    :goto_1
    if-nez v1, :cond_3

    .line 224
    :try_start_1
    iget-object v1, p0, Ljc;->f:Liz;

    .line 225
    iget-object v1, v1, Liz;->b:Landroid/content/Context;

    .line 226
    invoke-static {v1, v4}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v3

    .line 227
    if-eqz v3, :cond_3

    .line 228
    new-instance v1, Ljl;

    .line 229
    invoke-direct {v1, v3}, Ljl;-><init>(Landroid/animation/Animator;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v0, v1

    .line 230
    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    throw v0

    :catch_1
    move-exception v1

    :cond_1
    move v1, v3

    goto :goto_1

    .line 232
    :catch_2
    move-exception v1

    .line 233
    if-eqz v5, :cond_2

    .line 234
    throw v1

    .line 235
    :cond_2
    iget-object v1, p0, Ljc;->f:Liz;

    .line 236
    iget-object v1, v1, Liz;->b:Landroid/content/Context;

    .line 237
    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 238
    if-eqz v1, :cond_3

    .line 239
    new-instance v0, Ljl;

    .line 240
    invoke-direct {v0, v1}, Ljl;-><init>(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 242
    :cond_3
    if-nez p2, :cond_4

    move-object v0, v2

    .line 243
    goto :goto_0

    .line 245
    :cond_4
    const/4 v1, -0x1

    .line 246
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 254
    :cond_5
    :goto_2
    if-gez v0, :cond_8

    move-object v0, v2

    .line 255
    goto :goto_0

    .line 247
    :sswitch_0
    if-nez p3, :cond_5

    const/4 v0, 0x2

    goto :goto_2

    .line 249
    :sswitch_1
    if-eqz p3, :cond_6

    const/4 v0, 0x3

    goto :goto_2

    :cond_6
    const/4 v0, 0x4

    goto :goto_2

    .line 251
    :sswitch_2
    if-eqz p3, :cond_7

    const/4 v0, 0x5

    goto :goto_2

    :cond_7
    const/4 v0, 0x6

    goto :goto_2

    .line 256
    :cond_8
    packed-switch v0, :pswitch_data_0

    .line 263
    if-nez p4, :cond_9

    iget-object v0, p0, Ljc;->f:Liz;

    invoke-virtual {v0}, Liz;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 264
    iget-object v0, p0, Ljc;->f:Liz;

    invoke-virtual {v0}, Liz;->f()I

    move-result p4

    .line 265
    :cond_9
    if-nez p4, :cond_a

    move-object v0, v2

    .line 266
    goto :goto_0

    .line 257
    :pswitch_0
    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v7, v8, v7}, Ljc;->a(FFFF)Ljl;

    move-result-object v0

    goto :goto_0

    .line 258
    :pswitch_1
    invoke-static {v7, v9, v7, v8}, Ljc;->a(FFFF)Ljl;

    move-result-object v0

    goto :goto_0

    .line 259
    :pswitch_2
    invoke-static {v9, v7, v8, v7}, Ljc;->a(FFFF)Ljl;

    move-result-object v0

    goto :goto_0

    .line 260
    :pswitch_3
    const v0, 0x3f89999a    # 1.075f

    invoke-static {v7, v0, v7, v8}, Ljc;->a(FFFF)Ljl;

    move-result-object v0

    goto :goto_0

    .line 261
    :pswitch_4
    invoke-static {v8, v7}, Ljc;->a(FF)Ljl;

    move-result-object v0

    goto :goto_0

    .line 262
    :pswitch_5
    invoke-static {v7, v8}, Ljc;->a(FF)Ljl;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    move-object v0, v2

    .line 267
    goto/16 :goto_0

    .line 246
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(ILii;)V
    .locals 3

    .prologue
    .line 819
    monitor-enter p0

    .line 820
    :try_start_0
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 821
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    .line 822
    :cond_0
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 823
    if-ge p1, v0, :cond_1

    .line 824
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 832
    :goto_0
    monitor-exit p0

    return-void

    .line 825
    :cond_1
    :goto_1
    if-ge v0, p1, :cond_3

    .line 826
    iget-object v1, p0, Ljc;->p:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    iget-object v1, p0, Ljc;->q:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 828
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ljc;->q:Ljava/util/ArrayList;

    .line 829
    :cond_2
    iget-object v1, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 830
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 831
    :cond_3
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 832
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Landroid/view/View;Ljl;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 275
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    .line 297
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 298
    iget-object v0, p1, Ljl;->b:Landroid/animation/Animator;

    if-eqz v0, :cond_8

    .line 299
    iget-object v0, p1, Ljl;->b:Landroid/animation/Animator;

    new-instance v1, Ljm;

    invoke-direct {v1, p0}, Ljm;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 280
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v0, v3, :cond_2

    .line 281
    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    move-result v0

    if-nez v0, :cond_2

    .line 283
    sget-object v0, Lqy;->a:Lri;

    invoke-virtual {v0, p0}, Lri;->i(Landroid/view/View;)Z

    move-result v0

    .line 284
    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p1, Ljl;->a:Landroid/view/animation/Animation;

    instance-of v0, v0, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_4

    move v0, v2

    .line 296
    :goto_2
    if-eqz v0, :cond_2

    move v1, v2

    goto :goto_1

    .line 288
    :cond_4
    iget-object v0, p1, Ljl;->a:Landroid/view/animation/Animation;

    instance-of v0, v0, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_7

    .line 289
    iget-object v0, p1, Ljl;->a:Landroid/view/animation/Animation;

    check-cast v0, Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v3

    move v0, v1

    .line 290
    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_6

    .line 291
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Landroid/view/animation/AlphaAnimation;

    if-eqz v4, :cond_5

    move v0, v2

    .line 292
    goto :goto_2

    .line 293
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v1

    .line 294
    goto :goto_2

    .line 295
    :cond_7
    iget-object v0, p1, Ljl;->b:Landroid/animation/Animator;

    invoke-static {v0}, Ljc;->a(Landroid/animation/Animator;)Z

    move-result v0

    goto :goto_2

    .line 300
    :cond_8
    iget-object v0, p1, Ljl;->a:Landroid/view/animation/Animation;

    invoke-static {v0}, Ljc;->a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v0

    .line 301
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 302
    iget-object v1, p1, Ljl;->a:Landroid/view/animation/Animation;

    new-instance v2, Lji;

    invoke-direct {v2, p0, v0}, Lji;-><init>(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method private final a(Lii;ZZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1023
    if-eqz p2, :cond_4

    .line 1024
    invoke-virtual {p1, p4}, Lii;->a(Z)V

    .line 1026
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1027
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1028
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1029
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1030
    if-eqz p3, :cond_0

    move-object v0, p0

    move v5, v4

    .line 1031
    invoke-static/range {v0 .. v5}, Ljz;->a(Ljc;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    .line 1032
    :cond_0
    if-eqz p4, :cond_1

    .line 1033
    iget v0, p0, Ljc;->e:I

    invoke-virtual {p0, v0, v4}, Ljc;->a(IZ)V

    .line 1034
    :cond_1
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_6

    .line 1035
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v3

    .line 1036
    :goto_1
    if-ge v1, v2, :cond_6

    .line 1037
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1038
    if-eqz v0, :cond_3

    iget-object v4, v0, Lip;->I:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-boolean v4, v0, Lip;->Q:Z

    if-eqz v4, :cond_3

    iget v4, v0, Lip;->y:I

    .line 1039
    invoke-virtual {p1, v4}, Lii;->b(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1040
    iget v4, v0, Lip;->S:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_2

    .line 1041
    iget-object v4, v0, Lip;->I:Landroid/view/View;

    iget v5, v0, Lip;->S:F

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1042
    :cond_2
    if-eqz p4, :cond_5

    .line 1043
    iput v6, v0, Lip;->S:F

    .line 1046
    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1025
    :cond_4
    invoke-virtual {p1}, Lii;->e()V

    goto :goto_0

    .line 1044
    :cond_5
    const/high16 v4, -0x40800000    # -1.0f

    iput v4, v0, Lip;->S:F

    .line 1045
    iput-boolean v3, v0, Lip;->Q:Z

    goto :goto_2

    .line 1047
    :cond_6
    return-void
.end method

.method private a(Lip;Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 1576
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1577
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1578
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1580
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1581
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1582
    invoke-direct {v0, p1, p2, v1}, Ljc;->a(Lip;Landroid/content/Context;Z)V

    .line 1583
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1584
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1586
    :cond_2
    return-void
.end method

.method private a(Lip;Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 1598
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1599
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1600
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1602
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1603
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1604
    invoke-direct {v0, p1, p2, v1}, Ljc;->a(Lip;Landroid/os/Bundle;Z)V

    .line 1605
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1606
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1608
    :cond_2
    return-void
.end method

.method private a(Lip;Landroid/view/View;Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 1631
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1632
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1633
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1635
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1636
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1637
    invoke-direct {v0, p1, p2, p3, v1}, Ljc;->a(Lip;Landroid/view/View;Landroid/os/Bundle;Z)V

    .line 1638
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1639
    if-eqz p4, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1641
    :cond_2
    return-void
.end method

.method private final a(Ljava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 26
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    new-instance v0, Lpi;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Lpi;-><init>(Ljava/lang/String;)V

    .line 29
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 30
    iget-object v0, p0, Ljc;->f:Liz;

    if-eqz v0, :cond_0

    .line 31
    :try_start_0
    iget-object v0, p0, Ljc;->f:Liz;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, Liz;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    throw p1

    .line 33
    :catch_0
    move-exception v0

    .line 34
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 36
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Ljc;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 38
    :catch_1
    move-exception v0

    .line 39
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private final a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 877
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    move v3, v2

    move v4, v0

    .line 878
    :goto_1
    if-ge v3, v4, :cond_6

    .line 879
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljq;

    .line 880
    if-eqz p1, :cond_1

    .line 881
    iget-boolean v1, v0, Ljq;->a:Z

    .line 882
    if-nez v1, :cond_1

    .line 884
    iget-object v1, v0, Ljq;->b:Lii;

    .line 885
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 886
    if-eq v1, v6, :cond_1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 887
    invoke-virtual {v0}, Ljq;->d()V

    move v0, v3

    move v1, v4

    .line 907
    :goto_2
    add-int/lit8 v3, v0, 0x1

    move v4, v1

    goto :goto_1

    .line 877
    :cond_0
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    .line 890
    :cond_1
    iget v1, v0, Ljq;->c:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    .line 891
    :goto_3
    if-nez v1, :cond_2

    if-eqz p1, :cond_5

    .line 893
    iget-object v1, v0, Ljq;->b:Lii;

    .line 894
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v1, p1, v2, v5}, Lii;->a(Ljava/util/ArrayList;II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 895
    :cond_2
    iget-object v1, p0, Ljc;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 896
    add-int/lit8 v3, v3, -0x1

    .line 897
    add-int/lit8 v4, v4, -0x1

    .line 898
    if-eqz p1, :cond_4

    .line 899
    iget-boolean v1, v0, Ljq;->a:Z

    .line 900
    if-nez v1, :cond_4

    .line 902
    iget-object v1, v0, Ljq;->b:Lii;

    .line 903
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v6, :cond_4

    .line 904
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 905
    invoke-virtual {v0}, Ljq;->d()V

    move v0, v3

    move v1, v4

    goto :goto_2

    :cond_3
    move v1, v2

    .line 890
    goto :goto_3

    .line 906
    :cond_4
    invoke-virtual {v0}, Ljq;->c()V

    :cond_5
    move v0, v3

    move v1, v4

    goto :goto_2

    .line 908
    :cond_6
    return-void
.end method

.method private final a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 934
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    iget-boolean v8, v0, Lii;->r:Z

    .line 936
    iget-object v0, p0, Ljc;->z:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 937
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->z:Ljava/util/ArrayList;

    .line 939
    :goto_0
    iget-object v0, p0, Ljc;->z:Ljava/util/ArrayList;

    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 941
    iget-object v1, p0, Ljc;->h:Lip;

    move v2, p3

    move-object v3, v1

    move v7, v5

    .line 943
    :goto_1
    if-ge v2, p4, :cond_4

    .line 944
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 945
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 946
    if-nez v1, :cond_2

    .line 947
    iget-object v1, p0, Ljc;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Lii;->a(Ljava/util/ArrayList;Lip;)Lip;

    move-result-object v1

    .line 949
    :goto_2
    if-nez v7, :cond_0

    iget-boolean v0, v0, Lii;->i:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v6

    .line 950
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    move v7, v0

    goto :goto_1

    .line 938
    :cond_1
    iget-object v0, p0, Ljc;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 948
    :cond_2
    iget-object v1, p0, Ljc;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Lii;->b(Ljava/util/ArrayList;Lip;)Lip;

    move-result-object v1

    goto :goto_2

    :cond_3
    move v0, v5

    .line 949
    goto :goto_3

    .line 951
    :cond_4
    iget-object v0, p0, Ljc;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 952
    if-nez v8, :cond_5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 953
    invoke-static/range {v0 .. v5}, Ljz;->a(Ljc;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    .line 954
    :cond_5
    invoke-static {p1, p2, p3, p4}, Ljc;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    .line 956
    if-eqz v8, :cond_b

    .line 957
    new-instance v5, Lpf;

    invoke-direct {v5}, Lpf;-><init>()V

    .line 958
    invoke-direct {p0, v5}, Ljc;->b(Lpf;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 959
    invoke-direct/range {v0 .. v5}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILpf;)I

    move-result v4

    .line 960
    invoke-static {v5}, Ljc;->a(Lpf;)V

    .line 961
    :goto_4
    if-eq v4, p3, :cond_6

    if-eqz v8, :cond_6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v6

    .line 962
    invoke-static/range {v0 .. v5}, Ljz;->a(Ljc;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    .line 963
    iget v0, p0, Ljc;->e:I

    invoke-virtual {p0, v0, v6}, Ljc;->a(IZ)V

    .line 964
    :cond_6
    :goto_5
    if-ge p3, p4, :cond_9

    .line 965
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 966
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 967
    if-eqz v1, :cond_8

    iget v1, v0, Lii;->k:I

    if-ltz v1, :cond_8

    .line 968
    iget v1, v0, Lii;->k:I

    .line 969
    monitor-enter p0

    .line 970
    :try_start_0
    iget-object v2, p0, Ljc;->p:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 971
    iget-object v2, p0, Ljc;->q:Ljava/util/ArrayList;

    if-nez v2, :cond_7

    .line 972
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljc;->q:Ljava/util/ArrayList;

    .line 973
    :cond_7
    iget-object v2, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 974
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 975
    const/4 v1, -0x1

    iput v1, v0, Lii;->k:I

    .line 977
    :cond_8
    add-int/lit8 p3, p3, 0x1

    goto :goto_5

    .line 974
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 978
    :cond_9
    if-eqz v7, :cond_a

    .line 980
    :cond_a
    return-void

    :cond_b
    move v4, p4

    goto :goto_4
.end method

.method static synthetic a(Ljc;Lii;ZZZ)V
    .locals 0

    .prologue
    .line 1789
    invoke-direct {p0, p1, p2, p3, p4}, Ljc;->a(Lii;ZZZ)V

    return-void
.end method

.method static a(Ljr;)V
    .locals 3

    .prologue
    .line 1139
    if-nez p0, :cond_1

    .line 1155
    :cond_0
    return-void

    .line 1142
    :cond_1
    iget-object v0, p0, Ljr;->a:Ljava/util/List;

    .line 1144
    if-eqz v0, :cond_2

    .line 1145
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1146
    const/4 v2, 0x1

    iput-boolean v2, v0, Lip;->D:Z

    goto :goto_0

    .line 1149
    :cond_2
    iget-object v0, p0, Ljr;->b:Ljava/util/List;

    .line 1151
    if-eqz v0, :cond_0

    .line 1152
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljr;

    .line 1153
    invoke-static {v0}, Ljc;->a(Ljr;)V

    goto :goto_1
.end method

.method private static a(Lpf;)V
    .locals 5

    .prologue
    .line 981
    invoke-virtual {p0}, Lpf;->size()I

    move-result v2

    .line 982
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 984
    iget-object v0, p0, Lpf;->a:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 985
    check-cast v0, Lip;

    .line 986
    iget-boolean v3, v0, Lip;->l:Z

    if-nez v3, :cond_0

    .line 988
    iget-object v3, v0, Lip;->I:Landroid/view/View;

    .line 990
    invoke-virtual {v3}, Landroid/view/View;->getAlpha()F

    move-result v4

    iput v4, v0, Lip;->S:F

    .line 991
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 992
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 993
    :cond_1
    return-void
.end method

.method private static a(Landroid/animation/Animator;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 10
    if-nez p0, :cond_1

    .line 25
    :cond_0
    :goto_0
    return v1

    .line 12
    :cond_1
    instance-of v0, p0, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 13
    check-cast p0, Landroid/animation/ValueAnimator;

    .line 14
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    move v0, v1

    .line 15
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 16
    const-string v4, "alpha"

    aget-object v5, v2, v0

    invoke-virtual {v5}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v3

    .line 17
    goto :goto_0

    .line 18
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 19
    :cond_3
    instance-of v0, p0, Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 20
    check-cast p0, Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v4

    move v2, v1

    .line 21
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 22
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-static {v0}, Ljc;->a(Landroid/animation/Animator;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v1, v3

    .line 23
    goto :goto_0

    .line 24
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method

.method private final a(Ljava/lang/String;II)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 51
    invoke-virtual {p0}, Ljc;->i()Z

    .line 52
    invoke-direct {p0, v6}, Ljc;->c(Z)V

    .line 53
    iget-object v0, p0, Ljc;->h:Lip;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Ljc;->h:Lip;

    .line 55
    iget-object v0, v0, Lip;->u:Ljc;

    .line 57
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lja;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    .line 68
    :goto_0
    return v0

    .line 59
    :cond_0
    iget-object v1, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v2, p0, Ljc;->y:Ljava/util/ArrayList;

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z

    move-result v0

    .line 60
    if-eqz v0, :cond_1

    .line 61
    iput-boolean v6, p0, Ljc;->m:Z

    .line 62
    :try_start_0
    iget-object v1, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v2, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v2}, Ljc;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    invoke-direct {p0}, Ljc;->t()V

    .line 66
    :cond_1
    invoke-direct {p0}, Ljc;->v()V

    .line 67
    invoke-direct {p0}, Ljc;->x()V

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ljc;->t()V

    throw v0
.end method

.method private b(Lip;Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 1587
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1588
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1589
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1591
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1592
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1593
    invoke-direct {v0, p1, p2, v1}, Ljc;->b(Lip;Landroid/content/Context;Z)V

    .line 1594
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1595
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1597
    :cond_2
    return-void
.end method

.method private b(Lip;Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 1609
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1610
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1611
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1613
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1614
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1615
    invoke-direct {v0, p1, p2, v1}, Ljc;->b(Lip;Landroid/os/Bundle;Z)V

    .line 1616
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1617
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1619
    :cond_2
    return-void
.end method

.method private b(Lip;Z)V
    .locals 2

    .prologue
    .line 1642
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1643
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1644
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1646
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1647
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1648
    invoke-direct {v0, p1, v1}, Ljc;->b(Lip;Z)V

    .line 1649
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1650
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1652
    :cond_2
    return-void
.end method

.method private final b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 909
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 911
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 912
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Internal error with the back stack records"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 913
    :cond_3
    invoke-direct {p0, p1, p2}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 914
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 916
    :goto_1
    if-ge v2, v3, :cond_6

    .line 917
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    iget-boolean v0, v0, Lii;->r:Z

    .line 918
    if-nez v0, :cond_7

    .line 919
    if-eq v1, v2, :cond_4

    .line 920
    invoke-direct {p0, p1, p2, v1, v2}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    .line 921
    :cond_4
    add-int/lit8 v1, v2, 0x1

    .line 922
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 923
    :goto_2
    if-ge v1, v3, :cond_5

    .line 924
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 925
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    iget-boolean v0, v0, Lii;->r:Z

    if-nez v0, :cond_5

    .line 926
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 927
    invoke-direct {p0, p1, p2, v2, v0}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    .line 929
    add-int/lit8 v1, v0, -0x1

    move v4, v1

    move v1, v0

    move v0, v4

    .line 930
    :goto_3
    add-int/lit8 v2, v0, 0x1

    goto :goto_1

    .line 931
    :cond_6
    if-eq v1, v3, :cond_0

    .line 932
    invoke-direct {p0, p1, p2, v1, v3}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method private static b(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1048
    :goto_0
    if-ge p2, p3, :cond_2

    .line 1049
    invoke-virtual {p0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 1050
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1051
    if-eqz v1, :cond_1

    .line 1052
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lii;->a(I)V

    .line 1053
    add-int/lit8 v1, p3, -0x1

    if-ne p2, v1, :cond_0

    move v1, v2

    .line 1054
    :goto_1
    invoke-virtual {v0, v1}, Lii;->a(Z)V

    .line 1058
    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1053
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1056
    :cond_1
    invoke-virtual {v0, v2}, Lii;->a(I)V

    .line 1057
    invoke-virtual {v0}, Lii;->e()V

    goto :goto_2

    .line 1059
    :cond_2
    return-void
.end method

.method private final b(Lpf;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1060
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_1

    .line 1071
    :cond_0
    return-void

    .line 1062
    :cond_1
    iget v0, p0, Ljc;->e:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1063
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    .line 1064
    :goto_0
    if-ge v6, v7, :cond_0

    .line 1065
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lip;

    .line 1066
    iget v0, v1, Lip;->c:I

    if-ge v0, v2, :cond_2

    .line 1067
    invoke-virtual {v1}, Lip;->J()I

    move-result v3

    invoke-virtual {v1}, Lip;->K()I

    move-result v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 1068
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-boolean v0, v1, Lip;->A:Z

    if-nez v0, :cond_2

    iget-boolean v0, v1, Lip;->Q:Z

    if-eqz v0, :cond_2

    .line 1069
    invoke-virtual {p1, v1}, Lpf;->add(Ljava/lang/Object;)Z

    .line 1070
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 1730
    const/4 v0, 0x0

    .line 1731
    sparse-switch p0, :sswitch_data_0

    .line 1737
    :goto_0
    return v0

    .line 1732
    :sswitch_0
    const/16 v0, 0x2002

    .line 1733
    goto :goto_0

    .line 1734
    :sswitch_1
    const/16 v0, 0x1001

    .line 1735
    goto :goto_0

    .line 1736
    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    .line 1731
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private c(Lip;Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 1620
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1621
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1622
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1624
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1625
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1626
    invoke-direct {v0, p1, p2, v1}, Ljc;->c(Lip;Landroid/os/Bundle;Z)V

    .line 1627
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1628
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1630
    :cond_2
    return-void
.end method

.method private c(Lip;Z)V
    .locals 2

    .prologue
    .line 1653
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1654
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1655
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1657
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1658
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1659
    invoke-direct {v0, p1, v1}, Ljc;->c(Lip;Z)V

    .line 1660
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1661
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1663
    :cond_2
    return-void
.end method

.method private final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 833
    iget-boolean v0, p0, Ljc;->m:Z

    if-eqz v0, :cond_0

    .line 834
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FragmentManager is already executing transactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 835
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Ljc;->f:Liz;

    .line 836
    iget-object v1, v1, Liz;->c:Landroid/os/Handler;

    .line 837
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 838
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of fragment host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 839
    :cond_1
    if-nez p1, :cond_2

    .line 840
    invoke-direct {p0}, Ljc;->s()V

    .line 841
    :cond_2
    iget-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 842
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    .line 843
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->y:Ljava/util/ArrayList;

    .line 844
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljc;->m:Z

    .line 845
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v0, v1}, Ljc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 846
    iput-boolean v2, p0, Ljc;->m:Z

    .line 847
    return-void

    .line 848
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Ljc;->m:Z

    throw v0
.end method

.method private final c(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1076
    .line 1077
    monitor-enter p0

    .line 1078
    :try_start_0
    iget-object v1, p0, Ljc;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1079
    :cond_0
    monitor-exit p0

    .line 1089
    :goto_0
    return v0

    .line 1080
    :cond_1
    iget-object v1, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 1081
    :goto_1
    if-ge v2, v3, :cond_2

    .line 1082
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljo;

    invoke-interface {v0, p1, p2}, Ljo;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 1083
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1084
    :cond_2
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1085
    iget-object v0, p0, Ljc;->f:Liz;

    .line 1086
    iget-object v0, v0, Liz;->c:Landroid/os/Handler;

    .line 1087
    iget-object v2, p0, Ljc;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1088
    monitor-exit p0

    move v0, v1

    .line 1089
    goto :goto_0

    .line 1088
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d(Lip;Landroid/os/Bundle;Z)V
    .locals 2

    .prologue
    .line 1686
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1687
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1688
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1690
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1691
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1692
    invoke-direct {v0, p1, p2, v1}, Ljc;->d(Lip;Landroid/os/Bundle;Z)V

    .line 1693
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1694
    if-eqz p3, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1696
    :cond_2
    return-void
.end method

.method private d(Lip;Z)V
    .locals 2

    .prologue
    .line 1664
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1665
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1666
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1668
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1669
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1670
    invoke-direct {v0, p1, v1}, Ljc;->d(Lip;Z)V

    .line 1671
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1672
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1674
    :cond_2
    return-void
.end method

.method private e(Lip;Z)V
    .locals 2

    .prologue
    .line 1675
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1676
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1677
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1679
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1680
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1681
    invoke-direct {v0, p1, v1}, Ljc;->e(Lip;Z)V

    .line 1682
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1683
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1685
    :cond_2
    return-void
.end method

.method public static f(Lip;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 710
    iget-boolean v1, p0, Lip;->A:Z

    if-nez v1, :cond_0

    .line 711
    iput-boolean v0, p0, Lip;->A:Z

    .line 712
    iget-boolean v1, p0, Lip;->R:Z

    if-nez v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lip;->R:Z

    .line 713
    :cond_0
    return-void

    .line 712
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lip;Z)V
    .locals 2

    .prologue
    .line 1697
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1698
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1699
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1701
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1702
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1703
    invoke-direct {v0, p1, v1}, Ljc;->f(Lip;Z)V

    .line 1704
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1705
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1707
    :cond_2
    return-void
.end method

.method public static g(Lip;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 714
    iget-boolean v1, p0, Lip;->A:Z

    if-eqz v1, :cond_1

    .line 715
    iput-boolean v0, p0, Lip;->A:Z

    .line 716
    iget-boolean v1, p0, Lip;->R:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lip;->R:Z

    .line 717
    :cond_1
    return-void
.end method

.method private g(Lip;Z)V
    .locals 2

    .prologue
    .line 1708
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1709
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1710
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1712
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1713
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1714
    invoke-direct {v0, p1, v1}, Ljc;->g(Lip;Z)V

    .line 1715
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1716
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1718
    :cond_2
    return-void
.end method

.method private h(Lip;Z)V
    .locals 2

    .prologue
    .line 1719
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_0

    .line 1720
    iget-object v0, p0, Ljc;->s:Lip;

    .line 1721
    iget-object v0, v0, Lip;->s:Ljc;

    .line 1723
    instance-of v1, v0, Ljc;

    if-eqz v1, :cond_0

    .line 1724
    check-cast v0, Ljc;

    const/4 v1, 0x1

    .line 1725
    invoke-direct {v0, p1, v1}, Ljc;->h(Lip;Z)V

    .line 1726
    :cond_0
    iget-object v0, p0, Ljc;->r:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpr;

    .line 1727
    if-eqz p2, :cond_1

    iget-object v0, v0, Lpr;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    goto :goto_0

    .line 1729
    :cond_2
    return-void
.end method

.method private k(Lip;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 564
    iget v2, p0, Ljc;->e:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 565
    return-void
.end method

.method private l(Lip;)V
    .locals 2

    .prologue
    .line 1183
    iget-object v0, p1, Lip;->J:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return-void

    .line 1185
    :cond_1
    iget-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 1186
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    .line 1188
    :goto_1
    iget-object v0, p1, Lip;->J:Landroid/view/View;

    iget-object v1, p0, Ljc;->B:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 1189
    iget-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1190
    iget-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    iput-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    .line 1191
    const/4 v0, 0x0

    iput-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    goto :goto_0

    .line 1187
    :cond_2
    iget-object v0, p0, Ljc;->B:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method private m(Lip;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1193
    .line 1194
    iget-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1195
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    .line 1196
    :cond_0
    iget-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lip;->k(Landroid/os/Bundle;)V

    .line 1197
    iget-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v2}, Ljc;->d(Lip;Landroid/os/Bundle;Z)V

    .line 1198
    iget-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1199
    iget-object v0, p0, Ljc;->A:Landroid/os/Bundle;

    .line 1200
    iput-object v1, p0, Ljc;->A:Landroid/os/Bundle;

    .line 1201
    :goto_0
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1202
    invoke-direct {p0, p1}, Ljc;->l(Lip;)V

    .line 1203
    :cond_1
    iget-object v1, p1, Lip;->e:Landroid/util/SparseArray;

    if-eqz v1, :cond_3

    .line 1204
    if-nez v0, :cond_2

    .line 1205
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1206
    :cond_2
    const-string v1, "android:view_state"

    iget-object v2, p1, Lip;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 1207
    :cond_3
    iget-boolean v1, p1, Lip;->L:Z

    if-nez v1, :cond_5

    .line 1208
    if-nez v0, :cond_4

    .line 1209
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1210
    :cond_4
    const-string v1, "android:user_visible_hint"

    iget-boolean v2, p1, Lip;->L:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1211
    :cond_5
    return-object v0

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method private final s()V
    .locals 3

    .prologue
    .line 778
    iget-boolean v0, p0, Ljc;->i:Z

    if-eqz v0, :cond_0

    .line 779
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 780
    :cond_0
    iget-object v0, p0, Ljc;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 781
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ljc;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 782
    :cond_1
    return-void
.end method

.method private final t()V
    .locals 1

    .prologue
    .line 861
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljc;->m:Z

    .line 862
    iget-object v0, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 863
    iget-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 864
    return-void
.end method

.method private final u()V
    .locals 2

    .prologue
    .line 1072
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1073
    :goto_0
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074
    iget-object v0, p0, Ljc;->C:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljq;

    invoke-virtual {v0}, Ljq;->c()V

    goto :goto_0

    .line 1075
    :cond_0
    return-void
.end method

.method private v()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1090
    iget-boolean v0, p0, Ljc;->w:Z

    if-eqz v0, :cond_2

    move v1, v2

    move v3, v2

    .line 1092
    :goto_0
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1093
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1094
    if-eqz v0, :cond_0

    iget-object v4, v0, Lip;->M:Lkz;

    if-eqz v4, :cond_0

    .line 1095
    iget-object v0, v0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->a()Z

    move-result v0

    or-int/2addr v3, v0

    .line 1096
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1097
    :cond_1
    if-nez v3, :cond_2

    .line 1098
    iput-boolean v2, p0, Ljc;->w:Z

    .line 1099
    invoke-virtual {p0}, Ljc;->g()V

    .line 1100
    :cond_2
    return-void
.end method

.method private w()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1156
    .line 1158
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_6

    move v3, v4

    move-object v1, v5

    move-object v2, v5

    .line 1159
    :goto_0
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 1160
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1161
    if-eqz v0, :cond_9

    .line 1162
    iget-boolean v6, v0, Lip;->C:Z

    if-eqz v6, :cond_1

    .line 1163
    if-nez v2, :cond_0

    .line 1164
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1165
    :cond_0
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1166
    iget-object v6, v0, Lip;->i:Lip;

    if-eqz v6, :cond_2

    iget-object v6, v0, Lip;->i:Lip;

    iget v6, v6, Lip;->f:I

    :goto_1
    iput v6, v0, Lip;->j:I

    .line 1167
    :cond_1
    iget-object v6, v0, Lip;->u:Ljc;

    if-eqz v6, :cond_3

    .line 1168
    iget-object v6, v0, Lip;->u:Ljc;

    invoke-direct {v6}, Ljc;->w()V

    .line 1169
    iget-object v0, v0, Lip;->u:Ljc;

    iget-object v0, v0, Ljc;->k:Ljr;

    move-object v6, v0

    .line 1171
    :goto_2
    if-nez v1, :cond_4

    if-eqz v6, :cond_4

    .line 1172
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v4

    .line 1173
    :goto_3
    if-ge v0, v3, :cond_4

    .line 1174
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1175
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1166
    :cond_2
    const/4 v6, -0x1

    goto :goto_1

    .line 1170
    :cond_3
    iget-object v0, v0, Lip;->v:Ljr;

    move-object v6, v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 1176
    if-eqz v0, :cond_5

    .line 1177
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object v1, v2

    .line 1178
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_6
    move-object v1, v5

    move-object v2, v5

    .line 1179
    :cond_7
    if-nez v2, :cond_8

    if-nez v1, :cond_8

    .line 1180
    iput-object v5, p0, Ljc;->k:Ljr;

    .line 1182
    :goto_5
    return-void

    .line 1181
    :cond_8
    new-instance v0, Ljr;

    invoke-direct {v0, v2, v1}, Ljr;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object v0, p0, Ljc;->k:Ljr;

    goto :goto_5

    :cond_9
    move-object v0, v1

    move-object v1, v2

    goto :goto_4
.end method

.method private final x()V
    .locals 3

    .prologue
    .line 1395
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 1396
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1397
    iget-object v1, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1398
    iget-object v1, p0, Ljc;->c:Landroid/util/SparseArray;

    iget-object v2, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->delete(I)V

    .line 1399
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1400
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lii;)I
    .locals 2

    .prologue
    .line 808
    monitor-enter p0

    .line 809
    :try_start_0
    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 810
    :cond_0
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 811
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    .line 812
    :cond_1
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 813
    iget-object v1, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 814
    monitor-exit p0

    .line 817
    :goto_0
    return v0

    .line 815
    :cond_2
    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    iget-object v1, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 816
    iget-object v1, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 817
    monitor-exit p0

    goto :goto_0

    .line 818
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lip;)Lip$d;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 85
    iget v1, p1, Lip;->f:I

    if-gez v1, :cond_0

    .line 86
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not currently in the FragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 87
    :cond_0
    iget v1, p1, Lip;->c:I

    if-lez v1, :cond_1

    .line 88
    invoke-direct {p0, p1}, Ljc;->m(Lip;)Landroid/os/Bundle;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_1

    new-instance v0, Lip$d;

    invoke-direct {v0, v1}, Lip$d;-><init>(Landroid/os/Bundle;)V

    .line 90
    :cond_1
    return-object v0
.end method

.method public final a(I)Lip;
    .locals 3

    .prologue
    .line 740
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 741
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 742
    if-eqz v0, :cond_1

    iget v2, v0, Lip;->x:I

    if-ne v2, p1, :cond_1

    .line 751
    :cond_0
    :goto_1
    return-object v0

    .line 744
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 745
    :cond_2
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_4

    .line 746
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 747
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 748
    if-eqz v0, :cond_3

    iget v2, v0, Lip;->x:I

    if-eq v2, p1, :cond_0

    .line 750
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 751
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Lip;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 73
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 74
    if-ne v1, v0, :cond_1

    .line 75
    const/4 v0, 0x0

    .line 79
    :cond_0
    :goto_0
    return-object v0

    .line 76
    :cond_1
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 77
    if-nez v0, :cond_0

    .line 78
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Ljc;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lip;
    .locals 3

    .prologue
    .line 752
    if-eqz p1, :cond_2

    .line 753
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 754
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 755
    if-eqz v0, :cond_1

    iget-object v2, v0, Lip;->z:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 764
    :cond_0
    :goto_1
    return-object v0

    .line 757
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 758
    :cond_2
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 759
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 760
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 761
    if-eqz v0, :cond_3

    iget-object v2, v0, Lip;->z:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 763
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 764
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Ljy;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lii;

    invoke-direct {v0, p0}, Lii;-><init>(Ljc;)V

    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 47
    if-gez p1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    new-instance v0, Ljp;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p1, v2}, Ljp;-><init>(Ljc;Ljava/lang/String;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljc;->a(Ljo;Z)V

    .line 50
    return-void
.end method

.method final a(IZ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 642
    iget-object v0, p0, Ljc;->f:Liz;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 643
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 644
    :cond_0
    if-nez p2, :cond_2

    iget v0, p0, Ljc;->e:I

    if-ne p1, v0, :cond_2

    .line 669
    :cond_1
    :goto_0
    return-void

    .line 646
    :cond_2
    iput p1, p0, Ljc;->e:I

    .line 647
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    move v1, v3

    .line 650
    :goto_1
    if-ge v2, v4, :cond_3

    .line 651
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 652
    invoke-virtual {p0, v0}, Ljc;->c(Lip;)V

    .line 653
    iget-object v5, v0, Lip;->M:Lkz;

    if-eqz v5, :cond_8

    .line 654
    iget-object v0, v0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->a()Z

    move-result v0

    or-int/2addr v0, v1

    .line 655
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 656
    :cond_3
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v3

    .line 657
    :goto_3
    if-ge v2, v4, :cond_5

    .line 658
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 659
    if-eqz v0, :cond_7

    iget-boolean v5, v0, Lip;->m:Z

    if-nez v5, :cond_4

    iget-boolean v5, v0, Lip;->B:Z

    if-eqz v5, :cond_7

    :cond_4
    iget-boolean v5, v0, Lip;->Q:Z

    if-nez v5, :cond_7

    .line 660
    invoke-virtual {p0, v0}, Ljc;->c(Lip;)V

    .line 661
    iget-object v5, v0, Lip;->M:Lkz;

    if-eqz v5, :cond_7

    .line 662
    iget-object v0, v0, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->a()Z

    move-result v0

    or-int/2addr v0, v1

    .line 663
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 664
    :cond_5
    if-nez v1, :cond_6

    .line 665
    invoke-virtual {p0}, Ljc;->g()V

    .line 666
    :cond_6
    iget-boolean v0, p0, Ljc;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljc;->f:Liz;

    if-eqz v0, :cond_1

    iget v0, p0, Ljc;->e:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 667
    iget-object v0, p0, Ljc;->f:Liz;

    invoke-virtual {v0}, Liz;->d()V

    .line 668
    iput-boolean v3, p0, Ljc;->u:Z

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1463
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1464
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1465
    if-eqz v0, :cond_0

    .line 1467
    invoke-virtual {v0, p1}, Lip;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1468
    iget-object v2, v0, Lip;->u:Ljc;

    if-eqz v2, :cond_0

    .line 1469
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Landroid/content/res/Configuration;)V

    .line 1470
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1471
    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;Lip;)V
    .locals 3

    .prologue
    .line 69
    iget v0, p3, Lip;->f:I

    if-gez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 71
    :cond_0
    iget v0, p3, Lip;->f:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    return-void
.end method

.method final a(Landroid/os/Parcelable;Ljr;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1288
    if-nez p1, :cond_1

    .line 1394
    :cond_0
    :goto_0
    return-void

    .line 1289
    :cond_1
    check-cast p1, Ljs;

    .line 1290
    iget-object v0, p1, Ljs;->a:[Ljv;

    if-eqz v0, :cond_0

    .line 1292
    if-eqz p2, :cond_18

    .line 1294
    iget-object v7, p2, Ljr;->a:Ljava/util/List;

    .line 1297
    iget-object v4, p2, Ljr;->b:Ljava/util/List;

    .line 1299
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    :goto_1
    move v6, v2

    .line 1300
    :goto_2
    if-ge v6, v1, :cond_6

    .line 1301
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    move v3, v2

    .line 1303
    :goto_3
    iget-object v8, p1, Ljs;->a:[Ljv;

    array-length v8, v8

    if-ge v3, v8, :cond_3

    iget-object v8, p1, Ljs;->a:[Ljv;

    aget-object v8, v8, v3

    iget v8, v8, Ljv;->b:I

    iget v9, v0, Lip;->f:I

    if-eq v8, v9, :cond_3

    .line 1304
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_2
    move v1, v2

    .line 1299
    goto :goto_1

    .line 1305
    :cond_3
    iget-object v8, p1, Ljs;->a:[Ljv;

    array-length v8, v8

    if-ne v3, v8, :cond_4

    .line 1306
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Could not find active fragment with index "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v0, Lip;->f:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 1307
    :cond_4
    iget-object v8, p1, Ljs;->a:[Ljv;

    aget-object v3, v8, v3

    .line 1308
    iput-object v0, v3, Ljv;->l:Lip;

    .line 1309
    iput-object v5, v0, Lip;->e:Landroid/util/SparseArray;

    .line 1310
    iput v2, v0, Lip;->r:I

    .line 1311
    iput-boolean v2, v0, Lip;->o:Z

    .line 1312
    iput-boolean v2, v0, Lip;->l:Z

    .line 1313
    iput-object v5, v0, Lip;->i:Lip;

    .line 1314
    iget-object v8, v3, Ljv;->k:Landroid/os/Bundle;

    if-eqz v8, :cond_5

    .line 1315
    iget-object v8, v3, Ljv;->k:Landroid/os/Bundle;

    iget-object v9, p0, Ljc;->f:Liz;

    .line 1316
    iget-object v9, v9, Liz;->b:Landroid/content/Context;

    .line 1317
    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1318
    iget-object v8, v3, Ljv;->k:Landroid/os/Bundle;

    const-string v9, "android:view_state"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v8

    iput-object v8, v0, Lip;->e:Landroid/util/SparseArray;

    .line 1319
    iget-object v3, v3, Ljv;->k:Landroid/os/Bundle;

    iput-object v3, v0, Lip;->d:Landroid/os/Bundle;

    .line 1320
    :cond_5
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_6
    move-object v1, v4

    .line 1321
    :goto_4
    new-instance v0, Landroid/util/SparseArray;

    iget-object v3, p1, Ljs;->a:[Ljv;

    array-length v3, v3

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    move v3, v2

    .line 1322
    :goto_5
    iget-object v0, p1, Ljs;->a:[Ljv;

    array-length v0, v0

    if-ge v3, v0, :cond_c

    .line 1323
    iget-object v0, p1, Ljs;->a:[Ljv;

    aget-object v4, v0, v3

    .line 1324
    if-eqz v4, :cond_a

    .line 1326
    if-eqz v1, :cond_17

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_17

    .line 1327
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljr;

    .line 1328
    :goto_6
    iget-object v6, p0, Ljc;->f:Liz;

    iget-object v7, p0, Ljc;->g:Lix;

    iget-object v8, p0, Ljc;->s:Lip;

    .line 1329
    iget-object v9, v4, Ljv;->l:Lip;

    if-nez v9, :cond_9

    .line 1331
    iget-object v9, v6, Liz;->b:Landroid/content/Context;

    .line 1333
    iget-object v10, v4, Ljv;->i:Landroid/os/Bundle;

    if-eqz v10, :cond_7

    .line 1334
    iget-object v10, v4, Ljv;->i:Landroid/os/Bundle;

    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1335
    :cond_7
    if-eqz v7, :cond_b

    .line 1336
    iget-object v10, v4, Ljv;->a:Ljava/lang/String;

    iget-object v11, v4, Ljv;->i:Landroid/os/Bundle;

    invoke-virtual {v7, v9, v10, v11}, Lix;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lip;

    move-result-object v7

    iput-object v7, v4, Ljv;->l:Lip;

    .line 1338
    :goto_7
    iget-object v7, v4, Ljv;->k:Landroid/os/Bundle;

    if-eqz v7, :cond_8

    .line 1339
    iget-object v7, v4, Ljv;->k:Landroid/os/Bundle;

    invoke-virtual {v9}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1340
    iget-object v7, v4, Ljv;->l:Lip;

    iget-object v9, v4, Ljv;->k:Landroid/os/Bundle;

    iput-object v9, v7, Lip;->d:Landroid/os/Bundle;

    .line 1341
    :cond_8
    iget-object v7, v4, Ljv;->l:Lip;

    iget v9, v4, Ljv;->b:I

    invoke-virtual {v7, v9, v8}, Lip;->a(ILip;)V

    .line 1342
    iget-object v7, v4, Ljv;->l:Lip;

    iget-boolean v8, v4, Ljv;->c:Z

    iput-boolean v8, v7, Lip;->n:Z

    .line 1343
    iget-object v7, v4, Ljv;->l:Lip;

    iput-boolean v12, v7, Lip;->p:Z

    .line 1344
    iget-object v7, v4, Ljv;->l:Lip;

    iget v8, v4, Ljv;->d:I

    iput v8, v7, Lip;->x:I

    .line 1345
    iget-object v7, v4, Ljv;->l:Lip;

    iget v8, v4, Ljv;->e:I

    iput v8, v7, Lip;->y:I

    .line 1346
    iget-object v7, v4, Ljv;->l:Lip;

    iget-object v8, v4, Ljv;->f:Ljava/lang/String;

    iput-object v8, v7, Lip;->z:Ljava/lang/String;

    .line 1347
    iget-object v7, v4, Ljv;->l:Lip;

    iget-boolean v8, v4, Ljv;->g:Z

    iput-boolean v8, v7, Lip;->C:Z

    .line 1348
    iget-object v7, v4, Ljv;->l:Lip;

    iget-boolean v8, v4, Ljv;->h:Z

    iput-boolean v8, v7, Lip;->B:Z

    .line 1349
    iget-object v7, v4, Ljv;->l:Lip;

    iget-boolean v8, v4, Ljv;->j:Z

    iput-boolean v8, v7, Lip;->A:Z

    .line 1350
    iget-object v7, v4, Ljv;->l:Lip;

    iget-object v6, v6, Liz;->d:Ljc;

    iput-object v6, v7, Lip;->s:Ljc;

    .line 1351
    :cond_9
    iget-object v6, v4, Ljv;->l:Lip;

    iput-object v0, v6, Lip;->v:Ljr;

    .line 1352
    iget-object v0, v4, Ljv;->l:Lip;

    .line 1354
    iget-object v6, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v7, v0, Lip;->f:I

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1355
    iput-object v5, v4, Ljv;->l:Lip;

    .line 1356
    :cond_a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_5

    .line 1337
    :cond_b
    iget-object v7, v4, Ljv;->a:Ljava/lang/String;

    iget-object v10, v4, Ljv;->i:Landroid/os/Bundle;

    invoke-static {v9, v7, v10}, Lip;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lip;

    move-result-object v7

    iput-object v7, v4, Ljv;->l:Lip;

    goto :goto_7

    .line 1357
    :cond_c
    if-eqz p2, :cond_f

    .line 1359
    iget-object v6, p2, Ljr;->a:Ljava/util/List;

    .line 1361
    if-eqz v6, :cond_e

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    :goto_8
    move v4, v2

    .line 1362
    :goto_9
    if-ge v4, v3, :cond_f

    .line 1363
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1364
    iget v1, v0, Lip;->j:I

    if-ltz v1, :cond_d

    .line 1365
    iget-object v1, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v7, v0, Lip;->j:I

    invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lip;

    iput-object v1, v0, Lip;->i:Lip;

    .line 1366
    iget-object v1, v0, Lip;->i:Lip;

    if-nez v1, :cond_d

    .line 1367
    const-string v1, "FragmentManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Re-attaching retained fragment "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " target no longer exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Lip;->j:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    :cond_d
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_9

    :cond_e
    move v3, v2

    .line 1361
    goto :goto_8

    .line 1369
    :cond_f
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1370
    iget-object v0, p1, Ljs;->b:[I

    if-eqz v0, :cond_12

    move v1, v2

    .line 1371
    :goto_a
    iget-object v0, p1, Ljs;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_12

    .line 1372
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget-object v3, p1, Ljs;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1373
    if-nez v0, :cond_10

    .line 1374
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "No instantiated fragment for index #"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Ljs;->b:[I

    aget v6, v6, v1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 1375
    :cond_10
    iput-boolean v12, v0, Lip;->l:Z

    .line 1376
    iget-object v3, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 1377
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1378
    :cond_11
    iget-object v3, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1379
    :try_start_0
    iget-object v4, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1380
    monitor-exit v3

    .line 1381
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1380
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1382
    :cond_12
    iget-object v0, p1, Ljs;->c:[Lik;

    if-eqz v0, :cond_14

    .line 1383
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Ljs;->c:[Lik;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    .line 1384
    :goto_b
    iget-object v0, p1, Ljs;->c:[Lik;

    array-length v0, v0

    if-ge v2, v0, :cond_15

    .line 1385
    iget-object v0, p1, Ljs;->c:[Lik;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Lik;->a(Ljc;)Lii;

    move-result-object v0

    .line 1386
    iget-object v1, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1387
    iget v1, v0, Lii;->k:I

    if-ltz v1, :cond_13

    .line 1388
    iget v1, v0, Lii;->k:I

    invoke-direct {p0, v1, v0}, Ljc;->a(ILii;)V

    .line 1389
    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 1390
    :cond_14
    iput-object v5, p0, Ljc;->d:Ljava/util/ArrayList;

    .line 1391
    :cond_15
    iget v0, p1, Ljs;->d:I

    if-ltz v0, :cond_16

    .line 1392
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v1, p1, Ljs;->d:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    iput-object v0, p0, Ljc;->h:Lip;

    .line 1393
    :cond_16
    iget v0, p1, Ljs;->e:I

    iput v0, p0, Ljc;->n:I

    goto/16 :goto_0

    :cond_17
    move-object v0, v5

    goto/16 :goto_6

    :cond_18
    move-object v1, v5

    goto/16 :goto_4
.end method

.method final a(Lip;IIIZ)V
    .locals 8

    .prologue
    .line 316
    iget-boolean v0, p1, Lip;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lip;->B:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    if-le p2, v0, :cond_1

    .line 317
    const/4 p2, 0x1

    .line 318
    :cond_1
    iget-boolean v0, p1, Lip;->m:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lip;->c:I

    if-le p2, v0, :cond_2

    .line 319
    iget v0, p1, Lip;->c:I

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lip;->r_()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 320
    const/4 p2, 0x1

    .line 322
    :cond_2
    :goto_0
    iget-boolean v0, p1, Lip;->K:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lip;->c:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_3

    const/4 v0, 0x3

    if-le p2, v0, :cond_3

    .line 323
    const/4 p2, 0x3

    .line 324
    :cond_3
    iget v0, p1, Lip;->c:I

    if-gt v0, p2, :cond_22

    .line 325
    iget-boolean v0, p1, Lip;->n:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p1, Lip;->o:Z

    if-nez v0, :cond_6

    .line 563
    :cond_4
    :goto_1
    return-void

    .line 321
    :cond_5
    iget p2, p1, Lip;->c:I

    goto :goto_0

    .line 327
    :cond_6
    invoke-virtual {p1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 328
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lip;->b(Landroid/view/View;)V

    .line 329
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lip;->a(Landroid/animation/Animator;)V

    .line 330
    invoke-virtual {p1}, Lip;->Q()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 331
    :cond_8
    iget v0, p1, Lip;->c:I

    packed-switch v0, :pswitch_data_0

    .line 560
    :cond_9
    :goto_2
    iget v0, p1, Lip;->c:I

    if-eq v0, p2, :cond_4

    .line 561
    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "moveToState: Fragment state for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not updated inline; expected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lip;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    iput p2, p1, Lip;->c:I

    goto :goto_1

    .line 332
    :pswitch_0
    if-lez p2, :cond_11

    .line 333
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_b

    .line 334
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    iget-object v1, p0, Ljc;->f:Liz;

    .line 335
    iget-object v1, v1, Liz;->b:Landroid/content/Context;

    .line 336
    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 337
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 338
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    .line 339
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-virtual {p0, v0, v1}, Ljc;->a(Landroid/os/Bundle;Ljava/lang/String;)Lip;

    move-result-object v0

    iput-object v0, p1, Lip;->i:Lip;

    .line 340
    iget-object v0, p1, Lip;->i:Lip;

    if-eqz v0, :cond_a

    .line 341
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lip;->k:I

    .line 342
    :cond_a
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lip;->L:Z

    .line 343
    iget-boolean v0, p1, Lip;->L:Z

    if-nez v0, :cond_b

    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p1, Lip;->K:Z

    .line 345
    const/4 v0, 0x3

    if-le p2, v0, :cond_b

    .line 346
    const/4 p2, 0x3

    .line 347
    :cond_b
    iget-object v0, p0, Ljc;->f:Liz;

    iput-object v0, p1, Lip;->t:Liz;

    .line 348
    iget-object v0, p0, Ljc;->s:Lip;

    iput-object v0, p1, Lip;->w:Lip;

    .line 349
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_c

    iget-object v0, p0, Ljc;->s:Lip;

    iget-object v0, v0, Lip;->u:Ljc;

    .line 352
    :goto_3
    iput-object v0, p1, Lip;->s:Ljc;

    .line 353
    iget-object v0, p1, Lip;->i:Lip;

    if-eqz v0, :cond_e

    .line 354
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget-object v1, p1, Lip;->i:Lip;

    iget v1, v1, Lip;->f:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p1, Lip;->i:Lip;

    if-eq v0, v1, :cond_d

    .line 355
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " declared target fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lip;->i:Lip;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " that does not belong to this FragmentManager!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_c
    iget-object v0, p0, Ljc;->f:Liz;

    .line 351
    iget-object v0, v0, Liz;->d:Ljc;

    goto :goto_3

    .line 356
    :cond_d
    iget-object v0, p1, Lip;->i:Lip;

    iget v0, v0, Lip;->c:I

    if-gtz v0, :cond_e

    .line 357
    iget-object v1, p1, Lip;->i:Lip;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 358
    :cond_e
    iget-object v0, p0, Ljc;->f:Liz;

    .line 359
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 360
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ljc;->a(Lip;Landroid/content/Context;Z)V

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->G:Z

    .line 362
    iget-object v0, p0, Ljc;->f:Liz;

    .line 363
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 364
    invoke-virtual {p1, v0}, Lip;->a(Landroid/content/Context;)V

    .line 365
    iget-boolean v0, p1, Lip;->G:Z

    if-nez v0, :cond_f

    .line 366
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_f
    iget-object v0, p1, Lip;->w:Lip;

    if-eqz v0, :cond_10

    .line 368
    iget-object v0, p1, Lip;->w:Lip;

    invoke-static {}, Lip;->o()V

    .line 369
    :cond_10
    iget-object v0, p0, Ljc;->f:Liz;

    .line 370
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 371
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ljc;->b(Lip;Landroid/content/Context;Z)V

    .line 372
    iget-boolean v0, p1, Lip;->U:Z

    if-nez v0, :cond_1a

    .line 373
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ljc;->a(Lip;Landroid/os/Bundle;Z)V

    .line 374
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lip;->i(Landroid/os/Bundle;)V

    .line 375
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ljc;->b(Lip;Landroid/os/Bundle;Z)V

    .line 378
    :goto_4
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->D:Z

    .line 380
    :cond_11
    :pswitch_1
    iget-boolean v0, p1, Lip;->n:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p1, Lip;->q:Z

    if-nez v0, :cond_13

    .line 381
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lip;->g(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1, v2}, Lip;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lip;->I:Landroid/view/View;

    .line 382
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_1b

    .line 383
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iput-object v0, p1, Lip;->J:Landroid/view/View;

    .line 384
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    .line 385
    iget-boolean v0, p1, Lip;->A:Z

    if-eqz v0, :cond_12

    iget-object v0, p1, Lip;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 386
    :cond_12
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iget-object v1, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 387
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iget-object v1, p1, Lip;->d:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Ljc;->a(Lip;Landroid/view/View;Landroid/os/Bundle;Z)V

    .line 389
    :cond_13
    :goto_5
    const/4 v0, 0x1

    if-le p2, v0, :cond_1f

    .line 390
    iget-boolean v0, p1, Lip;->n:Z

    if-nez v0, :cond_18

    .line 391
    const/4 v0, 0x0

    .line 392
    iget v1, p1, Lip;->y:I

    if-eqz v1, :cond_15

    .line 393
    iget v0, p1, Lip;->y:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_14

    .line 394
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for a container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 395
    :cond_14
    iget-object v0, p0, Ljc;->g:Lix;

    iget v1, p1, Lip;->y:I

    invoke-virtual {v0, v1}, Lix;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 396
    if-nez v0, :cond_15

    iget-boolean v1, p1, Lip;->p:Z

    if-nez v1, :cond_15

    .line 397
    :try_start_0
    invoke-virtual {p1}, Lip;->i()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p1, Lip;->y:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 401
    :goto_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lip;->y:I

    .line 402
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") for fragment "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 403
    invoke-direct {p0, v2}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 404
    :cond_15
    iput-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 405
    iget-object v1, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Lip;->g(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Lip;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lip;->I:Landroid/view/View;

    .line 406
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    if-eqz v1, :cond_1d

    .line 407
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    iput-object v1, p1, Lip;->J:Landroid/view/View;

    .line 408
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    .line 409
    if-eqz v0, :cond_16

    .line 410
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 411
    :cond_16
    iget-boolean v0, p1, Lip;->A:Z

    if-eqz v0, :cond_17

    .line 412
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 413
    :cond_17
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iget-object v1, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 414
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iget-object v1, p1, Lip;->d:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Ljc;->a(Lip;Landroid/view/View;Landroid/os/Bundle;Z)V

    .line 415
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p1, Lip;->Q:Z

    .line 417
    :cond_18
    :goto_8
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lip;->j(Landroid/os/Bundle;)V

    .line 418
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ljc;->c(Lip;Landroid/os/Bundle;Z)V

    .line 419
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_1e

    .line 420
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    .line 421
    iget-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_19

    .line 422
    iget-object v0, p1, Lip;->J:Landroid/view/View;

    iget-object v1, p1, Lip;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 423
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    .line 424
    :cond_19
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->G:Z

    .line 426
    const/4 v0, 0x1

    iput-boolean v0, p1, Lip;->G:Z

    .line 427
    iget-boolean v0, p1, Lip;->G:Z

    if-nez v0, :cond_1e

    .line 428
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_1a
    iget-object v0, p1, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lip;->h(Landroid/os/Bundle;)V

    .line 377
    const/4 v0, 0x1

    iput v0, p1, Lip;->c:I

    goto/16 :goto_4

    .line 388
    :cond_1b
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->J:Landroid/view/View;

    goto/16 :goto_5

    .line 400
    :catch_0
    move-exception v1

    const-string v1, "unknown"

    goto/16 :goto_6

    .line 415
    :cond_1c
    const/4 v0, 0x0

    goto :goto_7

    .line 416
    :cond_1d
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->J:Landroid/view/View;

    goto :goto_8

    .line 429
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->d:Landroid/os/Bundle;

    .line 430
    :cond_1f
    :pswitch_2
    const/4 v0, 0x2

    if-le p2, v0, :cond_20

    .line 431
    const/4 v0, 0x3

    iput v0, p1, Lip;->c:I

    .line 432
    :cond_20
    :pswitch_3
    const/4 v0, 0x3

    if-le p2, v0, :cond_21

    .line 433
    invoke-virtual {p1}, Lip;->C()V

    .line 434
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->b(Lip;Z)V

    .line 435
    :cond_21
    :pswitch_4
    const/4 v0, 0x4

    if-le p2, v0, :cond_9

    .line 436
    invoke-virtual {p1}, Lip;->D()V

    .line 437
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->c(Lip;Z)V

    .line 438
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->d:Landroid/os/Bundle;

    .line 439
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    goto/16 :goto_2

    .line 440
    :cond_22
    iget v0, p1, Lip;->c:I

    if-le v0, p2, :cond_9

    .line 441
    iget v0, p1, Lip;->c:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 500
    :cond_23
    :goto_9
    :pswitch_5
    if-gtz p2, :cond_9

    .line 501
    iget-boolean v0, p0, Ljc;->v:Z

    if-eqz v0, :cond_24

    .line 502
    invoke-virtual {p1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 503
    invoke-virtual {p1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    .line 504
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lip;->b(Landroid/view/View;)V

    .line 505
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 510
    :cond_24
    :goto_a
    invoke-virtual {p1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_25

    invoke-virtual {p1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 511
    :cond_25
    invoke-virtual {p1, p2}, Lip;->d(I)V

    .line 512
    const/4 p2, 0x1

    goto/16 :goto_2

    .line 442
    :pswitch_6
    const/4 v0, 0x5

    if-ge p2, v0, :cond_26

    .line 443
    invoke-virtual {p1}, Lip;->E()V

    .line 444
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->d(Lip;Z)V

    .line 445
    :cond_26
    :pswitch_7
    const/4 v0, 0x4

    if-ge p2, v0, :cond_27

    .line 446
    invoke-virtual {p1}, Lip;->F()V

    .line 447
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->e(Lip;Z)V

    .line 448
    :cond_27
    :pswitch_8
    const/4 v0, 0x3

    if-ge p2, v0, :cond_28

    .line 449
    invoke-virtual {p1}, Lip;->G()V

    .line 450
    :cond_28
    :pswitch_9
    const/4 v0, 0x2

    if-ge p2, v0, :cond_23

    .line 451
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_29

    .line 452
    iget-object v0, p0, Ljc;->f:Liz;

    invoke-virtual {v0}, Liz;->b()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p1, Lip;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_29

    .line 453
    invoke-direct {p0, p1}, Ljc;->l(Lip;)V

    .line 455
    :cond_29
    iget-object v0, p1, Lip;->u:Ljc;

    if-eqz v0, :cond_2a

    .line 456
    iget-object v0, p1, Lip;->u:Ljc;

    .line 457
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljc;->b(I)V

    .line 458
    :cond_2a
    const/4 v0, 0x1

    iput v0, p1, Lip;->c:I

    .line 459
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->G:Z

    .line 460
    invoke-virtual {p1}, Lip;->n_()V

    .line 461
    iget-boolean v0, p1, Lip;->G:Z

    if-nez v0, :cond_2b

    .line 462
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :cond_2b
    iget-object v0, p1, Lip;->M:Lkz;

    if-eqz v0, :cond_2c

    .line 464
    iget-object v0, p1, Lip;->M:Lkz;

    invoke-virtual {v0}, Lkz;->e()V

    .line 465
    :cond_2c
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->q:Z

    .line 466
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->f(Lip;Z)V

    .line 467
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_2e

    iget-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2e

    .line 468
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 469
    iget-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    .line 470
    const/4 v0, 0x0

    .line 471
    iget v1, p0, Ljc;->e:I

    if-lez v1, :cond_37

    iget-boolean v1, p0, Ljc;->v:Z

    if-nez v1, :cond_37

    iget-object v1, p1, Lip;->I:Landroid/view/View;

    .line 472
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_37

    iget v1, p1, Lip;->S:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_37

    .line 473
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0, p4}, Ljc;->a(Lip;IZI)Ljl;

    move-result-object v0

    move-object v6, v0

    .line 474
    :goto_b
    const/4 v0, 0x0

    iput v0, p1, Lip;->S:F

    .line 475
    if-eqz v6, :cond_2d

    .line 477
    iget-object v4, p1, Lip;->I:Landroid/view/View;

    .line 478
    iget-object v3, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 479
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    .line 480
    invoke-virtual {p1, p2}, Lip;->d(I)V

    .line 481
    iget-object v0, v6, Ljl;->a:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2f

    .line 482
    iget-object v7, v6, Ljl;->a:Landroid/view/animation/Animation;

    .line 483
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {p1, v0}, Lip;->b(Landroid/view/View;)V

    .line 484
    invoke-static {v7}, Ljc;->a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    .line 485
    new-instance v0, Lje;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lje;-><init>(Ljc;Landroid/view/animation/Animation$AnimationListener;Landroid/view/ViewGroup;Landroid/view/View;Lip;)V

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 486
    invoke-static {v4, v6}, Ljc;->a(Landroid/view/View;Ljl;)V

    .line 487
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 495
    :cond_2d
    :goto_c
    iget-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 496
    :cond_2e
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 497
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->I:Landroid/view/View;

    .line 498
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->J:Landroid/view/View;

    .line 499
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->o:Z

    goto/16 :goto_9

    .line 489
    :cond_2f
    iget-object v0, v6, Ljl;->b:Landroid/animation/Animator;

    .line 490
    iget-object v1, v6, Ljl;->b:Landroid/animation/Animator;

    invoke-virtual {p1, v1}, Lip;->a(Landroid/animation/Animator;)V

    .line 491
    new-instance v1, Ljg;

    invoke-direct {v1, p0, v3, v4, p1}, Ljg;-><init>(Ljc;Landroid/view/ViewGroup;Landroid/view/View;Lip;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 492
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 493
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-static {v1, v6}, Ljc;->a(Landroid/view/View;Ljl;)V

    .line 494
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_c

    .line 506
    :cond_30
    invoke-virtual {p1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 507
    invoke-virtual {p1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    .line 508
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lip;->a(Landroid/animation/Animator;)V

    .line 509
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    goto/16 :goto_a

    .line 513
    :cond_31
    iget-boolean v0, p1, Lip;->D:Z

    if-nez v0, :cond_32

    .line 514
    invoke-virtual {p1}, Lip;->H()V

    .line 515
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->g(Lip;Z)V

    .line 518
    :goto_d
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->G:Z

    .line 519
    invoke-virtual {p1}, Lip;->b()V

    .line 520
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->T:Landroid/view/LayoutInflater;

    .line 521
    iget-boolean v0, p1, Lip;->G:Z

    if-nez v0, :cond_33

    .line 522
    new-instance v0, Llp;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Llp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_32
    const/4 v0, 0x0

    iput v0, p1, Lip;->c:I

    goto :goto_d

    .line 523
    :cond_33
    iget-object v0, p1, Lip;->u:Ljc;

    if-eqz v0, :cond_35

    .line 524
    iget-boolean v0, p1, Lip;->D:Z

    if-nez v0, :cond_34

    .line 525
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Child FragmentManager of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not  destroyed and this fragment is not retaining instance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_34
    iget-object v0, p1, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->q()V

    .line 527
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->u:Ljc;

    .line 528
    :cond_35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljc;->h(Lip;Z)V

    .line 529
    if-nez p5, :cond_9

    .line 530
    iget-boolean v0, p1, Lip;->D:Z

    if-nez v0, :cond_36

    .line 532
    iget v0, p1, Lip;->f:I

    if-ltz v0, :cond_9

    .line 533
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v1, p1, Lip;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 534
    iget-object v0, p0, Ljc;->f:Liz;

    iget-object v1, p1, Lip;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Liz;->b(Ljava/lang/String;)V

    .line 536
    const/4 v0, -0x1

    iput v0, p1, Lip;->f:I

    .line 537
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->g:Ljava/lang/String;

    .line 538
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->l:Z

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->m:Z

    .line 540
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->n:Z

    .line 541
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->o:Z

    .line 542
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->p:Z

    .line 543
    const/4 v0, 0x0

    iput v0, p1, Lip;->r:I

    .line 544
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->s:Ljc;

    .line 545
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->u:Ljc;

    .line 546
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->t:Liz;

    .line 547
    const/4 v0, 0x0

    iput v0, p1, Lip;->x:I

    .line 548
    const/4 v0, 0x0

    iput v0, p1, Lip;->y:I

    .line 549
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->z:Ljava/lang/String;

    .line 550
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->A:Z

    .line 551
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->B:Z

    .line 552
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->D:Z

    .line 553
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->M:Lkz;

    .line 554
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->N:Z

    .line 555
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->O:Z

    goto/16 :goto_2

    .line 557
    :cond_36
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->t:Liz;

    .line 558
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->w:Lip;

    .line 559
    const/4 v0, 0x0

    iput-object v0, p1, Lip;->s:Ljc;

    goto/16 :goto_2

    :cond_37
    move-object v6, v0

    goto/16 :goto_b

    .line 331
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 441
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lip;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 684
    invoke-virtual {p0, p1}, Ljc;->d(Lip;)V

    .line 685
    iget-boolean v0, p1, Lip;->B:Z

    if-nez v0, :cond_3

    .line 686
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 688
    :cond_0
    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v1

    .line 689
    :try_start_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    iput-boolean v3, p1, Lip;->l:Z

    .line 692
    iput-boolean v2, p1, Lip;->m:Z

    .line 693
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-nez v0, :cond_1

    .line 694
    iput-boolean v2, p1, Lip;->R:Z

    .line 695
    :cond_1
    iget-boolean v0, p1, Lip;->E:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lip;->F:Z

    if-eqz v0, :cond_2

    .line 696
    iput-boolean v3, p0, Ljc;->u:Z

    .line 697
    :cond_2
    if-eqz p2, :cond_3

    .line 698
    invoke-direct {p0, p1}, Ljc;->k(Lip;)V

    .line 699
    :cond_3
    return-void

    .line 690
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Liz;Lix;Lip;)V
    .locals 2

    .prologue
    .line 1401
    iget-object v0, p0, Ljc;->f:Liz;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1402
    :cond_0
    iput-object p1, p0, Ljc;->f:Liz;

    .line 1403
    iput-object p2, p0, Ljc;->g:Lix;

    .line 1404
    iput-object p3, p0, Ljc;->s:Lip;

    .line 1405
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 104
    if-lez v4, :cond_1

    .line 105
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 106
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 107
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 108
    :goto_0
    if-ge v2, v4, :cond_1

    .line 109
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 110
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 111
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0, v3, p2, p3, p4}, Lip;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 114
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 116
    if-lez v4, :cond_2

    .line 117
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 118
    :goto_1
    if-ge v2, v4, :cond_2

    .line 119
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 120
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 121
    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 123
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v0}, Lip;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 126
    :cond_2
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 128
    if-lez v4, :cond_3

    .line 129
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 130
    :goto_2
    if-ge v2, v4, :cond_3

    .line 131
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 132
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 133
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lip;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 134
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 135
    :cond_3
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 136
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 137
    if-lez v4, :cond_4

    .line 138
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 139
    :goto_3
    if-ge v2, v4, :cond_4

    .line 140
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 141
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 142
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lii;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v0, v3, p3}, Lii;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 144
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 145
    :cond_4
    monitor-enter p0

    .line 146
    :try_start_0
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 147
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 148
    if-lez v3, :cond_5

    .line 149
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 150
    :goto_4
    if-ge v2, v3, :cond_5

    .line 151
    iget-object v0, p0, Ljc;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 152
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 153
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 154
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 155
    :cond_5
    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 156
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Ljc;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 158
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 160
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 161
    if-lez v2, :cond_7

    .line 162
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 163
    :goto_5
    if-ge v1, v2, :cond_7

    .line 164
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljo;

    .line 165
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 166
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 158
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 168
    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Ljc;->f:Liz;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 170
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Ljc;->g:Lix;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Ljc;->s:Lip;

    if-eqz v0, :cond_8

    .line 172
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Ljc;->s:Lip;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 173
    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Ljc;->e:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 174
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Ljc;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 175
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Ljc;->v:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 176
    iget-boolean v0, p0, Ljc;->u:Z

    if-eqz v0, :cond_9

    .line 177
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 178
    iget-boolean v0, p0, Ljc;->u:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 179
    :cond_9
    iget-object v0, p0, Ljc;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 180
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Ljc;->j:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 182
    :cond_a
    return-void
.end method

.method public final a(Ljo;Z)V
    .locals 2

    .prologue
    .line 784
    if-nez p2, :cond_0

    .line 785
    invoke-direct {p0}, Ljc;->s()V

    .line 786
    :cond_0
    monitor-enter p0

    .line 787
    :try_start_0
    iget-boolean v0, p0, Ljc;->v:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ljc;->f:Liz;

    if-nez v0, :cond_3

    .line 788
    :cond_1
    if-eqz p2, :cond_2

    .line 789
    monitor-exit p0

    .line 795
    :goto_0
    return-void

    .line 790
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 791
    :cond_3
    :try_start_1
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 792
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    .line 793
    :cond_4
    iget-object v0, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 794
    invoke-virtual {p0}, Ljc;->h()V

    .line 795
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1446
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1447
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1448
    if-eqz v0, :cond_0

    .line 1450
    invoke-virtual {v0, p1}, Lip;->d(Z)V

    .line 1451
    iget-object v2, v0, Lip;->u:Ljc;

    if-eqz v2, :cond_0

    .line 1452
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Z)V

    .line 1453
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1454
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1510
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_0

    .line 1527
    :goto_0
    return v2

    :cond_0
    move v1, v2

    move v3, v2

    .line 1513
    :goto_1
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1514
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1515
    if-eqz v0, :cond_2

    .line 1518
    iget-boolean v4, v0, Lip;->A:Z

    if-nez v4, :cond_5

    .line 1519
    iget-boolean v4, v0, Lip;->E:Z

    if-eqz v4, :cond_4

    iget-boolean v4, v0, Lip;->F:Z

    if-eqz v4, :cond_4

    move v4, v5

    .line 1521
    :goto_2
    iget-object v6, v0, Lip;->u:Ljc;

    if-eqz v6, :cond_1

    .line 1522
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Landroid/view/Menu;)Z

    move-result v0

    or-int/2addr v4, v0

    .line 1524
    :cond_1
    :goto_3
    if-eqz v4, :cond_2

    move v3, v5

    .line 1526
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v2, v3

    .line 1527
    goto :goto_0

    :cond_4
    move v4, v2

    goto :goto_2

    :cond_5
    move v4, v2

    goto :goto_3
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1481
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_0

    .line 1509
    :goto_0
    return v5

    .line 1484
    :cond_0
    const/4 v1, 0x0

    move v4, v5

    move v3, v5

    .line 1485
    :goto_1
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 1486
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1487
    if-eqz v0, :cond_7

    .line 1490
    iget-boolean v6, v0, Lip;->A:Z

    if-nez v6, :cond_9

    .line 1491
    iget-boolean v6, v0, Lip;->E:Z

    if-eqz v6, :cond_8

    iget-boolean v6, v0, Lip;->F:Z

    if-eqz v6, :cond_8

    move v6, v2

    .line 1493
    :goto_2
    iget-object v7, v0, Lip;->u:Ljc;

    if-eqz v7, :cond_1

    .line 1494
    iget-object v7, v0, Lip;->u:Ljc;

    invoke-virtual {v7, p1, p2}, Ljc;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v7

    or-int/2addr v6, v7

    .line 1496
    :cond_1
    :goto_3
    if-eqz v6, :cond_7

    .line 1498
    if-nez v1, :cond_2

    .line 1499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1500
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 1501
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    .line 1502
    :cond_3
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 1503
    :goto_5
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v5, v0, :cond_6

    .line 1504
    iget-object v0, p0, Ljc;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1505
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1506
    :cond_4
    invoke-static {}, Lip;->u()V

    .line 1507
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 1508
    :cond_6
    iput-object v1, p0, Ljc;->o:Ljava/util/ArrayList;

    move v5, v3

    .line 1509
    goto :goto_0

    :cond_7
    move v0, v3

    goto :goto_4

    :cond_8
    move v6, v5

    goto :goto_2

    :cond_9
    move v6, v5

    goto :goto_3
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1528
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_1

    .line 1542
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 1530
    :goto_1
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1531
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1532
    if-eqz v0, :cond_3

    .line 1534
    iget-boolean v4, v0, Lip;->A:Z

    if-nez v4, :cond_2

    .line 1535
    iget-object v4, v0, Lip;->u:Ljc;

    if-eqz v4, :cond_2

    .line 1536
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 1539
    :goto_2
    if-eqz v0, :cond_3

    move v2, v3

    .line 1540
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1538
    goto :goto_2

    .line 1541
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method final a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1101
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v2

    .line 1138
    :goto_0
    return v0

    .line 1103
    :cond_0
    if-nez p3, :cond_3

    if-gez p4, :cond_3

    and-int/lit8 v0, p5, 0x1

    if-nez v0, :cond_3

    .line 1104
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1105
    if-gez v0, :cond_1

    move v0, v2

    .line 1106
    goto :goto_0

    .line 1107
    :cond_1
    iget-object v1, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1108
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move v0, v3

    .line 1138
    goto :goto_0

    .line 1110
    :cond_3
    const/4 v0, -0x1

    .line 1111
    if-nez p3, :cond_4

    if-ltz p4, :cond_c

    .line 1112
    :cond_4
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1113
    :goto_1
    if-ltz v1, :cond_7

    .line 1114
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 1115
    if-eqz p3, :cond_5

    .line 1116
    iget-object v4, v0, Lii;->j:Ljava/lang/String;

    .line 1117
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1118
    :cond_5
    if-ltz p4, :cond_6

    iget v0, v0, Lii;->k:I

    if-eq p4, v0, :cond_7

    .line 1119
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 1120
    goto :goto_1

    .line 1121
    :cond_7
    if-gez v1, :cond_8

    move v0, v2

    .line 1122
    goto :goto_0

    .line 1123
    :cond_8
    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_b

    .line 1124
    add-int/lit8 v1, v1, -0x1

    .line 1125
    :goto_2
    if-ltz v1, :cond_b

    .line 1126
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 1127
    if-eqz p3, :cond_9

    .line 1128
    iget-object v4, v0, Lii;->j:Ljava/lang/String;

    .line 1129
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    :cond_9
    if-ltz p4, :cond_b

    iget v0, v0, Lii;->k:I

    if-ne p4, v0, :cond_b

    .line 1130
    :cond_a
    add-int/lit8 v1, v1, -0x1

    .line 1131
    goto :goto_2

    :cond_b
    move v0, v1

    .line 1132
    :cond_c
    iget-object v1, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_d

    move v0, v2

    .line 1133
    goto/16 :goto_0

    .line 1134
    :cond_d
    iget-object v1, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_2

    .line 1135
    iget-object v2, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1136
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    add-int/lit8 v1, v1, -0x1

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)Lip;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 765
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 766
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    .line 767
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 768
    if-eqz v0, :cond_2

    .line 769
    iget-object v3, v0, Lip;->g:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 774
    :goto_1
    if-eqz v0, :cond_2

    .line 777
    :goto_2
    return-object v0

    .line 771
    :cond_0
    iget-object v3, v0, Lip;->u:Ljc;

    if-eqz v3, :cond_1

    .line 772
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->b(Ljava/lang/String;)Lip;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 773
    goto :goto_1

    .line 776
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 777
    goto :goto_2
.end method

.method final b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1439
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ljc;->m:Z

    .line 1440
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ljc;->a(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441
    iput-boolean v1, p0, Ljc;->m:Z

    .line 1444
    invoke-virtual {p0}, Ljc;->i()Z

    .line 1445
    return-void

    .line 1443
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Ljc;->m:Z

    throw v0
.end method

.method public final b(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 1558
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_1

    .line 1568
    :cond_0
    return-void

    .line 1560
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1561
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1562
    if-eqz v0, :cond_2

    .line 1564
    iget-boolean v2, v0, Lip;->A:Z

    if-nez v2, :cond_2

    .line 1565
    iget-object v2, v0, Lip;->u:Ljc;

    if-eqz v2, :cond_2

    .line 1566
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->b(Landroid/view/Menu;)V

    .line 1567
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b(Lip;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-boolean v0, p1, Lip;->K:Z

    if-eqz v0, :cond_0

    .line 269
    iget-boolean v0, p0, Ljc;->m:Z

    if-eqz v0, :cond_1

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljc;->w:Z

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iput-boolean v3, p1, Lip;->K:Z

    .line 273
    iget v2, p0, Ljc;->e:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    goto :goto_0
.end method

.method public final b(Ljo;Z)V
    .locals 2

    .prologue
    .line 849
    if-eqz p2, :cond_1

    iget-object v0, p0, Ljc;->f:Liz;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ljc;->v:Z

    if-eqz v0, :cond_1

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 851
    :cond_1
    invoke-direct {p0, p2}, Ljc;->c(Z)V

    .line 852
    iget-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v1, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-interface {p1, v0, v1}, Ljo;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 853
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljc;->m:Z

    .line 854
    :try_start_0
    iget-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v1, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Ljc;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 855
    invoke-direct {p0}, Ljc;->t()V

    .line 858
    :cond_2
    invoke-direct {p0}, Ljc;->v()V

    .line 859
    invoke-direct {p0}, Ljc;->x()V

    goto :goto_0

    .line 857
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ljc;->t()V

    throw v0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 1455
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1456
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1457
    if-eqz v0, :cond_0

    .line 1459
    iget-object v2, v0, Lip;->u:Ljc;

    if-eqz v2, :cond_0

    .line 1460
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->b(Z)V

    .line 1461
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1462
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Ljc;->i()Z

    move-result v0

    .line 43
    invoke-direct {p0}, Ljc;->u()V

    .line 44
    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1543
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_1

    .line 1557
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 1545
    :goto_1
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1546
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1547
    if-eqz v0, :cond_3

    .line 1549
    iget-boolean v4, v0, Lip;->A:Z

    if-nez v4, :cond_2

    .line 1550
    iget-object v4, v0, Lip;->u:Ljc;

    if-eqz v4, :cond_2

    .line 1551
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0, p1}, Ljc;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 1554
    :goto_2
    if-eqz v0, :cond_3

    move v2, v3

    .line 1555
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1553
    goto :goto_2

    .line 1556
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method final c(Lip;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 566
    if-nez p1, :cond_1

    .line 641
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget v2, p0, Ljc;->e:I

    .line 569
    iget-boolean v0, p1, Lip;->m:Z

    if-eqz v0, :cond_2

    .line 570
    invoke-virtual {p1}, Lip;->r_()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 571
    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 573
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lip;->K()I

    move-result v3

    invoke-virtual {p1}, Lip;->L()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 574
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 576
    iget-object v2, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 577
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    .line 578
    if-eqz v2, :cond_3

    if-nez v0, :cond_b

    :cond_3
    move-object v0, v7

    .line 588
    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    .line 589
    iget-object v0, v0, Lip;->I:Landroid/view/View;

    .line 590
    iget-object v1, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 591
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 592
    iget-object v2, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 593
    if-ge v2, v0, :cond_5

    .line 594
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 595
    iget-object v2, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 596
    :cond_5
    iget-boolean v0, p1, Lip;->Q:Z

    if-eqz v0, :cond_7

    iget-object v0, p1, Lip;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_7

    .line 597
    iget v0, p1, Lip;->S:F

    cmpl-float v0, v0, v8

    if-lez v0, :cond_6

    .line 598
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    iget v1, p1, Lip;->S:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 599
    :cond_6
    iput v8, p1, Lip;->S:F

    .line 600
    iput-boolean v5, p1, Lip;->Q:Z

    .line 601
    invoke-virtual {p1}, Lip;->K()I

    move-result v0

    .line 602
    invoke-virtual {p1}, Lip;->L()I

    move-result v1

    .line 603
    invoke-direct {p0, p1, v0, v6, v1}, Ljc;->a(Lip;IZI)Ljl;

    move-result-object v0

    .line 604
    if-eqz v0, :cond_7

    .line 605
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-static {v1, v0}, Ljc;->a(Landroid/view/View;Ljl;)V

    .line 606
    iget-object v1, v0, Ljl;->a:Landroid/view/animation/Animation;

    if-eqz v1, :cond_e

    .line 607
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    iget-object v0, v0, Ljl;->a:Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 610
    :cond_7
    :goto_3
    iget-boolean v0, p1, Lip;->R:Z

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 613
    invoke-virtual {p1}, Lip;->K()I

    move-result v1

    iget-boolean v0, p1, Lip;->A:Z

    if-nez v0, :cond_f

    move v0, v6

    .line 614
    :goto_4
    invoke-virtual {p1}, Lip;->L()I

    move-result v2

    .line 615
    invoke-direct {p0, p1, v1, v0, v2}, Ljc;->a(Lip;IZI)Ljl;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_12

    iget-object v1, v0, Ljl;->b:Landroid/animation/Animator;

    if-eqz v1, :cond_12

    .line 617
    iget-object v1, v0, Ljl;->b:Landroid/animation/Animator;

    iget-object v2, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 618
    iget-boolean v1, p1, Lip;->A:Z

    if-eqz v1, :cond_11

    .line 619
    invoke-virtual {p1}, Lip;->S()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 620
    invoke-virtual {p1, v5}, Lip;->e(Z)V

    .line 627
    :goto_5
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-static {v1, v0}, Ljc;->a(Landroid/view/View;Ljl;)V

    .line 628
    iget-object v0, v0, Ljl;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 637
    :cond_8
    :goto_6
    iget-boolean v0, p1, Lip;->l:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p1, Lip;->E:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p1, Lip;->F:Z

    if-eqz v0, :cond_9

    .line 638
    iput-boolean v6, p0, Ljc;->u:Z

    .line 639
    :cond_9
    iput-boolean v5, p1, Lip;->R:Z

    .line 640
    iget-boolean v0, p1, Lip;->A:Z

    invoke-static {}, Lip;->m()V

    goto/16 :goto_0

    .line 572
    :cond_a
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto/16 :goto_1

    .line 580
    :cond_b
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 581
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_7
    if-ltz v1, :cond_d

    .line 582
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 583
    iget-object v3, v0, Lip;->H:Landroid/view/ViewGroup;

    if-ne v3, v2, :cond_c

    iget-object v3, v0, Lip;->I:Landroid/view/View;

    if-nez v3, :cond_4

    .line 585
    :cond_c
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_7

    :cond_d
    move-object v0, v7

    .line 586
    goto/16 :goto_2

    .line 608
    :cond_e
    iget-object v1, v0, Ljl;->b:Landroid/animation/Animator;

    iget-object v2, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 609
    iget-object v0, v0, Ljl;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto/16 :goto_3

    :cond_f
    move v0, v5

    .line 613
    goto :goto_4

    .line 621
    :cond_10
    iget-object v1, p1, Lip;->H:Landroid/view/ViewGroup;

    .line 622
    iget-object v2, p1, Lip;->I:Landroid/view/View;

    .line 623
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    .line 624
    iget-object v3, v0, Ljl;->b:Landroid/animation/Animator;

    new-instance v4, Ljh;

    invoke-direct {v4, p0, v1, v2, p1}, Ljh;-><init>(Ljc;Landroid/view/ViewGroup;Landroid/view/View;Lip;)V

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_5

    .line 626
    :cond_11
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 629
    :cond_12
    if-eqz v0, :cond_13

    .line 630
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-static {v1, v0}, Ljc;->a(Landroid/view/View;Ljl;)V

    .line 631
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    iget-object v2, v0, Ljl;->a:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 632
    iget-object v0, v0, Ljl;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 633
    :cond_13
    iget-boolean v0, p1, Lip;->A:Z

    if-eqz v0, :cond_14

    invoke-virtual {p1}, Lip;->S()Z

    move-result v0

    if-nez v0, :cond_14

    const/16 v0, 0x8

    .line 634
    :goto_8
    iget-object v1, p1, Lip;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 635
    invoke-virtual {p1}, Lip;->S()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 636
    invoke-virtual {p1, v5}, Lip;->e(Z)V

    goto/16 :goto_6

    :cond_14
    move v0, v5

    .line 633
    goto :goto_8
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 45
    invoke-direct {p0}, Ljc;->s()V

    .line 46
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ljc;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 83
    :goto_0
    return-object v0

    .line 82
    :cond_0
    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    monitor-exit v1

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final d(Lip;)V
    .locals 2

    .prologue
    .line 677
    iget v0, p1, Lip;->f:I

    if-ltz v0, :cond_0

    .line 683
    :goto_0
    return-void

    .line 679
    :cond_0
    iget v0, p0, Ljc;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ljc;->n:I

    iget-object v1, p0, Ljc;->s:Lip;

    invoke-virtual {p1, v0, v1}, Lip;->a(ILip;)V

    .line 680
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 681
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    .line 682
    :cond_1
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v1, p1, Lip;->f:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final e(Lip;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 700
    invoke-virtual {p1}, Lip;->r_()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 701
    :goto_0
    iget-boolean v3, p1, Lip;->B:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_2

    .line 702
    :cond_0
    iget-object v3, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v3

    .line 703
    :try_start_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 704
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    iget-boolean v0, p1, Lip;->E:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lip;->F:Z

    if-eqz v0, :cond_1

    .line 706
    iput-boolean v1, p0, Ljc;->u:Z

    .line 707
    :cond_1
    iput-boolean v2, p1, Lip;->l:Z

    .line 708
    iput-boolean v1, p1, Lip;->m:Z

    .line 709
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 700
    goto :goto_0

    .line 704
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Ljc;->v:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 783
    iget-boolean v0, p0, Ljc;->i:Z

    return v0
.end method

.method final g()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 676
    :cond_0
    return-void

    .line 671
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 672
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 673
    if-eqz v0, :cond_2

    .line 674
    invoke-virtual {p0, v0}, Ljc;->b(Lip;)V

    .line 675
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method final h()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 796
    monitor-enter p0

    .line 797
    :try_start_0
    iget-object v2, p0, Ljc;->C:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljc;->C:Ljava/util/ArrayList;

    .line 798
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 799
    :goto_0
    iget-object v3, p0, Ljc;->l:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ljc;->l:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v0, :cond_3

    .line 800
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 801
    :cond_0
    iget-object v0, p0, Ljc;->f:Liz;

    .line 802
    iget-object v0, v0, Liz;->c:Landroid/os/Handler;

    .line 803
    iget-object v1, p0, Ljc;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 804
    iget-object v0, p0, Ljc;->f:Liz;

    .line 805
    iget-object v0, v0, Liz;->c:Landroid/os/Handler;

    .line 806
    iget-object v1, p0, Ljc;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 807
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    .line 798
    goto :goto_0

    :cond_3
    move v0, v1

    .line 799
    goto :goto_1

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h(Lip;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 718
    iget-boolean v0, p1, Lip;->B:Z

    if-nez v0, :cond_1

    .line 719
    iput-boolean v2, p1, Lip;->B:Z

    .line 720
    iget-boolean v0, p1, Lip;->l:Z

    if-eqz v0, :cond_1

    .line 721
    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v1

    .line 722
    :try_start_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 723
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 724
    iget-boolean v0, p1, Lip;->E:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lip;->F:Z

    if-eqz v0, :cond_0

    .line 725
    iput-boolean v2, p0, Ljc;->u:Z

    .line 726
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->l:Z

    .line 727
    :cond_1
    return-void

    .line 723
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final i(Lip;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 728
    iget-boolean v0, p1, Lip;->B:Z

    if-eqz v0, :cond_1

    .line 729
    const/4 v0, 0x0

    iput-boolean v0, p1, Lip;->B:Z

    .line 730
    iget-boolean v0, p1, Lip;->l:Z

    if-nez v0, :cond_1

    .line 731
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 733
    :cond_0
    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    monitor-enter v1

    .line 734
    :try_start_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 736
    iput-boolean v2, p1, Lip;->l:Z

    .line 737
    iget-boolean v0, p1, Lip;->E:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lip;->F:Z

    if-eqz v0, :cond_1

    .line 738
    iput-boolean v2, p0, Ljc;->u:Z

    .line 739
    :cond_1
    return-void

    .line 735
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 865
    invoke-direct {p0, v1}, Ljc;->c(Z)V

    .line 866
    const/4 v0, 0x0

    .line 867
    :goto_0
    iget-object v2, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v3, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Ljc;->c(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868
    iput-boolean v1, p0, Ljc;->m:Z

    .line 869
    :try_start_0
    iget-object v0, p0, Ljc;->x:Ljava/util/ArrayList;

    iget-object v2, p0, Ljc;->y:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v2}, Ljc;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 870
    invoke-direct {p0}, Ljc;->t()V

    move v0, v1

    .line 873
    goto :goto_0

    .line 872
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ljc;->t()V

    throw v0

    .line 874
    :cond_0
    invoke-direct {p0}, Ljc;->v()V

    .line 875
    invoke-direct {p0}, Ljc;->x()V

    .line 876
    return v0
.end method

.method final j()Landroid/os/Parcelable;
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1212
    invoke-direct {p0}, Ljc;->u()V

    .line 1214
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    move v6, v3

    :goto_0
    move v9, v3

    .line 1215
    :goto_1
    if-ge v9, v6, :cond_4

    .line 1216
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lip;

    .line 1217
    if-eqz v1, :cond_1

    .line 1218
    invoke-virtual {v1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1219
    invoke-virtual {v1}, Lip;->Q()I

    move-result v2

    .line 1220
    invoke-virtual {v1}, Lip;->O()Landroid/view/View;

    move-result-object v0

    .line 1221
    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    .line 1222
    if-eqz v4, :cond_0

    .line 1223
    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    .line 1224
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1225
    :cond_0
    invoke-virtual {v1, v8}, Lip;->b(Landroid/view/View;)V

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 1226
    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 1229
    :cond_1
    :goto_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 1214
    :cond_2
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    move v6, v0

    goto :goto_0

    .line 1227
    :cond_3
    invoke-virtual {v1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1228
    invoke-virtual {v1}, Lip;->P()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    goto :goto_2

    .line 1230
    :cond_4
    invoke-virtual {p0}, Ljc;->i()Z

    .line 1231
    iput-boolean v7, p0, Ljc;->i:Z

    .line 1232
    iput-object v8, p0, Ljc;->k:Ljr;

    .line 1233
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gtz v0, :cond_6

    .line 1287
    :cond_5
    :goto_3
    return-object v8

    .line 1235
    :cond_6
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 1236
    new-array v5, v4, [Ljv;

    move v2, v3

    move v1, v3

    .line 1238
    :goto_4
    if-ge v2, v4, :cond_c

    .line 1239
    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1240
    if-eqz v0, :cond_12

    .line 1241
    iget v1, v0, Lip;->f:I

    if-gez v1, :cond_7

    .line 1242
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " has cleared index: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v9, v0, Lip;->f:I

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 1244
    :cond_7
    new-instance v1, Ljv;

    invoke-direct {v1, v0}, Ljv;-><init>(Lip;)V

    .line 1245
    aput-object v1, v5, v2

    .line 1246
    iget v6, v0, Lip;->c:I

    if-lez v6, :cond_a

    iget-object v6, v1, Ljv;->k:Landroid/os/Bundle;

    if-nez v6, :cond_a

    .line 1247
    invoke-direct {p0, v0}, Ljc;->m(Lip;)Landroid/os/Bundle;

    move-result-object v6

    iput-object v6, v1, Ljv;->k:Landroid/os/Bundle;

    .line 1248
    iget-object v6, v0, Lip;->i:Lip;

    if-eqz v6, :cond_b

    .line 1249
    iget-object v6, v0, Lip;->i:Lip;

    iget v6, v6, Lip;->f:I

    if-gez v6, :cond_8

    .line 1250
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lip;->i:Lip;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v6}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 1251
    :cond_8
    iget-object v6, v1, Ljv;->k:Landroid/os/Bundle;

    if-nez v6, :cond_9

    .line 1252
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    iput-object v6, v1, Ljv;->k:Landroid/os/Bundle;

    .line 1253
    :cond_9
    iget-object v6, v1, Ljv;->k:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Lip;->i:Lip;

    invoke-virtual {p0, v6, v9, v10}, Ljc;->a(Landroid/os/Bundle;Ljava/lang/String;Lip;)V

    .line 1254
    iget v6, v0, Lip;->k:I

    if-eqz v6, :cond_b

    .line 1255
    iget-object v1, v1, Ljv;->k:Landroid/os/Bundle;

    const-string v6, "android:target_req_state"

    iget v0, v0, Lip;->k:I

    invoke-virtual {v1, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v7

    .line 1257
    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto/16 :goto_4

    .line 1256
    :cond_a
    iget-object v0, v0, Lip;->d:Landroid/os/Bundle;

    iput-object v0, v1, Ljv;->k:Landroid/os/Bundle;

    :cond_b
    move v0, v7

    goto :goto_5

    .line 1258
    :cond_c
    if-eqz v1, :cond_5

    .line 1262
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1263
    if-lez v4, :cond_e

    .line 1264
    new-array v1, v4, [I

    move v2, v3

    .line 1265
    :goto_6
    if-ge v2, v4, :cond_f

    .line 1266
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    iget v0, v0, Lip;->f:I

    aput v0, v1, v2

    .line 1267
    aget v0, v1, v2

    if-gez v0, :cond_d

    .line 1268
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failure saving state: active "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Ljc;->b:Ljava/util/ArrayList;

    .line 1269
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " has cleared index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v1, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1270
    invoke-direct {p0, v0}, Ljc;->a(Ljava/lang/RuntimeException;)V

    .line 1271
    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_e
    move-object v1, v8

    .line 1272
    :cond_f
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_10

    .line 1273
    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1274
    if-lez v2, :cond_10

    .line 1275
    new-array v8, v2, [Lik;

    .line 1276
    :goto_7
    if-ge v3, v2, :cond_10

    .line 1277
    new-instance v4, Lik;

    iget-object v0, p0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    invoke-direct {v4, v0}, Lik;-><init>(Lii;)V

    aput-object v4, v8, v3

    .line 1278
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1279
    :cond_10
    new-instance v0, Ljs;

    invoke-direct {v0}, Ljs;-><init>()V

    .line 1280
    iput-object v5, v0, Ljs;->a:[Ljv;

    .line 1281
    iput-object v1, v0, Ljs;->b:[I

    .line 1282
    iput-object v8, v0, Ljs;->c:[Lik;

    .line 1283
    iget-object v1, p0, Ljc;->h:Lip;

    if-eqz v1, :cond_11

    .line 1284
    iget-object v1, p0, Ljc;->h:Lip;

    iget v1, v1, Lip;->f:I

    iput v1, v0, Ljs;->d:I

    .line 1285
    :cond_11
    iget v1, p0, Ljc;->n:I

    iput v1, v0, Ljs;->e:I

    .line 1286
    invoke-direct {p0}, Ljc;->w()V

    move-object v8, v0

    .line 1287
    goto/16 :goto_3

    :cond_12
    move v0, v1

    goto/16 :goto_5
.end method

.method public final j(Lip;)V
    .locals 3

    .prologue
    .line 1569
    if-eqz p1, :cond_1

    iget-object v0, p0, Ljc;->c:Landroid/util/SparseArray;

    iget v1, p1, Lip;->f:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p1, Lip;->t:Liz;

    if-eqz v0, :cond_1

    .line 1571
    iget-object v0, p1, Lip;->s:Ljc;

    .line 1572
    if-eq v0, p0, :cond_1

    .line 1573
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not an active fragment of FragmentManager "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1574
    :cond_1
    iput-object p1, p0, Ljc;->h:Lip;

    .line 1575
    return-void
.end method

.method public final k()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1406
    const/4 v1, 0x0

    iput-object v1, p0, Ljc;->k:Ljr;

    .line 1407
    iput-boolean v0, p0, Ljc;->i:Z

    .line 1408
    iget-object v1, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 1409
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1410
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1411
    if-eqz v0, :cond_0

    .line 1413
    iget-object v3, v0, Lip;->u:Ljc;

    if-eqz v3, :cond_0

    .line 1414
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->k()V

    .line 1415
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1416
    :cond_1
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1417
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljc;->i:Z

    .line 1418
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1419
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1420
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljc;->i:Z

    .line 1421
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1422
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1423
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljc;->i:Z

    .line 1424
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1425
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 1426
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljc;->i:Z

    .line 1427
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1428
    return-void
.end method

.method public final onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v2, 0x1

    .line 1738
    const-string v0, "fragment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v4

    .line 1787
    :goto_0
    return-object v0

    .line 1740
    :cond_0
    const-string v0, "class"

    invoke-interface {p4, v4, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1741
    sget-object v1, Ljn;->a:[I

    invoke-virtual {p3, p4, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1742
    if-nez v0, :cond_f

    .line 1743
    invoke-virtual {v1, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 1744
    :goto_1
    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 1745
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1746
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1747
    iget-object v0, p0, Ljc;->f:Liz;

    .line 1748
    iget-object v0, v0, Liz;->b:Landroid/content/Context;

    .line 1749
    invoke-static {v0, v6}, Lip;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v4

    .line 1750
    goto :goto_0

    .line 1751
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 1752
    :goto_2
    if-ne v1, v5, :cond_3

    if-ne v7, v5, :cond_3

    if-nez v8, :cond_3

    .line 1753
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v1, v3

    .line 1751
    goto :goto_2

    .line 1754
    :cond_3
    if-eq v7, v5, :cond_6

    invoke-virtual {p0, v7}, Ljc;->a(I)Lip;

    move-result-object v0

    .line 1755
    :goto_3
    if-nez v0, :cond_4

    if-eqz v8, :cond_4

    .line 1756
    invoke-virtual {p0, v8}, Ljc;->a(Ljava/lang/String;)Lip;

    move-result-object v0

    .line 1757
    :cond_4
    if-nez v0, :cond_5

    if-eq v1, v5, :cond_5

    .line 1758
    invoke-virtual {p0, v1}, Ljc;->a(I)Lip;

    move-result-object v0

    .line 1759
    :cond_5
    if-nez v0, :cond_8

    .line 1760
    iget-object v0, p0, Ljc;->g:Lix;

    invoke-virtual {v0, p3, v6, v4}, Lix;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lip;

    move-result-object v4

    .line 1761
    iput-boolean v2, v4, Lip;->n:Z

    .line 1762
    if-eqz v7, :cond_7

    move v0, v7

    :goto_4
    iput v0, v4, Lip;->x:I

    .line 1763
    iput v1, v4, Lip;->y:I

    .line 1764
    iput-object v8, v4, Lip;->z:Ljava/lang/String;

    .line 1765
    iput-boolean v2, v4, Lip;->o:Z

    .line 1766
    iput-object p0, v4, Lip;->s:Ljc;

    .line 1767
    iget-object v0, p0, Ljc;->f:Liz;

    iput-object v0, v4, Lip;->t:Liz;

    .line 1768
    iget-object v0, v4, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {v4, p4, v0}, Lip;->a(Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    .line 1769
    invoke-virtual {p0, v4, v2}, Ljc;->a(Lip;Z)V

    move-object v1, v4

    .line 1778
    :goto_5
    iget v0, p0, Ljc;->e:I

    if-gtz v0, :cond_b

    iget-boolean v0, v1, Lip;->n:Z

    if-eqz v0, :cond_b

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 1779
    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 1781
    :goto_6
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    if-nez v0, :cond_c

    .line 1782
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v0, v4

    .line 1754
    goto :goto_3

    :cond_7
    move v0, v1

    .line 1762
    goto :goto_4

    .line 1770
    :cond_8
    iget-boolean v4, v0, Lip;->o:Z

    if-eqz v4, :cond_9

    .line 1771
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Duplicate id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1772
    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tag "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", or parent id 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1773
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1774
    :cond_9
    iput-boolean v2, v0, Lip;->o:Z

    .line 1775
    iget-object v1, p0, Ljc;->f:Liz;

    iput-object v1, v0, Lip;->t:Liz;

    .line 1776
    iget-boolean v1, v0, Lip;->D:Z

    if-nez v1, :cond_a

    .line 1777
    iget-object v1, v0, Lip;->d:Landroid/os/Bundle;

    invoke-virtual {v0, p4, v1}, Lip;->a(Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    :cond_a
    move-object v1, v0

    goto/16 :goto_5

    .line 1780
    :cond_b
    invoke-direct {p0, v1}, Ljc;->k(Lip;)V

    goto/16 :goto_6

    .line 1783
    :cond_c
    if-eqz v7, :cond_d

    .line 1784
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 1785
    :cond_d
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_e

    .line 1786
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1787
    :cond_e
    iget-object v0, v1, Lip;->I:Landroid/view/View;

    goto/16 :goto_0

    :cond_f
    move-object v6, v0

    goto/16 :goto_1
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1788
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Ljc;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1429
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljc;->i:Z

    .line 1430
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1431
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1432
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljc;->v:Z

    .line 1433
    invoke-virtual {p0}, Ljc;->i()Z

    .line 1434
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljc;->b(I)V

    .line 1435
    iput-object v1, p0, Ljc;->f:Liz;

    .line 1436
    iput-object v1, p0, Ljc;->g:Lix;

    .line 1437
    iput-object v1, p0, Ljc;->s:Lip;

    .line 1438
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 1472
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1473
    iget-object v0, p0, Ljc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 1474
    if-eqz v0, :cond_0

    .line 1476
    invoke-virtual {v0}, Lip;->onLowMemory()V

    .line 1477
    iget-object v2, v0, Lip;->u:Ljc;

    if-eqz v2, :cond_0

    .line 1478
    iget-object v0, v0, Lip;->u:Ljc;

    invoke-virtual {v0}, Ljc;->r()V

    .line 1479
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1480
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 93
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v1, p0, Ljc;->s:Lip;

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Ljc;->s:Lip;

    invoke-static {v1, v0}, Lbw;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 99
    :goto_0
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 98
    :cond_0
    iget-object v1, p0, Ljc;->f:Liz;

    invoke-static {v1, v0}, Lbw;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method
