.class public final enum Lhnp;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhnp;

.field public static final enum b:Lhnp;

.field public static final enum c:Lhnp;

.field private static enum d:Lhnp;

.field private static enum e:Lhnp;

.field private static synthetic g:[Lhnp;


# instance fields
.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lhnp;

    const-string v1, "HTTP_1_0"

    const-string v2, "http/1.0"

    invoke-direct {v0, v1, v3, v2}, Lhnp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhnp;->a:Lhnp;

    .line 13
    new-instance v0, Lhnp;

    const-string v1, "HTTP_1_1"

    const-string v2, "http/1.1"

    invoke-direct {v0, v1, v4, v2}, Lhnp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhnp;->d:Lhnp;

    .line 14
    new-instance v0, Lhnp;

    const-string v1, "SPDY_3"

    const-string v2, "spdy/3.1"

    invoke-direct {v0, v1, v5, v2}, Lhnp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhnp;->e:Lhnp;

    .line 15
    new-instance v0, Lhnp;

    const-string v1, "HTTP_2"

    const-string v2, "h2"

    invoke-direct {v0, v1, v6, v2}, Lhnp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhnp;->b:Lhnp;

    .line 16
    new-instance v0, Lhnp;

    const-string v1, "GRPC_EXP"

    const-string v2, "grpc-exp"

    invoke-direct {v0, v1, v7, v2}, Lhnp;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lhnp;->c:Lhnp;

    .line 17
    const/4 v0, 0x5

    new-array v0, v0, [Lhnp;

    sget-object v1, Lhnp;->a:Lhnp;

    aput-object v1, v0, v3

    sget-object v1, Lhnp;->d:Lhnp;

    aput-object v1, v0, v4

    sget-object v1, Lhnp;->e:Lhnp;

    aput-object v1, v0, v5

    sget-object v1, Lhnp;->b:Lhnp;

    aput-object v1, v0, v6

    sget-object v1, Lhnp;->c:Lhnp;

    aput-object v1, v0, v7

    sput-object v0, Lhnp;->g:[Lhnp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lhnp;->f:Ljava/lang/String;

    .line 4
    return-void
.end method

.method public static a(Ljava/lang/String;)Lhnp;
    .locals 4

    .prologue
    .line 5
    sget-object v0, Lhnp;->a:Lhnp;

    iget-object v0, v0, Lhnp;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhnp;->a:Lhnp;

    .line 9
    :goto_0
    return-object v0

    .line 6
    :cond_0
    sget-object v0, Lhnp;->d:Lhnp;

    iget-object v0, v0, Lhnp;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lhnp;->d:Lhnp;

    goto :goto_0

    .line 7
    :cond_1
    sget-object v0, Lhnp;->b:Lhnp;

    iget-object v0, v0, Lhnp;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lhnp;->b:Lhnp;

    goto :goto_0

    .line 8
    :cond_2
    sget-object v0, Lhnp;->c:Lhnp;

    iget-object v0, v0, Lhnp;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lhnp;->c:Lhnp;

    goto :goto_0

    .line 9
    :cond_3
    sget-object v0, Lhnp;->e:Lhnp;

    iget-object v0, v0, Lhnp;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lhnp;->e:Lhnp;

    goto :goto_0

    .line 10
    :cond_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected protocol: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static values()[Lhnp;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhnp;->g:[Lhnp;

    invoke-virtual {v0}, [Lhnp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhnp;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lhnp;->f:Ljava/lang/String;

    return-object v0
.end method
