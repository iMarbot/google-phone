.class public final Lhg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lhn;

.field public static final b:Landroid/util/Property;

.field private static c:Ljava/lang/reflect/Field;

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    .line 20
    new-instance v0, Lhm;

    invoke-direct {v0}, Lhm;-><init>()V

    sput-object v0, Lhg;->a:Lhn;

    .line 28
    :goto_0
    new-instance v0, Lhh;

    const-class v1, Ljava/lang/Float;

    const-string v2, "translationAlpha"

    invoke-direct {v0, v1, v2}, Lhh;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lhg;->b:Landroid/util/Property;

    .line 29
    new-instance v0, Lhi;

    const-class v1, Landroid/graphics/Rect;

    const-string v2, "clipBounds"

    invoke-direct {v0, v1, v2}, Lhi;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void

    .line 21
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 22
    new-instance v0, Lhl;

    invoke-direct {v0}, Lhl;-><init>()V

    sput-object v0, Lhg;->a:Lhn;

    goto :goto_0

    .line 23
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 24
    new-instance v0, Lhk;

    invoke-direct {v0}, Lhk;-><init>()V

    sput-object v0, Lhg;->a:Lhn;

    goto :goto_0

    .line 25
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_3

    .line 26
    new-instance v0, Lhj;

    invoke-direct {v0}, Lhj;-><init>()V

    sput-object v0, Lhg;->a:Lhn;

    goto :goto_0

    .line 27
    :cond_3
    new-instance v0, Lhn;

    invoke-direct {v0}, Lhn;-><init>()V

    sput-object v0, Lhg;->a:Lhn;

    goto :goto_0
.end method

.method static a(Landroid/view/View;)Lhu;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhg;->a:Lhn;

    invoke-virtual {v0, p0}, Lhn;->a(Landroid/view/View;)Lhu;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lhg;->a:Lhn;

    invoke-virtual {v0, p0, p1}, Lhn;->a(Landroid/view/View;F)V

    .line 3
    return-void
.end method

.method static a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5
    sget-boolean v0, Lhg;->d:Z

    if-nez v0, :cond_0

    .line 6
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "mViewFlags"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 7
    sput-object v0, Lhg;->c:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 10
    :goto_0
    sput-boolean v2, Lhg;->d:Z

    .line 11
    :cond_0
    sget-object v0, Lhg;->c:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_1

    .line 12
    :try_start_1
    sget-object v0, Lhg;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 13
    sget-object v1, Lhg;->c:Ljava/lang/reflect/Field;

    and-int/lit8 v0, v0, -0xd

    or-int/2addr v0, p1

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0

    .line 16
    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static a(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    .line 17
    sget-object v0, Lhg;->a:Lhn;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lhn;->a(Landroid/view/View;IIII)V

    .line 18
    return-void
.end method

.method static b(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lhg;->a:Lhn;

    invoke-virtual {v0, p0}, Lhn;->b(Landroid/view/View;)F

    move-result v0

    return v0
.end method
