.class final Lhny;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Lhuj;

.field public c:I

.field public d:I

.field public e:[Lhnw;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method constructor <init>(ILhuv;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhny;->a:Ljava/util/List;

    .line 3
    const/16 v0, 0x8

    new-array v0, v0, [Lhnw;

    iput-object v0, p0, Lhny;->e:[Lhnw;

    .line 4
    iget-object v0, p0, Lhny;->e:[Lhnw;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhny;->f:I

    .line 5
    iput v1, p0, Lhny;->g:I

    .line 6
    iput v1, p0, Lhny;->h:I

    .line 7
    iput p1, p0, Lhny;->c:I

    .line 8
    iput p1, p0, Lhny;->d:I

    .line 9
    invoke-static {p2}, Lhul;->a(Lhuv;)Lhuj;

    move-result-object v0

    iput-object v0, p0, Lhny;->b:Lhuj;

    .line 10
    return-void
.end method

.method private final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    iget-object v0, p0, Lhny;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 17
    iget-object v0, p0, Lhny;->e:[Lhnw;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 18
    iget-object v0, p0, Lhny;->e:[Lhnw;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhny;->f:I

    .line 19
    iput v2, p0, Lhny;->g:I

    .line 20
    iput v2, p0, Lhny;->h:I

    .line 21
    return-void
.end method

.method static c(I)Z
    .locals 1

    .prologue
    .line 40
    if-ltz p0, :cond_0

    .line 41
    sget-object v0, Lhnx;->a:[Lhnw;

    .line 42
    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lhny;->b:Lhuj;

    invoke-interface {v0}, Lhuj;->c()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private final d(I)I
    .locals 6

    .prologue
    .line 22
    const/4 v1, 0x0

    .line 23
    if-lez p1, :cond_1

    .line 24
    iget-object v0, p0, Lhny;->e:[Lhnw;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, Lhny;->f:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    .line 25
    iget-object v2, p0, Lhny;->e:[Lhnw;

    aget-object v2, v2, v0

    iget v2, v2, Lhnw;->h:I

    sub-int/2addr p1, v2

    .line 26
    iget v2, p0, Lhny;->h:I

    iget-object v3, p0, Lhny;->e:[Lhnw;

    aget-object v3, v3, v0

    iget v3, v3, Lhnw;->h:I

    sub-int/2addr v2, v3

    iput v2, p0, Lhny;->h:I

    .line 27
    iget v2, p0, Lhny;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lhny;->g:I

    .line 28
    add-int/lit8 v1, v1, 0x1

    .line 29
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 30
    :cond_0
    iget-object v0, p0, Lhny;->e:[Lhnw;

    iget v2, p0, Lhny;->f:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lhny;->e:[Lhnw;

    iget v4, p0, Lhny;->f:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, Lhny;->g:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 31
    iget v0, p0, Lhny;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lhny;->f:I

    .line 32
    :cond_1
    return v1
.end method


# virtual methods
.method final a(I)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lhny;->f:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method final a(II)I
    .locals 3

    .prologue
    .line 61
    and-int v0, p1, p2

    .line 62
    if-ge v0, p2, :cond_0

    .line 72
    :goto_0
    return v0

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 66
    :goto_1
    invoke-direct {p0}, Lhny;->d()I

    move-result v1

    .line 67
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 68
    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    .line 69
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 70
    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    .line 71
    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 11
    iget v0, p0, Lhny;->d:I

    iget v1, p0, Lhny;->h:I

    if-ge v0, v1, :cond_0

    .line 12
    iget v0, p0, Lhny;->d:I

    if-nez v0, :cond_1

    .line 13
    invoke-direct {p0}, Lhny;->c()V

    .line 15
    :cond_0
    :goto_0
    return-void

    .line 14
    :cond_1
    iget v0, p0, Lhny;->h:I

    iget v1, p0, Lhny;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lhny;->d(I)I

    goto :goto_0
.end method

.method final a(ILhnw;)V
    .locals 6

    .prologue
    .line 43
    iget-object v0, p0, Lhny;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    iget v0, p2, Lhnw;->h:I

    .line 45
    iget v1, p0, Lhny;->d:I

    if-le v0, v1, :cond_0

    .line 46
    invoke-direct {p0}, Lhny;->c()V

    .line 59
    :goto_0
    return-void

    .line 48
    :cond_0
    iget v1, p0, Lhny;->h:I

    add-int/2addr v1, v0

    iget v2, p0, Lhny;->d:I

    sub-int/2addr v1, v2

    .line 49
    invoke-direct {p0, v1}, Lhny;->d(I)I

    .line 50
    iget v1, p0, Lhny;->g:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lhny;->e:[Lhnw;

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 51
    iget-object v1, p0, Lhny;->e:[Lhnw;

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Lhnw;

    .line 52
    iget-object v2, p0, Lhny;->e:[Lhnw;

    const/4 v3, 0x0

    iget-object v4, p0, Lhny;->e:[Lhnw;

    array-length v4, v4

    iget-object v5, p0, Lhny;->e:[Lhnw;

    array-length v5, v5

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    iget-object v2, p0, Lhny;->e:[Lhnw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lhny;->f:I

    .line 54
    iput-object v1, p0, Lhny;->e:[Lhnw;

    .line 55
    :cond_1
    iget v1, p0, Lhny;->f:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lhny;->f:I

    .line 56
    iget-object v2, p0, Lhny;->e:[Lhnw;

    aput-object p2, v2, v1

    .line 57
    iget v1, p0, Lhny;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhny;->g:I

    .line 58
    iget v1, p0, Lhny;->h:I

    add-int/2addr v0, v1

    iput v0, p0, Lhny;->h:I

    goto :goto_0
.end method

.method final b()Lhuk;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lhny;->d()I

    move-result v2

    .line 74
    and-int/lit16 v0, v2, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    .line 75
    :goto_0
    const/16 v3, 0x7f

    invoke-virtual {p0, v2, v3}, Lhny;->a(II)I

    move-result v2

    .line 76
    if-eqz v0, :cond_5

    .line 77
    sget-object v4, Lhod;->a:Lhod;

    .line 78
    iget-object v0, p0, Lhny;->b:Lhuj;

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lhuj;->e(J)[B

    move-result-object v5

    .line 79
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 80
    iget-object v0, v4, Lhod;->b:Lhoe;

    move v2, v1

    move-object v3, v0

    move v0, v1

    .line 83
    :goto_1
    array-length v7, v5

    if-ge v1, v7, :cond_3

    .line 84
    aget-byte v7, v5, v1

    and-int/lit16 v7, v7, 0xff

    .line 85
    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v7

    .line 86
    add-int/lit8 v0, v0, 0x8

    .line 87
    :goto_2
    const/16 v7, 0x8

    if-lt v0, v7, :cond_2

    .line 88
    add-int/lit8 v7, v0, -0x8

    ushr-int v7, v2, v7

    and-int/lit16 v7, v7, 0xff

    .line 90
    iget-object v3, v3, Lhoe;->a:[Lhoe;

    .line 91
    aget-object v3, v3, v7

    .line 93
    iget-object v7, v3, Lhoe;->a:[Lhoe;

    .line 94
    if-nez v7, :cond_1

    .line 96
    iget v7, v3, Lhoe;->b:I

    .line 97
    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 99
    iget v3, v3, Lhoe;->c:I

    .line 100
    sub-int/2addr v0, v3

    .line 101
    iget-object v3, v4, Lhod;->b:Lhoe;

    goto :goto_2

    :cond_0
    move v0, v1

    .line 74
    goto :goto_0

    .line 102
    :cond_1
    add-int/lit8 v0, v0, -0x8

    .line 103
    goto :goto_2

    .line 104
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 105
    :cond_3
    :goto_3
    if-lez v0, :cond_4

    .line 106
    rsub-int/lit8 v1, v0, 0x8

    shl-int v1, v2, v1

    and-int/lit16 v1, v1, 0xff

    .line 108
    iget-object v3, v3, Lhoe;->a:[Lhoe;

    .line 109
    aget-object v1, v3, v1

    .line 111
    iget-object v3, v1, Lhoe;->a:[Lhoe;

    .line 112
    if-nez v3, :cond_4

    .line 113
    iget v3, v1, Lhoe;->c:I

    .line 114
    if-gt v3, v0, :cond_4

    .line 116
    iget v3, v1, Lhoe;->b:I

    .line 117
    invoke-virtual {v6, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 119
    iget v1, v1, Lhoe;->c:I

    .line 120
    sub-int/2addr v0, v1

    .line 121
    iget-object v3, v4, Lhod;->b:Lhoe;

    goto :goto_3

    .line 123
    :cond_4
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 124
    invoke-static {v0}, Lhuk;->a([B)Lhuk;

    move-result-object v0

    .line 125
    :goto_4
    return-object v0

    :cond_5
    iget-object v0, p0, Lhny;->b:Lhuj;

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lhuj;->c(J)Lhuk;

    move-result-object v0

    goto :goto_4
.end method

.method final b(I)Lhuk;
    .locals 2

    .prologue
    .line 34
    invoke-static {p1}, Lhny;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    sget-object v0, Lhnx;->a:[Lhnw;

    .line 36
    aget-object v0, v0, p1

    iget-object v0, v0, Lhnw;->f:Lhuk;

    .line 39
    :goto_0
    return-object v0

    .line 37
    :cond_0
    iget-object v0, p0, Lhny;->e:[Lhnw;

    .line 38
    sget-object v1, Lhnx;->a:[Lhnw;

    .line 39
    array-length v1, v1

    sub-int v1, p1, v1

    invoke-virtual {p0, v1}, Lhny;->a(I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lhnw;->f:Lhuk;

    goto :goto_0
.end method
