.class final enum Lhqd$b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhqd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "b"
.end annotation


# static fields
.field private static enum b:Lhqd$b;

.field private static enum c:Lhqd$b;

.field private static synthetic e:[Lhqd$b;


# instance fields
.field public a:I

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 14
    new-instance v0, Lhqd$b;

    const-string v1, "NEXUS7"

    const-string v2, "Nexus 7"

    invoke-direct {v0, v1, v4, v2, v5}, Lhqd$b;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lhqd$b;->b:Lhqd$b;

    .line 15
    new-instance v0, Lhqd$b;

    const-string v1, "DEFAULT"

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2, v3}, Lhqd$b;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lhqd$b;->c:Lhqd$b;

    .line 16
    new-array v0, v5, [Lhqd$b;

    sget-object v1, Lhqd$b;->b:Lhqd$b;

    aput-object v1, v0, v4

    sget-object v1, Lhqd$b;->c:Lhqd$b;

    aput-object v1, v0, v3

    sput-object v0, Lhqd$b;->e:[Lhqd$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lhqd$b;->d:Ljava/lang/String;

    .line 4
    iput p4, p0, Lhqd$b;->a:I

    .line 5
    return-void
.end method

.method public static a()Lhqd$b;
    .locals 6

    .prologue
    .line 6
    const-string v0, "Obtaining SingleCameraDeviceInfo for model: "

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    :goto_0
    invoke-static {}, Lhqd$b;->values()[Lhqd$b;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 8
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 9
    iget-object v5, v0, Lhqd$b;->d:Ljava/lang/String;

    .line 10
    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 13
    :goto_2
    return-object v0

    .line 6
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 12
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 13
    :cond_2
    sget-object v0, Lhqd$b;->c:Lhqd$b;

    goto :goto_2
.end method

.method public static values()[Lhqd$b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhqd$b;->e:[Lhqd$b;

    invoke-virtual {v0}, [Lhqd$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhqd$b;

    return-object v0
.end method
