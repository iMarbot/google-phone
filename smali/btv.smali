.class public final enum Lbtv;
.super Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;-><init>(Ljava/lang/String;IB)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 2
    .line 3
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->g:Landroid/widget/TextView;

    .line 4
    const v1, 0x7f1100ba

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 6
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->h:Landroid/widget/TextView;

    .line 7
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    .line 10
    const v1, 0x7f1100c1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 11
    return-void
.end method

.method public final a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 39
    if-nez p2, :cond_0

    .line 41
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a:Lcll;

    .line 42
    invoke-virtual {v0, v1}, Lcll;->a(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->finish()V

    .line 44
    invoke-static {p1}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->bj:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 45
    const v0, 0x7f1100c2

    .line 46
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {p1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 71
    :goto_0
    return-void

    .line 51
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 58
    const-string v0, "VmChangePinActivity"

    const/16 v2, 0x26

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected ChangePinResult "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 61
    :goto_1
    const-string v2, "VmChangePinActivity"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Change PIN failed: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 65
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 66
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->b:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 67
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    goto :goto_0

    .line 52
    :pswitch_0
    const v0, 0x7f11033b

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 53
    :pswitch_1
    const v0, 0x7f11033a

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 54
    :pswitch_2
    const v0, 0x7f11033c

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 55
    :pswitch_3
    const v0, 0x7f110337

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 56
    :pswitch_4
    const v0, 0x7f110338

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 57
    :pswitch_5
    const v0, 0x7f110339

    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 69
    :cond_1
    sget-object v0, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;->d:Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;

    .line 70
    invoke-virtual {p1, v0}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity$d;)V

    goto/16 :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final b(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    .line 14
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 18
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 38
    :goto_0
    return-void

    .line 22
    :cond_0
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 24
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->f:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x1

    .line 28
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 30
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 31
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 36
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->i:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f1100bb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public final c(Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;)V
    .locals 2

    .prologue
    .line 72
    .line 73
    iget-object v0, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->e:Ljava/lang/String;

    .line 75
    iget-object v1, p1, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->f:Ljava/lang/String;

    .line 77
    invoke-virtual {p1, v0, v1}, Lcom/android/dialer/voicemail/settings/VoicemailChangePinActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-void
.end method
