.class public final Lbou;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbov;


# instance fields
.field public b:Lboc;

.field public c:Lboc;

.field public d:Lboc;

.field public e:Ljava/util/List;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lbov;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lbov;-><init>([Ljava/lang/String;)V

    sput-object v0, Lbou;->a:Lbov;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object v0, p0, Lbou;->b:Lboc;

    .line 3
    iput-object v0, p0, Lbou;->c:Lboc;

    .line 4
    iput-object v0, p0, Lbou;->d:Lboc;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbou;->e:Ljava/util/List;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 2

    .prologue
    .line 17
    const/4 v0, 0x0

    .line 18
    iget-object v1, p0, Lbou;->b:Lboc;

    if-eqz v1, :cond_0

    .line 19
    iget-object v0, p0, Lbou;->b:Lboc;

    invoke-interface {v0}, Lboc;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20
    :cond_0
    iget-boolean v1, p0, Lbou;->f:Z

    if-eqz v1, :cond_3

    .line 21
    add-int/lit8 v0, v0, 0x1

    .line 24
    :cond_1
    :goto_0
    iget-object v1, p0, Lbou;->d:Lboc;

    if-eqz v1, :cond_2

    .line 25
    iget-object v1, p0, Lbou;->d:Lboc;

    invoke-interface {v1}, Lboc;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 26
    :cond_2
    iget-object v1, p0, Lbou;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 22
    :cond_3
    iget-object v1, p0, Lbou;->c:Lboc;

    if-eqz v1, :cond_1

    .line 23
    iget-object v1, p0, Lbou;->c:Lboc;

    invoke-interface {v1}, Lboc;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method final a(I)Lboc;
    .locals 2

    .prologue
    .line 27
    iget-boolean v0, p0, Lbou;->f:Z

    if-eqz v0, :cond_1

    .line 28
    if-nez p1, :cond_0

    .line 29
    sget-object v0, Lbou;->a:Lbov;

    .line 47
    :goto_0
    return-object v0

    .line 30
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 31
    :cond_1
    iget-object v0, p0, Lbou;->b:Lboc;

    if-eqz v0, :cond_3

    .line 32
    iget-object v0, p0, Lbou;->b:Lboc;

    invoke-interface {v0}, Lboc;->getCount()I

    move-result v0

    .line 33
    sub-int v1, p1, v0

    if-gez v1, :cond_2

    .line 34
    iget-object v0, p0, Lbou;->b:Lboc;

    invoke-interface {v0, p1}, Lboc;->moveToPosition(I)Z

    .line 35
    iget-object v0, p0, Lbou;->b:Lboc;

    goto :goto_0

    .line 36
    :cond_2
    sub-int/2addr p1, v0

    .line 37
    :cond_3
    iget-boolean v0, p0, Lbou;->f:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lbou;->c:Lboc;

    if-eqz v0, :cond_5

    .line 38
    iget-object v0, p0, Lbou;->c:Lboc;

    invoke-interface {v0}, Lboc;->getCount()I

    move-result v0

    .line 39
    sub-int v1, p1, v0

    if-gez v1, :cond_4

    .line 40
    iget-object v0, p0, Lbou;->c:Lboc;

    invoke-interface {v0, p1}, Lboc;->moveToPosition(I)Z

    .line 41
    iget-object v0, p0, Lbou;->c:Lboc;

    goto :goto_0

    .line 42
    :cond_4
    sub-int/2addr p1, v0

    .line 43
    :cond_5
    iget-object v0, p0, Lbou;->d:Lboc;

    if-eqz v0, :cond_6

    .line 44
    iget-object v0, p0, Lbou;->d:Lboc;

    invoke-interface {v0}, Lboc;->getCount()I

    move-result v0

    .line 45
    sub-int v0, p1, v0

    if-gez v0, :cond_6

    .line 46
    iget-object v0, p0, Lbou;->d:Lboc;

    invoke-interface {v0, p1}, Lboc;->moveToPosition(I)Z

    .line 47
    iget-object v0, p0, Lbou;->d:Lboc;

    goto :goto_0

    .line 48
    :cond_6
    const-string v0, "No valid cursor."

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lbou;->b:Lboc;

    if-eqz v0, :cond_0

    .line 11
    iget-object v0, p0, Lbou;->b:Lboc;

    invoke-interface {v0}, Lboc;->b()Z

    .line 12
    :cond_0
    iget-object v0, p0, Lbou;->c:Lboc;

    if-eqz v0, :cond_1

    .line 13
    iget-object v0, p0, Lbou;->c:Lboc;

    invoke-interface {v0}, Lboc;->b()Z

    .line 14
    :cond_1
    iget-object v0, p0, Lbou;->d:Lboc;

    if-eqz v0, :cond_2

    .line 15
    iget-object v0, p0, Lbou;->d:Lboc;

    invoke-interface {v0}, Lboc;->b()Z

    .line 16
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method final a(Z)Z
    .locals 1

    .prologue
    .line 6
    iget-boolean v0, p0, Lbou;->f:Z

    if-ne v0, p1, :cond_0

    .line 7
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0

    .line 8
    :cond_0
    iput-boolean p1, p0, Lbou;->f:Z

    .line 9
    const/4 v0, 0x1

    goto :goto_0
.end method
