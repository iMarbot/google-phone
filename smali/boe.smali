.class public final Lboe;
.super Landroid/content/CursorLoader;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1
    .line 3
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 5
    invoke-static {p1}, Lboe;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v3

    .line 7
    invoke-static {p1}, Lboe;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x4

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, " IS NOT NULL"

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 8
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, " AND data1 IS NOT NULL"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 10
    :goto_0
    const/4 v5, 0x0

    .line 12
    new-instance v1, Lalj;

    invoke-direct {v1, p1}, Lalj;-><init>(Landroid/content/Context;)V

    .line 14
    invoke-virtual {v1}, Lalj;->a()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 15
    :goto_1
    if-eqz v0, :cond_3

    const-string v0, "sort_key"

    .line 16
    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    .line 17
    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, ""

    :cond_0
    iput-object p2, p0, Lboe;->a:Ljava/lang/String;

    .line 19
    iput-boolean p3, p0, Lboe;->b:Z

    .line 20
    return-void

    .line 8
    :cond_1
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 14
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 15
    :cond_3
    const-string v0, "sort_key_alt"

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 21
    new-instance v1, Lalj;

    invoke-direct {v1, p0}, Lalj;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {v1}, Lalj;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 24
    :goto_0
    if-eqz v0, :cond_1

    .line 25
    sget-object v0, Lbnz;->a:[Ljava/lang/String;

    .line 27
    :goto_1
    return-object v0

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26
    :cond_1
    sget-object v0, Lbnz;->b:[Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 28
    iget-boolean v0, p0, Lboe;->b:Z

    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lboe;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lbof;->a(Landroid/content/Context;Landroid/database/Cursor;)Lbof;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    new-instance v0, Lbil;

    invoke-virtual {p0}, Lboe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbil;-><init>(Landroid/content/Context;)V

    .line 32
    iget-object v1, p0, Lboe;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbil;->a(Ljava/lang/String;)V

    .line 33
    invoke-virtual {v0}, Lbil;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 34
    invoke-virtual {p0}, Lboe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lbog;->a(Landroid/content/Context;Landroid/database/Cursor;)Lbog;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lboe;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
