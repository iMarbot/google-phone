.class public final Layc;
.super Laya;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Laza;
.implements Lazb;


# instance fields
.field public W:Landroid/net/Uri;

.field public X:Z

.field public Y:Laye;

.field private Z:Landroid/view/View;

.field public a:Landroid/widget/ProgressBar;

.field private aa:Landroid/widget/ImageButton;

.field private ab:Landroid/widget/ImageButton;

.field private ac:Landroid/widget/ImageButton;

.field private ad:Landroid/widget/ImageButton;

.field private ae:Landroid/widget/ImageButton;

.field private af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

.field private ag:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

.field private ah:Landroid/view/View;

.field private ai:Landroid/view/View;

.field private aj:Lazf;

.field private ak:Landroid/widget/ImageView;

.field private al:[Ljava/lang/String;

.field private am:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Laya;-><init>()V

    .line 2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    iput-object v0, p0, Layc;->al:[Ljava/lang/String;

    .line 3
    iput v2, p0, Layc;->am:I

    return-void
.end method

.method public static W()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    const-string v0, "image/jpeg"

    return-object v0
.end method

.method private final X()V
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->k(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->l(Landroid/content/Context;)V

    .line 43
    :cond_0
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    invoke-virtual {v0, p0}, Layq;->a(Laza;)V

    .line 44
    iget-object v0, p0, Layc;->aj:Lazf;

    invoke-interface {v0}, Lazf;->c()V

    .line 45
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    iget-object v1, p0, Layc;->ag:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    .line 46
    iget-object v2, v0, Layq;->n:Lazn;

    .line 47
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->a()Lazr;

    move-result-object v0

    .line 49
    :goto_0
    iput-object v0, v2, Lazn;->g:Lazr;

    .line 50
    iget-object v0, v2, Lazn;->f:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lazn;->b:Z

    .line 51
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    iget v1, p0, Layc;->am:I

    invoke-virtual {v0, v1}, Layq;->a(I)Z

    .line 52
    iget-object v0, p0, Layc;->W:Landroid/net/Uri;

    invoke-direct {p0, v0}, Layc;->b(Landroid/net/Uri;)V

    .line 53
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final Y()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v5, 0x8

    .line 170
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-boolean v0, p0, Lip;->B:Z

    .line 173
    if-nez v0, :cond_0

    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 174
    :cond_0
    const-string v0, "CameraComposerFragment.updateViewState"

    const-string v1, "Fragment detached, cannot update view state"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    .line 177
    iget-object v3, v0, Layq;->j:Landroid/hardware/Camera;

    if-eqz v3, :cond_6

    iget-boolean v3, v0, Layq;->m:Z

    if-nez v3, :cond_6

    iget-boolean v0, v0, Layq;->g:Z

    if-eqz v0, :cond_6

    move v0, v1

    .line 179
    :goto_1
    iget-object v3, p0, Layc;->W:Landroid/net/Uri;

    if-nez v3, :cond_2

    iget-boolean v3, p0, Layc;->X:Z

    if-eqz v3, :cond_7

    :cond_2
    move v3, v1

    .line 180
    :goto_2
    iget-object v4, p0, Layc;->W:Landroid/net/Uri;

    if-eqz v4, :cond_9

    .line 181
    iget-object v4, p0, Layc;->ak:Landroid/widget/ImageView;

    iget-object v6, p0, Layc;->W:Landroid/net/Uri;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 182
    iget-object v4, p0, Layc;->ak:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    iget-object v6, p0, Layc;->ak:Landroid/widget/ImageView;

    iget v4, p0, Layc;->am:I

    if-ne v4, v1, :cond_8

    const/high16 v4, -0x40800000    # -1.0f

    :goto_3
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 185
    :goto_4
    iget v4, p0, Layc;->am:I

    if-ne v4, v1, :cond_a

    .line 186
    iget-object v4, p0, Layc;->ac:Landroid/widget/ImageButton;

    const v6, 0x7f110106

    invoke-virtual {p0, v6}, Layc;->b_(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 188
    :goto_5
    iget-object v4, p0, Layc;->W:Landroid/net/Uri;

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    .line 189
    invoke-static {}, Layq;->a()Layq;

    move-result-object v4

    .line 190
    iget-object v6, v4, Layq;->j:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->startPreview()V

    .line 191
    iget-object v6, v4, Layq;->f:Laze;

    if-eqz v6, :cond_3

    .line 192
    iget-object v4, v4, Layq;->f:Laze;

    invoke-virtual {v4, v1}, Laze;->a(Z)V

    .line 193
    :cond_3
    iget-object v1, p0, Layc;->ae:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 194
    :cond_4
    invoke-static {}, Layq;->a()Layq;

    move-result-object v1

    .line 195
    iget-boolean v1, v1, Layq;->d:Z

    .line 196
    if-nez v1, :cond_b

    .line 197
    iget-object v1, p0, Layc;->ac:Landroid/widget/ImageButton;

    move-object v4, v1

    move v1, v5

    .line 198
    :goto_6
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 199
    iget-object v4, p0, Layc;->ad:Landroid/widget/ImageButton;

    if-eqz v3, :cond_d

    move v1, v5

    :goto_7
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 200
    iget-object v4, p0, Layc;->ae:Landroid/widget/ImageButton;

    if-eqz v3, :cond_e

    move v1, v2

    :goto_8
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 201
    if-nez v3, :cond_5

    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v1

    invoke-interface {v1}, Laya$a;->i()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 202
    :cond_5
    iget-object v1, p0, Layc;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 203
    iget-object v1, p0, Layc;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 209
    :goto_9
    iget-object v1, p0, Layc;->ac:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 210
    iget-object v1, p0, Layc;->ad:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 177
    goto/16 :goto_1

    :cond_7
    move v3, v2

    .line 179
    goto/16 :goto_2

    .line 183
    :cond_8
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_3

    .line 184
    :cond_9
    iget-object v4, p0, Layc;->ak:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 187
    :cond_a
    iget-object v4, p0, Layc;->ac:Landroid/widget/ImageButton;

    const v6, 0x7f110105

    invoke-virtual {p0, v6}, Layc;->b_(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 198
    :cond_b
    iget-object v1, p0, Layc;->ac:Landroid/widget/ImageButton;

    if-eqz v3, :cond_c

    move-object v4, v1

    move v1, v5

    goto :goto_6

    :cond_c
    move-object v4, v1

    move v1, v2

    goto :goto_6

    :cond_d
    move v1, v2

    .line 199
    goto :goto_7

    :cond_e
    move v1, v5

    .line 200
    goto :goto_8

    .line 204
    :cond_f
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v1

    invoke-interface {v1}, Laya$a;->j()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 205
    iget-object v1, p0, Layc;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 206
    iget-object v1, p0, Layc;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_9

    .line 207
    :cond_10
    iget-object v1, p0, Layc;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 208
    iget-object v1, p0, Layc;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_9
.end method

.method private final b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 160
    iput-object p1, p0, Layc;->W:Landroid/net/Uri;

    .line 161
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0}, Layc;->Y()V

    .line 163
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v0

    invoke-interface {v0, p0}, Laya$a;->a(Laya;)V

    .line 164
    :cond_0
    return-void
.end method


# virtual methods
.method public final U()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Layc;->X:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Layc;->W:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final V()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Layc;->X:Z

    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Layc;->b(Landroid/net/Uri;)V

    .line 61
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4
    const v0, 0x7f040066

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 5
    const v0, 0x7f0e01c7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Layc;->Z:Landroid/view/View;

    .line 6
    const v0, 0x7f0e0155

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Layc;->a:Landroid/widget/ProgressBar;

    .line 7
    const v0, 0x7f0e0149

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    iput-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    .line 8
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e014d

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Layc;->ah:Landroid/view/View;

    .line 9
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e0151

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Layc;->aa:Landroid/widget/ImageButton;

    .line 10
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e0150

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Layc;->ab:Landroid/widget/ImageButton;

    .line 11
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e0153

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Layc;->ac:Landroid/widget/ImageButton;

    .line 12
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e0152

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Layc;->ad:Landroid/widget/ImageButton;

    .line 13
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e0154

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Layc;->ae:Landroid/widget/ImageButton;

    .line 14
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e014c

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    iput-object v0, p0, Layc;->ag:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    .line 15
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    const v1, 0x7f0e014b

    invoke-virtual {v0, v1}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lazf;

    iput-object v0, p0, Layc;->aj:Lazf;

    .line 16
    const v0, 0x7f0e014e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Layc;->ak:Landroid/widget/ImageView;

    .line 17
    iget-object v0, p0, Layc;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    iget-object v0, p0, Layc;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    iget-object v0, p0, Layc;->ac:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    iget-object v0, p0, Layc;->ad:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    iget-object v0, p0, Layc;->ae:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    const-string v0, "CameraComposerFragment.onCreateView"

    const-string v1, "Permission view shown."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->Y:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 25
    iget-object v0, p0, Layc;->Z:Landroid/view/View;

    const v1, 0x7f0e0237

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 26
    iget-object v1, p0, Layc;->Z:Landroid/view/View;

    const v3, 0x7f0e020a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 27
    iget-object v3, p0, Layc;->Z:Landroid/view/View;

    const v4, 0x7f0e0238

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Layc;->ai:Landroid/view/View;

    .line 28
    iget-object v3, p0, Layc;->ai:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    const v3, 0x7f1100b7

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 30
    const v1, 0x7f020137

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 32
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0c0071

    invoke-static {v1, v3}, Llw;->c(Landroid/content/Context;I)I

    move-result v1

    .line 33
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 34
    iget-object v0, p0, Layc;->Z:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 40
    :goto_0
    return-object v2

    .line 36
    :cond_0
    if-eqz p3, :cond_1

    .line 37
    const-string v0, "camera_direction"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Layc;->am:I

    .line 38
    const-string v0, "camera_key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Layc;->W:Landroid/net/Uri;

    .line 39
    :cond_1
    invoke-direct {p0}, Layc;->X()V

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Layc;->Y()V

    .line 57
    return-void
.end method

.method public final a(ILjava/lang/Exception;)V
    .locals 5

    .prologue
    .line 54
    const-string v0, "CameraComposerFragment.onCameraError"

    const-string v1, "errorCode: "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method public final a(I[Ljava/lang/String;[I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 216
    array-length v0, p2

    if-lez v0, :cond_0

    aget-object v0, p2, v2

    iget-object v1, p0, Layc;->al:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    aget-object v1, p2, v2

    invoke-static {v0, v1}, Lbsw;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 218
    :cond_0
    if-ne p1, v3, :cond_2

    array-length v0, p3

    if-lez v0, :cond_2

    aget v0, p3, v2

    if-nez v0, :cond_2

    .line 219
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ae:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 220
    const-string v0, "CameraComposerFragment.onRequestPermissionsResult"

    const-string v1, "Permission granted."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Layc;->Z:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->m(Landroid/content/Context;)V

    .line 223
    invoke-direct {p0}, Layc;->X()V

    .line 227
    :cond_1
    :goto_0
    return-void

    .line 224
    :cond_2
    if-ne p1, v3, :cond_1

    .line 225
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ag:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 226
    const-string v0, "CameraComposerFragment.onRequestPermissionsResult"

    const-string v1, "Permission denied."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Layc;->X:Z

    if-eqz v0, :cond_1

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Layc;->X:Z

    .line 137
    invoke-direct {p0, p1}, Layc;->b(Landroid/net/Uri;)V

    .line 138
    iget-object v0, p0, Layc;->Y:Laye;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Layc;->Y:Laye;

    invoke-virtual {v0, p1}, Laye;->a(Landroid/net/Uri;)V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Layc;->Y:Laye;

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-direct {p0}, Layc;->Y()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    const-string v0, "CallComposerFragment.onMediaFailed"

    invoke-static {v0, v3, p1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1100b6

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 145
    invoke-direct {p0, v3}, Layc;->b(Landroid/net/Uri;)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Layc;->X:Z

    .line 147
    iget-object v0, p0, Layc;->Y:Laye;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Layc;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 149
    iput-object v3, p0, Layc;->Y:Laye;

    .line 150
    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 152
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1100b6

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 153
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Layc;->b(Landroid/net/Uri;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Layc;->X:Z

    .line 155
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1}, Laya;->e(Landroid/os/Bundle;)V

    .line 213
    const-string v0, "camera_direction"

    iget v1, p0, Layc;->am:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v0, "camera_key"

    iget-object v1, p0, Layc;->W:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 215
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/16 v8, 0x64

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62
    iget-object v0, p0, Layc;->ad:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_5

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    .line 64
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v1

    invoke-interface {v1}, Laya$a;->j()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v1

    invoke-interface {v1}, Laya$a;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 65
    iget-object v0, p0, Layc;->af:Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;

    invoke-virtual {v0}, Lcom/android/dialer/callcomposer/cameraui/CameraMediaChooserView;->getHeight()I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Layc;->aj:Lazf;

    if-nez v0, :cond_0

    throw v4

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 66
    :cond_1
    iget-object v1, p0, Layc;->ah:Landroid/view/View;

    .line 67
    new-instance v5, Landroid/view/animation/AnimationSet;

    invoke-direct {v5, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 68
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const v7, 0x3f333333    # 0.7f

    invoke-direct {v6, v10, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 69
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 70
    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 71
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const v7, 0x3f333333    # 0.7f

    invoke-direct {v6, v7, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 72
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 73
    invoke-virtual {v6, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 74
    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 75
    new-instance v6, Layd;

    invoke-direct {v6, v1}, Layd;-><init>(Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 76
    invoke-virtual {v1, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 77
    iput-boolean v2, p0, Layc;->X:Z

    .line 78
    invoke-direct {p0, v4}, Layc;->b(Landroid/net/Uri;)V

    .line 79
    iget-object v1, p0, Layc;->ag:Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;

    invoke-virtual {v1}, Lcom/android/dialer/callcomposer/camera/camerafocus/RenderOverlay;->a()Lazr;

    move-result-object v1

    invoke-virtual {v1}, Lazr;->b()V

    .line 80
    invoke-static {}, Layq;->a()Layq;

    move-result-object v5

    .line 81
    iget-boolean v1, v5, Layq;->m:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-static {v1}, Lbdf;->b(Z)V

    .line 82
    invoke-static {p0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v1, v5, Layq;->f:Laze;

    invoke-virtual {v1, v3}, Laze;->a(Z)V

    .line 84
    iget-object v1, v5, Layq;->n:Lazn;

    invoke-virtual {v1}, Lazn;->c()V

    .line 85
    iget-object v1, v5, Layq;->j:Landroid/hardware/Camera;

    if-nez v1, :cond_4

    .line 86
    invoke-interface {p0, v4}, Lazb;->a(Ljava/lang/Exception;)V

    .line 134
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v1, v3

    .line 81
    goto :goto_0

    .line 88
    :cond_4
    new-instance v1, Layu;

    invoke-direct {v1, v5, p0, v0}, Layu;-><init>(Layq;Lazb;F)V

    .line 89
    iput-boolean v2, v5, Layq;->m:Z

    .line 90
    :try_start_0
    iget-object v0, v5, Layq;->j:Landroid/hardware/Camera;

    sget-object v2, Layq;->a:Landroid/hardware/Camera$ShutterCallback;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v2, v4, v6, v1}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "CameraManager.takePicture"

    const-string v2, "RuntimeException in CameraManager.takePicture"

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    iput-boolean v3, v5, Layq;->m:Z

    .line 95
    iget-object v1, v5, Layq;->l:Laza;

    if-eqz v1, :cond_2

    .line 96
    iget-object v1, v5, Layq;->l:Laza;

    const/4 v2, 0x4

    invoke-interface {v1, v2, v0}, Laza;->a(ILjava/lang/Exception;)V

    goto :goto_1

    .line 97
    :cond_5
    iget-object v0, p0, Layc;->ac:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_9

    .line 98
    iget-object v0, p0, Layc;->ac:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 99
    invoke-static {}, Layq;->a()Layq;

    move-result-object v1

    .line 100
    iget v0, v1, Layq;->c:I

    if-ltz v0, :cond_6

    move v0, v2

    :goto_2
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 102
    iget-object v0, v1, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v2, :cond_7

    .line 105
    :goto_3
    invoke-virtual {v1, v3}, Layq;->a(I)Z

    .line 106
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    .line 107
    iget v1, v0, Layq;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_8

    move-object v0, v4

    .line 110
    :goto_4
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    iput v0, p0, Layc;->am:I

    goto :goto_1

    :cond_6
    move v0, v3

    .line 100
    goto :goto_2

    :cond_7
    move v3, v2

    .line 104
    goto :goto_3

    .line 109
    :cond_8
    iget-object v0, v0, Layq;->b:Landroid/hardware/Camera$CameraInfo;

    goto :goto_4

    .line 111
    :cond_9
    iget-object v0, p0, Layc;->ae:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_a

    .line 112
    invoke-virtual {p0}, Layc;->V()V

    goto :goto_1

    .line 113
    :cond_a
    iget-object v0, p0, Layc;->aa:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_b

    .line 114
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v0

    invoke-interface {v0, v3}, Laya$a;->b(Z)V

    .line 115
    iget-object v0, p0, Layc;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Layc;->aa:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 117
    :cond_b
    iget-object v0, p0, Layc;->ab:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_c

    .line 118
    invoke-virtual {p0}, Layc;->T()Laya$a;

    move-result-object v0

    invoke-interface {v0, v2}, Laya$a;->b(Z)V

    .line 119
    iget-object v0, p0, Layc;->ab:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Layc;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 121
    :cond_c
    iget-object v0, p0, Layc;->ai:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 122
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Layc;->al:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-static {v0, v1}, Lbsw;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Layc;->al:[Ljava/lang/String;

    aget-object v0, v0, v3

    .line 123
    invoke-virtual {p0, v0}, Layc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 124
    :cond_d
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->aa:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 125
    const-string v0, "CameraComposerFragment.onClick"

    const-string v1, "Camera permission requested."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Layc;->al:[Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Layc;->a([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 127
    :cond_e
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->ac:Lbkq$a;

    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 128
    const-string v0, "CameraComposerFragment.onClick"

    const-string v1, "Settings opened to enable permission."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 132
    const-string v2, "package:"

    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p0, v1}, Layc;->a(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 132
    :cond_f
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Laya;->r()V

    .line 166
    invoke-virtual {p0}, Layc;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Layc;->Z:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    invoke-direct {p0}, Layc;->X()V

    .line 169
    :cond_0
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Laya;->t()V

    .line 157
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Layq;->a(Laza;)V

    .line 158
    return-void
.end method
