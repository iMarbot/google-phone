.class public interface abstract Lfpq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ENDPOINT_AUDIO_MUTE_STATE_CHANGED:I = 0x4

.field public static final ENDPOINT_CHANGED:I = 0x3

.field public static final ENDPOINT_EXITED:I = 0x2

.field public static final ENDPOINT_REMOTE_AUDIO_MUTE_REQUESTED:I = 0x6

.field public static final ENDPOINT_VIDEO_MUTE_STATE_CHANGED:I = 0x5

.field public static final LOCAL_ENDPOINT_ENTERED:I = 0x0

.field public static final REMOTE_ENDPOINT_CROPPABLE_CHANGED:I = 0x7

.field public static final REMOTE_ENDPOINT_ENTERED:I = 0x1


# virtual methods
.method public abstract handleCallEnd(ILjava/lang/String;)V
.end method

.method public abstract handleClientDataMessage(Ljava/lang/String;[B)V
.end method

.method public abstract handleCloudHandoffCompleted([B)V
.end method

.method public abstract handleConferenceUpdate(IILjava/lang/String;[B)V
.end method

.method public abstract handleConnectionQualityUpdate([B)V
.end method

.method public abstract handleEndpointEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method public abstract handleFirstPacketReceived(I)V
.end method

.method public abstract handleLoggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
.end method

.method public abstract handleLoudestSpeakerUpdate(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract handleMediaStateChanged(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract handleMeetingsPush([B)V
.end method

.method public abstract handleP2pHandoffCompleted([B)V
.end method

.method public abstract handleRemoteSessionConnected(Ljava/lang/String;)V
.end method

.method public abstract handleRequestTerminate(I)V
.end method

.method public abstract handleSignedInStateUpdate(Z)V
.end method

.method public abstract handleStreamRequest([B)V
.end method

.method public abstract handleTimingLogEntry([BLjava/lang/String;)V
.end method

.method public abstract handleUmaEvent(JII)V
.end method

.method public abstract handleUnloggedStatsUpdate(Lcom/google/android/libraries/hangouts/video/internal/Stats;)V
.end method

.method public abstract handleVideoSourcesUpdate(Ljava/lang/String;Lcom/google/android/libraries/hangouts/video/internal/MediaSources;)V
.end method

.method public abstract handleVolumeLevelUpdate(ILjava/lang/String;)V
.end method

.method public abstract makeApiaryRequest(JLjava/lang/String;[BI)V
.end method

.method public abstract reportImpression(ILjava/lang/String;)V
.end method

.method public abstract reportTransportEvent([BJ)V
.end method
