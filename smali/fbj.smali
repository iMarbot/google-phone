.class public final Lfbj;
.super Lfat;


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Lfbh;

.field private c:Z

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Exception;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfat;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfbj;->a:Ljava/lang/Object;

    new-instance v0, Lfbh;

    invoke-direct {v0}, Lfbh;-><init>()V

    iput-object v0, p0, Lfbj;->b:Lfbh;

    return-void
.end method

.method private final e()V
    .locals 2

    iget-boolean v0, p0, Lfbj;->c:Z

    const-string v1, "Task is not yet complete"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    return-void
.end method

.method private final f()V
    .locals 2

    iget-boolean v0, p0, Lfbj;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Task is already complete"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final g()V
    .locals 2

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lfbj;->c:Z

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfbj;->b:Lfbh;

    invoke-virtual {v0, p0}, Lfbh;->a(Lfat;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lfao;)Lfat;
    .locals 1

    sget-object v0, Lfav;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfao;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfap;)Lfat;
    .locals 1

    sget-object v0, Lfav;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfap;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfaq;)Lfat;
    .locals 1

    sget-object v0, Lfav;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfaq;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfar;)Lfat;
    .locals 1

    sget-object v0, Lfav;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Lfat;->a(Ljava/util/concurrent/Executor;Lfar;)Lfat;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Executor;Lfao;)Lfat;
    .locals 3

    new-instance v0, Lfbj;

    invoke-direct {v0}, Lfbj;-><init>()V

    iget-object v1, p0, Lfbj;->b:Lfbh;

    new-instance v2, Lfay;

    invoke-direct {v2, p1, p2, v0}, Lfay;-><init>(Ljava/util/concurrent/Executor;Lfao;Lfbj;)V

    invoke-virtual {v1, v2}, Lfbh;->a(Lfbg;)V

    invoke-direct {p0}, Lfbj;->g()V

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Executor;Lfap;)Lfat;
    .locals 2

    iget-object v0, p0, Lfbj;->b:Lfbh;

    new-instance v1, Lfba;

    invoke-direct {v1, p1, p2}, Lfba;-><init>(Ljava/util/concurrent/Executor;Lfap;)V

    invoke-virtual {v0, v1}, Lfbh;->a(Lfbg;)V

    invoke-direct {p0}, Lfbj;->g()V

    return-object p0
.end method

.method public final a(Ljava/util/concurrent/Executor;Lfaq;)Lfat;
    .locals 2

    iget-object v0, p0, Lfbj;->b:Lfbh;

    new-instance v1, Lfbc;

    invoke-direct {v1, p1, p2}, Lfbc;-><init>(Ljava/util/concurrent/Executor;Lfaq;)V

    invoke-virtual {v0, v1}, Lfbh;->a(Lfbg;)V

    invoke-direct {p0}, Lfbj;->g()V

    return-object p0
.end method

.method public final a(Ljava/util/concurrent/Executor;Lfar;)Lfat;
    .locals 2

    iget-object v0, p0, Lfbj;->b:Lfbh;

    new-instance v1, Lfbe;

    invoke-direct {v1, p1, p2}, Lfbe;-><init>(Ljava/util/concurrent/Executor;Lfar;)V

    invoke-virtual {v0, v1}, Lfbh;->a(Lfbg;)V

    invoke-direct {p0}, Lfbj;->g()V

    return-object p0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfbj;->e()V

    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    new-instance v0, Lfas;

    iget-object v2, p0, Lfbj;->e:Ljava/lang/Exception;

    invoke-direct {v0, v2}, Lfas;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lfbj;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Exception must not be null"

    invoke-static {p1, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfbj;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfbj;->c:Z

    iput-object p1, p0, Lfbj;->e:Ljava/lang/Exception;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfbj;->b:Lfbh;

    invoke-virtual {v0, p0}, Lfbh;->a(Lfat;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfbj;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfbj;->c:Z

    iput-object p1, p0, Lfbj;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lfbj;->b:Lfbh;

    invoke-virtual {v0, p0}, Lfbh;->a(Lfat;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lfbj;->c:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()Z
    .locals 2

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lfbj;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Exception;)Z
    .locals 3

    const/4 v0, 0x1

    const-string v1, "Exception must not be null"

    invoke-static {p1, v1}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lfbj;->c:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lfbj;->c:Z

    iput-object p1, p0, Lfbj;->e:Ljava/lang/Exception;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lfbj;->b:Lfbh;

    invoke-virtual {v1, p0}, Lfbh;->a(Lfat;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Lfbj;->c:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lfbj;->c:Z

    iput-object p1, p0, Lfbj;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lfbj;->b:Lfbh;

    invoke-virtual {v1, p0}, Lfbh;->a(Lfat;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 3

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfbj;->e()V

    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    new-instance v0, Lfas;

    iget-object v2, p0, Lfbj;->e:Ljava/lang/Exception;

    invoke-direct {v0, v2}, Lfas;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lfbj;->d:Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final d()Ljava/lang/Exception;
    .locals 2

    iget-object v1, p0, Lfbj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfbj;->e:Ljava/lang/Exception;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
