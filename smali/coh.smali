.class public final Lcoh;
.super Lcoe;
.source "PG"


# instance fields
.field public final e:Z

.field private f:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcoe;-><init>()V

    .line 2
    iput-object p1, p0, Lcoh;->f:Ljava/lang/String;

    .line 3
    iput-boolean p2, p0, Lcoh;->e:Z

    .line 4
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcoh;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcoh;->c(I)Lcol;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcol;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcoh;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p0, v0}, Lcoh;->c(I)Lcol;

    move-result-object v1

    invoke-virtual {v1}, Lcol;->e()Ljava/lang/String;

    move-result-object v1

    .line 7
    const-string v2, "OK"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "NO"

    .line 8
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "BAD"

    .line 9
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "PREAUTH"

    .line 10
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "BYE"

    .line 11
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 12
    :cond_1
    return v0
.end method

.method public final h()Lcol;
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcoh;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    sget-object v0, Lcol;->d:Lcol;

    .line 16
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcoh;->b(I)Lcoe;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcoe;->c(I)Lcol;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcol;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 17
    invoke-virtual {p0}, Lcoh;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18
    sget-object v0, Lcol;->d:Lcol;

    .line 19
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcoh;->a(I)Lcoc;

    move-result-object v1

    invoke-virtual {v1}, Lcoc;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    :cond_1
    invoke-virtual {p0, v0}, Lcoh;->c(I)Lcol;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 20
    iget-object v0, p0, Lcoh;->f:Ljava/lang/String;

    .line 22
    iget-boolean v1, p0, Lcoh;->e:Z

    .line 23
    if-eqz v1, :cond_0

    .line 24
    const-string v0, "+"

    .line 25
    :cond_0
    invoke-super {p0}, Lcoe;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "#"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "# "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
