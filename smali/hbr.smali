.class public abstract Lhbr;
.super Lgzw;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhbr$d;,
        Lhbr$b;,
        Lhbr$c;,
        Lhbr$a;,
        Lhbr$e;
    }
.end annotation


# static fields
.field public static final ENABLE_EXPERIMENTAL_RUNTIME_AT_BUILD_TIME:Z = true

.field public static defaultInstanceMap:Ljava/util/Map;


# instance fields
.field public memoizedSerializedSize:I

.field public unknownFields:Lheq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 323
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lhbr;->defaultInstanceMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lgzw;-><init>()V

    .line 3
    sget-object v0, Lheq;->a:Lheq;

    .line 4
    iput-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Lhbr;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$000(Lhbe;)Lhbr$d;
    .locals 1

    .prologue
    .line 322
    invoke-static {p0}, Lhbr;->checkIsLite(Lhbe;)Lhbr$d;

    move-result-object v0

    return-object v0
.end method

.method private static checkIsLite(Lhbe;)Lhbr$d;
    .locals 0

    .prologue
    .line 186
    check-cast p0, Lhbr$d;

    return-object p0
.end method

.method private static checkMessageInitialized(Lhbr;)Lhbr;
    .locals 1

    .prologue
    .line 248
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lhbr;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 249
    invoke-virtual {p0}, Lhbr;->newUninitializedMessageException()Lheo;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Lheo;->a()Lhcf;

    move-result-object v0

    .line 251
    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    throw v0

    .line 252
    :cond_1
    return-object p0
.end method

.method protected static emptyBooleanList()Lhbv;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lhaf;->b:Lhaf;

    .line 227
    return-object v0
.end method

.method protected static emptyDoubleList()Lhbw;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lhbd;->b:Lhbd;

    .line 221
    return-object v0
.end method

.method protected static emptyFloatList()Lhbz;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lhbo;->b:Lhbo;

    .line 215
    return-object v0
.end method

.method public static emptyIntList()Lhca;
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lhbt;->b:Lhbt;

    .line 203
    return-object v0
.end method

.method public static emptyLongList()Lhcd;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lhcr;->b:Lhcr;

    .line 209
    return-object v0
.end method

.method public static emptyProtobufList()Lhce;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lhdq;->b:Lhdq;

    .line 233
    return-object v0
.end method

.method private final ensureUnknownFieldsInitialized()V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 23
    sget-object v1, Lheq;->a:Lheq;

    .line 24
    if-ne v0, v1, :cond_0

    .line 26
    new-instance v0, Lheq;

    invoke-direct {v0}, Lheq;-><init>()V

    .line 27
    iput-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 28
    :cond_0
    return-void
.end method

.method protected static fieldInfo(Ljava/lang/reflect/Field;ILhbm;)Lhbj;
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lhbr;->fieldInfo(Ljava/lang/reflect/Field;ILhbm;Z)Lhbj;

    move-result-object v0

    return-object v0
.end method

.method protected static fieldInfo(Ljava/lang/reflect/Field;ILhbm;Z)Lhbj;
    .locals 13

    .prologue
    .line 111
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 112
    :cond_0
    invoke-static {p1}, Lhbj;->a(I)V

    .line 113
    const-string v0, "field"

    invoke-static {p0, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 114
    const-string v0, "fieldType"

    invoke-static {p2, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lhbm;->f:Lhbm;

    if-eq p2, v0, :cond_1

    sget-object v0, Lhbm;->i:Lhbm;

    if-ne p2, v0, :cond_2

    .line 116
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shouldn\'t be called for repeated message fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_2
    new-instance v0, Lhbj;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move/from16 v8, p3

    invoke-direct/range {v0 .. v12}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method protected static fieldInfoForMap(Ljava/lang/reflect/Field;ILjava/lang/Object;Lhby;)Lhbj;
    .locals 13

    .prologue
    .line 175
    if-nez p0, :cond_0

    .line 176
    const/4 v0, 0x0

    .line 182
    :goto_0
    return-object v0

    .line 178
    :cond_0
    const-string v0, "mapDefaultEntry"

    invoke-static {p2, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 179
    invoke-static {p1}, Lhbj;->a(I)V

    .line 180
    const-string v0, "field"

    invoke-static {p0, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 181
    new-instance v0, Lhbj;

    sget-object v3, Lhbm;->j:Lhbm;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move v2, p1

    move-object v11, p2

    move-object/from16 v12, p3

    invoke-direct/range {v0 .. v12}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method protected static fieldInfoForOneofEnum(ILjava/lang/Object;Ljava/lang/Class;Lhby;)Lhbj;
    .locals 6

    .prologue
    .line 167
    if-nez p1, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 170
    :goto_0
    return-object v0

    .line 169
    :cond_0
    sget-object v1, Lhbm;->c:Lhbm;

    move-object v2, p1

    check-cast v2, Lhdl;

    const/4 v4, 0x0

    move v0, p0

    move-object v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lhbj;->a(ILhbm;Lhdl;Ljava/lang/Class;ZLhby;)Lhbj;

    move-result-object v0

    goto :goto_0
.end method

.method protected static fieldInfoForOneofMessage(ILhbm;Ljava/lang/Object;Ljava/lang/Class;)Lhbj;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 171
    if-nez p2, :cond_0

    .line 174
    :goto_0
    return-object v5

    :cond_0
    move-object v2, p2

    .line 173
    check-cast v2, Lhdl;

    const/4 v4, 0x0

    move v0, p0

    move-object v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lhbj;->a(ILhbm;Lhdl;Ljava/lang/Class;ZLhby;)Lhbj;

    move-result-object v5

    goto :goto_0
.end method

.method protected static fieldInfoForOneofPrimitive(ILhbm;Ljava/lang/Object;Ljava/lang/Class;)Lhbj;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 159
    if-nez p2, :cond_0

    .line 162
    :goto_0
    return-object v5

    :cond_0
    move-object v2, p2

    .line 161
    check-cast v2, Lhdl;

    const/4 v4, 0x0

    move v0, p0

    move-object v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lhbj;->a(ILhbm;Lhdl;Ljava/lang/Class;ZLhby;)Lhbj;

    move-result-object v5

    goto :goto_0
.end method

.method protected static fieldInfoForOneofString(ILjava/lang/Object;Z)Lhbj;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 163
    if-nez p1, :cond_0

    .line 166
    :goto_0
    return-object v5

    .line 165
    :cond_0
    sget-object v1, Lhbm;->a:Lhbm;

    move-object v2, p1

    check-cast v2, Lhdl;

    const-class v3, Ljava/lang/String;

    move v0, p0

    move v4, p2

    invoke-static/range {v0 .. v5}, Lhbj;->a(ILhbm;Lhdl;Ljava/lang/Class;ZLhby;)Lhbj;

    move-result-object v5

    goto :goto_0
.end method

.method public static fieldInfoForProto2Optional(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/reflect/Field;IZLhby;)Lhbj;
    .locals 14

    .prologue
    .line 136
    if-eqz p0, :cond_0

    if-nez p3, :cond_1

    .line 137
    :cond_0
    const/4 v1, 0x0

    .line 146
    :goto_0
    return-object v1

    .line 139
    :cond_1
    invoke-static {p1}, Lhbj;->a(I)V

    .line 140
    const-string v1, "field"

    invoke-static {p0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 141
    const-string v1, "fieldType"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 142
    const-string v1, "presenceField"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 143
    if-eqz p3, :cond_2

    invoke-static/range {p4 .. p4}, Lhbj;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 144
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/16 v2, 0x37

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "presenceMask must have exactly one bit set: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_2
    new-instance v1, Lhbj;

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v2, p0

    move v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v9, p5

    move-object/from16 v13, p6

    invoke-direct/range {v1 .. v13}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method protected static fieldInfoForProto2Optional(Ljava/lang/reflect/Field;JLhbm;Ljava/lang/reflect/Field;)Lhbj;
    .locals 7

    .prologue
    .line 135
    const/16 v0, 0x20

    ushr-long v0, p1, v0

    long-to-int v1, v0

    long-to-int v4, p1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lhbr;->fieldInfoForProto2Optional(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/reflect/Field;IZLhby;)Lhbj;

    move-result-object v0

    return-object v0
.end method

.method public static fieldInfoForProto2Required(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/reflect/Field;IZLhby;)Lhbj;
    .locals 14

    .prologue
    .line 148
    if-eqz p0, :cond_0

    if-nez p3, :cond_1

    .line 149
    :cond_0
    const/4 v1, 0x0

    .line 158
    :goto_0
    return-object v1

    .line 151
    :cond_1
    invoke-static {p1}, Lhbj;->a(I)V

    .line 152
    const-string v1, "field"

    invoke-static {p0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 153
    const-string v1, "fieldType"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 154
    const-string v1, "presenceField"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 155
    if-eqz p3, :cond_2

    invoke-static/range {p4 .. p4}, Lhbj;->b(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 156
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const/16 v2, 0x37

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "presenceMask must have exactly one bit set: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 157
    :cond_2
    new-instance v1, Lhbj;

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v2, p0

    move v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v9, p5

    move-object/from16 v13, p6

    invoke-direct/range {v1 .. v13}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method protected static fieldInfoForProto2Required(Ljava/lang/reflect/Field;JLhbm;Ljava/lang/reflect/Field;)Lhbj;
    .locals 7

    .prologue
    .line 147
    const/16 v0, 0x20

    ushr-long v0, p1, v0

    long-to-int v1, v0

    long-to-int v4, p1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lhbr;->fieldInfoForProto2Required(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/reflect/Field;IZLhby;)Lhbj;

    move-result-object v0

    return-object v0
.end method

.method protected static fieldInfoForRepeatedMessage(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;)Lhbj;
    .locals 14

    .prologue
    .line 119
    if-nez p0, :cond_0

    .line 120
    const/4 v1, 0x0

    .line 127
    :goto_0
    return-object v1

    .line 122
    :cond_0
    invoke-static {p1}, Lhbj;->a(I)V

    .line 123
    const-string v1, "field"

    invoke-static {p0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 124
    const-string v1, "fieldType"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 125
    const-string v1, "messageClass"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 126
    new-instance v1, Lhbj;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v2, p0

    move v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v13}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method protected static fieldInfoWithEnumMap(Ljava/lang/reflect/Field;ILhbm;Lhby;)Lhbj;
    .locals 13

    .prologue
    .line 128
    if-nez p0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    .line 131
    :cond_0
    invoke-static {p1}, Lhbj;->a(I)V

    .line 132
    const-string v0, "field"

    invoke-static {p0, v0}, Lhbu;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 133
    new-instance v0, Lhbj;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object/from16 v12, p3

    invoke-direct/range {v0 .. v12}, Lhbj;-><init>(Ljava/lang/reflect/Field;ILhbm;Ljava/lang/Class;Ljava/lang/reflect/Field;IZZLhdl;Ljava/lang/Class;Ljava/lang/Object;Lhby;)V

    goto :goto_0
.end method

.method static getDefaultInstance(Ljava/lang/Class;)Lhbr;
    .locals 4

    .prologue
    .line 70
    sget-object v0, Lhbr;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr;

    .line 71
    if-nez v0, :cond_0

    .line 72
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    sget-object v0, Lhbr;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr;

    .line 77
    :cond_0
    if-nez v0, :cond_2

    .line 78
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unable to get default instance for: "

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Class initialization cannot fail."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 78
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_2
    return-object v0
.end method

.method static varargs getMethodOrDie(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 5

    .prologue
    .line 89
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/RuntimeException;

    .line 92
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Generated message class \""

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" missing method \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static varargs invokeOrDie(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 93
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 96
    :catch_1
    move-exception v0

    .line 97
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 98
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    .line 99
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 100
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 101
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 102
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static final isInitialized(Lhbr;Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 187
    sget-object v0, Lhbr$e;->b:Lhbr$e;

    .line 188
    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 189
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 199
    :goto_0
    return v0

    .line 191
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    .line 192
    goto :goto_0

    .line 193
    :cond_1
    sget-object v0, Lhbr$e;->a:Lhbr$e;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 194
    invoke-virtual {p0, v0, v3}, Lhbr;->dynamicMethod(Lhbr$e;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 195
    :goto_1
    if-eqz p1, :cond_2

    .line 196
    sget-object v2, Lhbr$e;->c:Lhbr$e;

    .line 197
    if-eqz v1, :cond_4

    move-object v0, p0

    .line 198
    :goto_2
    invoke-virtual {p0, v2, v0}, Lhbr;->dynamicMethod(Lhbr$e;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move v0, v1

    .line 199
    goto :goto_0

    :cond_3
    move v1, v2

    .line 194
    goto :goto_1

    .line 197
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected static final makeImmutable(Lhbr;)V
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lhbr$e;->e:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    .line 201
    return-void
.end method

.method protected static mutableCopy(Lhbv;)Lhbv;
    .locals 1

    .prologue
    .line 228
    invoke-interface {p0}, Lhbv;->size()I

    move-result v0

    .line 230
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 231
    :goto_0
    invoke-interface {p0, v0}, Lhbv;->a(I)Lhbv;

    move-result-object v0

    return-object v0

    .line 230
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static mutableCopy(Lhbw;)Lhbw;
    .locals 1

    .prologue
    .line 222
    invoke-interface {p0}, Lhbw;->size()I

    move-result v0

    .line 224
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 225
    :goto_0
    invoke-interface {p0, v0}, Lhbw;->a(I)Lhbw;

    move-result-object v0

    return-object v0

    .line 224
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static mutableCopy(Lhbz;)Lhbz;
    .locals 1

    .prologue
    .line 216
    invoke-interface {p0}, Lhbz;->size()I

    move-result v0

    .line 218
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 219
    :goto_0
    invoke-interface {p0, v0}, Lhbz;->a(I)Lhbz;

    move-result-object v0

    return-object v0

    .line 218
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static mutableCopy(Lhca;)Lhca;
    .locals 1

    .prologue
    .line 204
    invoke-interface {p0}, Lhca;->size()I

    move-result v0

    .line 206
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 207
    :goto_0
    invoke-interface {p0, v0}, Lhca;->a(I)Lhca;

    move-result-object v0

    return-object v0

    .line 206
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static mutableCopy(Lhcd;)Lhcd;
    .locals 1

    .prologue
    .line 210
    invoke-interface {p0}, Lhcd;->size()I

    move-result v0

    .line 212
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 213
    :goto_0
    invoke-interface {p0, v0}, Lhcd;->c(I)Lhcd;

    move-result-object v0

    return-object v0

    .line 212
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static mutableCopy(Lhce;)Lhce;
    .locals 1

    .prologue
    .line 234
    invoke-interface {p0}, Lhce;->size()I

    move-result v0

    .line 236
    if-nez v0, :cond_0

    const/16 v0, 0xa

    .line 237
    :goto_0
    invoke-interface {p0, v0}, Lhce;->b(I)Lhce;

    move-result-object v0

    return-object v0

    .line 236
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static newFieldInfoArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 103
    new-array v0, p0, [Lhbj;

    return-object v0
.end method

.method protected static newMessageInfo(Lhdo;[I[Ljava/lang/Object;Ljava/lang/Object;)Lhdb;
    .locals 6

    .prologue
    .line 104
    new-instance v0, Lhem;

    const/4 v2, 0x0

    move-object v4, p2

    check-cast v4, [Lhbj;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhem;-><init>(Lhdo;Z[I[Lhbj;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lhdr;

    invoke-direct {v0, p0, p1, p2}, Lhdr;-><init>(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method protected static newMessageInfoForMessageSet(Lhdo;[I[Ljava/lang/Object;Ljava/lang/Object;)Lhdb;
    .locals 6

    .prologue
    .line 105
    new-instance v0, Lhem;

    const/4 v2, 0x1

    move-object v4, p2

    check-cast v4, [Lhbj;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhem;-><init>(Lhdo;Z[I[Lhbj;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected static newOneofInfo(ILjava/lang/reflect/Field;Ljava/lang/reflect/Field;)Lhdl;
    .locals 1

    .prologue
    .line 106
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    .line 108
    :cond_1
    new-instance v0, Lhdl;

    invoke-direct {v0, p0, p1, p2}, Lhdl;-><init>(ILjava/lang/reflect/Field;Ljava/lang/reflect/Field;)V

    goto :goto_0
.end method

.method public static newRepeatedGeneratedExtension(Lhdd;Lhdd;Lhby;ILhfd;ZLjava/lang/Class;)Lhbr$d;
    .locals 8

    .prologue
    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 88
    new-instance v7, Lhbr$d;

    new-instance v0, Lhbl;

    const/4 v4, 0x1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lhbl;-><init>(Lhby;ILhfd;ZZ)V

    invoke-direct {v7, p0, v6, p1, v0}, Lhbr$d;-><init>(Lhdd;Ljava/lang/Object;Lhdd;Lhbl;)V

    return-object v7
.end method

.method public static newSingularGeneratedExtension(Lhdd;Ljava/lang/Object;Lhdd;Lhby;ILhfd;Ljava/lang/Class;)Lhbr$d;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 86
    new-instance v6, Lhbr$d;

    new-instance v0, Lhbl;

    move-object v1, p3

    move v2, p4

    move-object v3, p5

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lhbl;-><init>(Lhby;ILhfd;ZZ)V

    invoke-direct {v6, p0, p1, p2, v0}, Lhbr$d;-><init>(Lhdd;Ljava/lang/Object;Lhdd;Lhbl;)V

    return-object v6
.end method

.method public static parseDelimitedFrom(Lhbr;Ljava/io/InputStream;)Lhbr;
    .locals 1

    .prologue
    .line 297
    .line 298
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    .line 299
    invoke-static {p0, p1, v0}, Lhbr;->parsePartialDelimitedFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;

    move-result-object v0

    .line 300
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 301
    .line 302
    invoke-static {p0, p1, p2}, Lhbr;->parsePartialDelimitedFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;

    move-result-object v0

    .line 303
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Lhah;)Lhbr;
    .locals 1

    .prologue
    .line 259
    .line 260
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhbr;->parseFrom(Lhbr;Lhah;Lhbg;)Lhbr;

    move-result-object v0

    .line 261
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Lhah;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 262
    invoke-static {p0, p1, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhah;Lhbg;)Lhbr;

    move-result-object v0

    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Lhaq;)Lhbr;
    .locals 1

    .prologue
    .line 293
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhbr;->parseFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Lhaq;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 294
    .line 295
    invoke-static {p0, p1, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    .line 296
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Ljava/io/InputStream;)Lhbr;
    .locals 2

    .prologue
    .line 285
    .line 286
    invoke-static {p1}, Lhaq;->a(Ljava/io/InputStream;)Lhaq;

    move-result-object v0

    .line 287
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v1

    .line 288
    invoke-static {p0, v0, v1}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    .line 289
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 290
    .line 291
    invoke-static {p1}, Lhaq;->a(Ljava/io/InputStream;)Lhaq;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    .line 292
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Ljava/nio/ByteBuffer;)Lhbr;
    .locals 1

    .prologue
    .line 258
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhbr;->parseFrom(Lhbr;Ljava/nio/ByteBuffer;Lhbg;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;Ljava/nio/ByteBuffer;Lhbg;)Lhbr;
    .locals 1

    .prologue
    .line 253
    .line 255
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lhaq;->a(Ljava/nio/ByteBuffer;Z)Lhaq;

    move-result-object v0

    .line 256
    invoke-static {p0, v0, p2}, Lhbr;->parseFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    .line 257
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;[B)Lhbr;
    .locals 1

    .prologue
    .line 281
    .line 282
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhbr;->parsePartialFrom(Lhbr;[BLhbg;)Lhbr;

    move-result-object v0

    .line 283
    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lhbr;[BLhbg;)Lhbr;
    .locals 1

    .prologue
    .line 284
    invoke-static {p0, p1, p2}, Lhbr;->parsePartialFrom(Lhbr;[BLhbg;)Lhbr;

    move-result-object v0

    invoke-static {v0}, Lhbr;->checkMessageInitialized(Lhbr;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method private static parsePartialDelimitedFrom(Lhbr;Ljava/io/InputStream;Lhbg;)Lhbr;
    .locals 3

    .prologue
    .line 304
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 305
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 306
    const/4 v0, 0x0

    .line 318
    :goto_0
    return-object v0

    .line 307
    :cond_0
    invoke-static {v0, p1}, Lhaq;->a(ILjava/io/InputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 311
    new-instance v1, Lgzy;

    invoke-direct {v1, p1, v0}, Lgzy;-><init>(Ljava/io/InputStream;I)V

    .line 312
    invoke-static {v1}, Lhaq;->a(Ljava/io/InputStream;)Lhaq;

    move-result-object v1

    .line 313
    invoke-static {p0, v1, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    .line 314
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Lhaq;->a(I)V
    :try_end_1
    .catch Lhcf; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v0

    .line 317
    throw v0

    .line 309
    :catch_1
    move-exception v0

    .line 310
    new-instance v1, Lhcf;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static parsePartialFrom(Lhbr;Lhah;Lhbg;)Lhbr;
    .locals 3

    .prologue
    .line 263
    :try_start_0
    invoke-virtual {p1}, Lhah;->e()Lhaq;

    move-result-object v0

    .line 264
    invoke-static {p0, v0, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 265
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Lhaq;->a(I)V
    :try_end_1
    .catch Lhcf; {:try_start_1 .. :try_end_1} :catch_0

    .line 269
    return-object v1

    .line 267
    :catch_0
    move-exception v0

    .line 268
    :try_start_2
    throw v0
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_1

    .line 270
    :catch_1
    move-exception v0

    .line 271
    throw v0
.end method

.method protected static parsePartialFrom(Lhbr;Lhaq;)Lhbr;
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lhbg;->a()Lhbg;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;

    move-result-object v0

    return-object v0
.end method

.method static parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;
    .locals 2

    .prologue
    .line 238
    sget-object v0, Lhbr$e;->f:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr;

    .line 239
    :try_start_0
    sget-object v1, Lhbr$e;->d:Lhbr$e;

    invoke-virtual {v0, v1, p1, p2}, Lhbr;->dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    invoke-virtual {v0}, Lhbr;->makeImmutable()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    return-object v0

    .line 242
    :catch_0
    move-exception v0

    .line 243
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lhcf;

    if-eqz v1, :cond_0

    .line 244
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lhcf;

    throw v0

    .line 245
    :cond_0
    throw v0
.end method

.method private static parsePartialFrom(Lhbr;[BLhbg;)Lhbr;
    .locals 3

    .prologue
    .line 272
    :try_start_0
    invoke-static {p1}, Lhaq;->a([B)Lhaq;

    move-result-object v0

    .line 273
    invoke-static {p0, v0, p2}, Lhbr;->parsePartialFrom(Lhbr;Lhaq;Lhbg;)Lhbr;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 274
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Lhaq;->a(I)V
    :try_end_1
    .catch Lhcf; {:try_start_1 .. :try_end_1} :catch_0

    .line 278
    return-object v1

    .line 276
    :catch_0
    move-exception v0

    .line 277
    :try_start_2
    throw v0
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_1

    .line 279
    :catch_1
    move-exception v0

    .line 280
    throw v0
.end method

.method protected static reflectField(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;
    .locals 1

    .prologue
    .line 183
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static registerDefaultInstance(Ljava/lang/Class;Lhbr;)V
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lhbr;->defaultInstanceMap:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    return-void
.end method


# virtual methods
.method public abstract buildMessageInfo()Ljava/lang/Object;
.end method

.method public final createBuilder()Lhbr$a;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lhbr$e;->g:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr$a;

    return-object v0
.end method

.method public final createBuilder(Lhbr;)Lhbr$a;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lhbr;->createBuilder()Lhbr$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    move-result-object v0

    return-object v0
.end method

.method protected dynamicMethod(Lhbr$e;)Ljava/lang/Object;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, p1, v0, v0}, Lhbr;->dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected dynamicMethod(Lhbr$e;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lhbr;->dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 16
    if-ne p0, p1, :cond_0

    .line 17
    const/4 v0, 0x1

    .line 21
    :goto_0
    return v0

    .line 18
    :cond_0
    invoke-virtual {p0}, Lhbr;->getDefaultInstanceForType()Lhbr;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 19
    const/4 v0, 0x0

    goto :goto_0

    .line 20
    :cond_1
    sget-object v0, Lhdp;->a:Lhdp;

    .line 21
    invoke-virtual {v0, p0}, Lhdp;->b(Ljava/lang/Object;)Lhdz;

    move-result-object v0

    check-cast p1, Lhbr;

    invoke-interface {v0, p0, p1}, Lhdz;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final getDefaultInstanceForType()Lhbr;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lhbr$e;->h:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lhdd;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lhbr;->getDefaultInstanceForType()Lhbr;

    move-result-object v0

    return-object v0
.end method

.method public final getParserForType()Lhdm;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lhbr$e;->i:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdm;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 2

    .prologue
    .line 67
    iget v0, p0, Lhbr;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 68
    invoke-virtual {p0}, Lhbr;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhbr;->memoizedSerializedSize:I

    .line 69
    :cond_0
    iget v0, p0, Lhbr;->memoizedSerializedSize:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lhbr;->memoizedHashCode:I

    if-eqz v0, :cond_0

    .line 11
    iget v0, p0, Lhbr;->memoizedHashCode:I

    .line 15
    :goto_0
    return v0

    .line 13
    :cond_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 14
    invoke-virtual {v0, p0}, Lhdp;->b(Ljava/lang/Object;)Lhdz;

    move-result-object v0

    invoke-interface {v0, p0}, Lhdz;->a(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lhbr;->memoizedHashCode:I

    .line 15
    iget v0, p0, Lhbr;->memoizedHashCode:I

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 59
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p0, v0}, Lhbr;->isInitialized(Lhbr;Z)Z

    move-result v0

    return v0
.end method

.method public makeImmutable()V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lhbr$e;->e:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 55
    const/4 v1, 0x0

    iput-boolean v1, v0, Lheq;->f:Z

    .line 56
    return-void
.end method

.method protected mergeLengthDelimitedField(ILhah;)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Lhbr;->ensureUnknownFieldsInitialized()V

    .line 45
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 46
    invoke-virtual {v0}, Lheq;->a()V

    .line 47
    if-nez p1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    const/4 v1, 0x2

    .line 50
    shl-int/lit8 v2, p1, 0x3

    or-int/2addr v1, v2

    .line 51
    invoke-virtual {v0, v1, p2}, Lheq;->a(ILjava/lang/Object;)V

    .line 52
    return-void
.end method

.method protected final mergeUnknownFields(Lheq;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    invoke-static {v0, p1}, Lheq;->a(Lheq;Lheq;)Lheq;

    move-result-object v0

    iput-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 84
    return-void
.end method

.method public mergeVarintField(II)V
    .locals 4

    .prologue
    .line 35
    invoke-direct {p0}, Lhbr;->ensureUnknownFieldsInitialized()V

    .line 36
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    .line 37
    invoke-virtual {v0}, Lheq;->a()V

    .line 38
    if-nez p1, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zero is not a valid field number."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    const/4 v1, 0x0

    .line 41
    shl-int/lit8 v2, p1, 0x3

    or-int/2addr v1, v2

    .line 42
    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lheq;->a(ILjava/lang/Object;)V

    .line 43
    return-void
.end method

.method public mutableCopy()Lhdi;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Lite does not support the mutable API."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final newBuilderForType()Lhbr$a;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lhbr$e;->g:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr$a;

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lhde;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0}, Lhbr;->newBuilderForType()Lhbr$a;

    move-result-object v0

    return-object v0
.end method

.method public parseUnknownField(ILhaq;)Z
    .locals 2

    .prologue
    .line 29
    .line 30
    and-int/lit8 v0, p1, 0x7

    .line 31
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    .line 33
    :cond_0
    invoke-direct {p0}, Lhbr;->ensureUnknownFieldsInitialized()V

    .line 34
    iget-object v0, p0, Lhbr;->unknownFields:Lheq;

    invoke-virtual {v0, p1, p2}, Lheq;->a(ILhaq;)Z

    move-result v0

    goto :goto_0
.end method

.method public final toBuilder()Lhbr$a;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lhbr$e;->g:Lhbr$e;

    invoke-virtual {p0, v0}, Lhbr;->dynamicMethod(Lhbr$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbr$a;

    .line 61
    invoke-virtual {v0, p0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 62
    return-object v0
.end method

.method public bridge synthetic toBuilder()Lhde;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lhbr;->toBuilder()Lhbr$a;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lhcw;->a(Lhdd;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lhaw;)V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lhbr;->writeToInternal(Lhaw;)V

    .line 66
    return-void
.end method
