.class public final Lgpf;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpf;


# instance fields
.field public rtpStreamId:[Ljava/lang/String;

.field public ssrc:[I

.field public ssrcGroup:[Lgpg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpf;->clear()Lgpf;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpf;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpf;->_emptyArray:[Lgpf;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpf;->_emptyArray:[Lgpf;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpf;

    sput-object v0, Lgpf;->_emptyArray:[Lgpf;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpf;->_emptyArray:[Lgpf;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpf;
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lgpf;

    invoke-direct {v0}, Lgpf;-><init>()V

    invoke-virtual {v0, p0}, Lgpf;->mergeFrom(Lhfp;)Lgpf;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpf;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lgpf;

    invoke-direct {v0}, Lgpf;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpf;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpf;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lhgc;->e:[I

    iput-object v0, p0, Lgpf;->ssrc:[I

    .line 11
    invoke-static {}, Lgpg;->emptyArray()[Lgpg;

    move-result-object v0

    iput-object v0, p0, Lgpf;->ssrcGroup:[Lgpg;

    .line 12
    sget-object v0, Lhgc;->g:[Ljava/lang/String;

    iput-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lgpf;->unknownFieldData:Lhfv;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lgpf;->cachedSize:I

    .line 15
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v3

    .line 35
    iget-object v0, p0, Lgpf;->ssrc:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgpf;->ssrc:[I

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    move v2, v1

    .line 37
    :goto_0
    iget-object v4, p0, Lgpf;->ssrc:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 38
    iget-object v4, p0, Lgpf;->ssrc:[I

    aget v4, v4, v0

    .line 41
    invoke-static {v4}, Lhfq;->d(I)I

    move-result v4

    .line 42
    add-int/2addr v2, v4

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    add-int v0, v3, v2

    .line 45
    iget-object v2, p0, Lgpf;->ssrc:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 46
    :goto_1
    iget-object v2, p0, Lgpf;->ssrcGroup:[Lgpg;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgpf;->ssrcGroup:[Lgpg;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 47
    :goto_2
    iget-object v3, p0, Lgpf;->ssrcGroup:[Lgpg;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 48
    iget-object v3, p0, Lgpf;->ssrcGroup:[Lgpg;

    aget-object v3, v3, v0

    .line 49
    if-eqz v3, :cond_1

    .line 50
    const/4 v4, 0x2

    .line 51
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 52
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 53
    :cond_3
    iget-object v2, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 56
    :goto_3
    iget-object v4, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 57
    iget-object v4, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 58
    if-eqz v4, :cond_4

    .line 59
    add-int/lit8 v3, v3, 0x1

    .line 61
    invoke-static {v4}, Lhfq;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 62
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 63
    :cond_5
    add-int/2addr v0, v2

    .line 64
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 65
    :cond_6
    return v0

    :cond_7
    move v0, v3

    goto :goto_1
.end method

.method public final mergeFrom(Lhfp;)Lgpf;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 69
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :sswitch_0
    return-object p0

    .line 71
    :sswitch_1
    const/16 v0, 0x8

    .line 72
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 73
    iget-object v0, p0, Lgpf;->ssrc:[I

    if-nez v0, :cond_2

    move v0, v1

    .line 74
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 75
    if-eqz v0, :cond_1

    .line 76
    iget-object v3, p0, Lgpf;->ssrc:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 79
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 80
    aput v3, v2, v0

    .line 81
    invoke-virtual {p1}, Lhfp;->a()I

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 73
    :cond_2
    iget-object v0, p0, Lgpf;->ssrc:[I

    array-length v0, v0

    goto :goto_1

    .line 84
    :cond_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 85
    aput v3, v2, v0

    .line 86
    iput-object v2, p0, Lgpf;->ssrc:[I

    goto :goto_0

    .line 88
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 89
    invoke-virtual {p1, v0}, Lhfp;->c(I)I

    move-result v3

    .line 91
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    move v0, v1

    .line 92
    :goto_3
    invoke-virtual {p1}, Lhfp;->k()I

    move-result v4

    if-lez v4, :cond_4

    .line 94
    invoke-virtual {p1}, Lhfp;->g()I

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 97
    :cond_4
    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 98
    iget-object v2, p0, Lgpf;->ssrc:[I

    if-nez v2, :cond_6

    move v2, v1

    .line 99
    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    .line 100
    if-eqz v2, :cond_5

    .line 101
    iget-object v4, p0, Lgpf;->ssrc:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 102
    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    .line 104
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v4

    .line 105
    aput v4, v0, v2

    .line 106
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 98
    :cond_6
    iget-object v2, p0, Lgpf;->ssrc:[I

    array-length v2, v2

    goto :goto_4

    .line 107
    :cond_7
    iput-object v0, p0, Lgpf;->ssrc:[I

    .line 108
    invoke-virtual {p1, v3}, Lhfp;->d(I)V

    goto/16 :goto_0

    .line 110
    :sswitch_3
    const/16 v0, 0x12

    .line 111
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 112
    iget-object v0, p0, Lgpf;->ssrcGroup:[Lgpg;

    if-nez v0, :cond_9

    move v0, v1

    .line 113
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lgpg;

    .line 114
    if-eqz v0, :cond_8

    .line 115
    iget-object v3, p0, Lgpf;->ssrcGroup:[Lgpg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    .line 117
    new-instance v3, Lgpg;

    invoke-direct {v3}, Lgpg;-><init>()V

    aput-object v3, v2, v0

    .line 118
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 119
    invoke-virtual {p1}, Lhfp;->a()I

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 112
    :cond_9
    iget-object v0, p0, Lgpf;->ssrcGroup:[Lgpg;

    array-length v0, v0

    goto :goto_6

    .line 121
    :cond_a
    new-instance v3, Lgpg;

    invoke-direct {v3}, Lgpg;-><init>()V

    aput-object v3, v2, v0

    .line 122
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 123
    iput-object v2, p0, Lgpf;->ssrcGroup:[Lgpg;

    goto/16 :goto_0

    .line 125
    :sswitch_4
    const/16 v0, 0x1a

    .line 126
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 127
    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    .line 128
    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 129
    if-eqz v0, :cond_b

    .line 130
    iget-object v3, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 131
    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    .line 132
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 133
    invoke-virtual {p1}, Lhfp;->a()I

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 127
    :cond_c
    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_8

    .line 135
    :cond_d
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 136
    iput-object v2, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lgpf;->mergeFrom(Lhfp;)Lgpf;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 16
    iget-object v0, p0, Lgpf;->ssrc:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpf;->ssrc:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 17
    :goto_0
    iget-object v2, p0, Lgpf;->ssrc:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 18
    const/4 v2, 0x1

    iget-object v3, p0, Lgpf;->ssrc:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lhfq;->c(II)V

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_0
    iget-object v0, p0, Lgpf;->ssrcGroup:[Lgpg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpf;->ssrcGroup:[Lgpg;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 21
    :goto_1
    iget-object v2, p0, Lgpf;->ssrcGroup:[Lgpg;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 22
    iget-object v2, p0, Lgpf;->ssrcGroup:[Lgpg;

    aget-object v2, v2, v0

    .line 23
    if-eqz v2, :cond_1

    .line 24
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 26
    :cond_2
    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 27
    :goto_2
    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 28
    iget-object v0, p0, Lgpf;->rtpStreamId:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 29
    if-eqz v0, :cond_3

    .line 30
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lhfq;->a(ILjava/lang/String;)V

    .line 31
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 32
    :cond_4
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 33
    return-void
.end method
