.class final Lfnu;
.super Lfof;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfnp;


# direct methods
.method private constructor <init>(Lfnp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfnu;->this$0:Lfnp;

    invoke-direct {p0}, Lfof;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfnp;Lfnt;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lfnu;-><init>(Lfnp;)V

    return-void
.end method


# virtual methods
.method public final onCallEnded(Lfoe;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 28
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    invoke-virtual {v0, p1}, Lfnp;->getEndCauseInfo(Lfoe;)Lfvx;

    move-result-object v0

    .line 30
    iget v1, v0, Lfvx;->a:I

    .line 32
    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v2}, Lfnp;->access$500(Lfnp;)Lfvy;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v2}, Lfnp;->access$700(Lfnp;)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    .line 33
    invoke-static {v1}, Lfmk;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 34
    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v2}, Lfnp;->access$600(Lfnp;)Lfuv;

    move-result-object v2

    const/16 v3, 0xb5b

    invoke-virtual {v2, v3}, Lfuv;->report(I)V

    .line 36
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 37
    new-instance v2, Lfwj;

    .line 38
    invoke-virtual {p1}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v3

    .line 39
    invoke-virtual {p1}, Lfoe;->getCallInfo()Lfvs;

    move-result-object v4

    .line 40
    iget-object v4, v4, Lfvs;->e:Ljava/lang/String;

    .line 41
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-direct {v2, v3, v4, v6, v7}, Lfwj;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 42
    iget-object v3, p0, Lfnu;->this$0:Lfnp;

    .line 43
    invoke-static {v3}, Lfnp;->access$900(Lfnp;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "CallgrokPref"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 44
    const-string v4, "previousCallKey"

    invoke-virtual {v2}, Lfwj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 45
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 46
    :cond_1
    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v2, v8, v9}, Lfnp;->access$702(Lfnp;J)J

    .line 47
    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Call.onCallEnded: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfvh;->logi(Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1, p1}, Lfnp;->access$1300(Lfnp;Lfoe;)V

    .line 49
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$1400(Lfnp;)V

    .line 50
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lfnp;->access$402(Lfnp;I)I

    .line 51
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$1000(Lfnp;)Lfwk;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfwk;->onCallEnd(Lfvx;)V

    .line 52
    return-void

    .line 35
    :cond_2
    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v2}, Lfnp;->access$600(Lfnp;)Lfuv;

    move-result-object v2

    const/16 v3, 0xa83

    invoke-virtual {v2, v3}, Lfuv;->report(I)V

    goto :goto_0
.end method

.method public final onEndpointEvent(Lfue;Lfuf;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2
    instance-of v0, p2, Lfug;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lfue;->isSelfEndpoint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lfnp;->access$402(Lfnp;I)I

    .line 4
    const-string v0, "Call joined; participant id = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lfvh;->logi(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v0}, Lfnp;->access$300(Lfnp;)Lfnv;

    move-result-object v0

    invoke-virtual {v0}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v0

    invoke-virtual {v0}, Lfoe;->getResolvedHangoutId()Ljava/lang/String;

    move-result-object v0

    .line 6
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    new-instance v2, Lfvy;

    invoke-direct {v2, v0}, Lfvy;-><init>(Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lfue;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    .line 8
    iput-object v3, v2, Lfvy;->b:Ljava/lang/String;

    .line 10
    invoke-static {v1, v2}, Lfnp;->access$502(Lfnp;Lfvy;)Lfvy;

    .line 11
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$600(Lfnp;)Lfuv;

    move-result-object v1

    const/16 v2, 0xa82

    invoke-virtual {v1, v2}, Lfuv;->report(I)V

    .line 12
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$700(Lfnp;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 13
    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lfnp;->access$702(Lfnp;J)J

    .line 14
    :cond_0
    new-instance v1, Lfwj;

    iget-object v2, p0, Lfnu;->this$0:Lfnp;

    .line 15
    invoke-static {v2}, Lfnp;->access$800(Lfnp;)Lfvs;

    move-result-object v2

    .line 16
    iget-object v2, v2, Lfvs;->e:Ljava/lang/String;

    .line 17
    iget-object v3, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v3}, Lfnp;->access$700(Lfnp;)J

    move-result-wide v4

    invoke-direct {v1, v0, v2, v4, v5}, Lfwj;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 18
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    .line 19
    invoke-static {v0}, Lfnp;->access$900(Lfnp;)Landroid/content/Context;

    move-result-object v0

    const-string v2, "CallgrokPref"

    invoke-virtual {v0, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 20
    const-string v2, "currentCallKey"

    invoke-virtual {v1}, Lfwj;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 21
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 22
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v0}, Lfnp;->access$1000(Lfnp;)Lfwk;

    move-result-object v0

    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$500(Lfnp;)Lfvy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfwk;->onCallJoin(Lfvy;)V

    .line 23
    :cond_1
    return-void
.end method

.method public final onLogDataReadyForUpload()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v0}, Lfnp;->access$300(Lfnp;)Lfnv;

    move-result-object v0

    invoke-virtual {v0}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v0

    if-nez v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$300(Lfnp;)Lfnv;

    move-result-object v1

    invoke-virtual {v1}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v1

    invoke-static {v0, v1}, Lfnp;->access$1300(Lfnp;Lfoe;)V

    goto :goto_0
.end method

.method public final onMediaStarted(Lfoe;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lfnp;->access$1102(Lfnp;Z)Z

    .line 25
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v0}, Lfnp;->access$1200(Lfnp;)V

    .line 26
    iget-object v0, p0, Lfnu;->this$0:Lfnp;

    iget-object v1, p0, Lfnu;->this$0:Lfnp;

    invoke-static {v1}, Lfnp;->access$300(Lfnp;)Lfnv;

    move-result-object v1

    invoke-virtual {v1}, Lfnv;->getCurrentCall()Lfoe;

    move-result-object v1

    invoke-static {v0, v1}, Lfnp;->access$1300(Lfnp;Lfoe;)V

    .line 27
    return-void
.end method
