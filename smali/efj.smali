.class public final Lefj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/Status;

.field public static final b:Lcom/google/android/gms/common/api/Status;

.field public static final c:Ljava/lang/Object;

.field public static d:Lefj;


# instance fields
.field public final e:Landroid/content/Context;

.field public final f:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final h:Ljava/util/Map;

.field public i:Leef;

.field public final j:Ljava/util/Set;

.field public final k:Landroid/os/Handler;

.field private l:J

.field private m:J

.field private n:J

.field private o:Lecn;

.field private p:I

.field private q:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v1, "Sign-out occurred while this API call was in progress."

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    sput-object v0, Lefj;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const-string v1, "The user must be signed in to make this API call."

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    sput-object v0, Lefj;->b:Lcom/google/android/gms/common/api/Status;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lefj;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lecn;)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lefj;->l:J

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lefj;->m:J

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lefj;->n:J

    const/4 v0, -0x1

    iput v0, p0, Lefj;->p:I

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lefj;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x5

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    iput-object v0, p0, Lefj;->h:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lefj;->i:Leef;

    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Lefj;->j:Ljava/util/Set;

    new-instance v0, Lpf;

    invoke-direct {v0}, Lpf;-><init>()V

    iput-object v0, p0, Lefj;->q:Ljava/util/Set;

    iput-object p1, p0, Lefj;->e:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iput-object p3, p0, Lefj;->o:Lecn;

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v1, p0, Lefj;->k:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method static synthetic a(Lefj;I)I
    .locals 0

    iput p1, p0, Lefj;->p:I

    return p1
.end method

.method static synthetic a(Lefj;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lefj;
    .locals 5

    .prologue
    .line 1
    sget-object v1, Lefj;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lefj;->d:Lefj;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "GoogleApiHandler"

    const/16 v3, 0x9

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Lefj;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 2
    sget-object v4, Lecn;->a:Lecn;

    .line 3
    invoke-direct {v2, v3, v0, v4}, Lefj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lecn;)V

    sput-object v2, Lefj;->d:Lefj;

    :cond_0
    sget-object v0, Lefj;->d:Lefj;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lefj;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lefj;->e:Landroid/content/Context;

    return-object v0
.end method

.method private final b()V
    .locals 3

    iget-object v0, p0, Lefj;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legz;

    iget-object v2, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    invoke-virtual {v0}, Lefk;->c()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lefj;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method private final b(Ledh;)V
    .locals 3

    .prologue
    .line 4
    .line 5
    iget-object v1, p1, Ledh;->c:Legz;

    .line 6
    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    if-nez v0, :cond_0

    new-instance v0, Lefk;

    invoke-direct {v0, p0, p1}, Lefk;-><init>(Lefj;Ledh;)V

    iget-object v2, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lefk;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lefj;->q:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lefk;->h()V

    return-void
.end method

.method static synthetic c(Lefj;)J
    .locals 2

    iget-wide v0, p0, Lefj;->l:J

    return-wide v0
.end method

.method static synthetic d(Lefj;)J
    .locals 2

    iget-wide v0, p0, Lefj;->m:J

    return-wide v0
.end method

.method static synthetic e(Lefj;)Leef;
    .locals 1

    iget-object v0, p0, Lefj;->i:Leef;

    return-object v0
.end method

.method static synthetic f(Lefj;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lefj;->j:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic g(Lefj;)Lecn;
    .locals 1

    iget-object v0, p0, Lefj;->o:Lecn;

    return-object v0
.end method

.method static synthetic h(Lefj;)J
    .locals 2

    iget-wide v0, p0, Lefj;->n:J

    return-wide v0
.end method

.method static synthetic i(Lefj;)I
    .locals 1

    iget v0, p0, Lefj;->p:I

    return v0
.end method

.method static synthetic j(Lefj;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lfat;
    .locals 4

    .prologue
    .line 10
    new-instance v1, Lehb;

    invoke-direct {v1, p1}, Lehb;-><init>(Ljava/lang/Iterable;)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledh;

    iget-object v3, p0, Lefj;->h:Ljava/util/Map;

    .line 11
    iget-object v0, v0, Ledh;->c:Legz;

    .line 12
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lefk;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v2, p0, Lefj;->k:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 13
    iget-object v0, v1, Lehb;->b:Lfau;

    .line 14
    iget-object v0, v0, Lfau;->a:Lfbj;

    .line 20
    :goto_0
    return-object v0

    .line 16
    :cond_2
    iget-object v0, v1, Lehb;->b:Lfau;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lfau;->a(Ljava/lang/Object;)V

    .line 18
    iget-object v0, v1, Lehb;->b:Lfau;

    .line 19
    iget-object v0, v0, Lfau;->a:Lfbj;

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v1, p0, Lefj;->k:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Ledh;)V
    .locals 3

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v1, p0, Lefj;->k:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Leef;)V
    .locals 3

    .prologue
    .line 7
    sget-object v1, Lefj;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lefj;->i:Leef;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lefj;->i:Leef;

    iget-object v0, p0, Lefj;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lefj;->j:Ljava/util/Set;

    .line 8
    iget-object v2, p1, Leef;->b:Lpf;

    .line 9
    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lecl;I)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    iget-object v3, p0, Lefj;->o:Lecn;

    iget-object v4, p0, Lefj;->e:Landroid/content/Context;

    .line 66
    invoke-virtual {p1}, Lecl;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p1, Lecl;->c:Landroid/app/PendingIntent;

    .line 71
    :goto_0
    if-eqz v2, :cond_1

    .line 72
    iget v5, p1, Lecl;->b:I

    .line 74
    invoke-static {v4, v2, p2, v0}, Lcom/google/android/gms/common/api/GoogleApiActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;IZ)Landroid/content/Intent;

    move-result-object v2

    const/high16 v6, 0x8000000

    invoke-static {v4, v1, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 75
    invoke-virtual {v3, v4, v5, v1}, Lecn;->a(Landroid/content/Context;ILandroid/app/PendingIntent;)V

    .line 76
    :goto_1
    return v0

    .line 69
    :cond_0
    iget v2, p1, Lecl;->b:I

    .line 70
    invoke-virtual {v3, v4, v2, v1}, Lecp;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 76
    goto :goto_1
.end method

.method public final b(Lecl;I)V
    .locals 4

    invoke-virtual {p0, p1, p2}, Lefj;->a(Lecl;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v1, p0, Lefj;->k:Landroid/os/Handler;

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const-wide/32 v2, 0x493e0

    const/16 v8, 0xc

    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 21
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 63
    const-string v0, "GoogleApiManager"

    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown message id: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    return v0

    .line 21
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2710

    :goto_1
    iput-wide v0, p0, Lefj;->n:J

    iget-object v0, p0, Lefj;->k:Landroid/os/Handler;

    invoke-virtual {v0, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legz;

    iget-object v2, p0, Lefj;->k:Landroid/os/Handler;

    iget-object v3, p0, Lefj;->k:Landroid/os/Handler;

    invoke-virtual {v3, v8, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-wide v6, p0, Lefj;->n:J

    invoke-virtual {v2, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    :cond_0
    move-wide v0, v2

    goto :goto_1

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lehb;

    .line 22
    iget-object v1, v0, Lehb;->a:Lpd;

    invoke-virtual {v1}, Lpd;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 23
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legz;

    iget-object v2, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lefk;

    if-nez v2, :cond_2

    new-instance v2, Lecl;

    const/16 v3, 0xd

    invoke-direct {v2, v3}, Lecl;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lehb;->a(Legz;Lecl;)V

    :cond_1
    :goto_4
    move v0, v4

    .line 63
    goto :goto_0

    .line 23
    :cond_2
    invoke-virtual {v2}, Lefk;->i()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v2, Lecl;->a:Lecl;

    invoke-virtual {v0, v1, v2}, Lehb;->a(Legz;Lecl;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Lefk;->e()Lecl;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lefk;->e()Lecl;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lehb;->a(Legz;Lecl;)V

    goto :goto_3

    .line 24
    :cond_4
    iget-object v1, v2, Lefk;->h:Lefj;

    .line 25
    iget-object v1, v1, Lefj;->k:Landroid/os/Handler;

    .line 26
    invoke-static {v1}, Letf;->a(Landroid/os/Handler;)V

    iget-object v1, v2, Lefk;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 27
    :pswitch_2
    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    invoke-virtual {v0}, Lefk;->d()V

    invoke-virtual {v0}, Lefk;->h()V

    goto :goto_5

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Legf;

    iget-object v1, p0, Lefj;->h:Ljava/util/Map;

    iget-object v2, v0, Legf;->c:Ledh;

    .line 28
    iget-object v2, v2, Ledh;->c:Legz;

    .line 29
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lefk;

    if-nez v1, :cond_5

    iget-object v1, v0, Legf;->c:Ledh;

    invoke-direct {p0, v1}, Lefj;->b(Ledh;)V

    iget-object v1, p0, Lefj;->h:Ljava/util/Map;

    iget-object v2, v0, Legf;->c:Ledh;

    .line 30
    iget-object v2, v2, Ledh;->c:Legz;

    .line 31
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lefk;

    :cond_5
    invoke-virtual {v1}, Lefk;->j()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lefj;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    iget v3, v0, Legf;->b:I

    if-eq v2, v3, :cond_6

    iget-object v0, v0, Legf;->a:Ledv;

    sget-object v2, Lefj;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v2}, Ledv;->a(Lcom/google/android/gms/common/api/Status;)V

    invoke-virtual {v1}, Lefk;->c()V

    goto/16 :goto_4

    :cond_6
    iget-object v0, v0, Legf;->a:Ledv;

    invoke-virtual {v1, v0}, Lefk;->a(Ledv;)V

    goto/16 :goto_4

    :pswitch_4
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lecl;

    const/4 v2, 0x0

    iget-object v1, p0, Lefj;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lefk;

    .line 32
    iget v6, v1, Lefk;->e:I

    .line 33
    if-ne v6, v3, :cond_7

    :goto_6
    if-eqz v1, :cond_8

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x11

    iget-object v5, p0, Lefj;->o:Lecn;

    .line 34
    iget v6, v0, Lecl;->b:I

    .line 35
    invoke-virtual {v5, v6}, Lecp;->c(I)Ljava/lang/String;

    move-result-object v5

    .line 36
    iget-object v0, v0, Lecl;->d:Ljava/lang/String;

    .line 37
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x45

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Error resolution was canceled by the user, original error message: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lefk;->a(Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_4

    :cond_8
    const-string v0, "GoogleApiManager"

    const/16 v1, 0x4c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Could not find API instance "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while trying to fail enqueued calls."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    :pswitch_5
    iget-object v0, p0, Lefj;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Application;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefj;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 38
    sget-object v1, Lehc;->a:Lehc;

    monitor-enter v1

    :try_start_0
    sget-object v5, Lehc;->a:Lehc;

    iget-boolean v5, v5, Lehc;->e:Z

    if-nez v5, :cond_9

    sget-object v5, Lehc;->a:Lehc;

    invoke-virtual {v0, v5}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    sget-object v5, Lehc;->a:Lehc;

    invoke-virtual {v0, v5}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    sget-object v0, Lehc;->a:Lehc;

    const/4 v5, 0x1

    iput-boolean v5, v0, Lehc;->e:Z

    :cond_9
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    sget-object v0, Lehc;->a:Lehc;

    .line 40
    new-instance v1, Lehd;

    invoke-direct {v1, p0}, Lehd;-><init>(Lefj;)V

    .line 41
    sget-object v5, Lehc;->a:Lehc;

    monitor-enter v5

    :try_start_1
    iget-object v0, v0, Lehc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 42
    sget-object v0, Lehc;->a:Lehc;

    .line 44
    iget-object v1, v0, Lehc;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {}, Letf;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$RunningAppProcessInfo;-><init>()V

    invoke-static {v1}, Landroid/app/ActivityManager;->getMyMemoryState(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    iget-object v5, v0, Lehc;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v5

    if-nez v5, :cond_a

    iget v1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v5, 0x64

    if-le v1, v5, :cond_a

    iget-object v1, v0, Lehc;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_a
    iget-object v0, v0, Lehc;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    .line 45
    :goto_7
    if-nez v0, :cond_1

    iput-wide v2, p0, Lefj;->n:J

    goto/16 :goto_4

    .line 38
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 41
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_b
    move v0, v4

    .line 44
    goto :goto_7

    .line 45
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ledh;

    invoke-direct {p0, v0}, Lefj;->b(Ledh;)V

    goto/16 :goto_4

    :pswitch_7
    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    .line 46
    iget-object v1, v0, Lefk;->h:Lefj;

    .line 47
    iget-object v1, v1, Lefj;->k:Landroid/os/Handler;

    .line 48
    invoke-static {v1}, Letf;->a(Landroid/os/Handler;)V

    iget-boolean v1, v0, Lefk;->g:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lefk;->h()V

    goto/16 :goto_4

    .line 49
    :pswitch_8
    invoke-direct {p0}, Lefj;->b()V

    goto/16 :goto_4

    :pswitch_9
    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    .line 50
    iget-object v1, v0, Lefk;->h:Lefj;

    .line 51
    iget-object v1, v1, Lefj;->k:Landroid/os/Handler;

    .line 52
    invoke-static {v1}, Letf;->a(Landroid/os/Handler;)V

    iget-boolean v1, v0, Lefk;->g:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lefk;->f()V

    iget-object v1, v0, Lefk;->h:Lefj;

    .line 53
    iget-object v1, v1, Lefj;->o:Lecn;

    .line 54
    iget-object v2, v0, Lefk;->h:Lefj;

    .line 55
    iget-object v2, v2, Lefj;->e:Landroid/content/Context;

    .line 56
    invoke-virtual {v1, v2}, Lecp;->b(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0x12

    if-ne v1, v2, :cond_c

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const-string v2, "Connection timed out while waiting for Google Play services update to complete."

    invoke-direct {v1, v5, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    :goto_8
    invoke-virtual {v0, v1}, Lefk;->a(Lcom/google/android/gms/common/api/Status;)V

    iget-object v0, v0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->e()V

    goto/16 :goto_4

    :cond_c
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const-string v2, "API failed to connect while resuming due to an unknown error."

    invoke-direct {v1, v5, v2}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;)V

    goto :goto_8

    .line 57
    :pswitch_a
    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefj;->h:Ljava/util/Map;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefk;

    .line 58
    iget-object v2, v0, Lefk;->h:Lefj;

    .line 59
    iget-object v2, v2, Lefj;->k:Landroid/os/Handler;

    .line 60
    invoke-static {v2}, Letf;->a(Landroid/os/Handler;)V

    iget-object v2, v0, Lefk;->a:Ledd;

    invoke-interface {v2}, Ledd;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lefk;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lefk;->b:Leec;

    .line 61
    iget-object v3, v2, Leec;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v2, v2, Leec;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    :cond_d
    move v1, v4

    .line 62
    :cond_e
    if-eqz v1, :cond_f

    invoke-virtual {v0}, Lefk;->g()V

    goto/16 :goto_4

    :cond_f
    iget-object v0, v0, Lefk;->a:Ledd;

    invoke-interface {v0}, Ledd;->e()V

    goto/16 :goto_4

    :cond_10
    move-object v1, v2

    goto/16 :goto_6

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_3
    .end packed-switch
.end method
