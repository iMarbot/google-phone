.class public interface abstract Lfnj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field public static final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lfnj;->a:I

    .line 2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfnj;->b:J

    return-void
.end method


# virtual methods
.method public abstract executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;)V
.end method

.method public abstract executeRequest(Ljava/lang/String;Lhfz;Ljava/lang/Class;Lfnn;IJI)V
.end method

.method public abstract release()V
.end method
