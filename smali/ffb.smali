.class public final Lffb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private c:I


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lffb;->c:I

    .line 24
    iput-object p2, p0, Lffb;->a:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lffb;->b:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p2, p0, Lffb;->c:I

    .line 3
    const-string v0, "phone"

    .line 4
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 5
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffb;->a:Ljava/lang/String;

    .line 7
    const-string v0, "phone"

    .line 8
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 9
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 13
    invoke-static {p1}, Lfho;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14
    invoke-static {p1, v0}, Lfho;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    :cond_0
    :goto_0
    iput-object v0, p0, Lffb;->b:Ljava/lang/String;

    .line 21
    return-void

    .line 16
    :cond_1
    invoke-static {p1}, Lfho;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 17
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lfgb;)Lffb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lfgb;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 28
    :goto_0
    iget-object v2, p0, Lfgb;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 29
    :goto_1
    new-instance v2, Lffb;

    iget v3, p0, Lfgb;->a:I

    invoke-direct {v2, v3, v0, v1}, Lffb;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 27
    :cond_0
    iget-object v0, p0, Lfgb;->b:Ljava/lang/String;

    goto :goto_0

    .line 28
    :cond_1
    iget-object v1, p0, Lfgb;->c:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lffb;->d()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x2

    .line 72
    .line 73
    iget-object v3, p0, Lffb;->a:Ljava/lang/String;

    .line 74
    if-eqz v3, :cond_6

    .line 75
    const-string v4, "310260"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v0

    .line 87
    :goto_0
    if-eq v3, v0, :cond_0

    if-ne v3, v1, :cond_1

    .line 88
    :cond_0
    iget v3, p0, Lffb;->c:I

    if-ne v3, v1, :cond_7

    .line 93
    :cond_1
    :goto_1
    return v0

    .line 77
    :cond_2
    const-string v4, "310120"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v3, v1

    .line 78
    goto :goto_0

    .line 79
    :cond_3
    const-string v4, "311580"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v3, v2

    .line 80
    goto :goto_0

    .line 81
    :cond_4
    const-string v4, "23420"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 82
    const/4 v3, 0x4

    goto :goto_0

    .line 83
    :cond_5
    const-string v4, "45403"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 84
    const/4 v3, 0x5

    goto :goto_0

    .line 85
    :cond_6
    const/4 v3, 0x0

    goto :goto_0

    .line 90
    :cond_7
    iget v3, p0, Lffb;->c:I

    if-ne v3, v0, :cond_8

    move v0, v1

    .line 91
    goto :goto_1

    :cond_8
    move v0, v2

    .line 92
    goto :goto_1
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lffb;->a:Ljava/lang/String;

    .line 95
    if-eqz v0, :cond_4

    .line 96
    const-string v1, "310260"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    const/4 v0, 0x2

    .line 107
    :goto_0
    return v0

    .line 98
    :cond_0
    const-string v1, "310120"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    const/4 v0, 0x1

    goto :goto_0

    .line 100
    :cond_1
    const-string v1, "311580"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    const/4 v0, 0x3

    goto :goto_0

    .line 102
    :cond_2
    const-string v1, "23420"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    const/4 v0, 0x4

    goto :goto_0

    .line 104
    :cond_3
    const-string v1, "45403"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 105
    const/4 v0, 0x5

    goto :goto_0

    .line 106
    :cond_4
    const/4 v0, 0x0

    .line 107
    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lffb;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x3

    .line 112
    :goto_0
    return v0

    .line 110
    :cond_0
    iget-object v0, p0, Lffb;->b:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x1

    goto :goto_0

    .line 112
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final e()Lfgb;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lfgb;

    invoke-direct {v0}, Lfgb;-><init>()V

    .line 114
    iget v1, p0, Lffb;->c:I

    iput v1, v0, Lfgb;->a:I

    .line 115
    iget-object v1, p0, Lffb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lffb;->a:Ljava/lang/String;

    iput-object v1, v0, Lfgb;->b:Ljava/lang/String;

    .line 117
    :cond_0
    iget-object v1, p0, Lffb;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lffb;->b:Ljava/lang/String;

    iput-object v1, v0, Lfgb;->c:Ljava/lang/String;

    .line 119
    :cond_1
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    .line 61
    :cond_3
    check-cast p1, Lffb;

    .line 62
    iget v2, p0, Lffb;->c:I

    iget v3, p1, Lffb;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lffb;->a:Ljava/lang/String;

    iget-object v3, p1, Lffb;->a:Ljava/lang/String;

    .line 63
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lffb;->b:Ljava/lang/String;

    iget-object v3, p1, Lffb;->b:Ljava/lang/String;

    .line 64
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    .line 65
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lffb;->c:I

    add-int/lit8 v0, v0, 0x1f

    .line 67
    iget-object v1, p0, Lffb;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 68
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lffb;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_0
    iget-object v1, p0, Lffb;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 70
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lffb;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_1
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 30
    .line 31
    iget-object v0, p0, Lffb;->a:Ljava/lang/String;

    .line 32
    if-eqz v0, :cond_4

    .line 33
    const-string v3, "310260"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 45
    :goto_0
    if-ne v0, v1, :cond_5

    .line 46
    const-string v0, "T-Mobile"

    .line 50
    :goto_1
    iget v3, p0, Lffb;->c:I

    if-ne v3, v2, :cond_8

    .line 51
    const-string v1, "roaming"

    .line 55
    :goto_2
    iget-object v2, p0, Lffb;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 35
    :cond_0
    const-string v3, "310120"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const-string v3, "311580"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 38
    const/4 v0, 0x3

    goto :goto_0

    .line 39
    :cond_2
    const-string v3, "23420"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 40
    const/4 v0, 0x4

    goto :goto_0

    .line 41
    :cond_3
    const-string v3, "45403"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x5

    goto :goto_0

    .line 43
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 47
    :cond_5
    if-ne v0, v2, :cond_6

    .line 48
    const-string v0, "Sprint"

    goto :goto_1

    .line 49
    :cond_6
    const-string v3, "Uknown carrier: "

    iget-object v0, p0, Lffb;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 52
    :cond_8
    iget v2, p0, Lffb;->c:I

    if-ne v2, v1, :cond_9

    .line 53
    const-string v1, "not roaming"

    goto/16 :goto_2

    .line 54
    :cond_9
    const-string v1, "roaming status unknown"

    goto/16 :goto_2
.end method
