.class public Laix;
.super Landroid/content/AsyncTaskLoader;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laix$a;,
        Laix$d;,
        Laix$c;,
        Laix$b;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Laiv;


# instance fields
.field private c:Landroid/net/Uri;

.field private d:Ljava/util/Set;

.field private e:Landroid/net/Uri;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Laiv;

.field private k:Landroid/content/Loader$ForceLoadContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470
    const-class v0, Laix;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laix;->a:Ljava/lang/String;

    .line 471
    const/4 v0, 0x0

    sput-object v0, Laix;->b:Laiv;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move v6, v3

    invoke-direct/range {v0 .. v6}, Laix;-><init>(Landroid/content/Context;Landroid/net/Uri;ZZZZ)V

    .line 2
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;ZZZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 4
    invoke-static {}, Lgfb$a;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Laix;->d:Ljava/util/Set;

    .line 5
    iput-object p2, p0, Laix;->e:Landroid/net/Uri;

    .line 6
    iput-object p2, p0, Laix;->c:Landroid/net/Uri;

    .line 7
    iput-boolean v1, p0, Laix;->f:Z

    .line 8
    iput-boolean v1, p0, Laix;->g:Z

    .line 9
    iput-boolean p5, p0, Laix;->h:Z

    .line 10
    iput-boolean v1, p0, Laix;->i:Z

    .line 11
    return-void
.end method

.method private a()Laiv;
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 70
    sget-object v0, Laix;->a:Ljava/lang/String;

    iget-object v1, p0, Laix;->e:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "loadInBackground="

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :try_start_0
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 72
    iget-object v1, p0, Laix;->e:Landroid/net/Uri;

    invoke-static {v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 73
    const/4 v2, 0x0

    sput-object v2, Laix;->b:Laiv;

    .line 74
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    const-string v3, "encoded"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 75
    iget-object v0, p0, Laix;->e:Landroid/net/Uri;

    invoke-static {v1, v0}, Laix;->a(Landroid/net/Uri;Landroid/net/Uri;)Laiv;

    move-result-object v6

    .line 77
    :goto_0
    invoke-virtual {v6}, Laiv;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    invoke-virtual {v6}, Laiv;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    iget-wide v2, v6, Laiv;->b:J

    .line 84
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Directory;->CONTENT_URI:Landroid/net/Uri;

    .line 86
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Laix$c;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 87
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 88
    if-eqz v8, :cond_1

    .line 89
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 92
    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 93
    const/4 v3, 0x3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 94
    const/4 v4, 0x4

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 95
    const/4 v5, 0x5

    invoke-interface {v8, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 97
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 98
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 99
    :try_start_2
    invoke-virtual {v9, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v9

    .line 100
    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    :goto_1
    move-object v0, v6

    .line 104
    :try_start_3
    invoke-virtual/range {v0 .. v5}, Laiv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105
    :cond_0
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 114
    :cond_1
    :goto_2
    iget-boolean v0, p0, Laix;->i:Z

    if-eqz v0, :cond_2

    .line 115
    invoke-direct {p0, v6}, Laix;->d(Laiv;)V

    .line 116
    :cond_2
    invoke-direct {p0, v6}, Laix;->a(Laiv;)V

    .line 117
    iget-boolean v0, p0, Laix;->g:Z

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, v6, Laiv;->i:Lgue;

    .line 119
    if-nez v0, :cond_3

    .line 120
    invoke-direct {p0, v6}, Laix;->b(Laiv;)V

    :cond_3
    move-object v0, v6

    .line 126
    :goto_3
    return-object v0

    .line 76
    :cond_4
    invoke-direct {p0, v0, v1}, Laix;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Laiv;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v6

    goto/16 :goto_0

    .line 103
    :catch_0
    move-exception v9

    :try_start_5
    sget-object v9, Laix;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x32

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Contact directory resource not found: "

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, "."

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v9, v0, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_5
    move-object v2, v7

    goto :goto_1

    .line 107
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 122
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 123
    sget-object v0, Laix;->a:Ljava/lang/String;

    iget-object v2, p0, Laix;->e:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error loading the contact: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    iget-object v2, p0, Laix;->c:Landroid/net/Uri;

    .line 125
    new-instance v0, Laiv;

    sget-object v3, Laiw;->b:Laiw;

    invoke-direct {v0, v2, v3, v1}, Laiv;-><init>(Landroid/net/Uri;Laiw;Ljava/lang/Exception;)V

    goto :goto_3

    .line 109
    :cond_6
    :try_start_7
    iget-boolean v0, p0, Laix;->f:Z

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, v6, Laiv;->j:Lgue;

    .line 112
    if-nez v0, :cond_1

    .line 113
    invoke-direct {p0, v6}, Laix;->c(Laiv;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_2
.end method

.method private final a(Landroid/content/ContentResolver;Landroid/net/Uri;)Laiv;
    .locals 26

    .prologue
    .line 127
    const-string v2, "entities"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 128
    sget-object v4, Laix$b;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "raw_contact_id"

    move-object/from16 v2, p1

    .line 129
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 130
    if-nez v25, :cond_0

    .line 131
    sget-object v2, Laix;->a:Ljava/lang/String;

    const-string v3, "No cursor returned in loadContactEntity"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Laix;->c:Landroid/net/Uri;

    invoke-static {v2}, Laiv;->a(Landroid/net/Uri;)Laiv;

    move-result-object v2

    .line 230
    :goto_0
    return-object v2

    .line 133
    :cond_0
    :try_start_0
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    .line 134
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Laix;->c:Landroid/net/Uri;

    invoke-static {v2}, Laiv;->a(Landroid/net/Uri;)Laiv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 136
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 139
    :cond_1
    :try_start_1
    const-string v2, "directory"

    .line 140
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    if-nez v2, :cond_6

    const-wide/16 v6, 0x0

    .line 142
    :goto_1
    const/16 v2, 0xd

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 143
    const/4 v2, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 144
    const/4 v2, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 145
    const/4 v2, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 146
    const/4 v2, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 147
    const/4 v2, 0x4

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 148
    const/4 v2, 0x5

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 149
    const/4 v2, 0x6

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 150
    const/16 v2, 0x3a

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 151
    const/4 v2, 0x7

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_7

    const/16 v20, 0x1

    .line 152
    :goto_2
    const/16 v2, 0x8

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 153
    const/16 v21, 0x0

    .line 155
    :goto_3
    const/16 v2, 0x3b

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    const/16 v22, 0x1

    .line 156
    :goto_4
    const/16 v2, 0x3c

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 157
    const/16 v2, 0x3d

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    const/16 v24, 0x1

    .line 158
    :goto_5
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-eqz v2, :cond_2

    const-wide/16 v2, 0x1

    cmp-long v2, v6, v2

    if-nez v2, :cond_b

    .line 159
    :cond_2
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 160
    invoke-static {v2, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 161
    invoke-static {v2, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 163
    :goto_6
    new-instance v2, Laiv;

    move-object/from16 v0, p0

    iget-object v3, v0, Laix;->c:Landroid/net/Uri;

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v24}, Laiv;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;Z)V

    .line 165
    const-wide/16 v4, -0x1

    .line 166
    const/4 v3, 0x0

    .line 167
    new-instance v8, Lguf;

    invoke-direct {v8}, Lguf;-><init>()V

    .line 168
    :cond_3
    const/16 v6, 0xe

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 169
    cmp-long v9, v6, v4

    if-eqz v9, :cond_4

    .line 171
    new-instance v3, Laiy;

    .line 172
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 173
    const-string v5, "_id"

    const/16 v9, 0xe

    move-object/from16 v0, v25

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 174
    const/16 v5, 0xf

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 175
    const/16 v5, 0x10

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 176
    const/16 v5, 0x11

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 177
    const/16 v5, 0x12

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 178
    const/16 v5, 0x13

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 179
    const/16 v5, 0x14

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 180
    const/16 v5, 0x15

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 181
    const/16 v5, 0x16

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 182
    const/16 v5, 0x17

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 183
    const/16 v5, 0x18

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 184
    const/16 v5, 0x19

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 185
    const/16 v5, 0xd

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 186
    const/4 v5, 0x7

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 188
    invoke-direct {v3, v4}, Laiy;-><init>(Landroid/content/ContentValues;)V

    .line 189
    invoke-virtual {v8, v3}, Lguf;->c(Ljava/lang/Object;)Lguf;

    move-wide v4, v6

    .line 190
    :cond_4
    const/16 v6, 0x1a

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 192
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 193
    const-string v7, "_id"

    const/16 v9, 0x1a

    move-object/from16 v0, v25

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v6, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 194
    const/16 v7, 0x1b

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 195
    const/16 v7, 0x1c

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 196
    const/16 v7, 0x1d

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 197
    const/16 v7, 0x1e

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 198
    const/16 v7, 0x1f

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 199
    const/16 v7, 0x20

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 200
    const/16 v7, 0x21

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 201
    const/16 v7, 0x22

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 202
    const/16 v7, 0x23

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 203
    const/16 v7, 0x24

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 204
    const/16 v7, 0x25

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 205
    const/16 v7, 0x26

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 206
    const/16 v7, 0x27

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 207
    const/16 v7, 0x28

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 208
    const/16 v7, 0x29

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 209
    const/16 v7, 0x2a

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 210
    const/16 v7, 0x2b

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 211
    const/16 v7, 0x2c

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 212
    const/16 v7, 0x2d

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 213
    const/16 v7, 0x2e

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 214
    const/16 v7, 0x2f

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 215
    const/16 v7, 0x30

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 216
    const/16 v7, 0x31

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 217
    const/16 v7, 0x32

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 218
    const/16 v7, 0x34

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 219
    const/16 v7, 0x3e

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 220
    const/16 v7, 0x3f

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 221
    const/16 v7, 0x40

    move-object/from16 v0, v25

    invoke-static {v0, v6, v7}, Laix;->a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V

    .line 224
    invoke-virtual {v3, v6}, Laiy;->a(Landroid/content/ContentValues;)V

    .line 225
    :cond_5
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 226
    invoke-virtual {v8}, Lguf;->a()Lgue;

    move-result-object v3

    .line 227
    iput-object v3, v2, Laiv;->h:Lgue;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 141
    :cond_6
    :try_start_2
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    goto/16 :goto_1

    .line 151
    :cond_7
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 154
    :cond_8
    const/16 v2, 0x8

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v21

    goto/16 :goto_3

    .line 155
    :cond_9
    const/16 v22, 0x0

    goto/16 :goto_4

    .line 157
    :cond_a
    const/16 v24, 0x0

    goto/16 :goto_5

    :cond_b
    move-object/from16 v5, p2

    .line 162
    goto/16 :goto_6

    .line 231
    :catchall_0
    move-exception v2

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public static a(Landroid/net/Uri;)Laiv;
    .locals 1

    .prologue
    .line 12
    :try_start_0
    invoke-static {p0, p0}, Laix;->a(Landroid/net/Uri;Landroid/net/Uri;)Laiv;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 14
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Landroid/net/Uri;)Laiv;
    .locals 26

    .prologue
    .line 15
    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v2

    .line 16
    new-instance v25, Lorg/json/JSONObject;

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 17
    const-string v2, "directory"

    .line 18
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 19
    const-string v2, "display_name"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 20
    const-string v2, "display_name_alt"

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 21
    const-string v2, "display_name_source"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 22
    const-string v2, "photo_uri"

    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 23
    new-instance v2, Laiv;

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const-wide/16 v11, -0x1

    const-wide/16 v14, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v24}, Laiv;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JLjava/lang/String;JJIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ZLjava/lang/String;Z)V

    .line 24
    const-string v3, "account_name"

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 25
    const-string v3, "displayName"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 26
    if-eqz v5, :cond_1

    .line 27
    const-string v4, "account_type"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 28
    const/4 v4, 0x0

    const-string v7, "exportSupport"

    const/4 v8, 0x1

    .line 29
    move-object/from16 v0, v25

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    .line 30
    invoke-virtual/range {v2 .. v7}, Laiv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 35
    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 36
    const-string v4, "_id"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 37
    const-string v4, "contact_id"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 38
    new-instance v5, Laiy;

    invoke-direct {v5, v3}, Laiy;-><init>(Landroid/content/ContentValues;)V

    .line 39
    const-string v3, "vnd.android.cursor.item/contact"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 40
    invoke-virtual {v6}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .line 41
    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 42
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 43
    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 44
    if-nez v4, :cond_2

    .line 45
    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 46
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 47
    invoke-virtual {v8, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 48
    invoke-static {v5, v9, v3}, Laix;->a(Laiy;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 49
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 32
    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "exportSupport"

    const/4 v8, 0x2

    .line 33
    move-object/from16 v0, v25

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v7

    .line 34
    invoke-virtual/range {v2 .. v7}, Laiv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 51
    :cond_2
    invoke-static {v5, v4, v3}, Laix;->a(Laiy;Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_1

    .line 53
    :cond_3
    new-instance v3, Lguf;

    invoke-direct {v3}, Lguf;-><init>()V

    invoke-virtual {v3, v5}, Lguf;->c(Ljava/lang/Object;)Lguf;

    move-result-object v3

    invoke-virtual {v3}, Lguf;->a()Lgue;

    move-result-object v3

    .line 54
    iput-object v3, v2, Laiv;->h:Lgue;

    .line 55
    return-object v2
.end method

.method private final a(Laiv;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 232
    .line 234
    iget-wide v4, p1, Laiv;->d:J

    .line 236
    const-wide/16 v2, 0x0

    cmp-long v0, v4, v2

    if-lez v0, :cond_2

    .line 238
    iget-object v0, p1, Laiv;->h:Lgue;

    .line 239
    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v3

    move v2, v1

    :cond_0
    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laiy;

    .line 240
    invoke-virtual {v1}, Laiy;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laks;

    .line 242
    iget-object v7, v1, Laks;->a:Landroid/content/ContentValues;

    const-string v8, "_id"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 243
    cmp-long v7, v8, v4

    if-nez v7, :cond_1

    .line 244
    instance-of v6, v1, Lald;

    if-eqz v6, :cond_0

    .line 245
    check-cast v1, Lald;

    .line 248
    iget-object v1, v1, Laks;->a:Landroid/content/ContentValues;

    .line 249
    const-string v6, "data15"

    invoke-virtual {v1, v6}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 251
    iput-object v1, p1, Laiv;->l:[B

    goto :goto_0

    .line 256
    :cond_2
    iget-object v0, p1, Laiv;->e:Ljava/lang/String;

    .line 258
    if-eqz v0, :cond_5

    .line 259
    :try_start_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 261
    const-string v3, "http"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "https"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 262
    :cond_3
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    .line 263
    const/4 v0, 0x0

    move-object v2, v1

    move-object v1, v0

    .line 266
    :goto_1
    const/16 v0, 0x4000

    new-array v0, v0, [B

    .line 267
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :goto_2
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_8

    .line 269
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 275
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 276
    if-eqz v1, :cond_4

    .line 277
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_4
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    .line 281
    :cond_5
    iget-object v0, p1, Laiv;->l:[B

    .line 283
    iput-object v0, p1, Laiv;->k:[B

    .line 284
    :cond_6
    :goto_3
    return-void

    .line 264
    :cond_7
    :try_start_3
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    goto :goto_1

    .line 270
    :cond_8
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 271
    iput-object v0, p1, Laiv;->k:[B
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 272
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 273
    if-eqz v1, :cond_6

    .line 274
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_3
.end method

.method private static a(Laiy;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 56
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 57
    const-string v0, "mimetype"

    invoke-virtual {v2, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "_id"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 59
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 60
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 63
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 64
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :cond_1
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 66
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p0, v2}, Laiy;->a(Landroid/content/ContentValues;)V

    .line 69
    return-void
.end method

.method private static a(Landroid/database/Cursor;Landroid/content/ContentValues;I)V
    .locals 4

    .prologue
    .line 305
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 313
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid or unhandled data type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :pswitch_1
    sget-object v0, Laix$b;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 312
    :goto_0
    :pswitch_2
    return-void

    .line 309
    :pswitch_3
    sget-object v0, Laix$b;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 311
    :pswitch_4
    sget-object v0, Laix$b;->a:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-interface {p0, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private final b()V
    .locals 9

    .prologue
    .line 418
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 419
    iget-object v0, p0, Laix;->j:Laiv;

    .line 420
    iget-object v0, v0, Laiv;->h:Lgue;

    .line 421
    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laiy;

    .line 424
    iget-object v5, v1, Laiy;->a:Landroid/content/ContentValues;

    .line 425
    const-string v6, "_id"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    .line 426
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 427
    iget-object v5, p0, Laix;->d:Ljava/util/Set;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 428
    iget-object v5, p0, Laix;->d:Ljava/util/Set;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 431
    iget-object v5, v1, Laiy;->b:Lain;

    if-nez v5, :cond_1

    .line 432
    invoke-static {v3}, Lain;->a(Landroid/content/Context;)Lain;

    move-result-object v5

    iput-object v5, v1, Laiy;->b:Lain;

    .line 433
    :cond_1
    iget-object v5, v1, Laiy;->b:Lain;

    .line 434
    invoke-virtual {v1}, Laiy;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Laiy;->b()Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-static {v8, v1}, Lajj;->a(Ljava/lang/String;Ljava/lang/String;)Lajj;

    move-result-object v1

    invoke-virtual {v5, v1}, Lain;->a(Lajj;)Lajc;

    move-result-object v1

    .line 437
    invoke-virtual {v1}, Lajc;->d()Ljava/lang/String;

    move-result-object v5

    .line 438
    invoke-virtual {v1}, Lajc;->e()Ljava/lang/String;

    move-result-object v1

    .line 439
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 440
    sget-object v8, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 441
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 442
    invoke-virtual {v7, v1, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    const-string v1, "vnd.android.cursor.item/raw_contact"

    invoke-virtual {v7, v6, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 445
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 446
    :try_start_0
    invoke-virtual {v3, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 448
    :catch_0
    move-exception v1

    .line 449
    sget-object v5, Laix;->a:Ljava/lang/String;

    const-string v6, "Error sending message to source-app"

    invoke-static {v5, v6, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 451
    :cond_2
    return-void
.end method

.method private final b(Laiv;)V
    .locals 7

    .prologue
    .line 285
    new-instance v3, Lguf;

    invoke-direct {v3}, Lguf;-><init>()V

    .line 287
    iget-boolean v0, p1, Laiv;->g:Z

    .line 288
    if-nez v0, :cond_1

    .line 290
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lain;->a(Landroid/content/Context;)Lain;

    move-result-object v0

    invoke-virtual {v0}, Lain;->a()Ljava/util/Map;

    move-result-object v0

    .line 291
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 292
    invoke-static {v0}, Lhcw;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v4

    .line 294
    iget-object v0, p1, Laiv;->h:Lgue;

    .line 295
    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laiy;

    .line 297
    invoke-virtual {v1}, Laiy;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Laiy;->b()Ljava/lang/String;

    move-result-object v1

    .line 298
    invoke-static {v6, v1}, Lajj;->a(Ljava/lang/String;Ljava/lang/String;)Lajj;

    move-result-object v1

    .line 299
    invoke-interface {v4, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 301
    :cond_0
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v3, v0}, Lguf;->b(Ljava/lang/Iterable;)Lguf;

    .line 302
    :cond_1
    invoke-virtual {v3}, Lguf;->a()Lgue;

    move-result-object v0

    .line 303
    iput-object v0, p1, Laiv;->i:Lgue;

    .line 304
    return-void
.end method

.method private final c()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    if-eqz v0, :cond_0

    .line 453
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 454
    const/4 v0, 0x0

    iput-object v0, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    .line 455
    :cond_0
    return-void
.end method

.method private final c(Laiv;)V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 314
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 315
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 316
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 318
    iget-object v0, p1, Laiv;->h:Lgue;

    .line 319
    check-cast v0, Lgue;

    invoke-virtual {v0}, Lgue;->size()I

    move-result v6

    move v2, v10

    :cond_0
    :goto_0
    if-ge v2, v6, :cond_3

    invoke-virtual {v0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    check-cast v1, Laiy;

    .line 322
    iget-object v7, v1, Laiy;->a:Landroid/content/ContentValues;

    .line 323
    const-string v8, "account_name"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 325
    invoke-virtual {v1}, Laiy;->a()Ljava/lang/String;

    move-result-object v8

    .line 326
    invoke-virtual {v1}, Laiy;->b()Ljava/lang/String;

    move-result-object v1

    .line 327
    new-instance v11, Laix$a;

    invoke-direct {v11, v7, v8, v1}, Laix$a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    if-eqz v7, :cond_0

    if-eqz v8, :cond_0

    invoke-virtual {v5, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 329
    invoke-virtual {v5, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-eqz v11, :cond_1

    .line 331
    const-string v11, " OR "

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_1
    const-string v11, "(account_name=? AND account_type=?"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    if-eqz v1, :cond_2

    .line 336
    const-string v7, " AND data_set=?"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    :goto_1
    const-string v1, ")"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 338
    :cond_2
    const-string v1, " AND data_set IS NULL"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 341
    :cond_3
    new-instance v11, Lguf;

    invoke-direct {v11}, Lguf;-><init>()V

    .line 343
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Laix$d;->a:[Ljava/lang/String;

    .line 345
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v10, [Ljava/lang/String;

    .line 346
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 347
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 348
    if-eqz v12, :cond_7

    .line 349
    :goto_2
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 350
    const/4 v0, 0x0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 351
    const/4 v0, 0x1

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 352
    const/4 v0, 0x2

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 353
    const/4 v0, 0x3

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 354
    const/4 v0, 0x4

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 355
    const/4 v0, 0x5

    .line 356
    invoke-interface {v12, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v7, v9

    .line 357
    :goto_3
    const/4 v0, 0x6

    .line 358
    invoke-interface {v12, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v8, v9

    .line 359
    :goto_4
    new-instance v0, Lagb;

    invoke-direct/range {v0 .. v8}, Lagb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZ)V

    invoke-virtual {v11, v0}, Lguf;->c(Ljava/lang/Object;)Lguf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 363
    :catchall_0
    move-exception v0

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    move v7, v10

    .line 356
    goto :goto_3

    :cond_5
    move v8, v10

    .line 358
    goto :goto_4

    .line 361
    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 364
    :cond_7
    invoke-virtual {v11}, Lguf;->a()Lgue;

    move-result-object v0

    .line 365
    iput-object v0, p1, Laiv;->j:Lgue;

    .line 366
    return-void
.end method

.method private final d(Laiv;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 367
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 369
    iget-object v5, p1, Laiv;->h:Lgue;

    .line 371
    invoke-virtual {v5}, Lgue;->size()I

    move-result v6

    move v1, v2

    .line 372
    :goto_0
    if-ge v1, v6, :cond_2

    .line 373
    invoke-virtual {v5, v1}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiy;

    .line 374
    invoke-virtual {v0}, Laiy;->d()Ljava/util/List;

    move-result-object v7

    .line 375
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    .line 376
    :goto_1
    if-ge v3, v8, :cond_1

    .line 377
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laks;

    .line 378
    instance-of v9, v0, Lalc;

    if-eqz v9, :cond_0

    .line 379
    check-cast v0, Lalc;

    .line 383
    iget-object v9, v0, Laks;->a:Landroid/content/ContentValues;

    .line 384
    const-string v10, "data1"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 386
    if-eqz v9, :cond_0

    .line 390
    iget-object v10, v0, Laks;->a:Landroid/content/ContentValues;

    .line 391
    const-string v11, "data4"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 393
    invoke-static {v9, v10, v4}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 395
    iget-object v0, v0, Laks;->a:Landroid/content/ContentValues;

    .line 396
    const-string v10, "formattedPhoneNumber"

    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 398
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 399
    :cond_2
    return-void
.end method

.method private e(Laiv;)V
    .locals 4

    .prologue
    .line 400
    invoke-direct {p0}, Laix;->c()V

    .line 401
    invoke-virtual {p0}, Laix;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    iput-object p1, p0, Laix;->j:Laiv;

    .line 404
    invoke-virtual {p1}, Laiv;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 406
    iget-object v0, p1, Laiv;->a:Landroid/net/Uri;

    .line 407
    iput-object v0, p0, Laix;->e:Landroid/net/Uri;

    .line 408
    invoke-virtual {p1}, Laiv;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 409
    iget-object v0, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    if-nez v0, :cond_2

    .line 410
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    .line 411
    :cond_2
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 412
    invoke-virtual {p0}, Laix;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Laix;->e:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Laix;->k:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 414
    :cond_3
    :goto_1
    iget-boolean v0, p0, Laix;->h:Z

    if-eqz v0, :cond_4

    .line 415
    invoke-direct {p0}, Laix;->b()V

    .line 416
    :cond_4
    iget-object v0, p0, Laix;->j:Laiv;

    invoke-super {p0, v0}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    goto :goto_0

    .line 413
    :cond_5
    const-string v0, "ContactLoader.deliverResult"

    const-string v1, "contacts permission not available"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 469
    check-cast p1, Laiv;

    invoke-direct {p0, p1}, Laix;->e(Laiv;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 468
    invoke-direct {p0}, Laix;->a()Laiv;

    move-result-object v0

    return-object v0
.end method

.method protected onReset()V
    .locals 1

    .prologue
    .line 463
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 464
    invoke-virtual {p0}, Laix;->cancelLoad()Z

    .line 465
    invoke-direct {p0}, Laix;->c()V

    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Laix;->j:Laiv;

    .line 467
    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Laix;->j:Laiv;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Laix;->j:Laiv;

    invoke-direct {p0, v0}, Laix;->e(Laiv;)V

    .line 458
    :cond_0
    invoke-virtual {p0}, Laix;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Laix;->j:Laiv;

    if-nez v0, :cond_2

    .line 459
    :cond_1
    invoke-virtual {p0}, Laix;->forceLoad()V

    .line 460
    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 461
    invoke-virtual {p0}, Laix;->cancelLoad()Z

    .line 462
    return-void
.end method
