.class final Lbio;
.super Lagj;
.source "PG"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lagj;-><init>(Landroid/content/ContentResolver;)V

    .line 2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbio;->a:Z

    .line 23
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbio;->cancelOperation(I)V

    .line 24
    return-void
.end method

.method protected final a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 3
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lbib;->a(Lbio;)Lbio;

    .line 4
    iget-boolean v0, p0, Lbio;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 5
    invoke-static {p3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    .line 20
    :goto_0
    return-void

    .line 7
    :cond_0
    :try_start_1
    check-cast p2, Lbip;

    .line 8
    iget-object v0, p2, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 9
    invoke-virtual {p2}, Lbip;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 10
    if-eqz p3, :cond_1

    if-eqz v0, :cond_1

    iget v1, p2, Lbip;->b:I

    invoke-interface {p3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    const-string v1, "name"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 12
    const-string v2, "number"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 13
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 14
    iget-object v0, p2, Lbip;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1101f0

    .line 17
    invoke-static {v2, v3, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 18
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19
    :cond_1
    invoke-static {p3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 21
    :catchall_0
    move-exception v0

    invoke-static {p3}, Landroid/support/v7/widget/ActionMenuView$b;->a(Landroid/database/Cursor;)V

    throw v0
.end method
