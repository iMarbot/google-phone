.class public final Lfst;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfsq;


# instance fields
.field public final apiaryUri:Ljava/lang/String;

.field public authTime:J

.field public authToken:Ljava/lang/String;

.field public final context:Landroid/content/Context;

.field public httpTransport:Lgga;

.field public final pendingRequests:Ljava/util/Queue;

.field public requestListener:Lfsr;

.field public final useCronetForHttp:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    new-instance v1, Lggk;

    invoke-direct {v1}, Lggk;-><init>()V

    const-class v0, Lfmv;

    .line 2
    invoke-static {p1, v0}, Lgdq;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 3
    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lfst;-><init>(Landroid/content/Context;Ljava/lang/String;Lgga;Z)V

    .line 4
    return-void

    .line 2
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lgga;Z)V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p1, p0, Lfst;->context:Landroid/content/Context;

    .line 7
    iput-object p2, p0, Lfst;->apiaryUri:Ljava/lang/String;

    .line 8
    iput-object p3, p0, Lfst;->httpTransport:Lgga;

    .line 9
    iput-boolean p4, p0, Lfst;->useCronetForHttp:Z

    .line 10
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    .line 11
    return-void
.end method

.method static synthetic access$000(Lfst;)Lfsr;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lfst;->requestListener:Lfsr;

    return-object v0
.end method

.method private final makeRequest(JLjava/lang/String;[BILfsr;)V
    .locals 11

    .prologue
    .line 40
    iget-boolean v0, p0, Lfst;->useCronetForHttp:Z

    if-eqz v0, :cond_0

    .line 41
    new-instance v1, Lfss;

    iget-object v0, p0, Lfst;->context:Landroid/content/Context;

    const-class v2, Lfmv;

    .line 42
    invoke-static {v0, v2}, Lgdq;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lfmv;

    iget-object v8, p0, Lfst;->apiaryUri:Ljava/lang/String;

    iget-object v10, p0, Lfst;->context:Landroid/content/Context;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v10}, Lfss;-><init>(JLjava/lang/String;[BILfmv;Ljava/lang/String;Lfsr;Landroid/content/Context;)V

    .line 44
    :goto_0
    iget-object v0, p0, Lfst;->authToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lfst;->authToken:Ljava/lang/String;

    iget-wide v2, p0, Lfst;->authTime:J

    invoke-interface {v1, v0, v2, v3}, Lfsw;->setAuthToken(Ljava/lang/String;J)V

    .line 46
    new-instance v0, Lfsy;

    invoke-direct {v0, v1}, Lfsy;-><init>(Lfsw;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfsy;->executeOnThreadPool([Ljava/lang/Object;)Lfmx;

    .line 47
    const-string v0, "Starting apiary request: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    :goto_1
    return-void

    .line 43
    :cond_0
    new-instance v1, Lfta;

    iget-object v7, p0, Lfst;->httpTransport:Lgga;

    iget-object v8, p0, Lfst;->apiaryUri:Ljava/lang/String;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lfta;-><init>(JLjava/lang/String;[BILgga;Ljava/lang/String;Lfsr;)V

    goto :goto_0

    .line 48
    :cond_1
    iget-object v0, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 49
    const-string v0, "authToken not available yet, delaying request. #pending: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    .line 50
    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 51
    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static makeUserAgent(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutsApiaryClient"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    const-string v1, "; G+ SDK/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    if-nez p0, :cond_0

    const-string p0, "1.0.0"

    :cond_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final sendPendingRequests()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 19
    const-string v0, "Issuing any pending requests, #requests: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 20
    :goto_0
    iget-object v0, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    iget-object v0, p0, Lfst;->pendingRequests:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsw;

    .line 22
    iget-object v1, p0, Lfst;->authToken:Ljava/lang/String;

    iget-wide v2, p0, Lfst;->authTime:J

    invoke-interface {v0, v1, v2, v3}, Lfsw;->setAuthToken(Ljava/lang/String;J)V

    .line 23
    new-instance v1, Lfsy;

    invoke-direct {v1, v0}, Lfsy;-><init>(Lfsw;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lfsy;->executeOnThreadPool([Ljava/lang/Object;)Lfmx;

    goto :goto_0

    .line 25
    :cond_0
    return-void
.end method


# virtual methods
.method final createCronetRequestTaskForTest(JLjava/lang/String;[BILfmv;)Landroid/os/AsyncTask;
    .locals 11

    .prologue
    .line 60
    iget-object v0, p0, Lfst;->requestListener:Lfsr;

    .line 61
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    new-instance v1, Lfss;

    iget-object v8, p0, Lfst;->apiaryUri:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lfst;->failsafeRequestListener()Lfsr;

    move-result-object v9

    iget-object v10, p0, Lfst;->context:Landroid/content/Context;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lfss;-><init>(JLjava/lang/String;[BILfmv;Ljava/lang/String;Lfsr;Landroid/content/Context;)V

    .line 65
    iget-object v0, p0, Lfst;->authToken:Ljava/lang/String;

    iget-wide v2, p0, Lfst;->authTime:J

    invoke-interface {v1, v0, v2, v3}, Lfsw;->setAuthToken(Ljava/lang/String;J)V

    .line 66
    new-instance v0, Lfsz;

    invoke-direct {v0, v1}, Lfsz;-><init>(Lfsw;)V

    return-object v0
.end method

.method final createRequestTaskForTest(JLjava/lang/String;[BI)Landroid/os/AsyncTask;
    .locals 11

    .prologue
    .line 53
    iget-object v0, p0, Lfst;->requestListener:Lfsr;

    .line 54
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    new-instance v1, Lfta;

    iget-object v7, p0, Lfst;->httpTransport:Lgga;

    iget-object v8, p0, Lfst;->apiaryUri:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Lfst;->failsafeRequestListener()Lfsr;

    move-result-object v9

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    invoke-direct/range {v1 .. v9}, Lfta;-><init>(JLjava/lang/String;[BILgga;Ljava/lang/String;Lfsr;)V

    .line 58
    iget-object v0, p0, Lfst;->authToken:Ljava/lang/String;

    iget-wide v2, p0, Lfst;->authTime:J

    invoke-interface {v1, v0, v2, v3}, Lfsw;->setAuthToken(Ljava/lang/String;J)V

    .line 59
    new-instance v0, Lfsz;

    invoke-direct {v0, v1}, Lfsz;-><init>(Lfsw;)V

    return-object v0
.end method

.method final failsafeRequestListener()Lfsr;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lfsu;

    invoke-direct {v0, p0}, Lfsu;-><init>(Lfst;)V

    return-object v0
.end method

.method public final makeRequest(JLjava/lang/String;[BI)V
    .locals 9

    .prologue
    .line 29
    iget-object v0, p0, Lfst;->requestListener:Lfsr;

    .line 30
    const-string v1, "Expected non-null"

    invoke-static {v1, v0}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-virtual {p0}, Lfst;->failsafeRequestListener()Lfsr;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lfst;->makeRequest(JLjava/lang/String;[BILfsr;)V

    .line 33
    return-void
.end method

.method public final makeRequest(Ljava/lang/String;[BILfsr;)V
    .locals 8

    .prologue
    .line 34
    .line 35
    const-string v0, "Expected non-null"

    invoke-static {v0, p4}, Lfmw;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lfst;->makeRequest(JLjava/lang/String;[BILfsr;)V

    .line 38
    return-void
.end method

.method public final release()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public final setAuthToken(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12
    iget-object v0, p0, Lfst;->authToken:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    .line 13
    :goto_0
    const-string v3, "Setting authToken, wasNull: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iput-object p1, p0, Lfst;->authToken:Ljava/lang/String;

    .line 15
    iput-wide p2, p0, Lfst;->authTime:J

    .line 16
    if-eqz v0, :cond_0

    .line 17
    invoke-direct {p0}, Lfst;->sendPendingRequests()V

    .line 18
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 12
    goto :goto_0
.end method

.method public final setListener(Lfsr;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lfst;->requestListener:Lfsr;

    .line 27
    return-void
.end method
