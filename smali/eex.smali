.class public final Leex;
.super Ledj;

# interfaces
.implements Lefv;


# instance fields
.field public final b:Ljava/util/Queue;

.field public final c:Ljava/util/Map;

.field public d:Ljava/util/Set;

.field public e:Ljava/util/Set;

.field public final f:Legs;

.field private g:Ljava/util/concurrent/locks/Lock;

.field private h:Z

.field private i:Leic;

.field private j:Lefu;

.field private k:I

.field private l:Landroid/content/Context;

.field private m:Landroid/os/Looper;

.field private volatile n:Z

.field private o:J

.field private p:J

.field private q:Lefc;

.field private r:Lecn;

.field private s:Lefr;

.field private t:Lejm;

.field private u:Ljava/util/Map;

.field private v:Ledb;

.field private w:Lege;

.field private x:Ljava/util/ArrayList;

.field private y:Ljava/lang/Integer;

.field private z:Leid;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lejm;Lecn;Ledb;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/Map;IILjava/util/ArrayList;)V
    .locals 5

    invoke-direct {p0}, Ledj;-><init>()V

    const/4 v2, 0x0

    iput-object v2, p0, Leex;->j:Lefu;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Leex;->b:Ljava/util/Queue;

    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Leex;->o:J

    const-wide/16 v2, 0x1388

    iput-wide v2, p0, Leex;->p:J

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Leex;->d:Ljava/util/Set;

    new-instance v2, Lege;

    invoke-direct {v2}, Lege;-><init>()V

    iput-object v2, p0, Leex;->w:Lege;

    const/4 v2, 0x0

    iput-object v2, p0, Leex;->y:Ljava/lang/Integer;

    const/4 v2, 0x0

    iput-object v2, p0, Leex;->e:Ljava/util/Set;

    new-instance v2, Leey;

    invoke-direct {v2, p0}, Leey;-><init>(Leex;)V

    iput-object v2, p0, Leex;->z:Leid;

    iput-object p1, p0, Leex;->l:Landroid/content/Context;

    iput-object p2, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    const/4 v2, 0x0

    iput-boolean v2, p0, Leex;->h:Z

    new-instance v2, Leic;

    iget-object v3, p0, Leex;->z:Leid;

    invoke-direct {v2, p3, v3}, Leic;-><init>(Landroid/os/Looper;Leid;)V

    iput-object v2, p0, Leex;->i:Leic;

    iput-object p3, p0, Leex;->m:Landroid/os/Looper;

    new-instance v2, Lefc;

    invoke-direct {v2, p0, p3}, Lefc;-><init>(Leex;Landroid/os/Looper;)V

    iput-object v2, p0, Leex;->q:Lefc;

    iput-object p5, p0, Leex;->r:Lecn;

    move/from16 v0, p11

    iput v0, p0, Leex;->k:I

    iget v2, p0, Leex;->k:I

    if-ltz v2, :cond_0

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Leex;->y:Ljava/lang/Integer;

    :cond_0
    iput-object p7, p0, Leex;->u:Ljava/util/Map;

    iput-object p10, p0, Leex;->c:Ljava/util/Map;

    move-object/from16 v0, p13

    iput-object v0, p0, Leex;->x:Ljava/util/ArrayList;

    new-instance v2, Legs;

    iget-object v3, p0, Leex;->c:Ljava/util/Map;

    invoke-direct {v2, v3}, Legs;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, Leex;->f:Legs;

    invoke-interface {p8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ledl;

    iget-object v4, p0, Leex;->i:Leic;

    invoke-virtual {v4, v2}, Leic;->a(Ledl;)V

    goto :goto_0

    :cond_1
    invoke-interface {p9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ledm;

    iget-object v4, p0, Leex;->i:Leic;

    invoke-virtual {v4, v2}, Leic;->a(Ledm;)V

    goto :goto_1

    :cond_2
    iput-object p4, p0, Leex;->t:Lejm;

    iput-object p6, p0, Leex;->v:Ledb;

    return-void
.end method

.method public static a(Ljava/lang/Iterable;Z)I
    .locals 6

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledd;

    invoke-interface {v0}, Ledd;->h()Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v3

    :cond_0
    invoke-interface {v0}, Ledd;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    const/4 v3, 0x2

    :cond_2
    :goto_2
    return v3

    :cond_3
    const/4 v3, 0x3

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private final a(Ledj;Legn;Z)V
    .locals 2

    sget-object v0, Lept;->c:Lepv;

    invoke-virtual {v0, p1}, Lepv;->a(Ledj;)Ledn;

    move-result-object v0

    new-instance v1, Lefb;

    invoke-direct {v1, p0, p2, p3, p1}, Lefb;-><init>(Leex;Legn;ZLedj;)V

    invoke-virtual {v0, v1}, Ledn;->a(Ledt;)V

    return-void
.end method

.method static synthetic a(Leex;)V
    .locals 2

    .prologue
    .line 46
    .line 47
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Leex;->n:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Leex;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(Leex;Ledj;Legn;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Leex;->a(Ledj;Legn;Z)V

    return-void
.end method

.method private final b(I)V
    .locals 12

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Leex;->y:Ljava/lang/Integer;

    if-nez v1, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Leex;->y:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Leex;->j:Lefu;

    if-eqz v1, :cond_2

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p1}, Leex;->c(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Leex;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x33

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot use sign-in mode: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ". Mode was already set to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Leex;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledd;

    invoke-interface {v0}, Ledd;->h()Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    :cond_3
    invoke-interface {v0}, Ledd;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_5
    :pswitch_0
    new-instance v0, Leff;

    iget-object v1, p0, Leex;->l:Landroid/content/Context;

    iget-object v3, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    iget-object v4, p0, Leex;->m:Landroid/os/Looper;

    iget-object v5, p0, Leex;->r:Lecn;

    iget-object v6, p0, Leex;->c:Ljava/util/Map;

    iget-object v7, p0, Leex;->t:Lejm;

    iget-object v8, p0, Leex;->u:Ljava/util/Map;

    iget-object v9, p0, Leex;->v:Ledb;

    iget-object v10, p0, Leex;->x:Ljava/util/ArrayList;

    move-object v2, p0

    move-object v11, p0

    invoke-direct/range {v0 .. v11}, Leff;-><init>(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;Lefv;)V

    iput-object v0, p0, Leex;->j:Lefu;

    goto/16 :goto_0

    :pswitch_1
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    if-eqz v2, :cond_5

    iget-object v0, p0, Leex;->l:Landroid/content/Context;

    iget-object v2, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    iget-object v3, p0, Leex;->m:Landroid/os/Looper;

    iget-object v4, p0, Leex;->r:Lecn;

    iget-object v5, p0, Leex;->c:Ljava/util/Map;

    iget-object v6, p0, Leex;->t:Lejm;

    iget-object v7, p0, Leex;->u:Ljava/util/Map;

    iget-object v8, p0, Leex;->v:Ledb;

    iget-object v9, p0, Leex;->x:Ljava/util/ArrayList;

    move-object v1, p0

    invoke-static/range {v0 .. v9}, Lehq;->a(Landroid/content/Context;Leex;Ljava/util/concurrent/locks/Lock;Landroid/os/Looper;Lecp;Ljava/util/Map;Lejm;Ljava/util/Map;Ledb;Ljava/util/ArrayList;)Lehq;

    move-result-object v0

    iput-object v0, p0, Leex;->j:Lefu;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Leex;)V
    .locals 2

    .prologue
    .line 48
    .line 49
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Leex;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Leex;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic c(Leex;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Leex;->l:Landroid/content/Context;

    return-object v0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "SIGN_IN_MODE_NONE"

    goto :goto_0

    :pswitch_1
    const-string v0, "SIGN_IN_MODE_REQUIRED"

    goto :goto_0

    :pswitch_2
    const-string v0, "SIGN_IN_MODE_OPTIONAL"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private final n()V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Leex;->i:Leic;

    .line 34
    const/4 v1, 0x1

    iput-boolean v1, v0, Leic;->d:Z

    .line 35
    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0}, Lefu;->a()V

    return-void
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lecl;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_1

    :goto_0
    const-string v1, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    const-string v0, "TimeUnit must not be null"

    invoke-static {p3, v0}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    if-nez v0, :cond_2

    iget-object v0, p0, Leex;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Leex;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leex;->y:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Leex;->b(I)V

    iget-object v0, p0, Leex;->i:Leic;

    .line 23
    const/4 v1, 0x1

    iput-boolean v1, v0, Leic;->d:Z

    .line 24
    iget-object v0, p0, Leex;->j:Lefu;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v2, v3, p3}, Lefu;->a(JLjava/util/concurrent/TimeUnit;)Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :cond_1
    move v0, v1

    .line 22
    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 24
    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lehe;)Lehe;
    .locals 4

    .prologue
    .line 1
    .line 2
    iget-object v0, p1, Lehe;->a:Letf;

    .line 3
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->c:Ljava/util/Map;

    .line 4
    iget-object v1, p1, Lehe;->a:Letf;

    .line 5
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 6
    iget-object v0, p1, Lehe;->b:Lesq;

    .line 7
    if-eqz v0, :cond_1

    .line 8
    iget-object v0, p1, Lehe;->b:Lesq;

    .line 9
    invoke-virtual {v0}, Lesq;->d()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x41

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GoogleApiClient is not configured to use "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " required for this call."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leex;->j:Lefu;

    if-nez v0, :cond_2

    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-object p1

    .line 3
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 9
    :cond_1
    const-string v0, "the API"

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0, p1}, Lefu;->a(Lehe;)Lehe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(I)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    const/16 v1, 0x21

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Illegal sign-in mode: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Leex;->b(I)V

    invoke-direct {p0}, Leex;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(IZ)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    .line 43
    if-ne p1, v2, :cond_1

    if-nez p2, :cond_1

    iget-boolean v0, p0, Leex;->n:Z

    if-nez v0, :cond_1

    iput-boolean v2, p0, Leex;->n:Z

    iget-object v0, p0, Leex;->s:Lefr;

    if-nez v0, :cond_0

    iget-object v0, p0, Leex;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lefd;

    invoke-direct {v1, p0}, Lefd;-><init>(Leex;)V

    invoke-static {v0, v1}, Lecn;->a(Landroid/content/Context;Lefs;)Lefr;

    move-result-object v0

    iput-object v0, p0, Leex;->s:Lefr;

    :cond_0
    iget-object v0, p0, Leex;->q:Lefc;

    iget-object v1, p0, Leex;->q:Lefc;

    invoke-virtual {v1, v2}, Lefc;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Leex;->o:J

    invoke-virtual {v0, v1, v2, v3}, Lefc;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Leex;->q:Lefc;

    iget-object v1, p0, Leex;->q:Lefc;

    invoke-virtual {v1, v5}, Lefc;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Leex;->p:J

    invoke-virtual {v0, v1, v2, v3}, Lefc;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    iget-object v0, p0, Leex;->f:Legs;

    .line 44
    iget-object v0, v0, Legs;->c:Ljava/util/Set;

    sget-object v1, Legs;->b:[Lehk;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lehk;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    sget-object v4, Legs;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3, v4}, Lehk;->c(Lcom/google/android/gms/common/api/Status;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0, p1}, Leic;->a(I)V

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0}, Leic;->a()V

    if-ne p1, v5, :cond_3

    invoke-direct {p0}, Leex;->n()V

    :cond_3
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    :goto_0
    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehe;

    invoke-virtual {p0, v0}, Ledj;->b(Lehe;)Lehe;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0, p1}, Leic;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lecl;)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Leex;->r:Lecn;

    iget-object v1, p0, Leex;->l:Landroid/content/Context;

    .line 41
    iget v2, p1, Lecl;->b:I

    .line 42
    invoke-virtual {v0, v1, v2}, Lecp;->b(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Leex;->k()Z

    :cond_0
    iget-boolean v0, p0, Leex;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0, p1}, Leic;->a(Lecl;)V

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0}, Leic;->a()V

    :cond_1
    return-void
.end method

.method public final a(Ledl;)V
    .locals 1

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0, p1}, Leic;->a(Ledl;)V

    return-void
.end method

.method public final a(Ledm;)V
    .locals 1

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0, p1}, Leic;->a(Ledm;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mContext="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Leex;->l:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "mResuming="

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v1, p0, Leex;->n:Z

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mWorkQueue.size()="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(I)V

    iget-object v0, p0, Leex;->f:Legs;

    const-string v1, " mUnconsumedApiCalls.size()="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    iget-object v0, v0, Legs;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(I)V

    iget-object v0, p0, Leex;->j:Lefu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0, p1, p2, p3, p4}, Lefu;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Legj;)Z
    .locals 1

    iget-object v0, p0, Leex;->j:Lefu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0, p1}, Lefu;->a(Legj;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Leex;->l:Landroid/content/Context;

    return-object v0
.end method

.method public final b(Lehe;)Lehe;
    .locals 4

    .prologue
    .line 10
    .line 11
    iget-object v0, p1, Lehe;->a:Letf;

    .line 12
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->c:Ljava/util/Map;

    .line 13
    iget-object v1, p1, Lehe;->a:Letf;

    .line 14
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 15
    iget-object v0, p1, Lehe;->b:Lesq;

    .line 16
    if-eqz v0, :cond_1

    .line 17
    iget-object v0, p1, Lehe;->b:Lesq;

    .line 18
    invoke-virtual {v0}, Lesq;->d()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x41

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GoogleApiClient is not configured to use "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " required for this call."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Letf;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leex;->j:Lefu;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 12
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 18
    :cond_1
    const-string v0, "the API"

    goto :goto_1

    :cond_2
    :try_start_1
    iget-boolean v0, p0, Leex;->n:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehe;

    iget-object v1, p0, Leex;->f:Legs;

    invoke-virtual {v1, v0}, Legs;->a(Lehk;)V

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lehe;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    return-object p1

    :cond_4
    :try_start_2
    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0, p1}, Lefu;->b(Lehe;)Lehe;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object p1

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final b(Ledl;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Leex;->i:Leic;

    .line 37
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Leic;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Leic;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "GmsClientEvents"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x34

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unregisterConnectionCallbacks(): listener "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-boolean v2, v0, Leic;->e:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Leic;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Ledm;)V
    .locals 5

    .prologue
    .line 38
    iget-object v0, p0, Leex;->i:Leic;

    .line 39
    invoke-static {p1}, Letf;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Leic;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Leic;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClientEvents"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x39

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Leex;->m:Landroid/os/Looper;

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Leex;->j:Lefu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0}, Lefu;->e()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, Leex;->k:I

    if-ltz v1, :cond_2

    iget-object v1, p0, Leex;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Sign-in mode should have been set explicitly by auto-manage."

    invoke-static {v0, v1}, Letf;->a(ZLjava/lang/Object;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Ledj;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    if-nez v0, :cond_3

    iget-object v0, p0, Leex;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Leex;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leex;->y:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_3
    :try_start_2
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final f()Lecl;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 19
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v3}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Leex;->k:I

    if-ltz v0, :cond_3

    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    :goto_1
    const-string v0, "Sign-in mode should have been set explicitly by auto-manage."

    invoke-static {v1, v0}, Letf;->a(ZLjava/lang/Object;)V

    :cond_0
    :goto_2
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Leex;->b(I)V

    iget-object v0, p0, Leex;->i:Leic;

    .line 20
    const/4 v1, 0x1

    iput-boolean v1, v0, Leic;->d:Z

    .line 21
    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0}, Lefu;->b()Lecl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :cond_1
    move v0, v2

    .line 19
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    if-nez v0, :cond_4

    iget-object v0, p0, Leex;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Leex;->a(Ljava/lang/Iterable;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leex;->y:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 21
    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 19
    :cond_4
    :try_start_2
    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 25
    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Leex;->f:Legs;

    invoke-virtual {v0}, Legs;->a()V

    iget-object v0, p0, Leex;->j:Lefu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0}, Lefu;->c()V

    :cond_0
    iget-object v1, p0, Leex;->w:Lege;

    .line 26
    iget-object v0, v1, Lege;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lega;

    .line 27
    const/4 v3, 0x0

    iput-object v3, v0, Lega;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 28
    :cond_1
    :try_start_1
    iget-object v0, v1, Lege;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 29
    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehe;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lehk;->a(Legv;)V

    invoke-virtual {v0}, Ledn;->a()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Leex;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Leex;->j:Lefu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_3

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-void

    :cond_3
    :try_start_2
    invoke-virtual {p0}, Leex;->k()Z

    iget-object v0, p0, Leex;->i:Leic;

    invoke-virtual {v0}, Leic;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2
.end method

.method public final h()V
    .locals 0

    invoke-virtual {p0}, Ledj;->g()V

    invoke-virtual {p0}, Ledj;->e()V

    return-void
.end method

.method public final i()Ledn;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-virtual {p0}, Ledj;->j()Z

    move-result v0

    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Leex;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API"

    invoke-static {v0, v2}, Letf;->a(ZLjava/lang/Object;)V

    new-instance v0, Legn;

    invoke-direct {v0, p0}, Legn;-><init>(Ledj;)V

    iget-object v2, p0, Leex;->c:Ljava/util/Map;

    sget-object v3, Lept;->a:Letf;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p0, v0, v1}, Leex;->a(Ledj;Legn;Z)V

    .line 32
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 30
    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v2, Leez;

    invoke-direct {v2, p0, v1, v0}, Leez;-><init>(Leex;Ljava/util/concurrent/atomic/AtomicReference;Legn;)V

    new-instance v3, Lefa;

    invoke-direct {v3, v0}, Lefa;-><init>(Legn;)V

    new-instance v4, Ledk;

    iget-object v5, p0, Leex;->l:Landroid/content/Context;

    invoke-direct {v4, v5}, Ledk;-><init>(Landroid/content/Context;)V

    sget-object v5, Lept;->b:Lesq;

    invoke-virtual {v4, v5}, Ledk;->a(Lesq;)Ledk;

    move-result-object v4

    invoke-virtual {v4, v2}, Ledk;->a(Ledl;)Ledk;

    move-result-object v2

    invoke-virtual {v2, v3}, Ledk;->a(Ledm;)Ledk;

    move-result-object v2

    iget-object v3, p0, Leex;->q:Lefc;

    .line 31
    const-string v4, "Handler must not be null"

    invoke-static {v3, v4}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iput-object v3, v2, Ledk;->d:Landroid/os/Looper;

    .line 32
    invoke-virtual {v2}, Ledk;->b()Ledj;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-virtual {v2}, Ledj;->e()V

    goto :goto_1
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Leex;->j:Lefu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leex;->j:Lefu;

    invoke-interface {v0}, Lefu;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final k()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Leex;->n:Z

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Leex;->n:Z

    iget-object v0, p0, Leex;->q:Lefc;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lefc;->removeMessages(I)V

    iget-object v0, p0, Leex;->q:Lefc;

    invoke-virtual {v0, v1}, Lefc;->removeMessages(I)V

    iget-object v0, p0, Leex;->s:Lefr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leex;->s:Lefr;

    invoke-virtual {v0}, Lefr;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Leex;->s:Lefr;

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method final l()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, Leex;->e:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Leex;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Leex;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final m()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    const-string v1, ""

    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v1, v3, v2, v3}, Ledj;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
