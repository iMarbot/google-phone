.class public final Lcnq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcnt;

.field public b:Lcnp;

.field public c:Z

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 420
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "seen"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "flagged"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "answered"

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>(Lcnt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcnq;->a:Lcnt;

    .line 3
    iput-object p2, p0, Lcnq;->d:Ljava/lang/String;

    .line 4
    return-void
.end method

.method private static a(Ljava/io/InputStream;Ljava/lang/String;)Lcms;
    .locals 6

    .prologue
    .line 203
    invoke-static {p0, p1}, Lcnn;->a(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 204
    new-instance v1, Lcng;

    invoke-direct {v1}, Lcng;-><init>()V

    .line 205
    invoke-virtual {v1}, Lcng;->a()Ljava/io/OutputStream;

    move-result-object v2

    .line 206
    const/16 v3, 0x4000

    :try_start_0
    new-array v3, v3, [B

    .line 207
    :goto_0
    const/4 v4, -0x1

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-eq v4, v5, :cond_0

    .line 208
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Landroid/util/Base64DataException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "\n\nThere was an error while decoding the message."

    .line 214
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 218
    :goto_1
    return-object v1

    .line 210
    :cond_0
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    goto :goto_1

    .line 217
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method private static a(Lcoe;Lcnd;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcoe;->a(I)Lcoc;

    move-result-object v0

    invoke-virtual {v0}, Lcoc;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    new-instance v1, Lcnm;

    invoke-direct {v1}, Lcnm;-><init>()V

    .line 221
    const/4 v0, 0x0

    .line 222
    iget-object v2, p0, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 223
    :goto_0
    if-ge v0, v2, :cond_2

    .line 224
    invoke-virtual {p0, v0}, Lcoe;->a(I)Lcoc;

    move-result-object v3

    .line 225
    invoke-virtual {v3}, Lcoc;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 226
    new-instance v3, Lcni;

    invoke-direct {v3}, Lcni;-><init>()V

    .line 227
    const-string v4, "TEXT"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 228
    invoke-virtual {p0, v0}, Lcoe;->b(I)Lcoe;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcnq;->a(Lcoe;Lcnd;Ljava/lang/String;)V

    .line 230
    :goto_1
    invoke-virtual {v1, v3}, Lcnm;->a(Lcmt;)V

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    invoke-virtual {p0, v0}, Lcoe;->b(I)Lcoe;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcnq;->a(Lcoe;Lcnd;Ljava/lang/String;)V

    goto :goto_1

    .line 232
    :cond_1
    invoke-virtual {v3}, Lcoc;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 233
    invoke-virtual {p0, v0}, Lcoe;->c(I)Lcol;

    move-result-object v0

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcnm;->b(Ljava/lang/String;)V

    .line 235
    :cond_2
    invoke-interface {p1, v1}, Lcnd;->a(Lcms;)V

    .line 303
    :goto_2
    return-void

    .line 237
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcoe;->c(I)Lcol;

    move-result-object v1

    .line 238
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcoe;->c(I)Lcol;

    move-result-object v0

    .line 239
    invoke-virtual {v1}, Lcol;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 240
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcoe;->b(I)Lcoe;

    move-result-object v2

    .line 241
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcoe;->c(I)Lcol;

    move-result-object v3

    .line 242
    const/4 v4, 0x5

    invoke-virtual {p0, v4}, Lcoe;->c(I)Lcol;

    move-result-object v4

    .line 243
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, Lcoe;->c(I)Lcol;

    move-result-object v5

    .line 244
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcol;->a(I)I

    move-result v5

    .line 246
    const-string v6, "message/rfc822"

    invoke-static {v0, v6}, Lcnn;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 247
    new-instance v0, Lcnb;

    const-string v1, "BODYSTRUCTURE message/rfc822 not yet supported."

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 249
    const/4 v0, 0x1

    .line 250
    iget-object v7, v2, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 251
    :goto_3
    if-ge v0, v7, :cond_5

    .line 252
    const-string v8, ";\n %s=\"%s\""

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, -0x1

    .line 253
    invoke-virtual {v2, v11}, Lcoe;->c(I)Lcol;

    move-result-object v11

    invoke-virtual {v11}, Lcol;->e()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 254
    invoke-virtual {v2, v0}, Lcoe;->c(I)Lcol;

    move-result-object v11

    invoke-virtual {v11}, Lcol;->e()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 255
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 256
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 258
    :cond_5
    const-string v0, "Content-Type"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcnd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v0, "TEXT"

    invoke-virtual {v1, v0}, Lcol;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcoe;->a(I)Lcoc;

    move-result-object v0

    invoke-virtual {v0}, Lcoc;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 260
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcoe;->b(I)Lcoe;

    move-result-object v0

    .line 262
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    iget-object v2, v0, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 265
    if-lez v2, :cond_8

    .line 266
    const/4 v2, 0x0

    .line 267
    invoke-virtual {v0, v2}, Lcoe;->c(I)Lcol;

    move-result-object v2

    invoke-virtual {v2}, Lcol;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 268
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 269
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_6
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcoe;->b(I)Lcoe;

    move-result-object v2

    .line 271
    invoke-virtual {v2}, Lcoe;->e()Z

    move-result v0

    if-nez v0, :cond_8

    .line 272
    const/4 v0, 0x1

    .line 273
    iget-object v6, v2, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 274
    :goto_5
    if-ge v0, v6, :cond_8

    .line 275
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, ";\n %s=\"%s\""

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, -0x1

    .line 276
    invoke-virtual {v2, v11}, Lcoe;->c(I)Lcol;

    move-result-object v11

    .line 277
    invoke-virtual {v11}, Lcol;->e()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 278
    invoke-virtual {v11, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 279
    invoke-virtual {v2, v0}, Lcoe;->c(I)Lcol;

    move-result-object v11

    invoke-virtual {v11}, Lcol;->e()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 280
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 281
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    add-int/lit8 v0, v0, 0x2

    goto :goto_5

    .line 261
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcoe;->b(I)Lcoe;

    move-result-object v0

    goto :goto_4

    .line 283
    :cond_8
    if-lez v5, :cond_9

    .line 284
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "size"

    invoke-static {v0, v2}, Lcnn;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 285
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, ";\n size=%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v2, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_a

    .line 287
    const-string v0, "Content-Disposition"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcnd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_a
    invoke-virtual {v4}, Lcol;->g()Z

    move-result v0

    if-nez v0, :cond_b

    .line 289
    const-string v0, "Content-Transfer-Encoding"

    invoke-virtual {v4}, Lcol;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcnd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_b
    invoke-virtual {v3}, Lcol;->g()Z

    move-result v0

    if-nez v0, :cond_c

    .line 291
    const-string v0, "Content-ID"

    invoke-virtual {v3}, Lcol;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcnd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_c
    if-lez v5, :cond_d

    .line 293
    instance-of v0, p1, Lcnv;

    if-eqz v0, :cond_e

    move-object v0, p1

    .line 294
    check-cast v0, Lcnv;

    .line 295
    iput v5, v0, Lcnv;->c:I

    .line 302
    :cond_d
    :goto_6
    const-string v0, "X-Android-Attachment-StoreData"

    invoke-interface {p1, v0, p2}, Lcnd;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 297
    :cond_e
    instance-of v0, p1, Lcni;

    if-eqz v0, :cond_f

    move-object v0, p1

    .line 298
    check-cast v0, Lcni;

    .line 299
    iput v5, v0, Lcni;->a:I

    goto :goto_6

    .line 301
    :cond_f
    new-instance v1, Lcnb;

    const-string v2, "Unknown part type "

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-direct {v1, v0}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_10
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7
.end method

.method private static a(Ljava/util/List;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 17
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 18
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    .line 19
    const/4 v1, 0x0

    const-string v4, "SEARCH"

    invoke-virtual {v0, v1, v4}, Lcoh;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x1

    .line 21
    :goto_0
    iget-object v4, v0, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 22
    if-ge v1, v4, :cond_0

    .line 23
    invoke-virtual {v0, v1}, Lcoh;->c(I)Lcol;

    move-result-object v4

    .line 27
    invoke-virtual {v4}, Lcol;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30
    :cond_1
    sget-object v0, Lcoq;->b:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private e()[Lcna;
    .locals 4

    .prologue
    .line 304
    invoke-direct {p0}, Lcnq;->f()V

    .line 305
    :try_start_0
    iget-object v0, p0, Lcnq;->b:Lcnp;

    const-string v1, "EXPUNGE"

    .line 306
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 308
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    .line 310
    const/4 v2, 0x1

    const-string v3, "EXISTS"

    invoke-virtual {v0, v2, v3}, Lcoh;->a(ILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcoh;->c(I)Lcol;

    move-result-object v0

    .line 312
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcol;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    .line 318
    :try_start_1
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 319
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 320
    sget-object v2, Lclt;->C:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 321
    iget-object v1, p0, Lcnq;->b:Lcnp;

    invoke-virtual {p0, v1, v0}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 322
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcnq;->a()V

    throw v0

    .line 315
    :cond_1
    invoke-virtual {p0}, Lcnq;->a()V

    .line 323
    const/4 v0, 0x0

    return-object v0
.end method

.method private final f()V
    .locals 4

    .prologue
    .line 412
    invoke-virtual {p0}, Lcnq;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    new-instance v0, Lcnb;

    iget-object v1, p0, Lcnq;->d:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Folder "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not open."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcnp;Ljava/io/IOException;)Lcnb;
    .locals 3

    .prologue
    .line 415
    invoke-virtual {p1}, Lcnp;->a()V

    .line 416
    iget-object v0, p0, Lcnq;->b:Lcnp;

    if-ne p1, v0, :cond_0

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lcnq;->b:Lcnp;

    .line 418
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcnq;->a(Z)V

    .line 419
    :cond_0
    new-instance v0, Lcnb;

    const/4 v1, 0x1

    const-string v2, "IO Error"

    invoke-direct {v0, v1, v2, p2}, Lcnb;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcnq;->b:Lcnp;

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcnq;->b:Lcnp;

    invoke-virtual {v0}, Lcnp;->b()V

    .line 7
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 9
    if-eqz p1, :cond_0

    .line 10
    :try_start_0
    invoke-direct {p0}, Lcnq;->e()[Lcna;
    :try_end_0
    .catch Lcnb; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    :cond_0
    :goto_0
    monitor-enter p0

    .line 15
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcnq;->b:Lcnp;

    .line 16
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "ImapFolder"

    const-string v2, "Messaging Exception"

    invoke-static {v1, v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 16
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public final a([Lcna;Lcmv;Lcnr;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 67
    .line 68
    :try_start_0
    array-length v0, p1

    if-eqz v0, :cond_8

    .line 69
    invoke-direct {p0}, Lcnq;->f()V

    .line 70
    new-instance v4, Landroid/util/ArrayMap;

    invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V

    .line 71
    array-length v1, p1

    move v0, v2

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v3, p1, v0

    .line 73
    iget-object v5, v3, Lcna;->b:Ljava/lang/String;

    .line 74
    invoke-virtual {v4, v5, v3}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 77
    const-string v1, "UID"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v1, Lcmw;->a:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    const-string v1, "FLAGS"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_1
    sget-object v1, Lcmw;->b:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    const-string v1, "INTERNALDATE"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v1, "RFC822.SIZE"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "BODY.PEEK[HEADER.FIELDS (date subject from content-type to cc message-id content-duration)]"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_2
    sget-object v1, Lcmw;->c:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    const-string v1, "BODYSTRUCTURE"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_3
    sget-object v1, Lcmw;->d:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 87
    sget-object v1, Lcob;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_4
    sget-object v1, Lcmw;->e:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    const-string v1, "BODY.PEEK[]"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_5
    invoke-virtual {p2}, Lcmv;->a()Lcnd;

    move-result-object v5

    .line 91
    if-eqz v5, :cond_6

    .line 92
    const-string v1, "X-Android-Attachment-StoreData"

    invoke-interface {v5, v1}, Lcnd;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 93
    if-eqz v1, :cond_6

    .line 94
    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "BODY.PEEK["

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 95
    :cond_6
    :try_start_1
    iget-object v1, p0, Lcnq;->b:Lcnp;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "UID FETCH %s (%s)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 96
    invoke-static {p1}, Lcnt;->a([Lcna;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    .line 97
    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    const/16 v9, 0x20

    invoke-static {v0, v9}, Lcoq;->a([Ljava/lang/Object;C)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    .line 98
    invoke-static {v3, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 99
    invoke-virtual {v1, v0, v3}, Lcnp;->b(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 100
    :cond_7
    :try_start_2
    iget-object v0, p0, Lcnq;->b:Lcnp;

    invoke-virtual {v0}, Lcnp;->c()Lcoh;

    move-result-object v6

    .line 101
    const/4 v0, 0x1

    const-string v1, "FETCH"

    invoke-virtual {v6, v0, v1}, Lcoh;->a(ILjava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_9

    .line 102
    :try_start_3
    invoke-virtual {p0}, Lcnq;->a()V

    .line 191
    :goto_1
    invoke-virtual {v6}, Lcoh;->f()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v0

    if-eqz v0, :cond_7

    .line 202
    :cond_8
    return-void

    .line 103
    :cond_9
    const/4 v0, 0x2

    :try_start_4
    invoke-virtual {v6, v0}, Lcoh;->b(I)Lcoe;

    move-result-object v7

    .line 104
    const-string v0, "UID"

    .line 105
    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcol;->e()Ljava/lang/String;

    move-result-object v0

    .line 107
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    if-eqz v1, :cond_a

    .line 108
    :try_start_5
    invoke-virtual {p0}, Lcnq;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 193
    :catch_0
    move-exception v0

    .line 194
    :try_start_6
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 195
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 196
    sget-object v2, Lclt;->C:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 197
    iget-object v1, p0, Lcnq;->b:Lcnp;

    invoke-virtual {p0, v1, v0}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1

    .line 199
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 200
    const-string v2, "ImapFolder"

    const-string v3, "Exception detected: "

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1b

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v2, v0}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    throw v1

    .line 109
    :cond_a
    :try_start_7
    invoke-virtual {v4, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnv;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 110
    if-nez v0, :cond_b

    .line 111
    :try_start_8
    invoke-virtual {p0}, Lcnq;->a()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_1

    .line 112
    :cond_b
    :try_start_9
    sget-object v1, Lcmw;->a:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 113
    const-string v1, "FLAGS"

    .line 115
    const/4 v3, 0x0

    invoke-virtual {v7, v1, v3}, Lcoe;->a(Ljava/lang/String;Z)Lcoc;

    move-result-object v1

    .line 116
    if-eqz v1, :cond_d

    check-cast v1, Lcoe;

    move-object v3, v1

    .line 119
    :goto_3
    iget-object v1, v3, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v1, v2

    .line 120
    :goto_4
    if-ge v1, v8, :cond_11

    .line 121
    invoke-virtual {v3, v1}, Lcoe;->c(I)Lcol;

    move-result-object v9

    .line 122
    const-string v10, "\\DELETED"

    invoke-virtual {v9, v10}, Lcol;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 123
    const-string v9, "deleted"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Lcnv;->b(Ljava/lang/String;Z)V

    .line 130
    :cond_c
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 116
    :cond_d
    sget-object v1, Lcoe;->c:Lcoe;

    move-object v3, v1

    goto :goto_3

    .line 124
    :cond_e
    const-string v10, "\\ANSWERED"

    invoke-virtual {v9, v10}, Lcol;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 125
    const-string v9, "answered"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Lcnv;->b(Ljava/lang/String;Z)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 190
    :catchall_0
    move-exception v0

    :try_start_a
    invoke-virtual {p0}, Lcnq;->a()V

    throw v0
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_1

    .line 126
    :cond_f
    :try_start_b
    const-string v10, "\\SEEN"

    invoke-virtual {v9, v10}, Lcol;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 127
    const-string v9, "seen"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Lcnv;->b(Ljava/lang/String;Z)V

    goto :goto_5

    .line 128
    :cond_10
    const-string v10, "\\FLAGGED"

    invoke-virtual {v9, v10}, Lcol;->a(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 129
    const-string v9, "flagged"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Lcnv;->b(Ljava/lang/String;Z)V

    goto :goto_5

    .line 131
    :cond_11
    sget-object v1, Lcmw;->b:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 132
    const-string v1, "INTERNALDATE"

    .line 134
    const/4 v3, 0x0

    invoke-virtual {v7, v1, v3}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lcol;->h()Z

    move-result v3

    if-nez v3, :cond_18

    .line 140
    :goto_6
    const-string v1, "RFC822.SIZE"

    .line 142
    const/4 v3, 0x0

    invoke-virtual {v7, v1, v3}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v1

    .line 144
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcol;->a(I)I

    move-result v1

    .line 146
    const-string v3, "BODY[HEADER"

    const/4 v8, 0x1

    .line 147
    invoke-virtual {v7, v3, v8}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v3

    .line 148
    invoke-virtual {v3}, Lcol;->e()Ljava/lang/String;

    move-result-object v3

    .line 150
    iput v1, v0, Lcnv;->c:I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 151
    :try_start_c
    invoke-static {v3}, Lcoq;->a(Ljava/lang/String;)Ljava/io/ByteArrayInputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcnv;->a(Ljava/io/InputStream;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 155
    :cond_12
    :goto_7
    :try_start_d
    sget-object v1, Lcmw;->c:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 156
    const-string v1, "BODYSTRUCTURE"

    .line 158
    const/4 v3, 0x0

    invoke-virtual {v7, v1, v3}, Lcoe;->a(Ljava/lang/String;Z)Lcoc;

    move-result-object v1

    .line 159
    if-eqz v1, :cond_19

    check-cast v1, Lcoe;

    .line 161
    :goto_8
    invoke-virtual {v1}, Lcoe;->e()Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result v3

    if-nez v3, :cond_13

    .line 162
    :try_start_e
    const-string v3, "TEXT"

    invoke-static {v1, v0, v3}, Lcnq;->a(Lcoe;Lcnd;Ljava/lang/String;)V
    :try_end_e
    .catch Lcnb; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 166
    :cond_13
    :goto_9
    :try_start_f
    sget-object v1, Lcmw;->e:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    sget-object v1, Lcmw;->d:Lcmw;

    invoke-virtual {p2, v1}, Lcmv;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 167
    :cond_14
    const-string v1, "BODY[]"

    const/4 v3, 0x1

    invoke-virtual {v7, v1, v3}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v1

    .line 168
    invoke-virtual {v1}, Lcol;->f()Ljava/io/InputStream;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result-object v1

    .line 169
    :try_start_10
    invoke-virtual {v0, v1}, Lcnv;->a(Ljava/io/InputStream;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 173
    :cond_15
    :goto_a
    if-eqz v5, :cond_16

    .line 174
    :try_start_11
    const-string v1, "BODY["

    const/4 v3, 0x1

    invoke-virtual {v7, v1, v3}, Lcoe;->b(Ljava/lang/String;Z)Lcol;

    move-result-object v1

    invoke-virtual {v1}, Lcol;->f()Ljava/io/InputStream;

    move-result-object v3

    .line 175
    const-string v1, "Content-Transfer-Encoding"

    invoke-interface {v5, v1}, Lcnd;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 176
    if-eqz v1, :cond_1a

    array-length v7, v1

    if-lez v7, :cond_1a

    .line 177
    const/4 v7, 0x0

    aget-object v1, v1, v7
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 181
    :goto_b
    :try_start_12
    invoke-static {v3, v1}, Lcnq;->a(Ljava/io/InputStream;Ljava/lang/String;)Lcms;

    move-result-object v1

    .line 182
    invoke-virtual {v0, v1}, Lcnv;->a(Lcms;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 186
    :cond_16
    :goto_c
    if-eqz p3, :cond_17

    .line 187
    :try_start_13
    invoke-interface {p3, v0}, Lcnr;->a(Lcna;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 188
    :cond_17
    :try_start_14
    invoke-virtual {p0}, Lcnq;->a()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_1

    goto/16 :goto_1

    .line 138
    :cond_18
    :try_start_15
    iget-object v1, v1, Lcol;->e:Ljava/util/Date;

    goto/16 :goto_6

    .line 153
    :catch_2
    move-exception v1

    .line 154
    const-string v3, "ImapFolder"

    const-string v8, "Error parsing header %s"

    invoke-static {v3, v8, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 159
    :cond_19
    sget-object v1, Lcoe;->c:Lcoe;

    goto :goto_8

    .line 165
    :catch_3
    move-exception v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcnv;->a(Lcms;)V

    goto :goto_9

    .line 171
    :catch_4
    move-exception v1

    .line 172
    const-string v3, "ImapFolder"

    const-string v8, "Error parsing body %s"

    invoke-static {v3, v8, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 178
    :cond_1a
    const-string v1, "7bit"

    goto :goto_b

    .line 184
    :catch_5
    move-exception v1

    .line 185
    const-string v3, "ImapFolder"

    const-string v7, "Error fetching body %s"

    invoke-static {v3, v7, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_c

    .line 200
    :cond_1b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public final a([Lcna;[Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 324
    invoke-direct {p0}, Lcnq;->f()V

    .line 325
    const-string v0, ""

    .line 326
    array-length v2, p2

    if-lez v2, :cond_6

    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 328
    array-length v3, p2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_4

    .line 329
    aget-object v1, p2, v0

    .line 330
    const-string v4, "seen"

    if-ne v1, v4, :cond_1

    .line 331
    const-string v1, " \\SEEN"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 332
    :cond_1
    const-string v4, "deleted"

    if-ne v1, v4, :cond_2

    .line 333
    const-string v1, " \\DELETED"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 334
    :cond_2
    const-string v4, "flagged"

    if-ne v1, v4, :cond_3

    .line 335
    const-string v1, " \\FLAGGED"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 336
    :cond_3
    const-string v4, "answered"

    if-ne v1, v4, :cond_0

    .line 337
    const-string v1, " \\ANSWERED"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 339
    :cond_4
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 340
    :goto_2
    :try_start_0
    iget-object v2, p0, Lcnq;->b:Lcnp;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "UID STORE %s %sFLAGS.SILENT (%s)"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 341
    invoke-static {p1}, Lcnt;->a([Lcna;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v6, 0x1

    .line 342
    if-eqz p3, :cond_5

    const-string v0, "+"

    :goto_3
    aput-object v0, v5, v6

    const/4 v0, 0x2

    aput-object v1, v5, v0

    .line 343
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 345
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    invoke-virtual {p0}, Lcnq;->a()V

    .line 348
    return-void

    .line 342
    :cond_5
    :try_start_1
    const-string v0, "-"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 349
    :catch_0
    move-exception v0

    .line 350
    :try_start_2
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 351
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 352
    sget-object v2, Lclt;->C:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 353
    iget-object v1, p0, Lcnq;->b:Lcnp;

    invoke-virtual {p0, v1, v0}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcnq;->a()V

    throw v0

    :cond_6
    move-object v1, v0

    goto :goto_2
.end method

.method public final a([Ljava/lang/String;)[Lcna;
    .locals 4

    .prologue
    .line 60
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 61
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 62
    aget-object v2, p1, v0

    .line 63
    new-instance v3, Lcnv;

    invoke-direct {v3, v2, p0}, Lcnv;-><init>(Ljava/lang/String;Lcnq;)V

    .line 64
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_0
    sget-object v0, Lcna;->a:[Lcna;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcna;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Lcnq;->f()V

    .line 32
    :try_start_0
    const-string v0, "UID SEARCH "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    :goto_0
    iget-object v1, p0, Lcnq;->b:Lcnp;

    .line 34
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 35
    invoke-static {v0}, Lcnq;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    .line 36
    array-length v1, v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "searchForUids \'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' results: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcnu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    invoke-virtual {p0}, Lcnq;->a()V

    .line 44
    :goto_1
    return-object v0

    .line 32
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lcnu; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 41
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "ImapException in search: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 42
    :goto_2
    sget-object v0, Lcoq;->b:[Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 43
    invoke-virtual {p0}, Lcnq;->a()V

    goto :goto_1

    .line 41
    :cond_1
    :try_start_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 51
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcnq;->a()V

    throw v0

    .line 45
    :catch_1
    move-exception v0

    .line 46
    :try_start_4
    const-string v1, "IOException in search: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    :goto_3
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 48
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 49
    sget-object v2, Lclt;->C:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 50
    iget-object v1, p0, Lcnq;->b:Lcnp;

    invoke-virtual {p0, v1, v0}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v0

    throw v0

    .line 46
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)Lcna;
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Lcnq;->f()V

    .line 53
    const-string v0, "UID "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcnq;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 54
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 55
    aget-object v2, v1, v0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    new-instance v0, Lcnv;

    invoke-direct {v0, p1, p0}, Lcnv;-><init>(Ljava/lang/String;Lcnq;)V

    .line 59
    :goto_2
    return-object v0

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58
    :cond_2
    const-string v0, "ImapFolder"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "UID "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found on server"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 8
    iget-boolean v0, p0, Lcnq;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnq;->b:Lcnp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 355
    iget-object v0, p0, Lcnq;->b:Lcnp;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "SELECT \"%s\""

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcnq;->d:Ljava/lang/String;

    aput-object v5, v4, v6

    .line 356
    invoke-static {v1, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-virtual {v0, v1, v6}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 361
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    .line 362
    const-string v4, "EXISTS"

    invoke-virtual {v0, v7, v4}, Lcoh;->a(ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 363
    invoke-virtual {v0, v6}, Lcoh;->c(I)Lcol;

    move-result-object v0

    .line 364
    invoke-virtual {v0, v6}, Lcol;->a(I)I

    move-result v0

    move v1, v0

    .line 365
    goto :goto_0

    .line 367
    :cond_1
    const-string v4, "OK"

    .line 368
    invoke-virtual {v0, v6, v4, v6}, Lcoe;->a(ILjava/lang/String;Z)Z

    move-result v4

    .line 369
    if-eqz v4, :cond_2

    .line 370
    invoke-virtual {v0}, Lcoh;->h()Lcol;

    move-result-object v0

    .line 371
    const-string v4, "READ-ONLY"

    invoke-virtual {v0, v4}, Lcol;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 372
    const-string v4, "READ-WRITE"

    invoke-virtual {v0, v4}, Lcol;->a(Ljava/lang/String;)Z

    goto :goto_0

    .line 373
    :cond_2
    invoke-virtual {v0}, Lcoh;->f()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 374
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 375
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 376
    sget-object v2, Lclt;->B:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 377
    new-instance v1, Lcnb;

    .line 378
    invoke-virtual {v0}, Lcoh;->i()Lcol;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Can\'t open mailbox: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v1

    .line 380
    :cond_3
    if-ne v1, v2, :cond_4

    .line 381
    new-instance v0, Lcnb;

    const-string v1, "Did not find message count during select"

    invoke-direct {v0, v1}, Lcnb;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_4
    iput-boolean v7, p0, Lcnq;->c:Z

    .line 383
    return-void
.end method

.method public final d()Lcns;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 384
    :try_start_0
    iget-object v0, p0, Lcnq;->b:Lcnp;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "GETQUOTAROOT \"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcnq;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 385
    invoke-static {v1, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 387
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcnp;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 389
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoh;

    .line 390
    const/4 v1, 0x0

    const-string v4, "QUOTA"

    invoke-virtual {v0, v1, v4}, Lcoh;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 391
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcoh;->b(I)Lcoe;

    move-result-object v4

    move v1, v2

    .line 393
    :goto_0
    iget-object v0, v4, Lcoe;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 394
    if-ge v1, v0, :cond_0

    .line 395
    invoke-virtual {v4, v1}, Lcoe;->c(I)Lcol;

    move-result-object v0

    const-string v5, "voice"

    invoke-virtual {v0, v5}, Lcol;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    new-instance v0, Lcns;

    add-int/lit8 v2, v1, 0x1

    .line 397
    invoke-virtual {v4, v2}, Lcoe;->c(I)Lcol;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcol;->a(I)I

    move-result v2

    add-int/lit8 v1, v1, 0x2

    .line 398
    invoke-virtual {v4, v1}, Lcoe;->c(I)Lcol;

    move-result-object v1

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Lcol;->a(I)I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcns;-><init>(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    invoke-virtual {p0}, Lcnq;->a()V

    .line 411
    :goto_1
    return-object v0

    .line 401
    :cond_1
    add-int/lit8 v0, v1, 0x3

    move v1, v0

    goto :goto_0

    .line 403
    :cond_2
    invoke-virtual {p0}, Lcnq;->a()V

    .line 411
    const/4 v0, 0x0

    goto :goto_1

    .line 405
    :catch_0
    move-exception v0

    .line 406
    :try_start_1
    iget-object v1, p0, Lcnq;->a:Lcnt;

    .line 407
    iget-object v1, v1, Lcnt;->a:Lcmi;

    .line 408
    sget-object v2, Lclt;->C:Lclt;

    invoke-virtual {v1, v2}, Lcmi;->a(Lclt;)V

    .line 409
    iget-object v1, p0, Lcnq;->b:Lcnp;

    invoke-virtual {p0, v1, v0}, Lcnq;->a(Lcnp;Ljava/io/IOException;)Lcnb;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 410
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcnq;->a()V

    throw v0
.end method
