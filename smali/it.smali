.class public Lit;
.super Lin;
.source "PG"

# interfaces
.implements Lif;
.implements Lih;


# instance fields
.field public final b:Landroid/os/Handler;

.field public final c:Liy;

.field public d:Z

.field public e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Lpw;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Lin;-><init>()V

    .line 2
    new-instance v0, Liu;

    invoke-direct {v0, p0}, Liu;-><init>(Lit;)V

    iput-object v0, p0, Lit;->b:Landroid/os/Handler;

    .line 3
    new-instance v0, Liv;

    invoke-direct {v0, p0}, Liv;-><init>(Lit;)V

    .line 4
    new-instance v1, Liy;

    invoke-direct {v1, v0}, Liy;-><init>(Liz;)V

    .line 5
    iput-object v1, p0, Lit;->c:Liy;

    .line 6
    iput-boolean v2, p0, Lit;->d:Z

    .line 7
    iput-boolean v2, p0, Lit;->h:Z

    .line 8
    return-void
.end method

.method private static a(Lja;Lj;)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 390
    .line 391
    invoke-virtual {p0}, Lja;->d()Ljava/util/List;

    move-result-object v0

    .line 392
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lip;

    .line 393
    if-eqz v0, :cond_0

    .line 395
    iget-object v4, v0, Lip;->V:Ll;

    .line 396
    invoke-virtual {v4}, Lh;->a()Lj;

    move-result-object v4

    sget-object v6, Lj;->d:Lj;

    .line 397
    invoke-virtual {v4, v6}, Lj;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-ltz v4, :cond_2

    move v4, v3

    .line 398
    :goto_1
    if-eqz v4, :cond_1

    .line 399
    iget-object v1, v0, Lip;->V:Ll;

    .line 400
    iput-object p1, v1, Ll;->a:Lj;

    move v1, v3

    .line 403
    :cond_1
    iget-object v0, v0, Lip;->u:Ljc;

    .line 405
    if-eqz v0, :cond_4

    .line 406
    invoke-static {v0, p1}, Lit;->a(Lja;Lj;)Z

    move-result v0

    or-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 407
    goto :goto_0

    :cond_2
    move v4, v2

    .line 397
    goto :goto_1

    .line 408
    :cond_3
    return v1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private final e()V
    .locals 2

    .prologue
    .line 387
    :cond_0
    invoke-virtual {p0}, Lit;->c()Lja;

    move-result-object v0

    sget-object v1, Lj;->c:Lj;

    invoke-static {v0, v1}, Lit;->a(Lja;Lj;)Z

    move-result v0

    .line 388
    if-nez v0, :cond_0

    .line 389
    return-void
.end method


# virtual methods
.method final a(Lip;)I
    .locals 4

    .prologue
    const v3, 0xfffe

    .line 379
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 380
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Too many pending Fragment activity results."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_0
    :goto_0
    iget-object v0, p0, Lit;->k:Lpw;

    iget v1, p0, Lit;->j:I

    invoke-virtual {v0, v1}, Lpw;->e(I)I

    move-result v0

    if-ltz v0, :cond_1

    .line 382
    iget v0, p0, Lit;->j:I

    add-int/lit8 v0, v0, 0x1

    rem-int/2addr v0, v3

    iput v0, p0, Lit;->j:I

    goto :goto_0

    .line 383
    :cond_1
    iget v0, p0, Lit;->j:I

    .line 384
    iget-object v1, p0, Lit;->k:Lpw;

    iget-object v2, p1, Lip;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lpw;->a(ILjava/lang/Object;)V

    .line 385
    iget v1, p0, Lit;->j:I

    add-int/lit8 v1, v1, 0x1

    rem-int/2addr v1, v3

    iput v1, p0, Lit;->j:I

    .line 386
    return v0
.end method

.method final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lit;->c:Liy;

    .line 103
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljc;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method final a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 309
    iget-boolean v0, p0, Lit;->h:Z

    if-nez v0, :cond_1

    .line 310
    iput-boolean v4, p0, Lit;->h:Z

    .line 311
    iput-boolean p1, p0, Lit;->i:Z

    .line 312
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 314
    iget-object v0, p0, Lit;->c:Liy;

    iget-boolean v1, p0, Lit;->i:Z

    invoke-virtual {v0, v1}, Liy;->a(Z)V

    .line 315
    iget-object v0, p0, Lit;->c:Liy;

    .line 316
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    .line 317
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljc;->b(I)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    if-eqz p1, :cond_0

    .line 320
    iget-object v0, p0, Lit;->c:Liy;

    .line 321
    iget-object v0, v0, Liy;->a:Liz;

    .line 322
    iget-boolean v1, v0, Liz;->i:Z

    if-nez v1, :cond_3

    .line 323
    iput-boolean v4, v0, Liz;->i:Z

    .line 324
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_4

    .line 325
    iget-object v1, v0, Liz;->g:Lkz;

    invoke-virtual {v1}, Lkz;->b()V

    .line 330
    :cond_2
    :goto_1
    iput-boolean v4, v0, Liz;->h:Z

    .line 331
    :cond_3
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0, v4}, Liy;->a(Z)V

    goto :goto_0

    .line 326
    :cond_4
    iget-boolean v1, v0, Liz;->h:Z

    if-nez v1, :cond_2

    .line 327
    const-string v1, "(root)"

    iget-boolean v2, v0, Liz;->i:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v1

    iput-object v1, v0, Liz;->g:Lkz;

    .line 328
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_2

    iget-object v1, v0, Liz;->g:Lkz;

    iget-boolean v1, v1, Lkz;->e:Z

    if-nez v1, :cond_2

    .line 329
    iget-object v1, v0, Liz;->g:Lkz;

    invoke-virtual {v1}, Lkz;->b()V

    goto :goto_1
.end method

.method public final c()Lja;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lit;->c:Liy;

    .line 334
    iget-object v0, v0, Liy;->a:Liz;

    .line 335
    iget-object v0, v0, Liz;->d:Ljc;

    .line 336
    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 350
    iget-boolean v0, p0, Lit;->e:Z

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 351
    invoke-static {p1}, Lit;->d(I)V

    .line 352
    :cond_0
    return-void
.end method

.method public final d()Lkx;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 337
    iget-object v0, p0, Lit;->c:Liy;

    .line 338
    iget-object v0, v0, Liy;->a:Liz;

    .line 339
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_0

    .line 340
    iget-object v0, v0, Liz;->g:Lkz;

    .line 344
    :goto_0
    return-object v0

    .line 341
    :cond_0
    iput-boolean v3, v0, Liz;->h:Z

    .line 342
    const-string v1, "(root)"

    iget-boolean v2, v0, Liz;->i:Z

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v1

    iput-object v1, v0, Liz;->g:Lkz;

    .line 343
    iget-object v0, v0, Liz;->g:Lkz;

    goto :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 285
    invoke-super {p0, p1, p2, p3, p4}, Lin;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 286
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Local FragmentActivity "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 287
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 288
    const-string v0, " State:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "mCreated="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 291
    iget-boolean v1, p0, Lit;->f:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, "mResumed="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 292
    iget-boolean v1, p0, Lit;->g:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 293
    iget-boolean v1, p0, Lit;->d:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mReallyStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 294
    iget-boolean v1, p0, Lit;->h:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 295
    iget-object v1, p0, Lit;->c:Liy;

    .line 296
    iget-object v1, v1, Liy;->a:Liz;

    .line 297
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mLoadersStarted="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 298
    iget-boolean v2, v1, Liz;->i:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Z)V

    .line 299
    iget-object v2, v1, Liz;->g:Lkz;

    if-eqz v2, :cond_0

    .line 300
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Loader Manager "

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 301
    iget-object v2, v1, Liz;->g:Lkz;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 302
    const-string v2, ":"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 303
    iget-object v1, v1, Liz;->g:Lkz;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2, p3, p4}, Lkz;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 304
    :cond_0
    iget-object v0, p0, Lit;->c:Liy;

    .line 305
    iget-object v0, v0, Liy;->a:Liz;

    .line 306
    iget-object v0, v0, Liz;->d:Ljc;

    .line 307
    invoke-virtual {v0, p1, p2, p3, p4}, Lja;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method public e_()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 283
    invoke-virtual {p0}, Lit;->invalidateOptionsMenu()V

    .line 284
    return-void
.end method

.method public f_()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lit;->c:Liy;

    .line 158
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->o()V

    .line 159
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 9
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->a()V

    .line 10
    shr-int/lit8 v0, p1, 0x10

    .line 11
    if-eqz v0, :cond_6

    .line 12
    add-int/lit8 v1, v0, -0x1

    .line 13
    iget-object v0, p0, Lit;->k:Lpw;

    .line 15
    iget-object v2, v0, Lpw;->c:[I

    iget v3, v0, Lpw;->e:I

    invoke-static {v2, v3, v1}, Lph;->a([III)I

    move-result v2

    .line 16
    if-ltz v2, :cond_0

    iget-object v3, v0, Lpw;->d:[Ljava/lang/Object;

    aget-object v3, v3, v2

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    if-ne v3, v4, :cond_3

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    check-cast v0, Ljava/lang/String;

    .line 20
    iget-object v2, p0, Lit;->k:Lpw;

    .line 22
    iget-object v3, v2, Lpw;->c:[I

    iget v4, v2, Lpw;->e:I

    invoke-static {v3, v4, v1}, Lph;->a([III)I

    move-result v1

    .line 23
    if-ltz v1, :cond_1

    .line 24
    iget-object v3, v2, Lpw;->d:[Ljava/lang/Object;

    aget-object v3, v3, v1

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    if-eq v3, v4, :cond_1

    .line 25
    iget-object v3, v2, Lpw;->d:[Ljava/lang/Object;

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    aput-object v4, v3, v1

    .line 26
    const/4 v1, 0x1

    iput-boolean v1, v2, Lpw;->b:Z

    .line 27
    :cond_1
    if-nez v0, :cond_4

    .line 28
    const-string v0, "FragmentActivity"

    const-string v1, "Activity result delivered for unknown Fragment."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    :cond_2
    :goto_1
    return-void

    .line 18
    :cond_3
    iget-object v0, v0, Lpw;->d:[Ljava/lang/Object;

    aget-object v0, v0, v2

    goto :goto_0

    .line 30
    :cond_4
    iget-object v1, p0, Lit;->c:Liy;

    invoke-virtual {v1, v0}, Liy;->a(Ljava/lang/String;)Lip;

    move-result-object v1

    .line 31
    if-nez v1, :cond_5

    .line 32
    const-string v1, "FragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity result no fragment exists for who: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 33
    :cond_5
    const v0, 0xffff

    and-int/2addr v0, p1

    invoke-virtual {v1, v0, p2, p3}, Lip;->a(IILandroid/content/Intent;)V

    goto :goto_1

    .line 35
    :cond_6
    invoke-static {}, Lid;->a()Lig;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_7

    invoke-interface {v0}, Lig;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 38
    :cond_7
    invoke-super {p0, p1, p2, p3}, Lin;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lit;->c:Liy;

    .line 41
    iget-object v0, v0, Liy;->a:Liz;

    .line 42
    iget-object v0, v0, Liz;->d:Ljc;

    .line 44
    invoke-virtual {v0}, Lja;->f()Z

    move-result v1

    .line 45
    if-eqz v1, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x19

    if-gt v2, v3, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    if-nez v1, :cond_2

    invoke-virtual {v0}, Lja;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    :cond_2
    invoke-super {p0}, Lin;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lin;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 57
    iget-object v0, p0, Lit;->c:Liy;

    .line 58
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Landroid/content/res/Configuration;)V

    .line 59
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lit;->c:Liy;

    .line 61
    iget-object v1, v0, Liy;->a:Liz;

    iget-object v1, v1, Liz;->d:Ljc;

    iget-object v4, v0, Liy;->a:Liz;

    iget-object v0, v0, Liy;->a:Liz;

    invoke-virtual {v1, v4, v0, v2}, Ljc;->a(Liz;Lix;Lip;)V

    .line 62
    invoke-super {p0, p1}, Lin;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lit;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liw;

    .line 65
    if-eqz v0, :cond_1

    .line 66
    iget-object v1, p0, Lit;->c:Liy;

    iget-object v5, v0, Liw;->b:Lpv;

    .line 67
    iget-object v6, v1, Liy;->a:Liz;

    .line 68
    if-eqz v5, :cond_0

    .line 69
    invoke-virtual {v5}, Lpv;->size()I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    .line 70
    invoke-virtual {v5, v4}, Lpv;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkz;

    .line 71
    iput-object v6, v1, Lkz;->g:Liz;

    .line 72
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 73
    :cond_0
    iput-object v5, v6, Liz;->e:Lpv;

    .line 74
    :cond_1
    if-eqz p1, :cond_3

    .line 75
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 76
    iget-object v4, p0, Lit;->c:Liy;

    if-eqz v0, :cond_5

    iget-object v0, v0, Liw;->a:Ljr;

    .line 77
    :goto_1
    iget-object v2, v4, Liy;->a:Liz;

    iget-object v2, v2, Liz;->d:Ljc;

    invoke-virtual {v2, v1, v0}, Ljc;->a(Landroid/os/Parcelable;Ljr;)V

    .line 78
    const-string v0, "android:support:next_request_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 79
    const-string v0, "android:support:next_request_index"

    .line 80
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lit;->j:I

    .line 81
    const-string v0, "android:support:request_indicies"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 82
    const-string v0, "android:support:request_fragment_who"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 83
    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    array-length v0, v1

    array-length v4, v2

    if-eq v0, v4, :cond_6

    .line 84
    :cond_2
    const-string v0, "FragmentActivity"

    const-string v1, "Invalid requestCode mapping in savedInstanceState."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_3
    iget-object v0, p0, Lit;->k:Lpw;

    if-nez v0, :cond_4

    .line 90
    new-instance v0, Lpw;

    invoke-direct {v0}, Lpw;-><init>()V

    iput-object v0, p0, Lit;->k:Lpw;

    .line 91
    iput v3, p0, Lit;->j:I

    .line 92
    :cond_4
    iget-object v0, p0, Lit;->c:Liy;

    .line 93
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->l()V

    .line 94
    return-void

    :cond_5
    move-object v0, v2

    .line 76
    goto :goto_1

    .line 85
    :cond_6
    new-instance v0, Lpw;

    array-length v4, v1

    invoke-direct {v0, v4}, Lpw;-><init>(I)V

    iput-object v0, p0, Lit;->k:Lpw;

    move v0, v3

    .line 86
    :goto_2
    array-length v4, v1

    if-ge v0, v4, :cond_3

    .line 87
    iget-object v4, p0, Lit;->k:Lpw;

    aget v5, v1, v0

    aget-object v6, v2, v0

    invoke-virtual {v4, v5, v6}, Lpw;->a(ILjava/lang/Object;)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 95
    if-nez p1, :cond_0

    .line 96
    invoke-super {p0, p1, p2}, Lin;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    .line 97
    iget-object v1, p0, Lit;->c:Liy;

    invoke-virtual {p0}, Lit;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 98
    iget-object v1, v1, Liy;->a:Liz;

    iget-object v1, v1, Liz;->d:Ljc;

    invoke-virtual {v1, p2, v2}, Ljc;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    .line 99
    or-int/2addr v0, v1

    .line 101
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lin;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 413
    invoke-super {p0, p1, p2, p3, p4}, Lin;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 412
    invoke-super {p0, p1, p2, p3}, Lin;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Lin;->onDestroy()V

    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lit;->a(Z)V

    .line 107
    iget-object v0, p0, Lit;->c:Liy;

    .line 108
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->q()V

    .line 109
    iget-object v0, p0, Lit;->c:Liy;

    .line 110
    iget-object v0, v0, Liy;->a:Liz;

    .line 111
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_0

    .line 112
    iget-object v0, v0, Liz;->g:Lkz;

    invoke-virtual {v0}, Lkz;->g()V

    .line 113
    :cond_0
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lin;->onLowMemory()V

    .line 115
    iget-object v0, p0, Lit;->c:Liy;

    .line 116
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->r()V

    .line 117
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1, p2}, Lin;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    .line 120
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 127
    const/4 v0, 0x0

    goto :goto_0

    .line 121
    :sswitch_0
    iget-object v0, p0, Lit;->c:Liy;

    .line 122
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p2}, Ljc;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 124
    :sswitch_1
    iget-object v0, p0, Lit;->c:Liy;

    .line 125
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p2}, Ljc;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lit;->c:Liy;

    .line 51
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p1}, Ljc;->a(Z)V

    .line 52
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0, p1}, Lin;->onNewIntent(Landroid/content/Intent;)V

    .line 143
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->a()V

    .line 144
    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 128
    packed-switch p1, :pswitch_data_0

    .line 131
    :goto_0
    invoke-super {p0, p1, p2}, Lin;->onPanelClosed(ILandroid/view/Menu;)V

    .line 132
    return-void

    .line 129
    :pswitch_0
    iget-object v0, p0, Lit;->c:Liy;

    .line 130
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p2}, Ljc;->b(Landroid/view/Menu;)V

    goto :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 133
    invoke-super {p0}, Lin;->onPause()V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lit;->g:Z

    .line 135
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 137
    invoke-virtual {p0}, Lit;->f_()V

    .line 138
    :cond_0
    iget-object v0, p0, Lit;->c:Liy;

    .line 139
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    .line 140
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljc;->b(I)V

    .line 141
    return-void
.end method

.method public onPictureInPictureModeChanged(Z)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lit;->c:Liy;

    .line 54
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0, p1}, Ljc;->b(Z)V

    .line 55
    return-void
.end method

.method public onPostResume()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Lin;->onPostResume()V

    .line 153
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 154
    invoke-virtual {p0}, Lit;->f_()V

    .line 155
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->b()Z

    .line 156
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 160
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    .line 162
    const/4 v0, 0x0

    invoke-super {p0, v0, p2, p3}, Lin;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 164
    iget-object v1, p0, Lit;->c:Liy;

    .line 165
    iget-object v1, v1, Liy;->a:Liz;

    iget-object v1, v1, Liz;->d:Ljc;

    invoke-virtual {v1, p3}, Ljc;->a(Landroid/view/Menu;)Z

    move-result v1

    .line 166
    or-int/2addr v0, v1

    .line 168
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lin;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 6

    .prologue
    const v5, 0xffff

    .line 353
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->a()V

    .line 354
    shr-int/lit8 v0, p1, 0x10

    and-int/2addr v0, v5

    .line 355
    if-eqz v0, :cond_2

    .line 356
    add-int/lit8 v1, v0, -0x1

    .line 357
    iget-object v0, p0, Lit;->k:Lpw;

    .line 359
    iget-object v2, v0, Lpw;->c:[I

    iget v3, v0, Lpw;->e:I

    invoke-static {v2, v3, v1}, Lph;->a([III)I

    move-result v2

    .line 360
    if-ltz v2, :cond_0

    iget-object v3, v0, Lpw;->d:[Ljava/lang/Object;

    aget-object v3, v3, v2

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    if-ne v3, v4, :cond_3

    .line 361
    :cond_0
    const/4 v0, 0x0

    .line 363
    :goto_0
    check-cast v0, Ljava/lang/String;

    .line 364
    iget-object v2, p0, Lit;->k:Lpw;

    .line 366
    iget-object v3, v2, Lpw;->c:[I

    iget v4, v2, Lpw;->e:I

    invoke-static {v3, v4, v1}, Lph;->a([III)I

    move-result v1

    .line 367
    if-ltz v1, :cond_1

    .line 368
    iget-object v3, v2, Lpw;->d:[Ljava/lang/Object;

    aget-object v3, v3, v1

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    if-eq v3, v4, :cond_1

    .line 369
    iget-object v3, v2, Lpw;->d:[Ljava/lang/Object;

    sget-object v4, Lpw;->a:Ljava/lang/Object;

    aput-object v4, v3, v1

    .line 370
    const/4 v1, 0x1

    iput-boolean v1, v2, Lpw;->b:Z

    .line 371
    :cond_1
    if-nez v0, :cond_4

    .line 372
    const-string v0, "FragmentActivity"

    const-string v1, "Activity result delivered for unknown Fragment."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_2
    :goto_1
    return-void

    .line 362
    :cond_3
    iget-object v0, v0, Lpw;->d:[Ljava/lang/Object;

    aget-object v0, v0, v2

    goto :goto_0

    .line 374
    :cond_4
    iget-object v1, p0, Lit;->c:Liy;

    invoke-virtual {v1, v0}, Liy;->a(Ljava/lang/String;)Lip;

    move-result-object v1

    .line 375
    if-nez v1, :cond_5

    .line 376
    const-string v1, "FragmentActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity result no fragment exists for who: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 377
    :cond_5
    and-int v0, p1, v5

    invoke-virtual {v1, v0, p2, p3}, Lip;->a(I[Ljava/lang/String;[I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 147
    invoke-super {p0}, Lin;->onResume()V

    .line 148
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lit;->g:Z

    .line 150
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->b()Z

    .line 151
    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 169
    iget-boolean v0, p0, Lit;->d:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0, v1}, Lit;->a(Z)V

    .line 171
    :cond_0
    iget-object v0, p0, Lit;->c:Liy;

    .line 172
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    .line 173
    iget-object v4, v0, Ljc;->k:Ljr;

    invoke-static {v4}, Ljc;->a(Ljr;)V

    .line 174
    iget-object v5, v0, Ljc;->k:Ljr;

    .line 176
    iget-object v0, p0, Lit;->c:Liy;

    .line 177
    iget-object v6, v0, Liy;->a:Liz;

    .line 179
    iget-object v0, v6, Liz;->e:Lpv;

    if-eqz v0, :cond_5

    .line 180
    iget-object v0, v6, Liz;->e:Lpv;

    invoke-virtual {v0}, Lpv;->size()I

    move-result v7

    .line 181
    new-array v8, v7, [Lkz;

    .line 182
    add-int/lit8 v0, v7, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_1

    .line 183
    iget-object v0, v6, Liz;->e:Lpv;

    invoke-virtual {v0, v4}, Lpv;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkz;

    aput-object v0, v8, v4

    .line 184
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    .line 186
    :cond_1
    iget-boolean v4, v6, Liz;->f:Z

    move v0, v3

    .line 188
    :goto_1
    if-ge v3, v7, :cond_6

    .line 189
    aget-object v9, v8, v3

    .line 190
    iget-boolean v10, v9, Lkz;->f:Z

    if-nez v10, :cond_3

    if-eqz v4, :cond_3

    .line 191
    iget-boolean v10, v9, Lkz;->e:Z

    if-nez v10, :cond_2

    .line 192
    invoke-virtual {v9}, Lkz;->b()V

    .line 193
    :cond_2
    invoke-virtual {v9}, Lkz;->d()V

    .line 194
    :cond_3
    iget-boolean v10, v9, Lkz;->f:Z

    if-eqz v10, :cond_4

    move v0, v1

    .line 198
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 196
    :cond_4
    invoke-virtual {v9}, Lkz;->g()V

    .line 197
    iget-object v10, v6, Liz;->e:Lpv;

    iget-object v9, v9, Lkz;->d:Ljava/lang/String;

    invoke-virtual {v10, v9}, Lpv;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    move v0, v3

    .line 199
    :cond_6
    if-eqz v0, :cond_7

    .line 200
    iget-object v0, v6, Liz;->e:Lpv;

    move-object v1, v0

    .line 203
    :goto_3
    if-nez v5, :cond_8

    if-nez v1, :cond_8

    move-object v0, v2

    .line 208
    :goto_4
    return-object v0

    :cond_7
    move-object v1, v2

    .line 201
    goto :goto_3

    .line 205
    :cond_8
    new-instance v0, Liw;

    invoke-direct {v0}, Liw;-><init>()V

    .line 206
    iput-object v5, v0, Liw;->a:Ljr;

    .line 207
    iput-object v1, v0, Liw;->b:Lpv;

    goto :goto_4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 209
    invoke-super {p0, p1}, Lin;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 210
    invoke-direct {p0}, Lit;->e()V

    .line 211
    iget-object v0, p0, Lit;->c:Liy;

    .line 212
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->j()Landroid/os/Parcelable;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_0

    .line 215
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 216
    :cond_0
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    if-lez v0, :cond_2

    .line 217
    const-string v0, "android:support:next_request_index"

    iget v1, p0, Lit;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    new-array v2, v0, [I

    .line 219
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 220
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 221
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0, v1}, Lpw;->c(I)I

    move-result v0

    aput v0, v2, v1

    .line 222
    iget-object v0, p0, Lit;->k:Lpw;

    invoke-virtual {v0, v1}, Lpw;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 223
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 224
    :cond_1
    const-string v0, "android:support:request_indicies"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 225
    const-string v0, "android:support:request_fragment_who"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 226
    :cond_2
    return-void
.end method

.method public onStart()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 227
    invoke-super {p0}, Lin;->onStart()V

    .line 228
    iput-boolean v3, p0, Lit;->d:Z

    .line 229
    iput-boolean v3, p0, Lit;->h:Z

    .line 230
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 231
    iget-boolean v0, p0, Lit;->f:Z

    if-nez v0, :cond_0

    .line 232
    iput-boolean v4, p0, Lit;->f:Z

    .line 233
    iget-object v0, p0, Lit;->c:Liy;

    .line 234
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->m()V

    .line 235
    :cond_0
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->a()V

    .line 236
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->b()Z

    .line 237
    iget-object v0, p0, Lit;->c:Liy;

    .line 238
    iget-object v0, v0, Liy;->a:Liz;

    .line 239
    iget-boolean v1, v0, Liz;->i:Z

    if-nez v1, :cond_2

    .line 240
    iput-boolean v4, v0, Liz;->i:Z

    .line 241
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_3

    .line 242
    iget-object v1, v0, Liz;->g:Lkz;

    invoke-virtual {v1}, Lkz;->b()V

    .line 247
    :cond_1
    :goto_0
    iput-boolean v4, v0, Liz;->h:Z

    .line 248
    :cond_2
    iget-object v0, p0, Lit;->c:Liy;

    .line 249
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->n()V

    .line 250
    iget-object v0, p0, Lit;->c:Liy;

    .line 251
    iget-object v2, v0, Liy;->a:Liz;

    .line 252
    iget-object v0, v2, Liz;->e:Lpv;

    if-eqz v0, :cond_8

    .line 253
    iget-object v0, v2, Liz;->e:Lpv;

    invoke-virtual {v0}, Lpv;->size()I

    move-result v4

    .line 254
    new-array v5, v4, [Lkz;

    .line 255
    add-int/lit8 v0, v4, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_4

    .line 256
    iget-object v0, v2, Liz;->e:Lpv;

    invoke-virtual {v0, v1}, Lpv;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkz;

    aput-object v0, v5, v1

    .line 257
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 243
    :cond_3
    iget-boolean v1, v0, Liz;->h:Z

    if-nez v1, :cond_1

    .line 244
    const-string v1, "(root)"

    iget-boolean v2, v0, Liz;->i:Z

    invoke-virtual {v0, v1, v2, v3}, Liz;->a(Ljava/lang/String;ZZ)Lkz;

    move-result-object v1

    iput-object v1, v0, Liz;->g:Lkz;

    .line 245
    iget-object v1, v0, Liz;->g:Lkz;

    if-eqz v1, :cond_1

    iget-object v1, v0, Liz;->g:Lkz;

    iget-boolean v1, v1, Lkz;->e:Z

    if-nez v1, :cond_1

    .line 246
    iget-object v1, v0, Liz;->g:Lkz;

    invoke-virtual {v1}, Lkz;->b()V

    goto :goto_0

    :cond_4
    move v2, v3

    .line 258
    :goto_2
    if-ge v2, v4, :cond_8

    .line 259
    aget-object v6, v5, v2

    .line 261
    iget-boolean v0, v6, Lkz;->f:Z

    if-eqz v0, :cond_7

    .line 262
    iput-boolean v3, v6, Lkz;->f:Z

    .line 263
    iget-object v0, v6, Lkz;->b:Lpw;

    invoke-virtual {v0}, Lpw;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_7

    .line 264
    iget-object v0, v6, Lkz;->b:Lpw;

    invoke-virtual {v0, v1}, Lpw;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lla;

    .line 265
    iget-boolean v7, v0, Lla;->h:Z

    if-eqz v7, :cond_5

    .line 266
    iput-boolean v3, v0, Lla;->h:Z

    .line 267
    iget-boolean v7, v0, Lla;->g:Z

    iget-boolean v8, v0, Lla;->i:Z

    if-eq v7, v8, :cond_5

    .line 268
    iget-boolean v7, v0, Lla;->g:Z

    if-nez v7, :cond_5

    .line 269
    invoke-virtual {v0}, Lla;->b()V

    .line 270
    :cond_5
    iget-boolean v7, v0, Lla;->g:Z

    if-eqz v7, :cond_6

    iget-boolean v7, v0, Lla;->d:Z

    if-eqz v7, :cond_6

    iget-boolean v7, v0, Lla;->j:Z

    if-nez v7, :cond_6

    .line 271
    iget-object v7, v0, Lla;->c:Lly;

    iget-object v8, v0, Lla;->f:Ljava/lang/Object;

    invoke-virtual {v0, v7, v8}, Lla;->b(Lly;Ljava/lang/Object;)V

    .line 272
    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 273
    :cond_7
    invoke-virtual {v6}, Lkz;->f()V

    .line 274
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 275
    :cond_8
    return-void
.end method

.method public onStateNotSaved()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lit;->c:Liy;

    invoke-virtual {v0}, Liy;->a()V

    .line 146
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 276
    invoke-super {p0}, Lin;->onStop()V

    .line 277
    iput-boolean v1, p0, Lit;->d:Z

    .line 278
    invoke-direct {p0}, Lit;->e()V

    .line 279
    iget-object v0, p0, Lit;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 280
    iget-object v0, p0, Lit;->c:Liy;

    .line 281
    iget-object v0, v0, Liy;->a:Liz;

    iget-object v0, v0, Liz;->d:Ljc;

    invoke-virtual {v0}, Ljc;->p()V

    .line 282
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lit;->a:Z

    if-nez v0, :cond_0

    .line 346
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 347
    invoke-static {p2}, Lit;->d(I)V

    .line 348
    :cond_0
    invoke-super {p0, p1, p2}, Lin;->startActivityForResult(Landroid/content/Intent;I)V

    .line 349
    return-void
.end method

.method public bridge synthetic startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 410
    invoke-super {p0, p1, p2, p3}, Lin;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    .locals 0

    .prologue
    .line 411
    invoke-super/range {p0 .. p6}, Lin;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    return-void
.end method

.method public bridge synthetic startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 409
    invoke-super/range {p0 .. p7}, Lin;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V

    return-void
.end method
