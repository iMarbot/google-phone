.class public final Ltr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static u:Landroid/view/animation/Interpolator;


# instance fields
.field public a:I

.field private b:I

.field private c:I

.field private d:[F

.field private e:[F

.field private f:[F

.field private g:[F

.field private h:[I

.field private i:[I

.field private j:[I

.field private k:I

.field private l:Landroid/view/VelocityTracker;

.field private m:F

.field private n:F

.field private o:I

.field private p:Landroid/widget/OverScroller;

.field private q:Ltu;

.field private r:Landroid/view/View;

.field private s:Z

.field private t:Landroid/view/ViewGroup;

.field private v:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 408
    new-instance v0, Lts;

    invoke-direct {v0}, Lts;-><init>()V

    sput-object v0, Ltr;->u:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Ltu;)V
    .locals 3

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    const/4 v0, -0x1

    iput v0, p0, Ltr;->c:I

    .line 4
    new-instance v0, Ltt;

    invoke-direct {v0, p0}, Ltt;-><init>(Ltr;)V

    iput-object v0, p0, Ltr;->v:Ljava/lang/Runnable;

    .line 5
    if-nez p2, :cond_0

    .line 6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parent view may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    if-nez p3, :cond_1

    .line 8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Callback may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_1
    iput-object p2, p0, Ltr;->t:Landroid/view/ViewGroup;

    .line 10
    iput-object p3, p0, Ltr;->q:Ltu;

    .line 11
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 13
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Ltr;->o:I

    .line 14
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Ltr;->a:I

    .line 15
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Ltr;->m:F

    .line 16
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Ltr;->n:F

    .line 17
    new-instance v0, Landroid/widget/OverScroller;

    sget-object v1, Ltr;->u:Landroid/view/animation/Interpolator;

    invoke-direct {v0, p1, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Ltr;->p:Landroid/widget/OverScroller;

    .line 18
    return-void
.end method

.method private static a(FFF)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 100
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 101
    cmpg-float v2, v1, p1

    if-gez v2, :cond_1

    move p2, v0

    .line 103
    :cond_0
    :goto_0
    return p2

    .line 102
    :cond_1
    cmpl-float v1, v1, p2

    if-lez v1, :cond_2

    cmpl-float v0, p0, v0

    if-gtz v0, :cond_0

    neg-float p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 103
    goto :goto_0
.end method

.method private final a(III)I
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 79
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 82
    div-int/lit8 v1, v0, 0x2

    .line 83
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 84
    int-to-float v2, v1

    int-to-float v1, v1

    .line 86
    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v0, v3

    .line 87
    const v3, 0x3ef1463b

    mul-float/2addr v0, v3

    .line 88
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 89
    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 90
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 91
    if-lez v1, :cond_1

    .line 92
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 95
    :goto_1
    const/16 v1, 0x258

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 93
    :cond_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 94
    add-float/2addr v0, v6

    const/high16 v1, 0x43800000    # 256.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_1
.end method

.method public static a(Landroid/view/ViewGroup;Ltu;)Ltr;
    .locals 2

    .prologue
    .line 1
    new-instance v0, Ltr;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Ltr;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Ltu;)V

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Ltr;->c:I

    .line 28
    iget-object v0, p0, Ltr;->d:[F

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Ltr;->d:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 30
    iget-object v0, p0, Ltr;->e:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 31
    iget-object v0, p0, Ltr;->f:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 32
    iget-object v0, p0, Ltr;->g:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 33
    iget-object v0, p0, Ltr;->h:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 34
    iget-object v0, p0, Ltr;->i:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 35
    iget-object v0, p0, Ltr;->j:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 36
    iput v2, p0, Ltr;->k:I

    .line 37
    :cond_0
    iget-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    .line 40
    :cond_1
    return-void
.end method

.method private final a(FF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    iput-boolean v3, p0, Ltr;->s:Z

    .line 123
    iget-object v0, p0, Ltr;->q:Ltu;

    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v0, v1, p1, p2}, Ltu;->a(Landroid/view/View;FF)V

    .line 124
    iput-boolean v2, p0, Ltr;->s:Z

    .line 125
    iget v0, p0, Ltr;->b:I

    if-ne v0, v3, :cond_0

    .line 126
    invoke-virtual {p0, v2}, Ltr;->a(I)V

    .line 127
    :cond_0
    return-void
.end method

.method private final a(FFI)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 139
    .line 140
    iget-object v2, p0, Ltr;->d:[F

    if-eqz v2, :cond_0

    iget-object v2, p0, Ltr;->d:[F

    array-length v2, v2

    if-gt v2, p3, :cond_2

    .line 141
    :cond_0
    add-int/lit8 v2, p3, 0x1

    new-array v2, v2, [F

    .line 142
    add-int/lit8 v3, p3, 0x1

    new-array v3, v3, [F

    .line 143
    add-int/lit8 v4, p3, 0x1

    new-array v4, v4, [F

    .line 144
    add-int/lit8 v5, p3, 0x1

    new-array v5, v5, [F

    .line 145
    add-int/lit8 v6, p3, 0x1

    new-array v6, v6, [I

    .line 146
    add-int/lit8 v7, p3, 0x1

    new-array v7, v7, [I

    .line 147
    add-int/lit8 v8, p3, 0x1

    new-array v8, v8, [I

    .line 148
    iget-object v9, p0, Ltr;->d:[F

    if-eqz v9, :cond_1

    .line 149
    iget-object v9, p0, Ltr;->d:[F

    iget-object v10, p0, Ltr;->d:[F

    array-length v10, v10

    invoke-static {v9, v0, v2, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    iget-object v9, p0, Ltr;->e:[F

    iget-object v10, p0, Ltr;->e:[F

    array-length v10, v10

    invoke-static {v9, v0, v3, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    iget-object v9, p0, Ltr;->f:[F

    iget-object v10, p0, Ltr;->f:[F

    array-length v10, v10

    invoke-static {v9, v0, v4, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 152
    iget-object v9, p0, Ltr;->g:[F

    iget-object v10, p0, Ltr;->g:[F

    array-length v10, v10

    invoke-static {v9, v0, v5, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    iget-object v9, p0, Ltr;->h:[I

    iget-object v10, p0, Ltr;->h:[I

    array-length v10, v10

    invoke-static {v9, v0, v6, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    iget-object v9, p0, Ltr;->i:[I

    iget-object v10, p0, Ltr;->i:[I

    array-length v10, v10

    invoke-static {v9, v0, v7, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    iget-object v9, p0, Ltr;->j:[I

    iget-object v10, p0, Ltr;->j:[I

    array-length v10, v10

    invoke-static {v9, v0, v8, v0, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    :cond_1
    iput-object v2, p0, Ltr;->d:[F

    .line 157
    iput-object v3, p0, Ltr;->e:[F

    .line 158
    iput-object v4, p0, Ltr;->f:[F

    .line 159
    iput-object v5, p0, Ltr;->g:[F

    .line 160
    iput-object v6, p0, Ltr;->h:[I

    .line 161
    iput-object v7, p0, Ltr;->i:[I

    .line 162
    iput-object v8, p0, Ltr;->j:[I

    .line 163
    :cond_2
    iget-object v2, p0, Ltr;->d:[F

    iget-object v3, p0, Ltr;->f:[F

    aput p1, v3, p3

    aput p1, v2, p3

    .line 164
    iget-object v2, p0, Ltr;->e:[F

    iget-object v3, p0, Ltr;->g:[F

    aput p2, v3, p3

    aput p2, v2, p3

    .line 165
    iget-object v2, p0, Ltr;->h:[I

    float-to-int v3, p1

    float-to-int v4, p2

    .line 167
    iget-object v5, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLeft()I

    move-result v5

    iget v6, p0, Ltr;->o:I

    add-int/2addr v5, v6

    if-ge v3, v5, :cond_3

    move v0, v1

    .line 168
    :cond_3
    iget-object v5, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getTop()I

    move-result v5

    iget v6, p0, Ltr;->o:I

    add-int/2addr v5, v6

    if-ge v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x4

    .line 169
    :cond_4
    iget-object v5, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getRight()I

    move-result v5

    iget v6, p0, Ltr;->o:I

    sub-int/2addr v5, v6

    if-le v3, v5, :cond_5

    or-int/lit8 v0, v0, 0x2

    .line 170
    :cond_5
    iget-object v3, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    iget v5, p0, Ltr;->o:I

    sub-int/2addr v3, v5

    if-le v4, v3, :cond_6

    or-int/lit8 v0, v0, 0x8

    .line 172
    :cond_6
    aput v0, v2, p3

    .line 173
    iget v0, p0, Ltr;->k:I

    shl-int/2addr v1, p3

    or-int/2addr v0, v1

    iput v0, p0, Ltr;->k:I

    .line 174
    return-void
.end method

.method private final a(FFII)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 369
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 370
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 371
    iget-object v3, p0, Ltr;->h:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-ne v3, p4, :cond_0

    and-int/lit8 v3, p4, 0x0

    if-eqz v3, :cond_0

    iget-object v3, p0, Ltr;->j:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget-object v3, p0, Ltr;->i:[I

    aget v3, v3, p3

    and-int/2addr v3, p4

    if-eq v3, p4, :cond_0

    iget v3, p0, Ltr;->a:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_1

    iget v3, p0, Ltr;->a:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 373
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Ltr;->i:[I

    aget v2, v2, p3

    and-int/2addr v2, p4

    if-nez v2, :cond_0

    iget v2, p0, Ltr;->a:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private final a(IIII)Z
    .locals 14

    .prologue
    .line 53
    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 54
    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 55
    sub-int v4, p1, v2

    .line 56
    sub-int v5, p2, v3

    .line 57
    if-nez v4, :cond_0

    if-nez v5, :cond_0

    .line 58
    iget-object v1, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 59
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ltr;->a(I)V

    .line 60
    const/4 v1, 0x0

    .line 78
    :goto_0
    return v1

    .line 61
    :cond_0
    iget-object v7, p0, Ltr;->r:Landroid/view/View;

    .line 62
    iget v1, p0, Ltr;->n:F

    float-to-int v1, v1

    iget v6, p0, Ltr;->m:F

    float-to-int v6, v6

    move/from16 v0, p3

    invoke-static {v0, v1, v6}, Ltr;->b(III)I

    move-result v8

    .line 63
    iget v1, p0, Ltr;->n:F

    float-to-int v1, v1

    iget v6, p0, Ltr;->m:F

    float-to-int v6, v6

    move/from16 v0, p4

    invoke-static {v0, v1, v6}, Ltr;->b(III)I

    move-result v9

    .line 64
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 65
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .line 66
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 67
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v11

    .line 68
    add-int v12, v6, v11

    .line 69
    add-int v13, v1, v10

    .line 70
    if-eqz v8, :cond_1

    int-to-float v1, v6

    int-to-float v6, v12

    div-float/2addr v1, v6

    move v6, v1

    .line 71
    :goto_1
    if-eqz v9, :cond_2

    int-to-float v1, v11

    int-to-float v10, v12

    div-float/2addr v1, v10

    .line 72
    :goto_2
    iget-object v10, p0, Ltr;->q:Ltu;

    invoke-virtual {v10, v7}, Ltu;->a(Landroid/view/View;)I

    move-result v7

    invoke-direct {p0, v4, v8, v7}, Ltr;->a(III)I

    move-result v7

    .line 73
    iget-object v8, p0, Ltr;->q:Ltu;

    invoke-virtual {v8}, Ltu;->a()I

    move-result v8

    invoke-direct {p0, v5, v9, v8}, Ltr;->a(III)I

    move-result v8

    .line 74
    int-to-float v7, v7

    mul-float/2addr v6, v7

    int-to-float v7, v8

    mul-float/2addr v1, v7

    add-float/2addr v1, v6

    float-to-int v6, v1

    .line 76
    iget-object v1, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual/range {v1 .. v6}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 77
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ltr;->a(I)V

    .line 78
    const/4 v1, 0x1

    goto :goto_0

    .line 70
    :cond_1
    int-to-float v1, v1

    int-to-float v6, v13

    div-float/2addr v1, v6

    move v6, v1

    goto :goto_1

    .line 71
    :cond_2
    int-to-float v1, v10

    int-to-float v10, v13

    div-float/2addr v1, v10

    goto :goto_2
.end method

.method private final a(Landroid/view/View;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 374
    if-nez p1, :cond_1

    .line 384
    :cond_0
    :goto_0
    return v2

    .line 376
    :cond_1
    iget-object v0, p0, Ltr;->q:Ltu;

    invoke-virtual {v0, p1}, Ltu;->a(Landroid/view/View;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 377
    :goto_1
    iget-object v3, p0, Ltr;->q:Ltu;

    invoke-virtual {v3}, Ltu;->a()I

    move-result v3

    if-lez v3, :cond_3

    move v3, v1

    .line 378
    :goto_2
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    .line 379
    mul-float v0, p2, p2

    mul-float v3, p3, p3

    add-float/2addr v0, v3

    iget v3, p0, Ltr;->a:I

    iget v4, p0, Ltr;->a:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 376
    goto :goto_1

    :cond_3
    move v3, v2

    .line 377
    goto :goto_2

    .line 380
    :cond_4
    if-eqz v0, :cond_5

    .line 381
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Ltr;->a:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0

    .line 382
    :cond_5
    if-eqz v3, :cond_0

    .line 383
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v3, p0, Ltr;->a:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method private static b(III)I
    .locals 1

    .prologue
    .line 96
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 97
    if-ge v0, p1, :cond_1

    const/4 p2, 0x0

    .line 99
    :cond_0
    :goto_0
    return p2

    .line 98
    :cond_1
    if-le v0, p2, :cond_2

    if-gtz p0, :cond_0

    neg-int p2, p2

    goto :goto_0

    :cond_2
    move p2, p0

    .line 99
    goto :goto_0
.end method

.method private b(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 394
    iget-object v0, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 395
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 396
    iget-object v0, p0, Ltr;->t:Landroid/view/ViewGroup;

    .line 398
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    if-lt p1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 400
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-lt p2, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 403
    :goto_1
    return-object v0

    .line 402
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 403
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final b()V
    .locals 4

    .prologue
    .line 385
    iget-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Ltr;->m:F

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 386
    iget-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    iget v1, p0, Ltr;->c:I

    .line 387
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    iget v1, p0, Ltr;->n:F

    iget v2, p0, Ltr;->m:F

    .line 388
    invoke-static {v0, v1, v2}, Ltr;->a(FFF)F

    move-result v0

    .line 389
    iget-object v1, p0, Ltr;->l:Landroid/view/VelocityTracker;

    iget v2, p0, Ltr;->c:I

    .line 390
    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v1

    iget v2, p0, Ltr;->n:F

    iget v3, p0, Ltr;->m:F

    .line 391
    invoke-static {v1, v2, v3}, Ltr;->a(FFF)F

    move-result v1

    .line 392
    invoke-direct {p0, v0, v1}, Ltr;->a(FF)V

    .line 393
    return-void
.end method

.method private final b(FFI)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 357
    const/4 v1, 0x0

    .line 358
    invoke-direct {p0, p1, p2, p3, v0}, Ltr;->a(FFII)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 360
    :goto_0
    const/4 v1, 0x4

    invoke-direct {p0, p2, p1, p3, v1}, Ltr;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361
    or-int/lit8 v0, v0, 0x4

    .line 362
    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, p3, v1}, Ltr;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    or-int/lit8 v0, v0, 0x2

    .line 364
    :cond_1
    const/16 v1, 0x8

    invoke-direct {p0, p2, p1, p3, v1}, Ltr;->a(FFII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 365
    or-int/lit8 v0, v0, 0x8

    .line 366
    :cond_2
    if-eqz v0, :cond_3

    .line 367
    iget-object v1, p0, Ltr;->i:[I

    aget v2, v1, p3

    or-int/2addr v0, v2

    aput v0, v1, p3

    .line 368
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Ltr;->d:[F

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Ltr;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Ltr;->d:[F

    aput v1, v0, p1

    .line 131
    iget-object v0, p0, Ltr;->e:[F

    aput v1, v0, p1

    .line 132
    iget-object v0, p0, Ltr;->f:[F

    aput v1, v0, p1

    .line 133
    iget-object v0, p0, Ltr;->g:[F

    aput v1, v0, p1

    .line 134
    iget-object v0, p0, Ltr;->h:[I

    aput v2, v0, p1

    .line 135
    iget-object v0, p0, Ltr;->i:[I

    aput v2, v0, p1

    .line 136
    iget-object v0, p0, Ltr;->j:[I

    aput v2, v0, p1

    .line 137
    iget v0, p0, Ltr;->k:I

    const/4 v1, 0x1

    shl-int/2addr v1, p1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Ltr;->k:I

    goto :goto_0
.end method

.method private b(Landroid/view/View;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 193
    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    if-ne p1, v1, :cond_0

    iget v1, p0, Ltr;->c:I

    if-ne v1, p2, :cond_0

    .line 199
    :goto_0
    return v0

    .line 195
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Ltr;->q:Ltu;

    invoke-virtual {v1, p1, p2}, Ltu;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    iput p2, p0, Ltr;->c:I

    .line 197
    invoke-virtual {p0, p1, p2}, Ltr;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 176
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 177
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 178
    invoke-direct {p0, v2}, Ltr;->d(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 180
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 181
    iget-object v5, p0, Ltr;->f:[F

    aput v3, v5, v2

    .line 182
    iget-object v3, p0, Ltr;->g:[F

    aput v4, v3, v2

    .line 183
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    :cond_1
    return-void
.end method

.method private c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 185
    iget v1, p0, Ltr;->k:I

    shl-int v2, v0, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final d(I)Z
    .locals 3

    .prologue
    .line 404
    invoke-direct {p0, p1}, Ltr;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    const-string v0, "ViewDragHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring pointerId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because ACTION_DOWN was not received for this pointer before ACTION_MOVE. It likely happened because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ViewDragHelper did not receive all the events in the event stream."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const/4 v0, 0x0

    .line 407
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final a(I)V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Ltr;->t:Landroid/view/ViewGroup;

    iget-object v1, p0, Ltr;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 187
    iget v0, p0, Ltr;->b:I

    if-eq v0, p1, :cond_0

    .line 188
    iput p1, p0, Ltr;->b:I

    .line 189
    iget-object v0, p0, Ltr;->q:Ltu;

    invoke-virtual {v0, p1}, Ltu;->a(I)V

    .line 190
    iget v0, p0, Ltr;->b:I

    if-nez v0, :cond_0

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Ltr;->r:Landroid/view/View;

    .line 192
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 19
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Ltr;->t:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "captureChildView: parameter must be a descendant of the ViewDragHelper\'s tracked parent view ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ltr;->t:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Ltr;->r:Landroid/view/View;

    .line 22
    iput p2, p0, Ltr;->c:I

    .line 23
    iget-object v0, p0, Ltr;->q:Ltu;

    invoke-virtual {v0, p1, p2}, Ltu;->d(Landroid/view/View;I)V

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltr;->a(I)V

    .line 25
    return-void
.end method

.method public final a(II)Z
    .locals 3

    .prologue
    .line 47
    iget-boolean v0, p0, Ltr;->s:Z

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    iget-object v0, p0, Ltr;->l:Landroid/view/VelocityTracker;

    iget v1, p0, Ltr;->c:I

    .line 50
    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Ltr;->l:Landroid/view/VelocityTracker;

    iget v2, p0, Ltr;->c:I

    .line 51
    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v1

    float-to-int v1, v1

    .line 52
    invoke-direct {p0, p1, p2, v0, v1}, Ltr;->a(IIII)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 201
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 202
    if-nez v0, :cond_0

    .line 203
    invoke-direct {p0}, Ltr;->a()V

    .line 204
    :cond_0
    iget-object v2, p0, Ltr;->l:Landroid/view/VelocityTracker;

    if-nez v2, :cond_1

    .line 205
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Ltr;->l:Landroid/view/VelocityTracker;

    .line 206
    :cond_1
    iget-object v2, p0, Ltr;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 207
    packed-switch v0, :pswitch_data_0

    .line 259
    :cond_2
    :goto_0
    :pswitch_0
    iget v0, p0, Ltr;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 208
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 210
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 211
    invoke-direct {p0, v0, v1, v2}, Ltr;->a(FFI)V

    .line 212
    float-to-int v0, v0

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Ltr;->b(II)Landroid/view/View;

    move-result-object v0

    .line 213
    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    if-ne v0, v1, :cond_3

    iget v1, p0, Ltr;->b:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    .line 214
    invoke-direct {p0, v0, v2}, Ltr;->b(Landroid/view/View;I)Z

    .line 215
    :cond_3
    iget-object v0, p0, Ltr;->h:[I

    aget v0, v0, v2

    goto :goto_0

    .line 218
    :pswitch_2
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 219
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 220
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 221
    invoke-direct {p0, v2, v1, v0}, Ltr;->a(FFI)V

    .line 222
    iget v3, p0, Ltr;->b:I

    if-eqz v3, :cond_2

    .line 223
    iget v3, p0, Ltr;->b:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 224
    float-to-int v2, v2

    float-to-int v1, v1

    invoke-direct {p0, v2, v1}, Ltr;->b(II)Landroid/view/View;

    move-result-object v1

    .line 225
    iget-object v2, p0, Ltr;->r:Landroid/view/View;

    if-ne v1, v2, :cond_2

    .line 226
    invoke-direct {p0, v1, v0}, Ltr;->b(Landroid/view/View;I)Z

    goto :goto_0

    .line 228
    :pswitch_3
    iget-object v0, p0, Ltr;->d:[F

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltr;->e:[F

    if-eqz v0, :cond_2

    .line 229
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 230
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_8

    .line 231
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 232
    invoke-direct {p0, v3}, Ltr;->d(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 233
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 234
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 235
    iget-object v5, p0, Ltr;->d:[F

    aget v5, v5, v3

    sub-float v5, v0, v5

    .line 236
    iget-object v6, p0, Ltr;->e:[F

    aget v6, v6, v3

    sub-float v6, v4, v6

    .line 237
    float-to-int v0, v0

    float-to-int v4, v4

    invoke-direct {p0, v0, v4}, Ltr;->b(II)Landroid/view/View;

    move-result-object v4

    .line 238
    if-eqz v4, :cond_7

    invoke-direct {p0, v4, v5, v6}, Ltr;->a(Landroid/view/View;FF)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    .line 239
    :goto_3
    if-eqz v0, :cond_5

    .line 240
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    .line 241
    float-to-int v8, v5

    add-int/2addr v8, v7

    .line 242
    iget-object v9, p0, Ltr;->q:Ltu;

    invoke-virtual {v9, v4, v8}, Ltu;->c(Landroid/view/View;I)I

    move-result v8

    .line 243
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v9

    .line 244
    float-to-int v10, v6

    add-int/2addr v10, v9

    .line 245
    iget-object v11, p0, Ltr;->q:Ltu;

    invoke-virtual {v11, v4, v10}, Ltu;->b(Landroid/view/View;I)I

    move-result v10

    .line 246
    iget-object v11, p0, Ltr;->q:Ltu;

    invoke-virtual {v11, v4}, Ltu;->a(Landroid/view/View;)I

    move-result v11

    .line 247
    iget-object v12, p0, Ltr;->q:Ltu;

    invoke-virtual {v12}, Ltu;->a()I

    move-result v12

    .line 248
    if-eqz v11, :cond_4

    if-lez v11, :cond_5

    if-ne v8, v7, :cond_5

    :cond_4
    if-eqz v12, :cond_8

    if-lez v12, :cond_5

    if-eq v10, v9, :cond_8

    .line 249
    :cond_5
    invoke-direct {p0, v5, v6, v3}, Ltr;->b(FFI)V

    .line 250
    iget v5, p0, Ltr;->b:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_8

    .line 251
    if-eqz v0, :cond_6

    invoke-direct {p0, v4, v3}, Ltr;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 252
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 238
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 253
    :cond_8
    invoke-direct {p0, p1}, Ltr;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 255
    :pswitch_4
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 256
    invoke-direct {p0, v0}, Ltr;->b(I)V

    goto/16 :goto_0

    .line 258
    :pswitch_5
    invoke-direct {p0}, Ltr;->a()V

    goto/16 :goto_0

    .line 259
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 207
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/View;II)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iput-object p1, p0, Ltr;->r:Landroid/view/View;

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Ltr;->c:I

    .line 43
    invoke-direct {p0, p2, p3, v1, v1}, Ltr;->a(IIII)Z

    move-result v0

    .line 44
    if-nez v0, :cond_0

    iget v1, p0, Ltr;->b:I

    if-nez v1, :cond_0

    iget-object v1, p0, Ltr;->r:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x0

    iput-object v1, p0, Ltr;->r:Landroid/view/View;

    .line 46
    :cond_0
    return v0
.end method

.method public final a(Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 104
    iget v1, p0, Ltr;->b:I

    if-ne v1, v7, :cond_5

    .line 105
    iget-object v1, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v1

    .line 106
    iget-object v2, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v2

    .line 107
    iget-object v3, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    .line 108
    iget-object v4, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int v4, v2, v4

    .line 109
    iget-object v5, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int v5, v3, v5

    .line 110
    if-eqz v4, :cond_0

    .line 111
    iget-object v6, p0, Ltr;->r:Landroid/view/View;

    invoke-static {v6, v4}, Lqy;->e(Landroid/view/View;I)V

    .line 112
    :cond_0
    if-eqz v5, :cond_1

    .line 113
    iget-object v6, p0, Ltr;->r:Landroid/view/View;

    invoke-static {v6, v5}, Lqy;->d(Landroid/view/View;I)V

    .line 114
    :cond_1
    if-nez v4, :cond_2

    if-eqz v5, :cond_3

    .line 115
    :cond_2
    iget-object v4, p0, Ltr;->q:Ltu;

    iget-object v5, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v4, v5, v2, v3}, Ltu;->a(Landroid/view/View;II)V

    .line 116
    :cond_3
    if-eqz v1, :cond_4

    iget-object v4, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v4}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v4

    if-ne v2, v4, :cond_4

    iget-object v2, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v2

    if-ne v3, v2, :cond_4

    .line 117
    iget-object v1, p0, Ltr;->p:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->abortAnimation()V

    move v1, v0

    .line 119
    :cond_4
    if-nez v1, :cond_5

    .line 120
    iget-object v1, p0, Ltr;->t:Landroid/view/ViewGroup;

    iget-object v2, p0, Ltr;->v:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 121
    :cond_5
    iget v1, p0, Ltr;->b:I

    if-ne v1, v7, :cond_6

    const/4 v0, 0x1

    :cond_6
    return v0
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 260
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    .line 262
    if-nez v3, :cond_0

    .line 263
    invoke-direct {p0}, Ltr;->a()V

    .line 264
    :cond_0
    iget-object v5, p0, Ltr;->l:Landroid/view/VelocityTracker;

    if-nez v5, :cond_1

    .line 265
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Ltr;->l:Landroid/view/VelocityTracker;

    .line 266
    :cond_1
    iget-object v5, p0, Ltr;->l:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 267
    packed-switch v3, :pswitch_data_0

    .line 356
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 268
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 269
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 270
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 271
    float-to-int v3, v1

    float-to-int v4, v2

    invoke-direct {p0, v3, v4}, Ltr;->b(II)Landroid/view/View;

    move-result-object v3

    .line 272
    invoke-direct {p0, v1, v2, v0}, Ltr;->a(FFI)V

    .line 273
    invoke-direct {p0, v3, v0}, Ltr;->b(Landroid/view/View;I)Z

    .line 274
    iget-object v1, p0, Ltr;->h:[I

    aget v0, v1, v0

    goto :goto_0

    .line 277
    :pswitch_2
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 278
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 279
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 280
    invoke-direct {p0, v3, v4, v1}, Ltr;->a(FFI)V

    .line 281
    iget v5, p0, Ltr;->b:I

    if-nez v5, :cond_3

    .line 282
    float-to-int v0, v3

    float-to-int v2, v4

    invoke-direct {p0, v0, v2}, Ltr;->b(II)Landroid/view/View;

    move-result-object v0

    .line 283
    invoke-direct {p0, v0, v1}, Ltr;->b(Landroid/view/View;I)Z

    goto :goto_0

    .line 284
    :cond_3
    float-to-int v3, v3

    float-to-int v4, v4

    .line 285
    iget-object v5, p0, Ltr;->r:Landroid/view/View;

    .line 286
    if-eqz v5, :cond_4

    .line 287
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v3, v6, :cond_4

    .line 288
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 289
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v4, v3, :cond_4

    .line 290
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v4, v3, :cond_4

    move v0, v2

    .line 291
    :cond_4
    if-eqz v0, :cond_2

    .line 292
    iget-object v0, p0, Ltr;->r:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Ltr;->b(Landroid/view/View;I)Z

    goto :goto_0

    .line 293
    :pswitch_3
    iget v1, p0, Ltr;->b:I

    if-ne v1, v2, :cond_9

    .line 294
    iget v0, p0, Ltr;->c:I

    invoke-direct {p0, v0}, Ltr;->d(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    iget v0, p0, Ltr;->c:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 296
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 297
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 298
    iget-object v2, p0, Ltr;->f:[F

    iget v3, p0, Ltr;->c:I

    aget v2, v2, v3

    sub-float/2addr v1, v2

    float-to-int v2, v1

    .line 299
    iget-object v1, p0, Ltr;->g:[F

    iget v3, p0, Ltr;->c:I

    aget v1, v1, v3

    sub-float/2addr v0, v1

    float-to-int v3, v0

    .line 300
    iget-object v0, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int v1, v0, v2

    iget-object v0, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    add-int/2addr v0, v3

    .line 303
    iget-object v4, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 304
    iget-object v5, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 305
    if-eqz v2, :cond_5

    .line 306
    iget-object v6, p0, Ltr;->q:Ltu;

    iget-object v7, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v6, v7, v1}, Ltu;->c(Landroid/view/View;I)I

    move-result v1

    .line 307
    iget-object v6, p0, Ltr;->r:Landroid/view/View;

    sub-int v4, v1, v4

    invoke-static {v6, v4}, Lqy;->e(Landroid/view/View;I)V

    .line 308
    :cond_5
    if-eqz v3, :cond_6

    .line 309
    iget-object v4, p0, Ltr;->q:Ltu;

    iget-object v6, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v4, v6, v0}, Ltu;->b(Landroid/view/View;I)I

    move-result v0

    .line 310
    iget-object v4, p0, Ltr;->r:Landroid/view/View;

    sub-int v5, v0, v5

    invoke-static {v4, v5}, Lqy;->d(Landroid/view/View;I)V

    .line 311
    :cond_6
    if-nez v2, :cond_7

    if-eqz v3, :cond_8

    .line 312
    :cond_7
    iget-object v2, p0, Ltr;->q:Ltu;

    iget-object v3, p0, Ltr;->r:Landroid/view/View;

    invoke-virtual {v2, v3, v1, v0}, Ltu;->a(Landroid/view/View;II)V

    .line 313
    :cond_8
    invoke-direct {p0, p1}, Ltr;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 315
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 316
    :goto_1
    if-ge v0, v1, :cond_b

    .line 317
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 318
    invoke-direct {p0, v3}, Ltr;->d(I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 319
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 320
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    .line 321
    iget-object v6, p0, Ltr;->d:[F

    aget v6, v6, v3

    sub-float v6, v4, v6

    .line 322
    iget-object v7, p0, Ltr;->e:[F

    aget v7, v7, v3

    sub-float v7, v5, v7

    .line 323
    invoke-direct {p0, v6, v7, v3}, Ltr;->b(FFI)V

    .line 324
    iget v8, p0, Ltr;->b:I

    if-eq v8, v2, :cond_b

    .line 325
    float-to-int v4, v4

    float-to-int v5, v5

    invoke-direct {p0, v4, v5}, Ltr;->b(II)Landroid/view/View;

    move-result-object v4

    .line 326
    invoke-direct {p0, v4, v6, v7}, Ltr;->a(Landroid/view/View;FF)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 327
    invoke-direct {p0, v4, v3}, Ltr;->b(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_b

    .line 328
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 329
    :cond_b
    invoke-direct {p0, p1}, Ltr;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 331
    :pswitch_4
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 332
    iget v4, p0, Ltr;->b:I

    if-ne v4, v2, :cond_c

    iget v2, p0, Ltr;->c:I

    if-ne v3, v2, :cond_c

    .line 334
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 335
    :goto_2
    if-ge v0, v2, :cond_10

    .line 336
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    .line 337
    iget v5, p0, Ltr;->c:I

    if-eq v4, v5, :cond_d

    .line 338
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 339
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 340
    float-to-int v5, v5

    float-to-int v6, v6

    invoke-direct {p0, v5, v6}, Ltr;->b(II)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Ltr;->r:Landroid/view/View;

    if-ne v5, v6, :cond_d

    iget-object v5, p0, Ltr;->r:Landroid/view/View;

    .line 341
    invoke-direct {p0, v5, v4}, Ltr;->b(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 342
    iget v0, p0, Ltr;->c:I

    .line 345
    :goto_3
    if-ne v0, v1, :cond_c

    .line 346
    invoke-direct {p0}, Ltr;->b()V

    .line 347
    :cond_c
    invoke-direct {p0, v3}, Ltr;->b(I)V

    goto/16 :goto_0

    .line 344
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 349
    :pswitch_5
    iget v0, p0, Ltr;->b:I

    if-ne v0, v2, :cond_e

    .line 350
    invoke-direct {p0}, Ltr;->b()V

    .line 351
    :cond_e
    invoke-direct {p0}, Ltr;->a()V

    goto/16 :goto_0

    .line 353
    :pswitch_6
    iget v0, p0, Ltr;->b:I

    if-ne v0, v2, :cond_f

    .line 354
    invoke-direct {p0, v6, v6}, Ltr;->a(FF)V

    .line 355
    :cond_f
    invoke-direct {p0}, Ltr;->a()V

    goto/16 :goto_0

    :cond_10
    move v0, v1

    goto :goto_3

    .line 267
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
