.class public final Lgxl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final serialVersionUID:J = 0x1L


# instance fields
.field public a:Z

.field public b:I

.field public c:Z

.field public d:J

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:I

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Lgxm;

.field public o:Z

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v2, p0, Lgxl;->b:I

    .line 3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgxl;->d:J

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lgxl;->f:Ljava/lang/String;

    .line 5
    iput-boolean v2, p0, Lgxl;->h:Z

    .line 6
    const/4 v0, 0x1

    iput v0, p0, Lgxl;->j:I

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lgxl;->l:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lgxl;->p:Ljava/lang/String;

    .line 9
    sget-object v0, Lgxm;->e:Lgxm;

    iput-object v0, p0, Lgxl;->n:Lgxm;

    .line 10
    return-void
.end method


# virtual methods
.method public final a(I)Lgxl;
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->a:Z

    .line 12
    iput p1, p0, Lgxl;->b:I

    .line 13
    return-object p0
.end method

.method public final a(J)Lgxl;
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->c:Z

    .line 15
    iput-wide p1, p0, Lgxl;->d:J

    .line 16
    return-object p0
.end method

.method public final a(Lgxm;)Lgxl;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->m:Z

    .line 36
    iput-object p1, p0, Lgxl;->n:Lgxm;

    .line 37
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lgxl;
    .locals 1

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->e:Z

    .line 20
    iput-object p1, p0, Lgxl;->f:Ljava/lang/String;

    .line 21
    return-object p0
.end method

.method public final a(Z)Lgxl;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->g:Z

    .line 23
    iput-boolean p1, p0, Lgxl;->h:Z

    .line 24
    return-object p0
.end method

.method public final a(Lgxl;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 43
    if-nez p1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 46
    goto :goto_0

    .line 47
    :cond_2
    iget v2, p0, Lgxl;->b:I

    iget v3, p1, Lgxl;->b:I

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lgxl;->d:J

    iget-wide v4, p1, Lgxl;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lgxl;->f:Ljava/lang/String;

    iget-object v3, p1, Lgxl;->f:Ljava/lang/String;

    .line 48
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lgxl;->h:Z

    iget-boolean v3, p1, Lgxl;->h:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lgxl;->j:I

    iget v3, p1, Lgxl;->j:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lgxl;->l:Ljava/lang/String;

    iget-object v3, p1, Lgxl;->l:Ljava/lang/String;

    .line 49
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgxl;->n:Lgxm;

    iget-object v3, p1, Lgxl;->n:Lgxm;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lgxl;->p:Ljava/lang/String;

    iget-object v3, p1, Lgxl;->p:Ljava/lang/String;

    .line 50
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    iget-boolean v2, p0, Lgxl;->o:Z

    .line 54
    iget-boolean v3, p1, Lgxl;->o:Z

    .line 55
    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b(I)Lgxl;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->i:Z

    .line 26
    iput p1, p0, Lgxl;->j:I

    .line 27
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lgxl;
    .locals 1

    .prologue
    .line 28
    if-nez p1, :cond_0

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->k:Z

    .line 31
    iput-object p1, p0, Lgxl;->l:Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lgxl;
    .locals 1

    .prologue
    .line 38
    if-nez p1, :cond_0

    .line 39
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxl;->o:Z

    .line 41
    iput-object p1, p0, Lgxl;->p:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 57
    instance-of v0, p1, Lgxl;

    if-eqz v0, :cond_0

    check-cast p1, Lgxl;

    invoke-virtual {p0, p1}, Lgxl;->a(Lgxl;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 58
    .line 59
    iget v0, p0, Lgxl;->b:I

    .line 60
    add-int/lit16 v0, v0, 0x87d

    .line 61
    mul-int/lit8 v0, v0, 0x35

    .line 62
    iget-wide v4, p0, Lgxl;->d:J

    .line 63
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 64
    mul-int/lit8 v0, v0, 0x35

    .line 65
    iget-object v3, p0, Lgxl;->f:Ljava/lang/String;

    .line 66
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 67
    mul-int/lit8 v3, v0, 0x35

    .line 68
    iget-boolean v0, p0, Lgxl;->h:Z

    .line 69
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 70
    mul-int/lit8 v0, v0, 0x35

    .line 71
    iget v3, p0, Lgxl;->j:I

    .line 72
    add-int/2addr v0, v3

    .line 73
    mul-int/lit8 v0, v0, 0x35

    .line 74
    iget-object v3, p0, Lgxl;->l:Ljava/lang/String;

    .line 75
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 76
    mul-int/lit8 v0, v0, 0x35

    .line 77
    iget-object v3, p0, Lgxl;->n:Lgxm;

    .line 78
    invoke-virtual {v3}, Lgxm;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 79
    mul-int/lit8 v0, v0, 0x35

    .line 80
    iget-object v3, p0, Lgxl;->p:Ljava/lang/String;

    .line 81
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 82
    mul-int/lit8 v0, v0, 0x35

    .line 83
    iget-boolean v3, p0, Lgxl;->o:Z

    .line 84
    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 85
    return v0

    :cond_0
    move v0, v2

    .line 69
    goto :goto_0

    :cond_1
    move v1, v2

    .line 84
    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    const-string v1, "Country Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lgxl;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 88
    const-string v1, " National Number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lgxl;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 90
    iget-boolean v1, p0, Lgxl;->g:Z

    .line 91
    if-eqz v1, :cond_0

    .line 92
    iget-boolean v1, p0, Lgxl;->h:Z

    .line 93
    if-eqz v1, :cond_0

    .line 94
    const-string v1, " Leading Zero(s): true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-boolean v1, p0, Lgxl;->i:Z

    .line 97
    if-eqz v1, :cond_1

    .line 98
    const-string v1, " Number of leading zeros: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lgxl;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-boolean v1, p0, Lgxl;->e:Z

    .line 101
    if-eqz v1, :cond_2

    .line 102
    const-string v1, " Extension: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgxl;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_2
    iget-boolean v1, p0, Lgxl;->m:Z

    .line 105
    if-eqz v1, :cond_3

    .line 106
    const-string v1, " Country Code Source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgxl;->n:Lgxm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_3
    iget-boolean v1, p0, Lgxl;->o:Z

    .line 109
    if-eqz v1, :cond_4

    .line 110
    const-string v1, " Preferred Domestic Carrier Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgxl;->p:Ljava/lang/String;

    .line 111
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
