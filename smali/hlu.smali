.class public final enum Lhlu;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhlu;

.field private static enum b:Lhlu;

.field private static enum c:Lhlu;

.field private static synthetic d:[Lhlu;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3
    new-instance v0, Lhlu;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lhlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlu;->a:Lhlu;

    .line 4
    new-instance v0, Lhlu;

    const-string v1, "INTEGRITY"

    invoke-direct {v0, v1, v3}, Lhlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlu;->b:Lhlu;

    .line 5
    new-instance v0, Lhlu;

    const-string v1, "PRIVACY_AND_INTEGRITY"

    invoke-direct {v0, v1, v4}, Lhlu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhlu;->c:Lhlu;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lhlu;

    sget-object v1, Lhlu;->a:Lhlu;

    aput-object v1, v0, v2

    sget-object v1, Lhlu;->b:Lhlu;

    aput-object v1, v0, v3

    sget-object v1, Lhlu;->c:Lhlu;

    aput-object v1, v0, v4

    sput-object v0, Lhlu;->d:[Lhlu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static values()[Lhlu;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhlu;->d:[Lhlu;

    invoke-virtual {v0}, [Lhlu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhlu;

    return-object v0
.end method
