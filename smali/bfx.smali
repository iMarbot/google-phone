.class final Lbfx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:J

.field public final b:Landroid/net/Uri;

.field public final c:I

.field public final d:Z

.field private e:Z

.field private f:Lbfp;


# direct methods
.method constructor <init>(JLandroid/net/Uri;IZZLbfp;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lbfx;->a:J

    .line 3
    iput-object p3, p0, Lbfx;->b:Landroid/net/Uri;

    .line 4
    iput-boolean p5, p0, Lbfx;->e:Z

    .line 5
    iput-boolean p6, p0, Lbfx;->d:Z

    .line 6
    iput p4, p0, Lbfx;->c:I

    .line 7
    iput-object p7, p0, Lbfx;->f:Lbfp;

    .line 8
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lbfx;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final a(Landroid/widget/ImageView;Z)V
    .locals 2

    .prologue
    .line 28
    if-eqz p2, :cond_1

    .line 29
    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    invoke-static {v0}, Lbfo;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lbfq;->d:Lbfq;

    .line 35
    :goto_0
    iget-object v1, p0, Lbfx;->f:Lbfp;

    invoke-virtual {v1, p1, v0}, Lbfp;->a(Landroid/widget/ImageView;Lbfq;)V

    .line 36
    return-void

    .line 31
    :cond_0
    sget-object v0, Lbfq;->c:Lbfq;

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    invoke-static {v0}, Lbfo;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 33
    sget-object v0, Lbfq;->b:Lbfq;

    goto :goto_0

    .line 34
    :cond_2
    sget-object v0, Lbfq;->a:Lbfq;

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13
    if-ne p0, p1, :cond_1

    .line 26
    :cond_0
    :goto_0
    return v0

    .line 15
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 16
    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 18
    goto :goto_0

    .line 19
    :cond_3
    check-cast p1, Lbfx;

    .line 20
    iget-wide v2, p0, Lbfx;->a:J

    iget-wide v4, p1, Lbfx;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 21
    goto :goto_0

    .line 22
    :cond_4
    iget v2, p0, Lbfx;->c:I

    iget v3, p1, Lbfx;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 23
    goto :goto_0

    .line 24
    :cond_5
    iget-object v2, p0, Lbfx;->b:Landroid/net/Uri;

    iget-object v3, p1, Lbfx;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, Lbib;->a(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 25
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 9
    iget-wide v0, p0, Lbfx;->a:J

    iget-wide v2, p0, Lbfx;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1f

    .line 10
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lbfx;->c:I

    add-int/2addr v0, v1

    .line 11
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 12
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lbfx;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0
.end method
