.class public final Lgnt;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgnt;


# instance fields
.field public externalKey:Lgni;

.field public invitee:[Lglc;

.field public isJoining:Ljava/lang/Boolean;

.field public mediaType:Ljava/lang/Integer;

.field public namedHangout:Lgnu;

.field public requestHeader:Lgls;

.field public sharingUrl:Ljava/lang/String;

.field public syncMetadata:Lgoa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgnt;->clear()Lgnt;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgnt;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgnt;->_emptyArray:[Lgnt;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgnt;->_emptyArray:[Lgnt;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgnt;

    sput-object v0, Lgnt;->_emptyArray:[Lgnt;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgnt;->_emptyArray:[Lgnt;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgnt;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lgnt;

    invoke-direct {v0}, Lgnt;-><init>()V

    invoke-virtual {v0, p0}, Lgnt;->mergeFrom(Lhfp;)Lgnt;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgnt;
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lgnt;

    invoke-direct {v0}, Lgnt;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgnt;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgnt;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    iput-object v1, p0, Lgnt;->requestHeader:Lgls;

    .line 11
    iput-object v1, p0, Lgnt;->externalKey:Lgni;

    .line 12
    invoke-static {}, Lglc;->a()[Lglc;

    move-result-object v0

    iput-object v0, p0, Lgnt;->invitee:[Lglc;

    .line 13
    iput-object v1, p0, Lgnt;->namedHangout:Lgnu;

    .line 14
    iput-object v1, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    .line 15
    iput-object v1, p0, Lgnt;->mediaType:Ljava/lang/Integer;

    .line 16
    iput-object v1, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lgnt;->syncMetadata:Lgoa;

    .line 18
    iput-object v1, p0, Lgnt;->unknownFieldData:Lhfv;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lgnt;->cachedSize:I

    .line 20
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 43
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 44
    iget-object v1, p0, Lgnt;->requestHeader:Lgls;

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x1

    iget-object v2, p0, Lgnt;->requestHeader:Lgls;

    .line 46
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_0
    iget-object v1, p0, Lgnt;->externalKey:Lgni;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lgnt;->externalKey:Lgni;

    .line 49
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_1
    iget-object v1, p0, Lgnt;->invitee:[Lglc;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgnt;->invitee:[Lglc;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 51
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgnt;->invitee:[Lglc;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 52
    iget-object v2, p0, Lgnt;->invitee:[Lglc;

    aget-object v2, v2, v0

    .line 53
    if-eqz v2, :cond_2

    .line 54
    const/4 v3, 0x3

    .line 55
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v1, v2

    .line 56
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 57
    :cond_4
    iget-object v1, p0, Lgnt;->namedHangout:Lgnu;

    if-eqz v1, :cond_5

    .line 58
    const/4 v1, 0x4

    iget-object v2, p0, Lgnt;->namedHangout:Lgnu;

    .line 59
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_5
    iget-object v1, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 61
    const/4 v1, 0x5

    iget-object v2, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 63
    invoke-static {v1}, Lhfq;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 64
    add-int/2addr v0, v1

    .line 65
    :cond_6
    iget-object v1, p0, Lgnt;->mediaType:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 66
    const/4 v1, 0x6

    iget-object v2, p0, Lgnt;->mediaType:Ljava/lang/Integer;

    .line 67
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_7
    iget-object v1, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 69
    const/4 v1, 0x7

    iget-object v2, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    .line 70
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_8
    iget-object v1, p0, Lgnt;->syncMetadata:Lgoa;

    if-eqz v1, :cond_9

    .line 72
    const/16 v1, 0x8

    iget-object v2, p0, Lgnt;->syncMetadata:Lgoa;

    .line 73
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_9
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgnt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 75
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 76
    sparse-switch v0, :sswitch_data_0

    .line 78
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :sswitch_0
    return-object p0

    .line 80
    :sswitch_1
    iget-object v0, p0, Lgnt;->requestHeader:Lgls;

    if-nez v0, :cond_1

    .line 81
    new-instance v0, Lgls;

    invoke-direct {v0}, Lgls;-><init>()V

    iput-object v0, p0, Lgnt;->requestHeader:Lgls;

    .line 82
    :cond_1
    iget-object v0, p0, Lgnt;->requestHeader:Lgls;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 84
    :sswitch_2
    iget-object v0, p0, Lgnt;->externalKey:Lgni;

    if-nez v0, :cond_2

    .line 85
    new-instance v0, Lgni;

    invoke-direct {v0}, Lgni;-><init>()V

    iput-object v0, p0, Lgnt;->externalKey:Lgni;

    .line 86
    :cond_2
    iget-object v0, p0, Lgnt;->externalKey:Lgni;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 88
    :sswitch_3
    const/16 v0, 0x1a

    .line 89
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 90
    iget-object v0, p0, Lgnt;->invitee:[Lglc;

    if-nez v0, :cond_4

    move v0, v1

    .line 91
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lglc;

    .line 92
    if-eqz v0, :cond_3

    .line 93
    iget-object v3, p0, Lgnt;->invitee:[Lglc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    .line 95
    new-instance v3, Lglc;

    invoke-direct {v3}, Lglc;-><init>()V

    aput-object v3, v2, v0

    .line 96
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 97
    invoke-virtual {p1}, Lhfp;->a()I

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 90
    :cond_4
    iget-object v0, p0, Lgnt;->invitee:[Lglc;

    array-length v0, v0

    goto :goto_1

    .line 99
    :cond_5
    new-instance v3, Lglc;

    invoke-direct {v3}, Lglc;-><init>()V

    aput-object v3, v2, v0

    .line 100
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 101
    iput-object v2, p0, Lgnt;->invitee:[Lglc;

    goto :goto_0

    .line 103
    :sswitch_4
    iget-object v0, p0, Lgnt;->namedHangout:Lgnu;

    if-nez v0, :cond_6

    .line 104
    new-instance v0, Lgnu;

    invoke-direct {v0}, Lgnu;-><init>()V

    iput-object v0, p0, Lgnt;->namedHangout:Lgnu;

    .line 105
    :cond_6
    iget-object v0, p0, Lgnt;->namedHangout:Lgnu;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 107
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 109
    :sswitch_6
    invoke-virtual {p1}, Lhfp;->m()I

    move-result v2

    .line 111
    :try_start_0
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v3

    .line 112
    invoke-static {v3}, Lgnj;->checkHangoutMediaTypeOrThrow(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lgnt;->mediaType:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 115
    :catch_0
    move-exception v3

    invoke-virtual {p1, v2}, Lhfp;->e(I)V

    .line 116
    invoke-virtual {p0, p1, v0}, Lgnt;->storeUnknownField(Lhfp;I)Z

    goto/16 :goto_0

    .line 118
    :sswitch_7
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 120
    :sswitch_8
    iget-object v0, p0, Lgnt;->syncMetadata:Lgoa;

    if-nez v0, :cond_7

    .line 121
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    iput-object v0, p0, Lgnt;->syncMetadata:Lgoa;

    .line 122
    :cond_7
    iget-object v0, p0, Lgnt;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto/16 :goto_0

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lgnt;->mergeFrom(Lhfp;)Lgnt;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lgnt;->requestHeader:Lgls;

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iget-object v1, p0, Lgnt;->requestHeader:Lgls;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 23
    :cond_0
    iget-object v0, p0, Lgnt;->externalKey:Lgni;

    if-eqz v0, :cond_1

    .line 24
    const/4 v0, 0x2

    iget-object v1, p0, Lgnt;->externalKey:Lgni;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 25
    :cond_1
    iget-object v0, p0, Lgnt;->invitee:[Lglc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgnt;->invitee:[Lglc;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 26
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgnt;->invitee:[Lglc;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 27
    iget-object v1, p0, Lgnt;->invitee:[Lglc;

    aget-object v1, v1, v0

    .line 28
    if-eqz v1, :cond_2

    .line 29
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lhfq;->b(ILhfz;)V

    .line 30
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_3
    iget-object v0, p0, Lgnt;->namedHangout:Lgnu;

    if-eqz v0, :cond_4

    .line 32
    const/4 v0, 0x4

    iget-object v1, p0, Lgnt;->namedHangout:Lgnu;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 33
    :cond_4
    iget-object v0, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 34
    const/4 v0, 0x5

    iget-object v1, p0, Lgnt;->isJoining:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(IZ)V

    .line 35
    :cond_5
    iget-object v0, p0, Lgnt;->mediaType:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 36
    const/4 v0, 0x6

    iget-object v1, p0, Lgnt;->mediaType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 37
    :cond_6
    iget-object v0, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 38
    const/4 v0, 0x7

    iget-object v1, p0, Lgnt;->sharingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 39
    :cond_7
    iget-object v0, p0, Lgnt;->syncMetadata:Lgoa;

    if-eqz v0, :cond_8

    .line 40
    const/16 v0, 0x8

    iget-object v1, p0, Lgnt;->syncMetadata:Lgoa;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 41
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 42
    return-void
.end method
