.class public final enum Lfci;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lfci;

.field public static final enum b:Lfci;

.field private static enum c:Lfci;

.field private static enum d:Lfci;

.field private static enum e:Lfci;

.field private static synthetic g:[Lfci;


# instance fields
.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lfci;

    const-string v1, "AUDIO"

    const-string v2, "audio"

    invoke-direct {v0, v1, v3, v2}, Lfci;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfci;->a:Lfci;

    .line 6
    new-instance v0, Lfci;

    const-string v1, "VIDEO"

    const-string v2, "video"

    invoke-direct {v0, v1, v4, v2}, Lfci;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfci;->b:Lfci;

    .line 7
    new-instance v0, Lfci;

    const-string v1, "APPLICATION"

    const-string v2, "application"

    invoke-direct {v0, v1, v5, v2}, Lfci;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfci;->c:Lfci;

    .line 8
    new-instance v0, Lfci;

    const-string v1, "DATA"

    const-string v2, "data"

    invoke-direct {v0, v1, v6, v2}, Lfci;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfci;->d:Lfci;

    .line 9
    new-instance v0, Lfci;

    const-string v1, "MESSAGE"

    const-string v2, "message"

    invoke-direct {v0, v1, v7, v2}, Lfci;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfci;->e:Lfci;

    .line 10
    const/4 v0, 0x5

    new-array v0, v0, [Lfci;

    sget-object v1, Lfci;->a:Lfci;

    aput-object v1, v0, v3

    sget-object v1, Lfci;->b:Lfci;

    aput-object v1, v0, v4

    sget-object v1, Lfci;->c:Lfci;

    aput-object v1, v0, v5

    sget-object v1, Lfci;->d:Lfci;

    aput-object v1, v0, v6

    sget-object v1, Lfci;->e:Lfci;

    aput-object v1, v0, v7

    sput-object v0, Lfci;->g:[Lfci;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lfci;->f:Ljava/lang/String;

    .line 4
    return-void
.end method

.method public static values()[Lfci;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lfci;->g:[Lfci;

    invoke-virtual {v0}, [Lfci;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfci;

    return-object v0
.end method
