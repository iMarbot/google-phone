.class public final Lgpx;
.super Lhft;
.source "PG"


# static fields
.field public static volatile _emptyArray:[Lgpx;


# instance fields
.field public hangoutId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lhft;-><init>()V

    .line 8
    invoke-virtual {p0}, Lgpx;->clear()Lgpx;

    .line 9
    return-void
.end method

.method public static emptyArray()[Lgpx;
    .locals 2

    .prologue
    .line 1
    sget-object v0, Lgpx;->_emptyArray:[Lgpx;

    if-nez v0, :cond_1

    .line 2
    sget-object v1, Lhfx;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 3
    :try_start_0
    sget-object v0, Lgpx;->_emptyArray:[Lgpx;

    if-nez v0, :cond_0

    .line 4
    const/4 v0, 0x0

    new-array v0, v0, [Lgpx;

    sput-object v0, Lgpx;->_emptyArray:[Lgpx;

    .line 5
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :cond_1
    sget-object v0, Lgpx;->_emptyArray:[Lgpx;

    return-object v0

    .line 5
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom(Lhfp;)Lgpx;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lgpx;

    invoke-direct {v0}, Lgpx;-><init>()V

    invoke-virtual {v0, p0}, Lgpx;->mergeFrom(Lhfp;)Lgpx;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lgpx;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lgpx;

    invoke-direct {v0}, Lgpx;-><init>()V

    invoke-static {v0, p0}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    move-result-object v0

    check-cast v0, Lgpx;

    return-object v0
.end method


# virtual methods
.method public final clear()Lgpx;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lgpx;->hangoutId:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lgpx;->unknownFieldData:Lhfv;

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lgpx;->cachedSize:I

    .line 13
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 19
    iget-object v1, p0, Lgpx;->hangoutId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20
    const/4 v1, 0x1

    iget-object v2, p0, Lgpx;->hangoutId:Ljava/lang/String;

    .line 21
    invoke-static {v1, v2}, Lhfq;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22
    :cond_0
    return v0
.end method

.method public final mergeFrom(Lhfp;)Lgpx;
    .locals 1

    .prologue
    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 24
    sparse-switch v0, :sswitch_data_0

    .line 26
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    :sswitch_0
    return-object p0

    .line 28
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpx;->hangoutId:Ljava/lang/String;

    goto :goto_0

    .line 24
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lgpx;->mergeFrom(Lhfp;)Lgpx;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lgpx;->hangoutId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 15
    const/4 v0, 0x1

    iget-object v1, p0, Lgpx;->hangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lhfq;->a(ILjava/lang/String;)V

    .line 16
    :cond_0
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 17
    return-void
.end method
