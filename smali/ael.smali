.class public final Lael;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static p:Lafe;

.field private static q:Ljava/lang/Boolean;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/WindowManager;

.field public final c:Landroid/os/Handler;

.field public d:Landroid/view/WindowManager$LayoutParams;

.field public e:Lafi;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/CharSequence;

.field public k:I

.field public l:Laff;

.field public m:Landroid/view/ViewPropertyAnimator;

.field public n:Ljava/lang/Integer;

.field public o:Lafd;

.field private r:Landroid/view/ViewPropertyAnimator;

.field private s:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    sput-object v0, Lael;->q:Ljava/lang/Boolean;

    .line 286
    sget-object v0, Laeo;->a:Lafe;

    sput-object v0, Lael;->p:Lafe;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    new-instance v0, Laex;

    invoke-direct {v0, p0}, Laex;-><init>(Lael;)V

    iput-object v0, p0, Lael;->s:Ljava/lang/Runnable;

    .line 5
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v0, 0x7f12015d

    invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 6
    iput-object v1, p0, Lael;->a:Landroid/content/Context;

    .line 7
    iput-object p2, p0, Lael;->c:Landroid/os/Handler;

    .line 8
    const-class v0, Landroid/view/WindowManager;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lael;->b:Landroid/view/WindowManager;

    .line 9
    new-instance v0, Laff;

    invoke-direct {v0, p0, v1}, Laff;-><init>(Lael;Landroid/content/Context;)V

    iput-object v0, p0, Lael;->l:Laff;

    .line 10
    return-void
.end method

.method private final a(Lafj;Lcom/android/bubble/CheckableImageButton;)V
    .locals 4

    .prologue
    .line 213
    .line 214
    invoke-virtual {p1}, Lafj;->a()Landroid/graphics/drawable/Icon;

    move-result-object v0

    iget-object v1, p0, Lael;->a:Landroid/content/Context;

    new-instance v2, Laet;

    invoke-direct {v2, p2, p1}, Laet;-><init>(Lcom/android/bubble/CheckableImageButton;Lafj;)V

    iget-object v3, p0, Lael;->c:Landroid/os/Handler;

    .line 215
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/drawable/Icon;->loadDrawableAsync(Landroid/content/Context;Landroid/graphics/drawable/Icon$OnDrawableLoadedListener;Landroid/os/Handler;)V

    .line 216
    new-instance v0, Laeu;

    invoke-direct {v0, p0, p1}, Laeu;-><init>(Lael;Lafj;)V

    invoke-virtual {p2, v0}, Lcom/android/bubble/CheckableImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    return-void
.end method

.method private final a(Lcom/android/bubble/CheckableImageButton;I)V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lael;->a:Landroid/content/Context;

    .line 184
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020067

    iget-object v2, p0, Lael;->a:Landroid/content/Context;

    .line 185
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/RippleDrawable;

    .line 186
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/RippleDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 187
    invoke-virtual {p1, v0}, Lcom/android/bubble/CheckableImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 188
    return-void
.end method

.method static final synthetic a(Lcom/android/bubble/CheckableImageButton;Lafj;)V
    .locals 1

    .prologue
    .line 280
    invoke-virtual {p1}, Lafj;->a()Landroid/graphics/drawable/Icon;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/bubble/CheckableImageButton;->setImageIcon(Landroid/graphics/drawable/Icon;)V

    .line 281
    invoke-virtual {p1}, Lafj;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/bubble/CheckableImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 282
    invoke-virtual {p1}, Lafj;->e()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/bubble/CheckableImageButton;->setChecked(Z)V

    .line 283
    invoke-virtual {p1}, Lafj;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/bubble/CheckableImageButton;->setEnabled(Z)V

    .line 284
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2
    :goto_0
    return v0

    .line 1
    :cond_1
    const/4 v0, 0x0

    .line 2
    goto :goto_0
.end method

.method private final b(Ljava/lang/Runnable;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 110
    iget v0, p0, Lael;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lael;->f:I

    if-ne v0, v5, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lael;->l:Laff;

    .line 114
    iget-object v1, v0, Laff;->f:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 115
    iget-object v1, v0, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 116
    iget-object v1, v0, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 117
    iget-object v1, v0, Laff;->c:Landroid/widget/ViewAnimator;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/ViewAnimator;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    iget-boolean v0, p0, Lael;->h:Z

    if-eqz v0, :cond_2

    .line 119
    iput-boolean v2, p0, Lael;->i:Z

    goto :goto_0

    .line 121
    :cond_2
    iget-object v0, p0, Lael;->m:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_3

    .line 122
    iput v2, p0, Lael;->k:I

    goto :goto_0

    .line 124
    :cond_3
    iget-boolean v0, p0, Lael;->g:Z

    if-eqz v0, :cond_4

    .line 125
    invoke-virtual {p0, v2, v4}, Lael;->a(IZ)V

    goto :goto_0

    .line 127
    :cond_4
    iput v5, p0, Lael;->f:I

    .line 128
    iget-object v0, p0, Lael;->l:Laff;

    .line 130
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 131
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    .line 132
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 133
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 134
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 135
    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    .line 136
    iget-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private final j()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lael;->l:Laff;

    .line 176
    iget-object v0, v0, Laff;->d:Landroid/widget/ImageView;

    .line 177
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 178
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {p0}, Lael;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 11
    iget v0, p0, Lael;->k:I

    if-ne v0, v4, :cond_0

    .line 12
    iput v2, p0, Lael;->k:I

    .line 13
    :cond_0
    iget v0, p0, Lael;->f:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lael;->f:I

    if-ne v0, v4, :cond_2

    .line 55
    :cond_1
    :goto_0
    return-void

    .line 15
    :cond_2
    iput-boolean v2, p0, Lael;->i:Z

    .line 16
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_3

    .line 17
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18
    const/16 v0, 0x7f6

    .line 20
    :goto_1
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const v2, 0x40228

    const/4 v3, -0x3

    invoke-direct {v1, v0, v2, v3}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    iput-object v1, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    .line 21
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 22
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lael;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 23
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lael;->e:Lafi;

    invoke-virtual {v1}, Lafi;->d()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 24
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 25
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 26
    :cond_3
    iget-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_5

    .line 27
    iget-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    .line 38
    :goto_2
    iget-object v0, p0, Lael;->l:Laff;

    .line 40
    iget-object v1, v0, Laff;->f:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 41
    iget-object v1, v0, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 42
    iget-object v1, v0, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    invoke-virtual {v1, v4}, Lcom/android/bubble/CheckableImageButton;->setClickable(Z)V

    .line 43
    iget-object v1, v0, Laff;->c:Landroid/widget/ViewAnimator;

    iget-object v0, v0, Laff;->a:Lafr;

    invoke-virtual {v1, v0}, Landroid/widget/ViewAnimator;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 44
    iput v4, p0, Lael;->f:I

    .line 45
    iget-object v0, p0, Lael;->l:Laff;

    .line 47
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 48
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Laep;

    invoke-direct {v1, p0}, Laep;-><init>(Lael;)V

    .line 52
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 54
    invoke-direct {p0}, Lael;->j()V

    goto/16 :goto_0

    .line 19
    :cond_4
    const/16 v0, 0x7d2

    goto/16 :goto_1

    .line 29
    :cond_5
    iget-object v0, p0, Lael;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lael;->l:Laff;

    .line 30
    iget-object v1, v1, Laff;->b:Lafu;

    .line 31
    iget-object v2, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    iget-object v0, p0, Lael;->l:Laff;

    .line 33
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 34
    invoke-virtual {v0, v5}, Landroid/widget/ViewAnimator;->setScaleX(F)V

    .line 35
    iget-object v0, p0, Lael;->l:Laff;

    .line 36
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 37
    invoke-virtual {v0, v5}, Landroid/widget/ViewAnimator;->setScaleY(F)V

    goto :goto_2
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lael;->l:Laff;

    .line 251
    iget-object v1, v0, Laff;->i:Landroid/view/View;

    .line 253
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lael;->m:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p0}, Lael;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lael;->n:Ljava/lang/Integer;

    .line 256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lael;->b(Z)V

    .line 257
    iget v0, p0, Lael;->k:I

    if-nez v0, :cond_2

    .line 258
    iput p1, p0, Lael;->k:I

    .line 259
    :cond_2
    iget-object v0, p0, Lael;->o:Lafd;

    if-eqz v0, :cond_3

    iget v0, p0, Lael;->k:I

    if-nez v0, :cond_3

    .line 260
    iget-object v0, p0, Lael;->o:Lafd;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p2}, Lafd;->a(IZ)V

    .line 262
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 263
    invoke-virtual {p0}, Lael;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lsj;

    invoke-direct {v1}, Lsj;-><init>()V

    .line 264
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Laev;

    invoke-direct {v1, p0}, Laev;-><init>(Lael;)V

    .line 265
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lael;->m:Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 255
    :cond_4
    const/4 v0, 0x3

    goto :goto_1

    .line 263
    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lael;->h:Z

    .line 64
    iget-boolean v0, p0, Lael;->g:Z

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0, v1, v1}, Lael;->a(IZ)V

    .line 66
    invoke-virtual {p0, p1}, Lael;->b(Ljava/lang/CharSequence;)V

    .line 78
    :goto_0
    iget-object v0, p0, Lael;->c:Landroid/os/Handler;

    iget-object v1, p0, Lael;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 79
    iget-object v0, p0, Lael;->c:Landroid/os/Handler;

    iget-object v1, p0, Lael;->s:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 80
    :goto_1
    return-void

    .line 67
    :cond_0
    new-instance v0, Lafm;

    invoke-direct {v0}, Lafm;-><init>()V

    .line 68
    new-instance v1, Landroid/transition/TransitionValues;

    invoke-direct {v1}, Landroid/transition/TransitionValues;-><init>()V

    .line 69
    iget-object v2, p0, Lael;->l:Laff;

    .line 70
    iget-object v2, v2, Laff;->c:Landroid/widget/ViewAnimator;

    .line 71
    iput-object v2, v1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 72
    iget-object v2, v1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0, v2}, Lafm;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 73
    invoke-virtual {v0, v1}, Lafm;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 74
    iget-object v2, v1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    iput-object p1, p0, Lael;->j:Ljava/lang/CharSequence;

    goto :goto_1

    .line 77
    :cond_1
    new-instance v2, Laes;

    invoke-direct {v2, p0, p1, v0, v1}, Laes;-><init>(Lael;Ljava/lang/CharSequence;Lafm;Landroid/transition/TransitionValues;)V

    invoke-virtual {p0, v2}, Lael;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Lael;->l:Laff;

    .line 219
    invoke-virtual {p0}, Lael;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    new-instance v1, Laff;

    .line 221
    iget-object v2, v0, Laff;->b:Lafu;

    .line 222
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Laff;-><init>(Lael;Landroid/content/Context;)V

    iput-object v1, p0, Lael;->l:Laff;

    .line 223
    invoke-virtual {p0}, Lael;->f()V

    .line 224
    iget-object v1, p0, Lael;->l:Laff;

    .line 226
    iget-object v1, v1, Laff;->c:Landroid/widget/ViewAnimator;

    .line 229
    iget-object v2, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 230
    invoke-virtual {v2}, Landroid/widget/ViewAnimator;->getDisplayedChild()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 231
    iget-object v1, p0, Lael;->l:Laff;

    .line 232
    iget-object v1, v1, Laff;->e:Landroid/widget/TextView;

    .line 234
    iget-object v2, v0, Laff;->e:Landroid/widget/TextView;

    .line 235
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :cond_0
    if-eqz p1, :cond_1

    .line 237
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 238
    :cond_1
    invoke-virtual {p0}, Lael;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 241
    iget-object v1, v0, Laff;->j:Landroid/view/View;

    .line 242
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v1, p0, Lael;->l:Laff;

    .line 244
    iget-object v1, v1, Laff;->b:Lafu;

    .line 246
    iget-object v2, p0, Lael;->b:Landroid/view/WindowManager;

    iget-object v3, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 247
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lafb;

    invoke-direct {v3, p0, v1, v0}, Lafb;-><init>(Lael;Landroid/view/ViewGroup;Laff;)V

    .line 248
    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 249
    :cond_2
    return-void
.end method

.method final a(Z)V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lael;->l:Laff;

    .line 82
    iget-object v0, v0, Laff;->a:Lafr;

    .line 83
    iget-boolean v0, v0, Lafr;->j:Z

    .line 84
    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lael;->l:Laff;

    .line 86
    iget-object v0, v0, Laff;->i:Landroid/view/View;

    .line 87
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lael;->l:Laff;

    .line 91
    iget-object v1, v0, Laff;->b:Lafu;

    .line 92
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutDirection(I)V

    .line 93
    iget-object v0, p0, Lael;->l:Laff;

    .line 94
    iget-object v0, v0, Laff;->b:Lafu;

    .line 95
    const v1, 0x7f0e00f9

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 96
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v0, v1

    .line 97
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz p1, :cond_2

    const/4 v2, 0x5

    :goto_2
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 98
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    iget-object v0, p0, Lael;->l:Laff;

    .line 101
    iget-object v1, v0, Laff;->i:Landroid/view/View;

    .line 102
    if-eqz p1, :cond_3

    .line 103
    const v0, 0x7f020063

    .line 105
    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 92
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 97
    :cond_2
    const/4 v2, 0x3

    goto :goto_2

    .line 104
    :cond_3
    const v0, 0x7f020062

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lael;->i:Z

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    new-instance v0, Laeq;

    invoke-direct {v0, p0}, Laeq;-><init>(Lael;)V

    invoke-direct {p0, v0}, Lael;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lael;->l:Laff;

    .line 204
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 205
    invoke-virtual {v0}, Landroid/widget/ViewAnimator;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 206
    iget-object v0, p0, Lael;->l:Laff;

    .line 207
    iget-object v0, v0, Laff;->e:Landroid/widget/TextView;

    .line 208
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lael;->l:Laff;

    .line 210
    iget-object v0, v0, Laff;->c:Landroid/widget/ViewAnimator;

    .line 211
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 212
    return-void
.end method

.method final b(Z)V
    .locals 3

    .prologue
    .line 268
    if-eqz p1, :cond_0

    .line 269
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x9

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 271
    :goto_0
    iget-object v0, p0, Lael;->b:Landroid/view/WindowManager;

    invoke-virtual {p0}, Lael;->e()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Laer;

    invoke-direct {v0, p0}, Laer;-><init>(Lael;)V

    invoke-direct {p0, v0}, Lael;->b(Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 62
    iget v1, p0, Lael;->f:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lael;->f:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lael;->f:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()Landroid/view/View;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lael;->l:Laff;

    .line 108
    iget-object v0, v0, Laff;->b:Lafu;

    .line 109
    return-object v0
.end method

.method public final f()V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 138
    iget-object v0, p0, Lael;->a:Landroid/content/Context;

    .line 139
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020068

    iget-object v4, p0, Lael;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/RippleDrawable;

    .line 140
    iget-object v3, p0, Lael;->a:Landroid/content/Context;

    const v4, 0x7f0c002a

    .line 141
    invoke-virtual {v3, v4}, Landroid/content/Context;->getColor(I)I

    move-result v3

    iget-object v4, p0, Lael;->e:Lafi;

    .line 142
    invoke-virtual {v4}, Lafi;->a()I

    move-result v4

    .line 143
    invoke-static {v3, v4}, Lmt;->a(II)I

    move-result v3

    .line 144
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/RippleDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 145
    iget-object v4, p0, Lael;->l:Laff;

    .line 146
    iget-object v4, v4, Laff;->c:Landroid/widget/ViewAnimator;

    .line 147
    invoke-virtual {v4, v0}, Landroid/widget/ViewAnimator;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 148
    iget-object v0, p0, Lael;->l:Laff;

    .line 149
    iget-object v0, v0, Laff;->f:Lcom/android/bubble/CheckableImageButton;

    .line 150
    invoke-direct {p0, v0, v3}, Lael;->a(Lcom/android/bubble/CheckableImageButton;I)V

    .line 151
    iget-object v0, p0, Lael;->l:Laff;

    .line 152
    iget-object v0, v0, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    .line 153
    invoke-direct {p0, v0, v3}, Lael;->a(Lcom/android/bubble/CheckableImageButton;I)V

    .line 154
    iget-object v0, p0, Lael;->l:Laff;

    .line 155
    iget-object v0, v0, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    .line 156
    invoke-direct {p0, v0, v3}, Lael;->a(Lcom/android/bubble/CheckableImageButton;I)V

    .line 157
    iget-object v0, p0, Lael;->e:Lafi;

    invoke-virtual {v0}, Lafi;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 158
    iget-object v0, p0, Lael;->l:Laff;

    .line 159
    iget-object v4, v0, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    .line 160
    const/4 v0, 0x3

    if-ge v3, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/android/bubble/CheckableImageButton;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lael;->l:Laff;

    .line 162
    iget-object v0, v0, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    .line 163
    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/bubble/CheckableImageButton;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lael;->l:Laff;

    .line 165
    iget-object v0, v0, Laff;->d:Landroid/widget/ImageView;

    .line 166
    iget-object v1, p0, Lael;->e:Lafi;

    invoke-virtual {v1}, Lafi;->b()Landroid/graphics/drawable/Icon;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageIcon(Landroid/graphics/drawable/Icon;)V

    .line 167
    invoke-direct {p0}, Lael;->j()V

    .line 168
    iget-object v0, p0, Lael;->l:Laff;

    .line 170
    iget-object v0, v0, Laff;->i:Landroid/view/View;

    .line 171
    iget-object v1, p0, Lael;->e:Lafi;

    .line 172
    invoke-virtual {v1}, Lafi;->a()I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 173
    invoke-virtual {p0}, Lael;->g()V

    .line 174
    return-void

    :cond_0
    move v0, v2

    .line 160
    goto :goto_0

    :cond_1
    move v1, v2

    .line 163
    goto :goto_1
.end method

.method public final g()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 189
    iget-object v0, p0, Lael;->e:Lafi;

    invoke-virtual {v0}, Lafi;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 190
    if-lez v1, :cond_0

    .line 191
    iget-object v0, p0, Lael;->e:Lafi;

    invoke-virtual {v0}, Lafi;->e()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafj;

    iget-object v2, p0, Lael;->l:Laff;

    .line 192
    iget-object v2, v2, Laff;->f:Lcom/android/bubble/CheckableImageButton;

    .line 193
    invoke-direct {p0, v0, v2}, Lael;->a(Lafj;Lcom/android/bubble/CheckableImageButton;)V

    .line 194
    if-lt v1, v3, :cond_0

    .line 195
    iget-object v0, p0, Lael;->e:Lafi;

    invoke-virtual {v0}, Lafi;->e()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafj;

    iget-object v2, p0, Lael;->l:Laff;

    .line 196
    iget-object v2, v2, Laff;->g:Lcom/android/bubble/CheckableImageButton;

    .line 197
    invoke-direct {p0, v0, v2}, Lael;->a(Lafj;Lcom/android/bubble/CheckableImageButton;)V

    .line 198
    const/4 v0, 0x3

    if-lt v1, v0, :cond_0

    .line 199
    iget-object v0, p0, Lael;->e:Lafi;

    invoke-virtual {v0}, Lafi;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafj;

    iget-object v1, p0, Lael;->l:Laff;

    .line 200
    iget-object v1, v1, Laff;->h:Lcom/android/bubble/CheckableImageButton;

    .line 201
    invoke-direct {p0, v0, v1}, Lael;->a(Lafj;Lcom/android/bubble/CheckableImageButton;)V

    .line 202
    :cond_0
    return-void
.end method

.method final h()Z
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lael;->d:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()V
    .locals 2

    .prologue
    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lael;->r:Landroid/view/ViewPropertyAnimator;

    .line 274
    iget-object v0, p0, Lael;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lael;->l:Laff;

    .line 275
    iget-object v1, v1, Laff;->b:Lafu;

    .line 276
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lael;->f:I

    .line 278
    invoke-direct {p0}, Lael;->j()V

    .line 279
    return-void
.end method
