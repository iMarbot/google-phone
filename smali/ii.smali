.class final Lii;
.super Ljy;
.source "PG"

# interfaces
.implements Ljo;


# instance fields
.field public final a:Ljc;

.field public b:Ljava/util/ArrayList;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:I

.field public l:I

.field public m:Ljava/lang/CharSequence;

.field public n:I

.field public o:Ljava/lang/CharSequence;

.field public p:Ljava/util/ArrayList;

.field public q:Ljava/util/ArrayList;

.field public r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Ljc;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljy;-><init>()V

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lii;->k:I

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lii;->r:Z

    .line 80
    iput-object p1, p0, Lii;->a:Ljc;

    .line 81
    return-void
.end method

.method private final a(ILip;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 94
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v1

    .line 96
    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 98
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " must be a public static class to be  properly recreated from instance state."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 99
    :cond_1
    iget-object v0, p0, Lii;->a:Ljc;

    iput-object v0, p2, Lip;->s:Ljc;

    .line 100
    if-eqz p3, :cond_3

    .line 101
    iget-object v0, p2, Lip;->z:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lip;->z:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change tag of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lip;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_2
    iput-object p3, p2, Lip;->z:Ljava/lang/String;

    .line 104
    :cond_3
    if-eqz p1, :cond_6

    .line 105
    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t add fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_4
    iget v0, p2, Lip;->x:I

    if-eqz v0, :cond_5

    iget v0, p2, Lip;->x:I

    if-eq v0, p1, :cond_5

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change container ID of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lip;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_5
    iput p1, p2, Lip;->x:I

    iput p1, p2, Lip;->y:I

    .line 110
    :cond_6
    new-instance v0, Lij;

    invoke-direct {v0, p4, p2}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 111
    return-void
.end method

.method private b(Z)I
    .locals 2

    .prologue
    .line 155
    iget-boolean v0, p0, Lii;->s:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "commit already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lii;->s:Z

    .line 157
    iget-boolean v0, p0, Lii;->i:Z

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lii;->a:Ljc;

    invoke-virtual {v0, p0}, Ljc;->a(Lii;)I

    move-result v0

    iput v0, p0, Lii;->k:I

    .line 160
    :goto_0
    iget-object v0, p0, Lii;->a:Ljc;

    invoke-virtual {v0, p0, p1}, Ljc;->a(Ljo;Z)V

    .line 161
    iget v0, p0, Lii;->k:I

    return v0

    .line 159
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lii;->k:I

    goto :goto_0
.end method

.method static b(Lij;)Z
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lij;->b:Lip;

    .line 331
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lip;->l:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lip;->I:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lip;->B:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lip;->A:Z

    if-nez v1, :cond_0

    .line 332
    invoke-virtual {v0}, Lip;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Ljy;
    .locals 2

    .prologue
    .line 135
    iget-boolean v0, p0, Lii;->i:Z

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This transaction is already being added to the back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lii;->b(Z)I

    move-result v0

    return v0
.end method

.method final a(Ljava/util/ArrayList;Lip;)Lip;
    .locals 10

    .prologue
    .line 270
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 271
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 272
    iget v2, v0, Lij;->a:I

    packed-switch v2, :pswitch_data_0

    .line 310
    :cond_0
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 273
    :pswitch_1
    iget-object v0, v0, Lij;->b:Lip;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 275
    :pswitch_2
    iget-object v2, v0, Lij;->b:Lip;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 276
    iget-object v2, v0, Lij;->b:Lip;

    if-ne v2, p2, :cond_0

    .line 277
    iget-object v2, p0, Lii;->b:Ljava/util/ArrayList;

    new-instance v3, Lij;

    const/16 v4, 0x9

    iget-object v0, v0, Lij;->b:Lip;

    invoke-direct {v3, v4, v0}, Lij;-><init>(ILip;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 278
    add-int/lit8 v1, v1, 0x1

    .line 279
    const/4 p2, 0x0

    goto :goto_1

    .line 280
    :pswitch_3
    iget-object v6, v0, Lij;->b:Lip;

    .line 281
    iget v7, v6, Lip;->y:I

    .line 282
    const/4 v4, 0x0

    .line 283
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    move-object v3, p2

    move v2, v1

    :goto_2
    if-ltz v5, :cond_3

    .line 284
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lip;

    .line 285
    iget v8, v1, Lip;->y:I

    if-ne v8, v7, :cond_6

    .line 286
    if-ne v1, v6, :cond_1

    .line 287
    const/4 v1, 0x1

    .line 300
    :goto_3
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    move v4, v1

    goto :goto_2

    .line 288
    :cond_1
    if-ne v1, v3, :cond_2

    .line 289
    iget-object v3, p0, Lii;->b:Ljava/util/ArrayList;

    new-instance v8, Lij;

    const/16 v9, 0x9

    invoke-direct {v8, v9, v1}, Lij;-><init>(ILip;)V

    invoke-virtual {v3, v2, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 290
    add-int/lit8 v2, v2, 0x1

    .line 291
    const/4 v3, 0x0

    .line 292
    :cond_2
    new-instance v8, Lij;

    const/4 v9, 0x3

    invoke-direct {v8, v9, v1}, Lij;-><init>(ILip;)V

    .line 293
    iget v9, v0, Lij;->c:I

    iput v9, v8, Lij;->c:I

    .line 294
    iget v9, v0, Lij;->e:I

    iput v9, v8, Lij;->e:I

    .line 295
    iget v9, v0, Lij;->d:I

    iput v9, v8, Lij;->d:I

    .line 296
    iget v9, v0, Lij;->f:I

    iput v9, v8, Lij;->f:I

    .line 297
    iget-object v9, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v9, v2, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 298
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 299
    add-int/lit8 v2, v2, 0x1

    move v1, v4

    goto :goto_3

    .line 301
    :cond_3
    if-eqz v4, :cond_4

    .line 302
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 303
    add-int/lit8 v1, v2, -0x1

    move-object p2, v3

    goto/16 :goto_1

    .line 304
    :cond_4
    const/4 v1, 0x1

    iput v1, v0, Lij;->a:I

    .line 305
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    move-object p2, v3

    .line 306
    goto/16 :goto_1

    .line 307
    :pswitch_4
    iget-object v2, p0, Lii;->b:Ljava/util/ArrayList;

    new-instance v3, Lij;

    const/16 v4, 0x9

    invoke-direct {v3, v4, p2}, Lij;-><init>(ILip;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 308
    add-int/lit8 v1, v1, 0x1

    .line 309
    iget-object p2, v0, Lij;->b:Lip;

    goto/16 :goto_1

    .line 311
    :cond_5
    return-object p2

    :cond_6
    move v1, v4

    goto :goto_3

    .line 272
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public final a(II)Ljy;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 128
    .line 129
    iput p1, p0, Lii;->c:I

    .line 130
    iput p2, p0, Lii;->d:I

    .line 131
    iput v0, p0, Lii;->e:I

    .line 132
    iput v0, p0, Lii;->f:I

    .line 134
    return-object p0
.end method

.method public final a(ILip;)Ljy;
    .locals 2

    .prologue
    .line 90
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lii;->a(ILip;Ljava/lang/String;I)V

    .line 91
    return-object p0
.end method

.method public final a(ILip;Ljava/lang/String;)Ljy;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lii;->a(ILip;Ljava/lang/String;I)V

    .line 93
    return-object p0
.end method

.method public final a(Lip;)Ljy;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lij;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 119
    return-object p0
.end method

.method public final a(Lip;Ljava/lang/String;)Ljy;
    .locals 2

    .prologue
    .line 88
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lii;->a(ILip;Ljava/lang/String;I)V

    .line 89
    return-object p0
.end method

.method final a(I)V
    .locals 4

    .prologue
    .line 138
    iget-boolean v0, p0, Lii;->i:Z

    if-nez v0, :cond_1

    .line 146
    :cond_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 141
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 142
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 143
    iget-object v3, v0, Lij;->b:Lip;

    if-eqz v3, :cond_2

    .line 144
    iget-object v0, v0, Lij;->b:Lip;

    iget v3, v0, Lip;->r:I

    add-int/2addr v3, p1

    iput v3, v0, Lip;->r:I

    .line 145
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method final a(Lij;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget v0, p0, Lii;->c:I

    iput v0, p1, Lij;->c:I

    .line 84
    iget v0, p0, Lii;->d:I

    iput v0, p1, Lij;->d:I

    .line 85
    iget v0, p0, Lii;->e:I

    iput v0, p1, Lij;->e:I

    .line 86
    iget v0, p0, Lii;->f:I

    iput v0, p1, Lij;->f:I

    .line 87
    return-void
.end method

.method final a(Lip$c;)V
    .locals 3

    .prologue
    .line 324
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 325
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 326
    invoke-static {v0}, Lii;->b(Lij;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    iget-object v0, v0, Lij;->b:Lip;

    invoke-virtual {v0, p1}, Lip;->a(Lip$c;)V

    .line 328
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 329
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    .line 12
    .line 14
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lii;->j:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 15
    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lii;->k:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 16
    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lii;->s:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 17
    iget v0, p0, Lii;->g:I

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 19
    iget v0, p0, Lii;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 20
    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 21
    iget v0, p0, Lii;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 22
    :cond_0
    iget v0, p0, Lii;->c:I

    if-nez v0, :cond_1

    iget v0, p0, Lii;->d:I

    if-eqz v0, :cond_2

    .line 23
    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 24
    iget v0, p0, Lii;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 25
    const-string v0, " mExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 26
    iget v0, p0, Lii;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 27
    :cond_2
    iget v0, p0, Lii;->e:I

    if-nez v0, :cond_3

    iget v0, p0, Lii;->f:I

    if-eqz v0, :cond_4

    .line 28
    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mPopEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 29
    iget v0, p0, Lii;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 30
    const-string v0, " mPopExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 31
    iget v0, p0, Lii;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 32
    :cond_4
    iget v0, p0, Lii;->l:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lii;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 33
    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 34
    iget v0, p0, Lii;->l:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 35
    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lii;->m:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 37
    :cond_6
    iget v0, p0, Lii;->n:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lii;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    .line 38
    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 39
    iget v0, p0, Lii;->n:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 40
    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lii;->o:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 42
    :cond_8
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 43
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 46
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_d

    .line 47
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 48
    iget v1, v0, Lij;->a:I

    packed-switch v1, :pswitch_data_0

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "cmd="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lij;->a:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  Op #"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 61
    const-string v4, ": "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 62
    const-string v1, " "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Lij;->b:Lip;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 64
    iget v1, v0, Lij;->c:I

    if-nez v1, :cond_9

    iget v1, v0, Lij;->d:I

    if-eqz v1, :cond_a

    .line 65
    :cond_9
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "enterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 66
    iget v1, v0, Lij;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 67
    const-string v1, " exitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 68
    iget v1, v0, Lij;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 69
    :cond_a
    iget v1, v0, Lij;->e:I

    if-nez v1, :cond_b

    iget v1, v0, Lij;->f:I

    if-eqz v1, :cond_c

    .line 70
    :cond_b
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "popEnterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 71
    iget v1, v0, Lij;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 72
    const-string v1, " popExitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 73
    iget v0, v0, Lij;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 74
    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 49
    :pswitch_0
    const-string v1, "NULL"

    goto :goto_1

    .line 50
    :pswitch_1
    const-string v1, "ADD"

    goto :goto_1

    .line 51
    :pswitch_2
    const-string v1, "REPLACE"

    goto :goto_1

    .line 52
    :pswitch_3
    const-string v1, "REMOVE"

    goto :goto_1

    .line 53
    :pswitch_4
    const-string v1, "HIDE"

    goto :goto_1

    .line 54
    :pswitch_5
    const-string v1, "SHOW"

    goto/16 :goto_1

    .line 55
    :pswitch_6
    const-string v1, "DETACH"

    goto/16 :goto_1

    .line 56
    :pswitch_7
    const-string v1, "ATTACH"

    goto/16 :goto_1

    .line 57
    :pswitch_8
    const-string v1, "SET_PRIMARY_NAV"

    goto/16 :goto_1

    .line 58
    :pswitch_9
    const-string v1, "UNSET_PRIMARY_NAV"

    goto/16 :goto_1

    .line 75
    :cond_d
    return-void

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method final a(Z)V
    .locals 5

    .prologue
    .line 235
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 236
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 237
    iget-object v2, v0, Lij;->b:Lip;

    .line 238
    if-eqz v2, :cond_0

    .line 239
    iget v3, p0, Lii;->g:I

    invoke-static {v3}, Ljc;->c(I)I

    move-result v3

    iget v4, p0, Lii;->h:I

    invoke-virtual {v2, v3, v4}, Lip;->a(II)V

    .line 240
    :cond_0
    iget v3, v0, Lij;->a:I

    packed-switch v3, :pswitch_data_0

    .line 263
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown cmd: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lij;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 241
    :pswitch_1
    iget v3, v0, Lij;->f:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 242
    iget-object v3, p0, Lii;->a:Ljc;

    invoke-virtual {v3, v2}, Ljc;->e(Lip;)V

    .line 264
    :goto_1
    iget-boolean v3, p0, Lii;->r:Z

    if-nez v3, :cond_1

    iget v0, v0, Lij;->a:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    if-eqz v2, :cond_1

    .line 265
    iget-object v0, p0, Lii;->a:Ljc;

    invoke-virtual {v0, v2}, Ljc;->c(Lip;)V

    .line 266
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 244
    :pswitch_2
    iget v3, v0, Lij;->e:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 245
    iget-object v3, p0, Lii;->a:Ljc;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Ljc;->a(Lip;Z)V

    goto :goto_1

    .line 247
    :pswitch_3
    iget v3, v0, Lij;->e:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 248
    invoke-static {v2}, Ljc;->g(Lip;)V

    goto :goto_1

    .line 250
    :pswitch_4
    iget v3, v0, Lij;->f:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 251
    invoke-static {v2}, Ljc;->f(Lip;)V

    goto :goto_1

    .line 253
    :pswitch_5
    iget v3, v0, Lij;->e:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 254
    iget-object v3, p0, Lii;->a:Ljc;

    invoke-virtual {v3, v2}, Ljc;->i(Lip;)V

    goto :goto_1

    .line 256
    :pswitch_6
    iget v3, v0, Lij;->f:I

    invoke-virtual {v2, v3}, Lip;->c(I)V

    .line 257
    iget-object v3, p0, Lii;->a:Ljc;

    invoke-virtual {v3, v2}, Ljc;->h(Lip;)V

    goto :goto_1

    .line 259
    :pswitch_7
    iget-object v3, p0, Lii;->a:Ljc;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljc;->j(Lip;)V

    goto :goto_1

    .line 261
    :pswitch_8
    iget-object v3, p0, Lii;->a:Ljc;

    invoke-virtual {v3, v2}, Ljc;->j(Lip;)V

    goto :goto_1

    .line 267
    :cond_2
    iget-boolean v0, p0, Lii;->r:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 268
    iget-object v0, p0, Lii;->a:Ljc;

    iget-object v1, p0, Lii;->a:Ljc;

    iget v1, v1, Ljc;->e:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljc;->a(IZ)V

    .line 269
    :cond_3
    return-void

    .line 240
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method final a(Ljava/util/ArrayList;II)Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 178
    if-ne p3, p2, :cond_0

    move v0, v3

    .line 198
    :goto_0
    return v0

    .line 180
    :cond_0
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 181
    const/4 v1, -0x1

    move v6, v3

    .line 182
    :goto_1
    if-ge v6, v7, :cond_6

    .line 183
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 184
    iget-object v2, v0, Lij;->b:Lip;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lij;->b:Lip;

    iget v2, v0, Lip;->y:I

    .line 185
    :goto_2
    if-eqz v2, :cond_7

    if-eq v2, v1, :cond_7

    move v5, p2

    .line 187
    :goto_3
    if-ge v5, p3, :cond_5

    .line 188
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 189
    iget-object v1, v0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v3

    .line 190
    :goto_4
    if-ge v4, v8, :cond_4

    .line 191
    iget-object v1, v0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lij;

    .line 192
    iget-object v9, v1, Lij;->b:Lip;

    if-eqz v9, :cond_2

    iget-object v1, v1, Lij;->b:Lip;

    iget v1, v1, Lip;->y:I

    .line 193
    :goto_5
    if-ne v1, v2, :cond_3

    .line 194
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 184
    goto :goto_2

    :cond_2
    move v1, v3

    .line 192
    goto :goto_5

    .line 195
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    .line 196
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    :cond_5
    move v0, v2

    .line 197
    :goto_6
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_1

    :cond_6
    move v0, v3

    .line 198
    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_6
.end method

.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-boolean v0, p0, Lii;->i:Z

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lii;->a:Ljc;

    .line 166
    iget-object v1, v0, Ljc;->d:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Ljc;->d:Ljava/util/ArrayList;

    .line 168
    :cond_0
    iget-object v0, v0, Ljc;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lii;->b(Z)I

    move-result v0

    return v0
.end method

.method final b(Ljava/util/ArrayList;Lip;)Lip;
    .locals 3

    .prologue
    .line 312
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 313
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 314
    iget v2, v0, Lij;->a:I

    packed-switch v2, :pswitch_data_0

    .line 322
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 315
    :pswitch_1
    iget-object v0, v0, Lij;->b:Lip;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 317
    :pswitch_2
    iget-object v0, v0, Lij;->b:Lip;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 319
    :pswitch_3
    iget-object p2, v0, Lij;->b:Lip;

    goto :goto_1

    .line 321
    :pswitch_4
    const/4 p2, 0x0

    goto :goto_1

    .line 323
    :cond_0
    return-object p2

    .line 314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final b(ILip;)Ljy;
    .locals 2

    .prologue
    .line 112
    .line 113
    if-nez p1, :cond_0

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must use non-zero containerViewId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, v0, v1}, Lii;->a(ILip;Ljava/lang/String;I)V

    .line 117
    return-object p0
.end method

.method public final b(Lip;)Ljy;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lij;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 121
    return-object p0
.end method

.method final b(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 171
    :goto_0
    if-ge v2, v3, :cond_2

    .line 172
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 173
    iget-object v4, v0, Lij;->b:Lip;

    if-eqz v4, :cond_0

    iget-object v0, v0, Lij;->b:Lip;

    iget v0, v0, Lip;->y:I

    .line 174
    :goto_1
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_1

    .line 175
    const/4 v0, 0x1

    .line 177
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 173
    goto :goto_1

    .line 176
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 177
    goto :goto_2
.end method

.method public final c(Lip;)Ljy;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Lij;

    const/4 v1, 0x5

    invoke-direct {v0, v1, p1}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 123
    return-object p0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 149
    invoke-direct {p0}, Lii;->f()Ljy;

    .line 150
    iget-object v0, p0, Lii;->a:Ljc;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljc;->b(Ljo;Z)V

    .line 151
    return-void
.end method

.method public final d(Lip;)Ljy;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lij;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p1}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 125
    return-object p0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 152
    invoke-direct {p0}, Lii;->f()Ljy;

    .line 153
    iget-object v0, p0, Lii;->a:Ljc;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Ljc;->b(Ljo;Z)V

    .line 154
    return-void
.end method

.method public final e(Lip;)Ljy;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lij;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p1}, Lij;-><init>(ILip;)V

    invoke-virtual {p0, v0}, Lii;->a(Lij;)V

    .line 127
    return-object p0
.end method

.method final e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 199
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 200
    :goto_0
    if-ge v1, v3, :cond_2

    .line 201
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 202
    iget-object v4, v0, Lij;->b:Lip;

    .line 203
    if-eqz v4, :cond_0

    .line 204
    iget v5, p0, Lii;->g:I

    iget v6, p0, Lii;->h:I

    invoke-virtual {v4, v5, v6}, Lip;->a(II)V

    .line 205
    :cond_0
    iget v5, v0, Lij;->a:I

    packed-switch v5, :pswitch_data_0

    .line 228
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown cmd: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lij;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :pswitch_1
    iget v5, v0, Lij;->c:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 207
    iget-object v5, p0, Lii;->a:Ljc;

    invoke-virtual {v5, v4, v2}, Ljc;->a(Lip;Z)V

    .line 229
    :goto_1
    iget-boolean v5, p0, Lii;->r:Z

    if-nez v5, :cond_1

    iget v0, v0, Lij;->a:I

    if-eq v0, v7, :cond_1

    if-eqz v4, :cond_1

    .line 230
    iget-object v0, p0, Lii;->a:Ljc;

    invoke-virtual {v0, v4}, Ljc;->c(Lip;)V

    .line 231
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 209
    :pswitch_2
    iget v5, v0, Lij;->d:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 210
    iget-object v5, p0, Lii;->a:Ljc;

    invoke-virtual {v5, v4}, Ljc;->e(Lip;)V

    goto :goto_1

    .line 212
    :pswitch_3
    iget v5, v0, Lij;->d:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 213
    invoke-static {v4}, Ljc;->f(Lip;)V

    goto :goto_1

    .line 215
    :pswitch_4
    iget v5, v0, Lij;->c:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 216
    invoke-static {v4}, Ljc;->g(Lip;)V

    goto :goto_1

    .line 218
    :pswitch_5
    iget v5, v0, Lij;->d:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 219
    iget-object v5, p0, Lii;->a:Ljc;

    invoke-virtual {v5, v4}, Ljc;->h(Lip;)V

    goto :goto_1

    .line 221
    :pswitch_6
    iget v5, v0, Lij;->c:I

    invoke-virtual {v4, v5}, Lip;->c(I)V

    .line 222
    iget-object v5, p0, Lii;->a:Ljc;

    invoke-virtual {v5, v4}, Ljc;->i(Lip;)V

    goto :goto_1

    .line 224
    :pswitch_7
    iget-object v5, p0, Lii;->a:Ljc;

    invoke-virtual {v5, v4}, Ljc;->j(Lip;)V

    goto :goto_1

    .line 226
    :pswitch_8
    iget-object v5, p0, Lii;->a:Ljc;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljc;->j(Lip;)V

    goto :goto_1

    .line 232
    :cond_2
    iget-boolean v0, p0, Lii;->r:Z

    if-nez v0, :cond_3

    .line 233
    iget-object v0, p0, Lii;->a:Ljc;

    iget-object v1, p0, Lii;->a:Ljc;

    iget v1, v1, Ljc;->e:I

    invoke-virtual {v0, v1, v7}, Ljc;->a(IZ)V

    .line 234
    :cond_3
    return-void

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2
    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4
    iget v1, p0, Lii;->k:I

    if-ltz v1, :cond_0

    .line 5
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6
    iget v1, p0, Lii;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 7
    :cond_0
    iget-object v1, p0, Lii;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    iget-object v1, p0, Lii;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
