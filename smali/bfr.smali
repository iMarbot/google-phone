.class final Lbfr;
.super Lbfp;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lbfp;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Lbfq;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3
    new-instance v2, Lbkg;

    invoke-direct {v2, v1}, Lbkg;-><init>(Landroid/content/res/Resources;)V

    .line 4
    iget-boolean v1, p2, Lbfq;->j:Z

    if-eqz v1, :cond_1

    move v1, v0

    .line 7
    :goto_0
    if-eqz p2, :cond_0

    .line 8
    iget-object v3, p2, Lbfq;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 9
    const/4 v3, 0x0

    iget-object v4, p2, Lbfq;->e:Ljava/lang/String;

    iget v5, p2, Lbfq;->g:I

    invoke-virtual {v2, v3, v4, v1, v5}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    .line 11
    :goto_1
    iget v1, p2, Lbfq;->h:F

    .line 12
    iput v1, v2, Lbkg;->a:F

    .line 13
    iget v1, p2, Lbfq;->i:F

    .line 14
    const/high16 v3, -0x41000000    # -0.5f

    cmpl-float v3, v1, v3

    if-ltz v3, :cond_3

    const/high16 v3, 0x3f000000    # 0.5f

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_3

    :goto_2
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 15
    iput v1, v2, Lbkg;->b:F

    .line 18
    :cond_0
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 19
    return-void

    .line 6
    :cond_1
    const/4 v1, 0x2

    goto :goto_0

    .line 10
    :cond_2
    iget-object v3, p2, Lbfq;->e:Ljava/lang/String;

    iget-object v4, p2, Lbfq;->f:Ljava/lang/String;

    iget v5, p2, Lbfq;->g:I

    invoke-virtual {v2, v3, v4, v1, v5}, Lbkg;->a(Ljava/lang/String;Ljava/lang/String;II)Lbkg;

    goto :goto_1

    .line 14
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
