.class final Ljz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:[I

.field private static b:Lkk;

.field private static c:Lkk;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 450
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ljz;->a:[I

    .line 451
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Lkf;

    invoke-direct {v0}, Lkf;-><init>()V

    :goto_0
    sput-object v0, Ljz;->b:Lkk;

    .line 452
    invoke-static {}, Ljz;->a()Lkk;

    move-result-object v0

    sput-object v0, Ljz;->c:Lkk;

    return-void

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 450
    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0x0
        0x1
        0x5
        0x4
        0x7
        0x6
        0x9
        0x8
    .end array-data
.end method

.method static a(Lpd;Lke;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    iget-object v0, p1, Lke;->c:Lii;

    .line 303
    if-eqz p2, :cond_1

    if-eqz p0, :cond_1

    iget-object v1, v0, Lii;->p:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lii;->p:Ljava/util/ArrayList;

    .line 304
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 305
    if-eqz p3, :cond_0

    iget-object v0, v0, Lii;->p:Ljava/util/ArrayList;

    .line 306
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 308
    :goto_0
    invoke-virtual {p0, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 309
    :goto_1
    return-object v0

    .line 306
    :cond_0
    iget-object v0, v0, Lii;->q:Ljava/util/ArrayList;

    .line 307
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 309
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lkk;Lip;Lip;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 229
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 230
    :cond_0
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    .line 231
    :cond_1
    if-eqz p3, :cond_2

    .line 232
    invoke-virtual {p2}, Lip;->A()Ljava/lang/Object;

    move-result-object v0

    .line 234
    :goto_1
    invoke-virtual {p0, v0}, Lkk;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 235
    invoke-virtual {p0, v0}, Lkk;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p1}, Lip;->z()Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lkk;Lip;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 236
    if-nez p1, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 241
    :goto_0
    return-object v0

    .line 238
    :cond_0
    if-eqz p2, :cond_1

    .line 239
    invoke-virtual {p1}, Lip;->y()Ljava/lang/Object;

    move-result-object v0

    .line 241
    :goto_1
    invoke-virtual {p0, v0}, Lkk;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 240
    :cond_1
    invoke-virtual {p1}, Lip;->v()Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lip;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 362
    .line 363
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    .line 364
    if-eqz p5, :cond_1

    .line 365
    iget-object v0, p4, Lip;->P:Lip$a;

    if-eqz v0, :cond_0

    iget-object v0, p4, Lip;->P:Lip$a;

    .line 373
    :cond_0
    :goto_0
    invoke-virtual {p0, p2, p1, p3}, Lkk;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 375
    return-object v0

    .line 369
    :cond_1
    iget-object v0, p4, Lip;->P:Lip$a;

    if-eqz v0, :cond_0

    iget-object v0, p4, Lip;->P:Lip$a;

    goto :goto_0
.end method

.method static a(Lkk;Ljava/lang/Object;Lip;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 342
    if-eqz p1, :cond_2

    .line 343
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 345
    iget-object v1, p2, Lip;->I:Landroid/view/View;

    .line 347
    if-eqz v1, :cond_0

    .line 348
    invoke-virtual {p0, v0, v1}, Lkk;->a(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 349
    :cond_0
    if-eqz p3, :cond_1

    .line 350
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 351
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 352
    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    invoke-virtual {p0, p1, v0}, Lkk;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 354
    :cond_2
    return-object v0
.end method

.method private static a(Lke;Landroid/util/SparseArray;I)Lke;
    .locals 0

    .prologue
    .line 445
    if-nez p0, :cond_0

    .line 446
    new-instance p0, Lke;

    invoke-direct {p0}, Lke;-><init>()V

    .line 447
    invoke-virtual {p1, p2, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 448
    :cond_0
    return-object p0
.end method

.method private static a()Lkk;
    .locals 2

    .prologue
    .line 1
    :try_start_0
    const-string v0, "fv"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkk;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lip;Lip;)Lkk;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 194
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 195
    if-eqz p0, :cond_2

    .line 196
    invoke-virtual {p0}, Lip;->x()Ljava/lang/Object;

    move-result-object v2

    .line 197
    if-eqz v2, :cond_0

    .line 198
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_0
    invoke-virtual {p0}, Lip;->w()Ljava/lang/Object;

    move-result-object v2

    .line 200
    if-eqz v2, :cond_1

    .line 201
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_1
    invoke-virtual {p0}, Lip;->A()Ljava/lang/Object;

    move-result-object v2

    .line 203
    if-eqz v2, :cond_2

    .line 204
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_2
    if-eqz p1, :cond_5

    .line 206
    invoke-virtual {p1}, Lip;->v()Ljava/lang/Object;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_3

    .line 208
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_3
    invoke-virtual {p1}, Lip;->y()Ljava/lang/Object;

    move-result-object v2

    .line 210
    if-eqz v2, :cond_4

    .line 211
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_4
    invoke-virtual {p1}, Lip;->z()Ljava/lang/Object;

    move-result-object v2

    .line 213
    if-eqz v2, :cond_5

    .line 214
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 223
    :cond_6
    :goto_0
    return-object v0

    .line 217
    :cond_7
    sget-object v2, Ljz;->b:Lkk;

    if-eqz v2, :cond_8

    sget-object v2, Ljz;->b:Lkk;

    invoke-static {v2, v1}, Ljz;->a(Lkk;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 218
    sget-object v0, Ljz;->b:Lkk;

    goto :goto_0

    .line 219
    :cond_8
    sget-object v2, Ljz;->c:Lkk;

    if-eqz v2, :cond_9

    sget-object v2, Ljz;->c:Lkk;

    invoke-static {v2, v1}, Ljz;->a(Lkk;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 220
    sget-object v0, Ljz;->c:Lkk;

    goto :goto_0

    .line 221
    :cond_9
    sget-object v1, Ljz;->b:Lkk;

    if-nez v1, :cond_a

    sget-object v1, Ljz;->c:Lkk;

    if-eqz v1, :cond_6

    .line 222
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Transition types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(ILjava/util/ArrayList;Ljava/util/ArrayList;II)Lpd;
    .locals 9

    .prologue
    .line 172
    new-instance v7, Lpd;

    invoke-direct {v7}, Lpd;-><init>()V

    .line 173
    add-int/lit8 v0, p4, -0x1

    move v6, v0

    :goto_0
    if-lt v6, p3, :cond_3

    .line 174
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lii;

    .line 175
    invoke-virtual {v0, p0}, Lii;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 177
    iget-object v2, v0, Lii;->p:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 178
    iget-object v2, v0, Lii;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 179
    if-eqz v1, :cond_0

    .line 180
    iget-object v1, v0, Lii;->p:Ljava/util/ArrayList;

    .line 181
    iget-object v0, v0, Lii;->q:Ljava/util/ArrayList;

    move-object v3, v1

    move-object v4, v0

    .line 184
    :goto_1
    const/4 v0, 0x0

    move v5, v0

    :goto_2
    if-ge v5, v8, :cond_2

    .line 185
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 186
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 187
    invoke-virtual {v7, v1}, Lpd;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 188
    if-eqz v2, :cond_1

    .line 189
    invoke-virtual {v7, v0, v2}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    :goto_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 182
    :cond_0
    iget-object v1, v0, Lii;->p:Ljava/util/ArrayList;

    .line 183
    iget-object v0, v0, Lii;->q:Ljava/util/ArrayList;

    move-object v3, v0

    move-object v4, v1

    goto :goto_1

    .line 190
    :cond_1
    invoke-virtual {v7, v0, v1}, Lpd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 192
    :cond_2
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto :goto_0

    .line 193
    :cond_3
    return-object v7
.end method

.method static a(Lkk;Lpd;Ljava/lang/Object;Lke;)Lpd;
    .locals 4

    .prologue
    .line 279
    iget-object v1, p3, Lke;->a:Lip;

    .line 281
    iget-object v2, v1, Lip;->I:Landroid/view/View;

    .line 283
    invoke-virtual {p1}, Lpd;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    if-nez v2, :cond_1

    .line 284
    :cond_0
    invoke-virtual {p1}, Lpd;->clear()V

    .line 285
    const/4 v0, 0x0

    .line 301
    :goto_0
    return-object v0

    .line 286
    :cond_1
    new-instance v0, Lpd;

    invoke-direct {v0}, Lpd;-><init>()V

    .line 287
    invoke-virtual {p0, v0, v2}, Lkk;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 288
    iget-object v3, p3, Lke;->c:Lii;

    .line 289
    iget-boolean v2, p3, Lke;->b:Z

    if-eqz v2, :cond_3

    .line 290
    invoke-virtual {v1}, Lip;->N()Llo;

    move-result-object v2

    .line 291
    iget-object v1, v3, Lii;->p:Ljava/util/ArrayList;

    .line 294
    :goto_1
    if-eqz v1, :cond_2

    .line 296
    invoke-static {v0, v1}, Lpl;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    .line 298
    :cond_2
    if-eqz v2, :cond_4

    .line 299
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 292
    :cond_3
    invoke-virtual {v1}, Lip;->M()Llo;

    move-result-object v2

    .line 293
    iget-object v1, v3, Lii;->q:Ljava/util/ArrayList;

    goto :goto_1

    .line 300
    :cond_4
    invoke-static {p1, v0}, Ljz;->a(Lpd;Lpd;)V

    goto :goto_0
.end method

.method private static a(Lii;Landroid/util/SparseArray;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 376
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 377
    :goto_0
    if-ge v1, v3, :cond_0

    .line 378
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 379
    invoke-static {p0, v0, p1, v2, p2}, Ljz;->a(Lii;Lij;Landroid/util/SparseArray;ZZ)V

    .line 380
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 381
    :cond_0
    return-void
.end method

.method private static a(Lii;Lij;Landroid/util/SparseArray;ZZ)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 390
    iget-object v1, p1, Lij;->b:Lip;

    .line 391
    if-nez v1, :cond_1

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget v9, v1, Lip;->y:I

    .line 394
    if-eqz v9, :cond_0

    .line 396
    if-eqz p3, :cond_5

    sget-object v0, Ljz;->a:[I

    iget v4, p1, Lij;->a:I

    aget v0, v0, v4

    .line 401
    :goto_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v4, v3

    move v6, v3

    move v7, v3

    move v5, v3

    .line 422
    :goto_2
    invoke-virtual {p2, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lke;

    .line 423
    if-eqz v5, :cond_11

    .line 425
    invoke-static {v0, p2, v9}, Ljz;->a(Lke;Landroid/util/SparseArray;I)Lke;

    move-result-object v8

    .line 426
    iput-object v1, v8, Lke;->a:Lip;

    .line 427
    iput-boolean p3, v8, Lke;->b:Z

    .line 428
    iput-object p0, v8, Lke;->c:Lii;

    .line 429
    :goto_3
    if-nez p4, :cond_3

    if-eqz v4, :cond_3

    .line 430
    if-eqz v8, :cond_2

    iget-object v0, v8, Lke;->d:Lip;

    if-ne v0, v1, :cond_2

    .line 431
    iput-object v10, v8, Lke;->d:Lip;

    .line 432
    :cond_2
    iget-object v0, p0, Lii;->a:Ljc;

    .line 433
    iget v4, v1, Lip;->c:I

    if-gtz v4, :cond_3

    iget v4, v0, Ljc;->e:I

    if-lez v4, :cond_3

    iget-boolean v4, p0, Lii;->r:Z

    if-nez v4, :cond_3

    .line 434
    invoke-virtual {v0, v1}, Ljc;->d(Lip;)V

    move v4, v3

    move v5, v3

    .line 435
    invoke-virtual/range {v0 .. v5}, Ljc;->a(Lip;IIIZ)V

    .line 436
    :cond_3
    if-eqz v6, :cond_10

    if-eqz v8, :cond_4

    iget-object v0, v8, Lke;->d:Lip;

    if-nez v0, :cond_10

    .line 438
    :cond_4
    invoke-static {v8, p2, v9}, Ljz;->a(Lke;Landroid/util/SparseArray;I)Lke;

    move-result-object v0

    .line 439
    iput-object v1, v0, Lke;->d:Lip;

    .line 440
    iput-boolean p3, v0, Lke;->e:Z

    .line 441
    iput-object p0, v0, Lke;->f:Lii;

    .line 442
    :goto_4
    if-nez p4, :cond_0

    if-eqz v7, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, v0, Lke;->a:Lip;

    if-ne v2, v1, :cond_0

    .line 443
    iput-object v10, v0, Lke;->a:Lip;

    goto :goto_0

    .line 396
    :cond_5
    iget v0, p1, Lij;->a:I

    goto :goto_1

    .line 402
    :pswitch_1
    if-eqz p4, :cond_7

    .line 403
    iget-boolean v0, v1, Lip;->R:Z

    if-eqz v0, :cond_6

    iget-boolean v0, v1, Lip;->A:Z

    if-nez v0, :cond_6

    iget-boolean v0, v1, Lip;->l:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    move v4, v2

    move v6, v3

    move v7, v3

    move v5, v0

    .line 406
    goto :goto_2

    :cond_6
    move v0, v3

    .line 403
    goto :goto_5

    .line 404
    :cond_7
    iget-boolean v0, v1, Lip;->A:Z

    goto :goto_5

    .line 407
    :pswitch_2
    if-eqz p4, :cond_8

    .line 408
    iget-boolean v0, v1, Lip;->Q:Z

    :goto_6
    move v4, v2

    move v6, v3

    move v7, v3

    move v5, v0

    .line 411
    goto :goto_2

    .line 409
    :cond_8
    iget-boolean v0, v1, Lip;->l:Z

    if-nez v0, :cond_9

    iget-boolean v0, v1, Lip;->A:Z

    if-nez v0, :cond_9

    move v0, v2

    goto :goto_6

    :cond_9
    move v0, v3

    goto :goto_6

    .line 412
    :pswitch_3
    if-eqz p4, :cond_b

    .line 413
    iget-boolean v0, v1, Lip;->R:Z

    if-eqz v0, :cond_a

    iget-boolean v0, v1, Lip;->l:Z

    if-eqz v0, :cond_a

    iget-boolean v0, v1, Lip;->A:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_7
    move v4, v3

    move v6, v0

    move v7, v2

    move v5, v3

    .line 416
    goto/16 :goto_2

    :cond_a
    move v0, v3

    .line 413
    goto :goto_7

    .line 414
    :cond_b
    iget-boolean v0, v1, Lip;->l:Z

    if-eqz v0, :cond_c

    iget-boolean v0, v1, Lip;->A:Z

    if-nez v0, :cond_c

    move v0, v2

    goto :goto_7

    :cond_c
    move v0, v3

    goto :goto_7

    .line 417
    :pswitch_4
    if-eqz p4, :cond_e

    .line 418
    iget-boolean v0, v1, Lip;->l:Z

    if-nez v0, :cond_d

    iget-object v0, v1, Lip;->I:Landroid/view/View;

    if-eqz v0, :cond_d

    iget-object v0, v1, Lip;->I:Landroid/view/View;

    .line 419
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    iget v0, v1, Lip;->S:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_d

    move v0, v2

    :goto_8
    move v4, v3

    move v6, v0

    move v7, v2

    move v5, v3

    .line 421
    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 419
    goto :goto_8

    .line 420
    :cond_e
    iget-boolean v0, v1, Lip;->l:Z

    if-eqz v0, :cond_f

    iget-boolean v0, v1, Lip;->A:Z

    if-nez v0, :cond_f

    move v0, v2

    goto :goto_8

    :cond_f
    move v0, v3

    goto :goto_8

    :cond_10
    move-object v0, v8

    goto/16 :goto_4

    :cond_11
    move-object v8, v0

    goto/16 :goto_3

    .line 401
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lip;Lip;ZLpd;Z)V
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Ljz;->b(Lip;Lip;ZLpd;Z)V

    return-void
.end method

.method static a(Ljava/util/ArrayList;I)V
    .locals 2

    .prologue
    .line 355
    if-nez p0, :cond_1

    .line 361
    :cond_0
    return-void

    .line 357
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 358
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 359
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 360
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Lpd;Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 248
    invoke-virtual {p1}, Lpd;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 249
    invoke-virtual {p1, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 251
    sget-object v2, Lqy;->a:Lri;

    invoke-virtual {v2, v0}, Lri;->s(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 252
    invoke-interface {p2, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 255
    :cond_1
    return-void
.end method

.method static a(Ljc;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V
    .locals 42

    .prologue
    .line 5
    move-object/from16 v0, p0

    iget v4, v0, Ljc;->e:I

    if-gtz v4, :cond_1

    .line 171
    :cond_0
    return-void

    .line 7
    :cond_1
    new-instance v40, Landroid/util/SparseArray;

    invoke-direct/range {v40 .. v40}, Landroid/util/SparseArray;-><init>()V

    move/from16 v6, p3

    .line 8
    :goto_0
    move/from16 v0, p4

    if-ge v6, v0, :cond_3

    .line 9
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lii;

    .line 10
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 11
    if-eqz v5, :cond_2

    .line 12
    move-object/from16 v0, v40

    move/from16 v1, p5

    invoke-static {v4, v0, v1}, Ljz;->b(Lii;Landroid/util/SparseArray;Z)V

    .line 14
    :goto_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 13
    :cond_2
    move-object/from16 v0, v40

    move/from16 v1, p5

    invoke-static {v4, v0, v1}, Ljz;->a(Lii;Landroid/util/SparseArray;Z)V

    goto :goto_1

    .line 15
    :cond_3
    invoke-virtual/range {v40 .. v40}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 16
    new-instance v21, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Ljc;->f:Liz;

    .line 17
    iget-object v4, v4, Liz;->b:Landroid/content/Context;

    .line 18
    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-virtual/range {v40 .. v40}, Landroid/util/SparseArray;->size()I

    move-result v41

    .line 20
    const/4 v4, 0x0

    move/from16 v39, v4

    :goto_2
    move/from16 v0, v39

    move/from16 v1, v41

    if-ge v0, v1, :cond_0

    .line 21
    move-object/from16 v0, v40

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 22
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-static {v5, v0, v1, v2, v3}, Ljz;->a(ILjava/util/ArrayList;Ljava/util/ArrayList;II)Lpd;

    move-result-object v17

    .line 24
    move-object/from16 v0, v40

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lke;

    .line 25
    if-eqz p5, :cond_11

    .line 27
    const/4 v4, 0x0

    .line 28
    move-object/from16 v0, p0

    iget-object v6, v0, Ljc;->g:Lix;

    invoke-virtual {v6}, Lix;->a()Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 29
    move-object/from16 v0, p0

    iget-object v4, v0, Ljc;->g:Lix;

    invoke-virtual {v4, v5}, Lix;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    move-object/from16 v18, v4

    .line 30
    :goto_3
    if-eqz v18, :cond_8

    .line 31
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->a:Lip;

    move-object/from16 v16, v0

    .line 32
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->d:Lip;

    move-object/from16 v20, v0

    .line 33
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Ljz;->a(Lip;Lip;)Lkk;

    move-result-object v4

    .line 34
    if-eqz v4, :cond_8

    .line 35
    move-object/from16 v0, v19

    iget-boolean v0, v0, Lke;->b:Z

    move/from16 v22, v0

    .line 36
    move-object/from16 v0, v19

    iget-boolean v5, v0, Lke;->e:Z

    .line 37
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 38
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 39
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-static {v4, v0, v1}, Ljz;->a(Lkk;Lip;Z)Ljava/lang/Object;

    move-result-object v24

    .line 40
    move-object/from16 v0, v20

    invoke-static {v4, v0, v5}, Ljz;->b(Lkk;Lip;Z)Ljava/lang/Object;

    move-result-object v6

    .line 42
    move-object/from16 v0, v19

    iget-object v13, v0, Lke;->a:Lip;

    .line 43
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->d:Lip;

    move-object/from16 v25, v0

    .line 44
    if-eqz v13, :cond_4

    .line 46
    iget-object v5, v13, Lip;->I:Landroid/view/View;

    .line 47
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 48
    :cond_4
    if-eqz v13, :cond_5

    if-nez v25, :cond_9

    .line 49
    :cond_5
    const/4 v7, 0x0

    .line 86
    :goto_4
    if-nez v24, :cond_6

    if-nez v7, :cond_6

    if-eqz v6, :cond_8

    .line 87
    :cond_6
    move-object/from16 v0, v20

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-static {v4, v6, v0, v1, v2}, Ljz;->a(Lkk;Ljava/lang/Object;Lip;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v13

    .line 88
    move-object/from16 v0, v24

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-static {v4, v0, v1, v15, v2}, Ljz;->a(Lkk;Ljava/lang/Object;Lip;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v11

    .line 89
    const/4 v5, 0x4

    invoke-static {v11, v5}, Ljz;->a(Ljava/util/ArrayList;I)V

    move-object/from16 v5, v24

    move-object/from16 v8, v16

    move/from16 v9, v22

    .line 90
    invoke-static/range {v4 .. v9}, Ljz;->a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lip;Z)Ljava/lang/Object;

    move-result-object v9

    .line 91
    if-eqz v9, :cond_8

    .line 93
    if-eqz v20, :cond_7

    if-eqz v6, :cond_7

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lip;->l:Z

    if-eqz v5, :cond_7

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lip;->A:Z

    if-eqz v5, :cond_7

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lip;->R:Z

    if-eqz v5, :cond_7

    .line 94
    const/4 v5, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lip;->e(Z)V

    .line 97
    move-object/from16 v0, v20

    iget-object v5, v0, Lip;->I:Landroid/view/View;

    .line 99
    invoke-virtual {v4, v6, v5, v13}, Lkk;->b(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    .line 100
    move-object/from16 v0, v20

    iget-object v5, v0, Lip;->H:Landroid/view/ViewGroup;

    .line 101
    new-instance v8, Lka;

    invoke-direct {v8, v13}, Lka;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v5, v8}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    .line 103
    :cond_7
    invoke-static {v15}, Lkk;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v16

    move-object v8, v4

    move-object/from16 v10, v24

    move-object v12, v6

    move-object v14, v7

    .line 104
    invoke-virtual/range {v8 .. v15}, Lkk;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 105
    move-object/from16 v0, v18

    invoke-virtual {v4, v0, v9}, Lkk;->a(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    move-object v12, v4

    move-object/from16 v13, v18

    move-object/from16 v14, v23

    .line 106
    invoke-virtual/range {v12 .. v17}, Lkk;->a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;)V

    .line 107
    const/4 v5, 0x0

    invoke-static {v11, v5}, Ljz;->a(Ljava/util/ArrayList;I)V

    .line 108
    move-object/from16 v0, v23

    invoke-virtual {v4, v7, v0, v15}, Lkk;->a(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 170
    :cond_8
    :goto_5
    add-int/lit8 v4, v39, 0x1

    move/from16 v39, v4

    goto/16 :goto_2

    .line 50
    :cond_9
    move-object/from16 v0, v19

    iget-boolean v10, v0, Lke;->b:Z

    .line 51
    invoke-virtual/range {v17 .. v17}, Lpd;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v5, 0x0

    .line 53
    :goto_6
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v4, v0, v5, v1}, Ljz;->b(Lkk;Lpd;Ljava/lang/Object;Lke;)Lpd;

    move-result-object v7

    .line 54
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v4, v0, v5, v1}, Ljz;->a(Lkk;Lpd;Ljava/lang/Object;Lke;)Lpd;

    move-result-object v11

    .line 55
    invoke-virtual/range {v17 .. v17}, Lpd;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 56
    const/4 v5, 0x0

    .line 57
    if-eqz v7, :cond_a

    .line 58
    invoke-virtual {v7}, Lpd;->clear()V

    .line 59
    :cond_a
    if-eqz v11, :cond_b

    .line 60
    invoke-virtual {v11}, Lpd;->clear()V

    .line 67
    :cond_b
    :goto_7
    if-nez v24, :cond_e

    if-nez v6, :cond_e

    if-nez v5, :cond_e

    .line 68
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 52
    :cond_c
    move-object/from16 v0, v25

    invoke-static {v4, v13, v0, v10}, Ljz;->a(Lkk;Lip;Lip;Z)Ljava/lang/Object;

    move-result-object v5

    goto :goto_6

    .line 62
    :cond_d
    invoke-virtual/range {v17 .. v17}, Lpd;->keySet()Ljava/util/Set;

    move-result-object v8

    .line 63
    move-object/from16 v0, v23

    invoke-static {v0, v7, v8}, Ljz;->a(Ljava/util/ArrayList;Lpd;Ljava/util/Collection;)V

    .line 65
    invoke-virtual/range {v17 .. v17}, Lpd;->values()Ljava/util/Collection;

    move-result-object v8

    .line 66
    invoke-static {v15, v11, v8}, Ljz;->a(Ljava/util/ArrayList;Lpd;Ljava/util/Collection;)V

    goto :goto_7

    .line 69
    :cond_e
    const/4 v8, 0x1

    move-object/from16 v0, v25

    invoke-static {v13, v0, v10, v7, v8}, Ljz;->b(Lip;Lip;ZLpd;Z)V

    .line 70
    if-eqz v5, :cond_10

    .line 71
    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v4, v5, v0, v1}, Lkk;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    .line 73
    move-object/from16 v0, v19

    iget-boolean v8, v0, Lke;->e:Z

    .line 74
    move-object/from16 v0, v19

    iget-object v9, v0, Lke;->f:Lii;

    .line 75
    invoke-static/range {v4 .. v9}, Ljz;->a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Lpd;ZLii;)V

    .line 76
    new-instance v14, Landroid/graphics/Rect;

    invoke-direct {v14}, Landroid/graphics/Rect;-><init>()V

    .line 77
    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-static {v11, v0, v1, v10}, Ljz;->a(Lpd;Lke;Ljava/lang/Object;Z)Landroid/view/View;

    move-result-object v12

    .line 78
    if-eqz v12, :cond_f

    .line 79
    move-object/from16 v0, v24

    invoke-virtual {v4, v0, v14}, Lkk;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 83
    :cond_f
    :goto_8
    new-instance v7, Lkc;

    move-object v8, v13

    move-object/from16 v9, v25

    move-object v13, v4

    invoke-direct/range {v7 .. v14}, Lkc;-><init>(Lip;Lip;ZLpd;Landroid/view/View;Lkk;Landroid/graphics/Rect;)V

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    move-object v7, v5

    .line 84
    goto/16 :goto_4

    .line 81
    :cond_10
    const/4 v14, 0x0

    .line 82
    const/4 v12, 0x0

    goto :goto_8

    .line 111
    :cond_11
    const/4 v4, 0x0

    .line 112
    move-object/from16 v0, p0

    iget-object v6, v0, Ljc;->g:Lix;

    invoke-virtual {v6}, Lix;->a()Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 113
    move-object/from16 v0, p0

    iget-object v4, v0, Ljc;->g:Lix;

    invoke-virtual {v4, v5}, Lix;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    move-object v12, v4

    .line 114
    :goto_9
    if-eqz v12, :cond_8

    .line 115
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->a:Lip;

    move-object/from16 v29, v0

    .line 116
    move-object/from16 v0, v19

    iget-object v10, v0, Lke;->d:Lip;

    .line 117
    move-object/from16 v0, v29

    invoke-static {v10, v0}, Ljz;->a(Lip;Lip;)Lkk;

    move-result-object v4

    .line 118
    if-eqz v4, :cond_8

    .line 119
    move-object/from16 v0, v19

    iget-boolean v5, v0, Lke;->b:Z

    .line 120
    move-object/from16 v0, v19

    iget-boolean v6, v0, Lke;->e:Z

    .line 121
    move-object/from16 v0, v29

    invoke-static {v4, v0, v5}, Ljz;->a(Lkk;Lip;Z)Ljava/lang/Object;

    move-result-object v26

    .line 122
    invoke-static {v4, v10, v6}, Ljz;->b(Lkk;Lip;Z)Ljava/lang/Object;

    move-result-object v6

    .line 123
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 124
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 126
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->a:Lip;

    move-object/from16 v22, v0

    .line 127
    move-object/from16 v0, v19

    iget-object v0, v0, Lke;->d:Lip;

    move-object/from16 v23, v0

    .line 128
    if-eqz v22, :cond_12

    if-nez v23, :cond_15

    .line 129
    :cond_12
    const/16 v28, 0x0

    .line 154
    :goto_a
    if-nez v26, :cond_13

    if-nez v28, :cond_13

    if-eqz v6, :cond_8

    .line 155
    :cond_13
    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-static {v4, v6, v10, v0, v1}, Ljz;->a(Lkk;Ljava/lang/Object;Lip;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v9

    .line 156
    if-eqz v9, :cond_14

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 157
    :cond_14
    const/16 v27, 0x0

    .line 158
    :goto_b
    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lkk;->b(Ljava/lang/Object;Landroid/view/View;)V

    .line 159
    move-object/from16 v0, v19

    iget-boolean v0, v0, Lke;->b:Z

    move/from16 v30, v0

    move-object/from16 v25, v4

    invoke-static/range {v25 .. v30}, Ljz;->a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lip;Z)Ljava/lang/Object;

    move-result-object v5

    .line 160
    if-eqz v5, :cond_8

    .line 161
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v6, v26

    move-object/from16 v8, v27

    move-object/from16 v10, v28

    move-object/from16 v11, v20

    .line 162
    invoke-virtual/range {v4 .. v11}, Lkk;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 164
    new-instance v30, Lkb;

    move-object/from16 v31, v26

    move-object/from16 v32, v4

    move-object/from16 v33, v21

    move-object/from16 v34, v29

    move-object/from16 v35, v20

    move-object/from16 v36, v7

    move-object/from16 v37, v9

    move-object/from16 v38, v27

    invoke-direct/range {v30 .. v38}, Lkb;-><init>(Ljava/lang/Object;Lkk;Landroid/view/View;Lip;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;)V

    move-object/from16 v0, v30

    invoke-static {v12, v0}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    .line 166
    new-instance v6, Lkm;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v6, v4, v0, v1}, Lkm;-><init>(Lkk;Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-static {v12, v6}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    .line 167
    invoke-virtual {v4, v12, v5}, Lkk;->a(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    .line 169
    new-instance v5, Lkn;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v5, v4, v0, v1}, Lkn;-><init>(Lkk;Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-static {v12, v5}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    goto/16 :goto_5

    .line 130
    :cond_15
    move-object/from16 v0, v19

    iget-boolean v0, v0, Lke;->b:Z

    move/from16 v24, v0

    .line 131
    invoke-virtual/range {v17 .. v17}, Lpd;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_16

    const/4 v5, 0x0

    .line 133
    :goto_c
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v4, v0, v5, v1}, Ljz;->b(Lkk;Lpd;Ljava/lang/Object;Lke;)Lpd;

    move-result-object v7

    .line 134
    invoke-virtual/range {v17 .. v17}, Lpd;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_17

    .line 135
    const/4 v5, 0x0

    .line 137
    :goto_d
    if-nez v26, :cond_18

    if-nez v6, :cond_18

    if-nez v5, :cond_18

    .line 138
    const/16 v28, 0x0

    goto/16 :goto_a

    .line 132
    :cond_16
    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v4, v0, v1, v2}, Ljz;->a(Lkk;Lip;Lip;Z)Ljava/lang/Object;

    move-result-object v5

    goto :goto_c

    .line 136
    :cond_17
    invoke-virtual {v7}, Lpd;->values()Ljava/util/Collection;

    move-result-object v8

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_d

    .line 139
    :cond_18
    const/4 v8, 0x1

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v2, v7, v8}, Ljz;->b(Lip;Lip;ZLpd;Z)V

    .line 140
    if-eqz v5, :cond_1a

    .line 141
    new-instance v27, Landroid/graphics/Rect;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/Rect;-><init>()V

    .line 142
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v4, v5, v0, v1}, Lkk;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    .line 143
    move-object/from16 v0, v19

    iget-boolean v8, v0, Lke;->e:Z

    .line 144
    move-object/from16 v0, v19

    iget-object v9, v0, Lke;->f:Lii;

    .line 145
    invoke-static/range {v4 .. v9}, Ljz;->a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Lpd;ZLii;)V

    .line 146
    if-eqz v26, :cond_19

    .line 147
    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Lkk;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    .line 151
    :cond_19
    :goto_e
    new-instance v15, Lkd;

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    invoke-direct/range {v15 .. v27}, Lkd;-><init>(Lkk;Lpd;Ljava/lang/Object;Lke;Ljava/util/ArrayList;Landroid/view/View;Lip;Lip;ZLjava/util/ArrayList;Ljava/lang/Object;Landroid/graphics/Rect;)V

    invoke-static {v12, v15}, Lln;->a(Landroid/view/View;Ljava/lang/Runnable;)Lln;

    move-object/from16 v28, v5

    .line 152
    goto/16 :goto_a

    .line 149
    :cond_1a
    const/16 v27, 0x0

    goto :goto_e

    :cond_1b
    move-object/from16 v27, v6

    goto/16 :goto_b

    :cond_1c
    move-object v12, v4

    goto/16 :goto_9

    :cond_1d
    move-object/from16 v18, v4

    goto/16 :goto_3
.end method

.method private static a(Lkk;Ljava/lang/Object;Ljava/lang/Object;Lpd;ZLii;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 310
    iget-object v0, p5, Lii;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p5, Lii;->p:Ljava/util/ArrayList;

    .line 311
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    if-eqz p4, :cond_1

    iget-object v0, p5, Lii;->q:Ljava/util/ArrayList;

    .line 313
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 315
    :goto_0
    invoke-virtual {p3, v0}, Lpd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 316
    invoke-virtual {p0, p1, v0}, Lkk;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 317
    if-eqz p2, :cond_0

    .line 318
    invoke-virtual {p0, p2, v0}, Lkk;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 319
    :cond_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p5, Lii;->p:Ljava/util/ArrayList;

    .line 314
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Lpd;Lpd;)V
    .locals 2

    .prologue
    .line 320
    invoke-virtual {p0}, Lpd;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 321
    invoke-virtual {p0, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 322
    invoke-virtual {p1, v0}, Lpd;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    invoke-virtual {p0, v1}, Lpd;->d(I)Ljava/lang/Object;

    .line 324
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 325
    :cond_1
    return-void
.end method

.method private static a(Lkk;Ljava/util/List;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 224
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 225
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lkk;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    :goto_1
    return v0

    .line 227
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static b(Lkk;Lip;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 242
    if-nez p1, :cond_0

    .line 243
    const/4 v0, 0x0

    .line 247
    :goto_0
    return-object v0

    .line 244
    :cond_0
    if-eqz p2, :cond_1

    .line 245
    invoke-virtual {p1}, Lip;->w()Ljava/lang/Object;

    move-result-object v0

    .line 247
    :goto_1
    invoke-virtual {p0, v0}, Lkk;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 246
    :cond_1
    invoke-virtual {p1}, Lip;->x()Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method private static b(Lkk;Lpd;Ljava/lang/Object;Lke;)Lpd;
    .locals 4

    .prologue
    .line 256
    invoke-virtual {p1}, Lpd;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 257
    :cond_0
    invoke-virtual {p1}, Lpd;->clear()V

    .line 258
    const/4 v0, 0x0

    .line 278
    :goto_0
    return-object v0

    .line 259
    :cond_1
    iget-object v0, p3, Lke;->d:Lip;

    .line 260
    new-instance v2, Lpd;

    invoke-direct {v2}, Lpd;-><init>()V

    .line 262
    iget-object v1, v0, Lip;->I:Landroid/view/View;

    .line 263
    invoke-virtual {p0, v2, v1}, Lkk;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 264
    iget-object v3, p3, Lke;->f:Lii;

    .line 265
    iget-boolean v1, p3, Lke;->e:Z

    if-eqz v1, :cond_2

    .line 266
    invoke-virtual {v0}, Lip;->M()Llo;

    move-result-object v1

    .line 267
    iget-object v0, v3, Lii;->q:Ljava/util/ArrayList;

    .line 271
    :goto_1
    invoke-static {v2, v0}, Lpl;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    .line 273
    if-eqz v1, :cond_3

    .line 274
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 268
    :cond_2
    invoke-virtual {v0}, Lip;->N()Llo;

    move-result-object v1

    .line 269
    iget-object v0, v3, Lii;->p:Ljava/util/ArrayList;

    goto :goto_1

    .line 275
    :cond_3
    invoke-virtual {v2}, Lpd;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 276
    invoke-static {p1, v0}, Lpl;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    move-object v0, v2

    .line 278
    goto :goto_0
.end method

.method private static b(Lii;Landroid/util/SparseArray;Z)V
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Lii;->a:Ljc;

    iget-object v0, v0, Ljc;->g:Lix;

    invoke-virtual {v0}, Lix;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 389
    :cond_0
    return-void

    .line 384
    :cond_1
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 385
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 386
    iget-object v0, p0, Lii;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij;

    .line 387
    const/4 v2, 0x1

    invoke-static {p0, v0, p1, v2, p2}, Ljz;->a(Lii;Lij;Landroid/util/SparseArray;ZZ)V

    .line 388
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method private static b(Lip;Lip;ZLpd;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 326
    if-eqz p2, :cond_0

    .line 327
    invoke-virtual {p1}, Lip;->M()Llo;

    move-result-object v0

    .line 329
    :goto_0
    if-eqz v0, :cond_4

    .line 330
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 331
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 332
    if-nez p3, :cond_1

    move v0, v1

    .line 333
    :goto_1
    if-ge v1, v0, :cond_2

    .line 334
    invoke-virtual {p3, v1}, Lpd;->b(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    invoke-virtual {p3, v1}, Lpd;->c(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 328
    :cond_0
    invoke-virtual {p0}, Lip;->M()Llo;

    move-result-object v0

    goto :goto_0

    .line 332
    :cond_1
    invoke-virtual {p3}, Lpd;->size()I

    move-result v0

    goto :goto_1

    .line 337
    :cond_2
    if-eqz p4, :cond_3

    .line 338
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 339
    :cond_3
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    .line 340
    :cond_4
    return-void
.end method
