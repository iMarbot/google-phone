.class public final Lfrc;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final FORMAT_KEY_CROP_BOTTOM:Ljava/lang/String; = "crop-bottom"

.field public static final FORMAT_KEY_CROP_LEFT:Ljava/lang/String; = "crop-left"

.field public static final FORMAT_KEY_CROP_RIGHT:Ljava/lang/String; = "crop-right"

.field public static final FORMAT_KEY_CROP_TOP:Ljava/lang/String; = "crop-top"

.field public static final H264_CODEC_TYPE:I = 0x1

.field public static final H264_HARDWARE_SUPPORT_FLAG:I = 0x2

.field public static final H264_MIME_TYPE:Ljava/lang/String; = "video/avc"

.field public static final INVALID_CODEC_TYPE:I = -0x1

.field public static final SW_PREFIXES:[Ljava/lang/String;

.field public static final VP8_CODEC_TYPE:I = 0x0

.field public static final VP8_HARDWARE_SUPPORT_FLAG:I = 0x1

.field public static final VP8_MIME_TYPE:Ljava/lang/String; = "video/x-vnd.on2.vp8"

.field public static gservicesAccessorFactory:Lfpp;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "OMX.google."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "OMX.SEC."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "OMX.Intel.VideoEncoder.VP8"

    aput-object v2, v0, v1

    sput-object v0, Lfrc;->SW_PREFIXES:[Ljava/lang/String;

    .line 77
    new-instance v0, Lfpp;

    invoke-direct {v0}, Lfpp;-><init>()V

    sput-object v0, Lfrc;->gservicesAccessorFactory:Lfpp;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static doesCodecSupportHd(Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0, p1}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v1

    .line 52
    if-nez v1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    invoke-virtual {v1}, Landroid/media/MediaCodecInfo$CodecCapabilities;->getVideoCapabilities()Landroid/media/MediaCodecInfo$VideoCapabilities;

    move-result-object v1

    .line 55
    if-eqz v1, :cond_0

    const/16 v2, 0x500

    const/16 v3, 0x2d0

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/media/MediaCodecInfo$VideoCapabilities;->areSizeAndRateSupported(IID)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static doesCodecSupportMimeType(Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 71
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 72
    aget-object v3, v2, v0

    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 73
    const/4 v1, 0x1

    .line 75
    :cond_0
    return v1

    .line 74
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static getCodecInfoForMimeType(Lfpo;Ljava/lang/String;Z)Landroid/media/MediaCodecInfo;
    .locals 4

    .prologue
    .line 58
    :try_start_0
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v2

    .line 59
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 60
    invoke-static {v1}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v0

    .line 61
    invoke-static {v0, p1}, Lfrc;->doesCodecSupportMimeType(Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v3

    if-ne v3, p2, :cond_0

    .line 63
    invoke-static {p0, v0, p1}, Lfrc;->isHardwareAcceleratedCodec(Lfpo;Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    :goto_1
    return-object v0

    .line 65
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "Failed to query MediaCodecList:"

    invoke-static {v1, v0}, Lfvh;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getSupportedHardwareAcceleratedCodecs(Landroid/content/Context;Z)I
    .locals 2

    .prologue
    .line 2
    const/4 v0, 0x0

    .line 3
    const-string v1, "video/x-vnd.on2.vp8"

    invoke-static {p0, v1, p1}, Lfrc;->supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4
    const/4 v0, 0x1

    .line 5
    :cond_0
    const-string v1, "video/avc"

    invoke-static {p0, v1, p1}, Lfrc;->supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6
    or-int/lit8 v0, v0, 0x2

    .line 7
    :cond_1
    return v0
.end method

.method static isBlockedByGservicesFlags(Lfpo;Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19
    if-eqz p2, :cond_0

    .line 20
    const-string v0, "babel_hangout_hardware_encode"

    .line 22
    :goto_0
    invoke-virtual {p0, v0, v1}, Lfpo;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 36
    :goto_1
    return v0

    .line 21
    :cond_0
    const-string v0, "babel_hangout_hardware_decode"

    goto :goto_0

    .line 24
    :cond_1
    const-string v0, "video/x-vnd.on2.vp8"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    if-eqz p2, :cond_2

    .line 26
    const-string v0, "babel_hangout_vp8_hardware_encode"

    .line 28
    :goto_2
    invoke-virtual {p0, v0, v1}, Lfpo;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 29
    goto :goto_1

    .line 27
    :cond_2
    const-string v0, "babel_hangout_vp8_hardware_decode"

    goto :goto_2

    .line 30
    :cond_3
    const-string v0, "video/avc"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 31
    if-eqz p2, :cond_4

    .line 32
    const-string v0, "babel_hangout_h264_hardware_encode2"

    .line 34
    :goto_3
    invoke-virtual {p0, v0, v1}, Lfpo;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 35
    goto :goto_1

    .line 33
    :cond_4
    const-string v0, "babel_hangout_h264_hardware_decode2"

    goto :goto_3

    .line 36
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static isHardwareAcceleratedCodec(Lfpo;Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 44
    const-string v1, "babel_hangout_allow_software_media_codec"

    invoke-virtual {p0, v1, v0}, Lfpo;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 45
    invoke-virtual {p1}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfrc;->isKnownSoftwareImplementation(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 48
    invoke-static {p1, p2}, Lfrc;->doesCodecSupportHd(Landroid/media/MediaCodecInfo;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isKnownSoftwareImplementation(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 37
    if-nez p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    sget-object v2, Lfrc;->SW_PREFIXES:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 40
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 41
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static supportsHardwareAcceleration(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v1, v2, :cond_1

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 10
    :cond_1
    if-eqz p2, :cond_2

    .line 11
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 13
    const-string v1, "video/x-vnd.on2.vp8"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 15
    :cond_2
    sget-object v1, Lfrc;->gservicesAccessorFactory:Lfpp;

    invoke-virtual {v1, p0}, Lfpp;->createAccessor(Landroid/content/Context;)Lfpo;

    move-result-object v1

    .line 16
    invoke-static {v1, p1, p2}, Lfrc;->isBlockedByGservicesFlags(Lfpo;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 18
    invoke-static {v1, p1, p2}, Lfrc;->getCodecInfoForMimeType(Lfpo;Ljava/lang/String;Z)Landroid/media/MediaCodecInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
