.class final Leoe;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/gms/googlehelp/GoogleHelp;

.field private synthetic b:Lenp;

.field private synthetic c:Landroid/app/Activity;

.field private synthetic d:Landroid/content/Intent;

.field private synthetic e:Lenx;


# direct methods
.method constructor <init>(Lenx;Lcom/google/android/gms/googlehelp/GoogleHelp;Lenp;Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Leoe;->e:Lenx;

    iput-object p2, p0, Leoe;->a:Lcom/google/android/gms/googlehelp/GoogleHelp;

    iput-object p3, p0, Leoe;->b:Lenp;

    iput-object p4, p0, Leoe;->c:Landroid/app/Activity;

    iput-object p5, p0, Leoe;->d:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Leoe;->e:Lenx;

    invoke-virtual {v0}, Lenx;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "gH_GoogleHelpApiImpl"

    const-string v1, "Getting sync help psd timed out."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "gms:googlehelp:sync_help_psd_failure"

    const-string v1, "timeout"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {v0}, Letf;->d(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Leoe;->a:Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/util/List;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    iget-object v0, p0, Leoe;->b:Lenp;

    iget-object v1, p0, Leoe;->c:Landroid/app/Activity;

    iget-object v2, p0, Leoe;->d:Landroid/content/Intent;

    iget-object v3, p0, Leoe;->a:Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-static {v0, v1, v2, v3}, Lenx;->a(Lenp;Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/gms/googlehelp/GoogleHelp;)V

    goto :goto_0
.end method
