.class public final Lhsl;
.super Lhft;
.source "PG"


# instance fields
.field public a:[Lhra;

.field public b:[Lhrb;

.field public c:[Lhqt;

.field public d:[Lhsr;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 2
    invoke-static {}, Lhra;->a()[Lhra;

    move-result-object v0

    iput-object v0, p0, Lhsl;->a:[Lhra;

    .line 3
    invoke-static {}, Lhrb;->a()[Lhrb;

    move-result-object v0

    iput-object v0, p0, Lhsl;->b:[Lhrb;

    .line 4
    invoke-static {}, Lhqt;->a()[Lhqt;

    move-result-object v0

    iput-object v0, p0, Lhsl;->c:[Lhqt;

    .line 5
    invoke-static {}, Lhsr;->a()[Lhsr;

    move-result-object v0

    iput-object v0, p0, Lhsl;->d:[Lhsr;

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lhsl;->e:Ljava/lang/Integer;

    .line 7
    const/4 v0, -0x1

    iput v0, p0, Lhsl;->cachedSize:I

    .line 8
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 38
    iget-object v2, p0, Lhsl;->a:[Lhra;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhsl;->a:[Lhra;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 39
    :goto_0
    iget-object v3, p0, Lhsl;->a:[Lhra;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 40
    iget-object v3, p0, Lhsl;->a:[Lhra;

    aget-object v3, v3, v0

    .line 41
    if-eqz v3, :cond_0

    .line 42
    const/4 v4, 0x1

    .line 43
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 44
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 45
    :cond_2
    iget-object v2, p0, Lhsl;->b:[Lhrb;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhsl;->b:[Lhrb;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 46
    :goto_1
    iget-object v3, p0, Lhsl;->b:[Lhrb;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 47
    iget-object v3, p0, Lhsl;->b:[Lhrb;

    aget-object v3, v3, v0

    .line 48
    if-eqz v3, :cond_3

    .line 49
    const/4 v4, 0x2

    .line 50
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 51
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 52
    :cond_5
    iget-object v2, p0, Lhsl;->c:[Lhqt;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lhsl;->c:[Lhqt;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 53
    :goto_2
    iget-object v3, p0, Lhsl;->c:[Lhqt;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 54
    iget-object v3, p0, Lhsl;->c:[Lhqt;

    aget-object v3, v3, v0

    .line 55
    if-eqz v3, :cond_6

    .line 56
    const/4 v4, 0x3

    .line 57
    invoke-static {v4, v3}, Lhfq;->d(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 58
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    move v0, v2

    .line 59
    :cond_8
    iget-object v2, p0, Lhsl;->d:[Lhsr;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lhsl;->d:[Lhsr;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 60
    :goto_3
    iget-object v2, p0, Lhsl;->d:[Lhsr;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 61
    iget-object v2, p0, Lhsl;->d:[Lhsr;

    aget-object v2, v2, v1

    .line 62
    if-eqz v2, :cond_9

    .line 63
    const/4 v3, 0x4

    .line 64
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 65
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 66
    :cond_a
    iget-object v1, p0, Lhsl;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 67
    const/4 v1, 0x5

    iget-object v2, p0, Lhsl;->e:Ljava/lang/Integer;

    .line 68
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lhfq;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_b
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 72
    sparse-switch v0, :sswitch_data_0

    .line 74
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    const/16 v0, 0xa

    .line 77
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 78
    iget-object v0, p0, Lhsl;->a:[Lhra;

    if-nez v0, :cond_2

    move v0, v1

    .line 79
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhra;

    .line 80
    if-eqz v0, :cond_1

    .line 81
    iget-object v3, p0, Lhsl;->a:[Lhra;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 82
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 83
    new-instance v3, Lhra;

    invoke-direct {v3}, Lhra;-><init>()V

    aput-object v3, v2, v0

    .line 84
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 85
    invoke-virtual {p1}, Lhfp;->a()I

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 78
    :cond_2
    iget-object v0, p0, Lhsl;->a:[Lhra;

    array-length v0, v0

    goto :goto_1

    .line 87
    :cond_3
    new-instance v3, Lhra;

    invoke-direct {v3}, Lhra;-><init>()V

    aput-object v3, v2, v0

    .line 88
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 89
    iput-object v2, p0, Lhsl;->a:[Lhra;

    goto :goto_0

    .line 91
    :sswitch_2
    const/16 v0, 0x12

    .line 92
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 93
    iget-object v0, p0, Lhsl;->b:[Lhrb;

    if-nez v0, :cond_5

    move v0, v1

    .line 94
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhrb;

    .line 95
    if-eqz v0, :cond_4

    .line 96
    iget-object v3, p0, Lhsl;->b:[Lhrb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 98
    new-instance v3, Lhrb;

    invoke-direct {v3}, Lhrb;-><init>()V

    aput-object v3, v2, v0

    .line 99
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 100
    invoke-virtual {p1}, Lhfp;->a()I

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 93
    :cond_5
    iget-object v0, p0, Lhsl;->b:[Lhrb;

    array-length v0, v0

    goto :goto_3

    .line 102
    :cond_6
    new-instance v3, Lhrb;

    invoke-direct {v3}, Lhrb;-><init>()V

    aput-object v3, v2, v0

    .line 103
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 104
    iput-object v2, p0, Lhsl;->b:[Lhrb;

    goto/16 :goto_0

    .line 106
    :sswitch_3
    const/16 v0, 0x1a

    .line 107
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 108
    iget-object v0, p0, Lhsl;->c:[Lhqt;

    if-nez v0, :cond_8

    move v0, v1

    .line 109
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhqt;

    .line 110
    if-eqz v0, :cond_7

    .line 111
    iget-object v3, p0, Lhsl;->c:[Lhqt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    .line 113
    new-instance v3, Lhqt;

    invoke-direct {v3}, Lhqt;-><init>()V

    aput-object v3, v2, v0

    .line 114
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 115
    invoke-virtual {p1}, Lhfp;->a()I

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 108
    :cond_8
    iget-object v0, p0, Lhsl;->c:[Lhqt;

    array-length v0, v0

    goto :goto_5

    .line 117
    :cond_9
    new-instance v3, Lhqt;

    invoke-direct {v3}, Lhqt;-><init>()V

    aput-object v3, v2, v0

    .line 118
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 119
    iput-object v2, p0, Lhsl;->c:[Lhqt;

    goto/16 :goto_0

    .line 121
    :sswitch_4
    const/16 v0, 0x22

    .line 122
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 123
    iget-object v0, p0, Lhsl;->d:[Lhsr;

    if-nez v0, :cond_b

    move v0, v1

    .line 124
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lhsr;

    .line 125
    if-eqz v0, :cond_a

    .line 126
    iget-object v3, p0, Lhsl;->d:[Lhsr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    .line 128
    new-instance v3, Lhsr;

    invoke-direct {v3}, Lhsr;-><init>()V

    aput-object v3, v2, v0

    .line 129
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 130
    invoke-virtual {p1}, Lhfp;->a()I

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 123
    :cond_b
    iget-object v0, p0, Lhsl;->d:[Lhsr;

    array-length v0, v0

    goto :goto_7

    .line 132
    :cond_c
    new-instance v3, Lhsr;

    invoke-direct {v3}, Lhsr;-><init>()V

    aput-object v3, v2, v0

    .line 133
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 134
    iput-object v2, p0, Lhsl;->d:[Lhsr;

    goto/16 :goto_0

    .line 137
    :sswitch_5
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 138
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lhsl;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 72
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9
    iget-object v0, p0, Lhsl;->a:[Lhra;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhsl;->a:[Lhra;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 10
    :goto_0
    iget-object v2, p0, Lhsl;->a:[Lhra;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 11
    iget-object v2, p0, Lhsl;->a:[Lhra;

    aget-object v2, v2, v0

    .line 12
    if-eqz v2, :cond_0

    .line 13
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_1
    iget-object v0, p0, Lhsl;->b:[Lhrb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhsl;->b:[Lhrb;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 16
    :goto_1
    iget-object v2, p0, Lhsl;->b:[Lhrb;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 17
    iget-object v2, p0, Lhsl;->b:[Lhrb;

    aget-object v2, v2, v0

    .line 18
    if-eqz v2, :cond_2

    .line 19
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 20
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 21
    :cond_3
    iget-object v0, p0, Lhsl;->c:[Lhqt;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhsl;->c:[Lhqt;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 22
    :goto_2
    iget-object v2, p0, Lhsl;->c:[Lhqt;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 23
    iget-object v2, p0, Lhsl;->c:[Lhqt;

    aget-object v2, v2, v0

    .line 24
    if-eqz v2, :cond_4

    .line 25
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lhfq;->b(ILhfz;)V

    .line 26
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 27
    :cond_5
    iget-object v0, p0, Lhsl;->d:[Lhsr;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhsl;->d:[Lhsr;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 28
    :goto_3
    iget-object v0, p0, Lhsl;->d:[Lhsr;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 29
    iget-object v0, p0, Lhsl;->d:[Lhsr;

    aget-object v0, v0, v1

    .line 30
    if-eqz v0, :cond_6

    .line 31
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 32
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 33
    :cond_7
    iget-object v0, p0, Lhsl;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 34
    const/4 v0, 0x5

    iget-object v1, p0, Lhsl;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lhfq;->a(II)V

    .line 35
    :cond_8
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 36
    return-void
.end method
