.class public final Lbud;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/support/design/widget/FloatingActionButton;

.field public b:I

.field public c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/support/design/widget/FloatingActionButton;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lbud;->b:I

    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 4
    const v1, 0x10c000d

    .line 5
    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    iput-object v1, p0, Lbud;->g:Landroid/view/animation/Interpolator;

    .line 6
    const v1, 0x7f0d0147

    .line 7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lbud;->e:I

    .line 8
    const v1, 0x7f0d0145

    .line 9
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lbud;->f:I

    .line 10
    const v1, 0x7f0f0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lbud;->d:I

    .line 11
    iput-object p2, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 12
    return-void
.end method

.method private final a(I)I
    .locals 3

    .prologue
    .line 39
    packed-switch p1, :pswitch_data_0

    .line 45
    const/16 v0, 0x24

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "Invalid alignment value: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbdf;->c(Ljava/lang/String;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 40
    :pswitch_0
    const/4 v0, 0x0

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 41
    :pswitch_1
    iget v0, p0, Lbud;->c:I

    div-int/lit8 v0, v0, 0x4

    .line 47
    :goto_1
    iget-object v1, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v1}, Landroid/support/design/widget/FloatingActionButton;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 48
    neg-int v0, v0

    goto :goto_0

    .line 43
    :pswitch_2
    iget v0, p0, Lbud;->c:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lbud;->e:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lbud;->f:I

    sub-int/2addr v0, v1

    .line 44
    goto :goto_1

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lbud;->a(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setTranslationX(F)V

    .line 25
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 26
    .line 27
    iget v0, p0, Lbud;->c:I

    if-eqz v0, :cond_0

    .line 28
    invoke-direct {p0, p1}, Lbud;->a(I)I

    move-result v0

    .line 29
    if-eqz p2, :cond_1

    iget-object v1, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v1}, Landroid/support/design/widget/FloatingActionButton;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30
    iget-object v1, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v1}, Landroid/support/design/widget/FloatingActionButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    .line 31
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lbud;->g:Landroid/view/animation/Interpolator;

    .line 33
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lbud;->d:I

    int-to-long v2, v1

    .line 34
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    iget-object v1, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/support/design/widget/FloatingActionButton;->setTranslationX(F)V

    .line 37
    iget-object v0, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, v2}, Landroid/support/design/widget/FloatingActionButton;->setTranslationY(F)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 13
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 17
    invoke-virtual {v0, v2, v1}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    .line 23
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lbud;->a:Landroid/support/design/widget/FloatingActionButton;

    .line 22
    invoke-virtual {v0, v2, v1}, Landroid/support/design/widget/FloatingActionButton;->b(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;Z)V

    goto :goto_0
.end method
