.class public final Lbao$a;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final j:Lbao$a;

.field private static volatile k:Lhdm;


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:I

.field public e:J

.field public f:J

.field public g:J

.field public h:Lhce;

.field public i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 133
    new-instance v0, Lbao$a;

    invoke-direct {v0}, Lbao$a;-><init>()V

    .line 134
    sput-object v0, Lbao$a;->j:Lbao$a;

    invoke-virtual {v0}, Lbao$a;->makeImmutable()V

    .line 135
    const-class v0, Lbao$a;

    sget-object v1, Lbao$a;->j:Lbao$a;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 136
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    invoke-static {}, Lbao$a;->emptyProtobufList()Lhce;

    move-result-object v0

    iput-object v0, p0, Lbao$a;->h:Lhce;

    .line 3
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 130
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lbjo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "i"

    aput-object v2, v0, v1

    .line 131
    const-string v1, "\u0001\u0008\u0000\u0001\u0001\u0008\u0000\u0001\u0000\u0001\u0002\u0000\u0002\u0004\u0001\u0003\u0004\u0002\u0004\u0002\u0003\u0005\u0002\u0004\u0006\u0002\u0005\u0007\u001b\u0008\u0007\u0006"

    .line 132
    sget-object v2, Lbao$a;->j:Lbao$a;

    invoke-static {v2, v1, v0}, Lbao$a;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 65
    :pswitch_0
    new-instance v0, Lbao$a;

    invoke-direct {v0}, Lbao$a;-><init>()V

    .line 128
    :goto_0
    :pswitch_1
    return-object v0

    .line 66
    :pswitch_2
    sget-object v0, Lbao$a;->j:Lbao$a;

    goto :goto_0

    .line 67
    :pswitch_3
    iget-object v1, p0, Lbao$a;->h:Lhce;

    invoke-interface {v1}, Lhce;->b()V

    goto :goto_0

    .line 69
    :pswitch_4
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v1, v1}, Lhbr$a;-><init>(BI)V

    goto :goto_0

    .line 70
    :pswitch_5
    check-cast p2, Lhaq;

    .line 71
    check-cast p3, Lhbg;

    .line 72
    if-nez p3, :cond_0

    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 74
    :cond_0
    :try_start_0
    sget-boolean v0, Lbao$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0, p2, p3}, Lbao$a;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 76
    sget-object v0, Lbao$a;->j:Lbao$a;

    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 78
    :cond_1
    :goto_1
    if-nez v1, :cond_3

    .line 79
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 80
    sparse-switch v0, :sswitch_data_0

    .line 83
    invoke-virtual {p0, v0, p2}, Lbao$a;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 84
    goto :goto_1

    .line 85
    :sswitch_1
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbao$a;->a:I

    .line 86
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbao$a;->b:J
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    throw v0

    .line 88
    :sswitch_2
    :try_start_2
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lbao$a;->a:I

    .line 89
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lbao$a;->c:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 117
    :catch_1
    move-exception v0

    .line 118
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 119
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 91
    :sswitch_3
    :try_start_4
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lbao$a;->a:I

    .line 92
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v0

    iput v0, p0, Lbao$a;->d:I

    goto :goto_1

    .line 94
    :sswitch_4
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lbao$a;->a:I

    .line 95
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbao$a;->e:J

    goto :goto_1

    .line 97
    :sswitch_5
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lbao$a;->a:I

    .line 98
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbao$a;->f:J

    goto :goto_1

    .line 100
    :sswitch_6
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lbao$a;->a:I

    .line 101
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v4

    iput-wide v4, p0, Lbao$a;->g:J

    goto :goto_1

    .line 103
    :sswitch_7
    iget-object v0, p0, Lbao$a;->h:Lhce;

    invoke-interface {v0}, Lhce;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    iget-object v0, p0, Lbao$a;->h:Lhce;

    .line 105
    invoke-static {v0}, Lhbr;->mutableCopy(Lhce;)Lhce;

    move-result-object v0

    iput-object v0, p0, Lbao$a;->h:Lhce;

    .line 106
    :cond_2
    iget-object v3, p0, Lbao$a;->h:Lhce;

    .line 107
    sget-object v0, Lbjo;->g:Lbjo;

    .line 109
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lbjo;

    invoke-interface {v3, v0}, Lhce;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 111
    :sswitch_8
    iget v0, p0, Lbao$a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lbao$a;->a:I

    .line 112
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v0

    iput-boolean v0, p0, Lbao$a;->i:Z
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 121
    :cond_3
    :pswitch_6
    sget-object v0, Lbao$a;->j:Lbao$a;

    goto/16 :goto_0

    .line 122
    :pswitch_7
    sget-object v0, Lbao$a;->k:Lhdm;

    if-nez v0, :cond_5

    const-class v1, Lbao$a;

    monitor-enter v1

    .line 123
    :try_start_5
    sget-object v0, Lbao$a;->k:Lhdm;

    if-nez v0, :cond_4

    .line 124
    new-instance v0, Lhaa;

    sget-object v2, Lbao$a;->j:Lbao$a;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lbao$a;->k:Lhdm;

    .line 125
    :cond_4
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 126
    :cond_5
    sget-object v0, Lbao$a;->k:Lhdm;

    goto/16 :goto_0

    .line 125
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 127
    :pswitch_8
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 30
    iget v0, p0, Lbao$a;->memoizedSerializedSize:I

    .line 31
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 63
    :goto_0
    return v0

    .line 32
    :cond_0
    sget-boolean v0, Lbao$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 33
    invoke-virtual {p0}, Lbao$a;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lbao$a;->memoizedSerializedSize:I

    .line 34
    iget v0, p0, Lbao$a;->memoizedSerializedSize:I

    goto :goto_0

    .line 36
    :cond_1
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 37
    iget-wide v2, p0, Lbao$a;->b:J

    .line 38
    invoke-static {v4, v2, v3}, Lhaw;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 39
    :goto_1
    iget v2, p0, Lbao$a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    .line 40
    iget v2, p0, Lbao$a;->c:I

    .line 41
    invoke-static {v5, v2}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 42
    :cond_2
    iget v2, p0, Lbao$a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_3

    .line 43
    const/4 v2, 0x3

    iget v3, p0, Lbao$a;->d:I

    .line 44
    invoke-static {v2, v3}, Lhaw;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 45
    :cond_3
    iget v2, p0, Lbao$a;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v7, :cond_4

    .line 46
    iget-wide v2, p0, Lbao$a;->e:J

    .line 47
    invoke-static {v6, v2, v3}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 48
    :cond_4
    iget v2, p0, Lbao$a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    .line 49
    const/4 v2, 0x5

    iget-wide v4, p0, Lbao$a;->f:J

    .line 50
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 51
    :cond_5
    iget v2, p0, Lbao$a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 52
    const/4 v2, 0x6

    iget-wide v4, p0, Lbao$a;->g:J

    .line 53
    invoke-static {v2, v4, v5}, Lhaw;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    move v2, v0

    .line 54
    :goto_2
    iget-object v0, p0, Lbao$a;->h:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 55
    const/4 v3, 0x7

    iget-object v0, p0, Lbao$a;->h:Lhce;

    .line 56
    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-static {v3, v0}, Lhaw;->c(ILhdd;)I

    move-result v0

    add-int/2addr v0, v2

    .line 57
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 58
    :cond_7
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 59
    iget-boolean v0, p0, Lbao$a;->i:Z

    .line 60
    invoke-static {v7, v0}, Lhaw;->b(IZ)I

    move-result v0

    add-int/2addr v2, v0

    .line 61
    :cond_8
    iget-object v0, p0, Lbao$a;->unknownFields:Lheq;

    invoke-virtual {v0}, Lheq;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 62
    iput v0, p0, Lbao$a;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4
    sget-boolean v0, Lbao$a;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 5
    invoke-virtual {p0, p1}, Lbao$a;->writeToInternal(Lhaw;)V

    .line 29
    :goto_0
    return-void

    .line 7
    :cond_0
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 8
    iget-wide v0, p0, Lbao$a;->b:J

    .line 9
    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 10
    :cond_1
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 11
    iget v0, p0, Lbao$a;->c:I

    invoke-virtual {p1, v3, v0}, Lhaw;->b(II)V

    .line 12
    :cond_2
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 13
    const/4 v0, 0x3

    iget v1, p0, Lbao$a;->d:I

    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 14
    :cond_3
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 15
    iget-wide v0, p0, Lbao$a;->e:J

    .line 16
    invoke-virtual {p1, v4, v0, v1}, Lhaw;->a(IJ)V

    .line 17
    :cond_4
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 18
    const/4 v0, 0x5

    iget-wide v2, p0, Lbao$a;->f:J

    .line 19
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 20
    :cond_5
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 21
    const/4 v0, 0x6

    iget-wide v2, p0, Lbao$a;->g:J

    .line 22
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 23
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lbao$a;->h:Lhce;

    invoke-interface {v0}, Lhce;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 24
    const/4 v2, 0x7

    iget-object v0, p0, Lbao$a;->h:Lhce;

    invoke-interface {v0, v1}, Lhce;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdd;

    invoke-virtual {p1, v2, v0}, Lhaw;->a(ILhdd;)V

    .line 25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 26
    :cond_7
    iget v0, p0, Lbao$a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 27
    iget-boolean v0, p0, Lbao$a;->i:Z

    invoke-virtual {p1, v5, v0}, Lhaw;->a(IZ)V

    .line 28
    :cond_8
    iget-object v0, p0, Lbao$a;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
