.class final Lhad;
.super Lhac;
.source "PG"


# instance fields
.field private a:Z

.field private b:[B

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;Z)V
    .locals 2

    .prologue
    .line 1
    .line 2
    invoke-direct {p0}, Lhac;-><init>()V

    .line 3
    iput-boolean p2, p0, Lhad;->a:Z

    .line 4
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lhad;->b:[B

    .line 5
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhad;->c:I

    .line 6
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhad;->d:I

    .line 7
    return-void
.end method

.method private final A()J
    .locals 2

    .prologue
    .line 876
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lhad;->b(I)V

    .line 877
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v0

    return-wide v0
.end method

.method private final B()I
    .locals 4

    .prologue
    .line 878
    iget v0, p0, Lhad;->c:I

    .line 879
    iget-object v1, p0, Lhad;->b:[B

    .line 880
    add-int/lit8 v2, v0, 0x4

    iput v2, p0, Lhad;->c:I

    .line 881
    aget-byte v2, v1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v2

    return v0
.end method

.method private final C()J
    .locals 10

    .prologue
    const-wide/16 v8, 0xff

    .line 882
    iget v0, p0, Lhad;->c:I

    .line 883
    iget-object v1, p0, Lhad;->b:[B

    .line 884
    add-int/lit8 v2, v0, 0x8

    iput v2, p0, Lhad;->c:I

    .line 885
    aget-byte v2, v1, v0

    int-to-long v2, v2

    and-long/2addr v2, v8

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x8

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x4

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x5

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x28

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v4, v0, 0x6

    aget-byte v4, v1, v4

    int-to-long v4, v4

    and-long/2addr v4, v8

    const/16 v6, 0x30

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x7

    aget-byte v0, v1, v0

    int-to-long v0, v0

    and-long/2addr v0, v8

    const/16 v4, 0x38

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private final a(Lhdz;Lhbg;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 91
    invoke-direct {p0, v0}, Lhad;->b(I)V

    .line 92
    iget v1, p0, Lhad;->d:I

    .line 93
    iget v2, p0, Lhad;->c:I

    add-int/2addr v0, v2

    .line 94
    iput v0, p0, Lhad;->d:I

    .line 95
    :try_start_0
    invoke-interface {p1}, Lhdz;->a()Ljava/lang/Object;

    move-result-object v2

    .line 96
    invoke-interface {p1, v2, p0, p2}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 97
    invoke-interface {p1, v2}, Lhdz;->c(Ljava/lang/Object;)V

    .line 98
    iget v3, p0, Lhad;->c:I

    if-eq v3, v0, :cond_0

    .line 99
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    iput v1, p0, Lhad;->d:I

    throw v0

    .line 101
    :cond_0
    iput v1, p0, Lhad;->d:I

    .line 102
    return-object v2
.end method

.method private final a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 791
    invoke-virtual {p1}, Lhfd;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 811
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unsupported field type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 792
    :pswitch_1
    invoke-virtual {p0}, Lhad;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 810
    :goto_0
    return-object v0

    .line 793
    :pswitch_2
    invoke-virtual {p0}, Lhad;->n()Lhah;

    move-result-object v0

    goto :goto_0

    .line 794
    :pswitch_3
    invoke-virtual {p0}, Lhad;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 795
    :pswitch_4
    invoke-virtual {p0}, Lhad;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 796
    :pswitch_5
    invoke-virtual {p0}, Lhad;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 797
    :pswitch_6
    invoke-virtual {p0}, Lhad;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 798
    :pswitch_7
    invoke-virtual {p0}, Lhad;->e()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 799
    :pswitch_8
    invoke-virtual {p0}, Lhad;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 800
    :pswitch_9
    invoke-virtual {p0}, Lhad;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 801
    :pswitch_a
    invoke-virtual {p0, p2, p3}, Lhad;->a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 802
    :pswitch_b
    invoke-virtual {p0}, Lhad;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 803
    :pswitch_c
    invoke-virtual {p0}, Lhad;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 804
    :pswitch_d
    invoke-virtual {p0}, Lhad;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 805
    :pswitch_e
    invoke-virtual {p0}, Lhad;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 807
    :pswitch_f
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhad;->a(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 809
    :pswitch_10
    invoke-virtual {p0}, Lhad;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 810
    :pswitch_11
    invoke-virtual {p0}, Lhad;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 791
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_7
        :pswitch_9
        :pswitch_11
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_f
        :pswitch_0
        :pswitch_a
        :pswitch_2
        :pswitch_10
        :pswitch_4
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private a(Z)Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 77
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 78
    if-nez v1, :cond_0

    .line 79
    const-string v0, ""

    .line 85
    :goto_0
    return-object v0

    .line 80
    :cond_0
    invoke-direct {p0, v1}, Lhad;->b(I)V

    .line 81
    if-eqz p1, :cond_1

    iget-object v0, p0, Lhad;->b:[B

    iget v2, p0, Lhad;->c:I

    iget v3, p0, Lhad;->c:I

    add-int/2addr v3, v1

    invoke-static {v0, v2, v3}, Lhex;->a([BII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    invoke-static {}, Lhcf;->j()Lhcf;

    move-result-object v0

    throw v0

    .line 83
    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lhad;->b:[B

    iget v3, p0, Lhad;->c:I

    sget-object v4, Lhbu;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 84
    iget v2, p0, Lhad;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lhad;->c:I

    goto :goto_0
.end method

.method private final a(I)V
    .locals 1

    .prologue
    .line 886
    invoke-direct {p0, p1}, Lhad;->b(I)V

    .line 887
    iget v0, p0, Lhad;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lhad;->c:I

    .line 888
    return-void
.end method

.method private a(Ljava/util/List;Z)V
    .locals 3

    .prologue
    .line 462
    iget v0, p0, Lhad;->e:I

    .line 463
    and-int/lit8 v0, v0, 0x7

    .line 464
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 465
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 466
    :cond_0
    instance-of v0, p1, Lhcn;

    if-eqz v0, :cond_4

    if-nez p2, :cond_4

    .line 467
    check-cast p1, Lhcn;

    .line 468
    :cond_1
    invoke-virtual {p0}, Lhad;->n()Lhah;

    move-result-object v0

    invoke-interface {p1, v0}, Lhcn;->a(Lhah;)V

    .line 469
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 484
    :cond_2
    :goto_0
    return-void

    .line 471
    :cond_3
    iget v0, p0, Lhad;->c:I

    .line 472
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 473
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_1

    .line 474
    iput v0, p0, Lhad;->c:I

    goto :goto_0

    .line 477
    :cond_4
    invoke-direct {p0, p2}, Lhad;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 478
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 480
    iget v0, p0, Lhad;->c:I

    .line 481
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 482
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 483
    iput v0, p0, Lhad;->c:I

    goto :goto_0
.end method

.method private final b(Lhdz;Lhbg;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 108
    iget v1, p0, Lhad;->f:I

    .line 109
    iget v0, p0, Lhad;->e:I

    .line 111
    ushr-int/lit8 v0, v0, 0x3

    .line 112
    const/4 v2, 0x4

    .line 113
    shl-int/lit8 v0, v0, 0x3

    or-int/2addr v0, v2

    .line 114
    iput v0, p0, Lhad;->f:I

    .line 115
    :try_start_0
    invoke-interface {p1}, Lhdz;->a()Ljava/lang/Object;

    move-result-object v0

    .line 116
    invoke-interface {p1, v0, p0, p2}, Lhdz;->a(Ljava/lang/Object;Lhdu;Lhbg;)V

    .line 117
    invoke-interface {p1, v0}, Lhdz;->c(Ljava/lang/Object;)V

    .line 118
    iget v2, p0, Lhad;->e:I

    iget v3, p0, Lhad;->f:I

    if-eq v2, v3, :cond_0

    .line 119
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :catchall_0
    move-exception v0

    iput v1, p0, Lhad;->f:I

    throw v0

    .line 121
    :cond_0
    iput v1, p0, Lhad;->f:I

    .line 122
    return-object v0
.end method

.method private final b(I)V
    .locals 2

    .prologue
    .line 889
    if-ltz p1, :cond_0

    iget v0, p0, Lhad;->d:I

    iget v1, p0, Lhad;->c:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_1

    .line 890
    :cond_0
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 891
    :cond_1
    return-void
.end method

.method private final c(I)V
    .locals 1

    .prologue
    .line 892
    iget v0, p0, Lhad;->e:I

    .line 893
    and-int/lit8 v0, v0, 0x7

    .line 894
    if-eq v0, p1, :cond_0

    .line 895
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 896
    :cond_0
    return-void
.end method

.method private final d(I)V
    .locals 1

    .prologue
    .line 897
    invoke-direct {p0, p1}, Lhad;->b(I)V

    .line 898
    and-int/lit8 v0, p1, 0x7

    if-eqz v0, :cond_0

    .line 899
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0

    .line 900
    :cond_0
    return-void
.end method

.method private final e(I)V
    .locals 1

    .prologue
    .line 901
    invoke-direct {p0, p1}, Lhad;->b(I)V

    .line 902
    and-int/lit8 v0, p1, 0x3

    if-eqz v0, :cond_0

    .line 903
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0

    .line 904
    :cond_0
    return-void
.end method

.method private final u()Z
    .locals 2

    .prologue
    .line 8
    iget v0, p0, Lhad;->c:I

    iget v1, p0, Lhad;->d:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final v()I
    .locals 4

    .prologue
    .line 812
    iget v0, p0, Lhad;->c:I

    .line 813
    iget v1, p0, Lhad;->d:I

    iget v2, p0, Lhad;->c:I

    if-ne v1, v2, :cond_0

    .line 814
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 815
    :cond_0
    iget-object v1, p0, Lhad;->b:[B

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, v1, v0

    if-ltz v0, :cond_1

    .line 816
    iput v2, p0, Lhad;->c:I

    .line 832
    :goto_0
    return v0

    .line 818
    :cond_1
    iget v1, p0, Lhad;->d:I

    sub-int/2addr v1, v2

    const/16 v3, 0x9

    if-ge v1, v3, :cond_2

    .line 819
    invoke-direct {p0}, Lhad;->x()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 820
    :cond_2
    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    shl-int/lit8 v2, v2, 0x7

    xor-int/2addr v0, v2

    if-gez v0, :cond_4

    .line 821
    xor-int/lit8 v0, v0, -0x80

    .line 831
    :cond_3
    :goto_1
    iput v1, p0, Lhad;->c:I

    goto :goto_0

    .line 822
    :cond_4
    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_5

    .line 823
    xor-int/lit16 v0, v0, 0x3f80

    move v1, v2

    goto :goto_1

    .line 824
    :cond_5
    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    shl-int/lit8 v2, v2, 0x15

    xor-int/2addr v0, v2

    if-gez v0, :cond_6

    .line 825
    const v2, -0x1fc080

    xor-int/2addr v0, v2

    goto :goto_1

    .line 826
    :cond_6
    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    .line 827
    shl-int/lit8 v3, v1, 0x1c

    xor-int/2addr v0, v3

    .line 828
    const v3, 0xfe03f80

    xor-int/2addr v0, v3

    .line 829
    if-gez v1, :cond_7

    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_3

    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    if-gez v1, :cond_7

    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_3

    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v3, v1

    if-gez v1, :cond_7

    iget-object v3, p0, Lhad;->b:[B

    add-int/lit8 v1, v2, 0x1

    aget-byte v2, v3, v2

    if-gez v2, :cond_3

    .line 830
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0

    :cond_7
    move v1, v2

    goto :goto_1
.end method

.method private w()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 833
    iget v0, p0, Lhad;->c:I

    .line 834
    iget v1, p0, Lhad;->d:I

    if-ne v1, v0, :cond_0

    .line 835
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 836
    :cond_0
    iget-object v4, p0, Lhad;->b:[B

    .line 837
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v4, v0

    if-ltz v0, :cond_1

    .line 838
    iput v1, p0, Lhad;->c:I

    .line 839
    int-to-long v0, v0

    .line 862
    :goto_0
    return-wide v0

    .line 840
    :cond_1
    iget v2, p0, Lhad;->d:I

    sub-int/2addr v2, v1

    const/16 v3, 0x9

    if-ge v2, v3, :cond_2

    .line 841
    invoke-direct {p0}, Lhad;->x()J

    move-result-wide v0

    goto :goto_0

    .line 842
    :cond_2
    add-int/lit8 v2, v1, 0x1

    aget-byte v1, v4, v1

    shl-int/lit8 v1, v1, 0x7

    xor-int/2addr v0, v1

    if-gez v0, :cond_4

    .line 843
    xor-int/lit8 v0, v0, -0x80

    int-to-long v0, v0

    .line 861
    :cond_3
    :goto_1
    iput v2, p0, Lhad;->c:I

    goto :goto_0

    .line 844
    :cond_4
    add-int/lit8 v3, v2, 0x1

    aget-byte v1, v4, v2

    shl-int/lit8 v1, v1, 0xe

    xor-int/2addr v0, v1

    if-ltz v0, :cond_5

    .line 845
    xor-int/lit16 v0, v0, 0x3f80

    int-to-long v0, v0

    move v2, v3

    goto :goto_1

    .line 846
    :cond_5
    add-int/lit8 v2, v3, 0x1

    aget-byte v1, v4, v3

    shl-int/lit8 v1, v1, 0x15

    xor-int/2addr v0, v1

    if-gez v0, :cond_6

    .line 847
    const v1, -0x1fc080

    xor-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_1

    .line 848
    :cond_6
    int-to-long v0, v0

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x1c

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    cmp-long v2, v0, v8

    if-ltz v2, :cond_7

    .line 849
    const-wide/32 v4, 0xfe03f80

    xor-long/2addr v0, v4

    move v2, v3

    goto :goto_1

    .line 850
    :cond_7
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v6, v3

    const/16 v3, 0x23

    shl-long/2addr v6, v3

    xor-long/2addr v0, v6

    cmp-long v3, v0, v8

    if-gez v3, :cond_8

    .line 851
    const-wide v4, -0x7f01fc080L

    xor-long/2addr v0, v4

    goto :goto_1

    .line 852
    :cond_8
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x2a

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    cmp-long v2, v0, v8

    if-ltz v2, :cond_9

    .line 853
    const-wide v4, 0x3f80fe03f80L

    xor-long/2addr v0, v4

    move v2, v3

    goto :goto_1

    .line 854
    :cond_9
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v6, v3

    const/16 v3, 0x31

    shl-long/2addr v6, v3

    xor-long/2addr v0, v6

    cmp-long v3, v0, v8

    if-gez v3, :cond_a

    .line 855
    const-wide v4, -0x1fc07f01fc080L

    xor-long/2addr v0, v4

    goto :goto_1

    .line 856
    :cond_a
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v4, v2

    int-to-long v6, v2

    const/16 v2, 0x38

    shl-long/2addr v6, v2

    xor-long/2addr v0, v6

    .line 857
    const-wide v6, 0xfe03f80fe03f80L

    xor-long/2addr v0, v6

    .line 858
    cmp-long v2, v0, v8

    if-gez v2, :cond_b

    .line 859
    add-int/lit8 v2, v3, 0x1

    aget-byte v3, v4, v3

    int-to-long v4, v3

    cmp-long v3, v4, v8

    if-gez v3, :cond_3

    .line 860
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0

    :cond_b
    move v2, v3

    goto/16 :goto_1
.end method

.method private final x()J
    .locals 6

    .prologue
    .line 863
    const-wide/16 v2, 0x0

    .line 864
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x40

    if-ge v0, v1, :cond_1

    .line 865
    invoke-direct {p0}, Lhad;->y()B

    move-result v1

    .line 866
    and-int/lit8 v4, v1, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v0

    or-long/2addr v2, v4

    .line 867
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    .line 868
    return-wide v2

    .line 869
    :cond_0
    add-int/lit8 v0, v0, 0x7

    goto :goto_0

    .line 870
    :cond_1
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0
.end method

.method private final y()B
    .locals 3

    .prologue
    .line 871
    iget v0, p0, Lhad;->c:I

    iget v1, p0, Lhad;->d:I

    if-ne v0, v1, :cond_0

    .line 872
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 873
    :cond_0
    iget-object v0, p0, Lhad;->b:[B

    iget v1, p0, Lhad;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lhad;->c:I

    aget-byte v0, v0, v1

    return v0
.end method

.method private final z()I
    .locals 1

    .prologue
    .line 874
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lhad;->b(I)V

    .line 875
    invoke-direct {p0}, Lhad;->B()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const v0, 0x7fffffff

    .line 9
    invoke-direct {p0}, Lhad;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16
    :cond_0
    :goto_0
    return v0

    .line 11
    :cond_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    iput v1, p0, Lhad;->e:I

    .line 12
    iget v1, p0, Lhad;->e:I

    iget v2, p0, Lhad;->f:I

    if-eq v1, v2, :cond_0

    .line 14
    iget v0, p0, Lhad;->e:I

    .line 15
    ushr-int/lit8 v0, v0, 0x3

    .line 16
    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 88
    sget-object v0, Lhdp;->a:Lhdp;

    .line 89
    invoke-virtual {v0, p1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lhad;->a(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 146
    instance-of v0, p1, Lhbd;

    if-eqz v0, :cond_3

    .line 147
    check-cast p1, Lhbd;

    .line 148
    iget v0, p0, Lhad;->e:I

    .line 149
    and-int/lit8 v0, v0, 0x7

    .line 150
    packed-switch v0, :pswitch_data_0

    .line 165
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 151
    :pswitch_0
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 152
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 153
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 154
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 155
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhbd;->a(D)V

    goto :goto_0

    .line 156
    :cond_0
    :pswitch_1
    invoke-virtual {p0}, Lhad;->d()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhbd;->a(D)V

    .line 157
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    :cond_1
    :goto_1
    return-void

    .line 159
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 160
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 161
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 162
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 167
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 168
    and-int/lit8 v0, v0, 0x7

    .line 169
    packed-switch v0, :pswitch_data_1

    .line 184
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 170
    :pswitch_2
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 171
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 172
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 173
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 174
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 175
    :cond_4
    :pswitch_3
    invoke-virtual {p0}, Lhad;->d()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    iget v0, p0, Lhad;->c:I

    .line 179
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 180
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 181
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 169
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Ljava/lang/Class;Lhbg;)V
    .locals 4

    .prologue
    .line 486
    iget v0, p0, Lhad;->e:I

    .line 487
    and-int/lit8 v0, v0, 0x7

    .line 488
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 489
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 490
    :cond_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 491
    invoke-virtual {v0, p2}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    .line 492
    iget v1, p0, Lhad;->e:I

    .line 493
    :cond_1
    invoke-direct {p0, v0, p3}, Lhad;->a(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494
    invoke-direct {p0}, Lhad;->u()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 500
    :goto_0
    return-void

    .line 496
    :cond_2
    iget v2, p0, Lhad;->c:I

    .line 497
    invoke-direct {p0}, Lhad;->v()I

    move-result v3

    .line 498
    if-eq v3, v1, :cond_1

    .line 499
    iput v2, p0, Lhad;->c:I

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;Lhcx;Lhbg;)V
    .locals 6

    .prologue
    .line 763
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 764
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 765
    invoke-direct {p0, v0}, Lhad;->b(I)V

    .line 766
    iget v2, p0, Lhad;->d:I

    .line 767
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 768
    iput v0, p0, Lhad;->d:I

    .line 769
    :try_start_0
    iget-object v1, p2, Lhcx;->b:Ljava/lang/Object;

    .line 770
    iget-object v0, p2, Lhcx;->d:Ljava/lang/Object;

    .line 771
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lhad;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 772
    const v4, 0x7fffffff

    if-eq v3, v4, :cond_1

    .line 773
    packed-switch v3, :pswitch_data_0

    .line 780
    :try_start_1
    invoke-virtual {p0}, Lhad;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 781
    new-instance v3, Lhcf;

    const-string v4, "Unable to parse map entry."

    invoke-direct {v3, v4}, Lhcf;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Lhcg; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 784
    :catch_0
    move-exception v3

    :try_start_2
    invoke-virtual {p0}, Lhad;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 785
    new-instance v0, Lhcf;

    const-string v1, "Unable to parse map entry."

    invoke-direct {v0, v1}, Lhcf;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 790
    :catchall_0
    move-exception v0

    iput v2, p0, Lhad;->d:I

    throw v0

    .line 774
    :pswitch_0
    :try_start_3
    iget-object v3, p2, Lhcx;->a:Lhfd;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lhad;->a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 776
    :pswitch_1
    iget-object v3, p2, Lhcx;->c:Lhfd;

    iget-object v4, p2, Lhcx;->d:Ljava/lang/Object;

    .line 777
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 778
    invoke-direct {p0, v3, v4, p3}, Lhad;->a(Lhfd;Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    :try_end_3
    .catch Lhcg; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 787
    :cond_1
    :try_start_4
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 788
    iput v2, p0, Lhad;->d:I

    .line 789
    return-void

    .line 773
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lhad;->e:I

    return v0
.end method

.method public final b(Ljava/lang/Class;Lhbg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 106
    sget-object v0, Lhdp;->a:Lhdp;

    .line 107
    invoke-virtual {v0, p1}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lhad;->b(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 186
    instance-of v0, p1, Lhbo;

    if-eqz v0, :cond_3

    .line 187
    check-cast p1, Lhbo;

    .line 188
    iget v0, p0, Lhad;->e:I

    .line 189
    and-int/lit8 v0, v0, 0x7

    .line 190
    packed-switch v0, :pswitch_data_0

    .line 205
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 191
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 192
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 193
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 194
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 195
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    invoke-virtual {p1, v1}, Lhbo;->a(F)V

    goto :goto_0

    .line 196
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->e()F

    move-result v0

    invoke-virtual {p1, v0}, Lhbo;->a(F)V

    .line 197
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    :cond_1
    :goto_1
    return-void

    .line 199
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 200
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 201
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 202
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 207
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 208
    and-int/lit8 v0, v0, 0x7

    .line 209
    packed-switch v0, :pswitch_data_1

    .line 224
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 210
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 211
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 212
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 213
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 214
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 215
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->e()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 218
    iget v0, p0, Lhad;->c:I

    .line 219
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 220
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 221
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 209
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final b(Ljava/util/List;Ljava/lang/Class;Lhbg;)V
    .locals 4

    .prologue
    .line 502
    iget v0, p0, Lhad;->e:I

    .line 503
    and-int/lit8 v0, v0, 0x7

    .line 504
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 505
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 506
    :cond_0
    sget-object v0, Lhdp;->a:Lhdp;

    .line 507
    invoke-virtual {v0, p2}, Lhdp;->a(Ljava/lang/Class;)Lhdz;

    move-result-object v0

    .line 508
    iget v1, p0, Lhad;->e:I

    .line 509
    :cond_1
    invoke-direct {p0, v0, p3}, Lhad;->b(Lhdz;Lhbg;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    invoke-direct {p0}, Lhad;->u()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 516
    :goto_0
    return-void

    .line 512
    :cond_2
    iget v2, p0, Lhad;->c:I

    .line 513
    invoke-direct {p0}, Lhad;->v()I

    move-result v3

    .line 514
    if-eq v3, v1, :cond_1

    .line 515
    iput v2, p0, Lhad;->c:I

    goto :goto_0
.end method

.method public final c(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 226
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 227
    check-cast p1, Lhcr;

    .line 228
    iget v0, p0, Lhad;->e:I

    .line 229
    and-int/lit8 v0, v0, 0x7

    .line 230
    packed-switch v0, :pswitch_data_0

    .line 244
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 231
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 232
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 233
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 234
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    goto :goto_0

    .line 235
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->f()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 236
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    :cond_1
    :goto_1
    return-void

    .line 238
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 239
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 240
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 241
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 246
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 247
    and-int/lit8 v0, v0, 0x7

    .line 248
    packed-switch v0, :pswitch_data_1

    .line 262
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 249
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 250
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 251
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 252
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 253
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    iget v0, p0, Lhad;->c:I

    .line 257
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 258
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 259
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 248
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c()Z
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/16 v6, 0xa

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 18
    invoke-direct {p0}, Lhad;->u()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lhad;->e:I

    iget v3, p0, Lhad;->f:I

    if-ne v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 56
    :cond_1
    :goto_0
    return v0

    .line 20
    :cond_2
    iget v2, p0, Lhad;->e:I

    .line 21
    and-int/lit8 v2, v2, 0x7

    .line 22
    packed-switch v2, :pswitch_data_0

    .line 57
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 24
    :pswitch_1
    iget v2, p0, Lhad;->d:I

    iget v3, p0, Lhad;->c:I

    sub-int/2addr v2, v3

    if-lt v2, v6, :cond_4

    .line 25
    iget-object v5, p0, Lhad;->b:[B

    .line 26
    iget v2, p0, Lhad;->c:I

    move v3, v2

    move v2, v1

    .line 27
    :goto_1
    if-ge v2, v6, :cond_4

    .line 28
    add-int/lit8 v4, v3, 0x1

    aget-byte v3, v5, v3

    if-ltz v3, :cond_3

    .line 29
    iput v4, p0, Lhad;->c:I

    goto :goto_0

    .line 31
    :cond_3
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_1

    .line 33
    :cond_4
    :goto_2
    if-ge v1, v6, :cond_5

    .line 34
    invoke-direct {p0}, Lhad;->y()B

    move-result v2

    if-gez v2, :cond_1

    .line 35
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 36
    :cond_5
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0

    .line 38
    :pswitch_2
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lhad;->a(I)V

    goto :goto_0

    .line 40
    :pswitch_3
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-direct {p0, v1}, Lhad;->a(I)V

    goto :goto_0

    .line 42
    :pswitch_4
    invoke-direct {p0, v4}, Lhad;->a(I)V

    goto :goto_0

    .line 45
    :pswitch_5
    iget v1, p0, Lhad;->f:I

    .line 46
    iget v2, p0, Lhad;->e:I

    .line 48
    ushr-int/lit8 v2, v2, 0x3

    .line 50
    shl-int/lit8 v2, v2, 0x3

    or-int/2addr v2, v4

    .line 51
    iput v2, p0, Lhad;->f:I

    .line 52
    :cond_6
    invoke-virtual {p0}, Lhad;->a()I

    move-result v2

    const v3, 0x7fffffff

    if-eq v2, v3, :cond_7

    invoke-virtual {p0}, Lhad;->c()Z

    move-result v2

    if-nez v2, :cond_6

    .line 53
    :cond_7
    iget v2, p0, Lhad;->e:I

    iget v3, p0, Lhad;->f:I

    if-eq v2, v3, :cond_8

    .line 54
    invoke-static {}, Lhcf;->i()Lhcf;

    move-result-object v0

    throw v0

    .line 55
    :cond_8
    iput v1, p0, Lhad;->f:I

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 59
    invoke-direct {p0}, Lhad;->A()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final d(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 264
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 265
    check-cast p1, Lhcr;

    .line 266
    iget v0, p0, Lhad;->e:I

    .line 267
    and-int/lit8 v0, v0, 0x7

    .line 268
    packed-switch v0, :pswitch_data_0

    .line 282
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 269
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 270
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 271
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 272
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    goto :goto_0

    .line 273
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->g()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 274
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    :cond_1
    :goto_1
    return-void

    .line 276
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 277
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 278
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 279
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 284
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 285
    and-int/lit8 v0, v0, 0x7

    .line 286
    packed-switch v0, :pswitch_data_1

    .line 300
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 287
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 288
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 289
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 290
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 291
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    iget v0, p0, Lhad;->c:I

    .line 295
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 296
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 297
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 286
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 61
    invoke-direct {p0}, Lhad;->z()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public final e(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 302
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 303
    check-cast p1, Lhbt;

    .line 304
    iget v0, p0, Lhad;->e:I

    .line 305
    and-int/lit8 v0, v0, 0x7

    .line 306
    packed-switch v0, :pswitch_data_0

    .line 320
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 307
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 308
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 309
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 310
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 311
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->h()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 312
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    :cond_1
    :goto_1
    return-void

    .line 314
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 315
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 316
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 317
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 322
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 323
    and-int/lit8 v0, v0, 0x7

    .line 324
    packed-switch v0, :pswitch_data_1

    .line 338
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 325
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 326
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 327
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 328
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 329
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 332
    iget v0, p0, Lhad;->c:I

    .line 333
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 334
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 335
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 324
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 63
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 340
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 341
    check-cast p1, Lhcr;

    .line 342
    iget v0, p0, Lhad;->e:I

    .line 343
    and-int/lit8 v0, v0, 0x7

    .line 344
    packed-switch v0, :pswitch_data_0

    .line 359
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 345
    :pswitch_0
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 346
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 347
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 348
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 349
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    goto :goto_0

    .line 350
    :cond_0
    :pswitch_1
    invoke-virtual {p0}, Lhad;->i()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 351
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    :cond_1
    :goto_1
    return-void

    .line 353
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 354
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 355
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 356
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 361
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 362
    and-int/lit8 v0, v0, 0x7

    .line 363
    packed-switch v0, :pswitch_data_1

    .line 378
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 364
    :pswitch_2
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 365
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 366
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 367
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 368
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 369
    :cond_4
    :pswitch_3
    invoke-virtual {p0}, Lhad;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 372
    iget v0, p0, Lhad;->c:I

    .line 373
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 374
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 375
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 363
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 65
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 380
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 381
    check-cast p1, Lhbt;

    .line 382
    iget v0, p0, Lhad;->e:I

    .line 383
    and-int/lit8 v0, v0, 0x7

    .line 384
    packed-switch v0, :pswitch_data_0

    .line 399
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 385
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 386
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 387
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 388
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 389
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 390
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->j()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 391
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    :cond_1
    :goto_1
    return-void

    .line 393
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 394
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 395
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 396
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 401
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 402
    and-int/lit8 v0, v0, 0x7

    .line 403
    packed-switch v0, :pswitch_data_1

    .line 418
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 404
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 405
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 406
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 407
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 408
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 409
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 412
    iget v0, p0, Lhad;->c:I

    .line 413
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 414
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 415
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 403
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 67
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    return v0
.end method

.method public final h(Ljava/util/List;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 420
    instance-of v0, p1, Lhaf;

    if-eqz v0, :cond_4

    .line 421
    check-cast p1, Lhaf;

    .line 422
    iget v0, p0, Lhad;->e:I

    .line 423
    and-int/lit8 v0, v0, 0x7

    .line 424
    packed-switch v0, :pswitch_data_0

    .line 438
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 425
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 426
    iget v3, p0, Lhad;->c:I

    add-int/2addr v3, v0

    .line 427
    :goto_0
    iget v0, p0, Lhad;->c:I

    if-ge v0, v3, :cond_2

    .line 428
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lhaf;->a(Z)V

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    .line 429
    :cond_1
    :pswitch_2
    invoke-virtual {p0}, Lhad;->k()Z

    move-result v0

    invoke-virtual {p1, v0}, Lhaf;->a(Z)V

    .line 430
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 457
    :cond_2
    :goto_2
    return-void

    .line 432
    :cond_3
    iget v0, p0, Lhad;->c:I

    .line 433
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 434
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_1

    .line 435
    iput v0, p0, Lhad;->c:I

    goto :goto_2

    .line 440
    :cond_4
    iget v0, p0, Lhad;->e:I

    .line 441
    and-int/lit8 v0, v0, 0x7

    .line 442
    packed-switch v0, :pswitch_data_1

    .line 456
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 443
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 444
    iget v3, p0, Lhad;->c:I

    add-int/2addr v3, v0

    .line 445
    :goto_3
    iget v0, p0, Lhad;->c:I

    if-ge v0, v3, :cond_2

    .line 446
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    .line 447
    :cond_6
    :pswitch_5
    invoke-virtual {p0}, Lhad;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 450
    iget v0, p0, Lhad;->c:I

    .line 451
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 452
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_6

    .line 453
    iput v0, p0, Lhad;->c:I

    goto :goto_2

    .line 424
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 442
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 68
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 69
    invoke-direct {p0}, Lhad;->A()J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhad;->a(Ljava/util/List;Z)V

    .line 459
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 71
    invoke-direct {p0}, Lhad;->z()I

    move-result v0

    return v0
.end method

.method public final j(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lhad;->a(Ljava/util/List;Z)V

    .line 461
    return-void
.end method

.method public final k(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 518
    iget v0, p0, Lhad;->e:I

    .line 519
    and-int/lit8 v0, v0, 0x7

    .line 520
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 521
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 522
    :cond_0
    invoke-virtual {p0}, Lhad;->n()Lhah;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 529
    :goto_0
    return-void

    .line 525
    :cond_1
    iget v0, p0, Lhad;->c:I

    .line 526
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 527
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 528
    iput v0, p0, Lhad;->c:I

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 73
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 531
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 532
    check-cast p1, Lhbt;

    .line 533
    iget v0, p0, Lhad;->e:I

    .line 534
    and-int/lit8 v0, v0, 0x7

    .line 535
    packed-switch v0, :pswitch_data_0

    .line 549
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 536
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 537
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 538
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 539
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 540
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->o()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 541
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 568
    :cond_1
    :goto_1
    return-void

    .line 543
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 544
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 545
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 546
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 551
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 552
    and-int/lit8 v0, v0, 0x7

    .line 553
    packed-switch v0, :pswitch_data_1

    .line 567
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 554
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 555
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 556
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 557
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 558
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 561
    iget v0, p0, Lhad;->c:I

    .line 562
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 563
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 564
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 535
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 553
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhad;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 569
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 570
    check-cast p1, Lhbt;

    .line 571
    iget v0, p0, Lhad;->e:I

    .line 572
    and-int/lit8 v0, v0, 0x7

    .line 573
    packed-switch v0, :pswitch_data_0

    .line 587
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 574
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 575
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 576
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 577
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 578
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->p()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 579
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 606
    :cond_1
    :goto_1
    return-void

    .line 581
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 582
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 583
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 584
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 589
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 590
    and-int/lit8 v0, v0, 0x7

    .line 591
    packed-switch v0, :pswitch_data_1

    .line 605
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 592
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 593
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 594
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 595
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 596
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 599
    iget v0, p0, Lhad;->c:I

    .line 600
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 601
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 602
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 573
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 591
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final n()Lhah;
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 125
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 126
    if-nez v1, :cond_0

    .line 127
    sget-object v0, Lhah;->a:Lhah;

    .line 133
    :goto_0
    return-object v0

    .line 128
    :cond_0
    invoke-direct {p0, v1}, Lhad;->b(I)V

    .line 129
    iget-boolean v0, p0, Lhad;->a:Z

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lhad;->b:[B

    iget v2, p0, Lhad;->c:I

    invoke-static {v0, v2, v1}, Lhah;->b([BII)Lhah;

    move-result-object v0

    .line 132
    :goto_1
    iget v2, p0, Lhad;->c:I

    add-int/2addr v1, v2

    iput v1, p0, Lhad;->c:I

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lhad;->b:[B

    iget v2, p0, Lhad;->c:I

    invoke-static {v0, v2, v1}, Lhah;->a([BII)Lhah;

    move-result-object v0

    goto :goto_1
.end method

.method public final n(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 607
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 608
    check-cast p1, Lhbt;

    .line 609
    iget v0, p0, Lhad;->e:I

    .line 610
    and-int/lit8 v0, v0, 0x7

    .line 611
    packed-switch v0, :pswitch_data_0

    .line 626
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 612
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 613
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 614
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 615
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 616
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 617
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->q()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 618
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 646
    :cond_1
    :goto_1
    return-void

    .line 620
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 621
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 622
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 623
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 628
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 629
    and-int/lit8 v0, v0, 0x7

    .line 630
    packed-switch v0, :pswitch_data_1

    .line 645
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 631
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 632
    invoke-direct {p0, v0}, Lhad;->e(I)V

    .line 633
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 634
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 635
    invoke-direct {p0}, Lhad;->B()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 636
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->q()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 639
    iget v0, p0, Lhad;->c:I

    .line 640
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 641
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 642
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 611
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 630
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 135
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    return v0
.end method

.method public final o(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 647
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 648
    check-cast p1, Lhcr;

    .line 649
    iget v0, p0, Lhad;->e:I

    .line 650
    and-int/lit8 v0, v0, 0x7

    .line 651
    packed-switch v0, :pswitch_data_0

    .line 666
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 652
    :pswitch_0
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 653
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 654
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 655
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 656
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    goto :goto_0

    .line 657
    :cond_0
    :pswitch_1
    invoke-virtual {p0}, Lhad;->r()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 658
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 686
    :cond_1
    :goto_1
    return-void

    .line 660
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 661
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 662
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 663
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 668
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 669
    and-int/lit8 v0, v0, 0x7

    .line 670
    packed-switch v0, :pswitch_data_1

    .line 685
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 671
    :pswitch_2
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 672
    invoke-direct {p0, v0}, Lhad;->d(I)V

    .line 673
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 674
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 675
    invoke-direct {p0}, Lhad;->C()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 676
    :cond_4
    :pswitch_3
    invoke-virtual {p0}, Lhad;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 677
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 679
    iget v0, p0, Lhad;->c:I

    .line 680
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 681
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 682
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 651
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 670
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 137
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    return v0
.end method

.method public final p(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 687
    instance-of v0, p1, Lhbt;

    if-eqz v0, :cond_3

    .line 688
    check-cast p1, Lhbt;

    .line 689
    iget v0, p0, Lhad;->e:I

    .line 690
    and-int/lit8 v0, v0, 0x7

    .line 691
    packed-switch v0, :pswitch_data_0

    .line 705
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 692
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 693
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 694
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 695
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-static {v1}, Lhaq;->e(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lhbt;->d(I)V

    goto :goto_0

    .line 696
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->s()I

    move-result v0

    invoke-virtual {p1, v0}, Lhbt;->d(I)V

    .line 697
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724
    :cond_1
    :goto_1
    return-void

    .line 699
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 700
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 701
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 702
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 707
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 708
    and-int/lit8 v0, v0, 0x7

    .line 709
    packed-switch v0, :pswitch_data_1

    .line 723
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 710
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 711
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 712
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 713
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    invoke-static {v1}, Lhaq;->e(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 714
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->s()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 717
    iget v0, p0, Lhad;->c:I

    .line 718
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 719
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 720
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 691
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 709
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 139
    invoke-direct {p0}, Lhad;->z()I

    move-result v0

    return v0
.end method

.method public final q(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 725
    instance-of v0, p1, Lhcr;

    if-eqz v0, :cond_3

    .line 726
    check-cast p1, Lhcr;

    .line 727
    iget v0, p0, Lhad;->e:I

    .line 728
    and-int/lit8 v0, v0, 0x7

    .line 729
    packed-switch v0, :pswitch_data_0

    .line 743
    :pswitch_0
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 730
    :pswitch_1
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 731
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 732
    :goto_0
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 733
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-static {v2, v3}, Lhaq;->a(J)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lhcr;->a(J)V

    goto :goto_0

    .line 734
    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lhad;->t()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lhcr;->a(J)V

    .line 735
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 762
    :cond_1
    :goto_1
    return-void

    .line 737
    :cond_2
    iget v0, p0, Lhad;->c:I

    .line 738
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 739
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_0

    .line 740
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 745
    :cond_3
    iget v0, p0, Lhad;->e:I

    .line 746
    and-int/lit8 v0, v0, 0x7

    .line 747
    packed-switch v0, :pswitch_data_1

    .line 761
    :pswitch_3
    invoke-static {}, Lhcf;->f()Lhcg;

    move-result-object v0

    throw v0

    .line 748
    :pswitch_4
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    .line 749
    iget v1, p0, Lhad;->c:I

    add-int/2addr v0, v1

    .line 750
    :goto_2
    iget v1, p0, Lhad;->c:I

    if-ge v1, v0, :cond_1

    .line 751
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v2

    invoke-static {v2, v3}, Lhaq;->a(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 752
    :cond_4
    :pswitch_5
    invoke-virtual {p0}, Lhad;->t()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 753
    invoke-direct {p0}, Lhad;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 755
    iget v0, p0, Lhad;->c:I

    .line 756
    invoke-direct {p0}, Lhad;->v()I

    move-result v1

    .line 757
    iget v2, p0, Lhad;->e:I

    if-eq v1, v2, :cond_4

    .line 758
    iput v0, p0, Lhad;->c:I

    goto :goto_1

    .line 729
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 747
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 141
    invoke-direct {p0}, Lhad;->A()J

    move-result-wide v0

    return-wide v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 143
    invoke-direct {p0}, Lhad;->v()I

    move-result v0

    invoke-static {v0}, Lhaq;->e(I)I

    move-result v0

    return v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(I)V

    .line 145
    invoke-direct {p0}, Lhad;->w()J

    move-result-wide v0

    invoke-static {v0, v1}, Lhaq;->a(J)J

    move-result-wide v0

    return-wide v0
.end method
