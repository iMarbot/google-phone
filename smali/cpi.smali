.class public final Lcpi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Landroid/telecom/PhoneAccountHandle;


# direct methods
.method public constructor <init>(ILandroid/telecom/PhoneAccountHandle;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcpi;->a:I

    .line 3
    iput-object p2, p0, Lcpi;->b:Landroid/telecom/PhoneAccountHandle;

    .line 4
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5
    instance-of v1, p1, Lcpi;

    if-nez v1, :cond_1

    .line 8
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    check-cast p1, Lcpi;

    .line 8
    iget v1, p0, Lcpi;->a:I

    iget v2, p1, Lcpi;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcpi;->b:Landroid/telecom/PhoneAccountHandle;

    iget-object v2, p1, Lcpi;->b:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v1, v2}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcpi;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcpi;->b:Landroid/telecom/PhoneAccountHandle;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
