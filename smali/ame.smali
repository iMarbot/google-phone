.class public final Lame;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lame$a;
    }
.end annotation


# static fields
.field public static final j:Lame;

.field private static volatile l:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:I

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Ljava/lang/String;

.field private k:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 167
    new-instance v0, Lame;

    invoke-direct {v0}, Lame;-><init>()V

    .line 168
    sput-object v0, Lame;->j:Lame;

    invoke-virtual {v0}, Lame;->makeImmutable()V

    .line 169
    const-class v0, Lame;

    sget-object v1, Lame;->j:Lame;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 170
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/4 v0, 0x2

    iput-byte v0, p0, Lame;->k:B

    .line 3
    const-string v0, ""

    iput-object v0, p0, Lame;->d:Ljava/lang/String;

    .line 4
    iput v1, p0, Lame;->f:I

    .line 5
    const-string v0, ""

    iput-object v0, p0, Lame;->g:Ljava/lang/String;

    .line 6
    iput v1, p0, Lame;->h:I

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lame;->i:Ljava/lang/String;

    .line 8
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 162
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "c"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "h"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 163
    sget-object v2, Lame$a;->e:Lhby;

    .line 164
    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "f"

    aput-object v2, v0, v1

    .line 165
    const-string v1, "\u0001\u0008\u0000\u0001\u0001\u0008\u0000\u0000\u0002\u0001\u0504\u0000\u0002\u0503\u0001\u0003\u0008\u0002\u0004\u0007\u0003\u0005\u0008\u0005\u0006\u000c\u0006\u0007\u0008\u0007\u0008\u0004\u0004"

    .line 166
    sget-object v2, Lame;->j:Lame;

    invoke-static {v2, v1, v0}, Lame;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 76
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 161
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 77
    :pswitch_0
    new-instance v0, Lame;

    invoke-direct {v0}, Lame;-><init>()V

    .line 160
    :goto_0
    return-object v0

    .line 78
    :pswitch_1
    iget-byte v3, p0, Lame;->k:B

    .line 79
    if-ne v3, v1, :cond_0

    sget-object v0, Lame;->j:Lame;

    goto :goto_0

    .line 80
    :cond_0
    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 81
    :cond_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 82
    sget-boolean v4, Lame;->usingExperimentalRuntime:Z

    if-eqz v4, :cond_5

    .line 83
    invoke-virtual {p0}, Lame;->isInitializedInternal()Z

    move-result v4

    if-nez v4, :cond_3

    .line 84
    if-eqz v3, :cond_2

    iput-byte v0, p0, Lame;->k:B

    :cond_2
    move-object v0, v2

    .line 85
    goto :goto_0

    .line 86
    :cond_3
    if-eqz v3, :cond_4

    iput-byte v1, p0, Lame;->k:B

    .line 87
    :cond_4
    sget-object v0, Lame;->j:Lame;

    goto :goto_0

    .line 89
    :cond_5
    iget v3, p0, Lame;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_6

    move v3, v1

    .line 90
    :goto_1
    if-nez v3, :cond_7

    move-object v0, v2

    .line 91
    goto :goto_0

    :cond_6
    move v3, v0

    .line 89
    goto :goto_1

    .line 93
    :cond_7
    iget v3, p0, Lame;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    .line 94
    :goto_2
    if-nez v1, :cond_9

    move-object v0, v2

    .line 95
    goto :goto_0

    :cond_8
    move v1, v0

    .line 93
    goto :goto_2

    .line 96
    :cond_9
    sget-object v0, Lame;->j:Lame;

    goto :goto_0

    :pswitch_2
    move-object v0, v2

    .line 97
    goto :goto_0

    .line 98
    :pswitch_3
    new-instance v1, Lhbr$a;

    invoke-direct {v1, v0}, Lhbr$a;-><init>(B)V

    move-object v0, v1

    goto :goto_0

    .line 99
    :pswitch_4
    check-cast p2, Lhaq;

    .line 100
    check-cast p3, Lhbg;

    .line 101
    if-nez p3, :cond_a

    .line 102
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 103
    :cond_a
    :try_start_0
    sget-boolean v2, Lame;->usingExperimentalRuntime:Z

    if-eqz v2, :cond_b

    .line 104
    invoke-virtual {p0, p2, p3}, Lame;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 105
    sget-object v0, Lame;->j:Lame;

    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 107
    :cond_b
    :goto_3
    if-nez v0, :cond_d

    .line 108
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v2

    .line 109
    sparse-switch v2, :sswitch_data_0

    .line 112
    invoke-virtual {p0, v2, p2}, Lame;->parseUnknownField(ILhaq;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 113
    goto :goto_3

    .line 114
    :sswitch_1
    iget v2, p0, Lame;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lame;->a:I

    .line 115
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lame;->b:I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    throw v0

    .line 117
    :sswitch_2
    :try_start_2
    iget v2, p0, Lame;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lame;->a:I

    .line 118
    invoke-virtual {p2}, Lhaq;->d()J

    move-result-wide v2

    iput-wide v2, p0, Lame;->c:J
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 148
    :catch_1
    move-exception v0

    .line 149
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 150
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    :sswitch_3
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 121
    iget v3, p0, Lame;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lame;->a:I

    .line 122
    iput-object v2, p0, Lame;->d:Ljava/lang/String;

    goto :goto_3

    .line 124
    :sswitch_4
    iget v2, p0, Lame;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lame;->a:I

    .line 125
    invoke-virtual {p2}, Lhaq;->i()Z

    move-result v2

    iput-boolean v2, p0, Lame;->e:Z

    goto :goto_3

    .line 127
    :sswitch_5
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 128
    iget v3, p0, Lame;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lame;->a:I

    .line 129
    iput-object v2, p0, Lame;->g:Ljava/lang/String;

    goto :goto_3

    .line 131
    :sswitch_6
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v2

    .line 132
    invoke-static {v2}, Lame$a;->a(I)Lame$a;

    move-result-object v3

    .line 133
    if-nez v3, :cond_c

    .line 134
    const/4 v3, 0x6

    invoke-super {p0, v3, v2}, Lhbr;->mergeVarintField(II)V

    goto :goto_3

    .line 135
    :cond_c
    iget v3, p0, Lame;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lame;->a:I

    .line 136
    iput v2, p0, Lame;->h:I

    goto/16 :goto_3

    .line 138
    :sswitch_7
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v2

    .line 139
    iget v3, p0, Lame;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lame;->a:I

    .line 140
    iput-object v2, p0, Lame;->i:Ljava/lang/String;

    goto/16 :goto_3

    .line 142
    :sswitch_8
    iget v2, p0, Lame;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lame;->a:I

    .line 143
    invoke-virtual {p2}, Lhaq;->f()I

    move-result v2

    iput v2, p0, Lame;->f:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 152
    :cond_d
    :pswitch_5
    sget-object v0, Lame;->j:Lame;

    goto/16 :goto_0

    .line 153
    :pswitch_6
    sget-object v0, Lame;->l:Lhdm;

    if-nez v0, :cond_f

    const-class v1, Lame;

    monitor-enter v1

    .line 154
    :try_start_5
    sget-object v0, Lame;->l:Lhdm;

    if-nez v0, :cond_e

    .line 155
    new-instance v0, Lhaa;

    sget-object v2, Lame;->j:Lame;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lame;->l:Lhdm;

    .line 156
    :cond_e
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 157
    :cond_f
    sget-object v0, Lame;->l:Lhdm;

    goto/16 :goto_0

    .line 156
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 158
    :pswitch_7
    iget-byte v0, p0, Lame;->k:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    .line 159
    :pswitch_8
    if-nez p2, :cond_10

    :goto_4
    int-to-byte v0, v0

    iput-byte v0, p0, Lame;->k:B

    move-object v0, v2

    .line 160
    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 159
    goto :goto_4

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 37
    iget v0, p0, Lame;->memoizedSerializedSize:I

    .line 38
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 75
    :goto_0
    return v0

    .line 39
    :cond_0
    sget-boolean v0, Lame;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0}, Lame;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lame;->memoizedSerializedSize:I

    .line 41
    iget v0, p0, Lame;->memoizedSerializedSize:I

    goto :goto_0

    .line 42
    :cond_1
    const/4 v0, 0x0

    .line 43
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 44
    iget v0, p0, Lame;->b:I

    .line 45
    invoke-static {v2, v0}, Lhaw;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 46
    :cond_2
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    .line 47
    iget-wide v2, p0, Lame;->c:J

    .line 48
    invoke-static {v4, v2, v3}, Lhaw;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_3
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_4

    .line 50
    const/4 v1, 0x3

    .line 52
    iget-object v2, p0, Lame;->d:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_4
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_5

    .line 55
    iget-boolean v1, p0, Lame;->e:Z

    .line 56
    invoke-static {v5, v1}, Lhaw;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_5
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 58
    const/4 v1, 0x5

    .line 60
    iget-object v2, p0, Lame;->g:Ljava/lang/String;

    .line 61
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    :cond_6
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 63
    const/4 v1, 0x6

    iget v2, p0, Lame;->h:I

    .line 64
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_7
    iget v1, p0, Lame;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 66
    const/4 v1, 0x7

    .line 68
    iget-object v2, p0, Lame;->i:Ljava/lang/String;

    .line 69
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_8
    iget v1, p0, Lame;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_9

    .line 71
    iget v1, p0, Lame;->f:I

    .line 72
    invoke-static {v6, v1}, Lhaw;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_9
    iget-object v1, p0, Lame;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    iput v0, p0, Lame;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 9
    sget-boolean v0, Lame;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 10
    invoke-virtual {p0, p1}, Lame;->writeToInternal(Lhaw;)V

    .line 36
    :goto_0
    return-void

    .line 12
    :cond_0
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 13
    iget v0, p0, Lame;->b:I

    invoke-virtual {p1, v1, v0}, Lhaw;->b(II)V

    .line 14
    :cond_1
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 15
    iget-wide v0, p0, Lame;->c:J

    invoke-virtual {p1, v2, v0, v1}, Lhaw;->a(IJ)V

    .line 16
    :cond_2
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 17
    const/4 v0, 0x3

    .line 18
    iget-object v1, p0, Lame;->d:Ljava/lang/String;

    .line 19
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 20
    :cond_3
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 21
    iget-boolean v0, p0, Lame;->e:Z

    invoke-virtual {p1, v3, v0}, Lhaw;->a(IZ)V

    .line 22
    :cond_4
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 23
    const/4 v0, 0x5

    .line 24
    iget-object v1, p0, Lame;->g:Ljava/lang/String;

    .line 25
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 26
    :cond_5
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 27
    const/4 v0, 0x6

    iget v1, p0, Lame;->h:I

    .line 28
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 29
    :cond_6
    iget v0, p0, Lame;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 30
    const/4 v0, 0x7

    .line 31
    iget-object v1, p0, Lame;->i:Ljava/lang/String;

    .line 32
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 33
    :cond_7
    iget v0, p0, Lame;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 34
    iget v0, p0, Lame;->f:I

    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 35
    :cond_8
    iget-object v0, p0, Lame;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0
.end method
