.class final Lhna$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhnu;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhna;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private a:Lhnt;

.field private b:Z

.field private synthetic c:Lhna;


# direct methods
.method constructor <init>(Lhna;Lhnt;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lhna$a;->c:Lhna;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhna$a;->b:Z

    .line 3
    iput-object p2, p0, Lhna$a;->a:Lhnt;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 284
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 285
    sget-object v1, Lhns;->b:Lhns;

    invoke-virtual {v0, p1, v1}, Lhme;->a(ILhns;)V

    .line 286
    return-void
.end method

.method public final a(IJ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 287
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_2

    .line 288
    const-string v1, "Received 0 flow control window increment."

    .line 289
    if-nez p1, :cond_1

    .line 290
    iget-object v0, p0, Lhna$a;->c:Lhna;

    sget-object v2, Lhns;->b:Lhns;

    .line 291
    invoke-virtual {v0, v2, v1}, Lhna;->a(Lhns;Ljava/lang/String;)V

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lhna$a;->c:Lhna;

    sget-object v2, Lhlw;->h:Lhlw;

    .line 294
    invoke-virtual {v2, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v2

    sget-object v4, Lhns;->b:Lhns;

    move v1, p1

    .line 295
    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    goto :goto_0

    .line 298
    :cond_2
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 299
    iget-object v1, v0, Lhna;->g:Ljava/lang/Object;

    .line 300
    monitor-enter v1

    .line 301
    if-nez p1, :cond_3

    .line 302
    :try_start_0
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 303
    iget-object v0, v0, Lhna;->f:Lhng;

    .line 304
    const/4 v2, 0x0

    long-to-int v3, p2

    invoke-virtual {v0, v2, v3}, Lhng;->a(Lhmx;I)V

    .line 305
    monitor-exit v1

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 306
    :cond_3
    :try_start_1
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 307
    iget-object v0, v0, Lhna;->h:Ljava/util/Map;

    .line 308
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 309
    if-eqz v0, :cond_5

    .line 310
    iget-object v2, p0, Lhna$a;->c:Lhna;

    .line 311
    iget-object v2, v2, Lhna;->f:Lhng;

    .line 312
    long-to-int v4, p2

    invoke-virtual {v2, v0, v4}, Lhng;->a(Lhmx;I)V

    .line 315
    :cond_4
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    if-eqz v3, :cond_0

    .line 317
    iget-object v0, p0, Lhna$a;->c:Lhna;

    sget-object v1, Lhns;->b:Lhns;

    const/16 v2, 0x36

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received window_update for unknown stream: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 318
    invoke-virtual {v0, v1, v2}, Lhna;->a(Lhns;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_5
    :try_start_2
    iget-object v0, p0, Lhna$a;->c:Lhna;

    invoke-virtual {v0, p1}, Lhna;->a(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_4

    .line 314
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public final a(ILhns;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 179
    invoke-static {p2}, Lhna;->a(Lhns;)Lhlw;

    move-result-object v0

    const-string v1, "Rst Stream"

    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v2

    .line 182
    iget-object v0, v2, Lhlw;->l:Lhlx;

    .line 183
    sget-object v1, Lhlx;->b:Lhlx;

    if-eq v0, v1, :cond_0

    .line 184
    iget-object v0, v2, Lhlw;->l:Lhlx;

    .line 185
    sget-object v1, Lhlx;->e:Lhlx;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 186
    :goto_0
    iget-object v0, p0, Lhna$a;->c:Lhna;

    move v1, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lhna;->a(ILhlw;ZLhns;Lhlh;)V

    .line 187
    return-void

    .line 185
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(ILhns;Lhuk;)V
    .locals 8

    .prologue
    .line 268
    sget-object v0, Lhns;->l:Lhns;

    if-ne p2, v0, :cond_0

    .line 269
    invoke-virtual {p3}, Lhuk;->a()Ljava/lang/String;

    move-result-object v0

    .line 270
    sget-object v1, Lhna;->a:Ljava/util/logging/Logger;

    .line 271
    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v4, "goAway"

    const-string v5, "%s: Received GOAWAY with ENHANCE_YOUR_CALM. Debug data: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v1, "too_many_pings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 274
    iget-object v0, v0, Lhna;->B:Ljava/lang/Runnable;

    .line 275
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 276
    :cond_0
    iget v0, p2, Lhns;->n:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lio/grpc/internal/cf$b;->a(J)Lhlw;

    move-result-object v0

    const-string v1, "Received Goaway"

    .line 277
    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 278
    invoke-virtual {p3}, Lhuk;->e()I

    move-result v1

    if-lez v1, :cond_1

    .line 279
    invoke-virtual {p3}, Lhuk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 280
    :cond_1
    iget-object v1, p0, Lhna$a;->c:Lhna;

    const/4 v2, 0x0

    .line 281
    invoke-virtual {v1, p1, v2, v0}, Lhna;->a(ILhns;Lhlw;)V

    .line 282
    return-void
.end method

.method public final a(Lhof;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 188
    iget-object v1, p0, Lhna$a;->c:Lhna;

    .line 189
    iget-object v2, v1, Lhna;->g:Ljava/lang/Object;

    .line 190
    monitor-enter v2

    .line 191
    const/4 v1, 0x4

    :try_start_0
    invoke-static {p1, v1}, Lio/grpc/internal/av;->a(Lhof;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lio/grpc/internal/av;->b(Lhof;I)I

    move-result v1

    .line 193
    iget-object v3, p0, Lhna$a;->c:Lhna;

    .line 194
    iput v1, v3, Lhna;->q:I

    .line 196
    :cond_0
    const/4 v1, 0x7

    invoke-static {p1, v1}, Lio/grpc/internal/av;->a(Lhof;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 197
    const/4 v1, 0x7

    invoke-static {p1, v1}, Lio/grpc/internal/av;->b(Lhof;I)I

    move-result v1

    .line 198
    iget-object v3, p0, Lhna$a;->c:Lhna;

    .line 199
    iget-object v3, v3, Lhna;->f:Lhng;

    .line 201
    if-gez v1, :cond_1

    .line 202
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v3, 0x28

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid initial window size: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 203
    :cond_1
    :try_start_1
    iget v4, v3, Lhng;->c:I

    sub-int v4, v1, v4

    .line 204
    iput v1, v3, Lhng;->c:I

    .line 205
    iget-object v1, v3, Lhng;->a:Lhna;

    invoke-virtual {v1}, Lhna;->d()[Lhmx;

    move-result-object v5

    array-length v6, v5

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    .line 207
    iget-object v0, v7, Lhmx;->h:Ljava/lang/Object;

    .line 208
    check-cast v0, Lhng$a;

    .line 209
    if-nez v0, :cond_2

    .line 210
    new-instance v0, Lhng$a;

    invoke-direct {v0, v3, v7}, Lhng$a;-><init>(Lhng;Lhmx;)V

    .line 212
    iput-object v0, v7, Lhmx;->h:Ljava/lang/Object;

    .line 215
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {v0, v4}, Lhng$a;->a(I)I

    goto :goto_1

    .line 216
    :cond_3
    if-lez v4, :cond_4

    .line 217
    invoke-virtual {v3}, Lhng;->b()V

    .line 218
    :cond_4
    iget-boolean v0, p0, Lhna$a;->b:Z

    if-eqz v0, :cond_5

    .line 219
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 220
    iget-object v0, v0, Lhna;->d:Lio/grpc/internal/dv;

    .line 221
    invoke-interface {v0}, Lio/grpc/internal/dv;->a()V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhna$a;->b:Z

    .line 223
    :cond_5
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 224
    invoke-virtual {v0}, Lhna;->c()Z

    .line 226
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 228
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 229
    invoke-virtual {v0, p1}, Lhme;->a(Lhof;)V

    .line 230
    return-void
.end method

.method public final a(ZII)V
    .locals 16

    .prologue
    .line 231
    if-nez p1, :cond_1

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lhna$a;->c:Lhna;

    .line 233
    iget-object v2, v2, Lhna;->e:Lhme;

    .line 234
    const/4 v3, 0x1

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v2, v3, v0, v1}, Lhme;->a(ZII)V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    const/4 v2, 0x0

    .line 236
    move/from16 v0, p2

    int-to-long v4, v0

    const/16 v3, 0x20

    shl-long/2addr v4, v3

    move/from16 v0, p3

    int-to-long v6, v0

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    .line 237
    move-object/from16 v0, p0

    iget-object v3, v0, Lhna$a;->c:Lhna;

    .line 238
    iget-object v3, v3, Lhna;->g:Ljava/lang/Object;

    .line 239
    monitor-enter v3

    .line 240
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lhna$a;->c:Lhna;

    .line 241
    iget-object v6, v6, Lhna;->m:Lio/grpc/internal/cp;

    .line 242
    if-eqz v6, :cond_3

    .line 243
    move-object/from16 v0, p0

    iget-object v6, v0, Lhna$a;->c:Lhna;

    .line 244
    iget-object v6, v6, Lhna;->m:Lio/grpc/internal/cp;

    .line 246
    iget-wide v6, v6, Lio/grpc/internal/cp;->a:J

    .line 247
    cmp-long v6, v6, v4

    if-nez v6, :cond_2

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lhna$a;->c:Lhna;

    .line 249
    iget-object v2, v2, Lhna;->m:Lio/grpc/internal/cp;

    .line 251
    move-object/from16 v0, p0

    iget-object v4, v0, Lhna$a;->c:Lhna;

    .line 252
    const/4 v5, 0x0

    iput-object v5, v4, Lhna;->m:Lio/grpc/internal/cp;

    .line 264
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    if-eqz v2, :cond_0

    .line 266
    invoke-virtual {v2}, Lio/grpc/internal/cp;->a()Z

    goto :goto_0

    .line 254
    :cond_2
    :try_start_1
    sget-object v6, Lhna;->a:Ljava/util/logging/Logger;

    .line 255
    sget-object v7, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v8, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v9, "ping"

    const-string v10, "Received unexpected ping ack. Expecting %d, got %d"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lhna$a;->c:Lhna;

    .line 257
    iget-object v13, v13, Lhna;->m:Lio/grpc/internal/cp;

    .line 259
    iget-wide v14, v13, Lio/grpc/internal/cp;->a:J

    .line 260
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v11, v12

    .line 261
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v8, v9, v4}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 264
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 262
    :cond_3
    :try_start_2
    sget-object v4, Lhna;->a:Ljava/util/logging/Logger;

    .line 263
    sget-object v5, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v6, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v7, "ping"

    const-string v8, "Received unexpected ping ack. No ping outstanding"

    invoke-virtual {v4, v5, v6, v7, v8}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(ZILhuj;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 54
    iget-object v0, p0, Lhna$a;->c:Lhna;

    invoke-virtual {v0, p2}, Lhna;->b(I)Lhmx;

    move-result-object v0

    .line 55
    if-nez v0, :cond_2

    .line 56
    iget-object v0, p0, Lhna$a;->c:Lhna;

    invoke-virtual {v0, p2}, Lhna;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 58
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 59
    sget-object v1, Lhns;->c:Lhns;

    invoke-virtual {v0, p2, v1}, Lhme;->a(ILhns;)V

    .line 60
    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, Lhuj;->f(J)V

    .line 74
    :goto_0
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 75
    iget v1, v0, Lhna;->j:I

    add-int/2addr v1, p4

    iput v1, v0, Lhna;->j:I

    .line 77
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 78
    iget v0, v0, Lhna;->j:I

    .line 79
    const/16 v1, 0x7fff

    if-lt v0, v1, :cond_0

    .line 80
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 81
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 82
    iget-object v1, p0, Lhna$a;->c:Lhna;

    .line 83
    iget v1, v1, Lhna;->j:I

    .line 84
    int-to-long v2, v1

    invoke-virtual {v0, v6, v2, v3}, Lhme;->a(IJ)V

    .line 85
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 86
    iput v6, v0, Lhna;->j:I

    .line 88
    :cond_0
    :goto_1
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lhna$a;->c:Lhna;

    sget-object v1, Lhns;->b:Lhns;

    const/16 v2, 0x2d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received data for unknown stream: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lhna;->a(Lhns;Ljava/lang/String;)V

    goto :goto_1

    .line 64
    :cond_2
    int-to-long v2, p4

    invoke-interface {p3, v2, v3}, Lhuj;->a(J)V

    .line 65
    new-instance v1, Lhuh;

    invoke-direct {v1}, Lhuh;-><init>()V

    .line 66
    invoke-interface {p3}, Lhuj;->a()Lhuh;

    move-result-object v2

    int-to-long v4, p4

    invoke-virtual {v1, v2, v4, v5}, Lhuh;->a_(Lhuh;J)V

    .line 67
    iget-object v2, p0, Lhna$a;->c:Lhna;

    .line 68
    iget-object v2, v2, Lhna;->g:Ljava/lang/Object;

    .line 69
    monitor-enter v2

    .line 71
    :try_start_0
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 72
    invoke-virtual {v0, v1, p1}, Lhmz;->a(Lhuh;Z)V

    .line 73
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(ZILjava/util/List;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 89
    .line 90
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 91
    iget-object v8, v0, Lhna;->g:Ljava/lang/Object;

    .line 92
    monitor-enter v8

    .line 93
    :try_start_0
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 94
    iget-object v0, v0, Lhna;->h:Ljava/util/Map;

    .line 95
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhmx;

    .line 96
    if-nez v0, :cond_2

    .line 97
    iget-object v0, p0, Lhna$a;->c:Lhna;

    invoke-virtual {v0, p2}, Lhna;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 99
    iget-object v0, v0, Lhna;->e:Lhme;

    .line 100
    sget-object v1, Lhns;->c:Lhns;

    invoke-virtual {v0, p2, v1}, Lhme;->a(ILhns;)V

    move v0, v6

    .line 174
    :goto_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 175
    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lhna$a;->c:Lhna;

    sget-object v1, Lhns;->b:Lhns;

    const/16 v2, 0x2f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received header for unknown stream: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-virtual {v0, v1, v2}, Lhna;->a(Lhns;Ljava/lang/String;)V

    .line 178
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_2
    :try_start_1
    iget-object v2, v0, Lhmx;->j:Lhmz;

    .line 105
    if-eqz p1, :cond_9

    .line 106
    invoke-static {p3}, Lio/grpc/internal/av;->b(Ljava/util/List;)Lhlh;

    move-result-object v9

    .line 107
    const-string v0, "trailers"

    invoke-static {v9, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-nez v0, :cond_3

    iget-boolean v0, v2, Lio/grpc/internal/cn;->n:Z

    if-nez v0, :cond_3

    .line 109
    invoke-static {v9}, Lio/grpc/internal/cn;->b(Lhlh;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 110
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_3

    .line 111
    iput-object v9, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 112
    :cond_3
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_4

    .line 113
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "trailers: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 114
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    const/4 v1, 0x0

    iget-object v3, v2, Lio/grpc/internal/cn;->l:Lhlh;

    invoke-virtual {v2, v0, v1, v3}, Lio/grpc/internal/cn;->b(Lhlw;ZLhlh;)V

    move v0, v6

    goto :goto_0

    .line 116
    :cond_4
    sget-object v0, Lhkw;->b:Lhlh$e;

    invoke-virtual {v9, v0}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    .line 117
    if-eqz v0, :cond_5

    .line 118
    sget-object v1, Lhkw;->a:Lhlh$e;

    invoke-virtual {v9, v1}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    move-object v7, v0

    .line 127
    :goto_1
    invoke-static {v9}, Lio/grpc/internal/cn;->d(Lhlh;)V

    .line 129
    const-string v0, "status"

    invoke-static {v7, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v0, "trailers"

    invoke-static {v9, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-boolean v0, v2, Lio/grpc/internal/a$c;->d:Z

    if-eqz v0, :cond_8

    .line 132
    sget-object v0, Lio/grpc/internal/a;->a:Ljava/util/logging/Logger;

    .line 133
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.AbstractClientStream$TransportState"

    const-string v3, "inboundTrailersReceived"

    const-string v4, "Received trailers on closed stream:\n {1}\n {2}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v5, v10

    const/4 v7, 0x1

    aput-object v9, v5, v7

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 134
    goto/16 :goto_0

    .line 119
    :cond_5
    iget-boolean v0, v2, Lio/grpc/internal/cn;->n:Z

    if-eqz v0, :cond_6

    .line 120
    sget-object v0, Lhlw;->d:Lhlw;

    const-string v1, "missing GRPC status in response"

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    .line 121
    :cond_6
    sget-object v0, Lio/grpc/internal/cn;->j:Lhlh$e;

    invoke-virtual {v9, v0}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 122
    if-eqz v0, :cond_7

    .line 123
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lio/grpc/internal/cf;->a(I)Lhlw;

    move-result-object v0

    .line 125
    :goto_2
    const-string v1, "missing GRPC status, inferred error from HTTP status code"

    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    .line 124
    :cond_7
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v1, "missing HTTP status code"

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    goto :goto_2

    .line 135
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {v2, v7, v0, v9}, Lio/grpc/internal/a$c;->a(Lhlw;ZLhlh;)V

    move v0, v6

    .line 136
    goto/16 :goto_0

    .line 137
    :cond_9
    invoke-static {p3}, Lio/grpc/internal/av;->a(Ljava/util/List;)Lhlh;

    move-result-object v1

    .line 138
    const-string v0, "headers"

    invoke-static {v1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_a

    .line 140
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "headers: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v0, v6

    .line 141
    goto/16 :goto_0

    .line 142
    :cond_a
    :try_start_2
    iget-boolean v0, v2, Lio/grpc/internal/cn;->n:Z

    if-eqz v0, :cond_c

    .line 143
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v3, "Received headers twice"

    invoke-virtual {v0, v3}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 144
    :try_start_3
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_b

    .line 145
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "headers: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 146
    iput-object v1, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 147
    invoke-static {v1}, Lio/grpc/internal/cn;->c(Lhlh;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_b
    move v0, v6

    .line 148
    goto/16 :goto_0

    .line 149
    :cond_c
    :try_start_4
    sget-object v0, Lio/grpc/internal/cn;->j:Lhlh$e;

    invoke-virtual {v1, v0}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 150
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x64

    if-lt v3, v4, :cond_e

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    const/16 v3, 0xc8

    if-ge v0, v3, :cond_e

    .line 151
    :try_start_5
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_d

    .line 152
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "headers: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 153
    iput-object v1, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 154
    invoke-static {v1}, Lio/grpc/internal/cn;->c(Lhlh;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_d
    move v0, v6

    .line 155
    goto/16 :goto_0

    .line 156
    :cond_e
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, v2, Lio/grpc/internal/cn;->n:Z

    .line 157
    invoke-static {v1}, Lio/grpc/internal/cn;->b(Lhlh;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 158
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_10

    .line 159
    :try_start_7
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_f

    .line 160
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "headers: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 161
    iput-object v1, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 162
    invoke-static {v1}, Lio/grpc/internal/cn;->c(Lhlh;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_f
    move v0, v6

    .line 163
    goto/16 :goto_0

    .line 164
    :cond_10
    :try_start_8
    invoke-static {v1}, Lio/grpc/internal/cn;->d(Lhlh;)V

    .line 165
    invoke-virtual {v2, v1}, Lio/grpc/internal/cn;->a(Lhlh;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 166
    :try_start_9
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v0, :cond_12

    .line 167
    iget-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x9

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "headers: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 168
    iput-object v1, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 169
    invoke-static {v1}, Lio/grpc/internal/cn;->c(Lhlh;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, v2, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;

    move v0, v6

    goto/16 :goto_0

    .line 170
    :catchall_0
    move-exception v0

    iget-object v3, v2, Lio/grpc/internal/cn;->k:Lhlw;

    if-eqz v3, :cond_11

    .line 171
    iget-object v3, v2, Lio/grpc/internal/cn;->k:Lhlw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "headers: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v3

    iput-object v3, v2, Lio/grpc/internal/cn;->k:Lhlw;

    .line 172
    iput-object v1, v2, Lio/grpc/internal/cn;->l:Lhlh;

    .line 173
    invoke-static {v1}, Lio/grpc/internal/cn;->c(Lhlh;)Ljava/nio/charset/Charset;

    move-result-object v1

    iput-object v1, v2, Lio/grpc/internal/cn;->m:Ljava/nio/charset/Charset;

    :cond_11
    throw v0

    .line 174
    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    :cond_12
    move v0, v6

    goto/16 :goto_0
.end method

.method public final run()V
    .locals 8

    .prologue
    .line 5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v7

    .line 6
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-nez v0, :cond_0

    .line 7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "OkHttpClientTransport"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 8
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lhna$a;->a:Lhnt;

    invoke-virtual {v0, p0}, Lhnt;->a(Lhnu;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 9
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 10
    iget-object v0, v0, Lhna;->t:Lio/grpc/internal/cz;

    .line 11
    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 13
    iget-object v0, v0, Lhna;->t:Lio/grpc/internal/cz;

    .line 14
    invoke-virtual {v0}, Lio/grpc/internal/cz;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    :try_start_1
    iget-object v1, p0, Lhna$a;->c:Lhna;

    const/4 v2, 0x0

    sget-object v3, Lhns;->b:Lhns;

    sget-object v4, Lhlw;->i:Lhlw;

    invoke-virtual {v4, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 31
    invoke-virtual {v1, v2, v3, v0}, Lhna;->a(ILhns;Lhlw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 32
    :try_start_2
    iget-object v0, p0, Lhna$a;->a:Lhnt;

    invoke-virtual {v0}, Lhnt;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 37
    :goto_1
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 38
    iget-object v0, v0, Lhna;->d:Lio/grpc/internal/dv;

    .line 39
    invoke-interface {v0}, Lio/grpc/internal/dv;->b()V

    .line 40
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-nez v0, :cond_1

    .line 41
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 53
    :cond_1
    :goto_2
    return-void

    .line 15
    :cond_2
    :try_start_3
    iget-object v0, p0, Lhna$a;->c:Lhna;

    const/4 v1, 0x0

    sget-object v2, Lhns;->d:Lhns;

    sget-object v3, Lhlw;->i:Lhlw;

    const-string v4, "End of stream or IOException"

    .line 16
    invoke-virtual {v3, v4}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v3

    .line 18
    invoke-virtual {v0, v1, v2, v3}, Lhna;->a(ILhns;Lhlw;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 19
    :try_start_4
    iget-object v0, p0, Lhna$a;->a:Lhnt;

    invoke-virtual {v0}, Lhnt;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 24
    :goto_3
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 25
    iget-object v0, v0, Lhna;->d:Lio/grpc/internal/dv;

    .line 26
    invoke-interface {v0}, Lio/grpc/internal/dv;->b()V

    .line 27
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-nez v0, :cond_1

    .line 28
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    goto :goto_2

    .line 21
    :catch_1
    move-exception v5

    .line 22
    sget-object v0, Lhna;->a:Ljava/util/logging/Logger;

    .line 23
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v3, "run"

    const-string v4, "Exception closing frame reader"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 34
    :catch_2
    move-exception v5

    .line 35
    sget-object v0, Lhna;->a:Ljava/util/logging/Logger;

    .line 36
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v3, "run"

    const-string v4, "Exception closing frame reader"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 42
    :catchall_0
    move-exception v0

    move-object v6, v0

    .line 43
    :try_start_5
    iget-object v0, p0, Lhna$a;->a:Lhnt;

    invoke-virtual {v0}, Lhnt;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 48
    :goto_4
    iget-object v0, p0, Lhna$a;->c:Lhna;

    .line 49
    iget-object v0, v0, Lhna;->d:Lio/grpc/internal/dv;

    .line 50
    invoke-interface {v0}, Lio/grpc/internal/dv;->b()V

    .line 51
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-nez v0, :cond_3

    .line 52
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :cond_3
    throw v6

    .line 45
    :catch_3
    move-exception v5

    .line 46
    sget-object v0, Lhna;->a:Ljava/util/logging/Logger;

    .line 47
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "io.grpc.okhttp.OkHttpClientTransport$ClientFrameHandler"

    const-string v3, "run"

    const-string v4, "Exception closing frame reader"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method
