.class public final Lfvi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final context:Landroid/content/Context;

.field public hasPermission:Z

.field public final permission:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfvi;->context:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lfvi;->permission:Ljava/lang/String;

    .line 4
    invoke-virtual {p0}, Lfvi;->checkPermission()Z

    move-result v0

    iput-boolean v0, p0, Lfvi;->hasPermission:Z

    .line 5
    return-void
.end method


# virtual methods
.method public final checkPermission()Z
    .locals 4

    .prologue
    .line 9
    iget-object v0, p0, Lfvi;->context:Landroid/content/Context;

    iget-object v1, p0, Lfvi;->permission:Ljava/lang/String;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPermissionChanged()Z
    .locals 2

    .prologue
    .line 6
    iget-boolean v0, p0, Lfvi;->hasPermission:Z

    .line 7
    invoke-virtual {p0}, Lfvi;->checkPermission()Z

    move-result v1

    iput-boolean v1, p0, Lfvi;->hasPermission:Z

    .line 8
    iget-boolean v1, p0, Lfvi;->hasPermission:Z

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
