.class public Lbef;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lbef;->a:Ljava/util/concurrent/ExecutorService;

    .line 9
    iput-object p2, p0, Lbef;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 10
    iput-object p3, p0, Lbef;->c:Ljava/util/concurrent/Executor;

    .line 11
    iput-object p4, p0, Lbef;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 12
    return-void
.end method


# virtual methods
.method public a(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;)Lbdz;
    .locals 6

    .prologue
    .line 1
    new-instance v0, Lbdw;

    .line 2
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/FragmentManager;

    .line 3
    invoke-static {p2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 4
    invoke-static {p3}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbec;

    iget-object v4, p0, Lbef;->d:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v5, p0, Lbef;->c:Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v5}, Lbdw;-><init>(Landroid/app/FragmentManager;Ljava/lang/String;Lbec;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V

    .line 5
    return-object v0
.end method

.method public a(Lbec;)Lbdz;
    .locals 4

    .prologue
    .line 6
    new-instance v1, Lbdu;

    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbec;

    iget-object v2, p0, Lbef;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v3, p0, Lbef;->a:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v0, v2, v3}, Lbdu;-><init>(Lbec;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;)V

    return-object v1
.end method
