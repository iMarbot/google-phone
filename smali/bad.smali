.class public final Lbad;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static g:Ljava/nio/charset/Charset;

.field private static r:S

.field private static s:S

.field private static t:S

.field private static u:S

.field private static v:S

.field private static w:S

.field private static x:S


# instance fields
.field public final a:Lazy;

.field public b:I

.field public c:Lbai;

.field public d:Lbai;

.field public e:Lbai;

.field public final f:Ljava/util/TreeMap;

.field private h:I

.field private i:I

.field private j:I

.field private k:Lbag;

.field private l:Z

.field private m:Z

.field private n:I

.field private o:[B

.field private p:I

.field private q:Lbaa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 415
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lbad;->g:Ljava/nio/charset/Charset;

    .line 416
    sget v0, Lbaa;->b:I

    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->r:S

    .line 417
    sget v0, Lbaa;->c:I

    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->s:S

    .line 418
    sget v0, Lbaa;->h:I

    .line 419
    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->t:S

    .line 420
    sget v0, Lbaa;->f:I

    .line 421
    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->u:S

    .line 422
    sget v0, Lbaa;->g:I

    .line 423
    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->v:S

    .line 424
    sget v0, Lbaa;->d:I

    .line 425
    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->w:S

    .line 426
    sget v0, Lbaa;->e:I

    .line 427
    invoke-static {v0}, Lbaa;->a(I)S

    move-result v0

    sput-short v0, Lbad;->x:S

    .line 428
    return-void
.end method

.method constructor <init>(Ljava/io/InputStream;ILbaa;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v4, p0, Lbad;->i:I

    .line 11
    iput v4, p0, Lbad;->j:I

    .line 12
    iput-boolean v4, p0, Lbad;->m:Z

    .line 13
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null argument inputStream to ExifParser"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iput-object p3, p0, Lbad;->q:Lbaa;

    .line 17
    invoke-direct {p0, p1}, Lbad;->a(Ljava/io/InputStream;)Z

    move-result v0

    iput-boolean v0, p0, Lbad;->m:Z

    .line 18
    new-instance v0, Lazy;

    invoke-direct {v0, p1}, Lazy;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lbad;->a:Lazy;

    .line 19
    const/16 v0, 0x3f

    iput v0, p0, Lbad;->h:I

    .line 20
    iget-boolean v0, p0, Lbad;->m:Z

    if-nez v0, :cond_2

    .line 41
    :cond_1
    :goto_0
    return-void

    .line 23
    :cond_2
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->a()S

    move-result v0

    .line 24
    const/16 v1, 0x4949

    if-ne v1, v0, :cond_3

    .line 25
    iget-object v0, p0, Lbad;->a:Lazy;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lazy;->a(Ljava/nio/ByteOrder;)V

    .line 29
    :goto_1
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->a()S

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    .line 30
    new-instance v0, Lbac;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lbac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_3
    const/16 v1, 0x4d4d

    if-ne v1, v0, :cond_4

    .line 27
    iget-object v0, p0, Lbad;->a:Lazy;

    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lazy;->a(Ljava/nio/ByteOrder;)V

    goto :goto_1

    .line 28
    :cond_4
    new-instance v0, Lbac;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lbac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_5
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->c()J

    move-result-wide v0

    .line 32
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 33
    new-instance v2, Lbac;

    const/16 v3, 0x23

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid offset "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lbac;-><init>(Ljava/lang/String;)V

    throw v2

    .line 34
    :cond_6
    long-to-int v2, v0

    iput v2, p0, Lbad;->p:I

    .line 35
    iput v4, p0, Lbad;->b:I

    .line 36
    invoke-direct {p0, v4}, Lbad;->a(I)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0}, Lbad;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    :cond_7
    invoke-direct {p0, v4, v0, v1}, Lbad;->a(IJ)V

    .line 38
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 39
    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lbad;->o:[B

    .line 40
    iget-object v0, p0, Lbad;->o:[B

    invoke-virtual {p0, v0}, Lbad;->a([B)I

    goto/16 :goto_0
.end method

.method private final a(IJ)V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lbaf;

    invoke-direct {p0, p1}, Lbad;->a(I)Z

    move-result v3

    invoke-direct {v2, p1, v3}, Lbaf;-><init>(IZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    return-void
.end method

.method private final a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 7
    :cond_0
    :goto_0
    return v0

    .line 2
    :pswitch_0
    iget v2, p0, Lbad;->h:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 3
    :pswitch_1
    iget v2, p0, Lbad;->h:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 4
    :pswitch_2
    iget v2, p0, Lbad;->h:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 5
    :pswitch_3
    iget v2, p0, Lbad;->h:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 6
    :pswitch_4
    iget v2, p0, Lbad;->h:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private final a(II)Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lbad;->q:Lbaa;

    invoke-virtual {v0}, Lbaa;->a()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 248
    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lbaa;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final a(Ljava/io/InputStream;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 389
    new-instance v3, Lazy;

    invoke-direct {v3, p1}, Lazy;-><init>(Ljava/io/InputStream;)V

    .line 390
    invoke-virtual {v3}, Lazy;->a()S

    move-result v1

    const/16 v2, -0x28

    if-eq v1, v2, :cond_0

    .line 391
    new-instance v0, Lbac;

    const-string v1, "Invalid JPEG format"

    invoke-direct {v0, v1}, Lbac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_0
    invoke-virtual {v3}, Lazy;->a()S

    move-result v1

    move v2, v1

    .line 393
    :goto_0
    const/16 v1, -0x27

    if-eq v2, v1, :cond_1

    invoke-static {v2}, Lapw;->a(S)Z

    move-result v1

    if-nez v1, :cond_1

    .line 395
    invoke-virtual {v3}, Lazy;->a()S

    move-result v1

    const v4, 0xffff

    and-int/2addr v1, v4

    .line 397
    const/16 v4, -0x1f

    if-ne v2, v4, :cond_2

    .line 398
    const/16 v2, 0x8

    if-lt v1, v2, :cond_2

    .line 399
    invoke-virtual {v3}, Lazy;->b()I

    move-result v2

    .line 400
    invoke-virtual {v3}, Lazy;->a()S

    move-result v4

    .line 401
    add-int/lit8 v1, v1, -0x6

    .line 402
    const v5, 0x45786966

    if-ne v2, v5, :cond_2

    if-nez v4, :cond_2

    .line 403
    iput v1, p0, Lbad;->n:I

    .line 404
    const/4 v0, 0x1

    .line 410
    :cond_1
    :goto_1
    return v0

    .line 405
    :cond_2
    const/4 v2, 0x2

    if-lt v1, v2, :cond_3

    add-int/lit8 v2, v1, -0x2

    int-to-long v4, v2

    add-int/lit8 v1, v1, -0x2

    int-to-long v6, v1

    invoke-virtual {v3, v6, v7}, Lazy;->skip(J)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    .line 406
    :cond_3
    const-string v1, "ExifParser.seekTiffData"

    const-string v2, "Invalid JPEG format."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 408
    :cond_4
    invoke-virtual {v3}, Lazy;->a()S

    move-result v1

    move v2, v1

    .line 409
    goto :goto_0
.end method

.method private final b(I)V
    .locals 6

    .prologue
    .line 138
    iget-object v1, p0, Lbad;->a:Lazy;

    int-to-long v2, p1

    .line 139
    iget v0, v1, Lazy;->a:I

    int-to-long v4, v0

    .line 140
    sub-long/2addr v2, v4

    .line 141
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 143
    invoke-virtual {v1, v2, v3}, Lazy;->skip(J)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 144
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    :cond_1
    :goto_1
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, p1, :cond_2

    .line 146
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    goto :goto_1

    .line 147
    :cond_2
    return-void
.end method

.method private final b(IJ)V
    .locals 4

    .prologue
    .line 150
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lbag;

    const/4 v3, 0x4

    invoke-direct {v2, v3, p1}, Lbag;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    return-void
.end method

.method private final b(Lbai;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 197
    .line 198
    iget v1, p1, Lbai;->f:I

    .line 199
    if-nez v1, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-short v1, p1, Lbai;->c:S

    .line 205
    iget v2, p1, Lbai;->g:I

    .line 207
    sget-short v3, Lbad;->r:S

    if-ne v1, v3, :cond_3

    sget v3, Lbaa;->b:I

    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 208
    invoke-direct {p0, v5}, Lbad;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v4}, Lbad;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 209
    :cond_2
    invoke-virtual {p1, v0}, Lbai;->b(I)J

    move-result-wide v0

    invoke-direct {p0, v5, v0, v1}, Lbad;->a(IJ)V

    goto :goto_0

    .line 210
    :cond_3
    sget-short v3, Lbad;->s:S

    if-ne v1, v3, :cond_4

    sget v3, Lbaa;->c:I

    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 211
    invoke-direct {p0, v6}, Lbad;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    invoke-virtual {p1, v0}, Lbai;->b(I)J

    move-result-wide v0

    invoke-direct {p0, v6, v0, v1}, Lbad;->a(IJ)V

    goto :goto_0

    .line 213
    :cond_4
    sget-short v3, Lbad;->t:S

    if-ne v1, v3, :cond_5

    sget v3, Lbaa;->h:I

    .line 214
    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 215
    invoke-direct {p0, v4}, Lbad;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    invoke-virtual {p1, v0}, Lbai;->b(I)J

    move-result-wide v0

    invoke-direct {p0, v4, v0, v1}, Lbad;->a(IJ)V

    goto :goto_0

    .line 217
    :cond_5
    sget-short v3, Lbad;->u:S

    if-ne v1, v3, :cond_6

    sget v3, Lbaa;->f:I

    .line 218
    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 219
    invoke-direct {p0}, Lbad;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    invoke-virtual {p1, v0}, Lbai;->b(I)J

    move-result-wide v0

    .line 221
    iget-object v2, p0, Lbad;->f:Ljava/util/TreeMap;

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lbag;

    invoke-direct {v1, v4}, Lbag;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 223
    :cond_6
    sget-short v3, Lbad;->v:S

    if-ne v1, v3, :cond_7

    sget v3, Lbaa;->g:I

    .line 224
    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 225
    invoke-direct {p0}, Lbad;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iput-object p1, p0, Lbad;->e:Lbai;

    goto/16 :goto_0

    .line 227
    :cond_7
    sget-short v3, Lbad;->w:S

    if-ne v1, v3, :cond_9

    sget v3, Lbaa;->d:I

    invoke-direct {p0, v2, v3}, Lbad;->a(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 228
    invoke-direct {p0}, Lbad;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    invoke-virtual {p1}, Lbai;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 231
    :goto_1
    iget v1, p1, Lbai;->f:I

    .line 232
    if-ge v0, v1, :cond_0

    .line 234
    iget-short v1, p1, Lbai;->d:S

    .line 236
    invoke-virtual {p1, v0}, Lbai;->b(I)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lbad;->b(IJ)V

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 238
    :cond_8
    iget-object v1, p0, Lbad;->f:Ljava/util/TreeMap;

    .line 239
    iget v2, p1, Lbai;->i:I

    .line 240
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lbae;

    invoke-direct {v3, p1, v0}, Lbae;-><init>(Lbai;Z)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 241
    :cond_9
    sget-short v0, Lbad;->x:S

    if-ne v1, v0, :cond_0

    sget v0, Lbaa;->e:I

    .line 242
    invoke-direct {p0, v2, v0}, Lbad;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-direct {p0}, Lbad;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p1}, Lbai;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iput-object p1, p0, Lbad;->d:Lbai;

    goto/16 :goto_0
.end method

.method private final b()Z
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lbad;->h:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 110
    iget v0, p0, Lbad;->i:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lbad;->j:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v1, v0

    .line 111
    iget-object v0, p0, Lbad;->a:Lazy;

    .line 112
    iget v0, v0, Lazy;->a:I

    .line 114
    if-le v0, v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-boolean v2, p0, Lbad;->l:Z

    if-eqz v2, :cond_3

    .line 117
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_4

    .line 118
    invoke-direct {p0}, Lbad;->e()Lbai;

    move-result-object v2

    iput-object v2, p0, Lbad;->c:Lbai;

    .line 119
    add-int/lit8 v0, v0, 0xc

    .line 120
    iget-object v2, p0, Lbad;->c:Lbai;

    if-eqz v2, :cond_2

    .line 121
    iget-object v2, p0, Lbad;->c:Lbai;

    invoke-direct {p0, v2}, Lbad;->b(Lbai;)V

    goto :goto_1

    .line 122
    :cond_3
    invoke-direct {p0, v1}, Lbad;->b(I)V

    .line 123
    :cond_4
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v0

    .line 124
    iget v2, p0, Lbad;->b:I

    if-nez v2, :cond_0

    .line 125
    invoke-direct {p0, v4}, Lbad;->a(I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lbad;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 127
    invoke-direct {p0, v4, v0, v1}, Lbad;->a(IJ)V

    goto :goto_0
.end method

.method private final d()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 129
    iget v2, p0, Lbad;->b:I

    packed-switch v2, :pswitch_data_0

    .line 137
    :cond_0
    :goto_0
    return v0

    .line 130
    :pswitch_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lbad;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x4

    .line 131
    invoke-direct {p0, v2}, Lbad;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 132
    invoke-direct {p0, v3}, Lbad;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 133
    invoke-direct {p0, v1}, Lbad;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 135
    :pswitch_1
    invoke-direct {p0}, Lbad;->b()Z

    move-result v0

    goto :goto_0

    .line 136
    :pswitch_2
    invoke-direct {p0, v3}, Lbad;->a(I)Z

    move-result v0

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final e()Lbai;
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 152
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->a()S

    move-result v1

    .line 153
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->a()S

    move-result v2

    .line 154
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->c()J

    move-result-wide v8

    .line 155
    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    .line 156
    new-instance v0, Lbac;

    const-string v1, "Number of component is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lbac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    invoke-static {v2}, Lbai;->a(S)Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    const-string v0, "ExifParser.readTag"

    const-string v3, "Tag %04x: Invalid data type %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    iget-object v0, p0, Lbad;->a:Lazy;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lazy;->skip(J)J

    .line 160
    const/4 v0, 0x0

    .line 196
    :goto_0
    return-object v0

    .line 161
    :cond_1
    new-instance v0, Lbai;

    long-to-int v3, v8

    iget v4, p0, Lbad;->b:I

    long-to-int v7, v8

    if-eqz v7, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Lbai;-><init>(SSIIZ)V

    .line 164
    iget v1, v0, Lbai;->f:I

    .line 166
    iget-short v3, v0, Lbai;->d:S

    .line 168
    sget-object v4, Lbai;->b:[I

    aget v3, v4, v3

    .line 169
    mul-int/2addr v1, v3

    .line 171
    const/4 v3, 0x4

    if-le v1, v3, :cond_5

    .line 172
    iget-object v1, p0, Lbad;->a:Lazy;

    invoke-virtual {v1}, Lazy;->c()J

    move-result-wide v4

    .line 173
    cmp-long v1, v4, v10

    if-lez v1, :cond_3

    .line 174
    new-instance v0, Lbac;

    const-string v1, "offset is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lbac;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v5, v6

    .line 161
    goto :goto_1

    .line 175
    :cond_3
    iget v1, p0, Lbad;->p:I

    int-to-long v10, v1

    cmp-long v1, v4, v10

    if-gez v1, :cond_4

    const/4 v1, 0x7

    if-ne v2, v1, :cond_4

    .line 176
    long-to-int v1, v8

    new-array v1, v1, [B

    .line 177
    iget-object v2, p0, Lbad;->o:[B

    long-to-int v3, v4

    add-int/lit8 v3, v3, -0x8

    long-to-int v4, v8

    invoke-static {v2, v3, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 178
    invoke-virtual {v0, v1}, Lbai;->a([B)Z

    goto :goto_0

    .line 180
    :cond_4
    long-to-int v1, v4

    .line 181
    iput v1, v0, Lbai;->i:I

    goto :goto_0

    .line 184
    :cond_5
    iget-boolean v2, v0, Lbai;->e:Z

    .line 187
    iput-boolean v6, v0, Lbai;->e:Z

    .line 188
    invoke-virtual {p0, v0}, Lbad;->a(Lbai;)V

    .line 190
    iput-boolean v2, v0, Lbai;->e:Z

    .line 191
    iget-object v2, p0, Lbad;->a:Lazy;

    rsub-int/lit8 v1, v1, 0x4

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lazy;->skip(J)J

    .line 192
    iget-object v1, p0, Lbad;->a:Lazy;

    .line 193
    iget v1, v1, Lazy;->a:I

    .line 194
    add-int/lit8 v1, v1, -0x4

    .line 195
    iput v1, v0, Lbai;->i:I

    goto :goto_0
.end method

.method private final f()J
    .locals 4

    .prologue
    .line 412
    .line 413
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0}, Lazy;->b()I

    move-result v0

    .line 414
    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method protected final a()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x5

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 42
    :cond_0
    iget-boolean v4, p0, Lbad;->m:Z

    if-nez v4, :cond_2

    move v0, v2

    .line 109
    :cond_1
    :goto_0
    return v0

    .line 44
    :cond_2
    iget-object v4, p0, Lbad;->a:Lazy;

    .line 45
    iget v4, v4, Lazy;->a:I

    .line 47
    iget v5, p0, Lbad;->i:I

    add-int/lit8 v5, v5, 0x2

    iget v6, p0, Lbad;->j:I

    mul-int/lit8 v6, v6, 0xc

    add-int/2addr v5, v6

    .line 48
    if-ge v4, v5, :cond_3

    .line 49
    invoke-direct {p0}, Lbad;->e()Lbai;

    move-result-object v4

    iput-object v4, p0, Lbad;->c:Lbai;

    .line 50
    iget-object v4, p0, Lbad;->c:Lbai;

    if-eqz v4, :cond_0

    .line 52
    iget-boolean v1, p0, Lbad;->l:Z

    if-eqz v1, :cond_1

    .line 53
    iget-object v1, p0, Lbad;->c:Lbai;

    invoke-direct {p0, v1}, Lbad;->b(Lbai;)V

    goto :goto_0

    .line 55
    :cond_3
    if-ne v4, v5, :cond_5

    .line 56
    iget v4, p0, Lbad;->b:I

    if-nez v4, :cond_6

    .line 57
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v4

    .line 58
    invoke-direct {p0, v0}, Lbad;->a(I)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lbad;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 59
    :cond_4
    cmp-long v1, v4, v8

    if-eqz v1, :cond_5

    .line 60
    invoke-direct {p0, v0, v4, v5}, Lbad;->a(IJ)V

    .line 72
    :cond_5
    :goto_1
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-eqz v0, :cond_d

    .line 73
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v4

    .line 74
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 75
    :try_start_0
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lbad;->b(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    instance-of v0, v1, Lbaf;

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 84
    check-cast v0, Lbaf;

    iget v0, v0, Lbaf;->a:I

    iput v0, p0, Lbad;->b:I

    .line 85
    iget-object v0, p0, Lbad;->a:Lazy;

    .line 86
    invoke-virtual {v0}, Lazy;->a()S

    move-result v0

    const v5, 0xffff

    and-int/2addr v0, v5

    .line 87
    iput v0, p0, Lbad;->j:I

    .line 88
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lbad;->i:I

    .line 89
    iget v0, p0, Lbad;->j:I

    mul-int/lit8 v0, v0, 0xc

    iget v4, p0, Lbad;->i:I

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x2

    iget v4, p0, Lbad;->n:I

    if-le v0, v4, :cond_8

    .line 90
    const-string v0, "ExifParser.next"

    iget v1, p0, Lbad;->b:I

    const/16 v4, 0x1f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid size of IFD "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 91
    goto/16 :goto_0

    .line 63
    :cond_6
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 64
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lbad;->a:Lazy;

    .line 65
    iget v4, v4, Lazy;->a:I

    .line 66
    sub-int/2addr v0, v4

    .line 67
    :goto_2
    if-ge v0, v1, :cond_7

    .line 68
    const-string v1, "ExifParser.next"

    const/16 v4, 0x2d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid size of link to next IFD: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 69
    :cond_7
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v0

    .line 70
    cmp-long v4, v0, v8

    if-eqz v4, :cond_5

    .line 71
    const-string v4, "ExifParser.next"

    const/16 v5, 0x2e

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid link to next IFD: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v4, v0, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 78
    :catch_0
    move-exception v0

    const-string v0, "ExifParser.next"

    .line 79
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x39

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Failed to skip to data at: "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", the file may be broken."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [Ljava/lang/Object;

    .line 81
    invoke-static {v0, v1, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 92
    :cond_8
    invoke-direct {p0}, Lbad;->d()Z

    move-result v0

    iput-boolean v0, p0, Lbad;->l:Z

    move-object v0, v1

    .line 93
    check-cast v0, Lbaf;

    iget-boolean v0, v0, Lbaf;->b:Z

    if-eqz v0, :cond_9

    move v0, v3

    .line 94
    goto/16 :goto_0

    .line 95
    :cond_9
    invoke-direct {p0}, Lbad;->c()V

    goto/16 :goto_1

    .line 96
    :cond_a
    instance-of v0, v1, Lbag;

    if-eqz v0, :cond_b

    .line 97
    check-cast v1, Lbag;

    iput-object v1, p0, Lbad;->k:Lbag;

    .line 98
    iget-object v0, p0, Lbad;->k:Lbag;

    iget v0, v0, Lbag;->a:I

    goto/16 :goto_0

    .line 99
    :cond_b
    check-cast v1, Lbae;

    .line 100
    iget-object v0, v1, Lbae;->a:Lbai;

    iput-object v0, p0, Lbad;->c:Lbai;

    .line 101
    iget-object v0, p0, Lbad;->c:Lbai;

    .line 102
    iget-short v0, v0, Lbai;->d:S

    .line 103
    const/4 v4, 0x7

    if-eq v0, v4, :cond_c

    .line 104
    iget-object v0, p0, Lbad;->c:Lbai;

    invoke-virtual {p0, v0}, Lbad;->a(Lbai;)V

    .line 105
    iget-object v0, p0, Lbad;->c:Lbai;

    invoke-direct {p0, v0}, Lbad;->b(Lbai;)V

    .line 106
    :cond_c
    iget-boolean v0, v1, Lbae;->b:Z

    if-eqz v0, :cond_5

    .line 107
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 109
    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_2
.end method

.method protected final a([B)I
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lbad;->a:Lazy;

    invoke-virtual {v0, p1}, Lazy;->read([B)I

    move-result v0

    return v0
.end method

.method final a(Lbai;)V
    .locals 10

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 249
    .line 250
    iget-short v0, p1, Lbai;->d:S

    .line 252
    if-eq v0, v7, :cond_0

    if-eq v0, v8, :cond_0

    if-ne v0, v6, :cond_1

    .line 254
    :cond_0
    iget v2, p1, Lbai;->f:I

    .line 256
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 257
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lbad;->a:Lazy;

    .line 258
    iget v3, v3, Lazy;->a:I

    .line 259
    add-int/2addr v2, v3

    if-ge v0, v2, :cond_1

    .line 260
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 261
    instance-of v2, v0, Lbag;

    if-eqz v2, :cond_4

    .line 262
    const-string v2, "ExifParser.readFullTagValue"

    const-string v3, "Thumbnail overlaps value for tag: \n"

    .line 263
    invoke-virtual {p1}, Lbai;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-array v3, v1, [Ljava/lang/Object;

    .line 264
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 266
    const-string v2, "ExifParser.readFullTagValue"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid thumbnail offset: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    :cond_1
    :goto_1
    iget-short v0, p1, Lbai;->d:S

    .line 287
    packed-switch v0, :pswitch_data_0

    .line 388
    :cond_2
    :goto_2
    :pswitch_0
    return-void

    .line 263
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_4
    instance-of v2, v0, Lbaf;

    if-eqz v2, :cond_6

    .line 269
    const-string v2, "ExifParser.readFullTagValue"

    check-cast v0, Lbaf;

    iget v0, v0, Lbaf;->a:I

    .line 270
    invoke-virtual {p1}, Lbai;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Ifd "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " overlaps value for tag: \n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    .line 271
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    :cond_5
    :goto_3
    iget-object v0, p0, Lbad;->f:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lbad;->a:Lazy;

    .line 278
    iget v2, v2, Lazy;->a:I

    .line 279
    sub-int/2addr v0, v2

    .line 280
    const-string v2, "ExifParser.readFullTagValue"

    .line 281
    invoke-virtual {p1}, Lbai;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x34

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid size of tag: \n"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " setting count to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    .line 282
    invoke-static {v2, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 284
    iput v0, p1, Lbai;->f:I

    goto/16 :goto_1

    .line 272
    :cond_6
    instance-of v2, v0, Lbae;

    if-eqz v2, :cond_5

    .line 273
    const-string v2, "ExifParser.readFullTagValue"

    check-cast v0, Lbae;

    iget-object v0, v0, Lbae;->a:Lbai;

    .line 274
    invoke-virtual {v0}, Lbai;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    invoke-virtual {p1}, Lbai;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2e

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Tag value for tag: \n"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " overlaps value for tag: \n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    .line 276
    invoke-static {v2, v0, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 289
    :pswitch_1
    iget v0, p1, Lbai;->f:I

    .line 290
    new-array v0, v0, [B

    .line 291
    invoke-virtual {p0, v0}, Lbad;->a([B)I

    .line 292
    invoke-virtual {p1, v0}, Lbai;->a([B)Z

    goto/16 :goto_2

    .line 295
    :pswitch_2
    iget v0, p1, Lbai;->f:I

    .line 297
    sget-object v2, Lbad;->g:Ljava/nio/charset/Charset;

    .line 298
    if-lez v0, :cond_9

    .line 299
    iget-object v3, p0, Lbad;->a:Lazy;

    .line 300
    new-array v4, v0, [B

    .line 302
    array-length v0, v4

    invoke-virtual {v3, v4, v1, v0}, Lazy;->a([BII)V

    .line 303
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 307
    :goto_4
    iget-short v2, p1, Lbai;->d:S

    if-eq v2, v7, :cond_7

    iget-short v2, p1, Lbai;->d:S

    if-ne v2, v8, :cond_2

    .line 309
    :cond_7
    sget-object v2, Lbai;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 311
    array-length v2, v0

    if-lez v2, :cond_b

    .line 312
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-byte v1, v0, v1

    if-eqz v1, :cond_8

    iget-short v1, p1, Lbai;->d:S

    if-ne v1, v8, :cond_a

    .line 317
    :cond_8
    :goto_5
    array-length v1, v0

    .line 318
    invoke-virtual {p1, v1}, Lbai;->c(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 320
    iput v1, p1, Lbai;->f:I

    .line 321
    iput-object v0, p1, Lbai;->h:Ljava/lang/Object;

    goto/16 :goto_2

    .line 305
    :cond_9
    const-string v0, ""

    goto :goto_4

    .line 314
    :cond_a
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto :goto_5

    .line 315
    :cond_b
    iget-short v2, p1, Lbai;->d:S

    if-ne v2, v7, :cond_8

    iget v2, p1, Lbai;->f:I

    if-ne v2, v6, :cond_8

    .line 316
    new-array v0, v6, [B

    aput-byte v1, v0, v1

    goto :goto_5

    .line 326
    :pswitch_3
    iget v0, p1, Lbai;->f:I

    .line 327
    new-array v0, v0, [J

    .line 328
    array-length v2, v0

    :goto_6
    if-ge v1, v2, :cond_c

    .line 329
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 330
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 332
    :cond_c
    array-length v1, v0

    invoke-virtual {p1, v1}, Lbai;->c(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-short v1, p1, Lbai;->d:S

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 334
    invoke-static {v0}, Lbai;->a([J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 336
    iput-object v0, p1, Lbai;->h:Ljava/lang/Object;

    .line 337
    array-length v0, v0

    iput v0, p1, Lbai;->f:I

    goto/16 :goto_2

    .line 342
    :pswitch_4
    iget v0, p1, Lbai;->f:I

    .line 343
    new-array v0, v0, [Lbak;

    .line 344
    array-length v2, v0

    :goto_7
    if-ge v1, v2, :cond_d

    .line 346
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v4

    .line 347
    invoke-direct {p0}, Lbad;->f()J

    move-result-wide v6

    .line 348
    new-instance v3, Lbak;

    invoke-direct {v3, v4, v5, v6, v7}, Lbak;-><init>(JJ)V

    .line 349
    aput-object v3, v0, v1

    .line 350
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 351
    :cond_d
    invoke-virtual {p1, v0}, Lbai;->a([Lbak;)Z

    goto/16 :goto_2

    .line 354
    :pswitch_5
    iget v0, p1, Lbai;->f:I

    .line 355
    new-array v0, v0, [I

    .line 356
    array-length v2, v0

    :goto_8
    if-ge v1, v2, :cond_e

    .line 358
    iget-object v3, p0, Lbad;->a:Lazy;

    invoke-virtual {v3}, Lazy;->a()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    .line 359
    aput v3, v0, v1

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 361
    :cond_e
    invoke-virtual {p1, v0}, Lbai;->a([I)Z

    goto/16 :goto_2

    .line 364
    :pswitch_6
    iget v0, p1, Lbai;->f:I

    .line 365
    new-array v0, v0, [I

    .line 366
    array-length v2, v0

    :goto_9
    if-ge v1, v2, :cond_f

    .line 368
    iget-object v3, p0, Lbad;->a:Lazy;

    invoke-virtual {v3}, Lazy;->b()I

    move-result v3

    .line 369
    aput v3, v0, v1

    .line 370
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 371
    :cond_f
    invoke-virtual {p1, v0}, Lbai;->a([I)Z

    goto/16 :goto_2

    .line 374
    :pswitch_7
    iget v0, p1, Lbai;->f:I

    .line 375
    new-array v2, v0, [Lbak;

    .line 376
    array-length v3, v2

    move v0, v1

    :goto_a
    if-ge v0, v3, :cond_10

    .line 379
    iget-object v1, p0, Lbad;->a:Lazy;

    invoke-virtual {v1}, Lazy;->b()I

    move-result v1

    .line 382
    iget-object v4, p0, Lbad;->a:Lazy;

    invoke-virtual {v4}, Lazy;->b()I

    move-result v4

    .line 384
    new-instance v5, Lbak;

    int-to-long v6, v1

    int-to-long v8, v4

    invoke-direct {v5, v6, v7, v8, v9}, Lbak;-><init>(JJ)V

    .line 385
    aput-object v5, v2, v0

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 387
    :cond_10
    invoke-virtual {p1, v2}, Lbai;->a([Lbak;)Z

    goto/16 :goto_2

    .line 287
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
