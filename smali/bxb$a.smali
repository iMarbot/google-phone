.class public final Lbxb$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbxb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:Landroid/hardware/display/DisplayManager;

.field private b:Z

.field private synthetic c:Lbxb;


# direct methods
.method constructor <init>(Lbxb;Landroid/hardware/display/DisplayManager;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lbxb$a;->c:Lbxb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxb$a;->b:Z

    .line 3
    iput-object p2, p0, Lbxb$a;->a:Landroid/hardware/display/DisplayManager;

    .line 4
    return-void
.end method


# virtual methods
.method public final onDisplayAdded(I)V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public final onDisplayChanged(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 6
    if-nez p1, :cond_0

    .line 7
    iget-object v0, p0, Lbxb$a;->a:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 8
    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    if-eq v0, v1, :cond_1

    move v0, v1

    .line 9
    :goto_0
    iget-boolean v3, p0, Lbxb$a;->b:Z

    if-eq v0, v3, :cond_0

    .line 10
    iput-boolean v0, p0, Lbxb$a;->b:Z

    .line 11
    iget-object v0, p0, Lbxb$a;->c:Lbxb;

    iget-boolean v3, p0, Lbxb$a;->b:Z

    .line 12
    const-string v4, "ProximitySensor.onDisplayStateChanged"

    const-string v5, "isDisplayOn: %b"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v4, v5, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    iget-object v0, v0, Lbxb;->c:Lbui;

    invoke-virtual {v0, v3}, Lbui;->a(Z)V

    .line 14
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 8
    goto :goto_0
.end method

.method public final onDisplayRemoved(I)V
    .locals 0

    .prologue
    .line 5
    return-void
.end method
