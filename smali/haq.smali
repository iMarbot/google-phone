.class public abstract Lhaq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile e:Z


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Lhav;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lhaq;->e:Z

    .line 61
    const/4 v0, 0x1

    sput-boolean v0, Lhaq;->e:Z

    .line 62
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/16 v0, 0x64

    iput v0, p0, Lhaq;->b:I

    .line 36
    const v0, 0x7fffffff

    iput v0, p0, Lhaq;->c:I

    .line 37
    return-void
.end method

.method public static a(ILjava/io/InputStream;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 40
    and-int/lit16 v0, p0, 0x80

    if-nez v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return p0

    .line 42
    :cond_1
    and-int/lit8 p0, p0, 0x7f

    .line 43
    const/4 v0, 0x7

    .line 44
    :goto_1
    const/16 v1, 0x20

    if-ge v0, v1, :cond_4

    .line 45
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 46
    if-ne v1, v3, :cond_2

    .line 47
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 48
    :cond_2
    and-int/lit8 v2, v1, 0x7f

    shl-int/2addr v2, v0

    or-int/2addr p0, v2

    .line 49
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    .line 51
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 58
    :cond_3
    add-int/lit8 v0, v0, 0x7

    .line 52
    :cond_4
    const/16 v1, 0x40

    if-ge v0, v1, :cond_6

    .line 53
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 54
    if-ne v1, v3, :cond_5

    .line 55
    invoke-static {}, Lhcf;->a()Lhcf;

    move-result-object v0

    throw v0

    .line 56
    :cond_5
    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_3

    goto :goto_0

    .line 59
    :cond_6
    invoke-static {}, Lhcf;->c()Lhcf;

    move-result-object v0

    throw v0
.end method

.method public static a(J)J
    .locals 4

    .prologue
    .line 39
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;)Lhaq;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    .line 2
    if-nez p0, :cond_0

    .line 3
    sget-object v0, Lhbu;->b:[B

    .line 4
    array-length v1, v0

    .line 5
    invoke-static {v0, v2, v1, v2}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 9
    :goto_0
    return-object v0

    .line 7
    :cond_0
    new-instance v0, Lhas;

    const/16 v1, 0x1000

    .line 8
    invoke-direct {v0, p0, v1}, Lhas;-><init>(Ljava/io/InputStream;I)V

    goto :goto_0
.end method

.method static a(Ljava/nio/ByteBuffer;Z)Lhaq;
    .locals 4

    .prologue
    .line 21
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    .line 24
    invoke-static {v0, v1, v2, p1}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    .line 25
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    sget-boolean v0, Lhev;->b:Z

    .line 27
    if-eqz v0, :cond_1

    .line 28
    new-instance v0, Lhau;

    .line 29
    invoke-direct {v0, p0, p1}, Lhau;-><init>(Ljava/nio/ByteBuffer;Z)V

    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    new-array v0, v0, [B

    .line 32
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 33
    const/4 v1, 0x0

    array-length v2, v0

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([B)Lhaq;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    array-length v0, p0

    .line 11
    invoke-static {p0, v1, v0, v1}, Lhaq;->a([BIIZ)Lhaq;

    move-result-object v0

    .line 12
    return-object v0
.end method

.method public static a([BIIZ)Lhaq;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lhar;

    .line 14
    invoke-direct {v0, p0, p1, p2, p3}, Lhar;-><init>([BIIZ)V

    .line 16
    :try_start_0
    invoke-virtual {v0, p2}, Lhar;->c(I)I
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static e(I)I
    .locals 2

    .prologue
    .line 38
    ushr-int/lit8 v0, p0, 0x1

    and-int/lit8 v1, p0, 0x1

    neg-int v1, v1

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Lhbr;Lhbg;)Lhbr;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(ILhde;Lhbg;)V
.end method

.method public abstract a(Lhde;Lhbg;)V
.end method

.method public abstract b()D
.end method

.method public abstract b(I)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(I)I
.end method

.method public abstract d()J
.end method

.method public abstract d(I)V
.end method

.method public abstract e()J
.end method

.method public abstract f()I
.end method

.method public abstract g()J
.end method

.method public abstract h()I
.end method

.method public abstract i()Z
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract l()Lhah;
.end method

.method public abstract m()I
.end method

.method public abstract n()I
.end method

.method public abstract o()I
.end method

.method public abstract p()J
.end method

.method public abstract q()I
.end method

.method public abstract r()J
.end method

.method public abstract s()I
.end method

.method abstract t()J
.end method

.method public abstract u()I
.end method

.method public abstract v()Z
.end method

.method public abstract w()I
.end method
