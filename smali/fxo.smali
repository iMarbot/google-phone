.class final Lfxo;
.super Lfwq;
.source "PG"

# interfaces
.implements Lgaj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfxo$b;,
        Lfxo$a;
    }
.end annotation


# static fields
.field private static volatile j:Lfxo;


# instance fields
.field public final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile e:Lfyt;

.field public final f:Lfxn;

.field public final g:Lgde;

.field public final h:Z

.field public final i:Z

.field private k:Z

.field private l:I

.field private m:Lfxe;

.field private volatile n:Lfxo$a;


# direct methods
.method private constructor <init>(Lgdc;Lfxn;Lgde;ZLgax;Landroid/app/Application;FZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x42c80000    # 100.0f

    .line 19
    sget v0, Lmg$c;->C:I

    invoke-direct {p0, p1, p6, p5, v0}, Lfwq;-><init>(Lgdc;Landroid/app/Application;Lgax;I)V

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfxo;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    invoke-static {p3}, Lhcw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const/4 v0, 0x0

    cmpl-float v0, p7, v0

    if-lez v0, :cond_2

    cmpg-float v0, p7, v5

    if-gtz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "StartupSamplePercentage should be a floating number > 0 and <= 100."

    invoke-static {v0, v3}, Lhcw;->a(ZLjava/lang/Object;)V

    .line 23
    invoke-static {p6}, Lfxe;->a(Landroid/app/Application;)Lfxe;

    move-result-object v0

    iput-object v0, p0, Lfxo;->m:Lfxe;

    .line 24
    new-instance v0, Lgcu;

    div-float v3, p7, v5

    invoke-direct {v0, v3}, Lgcu;-><init>(F)V

    .line 26
    iget v3, v0, Lgcu;->a:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    iget-object v3, v0, Lgcu;->b:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    iget v0, v0, Lgcu;->a:F

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_1

    :cond_0
    move v2, v1

    .line 27
    :cond_1
    iput-boolean v2, p0, Lfxo;->k:Z

    .line 28
    div-float v0, v5, p7

    float-to-int v0, v0

    iput v0, p0, Lfxo;->l:I

    .line 29
    iput-object p2, p0, Lfxo;->f:Lfxn;

    .line 30
    iput-object p3, p0, Lfxo;->g:Lgde;

    .line 31
    iput-boolean p4, p0, Lfxo;->h:Z

    .line 32
    iput-boolean p8, p0, Lfxo;->i:Z

    .line 33
    return-void

    :cond_2
    move v0, v2

    .line 22
    goto :goto_0
.end method

.method static a(Lgdc;Landroid/app/Application;Lgax;Lfzp;Z)Lfxo;
    .locals 10

    .prologue
    .line 1
    sget-object v0, Lfxo;->j:Lfxo;

    if-nez v0, :cond_1

    .line 2
    const-class v9, Lfxo;

    monitor-enter v9

    .line 3
    :try_start_0
    sget-object v0, Lfxo;->j:Lfxo;

    if-nez v0, :cond_0

    .line 4
    new-instance v0, Lfxo;

    .line 6
    iget-object v2, p3, Lfzp;->f:Lfxn;

    .line 9
    iget-object v3, p3, Lfzp;->e:Lgde;

    .line 12
    iget-boolean v4, p3, Lfzp;->d:Z

    .line 15
    iget v7, p3, Lfzp;->c:F

    move-object v1, p0

    move-object v5, p2

    move-object v6, p1

    move v8, p4

    .line 16
    invoke-direct/range {v0 .. v8}, Lfxo;-><init>(Lgdc;Lfxn;Lgde;ZLgax;Landroid/app/Application;FZ)V

    sput-object v0, Lfxo;->j:Lfxo;

    .line 17
    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    :cond_1
    sget-object v0, Lfxo;->j:Lfxo;

    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Ljava/io/File;Lhrf;)Z
    .locals 6

    .prologue
    .line 82
    const/4 v2, 0x0

    .line 83
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 84
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_2

    const-wide/32 v4, 0x7fffffff

    cmp-long v3, v0, v4

    if-gez v3, :cond_2

    .line 85
    long-to-int v3, v0

    .line 86
    new-array v4, v3, [B

    .line 87
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 89
    sub-int v2, v3, v0

    :try_start_1
    invoke-virtual {v1, v4, v0, v2}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 90
    :cond_0
    invoke-static {p1, v4}, Lhfz;->mergeFrom(Lhfz;[B)Lhfz;

    .line 93
    :goto_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 94
    if-eqz v1, :cond_1

    .line 95
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 96
    :cond_1
    return v0

    .line 92
    :cond_2
    const/4 v0, 0x1

    :try_start_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lhrf;->a:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v2

    goto :goto_1

    .line 97
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_3

    .line 98
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_3
    throw v0

    .line 97
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method private final f()Lhrf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 64
    invoke-static {}, Lhcw;->c()V

    .line 65
    new-instance v1, Ljava/io/File;

    .line 66
    iget-object v0, p0, Lfwq;->a:Landroid/app/Application;

    .line 67
    invoke-virtual {v0}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "primes_crash"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    const-string v0, "CrashMetricService"

    const-string v2, "found persisted crash"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    new-instance v0, Lhrf;

    invoke-direct {v0}, Lhrf;-><init>()V

    .line 71
    invoke-static {v1, v0}, Lfxo;->a(Ljava/io/File;Lhrf;)Z

    move-result v1

    .line 72
    if-eqz v1, :cond_0

    .line 81
    :goto_0
    return-object v0

    .line 74
    :cond_0
    const-string v0, "CrashMetricService"

    const-string v1, "could not delete crash file"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 81
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "CrashMetricService"

    const-string v2, "IO failure"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v0, v3}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_1

    .line 79
    :catch_1
    move-exception v0

    .line 80
    const-string v1, "CrashMetricService"

    const-string v2, "Unexpected SecurityException"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v0, v3}, Lfmk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/Throwable;)Lhrf;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35
    new-instance v2, Lhrf;

    invoke-direct {v2}, Lhrf;-><init>()V

    .line 36
    iget-object v3, p0, Lfxo;->e:Lfyt;

    invoke-static {v3}, Lfyt;->a(Lfyt;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lhrf;->c:Ljava/lang/String;

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lhrf;->a:Ljava/lang/Boolean;

    .line 38
    iput-object p1, v2, Lhrf;->d:Ljava/lang/String;

    .line 39
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 40
    const-class v4, Ljava/lang/OutOfMemoryError;

    if-ne v3, v4, :cond_1

    .line 41
    const/4 v0, 0x2

    .line 49
    :cond_0
    :goto_0
    iput v0, v2, Lhrf;->e:I

    .line 50
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lhrf;->g:Ljava/lang/String;

    .line 51
    :try_start_0
    invoke-static {p2}, Lfmk;->b(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfxv;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lhrf;->f:Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_1
    :try_start_1
    new-instance v0, Lhst;

    invoke-direct {v0}, Lhst;-><init>()V

    iput-object v0, v2, Lhrf;->b:Lhst;

    .line 56
    iget-object v0, v2, Lhrf;->b:Lhst;

    .line 58
    iget-object v3, p0, Lfwq;->a:Landroid/app/Application;

    .line 59
    invoke-static {v3}, Lfmk;->h(Landroid/content/Context;)Lhqq;

    move-result-object v3

    iput-object v3, v0, Lhst;->a:Lhqq;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 63
    :goto_2
    return-object v2

    .line 42
    :cond_1
    const-class v4, Ljava/lang/NullPointerException;

    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 44
    const-class v0, Ljava/lang/RuntimeException;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    const/4 v0, 0x3

    goto :goto_0

    .line 46
    :cond_2
    const-class v0, Ljava/lang/Error;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    move v0, v1

    .line 48
    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    const-string v3, "CrashMetricService"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed to generate hashed stack trace."

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3, v0, v4}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 61
    :catch_1
    move-exception v0

    .line 62
    const-string v3, "CrashMetricService"

    const-string v4, "Failed to get process stats."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v0, v1}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method final a(Ljava/lang/Thread$UncaughtExceptionHandler;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lfxo$b;

    invoke-direct {v0, p0, p1}, Lfxo$b;-><init>(Lfxo;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-object v0
.end method

.method final a(ILhrf;)V
    .locals 3

    .prologue
    .line 127
    new-instance v0, Lhtd;

    invoke-direct {v0}, Lhtd;-><init>()V

    .line 128
    new-instance v1, Lhso;

    invoke-direct {v1}, Lhso;-><init>()V

    iput-object v1, v0, Lhtd;->h:Lhso;

    .line 129
    iget-object v1, v0, Lhtd;->h:Lhso;

    iget v2, p0, Lfxo;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lhso;->b:Ljava/lang/Integer;

    .line 130
    iget-object v1, v0, Lhtd;->h:Lhso;

    iput p1, v1, Lhso;->a:I

    .line 131
    if-eqz p2, :cond_0

    .line 132
    iget-object v1, v0, Lhtd;->h:Lhso;

    new-instance v2, Lhsp;

    invoke-direct {v2}, Lhsp;-><init>()V

    iput-object v2, v1, Lhso;->c:Lhsp;

    .line 133
    iget-object v1, v0, Lhtd;->h:Lhso;

    iget-object v1, v1, Lhso;->c:Lhsp;

    iput-object p2, v1, Lhsp;->a:Lhrf;

    .line 134
    :cond_0
    invoke-virtual {p0, v0}, Lfxo;->a(Lhtd;)V

    .line 135
    return-void
.end method

.method final a(Lfyt;)V
    .locals 4

    .prologue
    .line 111
    const-string v1, "CrashMetricService"

    const-string v2, "activeComponentName: "

    invoke-static {p1}, Lfyt;->a(Lfyt;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iput-object p1, p0, Lfxo;->e:Lfyt;

    .line 113
    return-void

    .line 111
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lfxo;->n:Lfxo$a;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lfxo;->m:Lfxe;

    iget-object v1, p0, Lfxo;->n:Lfxo$a;

    invoke-virtual {v0, v1}, Lfxe;->b(Lfwt;)V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lfxo;->n:Lfxo$a;

    .line 139
    :cond_0
    iget-object v0, p0, Lfxo;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    instance-of v0, v0, Lfxo$b;

    if-eqz v0, :cond_1

    .line 141
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    check-cast v0, Lfxo$b;

    .line 143
    iget-object v0, v0, Lfxo$b;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 144
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 145
    :cond_1
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 99
    const-string v0, "CrashMetricService"

    const-string v1, "onPrimesInitialize"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    const/4 v0, 0x0

    .line 101
    iget-boolean v1, p0, Lfxo;->i:Z

    if-eqz v1, :cond_0

    .line 102
    const-string v1, "CrashMetricService"

    const-string v2, "persistent crash enabled."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :try_start_0
    invoke-direct {p0}, Lfxo;->f()Lhrf;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 107
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lfxo;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    iget-boolean v1, p0, Lfxo;->k:Z

    if-eqz v1, :cond_2

    .line 108
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lfxo;->a(ILhrf;)V

    .line 110
    :goto_1
    return-void

    .line 105
    :catch_0
    move-exception v1

    .line 106
    const-string v2, "CrashMetricService"

    const-string v3, "Unexpected failure: "

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v1, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :cond_2
    const-string v0, "CrashMetricService"

    const-string v1, "Startup metric for \'PRIMES_CRASH_MONITORING_INITIALIZED\' dropped."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114
    const-string v0, "CrashMetricService"

    const-string v1, "onFirstActivityCreated"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    invoke-virtual {p0}, Lfxo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfxo;->k:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lfxo;->b()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lfxp;

    invoke-direct {v1, p0}, Lfxp;-><init>(Lfxo;)V

    .line 119
    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 123
    :goto_0
    new-instance v0, Lfxo$a;

    invoke-direct {v0, p0}, Lfxo$a;-><init>(Lfxo;)V

    .line 124
    iput-object v0, p0, Lfxo;->n:Lfxo$a;

    .line 125
    iget-object v0, p0, Lfxo;->m:Lfxe;

    iget-object v1, p0, Lfxo;->n:Lfxo$a;

    invoke-virtual {v0, v1}, Lfxe;->a(Lfwt;)V

    .line 126
    return-void

    .line 121
    :cond_0
    const-string v0, "CrashMetricService"

    const-string v1, "Startup metric for \'PRIMES_FIRST_ACTIVITY_LAUNCHED\' dropped."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lfmk;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
