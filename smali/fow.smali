.class final Lfow;
.super Lfof;
.source "PG"


# instance fields
.field public final synthetic this$0:Lfos;


# direct methods
.method private constructor <init>(Lfos;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lfow;->this$0:Lfos;

    invoke-direct {p0}, Lfof;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfos;Lfmt;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lfow;-><init>(Lfos;)V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onStreamRequest$0$Encoder$LocalCallStateListener(Lgpk;)V
    .locals 4

    .prologue
    .line 6
    const-string v0, "Got stream request %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lfvh;->logd(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 7
    iget-object v0, p1, Lgpk;->resolution:Lgpl;

    .line 8
    iget-object v1, v0, Lgpl;->width:Ljava/lang/Integer;

    .line 9
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, Lgpl;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/2addr v0, v1

    .line 10
    invoke-static {v0}, Lfon;->getVideoSpecificationForPixelCount(I)Lfwp;

    move-result-object v0

    .line 11
    iget-object v1, p0, Lfow;->this$0:Lfos;

    invoke-virtual {v0}, Lfwp;->a()I

    move-result v0

    invoke-static {v1, v0}, Lfos;->access$202(Lfos;I)I

    .line 12
    iget-object v0, p0, Lfow;->this$0:Lfos;

    invoke-static {v0}, Lfos;->access$300(Lfos;)Lfwo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 13
    iget-object v0, p0, Lfow;->this$0:Lfos;

    iget-object v1, p0, Lfow;->this$0:Lfos;

    invoke-static {v1}, Lfos;->access$300(Lfos;)Lfwo;

    move-result-object v1

    iget v1, v1, Lfwo;->a:I

    iget-object v2, p0, Lfow;->this$0:Lfos;

    invoke-static {v2}, Lfos;->access$300(Lfos;)Lfwo;

    move-result-object v2

    iget v2, v2, Lfwo;->b:I

    iget-object v3, p0, Lfow;->this$0:Lfos;

    invoke-static {v3}, Lfos;->access$400(Lfos;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lfos;->setResolution(IIZ)V

    .line 14
    :cond_0
    return-void
.end method

.method public final onStreamRequest(Lgpk;)V
    .locals 2

    .prologue
    .line 2
    if-eqz p1, :cond_0

    iget-object v0, p1, Lgpk;->resolution:Lgpl;

    if-nez v0, :cond_1

    .line 5
    :cond_0
    :goto_0
    return-void

    .line 4
    :cond_1
    iget-object v0, p0, Lfow;->this$0:Lfos;

    invoke-static {v0}, Lfos;->access$100(Lfos;)Lfpc;

    move-result-object v0

    new-instance v1, Lfox;

    invoke-direct {v1, p0, p1}, Lfox;-><init>(Lfow;Lgpk;)V

    invoke-virtual {v0, v1}, Lfpc;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
