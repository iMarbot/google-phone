.class public Lbmm;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lbmi;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    const-class v0, Lbmm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbmm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbmm;->b:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lbmm;->d:Ljava/lang/String;

    .line 4
    iget-object v0, p0, Lbmm;->b:Landroid/content/Context;

    invoke-static {v0}, Lbib;->z(Landroid/content/Context;)Lbmn;

    move-result-object v0

    invoke-interface {v0}, Lbmn;->a()Lbmi;

    move-result-object v0

    iput-object v0, p0, Lbmm;->c:Lbmi;

    .line 5
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 49
    const-wide/16 v0, -0x1

    invoke-static {p0, v0, v1}, Lbmm;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;J)Landroid/net/Uri;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 50
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->ENTERPRISE_CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 51
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-ge v1, v2, :cond_0

    .line 52
    cmp-long v1, p1, v4

    if-eqz v1, :cond_2

    .line 53
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sip"

    .line 58
    invoke-static {p0}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 60
    cmp-long v1, p1, v4

    if-eqz v1, :cond_1

    .line 61
    const-string v1, "directory"

    .line 62
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 64
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 54
    :cond_2
    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;)Lbml;
    .locals 4

    .prologue
    const/16 v3, 0x18

    .line 65
    new-instance v2, Lbml;

    invoke-direct {v2}, Lbml;-><init>()V

    .line 66
    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbib;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, Lbml;->b:Landroid/net/Uri;

    .line 67
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbml;->d:Ljava/lang/String;

    .line 68
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lbml;->f:I

    .line 69
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbml;->g:Ljava/lang/String;

    .line 70
    const/16 v0, 0xc

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_1

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 73
    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_1
    iput-object v0, v2, Lbml;->h:Ljava/lang/String;

    .line 74
    const/16 v0, 0xd

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbml;->k:Ljava/lang/String;

    .line 75
    const/16 v0, 0xe

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v2, Lbml;->l:J

    .line 76
    const/16 v0, 0x17

    .line 77
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbib;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 78
    invoke-static {v0}, Lbib;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, Lbml;->m:Landroid/net/Uri;

    .line 79
    const/16 v0, 0xf

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lbml;->i:Ljava/lang/String;

    .line 80
    return-object v2

    .line 71
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 73
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)Lbml;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 123
    if-nez p1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-object v3

    .line 125
    :cond_1
    iget-object v0, p0, Lbmm;->b:Landroid/content/Context;

    invoke-static {v0}, Lbsw;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 126
    sget-object v3, Lbml;->a:Lbml;

    goto :goto_0

    .line 127
    :cond_2
    iget-object v0, p0, Lbmm;->b:Landroid/content/Context;

    .line 128
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 129
    invoke-static {p1}, Lbmq;->a(Landroid/net/Uri;)[Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 131
    if-nez v2, :cond_3

    .line 132
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 134
    :cond_3
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_4

    .line 135
    sget-object v3, Lbml;->a:Lbml;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 136
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 138
    :cond_4
    const/4 v0, 0x4

    .line 139
    :try_start_1
    invoke-static {v2, v0, p1}, Lbmw;->a(Landroid/database/Cursor;ILandroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v1

    .line 140
    if-nez v1, :cond_5

    .line 141
    sget-object v3, Lbml;->a:Lbml;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 142
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 144
    :cond_5
    const/4 v0, 0x7

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 146
    new-instance v0, Lbml;

    invoke-direct {v0}, Lbml;-><init>()V

    .line 147
    iput-object v4, v0, Lbml;->c:Ljava/lang/String;

    .line 148
    const/4 v5, 0x0

    .line 149
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7, v4}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, v0, Lbml;->b:Landroid/net/Uri;

    .line 150
    const/4 v4, 0x1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lbml;->d:Ljava/lang/String;

    .line 151
    const/4 v4, 0x2

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v0, Lbml;->f:I

    .line 152
    const/4 v4, 0x3

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lbml;->g:Ljava/lang/String;

    .line 153
    const/4 v4, 0x4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lbml;->h:Ljava/lang/String;

    .line 154
    const/4 v4, 0x5

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lbml;->k:Ljava/lang/String;

    .line 155
    const/4 v4, 0x6

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v0, Lbml;->l:J

    .line 156
    const/16 v4, 0x8

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbib;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, v0, Lbml;->m:Landroid/net/Uri;

    .line 157
    const/4 v4, 0x0

    iput-object v4, v0, Lbml;->i:Ljava/lang/String;

    .line 158
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 159
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/support/v7/widget/ActionMenuView$b;->a(Ljava/lang/Long;Ljava/lang/Long;)J

    move-result-wide v4

    iput-wide v4, v0, Lbml;->p:J

    .line 160
    const/4 v1, 0x1

    iput-boolean v1, v0, Lbml;->r:Z

    .line 163
    iget-object v1, p0, Lbmm;->b:Landroid/content/Context;

    .line 164
    iget-object v4, v0, Lbml;->h:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 165
    sget-object v4, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v5, v0, Lbml;->h:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 167
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lbmq;->b:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 168
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 170
    if-eqz v4, :cond_6

    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v1

    if-nez v1, :cond_9

    .line 171
    :cond_6
    if-eqz v4, :cond_7

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 181
    :cond_7
    :goto_1
    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    move-object v3, v0

    .line 182
    goto/16 :goto_0

    .line 173
    :cond_9
    const/4 v1, 0x0

    .line 174
    :try_start_5
    invoke-interface {v4, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->e:Ljava/lang/String;

    .line 175
    const/4 v1, 0x1

    .line 176
    invoke-interface {v4, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lbml;->s:I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 177
    if-eqz v4, :cond_7

    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 183
    :catch_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 184
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_a

    if-eqz v3, :cond_d

    :try_start_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_a
    :goto_3
    throw v0

    .line 178
    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 179
    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_4
    if-eqz v4, :cond_b

    if-eqz v1, :cond_c

    :try_start_a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :cond_b
    :goto_5
    :try_start_b
    throw v0

    .line 184
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 179
    :catch_2
    move-exception v4

    invoke-static {v1, v4}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_c
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_5

    .line 184
    :catch_3
    move-exception v1

    invoke-static {v3, v1}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_d
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 179
    :catchall_3
    move-exception v0

    move-object v1, v3

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/Long;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 23
    if-eqz p1, :cond_0

    const-wide/16 v0, 0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v6

    .line 48
    :cond_1
    :goto_0
    return-object v0

    .line 25
    :cond_2
    if-eqz p4, :cond_4

    .line 26
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->c(J)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v6

    .line 27
    goto :goto_0

    .line 28
    :cond_3
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v6

    .line 29
    goto :goto_0

    .line 30
    :cond_4
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 33
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lbmq;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 34
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 35
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 36
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 37
    if-eqz v1, :cond_1

    .line 38
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 40
    :cond_5
    if-eqz v1, :cond_6

    .line 41
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    :goto_1
    move-object v0, v6

    .line 48
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 43
    :goto_2
    :try_start_2
    sget-object v2, Lbmm;->a:Ljava/lang/String;

    const-string v3, "IllegalArgumentException in lookUpDisplayNameAlternative"

    invoke-static {v2, v3, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 44
    if-eqz v1, :cond_6

    .line 45
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 46
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_7

    .line 47
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 46
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 42
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    const-string p1, ""

    .line 207
    :cond_0
    :goto_0
    return-object p1

    .line 203
    :cond_1
    invoke-static {p1}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    iget-object p3, p0, Lbmm;->d:Ljava/lang/String;

    .line 207
    :cond_2
    const/4 v0, 0x0

    invoke-static {p1, v0, p3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 105
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 106
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 107
    sget-object v1, Landroid/provider/ContactsContract$Directory;->ENTERPRISE_CONTENT_URI:Landroid/net/Uri;

    .line 109
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 110
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 111
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 112
    if-nez v1, :cond_1

    move-object v0, v6

    .line 122
    :goto_1
    return-object v0

    .line 108
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$Directory;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 114
    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 116
    invoke-static {v2, v3}, Landroid/support/v7/widget/ActionMenuView$b;->b(J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 117
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 121
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 119
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    .line 122
    goto :goto_1
.end method

.method public static a(Lbml;)Z
    .locals 1

    .prologue
    .line 104
    if-eqz p0, :cond_0

    iget-object v0, p0, Lbml;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 6
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "vnd.android.cursor.item/phone_v2"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "data1"

    .line 7
    invoke-virtual {v2, v3, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "data2"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    .line 8
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 9
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "display_name"

    .line 10
    invoke-virtual {v1, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "display_name_source"

    const/16 v3, 0x14

    .line 11
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "vnd.android.cursor.item/contact"

    .line 12
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 14
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    .line 15
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "encoded"

    .line 16
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "directory"

    const-string v3, "9223372036854775807"

    .line 17
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 18
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 22
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final b(Ljava/lang/String;Ljava/lang/String;J)Lbml;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 185
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 200
    :cond_0
    :goto_0
    return-object v0

    .line 187
    :cond_1
    invoke-static {p1, p3, p4}, Lbmm;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lbmm;->a(Landroid/net/Uri;)Lbml;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_3

    sget-object v2, Lbml;->a:Lbml;

    if-eq v0, v2, :cond_3

    .line 189
    invoke-direct {p0, p1, v1, p2}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->i:Ljava/lang/String;

    .line 190
    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-nez v1, :cond_2

    .line 191
    sget-object v1, Lbko$a;->b:Lbko$a;

    iput-object v1, v0, Lbml;->q:Lbko$a;

    goto :goto_0

    .line 192
    :cond_2
    sget-object v1, Lbko$a;->c:Lbko$a;

    iput-object v1, v0, Lbml;->q:Lbko$a;

    goto :goto_0

    .line 193
    :cond_3
    iget-object v1, p0, Lbmm;->c:Lbmi;

    if-eqz v1, :cond_0

    .line 194
    iget-object v1, p0, Lbmm;->c:Lbmi;

    iget-object v2, p0, Lbmm;->b:Landroid/content/Context;

    .line 195
    invoke-interface {v1, v2, p1}, Lbmi;->a(Landroid/content/Context;Ljava/lang/String;)Lbmj;

    move-result-object v1

    .line 196
    if-eqz v1, :cond_0

    .line 197
    invoke-interface {v1}, Lbmj;->a()Lbml;

    move-result-object v2

    iget-boolean v2, v2, Lbml;->n:Z

    if-nez v2, :cond_4

    .line 198
    invoke-interface {v1}, Lbmj;->a()Lbml;

    move-result-object v0

    goto :goto_0

    .line 199
    :cond_4
    const-string v1, "ContactInfoHelper.queryContactInfoForPhoneNumber"

    const-string v2, "info is bad data"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lbml;
    .locals 2

    .prologue
    .line 81
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;J)Lbml;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Lbml;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    :goto_0
    return-object v1

    .line 84
    :cond_0
    invoke-static {p1}, Lbmw;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    invoke-static {p1, p3, p4}, Lbmm;->a(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lbmm;->a(Landroid/net/Uri;)Lbml;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    sget-object v2, Lbml;->a:Lbml;

    if-ne v0, v2, :cond_2

    .line 87
    :cond_1
    invoke-static {p1}, Lbmw;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-static {v2}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 89
    invoke-direct {p0, v2, p2, p3, p4}, Lbmm;->b(Ljava/lang/String;Ljava/lang/String;J)Lbml;

    move-result-object v0

    .line 92
    :cond_2
    :goto_1
    if-nez v0, :cond_5

    move-object v0, v1

    :cond_3
    :goto_2
    move-object v1, v0

    .line 97
    goto :goto_0

    .line 91
    :cond_4
    invoke-direct {p0, p1, p2, p3, p4}, Lbmm;->b(Ljava/lang/String;Ljava/lang/String;J)Lbml;

    move-result-object v0

    goto :goto_1

    .line 94
    :cond_5
    sget-object v1, Lbml;->a:Lbml;

    if-ne v0, v1, :cond_3

    .line 95
    invoke-virtual {p0, p1, p2}, Lbmm;->b(Ljava/lang/String;Ljava/lang/String;)Lbml;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lbko$a;)Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lbmm;->c:Lbmi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbmm;->c:Lbmi;

    invoke-interface {v0, p1}, Lbmi;->a(Lbko$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lbml;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Lbml;

    invoke-direct {v0}, Lbml;-><init>()V

    .line 99
    iput-object p1, v0, Lbml;->h:Ljava/lang/String;

    .line 100
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2}, Lbmm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->i:Ljava/lang/String;

    .line 101
    invoke-static {p1, p2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbml;->k:Ljava/lang/String;

    .line 102
    iget-object v1, v0, Lbml;->i:Ljava/lang/String;

    invoke-static {v1}, Lbmm;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lbml;->b:Landroid/net/Uri;

    .line 103
    return-object v0
.end method
