.class public final Lhv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lia;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 6
    new-instance v0, Lhy;

    invoke-direct {v0}, Lhy;-><init>()V

    sput-object v0, Lhv;->a:Lia;

    .line 12
    :goto_0
    return-void

    .line 7
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 8
    new-instance v0, Lhx;

    invoke-direct {v0}, Lhx;-><init>()V

    sput-object v0, Lhv;->a:Lia;

    goto :goto_0

    .line 9
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_2

    .line 10
    new-instance v0, Lhw;

    invoke-direct {v0}, Lhw;-><init>()V

    sput-object v0, Lhv;->a:Lia;

    goto :goto_0

    .line 11
    :cond_2
    new-instance v0, Lia;

    invoke-direct {v0}, Lia;-><init>()V

    sput-object v0, Lhv;->a:Lia;

    goto :goto_0
.end method

.method public static a(Landroid/app/Fragment;Z)V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhv;->a:Lia;

    invoke-virtual {v0, p0, p1}, Lia;->a(Landroid/app/Fragment;Z)V

    .line 2
    return-void
.end method

.method public static a(Landroid/app/Fragment;[Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 3
    sget-object v0, Lhv;->a:Lia;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lia;->a(Landroid/app/Fragment;[Ljava/lang/String;I)V

    .line 4
    return-void
.end method
