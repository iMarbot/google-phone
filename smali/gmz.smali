.class public Lgmz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lgue;

.field public final b:Lgue;

.field public final c:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lgue;Lgue;Z)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lgmz;->a:Lgue;

    .line 5
    iput-object p2, p0, Lgmz;->b:Lgue;

    .line 6
    iput-boolean p3, p0, Lgmz;->c:Z

    .line 7
    return-void
.end method

.method public synthetic constructor <init>(Lgue;Lgue;ZB)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lgmz;-><init>(Lgue;Lgue;Z)V

    return-void
.end method

.method public static a(Lgue;Lgef;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 31
    .line 32
    check-cast p0, Lgue;

    invoke-virtual {p0}, Lgue;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, Lgue;->get(I)Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    check-cast v0, Lgth;

    .line 33
    invoke-interface {v0, p1}, Lgth;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 34
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    or-int/2addr v1, v4

    .line 36
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Lgef;->a()I

    move-result v4

    and-int/2addr v0, v4

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    move v1, v0

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    return v1
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 17
    :try_start_0
    const-string v0, "android.os.StrictMode$ViolationInfo"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 18
    const-string v2, "crashInfo"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 19
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 21
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 22
    const-class v2, Landroid/os/StrictMode;

    const-string v3, "parseViolationFromMessage"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    .line 23
    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 24
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 25
    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 30
    :goto_0
    return v0

    .line 26
    :catch_0
    move-exception v0

    .line 27
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 28
    :catch_1
    move-exception v0

    .line 29
    const-string v2, "ThreadStrictMode"

    const-string v3, "Unable to get violation."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 30
    goto :goto_0
.end method

.method public static a()Lgee;
    .locals 1

    .prologue
    .line 2
    new-instance v0, Lgee;

    invoke-direct {v0}, Lgee;-><init>()V

    return-object v0
.end method

.method public static synthetic a(Lgmz;)Lgue;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgmz;->a:Lgue;

    return-object v0
.end method

.method public static synthetic b(Lgue;Lgef;)I
    .locals 1

    .prologue
    .line 42
    invoke-static {p0, p1}, Lgmz;->a(Lgue;Lgef;)I

    move-result v0

    return v0
.end method

.method public static synthetic b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 40
    invoke-static {p0}, Lgmz;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static synthetic b(Lgmz;)Lgue;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lgmz;->b:Lgue;

    return-object v0
.end method

.method public static c()Ljava/lang/ThreadLocal;
    .locals 2

    .prologue
    .line 14
    const-class v0, Landroid/os/StrictMode;

    const-string v1, "violationsBeingTimed"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 15
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 16
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method public static synthetic c(Lgmz;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lgmz;->c:Z

    return v0
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 8
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Whitelisting is only available on threads with Loopers."

    .line 9
    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 10
    invoke-static {}, Lgmz;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 12
    new-instance v0, Lged;

    invoke-direct {v0, p0}, Lged;-><init>(Lgmz;)V

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 13
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
