.class Lguc;
.super Lgud;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:I

.field public c:Z


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lgud;-><init>()V

    .line 2
    const-string v0, "initialCapacity"

    invoke-static {p1, v0}, Lhcw;->a(ILjava/lang/String;)I

    .line 3
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lguc;->b:I

    .line 5
    return-void
.end method

.method private final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6
    iget-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_1

    .line 7
    iget-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    iget-object v1, p0, Lguc;->a:[Ljava/lang/Object;

    array-length v1, v1

    .line 8
    invoke-static {v1, p1}, Lguc;->a(II)I

    move-result v1

    .line 9
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    .line 10
    iput-boolean v2, p0, Lguc;->c:Z

    .line 14
    :cond_0
    :goto_0
    return-void

    .line 11
    :cond_1
    iget-boolean v0, p0, Lguc;->c:Z

    if-eqz v0, :cond_0

    .line 12
    iget-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    .line 13
    iput-boolean v2, p0, Lguc;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lguc;
    .locals 3

    .prologue
    .line 15
    invoke-static {p1}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iget v0, p0, Lguc;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lguc;->a(I)V

    .line 17
    iget-object v0, p0, Lguc;->a:[Ljava/lang/Object;

    iget v1, p0, Lguc;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lguc;->b:I

    aput-object p1, v0, v1

    .line 18
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lgud;
    .locals 3

    .prologue
    .line 19
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 20
    check-cast v0, Ljava/util/Collection;

    .line 21
    iget v1, p0, Lguc;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {p0, v1}, Lguc;->a(I)V

    .line 22
    instance-of v1, v0, Lgub;

    if-eqz v1, :cond_0

    .line 23
    check-cast v0, Lgub;

    .line 24
    iget-object v1, p0, Lguc;->a:[Ljava/lang/Object;

    iget v2, p0, Lguc;->b:I

    invoke-virtual {v0, v1, v2}, Lgub;->a([Ljava/lang/Object;I)I

    move-result v0

    iput v0, p0, Lguc;->b:I

    .line 27
    :goto_0
    return-object p0

    .line 26
    :cond_0
    invoke-super {p0, p1}, Lgud;->a(Ljava/lang/Iterable;)Lgud;

    goto :goto_0
.end method

.method public synthetic b(Ljava/lang/Object;)Lgud;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lguc;->a(Ljava/lang/Object;)Lguc;

    move-result-object v0

    return-object v0
.end method
