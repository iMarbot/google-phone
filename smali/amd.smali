.class public Lamd;
.super Landroid/os/ResultReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    .line 2
    return-void
.end method


# virtual methods
.method public a(Landroid/telecom/PhoneAccountHandle;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    return-void
.end method

.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 4
    const-string v0, "extra_selected_account_handle"

    .line 5
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    const-string v1, "extra_set_default"

    .line 6
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "extra_call_id"

    .line 7
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 8
    invoke-virtual {p0, v0, v1, v2}, Lamd;->a(Landroid/telecom/PhoneAccountHandle;ZLjava/lang/String;)V

    .line 11
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 10
    const-string v0, "extra_call_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lamd;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
