.class public final Lazh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbec;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:F

.field private d:[B

.field private e:Landroid/content/Context;


# direct methods
.method constructor <init>(IIF[BLandroid/content/Context;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbdf;->a(Z)V

    .line 3
    invoke-static {p4}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    invoke-static {p5}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    iput p1, p0, Lazh;->a:I

    .line 6
    iput p2, p0, Lazh;->b:I

    .line 7
    iput p3, p0, Lazh;->c:F

    .line 8
    iput-object p4, p0, Lazh;->d:[B

    .line 9
    iput-object p5, p0, Lazh;->e:Landroid/content/Context;

    .line 10
    return-void

    .line 2
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Lazi;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 11
    iget-object v0, p0, Lazh;->e:Landroid/content/Context;

    invoke-static {v0}, Lbss;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    .line 12
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 15
    :try_start_0
    new-instance v6, Lbaa;

    invoke-direct {v6}, Lbaa;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 16
    :try_start_1
    iget-object v0, p0, Lazh;->d:[B

    .line 17
    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v6, v7}, Lbaa;->a(Ljava/io/InputStream;)V

    .line 18
    sget v7, Lbaa;->a:I

    .line 20
    invoke-virtual {v6}, Lbaa;->a()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    const/4 v0, -0x1

    .line 28
    :goto_0
    invoke-static {v0}, Lbai;->a(I)Z

    move-result v8

    if-nez v8, :cond_2

    move-object v0, v2

    .line 35
    :goto_1
    if-nez v0, :cond_4

    move-object v0, v2

    .line 39
    :goto_2
    if-eqz v0, :cond_0

    array-length v7, v0

    if-gtz v7, :cond_5

    :cond_0
    move-object v0, v2

    .line 43
    :goto_3
    if-eqz v0, :cond_6

    .line 44
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 47
    :goto_4
    :try_start_2
    invoke-static {v0}, Lbaa;->c(I)Lbab;

    move-result-object v7

    .line 48
    iget-object v0, p0, Lazh;->d:[B

    const/4 v8, 0x0

    iget-object v9, p0, Lazh;->d:[B

    array-length v9, v9

    invoke-static {v0, v8, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 49
    iget-boolean v0, v7, Lbab;->b:Z

    if-eqz v0, :cond_9

    .line 50
    iget v0, p0, Lazh;->a:I

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-ne v0, v9, :cond_7

    move v0, v3

    :goto_5
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 51
    iget v0, p0, Lazh;->b:I

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-ne v0, v9, :cond_8

    :goto_6
    invoke-static {v3}, Lbdf;->b(Z)V

    .line 52
    iget v0, p0, Lazh;->b:I

    int-to-float v0, v0

    iget v3, p0, Lazh;->c:F

    mul-float/2addr v0, v3

    float-to-int v3, v0

    .line 53
    iget v0, p0, Lazh;->a:I

    .line 58
    :goto_7
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v9, v0

    div-int/lit8 v9, v9, 0x2

    .line 59
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    sub-int/2addr v10, v3

    div-int/lit8 v10, v10, 0x2

    .line 60
    iput v3, p0, Lazh;->a:I

    .line 61
    iput v0, p0, Lazh;->b:I

    .line 63
    invoke-static {v8, v10, v9, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 64
    iget v3, v7, Lbab;->a:I

    invoke-static {v0, v3}, Lapw;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 66
    new-instance v3, Lazz;

    invoke-direct {v3}, Lazz;-><init>()V

    iput-object v3, v6, Lbaa;->j:Lazz;

    .line 67
    invoke-static {v0, v5}, Lbaa;->a(Landroid/graphics/Bitmap;Ljava/io/OutputStream;)V

    .line 68
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 69
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 70
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 73
    new-instance v0, Lazj;

    invoke-direct {v0, v1}, Lazj;-><init>(B)V

    .line 74
    iget-object v1, p0, Lazh;->e:Landroid/content/Context;

    .line 75
    invoke-static {}, Lcom/android/dialer/constants/Constants;->a()Lcom/android/dialer/constants/Constants;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/dialer/constants/Constants;->c()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {v1, v2, v4}, Landroid/support/v4/content/FileProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lazj;->a(Landroid/net/Uri;)Lazj;

    move-result-object v0

    iget v1, p0, Lazh;->a:I

    .line 78
    invoke-virtual {v0, v1}, Lazj;->a(I)Lazj;

    move-result-object v0

    iget v1, p0, Lazh;->b:I

    .line 79
    invoke-virtual {v0, v1}, Lazj;->b(I)Lazj;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lazj;->a()Lazi;

    move-result-object v0

    .line 81
    return-object v0

    .line 23
    :cond_1
    :try_start_3
    invoke-static {v7}, Lbaa;->b(I)I

    move-result v0

    goto/16 :goto_0

    .line 30
    :cond_2
    iget-object v8, v6, Lbaa;->j:Lazz;

    invoke-static {v7}, Lbaa;->a(I)S

    move-result v7

    .line 31
    iget-object v8, v8, Lazz;->a:[Lbaj;

    aget-object v0, v8, v0

    .line 32
    if-nez v0, :cond_3

    move-object v0, v2

    goto/16 :goto_1

    .line 33
    :cond_3
    iget-object v0, v0, Lbaj;->b:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbai;

    goto/16 :goto_1

    .line 37
    :cond_4
    invoke-virtual {v0}, Lbai;->b()[I

    move-result-object v0

    goto/16 :goto_2

    .line 41
    :cond_5
    const/4 v7, 0x0

    aget v0, v0, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    goto/16 :goto_3

    :catch_0
    move-exception v0

    :cond_6
    move v0, v1

    goto/16 :goto_4

    :cond_7
    move v0, v1

    .line 50
    goto/16 :goto_5

    :cond_8
    move v3, v1

    .line 51
    goto/16 :goto_6

    .line 54
    :cond_9
    :try_start_4
    iget v0, p0, Lazh;->a:I

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    if-ne v0, v9, :cond_a

    move v0, v3

    :goto_8
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 55
    iget v0, p0, Lazh;->b:I

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    if-ne v0, v9, :cond_b

    move v0, v3

    :goto_9
    invoke-static {v0}, Lbdf;->b(Z)V

    .line 56
    iget v3, p0, Lazh;->a:I

    .line 57
    iget v0, p0, Lazh;->b:I

    int-to-float v0, v0

    iget v9, p0, Lazh;->c:F
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    mul-float/2addr v0, v9

    float-to-int v0, v0

    goto/16 :goto_7

    :cond_a
    move v0, v1

    .line 54
    goto :goto_8

    :cond_b
    move v0, v1

    .line 55
    goto :goto_9

    .line 71
    :catch_1
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 72
    :catchall_0
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_a
    if-eqz v1, :cond_c

    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    :goto_b
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, Lgwf;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_b

    :cond_c
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    goto :goto_b

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_a
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lazh;->a()Lazi;

    move-result-object v0

    return-object v0
.end method
