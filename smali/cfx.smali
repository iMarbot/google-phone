.class public final Lcfx;
.super Lip;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lccg$a;
.implements Lcfw$a;
.implements Lcgj;
.implements Lcgm;


# instance fields
.field public W:Lcgb;

.field public final X:Landroid/os/Handler;

.field public final Y:Ljava/lang/Runnable;

.field private Z:Ljava/util/List;

.field public a:Lcom/android/dialer/widget/LockableViewPager;

.field private aa:Landroid/view/View;

.field private ab:Lcom/android/incallui/incall/impl/InCallPaginator;

.field private ac:Lces;

.field private ad:Lcgn;

.field private ae:Lcgk;

.field private af:Lcfw;

.field private ag:Lcfe;

.field private ah:Lcgr;

.field private ai:I

.field private aj:I

.field private ak:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lip;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcfx;->Z:Ljava/util/List;

    .line 3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcfx;->X:Landroid/os/Handler;

    .line 4
    new-instance v0, Lcga;

    invoke-direct {v0, p0}, Lcga;-><init>(Lcfx;)V

    iput-object v0, p0, Lcfx;->Y:Ljava/lang/Runnable;

    return-void
.end method

.method private V()Z
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lcfx;->W()Lip;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lip;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final W()Lip;
    .locals 2

    .prologue
    .line 291
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v0

    const v1, 0x7f0e01b3

    invoke-virtual {v0, v1}, Lja;->a(I)Lip;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 292
    const v0, 0x7f040060

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static f(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5
    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    const/16 v1, 0xc

    if-eq p0, v1, :cond_0

    const/16 v1, 0xe

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final T()V
    .locals 14

    .prologue
    const v13, 0x7f0f0014

    const/16 v8, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 187
    iget-object v0, p0, Lcfx;->af:Lcfw;

    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v9, p0, Lcfx;->af:Lcfw;

    iget-object v4, p0, Lcfx;->Z:Ljava/util/List;

    iget-object v1, p0, Lcfx;->ag:Lcfe;

    iget v5, p0, Lcfx;->ai:I

    iget v10, p0, Lcfx;->aj:I

    .line 191
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    .line 192
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 193
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 194
    invoke-interface {v0}, Lcff;->b()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 195
    invoke-interface {v0}, Lcff;->c()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v2, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-interface {v0}, Lcff;->a()Z

    move-result v12

    if-nez v12, :cond_2

    .line 197
    invoke-interface {v0}, Lcff;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 200
    const/4 v11, 0x0

    invoke-interface {v0, v11}, Lcff;->a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    goto :goto_2

    .line 202
    :cond_4
    if-nez v1, :cond_c

    .line 204
    invoke-static {v5, v7, v10}, Lbvs;->a(IZI)Lcfe;

    move-result-object v0

    .line 205
    :goto_3
    invoke-virtual {v9}, Lcfw;->i()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    .line 208
    invoke-static {v2}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    if-ltz v1, :cond_6

    move v4, v6

    :goto_4
    invoke-static {v4}, Lbdf;->a(Z)V

    .line 210
    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 211
    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    :goto_5
    move v3, v7

    .line 218
    :goto_6
    const/4 v0, 0x6

    if-ge v3, v0, :cond_9

    .line 219
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v3, v0, :cond_8

    .line 220
    iget-object v0, v9, Lcfw;->a:[Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    aget-object v0, v0, v3

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->setVisibility(I)V

    .line 224
    :goto_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    :cond_6
    move v4, v7

    .line 209
    goto :goto_4

    .line 212
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 213
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 214
    invoke-virtual {v0, v1, v2, v4, v5}, Lcfe;->a(ILjava/util/Set;Ljava/util/List;Ljava/util/List;)V

    .line 215
    invoke-virtual/range {v0 .. v5}, Lcfe;->a(ILjava/util/Set;Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    .line 216
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v2, v0

    goto :goto_5

    .line 222
    :cond_8
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 223
    iget-object v4, v9, Lcfw;->W:Lcfw$a;

    invoke-interface {v4, v0}, Lcfw$a;->b(I)Lcff;

    move-result-object v0

    iget-object v4, v9, Lcfw;->a:[Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    aget-object v4, v4, v3

    invoke-interface {v0, v4}, Lcff;->a(Lcom/android/incallui/incall/impl/CheckableLabeledButton;)V

    goto :goto_7

    .line 227
    :cond_9
    if-nez v1, :cond_a

    move v0, v8

    .line 228
    :goto_8
    iget-object v1, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    invoke-virtual {v1, v0}, Lcom/android/dialer/widget/LockableViewPager;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcfx;->W:Lcgb;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcfx;->W:Lcgb;

    .line 230
    invoke-virtual {v0}, Lcgb;->b()I

    move-result v0

    if-le v0, v6, :cond_b

    .line 231
    invoke-virtual {p0}, Lcfx;->i()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-le v0, v6, :cond_b

    .line 232
    iget-object v0, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    invoke-virtual {v0, v7}, Lcom/android/incallui/incall/impl/InCallPaginator;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    .line 234
    iput-boolean v7, v0, Lcom/android/dialer/widget/LockableViewPager;->f:Z

    goto/16 :goto_0

    :cond_a
    move v0, v7

    .line 227
    goto :goto_8

    .line 236
    :cond_b
    iget-object v0, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    invoke-virtual {v0, v8}, Lcom/android/incallui/incall/impl/InCallPaginator;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcfx;->W:Lcgb;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    .line 239
    iput-boolean v6, v0, Lcom/android/dialer/widget/LockableViewPager;->f:Z

    .line 240
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    iget-object v1, p0, Lcfx;->W:Lcgb;

    .line 241
    invoke-virtual {v1}, Lcgb;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 242
    invoke-virtual {v0, v1}, Lcom/android/dialer/widget/LockableViewPager;->b(I)V

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public final U()V
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0}, Lcgk;->d()Landroid/telecom/CallAudioState;

    move-result-object v0

    invoke-static {v0}, Lccg;->a(Landroid/telecom/CallAudioState;)Lccg;

    move-result-object v0

    .line 251
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lccg;->a(Lja;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 145
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0}, Lcff;->b()Z

    move-result v0

    return v0
.end method

.method public final Z()V
    .locals 3

    .prologue
    .line 148
    const-string v0, "InCallFragment.showNoteSentToast"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1101c1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 18
    const-string v0, "InCallFragment.onCreateView"

    const/4 v1, 0x0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    new-instance v0, Lcfy;

    invoke-direct {v0, p1, p2}, Lcfy;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 20
    invoke-static {v0}, Lbso;->a(Lbjz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 21
    new-instance v3, Lces;

    const v1, 0x7f0e0013

    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 23
    invoke-virtual {p0}, Lcfx;->i()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0163

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {v3, v0, v1, v4, v5}, Lces;-><init>(Landroid/view/View;Landroid/widget/ImageView;IZ)V

    iput-object v3, p0, Lcfx;->ac:Lces;

    .line 24
    iget-object v1, p0, Lcfx;->ac:Lces;

    invoke-virtual {p0}, Lcfx;->h()Lit;

    move-result-object v3

    invoke-static {v3}, Lapw;->a(Landroid/app/Activity;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lces;->c(Z)V

    .line 25
    const v1, 0x7f0e01b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/incall/impl/InCallPaginator;

    iput-object v1, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    .line 26
    const v1, 0x7f0e01b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/dialer/widget/LockableViewPager;

    iput-object v1, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    .line 27
    iget-object v1, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    new-instance v3, Lcfz;

    invoke-direct {v3, p0}, Lcfz;-><init>(Lcfx;)V

    invoke-virtual {v1, v3}, Lcom/android/dialer/widget/LockableViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 28
    const v1, 0x7f0e01b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcfx;->aa:Landroid/view/View;

    .line 29
    iget-object v1, p0, Lcfx;->aa:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v1

    const-string v3, "android.permission.READ_PHONE_STATE"

    invoke-static {v1, v3}, Llw;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p0

    :goto_0
    move v6, v2

    move-object v2, v1

    move v1, v6

    .line 35
    :goto_1
    iput v1, v2, Lcfx;->ai:I

    .line 36
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    iput v1, p0, Lcfx;->aj:I

    .line 37
    const v1, 0x7f0e01b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 38
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbib;->al(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 39
    return-object v0

    .line 33
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v1, v3, :cond_1

    .line 34
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType()I

    move-result v1

    move-object v2, p0

    goto :goto_1

    :cond_1
    move-object v1, p0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 267
    const-string v0, "InCallFragment.onButtonGridCreated"

    const-string v1, "InCallUiUnready"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0}, Lcgk;->c()V

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcfx;->af:Lcfw;

    .line 270
    return-void
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 158
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 159
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 160
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 161
    invoke-static {p1}, Lcfx;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0, p1}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p2}, Lcff;->b(Z)V

    .line 163
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 164
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->cO:Lbkq$a;

    .line 165
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 166
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 6
    invoke-super {p0, p1}, Lip;->a(Landroid/content/Context;)V

    .line 7
    iget-object v0, p0, Lcfx;->ah:Lcgr;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lcfx;->ah:Lcgr;

    invoke-virtual {p0, v0}, Lcfx;->a(Lcgr;)V

    .line 9
    :cond_0
    return-void
.end method

.method public final a(Landroid/telecom/CallAudioState;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 182
    const-string v0, "InCallFragment.setAudioState"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "audioState: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    invoke-virtual {p0, v4}, Lcfx;->b(I)Lcff;

    move-result-object v0

    check-cast v0, Lcfq;

    .line 184
    invoke-virtual {v0, p1}, Lcfq;->a(Landroid/telecom/CallAudioState;)V

    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-virtual {p1}, Landroid/telecom/CallAudioState;->isMuted()Z

    move-result v1

    invoke-interface {v0, v1}, Lcff;->c(Z)V

    .line 186
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    const-string v0, "InCallFragment.onViewCreated"

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-super {p0, p1, p2}, Lip;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 46
    const-class v0, Lcgo;

    .line 47
    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgo;

    invoke-interface {v0}, Lcgo;->g()Lcgn;

    move-result-object v0

    iput-object v0, p0, Lcfx;->ad:Lcgn;

    .line 48
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfm;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfm;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfq;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfq;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfi;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfi;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfj;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfj;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfg;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfg;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfr;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfr;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfl;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfl;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfs;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfs;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfu;

    iget-object v2, p0, Lcfx;->ae:Lcgk;

    invoke-direct {v1, v2}, Lcfu;-><init>(Lcgk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcfk;

    iget-object v2, p0, Lcfx;->ad:Lcgn;

    invoke-direct {v1, v2}, Lcfk;-><init>(Lcgn;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    new-instance v1, Lcft;

    iget-object v2, p0, Lcfx;->ad:Lcgn;

    invoke-direct {v1, v2}, Lcft;-><init>(Lcgn;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-interface {v0, p0}, Lcgn;->a(Lcgm;)V

    .line 61
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-interface {v0}, Lcgn;->k()V

    .line 62
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcfx;->ac:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 147
    return-void
.end method

.method public final a(Lcfw;)V
    .locals 3

    .prologue
    .line 262
    const-string v0, "InCallFragment.onButtonGridCreated"

    const-string v1, "InCallUiReady"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    iput-object p1, p0, Lcfx;->af:Lcfw;

    .line 264
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0, p0}, Lcgk;->a(Lcgj;)V

    .line 265
    invoke-virtual {p0}, Lcfx;->T()V

    .line 266
    return-void
.end method

.method public final a(Lcgp;)V
    .locals 6

    .prologue
    const/16 v5, 0xd

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    const-string v0, "InCallFragment.setCallState"

    invoke-virtual {p1}, Lcgp;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcfx;->ac:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgp;)V

    .line 130
    invoke-virtual {p0, v5}, Lcfx;->b(I)Lcff;

    move-result-object v3

    iget v0, p1, Lcgp;->u:I

    if-eqz v0, :cond_0

    move v0, v1

    .line 131
    :goto_0
    invoke-interface {v3, v0}, Lcff;->b(Z)V

    .line 132
    invoke-virtual {p0, v5}, Lcfx;->b(I)Lcff;

    move-result-object v0

    iget v3, p1, Lcgp;->u:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 133
    :goto_1
    invoke-interface {v0, v1}, Lcff;->a(Z)V

    .line 134
    iget v0, p0, Lcfx;->ai:I

    iget-boolean v1, p1, Lcgp;->j:Z

    iget v2, p0, Lcfx;->aj:I

    .line 135
    invoke-static {v0, v1, v2}, Lbvs;->a(IZI)Lcfe;

    move-result-object v0

    iput-object v0, p0, Lcfx;->ag:Lcfe;

    .line 136
    invoke-virtual {p0}, Lcfx;->T()V

    .line 137
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v1, v2

    .line 132
    goto :goto_1
.end method

.method public final a(Lcgq;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    const-string v0, "InCallFragment.setPrimary"

    invoke-virtual {p1}, Lcgq;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    iget-object v0, p1, Lcgq;->m:Lbln;

    iget-boolean v3, p1, Lcgq;->n:Z

    .line 79
    iget-object v4, p0, Lcfx;->W:Lcgb;

    if-nez v4, :cond_3

    .line 80
    new-instance v4, Lcgb;

    .line 81
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v5

    invoke-direct {v4, v5, v0, v3}, Lcgb;-><init>(Lja;Lbln;Z)V

    iput-object v4, p0, Lcfx;->W:Lcgb;

    .line 82
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    iget-object v3, p0, Lcfx;->W:Lcgb;

    invoke-virtual {v0, v3}, Lcom/android/dialer/widget/LockableViewPager;->a(Lqv;)V

    .line 87
    :cond_0
    :goto_0
    iget-object v0, p0, Lcfx;->W:Lcgb;

    invoke-virtual {v0}, Lcgb;->b()I

    move-result v0

    if-le v0, v1, :cond_6

    invoke-virtual {p0}, Lcfx;->i()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0014

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-le v0, v1, :cond_6

    .line 88
    iget-object v0, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    invoke-virtual {v0, v2}, Lcom/android/incallui/incall/impl/InCallPaginator;->setVisibility(I)V

    .line 89
    iget-object v3, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    iget-object v4, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    .line 91
    iget-object v0, v4, Landroid/support/v4/view/ViewPager;->b:Lqv;

    .line 92
    invoke-virtual {v0}, Lqv;->b()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_4

    move v0, v1

    :goto_1
    const-string v5, "Invalid page count."

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lbdf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-virtual {v4, v3}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ViewPager$f;)V

    .line 94
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    .line 95
    iput-boolean v2, v0, Lcom/android/dialer/widget/LockableViewPager;->f:Z

    .line 96
    iget-boolean v0, p0, Lcfx;->ak:Z

    if-nez v0, :cond_5

    .line 97
    iget-object v0, p0, Lcfx;->X:Landroid/os/Handler;

    iget-object v2, p0, Lcfx;->Y:Ljava/lang/Runnable;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 102
    :goto_2
    iget-object v0, p0, Lcfx;->ac:Lces;

    invoke-virtual {v0, p1}, Lces;->a(Lcgq;)V

    .line 103
    iget-boolean v0, p1, Lcgq;->k:Z

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lcfx;->ac:Lces;

    invoke-virtual {v0, v1}, Lces;->a(Z)V

    .line 106
    iget-object v0, p0, Lip;->I:Landroid/view/View;

    .line 107
    const v1, 0x7f0e01b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 108
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 109
    instance-of v0, v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 110
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 111
    :cond_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    :cond_2
    return-void

    .line 83
    :cond_3
    iget-object v3, p0, Lcfx;->W:Lcgb;

    .line 84
    iget-object v4, v3, Lcgb;->b:Lbln;

    if-eq v4, v0, :cond_0

    .line 85
    iput-object v0, v3, Lcgb;->b:Lbln;

    .line 86
    invoke-virtual {v3}, Lcgb;->c()V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 92
    goto :goto_1

    .line 98
    :cond_5
    iget-object v0, p0, Lcfx;->a:Lcom/android/dialer/widget/LockableViewPager;

    iget-object v3, p0, Lcfx;->W:Lcgb;

    .line 99
    invoke-virtual {v3}, Lcgb;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 100
    invoke-virtual {v0, v3, v2}, Lcom/android/dialer/widget/LockableViewPager;->a(IZ)V

    goto :goto_2

    .line 101
    :cond_6
    iget-object v0, p0, Lcfx;->ab:Lcom/android/incallui/incall/impl/InCallPaginator;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/android/incallui/incall/impl/InCallPaginator;->setVisibility(I)V

    goto :goto_2
.end method

.method public final a(Lcgr;)V
    .locals 4

    .prologue
    const v3, 0x7f0e002e

    .line 113
    const-string v0, "InCallFragment.setSecondary"

    invoke-virtual {p1}, Lcgr;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    invoke-virtual {p0}, Lcfx;->T()V

    .line 115
    invoke-virtual {p0}, Lcfx;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iput-object p1, p0, Lcfx;->ah:Lcgr;

    .line 127
    :goto_0
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcfx;->ah:Lcgr;

    .line 119
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v0

    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 120
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v1

    invoke-virtual {v1, v3}, Lja;->a(I)Lip;

    move-result-object v1

    .line 121
    iget-boolean v2, p1, Lcgr;->a:Z

    if-eqz v2, :cond_2

    .line 122
    invoke-static {p1}, Lcfb;->a(Lcgr;)Lcfb;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljy;->b(ILip;)Ljy;

    .line 125
    :cond_1
    :goto_1
    const v1, 0x7f050007

    const v2, 0x7f050009

    invoke-virtual {v0, v1, v2}, Ljy;->a(II)Ljy;

    .line 126
    invoke-virtual {v0}, Ljy;->d()V

    goto :goto_0

    .line 123
    :cond_2
    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {v0, v1}, Ljy;->a(Lip;)Ljy;

    goto :goto_1
.end method

.method public final a(Lip;)V
    .locals 2

    .prologue
    .line 273
    invoke-direct {p0}, Lcfx;->V()Z

    move-result v0

    .line 274
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    .line 275
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    const v1, 0x7f0e01b3

    .line 277
    invoke-virtual {v0, v1, p1}, Ljy;->b(ILip;)Ljy;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Ljy;->b()I

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcfx;->j()Lja;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lja;->a()Ljy;

    move-result-object v0

    .line 282
    invoke-direct {p0}, Lcfx;->W()Lip;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljy;->a(Lip;)Ljy;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Ljy;->b()I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcfx;->aa:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcfx;->aa:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 140
    :cond_0
    return-void
.end method

.method public final a_(I)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(I)V

    .line 254
    return-void
.end method

.method public final aa()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public final ab()I
    .locals 1

    .prologue
    .line 157
    const v0, 0x7f0e01b7

    return v0
.end method

.method public final b(I)Lcff;
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 257
    invoke-interface {v0}, Lcff;->c()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 261
    :goto_0
    return-object v0

    .line 260
    :cond_1
    invoke-static {}, Lbdf;->a()V

    .line 261
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(IZ)V
    .locals 3

    .prologue
    .line 167
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 168
    invoke-static {p1}, Lbvw;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 169
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 170
    invoke-static {p1}, Lcfx;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p2}, Lcff;->a(Z)V

    .line 172
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0, p1}, Lip;->b(Landroid/os/Bundle;)V

    .line 11
    const-class v0, Lcgl;

    .line 12
    invoke-static {p0, v0}, Lapw;->a(Lip;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgl;

    .line 13
    invoke-interface {v0}, Lcgl;->h()Lcgk;

    move-result-object v0

    iput-object v0, p0, Lcfx;->ae:Lcgk;

    .line 14
    if-eqz p1, :cond_0

    .line 15
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->b(Landroid/os/Bundle;)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcfx;->ak:Z

    .line 17
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 3

    .prologue
    .line 285
    invoke-super {p0, p1}, Lip;->d(Z)V

    .line 286
    invoke-direct {p0}, Lcfx;->V()Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 287
    const-string v0, "InCallFragment.onMultiWindowModeChanged"

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "hide = "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcfx;->a(Lip;)V

    .line 289
    :cond_0
    iget-object v0, p0, Lcfx;->ac:Lces;

    invoke-virtual {v0, p1}, Lces;->c(Z)V

    .line 290
    return-void

    .line 288
    :cond_1
    invoke-direct {p0}, Lcfx;->W()Lip;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public final e(I)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcfx;->af:Lcfw;

    .line 245
    iget-object v2, v0, Lcfw;->a:[Lcom/android/incallui/incall/impl/CheckableLabeledButton;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 247
    iget-object v4, v4, Lcom/android/incallui/incall/impl/CheckableLabeledButton;->b:Landroid/widget/ImageView;

    new-instance v5, Landroid/content/res/ColorStateList;

    new-array v6, v10, [[I

    new-array v7, v9, [I

    const v8, 0x10100a0

    aput v8, v7, v1

    aput-object v7, v6, v1

    new-array v7, v1, [I

    aput-object v7, v6, v9

    new-array v7, v10, [I

    aput p1, v7, v1

    const/4 v8, -0x1

    aput v8, v7, v9

    invoke-direct {v5, v6, v7}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1}, Lip;->e(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0, p1}, Lcgk;->a(Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 141
    invoke-virtual {p0, v1}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p1}, Lcff;->b(Z)V

    .line 142
    invoke-virtual {p0, v1}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p1}, Lcff;->a(Z)V

    .line 143
    invoke-virtual {p0}, Lcfx;->T()V

    .line 144
    return-void
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    .line 152
    const-string v0, "InCallFragment.onInCallScreenDialpadVisibilityChange"

    const/16 v1, 0x10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "isShowing: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p1}, Lcff;->c(Z)V

    .line 154
    iget-object v0, p0, Lcfx;->af:Lcfw;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcfx;->af:Lcfw;

    invoke-virtual {v0, p1}, Lcfw;->a(Z)V

    .line 156
    :cond_0
    return-void
.end method

.method public final h(Z)V
    .locals 2

    .prologue
    .line 173
    const/16 v0, 0xe

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v0, "enabled: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 174
    iget-object v0, p0, Lcfx;->Z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    .line 175
    invoke-interface {v0, p1}, Lcff;->a(Z)V

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method public final i(Z)V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcfx;->b(I)Lcff;

    move-result-object v0

    invoke-interface {v0, p1}, Lcff;->c(Z)V

    .line 179
    return-void
.end method

.method public final j(Z)V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method public final k(Z)V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public final n_()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lip;->n_()V

    .line 64
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-interface {v0}, Lcgn;->l()V

    .line 65
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 69
    iget-object v0, p0, Lcfx;->aa:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 70
    const-string v0, "InCallFragment.onClick"

    const-string v1, "end call button clicked"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p0}, Lcfx;->s_()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbib;->b(Landroid/content/Context;)Lbku;

    move-result-object v0

    sget-object v1, Lbkq$a;->dq:Lbkq$a;

    .line 72
    invoke-interface {v0, v1}, Lbku;->a(Lbkq$a;)V

    .line 73
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-interface {v0}, Lcgn;->o()V

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    const-string v0, "InCallFragment.onClick"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unknown view: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-static {}, Lbdf;->a()V

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lip;->r()V

    .line 41
    iget-object v0, p0, Lcfx;->ae:Lcgk;

    invoke-interface {v0}, Lcgk;->n()V

    .line 42
    iget-object v0, p0, Lcfx;->ad:Lcgn;

    invoke-interface {v0}, Lcgn;->p()V

    .line 43
    return-void
.end method
