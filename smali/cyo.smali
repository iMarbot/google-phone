.class public final Lcyo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcyp;)V
    .locals 9

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iget-object v0, p1, Lcyp;->a:Landroid/content/Context;

    .line 4
    iput-object v0, p0, Lcyo;->d:Landroid/content/Context;

    .line 7
    iget-object v0, p1, Lcyp;->b:Landroid/app/ActivityManager;

    .line 8
    invoke-static {v0}, Lcyo;->a(Landroid/app/ActivityManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10
    iget v0, p1, Lcyp;->h:I

    .line 11
    div-int/lit8 v0, v0, 0x2

    .line 14
    :goto_0
    iput v0, p0, Lcyo;->c:I

    .line 17
    iget-object v2, p1, Lcyp;->b:Landroid/app/ActivityManager;

    .line 19
    iget v1, p1, Lcyp;->f:F

    .line 21
    iget v0, p1, Lcyp;->g:F

    .line 23
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    shl-int/lit8 v3, v3, 0xa

    shl-int/lit8 v3, v3, 0xa

    .line 24
    invoke-static {v2}, Lcyo;->a(Landroid/app/ActivityManager;)Z

    move-result v2

    .line 25
    int-to-float v3, v3

    if-eqz v2, :cond_2

    .line 26
    :goto_1
    mul-float/2addr v0, v3

    .line 27
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 30
    iget-object v0, p1, Lcyp;->c:Lcyq;

    .line 31
    invoke-virtual {v0}, Lcyq;->a()I

    move-result v0

    .line 33
    iget-object v2, p1, Lcyp;->c:Lcyq;

    .line 34
    invoke-virtual {v2}, Lcyq;->b()I

    move-result v2

    .line 35
    mul-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x2

    .line 36
    int-to-float v2, v0

    .line 37
    iget v3, p1, Lcyp;->e:F

    .line 38
    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 39
    int-to-float v0, v0

    .line 40
    iget v3, p1, Lcyp;->d:F

    .line 41
    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 42
    iget v3, p0, Lcyo;->c:I

    sub-int v3, v1, v3

    .line 43
    add-int v4, v0, v2

    if-gt v4, v3, :cond_3

    .line 44
    iput v0, p0, Lcyo;->b:I

    .line 45
    iput v2, p0, Lcyo;->a:I

    .line 57
    :goto_2
    const-string v3, "MemorySizeCalculator"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    iget v3, p0, Lcyo;->b:I

    .line 59
    invoke-direct {p0, v3}, Lcyo;->a(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcyo;->a:I

    .line 60
    invoke-direct {p0, v4}, Lcyo;->a(I)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcyo;->c:I

    .line 61
    invoke-direct {p0, v5}, Lcyo;->a(I)Ljava/lang/String;

    move-result-object v5

    add-int/2addr v0, v2

    if-le v0, v1, :cond_4

    const/4 v0, 0x1

    .line 62
    :goto_3
    invoke-direct {p0, v1}, Lcyo;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 64
    iget-object v2, p1, Lcyp;->b:Landroid/app/ActivityManager;

    .line 65
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    .line 67
    iget-object v6, p1, Lcyp;->b:Landroid/app/ActivityManager;

    .line 68
    invoke-static {v6}, Lcyo;->a(Landroid/app/ActivityManager;)Z

    move-result v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit16 v7, v7, 0xb1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Calculation complete, Calculated memory cache size: "

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", pool size: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", byte array size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", memory class limited? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", max size: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", memoryClass: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLowMemoryDevice: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 69
    :cond_0
    return-void

    .line 13
    :cond_1
    iget v0, p1, Lcyp;->h:I

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 26
    goto/16 :goto_1

    .line 46
    :cond_3
    int-to-float v3, v3

    .line 47
    iget v4, p1, Lcyp;->e:F

    .line 49
    iget v5, p1, Lcyp;->d:F

    .line 50
    add-float/2addr v4, v5

    div-float/2addr v3, v4

    .line 52
    iget v4, p1, Lcyp;->d:F

    .line 53
    mul-float/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v4, p0, Lcyo;->b:I

    .line 55
    iget v4, p1, Lcyp;->e:F

    .line 56
    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, p0, Lcyo;->a:I

    goto/16 :goto_2

    .line 61
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method private final a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcyo;->d:Landroid/content/Context;

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/app/ActivityManager;)Z
    .locals 2

    .prologue
    .line 71
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 72
    invoke-virtual {p0}, Landroid/app/ActivityManager;->isLowRamDevice()Z

    move-result v0

    .line 73
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
