.class public final Laze;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:Landroid/view/View$OnTouchListener;

.field public final d:Lazf;

.field private e:Z


# direct methods
.method public constructor <init>(Lazf;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Laze;->a:I

    .line 3
    iput v0, p0, Laze;->b:I

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Laze;->e:Z

    .line 5
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    invoke-interface {p1}, Lazf;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    iput-object p1, p0, Laze;->d:Lazf;

    .line 8
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 16
    iget v0, p0, Laze;->b:I

    if-ltz v0, :cond_0

    .line 17
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 18
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 19
    :cond_0
    return p1
.end method

.method public final a(II)I
    .locals 4

    .prologue
    .line 20
    iget v0, p0, Laze;->b:I

    if-ltz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Laze;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 22
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 23
    iget v2, p0, Laze;->a:I

    int-to-float v2, v2

    iget v3, p0, Laze;->b:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 24
    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 25
    int-to-float v0, v1

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 27
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 28
    :cond_0
    return p2

    .line 26
    :cond_1
    int-to-float v0, v1

    div-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Laze;->e:Z

    .line 10
    invoke-virtual {p0}, Laze;->b()V

    .line 11
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Laze;->d:Lazf;

    if-nez v0, :cond_0

    throw v1

    :cond_0
    check-cast v0, Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object v1, p0, Laze;->c:Landroid/view/View$OnTouchListener;

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 36
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Laze;->d:Lazf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 13
    :goto_0
    iget-boolean v1, p0, Laze;->e:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Laze;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    invoke-virtual {v0}, Layq;->b()V

    .line 15
    :cond_1
    return-void

    .line 12
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Laze;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbsw;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    if-nez p1, :cond_1

    .line 31
    invoke-virtual {p0}, Laze;->b()V

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-static {}, Layq;->a()Layq;

    move-result-object v0

    invoke-virtual {v0}, Layq;->c()V

    goto :goto_0
.end method

.method public final c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Laze;->d:Lazf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    throw v0

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
