.class public final Lhio;
.super Lhbr;
.source "PG"

# interfaces
.implements Lhdf;


# static fields
.field public static final h:Lhio;

.field private static volatile i:Lhdm;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:Lhii;

.field public f:Ljava/lang/String;

.field public g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 145
    new-instance v0, Lhio;

    invoke-direct {v0}, Lhio;-><init>()V

    .line 146
    sput-object v0, Lhio;->h:Lhio;

    invoke-virtual {v0}, Lhio;->makeImmutable()V

    .line 147
    const-class v0, Lhio;

    sget-object v1, Lhio;->h:Lhio;

    invoke-static {v0, v1}, Lhbr;->registerDefaultInstance(Ljava/lang/Class;Lhbr;)V

    .line 148
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhbr;-><init>()V

    .line 2
    const/16 v0, 0x3e8

    iput v0, p0, Lhio;->b:I

    .line 3
    const v0, 0x186a0

    iput v0, p0, Lhio;->c:I

    .line 4
    const-string v0, ""

    iput-object v0, p0, Lhio;->f:Ljava/lang/String;

    .line 5
    return-void
.end method


# virtual methods
.method protected final buildMessageInfo()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 138
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "f"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "g"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "b"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 139
    sget-object v2, Lbkq$a;->ee:Lhby;

    .line 140
    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "c"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 141
    sget-object v2, Ldnt$a;->p:Lhby;

    .line 142
    aput-object v2, v0, v1

    .line 143
    const-string v1, "\u0001\u0006\u0000\u0001\u0003\u0008\u0000\u0000\u0000\u0003\u0002\u0002\u0004\t\u0003\u0005\u0008\u0004\u0006\u0002\u0005\u0007\u000c\u0000\u0008\u000c\u0001"

    .line 144
    sget-object v2, Lhio;->h:Lhio;

    invoke-static {v2, v1, v0}, Lhio;->newMessageInfo(Lhdd;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final dynamicMethod(Lhbr$e;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 66
    invoke-virtual {p1}, Lhbr$e;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 67
    :pswitch_0
    new-instance v0, Lhio;

    invoke-direct {v0}, Lhio;-><init>()V

    .line 136
    :goto_0
    return-object v0

    .line 68
    :pswitch_1
    sget-object v0, Lhio;->h:Lhio;

    goto :goto_0

    :pswitch_2
    move-object v0, v1

    .line 69
    goto :goto_0

    .line 70
    :pswitch_3
    new-instance v0, Lhbr$a;

    invoke-direct {v0, v2, v1}, Lhbr$a;-><init>(B[[[[[[I)V

    goto :goto_0

    .line 71
    :pswitch_4
    check-cast p2, Lhaq;

    .line 72
    check-cast p3, Lhbg;

    .line 73
    if-nez p3, :cond_0

    .line 74
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_0
    :try_start_0
    sget-boolean v0, Lhio;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0, p2, p3}, Lhio;->mergeFromInternal(Lhaq;Lhbg;)V

    .line 77
    sget-object v0, Lhio;->h:Lhio;

    goto :goto_0

    :cond_1
    move v3, v2

    .line 79
    :cond_2
    :goto_1
    if-nez v3, :cond_6

    .line 80
    invoke-virtual {p2}, Lhaq;->a()I

    move-result v0

    .line 81
    sparse-switch v0, :sswitch_data_0

    .line 84
    invoke-virtual {p0, v0, p2}, Lhio;->parseUnknownField(ILhaq;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 85
    goto :goto_1

    :sswitch_0
    move v3, v4

    .line 83
    goto :goto_1

    .line 86
    :sswitch_1
    iget v0, p0, Lhio;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lhio;->a:I

    .line 87
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v6

    iput-wide v6, p0, Lhio;->d:J
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 123
    :catch_0
    move-exception v0

    .line 124
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    :catchall_0
    move-exception v0

    throw v0

    .line 90
    :sswitch_2
    :try_start_2
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_9

    .line 91
    iget-object v0, p0, Lhio;->e:Lhii;

    invoke-virtual {v0}, Lhii;->toBuilder()Lhbr$a;

    move-result-object v0

    check-cast v0, Lhbr$a;

    move-object v2, v0

    .line 93
    :goto_2
    sget-object v0, Lhii;->k:Lhii;

    .line 95
    invoke-virtual {p2, v0, p3}, Lhaq;->a(Lhbr;Lhbg;)Lhbr;

    move-result-object v0

    check-cast v0, Lhii;

    iput-object v0, p0, Lhio;->e:Lhii;

    .line 96
    if-eqz v2, :cond_3

    .line 97
    iget-object v0, p0, Lhio;->e:Lhii;

    invoke-virtual {v2, v0}, Lhbr$a;->mergeFrom(Lhbr;)Lhbr$a;

    .line 98
    invoke-virtual {v2}, Lhbr$a;->buildPartial()Lhbr;

    move-result-object v0

    check-cast v0, Lhii;

    iput-object v0, p0, Lhio;->e:Lhii;

    .line 99
    :cond_3
    iget v0, p0, Lhio;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lhio;->a:I
    :try_end_2
    .catch Lhcf; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 125
    :catch_1
    move-exception v0

    .line 126
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Lhcf;

    .line 127
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lhcf;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 101
    :sswitch_3
    :try_start_4
    invoke-virtual {p2}, Lhaq;->j()Ljava/lang/String;

    move-result-object v0

    .line 102
    iget v2, p0, Lhio;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lhio;->a:I

    .line 103
    iput-object v0, p0, Lhio;->f:Ljava/lang/String;

    goto :goto_1

    .line 105
    :sswitch_4
    iget v0, p0, Lhio;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lhio;->a:I

    .line 106
    invoke-virtual {p2}, Lhaq;->e()J

    move-result-wide v6

    iput-wide v6, p0, Lhio;->g:J

    goto/16 :goto_1

    .line 108
    :sswitch_5
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 109
    invoke-static {v0}, Lbkq$a;->a(I)Lbkq$a;

    move-result-object v2

    .line 110
    if-nez v2, :cond_4

    .line 111
    const/4 v2, 0x7

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 112
    :cond_4
    iget v2, p0, Lhio;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lhio;->a:I

    .line 113
    iput v0, p0, Lhio;->b:I

    goto/16 :goto_1

    .line 115
    :sswitch_6
    invoke-virtual {p2}, Lhaq;->n()I

    move-result v0

    .line 116
    invoke-static {v0}, Ldnt$a;->a(I)Ldnt$a;

    move-result-object v2

    .line 117
    if-nez v2, :cond_5

    .line 118
    const/16 v2, 0x8

    invoke-super {p0, v2, v0}, Lhbr;->mergeVarintField(II)V

    goto/16 :goto_1

    .line 119
    :cond_5
    iget v2, p0, Lhio;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lhio;->a:I

    .line 120
    iput v0, p0, Lhio;->c:I
    :try_end_4
    .catch Lhcf; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 129
    :cond_6
    :pswitch_5
    sget-object v0, Lhio;->h:Lhio;

    goto/16 :goto_0

    .line 130
    :pswitch_6
    sget-object v0, Lhio;->i:Lhdm;

    if-nez v0, :cond_8

    const-class v1, Lhio;

    monitor-enter v1

    .line 131
    :try_start_5
    sget-object v0, Lhio;->i:Lhdm;

    if-nez v0, :cond_7

    .line 132
    new-instance v0, Lhaa;

    sget-object v2, Lhio;->h:Lhio;

    invoke-direct {v0, v2}, Lhaa;-><init>(Lhbr;)V

    sput-object v0, Lhio;->i:Lhdm;

    .line 133
    :cond_7
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 134
    :cond_8
    sget-object v0, Lhio;->i:Lhdm;

    goto/16 :goto_0

    .line 133
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 135
    :pswitch_7
    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    move-object v0, v1

    .line 136
    goto/16 :goto_0

    :cond_9
    move-object v2, v1

    goto/16 :goto_2

    .line 66
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
    .end sparse-switch
.end method

.method public final getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    .line 33
    iget v0, p0, Lhio;->memoizedSerializedSize:I

    .line 34
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 65
    :goto_0
    return v0

    .line 35
    :cond_0
    sget-boolean v0, Lhio;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lhio;->getSerializedSizeInternal()I

    move-result v0

    iput v0, p0, Lhio;->memoizedSerializedSize:I

    .line 37
    iget v0, p0, Lhio;->memoizedSerializedSize:I

    goto :goto_0

    .line 38
    :cond_1
    const/4 v0, 0x0

    .line 39
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-wide v2, p0, Lhio;->d:J

    .line 41
    invoke-static {v0, v2, v3}, Lhaw;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_2
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_3

    .line 45
    iget-object v1, p0, Lhio;->e:Lhii;

    if-nez v1, :cond_8

    .line 46
    sget-object v1, Lhii;->k:Lhii;

    .line 48
    :goto_1
    invoke-static {v4, v1}, Lhaw;->c(ILhdd;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    :cond_3
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 50
    const/4 v1, 0x5

    .line 52
    iget-object v2, p0, Lhio;->f:Ljava/lang/String;

    .line 53
    invoke-static {v1, v2}, Lhaw;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    :cond_4
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 55
    const/4 v1, 0x6

    iget-wide v2, p0, Lhio;->g:J

    .line 56
    invoke-static {v1, v2, v3}, Lhaw;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    :cond_5
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 58
    const/4 v1, 0x7

    iget v2, p0, Lhio;->b:I

    .line 59
    invoke-static {v1, v2}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_6
    iget v1, p0, Lhio;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 61
    iget v1, p0, Lhio;->c:I

    .line 62
    invoke-static {v5, v1}, Lhaw;->k(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_7
    iget-object v1, p0, Lhio;->unknownFields:Lheq;

    invoke-virtual {v1}, Lheq;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    iput v0, p0, Lhio;->memoizedSerializedSize:I

    goto :goto_0

    .line 47
    :cond_8
    iget-object v1, p0, Lhio;->e:Lhii;

    goto :goto_1
.end method

.method public final writeTo(Lhaw;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x4

    .line 6
    sget-boolean v0, Lhio;->usingExperimentalRuntime:Z

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p0, p1}, Lhio;->writeToInternal(Lhaw;)V

    .line 32
    :goto_0
    return-void

    .line 9
    :cond_0
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v1, :cond_1

    .line 10
    const/4 v0, 0x3

    iget-wide v2, p0, Lhio;->d:J

    .line 11
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 12
    :cond_1
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_2

    .line 14
    iget-object v0, p0, Lhio;->e:Lhii;

    if-nez v0, :cond_7

    .line 15
    sget-object v0, Lhii;->k:Lhii;

    .line 17
    :goto_1
    invoke-virtual {p1, v1, v0}, Lhaw;->a(ILhdd;)V

    .line 18
    :cond_2
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 19
    const/4 v0, 0x5

    .line 20
    iget-object v1, p0, Lhio;->f:Ljava/lang/String;

    .line 21
    invoke-virtual {p1, v0, v1}, Lhaw;->a(ILjava/lang/String;)V

    .line 22
    :cond_3
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 23
    const/4 v0, 0x6

    iget-wide v2, p0, Lhio;->g:J

    .line 24
    invoke-virtual {p1, v0, v2, v3}, Lhaw;->a(IJ)V

    .line 25
    :cond_4
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 26
    const/4 v0, 0x7

    iget v1, p0, Lhio;->b:I

    .line 27
    invoke-virtual {p1, v0, v1}, Lhaw;->b(II)V

    .line 28
    :cond_5
    iget v0, p0, Lhio;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 29
    iget v0, p0, Lhio;->c:I

    .line 30
    invoke-virtual {p1, v4, v0}, Lhaw;->b(II)V

    .line 31
    :cond_6
    iget-object v0, p0, Lhio;->unknownFields:Lheq;

    invoke-virtual {v0, p1}, Lheq;->a(Lhaw;)V

    goto :goto_0

    .line 16
    :cond_7
    iget-object v0, p0, Lhio;->e:Lhii;

    goto :goto_1
.end method
