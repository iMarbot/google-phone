.class final Lghi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:I

.field private b:Lghl;

.field private c:Ljava/lang/Object;

.field private d:Z

.field private e:Z

.field private f:Lghl;

.field private synthetic g:Lghg;


# direct methods
.method constructor <init>(Lghg;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lghi;->g:Lghg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, -0x1

    iput v0, p0, Lghi;->a:I

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 3
    iget-boolean v0, p0, Lghi;->e:Z

    if-nez v0, :cond_0

    .line 4
    iput-boolean v1, p0, Lghi;->e:Z

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lghi;->c:Ljava/lang/Object;

    .line 6
    :goto_0
    iget-object v0, p0, Lghi;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget v0, p0, Lghi;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lghi;->a:I

    iget-object v2, p0, Lghi;->g:Lghg;

    iget-object v2, v2, Lghg;->b:Lghb;

    iget-object v2, v2, Lghb;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 7
    iget-object v0, p0, Lghi;->g:Lghg;

    iget-object v2, v0, Lghg;->b:Lghb;

    iget-object v0, p0, Lghi;->g:Lghg;

    iget-object v0, v0, Lghg;->b:Lghb;

    iget-object v0, v0, Lghb;->d:Ljava/util/List;

    iget v3, p0, Lghi;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lghb;->a(Ljava/lang/String;)Lghl;

    move-result-object v0

    iput-object v0, p0, Lghi;->b:Lghl;

    .line 8
    iget-object v0, p0, Lghi;->b:Lghl;

    iget-object v2, p0, Lghi;->g:Lghg;

    iget-object v2, v2, Lghg;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lghl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lghi;->c:Ljava/lang/Object;

    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lghi;->c:Ljava/lang/Object;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 14
    .line 15
    invoke-virtual {p0}, Lghi;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 17
    :cond_0
    iget-object v0, p0, Lghi;->b:Lghl;

    iput-object v0, p0, Lghi;->f:Lghl;

    .line 18
    iget-object v0, p0, Lghi;->c:Ljava/lang/Object;

    .line 19
    iput-boolean v1, p0, Lghi;->e:Z

    .line 20
    iput-boolean v1, p0, Lghi;->d:Z

    .line 21
    iput-object v2, p0, Lghi;->b:Lghl;

    .line 22
    iput-object v2, p0, Lghi;->c:Ljava/lang/Object;

    .line 23
    new-instance v1, Lghh;

    iget-object v2, p0, Lghi;->g:Lghg;

    iget-object v3, p0, Lghi;->f:Lghl;

    invoke-direct {v1, v2, v3, v0}, Lghh;-><init>(Lghg;Lghl;Ljava/lang/Object;)V

    .line 24
    return-object v1
.end method

.method public final remove()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 10
    iget-object v0, p0, Lghi;->f:Lghl;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lghi;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lgfb$a;->b(Z)V

    .line 11
    iput-boolean v1, p0, Lghi;->d:Z

    .line 12
    iget-object v0, p0, Lghi;->f:Lghl;

    iget-object v1, p0, Lghi;->g:Lghg;

    iget-object v1, v1, Lghg;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lghl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 13
    return-void

    .line 10
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
