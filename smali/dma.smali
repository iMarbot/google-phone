.class public Ldma;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldma;->a:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    iget-object v0, p0, Ldma;->a:Landroid/content/Context;

    invoke-static {v0}, Lcsw;->c(Landroid/content/Context;)Lcte;

    move-result-object v0

    invoke-virtual {v0}, Lcte;->d()Lctb;

    move-result-object v0

    .line 3
    invoke-virtual {v0, p1}, Lctb;->a(Ljava/lang/Object;)Lctb;

    move-result-object v0

    .line 6
    new-instance v1, Ldgj;

    iget-object v2, v0, Lctb;->b:Lcsy;

    .line 8
    iget-object v2, v2, Lcsy;->b:Landroid/os/Handler;

    .line 9
    invoke-direct {v1, v2, v3, v3}, Ldgj;-><init>(Landroid/os/Handler;II)V

    .line 10
    invoke-static {}, Ldhw;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 11
    iget-object v2, v0, Lctb;->b:Lcsy;

    .line 12
    iget-object v2, v2, Lcsy;->b:Landroid/os/Handler;

    .line 13
    new-instance v3, Lctc;

    invoke-direct {v3, v0, v1}, Lctc;-><init>(Lctb;Ldgj;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 16
    :goto_0
    invoke-interface {v1}, Ldgg;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0

    .line 14
    :cond_0
    invoke-virtual {v0, v1, v1}, Lctb;->a(Ldha;Ldgm;)Ldha;

    goto :goto_0
.end method
