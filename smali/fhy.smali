.class public final Lfhy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lfhy;->a:Landroid/content/Context;

    .line 3
    return-void
.end method

.method public static a(Landroid/content/Context;)Lfhy;
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lfhy;

    invoke-direct {v0, p0}, Lfhy;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V
    .locals 6

    .prologue
    .line 5
    new-instance v0, Lfid;

    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lfid;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 6
    const-string v1, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/chat.native https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/identity.plus.page.impersonation "

    .line 7
    iput-object v1, v0, Lfid;->i:Ljava/lang/String;

    .line 8
    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    .line 9
    invoke-static {v1}, Lfmd;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfhy;->a:Landroid/content/Context;

    .line 10
    invoke-static {v2}, Lfmd;->k(Landroid/content/Context;)I

    move-result v3

    const/4 v5, 0x0

    move-object v2, p2

    move-object v4, p3

    .line 11
    invoke-virtual/range {v0 .. v5}, Lfid;->a(Ljava/lang/String;Ljava/lang/String;ILhfz;Lfie;)V

    .line 12
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lhfz;)[B
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lfid;

    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lfid;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 22
    const-string v1, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/chat.native https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/identity.plus.page.impersonation "

    .line 23
    iput-object v1, v0, Lfid;->i:Ljava/lang/String;

    .line 24
    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    .line 25
    invoke-static {v1}, Lfmd;->y(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfhy;->a:Landroid/content/Context;

    .line 26
    invoke-static {v2}, Lfmd;->j(Landroid/content/Context;)I

    move-result v2

    .line 27
    invoke-virtual {v0, v1, p2, v2, p3}, Lfid;->a(Ljava/lang/String;Ljava/lang/String;ILhfz;)[B

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lhfz;Lfie;)V
    .locals 6

    .prologue
    .line 13
    new-instance v0, Lfid;

    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lfid;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 14
    const-string v1, "oauth2:https://www.googleapis.com/auth/chat https://www.googleapis.com/auth/hangouts https://www.googleapis.com/auth/chat.native https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/identity.plus.page.impersonation "

    .line 15
    iput-object v1, v0, Lfid;->i:Ljava/lang/String;

    .line 16
    iget-object v1, p0, Lfhy;->a:Landroid/content/Context;

    .line 17
    invoke-static {v1}, Lfmd;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfhy;->a:Landroid/content/Context;

    .line 18
    invoke-static {v2}, Lfmd;->m(Landroid/content/Context;)I

    move-result v3

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 19
    invoke-virtual/range {v0 .. v5}, Lfid;->a(Ljava/lang/String;Ljava/lang/String;ILhfz;Lfie;)V

    .line 20
    return-void
.end method
