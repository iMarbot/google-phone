.class public final Ldtw;
.super Lduz;


# instance fields
.field public a:Z

.field public b:Z

.field public final c:Landroid/app/AlarmManager;

.field private d:Ljava/lang/Integer;


# direct methods
.method protected constructor <init>(Ldvb;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lduz;-><init>(Ldvb;)V

    .line 2
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 3
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 4
    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Ldtw;->c:Landroid/app/AlarmManager;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 5
    :try_start_0
    invoke-virtual {p0}, Ldtw;->c()V

    invoke-static {}, Ldts;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 6
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 7
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 8
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    .line 9
    iget-object v2, p0, Lduy;->f:Ldvb;

    .line 10
    iget-object v2, v2, Ldvb;->a:Landroid/content/Context;

    .line 11
    const-string v3, "com.google.android.gms.analytics.AnalyticsReceiver"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v0, :cond_0

    const-string v0, "Receiver registered for local dispatch."

    invoke-virtual {p0, v0}, Lduy;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldtw;->a:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final b()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 12
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    .line 13
    iget-object v2, p0, Lduy;->f:Ldvb;

    .line 14
    iget-object v2, v2, Ldvb;->a:Landroid/content/Context;

    .line 15
    const-string v3, "com.google.android.gms.analytics.AnalyticsReceiver"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 16
    iget-object v1, p0, Lduy;->f:Ldvb;

    .line 17
    iget-object v1, v1, Ldvb;->a:Landroid/content/Context;

    .line 18
    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldtw;->b:Z

    iget-object v0, p0, Ldtw;->c:Landroid/app/AlarmManager;

    invoke-virtual {p0}, Ldtw;->b()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 20
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 21
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 22
    const-string v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const-string v1, "Cancelling job. JobID"

    invoke-virtual {p0}, Ldtw;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lduy;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Ldtw;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    :cond_0
    return-void
.end method

.method final d()I
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const-string v1, "analytics"

    .line 24
    iget-object v0, p0, Lduy;->f:Ldvb;

    .line 25
    iget-object v0, v0, Ldvb;->a:Landroid/content/Context;

    .line 26
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, Ldtw;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
