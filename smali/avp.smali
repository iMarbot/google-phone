.class public abstract Lavp;
.super Landroid/app/Application;
.source "PG"

# interfaces
.implements Lbkc;


# instance fields
.field private volatile a:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract c()Ljava/lang/Object;
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lavp;->a:Ljava/lang/Object;

    .line 23
    if-nez v0, :cond_1

    .line 24
    monitor-enter p0

    .line 25
    :try_start_0
    iget-object v0, p0, Lavp;->a:Ljava/lang/Object;

    .line 26
    if-nez v0, :cond_0

    .line 27
    invoke-virtual {p0}, Lavp;->c()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lavp;->a:Ljava/lang/Object;

    .line 28
    :cond_0
    monitor-exit p0

    .line 29
    :cond_1
    return-object v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 2
    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lbkc;

    invoke-interface {v0}, Lbkc;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsn;

    .line 5
    invoke-interface {v0}, Lbsn;->m()Lbsm;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Lbsm;->a()Lbsl;

    move-result-object v0

    invoke-interface {v0, p0}, Lbsl;->a(Landroid/app/Application;)V

    .line 7
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 8
    new-instance v0, Lawk;

    .line 9
    invoke-virtual {p0}, Lavp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lawr;

    invoke-direct {v2, p0}, Lawr;-><init>(Landroid/content/Context;)V

    .line 10
    invoke-static {p0}, Lbed;->a(Landroid/content/Context;)Lbed;

    move-result-object v3

    invoke-virtual {v3}, Lbed;->a()Lbef;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lawk;-><init>(Landroid/content/Context;Lawr;Lbef;)V

    .line 12
    iget-object v1, v0, Lawk;->c:Lbef;

    new-instance v2, Lawn;

    iget-object v3, v0, Lawk;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lawn;-><init>(Landroid/content/Context;)V

    .line 13
    invoke-virtual {v1, v2}, Lbef;->a(Lbec;)Lbdz;

    move-result-object v1

    new-instance v2, Lawl;

    invoke-direct {v2, v0}, Lawl;-><init>(Lawk;)V

    .line 14
    invoke-interface {v1, v2}, Lbdz;->a(Lbeb;)Lbdz;

    move-result-object v0

    .line 15
    invoke-interface {v0}, Lbdz;->a()Lbdy;

    move-result-object v0

    const/4 v1, 0x0

    .line 16
    invoke-interface {v0, v1}, Lbdy;->b(Ljava/lang/Object;)V

    .line 17
    invoke-static {p0}, Lbbo;->a(Landroid/content/Context;)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->a()Lbch;

    move-result-object v0

    invoke-virtual {p0}, Lavp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbch;->a(Landroid/content/Context;)V

    .line 18
    invoke-static {p0}, Lbmd;->a(Landroid/content/Context;)V

    .line 19
    invoke-static {}, Lbw;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-static {p0}, Lbib;->e(Landroid/content/Context;)V

    .line 21
    :cond_0
    return-void
.end method
