.class Lfxq$b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lfxq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lfxq;


# direct methods
.method constructor <init>(Lfxq;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lfxq$b;->a:Lfxq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lfxq$b;->a:Lfxq;

    .line 2
    invoke-virtual {v0, p1}, Lfxq;->a(I)V

    .line 3
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 4
    iget-object v0, p0, Lfxq$b;->a:Lfxq;

    .line 5
    iget-object v1, v0, Lfxq;->e:Ljava/util/Map;

    monitor-enter v1

    .line 6
    :try_start_0
    iget-object v2, v0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7
    const-string v0, "FrameMetricService"

    const-string v2, "measurement already started: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 8
    monitor-exit v1

    .line 24
    :goto_0
    return-void

    .line 9
    :cond_0
    iget-object v2, v0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/16 v3, 0x19

    if-lt v2, v3, :cond_1

    .line 10
    const-string v0, "FrameMetricService"

    const-string v2, "Too many concurrent measurements, ignoring %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v2, v3}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 11
    monitor-exit v1

    goto :goto_0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 12
    :cond_1
    :try_start_1
    iget-boolean v2, v0, Lfxq;->f:Z

    if-nez v2, :cond_3

    .line 13
    iget-object v2, v0, Lfxq;->e:Ljava/util/Map;

    new-instance v3, Lfxs;

    invoke-direct {v3}, Lfxs;-><init>()V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    :goto_1
    iget-object v2, v0, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-ne v2, v4, :cond_2

    iget-boolean v2, v0, Lfxq;->g:Z

    if-nez v2, :cond_2

    .line 16
    const-string v2, "FrameMetricService"

    const-string v3, "measuring start"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 17
    iget-object v2, v0, Lfxq;->d:Lfxq$a;

    .line 18
    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, v2, Lfxq$a;->b:Z

    .line 20
    iget-object v0, v2, Lfxq$a;->a:Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 21
    invoke-virtual {v2}, Lfxq$a;->a()V

    .line 23
    :goto_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 24
    :cond_2
    :try_start_3
    monitor-exit v1

    goto :goto_0

    .line 14
    :cond_3
    iget-object v2, v0, Lfxq;->e:Ljava/util/Map;

    new-instance v3, Lfxr;

    invoke-direct {v3, p1}, Lfxr;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 22
    :cond_4
    :try_start_4
    const-string v0, "FrameMetricService"

    const-string v3, "No activity"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lfmk;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 23
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 25
    iget-object v1, p0, Lfxq$b;->a:Lfxq;

    .line 26
    iget-object v2, v1, Lfxq;->e:Ljava/util/Map;

    monitor-enter v2

    .line 27
    :try_start_0
    iget-object v0, v1, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxt;

    .line 28
    iget-object v3, v1, Lfxq;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v1, Lfxq;->g:Z

    if-nez v3, :cond_0

    .line 29
    iget-object v3, v1, Lfxq;->d:Lfxq$a;

    invoke-virtual {v3}, Lfxq$a;->b()V

    .line 30
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    iget-boolean v2, v1, Lfxq;->f:Z

    if-nez v2, :cond_1

    invoke-interface {v0}, Lfxt;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 33
    new-instance v2, Lhtd;

    invoke-direct {v2}, Lhtd;-><init>()V

    .line 34
    invoke-interface {v0}, Lfxt;->b()Lhru;

    move-result-object v0

    iput-object v0, v2, Lhtd;->k:Lhru;

    .line 35
    iget-object v0, v2, Lhtd;->k:Lhru;

    .line 37
    iget-object v3, v1, Lfwq;->a:Landroid/app/Application;

    .line 38
    invoke-static {v3}, Lgcl;->b(Landroid/app/Application;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lhru;->f:Ljava/lang/Integer;

    .line 40
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v4, v2, v0}, Lfwq;->a(Ljava/lang/String;ZLhtd;Lhrz;)V

    .line 43
    :cond_1
    :goto_0
    return-void

    .line 30
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 42
    :cond_2
    const-string v0, "FrameMetricService"

    const-string v1, "Measurement not found: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lfmk;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
