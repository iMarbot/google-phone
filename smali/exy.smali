.class public final Lexy;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Ljava/util/List;

.field private b:F

.field private c:I

.field private d:F

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Lexp;

.field private i:Lexp;

.field private j:I

.field private k:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyq;

    invoke-direct {v0}, Leyq;-><init>()V

    sput-object v0, Lexy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexy;->b:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexy;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lexy;->d:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexy;->e:Z

    iput-boolean v1, p0, Lexy;->f:Z

    iput-boolean v1, p0, Lexy;->g:Z

    new-instance v0, Lexo;

    invoke-direct {v0}, Lexo;-><init>()V

    iput-object v0, p0, Lexy;->h:Lexp;

    new-instance v0, Lexo;

    invoke-direct {v0}, Lexo;-><init>()V

    iput-object v0, p0, Lexy;->i:Lexp;

    iput v1, p0, Lexy;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lexy;->k:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lexy;->a:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/util/List;FIFZZZLexp;Lexp;ILjava/util/List;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lepr;-><init>()V

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lexy;->b:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lexy;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lexy;->d:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexy;->e:Z

    iput-boolean v1, p0, Lexy;->f:Z

    iput-boolean v1, p0, Lexy;->g:Z

    new-instance v0, Lexo;

    invoke-direct {v0}, Lexo;-><init>()V

    iput-object v0, p0, Lexy;->h:Lexp;

    new-instance v0, Lexo;

    invoke-direct {v0}, Lexo;-><init>()V

    iput-object v0, p0, Lexy;->i:Lexp;

    iput v1, p0, Lexy;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lexy;->k:Ljava/util/List;

    iput-object p1, p0, Lexy;->a:Ljava/util/List;

    iput p2, p0, Lexy;->b:F

    iput p3, p0, Lexy;->c:I

    iput p4, p0, Lexy;->d:F

    iput-boolean p5, p0, Lexy;->e:Z

    iput-boolean p6, p0, Lexy;->f:Z

    iput-boolean p7, p0, Lexy;->g:Z

    if-eqz p8, :cond_0

    iput-object p8, p0, Lexy;->h:Lexp;

    :cond_0
    if-eqz p9, :cond_1

    iput-object p9, p0, Lexy;->i:Lexp;

    :cond_1
    iput p10, p0, Lexy;->j:I

    iput-object p11, p0, Lexy;->k:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    .line 2
    iget-object v2, p0, Lexy;->a:Ljava/util/List;

    .line 3
    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x3

    .line 4
    iget v2, p0, Lexy;->b:F

    .line 5
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x4

    .line 6
    iget v2, p0, Lexy;->c:I

    .line 7
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/4 v1, 0x5

    .line 8
    iget v2, p0, Lexy;->d:F

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x6

    .line 10
    iget-boolean v2, p0, Lexy;->e:Z

    .line 11
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x7

    .line 12
    iget-boolean v2, p0, Lexy;->f:Z

    .line 13
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x8

    .line 14
    iget-boolean v2, p0, Lexy;->g:Z

    .line 15
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x9

    .line 16
    iget-object v2, p0, Lexy;->h:Lexp;

    .line 17
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xa

    .line 18
    iget-object v2, p0, Lexy;->i:Lexp;

    .line 19
    invoke-static {p1, v1, v2, p2, v3}, Letf;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xb

    .line 20
    iget v2, p0, Lexy;->j:I

    .line 21
    invoke-static {p1, v1, v2}, Letf;->d(Landroid/os/Parcel;II)V

    const/16 v1, 0xc

    .line 22
    iget-object v2, p0, Lexy;->k:Ljava/util/List;

    .line 23
    invoke-static {p1, v1, v2, v3}, Letf;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
