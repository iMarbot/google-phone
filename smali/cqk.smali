.class public Lcqk;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x1a
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field public final b:Lcnw;

.field public c:Z

.field private d:Landroid/telecom/PhoneAccountHandle;

.field private e:Landroid/net/NetworkRequest;

.field private f:Landroid/net/ConnectivityManager;

.field private g:Lclu;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Lcnw;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    .line 2
    iput-boolean v0, p0, Lcqk;->h:Z

    .line 3
    iput-boolean v0, p0, Lcqk;->c:Z

    .line 4
    iput-object p1, p0, Lcqk;->a:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcqk;->d:Landroid/telecom/PhoneAccountHandle;

    .line 6
    iput-object p3, p0, Lcqk;->b:Lcnw;

    .line 7
    new-instance v0, Lclu;

    iget-object v1, p0, Lcqk;->d:Landroid/telecom/PhoneAccountHandle;

    invoke-direct {v0, p1, v1}, Lclu;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    iput-object v0, p0, Lcqk;->g:Lclu;

    .line 8
    invoke-direct {p0}, Lcqk;->c()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcqk;->e:Landroid/net/NetworkRequest;

    .line 9
    return-void
.end method

.method public constructor <init>(Lclu;Landroid/telecom/PhoneAccountHandle;Lcnw;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    .line 11
    iput-boolean v0, p0, Lcqk;->h:Z

    .line 12
    iput-boolean v0, p0, Lcqk;->c:Z

    .line 14
    iget-object v0, p1, Lclu;->a:Landroid/content/Context;

    .line 15
    iput-object v0, p0, Lcqk;->a:Landroid/content/Context;

    .line 16
    iput-object p2, p0, Lcqk;->d:Landroid/telecom/PhoneAccountHandle;

    .line 17
    iput-object p3, p0, Lcqk;->b:Lcnw;

    .line 18
    iput-object p1, p0, Lcqk;->g:Lclu;

    .line 19
    invoke-direct {p0}, Lcqk;->c()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcqk;->e:Landroid/net/NetworkRequest;

    .line 20
    return-void
.end method

.method private final c()Landroid/net/NetworkRequest;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    const/16 v1, 0xc

    .line 22
    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v1

    .line 23
    iget-object v0, p0, Lcqk;->a:Landroid/content/Context;

    const-class v2, Landroid/telephony/TelephonyManager;

    .line 24
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcqk;->d:Landroid/telecom/PhoneAccountHandle;

    .line 25
    invoke-virtual {v0, v2}, Landroid/telephony/TelephonyManager;->createForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 26
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iget-object v2, p0, Lcqk;->g:Lclu;

    invoke-virtual {v2}, Lclu;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const/4 v2, 0x0

    .line 29
    invoke-virtual {v1, v2}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v2

    .line 30
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkSpecifier()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/NetworkRequest$Builder;->setNetworkSpecifier(Ljava/lang/String;)Landroid/net/NetworkRequest$Builder;

    .line 31
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    return-object v0
.end method

.method private d()Landroid/net/ConnectivityManager;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcqk;->f:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcqk;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    .line 55
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcqk;->f:Landroid/net/ConnectivityManager;

    .line 56
    :cond_0
    iget-object v0, p0, Lcqk;->f:Landroid/net/ConnectivityManager;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 41
    iget-boolean v0, p0, Lcqk;->h:Z

    if-ne v0, v1, :cond_0

    .line 42
    const-string v0, "VvmNetworkRequest"

    const-string v1, "requestNetwork() called twice"

    invoke-static {v0, v1}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :goto_0
    return-void

    .line 44
    :cond_0
    iput-boolean v1, p0, Lcqk;->h:Z

    .line 45
    invoke-direct {p0}, Lcqk;->d()Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcqk;->e:Landroid/net/NetworkRequest;

    .line 47
    invoke-virtual {v0, v1, p0}, Landroid/net/ConnectivityManager;->requestNetwork(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 49
    new-instance v1, Lcql;

    invoke-direct {v1, p0}, Lcql;-><init>(Lcqk;)V

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    const-string v0, "onFailed: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 58
    :goto_0
    iget-object v0, p0, Lcqk;->g:Lclu;

    invoke-virtual {v0}, Lclu;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcqk;->g:Lclu;

    iget-object v1, p0, Lcqk;->b:Lcnw;

    sget-object v2, Lclt;->k:Lclt;

    invoke-virtual {v0, v1, v2}, Lclu;->a(Lcnw;Lclt;)V

    .line 61
    :goto_1
    invoke-virtual {p0}, Lcqk;->b()V

    .line 62
    return-void

    .line 57
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcqk;->g:Lclu;

    iget-object v1, p0, Lcqk;->b:Lcnw;

    sget-object v2, Lclt;->l:Lclt;

    invoke-virtual {v0, v1, v2}, Lclu;->a(Lcnw;Lclt;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcqk;->d()Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 52
    return-void
.end method

.method public onAvailable(Landroid/net/Network;)V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqk;->c:Z

    .line 37
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqk;->c:Z

    .line 33
    const-string v0, "lost"

    invoke-virtual {p0, v0}, Lcqk;->a(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public onUnavailable()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqk;->c:Z

    .line 39
    const-string v0, "timeout"

    invoke-virtual {p0, v0}, Lcqk;->a(Ljava/lang/String;)V

    .line 40
    return-void
.end method
