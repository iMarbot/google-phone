.class public final Lcog;
.super Lcol;
.source "PG"


# instance fields
.field private f:[B


# direct methods
.method constructor <init>(Lcmy;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Lcol;-><init>()V

    .line 3
    iget v0, p1, Lcmy;->a:I

    .line 4
    new-array v0, v0, [B

    iput-object v0, p0, Lcog;->f:[B

    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    iget-object v1, p0, Lcog;->f:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 7
    iget-object v1, p0, Lcog;->f:[B

    iget-object v2, p0, Lcog;->f:[B

    array-length v2, v2

    sub-int/2addr v2, v0

    invoke-virtual {p1, v1, v0, v2}, Lcmy;->read([BII)I

    move-result v1

    .line 8
    if-ltz v1, :cond_0

    .line 9
    add-int/2addr v0, v1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v1, p0, Lcog;->f:[B

    array-length v1, v1

    if-eq v0, v1, :cond_1

    .line 12
    const-string v0, "ImapMemoryLiteral"

    const-string v1, "length mismatch"

    invoke-static {v0, v1}, Lcmd;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    :cond_1
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcog;->f:[B

    .line 15
    invoke-super {p0}, Lcol;->c()V

    .line 16
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 17
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcog;->f:[B

    const-string v2, "US-ASCII"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    const-string v1, "ImapMemoryLiteral"

    const-string v2, "Unsupported encoding: "

    invoke-static {v1, v2, v0}, Lcmd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcog;->f:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 22
    const-string v0, "{%d byte literal(memory)}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcog;->f:[B

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
