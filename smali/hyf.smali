.class public final Lhyf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhxx;


# instance fields
.field public final a:Lhyi;

.field public final b:I

.field public final c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lhyi;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    if-nez p3, :cond_0

    .line 3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4
    :cond_0
    iput-object p1, p0, Lhyf;->a:Lhyi;

    .line 5
    iput p2, p0, Lhyf;->b:I

    .line 6
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhyf;->c:Ljava/lang/String;

    .line 7
    iput-object p4, p0, Lhyf;->d:Ljava/lang/String;

    .line 8
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 9
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1, p2}, Lhyf;-><init>(Lhyi;ILjava/lang/String;Ljava/lang/String;)V

    .line 10
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lhyf;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 13
    iget-object v0, p0, Lhyf;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 14
    iget-object v0, p0, Lhyf;->d:Ljava/lang/String;

    .line 21
    :goto_0
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lhyf;->a:Lhyi;

    if-eqz v0, :cond_2

    .line 16
    iget-object v0, p0, Lhyf;->a:Lhyi;

    invoke-interface {v0}, Lhyi;->a()I

    move-result v1

    .line 17
    iget v0, p0, Lhyf;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 18
    add-int/lit8 v2, v0, 0x1

    if-le v1, v2, :cond_1

    iget-object v2, p0, Lhyf;->a:Lhyi;

    invoke-interface {v2, v0}, Lhyi;->b(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    invoke-static {v2}, Lhyj;->a(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    add-int/lit8 v0, v0, 0x1

    .line 20
    :cond_1
    iget-object v2, p0, Lhyf;->a:Lhyi;

    sub-int/2addr v1, v0

    invoke-static {v2, v0, v1}, Lhyp;->a(Lhyi;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhyl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 21
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lhyi;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lhyf;->a:Lhyi;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lhyf;->a:Lhyi;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lhyf;->a:Lhyi;

    invoke-static {v0}, Lhyp;->a(Lhyi;)Ljava/lang/String;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    iget-object v1, p0, Lhyf;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    iget-object v1, p0, Lhyf;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 28
    iget-object v1, p0, Lhyf;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
