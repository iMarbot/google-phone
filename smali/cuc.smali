.class public final enum Lcuc;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lcuc;

.field public static final enum b:Lcuc;

.field public static final enum c:Lcuc;

.field public static final enum d:Lcuc;

.field public static final enum e:Lcuc;

.field public static final enum f:Lcuc;

.field public static final enum g:Lcuc;

.field private static enum i:Lcuc;

.field private static synthetic j:[Lcuc;


# instance fields
.field public final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lcuc;

    const-string v1, "GIF"

    invoke-direct {v0, v1, v3, v4}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->a:Lcuc;

    .line 6
    new-instance v0, Lcuc;

    const-string v1, "JPEG"

    invoke-direct {v0, v1, v4, v3}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->b:Lcuc;

    .line 7
    new-instance v0, Lcuc;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v5, v3}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->i:Lcuc;

    .line 8
    new-instance v0, Lcuc;

    const-string v1, "PNG_A"

    invoke-direct {v0, v1, v6, v4}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->c:Lcuc;

    .line 9
    new-instance v0, Lcuc;

    const-string v1, "PNG"

    invoke-direct {v0, v1, v7, v3}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->d:Lcuc;

    .line 10
    new-instance v0, Lcuc;

    const-string v1, "WEBP_A"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->e:Lcuc;

    .line 11
    new-instance v0, Lcuc;

    const-string v1, "WEBP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->f:Lcuc;

    .line 12
    new-instance v0, Lcuc;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcuc;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcuc;->g:Lcuc;

    .line 13
    const/16 v0, 0x8

    new-array v0, v0, [Lcuc;

    sget-object v1, Lcuc;->a:Lcuc;

    aput-object v1, v0, v3

    sget-object v1, Lcuc;->b:Lcuc;

    aput-object v1, v0, v4

    sget-object v1, Lcuc;->i:Lcuc;

    aput-object v1, v0, v5

    sget-object v1, Lcuc;->c:Lcuc;

    aput-object v1, v0, v6

    sget-object v1, Lcuc;->d:Lcuc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcuc;->e:Lcuc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcuc;->f:Lcuc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcuc;->g:Lcuc;

    aput-object v2, v0, v1

    sput-object v0, Lcuc;->j:[Lcuc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-boolean p3, p0, Lcuc;->h:Z

    .line 4
    return-void
.end method

.method public static values()[Lcuc;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcuc;->j:[Lcuc;

    invoke-virtual {v0}, [Lcuc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcuc;

    return-object v0
.end method
