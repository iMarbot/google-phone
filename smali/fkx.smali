.class public Lfkx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfjz;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Lfkw;

.field public c:Ledj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ledj;Lfkw;)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lfkx;->a:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lfkx;->c:Ledj;

    .line 5
    iput-object p3, p0, Lfkx;->b:Lfkw;

    .line 6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ledj;Lfkw;B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfkx;-><init>(Landroid/content/Context;Ledj;Lfkw;)V

    .line 24
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lfkx;->c:Ledj;

    invoke-virtual {v0}, Ledj;->e()V

    .line 8
    return-void
.end method

.method public a(Lfka;)V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lfkx;->c:Ledj;

    iget-object v1, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfka;)Ledl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledj;->a(Ledl;)V

    .line 13
    return-void
.end method

.method public a(Lfkb;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lfkx;->c:Ledj;

    iget-object v1, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfkb;)Ledm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledj;->a(Ledm;)V

    .line 15
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lfkx;->c:Ledj;

    invoke-virtual {v0}, Ledj;->g()V

    .line 10
    return-void
.end method

.method public b(Lfka;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lfkx;->c:Ledj;

    iget-object v1, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfka;)Ledl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledj;->b(Ledl;)V

    .line 17
    iget-object v0, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v0, p1}, Lfkw;->b(Lfka;)V

    .line 18
    return-void
.end method

.method public b(Lfkb;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lfkx;->c:Ledj;

    iget-object v1, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v1, p1}, Lfkw;->a(Lfkb;)Ledm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledj;->b(Ledm;)V

    .line 20
    iget-object v0, p0, Lfkx;->b:Lfkw;

    invoke-virtual {v0, p1}, Lfkw;->b(Lfkb;)V

    .line 21
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lfkx;->c:Ledj;

    invoke-virtual {v0}, Ledj;->j()Z

    move-result v0

    return v0
.end method

.method public d()Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfkx;->a:Landroid/content/Context;

    return-object v0
.end method

.method public e()Ledj;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lfkx;->c:Ledj;

    return-object v0
.end method
