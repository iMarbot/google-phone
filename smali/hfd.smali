.class public enum Lhfd;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum a:Lhfd;

.field public static final enum b:Lhfd;

.field public static final enum c:Lhfd;

.field public static final enum d:Lhfd;

.field public static final enum e:Lhfd;

.field public static final enum f:Lhfd;

.field public static final enum g:Lhfd;

.field public static final enum h:Lhfd;

.field public static final enum i:Lhfd;

.field public static final enum j:Lhfd;

.field public static final enum k:Lhfd;

.field public static final enum l:Lhfd;

.field public static final enum m:Lhfd;

.field public static final enum n:Lhfd;

.field public static final enum o:Lhfd;

.field public static final enum p:Lhfd;

.field public static final enum q:Lhfd;

.field public static final enum r:Lhfd;

.field private static synthetic u:[Lhfd;


# instance fields
.field public final s:Lhfi;

.field public final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 8
    new-instance v0, Lhfd;

    const-string v1, "DOUBLE"

    sget-object v2, Lhfi;->d:Lhfi;

    invoke-direct {v0, v1, v4, v2, v5}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->a:Lhfd;

    .line 9
    new-instance v0, Lhfd;

    const-string v1, "FLOAT"

    sget-object v2, Lhfi;->c:Lhfi;

    invoke-direct {v0, v1, v5, v2, v7}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->b:Lhfd;

    .line 10
    new-instance v0, Lhfd;

    const-string v1, "INT64"

    sget-object v2, Lhfi;->b:Lhfi;

    invoke-direct {v0, v1, v6, v2, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->c:Lhfd;

    .line 11
    new-instance v0, Lhfd;

    const-string v1, "UINT64"

    sget-object v2, Lhfi;->b:Lhfi;

    invoke-direct {v0, v1, v8, v2, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->d:Lhfd;

    .line 12
    new-instance v0, Lhfd;

    const-string v1, "INT32"

    const/4 v2, 0x4

    sget-object v3, Lhfi;->a:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->e:Lhfd;

    .line 13
    new-instance v0, Lhfd;

    const-string v1, "FIXED64"

    sget-object v2, Lhfi;->b:Lhfi;

    invoke-direct {v0, v1, v7, v2, v5}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->f:Lhfd;

    .line 14
    new-instance v0, Lhfd;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    sget-object v3, Lhfi;->a:Lhfi;

    invoke-direct {v0, v1, v2, v3, v7}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->g:Lhfd;

    .line 15
    new-instance v0, Lhfd;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    sget-object v3, Lhfi;->e:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->h:Lhfd;

    .line 16
    new-instance v0, Lhfe;

    const-string v1, "STRING"

    const/16 v2, 0x8

    sget-object v3, Lhfi;->f:Lhfi;

    invoke-direct {v0, v1, v2, v3, v6}, Lhfe;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->i:Lhfd;

    .line 17
    new-instance v0, Lhff;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    sget-object v3, Lhfi;->i:Lhfi;

    invoke-direct {v0, v1, v2, v3, v8}, Lhff;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->j:Lhfd;

    .line 18
    new-instance v0, Lhfg;

    const-string v1, "MESSAGE"

    const/16 v2, 0xa

    sget-object v3, Lhfi;->i:Lhfi;

    invoke-direct {v0, v1, v2, v3, v6}, Lhfg;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->k:Lhfd;

    .line 19
    new-instance v0, Lhfh;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    sget-object v3, Lhfi;->g:Lhfi;

    invoke-direct {v0, v1, v2, v3, v6}, Lhfh;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->l:Lhfd;

    .line 20
    new-instance v0, Lhfd;

    const-string v1, "UINT32"

    const/16 v2, 0xc

    sget-object v3, Lhfi;->a:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->m:Lhfd;

    .line 21
    new-instance v0, Lhfd;

    const-string v1, "ENUM"

    const/16 v2, 0xd

    sget-object v3, Lhfi;->h:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->n:Lhfd;

    .line 22
    new-instance v0, Lhfd;

    const-string v1, "SFIXED32"

    const/16 v2, 0xe

    sget-object v3, Lhfi;->a:Lhfi;

    invoke-direct {v0, v1, v2, v3, v7}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->o:Lhfd;

    .line 23
    new-instance v0, Lhfd;

    const-string v1, "SFIXED64"

    const/16 v2, 0xf

    sget-object v3, Lhfi;->b:Lhfi;

    invoke-direct {v0, v1, v2, v3, v5}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->p:Lhfd;

    .line 24
    new-instance v0, Lhfd;

    const-string v1, "SINT32"

    const/16 v2, 0x10

    sget-object v3, Lhfi;->a:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->q:Lhfd;

    .line 25
    new-instance v0, Lhfd;

    const-string v1, "SINT64"

    const/16 v2, 0x11

    sget-object v3, Lhfi;->b:Lhfi;

    invoke-direct {v0, v1, v2, v3, v4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    sput-object v0, Lhfd;->r:Lhfd;

    .line 26
    const/16 v0, 0x12

    new-array v0, v0, [Lhfd;

    sget-object v1, Lhfd;->a:Lhfd;

    aput-object v1, v0, v4

    sget-object v1, Lhfd;->b:Lhfd;

    aput-object v1, v0, v5

    sget-object v1, Lhfd;->c:Lhfd;

    aput-object v1, v0, v6

    sget-object v1, Lhfd;->d:Lhfd;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lhfd;->e:Lhfd;

    aput-object v2, v0, v1

    sget-object v1, Lhfd;->f:Lhfd;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, Lhfd;->g:Lhfd;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhfd;->h:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhfd;->i:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhfd;->j:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhfd;->k:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhfd;->l:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhfd;->m:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhfd;->n:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lhfd;->o:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lhfd;->p:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lhfd;->q:Lhfd;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lhfd;->r:Lhfd;

    aput-object v2, v0, v1

    sput-object v0, Lhfd;->u:[Lhfd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILhfi;I)V
    .locals 0

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lhfd;->s:Lhfi;

    .line 4
    iput p4, p0, Lhfd;->t:I

    .line 5
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILhfi;IB)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2, p3, p4}, Lhfd;-><init>(Ljava/lang/String;ILhfi;I)V

    return-void
.end method

.method public static values()[Lhfd;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lhfd;->u:[Lhfd;

    invoke-virtual {v0}, [Lhfd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhfd;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x1

    return v0
.end method
