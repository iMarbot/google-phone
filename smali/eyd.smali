.class public final Leyd;
.super Lepr;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lcom/google/android/gms/maps/model/internal/zzz;

.field private b:Z

.field private c:F

.field private d:Z

.field private e:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leyx;

    invoke-direct {v0}, Leyx;-><init>()V

    sput-object v0, Leyd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lepr;-><init>()V

    iput-boolean v0, p0, Leyd;->b:Z

    iput-boolean v0, p0, Leyd;->d:Z

    const/4 v0, 0x0

    iput v0, p0, Leyd;->e:F

    return-void
.end method

.method constructor <init>(Landroid/os/IBinder;ZFZF)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lepr;-><init>()V

    iput-boolean v0, p0, Leyd;->b:Z

    iput-boolean v0, p0, Leyd;->d:Z

    const/4 v0, 0x0

    iput v0, p0, Leyd;->e:F

    invoke-static {p1}, Lcom/google/android/gms/maps/model/internal/zzaa;->zzbw(Landroid/os/IBinder;)Lcom/google/android/gms/maps/model/internal/zzz;

    move-result-object v0

    iput-object v0, p0, Leyd;->a:Lcom/google/android/gms/maps/model/internal/zzz;

    iget-object v0, p0, Leyd;->a:Lcom/google/android/gms/maps/model/internal/zzz;

    if-eqz v0, :cond_0

    new-instance v0, Leyw;

    invoke-direct {v0, p0}, Leyw;-><init>(Leyd;)V

    :cond_0
    iput-boolean p2, p0, Leyd;->b:Z

    iput p3, p0, Leyd;->c:F

    iput-boolean p4, p0, Leyd;->d:Z

    iput p5, p0, Leyd;->e:F

    return-void
.end method

.method static synthetic a(Leyd;)Lcom/google/android/gms/maps/model/internal/zzz;
    .locals 1

    iget-object v0, p0, Leyd;->a:Lcom/google/android/gms/maps/model/internal/zzz;

    return-object v0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1
    invoke-static {p1}, Letf;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x2

    iget-object v2, p0, Leyd;->a:Lcom/google/android/gms/maps/model/internal/zzz;

    invoke-interface {v2}, Lcom/google/android/gms/maps/model/internal/zzz;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;ILandroid/os/IBinder;)V

    const/4 v1, 0x3

    .line 2
    iget-boolean v2, p0, Leyd;->b:Z

    .line 3
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x4

    .line 4
    iget v2, p0, Leyd;->c:F

    .line 5
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    .line 6
    iget-boolean v2, p0, Leyd;->d:Z

    .line 7
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x6

    .line 8
    iget v2, p0, Leyd;->e:F

    .line 9
    invoke-static {p1, v1, v2}, Letf;->a(Landroid/os/Parcel;IF)V

    invoke-static {p1, v0}, Letf;->H(Landroid/os/Parcel;I)V

    return-void
.end method
