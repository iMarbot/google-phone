.class public final Lbby;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lbcj;


# direct methods
.method public constructor <init>(Lbcj;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lbby;->a:Lbcj;

    .line 3
    return-void
.end method

.method private final a(Ljava/util/List;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 104
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 105
    iget-object v0, p0, Lbby;->a:Lbcj;

    invoke-virtual {v0}, Lbcj;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcg;

    .line 106
    invoke-interface {v0, p1}, Lbcg;->a(Ljava/util/List;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_0

    .line 108
    :cond_0
    return-object v1
.end method

.method private static a(Landroid/content/ContentValues;Landroid/database/MatrixCursor;I)V
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v2

    .line 110
    const-string v0, "_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 111
    invoke-virtual {p0}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/String;Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_0

    .line 114
    :cond_0
    return-void
.end method

.method private static a(Lbmv;Landroid/content/ContentValues;Landroid/content/ContentValues;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 38
    const-string v0, "phone_account_component_name"

    .line 39
    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "phone_account_id"

    .line 40
    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-static {v0, v1}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 42
    const-string v1, "phone_account_component_name"

    .line 43
    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "phone_account_id"

    .line 44
    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-static {v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 46
    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    .line 103
    :goto_0
    return v0

    .line 48
    :cond_0
    :try_start_0
    const-string v0, "number"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 49
    const-string v1, "number"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 50
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    :cond_1
    move v0, v3

    .line 51
    goto :goto_0

    .line 53
    :cond_2
    sget-object v2, Lamg;->d:Lamg;

    invoke-static {v2, v0}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v0

    check-cast v0, Lamg;

    .line 56
    sget-object v2, Lamg;->d:Lamg;

    invoke-static {v2, v1}, Lhbr;->parseFrom(Lhbr;[B)Lhbr;

    move-result-object v1

    check-cast v1, Lamg;
    :try_end_0
    .catch Lhcf; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    iget v2, v0, Lamg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_3

    .line 66
    iget v2, v1, Lamg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_4

    move v2, v4

    .line 67
    :goto_1
    if-nez v2, :cond_5

    :cond_3
    move v0, v3

    .line 68
    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    const-string v1, "error parsing DialerPhoneNumber proto"

    .line 61
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    throw v2

    :cond_4
    move v2, v3

    .line 66
    goto :goto_1

    .line 70
    :cond_5
    invoke-static {}, Lbdf;->c()V

    .line 71
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lamg;

    .line 72
    iget v2, v2, Lamg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_6

    .line 74
    invoke-static {v1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lamg;

    .line 75
    iget v2, v2, Lamg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_7

    move v2, v4

    .line 76
    :goto_2
    if-nez v2, :cond_a

    .line 78
    :cond_6
    iget-object v2, v0, Lamg;->c:Lamg$a;

    if-nez v2, :cond_8

    .line 79
    sget-object v0, Lamg$a;->d:Lamg$a;

    .line 82
    :goto_3
    iget-object v2, v1, Lamg;->c:Lamg$a;

    if-nez v2, :cond_9

    .line 83
    sget-object v1, Lamg$a;->d:Lamg$a;

    .line 85
    :goto_4
    invoke-virtual {v0, v1}, Lamg$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_7
    move v2, v3

    .line 75
    goto :goto_2

    .line 80
    :cond_8
    iget-object v0, v0, Lamg;->c:Lamg$a;

    goto :goto_3

    .line 84
    :cond_9
    iget-object v1, v1, Lamg;->c:Lamg$a;

    goto :goto_4

    .line 88
    :cond_a
    iget-object v2, v0, Lamg;->b:Lame;

    if-nez v2, :cond_b

    .line 89
    sget-object v0, Lame;->j:Lame;

    .line 93
    :goto_5
    iget-object v2, v1, Lamg;->b:Lame;

    if-nez v2, :cond_c

    .line 94
    sget-object v1, Lame;->j:Lame;

    .line 97
    :goto_6
    invoke-static {}, Lbdf;->c()V

    .line 98
    iget-object v2, p0, Lbmv;->a:Lgxg;

    .line 99
    invoke-static {v0}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lame;

    invoke-static {v0}, Lbib;->a(Lame;)Lgxl;

    move-result-object v5

    .line 100
    invoke-static {v1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lame;

    invoke-static {v0}, Lbib;->a(Lame;)Lgxl;

    move-result-object v0

    .line 101
    invoke-virtual {v2, v5, v0}, Lgxg;->a(Lgxl;Lgxl;)Lgxg$a;

    move-result-object v0

    .line 102
    sget-object v1, Lgxg$a;->e:Lgxg$a;

    if-ne v0, v1, :cond_d

    move v0, v4

    goto/16 :goto_0

    .line 90
    :cond_b
    iget-object v0, v0, Lamg;->b:Lame;

    goto :goto_5

    .line 95
    :cond_c
    iget-object v1, v1, Lamg;->b:Lame;

    goto :goto_6

    :cond_d
    move v0, v3

    .line 103
    goto/16 :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 29
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 30
    invoke-interface {p0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    .line 31
    array-length v3, v2

    .line 32
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 33
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getType(I)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 34
    aget-object v4, v2, v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 36
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    aget-object v4, v2, v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 37
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 4
    invoke-static {}, Lbdf;->c()V

    .line 5
    new-instance v2, Lbmv;

    .line 6
    invoke-static {}, Lgxg;->a()Lgxg;

    move-result-object v0

    invoke-direct {v2, v0}, Lbmv;-><init>(Lgxg;)V

    .line 7
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v1, Lbce;->b:[Ljava/lang/String;

    .line 8
    invoke-static {p1}, Lbdf;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v3, v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 9
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    const/4 v0, 0x0

    .line 11
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 13
    :goto_0
    invoke-static {p1}, Lbby;->b(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v5

    .line 14
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 24
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    .line 25
    invoke-direct {p0, v4}, Lbby;->a(Ljava/util/List;)Landroid/content/ContentValues;

    move-result-object v1

    .line 26
    const-string v2, "number_calls"

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 27
    invoke-static {v1, v3, v0}, Lbby;->a(Landroid/content/ContentValues;Landroid/database/MatrixCursor;I)V

    .line 28
    :cond_0
    return-object v3

    .line 17
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 18
    invoke-static {v2, v0, v5}, Lbby;->a(Lbmv;Landroid/content/ContentValues;Landroid/content/ContentValues;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 19
    invoke-direct {p0, v4}, Lbby;->a(Ljava/util/List;)Landroid/content/ContentValues;

    move-result-object v6

    .line 20
    const-string v0, "number_calls"

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 21
    add-int/lit8 v0, v1, 0x1

    invoke-static {v6, v3, v1}, Lbby;->a(Landroid/content/ContentValues;Landroid/database/MatrixCursor;I)V

    .line 22
    invoke-interface {v4}, Ljava/util/List;->clear()V

    move v1, v0

    .line 23
    :cond_2
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method
