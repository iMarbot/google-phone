.class final Lio/grpc/internal/au;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private volatile b:Lhkj;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/au;->a:Ljava/util/ArrayList;

    .line 3
    sget-object v0, Lhkj;->d:Lhkj;

    iput-object v0, p0, Lio/grpc/internal/au;->b:Lhkj;

    return-void
.end method


# virtual methods
.method final a(Lhkj;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4
    const-string v0, "newState"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    invoke-virtual {p0}, Lio/grpc/internal/au;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "ConnectivityStateManager is already disabled"

    invoke-static {v0, v2}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 7
    iget-object v0, p0, Lio/grpc/internal/au;->b:Lhkj;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lio/grpc/internal/au;->b:Lhkj;

    sget-object v2, Lhkj;->e:Lhkj;

    if-eq v0, v2, :cond_1

    .line 8
    iput-object p1, p0, Lio/grpc/internal/au;->b:Lhkj;

    .line 9
    iget-object v0, p0, Lio/grpc/internal/au;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 10
    iget-object v0, p0, Lio/grpc/internal/au;->a:Ljava/util/ArrayList;

    .line 11
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lio/grpc/internal/au;->a:Ljava/util/ArrayList;

    .line 12
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 13
    new-instance v0, Ljava/lang/NoSuchMethodError;

    invoke-direct {v0}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    .line 5
    goto :goto_0

    .line 14
    :cond_1
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lio/grpc/internal/au;->b:Lhkj;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
