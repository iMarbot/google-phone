.class public final Lio/grpc/internal/dr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/util/List;

.field private synthetic b:Lhjs;

.field private synthetic c:Lhlo;


# direct methods
.method public constructor <init>(Lhlo;Ljava/util/List;Lhjs;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iput-object p2, p0, Lio/grpc/internal/dr;->a:Ljava/util/List;

    iput-object p3, p0, Lio/grpc/internal/dr;->b:Lhjs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2
    iget-object v0, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iget-object v0, v0, Lhlo;->b:Lhla;

    iget-object v1, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iget-object v1, v1, Lhlo;->c:Lio/grpc/internal/dh;

    .line 3
    iget-object v1, v1, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 4
    if-eq v0, v1, :cond_0

    .line 17
    :goto_0
    return-void

    .line 6
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iget-object v0, v0, Lhlo;->a:Lhky;

    iget-object v1, p0, Lio/grpc/internal/dr;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lhky;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8
    :catch_0
    move-exception v5

    .line 9
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl$NameResolverListenerImpl$1"

    const-string v3, "run"

    iget-object v4, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iget-object v4, v4, Lhlo;->c:Lio/grpc/internal/dh;

    .line 11
    iget-object v4, v4, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 12
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x29

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "["

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] Unexpected exception from LoadBalancer"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 13
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 14
    iget-object v0, p0, Lio/grpc/internal/dr;->c:Lhlo;

    iget-object v0, v0, Lhlo;->a:Lhky;

    sget-object v1, Lhlw;->h:Lhlw;

    invoke-virtual {v1, v5}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Thrown from handleResolvedAddresses(): "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 15
    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Lhky;->a(Lhlw;)V

    goto :goto_0
.end method
