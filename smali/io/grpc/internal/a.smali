.class public abstract Lio/grpc/internal/a;
.super Lio/grpc/internal/e;
.source "PG"

# interfaces
.implements Lio/grpc/internal/al;
.implements Lio/grpc/internal/ee;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/a$a;,
        Lio/grpc/internal/a$c;,
        Lio/grpc/internal/a$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;


# instance fields
.field public final b:Lio/grpc/internal/ce;

.field private c:Z

.field private d:Lhlh;

.field private e:Z

.field private volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lio/grpc/internal/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/a;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lio/grpc/internal/fg;Lio/grpc/internal/ez;Lhlh;Z)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lio/grpc/internal/e;-><init>()V

    .line 2
    const-string v0, "headers"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    iput-boolean p4, p0, Lio/grpc/internal/a;->c:Z

    .line 4
    if-nez p4, :cond_0

    .line 5
    new-instance v0, Lio/grpc/internal/eb;

    invoke-direct {v0, p0, p1, p2}, Lio/grpc/internal/eb;-><init>(Lio/grpc/internal/ee;Lio/grpc/internal/fg;Lio/grpc/internal/ez;)V

    iput-object v0, p0, Lio/grpc/internal/a;->b:Lio/grpc/internal/ce;

    .line 6
    iput-object p3, p0, Lio/grpc/internal/a;->d:Lhlh;

    .line 8
    :goto_0
    return-void

    .line 7
    :cond_0
    new-instance v0, Lio/grpc/internal/a$a;

    invoke-direct {v0, p0, p3, p2}, Lio/grpc/internal/a$a;-><init>(Lio/grpc/internal/a;Lhlh;Lio/grpc/internal/ez;)V

    iput-object v0, p0, Lio/grpc/internal/a;->b:Lio/grpc/internal/ce;

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Lio/grpc/internal/a$c;
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lio/grpc/internal/a;->b:Lio/grpc/internal/ce;

    invoke-interface {v0, p1}, Lio/grpc/internal/ce;->a(I)V

    .line 10
    return-void
.end method

.method public final a(Lhkp;)V
    .locals 3

    .prologue
    .line 18
    invoke-virtual {p0}, Lio/grpc/internal/a;->a()Lio/grpc/internal/a$c;

    move-result-object v1

    .line 20
    iget-object v0, v1, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Already called start"

    invoke-static {v0, v2}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 21
    const-string v0, "decompressorRegistry"

    .line 22
    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkp;

    iput-object v0, v1, Lio/grpc/internal/a$c;->c:Lhkp;

    .line 23
    return-void

    .line 20
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhlw;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 42
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Should not cancel with OK status"

    invoke-static {v0, v2}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 43
    iput-boolean v1, p0, Lio/grpc/internal/a;->f:Z

    .line 44
    invoke-virtual {p0}, Lio/grpc/internal/a;->b()Lio/grpc/internal/a$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/grpc/internal/a$b;->a(Lhlw;)V

    .line 45
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lio/grpc/internal/fb;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24
    invoke-virtual {p0}, Lio/grpc/internal/a;->a()Lio/grpc/internal/a$c;

    move-result-object v1

    .line 25
    iget-object v0, v1, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Already called setListener"

    invoke-static {v0, v2}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 26
    const-string v0, "listener"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/fb;

    iput-object v0, v1, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    .line 27
    iget-boolean v0, p0, Lio/grpc/internal/a;->c:Z

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lio/grpc/internal/a;->b()Lio/grpc/internal/a$b;

    move-result-object v0

    iget-object v1, p0, Lio/grpc/internal/a;->d:Lhlh;

    invoke-virtual {v0, v1, v3}, Lio/grpc/internal/a$b;->a(Lhlh;[B)V

    .line 29
    iput-object v3, p0, Lio/grpc/internal/a;->d:Lhlh;

    .line 30
    :cond_0
    return-void

    .line 25
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lio/grpc/internal/ff;ZZ)V
    .locals 2

    .prologue
    .line 34
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "null frame before EOS"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 35
    invoke-virtual {p0}, Lio/grpc/internal/a;->b()Lio/grpc/internal/a$b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lio/grpc/internal/a$b;->a(Lio/grpc/internal/ff;ZZ)V

    .line 36
    return-void

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lio/grpc/internal/a;->a()Lio/grpc/internal/a$c;

    move-result-object v0

    .line 16
    iput-boolean p1, v0, Lio/grpc/internal/a$c;->b:Z

    .line 17
    return-void
.end method

.method public abstract b()Lio/grpc/internal/a$b;
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lio/grpc/internal/a;->a()Lio/grpc/internal/a$c;

    move-result-object v0

    .line 12
    iget-object v0, v0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0, p1}, Lio/grpc/internal/ax;->a(I)V

    .line 13
    return-void
.end method

.method public final c()Lio/grpc/internal/ce;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lio/grpc/internal/a;->b:Lio/grpc/internal/ce;

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lio/grpc/internal/a;->b()Lio/grpc/internal/a$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/grpc/internal/a$b;->a(I)V

    .line 33
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lio/grpc/internal/a;->e:Z

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/a;->e:Z

    .line 40
    invoke-virtual {p0}, Lio/grpc/internal/e;->c()Lio/grpc/internal/ce;

    move-result-object v0

    invoke-interface {v0}, Lio/grpc/internal/ce;->c()V

    .line 41
    :cond_0
    return-void
.end method

.method public synthetic e()Lio/grpc/internal/f;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lio/grpc/internal/a;->a()Lio/grpc/internal/a$c;

    move-result-object v0

    return-object v0
.end method
