.class final Lio/grpc/internal/ct;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/fe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/ct$a;,
        Lio/grpc/internal/ct$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;


# instance fields
.field public final b:Lio/grpc/internal/dg;

.field public final c:Lio/grpc/internal/s;

.field public final d:Lio/grpc/internal/ct$a;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;

.field public final f:Ljava/lang/Object;

.field public final g:Lio/grpc/internal/ad;

.field public h:Lhks;

.field public i:I

.field public j:Lio/grpc/internal/r;

.field public final k:Lgts;

.field public l:Ljava/util/concurrent/ScheduledFuture;

.field public final m:Ljava/util/Collection;

.field public final n:Lio/grpc/internal/cs;

.field public o:Lio/grpc/internal/at;

.field public volatile p:Lio/grpc/internal/du;

.field public q:Lhkk;

.field public r:Lhlw;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lio/grpc/internal/ao;

.field private v:Lio/grpc/internal/ei;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const-class v0, Lio/grpc/internal/ct;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>(Lhks;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/s;Lio/grpc/internal/ao;Ljava/util/concurrent/ScheduledExecutorService;Lgtu;Lio/grpc/internal/ad;Lio/grpc/internal/ct$a;Lio/grpc/internal/ei;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/grpc/internal/dg;->a(Ljava/lang/String;)Lio/grpc/internal/dg;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/ct;->m:Ljava/util/Collection;

    .line 5
    new-instance v0, Lio/grpc/internal/cu;

    invoke-direct {v0, p0}, Lio/grpc/internal/cu;-><init>(Lio/grpc/internal/ct;)V

    iput-object v0, p0, Lio/grpc/internal/ct;->n:Lio/grpc/internal/cs;

    .line 6
    sget-object v0, Lhkj;->d:Lhkj;

    .line 7
    invoke-static {v0}, Lhkk;->a(Lhkj;)Lhkk;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 8
    const-string v0, "addressGroup"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhks;

    iput-object v0, p0, Lio/grpc/internal/ct;->h:Lhks;

    .line 9
    iput-object p2, p0, Lio/grpc/internal/ct;->s:Ljava/lang/String;

    .line 10
    iput-object p3, p0, Lio/grpc/internal/ct;->t:Ljava/lang/String;

    .line 11
    iput-object p4, p0, Lio/grpc/internal/ct;->c:Lio/grpc/internal/s;

    .line 12
    iput-object p5, p0, Lio/grpc/internal/ct;->u:Lio/grpc/internal/ao;

    .line 13
    iput-object p6, p0, Lio/grpc/internal/ct;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 14
    invoke-interface {p7}, Lgtu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgts;

    iput-object v0, p0, Lio/grpc/internal/ct;->k:Lgts;

    .line 15
    iput-object p8, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 16
    iput-object p9, p0, Lio/grpc/internal/ct;->d:Lio/grpc/internal/ct$a;

    .line 17
    iput-object p10, p0, Lio/grpc/internal/ct;->v:Lio/grpc/internal/ei;

    .line 18
    return-void
.end method


# virtual methods
.method public final B_()Lio/grpc/internal/dg;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    return-object v0
.end method

.method final a(Lhkj;)V
    .locals 1

    .prologue
    .line 62
    invoke-static {p1}, Lhkk;->a(Lhkj;)Lhkk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/grpc/internal/ct;->a(Lhkk;)V

    .line 63
    return-void
.end method

.method final a(Lhkk;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 65
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 67
    iget-object v1, p1, Lhkk;->a:Lhkj;

    .line 68
    if-eq v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 70
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 71
    sget-object v1, Lhkj;->e:Lhkj;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cannot transition out of SHUTDOWN to "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 72
    iput-object p1, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 73
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    new-instance v1, Lio/grpc/internal/cw;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/cw;-><init>(Lio/grpc/internal/ct;Lhkk;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 74
    :cond_0
    return-void

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhlw;)V
    .locals 9

    .prologue
    .line 75
    :try_start_0
    iget-object v6, p0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 76
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 77
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 78
    sget-object v1, Lhkj;->e:Lhkj;

    if-ne v0, v1, :cond_1

    .line 79
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    :try_start_2
    iput-object p1, p0, Lio/grpc/internal/ct;->r:Lhlw;

    .line 83
    sget-object v0, Lhkj;->e:Lhkj;

    invoke-virtual {p0, v0}, Lio/grpc/internal/ct;->a(Lhkj;)V

    .line 84
    iget-object v7, p0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 85
    iget-object v8, p0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lio/grpc/internal/ct;->i:I

    .line 89
    iget-object v0, p0, Lio/grpc/internal/ct;->m:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    invoke-virtual {p0}, Lio/grpc/internal/ct;->d()V

    .line 91
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel"

    const-string v3, "shutdown"

    const-string v4, "[{0}] Terminated in shutdown()"

    iget-object v5, p0, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    :cond_2
    iget-object v0, p0, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    .line 97
    :cond_3
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 98
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 101
    if-eqz v7, :cond_4

    .line 102
    invoke-interface {v7, p1}, Lio/grpc/internal/du;->a(Lhlw;)V

    .line 103
    :cond_4
    if-eqz v8, :cond_0

    .line 104
    invoke-interface {v8, p1}, Lio/grpc/internal/at;->a(Lhlw;)V

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 100
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0
.end method

.method final a(Lio/grpc/internal/at;Z)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    new-instance v1, Lio/grpc/internal/cy;

    invoke-direct {v1, p0, p1, p2}, Lio/grpc/internal/cy;-><init>(Lio/grpc/internal/ct;Lio/grpc/internal/at;Z)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 110
    return-void
.end method

.method final b()Lio/grpc/internal/am;
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 20
    if-eqz v0, :cond_0

    .line 37
    :goto_0
    return-object v0

    .line 22
    :cond_0
    :try_start_0
    iget-object v1, p0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 23
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 24
    if-eqz v0, :cond_1

    .line 25
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 26
    iget-object v1, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 28
    :cond_1
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 29
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 30
    sget-object v2, Lhkj;->d:Lhkj;

    if-ne v0, v2, :cond_2

    .line 31
    sget-object v0, Lhkj;->a:Lhkj;

    invoke-virtual {p0, v0}, Lio/grpc/internal/ct;->a(Lhkj;)V

    .line 32
    invoke-virtual {p0}, Lio/grpc/internal/ct;->c()V

    .line 33
    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 34
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 37
    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 36
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0
.end method

.method final c()V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 38
    iget-object v0, p0, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_3

    move v0, v7

    :goto_0
    const-string v1, "Should have no reconnectTask scheduled"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 39
    iget v0, p0, Lio/grpc/internal/ct;->i:I

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lio/grpc/internal/ct;->k:Lgts;

    .line 41
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lgts;->c:J

    .line 42
    iput-boolean v8, v0, Lgts;->b:Z

    .line 44
    invoke-virtual {v0}, Lgts;->a()Lgts;

    .line 45
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/ct;->h:Lhks;

    .line 46
    iget-object v0, v0, Lhks;->a:Ljava/util/List;

    .line 48
    iget v1, p0, Lio/grpc/internal/ct;->i:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/net/SocketAddress;

    .line 49
    iget-object v0, p0, Lio/grpc/internal/ct;->v:Lio/grpc/internal/ei;

    invoke-interface {v0, v6}, Lio/grpc/internal/ei;->a(Ljava/net/SocketAddress;)Lio/grpc/internal/em;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lio/grpc/internal/ct;->u:Lio/grpc/internal/ao;

    iget-object v2, p0, Lio/grpc/internal/ct;->s:Ljava/lang/String;

    iget-object v3, p0, Lio/grpc/internal/ct;->t:Ljava/lang/String;

    .line 51
    invoke-interface {v1, v6, v2, v3, v0}, Lio/grpc/internal/ao;->a(Ljava/net/SocketAddress;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/em;)Lio/grpc/internal/at;

    move-result-object v9

    .line 52
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel"

    const-string v3, "startNewTransport"

    const-string v4, "[{0}] Created {1} for {2}"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v10, p0, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    aput-object v10, v5, v8

    .line 54
    invoke-interface {v9}, Lio/grpc/internal/at;->B_()Lio/grpc/internal/dg;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    aput-object v6, v5, v7

    .line 55
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    :cond_1
    iput-object v9, p0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 57
    iget-object v0, p0, Lio/grpc/internal/ct;->m:Ljava/util/Collection;

    invoke-interface {v0, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v0, Lio/grpc/internal/ct$b;

    invoke-direct {v0, p0, v9, v6}, Lio/grpc/internal/ct$b;-><init>(Lio/grpc/internal/ct;Lio/grpc/internal/at;Ljava/net/SocketAddress;)V

    invoke-interface {v9, v0}, Lio/grpc/internal/at;->a(Lio/grpc/internal/dv;)Ljava/lang/Runnable;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_2

    .line 60
    iget-object v1, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1, v0}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 61
    :cond_2
    return-void

    :cond_3
    move v0, v8

    .line 38
    goto :goto_0
.end method

.method final d()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    new-instance v1, Lio/grpc/internal/cx;

    invoke-direct {v1, p0}, Lio/grpc/internal/cx;-><init>(Lio/grpc/internal/ct;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 107
    return-void
.end method
