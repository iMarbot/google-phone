.class final Lio/grpc/internal/a$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ce;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private a:Lhlh;

.field private b:Z

.field private c:Lio/grpc/internal/ez;

.field private d:[B

.field private synthetic e:Lio/grpc/internal/a;


# direct methods
.method public constructor <init>(Lio/grpc/internal/a;Lhlh;Lio/grpc/internal/ez;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/a$a;->e:Lio/grpc/internal/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "headers"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlh;

    iput-object v0, p0, Lio/grpc/internal/a$a;->a:Lhlh;

    .line 3
    const-string v0, "statsTraceCtx"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ez;

    iput-object v0, p0, Lio/grpc/internal/a$a;->c:Lio/grpc/internal/ez;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lhkh;)Lio/grpc/internal/ce;
    .locals 0

    .prologue
    .line 23
    return-object p0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 15
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public final a(Ljava/io/InputStream;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5
    iget-object v0, p0, Lio/grpc/internal/a$a;->d:[B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "writePayload should not be called multiple times"

    invoke-static {v0, v2}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 6
    :try_start_0
    invoke-static {p1}, Lio/grpc/internal/av;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/a$a;->d:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    iget-object v0, p0, Lio/grpc/internal/a$a;->c:Lio/grpc/internal/ez;

    invoke-virtual {v0, v1}, Lio/grpc/internal/ez;->a(I)V

    .line 11
    iget-object v0, p0, Lio/grpc/internal/a$a;->c:Lio/grpc/internal/ez;

    iget-object v2, p0, Lio/grpc/internal/a$a;->d:[B

    array-length v2, v2

    int-to-long v2, v2

    iget-object v4, p0, Lio/grpc/internal/a$a;->d:[B

    array-length v4, v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lio/grpc/internal/ez;->a(IJJ)V

    .line 12
    iget-object v0, p0, Lio/grpc/internal/a$a;->c:Lio/grpc/internal/ez;

    iget-object v1, p0, Lio/grpc/internal/a$a;->d:[B

    array-length v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->a(J)V

    .line 13
    iget-object v0, p0, Lio/grpc/internal/a$a;->c:Lio/grpc/internal/ez;

    iget-object v1, p0, Lio/grpc/internal/a$a;->d:[B

    array-length v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->b(J)V

    .line 14
    return-void

    :cond_0
    move v0, v1

    .line 5
    goto :goto_0

    .line 8
    :catch_0
    move-exception v0

    .line 9
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lio/grpc/internal/a$a;->b:Z

    return v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lio/grpc/internal/a$a;->b:Z

    .line 18
    iget-object v1, p0, Lio/grpc/internal/a$a;->d:[B

    if-eqz v1, :cond_0

    :goto_0
    const-string v1, "Lack of request message. GET request is only supported for unary requests"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lio/grpc/internal/a$a;->e:Lio/grpc/internal/a;

    invoke-virtual {v0}, Lio/grpc/internal/a;->b()Lio/grpc/internal/a$b;

    move-result-object v0

    iget-object v1, p0, Lio/grpc/internal/a$a;->a:Lhlh;

    iget-object v2, p0, Lio/grpc/internal/a$a;->d:[B

    invoke-virtual {v0, v1, v2}, Lio/grpc/internal/a$b;->a(Lhlh;[B)V

    .line 20
    iput-object v3, p0, Lio/grpc/internal/a$a;->d:[B

    .line 21
    iput-object v3, p0, Lio/grpc/internal/a$a;->a:Lhlh;

    .line 22
    return-void

    .line 18
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
