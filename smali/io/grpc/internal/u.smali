.class final Lio/grpc/internal/u;
.super Lio/grpc/internal/cd;
.source "PG"


# instance fields
.field private a:Lio/grpc/internal/at;

.field private b:Ljava/lang/String;

.field private synthetic c:Lio/grpc/internal/t;


# direct methods
.method constructor <init>(Lio/grpc/internal/t;Lio/grpc/internal/at;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/u;->c:Lio/grpc/internal/t;

    invoke-direct {p0}, Lio/grpc/internal/cd;-><init>()V

    .line 2
    const-string v0, "delegate"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/at;

    iput-object v0, p0, Lio/grpc/internal/u;->a:Lio/grpc/internal/at;

    .line 3
    const-string v0, "authority"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/u;->b:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    .locals 5

    .prologue
    .line 6
    .line 7
    iget-object v0, p3, Lhjv;->e:Lhjt;

    .line 9
    if-eqz v0, :cond_1

    .line 10
    new-instance v1, Lhju;

    iget-object v2, p0, Lio/grpc/internal/u;->a:Lio/grpc/internal/at;

    invoke-direct {v1, v2, p1, p2, p3}, Lhju;-><init>(Lio/grpc/internal/am;Lhlk;Lhlh;Lhjv;)V

    .line 11
    invoke-static {}, Lhjs;->a()Lhjs$a;

    move-result-object v2

    sget-object v3, Lhjt;->b:Lhjs$b;

    iget-object v4, p0, Lio/grpc/internal/u;->b:Ljava/lang/String;

    .line 12
    invoke-virtual {v2, v3, v4}, Lhjs$a;->a(Lhjs$b;Ljava/lang/Object;)Lhjs$a;

    move-result-object v2

    sget-object v3, Lhjt;->a:Lhjs$b;

    sget-object v4, Lhlu;->a:Lhlu;

    .line 13
    invoke-virtual {v2, v3, v4}, Lhjs$a;->a(Lhjs$b;Ljava/lang/Object;)Lhjs$a;

    move-result-object v2

    iget-object v3, p0, Lio/grpc/internal/u;->a:Lio/grpc/internal/at;

    .line 14
    invoke-interface {v3}, Lio/grpc/internal/at;->b()Lhjs;

    move-result-object v3

    .line 16
    iget-object v4, v3, Lhjs;->a:Ljava/util/Map;

    .line 17
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Lhjs$a;->a(I)Ljava/util/Map;

    move-result-object v4

    .line 18
    iget-object v3, v3, Lhjs;->a:Ljava/util/Map;

    .line 19
    invoke-interface {v4, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 23
    iget-object v3, p3, Lhjv;->d:Ljava/lang/String;

    .line 24
    if-eqz v3, :cond_0

    .line 25
    sget-object v3, Lhjt;->b:Lhjs$b;

    .line 26
    iget-object v4, p3, Lhjv;->d:Ljava/lang/String;

    .line 27
    invoke-virtual {v2, v3, v4}, Lhjs$a;->a(Lhjs$b;Ljava/lang/Object;)Lhjs$a;

    .line 28
    :cond_0
    invoke-virtual {v2}, Lhjs$a;->a()Lhjs;

    .line 30
    iget-object v2, p3, Lhjv;->c:Ljava/util/concurrent/Executor;

    .line 31
    iget-object v3, p0, Lio/grpc/internal/u;->c:Lio/grpc/internal/t;

    .line 32
    iget-object v3, v3, Lio/grpc/internal/t;->a:Ljava/util/concurrent/Executor;

    .line 33
    invoke-static {v2, v3}, Lhcw;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-interface {v0}, Lhjt;->a()V

    .line 35
    invoke-virtual {v1}, Lhju;->a()Lio/grpc/internal/al;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lio/grpc/internal/u;->a:Lio/grpc/internal/at;

    invoke-interface {v0, p1, p2, p3}, Lio/grpc/internal/at;->a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a()Lio/grpc/internal/at;
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lio/grpc/internal/u;->a:Lio/grpc/internal/at;

    return-object v0
.end method
