.class public final Lio/grpc/internal/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ax;
.implements Lio/grpc/internal/dx;


# instance fields
.field public final a:Lio/grpc/internal/dx;

.field public final b:Lio/grpc/internal/dw;

.field public final c:Ljava/util/Queue;

.field private d:Lio/grpc/internal/p;


# direct methods
.method constructor <init>(Lio/grpc/internal/dx;Lio/grpc/internal/p;Lio/grpc/internal/dw;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/g;->c:Ljava/util/Queue;

    .line 3
    const-string v0, "listener"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/dx;

    iput-object v0, p0, Lio/grpc/internal/g;->a:Lio/grpc/internal/dx;

    .line 4
    const-string v0, "transportExecutor"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/p;

    iput-object v0, p0, Lio/grpc/internal/g;->d:Lio/grpc/internal/p;

    .line 6
    iput-object p0, p3, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    .line 7
    iput-object p3, p0, Lio/grpc/internal/g;->b:Lio/grpc/internal/dw;

    .line 8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lio/grpc/internal/g;->a:Lio/grpc/internal/dx;

    new-instance v1, Lio/grpc/internal/o;

    new-instance v2, Lio/grpc/internal/j;

    invoke-direct {v2, p0}, Lio/grpc/internal/j;-><init>(Lio/grpc/internal/g;)V

    .line 25
    invoke-direct {v1, p0, v2}, Lio/grpc/internal/o;-><init>(Lio/grpc/internal/g;Ljava/lang/Runnable;)V

    .line 26
    invoke-interface {v0, v1}, Lio/grpc/internal/dx;->a(Lio/grpc/internal/fc;)V

    .line 27
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lio/grpc/internal/g;->b:Lio/grpc/internal/dw;

    .line 10
    iput p1, v0, Lio/grpc/internal/dw;->b:I

    .line 11
    return-void
.end method

.method public final a(Lhko;)V
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lio/grpc/internal/g;->b:Lio/grpc/internal/dw;

    invoke-virtual {v0, p1}, Lio/grpc/internal/dw;->a(Lhko;)V

    .line 13
    return-void
.end method

.method public final a(Lio/grpc/internal/ck;)V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lio/grpc/internal/g;->b:Lio/grpc/internal/dw;

    invoke-virtual {v0, p1}, Lio/grpc/internal/dw;->a(Lio/grpc/internal/ck;)V

    .line 15
    return-void
.end method

.method public final a(Lio/grpc/internal/en;)V
    .locals 3

    .prologue
    .line 20
    iget-object v0, p0, Lio/grpc/internal/g;->a:Lio/grpc/internal/dx;

    new-instance v1, Lio/grpc/internal/o;

    new-instance v2, Lio/grpc/internal/i;

    invoke-direct {v2, p0, p1}, Lio/grpc/internal/i;-><init>(Lio/grpc/internal/g;Lio/grpc/internal/en;)V

    .line 21
    invoke-direct {v1, p0, v2}, Lio/grpc/internal/o;-><init>(Lio/grpc/internal/g;Ljava/lang/Runnable;)V

    .line 22
    invoke-interface {v0, v1}, Lio/grpc/internal/dx;->a(Lio/grpc/internal/fc;)V

    .line 23
    return-void
.end method

.method public final a(Lio/grpc/internal/fc;)V
    .locals 2

    .prologue
    .line 36
    :goto_0
    invoke-interface {p1}, Lio/grpc/internal/fc;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lio/grpc/internal/g;->c:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lio/grpc/internal/g;->d:Lio/grpc/internal/p;

    new-instance v1, Lio/grpc/internal/n;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/n;-><init>(Lio/grpc/internal/g;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lio/grpc/internal/p;->a(Ljava/lang/Runnable;)V

    .line 42
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lio/grpc/internal/g;->d:Lio/grpc/internal/p;

    new-instance v1, Lio/grpc/internal/m;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/m;-><init>(Lio/grpc/internal/g;Z)V

    invoke-interface {v0, v1}, Lio/grpc/internal/p;->a(Ljava/lang/Runnable;)V

    .line 40
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 16
    iget-object v0, p0, Lio/grpc/internal/g;->a:Lio/grpc/internal/dx;

    new-instance v1, Lio/grpc/internal/o;

    new-instance v2, Lio/grpc/internal/h;

    invoke-direct {v2, p0, p1}, Lio/grpc/internal/h;-><init>(Lio/grpc/internal/g;I)V

    .line 17
    invoke-direct {v1, p0, v2}, Lio/grpc/internal/o;-><init>(Lio/grpc/internal/g;Ljava/lang/Runnable;)V

    .line 18
    invoke-interface {v0, v1}, Lio/grpc/internal/dx;->a(Lio/grpc/internal/fc;)V

    .line 19
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lio/grpc/internal/g;->d:Lio/grpc/internal/p;

    new-instance v1, Lio/grpc/internal/l;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/l;-><init>(Lio/grpc/internal/g;I)V

    invoke-interface {v0, v1}, Lio/grpc/internal/p;->a(Ljava/lang/Runnable;)V

    .line 35
    return-void
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lio/grpc/internal/g;->b:Lio/grpc/internal/dw;

    .line 29
    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/grpc/internal/dw;->c:Z

    .line 30
    iget-object v0, p0, Lio/grpc/internal/g;->a:Lio/grpc/internal/dx;

    new-instance v1, Lio/grpc/internal/o;

    new-instance v2, Lio/grpc/internal/k;

    invoke-direct {v2, p0}, Lio/grpc/internal/k;-><init>(Lio/grpc/internal/g;)V

    .line 31
    invoke-direct {v1, p0, v2}, Lio/grpc/internal/o;-><init>(Lio/grpc/internal/g;Ljava/lang/Runnable;)V

    .line 32
    invoke-interface {v0, v1}, Lio/grpc/internal/dx;->a(Lio/grpc/internal/fc;)V

    .line 33
    return-void
.end method
