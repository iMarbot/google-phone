.class final Lio/grpc/internal/di;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/dv;


# instance fields
.field private synthetic a:Lio/grpc/internal/dh;


# direct methods
.method constructor <init>(Lio/grpc/internal/dh;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 6
    return-void
.end method

.method public final a(Lhlw;)V
    .locals 2

    .prologue
    .line 2
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 3
    iget-object v0, v0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 4
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "Channel must have been shut down"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 5
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 7
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    iget-object v0, v0, Lio/grpc/internal/dh;->F:Lio/grpc/internal/cs;

    iget-object v1, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 8
    iget-object v1, v1, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    .line 9
    invoke-virtual {v0, v1, p1}, Lio/grpc/internal/cs;->a(Ljava/lang/Object;Z)V

    .line 10
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 11
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 12
    iget-object v0, v0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 13
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "Channel must have been shut down"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 15
    iput-boolean v7, v0, Lio/grpc/internal/dh;->B:Z

    .line 17
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 18
    iget-object v0, v0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 19
    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 21
    iget-object v0, v0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 22
    iget-object v0, v0, Lio/grpc/internal/dh$b;->a:Lhky;

    invoke-virtual {v0}, Lhky;->a()V

    .line 23
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 24
    iput-object v2, v0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 26
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 27
    iget-object v0, v0, Lio/grpc/internal/dh;->t:Lhlm;

    .line 28
    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 30
    iget-object v0, v0, Lio/grpc/internal/dh;->t:Lhlm;

    .line 31
    invoke-virtual {v0}, Lhlm;->b()V

    .line 32
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 33
    iput-object v2, v0, Lio/grpc/internal/dh;->t:Lhlm;

    .line 35
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 37
    iget-object v6, p0, Lio/grpc/internal/di;->a:Lio/grpc/internal/dh;

    .line 39
    iget-boolean v0, v6, Lio/grpc/internal/dh;->C:Z

    if-nez v0, :cond_2

    .line 40
    iget-object v0, v6, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v6, Lio/grpc/internal/dh;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v6, Lio/grpc/internal/dh;->y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 41
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "maybeTerminateChannel"

    const-string v4, "[{0}] Terminated"

    .line 42
    iget-object v5, v6, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 43
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iput-boolean v7, v6, Lio/grpc/internal/dh;->C:Z

    .line 45
    iget-object v0, v6, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    .line 46
    iput-boolean v7, v0, Lio/grpc/internal/dh$c;->b:Z

    .line 48
    iget-object v0, v6, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    invoke-virtual {v0}, Lio/grpc/internal/dh$c;->clear()V

    .line 49
    iget-object v0, v6, Lio/grpc/internal/dh;->D:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 50
    iget-object v0, v6, Lio/grpc/internal/dh;->i:Lio/grpc/internal/eg;

    iget-object v1, v6, Lio/grpc/internal/dh;->h:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1}, Lio/grpc/internal/eg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, v6, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    invoke-interface {v0}, Lio/grpc/internal/ao;->close()V

    .line 52
    :cond_2
    return-void
.end method
