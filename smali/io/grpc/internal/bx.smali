.class final Lio/grpc/internal/bx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lio/grpc/internal/bw;


# direct methods
.method constructor <init>(Lio/grpc/internal/bw;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 3
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 4
    iget-object v0, v0, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 5
    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 7
    iget-object v0, v0, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 8
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 9
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v2, 0x0

    .line 10
    iput-object v2, v0, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 12
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 13
    iget-boolean v0, v0, Lio/grpc/internal/bw;->f:Z

    .line 14
    if-eqz v0, :cond_1

    .line 15
    monitor-exit v1

    .line 89
    :goto_0
    return-void

    .line 16
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 17
    iget-object v2, v0, Lio/grpc/internal/bw;->k:Lhlo;

    .line 19
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v3, 0x1

    .line 20
    iput-boolean v3, v0, Lio/grpc/internal/bw;->j:Z

    .line 22
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 23
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 24
    iget-object v0, v0, Lio/grpc/internal/bw;->c:Ljava/lang/String;

    .line 25
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 26
    iget v1, v1, Lio/grpc/internal/bw;->d:I

    .line 27
    invoke-static {v0, v1}, Ljava/net/InetSocketAddress;->createUnresolved(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 29
    iget-object v1, v1, Lio/grpc/internal/bw;->e:Lio/grpc/internal/ei;

    .line 30
    invoke-interface {v1, v0}, Lio/grpc/internal/ei;->a(Ljava/net/SocketAddress;)Lio/grpc/internal/em;

    move-result-object v1

    .line 31
    if-eqz v1, :cond_2

    .line 32
    new-instance v1, Lhks;

    invoke-direct {v1, v0}, Lhks;-><init>(Ljava/net/SocketAddress;)V

    .line 33
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lhjs;->b:Lhjs;

    invoke-virtual {v2, v0, v1}, Lhlo;->a(Ljava/util/List;Lhjs;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 34
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 35
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v2, 0x0

    .line 36
    iput-boolean v2, v0, Lio/grpc/internal/bw;->j:Z

    .line 38
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 22
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 39
    :cond_2
    :try_start_4
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 40
    iget-object v0, v0, Lio/grpc/internal/bw;->b:Lio/grpc/internal/bw$a;

    .line 41
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 42
    iget-object v1, v1, Lio/grpc/internal/bw;->c:Ljava/lang/String;

    .line 43
    invoke-virtual {v0, v1}, Lio/grpc/internal/bw$a;->a(Ljava/lang/String;)Lio/grpc/internal/bw$c;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v0

    .line 72
    :try_start_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    iget-object v0, v0, Lio/grpc/internal/bw$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 74
    new-instance v4, Lhks;

    new-instance v5, Ljava/net/InetSocketAddress;

    iget-object v6, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 75
    iget v6, v6, Lio/grpc/internal/bw;->d:I

    .line 76
    invoke-direct {v5, v0, v6}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-direct {v4, v5}, Lhks;-><init>(Ljava/net/SocketAddress;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    .line 84
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 85
    :try_start_6
    iget-object v2, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v3, 0x0

    .line 86
    iput-boolean v3, v2, Lio/grpc/internal/bw;->j:Z

    .line 88
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    throw v0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    :try_start_7
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 47
    :try_start_8
    iget-object v3, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 48
    iget-boolean v3, v3, Lio/grpc/internal/bw;->f:Z

    .line 49
    if-eqz v3, :cond_3

    .line 50
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    .line 51
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 52
    :try_start_9
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v2, 0x0

    .line 53
    iput-boolean v2, v0, Lio/grpc/internal/bw;->j:Z

    .line 55
    monitor-exit v1

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 56
    :cond_3
    :try_start_a
    iget-object v3, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    iget-object v4, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 58
    iget-object v4, v4, Lio/grpc/internal/bw;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 59
    new-instance v5, Lio/grpc/internal/df;

    iget-object v6, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    .line 60
    iget-object v6, v6, Lio/grpc/internal/bw;->m:Ljava/lang/Runnable;

    .line 61
    invoke-direct {v5, v6}, Lio/grpc/internal/df;-><init>(Ljava/lang/Runnable;)V

    const-wide/16 v6, 0x1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v4

    .line 63
    iput-object v4, v3, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    .line 65
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 66
    :try_start_b
    sget-object v1, Lhlw;->i:Lhlw;

    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    invoke-virtual {v2, v0}, Lhlo;->a(Lhlw;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 67
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 68
    :try_start_c
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v2, 0x0

    .line 69
    iput-boolean v2, v0, Lio/grpc/internal/bw;->j:Z

    .line 71
    monitor-exit v1

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v0

    .line 65
    :catchall_5
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :try_start_e
    throw v0

    .line 78
    :cond_4
    sget-object v0, Lhjs;->b:Lhjs;

    invoke-virtual {v2, v1, v0}, Lhlo;->a(Ljava/util/List;Lhjs;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 79
    iget-object v1, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    monitor-enter v1

    .line 80
    :try_start_f
    iget-object v0, p0, Lio/grpc/internal/bx;->a:Lio/grpc/internal/bw;

    const/4 v2, 0x0

    .line 81
    iput-boolean v2, v0, Lio/grpc/internal/bw;->j:Z

    .line 83
    monitor-exit v1

    goto/16 :goto_0

    :catchall_6
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v0

    .line 88
    :catchall_7
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    throw v0
.end method
