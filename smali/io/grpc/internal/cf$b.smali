.class public final enum Lio/grpc/internal/cf$b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/cf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation


# static fields
.field private static enum a:Lio/grpc/internal/cf$b;

.field private static enum b:Lio/grpc/internal/cf$b;

.field private static enum c:Lio/grpc/internal/cf$b;

.field private static enum d:Lio/grpc/internal/cf$b;

.field private static enum e:Lio/grpc/internal/cf$b;

.field private static enum f:Lio/grpc/internal/cf$b;

.field private static enum g:Lio/grpc/internal/cf$b;

.field private static enum h:Lio/grpc/internal/cf$b;

.field private static enum i:Lio/grpc/internal/cf$b;

.field private static enum j:Lio/grpc/internal/cf$b;

.field private static enum k:Lio/grpc/internal/cf$b;

.field private static enum l:Lio/grpc/internal/cf$b;

.field private static enum m:Lio/grpc/internal/cf$b;

.field private static enum n:Lio/grpc/internal/cf$b;

.field private static o:[Lio/grpc/internal/cf$b;

.field private static synthetic r:[Lio/grpc/internal/cf$b;


# instance fields
.field private p:I

.field private q:Lhlw;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 25
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "NO_ERROR"

    sget-object v3, Lhlw;->i:Lhlw;

    invoke-direct {v1, v2, v0, v0, v3}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->a:Lio/grpc/internal/cf$b;

    .line 26
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "PROTOCOL_ERROR"

    sget-object v3, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v7, v7, v3}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->b:Lio/grpc/internal/cf$b;

    .line 27
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "INTERNAL_ERROR"

    sget-object v3, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v8, v8, v3}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->c:Lio/grpc/internal/cf$b;

    .line 28
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "FLOW_CONTROL_ERROR"

    sget-object v3, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v9, v9, v3}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->d:Lio/grpc/internal/cf$b;

    .line 29
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "SETTINGS_TIMEOUT"

    sget-object v3, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v10, v10, v3}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->e:Lio/grpc/internal/cf$b;

    .line 30
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "STREAM_CLOSED"

    const/4 v3, 0x5

    const/4 v4, 0x5

    sget-object v5, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->f:Lio/grpc/internal/cf$b;

    .line 31
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "FRAME_SIZE_ERROR"

    const/4 v3, 0x6

    const/4 v4, 0x6

    sget-object v5, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->g:Lio/grpc/internal/cf$b;

    .line 32
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "REFUSED_STREAM"

    const/4 v3, 0x7

    const/4 v4, 0x7

    sget-object v5, Lhlw;->i:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->h:Lio/grpc/internal/cf$b;

    .line 33
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "CANCEL"

    const/16 v3, 0x8

    const/16 v4, 0x8

    sget-object v5, Lhlw;->c:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->i:Lio/grpc/internal/cf$b;

    .line 34
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "COMPRESSION_ERROR"

    const/16 v3, 0x9

    const/16 v4, 0x9

    sget-object v5, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->j:Lio/grpc/internal/cf$b;

    .line 35
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "CONNECT_ERROR"

    const/16 v3, 0xa

    const/16 v4, 0xa

    sget-object v5, Lhlw;->h:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->k:Lio/grpc/internal/cf$b;

    .line 36
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "ENHANCE_YOUR_CALM"

    const/16 v3, 0xb

    const/16 v4, 0xb

    sget-object v5, Lhlw;->g:Lhlw;

    const-string v6, "Bandwidth exhausted"

    invoke-virtual {v5, v6}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->l:Lio/grpc/internal/cf$b;

    .line 37
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "INADEQUATE_SECURITY"

    const/16 v3, 0xc

    const/16 v4, 0xc

    sget-object v5, Lhlw;->f:Lhlw;

    const-string v6, "Permission denied as protocol is not secure enough to call"

    invoke-virtual {v5, v6}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->m:Lio/grpc/internal/cf$b;

    .line 38
    new-instance v1, Lio/grpc/internal/cf$b;

    const-string v2, "HTTP_1_1_REQUIRED"

    const/16 v3, 0xd

    const/16 v4, 0xd

    sget-object v5, Lhlw;->d:Lhlw;

    invoke-direct {v1, v2, v3, v4, v5}, Lio/grpc/internal/cf$b;-><init>(Ljava/lang/String;IILhlw;)V

    sput-object v1, Lio/grpc/internal/cf$b;->n:Lio/grpc/internal/cf$b;

    .line 39
    const/16 v1, 0xe

    new-array v1, v1, [Lio/grpc/internal/cf$b;

    sget-object v2, Lio/grpc/internal/cf$b;->a:Lio/grpc/internal/cf$b;

    aput-object v2, v1, v0

    sget-object v2, Lio/grpc/internal/cf$b;->b:Lio/grpc/internal/cf$b;

    aput-object v2, v1, v7

    sget-object v2, Lio/grpc/internal/cf$b;->c:Lio/grpc/internal/cf$b;

    aput-object v2, v1, v8

    sget-object v2, Lio/grpc/internal/cf$b;->d:Lio/grpc/internal/cf$b;

    aput-object v2, v1, v9

    sget-object v2, Lio/grpc/internal/cf$b;->e:Lio/grpc/internal/cf$b;

    aput-object v2, v1, v10

    const/4 v2, 0x5

    sget-object v3, Lio/grpc/internal/cf$b;->f:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lio/grpc/internal/cf$b;->g:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lio/grpc/internal/cf$b;->h:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lio/grpc/internal/cf$b;->i:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lio/grpc/internal/cf$b;->j:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lio/grpc/internal/cf$b;->k:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lio/grpc/internal/cf$b;->l:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lio/grpc/internal/cf$b;->m:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lio/grpc/internal/cf$b;->n:Lio/grpc/internal/cf$b;

    aput-object v3, v1, v2

    sput-object v1, Lio/grpc/internal/cf$b;->r:[Lio/grpc/internal/cf$b;

    .line 40
    invoke-static {}, Lio/grpc/internal/cf$b;->values()[Lio/grpc/internal/cf$b;

    move-result-object v1

    .line 41
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    .line 42
    iget v2, v2, Lio/grpc/internal/cf$b;->p:I

    .line 43
    add-int/lit8 v2, v2, 0x1

    .line 44
    new-array v2, v2, [Lio/grpc/internal/cf$b;

    .line 45
    array-length v3, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 47
    iget v5, v4, Lio/grpc/internal/cf$b;->p:I

    .line 49
    aput-object v4, v2, v5

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_0
    sput-object v2, Lio/grpc/internal/cf$b;->o:[Lio/grpc/internal/cf$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILhlw;)V
    .locals 3

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput p3, p0, Lio/grpc/internal/cf$b;->p:I

    .line 4
    const-string v1, "HTTP/2 error code: "

    invoke-virtual {p0}, Lio/grpc/internal/cf$b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p4, v0}, Lhlw;->b(Ljava/lang/String;)Lhlw;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/cf$b;->q:Lhlw;

    .line 5
    return-void

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(J)Lhlw;
    .locals 4

    .prologue
    .line 6
    .line 7
    sget-object v0, Lio/grpc/internal/cf$b;->o:[Lio/grpc/internal/cf$b;

    array-length v0, v0

    int-to-long v0, v0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_2

    .line 12
    sget-object v0, Lio/grpc/internal/cf$b;->c:Lio/grpc/internal/cf$b;

    .line 13
    iget-object v0, v0, Lio/grpc/internal/cf$b;->q:Lhlw;

    .line 15
    iget-object v0, v0, Lhlw;->l:Lhlx;

    .line 18
    iget v0, v0, Lhlx;->r:I

    .line 19
    invoke-static {v0}, Lhlw;->a(I)Lhlw;

    move-result-object v0

    const/16 v1, 0x34

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unrecognized HTTP/2 error code: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 24
    :goto_1
    return-object v0

    .line 9
    :cond_1
    sget-object v0, Lio/grpc/internal/cf$b;->o:[Lio/grpc/internal/cf$b;

    long-to-int v1, p0

    aget-object v0, v0, v1

    goto :goto_0

    .line 23
    :cond_2
    iget-object v0, v0, Lio/grpc/internal/cf$b;->q:Lhlw;

    goto :goto_1
.end method

.method public static values()[Lio/grpc/internal/cf$b;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lio/grpc/internal/cf$b;->r:[Lio/grpc/internal/cf$b;

    invoke-virtual {v0}, [Lio/grpc/internal/cf$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/grpc/internal/cf$b;

    return-object v0
.end method
