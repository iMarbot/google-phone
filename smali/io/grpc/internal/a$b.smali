.class public Lio/grpc/internal/a$b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lhmx;


# direct methods
.method public constructor <init>(Lhmx;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 63
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 65
    iget-object v1, v0, Lhmz;->o:Ljava/lang/Object;

    .line 66
    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 68
    iget-object v2, v0, Lhmx;->j:Lhmz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :try_start_1
    iget-object v0, v2, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0, p1}, Lio/grpc/internal/ax;->b(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-virtual {v2, v0}, Lio/grpc/internal/f;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Lhlh;[B)V
    .locals 6

    .prologue
    .line 1
    const-string v1, "/"

    iget-object v0, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 2
    iget-object v0, v0, Lhmx;->d:Lhlk;

    .line 4
    iget-object v0, v0, Lhlk;->b:Ljava/lang/String;

    .line 5
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6
    :goto_0
    if-eqz p2, :cond_0

    .line 7
    iget-object v1, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 8
    const/4 v2, 0x1

    iput-boolean v2, v1, Lhmx;->k:Z

    .line 10
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 11
    sget-object v1, Lguy;->a:Lguy;

    .line 12
    invoke-virtual {v1, p2}, Lguy;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 13
    :cond_0
    iget-object v1, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 14
    iget-object v1, v1, Lhmx;->j:Lhmz;

    .line 16
    iget-object v1, v1, Lhmz;->o:Ljava/lang/Object;

    .line 17
    monitor-enter v1

    .line 18
    :try_start_0
    iget-object v2, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 19
    iget-object v2, v2, Lhmx;->j:Lhmz;

    .line 22
    iget-object v3, v2, Lhmz;->u:Lhmx;

    .line 23
    iget-object v3, v3, Lhmx;->g:Ljava/lang/String;

    .line 24
    iget-object v4, v2, Lhmz;->u:Lhmx;

    .line 25
    iget-object v4, v4, Lhmx;->e:Ljava/lang/String;

    .line 26
    iget-object v5, v2, Lhmz;->u:Lhmx;

    .line 27
    iget-boolean v5, v5, Lhmx;->k:Z

    .line 28
    invoke-static {p1, v0, v3, v4, v5}, Lhmr;->a(Lhlh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Lhmz;->p:Ljava/util/List;

    .line 29
    iget-object v0, v2, Lhmz;->t:Lhna;

    iget-object v2, v2, Lhmz;->u:Lhmx;

    invoke-virtual {v0, v2}, Lhna;->a(Lhmx;)V

    .line 30
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 5
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 30
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lhlw;)V
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 76
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 78
    iget-object v1, v0, Lhmz;->o:Ljava/lang/Object;

    .line 79
    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 81
    iget-object v0, v0, Lhmx;->j:Lhmz;

    .line 83
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v3}, Lhmz;->c(Lhlw;ZLhlh;)V

    .line 84
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lio/grpc/internal/ff;ZZ)V
    .locals 5

    .prologue
    .line 31
    if-nez p1, :cond_2

    .line 32
    sget-object v0, Lhmx;->c:Lhuh;

    .line 43
    :cond_0
    :goto_0
    iget-object v1, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 44
    iget-object v1, v1, Lhmx;->j:Lhmz;

    .line 46
    iget-object v2, v1, Lhmz;->o:Ljava/lang/Object;

    .line 47
    monitor-enter v2

    .line 48
    :try_start_0
    iget-object v1, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 49
    iget-object v3, v1, Lhmx;->j:Lhmz;

    .line 52
    iget-boolean v1, v3, Lhmz;->r:Z

    if-nez v1, :cond_1

    .line 53
    iget-object v1, v3, Lhmz;->q:Ljava/util/Queue;

    if-eqz v1, :cond_3

    .line 54
    iget-object v1, v3, Lhmz;->q:Ljava/util/Queue;

    new-instance v3, Lhmy;

    invoke-direct {v3, v0, p2, p3}, Lhmy;-><init>(Lhuh;ZZ)V

    invoke-interface {v1, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_1
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 34
    :cond_2
    check-cast p1, Lio/grpc/internal/ff;

    invoke-virtual {p1}, Lio/grpc/internal/ff;->c()Lhuh;

    move-result-object v0

    .line 36
    iget-wide v2, v0, Lhuh;->c:J

    .line 37
    long-to-int v1, v2

    .line 38
    if-lez v1, :cond_0

    .line 39
    iget-object v2, p0, Lio/grpc/internal/a$b;->a:Lhmx;

    .line 41
    invoke-virtual {v2}, Lio/grpc/internal/e;->e()Lio/grpc/internal/f;

    move-result-object v2

    .line 42
    invoke-virtual {v2, v1}, Lio/grpc/internal/f;->a(I)V

    goto :goto_0

    .line 55
    :cond_3
    :try_start_1
    iget-object v1, v3, Lhmz;->u:Lhmx;

    .line 56
    iget v1, v1, Lhmx;->i:I

    .line 57
    const/4 v4, -0x1

    if-eq v1, v4, :cond_4

    const/4 v1, 0x1

    :goto_2
    const-string v4, "streamId should be set"

    invoke-static {v1, v4}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 58
    iget-object v1, v3, Lhmz;->s:Lhng;

    iget-object v3, v3, Lhmz;->u:Lhmx;

    .line 59
    iget v3, v3, Lhmx;->i:I

    .line 60
    invoke-virtual {v1, p2, v3, v0, p3}, Lhng;->a(ZILhuh;Z)V

    goto :goto_1

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 57
    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method
