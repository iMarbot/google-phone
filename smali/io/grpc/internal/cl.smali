.class final Lio/grpc/internal/cl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic a:Lio/grpc/internal/ck;


# direct methods
.method constructor <init>(Lio/grpc/internal/ck;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 3
    iget v0, v0, Lio/grpc/internal/ck;->f:I

    .line 4
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 5
    iget v1, v1, Lio/grpc/internal/ck;->e:I

    .line 6
    sub-int/2addr v0, v1

    .line 7
    if-lez v0, :cond_0

    .line 8
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 9
    iget-object v0, v0, Lio/grpc/internal/ck;->d:[B

    .line 10
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 11
    iget v1, v1, Lio/grpc/internal/ck;->e:I

    .line 12
    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 13
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    invoke-static {v1, v2}, Lio/grpc/internal/ck;->a(Lio/grpc/internal/ck;I)I

    .line 17
    :goto_0
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 18
    iget-object v1, v1, Lio/grpc/internal/ck;->b:Ljava/util/zip/CRC32;

    .line 19
    invoke-virtual {v1, v0}, Ljava/util/zip/CRC32;->update(I)V

    .line 20
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    invoke-static {v1, v2}, Lio/grpc/internal/ck;->b(Lio/grpc/internal/ck;I)I

    .line 21
    return v0

    .line 14
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 15
    iget-object v0, v0, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    .line 16
    invoke-virtual {v0}, Lio/grpc/internal/ap;->b()I

    move-result v0

    goto :goto_0
.end method

.method final a(I)V
    .locals 7

    .prologue
    const/16 v6, 0x200

    const/4 v1, 0x0

    .line 22
    .line 23
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 24
    iget v0, v0, Lio/grpc/internal/ck;->f:I

    .line 25
    iget-object v2, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 26
    iget v2, v2, Lio/grpc/internal/ck;->e:I

    .line 27
    sub-int/2addr v0, v2

    .line 28
    if-lez v0, :cond_1

    .line 29
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 30
    iget-object v2, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 31
    iget-object v2, v2, Lio/grpc/internal/ck;->b:Ljava/util/zip/CRC32;

    .line 32
    iget-object v3, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 33
    iget-object v3, v3, Lio/grpc/internal/ck;->d:[B

    .line 34
    iget-object v4, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 35
    iget v4, v4, Lio/grpc/internal/ck;->e:I

    .line 36
    invoke-virtual {v2, v3, v4, v0}, Ljava/util/zip/CRC32;->update([BII)V

    .line 37
    iget-object v2, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    invoke-static {v2, v0}, Lio/grpc/internal/ck;->a(Lio/grpc/internal/ck;I)I

    .line 38
    sub-int v0, p1, v0

    move v2, v0

    .line 39
    :goto_0
    if-lez v2, :cond_0

    .line 40
    new-array v3, v6, [B

    move v0, v1

    .line 42
    :goto_1
    if-ge v0, v2, :cond_0

    .line 43
    sub-int v4, v2, v0

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 44
    iget-object v5, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 45
    iget-object v5, v5, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    .line 46
    invoke-virtual {v5, v3, v1, v4}, Lio/grpc/internal/ap;->a([BII)V

    .line 47
    iget-object v5, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 48
    iget-object v5, v5, Lio/grpc/internal/ck;->b:Ljava/util/zip/CRC32;

    .line 49
    invoke-virtual {v5, v3, v1, v4}, Ljava/util/zip/CRC32;->update([BII)V

    .line 50
    add-int/2addr v0, v4

    .line 51
    goto :goto_1

    .line 52
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    invoke-static {v0, p1}, Lio/grpc/internal/ck;->b(Lio/grpc/internal/ck;I)I

    .line 53
    return-void

    :cond_1
    move v2, p1

    goto :goto_0
.end method

.method final b()I
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 55
    iget v0, v0, Lio/grpc/internal/ck;->f:I

    .line 56
    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 57
    iget v1, v1, Lio/grpc/internal/ck;->e:I

    .line 58
    sub-int/2addr v0, v1

    iget-object v1, p0, Lio/grpc/internal/cl;->a:Lio/grpc/internal/ck;

    .line 59
    iget-object v1, v1, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    .line 61
    iget v1, v1, Lio/grpc/internal/ap;->a:I

    .line 62
    add-int/2addr v0, v1

    return v0
.end method

.method final c()Z
    .locals 1

    .prologue
    .line 63
    :cond_0
    invoke-virtual {p0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 64
    invoke-virtual {p0}, Lio/grpc/internal/cl;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()I
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lio/grpc/internal/cl;->a()I

    move-result v0

    invoke-virtual {p0}, Lio/grpc/internal/cl;->a()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method
