.class public final Lio/grpc/internal/dc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/dd;


# instance fields
.field public final a:Lio/grpc/internal/at;


# direct methods
.method public constructor <init>(Lio/grpc/internal/at;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lio/grpc/internal/dc;->a:Lio/grpc/internal/at;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 4
    iget-object v0, p0, Lio/grpc/internal/dc;->a:Lio/grpc/internal/at;

    new-instance v1, Lio/grpc/internal/an;

    invoke-direct {v1, p0}, Lio/grpc/internal/an;-><init>(Lio/grpc/internal/dc;)V

    .line 5
    invoke-static {}, Lhcw;->h()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 6
    invoke-interface {v0, v1, v2}, Lio/grpc/internal/at;->a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;)V

    .line 7
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 8
    iget-object v0, p0, Lio/grpc/internal/dc;->a:Lio/grpc/internal/at;

    sget-object v1, Lhlw;->i:Lhlw;

    const-string v2, "Keepalive failed. The connection is likely gone"

    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/grpc/internal/at;->b(Lhlw;)V

    .line 9
    return-void
.end method
