.class final Lio/grpc/internal/be;
.super Lio/grpc/internal/bf;
.source "PG"


# instance fields
.field public final a:Lhlc;

.field private c:Lhkl;

.field private synthetic d:Lio/grpc/internal/ay;


# direct methods
.method constructor <init>(Lio/grpc/internal/ay;Lhlc;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    invoke-direct {p0}, Lio/grpc/internal/bf;-><init>()V

    .line 2
    invoke-static {}, Lhkl;->a()Lhkl;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/be;->c:Lhkl;

    .line 3
    iput-object p2, p0, Lio/grpc/internal/be;->a:Lhlc;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Lhlw;)V
    .locals 3

    .prologue
    .line 20
    invoke-super {p0, p1}, Lio/grpc/internal/bf;->a(Lhlw;)V

    .line 21
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 22
    iget-object v1, v0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    .line 23
    monitor-enter v1

    .line 24
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 25
    iget-object v0, v0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 26
    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 28
    iget-object v0, v0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    .line 29
    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 30
    iget-object v2, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 31
    iget-object v2, v2, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    .line 32
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 34
    iget-object v0, v0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    .line 35
    iget-object v2, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 36
    iget-object v2, v2, Lio/grpc/internal/ay;->d:Ljava/lang/Runnable;

    .line 37
    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 38
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 39
    iget-object v0, v0, Lio/grpc/internal/ay;->h:Lhlw;

    .line 40
    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 42
    iget-object v0, v0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    .line 43
    iget-object v2, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 44
    iget-object v2, v2, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 45
    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 46
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 47
    const/4 v2, 0x0

    iput-object v2, v0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 49
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    iget-object v0, p0, Lio/grpc/internal/be;->d:Lio/grpc/internal/ay;

    .line 51
    iget-object v0, v0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    .line 52
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 53
    return-void

    .line 49
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final a(Lio/grpc/internal/am;)V
    .locals 4

    .prologue
    .line 5
    iget-object v0, p0, Lio/grpc/internal/be;->c:Lhkl;

    invoke-virtual {v0}, Lhkl;->c()Lhkl;

    move-result-object v1

    .line 6
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/be;->a:Lhlc;

    .line 7
    invoke-virtual {v0}, Lhlc;->c()Lhlk;

    move-result-object v0

    iget-object v2, p0, Lio/grpc/internal/be;->a:Lhlc;

    invoke-virtual {v2}, Lhlc;->b()Lhlh;

    move-result-object v2

    iget-object v3, p0, Lio/grpc/internal/be;->a:Lhlc;

    invoke-virtual {v3}, Lhlc;->a()Lhjv;

    move-result-object v3

    .line 8
    invoke-interface {p1, v0, v2, v3}, Lio/grpc/internal/am;->a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 9
    iget-object v2, p0, Lio/grpc/internal/be;->c:Lhkl;

    invoke-virtual {v2, v1}, Lhkl;->a(Lhkl;)V

    .line 13
    monitor-enter p0

    .line 14
    :try_start_1
    iget-object v1, p0, Lio/grpc/internal/bf;->b:Lio/grpc/internal/al;

    if-eqz v1, :cond_0

    .line 15
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 19
    :goto_0
    return-void

    .line 11
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lio/grpc/internal/be;->c:Lhkl;

    invoke-virtual {v2, v1}, Lhkl;->a(Lhkl;)V

    throw v0

    .line 16
    :cond_0
    :try_start_2
    const-string v1, "stream"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/al;

    iput-object v0, p0, Lio/grpc/internal/bf;->b:Lio/grpc/internal/al;

    .line 17
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 18
    invoke-super {p0}, Lio/grpc/internal/bf;->a()V

    goto :goto_0

    .line 17
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
