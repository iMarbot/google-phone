.class Lio/grpc/internal/ae$b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lio/grpc/internal/dh;


# direct methods
.method constructor <init>(Lio/grpc/internal/dh;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhlc;)Lio/grpc/internal/am;
    .locals 2

    .prologue
    .line 1
    iget-object v0, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 2
    iget-object v0, v0, Lio/grpc/internal/dh;->w:Lhle;

    .line 4
    iget-object v1, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 5
    iget-object v1, v1, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 6
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7
    iget-object v0, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 8
    iget-object v0, v0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    .line 27
    :cond_0
    :goto_0
    return-object v0

    .line 10
    :cond_1
    if-nez v0, :cond_2

    .line 11
    iget-object v0, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 12
    iget-object v0, v0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    .line 13
    new-instance v1, Lio/grpc/internal/dl;

    invoke-direct {v1, p0}, Lio/grpc/internal/dl;-><init>(Lio/grpc/internal/ae$b;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 15
    iget-object v0, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 16
    iget-object v0, v0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    goto :goto_0

    .line 18
    :cond_2
    invoke-virtual {v0}, Lhle;->a()Lhlb;

    move-result-object v0

    .line 20
    invoke-virtual {p1}, Lhlc;->a()Lhjv;

    move-result-object v1

    .line 21
    iget-boolean v1, v1, Lhjv;->h:Z

    .line 22
    invoke-static {v0, v1}, Lio/grpc/internal/cf;->a(Lhlb;Z)Lio/grpc/internal/am;

    move-result-object v0

    .line 23
    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lio/grpc/internal/ae$b;->a:Lio/grpc/internal/dh;

    .line 26
    iget-object v0, v0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    goto :goto_0
.end method
