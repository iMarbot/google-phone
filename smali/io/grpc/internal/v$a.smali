.class final Lio/grpc/internal/v$a;
.super Lhke;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

.field private static g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field public final b:Lio/grpc/internal/v;

.field public final c:Ljava/lang/String;

.field public final d:Lgts;

.field public volatile e:Lio/grpc/internal/v$b;

.field public volatile f:I

.field private h:Lgyz;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const-class v0, Lio/grpc/internal/v$a;

    const-class v1, Lio/grpc/internal/v$b;

    const-string v2, "e"

    .line 25
    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$a;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 26
    const-class v0, Lio/grpc/internal/v$a;

    const-string v1, "f"

    .line 27
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$a;->a:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    .line 28
    return-void
.end method

.method constructor <init>(Lio/grpc/internal/v;Lgyz;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lhke;-><init>()V

    .line 2
    iput-object p1, p0, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    .line 3
    const-string v0, "parentCtx"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgyz;

    iput-object v0, p0, Lio/grpc/internal/v$a;->h:Lgyz;

    .line 4
    const-string v0, "fullMethodName"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/v$a;->c:Ljava/lang/String;

    .line 6
    iget-object v0, p1, Lio/grpc/internal/v;->e:Lgtu;

    .line 7
    invoke-interface {v0}, Lgtu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgts;

    invoke-virtual {v0}, Lgts;->a()Lgts;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/v$a;->d:Lgts;

    .line 8
    return-void
.end method


# virtual methods
.method public final a(Lhlh;)Lhkd;
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lio/grpc/internal/v$b;

    .line 10
    invoke-direct {v0}, Lio/grpc/internal/v$b;-><init>()V

    .line 12
    sget-object v1, Lio/grpc/internal/v$a;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v2, 0x0

    .line 13
    invoke-virtual {v1, p0, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "Are you creating multiple streams per call? This class doesn\'t yet support this case."

    .line 14
    invoke-static {v1, v2}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 15
    iget-object v1, p0, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    .line 16
    iget-boolean v1, v1, Lio/grpc/internal/v;->h:Z

    .line 17
    if-eqz v1, :cond_0

    .line 18
    iget-object v1, p0, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    iget-object v1, v1, Lio/grpc/internal/v;->f:Lhlh$e;

    invoke-virtual {p1, v1}, Lhlh;->b(Lhlh$e;)V

    .line 19
    iget-object v1, p0, Lio/grpc/internal/v$a;->h:Lgyz;

    iget-object v2, p0, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    .line 20
    iget-object v2, v2, Lio/grpc/internal/v;->d:Lgza;

    .line 21
    invoke-virtual {v2}, Lgza;->b()Lgyz;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 22
    iget-object v1, p0, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    iget-object v1, v1, Lio/grpc/internal/v;->f:Lhlh$e;

    iget-object v2, p0, Lio/grpc/internal/v$a;->h:Lgyz;

    invoke-virtual {p1, v1, v2}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 23
    :cond_0
    return-object v0
.end method
