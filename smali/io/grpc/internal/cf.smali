.class public final Lio/grpc/internal/cf;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/cf$c;,
        Lio/grpc/internal/cf$b;,
        Lio/grpc/internal/cf$a;
    }
.end annotation


# static fields
.field public static final a:Z

.field public static final b:Lhlh$e;

.field public static final c:Lhlh$e;

.field public static final d:Lhlh$e;

.field public static final e:Lhlh$e;

.field public static final f:Lhlh$e;

.field public static final g:Lhlh$e;

.field public static final h:Lhlh$e;

.field public static final i:Lhlh$e;

.field public static final j:J

.field public static final k:Lio/grpc/internal/ew;

.field public static final l:Lio/grpc/internal/ew;

.field public static final m:Lgtu;

.field private static n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x14

    .line 98
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    .line 99
    const-string v0, "com.google.appengine.runtime.environment"

    .line 100
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "1.7"

    const-string v1, "java.specification.version"

    .line 101
    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lio/grpc/internal/cf;->a:Z

    .line 102
    const-string v0, "grpc-timeout"

    new-instance v1, Lio/grpc/internal/cf$c;

    invoke-direct {v1}, Lio/grpc/internal/cf$c;-><init>()V

    .line 103
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->b:Lhlh$e;

    .line 104
    const-string v0, "grpc-encoding"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 105
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->c:Lhlh$e;

    .line 106
    const-string v0, "grpc-accept-encoding"

    new-instance v1, Lio/grpc/internal/cf$a;

    .line 107
    invoke-direct {v1}, Lio/grpc/internal/cf$a;-><init>()V

    .line 108
    invoke-static {v0, v1}, Lhkv;->a(Ljava/lang/String;Lhlh$g;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->d:Lhlh$e;

    .line 109
    const-string v0, "content-encoding"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 110
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->e:Lhlh$e;

    .line 111
    const-string v0, "accept-encoding"

    new-instance v1, Lio/grpc/internal/cf$a;

    .line 112
    invoke-direct {v1}, Lio/grpc/internal/cf$a;-><init>()V

    .line 113
    invoke-static {v0, v1}, Lhkv;->a(Ljava/lang/String;Lhlh$g;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->f:Lhlh$e;

    .line 114
    const-string v0, "content-type"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 115
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->g:Lhlh$e;

    .line 116
    const-string v0, "te"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 117
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->h:Lhlh$e;

    .line 118
    const-string v0, "user-agent"

    sget-object v1, Lhlh;->a:Lhlh$b;

    .line 119
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$b;)Lhlh$e;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/cf;->i:Lhlh$e;

    .line 121
    const/16 v0, 0x2c

    invoke-static {v0}, Lgta;->a(C)Lgta;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    new-instance v1, Lgtq;

    new-instance v2, Lgtr;

    invoke-direct {v2, v0}, Lgtr;-><init>(Lgta;)V

    invoke-direct {v1, v2}, Lgtq;-><init>(Lgtr;)V

    .line 125
    invoke-static {}, Lgta;->b()Lgta;

    move-result-object v0

    .line 126
    invoke-static {v0}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    new-instance v2, Lgtq;

    iget-object v3, v1, Lgtq;->b:Lgtr;

    iget-boolean v4, v1, Lgtq;->a:Z

    iget v1, v1, Lgtq;->c:I

    invoke-direct {v2, v3, v4, v0, v1}, Lgtq;-><init>(Lgtr;ZLgta;I)V

    .line 129
    const-class v0, Lio/grpc/internal/cf;

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getImplementationVersion()Ljava/lang/String;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_2

    .line 131
    const-string v1, "/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_1
    sput-object v0, Lio/grpc/internal/cf;->n:Ljava/lang/String;

    .line 134
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 135
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    sput-wide v0, Lio/grpc/internal/cf;->j:J

    .line 136
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 137
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 138
    new-instance v0, Lio/grpc/internal/cg;

    invoke-direct {v0}, Lio/grpc/internal/cg;-><init>()V

    sput-object v0, Lio/grpc/internal/cf;->k:Lio/grpc/internal/ew;

    .line 139
    new-instance v0, Lio/grpc/internal/ch;

    invoke-direct {v0}, Lio/grpc/internal/ch;-><init>()V

    sput-object v0, Lio/grpc/internal/cf;->l:Lio/grpc/internal/ew;

    .line 140
    new-instance v0, Lio/grpc/internal/ci;

    invoke-direct {v0}, Lio/grpc/internal/ci;-><init>()V

    sput-object v0, Lio/grpc/internal/cf;->m:Lgtu;

    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 131
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 132
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Lhlw;
    .locals 3

    .prologue
    .line 1
    .line 2
    const/16 v0, 0x64

    if-lt p0, v0, :cond_0

    const/16 v0, 0xc8

    if-ge p0, v0, :cond_0

    .line 3
    sget-object v0, Lhlx;->n:Lhlx;

    .line 11
    :goto_0
    invoke-virtual {v0}, Lhlx;->a()Lhlw;

    move-result-object v0

    const/16 v1, 0x1c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HTTP status code "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 13
    return-object v0

    .line 4
    :cond_0
    sparse-switch p0, :sswitch_data_0

    .line 10
    sget-object v0, Lhlx;->c:Lhlx;

    goto :goto_0

    .line 5
    :sswitch_0
    sget-object v0, Lhlx;->n:Lhlx;

    goto :goto_0

    .line 6
    :sswitch_1
    sget-object v0, Lhlx;->q:Lhlx;

    goto :goto_0

    .line 7
    :sswitch_2
    sget-object v0, Lhlx;->h:Lhlx;

    goto :goto_0

    .line 8
    :sswitch_3
    sget-object v0, Lhlx;->m:Lhlx;

    goto :goto_0

    .line 9
    :sswitch_4
    sget-object v0, Lhlx;->o:Lhlx;

    goto :goto_0

    .line 4
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x191 -> :sswitch_1
        0x193 -> :sswitch_2
        0x194 -> :sswitch_3
        0x1ad -> :sswitch_4
        0x1af -> :sswitch_0
        0x1f6 -> :sswitch_4
        0x1f7 -> :sswitch_4
        0x1f8 -> :sswitch_4
    .end sparse-switch
.end method

.method static a(Lhlb;Z)Lio/grpc/internal/am;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 68
    .line 69
    iget-object v0, p0, Lhlb;->b:Lhld;

    .line 71
    if-eqz v0, :cond_0

    .line 72
    check-cast v0, Lhld;

    invoke-virtual {v0}, Lhld;->c()Lio/grpc/internal/am;

    move-result-object v0

    .line 74
    :goto_0
    if-eqz v0, :cond_2

    .line 76
    iget-object v2, p0, Lhlb;->c:Lhke;

    .line 78
    if-nez v2, :cond_1

    .line 89
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 73
    goto :goto_0

    .line 80
    :cond_1
    new-instance v1, Lio/grpc/internal/cj;

    invoke-direct {v1, v0, v2}, Lio/grpc/internal/cj;-><init>(Lio/grpc/internal/am;Lhke;)V

    move-object v0, v1

    goto :goto_1

    .line 82
    :cond_2
    iget-object v0, p0, Lhlb;->d:Lhlw;

    .line 83
    invoke-virtual {v0}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 84
    iget-boolean v0, p0, Lhlb;->e:Z

    .line 85
    if-nez v0, :cond_3

    if-nez p1, :cond_4

    .line 86
    :cond_3
    new-instance v0, Lio/grpc/internal/cb;

    .line 87
    iget-object v1, p0, Lhlb;->d:Lhlw;

    .line 88
    invoke-direct {v0, v1}, Lio/grpc/internal/cb;-><init>(Lhlw;)V

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 89
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 8

    .prologue
    .line 39
    :try_start_0
    new-instance v0, Ljava/net/URI;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p0

    move v4, p1

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URI;->getAuthority()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid host or port: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    if-eqz p1, :cond_0

    .line 27
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 29
    :cond_0
    const-string v1, "grpc-java-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    sget-object v1, Lio/grpc/internal/cf;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/net/InetSocketAddress;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 60
    :try_start_0
    const-class v0, Ljava/net/InetSocketAddress;

    const-string v1, "getHostString"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 61
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    invoke-virtual {p0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    goto :goto_1

    .line 63
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 42
    sget-boolean v0, Lio/grpc/internal/cf;->a:Z

    if-eqz v0, :cond_0

    .line 43
    invoke-static {}, Lhcw;->i()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    .line 45
    :cond_0
    new-instance v0, Lgvx;

    invoke-direct {v0}, Lgvx;-><init>()V

    .line 47
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lgvx;->b:Ljava/lang/Boolean;

    .line 51
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p0, v1}, Lgvx;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    iput-object p0, v0, Lgvx;->a:Ljava/lang/String;

    .line 54
    iget-object v2, v0, Lgvx;->a:Ljava/lang/String;

    .line 55
    iget-object v4, v0, Lgvx;->b:Ljava/lang/Boolean;

    .line 56
    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v1

    .line 57
    if-eqz v2, :cond_1

    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v6, 0x0

    invoke-direct {v3, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    .line 58
    :goto_1
    new-instance v0, Lgvy;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lgvy;-><init>(Ljava/util/concurrent/ThreadFactory;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicLong;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto :goto_0

    :cond_1
    move-object v3, v5

    .line 57
    goto :goto_1
.end method

.method static a(Lio/grpc/internal/fc;)V
    .locals 1

    .prologue
    .line 90
    :goto_0
    invoke-interface {p0}, Lio/grpc/internal/fc;->a()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-static {v0}, Lio/grpc/internal/cf;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method static a(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 93
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x10

    const/4 v0, 0x0

    .line 14
    if-nez p0, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v0

    .line 16
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v4, v2, :cond_0

    .line 18
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 19
    const-string v3, "application/grpc"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 21
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v4, :cond_2

    move v0, v1

    .line 22
    goto :goto_0

    .line 23
    :cond_2
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 24
    const/16 v3, 0x2b

    if-eq v2, v3, :cond_3

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/net/URI;
    .locals 6

    .prologue
    .line 33
    const-string v0, "authority"

    invoke-static {p0, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    :try_start_0
    new-instance v0, Ljava/net/URI;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    return-object v0

    .line 36
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 37
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid authority: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
