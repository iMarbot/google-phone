.class final Lio/grpc/internal/v;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/v$d;,
        Lio/grpc/internal/v$c;,
        Lio/grpc/internal/v$a;,
        Lio/grpc/internal/v$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:D

.field public static final c:Lio/grpc/internal/v$b;


# instance fields
.field public final d:Lgza;

.field public final e:Lgtu;

.field public final f:Lhlh$e;

.field public final g:Lio/grpc/internal/v$d;

.field public final h:Z

.field public final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 11
    const-class v0, Lio/grpc/internal/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v;->a:Ljava/util/logging/Logger;

    .line 12
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    long-to-double v0, v0

    sput-wide v0, Lio/grpc/internal/v;->b:D

    .line 13
    new-instance v0, Lio/grpc/internal/v$b;

    .line 14
    invoke-direct {v0}, Lio/grpc/internal/v$b;-><init>()V

    .line 15
    sput-object v0, Lio/grpc/internal/v;->c:Lio/grpc/internal/v$b;

    return-void
.end method

.method constructor <init>(Lgza;Lgtu;ZZ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lio/grpc/internal/v$d;

    invoke-direct {v0, p0}, Lio/grpc/internal/v$d;-><init>(Lio/grpc/internal/v;)V

    iput-object v0, p0, Lio/grpc/internal/v;->g:Lio/grpc/internal/v$d;

    .line 3
    new-instance v0, Lio/grpc/internal/v$c;

    invoke-direct {v0, p0}, Lio/grpc/internal/v$c;-><init>(Lio/grpc/internal/v;)V

    .line 4
    const-string v0, "statsCtxFactory"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgza;

    iput-object v0, p0, Lio/grpc/internal/v;->d:Lgza;

    .line 5
    const-string v0, "stopwatchSupplier"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtu;

    iput-object v0, p0, Lio/grpc/internal/v;->e:Lgtu;

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/v;->h:Z

    .line 7
    iput-boolean p4, p0, Lio/grpc/internal/v;->i:Z

    .line 8
    const-string v0, "grpc-tags-bin"

    new-instance v1, Lio/grpc/internal/w;

    invoke-direct {v1, p1}, Lio/grpc/internal/w;-><init>(Lgza;)V

    .line 9
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$d;)Lhlh$e;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/v;->f:Lhlh$e;

    .line 10
    return-void
.end method
