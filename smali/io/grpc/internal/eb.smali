.class public final Lio/grpc/internal/eb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ce;


# instance fields
.field public final a:Lio/grpc/internal/fg;

.field private b:Lio/grpc/internal/ee;

.field private c:I

.field private d:Lio/grpc/internal/ff;

.field private e:Lhkh;

.field private f:Z

.field private g:Lio/grpc/internal/ed;

.field private h:[B

.field private i:Lio/grpc/internal/ez;

.field private j:Z

.field private k:I

.field private l:J


# direct methods
.method public constructor <init>(Lio/grpc/internal/ee;Lio/grpc/internal/fg;Lio/grpc/internal/ez;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v1, p0, Lio/grpc/internal/eb;->c:I

    .line 3
    sget-object v0, Lhkg;->a:Lhkh;

    iput-object v0, p0, Lio/grpc/internal/eb;->e:Lhkh;

    .line 4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/eb;->f:Z

    .line 5
    new-instance v0, Lio/grpc/internal/ed;

    .line 6
    invoke-direct {v0, p0}, Lio/grpc/internal/ed;-><init>(Lio/grpc/internal/eb;)V

    .line 7
    iput-object v0, p0, Lio/grpc/internal/eb;->g:Lio/grpc/internal/ed;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [B

    iput-object v0, p0, Lio/grpc/internal/eb;->h:[B

    .line 9
    iput v1, p0, Lio/grpc/internal/eb;->k:I

    .line 10
    const-string v0, "sink"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ee;

    iput-object v0, p0, Lio/grpc/internal/eb;->b:Lio/grpc/internal/ee;

    .line 11
    const-string v0, "bufferAllocator"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/fg;

    iput-object v0, p0, Lio/grpc/internal/eb;->a:Lio/grpc/internal/fg;

    .line 12
    const-string v0, "statsTraceCtx"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ez;

    iput-object v0, p0, Lio/grpc/internal/eb;->i:Lio/grpc/internal/ez;

    .line 13
    return-void
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 4

    .prologue
    .line 121
    instance-of v0, p0, Lhkr;

    if-eqz v0, :cond_0

    .line 122
    check-cast p0, Lhkr;

    invoke-interface {p0, p1}, Lhkr;->a(Ljava/io/OutputStream;)I

    move-result v0

    .line 125
    :goto_0
    return v0

    .line 123
    :cond_0
    invoke-static {p0, p1}, Lio/grpc/internal/av;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    move-result-wide v2

    .line 124
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Message size overflow: %s"

    invoke-static {v0, v1, v2, v3}, Lgtn;->a(ZLjava/lang/String;J)V

    .line 125
    long-to-int v0, v2

    goto :goto_0

    .line 124
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private final a(Lio/grpc/internal/ec;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lio/grpc/internal/eb;->h:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 96
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 100
    iget-object v0, p1, Lio/grpc/internal/ec;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ff;

    .line 101
    invoke-virtual {v0}, Lio/grpc/internal/ff;->b()I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    .line 102
    goto :goto_1

    :cond_0
    move v0, v1

    .line 96
    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 106
    iget-object v0, p0, Lio/grpc/internal/eb;->a:Lio/grpc/internal/fg;

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Lio/grpc/internal/fg;->a(I)Lio/grpc/internal/ff;

    move-result-object v0

    .line 107
    iget-object v4, p0, Lio/grpc/internal/eb;->h:[B

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {v0, v4, v1, v3}, Lio/grpc/internal/ff;->a([BII)V

    .line 108
    if-nez v2, :cond_2

    .line 109
    iput-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 120
    :goto_2
    return-void

    .line 111
    :cond_2
    iget-object v3, p0, Lio/grpc/internal/eb;->b:Lio/grpc/internal/ee;

    invoke-interface {v3, v0, v1, v1}, Lio/grpc/internal/ee;->a(Lio/grpc/internal/ff;ZZ)V

    .line 113
    iget-object v4, p1, Lio/grpc/internal/ec;->a:Ljava/util/List;

    move v3, v1

    .line 115
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_3

    .line 116
    iget-object v5, p0, Lio/grpc/internal/eb;->b:Lio/grpc/internal/ee;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ff;

    invoke-interface {v5, v0, v1, v1}, Lio/grpc/internal/ee;->a(Lio/grpc/internal/ff;ZZ)V

    .line 117
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 118
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ff;

    iput-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 119
    int-to-long v0, v2

    iput-wide v0, p0, Lio/grpc/internal/eb;->l:J

    goto :goto_2
.end method

.method private final a(ZZ)V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 152
    const/4 v1, 0x0

    iput-object v1, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 153
    iget-object v1, p0, Lio/grpc/internal/eb;->b:Lio/grpc/internal/ee;

    invoke-interface {v1, v0, p1, p2}, Lio/grpc/internal/ee;->a(Lio/grpc/internal/ff;ZZ)V

    .line 154
    return-void
.end method

.method private final b(Ljava/io/InputStream;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 80
    new-instance v0, Lio/grpc/internal/ec;

    .line 81
    invoke-direct {v0, p0}, Lio/grpc/internal/ec;-><init>(Lio/grpc/internal/eb;)V

    .line 83
    iget-object v1, p0, Lio/grpc/internal/eb;->e:Lhkh;

    invoke-interface {v1, v0}, Lhkh;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v1

    .line 84
    :try_start_0
    invoke-static {p1, v1}, Lio/grpc/internal/eb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 85
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 88
    iget v1, p0, Lio/grpc/internal/eb;->c:I

    if-ltz v1, :cond_0

    iget v1, p0, Lio/grpc/internal/eb;->c:I

    if-le v2, v1, :cond_0

    .line 89
    sget-object v0, Lhlw;->g:Lhlw;

    const-string v1, "message too large %d > %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 90
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v4

    iget v2, p0, Lio/grpc/internal/eb;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0

    .line 93
    :cond_0
    invoke-direct {p0, v0, v5}, Lio/grpc/internal/eb;->a(Lio/grpc/internal/ec;Z)V

    .line 94
    return v2
.end method


# virtual methods
.method public final synthetic a(Lhkh;)Lio/grpc/internal/ce;
    .locals 1

    .prologue
    .line 155
    .line 156
    const-string v0, "Can\'t pass an empty compressor"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkh;

    iput-object v0, p0, Lio/grpc/internal/eb;->e:Lhkh;

    .line 158
    return-object p0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    invoke-virtual {v0}, Lio/grpc/internal/ff;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lio/grpc/internal/eb;->a(ZZ)V

    .line 139
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 14
    iget v0, p0, Lio/grpc/internal/eb;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "max size already set"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 15
    iput p1, p0, Lio/grpc/internal/eb;->c:I

    .line 16
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/io/InputStream;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 17
    .line 19
    iget-boolean v0, p0, Lio/grpc/internal/eb;->j:Z

    .line 20
    if-eqz v0, :cond_0

    .line 21
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Framer already closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iget v0, p0, Lio/grpc/internal/eb;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/grpc/internal/eb;->k:I

    .line 23
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lio/grpc/internal/eb;->l:J

    .line 24
    iget-object v0, p0, Lio/grpc/internal/eb;->i:Lio/grpc/internal/ez;

    iget v3, p0, Lio/grpc/internal/eb;->k:I

    invoke-virtual {v0, v3}, Lio/grpc/internal/ez;->a(I)V

    .line 25
    iget-boolean v0, p0, Lio/grpc/internal/eb;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/grpc/internal/eb;->e:Lhkh;

    sget-object v3, Lhkg;->a:Lhkh;

    if-eq v0, v3, :cond_2

    move v0, v1

    .line 27
    :goto_0
    :try_start_0
    instance-of v3, p1, Lhkx;

    if-nez v3, :cond_1

    instance-of v3, p1, Ljava/io/ByteArrayInputStream;

    if-eqz v3, :cond_3

    .line 28
    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v3

    .line 31
    :goto_1
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    .line 32
    invoke-direct {p0, p1}, Lio/grpc/internal/eb;->b(Ljava/io/InputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    move v4, v0

    .line 73
    :goto_2
    if-eq v3, v5, :cond_9

    if-eq v4, v3, :cond_9

    .line 74
    const-string v0, "Message length inaccurate %s != %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 75
    sget-object v1, Lhlw;->h:Lhlw;

    invoke-virtual {v1, v0}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    :cond_2
    move v0, v2

    .line 25
    goto :goto_0

    :cond_3
    move v3, v5

    .line 29
    goto :goto_1

    .line 34
    :cond_4
    if-eq v3, v5, :cond_7

    .line 35
    int-to-long v6, v3

    :try_start_1
    iput-wide v6, p0, Lio/grpc/internal/eb;->l:J

    .line 37
    iget v0, p0, Lio/grpc/internal/eb;->c:I

    if-ltz v0, :cond_5

    iget v0, p0, Lio/grpc/internal/eb;->c:I

    if-le v3, v0, :cond_5

    .line 38
    sget-object v0, Lhlw;->g:Lhlw;

    const-string v1, "message too large %d > %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 39
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget v4, p0, Lio/grpc/internal/eb;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 63
    :catch_0
    move-exception v0

    .line 64
    sget-object v1, Lhlw;->h:Lhlw;

    const-string v2, "Failed to frame message"

    .line 65
    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    .line 66
    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 42
    :cond_5
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/eb;->h:[B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 43
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 44
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 45
    iget-object v4, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-nez v4, :cond_6

    .line 46
    iget-object v4, p0, Lio/grpc/internal/eb;->a:Lio/grpc/internal/fg;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v6}, Lio/grpc/internal/fg;->a(I)Lio/grpc/internal/ff;

    move-result-object v4

    iput-object v4, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 47
    :cond_6
    iget-object v4, p0, Lio/grpc/internal/eb;->h:[B

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-virtual {p0, v4, v6, v0}, Lio/grpc/internal/eb;->a([BII)V

    .line 48
    iget-object v0, p0, Lio/grpc/internal/eb;->g:Lio/grpc/internal/ed;

    invoke-static {p1, v0}, Lio/grpc/internal/eb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    :goto_3
    move v4, v0

    .line 62
    goto/16 :goto_2

    .line 50
    :cond_7
    new-instance v4, Lio/grpc/internal/ec;

    .line 51
    invoke-direct {v4, p0}, Lio/grpc/internal/ec;-><init>(Lio/grpc/internal/eb;)V

    .line 53
    invoke-static {p1, v4}, Lio/grpc/internal/eb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v0

    .line 54
    iget v6, p0, Lio/grpc/internal/eb;->c:I

    if-ltz v6, :cond_8

    iget v6, p0, Lio/grpc/internal/eb;->c:I

    if-le v0, v6, :cond_8

    .line 55
    sget-object v1, Lhlw;->g:Lhlw;

    const-string v2, "message too large %d > %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 56
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget v4, p0, Lio/grpc/internal/eb;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-virtual {v1, v0}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 68
    :catch_1
    move-exception v0

    .line 69
    sget-object v1, Lhlw;->h:Lhlw;

    const-string v2, "Failed to frame message"

    .line 70
    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 59
    :cond_8
    const/4 v6, 0x0

    :try_start_3
    invoke-direct {p0, v4, v6}, Lio/grpc/internal/eb;->a(Lio/grpc/internal/ec;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 76
    :cond_9
    iget-object v0, p0, Lio/grpc/internal/eb;->i:Lio/grpc/internal/ez;

    int-to-long v2, v4

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->a(J)V

    .line 77
    iget-object v0, p0, Lio/grpc/internal/eb;->i:Lio/grpc/internal/ez;

    iget-wide v2, p0, Lio/grpc/internal/eb;->l:J

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->b(J)V

    .line 78
    iget-object v0, p0, Lio/grpc/internal/eb;->i:Lio/grpc/internal/ez;

    iget v1, p0, Lio/grpc/internal/eb;->k:I

    iget-wide v2, p0, Lio/grpc/internal/eb;->l:J

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lio/grpc/internal/ez;->a(IJJ)V

    .line 79
    return-void
.end method

.method final a([BII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    :goto_0
    if-lez p3, :cond_2

    .line 127
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    invoke-virtual {v0}, Lio/grpc/internal/ff;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 128
    invoke-direct {p0, v2, v2}, Lio/grpc/internal/eb;->a(ZZ)V

    .line 129
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lio/grpc/internal/eb;->a:Lio/grpc/internal/fg;

    invoke-virtual {v0, p3}, Lio/grpc/internal/fg;->a(I)Lio/grpc/internal/ff;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 131
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    invoke-virtual {v0}, Lio/grpc/internal/ff;->a()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 132
    iget-object v1, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    invoke-virtual {v1, p1, p2, v0}, Lio/grpc/internal/ff;->a([BII)V

    .line 133
    add-int/2addr p2, v0

    .line 134
    sub-int/2addr p3, v0

    .line 135
    goto :goto_0

    .line 136
    :cond_2
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lio/grpc/internal/eb;->j:Z

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 141
    .line 142
    iget-boolean v0, p0, Lio/grpc/internal/eb;->j:Z

    .line 143
    if-nez v0, :cond_1

    .line 144
    iput-boolean v1, p0, Lio/grpc/internal/eb;->j:Z

    .line 145
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    invoke-virtual {v0}, Lio/grpc/internal/ff;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/eb;->d:Lio/grpc/internal/ff;

    .line 149
    :cond_0
    invoke-direct {p0, v1, v1}, Lio/grpc/internal/eb;->a(ZZ)V

    .line 150
    :cond_1
    return-void
.end method
