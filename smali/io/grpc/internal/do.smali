.class final Lio/grpc/internal/do;
.super Lio/grpc/internal/ct$a;
.source "PG"


# instance fields
.field private synthetic a:Lio/grpc/internal/dh$e;

.field private synthetic b:Lio/grpc/internal/dh$b;


# direct methods
.method constructor <init>(Lio/grpc/internal/dh$b;Lio/grpc/internal/dh$e;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iput-object p2, p0, Lio/grpc/internal/do;->a:Lio/grpc/internal/dh$e;

    invoke-direct {p0}, Lio/grpc/internal/ct$a;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Lhkk;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    .line 24
    iget-object v1, p1, Lhkk;->a:Lhkj;

    .line 25
    sget-object v2, Lhkj;->c:Lhkj;

    if-eq v1, v2, :cond_0

    .line 26
    iget-object v1, p1, Lhkk;->a:Lhkj;

    .line 27
    sget-object v2, Lhkj;->d:Lhkj;

    if-ne v1, v2, :cond_1

    .line 28
    :cond_0
    iget-object v0, v0, Lio/grpc/internal/dh$b;->b:Lhlm;

    invoke-virtual {v0}, Lhlm;->c()V

    .line 29
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v1, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v1, v1, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 30
    iget-object v1, v1, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 31
    if-ne v0, v1, :cond_2

    .line 32
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->a:Lhky;

    iget-object v1, p0, Lio/grpc/internal/do;->a:Lio/grpc/internal/dh$e;

    invoke-virtual {v0, v1, p1}, Lhky;->a(Lhld;Lhkk;)V

    .line 33
    :cond_2
    return-void
.end method

.method final a(Lio/grpc/internal/ct;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 3
    iget-object v0, v0, Lio/grpc/internal/dh;->x:Ljava/util/Set;

    .line 4
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 5
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v6, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 7
    iget-boolean v0, v6, Lio/grpc/internal/dh;->C:Z

    if-nez v0, :cond_0

    .line 8
    iget-object v0, v6, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v6, Lio/grpc/internal/dh;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v6, Lio/grpc/internal/dh;->y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "maybeTerminateChannel"

    const-string v4, "[{0}] Terminated"

    .line 10
    iget-object v5, v6, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 11
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 12
    iput-boolean v7, v6, Lio/grpc/internal/dh;->C:Z

    .line 13
    iget-object v0, v6, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    .line 14
    iput-boolean v7, v0, Lio/grpc/internal/dh$c;->b:Z

    .line 16
    iget-object v0, v6, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    invoke-virtual {v0}, Lio/grpc/internal/dh$c;->clear()V

    .line 17
    iget-object v0, v6, Lio/grpc/internal/dh;->D:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 18
    iget-object v0, v6, Lio/grpc/internal/dh;->i:Lio/grpc/internal/eg;

    iget-object v1, v6, Lio/grpc/internal/dh;->h:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v1}, Lio/grpc/internal/eg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iget-object v0, v6, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    invoke-interface {v0}, Lio/grpc/internal/ao;->close()V

    .line 20
    :cond_0
    return-void
.end method

.method final b(Lio/grpc/internal/ct;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    iget-object v0, v0, Lio/grpc/internal/dh;->F:Lio/grpc/internal/cs;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lio/grpc/internal/cs;->a(Ljava/lang/Object;Z)V

    .line 35
    return-void
.end method

.method final c(Lio/grpc/internal/ct;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lio/grpc/internal/do;->b:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    iget-object v0, v0, Lio/grpc/internal/dh;->F:Lio/grpc/internal/cs;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lio/grpc/internal/cs;->a(Ljava/lang/Object;Z)V

    .line 37
    return-void
.end method
