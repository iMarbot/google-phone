.class final Lio/grpc/internal/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ao;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field private b:Lio/grpc/internal/ao;


# direct methods
.method constructor <init>(Lio/grpc/internal/ao;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "delegate"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ao;

    iput-object v0, p0, Lio/grpc/internal/t;->b:Lio/grpc/internal/ao;

    .line 3
    const-string v0, "appExecutor"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lio/grpc/internal/t;->a:Ljava/util/concurrent/Executor;

    .line 4
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/SocketAddress;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/em;)Lio/grpc/internal/at;
    .locals 2

    .prologue
    .line 5
    new-instance v0, Lio/grpc/internal/u;

    iget-object v1, p0, Lio/grpc/internal/t;->b:Lio/grpc/internal/ao;

    .line 6
    invoke-interface {v1, p1, p2, p3, p4}, Lio/grpc/internal/ao;->a(Ljava/net/SocketAddress;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/em;)Lio/grpc/internal/at;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2}, Lio/grpc/internal/u;-><init>(Lio/grpc/internal/t;Lio/grpc/internal/at;Ljava/lang/String;)V

    .line 7
    return-object v0
.end method

.method public final a()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lio/grpc/internal/t;->b:Lio/grpc/internal/ao;

    invoke-interface {v0}, Lio/grpc/internal/ao;->a()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lio/grpc/internal/t;->b:Lio/grpc/internal/ao;

    invoke-interface {v0}, Lio/grpc/internal/ao;->close()V

    .line 10
    return-void
.end method
