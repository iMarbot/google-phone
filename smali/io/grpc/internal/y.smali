.class final Lio/grpc/internal/y;
.super Lhku;
.source "PG"


# instance fields
.field private synthetic b:Lio/grpc/internal/x;


# direct methods
.method constructor <init>(Lio/grpc/internal/x;Lhjy;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/y;->b:Lio/grpc/internal/x;

    invoke-direct {p0, p2}, Lhku;-><init>(Lhjy;)V

    return-void
.end method


# virtual methods
.method public final a(Lhlw;Lhlh;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2
    iget-object v0, p0, Lio/grpc/internal/y;->b:Lio/grpc/internal/x;

    iget-object v2, v0, Lio/grpc/internal/x;->b:Lio/grpc/internal/v$a;

    .line 3
    sget-object v0, Lio/grpc/internal/v$a;->a:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndSet(Ljava/lang/Object;I)I

    move-result v0

    if-nez v0, :cond_5

    .line 4
    iget-object v0, v2, Lio/grpc/internal/v$a;->b:Lio/grpc/internal/v;

    .line 5
    iget-boolean v0, v0, Lio/grpc/internal/v;->i:Z

    .line 6
    if-eqz v0, :cond_5

    .line 7
    iget-object v0, v2, Lio/grpc/internal/v$a;->d:Lgts;

    .line 8
    iget-object v3, v0, Lgts;->a:Lgtw;

    invoke-virtual {v3}, Lgtw;->a()J

    move-result-wide v4

    .line 9
    iget-boolean v3, v0, Lgts;->b:Z

    const-string v6, "This stopwatch is already stopped."

    invoke-static {v3, v6}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 10
    iput-boolean v1, v0, Lgts;->b:Z

    .line 11
    iget-wide v6, v0, Lgts;->c:J

    iget-wide v8, v0, Lgts;->d:J

    sub-long/2addr v4, v8

    add-long/2addr v4, v6

    iput-wide v4, v0, Lgts;->c:J

    .line 12
    iget-object v0, v2, Lio/grpc/internal/v$a;->d:Lgts;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3}, Lgts;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 13
    iget-object v0, v2, Lio/grpc/internal/v$a;->e:Lio/grpc/internal/v$b;

    .line 14
    if-nez v0, :cond_0

    .line 15
    sget-object v0, Lio/grpc/internal/v;->c:Lio/grpc/internal/v$b;

    .line 17
    :cond_0
    new-instance v3, Lgyu;

    .line 18
    invoke-direct {v3}, Lgyu;-><init>()V

    .line 19
    sget-object v6, Lgyx;->f:Lgyp;

    long-to-double v4, v4

    .line 20
    sget-wide v8, Lio/grpc/internal/v;->b:D

    .line 21
    div-double/2addr v4, v8

    invoke-virtual {v3, v6, v4, v5}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->i:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->a:J

    long-to-double v6, v6

    .line 22
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->j:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->b:J

    long-to-double v6, v6

    .line 23
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->d:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->c:J

    long-to-double v6, v6

    .line 24
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->e:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->d:J

    long-to-double v6, v6

    .line 25
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->g:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->e:J

    long-to-double v6, v6

    .line 26
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    sget-object v4, Lgyx;->h:Lgyp;

    iget-wide v6, v0, Lio/grpc/internal/v$b;->f:J

    long-to-double v6, v6

    .line 27
    invoke-virtual {v3, v4, v6, v7}, Lgyu;->a(Lgyp;D)Lgyu;

    move-result-object v3

    .line 28
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 29
    sget-object v0, Lgyx;->c:Lgyp;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v3, v0, v4, v5}, Lgyu;->a(Lgyp;D)Lgyu;

    .line 30
    :cond_1
    iget-object v0, v2, Lio/grpc/internal/v$a;->c:Ljava/lang/String;

    .line 31
    invoke-static {v0}, Lgzd;->a(Ljava/lang/String;)Lgzd;

    .line 33
    iget-object v0, p1, Lhlw;->l:Lhlx;

    .line 34
    invoke-virtual {v0}, Lhlx;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgzd;->a(Ljava/lang/String;)Lgzd;

    .line 37
    :goto_0
    iget-object v0, v3, Lgyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 38
    iget-object v0, v3, Lgyu;->a:Ljava/util/ArrayList;

    .line 39
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgyw;

    .line 40
    iget-object v0, v0, Lgyw;->a:Lgyp;

    .line 41
    invoke-virtual {v0}, Lgyp;->a()Lgys;

    move-result-object v4

    .line 42
    add-int/lit8 v0, v1, 0x1

    move v2, v0

    :goto_1
    iget-object v0, v3, Lgyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 43
    iget-object v0, v3, Lgyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgyw;

    .line 44
    iget-object v0, v0, Lgyw;->a:Lgyp;

    .line 45
    invoke-virtual {v0}, Lgyp;->a()Lgys;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46
    iget-object v0, v3, Lgyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 47
    add-int/lit8 v2, v2, -0x1

    .line 48
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 49
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50
    :cond_4
    new-instance v0, Lgyt;

    iget-object v1, v3, Lgyu;->a:Ljava/util/ArrayList;

    .line 51
    invoke-direct {v0, v1}, Lgyt;-><init>(Ljava/util/ArrayList;)V

    .line 53
    :cond_5
    invoke-super {p0, p1, p2}, Lhku;->a(Lhlw;Lhlh;)V

    .line 54
    return-void
.end method
