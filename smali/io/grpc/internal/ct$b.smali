.class final Lio/grpc/internal/ct$b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/dv;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/ct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field private a:Lio/grpc/internal/at;

.field private b:Ljava/net/SocketAddress;

.field private synthetic c:Lio/grpc/internal/ct;


# direct methods
.method constructor <init>(Lio/grpc/internal/ct;Lio/grpc/internal/at;Ljava/net/SocketAddress;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    .line 3
    iput-object p3, p0, Lio/grpc/internal/ct$b;->b:Ljava/net/SocketAddress;

    .line 4
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 5
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 6
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 8
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel$TransportListener"

    const-string v3, "transportReady"

    const-string v4, "[{0}] {1} for {2} is ready"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 10
    iget-object v8, v8, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 11
    aput-object v8, v5, v7

    iget-object v8, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    invoke-interface {v8}, Lio/grpc/internal/at;->B_()Lio/grpc/internal/dg;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v8, 0x2

    iget-object v9, p0, Lio/grpc/internal/ct$b;->b:Ljava/net/SocketAddress;

    aput-object v9, v5, v8

    .line 12
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 13
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 14
    iget-object v1, v0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    .line 15
    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 16
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 17
    iget-object v2, v0, Lio/grpc/internal/ct;->r:Lhlw;

    .line 19
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 20
    const/4 v3, 0x0

    iput-object v3, v0, Lio/grpc/internal/ct;->j:Lio/grpc/internal/r;

    .line 22
    if-eqz v2, :cond_4

    .line 23
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 24
    iget-object v0, v0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 25
    if-nez v0, :cond_3

    move v0, v6

    :goto_0
    const-string v3, "Unexpected non-null activeTransport"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 37
    :cond_1
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 39
    iget-object v0, v0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 40
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 45
    if-eqz v2, :cond_2

    .line 46
    iget-object v0, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    invoke-interface {v0, v2}, Lio/grpc/internal/at;->a(Lhlw;)V

    .line 47
    :cond_2
    return-void

    :cond_3
    move v0, v7

    .line 25
    goto :goto_0

    .line 26
    :cond_4
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 27
    iget-object v0, v0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 28
    iget-object v3, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    if-ne v0, v3, :cond_1

    .line 29
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    sget-object v3, Lhkj;->b:Lhkj;

    .line 30
    invoke-virtual {v0, v3}, Lio/grpc/internal/ct;->a(Lhkj;)V

    .line 31
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    iget-object v3, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    .line 32
    iput-object v3, v0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 34
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 35
    const/4 v3, 0x0

    iput-object v3, v0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    goto :goto_1

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 42
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 43
    iget-object v1, v1, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 44
    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0
.end method

.method public final a(Lhlw;)V
    .locals 14

    .prologue
    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 51
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 52
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 54
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel$TransportListener"

    const-string v3, "transportShutdown"

    const-string v4, "[{0}] {1} for {2} is being shutdown with status {3}"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 56
    iget-object v8, v8, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 57
    aput-object v8, v5, v7

    iget-object v8, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    invoke-interface {v8}, Lio/grpc/internal/at;->B_()Lio/grpc/internal/dg;

    move-result-object v8

    aput-object v8, v5, v6

    iget-object v8, p0, Lio/grpc/internal/ct$b;->b:Ljava/net/SocketAddress;

    aput-object v8, v5, v9

    const/4 v8, 0x3

    aput-object p1, v5, v8

    .line 58
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 60
    iget-object v8, v0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    .line 61
    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 63
    iget-object v0, v0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 65
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 66
    sget-object v1, Lhkj;->e:Lhkj;

    if-ne v0, v1, :cond_1

    .line 67
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 69
    iget-object v0, v0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 70
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 133
    :goto_0
    return-void

    .line 72
    :cond_1
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 73
    iget-object v0, v0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 74
    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    if-ne v0, v1, :cond_3

    .line 75
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    sget-object v1, Lhkj;->d:Lhkj;

    .line 76
    invoke-virtual {v0, v1}, Lio/grpc/internal/ct;->a(Lhkj;)V

    .line 77
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    const/4 v1, 0x0

    .line 78
    iput-object v1, v0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 80
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 81
    const/4 v1, 0x0

    iput v1, v0, Lio/grpc/internal/ct;->i:I

    .line 129
    :cond_2
    :goto_1
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 130
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 131
    iget-object v0, v0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 132
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 83
    :cond_3
    :try_start_3
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 84
    iget-object v0, v0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 85
    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    if-ne v0, v1, :cond_2

    .line 86
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 87
    iget-object v0, v0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 89
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 90
    sget-object v1, Lhkj;->a:Lhkj;

    if-ne v0, v1, :cond_6

    move v0, v6

    :goto_2
    const-string v1, "Expected state is CONNECTING, actual state is %s"

    iget-object v2, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 92
    iget-object v2, v2, Lio/grpc/internal/ct;->q:Lhkk;

    .line 94
    iget-object v2, v2, Lhkk;->a:Lhkj;

    .line 95
    invoke-static {v0, v1, v2}, Lgtn;->b(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 97
    iget v1, v0, Lio/grpc/internal/ct;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lio/grpc/internal/ct;->i:I

    .line 99
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 100
    iget v0, v0, Lio/grpc/internal/ct;->i:I

    .line 101
    iget-object v1, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 102
    iget-object v1, v1, Lio/grpc/internal/ct;->h:Lhks;

    .line 104
    iget-object v1, v1, Lhks;->a:Ljava/util/List;

    .line 105
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 106
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 107
    const/4 v1, 0x0

    iput-object v1, v0, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 109
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 110
    const/4 v1, 0x0

    iput v1, v0, Lio/grpc/internal/ct;->i:I

    .line 112
    iget-object v9, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 115
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v6

    :goto_3
    const-string v1, "The error status must not be OK"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 116
    new-instance v0, Lhkk;

    sget-object v1, Lhkj;->c:Lhkj;

    invoke-direct {v0, v1, p1}, Lhkk;-><init>(Lhkj;Lhlw;)V

    .line 117
    invoke-virtual {v9, v0}, Lio/grpc/internal/ct;->a(Lhkk;)V

    .line 118
    iget-object v0, v9, Lio/grpc/internal/ct;->j:Lio/grpc/internal/r;

    if-nez v0, :cond_4

    .line 119
    iget-object v0, v9, Lio/grpc/internal/ct;->c:Lio/grpc/internal/s;

    invoke-virtual {v0}, Lio/grpc/internal/s;->a()Lio/grpc/internal/r;

    move-result-object v0

    iput-object v0, v9, Lio/grpc/internal/ct;->j:Lio/grpc/internal/r;

    .line 120
    :cond_4
    iget-object v0, v9, Lio/grpc/internal/ct;->j:Lio/grpc/internal/r;

    .line 121
    invoke-virtual {v0}, Lio/grpc/internal/r;->a()J

    move-result-wide v0

    iget-object v2, v9, Lio/grpc/internal/ct;->k:Lgts;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lgts;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sub-long v10, v0, v2

    .line 122
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 123
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel"

    const-string v3, "scheduleBackoff"

    const-string v4, "[{0}] Scheduling backoff for {1} ns"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v9, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    aput-object v13, v5, v12

    const/4 v12, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v5, v12

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    :cond_5
    iget-object v0, v9, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_8

    move v0, v6

    :goto_4
    const-string v1, "previous reconnectTask is not done"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 125
    iget-object v0, v9, Lio/grpc/internal/ct;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lio/grpc/internal/df;

    new-instance v2, Lio/grpc/internal/cv;

    invoke-direct {v2, v9}, Lio/grpc/internal/cv;-><init>(Lio/grpc/internal/ct;)V

    invoke-direct {v1, v2}, Lio/grpc/internal/df;-><init>(Ljava/lang/Runnable;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v10, v11, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, v9, Lio/grpc/internal/ct;->l:Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_1

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 134
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 135
    iget-object v1, v1, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 136
    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0

    :cond_6
    move v0, v7

    .line 90
    goto/16 :goto_2

    :cond_7
    move v0, v7

    .line 115
    goto/16 :goto_3

    :cond_8
    move v0, v7

    .line 124
    goto :goto_4

    .line 127
    :cond_9
    :try_start_5
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 128
    invoke-virtual {v0}, Lio/grpc/internal/ct;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    .line 49
    invoke-virtual {v0, v1, p1}, Lio/grpc/internal/ct;->a(Lio/grpc/internal/at;Z)V

    .line 50
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 137
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 138
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 140
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel$TransportListener"

    const-string v3, "transportTerminated"

    const-string v4, "[{0}] {1} for {2} is terminated"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v8, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 142
    iget-object v8, v8, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 143
    aput-object v8, v5, v7

    iget-object v8, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    invoke-interface {v8}, Lio/grpc/internal/at;->B_()Lio/grpc/internal/dg;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v8, 0x2

    iget-object v9, p0, Lio/grpc/internal/ct$b;->b:Ljava/net/SocketAddress;

    aput-object v9, v5, v8

    .line 144
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    .line 146
    invoke-virtual {v0, v1, v7}, Lio/grpc/internal/ct;->a(Lio/grpc/internal/at;Z)V

    .line 147
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 148
    iget-object v8, v0, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    .line 149
    monitor-enter v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 150
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 151
    iget-object v0, v0, Lio/grpc/internal/ct;->m:Ljava/util/Collection;

    .line 152
    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 154
    iget-object v0, v0, Lio/grpc/internal/ct;->q:Lhkk;

    .line 156
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 157
    sget-object v1, Lhkj;->e:Lhkj;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 158
    iget-object v0, v0, Lio/grpc/internal/ct;->m:Ljava/util/Collection;

    .line 159
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 161
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    sget-object v0, Lio/grpc/internal/ct;->a:Ljava/util/logging/Logger;

    .line 163
    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.InternalSubchannel$TransportListener"

    const-string v3, "transportTerminated"

    const-string v4, "[{0}] Terminated in transportTerminated()"

    iget-object v5, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 164
    iget-object v5, v5, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 165
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 166
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 167
    invoke-virtual {v0}, Lio/grpc/internal/ct;->d()V

    .line 168
    :cond_2
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 170
    iget-object v0, v0, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 171
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 176
    iget-object v0, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 177
    iget-object v0, v0, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 178
    iget-object v1, p0, Lio/grpc/internal/ct$b;->a:Lio/grpc/internal/at;

    if-eq v0, v1, :cond_3

    move v0, v6

    :goto_0
    const-string v1, "activeTransport still points to this transport. Seems transportShutdown() was not called."

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 179
    return-void

    .line 168
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 173
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ct$b;->c:Lio/grpc/internal/ct;

    .line 174
    iget-object v1, v1, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    .line 175
    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0

    :cond_3
    move v0, v7

    .line 178
    goto :goto_0
.end method
