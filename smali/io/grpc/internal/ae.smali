.class final Lio/grpc/internal/ae;
.super Lhjx;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/ae$a;,
        Lio/grpc/internal/ae$d;,
        Lio/grpc/internal/ae$b;,
        Lio/grpc/internal/ae$c;
    }
.end annotation


# static fields
.field private static i:Ljava/util/logging/Logger;

.field private static j:[B


# instance fields
.field public final a:Lhlk;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lhkl;

.field public d:Lio/grpc/internal/al;

.field public volatile e:Z

.field public f:Z

.field public g:Lhkp;

.field public h:Lhki;

.field private volatile k:Ljava/util/concurrent/ScheduledFuture;

.field private l:Z

.field private m:Lhjv;

.field private n:Z

.field private o:Z

.field private p:Lio/grpc/internal/ae$b;

.field private q:Lhkl$b;

.field private r:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 178
    const-class v0, Lio/grpc/internal/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/ae;->i:Ljava/util/logging/Logger;

    .line 179
    const-string v0, "gzip"

    const-string v1, "US-ASCII"

    .line 180
    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Lio/grpc/internal/ae;->j:[B

    .line 181
    return-void
.end method

.method constructor <init>(Lhlk;Ljava/util/concurrent/Executor;Lhjv;Lio/grpc/internal/ae$b;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lhjx;-><init>()V

    .line 2
    new-instance v0, Lio/grpc/internal/ae$c;

    .line 3
    invoke-direct {v0, p0}, Lio/grpc/internal/ae$c;-><init>(Lio/grpc/internal/ae;)V

    .line 4
    iput-object v0, p0, Lio/grpc/internal/ae;->q:Lhkl$b;

    .line 6
    sget-object v0, Lhkp;->a:Lhkp;

    .line 7
    iput-object v0, p0, Lio/grpc/internal/ae;->g:Lhkp;

    .line 9
    sget-object v0, Lhki;->a:Lhki;

    .line 10
    iput-object v0, p0, Lio/grpc/internal/ae;->h:Lhki;

    .line 11
    iput-object p1, p0, Lio/grpc/internal/ae;->a:Lhlk;

    .line 12
    invoke-static {}, Lhcw;->h()Ljava/util/concurrent/Executor;

    move-result-object v0

    if-ne p2, v0, :cond_1

    .line 13
    new-instance v0, Lio/grpc/internal/er;

    invoke-direct {v0}, Lio/grpc/internal/er;-><init>()V

    .line 14
    :goto_0
    iput-object v0, p0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    .line 15
    invoke-static {}, Lhkl;->a()Lhkl;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    .line 17
    iget-object v0, p1, Lhlk;->a:Lhlk$c;

    .line 18
    sget-object v1, Lhlk$c;->a:Lhlk$c;

    if-eq v0, v1, :cond_0

    .line 20
    iget-object v0, p1, Lhlk;->a:Lhlk$c;

    .line 21
    sget-object v1, Lhlk$c;->b:Lhlk$c;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lio/grpc/internal/ae;->l:Z

    .line 22
    iput-object p3, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 23
    iput-object p4, p0, Lio/grpc/internal/ae;->p:Lio/grpc/internal/ae$b;

    .line 24
    iput-object p5, p0, Lio/grpc/internal/ae;->r:Ljava/util/concurrent/ScheduledExecutorService;

    .line 25
    return-void

    .line 14
    :cond_1
    new-instance v0, Lio/grpc/internal/es;

    invoke-direct {v0, p2}, Lio/grpc/internal/es;-><init>(Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 21
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Lhjy;Lhlw;Lhlh;)V
    .locals 0

    .prologue
    .line 176
    invoke-virtual {p0, p1, p2}, Lhjy;->a(Lhlw;Lhlh;)V

    .line 177
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 156
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Not started"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 157
    iget-boolean v0, p0, Lio/grpc/internal/ae;->n:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "call was cancelled"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 158
    iget-boolean v0, p0, Lio/grpc/internal/ae;->o:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "call already half-closed"

    invoke-static {v2, v0}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 159
    iput-boolean v1, p0, Lio/grpc/internal/ae;->o:Z

    .line 160
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v0}, Lio/grpc/internal/al;->d()V

    .line 161
    return-void

    :cond_1
    move v0, v2

    .line 156
    goto :goto_0

    :cond_2
    move v0, v2

    .line 157
    goto :goto_1
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Not started"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 137
    if-ltz p1, :cond_1

    :goto_1
    const-string v0, "Number requested must be non-negative"

    invoke-static {v1, v0}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v0, p1}, Lio/grpc/internal/al;->c(I)V

    .line 139
    return-void

    :cond_0
    move v0, v2

    .line 136
    goto :goto_0

    :cond_1
    move v1, v2

    .line 137
    goto :goto_1
.end method

.method public final a(Lhjy;Lhlh;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Already started"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 27
    iget-boolean v0, p0, Lio/grpc/internal/ae;->n:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "call was cancelled"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 28
    const-string v0, "observer"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const-string v0, "headers"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    invoke-virtual {v0}, Lhkl;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 31
    sget-object v0, Lio/grpc/internal/ef;->a:Lio/grpc/internal/ef;

    iput-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    .line 32
    iget-object v0, p0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/grpc/internal/af;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/af;-><init>(Lio/grpc/internal/ae;Lhjy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 121
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 26
    goto :goto_0

    :cond_2
    move v0, v2

    .line 27
    goto :goto_1

    .line 34
    :cond_3
    iget-object v0, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 35
    iget-object v3, v0, Lhjv;->f:Ljava/lang/String;

    .line 37
    if-eqz v3, :cond_4

    .line 38
    iget-object v0, p0, Lio/grpc/internal/ae;->h:Lhki;

    .line 39
    iget-object v0, v0, Lhki;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkh;

    .line 41
    if-nez v0, :cond_5

    .line 42
    sget-object v0, Lio/grpc/internal/ef;->a:Lio/grpc/internal/ef;

    iput-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    .line 43
    iget-object v0, p0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lio/grpc/internal/ag;

    invoke-direct {v1, p0, p1, v3}, Lio/grpc/internal/ag;-><init>(Lio/grpc/internal/ae;Lhjy;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2

    .line 45
    :cond_4
    sget-object v0, Lhkg;->a:Lhkh;

    .line 46
    :cond_5
    iget-object v3, p0, Lio/grpc/internal/ae;->g:Lhkp;

    iget-boolean v4, p0, Lio/grpc/internal/ae;->f:Z

    .line 47
    sget-object v5, Lio/grpc/internal/cf;->c:Lhlh$e;

    invoke-virtual {p2, v5}, Lhlh;->b(Lhlh$e;)V

    .line 48
    sget-object v5, Lhkg;->a:Lhkh;

    if-eq v0, v5, :cond_6

    .line 49
    sget-object v5, Lio/grpc/internal/cf;->c:Lhlh$e;

    invoke-interface {v0}, Lhkh;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v5, v6}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 50
    :cond_6
    sget-object v5, Lio/grpc/internal/cf;->d:Lhlh$e;

    invoke-virtual {p2, v5}, Lhlh;->b(Lhlh$e;)V

    .line 52
    invoke-static {v3}, Lio/grpc/internal/av;->a(Lhkp;)[B

    move-result-object v3

    .line 53
    array-length v5, v3

    if-eqz v5, :cond_7

    .line 54
    sget-object v5, Lio/grpc/internal/cf;->d:Lhlh$e;

    invoke-virtual {p2, v5, v3}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 55
    :cond_7
    sget-object v3, Lio/grpc/internal/cf;->e:Lhlh$e;

    invoke-virtual {p2, v3}, Lhlh;->b(Lhlh$e;)V

    .line 56
    sget-object v3, Lio/grpc/internal/cf;->f:Lhlh$e;

    invoke-virtual {p2, v3}, Lhlh;->b(Lhlh$e;)V

    .line 57
    if-eqz v4, :cond_8

    .line 58
    sget-object v3, Lio/grpc/internal/cf;->f:Lhlh$e;

    sget-object v4, Lio/grpc/internal/ae;->j:[B

    invoke-virtual {p2, v3, v4}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 59
    :cond_8
    invoke-virtual {p0}, Lio/grpc/internal/ae;->c()Lhkm;

    move-result-object v4

    .line 60
    if-eqz v4, :cond_e

    invoke-static {}, Lhkm;->a()Z

    move-result v3

    if-eqz v3, :cond_e

    move v3, v1

    .line 61
    :goto_3
    if-nez v3, :cond_11

    .line 62
    iget-object v3, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 63
    iget-object v3, v3, Lhjv;->b:Lhkm;

    .line 64
    iget-object v5, p0, Lio/grpc/internal/ae;->c:Lhkl;

    .line 65
    invoke-virtual {v5}, Lhkl;->f()Lhkm;

    move-result-object v5

    .line 67
    sget-object v6, Lio/grpc/internal/cf;->b:Lhlh$e;

    invoke-virtual {p2, v6}, Lhlh;->b(Lhlh$e;)V

    .line 68
    if-eqz v4, :cond_9

    .line 69
    const-wide/16 v6, 0x0

    invoke-static {}, Lhkm;->c()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 70
    sget-object v8, Lio/grpc/internal/cf;->b:Lhlh$e;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {p2, v8, v9}, Lhlh;->a(Lhlh$e;Ljava/lang/Object;)V

    .line 72
    sget-object v8, Lio/grpc/internal/ae;->i:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v8

    if-eqz v8, :cond_9

    if-eq v5, v4, :cond_f

    .line 83
    :cond_9
    :goto_4
    iget-object v1, p0, Lio/grpc/internal/ae;->p:Lio/grpc/internal/ae$b;

    new-instance v2, Lio/grpc/internal/eh;

    iget-object v3, p0, Lio/grpc/internal/ae;->a:Lhlk;

    iget-object v5, p0, Lio/grpc/internal/ae;->m:Lhjv;

    invoke-direct {v2, v3, p2, v5}, Lio/grpc/internal/eh;-><init>(Lhlk;Lhlh;Lhjv;)V

    invoke-virtual {v1, v2}, Lio/grpc/internal/ae$b;->a(Lhlc;)Lio/grpc/internal/am;

    move-result-object v1

    .line 84
    iget-object v2, p0, Lio/grpc/internal/ae;->c:Lhkl;

    invoke-virtual {v2}, Lhkl;->c()Lhkl;

    move-result-object v2

    .line 85
    :try_start_0
    iget-object v3, p0, Lio/grpc/internal/ae;->a:Lhlk;

    iget-object v5, p0, Lio/grpc/internal/ae;->m:Lhjv;

    invoke-interface {v1, v3, p2, v5}, Lio/grpc/internal/am;->a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;

    move-result-object v1

    iput-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    iget-object v1, p0, Lio/grpc/internal/ae;->c:Lhkl;

    invoke-virtual {v1, v2}, Lhkl;->a(Lhkl;)V

    .line 90
    :goto_5
    iget-object v1, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 91
    iget-object v1, v1, Lhjv;->d:Ljava/lang/String;

    .line 92
    if-eqz v1, :cond_a

    .line 93
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    iget-object v2, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 94
    iget-object v2, v2, Lhjv;->d:Ljava/lang/String;

    .line 95
    invoke-interface {v1, v2}, Lio/grpc/internal/al;->a(Ljava/lang/String;)V

    .line 96
    :cond_a
    iget-object v1, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 97
    iget-object v1, v1, Lhjv;->i:Ljava/lang/Integer;

    .line 98
    if-eqz v1, :cond_b

    .line 99
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    iget-object v2, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 100
    iget-object v2, v2, Lhjv;->i:Ljava/lang/Integer;

    .line 101
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Lio/grpc/internal/al;->b(I)V

    .line 102
    :cond_b
    iget-object v1, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 103
    iget-object v1, v1, Lhjv;->j:Ljava/lang/Integer;

    .line 104
    if-eqz v1, :cond_c

    .line 105
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    iget-object v2, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 106
    iget-object v2, v2, Lhjv;->j:Ljava/lang/Integer;

    .line 107
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Lio/grpc/internal/al;->a(I)V

    .line 108
    :cond_c
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v1, v0}, Lio/grpc/internal/al;->a(Lhkh;)V

    .line 109
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    iget-boolean v1, p0, Lio/grpc/internal/ae;->f:Z

    invoke-interface {v0, v1}, Lio/grpc/internal/al;->a(Z)V

    .line 110
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    iget-object v1, p0, Lio/grpc/internal/ae;->g:Lhkp;

    invoke-interface {v0, v1}, Lio/grpc/internal/al;->a(Lhkp;)V

    .line 111
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    new-instance v1, Lio/grpc/internal/ae$a;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/ae$a;-><init>(Lio/grpc/internal/ae;Lhjy;)V

    invoke-interface {v0, v1}, Lio/grpc/internal/al;->a(Lio/grpc/internal/fb;)V

    .line 112
    iget-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    iget-object v1, p0, Lio/grpc/internal/ae;->q:Lhkl$b;

    invoke-static {}, Lhcw;->h()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lhkl;->a(Lhkl$b;Ljava/util/concurrent/Executor;)V

    .line 113
    if-eqz v4, :cond_d

    iget-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    .line 114
    invoke-virtual {v0}, Lhkl;->f()Lhkm;

    move-result-object v0

    if-eq v0, v4, :cond_d

    iget-object v0, p0, Lio/grpc/internal/ae;->r:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_d

    .line 116
    invoke-static {}, Lhkm;->c()J

    move-result-wide v0

    .line 117
    iget-object v2, p0, Lio/grpc/internal/ae;->r:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lio/grpc/internal/df;

    new-instance v4, Lio/grpc/internal/ae$d;

    invoke-direct {v4, p0, v0, v1}, Lio/grpc/internal/ae$d;-><init>(Lio/grpc/internal/ae;J)V

    invoke-direct {v3, v4}, Lio/grpc/internal/df;-><init>(Ljava/lang/Runnable;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    .line 118
    iput-object v0, p0, Lio/grpc/internal/ae;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 119
    :cond_d
    iget-boolean v0, p0, Lio/grpc/internal/ae;->e:Z

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lio/grpc/internal/ae;->b()V

    goto/16 :goto_2

    :cond_e
    move v3, v2

    .line 60
    goto/16 :goto_3

    .line 74
    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    const-string v8, "Call timeout set to \'%d\' ns, due to context deadline."

    new-array v9, v1, [Ljava/lang/Object;

    .line 76
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v2

    .line 77
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    if-nez v3, :cond_10

    .line 79
    const-string v1, " Explicit call timeout was not set."

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :goto_6
    sget-object v1, Lio/grpc/internal/ae;->i:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v3, "io.grpc.internal.ClientCallImpl"

    const-string v6, "logIfContextNarrowedTimeout"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v6, v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 80
    :cond_10
    invoke-static {}, Lhkm;->c()J

    move-result-wide v6

    .line 81
    const-string v3, " Explicit call timeout was \'%d\' ns."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 88
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ae;->c:Lhkl;

    invoke-virtual {v1, v2}, Lhkl;->a(Lhkl;)V

    throw v0

    .line 89
    :cond_11
    new-instance v1, Lio/grpc/internal/ca;

    sget-object v2, Lhlw;->e:Lhlw;

    invoke-direct {v1, v2}, Lio/grpc/internal/ca;-><init>(Lhlw;)V

    iput-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    goto/16 :goto_5
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Not started"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 163
    iget-boolean v0, p0, Lio/grpc/internal/ae;->n:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    const-string v3, "call was cancelled"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 164
    iget-boolean v0, p0, Lio/grpc/internal/ae;->o:Z

    if-nez v0, :cond_3

    :goto_2
    const-string v0, "call was half-closed"

    invoke-static {v1, v0}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 165
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ae;->a:Lhlk;

    .line 166
    iget-object v0, v0, Lhlk;->c:Lhlk$b;

    invoke-interface {v0, p1}, Lhlk$b;->a(Ljava/lang/Object;)Ljava/io/InputStream;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v1, v0}, Lio/grpc/internal/al;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    iget-boolean v0, p0, Lio/grpc/internal/ae;->l:Z

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v0}, Lio/grpc/internal/al;->f()V

    .line 175
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 162
    goto :goto_0

    :cond_2
    move v0, v2

    .line 163
    goto :goto_1

    :cond_3
    move v1, v2

    .line 164
    goto :goto_2

    .line 170
    :catch_0
    move-exception v0

    .line 171
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    sget-object v2, Lhlw;->c:Lhlw;

    invoke-virtual {v2, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    const-string v2, "Failed to stream message"

    invoke-virtual {v0, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    invoke-interface {v1, v0}, Lio/grpc/internal/al;->a(Lhlw;)V

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 140
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 141
    new-instance v5, Ljava/util/concurrent/CancellationException;

    const-string v0, "Cancelled without a message or cause"

    invoke-direct {v5, v0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    .line 142
    sget-object v0, Lio/grpc/internal/ae;->i:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ClientCallImpl"

    const-string v3, "cancel"

    const-string v4, "Cancelling without a message or cause is suboptimal"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p2, v5

    .line 143
    :cond_0
    iget-boolean v0, p0, Lio/grpc/internal/ae;->n:Z

    if-eqz v0, :cond_1

    .line 154
    :goto_0
    return-void

    .line 145
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/ae;->n:Z

    .line 146
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    if-eqz v0, :cond_4

    .line 147
    sget-object v0, Lhlw;->c:Lhlw;

    .line 148
    if-eqz p1, :cond_2

    .line 149
    invoke-virtual {v0, p1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 150
    :cond_2
    if-eqz p2, :cond_3

    .line 151
    invoke-virtual {v0, p2}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    .line 152
    :cond_3
    iget-object v1, p0, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    invoke-interface {v1, v0}, Lio/grpc/internal/al;->a(Lhlw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :cond_4
    invoke-virtual {p0}, Lio/grpc/internal/ae;->b()V

    goto :goto_0

    .line 155
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lio/grpc/internal/ae;->b()V

    throw v0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    iget-object v1, p0, Lio/grpc/internal/ae;->q:Lhkl$b;

    invoke-virtual {v0, v1}, Lhkl;->a(Lhkl$b;)V

    .line 123
    iget-object v0, p0, Lio/grpc/internal/ae;->k:Ljava/util/concurrent/ScheduledFuture;

    .line 124
    if-eqz v0, :cond_0

    .line 125
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 126
    :cond_0
    return-void
.end method

.method final c()Lhkm;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lio/grpc/internal/ae;->m:Lhjv;

    .line 128
    iget-object v1, v0, Lhjv;->b:Lhkm;

    .line 129
    iget-object v0, p0, Lio/grpc/internal/ae;->c:Lhkl;

    invoke-virtual {v0}, Lhkl;->f()Lhkm;

    move-result-object v0

    .line 130
    if-nez v1, :cond_0

    .line 135
    :goto_0
    return-object v0

    .line 132
    :cond_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 133
    goto :goto_0

    .line 134
    :cond_1
    invoke-static {}, Lhkm;->b()Lhkm;

    move-result-object v0

    goto :goto_0
.end method
