.class final Lio/grpc/internal/bw;
.super Lhlm;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/bw$b;,
        Lio/grpc/internal/bw$c;,
        Lio/grpc/internal/bw$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field private static n:Z

.field private static o:Z


# instance fields
.field public b:Lio/grpc/internal/bw$a;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Lio/grpc/internal/ei;

.field public f:Z

.field public g:Ljava/util/concurrent/ScheduledExecutorService;

.field public h:Ljava/util/concurrent/ExecutorService;

.field public i:Ljava/util/concurrent/ScheduledFuture;

.field public j:Z

.field public k:Lhlo;

.field public final l:Ljava/lang/Runnable;

.field public final m:Ljava/lang/Runnable;

.field private p:Ljava/lang/String;

.field private q:Lio/grpc/internal/ew;

.field private r:Lio/grpc/internal/ew;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lio/grpc/internal/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/bw;->a:Ljava/util/logging/Logger;

    .line 59
    invoke-static {}, Lio/grpc/internal/bw;->e()Z

    move-result v0

    sput-boolean v0, Lio/grpc/internal/bw;->n:Z

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lio/grpc/internal/bw;->o:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lhjs;Lio/grpc/internal/ew;Lio/grpc/internal/ew;Lio/grpc/internal/ei;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Lhlm;-><init>()V

    .line 3
    new-instance v0, Lio/grpc/internal/bw$b;

    invoke-direct {v0}, Lio/grpc/internal/bw$b;-><init>()V

    .line 5
    iput-object v0, p0, Lio/grpc/internal/bw;->b:Lio/grpc/internal/bw$a;

    .line 6
    new-instance v0, Lio/grpc/internal/bx;

    invoke-direct {v0, p0}, Lio/grpc/internal/bx;-><init>(Lio/grpc/internal/bw;)V

    iput-object v0, p0, Lio/grpc/internal/bw;->l:Ljava/lang/Runnable;

    .line 7
    new-instance v0, Lio/grpc/internal/by;

    invoke-direct {v0, p0}, Lio/grpc/internal/by;-><init>(Lio/grpc/internal/bw;)V

    iput-object v0, p0, Lio/grpc/internal/bw;->m:Ljava/lang/Runnable;

    .line 8
    iput-object p3, p0, Lio/grpc/internal/bw;->q:Lio/grpc/internal/ew;

    .line 9
    iput-object p4, p0, Lio/grpc/internal/bw;->r:Lio/grpc/internal/ew;

    .line 10
    const-string v1, "//"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 11
    invoke-virtual {v1}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v2, "nameUri (%s) doesn\'t have an authority"

    invoke-static {v0, v2, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/bw;->p:Ljava/lang/String;

    .line 12
    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v2, "host"

    invoke-static {v0, v2}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/bw;->c:Ljava/lang/String;

    .line 13
    invoke-virtual {v1}, Ljava/net/URI;->getPort()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 14
    sget-object v0, Lhln;->a:Lhjs$b;

    .line 15
    iget-object v1, p2, Lhjs;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 16
    check-cast v0, Ljava/lang/Integer;

    .line 17
    if-eqz v0, :cond_1

    .line 18
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lio/grpc/internal/bw;->d:I

    .line 21
    :goto_1
    iput-object p5, p0, Lio/grpc/internal/bw;->e:Lio/grpc/internal/ei;

    .line 22
    return-void

    .line 10
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x45

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "name \'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' doesn\'t contain a port, and default port is not set in params"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_2
    invoke-virtual {v1}, Ljava/net/URI;->getPort()I

    move-result v0

    iput v0, p0, Lio/grpc/internal/bw;->d:I

    goto :goto_1
.end method

.method private final d()V
    .locals 2

    .prologue
    .line 37
    iget-boolean v0, p0, Lio/grpc/internal/bw;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/grpc/internal/bw;->f:Z

    if-eqz v0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/bw;->h:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lio/grpc/internal/bw;->l:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static e()Z
    .locals 6

    .prologue
    .line 51
    :try_start_0
    const-string v0, "javax.naming.directory.InitialDirContext"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 52
    const-string v0, "com.sun.jndi.dns.DnsContextFactory"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 54
    :catch_0
    move-exception v5

    .line 55
    sget-object v0, Lio/grpc/internal/bw;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.DnsNameResolver"

    const-string v3, "jndiAvailable"

    const-string v4, "Unable to find JNDI DNS resolver, skipping"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lio/grpc/internal/bw;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized a(Lhlo;)V
    .locals 2

    .prologue
    .line 24
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/bw;->k:Lhlo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "already started"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lio/grpc/internal/bw;->q:Lio/grpc/internal/ew;

    .line 26
    sget-object v1, Lio/grpc/internal/et;->a:Lio/grpc/internal/et;

    invoke-virtual {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;)Ljava/lang/Object;

    move-result-object v0

    .line 27
    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lio/grpc/internal/bw;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 28
    iget-object v0, p0, Lio/grpc/internal/bw;->r:Lio/grpc/internal/ew;

    .line 29
    sget-object v1, Lio/grpc/internal/et;->a:Lio/grpc/internal/et;

    invoke-virtual {v1, v0}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;)Ljava/lang/Object;

    move-result-object v0

    .line 30
    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lio/grpc/internal/bw;->h:Ljava/util/concurrent/ExecutorService;

    .line 31
    const-string v0, "listener"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlo;

    iput-object v0, p0, Lio/grpc/internal/bw;->k:Lhlo;

    .line 32
    invoke-direct {p0}, Lio/grpc/internal/bw;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    monitor-exit p0

    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/grpc/internal/bw;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 43
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lio/grpc/internal/bw;->f:Z

    .line 44
    iget-object v0, p0, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 45
    iget-object v0, p0, Lio/grpc/internal/bw;->i:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 46
    :cond_2
    iget-object v0, p0, Lio/grpc/internal/bw;->g:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_3

    .line 47
    iget-object v0, p0, Lio/grpc/internal/bw;->q:Lio/grpc/internal/ew;

    iget-object v1, p0, Lio/grpc/internal/bw;->g:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lio/grpc/internal/bw;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 48
    :cond_3
    iget-object v0, p0, Lio/grpc/internal/bw;->h:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lio/grpc/internal/bw;->r:Lio/grpc/internal/ew;

    iget-object v1, p0, Lio/grpc/internal/bw;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1}, Lio/grpc/internal/et;->a(Lio/grpc/internal/ew;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lio/grpc/internal/bw;->h:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/bw;->k:Lhlo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "not started"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 35
    invoke-direct {p0}, Lio/grpc/internal/bw;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    monitor-exit p0

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
