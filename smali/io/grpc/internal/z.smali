.class final Lio/grpc/internal/z;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/z$e;,
        Lio/grpc/internal/z$d;,
        Lio/grpc/internal/z$c;,
        Lio/grpc/internal/z$b;,
        Lio/grpc/internal/z$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/logging/Logger;

.field public static final b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

.field private static f:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;


# instance fields
.field public final c:Lhpr;

.field public final d:Lhlh$e;

.field public final e:Lio/grpc/internal/z$e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    const-class v0, Lio/grpc/internal/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/z;->a:Ljava/util/logging/Logger;

    .line 79
    const-class v0, Lio/grpc/internal/z$a;

    const-string v1, "a"

    .line 80
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/z;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    .line 81
    const-class v0, Lio/grpc/internal/z$c;

    const-string v1, "a"

    .line 82
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/z;->f:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    .line 83
    new-instance v0, Lhll;

    invoke-direct {v0}, Lhll;-><init>()V

    invoke-static {v0}, Lio/grpc/internal/av;->a(Lhll;)V

    .line 85
    return-void
.end method

.method constructor <init>(Lhpr;Lhpw;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lio/grpc/internal/z$e;

    invoke-direct {v0, p0}, Lio/grpc/internal/z$e;-><init>(Lio/grpc/internal/z;)V

    iput-object v0, p0, Lio/grpc/internal/z;->e:Lio/grpc/internal/z$e;

    .line 3
    new-instance v0, Lio/grpc/internal/z$d;

    invoke-direct {v0, p0}, Lio/grpc/internal/z$d;-><init>(Lio/grpc/internal/z;)V

    .line 4
    const-string v0, "censusTracer"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpr;

    iput-object v0, p0, Lio/grpc/internal/z;->c:Lhpr;

    .line 5
    const-string v0, "censusPropagationBinaryFormat"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    const-string v0, "grpc-trace-bin"

    new-instance v1, Lio/grpc/internal/aa;

    invoke-direct {v1, p2}, Lio/grpc/internal/aa;-><init>(Lhpw;)V

    .line 7
    invoke-static {v0, v1}, Lhlh$e;->a(Ljava/lang/String;Lhlh$d;)Lhlh$e;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/z;->d:Lhlh$e;

    .line 8
    return-void
.end method

.method static a(Lhlw;)Lhpc;
    .locals 4

    .prologue
    .line 9
    invoke-static {}, Lhpc;->c()Lhpd;

    move-result-object v2

    .line 11
    iget-object v0, p0, Lhlw;->l:Lhlx;

    .line 12
    invoke-virtual {v0}, Lhlx;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 47
    new-instance v0, Ljava/lang/AssertionError;

    .line 48
    iget-object v1, p0, Lhlw;->l:Lhlx;

    .line 49
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled status code "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 13
    :pswitch_0
    sget-object v0, Lhpm;->b:Lhpm;

    .line 51
    :goto_0
    iget-object v1, p0, Lhlw;->m:Ljava/lang/String;

    .line 52
    if-eqz v1, :cond_0

    .line 54
    iget-object v3, p0, Lhlw;->m:Ljava/lang/String;

    .line 56
    iget-object v1, v0, Lhpm;->t:Ljava/lang/String;

    invoke-static {v1, v3}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    :goto_1
    invoke-virtual {v2, v0}, Lhpd;->a(Lhpm;)Lhpd;

    move-result-object v0

    invoke-virtual {v0}, Lhpd;->a()Lhpc;

    move-result-object v0

    return-object v0

    .line 15
    :pswitch_1
    sget-object v0, Lhpm;->c:Lhpm;

    goto :goto_0

    .line 17
    :pswitch_2
    sget-object v0, Lhpm;->d:Lhpm;

    goto :goto_0

    .line 19
    :pswitch_3
    sget-object v0, Lhpm;->e:Lhpm;

    goto :goto_0

    .line 21
    :pswitch_4
    sget-object v0, Lhpm;->f:Lhpm;

    goto :goto_0

    .line 23
    :pswitch_5
    sget-object v0, Lhpm;->g:Lhpm;

    goto :goto_0

    .line 25
    :pswitch_6
    sget-object v0, Lhpm;->h:Lhpm;

    goto :goto_0

    .line 27
    :pswitch_7
    sget-object v0, Lhpm;->i:Lhpm;

    goto :goto_0

    .line 29
    :pswitch_8
    sget-object v0, Lhpm;->k:Lhpm;

    goto :goto_0

    .line 31
    :pswitch_9
    sget-object v0, Lhpm;->l:Lhpm;

    goto :goto_0

    .line 33
    :pswitch_a
    sget-object v0, Lhpm;->m:Lhpm;

    goto :goto_0

    .line 35
    :pswitch_b
    sget-object v0, Lhpm;->n:Lhpm;

    goto :goto_0

    .line 37
    :pswitch_c
    sget-object v0, Lhpm;->o:Lhpm;

    goto :goto_0

    .line 39
    :pswitch_d
    sget-object v0, Lhpm;->p:Lhpm;

    goto :goto_0

    .line 41
    :pswitch_e
    sget-object v0, Lhpm;->q:Lhpm;

    goto :goto_0

    .line 43
    :pswitch_f
    sget-object v0, Lhpm;->r:Lhpm;

    goto :goto_0

    .line 45
    :pswitch_10
    sget-object v0, Lhpm;->j:Lhpm;

    goto :goto_0

    .line 58
    :cond_1
    new-instance v1, Lhpm;

    iget-object v0, v0, Lhpm;->s:Lhpn;

    invoke-direct {v1, v0, v3}, Lhpm;-><init>(Lhpn;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 12
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method static a(ZLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    if-eqz p0, :cond_0

    const-string v0, "Recv"

    .line 76
    :goto_0
    const/16 v1, 0x2f

    const/16 v2, 0x2e

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 75
    :cond_0
    const-string v0, "Sent"

    goto :goto_0
.end method

.method static synthetic a(Lhph;Lhpg;IJJ)V
    .locals 1

    .prologue
    .line 77
    invoke-static/range {p0 .. p6}, Lio/grpc/internal/z;->b(Lhph;Lhpg;IJJ)V

    return-void
.end method

.method private static b(Lhph;Lhpg;IJJ)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    .line 62
    int-to-long v2, p2

    .line 63
    new-instance v1, Lhpf;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lhpf;-><init>(B)V

    const-string v0, "type"

    .line 64
    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpg;

    invoke-virtual {v1, v0}, Lhpf;->a(Lhpg;)Lhpf;

    move-result-object v0

    .line 65
    invoke-virtual {v0, v2, v3}, Lhpf;->a(J)Lhpf;

    move-result-object v0

    .line 66
    invoke-virtual {v0, v6, v7}, Lhpf;->b(J)Lhpf;

    move-result-object v0

    .line 67
    invoke-virtual {v0, v6, v7}, Lhpf;->c(J)Lhpf;

    move-result-object v0

    .line 69
    cmp-long v1, p5, v4

    if-eqz v1, :cond_0

    .line 70
    invoke-virtual {v0, p5, p6}, Lhpf;->b(J)Lhpf;

    .line 71
    :cond_0
    cmp-long v1, p3, v4

    if-eqz v1, :cond_1

    .line 72
    invoke-virtual {v0, p3, p4}, Lhpf;->c(J)Lhpf;

    .line 73
    :cond_1
    invoke-virtual {v0}, Lhpf;->a()Lhpe;

    invoke-virtual {p0}, Lhph;->a()V

    .line 74
    return-void
.end method
