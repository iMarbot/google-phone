.class final Lio/grpc/internal/cb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/am;


# instance fields
.field public final a:Lhlw;


# direct methods
.method constructor <init>(Lhlw;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p1}, Lhlw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "error must not be OK"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 3
    iput-object p1, p0, Lio/grpc/internal/cb;->a:Lhlw;

    .line 4
    return-void

    .line 2
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    .locals 2

    .prologue
    .line 5
    new-instance v0, Lio/grpc/internal/ca;

    iget-object v1, p0, Lio/grpc/internal/cb;->a:Lhlw;

    invoke-direct {v0, v1}, Lio/grpc/internal/ca;-><init>(Lhlw;)V

    return-object v0
.end method

.method public final a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lio/grpc/internal/cc;

    invoke-direct {v0, p0, p1}, Lio/grpc/internal/cc;-><init>(Lio/grpc/internal/cb;Lio/grpc/internal/an;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 7
    return-void
.end method
