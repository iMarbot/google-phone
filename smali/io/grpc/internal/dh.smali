.class public final Lio/grpc/internal/dh;
.super Lhlf;
.source "PG"

# interfaces
.implements Lio/grpc/internal/fe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/grpc/internal/dh$c;,
        Lio/grpc/internal/dh$e;,
        Lio/grpc/internal/dh$b;,
        Lio/grpc/internal/dh$d;,
        Lio/grpc/internal/dh$a;
    }
.end annotation


# static fields
.field private static I:Ljava/util/regex/Pattern;

.field private static J:Lhlw;

.field public static final a:Ljava/util/logging/Logger;

.field public static final b:Lhlw;

.field public static final c:Lhlw;


# instance fields
.field public final A:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public volatile B:Z

.field public volatile C:Z

.field public final D:Ljava/util/concurrent/CountDownLatch;

.field public final E:Lio/grpc/internal/dh$c;

.field public final F:Lio/grpc/internal/cs;

.field public G:Lio/grpc/internal/dh$a;

.field public final H:Lio/grpc/internal/ae$b;

.field private K:Lhkz;

.field private L:J

.field private M:Lio/grpc/internal/dv;

.field private N:Ljava/util/concurrent/ScheduledFuture;

.field public final d:Ljava/lang/String;

.field public final e:Lhln;

.field public final f:Lhjs;

.field public final g:Lio/grpc/internal/ao;

.field public final h:Ljava/util/concurrent/Executor;

.field public final i:Lio/grpc/internal/eg;

.field public final j:Lio/grpc/internal/dg;

.field public final k:Lio/grpc/internal/ad;

.field public l:Z

.field public final m:Lhkp;

.field public final n:Lhki;

.field public final o:Lgtu;

.field public final p:Lio/grpc/internal/au;

.field public final q:Lio/grpc/internal/s;

.field public final r:Lhjw;

.field public final s:Ljava/lang/String;

.field public t:Lhlm;

.field public final u:Lio/grpc/internal/ei;

.field public v:Lio/grpc/internal/dh$b;

.field public volatile w:Lhle;

.field public final x:Ljava/util/Set;

.field public final y:Ljava/util/Set;

.field public final z:Lio/grpc/internal/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 121
    const-class v0, Lio/grpc/internal/dh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    .line 122
    const-string v0, "[a-zA-Z][a-zA-Z0-9+.-]*:/.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/dh;->I:Ljava/util/regex/Pattern;

    .line 123
    sget-object v0, Lhlw;->i:Lhlw;

    const-string v1, "Channel shutdownNow invoked"

    .line 124
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/dh;->J:Lhlw;

    .line 125
    sget-object v0, Lhlw;->i:Lhlw;

    const-string v1, "Channel shutdown invoked"

    .line 126
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/dh;->b:Lhlw;

    .line 127
    sget-object v0, Lhlw;->i:Lhlw;

    const-string v1, "Subchannel shutdown invoked"

    .line 128
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/dh;->c:Lhlw;

    .line 129
    return-void
.end method

.method constructor <init>(Lio/grpc/internal/c;Lio/grpc/internal/ao;Lio/grpc/internal/s;Lio/grpc/internal/eg;Lgtu;Ljava/util/List;Lio/grpc/internal/ei;)V
    .locals 8

    .prologue
    .line 35
    invoke-direct {p0}, Lhlf;-><init>()V

    .line 36
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/grpc/internal/dg;->a(Ljava/lang/String;)Lio/grpc/internal/dg;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 37
    new-instance v0, Lio/grpc/internal/ad;

    invoke-direct {v0}, Lio/grpc/internal/ad;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    .line 38
    new-instance v0, Lio/grpc/internal/au;

    invoke-direct {v0}, Lio/grpc/internal/au;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/dh;->p:Lio/grpc/internal/au;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x10

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v0, v1, v2}, Ljava/util/HashSet;-><init>(IF)V

    iput-object v0, p0, Lio/grpc/internal/dh;->x:Ljava/util/Set;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v0, v1, v2}, Ljava/util/HashSet;-><init>(IF)V

    iput-object v0, p0, Lio/grpc/internal/dh;->y:Ljava/util/Set;

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 42
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lio/grpc/internal/dh;->D:Ljava/util/concurrent/CountDownLatch;

    .line 43
    new-instance v0, Lio/grpc/internal/di;

    invoke-direct {v0, p0}, Lio/grpc/internal/di;-><init>(Lio/grpc/internal/dh;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->M:Lio/grpc/internal/dv;

    .line 44
    new-instance v0, Lio/grpc/internal/dj;

    invoke-direct {v0, p0}, Lio/grpc/internal/dj;-><init>(Lio/grpc/internal/dh;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->F:Lio/grpc/internal/cs;

    .line 45
    new-instance v0, Lio/grpc/internal/ae$b;

    invoke-direct {v0, p0}, Lio/grpc/internal/ae$b;-><init>(Lio/grpc/internal/dh;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->H:Lio/grpc/internal/ae$b;

    .line 46
    iget-object v0, p1, Lio/grpc/internal/c;->e:Ljava/lang/String;

    const-string v1, "target"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/dh;->d:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lio/grpc/internal/c;->d:Lhln;

    .line 49
    iput-object v0, p0, Lio/grpc/internal/dh;->e:Lhln;

    .line 50
    invoke-virtual {p1}, Lio/grpc/internal/c;->c()Lhjs;

    move-result-object v0

    const-string v1, "nameResolverParams"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjs;

    iput-object v0, p0, Lio/grpc/internal/dh;->f:Lhjs;

    .line 51
    iget-object v0, p0, Lio/grpc/internal/dh;->d:Ljava/lang/String;

    iget-object v1, p0, Lio/grpc/internal/dh;->e:Lhln;

    iget-object v2, p0, Lio/grpc/internal/dh;->f:Lhjs;

    invoke-static {v0, v1, v2}, Lio/grpc/internal/dh;->a(Ljava/lang/String;Lhln;Lhjs;)Lhlm;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/dh;->t:Lhlm;

    .line 52
    iget-object v0, p1, Lio/grpc/internal/c;->g:Lhkz;

    const-string v1, "loadBalancerFactory"

    .line 53
    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkz;

    iput-object v0, p0, Lio/grpc/internal/dh;->K:Lhkz;

    .line 54
    iget-object v0, p1, Lio/grpc/internal/c;->b:Lio/grpc/internal/eg;

    const-string v1, "executorPool"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/eg;

    iput-object v0, p0, Lio/grpc/internal/dh;->i:Lio/grpc/internal/eg;

    .line 55
    const-string v0, "oobExecutorPool"

    invoke-static {p4, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lio/grpc/internal/dh;->i:Lio/grpc/internal/eg;

    invoke-interface {v0}, Lio/grpc/internal/eg;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    const-string v1, "executor"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lio/grpc/internal/dh;->h:Ljava/util/concurrent/Executor;

    .line 57
    new-instance v0, Lio/grpc/internal/ay;

    iget-object v1, p0, Lio/grpc/internal/dh;->h:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    invoke-direct {v0, v1, v2}, Lio/grpc/internal/ay;-><init>(Ljava/util/concurrent/Executor;Lio/grpc/internal/ad;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    .line 58
    iget-object v0, p0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    iget-object v1, p0, Lio/grpc/internal/dh;->M:Lio/grpc/internal/dv;

    invoke-virtual {v0, v1}, Lio/grpc/internal/ay;->a(Lio/grpc/internal/dv;)Ljava/lang/Runnable;

    .line 59
    iput-object p3, p0, Lio/grpc/internal/dh;->q:Lio/grpc/internal/s;

    .line 60
    new-instance v0, Lio/grpc/internal/t;

    iget-object v1, p0, Lio/grpc/internal/dh;->h:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p2, v1}, Lio/grpc/internal/t;-><init>(Lio/grpc/internal/ao;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    .line 61
    new-instance v0, Lio/grpc/internal/dh$d;

    .line 62
    invoke-direct {v0, p0}, Lio/grpc/internal/dh$d;-><init>(Lio/grpc/internal/dh;)V

    .line 63
    invoke-static {v0, p6}, Lhka;->a(Lhjw;Ljava/util/List;)Lhjw;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/dh;->r:Lhjw;

    .line 64
    const-string v0, "stopwatchSupplier"

    invoke-static {p5, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtu;

    iput-object v0, p0, Lio/grpc/internal/dh;->o:Lgtu;

    .line 65
    iget-wide v0, p1, Lio/grpc/internal/c;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 66
    iget-wide v0, p1, Lio/grpc/internal/c;->k:J

    iput-wide v0, p0, Lio/grpc/internal/dh;->L:J

    .line 69
    :goto_0
    iget-boolean v0, p1, Lio/grpc/internal/c;->h:Z

    iput-boolean v0, p0, Lio/grpc/internal/dh;->l:Z

    .line 70
    iget-object v0, p1, Lio/grpc/internal/c;->i:Lhkp;

    const-string v1, "decompressorRegistry"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkp;

    iput-object v0, p0, Lio/grpc/internal/dh;->m:Lhkp;

    .line 71
    iget-object v0, p1, Lio/grpc/internal/c;->j:Lhki;

    const-string v1, "compressorRegistry"

    invoke-static {v0, v1}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhki;

    iput-object v0, p0, Lio/grpc/internal/dh;->n:Lhki;

    .line 72
    iget-object v0, p1, Lio/grpc/internal/c;->f:Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/dh;->s:Ljava/lang/String;

    .line 73
    iput-object p7, p0, Lio/grpc/internal/dh;->u:Lio/grpc/internal/ei;

    .line 74
    new-instance v0, Lio/grpc/internal/dh$c;

    invoke-direct {v0, p0}, Lio/grpc/internal/dh$c;-><init>(Lio/grpc/internal/dh;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    .line 75
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "<init>"

    const-string v4, "[{0}] Created with target {1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 76
    iget-object v7, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 77
    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lio/grpc/internal/dh;->d:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    return-void

    .line 67
    :cond_0
    iget-wide v0, p1, Lio/grpc/internal/c;->k:J

    sget-wide v2, Lio/grpc/internal/c;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "invalid idleTimeoutMillis %s"

    iget-wide v2, p1, Lio/grpc/internal/c;->k:J

    invoke-static {v0, v1, v2, v3}, Lgtn;->a(ZLjava/lang/String;J)V

    .line 68
    iget-wide v0, p1, Lio/grpc/internal/c;->k:J

    iput-wide v0, p0, Lio/grpc/internal/dh;->L:J

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Ljava/lang/String;Lhln;Lhjs;)Lhlm;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 79
    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p1, v0, p2}, Lhln;->a(Ljava/net/URI;Lhjs;)Lhlm;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_1

    .line 96
    :cond_0
    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    goto :goto_0

    .line 89
    :cond_1
    sget-object v0, Lio/grpc/internal/dh;->I:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2

    .line 90
    :try_start_1
    new-instance v1, Ljava/net/URI;

    invoke-virtual {p1}, Lhln;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, "/"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v0, v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    invoke-virtual {p1, v1, p2}, Lhln;->a(Ljava/net/URI;Lhjs;)Lhlm;

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    .line 97
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "cannot find a NameResolver for %s%s"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p0, v4, v0

    const/4 v5, 0x1

    .line 98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, " ("

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v4, v5

    .line 99
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_3
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 92
    :catch_1
    move-exception v0

    .line 93
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 98
    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method private g()Lio/grpc/internal/dh;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 100
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "shutdown"

    const-string v4, "[{0}] shutdown() called"

    .line 101
    iget-object v5, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 102
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-object p0

    .line 105
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/dh;->E:Lio/grpc/internal/dh$c;

    .line 106
    iput-boolean v6, v0, Lio/grpc/internal/dh$c;->a:Z

    .line 108
    iget-object v0, p0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    new-instance v1, Lio/grpc/internal/dm;

    invoke-direct {v1, p0}, Lio/grpc/internal/dm;-><init>(Lio/grpc/internal/dh;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 109
    iget-object v0, p0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    sget-object v1, Lio/grpc/internal/dh;->b:Lhlw;

    invoke-virtual {v0, v1}, Lio/grpc/internal/ay;->a(Lhlw;)V

    .line 110
    iget-object v0, p0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    new-instance v1, Lio/grpc/internal/dn;

    invoke-direct {v1, p0}, Lio/grpc/internal/dn;-><init>(Lio/grpc/internal/dh;)V

    invoke-virtual {v0, v1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 112
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "shutdown"

    const-string v4, "[{0}] Shutting down"

    .line 113
    iget-object v5, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 114
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final B_()Lio/grpc/internal/dg;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    return-object v0
.end method

.method public final a(Lhlk;Lhjv;)Lhjx;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lio/grpc/internal/dh;->r:Lhjw;

    invoke-virtual {v0, p1, p2}, Lhjw;->a(Lhlk;Lhjv;)Lhjx;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lio/grpc/internal/dh;->r:Lhjw;

    invoke-virtual {v0}, Lhjw;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Lhlf;
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lio/grpc/internal/dh;->g()Lio/grpc/internal/dh;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method final d()V
    .locals 6

    .prologue
    .line 1
    iget-object v0, p0, Lio/grpc/internal/dh;->A:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20
    :cond_0
    :goto_0
    return-void

    .line 3
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/dh;->F:Lio/grpc/internal/cs;

    .line 4
    iget-object v0, v0, Lio/grpc/internal/cs;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 5
    :goto_1
    if-eqz v0, :cond_3

    .line 6
    invoke-virtual {p0}, Lio/grpc/internal/dh;->e()V

    .line 8
    :goto_2
    iget-object v0, p0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    if-nez v0, :cond_0

    .line 10
    sget-object v0, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v2, "io.grpc.internal.ManagedChannelImpl"

    const-string v3, "exitIdleMode"

    const-string v4, "[{0}] Exiting idle mode"

    .line 11
    iget-object v5, p0, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 12
    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 13
    new-instance v0, Lio/grpc/internal/dh$b;

    iget-object v1, p0, Lio/grpc/internal/dh;->t:Lhlm;

    invoke-direct {v0, p0, v1}, Lio/grpc/internal/dh$b;-><init>(Lio/grpc/internal/dh;Lhlm;)V

    iput-object v0, p0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 14
    iget-object v0, p0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    iget-object v1, p0, Lio/grpc/internal/dh;->K:Lhkz;

    iget-object v2, p0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    invoke-virtual {v1, v2}, Lhkz;->a(Lhla;)Lhky;

    move-result-object v1

    iput-object v1, v0, Lio/grpc/internal/dh$b;->a:Lhky;

    .line 15
    new-instance v1, Lhlo;

    iget-object v0, p0, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    invoke-direct {v1, p0, v0}, Lhlo;-><init>(Lio/grpc/internal/dh;Lio/grpc/internal/dh$b;)V

    .line 16
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/dh;->t:Lhlm;

    invoke-virtual {v0, v1}, Lhlm;->a(Lhlo;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    invoke-static {v0}, Lhlw;->a(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhlo;->a(Lhlw;)V

    goto :goto_0

    .line 4
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 7
    :cond_3
    invoke-virtual {p0}, Lio/grpc/internal/dh;->f()V

    goto :goto_2
.end method

.method final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    iget-object v0, p0, Lio/grpc/internal/dh;->N:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lio/grpc/internal/dh;->N:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 23
    iget-object v0, p0, Lio/grpc/internal/dh;->G:Lio/grpc/internal/dh$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/grpc/internal/dh$a;->a:Z

    .line 24
    iput-object v2, p0, Lio/grpc/internal/dh;->N:Ljava/util/concurrent/ScheduledFuture;

    .line 25
    iput-object v2, p0, Lio/grpc/internal/dh;->G:Lio/grpc/internal/dh$a;

    .line 26
    :cond_0
    return-void
.end method

.method final f()V
    .locals 5

    .prologue
    .line 27
    iget-wide v0, p0, Lio/grpc/internal/dh;->L:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-virtual {p0}, Lio/grpc/internal/dh;->e()V

    .line 30
    new-instance v0, Lio/grpc/internal/dh$a;

    .line 31
    invoke-direct {v0, p0}, Lio/grpc/internal/dh$a;-><init>(Lio/grpc/internal/dh;)V

    .line 32
    iput-object v0, p0, Lio/grpc/internal/dh;->G:Lio/grpc/internal/dh$a;

    .line 33
    iget-object v0, p0, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    invoke-interface {v0}, Lio/grpc/internal/ao;->a()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lio/grpc/internal/df;

    new-instance v2, Lio/grpc/internal/dk;

    invoke-direct {v2, p0}, Lio/grpc/internal/dk;-><init>(Lio/grpc/internal/dh;)V

    invoke-direct {v1, v2}, Lio/grpc/internal/df;-><init>(Ljava/lang/Runnable;)V

    iget-wide v2, p0, Lio/grpc/internal/dh;->L:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/dh;->N:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method
