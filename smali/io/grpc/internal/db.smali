.class final Lio/grpc/internal/db;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lio/grpc/internal/cz;


# direct methods
.method constructor <init>(Lio/grpc/internal/cz;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 2
    iget-object v0, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    const/4 v1, 0x0

    .line 3
    iput-object v1, v0, Lio/grpc/internal/cz;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 5
    const/4 v0, 0x0

    .line 6
    iget-object v1, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    monitor-enter v1

    .line 7
    :try_start_0
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    invoke-static {v2}, Lio/grpc/internal/cz;->a(Lio/grpc/internal/cz;)I

    move-result v2

    sget v3, Lmg$c;->aL:I

    if-ne v2, v3, :cond_2

    .line 8
    const/4 v0, 0x1

    .line 9
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    sget v3, Lmg$c;->aN:I

    invoke-static {v2, v3}, Lio/grpc/internal/cz;->a(Lio/grpc/internal/cz;I)I

    .line 10
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    iget-object v3, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 11
    iget-object v3, v3, Lio/grpc/internal/cz;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 12
    iget-object v4, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 13
    iget-object v4, v4, Lio/grpc/internal/cz;->g:Ljava/lang/Runnable;

    .line 14
    iget-object v5, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 15
    iget-wide v6, v5, Lio/grpc/internal/cz;->i:J

    .line 16
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v6, v7, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    .line 17
    iput-object v3, v2, Lio/grpc/internal/cz;->e:Ljava/util/concurrent/ScheduledFuture;

    .line 35
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 38
    iget-object v0, v0, Lio/grpc/internal/cz;->c:Lio/grpc/internal/dd;

    .line 39
    invoke-interface {v0}, Lio/grpc/internal/dd;->a()V

    .line 40
    :cond_1
    return-void

    .line 19
    :cond_2
    :try_start_1
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    invoke-static {v2}, Lio/grpc/internal/cz;->a(Lio/grpc/internal/cz;)I

    move-result v2

    sget v3, Lmg$c;->aM:I

    if-ne v2, v3, :cond_0

    .line 20
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    iget-object v3, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 21
    iget-object v3, v3, Lio/grpc/internal/cz;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 22
    iget-object v4, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 24
    iget-object v4, v4, Lio/grpc/internal/cz;->h:Ljava/lang/Runnable;

    .line 25
    iget-object v5, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 27
    iget-wide v6, v5, Lio/grpc/internal/cz;->d:J

    .line 28
    iget-object v5, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    .line 29
    iget-object v5, v5, Lio/grpc/internal/cz;->b:Lio/grpc/internal/de;

    .line 30
    invoke-virtual {v5}, Lio/grpc/internal/de;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 31
    invoke-interface {v3, v4, v6, v7, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    .line 32
    iput-object v3, v2, Lio/grpc/internal/cz;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 34
    iget-object v2, p0, Lio/grpc/internal/db;->a:Lio/grpc/internal/cz;

    sget v3, Lmg$c;->aL:I

    invoke-static {v2, v3}, Lio/grpc/internal/cz;->a(Lio/grpc/internal/cz;I)I

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
