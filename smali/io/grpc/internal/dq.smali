.class final Lio/grpc/internal/dq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhle;

.field private synthetic b:Lhkj;

.field private synthetic c:Lio/grpc/internal/dh$b;


# direct methods
.method constructor <init>(Lio/grpc/internal/dh$b;Lhle;Lhkj;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iput-object p2, p0, Lio/grpc/internal/dq;->a:Lhle;

    iput-object p3, p0, Lio/grpc/internal/dq;->b:Lhkj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 2
    iget-object v0, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iget-object v1, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iget-object v1, v1, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 3
    iget-object v1, v1, Lio/grpc/internal/dh;->v:Lio/grpc/internal/dh$b;

    .line 4
    if-eq v0, v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 6
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    iget-object v1, p0, Lio/grpc/internal/dq;->a:Lhle;

    .line 7
    iput-object v1, v0, Lio/grpc/internal/dh;->w:Lhle;

    .line 9
    iget-object v0, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 10
    iget-object v4, v0, Lio/grpc/internal/dh;->z:Lio/grpc/internal/ay;

    .line 11
    iget-object v5, p0, Lio/grpc/internal/dq;->a:Lhle;

    .line 12
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 13
    iget-object v1, v4, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 14
    :try_start_0
    iput-object v5, v4, Lio/grpc/internal/ay;->i:Lhle;

    .line 15
    iget-wide v2, v4, Lio/grpc/internal/ay;->j:J

    const-wide/16 v8, 0x1

    add-long/2addr v2, v8

    iput-wide v2, v4, Lio/grpc/internal/ay;->j:J

    .line 16
    iget-object v0, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 17
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :goto_1
    iget-object v0, p0, Lio/grpc/internal/dq;->b:Lhkj;

    sget-object v1, Lhkj;->e:Lhkj;

    if-eq v0, v1, :cond_0

    .line 53
    iget-object v0, p0, Lio/grpc/internal/dq;->c:Lio/grpc/internal/dh$b;

    iget-object v0, v0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 54
    iget-object v0, v0, Lio/grpc/internal/dh;->p:Lio/grpc/internal/au;

    .line 55
    iget-object v1, p0, Lio/grpc/internal/dq;->b:Lhkj;

    invoke-virtual {v0, v1}, Lio/grpc/internal/au;->a(Lhkj;)V

    goto :goto_0

    .line 18
    :cond_2
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 19
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v7, :cond_5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v2, 0x1

    check-cast v1, Lio/grpc/internal/be;

    .line 21
    invoke-virtual {v5}, Lhle;->a()Lhlb;

    move-result-object v2

    .line 23
    iget-object v8, v1, Lio/grpc/internal/be;->a:Lhlc;

    .line 24
    invoke-virtual {v8}, Lhlc;->a()Lhjv;

    move-result-object v8

    .line 27
    iget-boolean v9, v8, Lhjv;->h:Z

    .line 28
    invoke-static {v2, v9}, Lio/grpc/internal/cf;->a(Lhlb;Z)Lio/grpc/internal/am;

    move-result-object v9

    .line 29
    if-eqz v9, :cond_4

    .line 30
    iget-object v2, v4, Lio/grpc/internal/ay;->b:Ljava/util/concurrent/Executor;

    .line 32
    iget-object v10, v8, Lhjv;->c:Ljava/util/concurrent/Executor;

    .line 33
    if-eqz v10, :cond_3

    .line 35
    iget-object v2, v8, Lhjv;->c:Ljava/util/concurrent/Executor;

    .line 37
    :cond_3
    new-instance v8, Lio/grpc/internal/bd;

    invoke-direct {v8, v1, v9}, Lio/grpc/internal/bd;-><init>(Lio/grpc/internal/be;Lio/grpc/internal/am;)V

    invoke-interface {v2, v8}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 38
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move v2, v3

    .line 39
    goto :goto_2

    .line 19
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 40
    :cond_5
    iget-object v1, v4, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_3
    iget-object v0, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 42
    monitor-exit v1

    goto :goto_1

    .line 50
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 43
    :cond_6
    :try_start_4
    iget-object v0, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v0, v6}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 44
    iget-object v0, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 45
    iget-object v0, v4, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    iget-object v2, v4, Lio/grpc/internal/ay;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 46
    iget-object v0, v4, Lio/grpc/internal/ay;->h:Lhlw;

    if-eqz v0, :cond_8

    iget-object v0, v4, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_8

    .line 47
    iget-object v0, v4, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    iget-object v2, v4, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 48
    const/4 v0, 0x0

    iput-object v0, v4, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 50
    :cond_7
    :goto_3
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 51
    iget-object v0, v4, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    goto/16 :goto_1

    .line 49
    :cond_8
    :try_start_5
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, v4, Lio/grpc/internal/ay;->g:Ljava/util/Collection;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3
.end method
