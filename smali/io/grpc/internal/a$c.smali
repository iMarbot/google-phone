.class public abstract Lio/grpc/internal/a$c;
.super Lio/grpc/internal/f;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# instance fields
.field public a:Lio/grpc/internal/fb;

.field public b:Z

.field public c:Lhkp;

.field public d:Z

.field private j:Lio/grpc/internal/ez;

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/Runnable;


# direct methods
.method protected constructor <init>(ILio/grpc/internal/ez;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0, p1, p2}, Lio/grpc/internal/f;-><init>(ILio/grpc/internal/ez;)V

    .line 3
    sget-object v0, Lhkp;->a:Lhkp;

    .line 4
    iput-object v0, p0, Lio/grpc/internal/a$c;->c:Lhkp;

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/grpc/internal/a$c;->l:Z

    .line 6
    const-string v0, "statsTraceCtx"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ez;

    iput-object v0, p0, Lio/grpc/internal/a$c;->j:Lio/grpc/internal/ez;

    .line 7
    return-void
.end method


# virtual methods
.method protected final synthetic a()Lio/grpc/internal/fb;
    .locals 1

    .prologue
    .line 85
    .line 86
    iget-object v0, p0, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    .line 87
    return-object v0
.end method

.method public final a(Lhlh;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 13
    iget-boolean v0, p0, Lio/grpc/internal/a$c;->d:Z

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    const-string v1, "Received headers on closed stream"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lio/grpc/internal/a$c;->j:Lio/grpc/internal/ez;

    .line 16
    sget-object v0, Lio/grpc/internal/cf;->e:Lhlh$e;

    invoke-virtual {p1, v0}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 17
    iget-boolean v1, p0, Lio/grpc/internal/a$c;->b:Z

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    .line 18
    const-string v1, "gzip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19
    new-instance v0, Lio/grpc/internal/ck;

    invoke-direct {v0}, Lio/grpc/internal/ck;-><init>()V

    .line 20
    iget-object v1, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v1, v0}, Lio/grpc/internal/ax;->a(Lio/grpc/internal/ck;)V

    .line 21
    new-instance v1, Lio/grpc/internal/g;

    iget-object v0, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    check-cast v0, Lio/grpc/internal/dw;

    invoke-direct {v1, p0, p0, v0}, Lio/grpc/internal/g;-><init>(Lio/grpc/internal/dx;Lio/grpc/internal/p;Lio/grpc/internal/dw;)V

    iput-object v1, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    move v4, v2

    .line 30
    :goto_1
    sget-object v0, Lio/grpc/internal/cf;->c:Lhlh$e;

    invoke-virtual {p1, v0}, Lhlh;->a(Lhlh$e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 31
    if-eqz v0, :cond_5

    .line 32
    iget-object v1, p0, Lio/grpc/internal/a$c;->c:Lhkp;

    .line 33
    iget-object v1, v1, Lhkp;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhkq;

    .line 34
    if-eqz v1, :cond_2

    iget-object v1, v1, Lhkq;->a:Lhko;

    .line 36
    :goto_2
    if-nez v1, :cond_3

    .line 37
    sget-object v1, Lhlw;->h:Lhlw;

    const-string v4, "Can\'t find decompressor for %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    .line 38
    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Lio/grpc/internal/a$c;->a(Ljava/lang/Throwable;)V

    .line 55
    :goto_3
    return-void

    :cond_0
    move v0, v3

    .line 13
    goto :goto_0

    .line 23
    :cond_1
    const-string v1, "identity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 24
    sget-object v1, Lhlw;->h:Lhlw;

    const-string v4, "Can\'t find full stream decompressor for %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    .line 25
    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 26
    invoke-virtual {v1, v0}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    .line 28
    invoke-virtual {p0, v0}, Lio/grpc/internal/a$c;->a(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 34
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 42
    :cond_3
    sget-object v0, Lhkg;->a:Lhkh;

    if-eq v1, v0, :cond_5

    .line 43
    if-eqz v4, :cond_4

    .line 44
    sget-object v0, Lhlw;->h:Lhlw;

    const-string v1, "Full stream and gRPC message encoding cannot both be set"

    new-array v2, v3, [Ljava/lang/Object;

    .line 45
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    .line 48
    invoke-virtual {p0, v0}, Lio/grpc/internal/a$c;->a(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 51
    :cond_4
    iget-object v0, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0, v1}, Lio/grpc/internal/ax;->a(Lhko;)V

    .line 53
    :cond_5
    iget-object v0, p0, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    .line 54
    invoke-interface {v0, p1}, Lio/grpc/internal/fb;->a(Lhlh;)V

    goto :goto_3

    :cond_6
    move v4, v3

    goto :goto_1
.end method

.method final a(Lhlw;Lhlh;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 74
    iget-boolean v1, p0, Lio/grpc/internal/a$c;->k:Z

    if-nez v1, :cond_1

    .line 75
    iput-boolean v3, p0, Lio/grpc/internal/a$c;->k:Z

    .line 76
    iget-object v1, p0, Lio/grpc/internal/a$c;->j:Lio/grpc/internal/ez;

    .line 77
    iget-object v2, v1, Lio/grpc/internal/ez;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    iget-object v1, v1, Lio/grpc/internal/ez;->a:[Lhmc;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 79
    invoke-virtual {v3}, Lhmc;->a()V

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/a$c;->a:Lio/grpc/internal/fb;

    .line 83
    invoke-interface {v0, p1, p2}, Lio/grpc/internal/fb;->b(Lhlw;Lhlh;)V

    .line 84
    :cond_1
    return-void
.end method

.method public final a(Lhlw;ZLhlh;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    const-string v0, "status"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v0, "trailers"

    invoke-static {p3, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-boolean v0, p0, Lio/grpc/internal/a$c;->d:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 73
    :goto_0
    return-void

    .line 60
    :cond_0
    iput-boolean v1, p0, Lio/grpc/internal/a$c;->d:Z

    .line 62
    iget-object v1, p0, Lio/grpc/internal/f;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lio/grpc/internal/f;->i:Z

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    iget-boolean v0, p0, Lio/grpc/internal/a$c;->l:Z

    if-eqz v0, :cond_1

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/a$c;->m:Ljava/lang/Runnable;

    .line 67
    invoke-virtual {p0, p1, p3}, Lio/grpc/internal/a$c;->a(Lhlw;Lhlh;)V

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 68
    :cond_1
    new-instance v0, Lio/grpc/internal/b;

    invoke-direct {v0, p0, p1, p3}, Lio/grpc/internal/b;-><init>(Lio/grpc/internal/a$c;Lhlw;Lhlh;)V

    iput-object v0, p0, Lio/grpc/internal/a$c;->m:Ljava/lang/Runnable;

    .line 70
    if-eqz p2, :cond_2

    .line 71
    iget-object v0, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0}, Lio/grpc/internal/ax;->close()V

    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lio/grpc/internal/f;->e:Lio/grpc/internal/ax;

    invoke-interface {v0}, Lio/grpc/internal/ax;->a()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/a$c;->l:Z

    .line 9
    iget-object v0, p0, Lio/grpc/internal/a$c;->m:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 10
    iget-object v0, p0, Lio/grpc/internal/a$c;->m:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/a$c;->m:Ljava/lang/Runnable;

    .line 12
    :cond_0
    return-void
.end method
