.class final Lio/grpc/internal/ae$a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/fb;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field public final a:Lhjy;

.field public b:Z

.field public final synthetic c:Lio/grpc/internal/ae;


# direct methods
.method public constructor <init>(Lio/grpc/internal/ae;Lhjy;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "observer"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjy;

    iput-object v0, p0, Lio/grpc/internal/ae$a;->a:Lhjy;

    .line 3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 40
    iget-object v0, v0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    .line 41
    new-instance v1, Lio/grpc/internal/ak;

    invoke-direct {v1, p0}, Lio/grpc/internal/ak;-><init>(Lio/grpc/internal/ae$a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 42
    return-void
.end method

.method public final a(Lhlh;)V
    .locals 2

    .prologue
    .line 4
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 5
    iget-object v0, v0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    .line 6
    new-instance v1, Lio/grpc/internal/ah;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/ah;-><init>(Lio/grpc/internal/ae$a;Lhlh;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 7
    return-void
.end method

.method final a(Lhlw;Lhlh;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12
    iput-boolean v1, p0, Lio/grpc/internal/ae$a;->b:Z

    .line 13
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 14
    iput-boolean v1, v0, Lio/grpc/internal/ae;->e:Z

    .line 16
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    iget-object v0, p0, Lio/grpc/internal/ae$a;->a:Lhjy;

    .line 17
    invoke-static {v0, p1, p2}, Lio/grpc/internal/ae;->a(Lhjy;Lhlw;Lhlh;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 19
    invoke-virtual {v0}, Lio/grpc/internal/ae;->b()V

    .line 20
    return-void

    .line 21
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 22
    invoke-virtual {v1}, Lio/grpc/internal/ae;->b()V

    .line 23
    throw v0
.end method

.method public final a(Lio/grpc/internal/fc;)V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 9
    iget-object v0, v0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    .line 10
    new-instance v1, Lio/grpc/internal/ai;

    invoke-direct {v1, p0, p1}, Lio/grpc/internal/ai;-><init>(Lio/grpc/internal/ae$a;Lio/grpc/internal/fc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 11
    return-void
.end method

.method public final b(Lhlw;Lhlh;)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 25
    invoke-virtual {v0}, Lio/grpc/internal/ae;->c()Lhkm;

    move-result-object v0

    .line 28
    iget-object v1, p1, Lhlw;->l:Lhlx;

    .line 29
    sget-object v2, Lhlx;->b:Lhlx;

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    .line 30
    invoke-static {}, Lhkm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget-object p1, Lhlw;->e:Lhlw;

    .line 32
    new-instance p2, Lhlh;

    invoke-direct {p2}, Lhlh;-><init>()V

    .line 35
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 36
    iget-object v0, v0, Lio/grpc/internal/ae;->b:Ljava/util/concurrent/Executor;

    .line 37
    new-instance v1, Lio/grpc/internal/aj;

    invoke-direct {v1, p0, p1, p2}, Lio/grpc/internal/aj;-><init>(Lio/grpc/internal/ae$a;Lhlw;Lhlh;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 38
    return-void
.end method
