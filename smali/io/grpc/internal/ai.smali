.class final Lio/grpc/internal/ai;
.super Lio/grpc/internal/aw;
.source "PG"


# instance fields
.field private synthetic a:Lio/grpc/internal/fc;

.field private synthetic b:Lio/grpc/internal/ae$a;


# direct methods
.method constructor <init>(Lio/grpc/internal/ae$a;Lio/grpc/internal/fc;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    iput-object p2, p0, Lio/grpc/internal/ai;->a:Lio/grpc/internal/fc;

    .line 2
    iget-object v0, p1, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 3
    iget-object v0, v0, Lio/grpc/internal/ae;->c:Lhkl;

    .line 4
    invoke-direct {p0, v0}, Lio/grpc/internal/aw;-><init>(Lhkl;)V

    .line 5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 6
    iget-object v0, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    .line 7
    iget-boolean v0, v0, Lio/grpc/internal/ae$a;->b:Z

    .line 8
    if-eqz v0, :cond_1

    .line 9
    iget-object v0, p0, Lio/grpc/internal/ai;->a:Lio/grpc/internal/fc;

    invoke-static {v0}, Lio/grpc/internal/cf;->a(Lio/grpc/internal/fc;)V

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 11
    :cond_1
    :goto_1
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ai;->a:Lio/grpc/internal/fc;

    invoke-interface {v0}, Lio/grpc/internal/fc;->a()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 12
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    .line 13
    iget-object v0, v0, Lio/grpc/internal/ae$a;->a:Lhjy;

    .line 14
    iget-object v2, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    iget-object v2, v2, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 15
    iget-object v2, v2, Lio/grpc/internal/ae;->a:Lhlk;

    .line 17
    iget-object v2, v2, Lhlk;->d:Lhlk$b;

    invoke-interface {v2, v1}, Lhlk$b;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v2

    .line 18
    invoke-virtual {v0, v2}, Lhjy;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 23
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 25
    :catch_0
    move-exception v0

    .line 26
    iget-object v1, p0, Lio/grpc/internal/ai;->a:Lio/grpc/internal/fc;

    invoke-static {v1}, Lio/grpc/internal/cf;->a(Lio/grpc/internal/fc;)V

    .line 27
    sget-object v1, Lhlw;->c:Lhlw;

    .line 28
    invoke-virtual {v1, v0}, Lhlw;->b(Ljava/lang/Throwable;)Lhlw;

    move-result-object v0

    const-string v1, "Failed to read message."

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    iget-object v1, v1, Lio/grpc/internal/ae$a;->c:Lio/grpc/internal/ae;

    .line 30
    iget-object v1, v1, Lio/grpc/internal/ae;->d:Lio/grpc/internal/al;

    .line 31
    invoke-interface {v1, v0}, Lio/grpc/internal/al;->a(Lhlw;)V

    .line 32
    iget-object v1, p0, Lio/grpc/internal/ai;->b:Lio/grpc/internal/ae$a;

    new-instance v2, Lhlh;

    invoke-direct {v2}, Lhlh;-><init>()V

    .line 33
    invoke-virtual {v1, v0, v2}, Lio/grpc/internal/ae$a;->a(Lhlw;Lhlh;)V

    goto :goto_0

    .line 20
    :catch_1
    move-exception v0

    .line 21
    :try_start_3
    invoke-static {v1}, Lio/grpc/internal/cf;->a(Ljava/io/InputStream;)V

    .line 22
    throw v0
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
.end method
