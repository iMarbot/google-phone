.class public final Lio/grpc/internal/dw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/ax;
.implements Ljava/io/Closeable;


# instance fields
.field public a:Lio/grpc/internal/dx;

.field public b:I

.field public volatile c:Z

.field private d:Lio/grpc/internal/ez;

.field private e:Ljava/lang/String;

.field private f:Lhko;

.field private g:Lio/grpc/internal/ck;

.field private h:[B

.field private i:I

.field private j:Lio/grpc/internal/ea;

.field private k:I

.field private l:Z

.field private m:Lio/grpc/internal/ap;

.field private n:Lio/grpc/internal/ap;

.field private o:J

.field private p:Z

.field private q:I

.field private r:I

.field private s:Z


# direct methods
.method public constructor <init>(Lio/grpc/internal/dx;Lhko;ILio/grpc/internal/ez;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lio/grpc/internal/ea;->a:Lio/grpc/internal/ea;

    iput-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    .line 3
    const/4 v0, 0x5

    iput v0, p0, Lio/grpc/internal/dw;->k:I

    .line 4
    new-instance v0, Lio/grpc/internal/ap;

    invoke-direct {v0}, Lio/grpc/internal/ap;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 5
    iput-boolean v1, p0, Lio/grpc/internal/dw;->p:Z

    .line 6
    const/4 v0, -0x1

    iput v0, p0, Lio/grpc/internal/dw;->q:I

    .line 7
    iput-boolean v1, p0, Lio/grpc/internal/dw;->s:Z

    .line 8
    iput-boolean v1, p0, Lio/grpc/internal/dw;->c:Z

    .line 9
    const-string v0, "sink"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/dx;

    iput-object v0, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    .line 10
    const-string v0, "decompressor"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhko;

    iput-object v0, p0, Lio/grpc/internal/dw;->f:Lhko;

    .line 11
    iput p3, p0, Lio/grpc/internal/dw;->b:I

    .line 12
    const-string v0, "statsTraceCtx"

    invoke-static {p4, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ez;

    iput-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    .line 13
    iput-object p5, p0, Lio/grpc/internal/dw;->e:Ljava/lang/String;

    .line 14
    return-void
.end method

.method private final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    iget-object v2, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v2, :cond_2

    .line 85
    iget-object v2, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 86
    iget-boolean v3, v2, Lio/grpc/internal/ck;->i:Z

    if-nez v3, :cond_1

    :goto_0
    const-string v1, "GzipInflatingBuffer is closed"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 87
    iget-boolean v0, v2, Lio/grpc/internal/ck;->n:Z

    .line 91
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 86
    goto :goto_0

    .line 89
    :cond_2
    iget-object v2, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 90
    iget v2, v2, Lio/grpc/internal/ap;->a:I

    .line 91
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method private final d()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 92
    iget-boolean v0, p0, Lio/grpc/internal/dw;->p:Z

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 94
    :cond_0
    iput-boolean v8, p0, Lio/grpc/internal/dw;->p:Z

    .line 95
    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lio/grpc/internal/dw;->c:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lio/grpc/internal/dw;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    invoke-direct {p0}, Lio/grpc/internal/dw;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 96
    iget-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    invoke-virtual {v0}, Lio/grpc/internal/ea;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 144
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid state: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :catchall_0
    move-exception v0

    iput-boolean v7, p0, Lio/grpc/internal/dw;->p:Z

    throw v0

    .line 98
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    invoke-virtual {v0}, Lio/grpc/internal/ap;->b()I

    move-result v0

    .line 99
    and-int/lit16 v1, v0, 0xfe

    if-eqz v1, :cond_1

    .line 100
    sget-object v0, Lhlw;->h:Lhlw;

    iget-object v1, p0, Lio/grpc/internal/dw;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ": Frame header malformed: reserved bits not zero"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 102
    :cond_1
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    move v0, v8

    :goto_2
    iput-boolean v0, p0, Lio/grpc/internal/dw;->l:Z

    .line 103
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 104
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lio/grpc/internal/d;->a(I)V

    .line 105
    invoke-virtual {v0}, Lio/grpc/internal/d;->b()I

    move-result v1

    .line 106
    invoke-virtual {v0}, Lio/grpc/internal/d;->b()I

    move-result v2

    .line 107
    invoke-virtual {v0}, Lio/grpc/internal/d;->b()I

    move-result v3

    .line 108
    invoke-virtual {v0}, Lio/grpc/internal/d;->b()I

    move-result v0

    .line 109
    shl-int/lit8 v1, v1, 0x18

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 110
    iput v0, p0, Lio/grpc/internal/dw;->k:I

    .line 111
    iget v0, p0, Lio/grpc/internal/dw;->k:I

    if-ltz v0, :cond_2

    iget v0, p0, Lio/grpc/internal/dw;->k:I

    iget v1, p0, Lio/grpc/internal/dw;->b:I

    if-le v0, v1, :cond_4

    .line 112
    :cond_2
    sget-object v0, Lhlw;->g:Lhlw;

    const-string v1, "%s: Frame size %d exceeds maximum: %d. "

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lio/grpc/internal/dw;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lio/grpc/internal/dw;->k:I

    .line 113
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lio/grpc/internal/dw;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 114
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    :cond_3
    move v0, v7

    .line 102
    goto :goto_2

    .line 117
    :cond_4
    iget v0, p0, Lio/grpc/internal/dw;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/grpc/internal/dw;->q:I

    .line 118
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    iget v1, p0, Lio/grpc/internal/dw;->q:I

    .line 119
    iget-object v1, v0, Lio/grpc/internal/ez;->a:[Lhmc;

    array-length v2, v1

    move v0, v7

    :goto_3
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 120
    invoke-virtual {v3}, Lhmc;->c()V

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 122
    :cond_5
    sget-object v0, Lio/grpc/internal/ea;->b:Lio/grpc/internal/ea;

    iput-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    goto/16 :goto_1

    .line 125
    :pswitch_1
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    iget v1, p0, Lio/grpc/internal/dw;->q:I

    iget v2, p0, Lio/grpc/internal/dw;->r:I

    int-to-long v2, v2

    .line 126
    iget-object v9, v0, Lio/grpc/internal/ez;->a:[Lhmc;

    array-length v10, v9

    move v6, v7

    :goto_4
    if-ge v6, v10, :cond_6

    aget-object v0, v9, v6

    .line 127
    const-wide/16 v4, -0x1

    invoke-virtual/range {v0 .. v5}, Lhmc;->b(IJJ)V

    .line 128
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    .line 129
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    .line 130
    iget-boolean v0, p0, Lio/grpc/internal/dw;->l:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lio/grpc/internal/dw;->f()Ljava/io/InputStream;

    move-result-object v0

    .line 136
    :goto_5
    const/4 v1, 0x0

    iput-object v1, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 137
    iget-object v1, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    new-instance v2, Lio/grpc/internal/dy;

    .line 138
    invoke-direct {v2, v0}, Lio/grpc/internal/dy;-><init>(Ljava/io/InputStream;)V

    .line 139
    invoke-interface {v1, v2}, Lio/grpc/internal/dx;->a(Lio/grpc/internal/fc;)V

    .line 140
    sget-object v0, Lio/grpc/internal/ea;->a:Lio/grpc/internal/ea;

    iput-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    .line 141
    const/4 v0, 0x5

    iput v0, p0, Lio/grpc/internal/dw;->k:I

    .line 142
    iget-wide v0, p0, Lio/grpc/internal/dw;->o:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lio/grpc/internal/dw;->o:J

    goto/16 :goto_1

    .line 131
    :cond_7
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    iget-object v1, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 132
    iget v1, v1, Lio/grpc/internal/ap;->a:I

    .line 133
    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->c(J)V

    .line 134
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lio/grpc/internal/eo;->a(Lio/grpc/internal/en;Z)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_5

    .line 145
    :cond_8
    iget-boolean v0, p0, Lio/grpc/internal/dw;->c:Z

    if-eqz v0, :cond_9

    .line 146
    invoke-virtual {p0}, Lio/grpc/internal/dw;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    iput-boolean v7, p0, Lio/grpc/internal/dw;->p:Z

    goto/16 :goto_0

    .line 149
    :cond_9
    :try_start_2
    iget-boolean v0, p0, Lio/grpc/internal/dw;->s:Z

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lio/grpc/internal/dw;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 150
    invoke-virtual {p0}, Lio/grpc/internal/dw;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    :cond_a
    iput-boolean v7, p0, Lio/grpc/internal/dw;->p:Z

    goto/16 :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final e()Z
    .locals 11

    .prologue
    .line 154
    const/4 v2, 0x0

    .line 155
    const/4 v1, 0x0

    .line 156
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Lio/grpc/internal/ap;

    invoke-direct {v0}, Lio/grpc/internal/ap;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 158
    :cond_0
    :goto_0
    iget v0, p0, Lio/grpc/internal/dw;->k:I

    iget-object v3, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 159
    iget v3, v3, Lio/grpc/internal/ap;->a:I

    .line 160
    sub-int/2addr v0, v3

    if-lez v0, :cond_1f

    .line 161
    iget-object v3, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_1b

    .line 162
    :try_start_1
    iget-object v3, p0, Lio/grpc/internal/dw;->h:[B

    if-eqz v3, :cond_1

    iget v3, p0, Lio/grpc/internal/dw;->i:I

    iget-object v4, p0, Lio/grpc/internal/dw;->h:[B

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 163
    :cond_1
    const/high16 v3, 0x200000

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lio/grpc/internal/dw;->h:[B

    .line 164
    const/4 v3, 0x0

    iput v3, p0, Lio/grpc/internal/dw;->i:I

    .line 165
    :cond_2
    iget-object v3, p0, Lio/grpc/internal/dw;->h:[B

    array-length v3, v3

    iget v4, p0, Lio/grpc/internal/dw;->i:I

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 166
    iget-object v5, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    iget-object v6, p0, Lio/grpc/internal/dw;->h:[B

    iget v7, p0, Lio/grpc/internal/dw;->i:I

    .line 167
    iget-boolean v0, v5, Lio/grpc/internal/ck;->i:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    const-string v3, "GzipInflatingBuffer is closed"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 168
    const/4 v3, 0x0

    .line 169
    const/4 v0, 0x1

    .line 170
    :goto_2
    if-eqz v0, :cond_15

    sub-int v8, v4, v3

    if-lez v8, :cond_15

    .line 171
    iget-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    invoke-virtual {v0}, Lio/grpc/internal/cm;->ordinal()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 291
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v3, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid state: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 321
    :catch_0
    move-exception v0

    .line 322
    :try_start_2
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 352
    :catchall_0
    move-exception v0

    if-lez v2, :cond_3

    .line 353
    iget-object v3, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    invoke-interface {v3, v2}, Lio/grpc/internal/dx;->c(I)V

    .line 354
    iget-object v3, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    sget-object v4, Lio/grpc/internal/ea;->b:Lio/grpc/internal/ea;

    if-ne v3, v4, :cond_3

    .line 355
    iget-object v3, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v3, :cond_22

    .line 356
    iget-object v2, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lio/grpc/internal/ez;->d(J)V

    .line 357
    iget v2, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v1, v2

    iput v1, p0, Lio/grpc/internal/dw;->r:I

    .line 359
    :cond_3
    :goto_3
    throw v0

    .line 167
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 173
    :pswitch_0
    :try_start_3
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 174
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 175
    const/16 v8, 0xa

    if-ge v0, v8, :cond_5

    .line 176
    const/4 v0, 0x0

    goto :goto_2

    .line 177
    :cond_5
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 178
    invoke-virtual {v0}, Lio/grpc/internal/cl;->d()I

    move-result v0

    .line 179
    const v8, 0x8b1f

    if-eq v0, v8, :cond_6

    .line 180
    new-instance v0, Ljava/util/zip/ZipException;

    const-string v3, "Not in GZIP format"

    invoke-direct {v0, v3}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 323
    :catch_1
    move-exception v0

    .line 324
    :try_start_4
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 181
    :cond_6
    :try_start_5
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 182
    invoke-virtual {v0}, Lio/grpc/internal/cl;->a()I

    move-result v0

    .line 183
    const/16 v8, 0x8

    if-eq v0, v8, :cond_7

    .line 184
    new-instance v0, Ljava/util/zip/ZipException;

    const-string v3, "Unsupported compression method"

    invoke-direct {v0, v3}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_7
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 186
    invoke-virtual {v0}, Lio/grpc/internal/cl;->a()I

    move-result v0

    .line 187
    iput v0, v5, Lio/grpc/internal/ck;->j:I

    .line 188
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    const/4 v8, 0x6

    .line 189
    invoke-virtual {v0, v8}, Lio/grpc/internal/cl;->a(I)V

    .line 190
    sget-object v0, Lio/grpc/internal/cm;->b:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 191
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 195
    :pswitch_1
    iget v0, v5, Lio/grpc/internal/ck;->j:I

    and-int/lit8 v0, v0, 0x4

    const/4 v8, 0x4

    if-eq v0, v8, :cond_8

    .line 196
    sget-object v0, Lio/grpc/internal/cm;->d:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 197
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 198
    :cond_8
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 199
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 200
    const/4 v8, 0x2

    if-ge v0, v8, :cond_9

    .line 201
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 202
    :cond_9
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 203
    invoke-virtual {v0}, Lio/grpc/internal/cl;->d()I

    move-result v0

    .line 204
    iput v0, v5, Lio/grpc/internal/ck;->k:I

    .line 205
    sget-object v0, Lio/grpc/internal/cm;->c:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 206
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 210
    :pswitch_2
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 211
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 212
    iget v8, v5, Lio/grpc/internal/ck;->k:I

    if-ge v0, v8, :cond_a

    .line 213
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 214
    :cond_a
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    iget v8, v5, Lio/grpc/internal/ck;->k:I

    .line 215
    invoke-virtual {v0, v8}, Lio/grpc/internal/cl;->a(I)V

    .line 216
    sget-object v0, Lio/grpc/internal/cm;->d:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 217
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 221
    :pswitch_3
    iget v0, v5, Lio/grpc/internal/ck;->j:I

    and-int/lit8 v0, v0, 0x8

    const/16 v8, 0x8

    if-ne v0, v8, :cond_b

    .line 222
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 223
    invoke-virtual {v0}, Lio/grpc/internal/cl;->c()Z

    move-result v0

    .line 224
    if-nez v0, :cond_b

    .line 225
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 226
    :cond_b
    sget-object v0, Lio/grpc/internal/cm;->e:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 227
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 231
    :pswitch_4
    iget v0, v5, Lio/grpc/internal/ck;->j:I

    and-int/lit8 v0, v0, 0x10

    const/16 v8, 0x10

    if-ne v0, v8, :cond_c

    .line 232
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 233
    invoke-virtual {v0}, Lio/grpc/internal/cl;->c()Z

    move-result v0

    .line 234
    if-nez v0, :cond_c

    .line 235
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 236
    :cond_c
    sget-object v0, Lio/grpc/internal/cm;->f:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 237
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 241
    :pswitch_5
    iget v0, v5, Lio/grpc/internal/ck;->j:I

    and-int/lit8 v0, v0, 0x2

    const/4 v8, 0x2

    if-ne v0, v8, :cond_e

    .line 242
    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 243
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 244
    const/4 v8, 0x2

    if-ge v0, v8, :cond_d

    .line 245
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 246
    :cond_d
    iget-object v0, v5, Lio/grpc/internal/ck;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v8

    long-to-int v0, v8

    const v8, 0xffff

    and-int/2addr v0, v8

    .line 247
    iget-object v8, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 248
    invoke-virtual {v8}, Lio/grpc/internal/cl;->d()I

    move-result v8

    .line 249
    if-eq v0, v8, :cond_e

    .line 250
    new-instance v0, Ljava/util/zip/ZipException;

    const-string v3, "Corrupt GZIP header"

    invoke-direct {v0, v3}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_e
    sget-object v0, Lio/grpc/internal/cm;->g:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 252
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 256
    :pswitch_6
    iget-object v0, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    if-nez v0, :cond_f

    .line 257
    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v8, 0x1

    invoke-direct {v0, v8}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    .line 259
    :goto_4
    iget-object v0, v5, Lio/grpc/internal/ck;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->reset()V

    .line 260
    iget v0, v5, Lio/grpc/internal/ck;->f:I

    iget v8, v5, Lio/grpc/internal/ck;->e:I

    sub-int/2addr v0, v8

    .line 261
    if-lez v0, :cond_10

    .line 262
    iget-object v8, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    iget-object v9, v5, Lio/grpc/internal/ck;->d:[B

    iget v10, v5, Lio/grpc/internal/ck;->e:I

    invoke-virtual {v8, v9, v10, v0}, Ljava/util/zip/Inflater;->setInput([BII)V

    .line 263
    sget-object v0, Lio/grpc/internal/cm;->h:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 265
    :goto_5
    const/4 v0, 0x1

    .line 267
    goto/16 :goto_2

    .line 258
    :cond_f
    iget-object v0, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->reset()V

    goto :goto_4

    .line 264
    :cond_10
    sget-object v0, Lio/grpc/internal/cm;->i:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    goto :goto_5

    .line 268
    :pswitch_7
    add-int v0, v7, v3

    invoke-virtual {v5, v6, v0, v8}, Lio/grpc/internal/ck;->a([BII)I

    move-result v0

    add-int/2addr v3, v0

    .line 269
    iget-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    sget-object v8, Lio/grpc/internal/cm;->j:Lio/grpc/internal/cm;

    if-ne v0, v8, :cond_11

    .line 270
    invoke-virtual {v5}, Lio/grpc/internal/ck;->a()Z

    move-result v0

    goto/16 :goto_2

    .line 271
    :cond_11
    const/4 v0, 0x1

    .line 272
    goto/16 :goto_2

    .line 274
    :pswitch_8
    iget-object v0, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_6
    const-string v8, "inflater is null"

    invoke-static {v0, v8}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 275
    iget v0, v5, Lio/grpc/internal/ck;->e:I

    iget v8, v5, Lio/grpc/internal/ck;->f:I

    if-ne v0, v8, :cond_13

    const/4 v0, 0x1

    :goto_7
    const-string v8, "inflaterInput has unconsumed bytes"

    invoke-static {v0, v8}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 276
    iget-object v0, v5, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    .line 277
    iget v0, v0, Lio/grpc/internal/ap;->a:I

    .line 278
    const/16 v8, 0x200

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 279
    if-nez v0, :cond_14

    .line 280
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 274
    :cond_12
    const/4 v0, 0x0

    goto :goto_6

    .line 275
    :cond_13
    const/4 v0, 0x0

    goto :goto_7

    .line 281
    :cond_14
    const/4 v8, 0x0

    iput v8, v5, Lio/grpc/internal/ck;->e:I

    .line 282
    iput v0, v5, Lio/grpc/internal/ck;->f:I

    .line 283
    iget-object v8, v5, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    iget-object v9, v5, Lio/grpc/internal/ck;->d:[B

    iget v10, v5, Lio/grpc/internal/ck;->e:I

    invoke-virtual {v8, v9, v10, v0}, Lio/grpc/internal/ap;->a([BII)V

    .line 284
    iget-object v8, v5, Lio/grpc/internal/ck;->g:Ljava/util/zip/Inflater;

    iget-object v9, v5, Lio/grpc/internal/ck;->d:[B

    iget v10, v5, Lio/grpc/internal/ck;->e:I

    invoke-virtual {v8, v9, v10, v0}, Ljava/util/zip/Inflater;->setInput([BII)V

    .line 285
    sget-object v0, Lio/grpc/internal/cm;->h:Lio/grpc/internal/cm;

    iput-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    .line 286
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 289
    :pswitch_9
    invoke-virtual {v5}, Lio/grpc/internal/ck;->a()Z

    move-result v0

    goto/16 :goto_2

    .line 292
    :cond_15
    if-eqz v0, :cond_16

    iget-object v0, v5, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    sget-object v4, Lio/grpc/internal/cm;->a:Lio/grpc/internal/cm;

    if-ne v0, v4, :cond_18

    iget-object v0, v5, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 294
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 295
    const/16 v4, 0xa

    if-ge v0, v4, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, v5, Lio/grpc/internal/ck;->n:Z

    .line 298
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 299
    iget v4, v0, Lio/grpc/internal/ck;->l:I

    .line 300
    const/4 v5, 0x0

    iput v5, v0, Lio/grpc/internal/ck;->l:I

    .line 302
    add-int/2addr v2, v4

    .line 303
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 304
    iget v4, v0, Lio/grpc/internal/ck;->m:I

    .line 305
    const/4 v5, 0x0

    iput v5, v0, Lio/grpc/internal/ck;->m:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 307
    add-int/2addr v1, v4

    .line 308
    if-nez v3, :cond_1a

    .line 309
    if-lez v2, :cond_17

    .line 310
    iget-object v0, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    invoke-interface {v0, v2}, Lio/grpc/internal/dx;->c(I)V

    .line 311
    iget-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    sget-object v3, Lio/grpc/internal/ea;->b:Lio/grpc/internal/ea;

    if-ne v0, v3, :cond_17

    .line 312
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v0, :cond_19

    .line 313
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->d(J)V

    .line 314
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v1

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    .line 317
    :cond_17
    :goto_9
    const/4 v0, 0x0

    .line 351
    :goto_a
    return v0

    .line 295
    :cond_18
    const/4 v0, 0x0

    goto :goto_8

    .line 315
    :cond_19
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Lio/grpc/internal/ez;->d(J)V

    .line 316
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v2

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    goto :goto_9

    .line 318
    :cond_1a
    :try_start_6
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    iget-object v4, p0, Lio/grpc/internal/dw;->h:[B

    iget v5, p0, Lio/grpc/internal/dw;->i:I

    invoke-static {v4, v5, v3}, Lio/grpc/internal/eo;->a([BII)Lio/grpc/internal/en;

    move-result-object v4

    invoke-virtual {v0, v4}, Lio/grpc/internal/ap;->a(Lio/grpc/internal/en;)V

    .line 319
    iget v0, p0, Lio/grpc/internal/dw;->i:I

    add-int/2addr v0, v3

    iput v0, p0, Lio/grpc/internal/dw;->i:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 325
    :cond_1b
    :try_start_7
    iget-object v3, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 326
    iget v3, v3, Lio/grpc/internal/ap;->a:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 327
    if-nez v3, :cond_1e

    .line 328
    if-lez v2, :cond_1c

    .line 329
    iget-object v0, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    invoke-interface {v0, v2}, Lio/grpc/internal/dx;->c(I)V

    .line 330
    iget-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    sget-object v3, Lio/grpc/internal/ea;->b:Lio/grpc/internal/ea;

    if-ne v0, v3, :cond_1c

    .line 331
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v0, :cond_1d

    .line 332
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->d(J)V

    .line 333
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v1

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    .line 336
    :cond_1c
    :goto_b
    const/4 v0, 0x0

    goto :goto_a

    .line 334
    :cond_1d
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Lio/grpc/internal/ez;->d(J)V

    .line 335
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v2

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    goto :goto_b

    .line 337
    :cond_1e
    :try_start_8
    iget-object v3, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 338
    iget v3, v3, Lio/grpc/internal/ap;->a:I

    .line 339
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 340
    add-int/2addr v2, v0

    .line 341
    iget-object v3, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    iget-object v4, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    invoke-virtual {v4, v0}, Lio/grpc/internal/ap;->b(I)Lio/grpc/internal/ap;

    move-result-object v0

    invoke-virtual {v3, v0}, Lio/grpc/internal/ap;->a(Lio/grpc/internal/en;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 343
    :cond_1f
    if-lez v2, :cond_20

    .line 344
    iget-object v0, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    invoke-interface {v0, v2}, Lio/grpc/internal/dx;->c(I)V

    .line 345
    iget-object v0, p0, Lio/grpc/internal/dw;->j:Lio/grpc/internal/ea;

    sget-object v3, Lio/grpc/internal/ea;->b:Lio/grpc/internal/ea;

    if-ne v0, v3, :cond_20

    .line 346
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v0, :cond_21

    .line 347
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lio/grpc/internal/ez;->d(J)V

    .line 348
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v1

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    .line 351
    :cond_20
    :goto_c
    const/4 v0, 0x1

    goto/16 :goto_a

    .line 349
    :cond_21
    iget-object v0, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Lio/grpc/internal/ez;->d(J)V

    .line 350
    iget v0, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v0, v2

    iput v0, p0, Lio/grpc/internal/dw;->r:I

    goto :goto_c

    .line 358
    :cond_22
    iget-object v1, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    int-to-long v4, v2

    invoke-virtual {v1, v4, v5}, Lio/grpc/internal/ez;->d(J)V

    .line 359
    iget v1, p0, Lio/grpc/internal/dw;->r:I

    add-int/2addr v1, v2

    iput v1, p0, Lio/grpc/internal/dw;->r:I

    goto/16 :goto_3

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private final f()Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 360
    iget-object v0, p0, Lio/grpc/internal/dw;->f:Lhko;

    sget-object v1, Lhkg;->a:Lhkh;

    if-ne v0, v1, :cond_0

    .line 361
    sget-object v0, Lhlw;->h:Lhlw;

    iget-object v1, p0, Lio/grpc/internal/dw;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ": Can\'t decode compressed frame as compression not configured."

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Lhlw;->b()Lhmb;

    move-result-object v0

    throw v0

    .line 363
    :cond_0
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/dw;->f:Lhko;

    iget-object v1, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    const/4 v2, 0x1

    .line 364
    invoke-static {v1, v2}, Lio/grpc/internal/eo;->a(Lio/grpc/internal/en;Z)Ljava/io/InputStream;

    move-result-object v1

    invoke-interface {v0, v1}, Lhko;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 365
    new-instance v1, Lio/grpc/internal/dz;

    iget v2, p0, Lio/grpc/internal/dw;->b:I

    iget-object v3, p0, Lio/grpc/internal/dw;->d:Lio/grpc/internal/ez;

    iget-object v4, p0, Lio/grpc/internal/dw;->e:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lio/grpc/internal/dz;-><init>(Ljava/io/InputStream;ILio/grpc/internal/ez;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 366
    :catch_0
    move-exception v0

    .line 367
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lio/grpc/internal/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-direct {p0}, Lio/grpc/internal/dw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {p0}, Lio/grpc/internal/dw;->close()V

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/grpc/internal/dw;->s:Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 15
    iput p1, p0, Lio/grpc/internal/dw;->b:I

    .line 16
    return-void
.end method

.method public final a(Lhko;)V
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Already set full stream decompressor"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 18
    const-string v0, "Can\'t pass an empty decompressor"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhko;

    iput-object v0, p0, Lio/grpc/internal/dw;->f:Lhko;

    .line 19
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lio/grpc/internal/ck;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    iget-object v0, p0, Lio/grpc/internal/dw;->f:Lhko;

    sget-object v3, Lhkg;->a:Lhkh;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "per-message decompressor already set"

    invoke-static {v0, v3}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 21
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "full stream decompressor already set"

    invoke-static {v1, v0}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 22
    const-string v0, "Can\'t pass a null full stream decompressor"

    .line 23
    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/ck;

    iput-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 25
    return-void

    :cond_0
    move v0, v2

    .line 20
    goto :goto_0

    :cond_1
    move v1, v2

    .line 21
    goto :goto_1
.end method

.method public final a(Lio/grpc/internal/en;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    const-string v0, "data"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    :try_start_0
    invoke-virtual {p0}, Lio/grpc/internal/dw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/grpc/internal/dw;->s:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    .line 36
    :goto_0
    if-nez v0, :cond_6

    .line 37
    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v0, :cond_4

    .line 38
    iget-object v3, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 39
    iget-boolean v0, v3, Lio/grpc/internal/ck;->i:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    const-string v4, "GzipInflatingBuffer is closed"

    invoke-static {v0, v4}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 40
    iget-object v0, v3, Lio/grpc/internal/ck;->a:Lio/grpc/internal/ap;

    invoke-virtual {v0, p1}, Lio/grpc/internal/ap;->a(Lio/grpc/internal/en;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, v3, Lio/grpc/internal/ck;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :goto_2
    :try_start_1
    invoke-direct {p0}, Lio/grpc/internal/dw;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 46
    :goto_3
    if-eqz v2, :cond_1

    .line 47
    invoke-interface {p1}, Lio/grpc/internal/en;->close()V

    .line 50
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 35
    goto :goto_0

    :cond_3
    move v0, v2

    .line 39
    goto :goto_1

    .line 43
    :cond_4
    :try_start_2
    iget-object v0, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    invoke-virtual {v0, p1}, Lio/grpc/internal/ap;->a(Lio/grpc/internal/en;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 48
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_5

    .line 49
    invoke-interface {p1}, Lio/grpc/internal/en;->close()V

    :cond_5
    throw v0

    .line 48
    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_3
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 26
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "numMessages must be > 0"

    invoke-static {v0, v1}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 27
    invoke-virtual {p0}, Lio/grpc/internal/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    :goto_1
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29
    :cond_1
    iget-wide v0, p0, Lio/grpc/internal/dw;->o:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lio/grpc/internal/dw;->o:J

    .line 30
    invoke-direct {p0}, Lio/grpc/internal/dw;->d()V

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 57
    invoke-virtual {p0}, Lio/grpc/internal/dw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 60
    iget v0, v0, Lio/grpc/internal/ap;->a:I

    .line 61
    if-lez v0, :cond_6

    move v0, v1

    .line 62
    :goto_1
    :try_start_0
    iget-object v3, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    if-eqz v3, :cond_3

    .line 63
    if-nez v0, :cond_2

    iget-object v3, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 64
    iget-boolean v0, v3, Lio/grpc/internal/ck;->i:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    const-string v4, "GzipInflatingBuffer is closed"

    invoke-static {v0, v4}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 65
    iget-object v0, v3, Lio/grpc/internal/ck;->c:Lio/grpc/internal/cl;

    .line 66
    invoke-virtual {v0}, Lio/grpc/internal/cl;->b()I

    move-result v0

    .line 67
    if-nez v0, :cond_1

    iget-object v0, v3, Lio/grpc/internal/ck;->h:Lio/grpc/internal/cm;

    sget-object v3, Lio/grpc/internal/cm;->a:Lio/grpc/internal/cm;

    if-eq v0, v3, :cond_8

    :cond_1
    move v0, v1

    .line 68
    :goto_3
    if-eqz v0, :cond_9

    :cond_2
    move v0, v1

    .line 69
    :goto_4
    iget-object v1, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    invoke-virtual {v1}, Lio/grpc/internal/ck;->close()V

    .line 70
    :cond_3
    iget-object v1, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    if-eqz v1, :cond_4

    .line 71
    iget-object v1, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    invoke-virtual {v1}, Lio/grpc/internal/ap;->close()V

    .line 72
    :cond_4
    iget-object v1, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    if-eqz v1, :cond_5

    .line 73
    iget-object v1, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    invoke-virtual {v1}, Lio/grpc/internal/ap;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :cond_5
    iput-object v5, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 75
    iput-object v5, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 76
    iput-object v5, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    .line 81
    iget-object v1, p0, Lio/grpc/internal/dw;->a:Lio/grpc/internal/dx;

    invoke-interface {v1, v0}, Lio/grpc/internal/dx;->a(Z)V

    goto :goto_0

    :cond_6
    move v0, v2

    .line 61
    goto :goto_1

    :cond_7
    move v0, v2

    .line 64
    goto :goto_2

    :cond_8
    move v0, v2

    .line 67
    goto :goto_3

    :cond_9
    move v0, v2

    .line 68
    goto :goto_4

    .line 78
    :catchall_0
    move-exception v0

    iput-object v5, p0, Lio/grpc/internal/dw;->g:Lio/grpc/internal/ck;

    .line 79
    iput-object v5, p0, Lio/grpc/internal/dw;->n:Lio/grpc/internal/ap;

    .line 80
    iput-object v5, p0, Lio/grpc/internal/dw;->m:Lio/grpc/internal/ap;

    throw v0
.end method
