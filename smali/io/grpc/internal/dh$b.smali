.class public final Lio/grpc/internal/dh$b;
.super Lhla;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/dh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public a:Lhky;

.field public final b:Lhlm;

.field public final synthetic c:Lio/grpc/internal/dh;


# direct methods
.method constructor <init>(Lio/grpc/internal/dh;Lhlm;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    invoke-direct {p0}, Lhla;-><init>()V

    .line 2
    const-string v0, "NameResolver"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlm;

    iput-object v0, p0, Lio/grpc/internal/dh$b;->b:Lhlm;

    .line 3
    return-void
.end method


# virtual methods
.method public final synthetic a(Lhks;Lhjs;)Lhld;
    .locals 12

    .prologue
    .line 51
    .line 52
    const-string v0, "addressGroup"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "attrs"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 55
    iget-boolean v0, v0, Lio/grpc/internal/dh;->C:Z

    .line 56
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Channel is terminated"

    invoke-static {v0, v1}, Lgtn;->b(ZLjava/lang/Object;)V

    .line 57
    new-instance v11, Lio/grpc/internal/dh$e;

    iget-object v0, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    invoke-direct {v11, v0, p2}, Lio/grpc/internal/dh$e;-><init>(Lio/grpc/internal/dh;Lhjs;)V

    .line 58
    new-instance v0, Lio/grpc/internal/ct;

    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 60
    iget-object v1, v1, Lio/grpc/internal/dh;->r:Lhjw;

    invoke-virtual {v1}, Lhjw;->a()Ljava/lang/String;

    move-result-object v2

    .line 61
    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 62
    iget-object v3, v1, Lio/grpc/internal/dh;->s:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 64
    iget-object v4, v1, Lio/grpc/internal/dh;->q:Lio/grpc/internal/s;

    .line 65
    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 66
    iget-object v5, v1, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    .line 67
    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 69
    iget-object v1, v1, Lio/grpc/internal/dh;->g:Lio/grpc/internal/ao;

    .line 70
    invoke-interface {v1}, Lio/grpc/internal/ao;->a()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 71
    iget-object v7, v1, Lio/grpc/internal/dh;->o:Lgtu;

    .line 72
    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 73
    iget-object v8, v1, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    .line 74
    new-instance v9, Lio/grpc/internal/do;

    invoke-direct {v9, p0, v11}, Lio/grpc/internal/do;-><init>(Lio/grpc/internal/dh$b;Lio/grpc/internal/dh$e;)V

    iget-object v1, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 76
    iget-object v10, v1, Lio/grpc/internal/dh;->u:Lio/grpc/internal/ei;

    move-object v1, p1

    .line 77
    invoke-direct/range {v0 .. v10}, Lio/grpc/internal/ct;-><init>(Lhks;Ljava/lang/String;Ljava/lang/String;Lio/grpc/internal/s;Lio/grpc/internal/ao;Ljava/util/concurrent/ScheduledExecutorService;Lgtu;Lio/grpc/internal/ad;Lio/grpc/internal/ct$a;Lio/grpc/internal/ei;)V

    .line 78
    iput-object v0, v11, Lio/grpc/internal/dh$e;->a:Lio/grpc/internal/ct;

    .line 79
    sget-object v1, Lio/grpc/internal/dh;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    const-string v3, "io.grpc.internal.ManagedChannelImpl$LbHelperImpl"

    const-string v4, "createSubchannel"

    const-string v5, "[{0}] {1} created for {2}"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 81
    iget-object v8, v8, Lio/grpc/internal/dh;->j:Lio/grpc/internal/dg;

    .line 82
    aput-object v8, v6, v7

    const/4 v7, 0x1

    .line 83
    iget-object v8, v0, Lio/grpc/internal/ct;->b:Lio/grpc/internal/dg;

    .line 84
    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object p1, v6, v7

    .line 85
    invoke-virtual/range {v1 .. v6}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    new-instance v1, Lio/grpc/internal/dp;

    invoke-direct {v1, p0, v0}, Lio/grpc/internal/dp;-><init>(Lio/grpc/internal/dh$b;Lio/grpc/internal/ct;)V

    invoke-virtual {p0, v1}, Lio/grpc/internal/dh$b;->a(Ljava/lang/Runnable;)V

    .line 88
    return-object v11

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhkj;Lhle;)V
    .locals 1

    .prologue
    .line 4
    const-string v0, "newState"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    const-string v0, "newPicker"

    invoke-static {p2, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    new-instance v0, Lio/grpc/internal/dq;

    invoke-direct {v0, p0, p2, p1}, Lio/grpc/internal/dq;-><init>(Lio/grpc/internal/dh$b;Lhle;Lhkj;)V

    invoke-virtual {p0, v0}, Lio/grpc/internal/dh$b;->a(Ljava/lang/Runnable;)V

    .line 7
    return-void
.end method

.method public final a(Lhld;Lhks;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8
    instance-of v0, p1, Lio/grpc/internal/dh$e;

    const-string v2, "subchannel must have been returned from createSubchannel"

    invoke-static {v0, v2}, Lgtn;->a(ZLjava/lang/Object;)V

    .line 9
    check-cast p1, Lio/grpc/internal/dh$e;

    iget-object v2, p1, Lio/grpc/internal/dh$e;->a:Lio/grpc/internal/ct;

    .line 11
    :try_start_0
    iget-object v3, v2, Lio/grpc/internal/ct;->f:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 12
    :try_start_1
    iget-object v0, v2, Lio/grpc/internal/ct;->h:Lhks;

    .line 13
    iput-object p2, v2, Lio/grpc/internal/ct;->h:Lhks;

    .line 14
    iget-object v4, v2, Lio/grpc/internal/ct;->q:Lhkk;

    .line 15
    iget-object v4, v4, Lhkk;->a:Lhkj;

    .line 16
    sget-object v5, Lhkj;->b:Lhkj;

    if-eq v4, v5, :cond_0

    iget-object v4, v2, Lio/grpc/internal/ct;->q:Lhkk;

    .line 17
    iget-object v4, v4, Lhkk;->a:Lhkj;

    .line 18
    sget-object v5, Lhkj;->a:Lhkj;

    if-ne v4, v5, :cond_4

    .line 20
    :cond_0
    iget-object v0, v0, Lhks;->a:Ljava/util/List;

    .line 21
    iget v4, v2, Lio/grpc/internal/ct;->i:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/SocketAddress;

    .line 23
    iget-object v4, p2, Lhks;->a:Ljava/util/List;

    .line 24
    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 25
    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    .line 26
    iput v0, v2, Lio/grpc/internal/ct;->i:I

    move-object v0, v1

    .line 38
    :goto_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39
    iget-object v1, v2, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    .line 42
    if-eqz v0, :cond_1

    .line 43
    sget-object v1, Lhlw;->i:Lhlw;

    const-string v2, "InternalSubchannel closed transport due to address change"

    .line 44
    invoke-virtual {v1, v2}, Lhlw;->a(Ljava/lang/String;)Lhlw;

    move-result-object v1

    .line 45
    invoke-interface {v0, v1}, Lio/grpc/internal/du;->a(Lhlw;)V

    .line 46
    :cond_1
    return-void

    .line 27
    :cond_2
    :try_start_2
    iget-object v0, v2, Lio/grpc/internal/ct;->q:Lhkk;

    .line 28
    iget-object v0, v0, Lhkk;->a:Lhkj;

    .line 29
    sget-object v1, Lhkj;->b:Lhkj;

    if-ne v0, v1, :cond_3

    .line 30
    iget-object v0, v2, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 31
    const/4 v1, 0x0

    iput-object v1, v2, Lio/grpc/internal/ct;->p:Lio/grpc/internal/du;

    .line 32
    const/4 v1, 0x0

    iput v1, v2, Lio/grpc/internal/ct;->i:I

    .line 33
    sget-object v1, Lhkj;->d:Lhkj;

    invoke-virtual {v2, v1}, Lio/grpc/internal/ct;->a(Lhkj;)V

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 41
    :catchall_1
    move-exception v0

    iget-object v1, v2, Lio/grpc/internal/ct;->g:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0

    .line 34
    :cond_3
    :try_start_4
    iget-object v0, v2, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 35
    const/4 v1, 0x0

    iput-object v1, v2, Lio/grpc/internal/ct;->o:Lio/grpc/internal/at;

    .line 36
    const/4 v1, 0x0

    iput v1, v2, Lio/grpc/internal/ct;->i:I

    .line 37
    invoke-virtual {v2}, Lio/grpc/internal/ct;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lio/grpc/internal/dh$b;->c:Lio/grpc/internal/dh;

    .line 48
    iget-object v0, v0, Lio/grpc/internal/dh;->k:Lio/grpc/internal/ad;

    .line 49
    invoke-virtual {v0, p1}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 50
    return-void
.end method
