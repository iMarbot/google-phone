.class final Lio/grpc/internal/v$b;
.super Lhkd;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/grpc/internal/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# static fields
.field private static g:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private static h:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private static i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private static j:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private static k:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

.field private static l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;


# instance fields
.field public volatile a:J

.field public volatile b:J

.field public volatile c:J

.field public volatile d:J

.field public volatile e:J

.field public volatile f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "a"

    .line 15
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->g:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 16
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "b"

    .line 17
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->h:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 18
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "c"

    .line 19
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 20
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "d"

    .line 21
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->j:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 22
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "e"

    .line 23
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->k:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 24
    const-class v0, Lio/grpc/internal/v$b;

    const-string v1, "f"

    .line 25
    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/v$b;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    .line 26
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lhkd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lio/grpc/internal/v$b;->i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 3
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lio/grpc/internal/v$b;->g:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndIncrement(Ljava/lang/Object;)J

    .line 13
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lio/grpc/internal/v$b;->k:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 7
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lio/grpc/internal/v$b;->h:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndIncrement(Ljava/lang/Object;)J

    .line 11
    return-void
.end method

.method public final c(J)V
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lio/grpc/internal/v$b;->j:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 5
    return-void
.end method

.method public final d(J)V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lio/grpc/internal/v$b;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 9
    return-void
.end method
