.class final Lio/grpc/internal/ay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lio/grpc/internal/du;


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lio/grpc/internal/ad;

.field public d:Ljava/lang/Runnable;

.field public e:Ljava/lang/Runnable;

.field public f:Lio/grpc/internal/dv;

.field public g:Ljava/util/Collection;

.field public h:Lhlw;

.field public i:Lhle;

.field public j:J

.field private k:Lio/grpc/internal/dg;

.field private l:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lio/grpc/internal/ad;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/grpc/internal/dg;->a(Ljava/lang/String;)Lio/grpc/internal/dg;

    move-result-object v0

    iput-object v0, p0, Lio/grpc/internal/ay;->k:Lio/grpc/internal/dg;

    .line 3
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    .line 4
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    .line 5
    iput-object p1, p0, Lio/grpc/internal/ay;->b:Ljava/util/concurrent/Executor;

    .line 6
    iput-object p2, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    .line 7
    return-void
.end method

.method private final a(Lhlc;)Lio/grpc/internal/be;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lio/grpc/internal/be;

    .line 51
    invoke-direct {v0, p0, p1}, Lio/grpc/internal/be;-><init>(Lio/grpc/internal/ay;Lhlc;)V

    .line 53
    iget-object v1, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v1, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 55
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    iget-object v2, p0, Lio/grpc/internal/ay;->l:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 56
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final B_()Lio/grpc/internal/dg;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lio/grpc/internal/ay;->k:Lio/grpc/internal/dg;

    return-object v0
.end method

.method public final a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    .locals 8

    .prologue
    .line 13
    :try_start_0
    new-instance v3, Lio/grpc/internal/eh;

    invoke-direct {v3, p1, p2, p3}, Lio/grpc/internal/eh;-><init>(Lhlk;Lhlh;Lhjv;)V

    .line 14
    iget-object v4, p0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 15
    :try_start_1
    iget-object v0, p0, Lio/grpc/internal/ay;->h:Lhlw;

    if-nez v0, :cond_1

    .line 16
    iget-object v0, p0, Lio/grpc/internal/ay;->i:Lhle;

    if-nez v0, :cond_0

    .line 17
    invoke-direct {p0, v3}, Lio/grpc/internal/ay;->a(Lhlc;)Lio/grpc/internal/be;

    move-result-object v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    .line 45
    :goto_0
    return-object v0

    .line 20
    :cond_0
    :try_start_2
    iget-object v2, p0, Lio/grpc/internal/ay;->i:Lhle;

    .line 21
    iget-wide v0, p0, Lio/grpc/internal/ay;->j:J

    .line 25
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 26
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Lhle;->a()Lhlb;

    move-result-object v2

    .line 29
    iget-boolean v4, p3, Lhjv;->h:Z

    .line 30
    invoke-static {v2, v4}, Lio/grpc/internal/cf;->a(Lhlb;Z)Lio/grpc/internal/am;

    move-result-object v2

    .line 31
    if-eqz v2, :cond_2

    .line 33
    invoke-virtual {v3}, Lhlc;->c()Lhlk;

    move-result-object v0

    invoke-virtual {v3}, Lhlc;->b()Lhlh;

    move-result-object v1

    invoke-virtual {v3}, Lhlc;->a()Lhjv;

    move-result-object v3

    .line 34
    invoke-interface {v2, v0, v1, v3}, Lio/grpc/internal/am;->a(Lhlk;Lhlh;Lhjv;)Lio/grpc/internal/al;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 35
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 22
    :cond_1
    :try_start_4
    new-instance v0, Lio/grpc/internal/ca;

    iget-object v1, p0, Lio/grpc/internal/ay;->h:Lhlw;

    invoke-direct {v0, v1}, Lio/grpc/internal/ca;-><init>(Lhlw;)V

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 23
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 49
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    throw v0

    .line 37
    :cond_2
    :try_start_7
    iget-object v4, p0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 38
    :try_start_8
    iget-object v2, p0, Lio/grpc/internal/ay;->h:Lhlw;

    if-eqz v2, :cond_3

    .line 39
    new-instance v0, Lio/grpc/internal/ca;

    iget-object v1, p0, Lio/grpc/internal/ay;->h:Lhlw;

    invoke-direct {v0, v1}, Lio/grpc/internal/ca;-><init>(Lhlw;)V

    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 40
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 42
    :cond_3
    :try_start_9
    iget-wide v6, p0, Lio/grpc/internal/ay;->j:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 43
    invoke-direct {p0, v3}, Lio/grpc/internal/ay;->a(Lhlc;)Lio/grpc/internal/be;

    move-result-object v0

    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 44
    iget-object v1, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v1}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 46
    :cond_4
    :try_start_a
    iget-object v2, p0, Lio/grpc/internal/ay;->i:Lhle;

    .line 47
    iget-wide v0, p0, Lio/grpc/internal/ay;->j:J

    .line 48
    monitor-exit v4

    goto :goto_1

    :catchall_2
    move-exception v0

    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1
.end method

.method public final a(Lio/grpc/internal/dv;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 8
    iput-object p1, p0, Lio/grpc/internal/ay;->f:Lio/grpc/internal/dv;

    .line 9
    new-instance v0, Lio/grpc/internal/az;

    invoke-direct {v0, p1}, Lio/grpc/internal/az;-><init>(Lio/grpc/internal/dv;)V

    iput-object v0, p0, Lio/grpc/internal/ay;->l:Ljava/lang/Runnable;

    .line 10
    new-instance v0, Lio/grpc/internal/ba;

    invoke-direct {v0, p1}, Lio/grpc/internal/ba;-><init>(Lio/grpc/internal/dv;)V

    iput-object v0, p0, Lio/grpc/internal/ay;->d:Ljava/lang/Runnable;

    .line 11
    new-instance v0, Lio/grpc/internal/bb;

    invoke-direct {v0, p1}, Lio/grpc/internal/bb;-><init>(Lio/grpc/internal/dv;)V

    iput-object v0, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 12
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lhlw;)V
    .locals 3

    .prologue
    .line 58
    iget-object v1, p0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 59
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ay;->h:Lhlw;

    if-eqz v0, :cond_0

    .line 60
    monitor-exit v1

    .line 68
    :goto_0
    return-void

    .line 61
    :cond_0
    iput-object p1, p0, Lio/grpc/internal/ay;->h:Lhlw;

    .line 62
    iget-object v0, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    new-instance v2, Lio/grpc/internal/bc;

    invoke-direct {v2, p0, p1}, Lio/grpc/internal/bc;-><init>(Lio/grpc/internal/ay;Lhlw;)V

    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 63
    iget-object v0, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    iget-object v2, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 66
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    iget-object v0, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lio/grpc/internal/an;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method is not expected to be called"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lhlw;)V
    .locals 4

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lio/grpc/internal/ay;->a(Lhlw;)V

    .line 70
    iget-object v1, p0, Lio/grpc/internal/ay;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    :try_start_0
    iget-object v0, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    .line 72
    iget-object v2, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 73
    const/4 v3, 0x0

    iput-object v3, p0, Lio/grpc/internal/ay;->e:Ljava/lang/Runnable;

    .line 74
    iget-object v3, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lio/grpc/internal/ay;->g:Ljava/util/Collection;

    .line 76
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    if-eqz v2, :cond_2

    .line 78
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/grpc/internal/be;

    .line 79
    invoke-virtual {v0, p1}, Lio/grpc/internal/be;->a(Lhlw;)V

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 81
    :cond_1
    iget-object v0, p0, Lio/grpc/internal/ay;->c:Lio/grpc/internal/ad;

    invoke-virtual {v0, v2}, Lio/grpc/internal/ad;->a(Ljava/lang/Runnable;)Lio/grpc/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lio/grpc/internal/ad;->a()V

    .line 82
    :cond_2
    return-void
.end method
