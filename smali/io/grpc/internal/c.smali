.class public abstract Lio/grpc/internal/c;
.super Lhlg;
.source "PG"


# static fields
.field public static final a:J

.field private static m:J

.field private static n:Lio/grpc/internal/eg;

.field private static o:Lhln;

.field private static p:Lhkz;

.field private static q:Lhkp;

.field private static r:Lhki;


# instance fields
.field public b:Lio/grpc/internal/eg;

.field public final c:Ljava/util/List;

.field public d:Lhln;

.field public final e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lhkz;

.field public h:Z

.field public i:Lhkp;

.field public j:Lhki;

.field public k:J

.field public l:I

.field private s:Lio/grpc/internal/ei;

.field private t:Z

.field private u:Z

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lio/grpc/internal/c;->m:J

    .line 42
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lio/grpc/internal/c;->a:J

    .line 43
    sget-object v0, Lio/grpc/internal/cf;->k:Lio/grpc/internal/ew;

    .line 44
    invoke-static {v0}, Lio/grpc/internal/ey;->a(Lio/grpc/internal/ew;)Lio/grpc/internal/ey;

    move-result-object v0

    sput-object v0, Lio/grpc/internal/c;->n:Lio/grpc/internal/eg;

    .line 45
    sget-object v0, Lhlp;->b:Lhln;

    .line 46
    sput-object v0, Lio/grpc/internal/c;->o:Lhln;

    .line 47
    sget-object v0, Lhlt;->a:Lhlt;

    .line 48
    sput-object v0, Lio/grpc/internal/c;->p:Lhkz;

    .line 49
    sget-object v0, Lhkp;->a:Lhkp;

    .line 50
    sput-object v0, Lio/grpc/internal/c;->q:Lhkp;

    .line 51
    sget-object v0, Lhki;->a:Lhki;

    .line 52
    sput-object v0, Lio/grpc/internal/c;->r:Lhki;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1
    invoke-direct {p0}, Lhlg;-><init>()V

    .line 2
    sget-object v0, Lio/grpc/internal/c;->n:Lio/grpc/internal/eg;

    iput-object v0, p0, Lio/grpc/internal/c;->b:Lio/grpc/internal/eg;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/grpc/internal/c;->c:Ljava/util/List;

    .line 4
    sget-object v0, Lio/grpc/internal/c;->o:Lhln;

    iput-object v0, p0, Lio/grpc/internal/c;->d:Lhln;

    .line 5
    sget-object v0, Lio/grpc/internal/ei;->a:Lio/grpc/internal/ei;

    iput-object v0, p0, Lio/grpc/internal/c;->s:Lio/grpc/internal/ei;

    .line 6
    sget-object v0, Lio/grpc/internal/c;->p:Lhkz;

    iput-object v0, p0, Lio/grpc/internal/c;->g:Lhkz;

    .line 7
    sget-object v0, Lio/grpc/internal/c;->q:Lhkp;

    iput-object v0, p0, Lio/grpc/internal/c;->i:Lhkp;

    .line 8
    sget-object v0, Lio/grpc/internal/c;->r:Lhki;

    iput-object v0, p0, Lio/grpc/internal/c;->j:Lhki;

    .line 9
    sget-wide v0, Lio/grpc/internal/c;->m:J

    iput-wide v0, p0, Lio/grpc/internal/c;->k:J

    .line 10
    const/high16 v0, 0x400000

    iput v0, p0, Lio/grpc/internal/c;->l:I

    .line 11
    iput-boolean v2, p0, Lio/grpc/internal/c;->t:Z

    .line 12
    iput-boolean v2, p0, Lio/grpc/internal/c;->u:Z

    .line 13
    iput-boolean v2, p0, Lio/grpc/internal/c;->v:Z

    .line 14
    const-string v0, "target"

    invoke-static {p1, v0}, Lgtn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lio/grpc/internal/c;->e:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public final a()Lhlf;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 16
    new-instance v0, Lio/grpc/internal/dh;

    .line 17
    invoke-virtual {p0}, Lio/grpc/internal/c;->b()Lio/grpc/internal/ao;

    move-result-object v2

    new-instance v3, Lio/grpc/internal/s;

    invoke-direct {v3}, Lio/grpc/internal/s;-><init>()V

    sget-object v1, Lio/grpc/internal/cf;->k:Lio/grpc/internal/ew;

    .line 18
    invoke-static {v1}, Lio/grpc/internal/ey;->a(Lio/grpc/internal/ew;)Lio/grpc/internal/ey;

    move-result-object v4

    sget-object v5, Lio/grpc/internal/cf;->m:Lgtu;

    .line 20
    new-instance v6, Ljava/util/ArrayList;

    iget-object v1, p0, Lio/grpc/internal/c;->c:Ljava/util/List;

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 21
    iget-boolean v1, p0, Lio/grpc/internal/c;->t:Z

    if-eqz v1, :cond_0

    .line 22
    sget-object v1, Lgyy;->a:Lgzb;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 24
    :goto_0
    if-eqz v1, :cond_0

    .line 25
    new-instance v7, Lio/grpc/internal/v;

    sget-object v8, Lio/grpc/internal/cf;->m:Lgtu;

    const/4 v9, 0x1

    iget-boolean v10, p0, Lio/grpc/internal/c;->u:Z

    invoke-direct {v7, v1, v8, v9, v10}, Lio/grpc/internal/v;-><init>(Lgza;Lgtu;ZZ)V

    .line 27
    iget-object v1, v7, Lio/grpc/internal/v;->g:Lio/grpc/internal/v$d;

    .line 28
    invoke-interface {v6, v11, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 29
    :cond_0
    iget-boolean v1, p0, Lio/grpc/internal/c;->v:Z

    if-eqz v1, :cond_1

    .line 30
    new-instance v1, Lio/grpc/internal/z;

    .line 31
    sget-object v7, Lhpt;->a:Lhpo;

    invoke-virtual {v7}, Lhpo;->a()Lhpr;

    move-result-object v7

    .line 32
    sget-object v8, Lhpt;->a:Lhpo;

    invoke-virtual {v8}, Lhpo;->b()Lhpy;

    move-result-object v8

    .line 33
    invoke-virtual {v8}, Lhpy;->a()Lhpw;

    move-result-object v8

    invoke-direct {v1, v7, v8}, Lio/grpc/internal/z;-><init>(Lhpr;Lhpw;)V

    .line 35
    iget-object v1, v1, Lio/grpc/internal/z;->e:Lio/grpc/internal/z$e;

    .line 36
    invoke-interface {v6, v11, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38
    :cond_1
    iget-object v7, p0, Lio/grpc/internal/c;->s:Lio/grpc/internal/ei;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lio/grpc/internal/dh;-><init>(Lio/grpc/internal/c;Lio/grpc/internal/ao;Lio/grpc/internal/s;Lio/grpc/internal/eg;Lgtu;Ljava/util/List;Lio/grpc/internal/ei;)V

    .line 39
    return-object v0

    .line 22
    :cond_2
    sget-object v1, Lgyy;->a:Lgzb;

    invoke-virtual {v1}, Lgzb;->a()Lgza;

    move-result-object v1

    goto :goto_0
.end method

.method public abstract b()Lio/grpc/internal/ao;
.end method

.method public c()Lhjs;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lhjs;->b:Lhjs;

    return-object v0
.end method
