.class public final Lio/grpc/internal/em;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/net/InetSocketAddress;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/net/InetSocketAddress;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lgtn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    iput-object v0, p0, Lio/grpc/internal/em;->a:Ljava/net/InetSocketAddress;

    .line 3
    iput-object p2, p0, Lio/grpc/internal/em;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lio/grpc/internal/em;->c:Ljava/lang/String;

    .line 5
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 6
    instance-of v1, p1, Lio/grpc/internal/em;

    if-nez v1, :cond_1

    .line 12
    :cond_0
    :goto_0
    return v0

    .line 8
    :cond_1
    check-cast p1, Lio/grpc/internal/em;

    .line 9
    iget-object v1, p0, Lio/grpc/internal/em;->a:Ljava/net/InetSocketAddress;

    iget-object v2, p1, Lio/grpc/internal/em;->a:Ljava/net/InetSocketAddress;

    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lio/grpc/internal/em;->b:Ljava/lang/String;

    iget-object v2, p1, Lio/grpc/internal/em;->b:Ljava/lang/String;

    .line 10
    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lio/grpc/internal/em;->c:Ljava/lang/String;

    iget-object v2, p1, Lio/grpc/internal/em;->c:Ljava/lang/String;

    .line 11
    invoke-static {v1, v2}, Lgtl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lio/grpc/internal/em;->a:Ljava/net/InetSocketAddress;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lio/grpc/internal/em;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lio/grpc/internal/em;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 14
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 15
    return v0
.end method
