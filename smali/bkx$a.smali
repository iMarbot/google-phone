.class public final enum Lbkx$a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbkx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation


# static fields
.field public static final enum a:Lbkx$a;

.field public static final enum b:Lbkx$a;

.field public static final enum c:Lbkx$a;

.field public static final enum d:Lbkx$a;

.field public static final e:Lhby;

.field private static enum f:Lbkx$a;

.field private static synthetic h:[Lbkx$a;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lbkx$a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lbkx$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkx$a;->f:Lbkx$a;

    .line 14
    new-instance v0, Lbkx$a;

    const-string v1, "HTTP_RESPONSE_ERROR"

    invoke-direct {v0, v1, v3, v3}, Lbkx$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkx$a;->a:Lbkx$a;

    .line 15
    new-instance v0, Lbkx$a;

    const-string v1, "WRONG_KIND_VALUE"

    invoke-direct {v0, v1, v4, v4}, Lbkx$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkx$a;->b:Lbkx$a;

    .line 16
    new-instance v0, Lbkx$a;

    const-string v1, "NO_ITEM_FOUND"

    invoke-direct {v0, v1, v5, v5}, Lbkx$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkx$a;->c:Lbkx$a;

    .line 17
    new-instance v0, Lbkx$a;

    const-string v1, "JSON_PARSING_ERROR"

    invoke-direct {v0, v1, v6, v6}, Lbkx$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbkx$a;->d:Lbkx$a;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lbkx$a;

    sget-object v1, Lbkx$a;->f:Lbkx$a;

    aput-object v1, v0, v2

    sget-object v1, Lbkx$a;->a:Lbkx$a;

    aput-object v1, v0, v3

    sget-object v1, Lbkx$a;->b:Lbkx$a;

    aput-object v1, v0, v4

    sget-object v1, Lbkx$a;->c:Lbkx$a;

    aput-object v1, v0, v5

    sget-object v1, Lbkx$a;->d:Lbkx$a;

    aput-object v1, v0, v6

    sput-object v0, Lbkx$a;->h:[Lbkx$a;

    .line 19
    new-instance v0, Lbky;

    invoke-direct {v0}, Lbky;-><init>()V

    sput-object v0, Lbkx$a;->e:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 11
    iput p3, p0, Lbkx$a;->g:I

    .line 12
    return-void
.end method

.method public static a(I)Lbkx$a;
    .locals 1

    .prologue
    .line 3
    packed-switch p0, :pswitch_data_0

    .line 9
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4
    :pswitch_0
    sget-object v0, Lbkx$a;->f:Lbkx$a;

    goto :goto_0

    .line 5
    :pswitch_1
    sget-object v0, Lbkx$a;->a:Lbkx$a;

    goto :goto_0

    .line 6
    :pswitch_2
    sget-object v0, Lbkx$a;->b:Lbkx$a;

    goto :goto_0

    .line 7
    :pswitch_3
    sget-object v0, Lbkx$a;->c:Lbkx$a;

    goto :goto_0

    .line 8
    :pswitch_4
    sget-object v0, Lbkx$a;->d:Lbkx$a;

    goto :goto_0

    .line 3
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static values()[Lbkx$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lbkx$a;->h:[Lbkx$a;

    invoke-virtual {v0}, [Lbkx$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbkx$a;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 2
    iget v0, p0, Lbkx$a;->g:I

    return v0
.end method
