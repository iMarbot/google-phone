.class final Lfsy;
.super Lfmx;
.source "PG"


# instance fields
.field public final requestTask:Lfsw;


# direct methods
.method constructor <init>(Lfsw;)V
    .locals 2

    .prologue
    .line 1
    sget-wide v0, Lfmx;->maxNetworkOperationTimeMillis:J

    invoke-direct {p0, v0, v1}, Lfmx;-><init>(J)V

    .line 2
    iput-object p1, p0, Lfsy;->requestTask:Lfsw;

    .line 3
    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackgroundTimed([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lfsy;->doInBackgroundTimed([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackgroundTimed([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lfsy;->requestTask:Lfsw;

    invoke-interface {v0}, Lfsw;->doInBackgroundTimed()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lfsy;->requestTask:Lfsw;

    invoke-interface {v0, p1}, Lfsw;->onPostExecute(Ljava/lang/Object;)V

    .line 8
    return-void
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lfsy;->requestTask:Lfsw;

    invoke-interface {v0}, Lfsw;->onPreExecute()V

    .line 5
    return-void
.end method
