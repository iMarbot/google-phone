.class final Lbzr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ClickableViewAccessibility"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Landroid/animation/Animator;

.field private e:Landroid/view/View;

.field private f:Lbzu;

.field private g:Landroid/view/VelocityTracker;

.field private h:Lcbo;

.field private i:F

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:Z

.field private t:F

.field private u:Z

.field private v:F

.field private w:F

.field private x:F

.field private y:Lcam;


# direct methods
.method private constructor <init>(Landroid/view/View;Lbzu;Lcam;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/high16 v3, 0x43160000    # 150.0f

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-boolean v0, p0, Lbzr;->a:Z

    .line 6
    iput-boolean v0, p0, Lbzr;->b:Z

    .line 7
    iput-object p1, p0, Lbzr;->e:Landroid/view/View;

    .line 8
    iput-object p2, p0, Lbzr;->f:Lbzu;

    .line 9
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 10
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lbzr;->n:F

    .line 11
    new-instance v1, Lcbo;

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v1, v0, v2}, Lcbo;-><init>(Landroid/content/Context;F)V

    iput-object v1, p0, Lbzr;->h:Lcbo;

    .line 12
    const/high16 v1, 0x42200000    # 40.0f

    invoke-static {v0, v1}, Lapw;->a(Landroid/content/Context;F)F

    move-result v1

    iput v1, p0, Lbzr;->t:F

    .line 13
    invoke-static {v0, v3}, Lapw;->a(Landroid/content/Context;F)F

    move-result v1

    iput v1, p0, Lbzr;->v:F

    .line 14
    invoke-static {v0, v3}, Lapw;->a(Landroid/content/Context;F)F

    move-result v1

    iput v1, p0, Lbzr;->w:F

    .line 16
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget v1, p0, Lbzr;->v:F

    .line 17
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lbzr;->x:F

    .line 18
    iput-object p3, p0, Lbzr;->y:Lcam;

    .line 19
    return-void
.end method

.method public static a(Landroid/view/View;Lbzu;Lcam;)Lbzr;
    .locals 1

    .prologue
    .line 1
    new-instance v0, Lbzr;

    invoke-direct {v0, p0, p1, p2}, Lbzr;-><init>(Landroid/view/View;Lbzu;Lcam;)V

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3
    return-object v0
.end method

.method private final a(FZF)V
    .locals 4

    .prologue
    .line 165
    iput p1, p0, Lbzr;->o:F

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzr;->l:Z

    .line 167
    float-to-double v0, p3

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 168
    const/4 v0, 0x0

    iget v1, p0, Lbzr;->o:F

    iget v2, p0, Lbzr;->v:F

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lbzr;->p:F

    .line 169
    iget-object v0, p0, Lbzr;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lbzr;->o:F

    iget v2, p0, Lbzr;->w:F

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lbzr;->q:F

    .line 170
    iget v0, p0, Lbzr;->o:F

    iput v0, p0, Lbzr;->r:F

    .line 171
    :cond_0
    if-eqz p2, :cond_1

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbzr;->k:Z

    .line 173
    invoke-direct {p0}, Lbzr;->c()V

    .line 174
    invoke-virtual {p0, p3}, Lbzr;->a(F)V

    .line 175
    :cond_1
    return-void
.end method

.method private final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 160
    :cond_0
    return-void
.end method

.method private final a(Landroid/view/MotionEvent;FZ)V
    .locals 8

    .prologue
    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lbzr;->m:I

    .line 76
    iget-boolean v0, p0, Lbzr;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbzr;->k:Z

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lbzr;->o:F

    sub-float v0, p2, v0

    .line 77
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lbzr;->n:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 78
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    if-eqz p3, :cond_1a

    .line 79
    :cond_1
    const/4 v4, 0x0

    .line 80
    const/4 v0, 0x0

    .line 81
    iget-object v1, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_2

    .line 82
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 83
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v4

    .line 84
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    .line 85
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 86
    invoke-static {v0, v4}, Ljava/lang/Math;->copySign(FF)F

    move-result v0

    .line 88
    :cond_2
    iget-object v1, p0, Lbzr;->y:Lcam;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lbzr;->y:Lcam;

    .line 89
    iget-object v1, v1, Lcam;->c:Lcaq;

    .line 90
    iget-boolean v1, v1, Lcaq;->c:Z

    .line 91
    if-eqz v1, :cond_c

    .line 92
    iget-object v1, p0, Lbzr;->y:Lcam;

    .line 93
    iget-object v2, v1, Lcam;->d:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v1, v1, Lcam;->c:Lcaq;

    .line 95
    iget-object v1, v1, Lcaq;->b:Lcao;

    .line 96
    iget-object v2, v1, Lcao;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lcao;->a(Ljava/util/ArrayList;)F

    move-result v2

    iget-object v1, v1, Lcao;->b:Ljava/util/ArrayList;

    invoke-static {v1}, Lcao;->a(Ljava/util/ArrayList;)F

    move-result v1

    add-float/2addr v1, v2

    .line 98
    const/high16 v2, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_7

    const/4 v1, 0x1

    .line 99
    :goto_0
    if-eqz v1, :cond_8

    const/4 v1, 0x1

    .line 100
    :goto_1
    if-eqz v1, :cond_b

    .line 101
    iget-boolean v1, p0, Lbzr;->u:Z

    if-eqz v1, :cond_9

    .line 102
    const-string v1, "FlingUpDownTouchHandler.isFalseTouch"

    const-string v2, "rejecting false touch"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    const/4 v1, 0x1

    move v7, v1

    .line 109
    :goto_2
    if-nez v7, :cond_3

    iget-boolean v1, p0, Lbzr;->k:Z

    if-eqz v1, :cond_3

    if-nez p3, :cond_3

    .line 110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_d

    :cond_3
    const/4 v1, 0x1

    .line 111
    :goto_3
    if-eqz v1, :cond_e

    const/4 v0, 0x0

    move v6, v0

    .line 128
    :goto_4
    int-to-float v0, v6

    .line 129
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lbzr;->i:F

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 130
    new-instance v0, Lbzt;

    invoke-direct {v0, p0}, Lbzt;-><init>(Lbzr;)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 133
    if-nez v6, :cond_19

    .line 134
    iget-object v0, p0, Lbzr;->h:Lcbo;

    iget v2, p0, Lbzr;->i:F

    int-to-float v3, v6

    invoke-virtual {v0, v1, v2, v3, v4}, Lcbo;->a(Landroid/animation/Animator;FFF)V

    .line 136
    :goto_5
    if-nez v6, :cond_4

    if-eqz v7, :cond_4

    .line 137
    const/4 v4, 0x0

    .line 138
    :cond_4
    const/4 v0, 0x0

    cmpl-float v0, v4, v0

    if-nez v0, :cond_5

    .line 139
    const-wide/16 v2, 0x15e

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 140
    :cond_5
    new-instance v0, Lbzs;

    invoke-direct {v0, p0}, Lbzs;-><init>(Lbzr;)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 141
    iput-object v1, p0, Lbzr;->d:Landroid/animation/Animator;

    .line 142
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzr;->c:Z

    .line 150
    :goto_6
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_6

    .line 151
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    .line 153
    :cond_6
    return-void

    .line 98
    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    .line 99
    :cond_8
    const/4 v1, 0x0

    goto :goto_1

    .line 104
    :cond_9
    const-string v1, "FlingUpDownTouchHandler.isFalseTouch"

    const-string v2, "Suspected false touch, but not using false touch rejection for this gesture"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    :cond_a
    const/4 v1, 0x0

    move v7, v1

    goto :goto_2

    .line 106
    :cond_b
    const/4 v1, 0x0

    move v7, v1

    goto :goto_2

    .line 107
    :cond_c
    iget-boolean v1, p0, Lbzr;->s:Z

    if-nez v1, :cond_a

    const/4 v1, 0x1

    move v7, v1

    goto :goto_2

    .line 110
    :cond_d
    const/4 v1, 0x0

    goto :goto_3

    .line 112
    :cond_e
    invoke-direct {p0, p2}, Lbzr;->b(F)F

    move-result v3

    .line 113
    iget-object v1, p0, Lbzr;->h:Lcbo;

    .line 114
    iget v1, v1, Lcbo;->a:F

    .line 116
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_f

    .line 117
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    .line 118
    :cond_f
    iget-boolean v2, p0, Lbzr;->b:Z

    if-eqz v2, :cond_10

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v1, v2, v1

    if-gez v1, :cond_13

    .line 119
    :cond_10
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3f4ccccd    # 0.8f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_12

    .line 120
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_11

    const/4 v0, 0x1

    move v6, v0

    goto/16 :goto_4

    :cond_11
    const/4 v0, -0x1

    move v6, v0

    goto/16 :goto_4

    .line 121
    :cond_12
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_4

    .line 122
    :cond_13
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_14

    const/4 v1, 0x1

    move v2, v1

    :goto_7
    const/4 v1, 0x0

    cmpl-float v1, v3, v1

    if-lez v1, :cond_15

    const/4 v1, 0x1

    :goto_8
    if-ne v2, v1, :cond_16

    const/4 v1, 0x1

    .line 123
    :goto_9
    if-nez v1, :cond_17

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_17

    .line 124
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_4

    .line 122
    :cond_14
    const/4 v1, 0x0

    move v2, v1

    goto :goto_7

    :cond_15
    const/4 v1, 0x0

    goto :goto_8

    :cond_16
    const/4 v1, 0x0

    goto :goto_9

    .line 125
    :cond_17
    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_18

    const/4 v0, 0x1

    move v6, v0

    goto/16 :goto_4

    :cond_18
    const/4 v0, -0x1

    move v6, v0

    goto/16 :goto_4

    .line 135
    :cond_19
    iget-object v0, p0, Lbzr;->h:Lcbo;

    iget v2, p0, Lbzr;->i:F

    int-to-float v3, v6

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v5}, Lcbo;->a(Landroid/animation/Animator;FFFF)V

    goto/16 :goto_5

    .line 147
    :cond_1a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzr;->c:Z

    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbzr;->a(F)V

    .line 149
    invoke-virtual {p0}, Lbzr;->b()V

    goto/16 :goto_6
.end method

.method private final b(F)F
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 154
    iget v0, p0, Lbzr;->r:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    move v2, v1

    .line 155
    :goto_0
    if-eqz v2, :cond_1

    iget v0, p0, Lbzr;->q:F

    .line 156
    :goto_1
    iget v3, p0, Lbzr;->r:F

    sub-float v3, p1, v3

    iget v4, p0, Lbzr;->r:F

    sub-float/2addr v0, v4

    div-float/2addr v3, v0

    .line 157
    if-eqz v2, :cond_2

    const/4 v0, -0x1

    :goto_2
    int-to-float v0, v0

    mul-float/2addr v0, v3

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Lapw;->b(FFF)F

    move-result v0

    return v0

    .line 154
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 155
    :cond_1
    iget v0, p0, Lbzr;->p:F

    goto :goto_1

    :cond_2
    move v0, v1

    .line 157
    goto :goto_2
.end method

.method private final c()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbzr;->c:Z

    .line 177
    iget-object v0, p0, Lbzr;->f:Lbzu;

    invoke-interface {v0}, Lbzu;->T()V

    .line 178
    return-void
.end method

.method private final d()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lbzr;->d:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lbzr;->d:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 181
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lbzr;->d()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbzr;->a:Z

    .line 23
    return-void
.end method

.method final a(F)V
    .locals 2

    .prologue
    .line 182
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3dcccccd    # 0.1f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbzr;->l:Z

    .line 184
    :cond_0
    iput p1, p0, Lbzr;->i:F

    .line 185
    iget-object v0, p0, Lbzr;->f:Lbzu;

    invoke-interface {v0, p1}, Lbzu;->a(F)V

    .line 186
    return-void
.end method

.method final b()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 161
    iget v2, p0, Lbzr;->i:F

    cmpl-float v2, v2, v4

    if-nez v2, :cond_1

    .line 162
    iget-object v2, p0, Lbzr;->f:Lbzu;

    iget-boolean v3, p0, Lbzr;->l:Z

    if-nez v3, :cond_0

    :goto_0
    invoke-interface {v2, v0}, Lbzu;->f(Z)V

    .line 164
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 162
    goto :goto_0

    .line 163
    :cond_1
    iget-object v2, p0, Lbzr;->f:Lbzu;

    iget v3, p0, Lbzr;->i:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    :goto_2
    invoke-interface {v2, v0}, Lbzu;->g(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 24
    iget-object v0, p0, Lbzr;->y:Lcam;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lbzr;->y:Lcam;

    .line 26
    iget-boolean v3, v0, Lcam;->e:Z

    if-eqz v3, :cond_0

    .line 27
    iget-object v0, v0, Lcam;->c:Lcaq;

    invoke-virtual {v0, p2}, Lcaq;->a(Landroid/view/MotionEvent;)V

    .line 28
    :cond_0
    iget-boolean v0, p0, Lbzr;->a:Z

    if-nez v0, :cond_2

    .line 74
    :cond_1
    :goto_0
    return v1

    .line 30
    :cond_2
    iget-boolean v0, p0, Lbzr;->j:Z

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_1

    .line 32
    :cond_3
    iget v0, p0, Lbzr;->m:I

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 33
    if-gez v0, :cond_4

    .line 35
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lbzr;->m:I

    move v0, v1

    .line 36
    :cond_4
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 37
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_5
    :goto_1
    :pswitch_0
    move v1, v2

    .line 74
    goto :goto_0

    .line 38
    :pswitch_1
    iget v3, p0, Lbzr;->x:F

    cmpg-float v3, v0, v3

    if-ltz v3, :cond_1

    .line 40
    iput-boolean v1, p0, Lbzr;->j:Z

    .line 41
    iget v3, p0, Lbzr;->i:F

    invoke-direct {p0, v0, v1, v3}, Lbzr;->a(FZF)V

    .line 42
    iput-boolean v1, p0, Lbzr;->s:Z

    .line 43
    iget-object v0, p0, Lbzr;->f:Lbzu;

    invoke-interface {v0, p2}, Lbzu;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lbzr;->u:Z

    .line 44
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    if-nez v0, :cond_7

    .line 46
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_6

    .line 47
    iget-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 48
    :cond_6
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lbzr;->g:Landroid/view/VelocityTracker;

    .line 49
    :cond_7
    invoke-direct {p0, p2}, Lbzr;->a(Landroid/view/MotionEvent;)V

    .line 50
    invoke-direct {p0}, Lbzr;->d()V

    .line 51
    iget-object v0, p0, Lbzr;->d:Landroid/animation/Animator;

    if-eqz v0, :cond_8

    move v1, v2

    :cond_8
    iput-boolean v1, p0, Lbzr;->k:Z

    .line 52
    invoke-direct {p0}, Lbzr;->c()V

    goto :goto_1

    .line 54
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 55
    iget v3, p0, Lbzr;->m:I

    if-ne v3, v0, :cond_5

    .line 56
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    if-eq v3, v0, :cond_9

    .line 57
    :goto_2
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 58
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lbzr;->m:I

    .line 59
    iget v1, p0, Lbzr;->i:F

    invoke-direct {p0, v0, v2, v1}, Lbzr;->a(FZF)V

    goto :goto_1

    :cond_9
    move v1, v2

    .line 56
    goto :goto_2

    .line 61
    :pswitch_3
    iput-boolean v2, p0, Lbzr;->j:Z

    .line 62
    invoke-direct {p0, p2, v0, v2}, Lbzr;->a(Landroid/view/MotionEvent;FZ)V

    goto/16 :goto_0

    .line 64
    :pswitch_4
    iget v1, p0, Lbzr;->o:F

    sub-float v1, v0, v1

    .line 65
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lbzr;->n:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_a

    .line 66
    iput-boolean v2, p0, Lbzr;->k:Z

    .line 67
    :cond_a
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lbzr;->t:F

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_b

    .line 68
    iput-boolean v2, p0, Lbzr;->s:Z

    .line 69
    :cond_b
    invoke-direct {p0, v0}, Lbzr;->b(F)F

    move-result v0

    invoke-virtual {p0, v0}, Lbzr;->a(F)V

    .line 70
    invoke-direct {p0, p2}, Lbzr;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 72
    :pswitch_5
    invoke-direct {p0, p2}, Lbzr;->a(Landroid/view/MotionEvent;)V

    .line 73
    invoke-direct {p0, p2, v0, v1}, Lbzr;->a(Landroid/view/MotionEvent;FZ)V

    goto/16 :goto_1

    .line 37
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
