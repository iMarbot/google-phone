.class final Ldpa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lccr;

.field private c:Lccq;

.field private d:Z

.field private synthetic e:Ldox;


# direct methods
.method public constructor <init>(Ldox;Ljava/lang/String;Lccr;Lccq;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Ldpa;->e:Ldox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Ldpa;->a:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Ldpa;->b:Lccr;

    .line 4
    iput-object p4, p0, Ldpa;->c:Lccq;

    .line 5
    iput-boolean p5, p0, Ldpa;->d:Z

    .line 6
    return-void
.end method

.method private final a(Ljava/lang/String;)Landroid/util/Pair;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 87
    iget-object v0, p0, Ldpa;->e:Ldox;

    .line 88
    iget-object v0, v0, Ldox;->b:Landroid/content/Context;

    .line 89
    invoke-static {v0}, Ldhh;->h(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const-string v0, "LookupRunnable.doLookup"

    const-string v1, "callerId and spam disabled"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :goto_0
    return-object v10

    .line 92
    :cond_0
    iget-object v0, p0, Ldpa;->e:Ldox;

    iget-object v0, p0, Ldpa;->e:Ldox;

    .line 93
    iget-object v0, v0, Ldox;->b:Landroid/content/Context;

    .line 95
    invoke-static {v0}, Ldox;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v11

    .line 97
    array-length v0, v11

    if-nez v0, :cond_1

    .line 98
    const-string v0, "LookupRunnable.doLookup"

    const-string v1, "No google account found. Skipping reverse lookup."

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Ldpa;->e:Ldox;

    .line 103
    iget-object v0, v0, Ldox;->b:Landroid/content/Context;

    .line 104
    invoke-static {v0}, Lbib;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, p0, Ldpa;->a:Ljava/lang/String;

    .line 106
    invoke-static {v1, p1, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move v7, v8

    move v5, v9

    move-object v2, v10

    move-object v0, v10

    .line 108
    :goto_1
    array-length v1, v11

    if-ge v7, v1, :cond_2

    if-ge v7, v13, :cond_2

    .line 109
    aget-object v0, v11, v7

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 110
    const-string v0, "LookupRunnable.doLookup"

    const-string v1, "performing lookup. accountName: %s, normalizedNumber: %s, formattedNumber: %s, includePlaces: %b"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    .line 111
    invoke-static {v2}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v8

    .line 112
    invoke-static {p1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v9

    const/4 v6, 0x2

    .line 113
    invoke-static {v4}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v3, v6

    .line 114
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v13

    .line 115
    invoke-static {v0, v1, v3}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Ldpa;->e:Ldox;

    .line 118
    iget-object v0, v0, Ldox;->d:Ldou;

    .line 119
    iget-object v1, p0, Ldpa;->e:Ldox;

    .line 121
    iget-object v1, v1, Ldox;->b:Landroid/content/Context;

    .line 122
    iget-boolean v6, p0, Ldpa;->d:Z

    move-object v3, p1

    .line 123
    invoke-virtual/range {v0 .. v6}, Ldou;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Ldow;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_5

    .line 126
    iget-object v0, v1, Ldow;->a:Ljava/lang/String;

    .line 127
    if-eqz v0, :cond_5

    .line 128
    const-string v0, "LookupRunnable.doLookup"

    const-string v3, "found result. displayName: %s"

    new-array v5, v9, [Ljava/lang/Object;

    .line 130
    iget-object v6, v1, Ldow;->a:Ljava/lang/String;

    .line 131
    invoke-static {v6}, Lapw;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    .line 132
    invoke-static {v0, v3, v5}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 135
    :cond_2
    sget-object v1, Ldox;->a:Ldoi;

    .line 136
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 137
    iget-object v1, v0, Ldow;->a:Ljava/lang/String;

    .line 138
    if-eqz v1, :cond_4

    .line 140
    iget-boolean v1, v0, Ldow;->h:Z

    .line 141
    if-eqz v1, :cond_6

    .line 142
    sget-object v1, Lbkm$a;->h:Lbkm$a;

    .line 143
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    .line 147
    :goto_2
    new-instance v1, Lbml;

    invoke-direct {v1}, Lbml;-><init>()V

    .line 148
    iput-object p1, v1, Lbml;->k:Ljava/lang/String;

    .line 150
    iget-object v3, v0, Ldow;->c:Ljava/lang/String;

    .line 151
    iput-object v3, v1, Lbml;->h:Ljava/lang/String;

    .line 152
    iget-object v3, v1, Lbml;->h:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 153
    iput-object v4, v1, Lbml;->h:Ljava/lang/String;

    .line 155
    :cond_3
    iget-object v3, v0, Ldow;->a:Ljava/lang/String;

    .line 156
    iput-object v3, v1, Lbml;->d:Ljava/lang/String;

    .line 158
    iget v3, v0, Ldow;->d:I

    .line 159
    iput v3, v1, Lbml;->f:I

    .line 161
    iget-object v3, v0, Ldow;->e:Ljava/lang/String;

    .line 162
    iput-object v3, v1, Lbml;->g:Ljava/lang/String;

    .line 164
    iget-object v3, v0, Ldow;->f:Ljava/lang/String;

    .line 166
    if-nez v3, :cond_7

    :goto_3
    iput-object v10, v1, Lbml;->m:Landroid/net/Uri;

    .line 168
    iget-object v3, v0, Ldow;->i:Ljava/lang/String;

    .line 169
    iput-object v3, v1, Lbml;->o:Ljava/lang/String;

    .line 170
    invoke-static {v1}, Ldoi;->b(Lbml;)Ldoj;

    move-result-object v3

    .line 172
    iget-boolean v1, v0, Ldow;->h:Z

    .line 175
    if-eqz v1, :cond_8

    .line 176
    sget-object v1, Lbko$a;->d:Lbko$a;

    .line 177
    :goto_4
    const-string v4, "Google Caller ID"

    const-wide/32 v6, 0x7fffffff

    .line 178
    invoke-virtual {v3, v1, v4, v6, v7}, Ldoj;->a(Lbko$a;Ljava/lang/String;J)V

    .line 180
    iget-object v1, v0, Ldow;->g:Ljava/lang/String;

    .line 182
    iput-object v1, v3, Ldoj;->e:Ljava/lang/String;

    .line 183
    sget-object v1, Ldox;->a:Ldoi;

    .line 184
    iget-object v4, p0, Ldpa;->e:Ldox;

    .line 185
    iget-object v4, v4, Ldox;->b:Landroid/content/Context;

    .line 186
    invoke-virtual {v1, v4, v3}, Ldoi;->a(Landroid/content/Context;Lbmj;)V

    .line 187
    :cond_4
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v10

    goto/16 :goto_0

    .line 134
    :cond_5
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v5, v8

    move-object v0, v1

    goto/16 :goto_1

    .line 145
    :cond_6
    sget-object v1, Lbkm$a;->i:Lbkm$a;

    .line 146
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_2

    .line 166
    :cond_7
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    goto :goto_3

    .line 177
    :cond_8
    sget-object v1, Lbko$a;->e:Lbko$a;

    goto :goto_4
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v0, 0x1

    const/4 v10, 0x0

    .line 7
    :try_start_0
    iget-object v1, p0, Ldpa;->a:Ljava/lang/String;

    iget-object v2, p0, Ldpa;->e:Ldox;

    .line 9
    iget-object v2, v2, Ldox;->c:Ljava/lang/String;

    .line 10
    invoke-static {v1, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 11
    iget-object v1, p0, Ldpa;->a:Ljava/lang/String;

    .line 12
    invoke-static {v1}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-static {v11}, Lapw;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "raw number: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", formatted e164: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    if-nez v11, :cond_0

    .line 86
    :goto_0
    return-void

    .line 18
    :cond_0
    sget-object v1, Ldox;->a:Ldoi;

    .line 19
    if-eqz v1, :cond_5

    .line 20
    sget-object v1, Ldox;->a:Ldoi;

    .line 21
    iget-object v2, p0, Ldpa;->e:Ldox;

    .line 22
    iget-object v2, v2, Ldox;->b:Landroid/content/Context;

    .line 23
    invoke-virtual {v1, v2, v11}, Ldoi;->b(Landroid/content/Context;Ljava/lang/String;)Ldoj;

    move-result-object v1

    .line 24
    if-eqz v1, :cond_5

    .line 26
    iget-object v12, v1, Ldoj;->a:Lbml;

    .line 28
    if-eqz v12, :cond_5

    sget-object v1, Lbml;->a:Lbml;

    .line 29
    invoke-virtual {v12, v1}, Lbml;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, v12, Lbml;->n:Z

    if-nez v1, :cond_5

    iget-object v1, v12, Lbml;->q:Lbko$a;

    .line 31
    sget-object v2, Lbko$a;->b:Lbko$a;

    if-ne v1, v2, :cond_2

    .line 32
    :goto_1
    if-nez v0, :cond_5

    .line 33
    iget-object v0, v12, Lbml;->m:Landroid/net/Uri;

    if-nez v0, :cond_3

    move-object v6, v10

    .line 34
    :goto_2
    new-instance v0, Ldow;

    iget-object v1, v12, Lbml;->d:Ljava/lang/String;

    iget-object v2, v12, Lbml;->k:Ljava/lang/String;

    iget-object v3, v12, Lbml;->h:Ljava/lang/String;

    iget v4, v12, Lbml;->f:I

    iget-object v5, v12, Lbml;->g:Ljava/lang/String;

    iget-object v7, v12, Lbml;->c:Ljava/lang/String;

    iget-object v8, v12, Lbml;->q:Lbko$a;

    .line 35
    invoke-static {v8}, Ldoj;->b(Lbko$a;)Z

    move-result v8

    iget-object v9, v12, Lbml;->o:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Ldow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 36
    iget-object v1, v12, Lbml;->q:Lbko$a;

    invoke-virtual {v1}, Lbko$a;->ordinal()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 55
    sget-object v1, Lbkm$a;->j:Lbkm$a;

    .line 56
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    .line 57
    :goto_3
    if-nez v0, :cond_4

    .line 58
    invoke-direct {p0, v11}, Ldpa;->a(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 59
    if-eqz v1, :cond_4

    .line 60
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldow;

    .line 61
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    move-object v6, v0

    .line 62
    :goto_4
    if-eqz v6, :cond_1

    .line 63
    iget-object v0, v6, Ldow;->a:Ljava/lang/String;

    .line 64
    if-eqz v0, :cond_1

    .line 65
    iget-object v0, v6, Ldow;->f:Ljava/lang/String;

    .line 66
    if-eqz v0, :cond_1

    .line 67
    new-instance v0, Ldoz;

    iget-object v1, p0, Ldpa;->e:Ldox;

    .line 69
    iget-object v3, v6, Ldow;->b:Ljava/lang/String;

    .line 71
    iget-object v4, v6, Ldow;->f:Ljava/lang/String;

    .line 72
    iget-object v5, p0, Ldpa;->c:Lccq;

    .line 73
    invoke-direct/range {v0 .. v5}, Ldoz;-><init>(Ldox;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lccq;)V

    .line 75
    iget-object v1, p0, Ldpa;->e:Ldox;

    .line 76
    iget-object v1, v1, Ldox;->e:Ljava/util/concurrent/ExecutorService;

    .line 77
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 78
    :cond_1
    iget-object v0, p0, Ldpa;->b:Lccr;

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 79
    iget-object v1, p0, Ldpa;->e:Ldox;

    .line 80
    iget-object v1, v1, Ldox;->f:Landroid/os/Handler;

    .line 81
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    const-string v1, "LookupRunnable.run"

    const-string v2, "Error running phone number lookup."

    invoke-static {v1, v2, v0}, Lapw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 31
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 33
    :cond_3
    :try_start_1
    iget-object v0, v12, Lbml;->m:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 37
    :pswitch_0
    sget-object v1, Lbkm$a;->k:Lbkm$a;

    .line 38
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_3

    .line 40
    :pswitch_1
    sget-object v1, Lbkm$a;->l:Lbkm$a;

    .line 41
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_3

    .line 43
    :pswitch_2
    sget-object v1, Lbkm$a;->m:Lbkm$a;

    .line 44
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_3

    .line 46
    :pswitch_3
    sget-object v1, Lbkm$a;->n:Lbkm$a;

    .line 47
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_3

    .line 49
    :pswitch_4
    sget-object v1, Lbkm$a;->o:Lbkm$a;

    .line 50
    iput-object v1, v0, Ldow;->j:Lbkm$a;

    goto :goto_3

    .line 52
    :pswitch_5
    sget-object v1, Lbkm$a;->p:Lbkm$a;

    .line 53
    iput-object v1, v0, Ldow;->j:Lbkm$a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_4
    move-object v2, v10

    move-object v6, v0

    goto :goto_4

    :cond_5
    move-object v0, v10

    goto :goto_3

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
