.class public final Lhhg;
.super Lhft;
.source "PG"


# instance fields
.field private a:Lhhc;

.field private b:Lhhf;

.field private c:Lhhe;

.field private d:Lhhd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v0, p0, Lhhg;->a:Lhhc;

    .line 4
    iput-object v0, p0, Lhhg;->b:Lhhf;

    .line 5
    iput-object v0, p0, Lhhg;->c:Lhhe;

    .line 6
    iput-object v0, p0, Lhhg;->d:Lhhd;

    .line 7
    iput-object v0, p0, Lhhg;->unknownFieldData:Lhfv;

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lhhg;->cachedSize:I

    .line 9
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 20
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 21
    iget-object v1, p0, Lhhg;->a:Lhhc;

    if-eqz v1, :cond_0

    .line 22
    const/4 v1, 0x1

    iget-object v2, p0, Lhhg;->a:Lhhc;

    .line 23
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24
    :cond_0
    iget-object v1, p0, Lhhg;->b:Lhhf;

    if-eqz v1, :cond_1

    .line 25
    const/4 v1, 0x2

    iget-object v2, p0, Lhhg;->b:Lhhf;

    .line 26
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27
    :cond_1
    iget-object v1, p0, Lhhg;->c:Lhhe;

    if-eqz v1, :cond_2

    .line 28
    const/4 v1, 0x3

    iget-object v2, p0, Lhhg;->c:Lhhe;

    .line 29
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30
    :cond_2
    iget-object v1, p0, Lhhg;->d:Lhhd;

    if-eqz v1, :cond_3

    .line 31
    const/4 v1, 0x4

    iget-object v2, p0, Lhhg;->d:Lhhd;

    .line 32
    invoke-static {v1, v2}, Lhfq;->d(ILhfz;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 1

    .prologue
    .line 34
    .line 35
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 38
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :sswitch_0
    return-object p0

    .line 40
    :sswitch_1
    iget-object v0, p0, Lhhg;->a:Lhhc;

    if-nez v0, :cond_1

    .line 41
    new-instance v0, Lhhc;

    invoke-direct {v0}, Lhhc;-><init>()V

    iput-object v0, p0, Lhhg;->a:Lhhc;

    .line 42
    :cond_1
    iget-object v0, p0, Lhhg;->a:Lhhc;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 44
    :sswitch_2
    iget-object v0, p0, Lhhg;->b:Lhhf;

    if-nez v0, :cond_2

    .line 45
    new-instance v0, Lhhf;

    invoke-direct {v0}, Lhhf;-><init>()V

    iput-object v0, p0, Lhhg;->b:Lhhf;

    .line 46
    :cond_2
    iget-object v0, p0, Lhhg;->b:Lhhf;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 48
    :sswitch_3
    iget-object v0, p0, Lhhg;->c:Lhhe;

    if-nez v0, :cond_3

    .line 49
    new-instance v0, Lhhe;

    invoke-direct {v0}, Lhhe;-><init>()V

    iput-object v0, p0, Lhhg;->c:Lhhe;

    .line 50
    :cond_3
    iget-object v0, p0, Lhhg;->c:Lhhe;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 52
    :sswitch_4
    iget-object v0, p0, Lhhg;->d:Lhhd;

    if-nez v0, :cond_4

    .line 53
    new-instance v0, Lhhd;

    invoke-direct {v0}, Lhhd;-><init>()V

    iput-object v0, p0, Lhhg;->d:Lhhd;

    .line 54
    :cond_4
    iget-object v0, p0, Lhhg;->d:Lhhd;

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    goto :goto_0

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lhhg;->a:Lhhc;

    if-eqz v0, :cond_0

    .line 11
    const/4 v0, 0x1

    iget-object v1, p0, Lhhg;->a:Lhhc;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 12
    :cond_0
    iget-object v0, p0, Lhhg;->b:Lhhf;

    if-eqz v0, :cond_1

    .line 13
    const/4 v0, 0x2

    iget-object v1, p0, Lhhg;->b:Lhhf;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 14
    :cond_1
    iget-object v0, p0, Lhhg;->c:Lhhe;

    if-eqz v0, :cond_2

    .line 15
    const/4 v0, 0x3

    iget-object v1, p0, Lhhg;->c:Lhhe;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 16
    :cond_2
    iget-object v0, p0, Lhhg;->d:Lhhd;

    if-eqz v0, :cond_3

    .line 17
    const/4 v0, 0x4

    iget-object v1, p0, Lhhg;->d:Lhhd;

    invoke-virtual {p1, v0, v1}, Lhfq;->b(ILhfz;)V

    .line 18
    :cond_3
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 19
    return-void
.end method
