.class final Lcxa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcun;
.implements Lcvl;


# instance fields
.field private a:Lcvm;

.field private b:Lcvn;

.field private c:I

.field private d:I

.field private e:Lcud;

.field private f:Ljava/util/List;

.field private g:I

.field private volatile h:Ldas;

.field private i:Ljava/io/File;

.field private j:Lcxb;


# direct methods
.method public constructor <init>(Lcvn;Lcvm;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcxa;->c:I

    .line 3
    const/4 v0, -0x1

    iput v0, p0, Lcxa;->d:I

    .line 4
    iput-object p1, p0, Lcxa;->b:Lcvn;

    .line 5
    iput-object p2, p0, Lcxa;->a:Lcvm;

    .line 6
    return-void
.end method

.method private final c()Z
    .locals 2

    .prologue
    .line 62
    iget v0, p0, Lcxa;->g:I

    iget-object v1, p0, Lcxa;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcxa;->a:Lcvm;

    iget-object v1, p0, Lcxa;->j:Lcxb;

    iget-object v2, p0, Lcxa;->h:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    sget-object v3, Lctw;->d:Lctw;

    invoke-interface {v0, v1, p1, v2, v3}, Lcvm;->a(Lcud;Ljava/lang/Exception;Lcum;Lctw;)V

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 67
    iget-object v0, p0, Lcxa;->a:Lcvm;

    iget-object v1, p0, Lcxa;->e:Lcud;

    iget-object v2, p0, Lcxa;->h:Ldas;

    iget-object v3, v2, Ldas;->c:Lcum;

    sget-object v4, Lctw;->d:Lctw;

    iget-object v5, p0, Lcxa;->j:Lcxb;

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcvm;->a(Lcud;Ljava/lang/Object;Lcum;Lctw;Lcud;)V

    .line 68
    return-void
.end method

.method public final a()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 7
    iget-object v0, p0, Lcxa;->b:Lcvn;

    invoke-virtual {v0}, Lcvn;->c()Ljava/util/List;

    move-result-object v9

    .line 8
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v8

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 10
    :cond_1
    iget-object v0, p0, Lcxa;->b:Lcvn;

    .line 11
    iget-object v1, v0, Lcvn;->c:Lcsy;

    .line 12
    iget-object v1, v1, Lcsy;->c:Lcta;

    .line 13
    iget-object v2, v0, Lcvn;->d:Ljava/lang/Object;

    .line 14
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    iget-object v3, v0, Lcvn;->g:Ljava/lang/Class;

    iget-object v0, v0, Lcvn;->k:Ljava/lang/Class;

    invoke-virtual {v1, v2, v3, v0}, Lcta;->b(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v10

    .line 16
    :cond_2
    :goto_1
    iget-object v0, p0, Lcxa;->f:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcxa;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 17
    :cond_3
    iget v0, p0, Lcxa;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcxa;->d:I

    .line 18
    iget v0, p0, Lcxa;->d:I

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_5

    .line 19
    iget v0, p0, Lcxa;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcxa;->c:I

    .line 20
    iget v0, p0, Lcxa;->c:I

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    move v1, v8

    .line 21
    goto :goto_0

    .line 22
    :cond_4
    iput v8, p0, Lcxa;->d:I

    .line 23
    :cond_5
    iget v0, p0, Lcxa;->c:I

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcud;

    .line 24
    iget v0, p0, Lcxa;->d:I

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Class;

    .line 25
    iget-object v0, p0, Lcxa;->b:Lcvn;

    invoke-virtual {v0, v6}, Lcvn;->c(Ljava/lang/Class;)Lcuk;

    move-result-object v5

    .line 26
    new-instance v0, Lcxb;

    iget-object v2, p0, Lcxa;->b:Lcvn;

    .line 27
    iget-object v2, v2, Lcvn;->n:Lcud;

    .line 28
    iget-object v3, p0, Lcxa;->b:Lcvn;

    .line 29
    iget v3, v3, Lcvn;->e:I

    .line 30
    iget-object v4, p0, Lcxa;->b:Lcvn;

    .line 32
    iget v4, v4, Lcvn;->f:I

    .line 33
    iget-object v7, p0, Lcxa;->b:Lcvn;

    .line 34
    iget-object v7, v7, Lcvn;->i:Lcuh;

    .line 35
    invoke-direct/range {v0 .. v7}, Lcxb;-><init>(Lcud;Lcud;IILcuk;Ljava/lang/Class;Lcuh;)V

    iput-object v0, p0, Lcxa;->j:Lcxb;

    .line 36
    iget-object v0, p0, Lcxa;->b:Lcvn;

    invoke-virtual {v0}, Lcvn;->a()Lcyb;

    move-result-object v0

    iget-object v2, p0, Lcxa;->j:Lcxb;

    invoke-interface {v0, v2}, Lcyb;->a(Lcud;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcxa;->i:Ljava/io/File;

    .line 37
    iget-object v0, p0, Lcxa;->i:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 38
    iput-object v1, p0, Lcxa;->e:Lcud;

    .line 39
    iget-object v0, p0, Lcxa;->b:Lcvn;

    iget-object v1, p0, Lcxa;->i:Ljava/io/File;

    invoke-virtual {v0, v1}, Lcvn;->a(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcxa;->f:Ljava/util/List;

    .line 40
    iput v8, p0, Lcxa;->g:I

    goto :goto_1

    .line 42
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcxa;->h:Ldas;

    move v1, v8

    .line 44
    :goto_2
    if-nez v1, :cond_0

    invoke-direct {p0}, Lcxa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcxa;->f:Ljava/util/List;

    iget v2, p0, Lcxa;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcxa;->g:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldar;

    .line 46
    iget-object v2, p0, Lcxa;->i:Ljava/io/File;

    iget-object v3, p0, Lcxa;->b:Lcvn;

    .line 48
    iget v3, v3, Lcvn;->e:I

    .line 49
    iget-object v4, p0, Lcxa;->b:Lcvn;

    .line 50
    iget v4, v4, Lcvn;->f:I

    .line 51
    iget-object v5, p0, Lcxa;->b:Lcvn;

    .line 53
    iget-object v5, v5, Lcvn;->i:Lcuh;

    .line 54
    invoke-interface {v0, v2, v3, v4, v5}, Ldar;->a(Ljava/lang/Object;IILcuh;)Ldas;

    move-result-object v0

    iput-object v0, p0, Lcxa;->h:Ldas;

    .line 55
    iget-object v0, p0, Lcxa;->h:Ldas;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcxa;->b:Lcvn;

    iget-object v2, p0, Lcxa;->h:Ldas;

    iget-object v2, v2, Ldas;->c:Lcum;

    invoke-interface {v2}, Lcum;->d()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcvn;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 56
    const/4 v0, 0x1

    .line 57
    iget-object v1, p0, Lcxa;->h:Ldas;

    iget-object v1, v1, Ldas;->c:Lcum;

    iget-object v2, p0, Lcxa;->b:Lcvn;

    .line 58
    iget-object v2, v2, Lcvn;->o:Lcsz;

    .line 59
    invoke-interface {v1, v2, p0}, Lcum;->a(Lcsz;Lcun;)V

    :goto_3
    move v1, v0

    .line 60
    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcxa;->h:Ldas;

    .line 64
    if-eqz v0, :cond_0

    .line 65
    iget-object v0, v0, Ldas;->c:Lcum;

    invoke-interface {v0}, Lcum;->b()V

    .line 66
    :cond_0
    return-void
.end method
