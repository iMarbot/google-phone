.class public final Leho;
.super Ljava/lang/Object;

# interfaces
.implements Ledl;
.implements Ledm;


# instance fields
.field public final a:Lesq;

.field public b:Lehp;

.field private c:Z


# direct methods
.method public constructor <init>(Lesq;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Leho;->a:Lesq;

    iput-boolean p2, p0, Leho;->c:Z

    return-void
.end method

.method private final a()V
    .locals 2

    iget-object v0, p0, Leho;->b:Lehp;

    const-string v1, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client."

    invoke-static {v0, v1}, Letf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final onConnected(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Leho;->a()V

    iget-object v0, p0, Leho;->b:Lehp;

    invoke-interface {v0, p1}, Lehp;->onConnected(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onConnectionFailed(Lecl;)V
    .locals 3

    invoke-direct {p0}, Leho;->a()V

    iget-object v0, p0, Leho;->b:Lehp;

    iget-object v1, p0, Leho;->a:Lesq;

    iget-boolean v2, p0, Leho;->c:Z

    invoke-interface {v0, p1, v1, v2}, Lehp;->a(Lecl;Lesq;Z)V

    return-void
.end method

.method public final onConnectionSuspended(I)V
    .locals 1

    invoke-direct {p0}, Leho;->a()V

    iget-object v0, p0, Leho;->b:Lehp;

    invoke-interface {v0, p1}, Lehp;->onConnectionSuspended(I)V

    return-void
.end method
