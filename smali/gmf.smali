.class public final enum Lgmf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lhbx;


# static fields
.field public static final synthetic $VALUES:[Lgmf;

.field public static final enum STATUS_FAIL:Lgmf;

.field public static final STATUS_FAIL_VALUE:I = 0x3

.field public static final enum STATUS_SUCCESS:Lgmf;

.field public static final STATUS_SUCCESS_VALUE:I = 0x1

.field public static final enum STATUS_UNKNOWN:Lgmf;

.field public static final STATUS_UNKNOWN_VALUE:I = 0x0

.field public static final enum STATUS_UNUSED:Lgmf;

.field public static final STATUS_UNUSED_VALUE:I = 0x4

.field public static final enum STATUS_WARN:Lgmf;

.field public static final STATUS_WARN_VALUE:I = 0x2

.field public static final internalValueMap:Lhby;


# instance fields
.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lgmf;

    const-string v1, "STATUS_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lgmf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmf;->STATUS_UNKNOWN:Lgmf;

    .line 16
    new-instance v0, Lgmf;

    const-string v1, "STATUS_SUCCESS"

    invoke-direct {v0, v1, v3, v3}, Lgmf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmf;->STATUS_SUCCESS:Lgmf;

    .line 17
    new-instance v0, Lgmf;

    const-string v1, "STATUS_WARN"

    invoke-direct {v0, v1, v4, v4}, Lgmf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmf;->STATUS_WARN:Lgmf;

    .line 18
    new-instance v0, Lgmf;

    const-string v1, "STATUS_FAIL"

    invoke-direct {v0, v1, v5, v5}, Lgmf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmf;->STATUS_FAIL:Lgmf;

    .line 19
    new-instance v0, Lgmf;

    const-string v1, "STATUS_UNUSED"

    invoke-direct {v0, v1, v6, v6}, Lgmf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgmf;->STATUS_UNUSED:Lgmf;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lgmf;

    sget-object v1, Lgmf;->STATUS_UNKNOWN:Lgmf;

    aput-object v1, v0, v2

    sget-object v1, Lgmf;->STATUS_SUCCESS:Lgmf;

    aput-object v1, v0, v3

    sget-object v1, Lgmf;->STATUS_WARN:Lgmf;

    aput-object v1, v0, v4

    sget-object v1, Lgmf;->STATUS_FAIL:Lgmf;

    aput-object v1, v0, v5

    sget-object v1, Lgmf;->STATUS_UNUSED:Lgmf;

    aput-object v1, v0, v6

    sput-object v0, Lgmf;->$VALUES:[Lgmf;

    .line 21
    new-instance v0, Lgmg;

    invoke-direct {v0}, Lgmg;-><init>()V

    sput-object v0, Lgmf;->internalValueMap:Lhby;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput p3, p0, Lgmf;->value:I

    .line 14
    return-void
.end method

.method public static forNumber(I)Lgmf;
    .locals 1

    .prologue
    .line 4
    packed-switch p0, :pswitch_data_0

    .line 10
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 5
    :pswitch_0
    sget-object v0, Lgmf;->STATUS_UNKNOWN:Lgmf;

    goto :goto_0

    .line 6
    :pswitch_1
    sget-object v0, Lgmf;->STATUS_SUCCESS:Lgmf;

    goto :goto_0

    .line 7
    :pswitch_2
    sget-object v0, Lgmf;->STATUS_WARN:Lgmf;

    goto :goto_0

    .line 8
    :pswitch_3
    sget-object v0, Lgmf;->STATUS_FAIL:Lgmf;

    goto :goto_0

    .line 9
    :pswitch_4
    sget-object v0, Lgmf;->STATUS_UNUSED:Lgmf;

    goto :goto_0

    .line 4
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static internalGetValueMap()Lhby;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lgmf;->internalValueMap:Lhby;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgmf;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lgmf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgmf;

    return-object v0
.end method

.method public static values()[Lgmf;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lgmf;->$VALUES:[Lgmf;

    invoke-virtual {v0}, [Lgmf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgmf;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 3
    iget v0, p0, Lgmf;->value:I

    return v0
.end method
