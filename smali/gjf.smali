.class public final Lgjf;
.super Lhft;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;

.field private e:[Lgjg;

.field private f:[Lgjh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Lhft;-><init>()V

    .line 3
    iput-object v1, p0, Lgjf;->a:Ljava/lang/String;

    .line 4
    iput-object v1, p0, Lgjf;->c:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lgjf;->b:Ljava/lang/Integer;

    .line 6
    iput-object v1, p0, Lgjf;->d:Ljava/lang/Integer;

    .line 7
    invoke-static {}, Lgjg;->a()[Lgjg;

    move-result-object v0

    iput-object v0, p0, Lgjf;->e:[Lgjg;

    .line 8
    invoke-static {}, Lgjh;->a()[Lgjh;

    move-result-object v0

    iput-object v0, p0, Lgjf;->f:[Lgjh;

    .line 9
    iput-object v1, p0, Lgjf;->unknownFieldData:Lhfv;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lgjf;->cachedSize:I

    .line 11
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-super {p0}, Lhft;->computeSerializedSize()I

    move-result v0

    .line 35
    iget-object v2, p0, Lgjf;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 36
    const/4 v2, 0x1

    iget-object v3, p0, Lgjf;->a:Ljava/lang/String;

    .line 37
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 38
    :cond_0
    iget-object v2, p0, Lgjf;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 39
    const/4 v2, 0x2

    iget-object v3, p0, Lgjf;->c:Ljava/lang/String;

    .line 40
    invoke-static {v2, v3}, Lhfq;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41
    :cond_1
    iget-object v2, p0, Lgjf;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 42
    const/4 v2, 0x3

    iget-object v3, p0, Lgjf;->b:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 44
    :cond_2
    iget-object v2, p0, Lgjf;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 45
    const/4 v2, 0x4

    iget-object v3, p0, Lgjf;->d:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lhfq;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 47
    :cond_3
    iget-object v2, p0, Lgjf;->e:[Lgjg;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lgjf;->e:[Lgjg;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 48
    :goto_0
    iget-object v3, p0, Lgjf;->e:[Lgjg;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 49
    iget-object v3, p0, Lgjf;->e:[Lgjg;

    aget-object v3, v3, v0

    .line 50
    if-eqz v3, :cond_4

    .line 51
    const/4 v4, 0x5

    .line 52
    invoke-static {v4, v3}, Lhfq;->c(ILhfz;)I

    move-result v3

    add-int/2addr v2, v3

    .line 53
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 54
    :cond_6
    iget-object v2, p0, Lgjf;->f:[Lgjh;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lgjf;->f:[Lgjh;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 55
    :goto_1
    iget-object v2, p0, Lgjf;->f:[Lgjh;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 56
    iget-object v2, p0, Lgjf;->f:[Lgjh;

    aget-object v2, v2, v1

    .line 57
    if-eqz v2, :cond_7

    .line 58
    const/16 v3, 0x8

    .line 59
    invoke-static {v3, v2}, Lhfq;->d(ILhfz;)I

    move-result v2

    add-int/2addr v0, v2

    .line 60
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lhfp;)Lhfz;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 62
    .line 63
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lhfp;->a()I

    move-result v0

    .line 64
    sparse-switch v0, :sswitch_data_0

    .line 66
    invoke-super {p0, p1, v0}, Lhft;->storeUnknownField(Lhfp;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    :sswitch_0
    return-object p0

    .line 68
    :sswitch_1
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjf;->a:Ljava/lang/String;

    goto :goto_0

    .line 70
    :sswitch_2
    invoke-virtual {p1}, Lhfp;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgjf;->c:Ljava/lang/String;

    goto :goto_0

    .line 73
    :sswitch_3
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 74
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjf;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 77
    :sswitch_4
    invoke-virtual {p1}, Lhfp;->g()I

    move-result v0

    .line 78
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgjf;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 80
    :sswitch_5
    const/16 v0, 0x2b

    .line 81
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 82
    iget-object v0, p0, Lgjf;->e:[Lgjg;

    if-nez v0, :cond_2

    move v0, v1

    .line 83
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjg;

    .line 84
    if-eqz v0, :cond_1

    .line 85
    iget-object v3, p0, Lgjf;->e:[Lgjg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 87
    new-instance v3, Lgjg;

    invoke-direct {v3}, Lgjg;-><init>()V

    aput-object v3, v2, v0

    .line 88
    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v4}, Lhfp;->a(Lhfz;I)V

    .line 89
    invoke-virtual {p1}, Lhfp;->a()I

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 82
    :cond_2
    iget-object v0, p0, Lgjf;->e:[Lgjg;

    array-length v0, v0

    goto :goto_1

    .line 91
    :cond_3
    new-instance v3, Lgjg;

    invoke-direct {v3}, Lgjg;-><init>()V

    aput-object v3, v2, v0

    .line 92
    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Lhfp;->a(Lhfz;I)V

    .line 93
    iput-object v2, p0, Lgjf;->e:[Lgjg;

    goto :goto_0

    .line 95
    :sswitch_6
    const/16 v0, 0x42

    .line 96
    invoke-static {p1, v0}, Lhgc;->a(Lhfp;I)I

    move-result v2

    .line 97
    iget-object v0, p0, Lgjf;->f:[Lgjh;

    if-nez v0, :cond_5

    move v0, v1

    .line 98
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgjh;

    .line 99
    if-eqz v0, :cond_4

    .line 100
    iget-object v3, p0, Lgjf;->f:[Lgjh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    .line 102
    new-instance v3, Lgjh;

    invoke-direct {v3}, Lgjh;-><init>()V

    aput-object v3, v2, v0

    .line 103
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lhfp;->a(Lhfz;)V

    .line 104
    invoke-virtual {p1}, Lhfp;->a()I

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 97
    :cond_5
    iget-object v0, p0, Lgjf;->f:[Lgjh;

    array-length v0, v0

    goto :goto_3

    .line 106
    :cond_6
    new-instance v3, Lgjh;

    invoke-direct {v3}, Lgjh;-><init>()V

    aput-object v3, v2, v0

    .line 107
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lhfp;->a(Lhfz;)V

    .line 108
    iput-object v2, p0, Lgjf;->f:[Lgjh;

    goto/16 :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2b -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lhfq;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12
    iget-object v0, p0, Lgjf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x1

    iget-object v2, p0, Lgjf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 14
    :cond_0
    iget-object v0, p0, Lgjf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 15
    const/4 v0, 0x2

    iget-object v2, p0, Lgjf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lhfq;->a(ILjava/lang/String;)V

    .line 16
    :cond_1
    iget-object v0, p0, Lgjf;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 17
    const/4 v0, 0x3

    iget-object v2, p0, Lgjf;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 18
    :cond_2
    iget-object v0, p0, Lgjf;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 19
    const/4 v0, 0x4

    iget-object v2, p0, Lgjf;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lhfq;->a(II)V

    .line 20
    :cond_3
    iget-object v0, p0, Lgjf;->e:[Lgjg;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgjf;->e:[Lgjg;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 21
    :goto_0
    iget-object v2, p0, Lgjf;->e:[Lgjg;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 22
    iget-object v2, p0, Lgjf;->e:[Lgjg;

    aget-object v2, v2, v0

    .line 23
    if-eqz v2, :cond_4

    .line 24
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lhfq;->a(ILhfz;)V

    .line 25
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_5
    iget-object v0, p0, Lgjf;->f:[Lgjh;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgjf;->f:[Lgjh;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 27
    :goto_1
    iget-object v0, p0, Lgjf;->f:[Lgjh;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 28
    iget-object v0, p0, Lgjf;->f:[Lgjh;

    aget-object v0, v0, v1

    .line 29
    if-eqz v0, :cond_6

    .line 30
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lhfq;->b(ILhfz;)V

    .line 31
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 32
    :cond_7
    invoke-super {p0, p1}, Lhft;->writeTo(Lhfq;)V

    .line 33
    return-void
.end method
