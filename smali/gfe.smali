.class public Lgfe;
.super Lghm;
.source "PG"


# instance fields
.field public a:Lgfv;

.field public b:Lgfq;

.field private e:Lgga;

.field private f:Landroid/support/design/widget/AppBarLayout$Behavior$a;

.field private g:Lgfn;


# direct methods
.method public constructor <init>(Lgga;Landroid/support/design/widget/AppBarLayout$Behavior$a;Lgfn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lghm;-><init>()V

    .line 2
    invoke-static {p1}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgga;

    iput-object v0, p0, Lgfe;->e:Lgga;

    .line 3
    invoke-static {p2}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/AppBarLayout$Behavior$a;

    iput-object v0, p0, Lgfe;->f:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    .line 4
    invoke-virtual {p0, p3}, Lgfe;->a(Lgfn;)Lgfe;

    .line 5
    invoke-virtual {p0, p4}, Lgfe;->a(Ljava/lang/String;)Lgfe;

    .line 6
    return-void
.end method


# virtual methods
.method public a(Lgfn;)Lgfe;
    .locals 1

    .prologue
    .line 11
    iput-object p1, p0, Lgfe;->g:Lgfn;

    .line 13
    iget-object v0, p1, Lgfn;->a:Ljava/lang/String;

    .line 14
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lgfb$a;->a(Z)V

    .line 15
    return-object p0

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lgfe;
    .locals 0

    .prologue
    .line 16
    invoke-static {p1}, Lgfb$a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lgfe;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lghm;->b(Ljava/lang/String;Ljava/lang/Object;)Lghm;

    move-result-object v0

    check-cast v0, Lgfe;

    return-object v0
.end method

.method public final a()Lgff;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 18
    .line 19
    iget-object v2, p0, Lgfe;->e:Lgga;

    new-instance v3, Lgoc;

    invoke-direct {v3, p0}, Lgoc;-><init>(Lgfe;)V

    .line 20
    invoke-virtual {v2, v3}, Lgga;->a(Lgfv;)Lgfu;

    move-result-object v2

    .line 21
    iget-object v3, p0, Lgfe;->g:Lgfn;

    new-instance v4, Lgge;

    invoke-direct {v4, p0}, Lgge;-><init>(Ljava/lang/Object;)V

    .line 22
    invoke-virtual {v2, v3, v4}, Lgfu;->a(Lgfn;Lgfo;)Lgqj;

    move-result-object v2

    .line 23
    new-instance v3, Lggm;

    iget-object v4, p0, Lgfe;->f:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-direct {v3, v4}, Lggm;-><init>(Landroid/support/design/widget/AppBarLayout$Behavior$a;)V

    invoke-virtual {v2, v3}, Lgqj;->a(Lghu;)Lgqj;

    .line 24
    invoke-virtual {v2, v1}, Lgqj;->a(Z)Lgqj;

    .line 25
    invoke-virtual {v2}, Lgqj;->i()Lgfw;

    move-result-object v2

    .line 27
    iget v3, v2, Lgfw;->b:I

    invoke-static {v3}, Lhcw;->a(I)Z

    move-result v3

    .line 28
    if-eqz v3, :cond_2

    .line 31
    const-class v3, Lgff;

    .line 34
    iget v4, v2, Lgfw;->b:I

    .line 37
    iget-object v5, v2, Lgfw;->d:Lgqj;

    .line 38
    invoke-virtual {v5}, Lgqj;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "HEAD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    div-int/lit8 v5, v4, 0x64

    if-eq v5, v0, :cond_0

    const/16 v5, 0xcc

    if-eq v4, v5, :cond_0

    const/16 v5, 0x130

    if-ne v4, v5, :cond_1

    .line 39
    :cond_0
    invoke-virtual {v2}, Lgfw;->b()V

    move v0, v1

    .line 42
    :cond_1
    if-nez v0, :cond_3

    .line 43
    const/4 v0, 0x0

    .line 45
    :goto_0
    check-cast v0, Lgff;

    return-object v0

    .line 30
    :cond_2
    iget-object v0, p0, Lgfe;->f:Landroid/support/design/widget/AppBarLayout$Behavior$a;

    invoke-static {v0, v2}, Lgfg;->a(Landroid/support/design/widget/AppBarLayout$Behavior$a;Lgfw;)Lgfg;

    move-result-object v0

    throw v0

    .line 44
    :cond_3
    iget-object v0, v2, Lgfw;->d:Lgqj;

    invoke-virtual {v0}, Lgqj;->g()Lghu;

    move-result-object v0

    invoke-virtual {v2}, Lgfw;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v2}, Lgfw;->e()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-interface {v0, v1, v2, v3}, Lghu;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lgfq;)Lgfe;
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lgfe;->b:Lgfq;

    .line 10
    return-object p0
.end method

.method public b(Lgfv;)Lgfe;
    .locals 0

    .prologue
    .line 7
    iput-object p1, p0, Lgfe;->a:Lgfv;

    .line 8
    return-object p0
.end method

.method public synthetic b(Ljava/lang/String;Ljava/lang/Object;)Lghm;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1, p2}, Lgfe;->a(Ljava/lang/String;Ljava/lang/Object;)Lgfe;

    move-result-object v0

    return-object v0
.end method
